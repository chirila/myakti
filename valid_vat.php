<?php
session_write_close();
$vars = array();
$vars['countryCode'] = $_GET['a'];
$vars['vatNumber'] = $_GET['b'];
// ,"exceptions" => true
$client = new SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl", array('trace' => 1));
try{
	$result = $client->checkVat($vars);
	if($result->valid === true){
		if($_GET['c']){
			if(strpos($result->address, "\n") === false){
				$result->address=preg_replace('/[\x00-\x1F\x7F-\xFF]/', " ", $result->address);
				$vies_last_empty=strrpos($result->address," ");	
				$vies_city=substr($result->address,$vies_last_empty);
				$full_left=substr($result->address,0,strrpos($result->address," "));
				$vies_zip_start=strrpos($full_left," ");
				$vies_zip=substr($full_left,$vies_zip_start);
				if(!is_numeric($vies_zip)){
				  $vies_city=$vies_zip.$vies_city;
				  $tmp_zip=strpos($result->address,$vies_city);
				  $full_left=rtrim(substr($result->address,0,$tmp_zip));
				  $vies_zip_start=strrpos($full_left," ");
				  $vies_zip=substr($full_left,$vies_zip_start);
				}
				$vies_address=substr($full_left,0,$vies_zip_start);
				switch($result->countryCode){
					case "RO":
						$result->address=trim($vies_city)."\n".trim($vies_address);
						break;
					case "NL":
						$result->address=" \n".trim($vies_address)."\n".trim($vies_zip)." ".trim($vies_city);
						break;
					default:
						$result->address=trim($vies_address)."\n".trim($vies_zip)." ".trim($vies_city);
						break;
				}
			}
			$response = "valid";
		}
		else{
			echo "true";
		}			
	}else{
		if($_GET['c']){
			$response = "invalid";
		}
		else{
			echo "false";
		}
	}

} catch (SoapFault $sf) {
	// print_r($sf);
    if($_GET['c']){
		$response = "error";
	}
	return;
	
} 
catch (Exception $e) { 
	if($_GET['c']){
		$response = "error";
	}
	return;
}

?>