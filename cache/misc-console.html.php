<div ng-if="console[0]">
	<div class="console showconsole">
		<div>
			<div class="row page-content">
				<div class="col-xs-12">
          <h1 class="title title_underline">Console</h1>
        </div>
				<div class="col-xs-12" ng-repeat="items in console | limitTo : 10">
          <ul class="active_rows orderTable">
            <li class="console_title">
              <h4 >Action: {{::items.actions[0].do}}</h4>
            </li>
            <li class="console_section">
              <div class="row">
                <div class="col-sm-6 " ng-click="items.showLogs=!items.showLogs">
                  <div class="text-primary"><span>Logs:</span> {{::items.logsCount}}</div>
                  <div ng-class="{ 'text-success':items.actionsTotalTimeFloat<=10, 'text-info':items.actionsTotalTimeFloat<=200, 'text-warning':items.actionsTotalTimeFloat>200 }"><span>Speed:</span> {{::items.actionsTotalTime}}</div>
                </div>
                <div class="col-sm-6 " ng-click="items.show=!items.show">
                  <div class="text-primary"><span>Queries:</span> {{::items.queryCount}}</div>
                  <div ng-class="{ 'text-success':items.queryTotalsTimeFloat<=10, 'text-info':items.queryTotalsTimeFloat<=200, 'text-warning':items.queryTotalsTimeFloat>200 }"><span>Time:</span> {{::items.queryTotalsTime}}</div>
                  <div class="text-muted"><span>Duplicates:</span> {{::items.queryTotalsDuplicates}}</div>
                </div>
              </div>
            </li>
            <li ng-repeat="item in items.queries" ng-if="items.show">
              <div class="row">
                <div class="col-sm-12 text-primary"><span>Query:</span> {{::item.sql}}</div>
                <div class="col-sm-12 text-muted"><span>Database:</span> {{::item.database}}</div>
                <div class="col-sm-12" ng-class="{ 'text-success':item.timeFloat<=10, 'text-info':item.timeFloat<=200, 'text-warning':item.timeFloat>200 }"><span>Speed:</span> {{::item.time}}</div>
                <div class="col-sm-12 text-muted"><span>File:</span> {{::item.file}} <span>at Line:</span> {{::item.line}}</div>
              </div>
            </li>
            <li ng-repeat="itemL in items.logs" ng-if="items.showLogs">
              <div class="row" >
                <div class="col-sm-12 text-primary"><span>Data: {{::itemL.type}}</span> <pre ng-class="{ 'bg-info':itemL.type=='in', 'bg-success':itemL.type=='session', 'bg-danger':itemL.type=='error', 'bg-warning':(itemL.type=='memory' || itemL.type=='speed'), 'bg-primary':itemL.type=='log' }">{{::itemL.data}}</pre></div>
              </div>
            </li>
          </ul>
				</div>
			</div><!-- /.page-content -->
		</div>
	</div>
</div>