<div class="modal-header">
  	<button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="setStock.cancel()"><span aria-hidden="true">&times;</span></button>
  	<h4 class="modal-title"><?php ob_start(); ?>Dispatch Address<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h4>
</div>
<div class="modal-body">
<div class="col-sm-12 col-bottom col-top">
      <form class="form-horizontal text-left" name="tresh_form" no-validate=''>
       <table class="table active_rows orderTable">
              <thead>
                <tr>
                 <th class="col-sm-5" ><strong><?php ob_start(); ?>Depot<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></strong></th>
                  <th class="col-sm-5" ><strong><?php ob_start(); ?>Address<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></strong></th>
                  
                    <th class="text-center col-sm-2" ><strong><?php ob_start(); ?>Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></strong></th>
                </tr>
              </thead> 
         
      <tbody>
                <tr ng-repeat="item in setStock.item.query" class="collapseTr">
                <td class="expand_row text-muted ng-cloak stc_reason">
                  <span class="glyphicon glyphicon-plus btn-expand" ng-click="setStock.listMore($index)" ng-if="item.require_reason_stock_modif"></span>
                  <span class="glyphicon glyphicon-minus btn-expand hidden" ng-click="setStock.listLess($index)"></span>{{::item.naming}}</td>
                <td class="text-muted ng-cloak">{{::item.address}},{{::item.zip}},{{::item.city}}<br/>{{::item.country}}
                  <div class="form-group stk_reason" ng-class="{ 'form-group_alert' : tresh_form['edit_reason_stock_id_'+$index].$invalid && !tresh_form['edit_reason_stock_id_'+$index].$pristine }">
                    <selectize ng-if="!setStock.item.view_mode && setStock.showTxt['stock_reason_'+$index]" config="setStock.extraCfg" name="edit_reason_stock_id_{{$index}}" options="setStock.item.edit_reason_stock_dd" ng-model="item.edit_reason_stock_id" table="pim_stock_edit_reasons" model="edit_reason_stock_id" opts="edit_reason_stock_dd" index="{{$index}}" required></selectize>
                    <div class="alert alert-sm alert-danger" ng-if="tresh_form['edit_reason_stock_id_'+$index].$invalid && setStock.alertsField['edit_reason_stock_id_'+$index]">{{setStock.alertsField['edit_reason_stock_id_'+$index]}}</div>
                  </div>  

                </td>
                <td class="col-adjust-itext-mutednput text-nowrap" ng-class="{'text-center': setStock.item.view_mode}">
                  <input ng-if="!setStock.item.view_mode" type="text" ng-model="item.stock"  size="7" class="form-control"  display-nr ng-change="setStock.checkStockValue(item,$index)" />
                  <span ng-if="setStock.item.view_mode">{{::item.stock}}</span>
                </td>
                
                </tr>

          </tbody>       
  </table>    
      


     <?php ob_start(); ?>dispatch to patners:<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <b class="ng-cloak">{{::setStock.item.extern_dispatch}}</b> <?php ob_start(); ?>articles<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> 
      </form>
</div>	
</div>
<div class="modal-footer">
      <button class="btn btn-plain" type="button" ng-click="setStock.cancel()"><?php ob_start(); ?>Cancel<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
    	<button ng-if="!setStock.item.view_mode" class="btn btn-default" type="button" ng-click="setStock.ok(tresh_form)"><?php ob_start(); ?>Save<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
</div>