<div class="container">
    <div class="page-header">
        <div class="row">
            <div class="col-xs-8 col-sm-6">
                <h1><?php ob_start(); ?>Inventory<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
            </div>
            <div class="col-xs-4 col-sm-6 text-right" ng-if="show_settings.is_admin_stock">
                <div class="dropdown new-settings">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li class="dropdown-item" ng-class="{ 'current':menu.submenu == 'stock_setting' }"><a href="StockSettings/" go-to="" state="stockSettings"><span class="fas fa-cog text-primary"></span> <?php ob_start(); ?>Stock Settings<?php echo $this->vars->enl;  ?></a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <div class="tabs-navigation">
        <ul class="nav nav-tabs">
            <li>
                <a href="stock_info/" go-to="" state="stock_info"><?php ob_start(); ?>Transaction Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_article/" go-to="" state="stock_article"><?php ob_start(); ?>Article Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li class="active">
                <a href="stock_batches/" go-to="" state="stock_batches"><?php ob_start(); ?>Based on batches<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_serial/" go-to="" state="stock_serial"><?php ob_start(); ?>Based on serial numbers<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_customer/" go-to="" state="stock_customer"><?php ob_start(); ?>Customer/Location Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_date/" go-to="" state="stock_date"><?php ob_start(); ?>Stock by Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row page-search page-search-options">
                    <form>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 col-search-input">
                                    <div class="so-field" ng-class="{ 'so-active':search.search!='' }">
                                        <span class="glyphicon glyphicon-search so-icon"></span>
                                        <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Search<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" search-it doit="search" callbk="renderList" ng-model="search.search">
                                        <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('search','')"></span>
                                    </div>
                                    
                                    <!-- /.input-search -->
                                </div>
                            
                            </div>
                        </div>
                        <div class="page-search-button">
                            <div class="btn-group export_list" uib-dropdown>
                                <button type="button" class="btn btn-tertiary dropdown-toggle" uib-dropdown-toggle>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                    <li><a href="index.php?do=stock-export_batches" target="_blank" ng-click="createExports($event)"><span class="fas fa-upload text-primary"></span> <?php ob_start(); ?>Export<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                </ul>
                            </div>
                         </div>
                    </form>
                </div>
                <!-- /.page-search -->
                <div class="row page-content">
                    <div class="col-xs-12">
                        <table class="table active_rows orderTable">
                            <thead>
                                <tr>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='batch_number') , 'sortable_row-down':(reverse && ord=='batch_number') }" ng-click="orderBY('batch_number')"><?php ob_start(); ?>Batch Number<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='item_code') , 'sortable_row-down':(reverse && ord=='item_code') }" ng-click="orderBY('item_code')"><?php ob_start(); ?>Code<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-3 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='internal_name') , 'sortable_row-down':(reverse && ord=='internal_name') }" ng-click="orderBY('internal_name')"><?php ob_start(); ?>Internal Name<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='in_stock') , 'sortable_row-down':(reverse && ord=='in_stock') }" ng-click="orderBY('in_stock')"><?php ob_start(); ?>In Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='date_exp') , 'sortable_row-down':(reverse && ord=='date_exp') }" ng-click="orderBY('date_exp')"><?php ob_start(); ?>Expiration date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-1 col-select">
                                        <div class="dropdown order_export">
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-square-o" ng-if="!all_pages_selected && !minimum_selected"></i>
                                            <i class="fas fa-minus-square" ng-if="!all_pages_selected && minimum_selected"></i>
                                            <i class="fas fa-check-square" ng-if="all_pages_selected"></i>
                                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#" ng-click="checkAllPage()"><?php ob_start(); ?>Select page<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                                <a class="dropdown-item" href="#" ng-if="!all_pages_selected" ng-click="checkAllPages()"><?php ob_start(); ?>Select all<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                                <a class="dropdown-item" href="#" ng-if="all_pages_selected || minimum_selected" ng-click="uncheckAllPages()"><?php ob_start(); ?>Select none<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>                      
                                            </div>
                                        </div>
                                    </th>
                                    <th class="table_actions">
                                        <!-- <div class="btn-group export_list" uib-dropdown>
                                            <button type="button" class="btn btn-tertiary dropdown-toggle" uib-dropdown-toggle>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                                <li><a href="index.php?do=stock-export_batches" target="_blank" ng-click="createExports($event)"><span class="fas fa-upload text-primary"></span> <?php ob_start(); ?>Export<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                            </ul>
                                        </div> -->
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in list">
                                    <td class="text-muted" >
                                        {{::item.batch_number}}
                                    </td>
                                    <td class="text-muted">
                                        {{::item.code}}
                                    </td>
                                    <td class="text-muted">
                                        {{::item.name}}
                                    </td>
                                    <td class="text-muted">
                                        {{::item.in_stock}}
                                    </td>
                                    <td class="text-muted">
                                        {{::item.date_exp}}
                                    </td>
                                    <td class="col-select">
                                        <div class="checkbox checkbox-primary">
                                            <input type="checkbox" id="checkbox_all_{{$index}}" ng-model="item.check_add_to_product" ng-click="markCheckNew(item)">
                                            <label for="checkbox_all_{{$index}}"></label>
                                        </div>
                                    </td>
                                    <td class="table_actions">
                                        <div class="btn-group" uib-dropdown>
                                            <button type="button" class="btn btn-plain dropdown-toggle" uib-dropdown-toggle>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                                <li class="dropdown-header"><?php ob_start(); ?>Action<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></li>
                                                <li>
                                                    <a class="dropdown-item bat_edit" href="#" ng-click="openModal('editBatchNo', item.id)"><span class="fas fa-pen text-primary"></span><?php ob_start(); ?>Edit<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.page-content -->
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-8">
            <ul ng-if="max_rows > 30" uib-pagination total-items="max_rows" ng-model="search.offset" ng-change="searchThing()" max-size="5" class="pager" boundary-links="true" items-per-page="lr"></ul>
        </div>
        <div class="col-sm-4 text-right">
            <label><?php ob_start(); ?>Number of batches based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>: <strong>{{max_rows}}</strong></label>
        </div>
    </div>
</div>
<!-- neede on all pages -->