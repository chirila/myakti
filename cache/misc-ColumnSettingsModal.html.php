<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="vm.cancel()"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?php ob_start(); ?>Edit column settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h4>
</div>
<div class="modal-body">
    <div class="loading_wrap loading_alt hidden">
        <div class="loading"></div>
        <div class="spinner"></div>
    </div>
    <!-- <div uib-alert ng-repeat="alert in vm.alerts" type="{{alert.type}}" close="vm.alerts.splice($index, 1);" dismiss-on-timeout="3500" ng-class="['alert-' + (alert.type || 'warning') ]">{{alert.msg}}</div> -->
    <div class="col-sm-12">
        <div class="col-sm-6">
            <h2><b><?php ob_start(); ?>Available columns<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></b></h2>
            <ul class="table table_orders sort_column drag_table" role="tablist" data-as-sortable="vm.dragControlListeners" data-ng-model="vm.obj.available">
                <li role="presentation" data-ng-repeat="col in vm.obj.available" data-as-sortable-item>
                    <div class="row" data-as-sortable-item-handle>
                        <div class="row_handler">
                            <span class="btn-drag drop">&nbsp;</span>
                        </div>
                        <div class="col-sm-12">{{col.name}}</div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-sm-6">
            <h2><b><?php ob_start(); ?>Selected columns<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></b></h2>
            <ul class="table table_orders sort_column drag_table" role="tablist" data-as-sortable="vm.dragControlListeners" data-ng-model="vm.obj.selected">
                <li role="presentation" data-ng-repeat="col in vm.obj.selected" data-as-sortable-item>
                    <div class="row" data-as-sortable-item-handle>
                        <div class="row_handler">
                            <span class="btn-drag drop">&nbsp;</span>
                        </div>
                        <div class="col-sm-12">{{col.name}}</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-plain" type="button" ng-click="vm.cancel()"><?php ob_start(); ?>Cancel<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
    <button class="btn btn-default" ng-class="{ 'add_disabled' : !vm.obj.selected.length }" type="button" ng-click="vm.ok()" uib-popover="<?php ob_start(); ?>You need to select at least 1 column<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" popover-trigger="'mouseenter'" popover-placement="'left-top'" popover-enable="(!vm.obj.selected.length ? true : false)"><?php ob_start(); ?>Save<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
</div>