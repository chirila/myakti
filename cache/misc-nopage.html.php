    <div class="vertical-allign col-md-12">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="row">
                    <div class="col-md-12 margin-bottom error_img">
                        <img src="images/error_new.png" alt="error">
                    </div>
                    <div class="error_text text-center">
                        <h2><?php ob_start(); ?>Sorry, the page you're looking for can't be displayed<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?><span>.</span></h2>
                        <h2><?php ob_start(); ?>It may have been deleted or you don't have permission to view it<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?><span>.</span></h2>
                        <h2><?php ob_start(); ?>Go back to<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?><a href="customers/" go-to="" state="customers" class="text-link"> <?php ob_start(); ?>Dashboard<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a><span>.</span></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>