<div class="col-md-4 sidebar-login">
    <div class="row">
        <div class="col-md-5"><img src="images/logo.png" alt="Akti" class="jefact-none">
			<img src="images/logo-je.png" alt="logoje" class="akti-none">
		</div>
        <div class="col-md-7"></div>
        <div class="col-md-12 text-center middle-text">
            <h1><?php ob_start(); ?>Welcome back!<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
            <h2><?php ob_start(); ?>Ready to simplify your administration?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h2>
            <p class="jefact-none"><?php ob_start(); ?>Don't have an account yet?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></p>
            <a class="btn btn-default btn-sm jefact-none" href="https://www.akti.com/free-trial/"><?php ob_start(); ?>Try Now<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
        </div>
    	<div class="col-md-6 bottom-stripe"><img src="images/stripe-transparent.png" alt="Akti stripe" class="jefact-none"></div>
    	<div class="col-md-6"></div>
    </div>
</div>
<div class="page-login-vertical col-md-8">
		<div class="page-login-box <?php echo $this->vars->LOGIN_SCREEN;  ?>">
				<div class="row">
						<div class="col-xs-12">
								<?php if($this->vars->IS_FORGET) { ?>
									<h1 class="text-center txt-blue jefact-none"><?php ob_start(); ?>Recover your Akti password<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
									<h1 class="text-center txt-blue akti-none"><?php ob_start(); ?>Recover your password<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
								<?php  } else {  ?>
									<h1 class="text-center txt-blue jefact-none"><?php ob_start(); ?>Sign in to your Akti account<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
									<h1 class="text-center txt-blue akti-none"><?php ob_start(); ?>Sign in to your account<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
								<?php  }  ?>						
							</div>
					</div>
			
					<div class="row">
						<div class="col-xs-12 page-login-panel">
								<!-- <div uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="alerts.splice($index, 1);" dismiss-on-timeout='3000' ng-class="['alert-' + (alert.type || 'warning') ]"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>{{alert.msg}}</div> -->
								
									<form class="form-horizontal text-left" action="index.php" method="post">
										<input type="hidden" name="do" value="<?php echo $this->vars->do_next;  ?>" id="do" />
								        <input type="hidden" name="forget" value="<?php echo $this->vars->FORGOT;  ?>"  />
								        <input type="hidden" name="reset_pass" value="<?php echo $this->vars->RESET_PASS;  ?>"  />
								        <input type="hidden" name="recover" value="<?php echo $this->vars->RECOVER;  ?>"  />
								        <input type="hidden" name="timezone_offset" value="" id="timezone_offset" />
								        <input type="hidden" name="crypt" value="<?php echo $this->vars->CRYPT;  ?>"  />
										   <input type="hidden" name="source" value="<?php echo $this->vars->source;  ?>"  />
								        <!-- <input type="hidden" id="error_message" value="<?php 	echo msg::$error;  ?>"> -->

										<div class="form-group <?php echo $this->vars->HIDE_INPUT3;  ?>">
												<div class="col-xs-12">
														<span class="page-login-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
														<input autofocus type="text" class="form-control input-lg" id="username" value="<?php echo htmlspecialchars(html_entity_decode($in['username']));  ?>" name="username" placeholder="<?php ob_start(); ?>Email address<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>">
													</div>                          	
											</div>
										<div class="form-group <?php echo $this->vars->HIDE_INPUT3;  ?>">
												<div class="col-xs-12">
														<span class="page-login-icon"><i class="fa fa-lock" aria-hidden="true"></i></span>
														<input type="password" class="form-control input-lg page-login-password" id="password" value="" name="password" ng-model="login.password" placeholder="<?php ob_start(); ?>Password<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>">
													</div>
											</div>

										<div class="form-group <?php echo $this->vars->HIDE_L;  ?>">
												<div class="col-xs-12">
														<span class="page-login-icon"><i class="far fa-envelope" aria-hidden="true"></i></span>
														<input type="status" class="form-control input-lg page-login-status" id="status" value="<?php echo $this->vars->email;  ?>" name="email" placeholder="<?php ob_start(); ?>Email<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>">
													</div>                          	
											</div>

								     	<!-- <div class="form-group <?php echo $this->vars->HIDE_L;  ?>" >
												<div class="col-xs-12">
													<span class="forgot_text"><?php ob_start(); ?>Please enter your email below and we will send you instructions for resetting your password.<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span>
												</div>
											</div> -->
									<?php if($this->vars->msg_error) { ?>
										<div class="alert alert-dismissible alert-danger" role="alert">
										  <span ><?php 	echo msg::$error;  ?></span>
										  <!-- <button type="button" class="close login_close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
										</div>
									<?php  }  ?>
									<?php if($this->vars->msg_success) { ?>
										<div class="alert alert-success" role="alert" dismiss-on-timeout='3000'>
										  <span ><?php 	echo msg::$success;  ?></span>
										  <!-- <button type="button" class="close login_close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
										</div>
									<?php  }  ?>
										<div class="form-group page-login-actions" >
												<div class="col-xs-7">
														<div class="checkbox checkbox-primary <?php echo $this->vars->HIDE_INPUT3;  ?>">
															<input type="checkbox" id="remember" name ="remember" <?php echo $this->vars->CHECKED;  ?> value="<?php echo $this->vars->remember;  ?>"> 
															<label for="remember">																
															</label>
															<span ><?php ob_start(); ?>Remember me<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span>
														</div>
												</div>
												<div class="col-xs-7 col-top <?php echo $this->vars->HIDE_L;  ?>">
													<a href="index.php?<?php echo $this->vars->source_param;  ?>" class="forget <?php echo $this->vars->HIDE_L;  ?>"><?php ob_start(); ?>Back to Login<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>?</a>
												</div>
												<div class="col-xs-5 text-right <?php echo $this->vars->HIDE_INPUT3;  ?>">
														<button type="submit" class="btn btn-lg btn-default" ><?php ob_start(); ?>Sign in<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
														<div class="<?php echo $this->vars->HIDE_INPUT3;  ?>">
												            <a href="index.php?forget=1<?php echo $this->vars->source_param;  ?>"><?php ob_start(); ?>Forgot your password<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>?</a>
												          </div> 
												</div>
												<div class="col-xs-5 text-right <?php echo $this->vars->HIDE_L;  ?>">
															<button type="submit" class="btn btn-lg btn-default" ><?php ob_start(); ?>Reset Password<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
												</div>
											</div>
						
									</form>
										<div class="row <?php echo $this->vars->HIDE_INPUT3;  ?> jefact-none">
											<div class=" col-xs-12 text-center MyID_signin">
												<h1><?php ob_start(); ?>OR<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
												<a href="<?php echo $this->vars->signin_kc_link;  ?>" class="btn btn-lg btn-default MyID"><?php ob_start(); ?>Sign in with MyID<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>		
											</div>
										</div>
							</div>
<!-- 						<div class="col-xs-12 page-login-panel page-login-banner text-right">
									<?php ob_start(); ?>Not yet registered?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> 
									<a href="https://akti.com/my/registration" ng-if="login.lang=='en'"><?php ob_start(); ?>Start Free Trial<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
									<a href="https://akti.com/fr_BE/my/inscription" ng-if="login.lang=='fr'"><?php ob_start(); ?>Start Free Trial<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
									<a href="https://akti.com/nl_BE/my/registratie" ng-if="login.lang=='nl'"><?php ob_start(); ?>Start Free Trial<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
							</div>  -->
					</div>
			</div>
			<div class="page-login-box forgot_screen <?php echo $this->vars->FORGOT_SCREEN;  ?>">
                <div class="row">
					<div class="col-xs-12">
						<h1 class="text-center txt-blue"><?php ob_start(); ?>Recover your Akti password test<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
					</div>
					</div>
                <div class="row">
                	<div class="col-xs-12 page-login-panel forgot forgot_screen">
                      <!-- <h1 class="text-center"><a href="http://akti.com"><img src="images/email_pr.png" /></a></h1>
                       <h2 class="text-center"><?php ob_start(); ?>Help is on the way<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h2>
                       <p><?php ob_start(); ?>Akti loves to help you<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>.</p>
                       <p><?php ob_start(); ?>You'll get an email at your address at<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></p>
                       <a href=""><?php echo $this->vars->email;  ?></a>
                       <p><?php ob_start(); ?>with further instructions.<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></p>
                       <p class="last_p"><?php ob_start(); ?>If nothing happens in the next 15 minutes<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>,</p><p><?php ob_start(); ?>please check your spam folder<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>.</p> -->
                       <p class="color-orange"><?php ob_start(); ?>Further instructions to reset your password have been sent to your email address.<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></p>
                    </div>
                    <div class="col-xs-12 page-login-panel forgot forgot_screen back">
                  <a href="#" ng-click="ls.back(ls.lang)"><?php ob_start(); ?>Back to login<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>?</a>
                  </div>
             </div>
       </div>
</div>

<script type="text/javascript">
	 $(document).ready(function () {
	 	$('#remember').click(function(){
		    var remember_me = $('#remember').val();
		    if(remember_me == 0){ 
		        $('#remember').val(1);
		    }else{    
		        $('#remember').val(0);
		    }
  		});
});
</script>
