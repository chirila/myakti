<div class="container">
    <div class="page-header">
        <div class="row"></div>
    </div>
    <div class="tabs-navigation">
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row page-content col-bottom col-top" id="no_css-companies_kpis">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="revenue">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 revenue_crm"><span class="full_wd"><span ng-if="!vm.search.vat_included" ng-bind-html="vm.kpis.revenue_this_year"></span><span ng-if="vm.search.vat_included" ng-bind-html="vm.kpis.revenue_this_year_with_vat"></span></span><span class="full_wdb"><?php ob_start(); ?>Revenue this year<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 revenue_crm"><span class="full_wd"><span ng-if="!vm.search.vat_included" ng-bind-html="vm.kpis.revenue_this_month"></span><span ng-if="vm.search.vat_included" ng-bind-html="vm.kpis.revenue_this_month_with_vat"></span></span><span class="full_wdb"><?php ob_start(); ?>Revenue this month<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 revenue_crm"><span class="full_wd"><span ng-if="!vm.search.vat_included" ng-bind-html="vm.kpis.outstanding"></span><span ng-if="vm.search.vat_included" ng-bind-html="vm.kpis.outstanding_with_vat"></span></span><span class="full_wdb"><?php ob_start(); ?>Outstanding Invoices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 revenue_crm last"><span class="revenue_red full_wd"><span ng-if="!vm.search.vat_included" ng-bind-html="vm.kpis.overdue"></span><span ng-if="vm.search.vat_included" ng-bind-html="vm.kpis.overdue_with_vat"></span></span><span class="full_wdb"><?php ob_start(); ?>Overdue Invoices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span>
                                            <div class="row col-md-12 ">
                                                    <div class="row">
                                                    <label class="first_lab"><?php ob_start(); ?>VAT excl<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                                                    <label class="switch pull-left small_margin sec_lab">
                                                    <input type="checkbox" id="" ng-model="vm.search.vat_included" class="switch-primary ng-pristine ng-untouched ng-valid" />
                                                    <span class="slider round"></span>
                                                    </label>
                                                    <label class="t_lab"><?php ob_start(); ?>VAT incl<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                                                 </div>
                                            </div>                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 shadow" ng-repeat="item in vm.graphs" ng-if="item.show">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <h3 class="text-muted col-bottom">{{item.name}}</h3>
                                    </div>
                                    <div class="col-sm-4 col-top text-right" ng-show="$last">
                                    </div>
                                </div>
                                <canvas class="chart chart-line" chart-type="vm[item.option].chart_type" chart-dataset-override="vm[item.option].datasetOverride" chart-series="vm[item.option].series" chart-data="vm[item.option].data" chart-labels="vm[item.option].labels" chart-options="vm[item.option].opt"
                                    chart-colors="vm[item.option].colors">
                            </div>           
                        </div>
                    </div>                
                </div><!-- /.page-content -->
            </div>
        </div>
    </div>
    <!-- /.page-content -->
</div><!-- neede on all pages -->