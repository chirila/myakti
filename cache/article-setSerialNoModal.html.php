<div class="modal-header">
  	<button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="setSerialNo.cancel()"><span aria-hidden="true">&times;</span></button>
  	<h4 class="modal-title"><?php ob_start(); ?>Add Serial Number<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h4>
</div>
<div class="modal-body">


<form class="form-horizontal text-left ng-pristine ng-valid" name="serial_no_form" novalidate="">


<div class="form-group">

<div class="col-sm-10" class="ng-clock">   
<textarea id="serial_number" class="form-control ng-pristine ng-valid ng-touched" rows="5" type="text" ng-model="setSerialNo.obj.serial_number" placeholder="<?php ob_start(); ?>Paste your serial numbers here<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"></textarea></div>
</div>


 
      
</form>



</div>	

<div class="modal-footer">
      <button class="btn btn-plain" type="button" ng-click="setSerialNo.cancel()"><?php ob_start(); ?>Cancel<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
    	<button class="btn btn-default" type="button" ng-click="setSerialNo.ok(serial_no_form)"><?php ob_start(); ?>Save<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
</div>