<?php if($this->vars->hide_them_all) { ?>
<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <a class="logo" href="<?php echo $this->vars->default_pag;  ?>/" title="Dashboard">
               
                <img src="images/logo.png" alt="Akti" class="jefact-none">
                <img src="images/logo-je.png" alt="jef" class="jef akti-none" >
                <img src="images/akti-icon_small.png" width="32" height="32" alt="Akti" class="mini-logo jefact-none">
                <img src="images/logo-je.png" width="32" height="32" alt="Akti" class="mini-logo akti-none">
            </a>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <span class="icons-bottom">
                        <i class="fas fa-chevron-right" uib-tooltip="<?php ob_start(); ?>Expand<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" tooltip-placement="right"></i>
                        <i class="fas fa-chevron-left" uib-tooltip="<?php ob_start(); ?>Collapse<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" tooltip-placement="right"></i>
                    </span>
                </button>
            </div>
        </nav>
        <ul class="list-unstyled components" id="navigation-menu">
            <?php if($this->vars->is_crm) { ?>
            <li id="sidebar-item" class="navigation-link modul_1 <?php echo $this->vars->hide_crm;  ?>" ng-class="{ 'navigation-current':menu.menu == 'customers', 'hidden':!main_menu[1] && !main_menu[5] && !main_menu[11] && !main_menu['DEALS']}">
                <a href="{{ !main_menu[1] ? (!main_menu['DEALS'] ? (!main_menu[5] ? 'contracts' : 'quotes') : 'deals') : 'customers' }}/" go-to="" state="{{ !main_menu[1] ? (!main_menu['DEALS'] ? (!main_menu[5] ? 'contracts' : 'quotes') : 'deals') : 'customers' }}"><i class="fas fa-user"></i><span><?php ob_start(); ?>CRM<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a href="{{ !main_menu[1] ? (!main_menu['DEALS'] ? (!main_menu[5] ? 'contracts' : 'quotes') : 'deals') : 'customers' }}/" go-to="" state="{{ !main_menu[1] ? (!main_menu['DEALS'] ? (!main_menu[5] ? 'contracts' : 'quotes') : 'deals') : 'customers' }}"><span><?php ob_start(); ?>CRM<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <?php if($this->vars->is_1) { ?>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'customers' || menu.submenu == 'smartList' || menu.submenu == 'setting' || menu.submenu == 'import', 'hidden':!main_menu[1] }">
                        <a href="customers/" go-to="" state="customers"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Accounts<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem" ng-class="{ 'current': menu.submenu == 'contacts' || menu.submenu == 'setting' || menu.submenu == 'import' || menu.submenu == 'smartListContact', 'hidden':!main_menu[1] }">
                        <a href="contacts/" go-to="" state="contacts"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Contacts<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?> <?php if($this->vars->adv_crm) { ?><?php if($this->vars->not_easyinv_subscription) { ?><?php if($this->vars->not_easy_invoice_diy) { ?>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'deals' || menu.submenu == 'dealsSetting', 'hidden':!main_menu['DEALS']}">
                        <a go-to="" href="deals/" state="deals"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Deals<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?><?php  }  ?><?php  }  ?> <?php if($this->vars->is_5) { ?><?php if($this->vars->not_easy_invoice_diy) { ?>
                    <li class="sidebar-subitem module_5 <?php echo $this->vars->hide_5;  ?>" ng-class="{ 'current':menu.submenu == 'quotes' || menu.submenu == 'quotesTemplates' || menu.submenu == 'quotes setting', 'hidden':!main_menu[5] }">
                        <a go-to="" href="quotes/" state="quotes"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Quotes<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?><?php  }  ?> <?php if($this->vars->is_11) { ?><?php if($this->vars->not_easy_invoice_diy) { ?>
                    <li class="sidebar-subitem module_11 <?php echo $this->vars->hide_11;  ?>" ng-class="{ 'current':menu.submenu == 'contracts' || menu.submenu == 'contractTemplates' || menu.submenu == 'contracts setting', 'hidden':!main_menu[11] }">
                        <a go-to="" href="contracts/" state="contracts"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Contracts<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?><?php  }  ?>
                </ul>
            </li><?php  }  ?> <?php if($this->vars->showcataloginv) { ?>
            <li id="sidebar-item" class="navigation-link module_12 <?php echo $this->vars->hide_catalogue;  ?>" ng-class="{ 'navigation-current':menu.menu == 'articles', 'hidden':!main_menu[12] }">
                <a href="<?php echo $this->vars->link;  ?>/" go-to="" state="<?php echo $this->vars->link;  ?>"><i class="fas fa-book"></i><span><?php ob_start(); ?>Catalogue<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a  href="<?php echo $this->vars->link;  ?>/" go-to="" state="<?php echo $this->vars->link;  ?>"><span><?php ob_start(); ?>Catalogue<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <?php if($this->vars->is_12) { ?>
                    <li id="articles" ng-class="{ 'current':menu.submenu == 'articles' || menu.submenu == 'articles setting' || menu.submenu == 'import' }" class="sidebar-subitem">
                        <a go-to="" href="articles/" state="articles"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Articles<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?>
                    <li id="aservices" ng-class="{ 'current':menu.submenu == 'aservices' }" class="sidebar-subitem">
                        <a go-to="" href="aservices/" state="aservices"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Services<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                </ul>
            </li><?php  }  ?> <?php if($this->vars->is_od) { ?>
            <li id="sidebar-item" class="navigation-link module_6 <?php echo $this->vars->hide_od;  ?>" ng-class="{ 'navigation-current':menu.menu == 'orders', 'hidden':!main_menu[6] && !main_menu[14] }">
                <a href="{{ !main_menu[6] ? 'purchase_orders' : 'orders' }}/" go-to="" state="{{ !main_menu[6] ? 'purchase_orders' : 'orders' }}"><i class="fas fa-shopping-cart"></i><span><?php ob_start(); ?>Orders & Deliveries<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a href="{{ !main_menu[6] ? 'purchase_orders' : 'orders' }}/" go-to="" state="{{ !main_menu[6] ? 'purchase_orders' : 'orders' }}"><span><?php ob_start(); ?>Orders & Deliveries<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <?php if($this->vars->is_6) { ?>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'orders' || menu.submenu == 'orders setting' || menu.submenu == 'order_deliveries' || menu.submenu == 'orders_deliver','hidden':!main_menu[6] }">
                        <a href="orders/" go-to="" state="orders"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Sales Orders<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?> <?php if($this->vars->is_14) { ?>
                    <li class="sidebar-subitem module_14 <?php echo $this->vars->hide_14;  ?>" ng-class="{ 'current':menu.submenu == 'purchase_orders' || menu.submenu == 'purchase_order_deliveries' || menu.submenu == 'stock_low' || menu.submenu == 'to_order' || menu.submenu == 'po_order_setting setting', 'hidden':!main_menu[14] }">
                        <a href="purchase_orders/" go-to="" state="purchase_orders"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Purchase Orders<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?>
                </ul>
            </li><?php  }  ?> <?php if($this->vars->is_fs) { ?>
            <li id="sidebar-item" class="navigation-link module_13 <?php echo $this->vars->hide_13;  ?>" ng-class="{ 'navigation-current':menu.menu == 'interventions', 'hidden':!main_menu[13] && !main_menu[17] }">
                <a href="{{ !main_menu[13] ? 'installations' : 'services' }}/" go-to="" state="{{ !main_menu[13] ? 'installations' : 'services' }}"><i class="fas fa-wrench"></i><span><?php ob_start(); ?>Field Services<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a href="{{ !main_menu[13] ? 'installations' : 'services' }}/" go-to="" state="{{ !main_menu[13] ? 'installations' : 'services' }}"><span><?php ob_start(); ?>Field Services<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <?php if($this->vars->is_13) { ?>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'intervention' || menu.submenu == 'rec_services' || menu.submenu == 'planning' || menu.submenu == 'teams' || menu.submenu == 'serviceModels' || menu.submenu == 'intervention setting', 'hidden':!main_menu[13] }">
                        <a href="services/" go-to="" state="services"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Interventions<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?> <?php if($this->vars->is_17) { ?>
                    <li class="sidebar-subitem module_17 <?php echo $this->vars->hide_17;  ?>" ng-class="{ 'current':menu.submenu == 'installations' || menu.submenu == 'install_fields' || menu.submenu == 'install_templates' || menu.submenu == 'installation setting', 'hidden':!main_menu[17] }">
                        <a href="installations/" go-to="" state="installations"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Installations<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?>
                </ul>
            </li><?php  }  ?> <?php if($this->vars->is_16) { ?>
            <li id="sidebar-item" class="navigation-link module_16 <?php echo $this->vars->hide_16;  ?>" ng-class="{ 'navigation-current':menu.menu == 'stock_info', 'hidden':!main_menu[16] && !main_menu['DISPATCHING'] }">
                <a href="{{ (!main_menu[16] ? 'stock_dispatching' : 'stock_info') }}/" go-to="" state="{{ (!main_menu[16] ? 'stock_dispatching' : 'stock_info') }}"><i class="fas fa-cubes"></i><span><?php ob_start(); ?>Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a href="{{ (!main_menu[16] ? 'stock_dispatching' : 'stock_info') }}/" go-to="" state="{{ (!main_menu[16] ? 'stock_dispatching' : 'stock_info') }}"><span><?php ob_start(); ?>Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'stock_info' || menu.submenu == 'stockSettings','hidden':!main_menu[16] }">
                        <a href="stock_info/" go-to="" state="stock_info"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Inventory<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'stock_dispatching','hidden':!main_menu['DISPATCHING'] }">
                        <a href="stock_dispatching/" go-to="" state="stock_dispatching"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Dispatching<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                </ul>
            </li><?php  }  ?> <?php if($this->vars->is_4) { ?>
            <li id="sidebar-item" class="navigation-link module_4" ng-class="{ 'navigation-current':menu.menu == 'invoices', 'hidden':!main_menu[4] && !main_menu['PURCHASE_INVOICE'] && !main_menu['INVOICE_EXPORTS'] }">
                <a href="{{ !main_menu[4] ? (!main_menu['PURCHASE_INVOICE'] ? 'export_apps' : 'purchase_invoices') : 'invoices' }}/" go-to="" state="{{ !main_menu[4] ? (!main_menu['PURCHASE_INVOICE'] ? 'export_apps' : 'purchase_invoices') : 'invoices' }}"><i class="fas fa-euro-sign"></i><span><?php ob_start(); ?>Invoicing<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a href="{{ !main_menu[4] ? (!main_menu['PURCHASE_INVOICE'] ? 'export_apps' : 'purchase_invoices') : 'invoices' }}/" go-to="" state="{{ !main_menu[4] ? (!main_menu['PURCHASE_INVOICE'] ? 'export_apps' : 'purchase_invoices') : 'invoices' }}"><span><?php ob_start(); ?>Invoicing<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'invoices' ||  menu.submenu == 'recinvoices' || menu.submenu == 'reminders' || menu.submenu == 'sepa' || menu.submenu == 'payments' || menu.submenu == 'invoices setting' || menu.submenu == 'import', 'hidden':!main_menu[4] }">
                        <a go-to="" href="invoices/" state="invoices"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Sales Invoices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <?php if($this->vars->not_easyinv_subscription) { ?><?php if($this->vars->ERP) { ?><?php if($this->vars->not_easy_invoice_diy) { ?>
                    <li class="sidebar-subitem" ng-class="{ 'current': menu.submenu == 'purchase_invoices', 'hidden':!main_menu['PURCHASE_INVOICE']}">
                        <a go-to="" href="purchase_invoices/" state="purchase_invoices"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Bills<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?><?php  }  ?><?php  }  ?>
                    <li class="sidebar-subitem" ng-class="{ 'current': menu.submenu == 'exports', 'hidden':!main_menu['INVOICE_EXPORTS']}">
                        <a go-to="" href="export_apps/" state="export_apps"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Export<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                </ul>
            </li><?php  }  ?> <?php if($this->vars->is_19) { ?>
            <li id="sidebar-item" class="navigation-link module_19 <?php echo $this->vars->hide_19;  ?>" ng-class="{ 'navigation-current':menu.menu == 'timetracker', 'hidden':!main_menu[19] }">
                <a go-to="" href="ptimesheets/" state="ptimesheets" class="allow-click"><i class="fas fa-stopwatch"></i><span><?php ob_start(); ?>Timesheets<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <!-- <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a go-to="" href="ptimesheets/" state="ptimesheets"><span><?php ob_start(); ?>Timesheets<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a></span>
                </ul> -->
            </li><?php  }  ?> <?php if($this->vars->is_3) { ?>
            <li id="sidebar-item" class="navigation-link module_3 <?php echo $this->vars->hide_3;  ?>" ng-class="{ 'navigation-current':menu.menu == 'projects', 'hidden':!main_menu[3] }">
                <a href="projects/" go-to="" state="projects" class="allow-click"><i class="fas fa-clipboard-list"></i><span><?php ob_start(); ?>Projects<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <!-- <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a href="projects/" go-to="" state="projects"><span><?php ob_start(); ?>Projects<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a></span>
                </ul> -->
            </li><?php  }  ?> <?php if($this->vars->is_15) { ?>
            <li id="sidebar-item" class="navigation-link" ng-class="{ 'navigation-current':menu.menu == 'cashregister', 'hidden':!main_menu[15] }">
                <a href="cashorder/" go-to="" state="cashorder"><i class="fas fa-map-marker-alt"></i><span><?php ob_start(); ?>Point of Sales<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu submenu-top">
                    <span class="title-menu"><a href="cashorder/" go-to="" state="cashorder"><span><?php ob_start(); ?>Point of Sales<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'cash_orders' }">
                        <a go-to="" href="cashorder/" state="cashorder"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Tickets<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'cash_articles' }">
                        <a go-to="" href="cashregister/" state="cashregister"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Products<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'cash_company' }">
                        <a go-to="" href="cashaccount/" state="cashaccount"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Account<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem" ng-class="{ 'current':menu.submenu == 'cash_customer' }">
                        <a go-to="" href="cashcustomer/" state="cashcustomer"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Customers<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                </ul>
            </li><?php  }  ?> <?php if($this->vars->is_18) { ?>
            <li id="sidebar-item" class="navigation-link" ng-class="{ 'navigation-current':menu.menu == 'reports', 'hidden':!main_menu[18] }">
                <a href="<?php echo $this->vars->report_state;  ?>/" go-to="" state="<?php echo $this->vars->report_state;  ?>" class="allow-click"><i class="fas fa-chart-line"></i><span><?php ob_start(); ?>Insights<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <!-- <ul class="list-unstyled navigation-submenu">
                    <span class="title-menu"><a href="<?php echo $this->vars->report_state;  ?>/" go-to="" state="<?php echo $this->vars->report_state;  ?>"><span><?php ob_start(); ?>Insights<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a></span>
                </ul> -->
            </li><?php  }  ?> <?php if($this->vars->is_admin) { ?>
            <li id="sidebar-item" class="navigation-link settings-nav">
                <a href="settings/" go-to="" state="settings"><i class="fas fa-cog"></i><span><?php ob_start(); ?>Settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                <ul class="list-unstyled navigation-submenu tabs-settings-nav-gen" id="settingsNav">
                    <span class="title-menu"><a class="test" href="settings/" go-to="" state="settings"><span><?php ob_start(); ?>Settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </span>
                    <li class="sidebar-subitem company-info">
                        <a href="#company" aria-controls="company" go-to="" state="settings" params='{"tab": "company" }' data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>General<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem">
                        <a href="#regional" go-to="" state="settings" params='{"tab": "regional" }' aria-controls="regional" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Regional<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <!-- <li class="sidebar-subitem">
                        <a href="#mail_settings" go-to="" state="settings" params='{"tab": "mail_settings" }' aria-controls="mail_settings" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Mail<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li> -->
                    <li class="sidebar-subitem">
                        <a href="#financial" go-to="" state="settings" params='{"tab": "financial" }' aria-controls="financial" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Financial<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem">
                        <a href="#vat" go-to="" state="settings" params='{"tab": "vat" }' aria-controls="vat" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>VAT<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <?php if($this->vars->noteasy) { ?>
                    <?php if($this->vars->not_easy_invoice_diy) { ?>
                    <li class="sidebar-subitem">
                        <a href="#users" go-to="" state="settings" params='{"tab": "users" }' aria-controls="users" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Users<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?><?php  }  ?>
                    <!-- <li class="sidebar-subitem">
                        <a href="#vat_regime" go-to="" state="settings" params='{"tab": "vat_regime" }' aria-controls="vat_regime" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>VAT Regime<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li> -->
                    <!-- <li class="sidebar-subitem">
                        <a href="#languages" go-to="" state="settings" params='{"tab": "languages" }' aria-controls="languages" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Languages<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li> -->
                    <li class="sidebar-subitem">
                        <a href="#apps" go-to="" state="settings" params='{"tab": "apps" }' aria-controls="apps" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Integrated Apps<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <?php if($this->vars->noteasy) { ?>
                    <?php if($this->vars->not_easy_invoice_diy) { ?>
                    <li class="sidebar-subitem">
                        <a href="#access" go-to="" state="settings" params='{"tab": "access" }' aria-controls="access" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>User Access<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <li class="sidebar-subitem">
                        <a href="#identity" go-to="" state="settings" params='{"tab": "identity" }' aria-controls="identity" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Multiple Identity<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?><?php  }  ?> <?php if($this->vars->noteasy) { ?><?php if($this->vars->is_accountant) { ?>
                    <li class="sidebar-subitem">
                        <a href="#accountantAccess" go-to="" state="settings" params='{"tab": "accountantAccess" }' aria-controls="accountantAccess" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Accountant Access<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?><?php  }  ?> <?php if($this->vars->is_zen) { ?>
                    <li class="sidebar-subitem">
                        <a href="#zenData" go-to="" state="settings" params='{"tab": "zenData" }' aria-controls="zenData" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Zendesk Plugin Data<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?>
                    <li class="sidebar-subitem">
                        <a href="#legal_info" go-to="" state="settings" params='{"tab": "legal_info" }' aria-controls="legal_info" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Legal Information<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li>
                    <?php if($this->vars->access_api_webhooks) { ?>
                    <li class="sidebar-subitem">
                        <a href="#api_interface" go-to="" state="settings" params='{"tab": "api_interface" }' aria-controls="api_interface" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Api applications<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li><?php  }  ?>
                    <!-- <li class="sidebar-subitem">
                        <a href="#error_page" aria-controls="error_page" data-toggle="tab"><i class="fa fa-chevron-right"></i><span><?php ob_start(); ?>Error Page<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span></a>
                    </li> -->
                </ul>
            </li><?php  }  ?>
        </ul>
    </nav>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#sidebarCollapse').on('click', function() {
            $('.wrapper').toggleClass('active');
            $('body').toggleClass('sidebar-mini');
        });
        /*$('.navigation-link>a').on('touchstart click', function(e) {  
            link_parent = $(this).parent();
            if (!link_parent.hasClass('navigation-current')) {
                $('#navigation-menu>li').removeClass('navigation-current');
                link_parent.toggleClass('navigation-current');
            }
        }); 
        $('.settings-nav>a').on('click', function () {
            $('.sidebar-subitem.active').toggleClass('active');
            $('.company-info').addClass('active');
        });*/
    });
</script>
<script>
    function myFunction(x) {
      if (x.matches) { // If media query matches
        $('.wrapper').addClass('active');
            $('body').addClass('sidebar-mini');
      } 
    }
    
    var x = window.matchMedia("(max-width: 560px)")
    myFunction(x) // Call listener function at run time
    x.addListener(myFunction) // Attach listener function on state changes

    // new drop
    var maxHeight = 400;

$(function(){

    $(".dropdown > li").hover(function() {
    
         var $container = $(this),
             $list = $container.find("ul"),
             $anchor = $container.find("a"),
             height = $list.height() * 1.1,       // make sure there is enough room at the bottom
             multiplier = height / maxHeight;     // needs to move faster if list is taller
        
        // need to save height here so it can revert on mouseout            
        $container.data("origHeight", $container.height());
        
        // so it can retain it's rollover color all the while the dropdown is open
        $anchor.addClass("hover");
        
        // make sure dropdown appears directly below parent list item    
        $list
            .show()
            .css({
                paddingTop: $container.data("origHeight")
            });
        
        // don't do any animation if list shorter than max
        if (multiplier > 1) {
            $container
                .css({
                    height: maxHeight,
                    overflow: "hidden"
                })
                .mousemove(function(e) {
                    var offset = $container.offset();
                    var relativeY = ((e.pageY - offset.top) * multiplier) - ($container.data("origHeight") * multiplier);
                    if (relativeY > $container.data("origHeight")) {
                        $list.css("top", -relativeY + $container.data("origHeight"));
                    };
                });
        }
        
    }, function() {
    
        var $el = $(this);
        
        // put things back to normal
        $el
            .height($(this).data("origHeight"))
            .find("ul")
            .css({ top: 0 })
            .hide()
            .end()
            .find("a")
            .removeClass("hover");
    
    });
    
    // Add down arrow only to menu items with submenus
    $(".dropdown > li:has('ul')").each(function() {
        $(this).find("a:first").append("<img src='images/down-arrow.png' />");
    });
    
});
   
    </script>
    
<?php  }  ?>