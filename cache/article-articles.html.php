<div class="container">
    <div class="page-header">
        <div class="row">
            <div class="col-xs-8 col-sm-6">
                <h1><?php ob_start(); ?>Products<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>
                    <button type="button" class="btn btn-primary btn-add" go-to="" state="article.add" params='{"article_id": "tmp"}' tooltip-placement="top"><span class="btn-add_icon"><span class="icon-bar"></span><span class="icon-bar-2"></span></span></button>
                </h1>
            </div>
            <div class="col-xs-4 col-sm-6 text-right" ng-if="show_settings.is_admin_article">
                <div class="dropdown new-settings">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li class="dropdown-item" ng-class="{ 'current':menu.submenu == 'import' }"><a href="import_articles/" go-to="" state="import_articles"><i class="fas fa-download"></i> <?php ob_start(); ?>Import<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li class="dropdown-item" ng-if="show_settings.not_easy_invoice_diy" ng-class="{ 'current':menu.submenu == 'import_prices' }"><a href="import_prices/" go-to="" state="import_prices"><i class="fas fa-download"></i> <?php ob_start(); ?>Import of prices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li class="dropdown-item" ng-class="{ 'current':menu.submenu == 'articles setting' }"><a go-to="" href="articlesSettings/" state="articlesSettings"><span class="fas fa-cog text-primary"></span> <?php ob_start(); ?>Catalog Settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <div class="row page-search page-search-options">
        <form>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-5 col-search-input">
                        <div class="so-field" ng-class="{ 'so-active':search.search!='' }">
                            <span class="glyphicon glyphicon-search so-icon"></span>
                            <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Search<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" search-it doit="search" callbk="renderList" ng-model="search.search">
                            <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('search','')"></span>
                        </div>
                        <!-- /.input-search -->
                    </div>
                    <div class="col-sm-3">
                        <div class="so-field" ng-class="{ 'so-active':search.article_category_id }">
                            <selectize placeholder="<?php ob_start(); ?>Family<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" config='filterCfgFam' options='product_fam' ng-model="search.article_category_id"></selectize>
                            <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('article_category_id','')"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="so-field" ng-class="{ 'so-active':search.supplier_id!=undefined }">
                            <selectize config='supplierCfg' options='suppliers' ng-model="search.supplier_id" id="supplier_list">
                            </selectize>
                            <!-- <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Supplier<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" search-it doit="search" callbk="renderList" ng-model="search.name"> -->
                            <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('supplier_id',undefined)"></span>
                            <!-- <selectize placeholder="<?php ob_start(); ?>All Product Type<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" config='filterCfg' options='product_type' ng-model="activeView" ></selectize> -->
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="so-field" ng-class="{ 'so-active':search.supplier_reference!='' }">
                            <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Supplier Reference<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" search-it doit="search" callbk="renderList" ng-model="search.supplier_reference">
                            <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('supplier_reference','')"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-search-button">
                <div class="btn-group" uib-dropdown>
                    <button type="button" class="btn btn-tertiary btn-square dropdown-toggle" uib-dropdown-toggle>
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
                    <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                        <li><a href="#" ng-click="search.showAdvanced=true;"><span class="fa fa-search text-primary" aria-hidden="true"></span> <?php ob_start(); ?>Advanced search<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li ng-if="adv_product==false && show_settings.is_admin_article">
                            <a href="index.php?do=article-article_raport_csv&search={{search.search}}&article_category_id={{search.article_category_id}}&archived={{search.archived}}&supplier_reference={{search.supplier_reference}}&supplier_id={{search.supplier_id}}" target="_blank"><span class="fas fa-upload text-primary"></span><?php ob_start(); ?>Export<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                        </li>
                        <li ng-if="adv_product==true && show_settings.is_admin_article">
                            <a href="index.php?do=article-article_raport_csv_adv&search={{search.search}}&article_category_id={{search.article_category_id}}&archived={{search.archived}}&supplier_reference={{search.supplier_reference}}&supplier_id={{search.supplier_id}}" target="_blank"><span class="fas fa-upload text-primary"></span><?php ob_start(); ?>Export<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                        </li>
                        <!--   <li ng-if="adv_product==false && show_settings.is_admin_article">
                <a href="index.php?do=article-article_raport&search={{search.search}}&article_category_id={{search.article_category_id}}&archived={{search.archived}}&supplier_reference={{search.supplier_reference}}&supplier_id={{search.supplier_id}}" target="_blank" ><span class="fa fa-file-o text-primary"></span><?php ob_start(); ?>Export Articles .xls<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
              </li>
              <li ng-if="adv_product==true && show_settings.is_admin_article">
                <a href="index.php?do=article-article_raport_adv&search={{search.search}}&article_category_id={{search.article_category_id}}&archived={{search.archived}}&supplier_reference={{search.supplier_reference}}&supplier_id={{search.supplier_id}}" target="_blank" ><span class="fa fa-file-o text-primary"></span><?php ob_start(); ?>Export Articles .xls<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
              </li> -->
                        <li ng-if="adv_product==true && show_settings.is_admin_article">
                            <a href="index.php?do=article-article_raport_custom_prices&search={{search.search}}&article_category_id={{search.article_category_id}}&archived={{search.archived}}&supplier_reference={{search.supplier_reference}}&supplier_id={{search.supplier_id}}" target="_blank"><span class="fas fa-upload text-primary"></span><?php ob_start(); ?>Export custom prices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                        </li>
                        <li ng-if="search.archived"><a href="#" ng-click="archived(0)"><span class="fa fa-list-ul"  ></span> <?php ob_start(); ?>Active Products<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li ng-if="!search.archived"><a href="#" ng-click="archived(1)"><span class="fas fa-archive new_archive" ></span> <?php ob_start(); ?>Archived Products<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li><a href="#" ng-click="columnSettings()"><span class="fas fa-cog text-primary"></span> <?php ob_start(); ?>Column settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 advanced_search adv_s" ng-class="{ 'advanced_search-show':search.showAdvanced }">
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Commercial Name<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-model="search.commercial_name">
                    </div>
                    <div class="col-sm-3 col-md-2">
                        <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Commercial Name2<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-model="search.commercial_name2">
                    </div>
                    <div class="col-sm-3 col-md-2">
<!--                        <selectize config='brandCfg' options='brands' ng-model="search.brand_id" id="brands_list"></selectize>-->
                        <div class="so-field" ng-class="{ 'so-active':search.brand_id }">
                            <selectize config='brandCfg' options='brands' ng-model="search.brand_id"></selectize>
                            <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('brand_id', '')"></span>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-2">
                        <input type="text" class="form-control" placeholder="<?php ob_start(); ?>EAN Code<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-model="search.ean_code">
                    </div>
                    <div class="col-sm-3 col-md-2 text-right pull-right">
                        <button class="btn btn-plain btn-search_cancel" type="button" ng-click="resetAdvanced()"><?php ob_start(); ?>Cancel<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
                        <button class="btn btn-default" type="submit" ng-click="searchThing()"><?php ob_start(); ?>Search<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
                    </div>
                </div>
            </div>
            <!-- /.advanced_search -->
        </form>
    </div>
    <!-- /.page-search -->
    <div class="row page-content">
        <div class="col-xs-12">
            <table class="table active_rows orderTable articles">
                <thead>
                    <tr>
                        <th ng-repeat="col in columns" ng-class="{ 'sortable_row':col.order_by, 'sortable_row-up':(!reverse && col.order_by && ord==col.order_by), 'sortable_row-down':(reverse && col.order_by && ord==col.order_by), 'expand_row': col.column_name=='item_code', 'base-price': col.column_name=='base_price', 'text-center':col.column_name=='total_price' || col.column_name=='stock' || col.column_name=='ant_stock' || col.column_name=='stock_pack' || col.column_name=='ant_stock_pack'}"
                            ng-click="(col.order_by ? orderBY(col.order_by) : '')">
                            <span ng-bind-html="col.name"></span>
                        </th>
                        <!-- <th class="{{code_width}} {{code_class}} sortable_row expand_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='item_code'), 'sortable_row-down':(reverse && ord=='item_code') }" ng-click="orderBY('item_code')"><?php ob_start(); ?>Code<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th class="col-sm-3 col-md-3 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='internal_name'), 'sortable_row-down':(reverse && ord=='internal_name') }" ng-click="orderBY('internal_name')"><?php ob_start(); ?>Internal Name<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th class="{{fam_width}} sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='article_category'), 'sortable_row-down':(reverse && ord=='article_category') }" ng-click="orderBY('article_category')"><?php ob_start(); ?>Article Category<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th class="{{cust_width}} sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='customer_name'), 'sortable_row-down':(reverse && ord=='customer_name') }" ng-click="orderBY('customer_name')"><?php ob_start(); ?>Supplier<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th class="base-price sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='price'), 'sortable_row-down':(reverse && ord=='price') }" ng-click="orderBY('price')"><?php ob_start(); ?>Base Price<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th class="col-sm-2 col-md-1 sortable_row text-center" ng-class="{ 'sortable_row-up':(!reverse && ord=='total_price'), 'sortable_row-down':(reverse && ord=='total_price') }" ng-click="orderBY('total_price')"><?php ob_start(); ?>Price + VAT<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th ng-if="allow_stock && adv_product" class="col-sm-1 col-md-1 text-center sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='stock'), 'sortable_row-down':(reverse && ord=='stock') }" ng-click="orderBY('stock')"><?php ob_start(); ?>Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th ng-if="allow_stock && adv_product" class="col-sm-1 col-md-1 text-center sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='stock_ant'), 'sortable_row-down':(reverse && ord=='stock_ant') }" ng-click="orderBY('stock_ant')"><?php ob_start(); ?>Anticipated Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th> -->
                        <!-- <th ng-if="adv_product" class="col-sm-1 col-md-1 text-center sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='block_discount'), 'sortable_row-down':(reverse && ord=='block_discount') }" ng-click="orderBY('block_discount')"><?php ob_start(); ?>Discounts blocked<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th> -->
<!--                        <th class="col-sm-1 col-select col-select-right" ng-if="!search.archived">-->
<!--                            <div class="checkbox checkbox-primary">-->
<!--                                <input type="checkbox" ng-model="listObj.check_add_all" ng-click="saveForArchive()">-->
<!--                                <label for="checkbox_all"></label>-->
<!--                            </div>-->
<!--                        </th>-->
                        <th class="col-sm-1 sortable_row col-select">
                            <div class="dropdown order_export">
                                <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-square-o" ng-if="!all_pages_selected && !minimum_selected"></i>
                                    <i class="fas fa-minus-square" ng-if="!all_pages_selected && minimum_selected"></i>
                                    <i class="fas fa-check-square" ng-if="all_pages_selected"></i>
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#" ng-click="checkAllPage()"><?php ob_start(); ?>Select page<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    <a class="dropdown-item" href="#" ng-if="!all_pages_selected" ng-click="checkAllPages()"><?php ob_start(); ?>Select all<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    <a class="dropdown-item" href="#" ng-if="all_pages_selected || minimum_selected" ng-click="uncheckAllPages()"><?php ob_start(); ?>Select none<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                </div>
                            </div>
                        </th>
                        <!--   <th class="col-sm-1 table_actions">
                         <div class="btn-group">
                          <button type="button" class="btn btn-tertiary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                        <a href="index.php?do=article-article_raport_csv&search={{search.search}}&article_category_id={{search.article_category_id}}&archived={{search.archived}}&supplier_reference={{search.supplier_reference}}" target="_blank" ><span class="fas fa-file-download text-primary">
                                     </span><?php ob_start(); ?>Export Articles .csv<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                </li>
                                <li>
                                     <a href="index.php?do=article-article_raport&search={{search.search}}&article_category_id={{search.article_category_id}}&archived={{search.archived}}&supplier_reference={{search.supplier_reference}}" target="_blank" ><span class="fa fa-file-o text-primary"></span><?php ob_start(); ?>Export Articles .xls<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                </li>
                            </ul>
                        </div>
                      </th>-->
                        <th class="table_actions">
                            <!-- <span class="hideOnTablet"><?php ob_start(); ?>Action<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span> -->
                            <div class="btn-group" uib-dropdown>
                                <button type="button" class="btn btn-tertiary dropdown-toggle" uib-dropdown-toggle>
                                                <span class="caret"></span>
                                            </button>
                                <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                    <li ng-if="!search.archived">
                                        <a href="#" ng-click="archiveBulkArticles()"><span class="fas fa-archive text-primary"></span> <?php ob_start(); ?>Archive Bulk Articles<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    </li>
                                    <li ng-if="search.archived">
                                        <a href="#" ng-click="restoreBulkArticles()"><span class="glyphicon glyphicon-repeat text-primary"></span> <?php ob_start(); ?>Restore Bulk Articles<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    </li>
                                </ul>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in list" class="collapseTr">
                        <td class="text-muted" ng-repeat="col in columns" ng-class="{ 'expand_row' : col.column_name=='item_code', 'col-adjust-input text-nowrap':col.column_name=='base_price', 'text-center col-amount' : (col.column_name=='total_price' || col.column_name=='stock_pack' || col.column_name=='ant_stock_pack'), 'text-center text-strong col-amount': (col.column_name=='stock' || col.column_name=='ant_stock')}"
                            go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$parent.$index}}, "total_items":{{max_rows}} } }' ng-attr-data-clickdisabled='{{ (col.column_name=="base_price") || (item.show_stock && col.column_name=="ant_stock") || (!item.use_batch_no && !item.use_serial_no && item.show_stock && item.adv_product &&  item.allow_stock && item.stock_multiple_locations && col.column_name=="stock") ? "1" :"0"}} '>
                            <span ng-if="col.column_name=='item_code' && adv_product" class="glyphicon glyphicon-plus btn-expand" ng-click="listMore($parent.$index)"></span>
                            <span ng-if="col.column_name=='item_code'" class="glyphicon glyphicon-minus btn-expand hidden" ng-click="listLess($parent.$index)"></span>
                            <span ng-if="col.column_name=='item_code'" class="text-ellipsis ng-cloak" go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$parent.$index}}, "total_items":{{max_rows}} } }'
                                ng-attr-data-clickdisabled='{{ (col.column_name=="base_price") || (item.show_stock && col.column_name=="ant_stock") || (!item.use_batch_no && !item.use_serial_no && item.show_stock && item.adv_product &&  item.allow_stock && item.stock_multiple_locations && col.column_name=="stock") ? "1" :"0"}} '>
                          <a href="#" class="text-muted" ng-bind-html="item[col.column_name]"></a>
                      </span>
                            <a ng-if="col.column_name!='item_code' && col.column_name!='total_price' && col.column_name!='base_price' && col.column_name!='stock' && col.column_name!='ant_stock'" href="#" class="text-muted" go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}'
                                list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$parent.$index}}, "total_items":{{max_rows}} } }' ng-attr-data-clickdisabled='{{ (col.column_name=="base_price" ) || (item.show_stock && col.column_name=="ant_stock") || (!item.use_batch_no && !item.use_serial_no && item.show_stock && item.adv_product &&  item.allow_stock && item.stock_multiple_locations && col.column_name=="stock") ? "1" :"0"}} '>
                                <span ng-if="col.column_name!='customer_name' || (col.column_name=='customer_name' && !item.has_variants)" ng-bind-html="item[col.column_name]"></span>
                            </a>
                            <a ng-if="col.column_name=='total_price' && !item.has_variants" href="#" class="text-muted" ng-bind-html="item.total_price"></a>
                            <div class="input-group" ng-if="col.column_name=='base_price' && !item.has_variants">
                                <a href="#" class="text-muted" ng-bind-html="item.base_price"></a>
                                <!--  <input type="text" ng-model="item.base_price" class="form-control input-group-addon_shrink" ng-blur="updatePrice(item)">
                               <span class="input-group-addon input-group-addon_shrink input-group-plain">
                                  <i class="fa fa-spinner fa-spin fa-fw" ng-if="item.loadRequest"></i>
                                  <i class="fa fa-check" aria-hidden="true" ng-if="item.loadSuccess"></i>
                                  <i class="fa fa-exclamation" aria-hidden="true" ng-if="item.loadError"></i>
                                </span> -->
                            </div>
                            <span class="{{item.stock_status}}" ng-if="item.show_stock && col.column_name=='stock' && item.adv_product && item.allow_stock && item.stock_multiple_locations && !item.use_combined" go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}'
                                list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$parent.$index}}, "total_items":{{max_rows}} } }' ng-attr-data-clickdisabled='{{ (col.column_name=="base_price" ) || (item.show_stock && col.column_name=="ant_stock") || (!item.use_batch_no && !item.use_serial_no && item.show_stock && item.adv_product &&  item.allow_stock && item.stock_multiple_locations && col.column_name=="stock") ? "1" :"0"}} '>{{item.stock}} <span ng-if="item.reserved_stock">({{item.reserved_stock}})</span></span>
                            <span ng-if="(!item.show_stock || item.use_combined) && col.column_name=='stock' && item.adv_product && item.allow_stock && item.stock_multiple_locations " go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$parent.$index}}, "total_items":{{max_rows}} } }'
                                ng-attr-data-clickdisabled='{{ (col.column_name=="base_price" ) || (item.show_stock && col.column_name=="ant_stock") || (!item.use_batch_no && !item.use_serial_no && item.show_stock && item.adv_product &&  item.allow_stock && item.stock_multiple_locations && col.column_name=="stock") ? "1" :"0"}} '>-</span>
                            <a href="#" ng-if="!item.use_combined && !item.use_batch_no && !item.use_serial_no && item.show_stock && item.adv_product &&  item.allow_stock && item.stock_multiple_locations && col.column_name=='stock'" ng-click="openModal('setStock',item,'md')"><span class="fas fa-pen"></span></a>
                            <div class="input-group" ng-if="col.column_name=='stock' && item.adv_product &&  item.allow_stock && !item.stock_multiple_locations">
                                <input type="text" ng-model="item.stock" class="form-control input-group-addon_shrink" ng-blur="updateStock(item)">
                                <span class="input-group-addon input-group-addon_shrink input-group-plain">
                            <i class="fa fa-spinner fa-spin fa-fw" ng-if="item.loadRequestStock"></i>
                            <i class="fa fa-check" aria-hidden="true" ng-if="item.loadSuccessStock"></i>
                            <i class="fa fa-exclamation" aria-hidden="true" ng-if="item.loadErrorStock"></i>
                          </span>
                            </div>
                            <span ng-if="item.show_stock && col.column_name=='ant_stock'" class="{{item.ant_stock_status}}">{{item.ant_stock}} <span ng-if="item.ant_stock_from_suppliers">({{item.ant_stock_from_suppliers}})</span></span>
                            <a href="#" ng-if="item.show_stock && col.column_name=='ant_stock'" ng-click="openModal('viewAntStock',item,'lg')"><span class="glyphicon glyphicon-info-sign text-primary"></span></a>
                        </td>
                        <!-- <td class="expand_row text-muted">
                      <span class="glyphicon glyphicon-plus btn-expand" ng-if="adv_product" ng-click="listMore($index)"></span>
                      <span class="glyphicon glyphicon-minus btn-expand hidden" ng-click="listLess($index)"></span>
                      <span class="text-ellipsis ng-cloak" go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$index}}, "total_items":{{max_rows}} } }'>
                          <a href="article/article/{{item.article_id}}" class="text-muted">{{::item.code}}</a>
                      </span>
                    </td>

                    <td class="text-muted ng-cloak" go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$index}}, "total_items":{{max_rows}} } }'>
                        <a href="article/article/{{item.article_id}}" class="text-muted"><span>{{::item.name}}</span></a>
                    </td>
                     <td class="text-muted ng-cloak" go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$index}}, "total_items":{{max_rows}} } }'>
                         <a href="article/article/{{item.article_id}}" class="text-muted"><span>{{::item.article_category}}</span></a>
                     </td>
                     <td class="text-muted ng-cloak" go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$index}}, "total_items":{{max_rows}} } }'>
                         <a href="article/article/{{item.article_id}}" class="text-muted"><span>{{::item.customer_name}}</span></a>
                     </td>
                    <td class="text-muted col-adjust-input text-nowrap" >
                        <div class="input-group">
                          <input type="text" ng-model="item.base_price" class="form-control input-group-addon_shrink" ng-blur="updatePrice(item)">
                          <span class="input-group-addon input-group-addon_shrink input-group-plain">
                            <i class="fa fa-spinner fa-spin fa-fw" ng-if="item.loadRequest"></i>
                            <i class="fa fa-check" aria-hidden="true" ng-if="item.loadSuccess"></i>
                            <i class="fa fa-exclamation" aria-hidden="true" ng-if="item.loadError"></i>
                          </span>
                        </div>
                    </td>
                    <td class="text-center text-muted col-amount">
                        <a href="article/article/{{item.article_id}}" class="text-muted" ng-bind-html="item.total_price"></a>
                    </td>
                     <td ng-if="item.adv_product &&  item.allow_stock && item.stock_multiple_locations" class="text-center text-strong col-amount"  list='{ "type":"article-articles", "obj": { "search":{{search}} } }'><span  class="{{item.stock_status}}" ng-if="item.show_stock">{{item.stock}} <span ng-if="item.reserved_stock">({{item.reserved_stock}})</span></span>
                            <a href="#" ng-if="!item.use_batch_no && !item.use_serial_no && item.show_stock" ng-click="openModal('setStock',item,'md')"><span class="fas fa-pen"></span></a>
                       </td>
                       <td ng-if="item.adv_product &&  item.allow_stock && !item.stock_multiple_locations" class="text-center  text-muted col-adjust-input text-nowrap"  list='{ "type":"article-articles", "obj": { "search":{{search}} } }'>
                       <div class="input-group">
                          <input type="text" ng-model="item.stock" class="form-control input-group-addon_shrink" ng-blur="updateStock(item)" >
                          <span class="input-group-addon input-group-addon_shrink input-group-plain">
                            <i class="fa fa-spinner fa-spin fa-fw" ng-if="item.loadRequestStock"></i>
                            <i class="fa fa-check" aria-hidden="true" ng-if="item.loadSuccessStock"></i>
                            <i class="fa fa-exclamation" aria-hidden="true" ng-if="item.loadErrorStock"></i>
                          </span>
                        </div>
                       </td>
                       <td ng-if="item.adv_product &&  item.allow_stock " class="text-center text-strong col-amount"  list='{ "type":"article-articles", "obj": { "search":{{search}} } }'><span ng-if="item.show_stock" class="{{item.ant_stock_status}}">{{item.ant_stock}} <span ng-if="item.ant_stock_from_suppliers">({{item.ant_stock_from_suppliers}})</span></span>
                          <a href="#" ng-if="item.show_stock"  ng-click="openModal('viewAntStock',item,'lg')"><span class="glyphicon glyphicon-info-sign text-primary"></span></a>

                       </td> -->
                        <!--   <td class="text-center" ng-if="item.adv_product"   list='{ "type":"article-articles", "obj": { "search":{{search}} } }' >
                        <div class="checkbox checkbox-primary">
                          <input type="checkbox" ng-click="discount_act(item)" id="checkbox_discount_{{$index}}" ng-model="item.block_discount">
                          <label for="checkbox_discount_{{$index}}">
                        </div>
                       </td> -->
<!--                        <td class="col-select col-select-right col-select_art" ng-if="!search.archived">-->
<!--                            <div class="checkbox checkbox-primary">-->
<!--                                <input type="checkbox" ng-model="item.check_add_to_product" ng-click="saveForArchive(item)">-->
<!--                                <label for="checkbox_all"></label>-->
<!--                            </div>-->
<!--                        </td>-->
                        <td class="col-select">
                            <div class="checkbox checkbox-primary">
                                <input type="checkbox" id="checkbox_all_{{$index}}" ng-model="item.check_add_to_product" ng-click="markCheckNew(item)">
                                <label for="checkbox_all_{{$index}}"></label>
                            </div>
                        </td>
                        <td class="table_actions">
                            <div class="btn-group" uib-dropdown>
                                <button type="button" class="btn btn-plain dropdown-toggle" uib-dropdown-toggle>
                        <span class="caret"></span>
                    </button>
                                <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                    <li class="dropdown-header"><?php ob_start(); ?>Action<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></li>

                                    <!-- <li>
                            <a href="#"  go-to="" state="article.add" params='{"article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}}, "cindex":{{$index}}, "total_items":{{max_rows}} } }'><span class="fas fa-pen text-primary"></span> <?php ob_start(); ?>Edit<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                        </li> -->
                                    <li ng-if="item.adv_product &&  item.allow_stock ">
                                        <a href="#" ng-if="!item.use_batch_no && !item.use_serial_no && item.show_stock" ng-click="openModal('setStock',item,'md')"><span class="fas fa-pen"></span><?php ob_start(); ?>Edit stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    </li>
                                    <li>
                                        <a href="#" go-to="" state="article.add" params='{"duplicate_article_id": {{::item.article_id}}}' list='{ "type":"article-articles", "obj": { "search":{{search}} } }'><i class="fa fa-clone" aria-hidden="true"></i>
 <?php ob_start(); ?>Duplicate<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    </li>
                                    <li ng-if="!item.is_archived">
                                        <a href="#" ng-click="action(item,'archive_link')" confirm="<?php ob_start(); ?>Are you sure, you wanna archive this entry?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" confirm-title="{{item.confirm}}" confirm-ok="{{item.ok}}" confirm-cancel="{{item.cancel}}"><span class="fas fa-archive text-primary"></span> <?php ob_start(); ?>Archive<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    </li>
                                    <li ng-if="item.is_archived">
                                        <a href="#" ng-click="action(item,'undo_link')" confirm="<?php ob_start(); ?>Are you sure, you wanna revert this entry?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" confirm-title="{{item.confirm}}" confirm-ok="{{item.ok}}" confirm-cancel="{{item.cancel}}"><span class="glyphicon glyphicon-repeat text-primary"></span> <?php ob_start(); ?>Revert<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    </li>
                                    <li ng-if="item.is_archived">
                                        <a href="#" ng-click="action(item,'delete_link')" confirm-title="{{item.confirm}}" confirm-ok="{{item.ok}}" confirm-cancel="{{item.cancel}}" confirm="<?php ob_start(); ?>Are you sure, you wanna delete this entry?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"><span class="fa fa-trash text-primary"></span> <?php ob_start(); ?>Delete<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                    </li>

                                </ul>
                            </div>
                        </td>

                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.page-content -->

    <!-- <pag-custom pages="pager" refresh="rederList()" doit='article-articles'></pag-custom> -->
    <div class="col-sm-12">
        <div class="col-sm-8">
            <ul ng-if="max_rows > 30" uib-pagination total-items="max_rows" ng-model="search.offset" ng-change="searchThing()" max-size="5" class="pager" boundary-links="true" items-per-page="lr"></ul>
        </div>
        <div class="col-sm-4 text-right">
            <label><?php ob_start(); ?>Number of articles<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>: <strong>{{max_rows}}</strong></label>
        </div>
    </div>
</div>
<!-- neede on all pages -->