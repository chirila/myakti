<div class="container main-branding">
    <div class="row">

        <div class="col-md-12 text-center urgent_message hide"><span class="text_from_snap"></span></div>

        <div class="col-xs-12 text-right col-user_options float-right" ng-class="{'col-md-12':(!is_trial),'col-md-12':(is_trial)}">
            <ul>
                <?php if($this->vars->hide_them_all) { ?> <?php if($this->vars->hide_add) { ?>
                <!-- <li class="helpdesk" ng-if="!is_trial">
                    <a href="https://support.akti.com" target="blank"><?php ob_start(); ?>Akti Helpdesk<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>: https://support.akti.com</a>
                </li> -->
                <li class="user_options-link uo-quick_add" ng-class="{ 'uo-quick_add-show': show.quickAdd }">
                    <a class="quick_add" href="#" ng-click="show.quickAdd = !show.quickAdd"><span class="icon-bar"></span><span class="icon-bar"></span></a>
                    <ul>
                        <?php if($this->vars->is_1) { ?>
                        <!-- <li class="module_1 <?php echo $this->vars->hide_1;  ?>"><a href="" ui-sref="customer({ 'customer_id':'tmp' })" ng-click="show.quickAdd = false" ><?php ob_start(); ?>Add a company<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li> -->
                        <li class="module_1 <?php echo $this->vars->hide_1;  ?>"><a href="" ng-click="createCustomer()"><i class="fas fa-user-friends"></i> <?php ob_start(); ?>Add a company<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li class="module_1 <?php echo $this->vars->hide_1;  ?>"><a href="" ng-click="createContact()"><i class="fas fa-user"></i> <?php ob_start(); ?>Add a contact<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <!-- <li class="module_1 <?php echo $this->vars->hide_1;  ?>"><a href="../pim/admin/index.php?do=quote-opportunities&amp;crm=true&amp;add_opportunity=true&amp;add=true" title="<?php ob_start(); ?>Add a new Opportunity<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"><?php ob_start(); ?>Add a new Opportunity<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li> --><?php  }  ?>
                        <!-- <?php if($this->vars->is_12) { ?><li class="module_1 <?php echo $this->vars->hide_1;  ?>"><a href="" ui-sref="article.add({ 'article_id':'tmp' })" ng-click="show.quickAdd = false"><?php ob_start(); ?>Add a product<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li class="module_1 <?php echo $this->vars->hide_1;  ?>"><a href="" ui-sref="article.add({ 'article_id':'tmp', 'form_type': 'service'})" ng-click="show.quickAdd = false"><?php ob_start(); ?>Add a service<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?> -->
                        <?php if($this->vars->is_5) { ?>
                        <li class="module_5 <?php echo $this->vars->hide_5;  ?>"><a href="" ui-sref="quote.edit({ 'quote_id':'tmp' })" ng-click="show.quickAdd = false"><i class="fas fa-file-invoice"></i> <?php ob_start(); ?>Add a quote<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?> <?php if($this->vars->is_6) { ?>
                        <li class="module_6 <?php echo $this->vars->hide_6;  ?>"><a href="" ui-sref="order.edit({ 'order_id':'tmp', 'buyer_id':'tmp' })" ng-click="show.quickAdd = false"><i class="fas fa-shopping-cart"></i> <?php ob_start(); ?>Add Order<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?> <?php if($this->vars->is_14) { ?>
                        <li class="module_6 <?php echo $this->vars->hide_14;  ?>"><a href="" ui-sref="purchase_order.edit({ 'p_order_id':'tmp', 'buyer_id':'tmp' })" ng-click="show.quickAdd = false" title="<?php ob_start(); ?>Add Purchase Order<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"><i class="fas fa-box"></i> <?php ob_start(); ?>Add Purchase Order<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?>
                        <?php if($this->vars->is_3) { ?><li class="module_3 <?php echo $this->vars->hide_3;  ?>"><a href="" ui-sref="project.edit({ 'project_id':'tmp' })" ng-click="show.quickAdd = false" ><i class="fas fa-clipboard-list"></i> <?php ob_start(); ?>Add a project<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?>
                        <!-- <?php if($this->vars->is_11) { ?><li class="module_11 <?php echo $this->vars->hide_11;  ?>"><a href="" ui-sref="contract.edit({ 'contract_id':'tmp' })" ng-click="show.quickAdd = false"><?php ob_start(); ?>Add a contract<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?> -->
                        <?php if($this->vars->is_4) { ?>
                        <li class="module_4 <?php echo $this->vars->hide_4;  ?>"><a href="" ui-sref="invoice.edit({ 'invoice_id':'tmp' })" ng-click="show.quickAdd = false"><i class="far fa-file-alt"></i> <?php ob_start(); ?>Add Invoice<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <li class="module_4 <?php echo $this->vars->hide_4;  ?>"><a href="" ui-sref="invoice.edit({'invoice_id': 'tmp', 'type':'1'})" ng-click="show.quickAdd = false"><i class="far fa-file-alt"></i> <?php ob_start(); ?>Add Proforma Invoice<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        <?php  }  ?> <?php if($this->vars->is_only_13) { ?>
                        <li class="module_13 <?php echo $this->vars->hide_13;  ?>"><a href="" ui-sref="service.edit({ 'service_id':'tmp' })" ng-click="show.quickAdd = false"><i class="fas fa-briefcase"></i> <?php ob_start(); ?>Add intervention<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?>
                        <!-- <li class="module_1"><a href="" ui-sref="article.add({ 'article_id':'tmp' })" ng-click="show.quickAdd=false"><?php ob_start(); ?>Add Article<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li> -->
                        <!-- <?php if($this->vars->is_17) { ?><li><a href="" ui-sref="installation.edit({'installation_id': 'tmp'})" ng-click="show.quickAdd = false"><?php ob_start(); ?>New Installation<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li><?php  }  ?> -->
                    </ul>
                </li><?php  }  ?>
                <?php if($this->vars->not_easy_invoice_diy) { ?>
                <li class="user_options-link uo-activities" ng-if="ADV_CRM">
                    <a class="activities" ui-sref="myActivities" uib-tooltip="<?php ob_start(); ?>My activities<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" tooltip-placement="bottom"><span class="akti-icon o_akti_user_alt"></span></a>
                </li><?php  }  ?>
                <?php if($this->vars->not_easy_invoice_diy) { ?>
                <?php if($this->vars->hide_calendar) { ?>
                <li class="user_options-link uo-notifications">
                    <a class="notify" ui-sref="calendar" uib-tooltip="<?php ob_start(); ?>Tasks<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" tooltip-placement="bottom"><i class="fas fa-tasks"></i><span class="bubble-holder header_task_number" ng-if="taskNr > 0"><ins>{{taskNr}}</ins></span></a>
                </li>
                <?php  }  ?><?php  }  ?>
                <!-- <li class="user_options-link uo-notifications">
                    <a id="notify" class="notify" href="../pim/admin/index.php?do=pim-message_center" title="<?php ob_start(); ?>Tasks<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"><span class="akti-icon o_akti_checklist"></span><span class="bubble-holder" ng-if="messages > 0"><ins>{{messages}}</ins></span></a
                ></li> -->
                <?php if($this->vars->is_19) { ?>
                <li class="user_options-link uo-timesheet">
                    <a class="timesheet" uib-tooltip="Fill in your timesheet" ui-sref="timesheet" tooltip-placement="bottom"><span class="akti-icon o_akti_timesheet"></span></a>
                </li>
                <?php  }  ?> <?php if($this->vars->is_13) { ?>
                <li class="user_options-link uo-interventions <?php echo $this->vars->hide_13;  ?>">
                    <a id="service_notify" class="notify service module_13 module_13_top" href="#" ui-sref="serviceTs" uib-tooltip="<?php ob_start(); ?>Fill in your intervention<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" tooltip-placement="bottom"><span class="fas fa-toolbox"></span><span class="bubble-holder" ng-if="intNr > 0"><ins>{{intNr}}</ins></span></a>
                </li><?php  }  ?>

                <!-- <li class="user_options-link support-page" ng-class="{'support-page-show' : show.support}">
                    <a href="#" ng-click="show.support = !show.support" uib-tooltip="<?php ob_start(); ?>Support Page<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" tooltip-placement="bottom">
                        <i class="fa fa-question" aria-hidden="true"></i>
                    </a>
                    <ul>
                        <li class="support-tutorials">
                            <a href="<?php echo $this->vars->tutorials_link;  ?>" target="_blank" title="<?php ob_start(); ?>Tutorials<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"> <?php ob_start(); ?> Tutorials <?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> </a>
                        </li>
                        <li class="support-knowledge-base">
                            <a href="<?php echo $this->vars->faq_link;  ?>" target="_blank" title="<?php ob_start(); ?>Knowledge Base<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"> <?php ob_start(); ?> Knowledge Base <?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> </a>
                        </li>
                        <li class="support-legal-information">
                            <a href="https://support.akti.com/hc/nl/categories/360000087145-Voorwaarden-en-Privacy" target="_blank" title="<?php ob_start(); ?>Legal information<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"> <?php ob_start(); ?> Legal information <?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> </a> 
                           
                            <a href="#" ui-sref="settings({'tab': 'legal_info'})" title="<?php ob_start(); ?>Legal information<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"> <?php ob_start(); ?> Legal information <?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> </a>
                        </li>
                    </ul>
                </li> -->
                <notification id="notification_container" />
                <?php if($this->vars->not_easy_invoice_diy) { ?>
                <li class="user_options-link uo-beamer">
                    <a href="#" uib-tooltip="<?php ob_start(); ?>What's new on Test Akti?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" tooltip-placement="bottom"><i class="fa fa-bullhorn" aria-hidden="true"></i></a>
                </li>
                <?php  }  ?>
                <li class="user_options-menu" ng-class="{ 'user_options-menu-show': show.profile }">
                    <a href="#" class="user_options-profile" ng-click="show.profile = !show.profile">
                        <div class="drop_images top-header-avatar">
                            <span class="akti-icon o_akti_user user-thumb"></span>
                        </div>
                        <!--  <?php if($this->vars->no_avatar) { ?><span class="akti-icon o_akti_user user-thumb"></span><?php  }  ?> -->
                        <!-- <?php if($this->vars->avatar) { ?>
                        <div class="drop_images top-header-avatar">
                            <div class="col-sm-2 account_avatar">
                                <div class="image avatar">
                                    <img src="<?php echo $this->vars->avatar;  ?>"/>
                                </div>
                            </div>
                        </div>
                        <?php  }  ?> -->
                        <!-- <span class="glyphicon glyphicon-chevron-down"></span> -->
                        <span class="user_options-firstname">{{userName}}</span>
                        <div class="company_name">
                            <span class="text-ellipsis">{{companyName}}</span>
                        </div>
                    </a>
                    <ul>
                        <li><a href="myaccount/" ui-sref="myaccount" title="<?php ob_start(); ?>My Preferences<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="show.profile = false"><!-- <span class="akti-icon o_akti_user"></span> --> <?php ob_start(); ?>My Preferences<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                        </li>
                        <li ng-if="switchAcc && switchList.length" uib-dropdown>
                            <a uib-dropdown-toggle="dropdown"><!-- <span class="akti-icon"><i class="fas fa-user-friends"></i></span> --> <?php ob_start(); ?>Switch account<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span class="glyphicon glyphicon-chevron-up"></span></a>
                            <ul class="dropdown-menu-right" uib-dropdown-menu ng-if="switchList.length">
                                <li ng-repeat="item in switchList" ng-click="switchAccount(item.id)" ng-class="{ 'switch_account_underline' : $index < switchList.length-1 }">
                                    <a href="#" class="p-l-0">
                                        <div class="col-sm-2 drop_images deals_avatar switch_account_letters">
                                            <div class="col-sm-2 account_avatar">
                                                <div class="image avatar">
                                                    <small class="letters_color" data-letters="{{item.u_letters}}" style="background-color:{{item.u_color}}"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <small>
                                            <label class="overflow-ellipsis link-pointer"><b class="active">{{::item.u_name}}</b>{{::item.value}}</label>
                                        </small>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php if($this->vars->HIDE_PAYMENT) { ?>
                        <!--                   <li><a href="billing/" ui-sref="billing" title="<?php ob_start(); ?>Billing<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="show.profile=false"><i class="fa fa-credit-card" aria-hidden="true"></i> <?php ob_start(); ?>Billing<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                                          </li> -->
                        <!-- <li><a href="" ui-sref="<?php echo $this->vars->plan_go;  ?>" title="<?php ob_start(); ?>Subscription<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="show.profile = false"> -->
                        <!-- <span class="akti-icon o_akti_logo"></span> -->
                        <!-- <?php ob_start(); ?>Subscription<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a> -->
                </li>
                <?php  }  ?>
                <li class="hidden hide"><a href="features/" ui-sref="features" title="<?php ob_start(); ?>Features<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="show.profile = false"><i class="fa fa-universal-access" aria-hidden="true"></i> <?php ob_start(); ?>Features<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                </li>
                <?php if($this->vars->is_admin) { ?><?php if($this->vars->noteasy) { ?>
                <!-- <li><a href="" ui-sref="users" title="<?php ob_start(); ?>Users<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="show.profile = false"><i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i><?php ob_start(); ?> Users<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a> -->
                </li><?php  }  ?>
                <!-- <li><a href="../pim/admin/index.php?do=settings-invoices" title="<?php ob_start(); ?>Billing<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"><?php ob_start(); ?>Billing<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li> -->
                <!-- <li class=""><a href="settings/" title="<?php ob_start(); ?>Settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ui-sref="settings" ng-click="show.profile = false"><?php ob_start(); ?>Settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li> --><?php  }  ?>
                <li class="hidden hide"><a href="#" title="<?php ob_start(); ?>What's New ?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="headerModal('HeaderModal', '', sm)"><i class="fa fa-question-circle-o" aria-hidden="true"></i> <?php ob_start(); ?>What's New ?<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                <li class="uo-menu-logout"><a href="" id="logout_link" title="<?php ob_start(); ?>Logout<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="show.profile = false"><!-- <span class="akti-icon akti-icon-16 o_akti_logout"> --></span> <?php ob_start(); ?>Logout<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                </li>
                <?php if($this->vars->not_easy_invoice_diy) { ?>
                <?php if($this->vars->show_invite) { ?>
                <li><a href="#" ng-click="headerModal('InviteModal', '', md)" title="<?php ob_start(); ?>Invite<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"><!-- <span class="akti-icon o_akti_gift"> --><!-- <i class="fas fa-gift"></i> --></span><?php ob_start(); ?> Invite a friend<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                </li><?php  }  ?><?php  }  ?>
                </ul>
                </li>

                <?php  }  ?> <?php if($this->vars->special_button) { ?>
                <li class="user_options-link">
                    <!-- <toggle-switch on-change="changeLang(language)" ng-model="language" class="switch-primary" on-label="<?php ob_start(); ?>EN<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" off-label="<?php ob_start(); ?>FR<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"></toggle-switch> -->
                    <div class="btn-group" uib-dropdown is-open="status.isopen">
                        <button id="single-button" type="button" class="btn btn-default" uib-dropdown-toggle ng-disabled="disabled"><?php ob_start(); ?>Language<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?><span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                            <li role="menuitem"><a href="#" ng-click="changeLang('en')"><?php ob_start(); ?>EN<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                            <li role="menuitem"><a href="#" ng-click="changeLang('fr')"><?php ob_start(); ?>FR<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                            <li role="menuitem"><a href="#" ng-click="changeLang('nl')"><?php ob_start(); ?>NL<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                        </ul>
                    </div>
                </li>
                <?php  }  ?>
                <!--<li class="user_options-toggle">
                    <a href="#"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
                </li>-->
            </ul>
            <!-- ???? -->
            <div id="add_tip"></div>
            <input type="hidden" name="current_page" id="current_page" value="invoice-export_btb_inc">
        </div>
        <!-- /.col-user_options -->
    </div>
</div>
<!-- /.container -->
<script>
    var beamer_config = {
        product_id: "<?php echo $this->vars->product_id;  ?>",
        selector: ".uo-beamer",
        user_firstname: "<?php echo $this->vars->first_name;  ?>",
        user_lastname: "<?php echo $this->vars->last_name;  ?>",
        user_email: "<?php echo $this->vars->email;  ?>",
        user_id: "<?php echo $this->vars->user_id;  ?>",
        language: "<?php echo $this->vars->language;  ?>",
        display: "right",
        top: 0,
        right: 0
    };
</script>
<?php if($this->vars->not_easy_invoice_diy) { ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.uo-menu-logout').on('click', '#logout_link', function(e) {
            $.post('index.php?do=auth-isLoged-auth-logout', function(o) {
                if (o.success) {
                    var ref = angular.element("base")[0].href;
                    ref = ref.substring(0, ref.length);
                    window.location.href = ref;
                }
            }, 'json');
        });
    });
</script>
<script type="text/javascript" src="https://app.getbeamer.com/js/beamer-embed.js" defer="defer"></script>
<?php  }  ?>
<?php if($this->vars->easy_invoice_diy) { ?>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.uo-menu-logout').on('click', '#logout_link', function(e) {
            $.post('index.php?do=auth-isLoged-auth-logout', function(o) {
                if (o.success) {
                    var ref = angular.element("base")[0].href;
                    ref = ref.substring(0, ref.length);
                    window.location.href = ref+"?source=mcf";
                }
            }, 'json');
        });
    });
</script>
<?php  }  ?>