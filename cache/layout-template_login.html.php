<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <base href="<?php echo $this->vars->basehref;  ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="author" content="Arkweb">
  <meta name="version" content="<?php echo $this->vars->versionNr;  ?>">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>

  <title><?php echo $this->vars->metatitle;  ?></title>


  <script type="text/javascript"><?php echo $this->vars->langjs;  ?></script>
  <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i|Raleway&subset=latin-ext" rel="stylesheet"> -->
<!--   <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" /> -->
  <link href="bootstrap/css/bootstrap.min.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" />
  <link href="js/libraries/selectize/selectize.bootstrap3.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" />
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"> -->
  <link href="font_awesome/css/font-awesome.min.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" />
 <!--  <link href="bootstrap/css/angular-toggle-switch-bootstrap-3.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" > -->
  <!-- <link href="css/scrolling-tabs.min.css" rel="stylesheet" type="text/css" media="screen" /> -->
  <?php if($this->vars->not_easy_invoice_diy) { ?>
     <link href="bootstrap/css/akti.css" rel="stylesheet" type="text/css" media="screen" />
  <?php  }  ?>
  <?php if($this->vars->easy_invoice_diy) { ?>
     <link href="bootstrap/css/jefacture.css" rel="stylesheet" type="text/css" media="screen" />
  <?php  }  ?>
  <link href="bootstrap/css/bootstrap-theme.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" />
 <!--  <link href="css/ng-sortable.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" /> -->

  <!-- color pick -->
<!--   <link rel="stylesheet" type="text/css" href="libraries/angular-colorpicker-directive-master/css/color-picker.min.css" /> -->
  
  <link href="js/libraries/scrollbar/custom_scrollbar.min.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" />
  <link href="css/lightbox.css<?php echo $this->vars->version;  ?>" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/ng-tree.css<?php echo $this->vars->version;  ?>">

  <!-- <link href="css/ng-sortable-style.css" rel="stylesheet" type="text/css" media="screen" /> -->
   <?php if($this->vars->not_easy_invoice_diy) { ?>
  <link rel="shortcut icon" href="images/akti_icon.ico" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/akti-icon_small.png">
  <?php  }  ?>
  <?php if($this->vars->easy_invoice_diy) { ?>
   <link rel="shortcut icon" href="images/je.ico" />
   <?php  }  ?>


    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <script src="js/libraries/jquery-1.11.1.min.js"></script>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-WJ32V5J');</script>
  <!-- End Google Tag Manager -->
<?php if($this->vars->not_easy_invoice_diy) { ?>
  <!-- Start of akti Zendesk Widget script -->
  <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{ o=s }catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="akti.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
  /*]]>*/</script>
  <!-- End of akti Zendesk Widget script -->
<?php  }  ?>
</head>
  <body class="login-bk">
    <div class="loading_wrap hidden"><div class="loading"></div><div class="spinner"></div></div>
    <div class="page page-login">
      <?php if($this->vars->is_first) { ?>
      <div class="content login col-md-12 landing">
        <div class="row"><?php echo $this->vars->page;  ?></div>
      </div>
      <?php  } else {  ?>
        <div class="content login col-md-12">
          <div class="row"><?php echo $this->vars->page;  ?></div>
        </div>
      <?php  }  ?>
    </div>
   
   
   
   <!--  <script src="js/libraries/tinymce/js/tinymce/tinymce.min.js"></script> -->
<!--     <script src="js/libraries/selectize/selectize.min.js"></script> -->
    <!-- <script src="js/libraries/loadsh.js"></script>
    <script src="js/libraries/a.js"></script>
    <script src="js/libraries/logger.js"></script>
    <script src="js/libraries/angular-module.js<?php echo $this->vars->version;  ?>"></script>
    <script src="js/libraries/chartjsv2.js"></script>
    <script src="js/libraries/angular-chartv2.js"></script> -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="js/libraries/ui-bootstrap-tpls-2.1.2.min.js"></script>
 -->
   <!--  <script src="js/libraries/tinymce.min.js<?php echo $this->vars->version;  ?>"></script>
    <script src="js/libraries/lightbox.js"></script> -->

    <script src="js/general.js<?php echo $this->vars->version;  ?>"></script>
    
   <!--  <script src="js/module/auth.js<?php echo $this->vars->version;  ?>"></script>
    <script src="js/module/customer.js<?php echo $this->vars->version;  ?>"></script>


    <script src="js/app_login.js<?php echo $this->vars->version;  ?>"></script>
    <script src="js/appConstants.js<?php echo $this->vars->version;  ?>"></script>
    <script src="js/services/appServices.js<?php echo $this->vars->version;  ?>"></script>
    <script src="js/directives/directives.js<?php echo $this->vars->version;  ?>"></script>

    <script src="js/controllers/auth_ctrl.js<?php echo $this->vars->version;  ?>"></script>
    <script src="js/controllers/customer_ctrl.js<?php echo $this->vars->version;  ?>"></script> -->
   

    <!-- <script src="js/controllers/auth_ctrl.js<?php echo $this->vars->version;  ?>"></script> -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WJ32V5J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

  </body>
</html>

