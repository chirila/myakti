<div class="col-md-12 trial-banner" ng-if="footer.is_trial">
    <span ng-if="footer.wizzard_complete" class="subscription-text"><?php ob_start(); ?>Your demo account will expire in <?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> {{footer.days_left}} <?php ob_start(); ?>days<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></span>
    <span ng-if="footer.HIDE_PAYMENT">
  <span ng-if="footer.wizzard_complete" class="subscription-text-link"><a href="" class="btn btn-default subs" ui-sref="{{footer.plan_go}}" title="<?php ob_start(); ?>Click here to activate your subscription<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>">
    <?php ob_start(); ?>Upgrade now<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></span>
    </span>
    <!--<span class="helpdesk account-trial">
      <a href="https://support.akti.com" target="blank"><?php ob_start(); ?>Akti Helpdesk<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>: https://support.akti.com</a>
    </span>-->
</div>
<div class="footer" ng-if="isLoged">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <a href="{{footer.privacy}}" target="_blank"><?php ob_start(); ?>Privacy Policy<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a><br>
                <small>© <a href="https://app.akti.com">Akti</a> 2017 &nbsp;&nbsp;&nbsp; Version: 1.3.1267</small>
                <!--  <a href="{{footer}}" target="_blank"><?php ob_start(); ?>Privacy Policy<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a><br> -->
                <a href="http://myhelp.akti.com/"><img src="images/akti-knowledge-base.png"><small><?php ob_start(); ?>Help<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></small></a>
                <a href="http://support.akti.com/"><img src="images/akti-faq.png"><small><?php ob_start(); ?>Help<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></small></a>
            </div>
            <div class="col-xs-6 text-right">
                <!-- <a href="../pim/admin/index.php?do=misc-timesheet">Fill in your timesheet</a> -->
                <!-- <a href="" ui-sref="myaccount" title="<?php ob_start(); ?>My Preferences<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"><?php ob_start(); ?>My Preferences<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
          <a href="" ui-sref="logout"><?php ob_start(); ?>Logout<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a> -->
                <small>Version: 1.0.492&nbsp;&nbsp;&nbsp;<?php ob_start(); ?>copyright<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <a href="https://app.akti.com">Akti</a> 2017&nbsp;©</small>
            </div>
        </div>
    </div>
</div>
<!-- /.footer -->
<script src="js/akti-layout.js">
</script>