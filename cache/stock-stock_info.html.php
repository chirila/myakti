<div class="container">
    <div class="page-header">
        <div class="row">
            <div class="col-xs-8 col-sm-6">
                <h1><?php ob_start(); ?>Inventory<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
            </div>
            <div class="col-xs-4 col-sm-6 text-right" ng-if="show_settings.is_admin_stock">
                <div class="dropdown new-settings">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li class="dropdown-item" ng-class="{ 'current':menu.submenu == 'stock_setting' }"><a href="StockSettings/" go-to="" state="stockSettings"><span class="fas fa-cog text-primary"></span> <?php ob_start(); ?>Stock Settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <div class="tabs-navigation">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="stock_info/" go-to="" state="stock_info"><?php ob_start(); ?>Transaction Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_article/" go-to="" state="stock_article"><?php ob_start(); ?>Article Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_batches/" go-to="" state="stock_batches"><?php ob_start(); ?>Based on batches<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_serial/" go-to="" state="stock_serial"><?php ob_start(); ?>Based on serial numbers<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_customer/" go-to="" state="stock_customer"><?php ob_start(); ?>Customer/Location Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_date/" go-to="" state="stock_date"><?php ob_start(); ?>Stock by Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row page-search">
                    <form>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6 col-search-input">
                                    <div class="so-field" ng-class="{ 'so-active':search.search!='' }">
                                        <span class="glyphicon glyphicon-search so-icon"></span>
                                        <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Search<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" search-it doit="search" callbk="renderList" ng-model="search.search">
                                        <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('search','')"></span>
                                    </div>
                                    <!-- /.input-search -->
                                </div>
                                <div class="col-sm-2">
<!--                                    <div class="btn-group btn-block">-->
<!--                                        <selectize placeholder="<?php ob_start(); ?>Movement type<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" config='filterCfg' options='views' ng-model="activeView"></selectize>-->
<!--                                    </div>-->
                                    <div class="so-field" ng-class="{ 'so-active':search.view }">
                                        <selectize config='filterCfg' options='view_dd' ng-model="search.view"></selectize>
                                        <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('view', '')"></span>
                                    </div>
                                </div>
                                <div class="col-sm-2  col-search-date">
                                    <div class="so-field" ng-class="{ 'so-active':search.start_date_js!=undefined }">
                                        <span class="glyphicon glyphicon-calendar so-icon" ng-click="openDate('start_date')"></span>
                                        <input type="text" class="form-control" ng-model="search.start_date_js" uib-datepicker-popup="{{format}}" is-open="datePickOpen.start_date" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" close-text="Close" placeholder="<?php ob_start(); ?>Start Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"
                                            show-button-bar="false" ng-click="datePickOpen.start_date=true" ng-change="searchThing()">
                                        <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('start_date_js',undefined)"></span>
                                    </div>
                                    <!-- /.input-date -->
                                </div>
                                <div class="col-sm-2  col-search-date white_space">
                                    <div class="so-field" ng-class="{ 'so-active':search.stop_date_js!=undefined }">
                                        <span class="glyphicon glyphicon-calendar so-icon" ng-click="openDate('stop_date')"></span>
                                        <input type="text" class="form-control" ng-model="search.stop_date_js" uib-datepicker-popup="{{format}}" is-open="datePickOpen.stop_date" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" close-text="Close" placeholder="<?php ob_start(); ?>End Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>"
                                            show-button-bar="false" ng-click="datePickOpen.stop_date=true" ng-change="searchThing()">
                                        <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('stop_date_js',undefined)"></span>
                                    </div>
                                    <!-- /.input-date -->
                                </div>
                            </div>
                        </div>
                        <div class="page-search-button">
                            <div class="btn-group" uib-dropdown>
                                <button type="button" class="btn btn-tertiary btn-square dropdown-toggle" uib-dropdown-toggle>
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                                <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                    <li ng-if="show_settings.is_admin_stock"><a href="index.php?do=stock-export_stock&amp;{{export_args}}" target="_blank"><span class="fas fa-upload"></span> <?php ob_start(); ?>Export stock transactions<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a href="#" ng-click="columnSettings()"><span class="fas fa-cog text-primary" ></span> <?php ob_start(); ?>Column settings<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-1 page-search-btn-only hide">
                            <div class="btn-group" uib-dropdown>
                                <button type="button" class="btn btn-tertiary btn-square dropdown-toggle" uib-dropdown-toggle>
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                                <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                    <li><a ui-sref="stock_info"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Action Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_article"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Article Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_customer"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Customer/Location Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_low"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Low Stock Articles<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_date"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Stock by Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>


                                </ul>
                            </div>

                        </div>


                    </form>
                </div>
                <!-- /.page-search -->
                <div class="row page-content">
                    <div class="col-xs-12">
                        <table class="table active_rows orderTable">
                            <thead>
                                <tr>

                                    <!-- <th class="col-sm-1 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='t_date') , 'sortable_row-down':(reverse && ord=='t_date') }" ng-click="orderBY('t_date')"><?php ob_start(); ?>Log Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th> -->
                                    <!-- <th class="col-sm-1 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='order_id') , 'sortable_row-down':(reverse && ord=='order_id') }" ng-click="orderBY('order_id')"><?php ob_start(); ?>Selling Ref<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                      <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='p_order_id') , 'sortable_row-down':(reverse && ord=='p_order_id') }" ng-click="orderBY('p_order_id')"><?php ob_start(); ?>Purchase Ref.<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th> -->
                                   <!--  <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='serial_number') , 'sortable_row-down':(reverse && ord=='serial_number') }" ng-click="orderBY('serial_number')"><?php ob_start(); ?>Reference<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='article_name') , 'sortable_row-down':(reverse && ord=='article_name') }" ng-click="orderBY('article_name')"><?php ob_start(); ?>Article Name<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-1 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='item_code') , 'sortable_row-down':(reverse && ord=='item_code') }" ng-click="orderBY('item_code')"><?php ob_start(); ?>Article Code<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-1 text-muted"><?php ob_start(); ?>Action made<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> </th>
                                    <th class="col-sm-1 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='delivery_Date') , 'sortable_row-down':(reverse && ord=='delivery_Date') }" ng-click="orderBY('delivery_Date')"><?php ob_start(); ?>Delivery Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 text-muted"><?php ob_start(); ?>Updated by<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-1 text-muted "><?php ob_start(); ?>Previous Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-1  text-muted"><?php ob_start(); ?>Actual Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th> -->
                                    <th ng-repeat="col in columns" ng-class="{ 'sortable_row':col.order_by, 'sortable_row-up':(!reverse && col.order_by && ord==col.order_by), 'sortable_row-down':(reverse && col.order_by && ord==col.order_by), 'text-muted':(col.column_name=='movement_type' || col.column_name=='created_by' || col.column_name=='stock' || col.column_name=='new_stock') }"
                                        ng-click="(col.order_by ? orderBY(col.order_by) : '')">
                                        <span ng-bind-html="col.name"></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in list" class="collapseTr">
                                    <td class="text-muted" ng-repeat="col in columns" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        <a href="order/view/{{item.order_id}}" class="text-muted" ng-if="col.column_name=='selling_reference' && item.order_id && !item.p_order_id"><span>{{::item[col.column_name]}}</span></a>
                                        <a href="purchase_order/view/{{item.p_order_id}}" class="text-muted" ng-if="col.column_name=='selling_reference' && !item.order_id && item.p_order_id"><span>{{::item[col.column_name]}}</span></a>
                                        <span ng-if="col.column_name=='selling_reference' && !item.order_id && !item.p_order_id">{{::item[col.column_name]}}</span>
                                        <span ng-if="col.column_name=='created_by'">
                                            <span ng-if="item.edit_reason.username">{{::item.edit_reason.username}}</span>
                                            <a href="#" ng-if="item.edit_reason.username" ng-click="openModal('viewStock',item,'md')"><span class="glyphicon glyphicon-info-sign"></span></a>
                                            <span ng-if="!item.edit_reason.username">{{::item[col.column_name]}}</span>
                                        </span>
                                        <span ng-if="col.column_name!='selling_reference' && col.column_name!='created_by'">{{::item[col.column_name]}}</span>
                                    </td>
                                    <!-- <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.date}}
                                    </td>
                                    <td ng-if="item.order_id && !item.p_order_id" go-to="" state="{{::item.state}}" params='{"order_id": {{::item.order_id}}}' class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.selling_reference}}
                                    </td>
                                    <td ng-if="!item.order_id && item.p_order_id" go-to="" state="purchase_order.view" params='{"p_order_id": {{::item.p_order_id}}}' class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.selling_reference}}
                                    </td>
                                    <td ng-if="!item.order_id && !item.p_order_id" class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.selling_reference}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.article_name}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.item_code}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.movement_type}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>{{::item.delivery_date}}</td>
                                    <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        <span ng-if="item.edit_reason.username">{{::item.edit_reason.username}}</span>
                                        <a href="#" ng-if="item.edit_reason.username" ng-click="openModal('viewStock',item,'md')"><span class="glyphicon glyphicon-info-sign"></span></a>
                                        <span ng-if="!item.edit_reason.username">{{::item.created_by}}</span>
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.stock}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_info", "obj": { "search":{{search}} } }'>
                                        {{::item.new_stock}}
                                    </td> -->
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.page-content -->
            </div>
        </div>
    </div>

    <!-- <pag-custom pages="pager" refresh="rederList()" doit='order-orders'></pag-custom> -->
    <div class="col-sm-12">
        <div class="col-sm-8">
            <ul ng-if="max_rows > 30" uib-pagination total-items="max_rows" ng-model="search.offset" ng-change="searchThing()" max-size="5" class="pager" boundary-links="true" items-per-page="lr"></ul>
        </div>
        <div class="col-sm-4 text-right">
            <label><?php ob_start(); ?>Number of transaction based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>: <strong>{{max_rows}}</strong></label>
        </div>
    </div>
</div>
<!-- neede on all pages -->