<div class="container">
    <div class="page-header">
        <div class="row">
            <div class="col-xs-8 col-sm-6">
                <h1><?php ob_start(); ?>Inventory<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
            </div>
            <div class="col-xs-4 col-sm-6 text-right" ng-if="show_settings.is_admin_stock">
                <div class="dropdown new-settings">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li class="dropdown-item" ng-class="{ 'current':menu.submenu == 'stock_setting' }"><a href="StockSettings/" go-to="" state="stockSettings"><span class="fas fa-cog text-primary"></span> <?php ob_start(); ?>Stock Settings<?php echo $this->vars->enl;  ?></a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <div class="tabs-navigation">
        <ul class="nav nav-tabs">
            <li>
                <a href="stock_info/" go-to="" state="stock_info"><?php ob_start(); ?>Transaction Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li class="active">
                <a href="stock_article/" go-to="" state="stock_article"><?php ob_start(); ?>Article Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_batches/" go-to="" state="stock_batches"><?php ob_start(); ?>Based on batches<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_serial/" go-to="" state="stock_serial"><?php ob_start(); ?>Based on serial numbers<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_customer/" go-to="" state="stock_customer"><?php ob_start(); ?>Customer/Location Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li>
                <a href="stock_date/" go-to="" state="stock_date"><?php ob_start(); ?>Stock by Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row page-search page-search-options">
                    <form>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 col-search-input">
                                    <div class="so-field" ng-class="{ 'so-active':search.search!='' }">
                                        <span class="glyphicon glyphicon-search so-icon"></span>
                                        <input type="text" class="form-control" placeholder="<?php ob_start(); ?>Search<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" search-it doit="search" callbk="renderList" ng-model="search.search">
                                        <span class="glyphicon glyphicon-remove so-icon so-remove" ng-click="toggleSearchButtons('search','')"></span>
                                    </div>
                                    <!-- /.input-search -->
                                </div>
                            </div>
                        </div>

                        <div class="page-search-button">
                            <div class="btn-group" uib-dropdown>
                                <button type="button" class="btn btn-tertiary btn-square dropdown-toggle" uib-dropdown-toggle>
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
                                <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                    <li ng-if="show_settings.is_admin_stock"><a href="index.php?do=stock-export_stock&amp;{{export_args}}" target="_blank"><span class="fas fa-upload"></span> <?php ob_start(); ?>Export stock transactions<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <!--  <li ng-if="show_settings.is_admin_stock"><a href="index.php?do=stock-export_stock&amp;{{export_args_location}}" target="_blank"><span class="fas fa-upload"></span> <?php ob_start(); ?>Export stock at main location<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li> -->
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-1 page-search-btn-only hide">
                            <div class="btn-group" uib-dropdown>
                                <button type="button" class="btn btn-tertiary btn-square dropdown-toggle" uib-dropdown-toggle>
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
                                <ul class="dropdown-menu dropdown-menu-right" uib-dropdown-menu>
                                    <li><a ui-sref="stock_info"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Action Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_article"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Article Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_customer"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Customer/Location Based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_low"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Low Stock Articles<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                                    <li><a ui-sref="stock_date"><span class="glyphicon glyphicon-info-sign text-primary"></span> <?php ob_start(); ?>Stock by Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>


                                </ul>
                            </div>

                        </div>


                    </form>
                </div>
                <!-- /.page-search -->
                <div class="row page-content">
                    <div class="col-xs-12">
                        <table class="table active_rows orderTable">
                            <thead>
                                <tr>

                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='item_code') , 'sortable_row-down':(reverse && ord=='item_code') }" ng-click="orderBY('item_code')"><?php ob_start(); ?>Code<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='internal_name') , 'sortable_row-down':(reverse && ord=='internal_name') }" ng-click="orderBY('internal_name')"><?php ob_start(); ?>Internal Name<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='article_category_id') , 'sortable_row-down':(reverse && ord=='article_category_id') }" ng-click="orderBY('article_category_id')"><?php ob_start(); ?>Article Category<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='stock') , 'sortable_row-down':(reverse && ord=='stock') }" ng-click="orderBY('stock')"><?php ob_start(); ?>Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 text-muted sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='virtual_stock') , 'sortable_row-down':(reverse && ord=='virtual_stock') }" ng-click="orderBY('virtual_stock')"><?php ob_start(); ?>Items Ordered<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                    <th class="col-sm-2 sortable_row" ng-class="{ 'sortable_row-up':(!reverse && ord=='article_threshold_value') , 'sortable_row-down':(reverse && ord=='article_threshold_value') }" ng-click="orderBY('article_threshold_value')"><?php ob_start(); ?>Threshold<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>



                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="item in list" class="collapseTr">
                                    <td class="text-muted expand_row" list='{ "type":"stock-stock_article", "obj": { "search":{{search}} } }'>
                                        <i ng-if="item.stock_multiple_locations" class="fa fa-chevron-right btn-expand" ng-click="showStats(item,$index)" ng-class="{'fa-chevron-down': item.isopen }"></i>
                                        <!-- <span ng-if="item.stock_multiple_locations" class="glyphicon glyphicon-plus btn-expand" ng-click="showStats(item,$index)"></span> -->

                                        <span class="text-ellipsis">  {{::item.code}}</span>
                                    </td>

                                    <td class="text-muted" list='{ "type":"stock-stock_article", "obj": { "search":{{search}} } }'>
                                        {{::item.name}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_article", "obj": { "search":{{search}} } }'>
                                        {{::item.article_category}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_article", "obj": { "search":{{search}} } }'>
                                        {{::item.stock}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_article", "obj": { "search":{{search}} } }'>
                                        {{::item.virtual_stock}}
                                    </td>
                                    <td class="text-muted" list='{ "type":"stock-stock_article", "obj": { "search":{{search}} } }'>
                                        {{::item.threshold}}
                                    </td>



                                </tr>

                                <tr ng-repeat-end class="versionInfo hidden">
                                    <td colspan="6" class="no_border">
                                        <table class="table active_rows orderTable">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-2"><?php ob_start(); ?>Depot<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                                    <th class="col-sm-2"><?php ob_start(); ?>Address<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                                    <th class="col-sm-2"><?php ob_start(); ?>Zip<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                                    <th class="col-sm-2"><?php ob_start(); ?>City<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                                    <th class="col-sm-2"><?php ob_start(); ?>Country<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                                    <th class="col-sm-2"><?php ob_start(); ?>Stock<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="item2 in listStats[$index].list">
                                                    <td>
                                                        <span class="text-ellipsis">{{item2.customer_name}}</span>
                                                    </td>
                                                    <td>
                                                        <span class="text-ellipsis">{{item2.address}}</span>
                                                    </td>
                                                    <td>
                                                        <span class="text-ellipsis">{{item2.zip}}</span>
                                                    </td>
                                                    <td>
                                                        <span class="text-ellipsis">{{item2.city}}</span>
                                                    </td>
                                                    <td>
                                                        <span class="text-ellipsis">{{item2.country}}</span>
                                                    </td>
                                                    <td>
                                                        <span class="text-ellipsis">{{item2.current_quantity}}</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.page-content -->
            </div>
        </div>
    </div>

    <!-- <pag-custom pages="pager" refresh="rederList()" doit='order-orders'></pag-custom> -->
    <div class="col-sm-12">
        <div class="col-sm-8">
            <ul ng-if="max_rows > 30" uib-pagination total-items="max_rows" ng-model="search.offset" ng-change="searchThing()" max-size="5" class="pager" boundary-links="true" items-per-page="lr"></ul>
        </div>
        <div class="col-sm-4 text-right">
            <label><?php ob_start(); ?>Number of article based<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>: <strong>{{max_rows}}</strong></label>
        </div>
    </div>
</div>
<!-- neede on all pages -->