<div ng-if="isLoged">
  <div class="container header_module" ng-if="show_settings.is_6 && main_menu[6] && (nextState == 'orders' || nextState == 'order_deliveries' || nextState == 'orders_deliver')">
    <div class="page-header">
      <div class="row">
            <div class="col-xs-8 col-sm-6">
              <h1><?php ob_start(); ?>Sales Orders<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
            </div>
            <div class="col-xs-4 col-sm-6 text-right" ng-if="show_settings.is_admin_order">
                <div class="dropdown new-settings">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                         <li class="dropdown-item" ng-class="{ 'current':menu.submenu == 'orders setting' }"><a go-to="" href="orderSettings/" state="orderSettings"><span class="fas fa-cog text-primary"></span> <?php ob_start(); ?>Order Module<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                    </div>
                </div>
            </div>
      </div>
    </div>
    <div class="tabs-navigation">
        <ul class="nav nav-tabs nav_plus" role="tablist" ng-if="show_settings.is_6">
            <li ng-class="{ 'active':menu.submenu == 'orders' }">
                <a href="#orders" go-to="" state="orders"><?php ob_start(); ?>Sales Orders<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                <div class="btn-group nav_inline">
                    <button type="button" class="btn btn-primary btn-add"  go-to="" state="order.edit" params='{ "order_id": "tmp", "buyer_id":"tmp" }'><span class="btn-add_icon"><span class="icon-bar"></span><span class="icon-bar-2"></span></span></button>
                </div>
            </li>
            <li ng-class="{ 'active':menu.submenu == 'order_deliveries' }">
                <a go-to="" href="order_deliveries/" state="order_deliveries"><?php ob_start(); ?>Deliveries<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-class="{ 'active':menu.submenu == 'orders_deliver' }">
                <a go-to="" href="orders_deliver/" state="orders_deliver"><?php ob_start(); ?>To be delivered<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
        </ul>
    </div>
  </div>  
  <div class="container header_module" ng-if="show_settings.is_4 && main_menu[4] && (nextState == 'invoices' || nextState == 'reminders' || nextState == 'recinvoices' || nextState == 'sepa' || nextState == 'payments')">
     <div class="page-header">
        <div class="row">
            <div class="col-xs-8 col-sm-6">
                <h1><?php ob_start(); ?>Sales Invoices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
            </div>
            <div class="col-xs-4 col-sm-6 text-right" ng-if="show_settings.is_admin_invoice">
                  <div class="dropdown new-settings">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <li class="dropdown-item" ng-if="show_settings.noteasy && show_settings.is_admin_invoice" ng-class="{ 'current':menu.submenu == 'import' }"><a go-to=""  href="import_invoices/" state="import_invoices"><i class="fas fa-download"></i> <?php ob_start(); ?>Import<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                            <li class="dropdown-item" ng-if="show_settings.is_admin_invoice" ng-class="{ 'current':menu.submenu == 'invoices setting' }"><a go-to="" href="invoiceSettings/" state="invoiceSettings"><span class="fas fa-cog text-primary"></span> <?php ob_start(); ?>Invoice Module<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a> </li>
                      </div>
                  </div>
            </div>
        </div>
      </div><!-- /.page-header -->
      <div class="tabs-navigation"> 
        <ul class="nav nav-tabs nav_plus" role="tablist" ng-if="show_settings.is_4">
            <li ng-class="{ 'active':menu.submenu == 'invoices' && !invoice_tab,'uo-quick_add-show': show.quickAdd }">
                <a ng-click="setInvoiceTab('')" href="#invoices" go-to="" state="invoices" params='{"tab": "invoices" }'><?php ob_start(); ?>Invoices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                <div class="btn-group" uib-dropdown uib-dropdown-toggle >
                <button type="button" class="btn btn-primary btn-add dropdown-toggle quick_add"  ><span class="btn-add_icon"><span class="icon-bar"></span><span class="icon-bar-2"></span></span></button>
                <ul class="dropdown-menu dropdown-menu-left" uib-dropdown-menu>
                <li><a ui-sref="invoice.edit({'invoice_id': 'tmp', 'type':'0'})"><i class="far fa-file-alt"></i> <?php ob_start(); ?>Invoice<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                <li><a ui-sref="invoice.edit({'invoice_id': 'tmp', 'type':'2'})"><i class="far fa-file-alt"></i> <?php ob_start(); ?>Credit Invoice<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                <li><a ui-sref="invoice.edit({'invoice_id': 'tmp', 'type':'1'})"><i class="far fa-file-alt"></i> <?php ob_start(); ?>Proforma Invoice<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                <li ng-if="show_settings.unique_id == 'salesassist_2' || show_settings.unique_id == '0561881e_90e4_ed68_616f5472328b'"><a ui-sref="invoice.edit({'invoice_id': 'tmp', 'type':'3'})"><i class="far fa-file-alt"></i> <?php ob_start(); ?>Invoice for international activities<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                <li ng-if="show_settings.unique_id == 'salesassist_2' || show_settings.unique_id == '0561881e_90e4_ed68_616f5472328b'"><a ui-sref="invoice.edit({'invoice_id': 'tmp', 'type':'4'})"><i class="far fa-file-alt"></i> <?php ob_start(); ?>Credit Invoice for international activities<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a></li>
                </ul>
                </div>
                </li>
            <li ng-if="show_settings.not_easyinv_subscription && show_settings.not_easy_invoice_diy" ng-class="{ 'active':menu.submenu == 'reminders' }">
                <a go-to="" href="reminders/" state="reminders"><?php ob_start(); ?>Reminders<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.not_easyinv_subscription  && show_settings.not_easy_invoice_diy" ng-class="{ 'active':menu.submenu == 'recinvoices' }">
                <a go-to="" href="recinvoices/" state="recinvoices"><?php ob_start(); ?>Recurring Invoices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
                <button type="button" class="btn btn-primary btn-add"  ui-sref="recinvoice.edit({'recurring_invoice_id': 'tmp'})" ><span class="btn-add_icon"><span class="icon-bar"></span><span class="icon-bar-2"></span></span></button>
            </li>
            <li ng-if="show_settings.not_easyinv_subscription && show_settings.show_sepa  && show_settings.not_easy_invoice_diy" ng-class="{ 'active':menu.submenu == 'sepa' }">
                <a go-to="" href="sepa/" state="sepa"><?php ob_start(); ?>SEPA<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.not_easyinv_subscription  && show_settings.not_easy_invoice_diy" ng-class="{ 'active':menu.submenu == 'payments' }">
                <a go-to="" href="payments/" state="payments"><?php ob_start(); ?>Payments<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.show_deliveries  && show_settings.not_easy_invoice_diy" ng-class="{ 'active':invoice_tab=='deliveries' }">
                <a ng-click="setInvoiceTab('deliveries')" href="deliveries" go-to="" state="invoices" params='{"tab": "deliveries" }'><?php ob_start(); ?>Deliveries<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span ng-if="nr_show"> <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i></span><span ng-if="!nr_show"> <span ng-bind-html="amount_o"></span></span></a>
            </li>
            <li ng-if="show_settings.show_projects  && show_settings.not_easy_invoice_diy" ng-class="{ 'active':invoice_tab=='projects' }">
                <a ng-click="setInvoiceTab('projects')" href="projects" go-to="" state="invoices" params='{"tab": "projects" }'><?php ob_start(); ?>Timetracker<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span ng-if="nr_show"> <i class="fa fa-spinner fa-spin fa-1x fa-fw" ng-if="nr_show"></i></span><span ng-if="!nr_show"> <span ng-bind-html="total_amount_p" ng-if="!nr_show"></span></span>
                </a>
            </li>
            <li ng-if="show_settings.show_interventions  && show_settings.not_easy_invoice_diy" ng-class="{ 'active':invoice_tab=='interventions' }">
                <a ng-click="setInvoiceTab('interventions')" href="interventions" go-to="" state="invoices" params='{"tab": "interventions" }'><?php ob_start(); ?>Interventions<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span ng-if="nr_show"> <i class="fa fa-spinner fa-spin fa-1x fa-fw" ng-if="nr_show"></i></span><span ng-if="!nr_show"> <span ng-bind-html="amount_s" ng-if="!nr_show"></span></span>
                </a>
            </li>
        </ul>
      </div>
  </div>
  <div class="container header_module" ng-if="show_settings.is_18 && main_menu[18] && (nextState == 'report_catalogue' || nextState == 'report_catalogue_invoices' || nextState=='report_quotes' || nextState=='report_orders' || nextState=='report_p_orders' || nextState=='report_projects' || nextState=='report_invoices' || nextState=='report_client_history')">
      <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php ob_start(); ?>Insights<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h1>
            </div>
        </div>
      </div><!-- /.page-header -->
      <div class="tabs-navigation">
        <ul class="nav nav-tabs" role="tablist" ng-if="show_settings.is_18">
            <li ng-if="show_settings.showcatalog" ng-class="{ 'active':menu.submenu == 'report_catalogue' }">
                <a go-to="" href="report_catalogue/" state="report_catalogue"><?php ob_start(); ?>Catalog<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.showcataloginv" ng-class="{ 'active':menu.submenu == 'report_catalogue_invoices' }">
                <a go-to="" href="report_catalogue_invoices/" state="report_catalogue_invoices"><?php ob_start(); ?>Catalogue (based on invoices)<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.is_5 && show_settings.not_easy_invoice_diy" ng-class="{ 'active':menu.submenu == 'report_quotes' }">
                <a go-to="" href="report_quotes/" state="report_quotes"><?php ob_start(); ?>Quotes<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.is_6" ng-class="{ 'active':menu.submenu == 'report_orders' }">
                <a go-to="" href="report_orders/" state="report_orders"><?php ob_start(); ?>Orders<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.is_14" ng-class="{ 'active':menu.submenu == 'report_p_orders' }">
                <a go-to="" href="report_p_orders/" state="report_p_orders"><?php ob_start(); ?>Purchase Orders<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.is_3" ng-class="{ 'active':menu.submenu == 'report_projects' }">
                <a go-to="" href="report_projects/" state="report_projects"><?php ob_start(); ?>Projects<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.is_4" ng-class="{ 'active':menu.submenu == 'report_invoices' }">
                <a go-to="" href="report_invoices/" state="report_invoices"><?php ob_start(); ?>Invoices<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
            <li ng-if="show_settings.is_4" ng-class="{ 'active':menu.submenu == 'report_client_history' }">
                <a go-to="" href="report_client_history/" state="report_client_history"><?php ob_start(); ?>Client History<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></a>
            </li>
        </ul>
      </div>
  </div>
</div>

