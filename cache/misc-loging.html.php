<div class="row col-top">
    <div class="col-sm-6" ng-if="advanced == 'true' && log.page !=undefined && log.page !='invoice' && log.page !='quote' && log.page !='order' && log.page !='p_order' && log.page!='purchase_invoice' && log.page!='recurring_invoice' && (log.page != 'service' || (log.page == 'service' && !log.regular_intervention)) && log.page!='installation' && log.page!='stock_disp' && log.page!='project'">
        <h4 class="title_underline"><?php ob_start(); ?>Activity Log<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h4>	
        <div ng-class="{ 'module_steps-highlight' : log.comment_style }">
            <div ng-if="comment.comments">
                <ul class="list-plain">
                    <li ng-repeat="item in comment.comments" ng-hide="comment.hide_comment">
                        <a class="glyphicon glyphicon-remove text-muted pull-right" title="<?php ob_start(); ?>Delete comment<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="dataComment(comment_form, 'delete_comment', item)"></a>
                        <p><strong>{{item.name}}</strong> <em class="text-muted">{{item.time}}</em></p>
                        <p ng-bind-html="item.message">{{item.message}}</p>
                        <small class="col-right text-muted" ng-if="item.assign_to"><span class="glyphicon glyphicon-user"></span> {{item.to}}</small>
                        <small class="text-muted" ng-if="item.due_date"><span class="glyphicon glyphicon-calendar"></span> {{item.due_date}}</small>
                    </li>
                </ul>
                <div class="buttons text-center">
                    <button ng-if="comment.comment && seeCmts" class="btn btn-plain" type="button" title="<?php ob_start(); ?>Show All<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="loadComments()">{{comment.count}} <?php ob_start(); ?>more comments<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
                    <button ng-if="comment.comment && !seeCmts" class="btn btn-plain" type="button" title="<?php ob_start(); ?>show less<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-click="loadComments()"><?php ob_start(); ?>show less<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
                    <button type="submit" class="btn btn-default" title="<?php ob_start(); ?>Add Comment<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" ng-show="comment.add_comment" ng-click="show_form()"><?php ob_start(); ?>Add Comment<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
                </div>
            </div>    
            <form name="comment_form" ng-hide="log.hide_comment_form">
                <!-- <div class="form-group">
                    <label><?php ob_start(); ?>New Message<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                    <textarea class="form-control" name="comment" cols="10" ng-model="log.comment" required></textarea>
                </div> -->
                <div class="form-group" ng-class="{ 'form-group_alert' : comment_form.comment.$invalid && !comment_form.comment.$pristine }">
                    <label for="comment" class="control-label"><?php ob_start(); ?>New Message<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span class="mandatory-label">*</span></label>
                    <div>
                        <textarea name="comment" cols="10" class="form-control" ng-model="log.comment" id="comment" name="comment" required ></textarea>
                        <!-- <div class="alert alert-sm alert-danger ng-cloak" ng-if="comment_form.comment.$invalid && alertsField.comment">{{alertsField.comment}}</div> -->
                    </div>
                </div>
                <div class="row form-group">
                    <!-- <div class="col-sm-6">
                        <label><?php ob_start(); ?>Assign to<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                        <select class="form-control" name="to_user_id" ng-model="log.to_user_id">
                            <option value="0"><?php ob_start(); ?>Select<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></option>
                            <option ng-repeat="option in log.user_dd track by option.id" value="{{option.id}}">{{option.value}}</option>
                        </select>
                    </div> -->
                    <div class="form-group col-sm-6">
                        <label for="to_user_id" class="control-label"><?php ob_start(); ?>Assign to<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span class="mandatory-label">*</span></label>
                        <div>
                            <select class="form-control" name="to_user_id" ng-model="log.to_user_id" required>
                            <option value=""><?php ob_start(); ?>Select<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></option>
                            <option ng-repeat="option in log.user_dd track by option.id" value="{{option.id}}">{{option.value}}</option>
                        </select>
                        </div>
                    </div>
                    <!-- <div class="col-sm-6">
                        <label><?php ob_start(); ?>Due Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                        <div class="so-field">
                            <span class="glyphicon glyphicon-calendar so-icon"></span>
                            <input type="text" class="form-control" ng-model="log.comment_due_date" uib-datepicker-popup="{{log.format}}" is-open="log.datePickOpen.comment_due_date" datepicker-options="log.dateOptions" close-text="Close" placeholder="<?php ob_start(); ?>Due Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" show-button-bar="false" ng-click="log.datePickOpen.comment_due_date = true" name="comment_due_date" required />
                            <span class="glyphicon glyphicon-remove so-icon so-remove"></span>
                        </div> 
                    </div> -->
                    <div class="form-group col-sm-6" >
                        <label for="due_date" class="control-label"><?php ob_start(); ?>Due Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span class="mandatory-label">*</span></label>
                        <div class="so-field">
                            <span class="glyphicon glyphicon-calendar so-icon"></span>
                            <input type="text" class="form-control" ng-model="log.comment_due_date" uib-datepicker-popup="{{log.format}}" is-open="log.datePickOpen.comment_due_date" datepicker-options="log.dateOptions" close-text="Close" placeholder="<?php ob_start(); ?>Due Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" show-button-bar="false" ng-click="log.datePickOpen.comment_due_date = true" name="comment_due_date" required />
                            <span class="glyphicon glyphicon-remove so-icon so-remove"></span>
                        </div> 
                    </div>
                </div>
                <div class="text-right">
                    <!-- <button type="submit" class="btn btn-default" ng-click="dataComment(comment_form, 'insert_comment')"><?php ob_start(); ?>Add Comment<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button> -->
                    <button class="btn btn-default" ng-disabled="comment_form.$invalid" ng-click="dataComment(comment_form, 'insert_comment')" type="submit"><?php ob_start(); ?>Add Comment<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
                </div>
            </form>    
        </div>
    </div>
    <div ng-if="log.history_style" ng-class="{'col-md-12' : 'isContactView', 'col-md-12':'!isContactView'}">
        <div class="history-title">
            <h4 ng-class="{ 'title_underline':log.page !=undefined && log.page!='quote' && log.page!='order' && log.page !='p_order' && log.page !='invoice' && log.page!='purchase_invoice' && log.page!='recurring_invoice' && (log.page != 'service' || (log.page == 'service' && !log.regular_intervention)) && log.page!='installation' && log.page!='stock_disp' && log.page!='project'}">
                <strong ng-if="log.page=='quote' || log.page=='order' || log.page =='p_order' || log.page =='invoice' || log.page=='purchase_invoice' || log.page=='recurring_invoice' || (log.page == 'service' && log.regular_intervention) || log.page=='installation' || log.page=='stock_disp' || log.page=='project'"><?php ob_start(); ?>History Log<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></strong>
                <h2 ng-if="log.page !=undefined && log.page!='quote' && log.page!='order' && log.page !='p_order' && log.page !='invoice' && log.page!='purchase_invoice' && log.page!='recurring_invoice' && (log.page != 'service' || (log.page == 'service' && !log.regular_intervention)) && log.page!='installation' && log.page!='stock_disp' && log.page!='project'" class="row"><?php ob_start(); ?>History Log<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h2>
            </h4>    
         
           
        </div>
        <!-- <p ng-if="log.field_name != 'article_id'"><strong>{{log.user_name}}</strong> {{log.created}} <em class="text-muted">{{log.ago}}</em></p> -->
        <ul class="list-unordered" ng-if="(isContactView) || !isContactView">
            <li class="text-muted" ng-repeat="item in log.messages"><span ng-bind-html="item.message"></span> <small class="text-strong">{{item.dates}}</small></li>
        </ul>
        <div class="col-top text-center" ng-if="log.more && !seeMore && isContactView">
            <button class="btn btn-plain" ng-click="loadActions()"><?php ob_start(); ?>Show More<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <i class="fas fa-chevron-right"></i></button>
        </div>
        <div class="col-top text-center" ng-if="log.more && seeMore && isContactView">
            <button class="btn btn-plain" ng-click="loadActions()"><?php ob_start(); ?>Show Less<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <i class="fas fa-chevron-up"></i></button>
        </div>
        <div class="col-top text-center" ng-if="log.more && !seeMore && !isContactView">
            <button class="btn btn-plain" ng-click="loadActions()"><?php ob_start(); ?>Show All<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
        </div>
        <div class="col-top text-center" ng-if="log.more && seeMore && !isContactView">
            <button class="btn btn-plain" ng-click="loadActions()"><?php ob_start(); ?>Show Less<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
        </div>
    </div>
    <div ng-if="log.page=='quote' || log.page=='order' || log.page =='p_order' || log.page =='invoice' || log.page=='purchase_invoice' || log.page=='recurring_invoice' || (log.page == 'service' && log.regular_intervention) || log.page=='installation' || log.page=='stock_disp' || log.page=='project'" class="col-sm-6">
        <h4><strong><?php ob_start(); ?>Internal Notes<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></strong></h4>  
        <div class="col-sm-12">
            <div class="form-horizontal text-left form-squeeze deal-notes" name="notes" novalidate>
                <div class="row">
                    <textarea ui-tinymce="notesTinymceOpts" ng-model="log.notes" class="variable_assign" ng-change="editNotes()">{{log.notes}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>