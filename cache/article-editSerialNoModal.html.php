<div class="modal-header">
  	<button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="editSerialNo.cancel()"><span aria-hidden="true">&times;</span></button>
  	<h4 class="modal-title"><?php ob_start(); ?>Edit Serial Number<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></h4>
</div>
<div class="modal-body">


<form class="form-horizontal text-left ng-pristine ng-valid" name="serial_no_form" novalidate="">

  <div class="col-sm-5">
<div class="form-group">
                        <label for="serial_number" class="col-sm-6 control-label"><?php ob_start(); ?>Serialnumber<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span class="mandatory-label">*</span></label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" ng-model="editSerialNo.obj.serial_number" id="serial_number">
                        </div>
                     </div>
   <div class="form-group">
               <label for="status" class="col-sm-6 control-label"><?php ob_start(); ?>Status<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span class="mandatory-label">*</span></label>
                        <div class="col-sm-6">
                           <select class="form-control" ng-model="editSerialNo.obj.status_id" id="inputVat" name="status_id" required >
                              <option ng-repeat="option in editSerialNo.obj.status_dd track by option.id" value="{{option.id}}">{{option.value}}</option>
                           </select>
                        </div>
                     </div>  
      <div class="form-group" ng-if="editSerialNo.obj.status_id==1">
                   <label for="status" class="col-sm-6 control-label"><?php ob_start(); ?>Status Details<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                        <div class="col-sm-6">
                           <textarea id="serial_number" class="form-control ng-pristine ng-valid ng-touched" rows="5" type="text" ng-model="editSerialNo.obj.status_details_1" ></textarea>

                        </div>
          </div>   
    <div class="form-group" ng-if="editSerialNo.obj.status_id==2">
                   <label for="status" class="col-sm-6 control-label"><?php ob_start(); ?>Status Details<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                        <div class="col-sm-6">
                           <textarea id="serial_number" class="form-control ng-pristine ng-valid ng-touched" rows="5" type="text" ng-model="editSerialNo.obj.status_details_2" ></textarea>

                        </div>
          </div>   
  <div class="form-group" ng-if="editSerialNo.obj.status_id==3">
                   <label for="status" class="col-sm-6 control-label"><?php ob_start(); ?>Status Details<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
                        <div class="col-sm-6">
                           <textarea id="serial_number" class="form-control ng-pristine ng-valid ng-touched" rows="5" type="text" ng-model="editSerialNo.obj.status_details_3" ></textarea>

                        </div>
          </div>                                                   



  </div>
 <div class="col-sm-4">
      <div class="form-group">
              <label for="serial_number" class="col-sm-6 control-label"><?php ob_start(); ?>In Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?> <span class="mandatory-label">*</span></label>
              <div class="col-sm-6">
                <div class="so-field input-date">
                  <span class="glyphicon glyphicon-calendar so-icon" ng-click="editSerialNo.openDate('date')"></span>
                  <input type="text" class="form-control form-control-border-right" ng-model="editSerialNo.obj.date_in" uib-datepicker-popup="{{editSerialNo.format}}" is-open="editSerialNo.datePickOpen.date" datepicker-options="editSerialNo.obj.dateOptions" ng-required="true" close-text="Close" placeholder="<?php ob_start(); ?>In Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" show-button-bar="false" ng-click="editSerialNo.datePickOpen.date=true" />
                </div><!-- /.so-field -->
              </div>
      </div>
      <div class="form-group">
        <label for="serial_number" class="col-sm-6 control-label"><?php ob_start(); ?>Out Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
        <div class="col-sm-6">
               <div class="so-field input-date">
                <span class="glyphicon glyphicon-calendar so-icon" ng-click="editSerialNo.openDate('date2')"></span>
                <input type="text" class="form-control form-control-border-right" ng-model="editSerialNo.obj.date_out" uib-datepicker-popup="{{editSerialNo.format}}" is-open="editSerialNo.datePickOpen.date2" datepicker-options="editSerialNo.obj.dateOptions" ng-required="true" close-text="Close" placeholder="<?php ob_start(); ?>Out Date<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" show-button-bar="false" ng-click="editSerialNo.datePickOpen.date2=true" />
              </div><!-- /.so-field -->
        </div>
     </div>   
      <div class="form-group">
        <label for="serial_number" class="col-sm-6 control-label"><?php ob_start(); ?>Stock Location<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></label>
        <div class="col-sm-6">
               <div class="so-field input-date">
                  <selectize placeholder="<?php ob_start(); ?>All Locations<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?>" config='editSerialNo.locationFilterCfg' options='editSerialNo.obj.addresses' ng-model="editSerialNo.obj.address_id" ></selectize>
                </div><!-- /.so-field -->
        </div>
     </div>              
  </div>


 
      
</form>



</div>	

<div class="modal-footer">
      <button class="btn btn-plain" type="button" ng-click="editSerialNo.cancel()"><?php ob_start(); ?>Cancel<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
    	<button class="btn btn-default" type="button" ng-click="editSerialNo.ok(serial_no_form)"><?php ob_start(); ?>Save<?php $mllabel = ob_get_clean();

		echo gm($mllabel) ?></button>
</div>