<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * -------------------------------------------------------------------
 * 	DATABASE CONNECTION SETTINGS
 * -------------------------------------------------------------------
 * This file contains the settings needed for you to access your database.
 * For more information read the documentation.
 *
 * -------------------------------------------------------------------
 * 	HELP
 * -------------------------------------------------------------------
 * All the settings are kept in an associative array.
 * The $database_config['default'] key contains the name of the default database driver,
 * by default this is mysql.
 *
 * The $database_config['allow_multiple_drivers'] keys determines if multiple drivers
 * are allowed.
 *
 * The $database_config also contains all the settings required to connect to the database
 * grouped by the driver name. The expected keys in this array are:
 * 		[hostname]			The hostname of your database server.
 * 		[username]			The username used to connect to the database
 * 		[password]			The password used to connect to the database
 * 		[database]			The name of the database you want to connect to
 * 		[tbl_prefix]		You can add an optional prefix, which will be added
 *							to the table name
 * 		[exit_on_error]     TRUE/FALSE - Wheather to exit if an sql error occures or not
 * 		[create_new]        TRUE/FALSE - Wheather to recycle the existing connection or always
 * 							start with a new one, by default the active connection is reused.
 * Depending on what database platform you are using (MySQL, Postgres, etc.) not all values will be needed.
 */
$database = $config['db_base'];

$database_config['mysql'] = array(
 	'hostname'   => $config['db_host'],
 	'username'   => $config['db_user'],
 	'password'   => $config['db_pass'],
 	'database'   => $database,
 	'tbl_prefix' => '',
);
$database_config['user_db'] = $config['db_users'];
$db_users = array(
 	'hostname'   => $config['db_host'],
 	'username'   => $config['db_user'],
 	'password'   => $config['db_pass'],
	'database'   => $database_config['user_db'],
	'tbl_prefix' => '',
);

if(isset($in['token'])){
	$db = new sqldb($db_users);
	$uinfo = $db->query("SELECT * FROM users INNER JOIN lang ON users.lang_id=lang.lang_id WHERE token ='".$in['token']."' ");
	$database = $uinfo->f('database_name');
	if($database){
		$database_config['mysql']['database'] = $database;
		require_once(APPSPATH.'auth/model/auth.php');
		$auth = new auth($in);
		$auth->set_session($db,$in);
	}else{
		http_response_code(401);
	    json_out();
		// $in['do'] = $default_app[ENVIRONMENT].'-'.$default_controller[ENVIRONMENT];
	}
}
define('DATABASE_NAME',$database);

$database_config['default'] = 'mysql';
$database_config['allow_multiple_drivers'] = false;
/* End of file database.php */