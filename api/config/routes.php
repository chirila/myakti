<?php   if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * Website Routes
 * --------------------------------------------------------------------------
  */
$routes = array(
    'auth' => array(
        'login' => array(
            'method' => "POST",
            'do' => 'auth-login-auth-login',
            'params' => ''
        ),
    ),
    'auth/login' => array(
        'method' => "POST",
        'do' => 'auth-login-auth-login',
        'params' => ''
    ),
    'crm/accountList' => array(
        'method' => "GET",
        // 'method' => array("POST","GET"),
        'do' => 'customers-customers',
        'params' => ''
    ),
    'crm/account' => array(
        'method' => "GET",
        'do' => 'customers-ViewCustomer',
        'params' => 'customer_id/xget/id'
        /*'params' => 'customer_id/xget/id'*/
    ),

    'crm/accountAdd' => array(
        'method' => "POST",
        'do' => 'customers-ViewCustomer-customers-addCustomer',
        'params' => ''
    ),
    'crm/accountUpdate' => array(
        'method' => "POST",
        'do' => 'customers-ViewCustomer-customers-updateCustomer',
        'params' => ''
    ),
    'crm/accountAddressAdd' => array(
        'method' => "POST",
        'do' => 'customers-ViewCustomer-customers-address_add',
        'params' => ''
    ),
    'crm/accountAddressUpdate' => array(
        'method' => "POST",
        'do' => 'customers-ViewCustomer-customers-address_update',
        'params' => ''
    ),
    'crm/accountContactAdd' => array(
        'method' => "POST",
        'do' => 'customers-ViewCustomer-customers-contact_add',
        'params' => ''
    ),
    'crm/accountContactUpdate' => array(
        'method' => "POST",
        'do' => 'customers-ViewCustomer-customers-contact_update',
        'params' => ''
    ),
    'crm/accountContacts' => array(
        'method' => "GET",
        // 'method' => array("POST","GET"),
        'do' => 'customers-customer_contacts',
        'params' => 'customer_id'
    ),
    'crm/nearBy' => array(
        'method' => "GET",
        // 'method' => array("POST","GET"),
        'do' => 'customers-map_location',
        'params' => 'latLng/range'
    ),
    
    'misc/miscellaneous' => array(
        'method' => "GET",
        // 'method' => array("POST","GET"),
        'do' => 'misc-miscellaneous',
        'params' => ''
    ),
    'misc/timesheet' => array(
        'method' => "GET",
        // 'method' => array("POST","GET"),
        'do' => 'misc-timesheet',
        'params' => 'start_date'
    ),

    'misc/get' => array(
        'method' => "GET",
        // 'method' => array("POST","GET"),
        'do' => 'misc-timesheet',
        'params' => 'xget/id'
    ),
        
    'atemplate' => array(
        'method' => "GET",
        'do' => 'article-template',
        'params' => 'filename/loadController/form_type'
    ),

    'invoice/updatePayment' => array(
        'method' => "POST",
        // 'method' => array("POST","GET"),
        'do' => 'invoice--invoice-pay',
        'params' => ''
    ),   

);

if(INSTALLPATH == '../'){
    $base = $config['install_path'].ENVIRONMENT.'/';
} else {
    $base = $config['install_path'];
}

$router = new arkRouter($routes,$base);

/*
$router->add('myaccount', 'GET', 'home-my_account', 'user_id');
$router->add('user', 'GET', 'home-user', '/user_id/test/');
*/

$router->run();