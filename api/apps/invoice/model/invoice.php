<?php

class invoice{

  var $dbu;
  var $dbu2;
  var $pag = 'invoice';
  var $pag2 = 'recurring_ninvoice';
  var $field_n = 'invoice_id';
  var $field_n2 = 'recurring_invoice_id';
  var $httpHeader = array();
  var $exact_setup = array();
  
  function __construct() {
    global $database_config;
    $this->db = new sqldb();
    $this->db2 = new sqldb();
    $this->dbu = new sqldb();
    $this->dbu2 = new sqldb();
    $this->db_users = new sqldb();
    $this->db_import = new sqldb;
    $database_2 = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );
    $this->dbu_users =  new sqldb($database_2); // recurring billing
  }

  /**
   * pay invoice
   *
   * @return bool
   * @author PM
   **/
  function pay(&$in){

    if(!$this->pay_validate($in))
    {
      http_response_code(400);
      json_out( array( 'error' => msg::get_errors() ) );
      return false;
    }
    if(!empty($in['payment_date'])){
      $in['payment_date'] =strtotime($in['payment_date']);
      $in['payment_date'] = mktime(23,59,59,date('n',$in['payment_date']),date('j',$in['payment_date']),date('y',$in['payment_date']));
    }

    $total = 0;
    $tblinvoice=$this->dbu->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE  id='".$in['invoice_id']."'");
    $tblinvoice->next();
    $currency = get_commission_type_list($this->dbu->f('currency_type'));
    $big_discount = $tblinvoice->f("discount");
    if($tblinvoice->f('apply_discount') < 2){
      $big_discount =0;
    }
    $lines = $this->dbu->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
    while ($lines->next()) {
      $line_disc = $lines->f('discount');
      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
        $line_disc = 0;
      }
      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
      $line = $line - $line * $big_discount / 100;

      $total += $line + ($line * ( $lines->f('vat') / 100));

    }

    //already payed
    if($tblinvoice->f('proforma_id')){
      $filter = " OR invoice_id='".$tblinvoice->f('proforma_id')."' ";
    }
    $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' {$filter}  AND credit_payment='0' ");
    $this->dbu->move_next();
    $total_payed = $this->dbu->f('total_payed');

    $negative = 1;
    if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
      $negative = -1;
    }
    $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' {$filter}  AND credit_payment='1' ");
    $this->dbu->move_next();
    $total_payed += ($negative*$this->dbu->f('total_payed'));

    $req_payment_value = $total;
    if($tblinvoice->f('type')==1){
      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
    }

    $amount_due = round($req_payment_value - $total_payed,2);
 //console::log($req_payment_value,$total_payed);
    $in['amount'] = return_value($in['amount']);

    if($in['cost']){
      $in['amount'] += return_value($in['cost']);
    }

    /*if(($in['amount'] > $amount_due) || $in['amount'] == 0){
      msg::$error =  gm('Invalid Amount');
      return false;
    }*/

    $payment_date=date("Y-m-d",$in['payment_date']);

    //Mark as payed
    if($in['amount'] == $amount_due || $in['amount']>$amount_due){
      $this->dbu->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$in['invoice_id']."'");
      $p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($p){
        $this->dbu->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
      }
    }
    else {
      $this->dbu->query("UPDATE tblinvoice SET paid='2' WHERE id='".$in['invoice_id']."'");
    }

    //$info='{l}Payment of{endl}'.'@/'.place_currency(display_number($in['amount']),$currency).'@/'.'{l}successfully recorded by{endl}'.'@/'.get_user_name($_SESSION['u_id']);
    if($in['notes']){
      $in['notes']='  -  '.$in['notes'];
    }

    $no_cost="";
    if($in['cost'] == '0,00'){
      $in['cost']=$no_cost;
    }else{
       $in['cost']='  -  '.$in['cost'];
    }

    $payment_view_date=date(ACCOUNT_DATE_FORMAT,$in['payment_date']);
    $info=$payment_view_date.'  -  '.place_currency(display_number($in['amount']),$currency).$in['notes'].$in['cost'];

    //INSERT PAYMENT
    $coda_pay=0;
    $new_pay_id=$this->dbu->insert("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$in['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$in['amount']."',
                          info        = '".$info."',
                          codapayment_id='".$coda_pay."' ");
    if($tblinvoice->f('bPaid_debt_id') != ''){
      $payment_row=$this->dbu->query("SELECT * FROM tblinvoice_payments WHERE payment_id='".$new_pay_id."' ")->getAll();
      foreach($payment_row as $key => $value){
        $this->bPaid_payment($value,$tblinvoice->f('bPaid_debt_id'),$tblinvoice->f('serial_number'),1);
      }  
    }

    insert_message_log($this->pag,'{l}Payment has been successfully recorded by {endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);

    msg::success(('Payment has been successfully recorded'),'success');
    json_out();

  }

  /**
   * pay invoice validate
   *
   * @return bool
   * @author PM
   **/
  function pay_validate(&$in)
  {
    $v = new validation($in);
    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
    $v->field('payment_date', 'Payment date', 'required:dateISO8601', gm('Invalid ISO8601 Date'));
    $v->field('amount','Amount','required');
    
    $is_ok = $v->run();
    if ($is_ok == true ){
      $this->dbu->query("SELECT paid,discount,sent,not_paid FROM tblinvoice WHERE id='".$in['invoice_id']."'");
      $this->dbu->move_next();
      if($this->dbu->f('sent') != '1'){
        http_response_code(400);
        json_out( array( 'error' => gm('Please mark as sent the invoice and then make a payment') ) );
        $is_ok = false;
      }

      if($this->dbu->f('paid') == 1){
        http_response_code(400);
        json_out( array( 'error' => gm('The invoice has already been paid') ) );
        $is_ok = false;
      }
      if($this->dbu->f('not_paid') == 1){
        http_response_code(400);
        json_out( array( 'error' => gm('The invoice will not be paid') ) );
        $is_ok= false;
      }
      $in['discount'] = $this->dbu->f('discount');
    }
    return $is_ok;
  }

  function bPaid_payment($row,$debt_id,$serial_number,$type){
      global $config;
      $ch = curl_init();

      $headers=array('Content-Type: application/json');
      $payment_data=array(
            'debt_id'                 => $debt_id,
            'debt_provider_id'        => $serial_number,
            'payment_amount'          => $row['amount'],
            'payment_date'            => $row['date']
      );

      $payment_data = json_encode($payment_data);

      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
      curl_setopt($ch, CURLOPT_USERPWD, $config['bpay_user'].":".$config['bpay_pass']);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);

      if($type==1){
        curl_setopt($ch, CURLOPT_POST, true);     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payment_data);
        curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/payments');
      }else if($type==2){
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/payments/'.$row['bPaid_payment_id']);
      }

      $put = curl_exec($ch);
      $info = curl_getinfo($ch);

      if($info['http_code']>=200 && $info['http_code']<300 && $type==1){
        $new_data=json_decode($put,true);
        if(array_key_exists('id',$new_data)){
          $this->dbu->query("UPDATE tblinvoice_payments SET bPaid_payment_id='".$new_data['id']."' WHERE payment_id='".$row['payment_id']."' ");       
        }
      }
      return true;
  }

}