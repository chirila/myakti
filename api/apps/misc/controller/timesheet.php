<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class Timesheet extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Week(){
		$in = $this->in;

		global $p_access;
		global $config;

		$result=array();

		//$tips = $page_tip->getTip('misc-timesheet');
		$filter = '1=1';
		$filter2 = ' AND 1=1 ';

		if(!$in['user_id'] )
		{
			$in['user_id'] = $_SESSION['u_id'];
		}

		$isUserActive = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='active' ");

		/*if(!is_numeric($in['start'])){
			$now 				= time();
			$today_of_week 		= date("N", $now);
			$month        		= date('n');
			$year         		= date('Y');
			$week_start 		= mktime(0,0,0,$month,(date('j')-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;

		}else if($in['start_date']){
			$now 				= pick_date_timestamp($in['start_date']);
			$today_of_week 		= date("N", $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;

		} else {
			if(date('H',$in['start']) != '00'){
				if(date('H',$in['start']) == '23' ){
					$week_start 		= $in['start'] + 3600;
					$week_end 			= $in['start'] + 604799;
					if(date('H',$week_end) == '22' ){
						$week_end= $week_end+ 3600;
					}
				}else {
					$week_start 		= $in['start'] - 3600;
					$week_end 			= $in['start'] + 604799;
					if(date('H',$week_end) == '00' ){
						$week_end= $week_end - 3600;
					}
				}
			}else{
				$week_start 		= $in['start'];
				$week_end 			= $in['start'] + 604799;
				if(date('H',$week_end) =='00'){
					$week_end 			= $in['start'] + 604799 -3600;
				}
			}
		}*/

		if($in['start_date']){
			$result['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}
		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		if(date('I',$now)=='1'){
			$week_start+=3600;
		}
		$week_end 			= $week_start + 604799;

		$is_pdf = true;
		$week_nr = date('W',$week_start);
		$year_nr = date("Y",$week_start);

		if($_SESSION['group'] != 'admin' && $_SESSION['u_id'] != $in['user_id'] ){
			$is_manager = $this->db->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");
			if(!empty($is_manager)){
				$filter2 =" AND task_time.project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
			}
		}

		$result['week_txt'] = date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end-3600);
		$result['PREV_WEEK_LINK']='index.php?do=misc-timesheet&user_id='.$in['user_id'].'&start='.($week_start-604800);
		$result['NEXT_WEEK_LINK']='index.php?do=misc-timesheet&user_id='.$in['user_id'].'&start='.($week_end+1);
		$result['ACTIVE_WEEK']='activated';

		$e = $this->db->query("SELECT task_time.contract_id,task_time.user_id,task_time.date as start_date,task_time.project_id,task_time.task_id,task_time.billed,task_time.submited AS submited,
		            tasks.task_name,projects.name as project_name,projects.active, projects.company_name AS c_name, projects.status_rate, task_time.customer_id AS c_id
		            FROM task_time
		            INNER JOIN tasks ON tasks.task_id=task_time.task_id
		            INNER JOIN projects ON projects.project_id=task_time.project_id
		            WHERE task_time.user_id='".$in['user_id']."' AND date BETWEEN '".$week_start."' AND '".$week_end."' ".$filter2."
		            GROUP BY task_time.task_id
		            ORDER BY c_name, project_name, task_name
				   ")->getAll();
		$showed_projects=array();
		$showed_projects2=array();
		$rows = count($e);
		$r=1;
		$submited = 1;
		$submited_total = 1;
		$result['task_row']=array();
		foreach ($e as $key => $value) {
			//select rows
			$t_d_row=$this->db->query("SELECT task_time.*, projects.stage
		                  FROM task_time
		                  INNER JOIN projects ON projects.project_id=task_time.project_id
		                  WHERE user_id='".$in['user_id']."'
		                  AND date BETWEEN '".$week_start."' AND '".$week_end."'
		                  AND task_time.project_id='".$value['project_id']."'
		                  AND task_time.task_id='".$value['task_id']."' ORDER BY date ASC ");
			$d=1;
			$task_day_row=array();
			while($t_d_row->move_next()){
				if($t_d_row->f('submited')==0 || $t_d_row->f('submited')==2){
					$submited_total = 0;
				}
				if($d > 7){
					break;
				}
				if( $t_d_row->f('hours') && ( $_SESSION['access_level'] == 1 || ( !empty($is_manager) && $is_manager == $t_d_row->f('project_id') ) ) )
				{
					/*$is_pdf= true;
					if($_SESSION['access_level'] != 1){
						$timesheet_only = 1;
					}*/
				}
				$day_id = 'tmp_day'.$d;
				$note = trim($t_d_row->f('notes'));		
				$temp_task_day_r=array(
					'TD_ID_TASK_DAY'=> $day_id,
				  	'TASK_DAY_ID' 	=> $t_d_row->f('task_time_id'),
					'HOURSB'      	=> $t_d_row->f('hours') ? ($t_d_row->f('project_status_rate') == 0 ? number_as_hour($t_d_row->f('hours')) : $t_d_row->f('hours')) : '',
					'HOURS'       	=> $t_d_row->f('hours'),
					'IS_BILLED'   	=> ($t_d_row->f('billed')==1 || $value['active']==0) ? true:false,
					'IS_SUBMITED' 	=> $t_d_row->f('submited') == 1 || $t_d_row->f('service') == 1 ? true:false,
					'SUBMITED'	  	=> $t_d_row->f('submited') == 1 ? 'submited':'',
					'BG_COLOR'		=> !empty($note) ? 'bg-warning' : ( date('D') == date('D',$t_d_row->f('date')) && (date('W') == date('W',$t_d_row->f('date'))) && (date('Y')==date('Y',$t_d_row->f('date'))) ? 'grayish2' : ''),
					'APP'			=> $t_d_row->f('approved') == 1 ? 'approvedish': ($t_d_row->f('submited') == 1 ? 'approvedish':''),
					'IS_APPROVED' 	=> $t_d_row->f('approved') == 1 ? true:false,
					'NO_PLUS'		=> !empty($note) ? 'no-plus' : '',
					'hourly_rate'	=> $t_d_row->f('project_status_rate') == 0 ? true : false,
					'change_hours'	=> $t_d_row->f('project_status_rate') == 0 ? 'hours' : 'days',
					'hide_spinner'	=> ($t_d_row->f('billed')==1 || $value['active']=='0' || $t_d_row->f('submited') == 1 || $t_d_row->f('service') == 1 || $t_d_row->f('approved') == 1 || $isUserActive == '1' || $t_d_row->f('stage')==2 || $t_d_row->f('stage')==0) ? false : true, 
					'POSITION_DAY'	=> ($t_d_row->f('billed')==1 || $value['active']=='0' || $t_d_row->f('submited') == 1 || $t_d_row->f('service') == 1 || $t_d_row->f('approved') == 1 || $isUserActive == '1') ? '' : 'margin-left:11px;float:left',
					'IS_CLOSED'		=> ($t_d_row->f('stage')=='2' || $t_d_row->f('stage')=='0') ? true:false,
					'BILLED_IMG'	=> $t_d_row->f('billed')==1 ? true : false,
					'UNAPPROVED'	=> $t_d_row->f('unapproved')==1 ? true : false,
			  	);
			  	array_push($task_day_row, $temp_task_day_r);
				$d++;
			}
			$in['hide_s'] = $value['submited'];
			$row_id = 'tmp'.$r;

			$t_row=array(
				'TR_ID_TASK'           		=> $row_id,
				'classN'				=> $value['contract_id'] ? 'contractId' : 'cId',
				'cId'					=> $value['c_id'],
				'project_id'			=> $value['project_id'],
			 	'PROJECT_LIST_NAME' 		=> $value['project_name'],
			 	'project_type'			=> $value['status_rate'],
			 	'HIDE_PROJECT_NAME'		=> in_array($value['c_id'], $showed_projects) ? false : true,
			 	'HIDE_PROJECT_NAME2'		=> in_array($value['project_id'], $showed_projects2) ? false : true,
			 	'PROJECT_CUSTOMER_NAME' 	=> $value['c_name'],
				'TASK_LIST_NAME'    		=> $value['contract_id'] ? ''  :$value['task_name'],
				'TOTAL'             		=> '&nbsp;',
				'HIDE_BILLED'			=> ($value['billed'] || $value['active']==0)? false : true,
				'HIDE_UNBILLED'			=> ($value['billed'] || $value['active']==0)? true : false,
				'HIDE_SUBMITED'			=> $value['submited'] ==1 ? true : false,
				'HIDE_SUBMITED_L'			=> $value['submited'] ==1 ? false : true,
				'items_border'			=> $r == $rows ? 'show_border' : ($value['c_id']!= $e[$key+1]['c_id'] ? 'show_border' : ''),
				'maintenance_icon_show'		=> $value['service'] == 1 ? 'maintenance_icon_show' : "",
				'hide_maintenance_delete' 	=> $value['service'] == 1 ? false : true,
				'task_day_row'			=> $task_day_row
			);
			array_push($result['task_row'], $t_row);
			array_push($showed_projects,$value['c_id']);
			array_push($showed_projects2,$value['project_id']);
			$r++;
			if($value['submited'] == 1 && $submited_total == 1 ){
				$submited++;
			}
		}

		$result['HIDE_TASKS']=true;
		if($r == 1){
			$result['HIDE_TASKS']=false;
			$in['info'] .= gm('There are no hours added.');
			if(date('W',$week_start) == date('W')){
				$prev_week_end = $week_start - 1;
				$prev_week_start = $prev_week_end - 604799;

		//select all tasks
				$prev_query = $this->db->query("SELECT task_time.user_id,task_time.project_id,task_time.task_id,task_time.billable,
				            task_time.customer_id AS c_id, task_time.project_status_rate, projects.stage
				            FROM task_time
				            INNER JOIN projects ON task_time.project_id = projects.project_id
				            WHERE task_time.user_id='".$in['user_id']."'
				            AND date BETWEEN '".$prev_week_start."' AND '".$prev_week_end."' AND projects.closed='0'
				            GROUP BY task_time.task_id
				            ORDER BY task_time.task_time_id
						   ");

				$i=0;
				while ($prev_query->next()) {
					$check_h = $this->db->field("SELECT SUM(hours) FROM task_time
											WHERE user_id='".$prev_query->f('user_id')."' AND
												  project_id='".$prev_query->f('project_id')."' AND
												  task_id='".$prev_query->f('task_id')."'
												  AND date BETWEEN '".$prev_week_start."' AND '".$prev_week_end."' ");
					if($check_h > 0 ){
						$days = $week_start;
						 $billable=$this->db->field("SELECT billable FROM tasks WHERE task_id='".$prev_query->f('task_id')."'");
						while ($days < $week_end ){
							if($prev_query->f('stage')==1){
								$query = $this->db->query("INSERT INTO task_time
													SET user_id='".$prev_query->f('user_id')."',
														project_id='".$prev_query->f('project_id')."',
														task_id='".$prev_query->f('task_id')."',
														customer_id='".$prev_query->f('c_id')."',
														date='".$days."',
														hours='0',
														billable='".$billable."',
														project_status_rate='".$prev_query->f('project_status_rate')."' ");
							}
							$days += 86400;

						}
						$i++;
					}
				}
				$prev_query = null;
			}
		}

		//now walk the days and display the info
		$j =1;
		//echo $week_start.' -- '.$week_end;
		$result['day_row']=array();
		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			$day_id = 'tmp_day'.$j;
			$d_row=array(
				'day_name'    	=> gm(date('D',$i)),
				'date_month'  	=> date('d',$i).' '.gm(date('M',$i)),
				'TIMESTAMP'   	=> $i,
				'TD_ID'       	=> $day_id,
				'NO_MARGIN'		=> $j == 7 ? 'no-margin' : '',
				'TODAY'		=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? 'today' : '',
				'HIDE_ARROW'	=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? '' : 'hide',
				'end_day'		=> date("N",$i)>5 ? true : false
			);
			array_push($result['day_row'], $d_row);
		    $j++;
		}

		//if we have a dummy task and an expense, we hide the buttons
		// because is already submited
		$count_dummy_task = $this->db->field("SELECT COUNT(task_time_id) FROM task_time
							WHERE task_time.user_id='".$in['user_id']."'
							AND date BETWEEN '".$week_start."' AND '".$week_end."'
							AND notes = 'no_task_just_expense' ");
		// echo $count_dummy_task;

		$i=0;
		$result['history_row']=array();
		$activity = $this->db->query("SELECT * FROM timesheet_log WHERE user_id='".$in['user_id']."' AND start_date='".$week_start."' limit 5 ");
		while($activity->next()){
			$h_row=array(
				'HISTORY_DATE'		=>$activity->f('date'),
				'HISTORY_DESCRIPTION'	=>$activity->f('message'),
			);
			array_push($result['history_row'], $h_row);
			$i++;
		}
		if($i==0){
			$result['HIDE_LOG']	= false;
		}else {
			$result['HIDE_LOG']=$_SESSION['access_level'] == 1 ? true : false;
			if($count_dummy_task>0){
				$result['HIDE_LOG']=$_SESSION['access_level'] == 1 ? true : false;
			}
		}

		if(!$in['tab']){
			$in['tab'] = 1;
		}
		/*if($in['success']){
			$view->assign('MSG_SUCCESS','<div class="success">'.$in['success'].'</div>');
		}*/
		$add_row_box = false;

		if(date('W') == date("W",$week_start)){
			$day = '';
		}else{
			$day = '&start='.$week_start;
		}

		$user_dd = build_user_dd_timesheet($in['user_id'], $_SESSION['access_level']);
		//$user_name = get_user_name($in['user_id']);

		$l=0;
		//print languages
		$result['languages_pdf']=array();
		$pim_lang = $this->db->query("SELECT * FROM pim_lang WHERE active=1 ORDER BY sort_order ");
		while($pim_lang->move_next()){
			// if($pim_lang->f('default_lang')){
		 		$default_lang = $pim_lang->f('lang_id');
		 	// }
		  	$temp_lang=array(
				'language'	        	=> gm($pim_lang->f('language')),
				'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$pim_lang->f('lang_id').'&timesheet_only='.$timesheet_only,
			);
			$l++;
			array_push($result['languages_pdf'], $temp_lang);
		}
		$pim_cust=$this->db->query("SELECT * from pim_custom_lang where active=1 ORDER BY sort_order ");
		 while($pim_cust->move_next()){
		 	$lbl_lang=$this->db->query("SELECT * FROM label_language_time WHERE lang_code='".$pim_cust->f('lang_id')."'");
		 	if($lbl_lang->move_next())
		 	{
			 	$temp_lg=array(
					'language'	        	=> $lbl_lang->f('name'),
					'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$lbl_lang->f('label_language_id').'&timesheet_only='.$timesheet_only,
				);
				$l++;
			array_push($result['languages_pdf'], $temp_lg);
			}
		}
		if($l==1){
			
			  $result['generate_pdf_id']  	= '';
			  $result['web_invoice_id']   	= '';
			  $result['hide_language']	  	= false;
			  $result['pdf_class']			= 'generate_pdf';
			  $result['pdf_link']	         	= "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$default_lang.'&timesheet_only='.$timesheet_only;
			
		}else{

			  $result['generate_pdf_id']  		= 'generate_pdf';
			  $result['web_invoice_id']   		= 'web_invoice';
			  $result['hide_language']	  		= '';
			  $result['pdf_link']	      		= '#';

		}
		$result['pdf_link']	         	= "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$default_lang.'&timesheet_only='.$timesheet_only;
		$t = time();
		$day_nr = 0;
		if($week_start < $t && $t < $week_end){
			$day_nr = date('N');
		}

		$in['close_srv'] = $submited == $r && $submited != 1 ? false : true;
		$i=13;
		$val =$this->db_users->field("SELECT value FROM user_meta WHERE name='MODULE_".$i."' AND user_id='".$_SESSION['u_id']."' ");
		if($val===NULL){
			$val = constant('MODULE_'.$i);
		}

		if(!in_array('13',$p_access)){ $val = 0; }
		$result['hide_'.$i]= $val == 0 ? false : true;

		unset($_SESSION['articles_id']);

		$partial_time=array();
		for($o=0;$o<=6;$o++){
			if(count($partial_time)==0){
				$partial_time[]=$week_start;
			}else if($o<6){
				$partial_time[$o]=strtotime("+1day",$partial_time[$o-1]);
			}else{
				$partial_time[]=$week_end;
			}
		}

		$partial_time_full=array();
		foreach($partial_time as $key => $val){
			$partial_time_full[$val]=date(ACCOUNT_DATE_FORMAT,$val);
		}


			$result['PAGE_TITLE']	    	= gm('Timesheet for').' <span id="username">'.$user_dd[0].$user_dd[1][0].($user_dd[1][1] > 1 ? '<b id="change_user" class="'.($in['is_ajax'] == 1 ? 'hide': '').'" >&nbsp;</b>' : '').'</span>';
			$result['user_dd']		= $user_dd;
			$result['user_id']           	= $in['user_id'];
			$result['is_pdf']			= $is_pdf;
			$result['PAG']              	= $in['is_ajax'] == 1 ? 'timesheet' : $in['pag'];
			$result['ADD_ROW_BOX']       	= $add_row_box;
			$result['NEXT_FUNCTION']     	= 'misc-timesheet-time-add_hour';
			$result['START']            	= $week_start;
			$result['START_DAY']		= $day;
			$result['week_s']        	= $week_start;
			$result['week_e']          	= $week_end;
			$result['HIDE_AJAX']         	= $in['is_ajax'] == 1 ? false: true;
			$result['HIDE_L']	        	= $_SESSION['access_level'] == 1 ? true : false;
			$result['DISPLAY_SAVE']      	= $submited == $r && $submited != 1 ? false : true;
			$result['IS_AJAX']           	= $in['is_ajax'] == 1 ? '1': '2';
		  	$result['STYLE']             	= ACCOUNT_NUMBER_FORMAT;
		  	$result['tab']			= $in['tab'];
		  	$result['page_tip']		= $tips;
		  	$result['pick_date_format']	= pick_date_format();
		  	$result['TODAY_nr']		= $today_of_week;
		  	$result['allowadhoc']		= USE_ADHOC_TMS == '1'? true:false;
		  	$result['partial_submit']	= build_simple_dropdown($partial_time_full);
		  	$result['projects_list']	= $this->get_projects_list($in);
		  	$result['tasks_list']		= $this->get_tasks_list($in);

		if($submited == $r && $submited != 1 ){
			$in['hide_s'] = 1;
		}else{
			$in['hide_s'] = 0;
		}

		if($count_dummy_task > 0){
			$in['hide_s'] = 1 ;//hide shit from timesheet_expense
			$in['close_srv'] = false; //hide shit from Maintenance
		}

			$result['hide_if_task_no_exp_submited']	= $count_dummy_task > 0 ? false : true;

		if(in_array('11',$p_access)){
			$result['is_contract']	= true;
		}else{
			$result['is_contract'] = false;
		}

		$this->out = $result;
	}

	public function get_Month(&$in){
		$in = $this->in;

		$result=array();

		if($in['start_date']){
			$result['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$month = date('n',$now);
		$year = date('Y',$now);
		$month_t = mktime(0,0,0,$month,1,$year);
		if(date('I',$now)=='1'){
			$month_t+=3600;
		}

		$time_start = mktime(0,0,0,$month,1,$year);
		if(date('I',$now)=='1'){
			$time_start+=3600;
		}
		$time_end = mktime(0,0,0,$month+1,1,$year);
		if(date('I',$now)=='1'){
			$time_end+=3600;
		}

		$user_dd = build_user_dd_timesheet($in['user_id'], $_SESSION['access_level']);

		$result['month_txt']		= gm(date('F',$month_t));
		$result['user_dd']		= $user_dd;
		$result['user_id']		= $in['user_id'];
		$result['year']			= $year;
		$result['next']			= mktime(0,0,0,$month+1,1,$year);
		$result['prev']			= mktime(0,0,0,$month-1,1,$year);
		$result['PAGE_TITLE']		= '<span id="username">'.$user_dd[0].$user_dd[1][0].($user_dd[1][1] > 1 ? '<b id="change_user" class="'.($in['is_ajax'] == 1 ? 'hide': '').'" >&nbsp;</b>' : '').'</span>';
		$result['expense_pdf_link']	= 'index.php?do=project-expenses_report_print&time_start='.$time_start.'&time_end='.$time_end.'&u_id='.$in['user_id'];
		$result['month_start']		= $time_start;
		$result['month_end']		= $time_end;

		$month_days = date('t',$month_t);
		$month_first_day = date('N',$month_t);

		$result['month']=array();
		$i = 1;
		$day = 1;
		$loopweek = true;
		$q=0;
		$w=0;
		while ($loopweek) {
			$k = 1;
			$day_final=array('week'=>array());
			while ($k <= 7) {
				$expenses = 0;
				$text = $day;
				$hours = '';
				$days = ';';
				$bg = 'planning_empty';
				if($i == 1 && $k < $month_first_day){

					$date1=mktime(0,0,0,$month,$day-($month_first_day-$k),$year);
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$text = date('j',$date1);
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours).' <span>'.gm('hours').'</span>';
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days.' <span>'.gm('days').'</span>';
					}
				}
				if($day > $month_days){
					$date1=mktime(0,0,0,$month,$day,date('y'));
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$text = date('j',$date1);
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours). ' <span>'.gm('hours').'</span>';
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days. ' <span>'.gm('days').'</span>';
					}
					$day++;
				}
				if($text == $day){
					$date1=mktime(0,0,0,$month,$day,$year);
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours).' <span>'.gm('hours').'</span>';
						$w++;
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days.' <span>'.gm('days').'</span>';
						$w++;
					}
					$day++;
					$bg = '';
				}
				$temp_data=array(
					'text'		=> $text,
					'hours'		=> $hours,
					'days'		=> $days,
					'expens'		=> $expenses ? place_currency(display_number($expenses)) : '',
					'bg'			=> $bg,
					'last'		=> $k == 7 ? 'last' : '',
				);
				array_push($day_final['week'], $temp_data);
				$k++;
				if($day > $month_days){
					$loopweek = false;
				}
			}
			array_push($result['month'], $day_final);
			$i++;
		}

		if(!$in['tab']){
			$in['tab'] = 1;
		}

		$project_admin = $this->db_users->field("SELECT value FROM user_meta WHERE name='project_admin' AND user_id='".$_SESSION['u_id']."' ");
		$group = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."'");

		if($group == 1 || $project_admin == 1){
			$is_project_admin = 1;
		}
		$is_manager = false;
		if($_SESSION['access_level']==1 || $is_project_admin == 1){
			$is_manager = true;
		}

		$result['is_manager']		= $is_manager;
		$result['are_expenses']		= $q>0? true : false;
		$result['is_time_data']		= $w>0? true : false;
		$result['tab']		= $in['tab'];

		$result['languages_pdf']=array();
		//print languages
		$l = 0;
		$pim_lang = $this->db->query("SELECT * FROM pim_lang WHERE active=1 ORDER BY sort_order ");
		while($pim_lang->move_next()){
			// if($pim_lang->f('default_lang')){
		 		$default_lang = $pim_lang->f('lang_id');
		 	// }
		  	$temp_lang=array(
				'language'	        	=> gm($pim_lang->f('language')),
				'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$time_start."&time_end=".$time_end."&user_id=".$in['user_id'].'&lid='.$pim_lang->f('lang_id').'&timesheet_only='.$timesheet_only,
			);
			$l++;
			array_push($result['languages_pdf'], $temp_lang);
		}
		$custom_lang=$this->db->query("SELECT * from pim_custom_lang where active=1 ORDER BY sort_order ");

		while($custom_lang->move_next()){
		 	$this->db->query("SELECT * FROM label_language_time WHERE lang_code='".$custom_lang->f('lang_id')."'");
		 	if($this->db->move_next())
		 	{
			 	$temp_c_lang=array(
					'language'	        	=> $this->db->f('name'),
					'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$time_start."&time_end=".$time_end."&user_id=".$in['user_id'].'&lid='.$this->db->f('label_language_id').'&timesheet_only='.$timesheet_only,
				);
				$l++;
				array_push($result['languages_pdf'], $temp_c_lang);
			}
		}

		$this->out = $result;
	}

	public function get_Day(){
		$in=$this->in;

		if(!$in['user_id'] )
		{
			$in['user_id'] = $_SESSION['u_id'];
		}
		
		$result=array();

		if($in['start_date']){
			$result['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$today_of_week= date("N", $now);
		$day=date('j',$now);
		$month= date('n', $now);
		$year= date('Y', $now);
		$start_day=mktime(0,0,0,$month,$day,$year);
		if(date('I',$now)=='1'){
			$start_day+=3600;
		}
		$end_day=$start_day+86399;
		$week_start= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		if(date('I',$now)=='1'){
			$week_start+=3600;
		}
		$week_end= $week_start + 604799;

		$result['day_txt']=date(ACCOUNT_DATE_FORMAT,$end_day-7200);

		// $user_dd = build_user_dd_timesheet($in['user_id'], $_SESSION['access_level']);

		// $result['user_dd']		= $user_dd;
		$result['user_id']		= $in['user_id'];
		$result['date']			= $start_day;

		// $result['day_row']=array();
		// for($i = $week_start; $i <= $week_end; $i += 86400)
		// {
		// 	$day_id = 'tmp_day'.$j;
		// 	$d_row=array(
		// 		'day_name'    	=> gm(date('D',$i)),
		// 		'date_month'  	=> date('d',$i).' '.gm(date('M',$i)),
		// 		'TIMESTAMP'   	=> $i,
		// 		'TD_ID'       	=> $day_id,
		// 		'NO_MARGIN'		=> $j == 7 ? 'no-margin' : '',
		// 		'TODAY'		=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? 'today' : '',
		// 		'HIDE_ARROW'	=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? '' : 'hide',
		// 		'end_day'		=> date("N",$i)>5 ? true : false
		// 	);
		// 	array_push($result['day_row'], $d_row);
		//     $j++;
		// }

		$order_by = " ORDER BY id ASC  ";
		$order_by_array = array('start_time');
		if($in['order_by']){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == true){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}
		$in['only_h']=true;
		$in['tasks_ids']='';
		//$in['projects_ids']='';
		$task_time_data=$this->db->query("SELECT task_id,project_id FROM task_time WHERE date BETWEEN '".$start_day."' AND '".$end_day."' AND project_status_rate='0' AND user_id='".$in['user_id']."' GROUP BY task_id");
		while($task_time_data->next()){
			$in['tasks_ids'].=$task_time_data->f('task_id').',';
			/*if(strpos($in['projects_ids'],$task_time_data->f('project_id')) === false){
				$in['projects_ids'].=$task_time_data->f('project_id').',';
			}*/	
		}
		$in['tasks_ids']=rtrim($in['tasks_ids'],",");
		//$in['projects_ids']=rtrim($in['projects_ids'],",");
		$result['DISPLAY_SAVE']=true;
		$result['rows']=array();
		if(!empty($in['tasks_ids'])){
			$entries=array();
			$entries=explode(",",$in['tasks_ids']);
			foreach($entries as $key=>$value){
				$entries_sheet=$this->db->field("SELECT COUNT(id) FROM servicing_support_sheet WHERE task_id='".$value."' AND date BETWEEN '".$start_day."' AND '".$end_day."' AND user_id='".$in['user_id']."'");
				$task_time_temp=$this->db->query("SELECT * FROM task_time WHERE task_id='".$value."' AND date BETWEEN '".$start_day."' AND '".$end_day."' AND user_id='".$in['user_id']."' ");	
				if($task_time_temp->f('hours')>0 && !$entries_sheet){
					$this->db->query("INSERT INTO servicing_support_sheet SET 
									task_id='".$value."',
									date='".$task_time_temp->f('date')."',
									user_id='".$in['user_id']."',
									project_id='".$task_time_temp->f('project_id')."',
									task_time_id='".$task_time_temp->f('task_time_id')."',
									total_hours='".$task_time_temp->f('hours')."' ");			
				}
			}
			$task_time_subdata=$this->db->query("SELECT servicing_support_sheet.*,task_time.billed,task_time.submited,task_time.approved,task_time.unapproved,projects.active,projects.company_name AS c_name,projects.name as project_name,projects.stage FROM servicing_support_sheet 
												INNER JOIN task_time ON servicing_support_sheet.task_time_id=task_time.task_time_id
												INNER JOIN projects ON task_time.project_id=projects.project_id
												WHERE servicing_support_sheet.project_id!='0' AND servicing_support_sheet.task_id IN (".$in['tasks_ids'].") AND servicing_support_sheet.date BETWEEN '".$start_day."' AND '".$end_day."' AND servicing_support_sheet.user_id='".$in['user_id']."' ".$order_by." ");	
			while($task_time_subdata->next()){
				if($task_time_subdata->f('submited')==1){
					$result['DISPLAY_SAVE']=false;
				}
				$temp_data=array(
					'id'			=> $task_time_subdata->f('id'),
					'start_time'	=> ($task_time_subdata->f('start_time')),
					'end_time'		=> ($task_time_subdata->f('end_time')),
					'break'			=> ($task_time_subdata->f('break')),
					'project_id'	=> $task_time_subdata->f('project_id'),
					'task_id'		=> $task_time_subdata->f('task_id'),
					'task_time_id'	=> $task_time_subdata->f('task_time_id'),
					// 'HIDE_UNBILLED'	=> ($task_time_subdata->f('billed') || $task_time_subdata->f('active')==0)? true : false,
					// 'HIDE_SUBMITED'	=> $task_time_subdata->f('submited')==1 ? true : false,
					'PROJECT_CUSTOMER_NAME' 	=> $task_time_subdata->f('c_name'),
					'PROJECT_LIST_NAME' 		=> $task_time_subdata->f('project_name'),
					'TASK_LIST_NAME'    		=> $this->db->field("SELECT task_name FROM tasks WHERE task_id='".$task_time_subdata->f('task_id')."' "),
					// 'IS_BILLED'   	=> ($task_time_subdata->f('billed')==1 || $task_time_subdata->f('active')==0) ? true:false,
					'IS_APPROVED' 	=> $task_time_subdata->f('approved') == 1 ? true:false,
					'IS_SUBMITED' 	=> $task_time_subdata->f('submited') == 1 ? true:false,
					'IS_CLOSED'		=> ($task_time_subdata->f('stage')=='2' || $task_time_subdata->f('stage')=='0') ? true:false,
					'IS_APPROVED' 	=> $task_time_subdata->f('approved') == 1 ? true:false,
					// 'UNAPPROVED'	=> $task_time_subdata->f('unapproved')==1 ? true : false,
					// 'disabled'		=> $task_time_subdata->f('submited') ? true : false,
					// 'BILLED_IMG'	=> $task_time_subdata->f('billed')==1 ? true : false,
					// 'HIDE_BILLED'			=> ($task_time_subdata->f('billed') || $task_time_subdata->f('active')==0)? false : true,
					// 'HIDE_SUBMITED_L'		=> $task_time_subdata->f('submited') ==1 ? false : true,
					'line_h'				=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? $task_time_subdata->f('total_hours') : $task_time_subdata->f('end_time')-$task_time_subdata->f('start_time')-$task_time_subdata->f('break'),
					// 'line_h'				=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? number_as_hour($task_time_subdata->f('total_hours')) : number_as_hour($task_time_subdata->f('end_time')-$task_time_subdata->f('start_time')-$task_time_subdata->f('break')),
					'is_total_h'			=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? true : false,
					'comment'			=> $task_time_subdata->f('notes'),
				);
				array_push($result['rows'], $temp_data);
			}
			
		}
		// if(!$in['tab']){
		// 	$in['tab'] = 1;
		// }
		// $result['tab']		= $in['tab'];
		// $result['projects_list']=$this->get_projects_list($in);
		// $result['tasks_list']=$this->get_tasks_list($in);
		//$result['tasks_ids']=$in['tasks_ids'];
		$this->out = $result;
	}

	public function get_projects_list(&$in){

		$q = strtolower($in["term"]);

		$filter ='';
		if($in['only_h']){
			$filter .=" AND projects.status_rate='0' ";
		}
		/*if($in['projects_ids'] && !empty($in['projects_ids'])){
			$filter .=" AND projects.project_id IN (".$in['projects_ids'].") ";
    	}else */if($in['user_id']){
			$filter .=" AND project_user.user_id='".$in['user_id']."'";
			if($_SESSION['group'] != 'admin' && $in['user_id'] != $_SESSION['u_id']){
				$is_manager = $this->db->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");
				if(!empty($is_manager)){
					$filter .=" AND projects.project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
				}
			}
		}
		if($q){
			$filter .=" AND (projects.name LIKE '%".$q."%' OR projects.company_name LIKE '%".$q."%') ";
		}

		if($in['exp'] == 1){
			$filter .= " AND add_expense='1' ";
		}

		$this->db->query("SELECT projects.project_id,projects.name AS p_name,projects.active, projects.customer_id, projects.company_name AS c_name
		            FROM projects
		            INNER JOIN project_user ON project_user.project_id=projects.project_id
		            WHERE projects.active=1 AND projects.closed='0' AND project_user.active='1' AND projects.stage='1' $filter
		          ");
		while($this->db->move_next()){
		  $projects[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('project_id');
		  $customer[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('customer_id');
		}

		$result = array();
		if($projects){
			foreach ($projects as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "name" => strip_tags($key), "c_id" => $customer[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "name" => strip_tags($key), "c_id" => $customer[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "name" => gm('No matches')));
		}
		return array('list' =>$result);
	}

	public function get_tasks_list(&$in){
		$in['project_id'] = $in['id'];
		$q = strtolower($in["term"]);

		$filter ='';

		if($q){
			$filter .=" AND tasks.task_name LIKE '%".$q."%'";
		}
		if($in['tasks_ids'] && !empty($in['tasks_ids'])){
			$filter.=" AND task_id IN (".$in['tasks_ids'].") ";
		}

		if($in['project_id']){
			$bil_type = $this->db->field("SELECT billable_type FROM projects WHERE project_id='".$in['project_id']."' ");
			if($bil_type == 7){
				$filter .= " AND default_task_id IN (SELECT func_id FROM user_function WHERE user_id='".$in['user_id']."' ) ";
			}

			$this->db->query("SELECT tasks.task_id,tasks.project_id,tasks.task_name,tasks.billable FROM tasks WHERE tasks.project_id='".$in["project_id"]."' AND closed='0' $filter");
			while($this->db->move_next()){
				$t_name = str_replace(array("\r\n", "\r"), " ", $this->db->f('task_name'));
				$tasks[$t_name]= $this->db->f('task_id');
				$billable[$t_name] = $this->db->f('billable');
			}
		}

		$result = array();
		if($tasks){
			foreach ($tasks as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "billable" => $billable[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "billable" => $billable[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "name" => gm('No matches')));
		}
		// return $result;
		return array('list' =>$result);
	} 

}

	$timesheet = new Timesheet($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $timesheet->output($timesheet->$fname($in));
	}

	$timesheet->get_Day();
	$timesheet->output();

?>