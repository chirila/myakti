<?php

class customers {

	function __construct() {
		$this->db = new sqldb();
		$this->db2 = new sqldb();
		$this->db_import = new sqldb;
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($database_users);
		$this->field_n='customer_id';
		$this->pag = 'customer';
		$this->pagc = 'xcustomer_contact';
		$this->field_nc = 'contact_id';
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addCustomer(&$in)
	{
		if(!$this->addCustomer_validate($in)){
			http_response_code(400);
			// $errors = array_values(msg::get_errors());
			// $errors = implode("\n", $errors);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}

		if(!$in['active']){
			$in['active'] = 1;
		}
		
		$c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			/*foreach ($in['c_type'] as $key) {
				if($key){
					$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
					$c_type_name .= $type.',';
				}
			}
			$c_types = implode(',', $in['c_type']);
			$c_type_name = rtrim($c_type_name,',');*/
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']){
			/*foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);*/
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		if(SET_DEF_PRICE_CAT == 1){
			$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
		}
		if($in['contact_id']){
			$indiv_created = 1;
			$this->db->query("UPDATE customer_contacts SET indiv_created = '".$indiv_created."' WHERE contact_id = '".$in['contact_id']."' ");
		}


		$country_n = get_country_name($in['country_id']);
        // $query_sync=array();
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       	$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $in['customer_id'] = $this->db->insert("INSERT INTO customers SET
                                                type                    = '".$in['type']."',
                                                firstname 				= '".$in['firstname']."',
												serial_number_customer  = '".$in['serial_number_customer']."',
												name 					= '".$in['name']."',
												commercial_name 		= '".$in['commercial_name']."',
												active					= '".$in['active']."',
												legal_type				= '".$in['legal_type']."',
												user_id					= '".$acc_manager_ids."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												c_email					= '".$in['c_email']."',
												sector					= '".$in['sector']."',
												comp_phone				= '".$in['comp_phone']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$in['c_type']."',
												zip_name				= '".$in['zip']."',
												our_reference			= '".$in['our_reference']."',
												invoice_email_type		= '1',
												acc_manager_name		= '".utf8_encode($in['acc_manager'])."',
												sales_rep				= '".$in['sales_rep_id']."',
												city_name 				= '".$in['city']."',
												country_name 			= '".get_country_name($in['country_id'])."',
												payment_term 			= '".$payment_term."',
												payment_term_type 		= '".$payment_term_type."',
												cat_id					= '".$in['cat_id']."',
												vat_regime_id			= '1',
												btw_nr					= '".$in['btw_nr']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												creation_date			= '".time()."' ");

		$address_id = $this->db->insert("INSERT INTO customer_addresses SET
												customer_id			=	'".$in['customer_id']."',
												country_id			=	'".$in['country_id']."',
												state_id			=	'".$in['state_id']."',
												city				=	'".$in['city']."',
												zip					=	'".$in['zip']."',
												address				=	'".$in['address']."',
												billing				=	'1',
												is_primary			=	'1',
												delivery			=	'1'");
		// if($in['address']){
			if(!$in['zip'] && $in['city']){
				$address = $in['address'].', '.$in['city'].', '.$country_n;
			}elseif(!$in['city'] && $in['zip']){
				$address = $in['address'].', '.$in['zip'].', '.$country_n;
			}elseif(!$in['zip'] && !$in['city']){
				$address = $in['address'].', '.$country_n;
			}else{
				$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
			}
			$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
			$output= json_decode($geocode);
			$in['lat'] = $output->results[0]->geometry->location->lat;
			$in['lng'] = $output->results[0]->geometry->location->lng;
			$this->save_location($in);

		$in['value']=$in['btw_nr'];
		$check_valid_vat=$this->check_vies_vat_number($in);
		if($check_valid_vat != false){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['customer_id']."' ");
		}else if($check_valid_vat == false && $in['trends_ok']){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['customer_id']."' ");
		}else{
			$in['value']='';
		}
		
		set_first_letter('customers',$in['name'],$this->field_n,$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		// msg::$success = gm("Changes have been saved.");
		insert_message_log($this->pag,'{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;

		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
										 								AND name 		= 'company-customers_show_info'	");
		if(!$show_info->move_next()) {
			$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name    = 'company-customers_show_info',
																value   = '1' ");
		} else {
			$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
																	  AND name 		= 'company-customers_show_info' ");
		}
		self::sendCustomerToZintra($in);

		$count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
		if($count == 1){
			doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
		}

		// ark::$controller = 'customer';
		msg::success ( gm("Changes have been saved."),'success');
		json_out(array('customer_id'=>$in['customer_id']));
		// header("Location: index.php?do=company-customer&customer_id=".$in['customer_id']."");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer(&$in)
	{
		if(!$this->CanEdit($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}
		if(!$this->updateCustomer_validate($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}
        // $query_sync=array();
		
		// $c_type = explode(',', $in['c_type']);
		$c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']){
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		// Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET name 				= '".$in['name']."',
		                 					firstname 				= '".$in['firstname']."',
											serial_number_customer  = '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											active					= '1',
											legal_type				= '".$in['legal_type']."',
											user_id					= '".$acc_manager_ids."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['c_email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['comp_phone']."',
											btw_nr					= '".$in['btw_nr']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											c_type_name				= '".$c_type_name."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											acc_manager_name		= '".utf8_encode($in['acc_manager'])."',
											sales_rep				= '".$in['sales_rep']."',
											is_supplier				= '".$in['is_supplier']."',
											is_customer				= '".$in['is_customer']."',
											our_reference			= '".$in['our_reference']."'
					  	WHERE customer_id='".$in['customer_id']."' ");

		$this->db->query("UPDATE customer_addresses SET
												country_id			=	'".$in['country_id']."',
												state_id			=	'".$in['state_id']."',
												city				=	'".$in['city']."',
												zip					=	'".$in['zip']."',
												address				=	'".$in['address']."'
												WHERE address_id 	=	'".$in['address_id']."'												
												");

		if($in['customer_id_linked']){
			$link_id = $this->db->insert("INSERT INTO customer_link SET customer_id='".$in['customer_id']."',
														customer_id_linked='".$in['customer_id_linked']."',
														link_type='".$in['link_type']."'");
			if($in['link_type'] == 1){
				$type = 2;
			}
			else{
				$type = 1;
			}
			$this->db->query("INSERT INTO customer_link SET customer_id='".$in['customer_id_linked']."',
														customer_id_linked='".$in['customer_id']."',
														link_type='".$type."',
														link_id='".$link_id."'");
		}

		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."'	WHERE buyer_id='".$in['customer_id']."' ");

		
		// Sync::end($in['customer_id']);

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		// $in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		json_out();
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		// if($in['customer_link']){
		// 	$v->field('link_type', 'Type of link', 'required');
		// }
		return $this -> addCustomer_validate($in);

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addCustomer_validate(&$in)
	{
		$in['c_email'] = trim($in['c_email']);
		$v = new validation($in);
		$v->field('name', 'Name', 'required','name is required');

		if (ark::$method == 'addCustomer') {
			$v->field('country_id', 'Country', 'required:exist[country.country_id]', 'country_id is required');
		}
		if (ark::$method == 'updateCustomer') {
        	$our_ref_val=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_COMPANY_NUMBER' ");
		  	if($in['our_reference'] && $our_ref_val){
		 			$v->field('our_reference','Company Nr.',"unique[customers.our_reference.(customer_id!='".$in['customer_id']."')]");
		  	}
	  	}


		if($in['c_email']){
			if (ark::$method == 'addCustomer')
			{
				$v->field('c_email', 'Email', 'email:unique[customers.c_email]');
			}
			else
			{
				$v->field('c_email','Email',"email:unique[customers.c_email.(customer_id!='".$in['customer_id']."')]");
			}
		}
		if($in['do']=='customers-EditCustomer-customers-addCustomer'){
			if(USE_COMPANY_NUMBER == 1){
         		$in['our_reference']= generate_customer_number(DATABASE_NAME);
         	}
      	}


		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function CanEdit(&$in){
		if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
			return true;
		}
		$c_id = $in['customer_id'];
		if(!$in['customer_id'] && $in['contact_id']) {
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		}
		if($c_id){
			if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
				$u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
				if($u){
					$u = explode(',', $u);
					if(in_array($_SESSION['u_id'], $u)){
						return true;
					}
					else{
						msg::$warning = gm("You don't have enought privileges");
						return false;
					}
				}else{
					msg::$warning = gm("You don't have enought privileges");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendCustomerToZintra(&$in)
	{	
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->update_financial_valid($in)){
			return false;
		}
		$apply_fix_disc = 0;
		if($in['apply_fix_disc']){$apply_fix_disc = 1;}
		$apply_line_disc = 0;
		if($in['apply_line_disc']){$apply_line_disc = 1;}
		$check_vat_number = $this->db->field("SELECT check_vat_number FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($in['btw_nr']!=$check_vat_number){
			$check_vat_number = '';
		}
		if($in['invoice_email_type']==1){

		    $in['invoice_email']='';
		    $in['attention_of_invoice']='';
		}
		// Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET
											btw_nr								=	'".$in['btw_nr']."',
											bank_name							=	'".$in['bank_name']."',
											payment_term					=	'".$in['payment_term']."',
											payment_term_type 		= '".$in['payment_term_start']."',
											bank_bic_code					=	'".$in['bank_bic_code']."',
											fixed_discount				=	'".return_value($in['fixed_discount'])."',
											bank_iban							=	'".$in['bank_iban']."',
                      						vat_id								=	'".$in['vat_id']."',
											cat_id								=	'".$in['cat_id']."',
											no_vat								=	'".$in['no_vat']."',
											currency_id						= '".$in['currency_id']."',
											invoice_email					=	'".$in['invoice_email']."',
											invoice_email_type					=	'".$in['invoice_email_type']."',
											attention_of_invoice		   =	'".$in['attention_of_invoice']."',
											invoice_note2					=	'".$in['invoice_note2']."',
											deliv_disp_note				=	'".$in['deliv_disp_note']."',
											external_id						=	'".$in['external_id']."',
											internal_language			=	'".$in['internal_language']."',
											apply_fix_disc				= '".$apply_fix_disc."',
											apply_line_disc				= '".$apply_line_disc."',
											line_discount 				= '".return_value($in['line_discount'])."',
											comp_reg_number 				= '".$in['comp_reg_number']."',
											vat_regime_id				= '".$in['vat_regime_id']."',
											siret						= '".$in['siret']."',
											check_vat_number 			= '".$check_vat_number."',
											identity_id                 = '".$in['identity_id']."'
											WHERE customer_id			= '".$in['customer_id']."' ");
		//quote_reference			=	'".$in['quote_reference']."',

		// Sync::end($in['customer_id']);
		// msg::$success = gm("Changes have been saved.");
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		self::sendCustomerToZintra($in);
		$in['updateFromFinancial'] = true;
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial_valid(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		if($in['invoice_email']){
			$v->field('invoice_email', 'Type of link', 'email');
		}

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function save_location(&$in)
	{
		$this->db->query("INSERT INTO addresses_coord SET
							customer_id='".$in['customer_id']."',
							location_lat='".$in['lat']."',
							location_lng='".$in['lng']."'");
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = 'error';
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			$in['crm']['country_dd']		= build_country_list(0);
			$in['crm']['country_id']		= $in['comp_country'] ? $in['comp_country'] : '26';
			$in['crm']['address']			= $in['comp_address'];
			$in['crm']['city']				= $in['comp_city'];
			$in['crm']['zip']				= $in['comp_zip'];
			$in['crm']['customer_id']		= $in['customer_id'];
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='2' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='1' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				$in['success'] = gm('VAT Number is valid');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_field(&$in)
	{
		$this->db->query("DELETE FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				if(!is_null($value)){
					$this->db->query("INSERT INTO customer_field SET customer_id='".$in['customer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				}
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function archive(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->archive_validate($in)){
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET active='0'  where customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE customer_contacts SET active='0'  where customer_id='".$in['customer_id']."' ");

		// Sync::end($in['customer_id']);
		msg::success ( gm("Company has archived"),'success');

		insert_message_log($this->pag,'{l}Customer archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		$this->deleteCustomerFromZintra($in);
		/*if($in['front_register']){
			return ark::run('company-new_companies_list');
		}*/
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function activate(&$in)
	{
		if(!$this->archive_validate($in)){
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET active='1'  where customer_id='".$in['customer_id']."' ");

		// Sync::end($in['customer_id']);
		msg::success ( gm("Company has activated"),'success');
		insert_message_log($this->pag,'{l}Customer activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		/*if($in['front_register']){
			return ark::run('company-new_companies_list');
		}*/
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function archive_validate(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteCustomerFromZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'delete');
		console::log($put);
		$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function showGraph(&$in)
	{
		// $this
		$sql_txt = "UPDATE ";
		$YodaDashboardGraph = $this->db_users->field("SELECT value FROM user_meta WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");
		if($YodaDashboardGraph == null){
			$sql_txt = "INSERT INTO ";
		}
		$YodaDashboardGraph = explode(';', $YodaDashboardGraph);
		if($in['show']==''){			
			array_push($YodaDashboardGraph,$in['key']);
			$value = implode(';', $YodaDashboardGraph);
			$this->db_users->field($sql_txt." user_meta SET value='".$value."' WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");
		}else{
			$diff = array_diff($YodaDashboardGraph, $in['key'] );
			$value = implode(';', $diff);
			$this->db_users->field($sql_txt." user_meta SET value='".$value."' WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");
		}
		return true;
	}

	/**
	 * Activate a contact
	 * @param  array $in
	 * @return bool
	 */
	function activate_contact(&$in)
	{
		if(!$this->archive_contact_validate($in)){
			return false;
		}
         // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET active='1'  where contact_id='".$in['contact_id']."' ");
		// Sync::end();
		msg::success ( gm('Contact has activated'),'success');
		insert_message_log($this->pagc,'{l}Contact activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contact_id']);
		return true;
	}

	/**
	 * Archive a contact
	 * @param  array $in
	 * @return bool
	 */
	function archive_contact(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->archive_contact_validate($in)){
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET active='0'  where contact_id='".$in['contact_id']."' ");
        // Sync::end();
		msg::success (gm('Contact has archived'),'success');

		insert_message_log($this->pagc,'{l}Contact archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contact_id']);
		return true;
	}

	/**
	 * Archive a contact validate
	 * @param  array $in
	 * @return bool
	 */
	function archive_contact_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		return $v->run();
	}
	
	function contact_unsubscribe(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		$in['time'] = time();
		$this->db->query("UPDATE customer_contacts SET unsubscribe='".$in['time']."', s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		msg::success ( gm("Contact Updated").".",'success');
		return true;
	}

	function save_view_list(&$in){
		$view='';
		if($in['view']==1){
			$view = '1';
		}else{
			$view = '0';
		}
		$this->db->query("UPDATE settings SET value = '".$view."' WHERE constant_name = 'CUSTOMER_VIEW_LIST' ");
		msg::success (gm('Changes have been saved.'),'success');
		json_out($in);
		return true;
	}

	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 */
	function contact_update(&$in)
	{
		if(!$this->CanEdit($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}
		if(!$this->contact_update_validate($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}

		if($in['birthdate']){
			$in['birthdate'] = strtotime($in['birthdate']);
		}

		// $pos = addslashes(preg_replace('/:[0-9]+/', '', $in['position_n']));
/*		$pos = array();
		foreach ($in['position'] as $key => $value) {
			$pos[] = addslashes($this->db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value."' "));
		}
		if(!empty($pos)){
			$pos = implode(',', $pos);
		}else{
			$pos = '';
		}
		$in['position'] = implode(',', $in['position']);*/
		// Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET
                                            customer_id	=	'".$in['customer_id']."',
                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$in['position']."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title']."',
										    e_title		=	'".$in['e_title']."',
											fax			=	'".$in['fax']."',
											company_name=	'".$in['customer']."',
											title_name	=	'".$in['title_name']."',
											position_n  = 	'".$pos."',
										    last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."'");
		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}
		if($in['customer_id']){
			$id = $this->db->field("SELECT id FROM customer_contactsIds WHERE customer_id='".$in['customer_id']."' AND contact_id='".$in['contact_id']."'  ");
			$in['accounts'][]=array(
				'position' => $in['position'],
				'department' =>	$in['department'],
				'title' => $in['title'],
				'e_title' => $in['e_title'],
				'email' => $in['email'],
				'phone' => $in['phone'],
				'fax' => $in['fax'],
				's_email' => $in['s_email'],
				'id' =>	$id
			);
		}
		$this->updateContactAccounts($in);

		$this->updateContactCustomerName($in); 

		// Sync::end($in['contact_id']);

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}
		if(!$in['customer_id']){
			$this->removeFromAddress($in);
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],$this->field_nc,$in['contact_id']);

		//save extra fields values
		$this->update_custom_contact_field($in);

		msg::success ( gm('Contact Updated').'.','success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;
		// $this->contact_address($in);
		json_out();
		return true;
	}

	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		/*if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}*/

		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function contact_add_validate(&$in)
	{
		$in['email'] = trim($in['email']);
		$v = new validation($in);
		$v->field('firstname', 'First name', 'required','firstname is required');
		$v->field('lastname', 'Last name', 'required', 'lastname is required');
		if(ark::$method=='contact_add'){
			$v->field('customer_id', 'Customer', 'required', 'customer_id is required');
		}
		if ($in['do']=='company-xcustomer_contact-customer-contact_add')
		{
			$v->field('email', 'Email', 'email:unique[customer_contacts.email]');
		}
		else
		{
			$v->field('email','Email',"email:unique[customer_contacts.email.(contact_id!='".$in['contact_id']."')]");
		}
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function removeFromAddress(&$in)
	{
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function update_custom_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				if(!is_null($value)){
					$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				}
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function contact_address(&$in)
	{
		$in['primary']=1;
		$exist = $this->db->field("SELECT address_id FROM customer_contact_address WHERE is_primary='1' AND contact_id='".$in['contact_id']."' ");
		if($exist){
			$in['address_id'] = $exist;
			$this->update_contact_address($in);
		}else{
			$this->add_contact_address($in);
		}
	}

	/****************************************************************
	* function add_contact_address(&$in)                         *
	****************************************************************/
	function add_contact_address(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$in['country_id'] && !$in['state'] && !$in['city'] && !$in['zip'] && !$in['address'] ){
			msg::error (  gm('Please fill at least one of the fields'),'error');
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
        $delivery = 0;
        $primary = 0;
        if($in['delivery']){
        	$delivery = 1;
        }
        if($in['primary']){
        	$primary = 1;
        }
        if($in['primary']){
        	$this->db->query("UPDATE customer_contact_address SET is_primary='0' WHERE contact_id='".$in['contact_id']."' ");
        	#updating existing recurring invoices
        	$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
			while ($this->db->move_next()) {
				$this->db->query("UPDATE recurring_invoice SET buyer_address	= '".$in['address']."',
																	buyer_zip	= '".$in['zip']."',
																	buyer_city	= '".$in['city']."',
																buyer_country_id= '".$in['country_id']."'
																WHERE contact_id= '".$in['contact_id']."' ");
			}
        }
        $this->db->query("INSERT INTO customer_contact_address SET country_id 	=	'".$in['country_id']."',
														  		   	state 		=	'".$in['state']."',
														           	city 		=	'".$in['city']."',
															        zip 		=	'".$in['zip']."',
															        address 	=	'".$in['address']."',
															        delivery 	=	'".$delivery."',
															        is_primary 	=	'".$primary."',
														      		contact_id 	=	'".$in['contact_id']."' ");
		// Sync::end($in['address_id']);
		msg::success (gm('Address added'),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;
		return true;
	}

	/****************************************************************
	* function update_contact_address(&$in)                         *
	****************************************************************/
	function update_contact_address(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$in['country_id'] && !$in['state'] && !$in['city'] && !$in['zip'] && !$in['address'] ){
			msg::error (gm('Please fill at least one of the fields'),'success');
			return false;
		}

        // Sync::start(ark::$model.'-'.ark::$method);
        $delivery = 0;
        if($in['delivery']){
        	$delivery = 1;
        }
        $this->db->query("UPDATE customer_contact_address SET country_id 	=	'".$in['country_id']."',
														  		   state 	=	'".$in['state']."',
														           city 	=	'".$in['city']."',
														           zip 		=	'".$in['zip']."',
														           address 	=	'".$in['address']."',
														           delivery =	'".$delivery."'
													      WHERE contact_id 	=	'".$in['contact_id']."'
													      AND  	address_id 	= 	'".$in['address_id']."' ");
        if($in['primary']){
        	$this->db->query("UPDATE customer_contact_address SET is_primary='0' WHERE contact_id='".$in['contact_id']."' ");
        	$this->db->query("UPDATE customer_contact_address SET is_primary='1' WHERE contact_id='".$in['contact_id']."' AND address_id='".$in['address_id']."' ");
        	#updating existing recurring invoices
        	$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
			while ($this->db->move_next()) {
				$this->db->query("UPDATE recurring_invoice SET buyer_address	= '".$in['address']."',
																	buyer_zip	= '".$in['zip']."',
																	buyer_city	= '".$in['city']."',
																buyer_country_id= '".$in['country_id']."'
																WHERE contact_id= '".$in['contact_id']."' ");
			}
        }
		// Sync::end($in['address_id']);
		msg::success ( gm('Address updated'),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function attackToAddress(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		$this->db->query("INSERT INTO customer_contact_addresses SET address_id='".$in['address_id']."', contact_id='".$in['contact_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
	 * Add contact to customer
	 * @param  array $in
	 * @return bool
	 */
	function contact_add(&$in)
	{
		if(!$this->CanEdit($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}
		if(!$this->contact_add_validate($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}

		if($in['birthdate']){
			$in['birthdate'] = strtotime($in['birthdate']);
		}

		// $pos = addslashes(preg_replace('/:[0-9]+/', '', $in['position_n']));
/*		$pos = array();
		foreach ($in['position'] as $key => $value) {
			$pos[] = addslashes($this->db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value."' "));
		}
		if(!empty($pos)){
			$pos = implode(',', $pos);
		}else{
			$pos = '';
		}
		$in['position'] = implode(',', $in['position']);*/

        // $query_sync=array();
		// $pos = addslashes(preg_replace('/:[0-9]+/', '', $in['position_n']));
		$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
						customer_id	=	'".$in['customer_id']."',
						firstname	=	'".$in['firstname']."',
						lastname	=	'".$in['lastname']."',
						position	=	'".$in['position']."',
						department	=	'".$in['department']."',
						email		=	'".$in['email']."',
						birthdate	=	'".$in['birthdate']."',
						phone		=	'".$in['phone']."',
						cell		=	'".$in['cell']."',
						note		=	'".$in['note']."',
						sex			=	'".$in['sex']."',
						title		=	'".$in['title']."',
						language	=	'".$in['language']."',
						e_title		=	'".$in['e_title']."',
						fax			=	'".$in['fax']."',
						company_name=	'".$in['customer']."',
						title_name	=	'".$in['title_name']."',
						`create`	=	'".time()."',
						position_n  = 	'".$pos."',
						last_update = 	'".time()."'");

       
		// if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1' WHERE contact_id='".$in['contact_id']."' ");
		// }

        // Sync::end($in['contact_id'],$query_sync);

		// $this->updateContactAccounts($in);

		set_first_letter('customer_contacts',$in['lastname'],$this->field_nc,$in['contact_id']);
		//save extra fields values
		$this->update_custom_contact_field($in);

		
		insert_message_log($this->pagc,'{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;

		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
										 								AND name 		= 'company-contacts_show_info'	");
		if(!$show_info->move_next()) {
			$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name    = 'company-contacts_show_info',
																value   = '1' ");
		} else {
			$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
																	  AND name 		= 'company-contacts_show_info' ");
		}
		// $this->add_contact_address($in);
		/*if(!$in['add_contact_from_company']){
			// header("Location: index.php?do=company-xcustomer_contact&contact_id=".$in['contact_id']."");
			ark::$controller = 'xcustomer_contact';
		}else{
			//$in['view']=0;
			ark::$controller = 'customer_contacts';
		}*/
		$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
		if($count == 1){
			doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
		}

		$this->addContactToCustomer($in);
		$this->setAccountAsPrimary($in);
		msg::success ( gm('Contact Added'),'success');

		if($in['customer_id']){
			$id = $this->db->field("SELECT id FROM customer_contactsIds WHERE customer_id='".$in['customer_id']."' AND contact_id='".$in['contact_id']."'  ");
			$in['accounts'][]=array(
				'position' => $in['position'],
				'department' =>	$in['department'],
				'title' => $in['title'],
				'e_title' => $in['e_title'],
				'email' => $in['email'],
				'phone' => $in['phone'],
				'fax' => $in['fax'],
				's_email' => $in['s_email'],
				'id' =>	$id
			);
		}

		$this->updateContactAccounts($in);
		json_out(array('contact_id'=>$in['contact_id']));
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function customise(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);

		if($is_extra_field =='extra_field_'){

			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE customer_fields SET value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");

		}else{

			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}else{
				#insert
				$this->db->query("INSERT INTO customise_field SET value='".$in['value']."', controller='".$in['controller']."', field='".$in['field']."' ");
			}

		}

		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function default_customise(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);

		if($is_extra_field =='extra_field_'){

			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE customer_fields SET default_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");

		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET default_value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}
		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function creation_customise(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);

		if($is_extra_field =='extra_field_'){

			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE customer_fields SET creation_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");

		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET creation_value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function customise_contact(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);
		if($is_extra_field =='extra_field_'){
			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE contact_fields SET value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");
		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}else{
				#insert
				$this->db->query("INSERT INTO customise_field SET value='".$in['value']."', controller='".$in['controller']."', field='".$in['field']."' ");
			}	
		}
		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function default_customise_contact(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);
		if($is_extra_field =='extra_field_'){
			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE contact_fields SET default_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");
		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET default_value = '".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}
		
		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function creation_customise_contact(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);
		if($is_extra_field =='extra_field_'){
			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE contact_fields SET creation_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");
		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET creation_value = '".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}	
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	/**
	 * update custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function update_field(&$in)
	{
		if($in['is_dd']=='1'){
			$bulk_type = '2';
			$bulk_table_name = 'customer_extrafield_dd';
			$bulk_field_name = 'name';
			$bulk_field_id_name = 'extra_field_id';
			$bulk_dd = 'build_extra_field_dd';
		}else{
			$bulk_type = '1';
			$bulk_table_name = 'customer_field';
			$bulk_field_name = 'value';
			$bulk_field_id_name = 'field_id';
			$bulk_dd = '';
		}
		$this->db->query("UPDATE customer_fields SET label='".$in['value_input']."', is_dd='".$in['is_dd']."' WHERE field_id='".$in['field_id']."' ");
		$this->db->query("UPDATE import_labels SET	label_name='".$in['value_input']."',
													is_custom_dd='".$in['is_dd']."',
													bulk_type='".$bulk_type."',
													bulk_table_name='".$bulk_table_name."',
													bulk_field_name='".$bulk_field_name."',
													bulk_field_id_name='".$bulk_field_id_name."',
													bulk_dd='".$bulk_dd."'
												WHERE field_name='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	/**
	 * delete a custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function delete_field(&$in)
	{
		$this->db->query("DELETE FROM customer_fields WHERE field_id='".$in['field_id']."' ");
		$this->db->query("DELETE FROM import_labels WHERE field_name='".$in['field_id']."' ");
		$this->db->query("DELETE FROM customer_extrafield_dd WHERE extra_field_id='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}


	/**
	 * add new custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function add_field(&$in)
	{
		$in['field_id'] = $this->db->insert("INSERT INTO customer_fields SET label='Label', value='1' ");
		$bulk_type = '1';
		$bulk_table_name = 'customer_field';
		$bulk_field_name = 'value';
		$bulk_field_id_name = 'field_id';
		$bulk_dd = '';

		$this->db->insert("INSERT INTO import_labels SET 	label_name='Label',
															field_name='".$in['field_id']."',
															tabel_name='customer_field',
															type='customer',
															custom_field='1',
															bulk_type='".$bulk_type."',
															bulk_table_name='".$bulk_table_name."',
															bulk_field_name='".$bulk_field_name."',
															bulk_field_id_name='".$bulk_field_id_name."',
															bulk_dd='".$bulk_dd."'
															");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function update_contact_field(&$in)
	{
		if($in['is_dd']=='1'){
			$bulk_type = '2';
			$bulk_table_name = 'contact_extrafield_dd';
			$bulk_field_name = 'name';
			$bulk_field_id_name = 'extra_field_id';
			$bulk_dd = 'build_extra_field_dd';
		}else{
			$bulk_type = '1';
			$bulk_table_name = 'contact_field';
			$bulk_field_name = 'value';
			$bulk_field_id_name = 'field_id';
			$bulk_dd = '';
		}
		$this->db->query("UPDATE contact_fields SET label='".$in['value_input']."', is_dd='".$in['is_dd']."' WHERE field_id='".$in['field_id']."' ");
		$this->db->query("UPDATE import_labels SET	label_name='".$in['value_input']."',
													is_custom_dd='".$in['is_dd']."',
													bulk_type='".$bulk_type."',
													bulk_table_name='".$bulk_table_name."',
													bulk_field_name='".$bulk_field_name."',
													bulk_field_id_name='".$bulk_field_id_name."',
													bulk_dd='".$bulk_dd."'
												WHERE field_name='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function delete_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_fields WHERE field_id='".$in['field_id']."' ");
		$this->db->query("DELETE FROM import_labels WHERE field_name='".$in['field_id']."' ");
		$this->db->query("DELETE FROM contact_extrafield_dd WHERE extra_field_id='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function add_contact_field(&$in)
	{
		$in['field_id'] = $this->db->insert("INSERT INTO contact_fields SET label='Label', value='1' ");
		$bulk_type = '1';
		$bulk_table_name = 'contact_field';
		$bulk_field_name = 'value';
		$bulk_field_id_name = 'field_id';
		$bulk_dd = '';

		$this->db->insert("INSERT INTO import_labels SET 	label_name='Label contact',
															field_name='".$in['field_id']."',
															tabel_name='contact_field',
															type='customer_contacts',
															custom_field='1',
															bulk_type='".$bulk_type."',
															bulk_table_name='".$bulk_table_name."',
															bulk_field_name='".$bulk_field_name."',
															bulk_field_id_name='".$bulk_field_id_name."',
															bulk_dd='".$bulk_dd."'
															");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	/**
	 * various function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function custRef(&$in)
	{
	
		if(!defined('USE_COMPANY_NUMBER')){
			$this->db->query("INSERT INTO settings SET value='".$in['use_company_number']."', constant_name='USE_COMPANY_NUMBER' ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['use_company_number']."' WHERE constant_name='USE_COMPANY_NUMBER'");
		}
		if($in['inc_part_c']){
			if(!defined('ACCOUNT_COMPANY_DIGIT_NR')){
				$this->db->query("INSERT INTO settings SET value='".$in['inc_part_c']."', constant_name='ACCOUNT_COMPANY_DIGIT_NR', module='billing' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['inc_part_c']."' WHERE constant_name='ACCOUNT_COMPANY_DIGIT_NR'");
			}
		}
		if($in['first_reference_c']){
			if(!defined('FIRST_REFERENCE_C')){
				$this->db->query("INSERT INTO settings SET value='".$in['first_reference_c']."', constant_name='FIRST_REFERENCE_C', module='billing' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['first_reference_c']."' WHERE constant_name='FIRST_REFERENCE_C'");
			}
		}
		
		msg::success ( gm("Changes saved"),'success');

		return true;
	}

	function update_exist_customer(&$in)
	{

		$customers = $this->db->query("SELECT customer_id, our_reference FROM customers WHERE our_reference='' ORDER BY `customers`.`creation_date`  ASC  ");
		if(!$in['our_reference']){
			while ($customers->next()) {
			 	$in['our_reference']= generate_customer_number(DATABASE_NAME);
				$this->db->query("UPDATE customers SET our_reference ='".$in['our_reference']."' WHERE customer_id='".$customers->f('customer_id')."' ");
			}
		}
	}

	function paymentTerm(&$in)
	{

		// Sync::start(ark::$model.'-'.ark::$method);

		$v = new validation($in);
		$v -> f('payment_term', 'Payment term', 'integer');

		if(!$v->run())
		{
			return false;
		}

		$this->db->query("UPDATE settings SET value='".$in['payment_term']."' WHERE constant_name='PAYMENT_TERM'");
		$this->db->query("UPDATE settings SET value='".$in['choose_payment_term_type']."' WHERE constant_name = 'PAYMENT_TERM_TYPE' ");

		// Sync::end();

		msg::$success = gm("Payment term updated");
		return true;
	}

	//ADDRESSES
	/****************************************************************
	* function address_add(&$in)                                          *
	****************************************************************/
	function address_add(&$in)
	{
		if(!$this->CanEdit($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}
		$in['failure']=false;
		if(!$this->address_add_validate($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}

		$country_n = get_country_name($in['country_id']);

 		$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																			customer_id			=	'".$in['customer_id']."',
																			country_id			=	'".$in['country_id']."',
																			state_id			=	'".$in['state_id']."',
																			city				=	'".$in['city']."',
																			zip					=	'".$in['zip']."',
																			address				=	'".$in['address']."',
																			billing				=	'".$in['billing']."',
																			is_primary			=	'".$in['primary']."',
																			delivery			=	'".$in['delivery']."'");
		if($in['billing']){
			$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
		}

		/*if($in['delivery']){
			$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
		}*/
		if($in['primary']){
			if($in['address'])
			{
				if(!$in['zip'] && $in['city'])
				{
					$address = $in['address'].', '.$in['city'].', '.$country_n;
				}elseif(!$in['city'] && $in['zip'])
				{
					$address = $in['address'].', '.$in['zip'].', '.$country_n;
				}elseif(!$in['zip'] && !$in['city'])
				{
					$address = $in['address'].', '.$country_n;
				}else
				{
					$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
				}
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
				$output= json_decode($geocode);
				$lat = $output->results[0]->geometry->location->lat;
				$long = $output->results[0]->geometry->location->lng;
			}
			$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
			$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
		}

		msg::success (gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		$in['address_id'] = $address_id;
		json_out(array('address_id'=>$in['address_id']));
		return true;
	}

	/****************************************************************
	* function address_add_validate(&$in)                                          *
	****************************************************************/
	function address_add_validate(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		$v->field('country_id', 'Country', 'required:exist[country.country_id]','country_id is required');

		return $v->run();
	}

	function check_valid_vat_number(&$in){
		/*$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
		if(in_array(get_country_code($in['account_billing_country_id']), $eu_countries) && $in['account_vat_number']){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $in['account_vat_number']);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('../valid_vat.php');
		}else{
			msg::$error = gm('Country is not in EU');
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			msg::$error = gm('Incorrect VAT number or format');
			return false;
		}else if(isset($response) && $response == 'error'){
			msg::$error = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
			return false;
		}else if(isset($response) && $response == 'valid'){
			$in['result_data']=json_encode($result);
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
		}else{
			msg::$error = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
			return false;
		}*/
		$in['value']=$in['account_vat_number'];
		$check_vies_number=$this->check_vies_vat_number($in);
		console::log($check_vies_number);
		if($check_vies_number != false){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."', vat_form='0' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
			$in['success'] = gm('VAT Number is valid');
		}else if($check_vies_number == false && $in['trends_ok']){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."', vat_form='1' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
			$in['success'] = gm('VAT Number is valid');
		}else{
			msg::$error = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
			$in['error'] = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
		}
		json_out($in);
		return true;
	}
	function backup_data(&$in)
	{
		set_time_limit(0);
	    ini_set('memory_limit', '-1');   
	    $folder=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm';
	    $folder_exist=file_get_contents($folder);

	    if($folder_exist==false){
	    	@mkdir($folder,0755,true);
	    }
	    $array_exclude = array('customer_import');
		$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml';
		$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml';
		$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml';
		$file_fill1 = file_get_contents($file1);
		$file_fill2 = file_get_contents($file2);
		$file_fill3 = file_get_contents($file3);
		if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
			if($in['file_id']!=0){
					ini_set('memory_limit', '-1');
					$this->db->query("show tables");
					$table_names=array();

					while ($this->db->move_next()){
						if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'customer')!==FALSE && strpos($this->db->f("Tables_in_".DATABASE_NAME), 'backup')===FALSE)
						{
							if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
								$table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
							}
						}
						
					}
					$xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');	

				    for($i=0;$i<count($table_names);$i++){
				    	// $cus = $xml->addChild('customers-data');
			            	 $tbl=$xml->addChild($table_names[$i],''); 
			                 //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
			                $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
			                $this->db->query("SELECT * FROM  ".$table_names[$i]."");
			                while($this->db->move_next()){
			                 	    $chese=$tbl->addChild('line',''); 
			                 	    foreach ($cols as $key => $value) {
			                 	   		$chese->addChild($value['Field'], $this->db->f($value['Field'])) ;
			                 	   	}
			                 	    /*$this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."");
						            while ($this->db2->move_next()) {
			                 	  	  $chese->addChild($this->db2->f('Field'), $this->db->f($this->db2->f('Field'))) ;
			 	               	  	}*/
			                }
			    	}
			    	$time=time();
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup'.$in['file_id'].'.xml');
		    		$this->db->query("UPDATE backup_data SET backup_time='".$time."' WHERE backup_id='".$in['file_id']."'");
		    	msg::success ( gm('Data successfully saved'),'success');
		    	$in['step']=2;
		    	return true;
			}else{
				msg::error ( gm('The file is full'),'error');
	    		return false;
			}
		}else{
			ini_set('memory_limit', '-1');
			$this->db->query("show tables");
			$table_names=array();

			while ($this->db->move_next()){
				if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'customer')!==FALSE && strpos($this->db->f("Tables_in_".DATABASE_NAME), 'backup')===FALSE)
				{
					if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
						$table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
					}
				}
				
			}
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');	
		    for($i=0;$i<count($table_names);$i++){
		    	// $cus = $xml->addChild('customers-data');
	            	 $tbl=$xml->addChild($table_names[$i],''); 
	                 //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
	                $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
	                $this->db->query("SELECT * FROM  ".$table_names[$i]."");
	                while($this->db->move_next()){
	                 	    $chese=$tbl->addChild('line',''); 
	                 	   	foreach ($cols as $key => $value) {
	                 	   		$chese->addChild($value['Field'], $this->db->f($value['Field'])) ;
	                 	   	}
				            /*while ($this->db2->move_next()) {
	                 	  	  $chese->addChild($this->db2->f('Field'), $this->db->f($this->db2->f('Field'))) ;
	 	               	  	}*/
	                }
	    	}
	    	$time=time();
	    	if($file_fill1==false){
	    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml');
	    		$this->db->insert("INSERT INTO backup_data SET backup_name='backup1.xml', backup_time='".$time."'");
	    	}elseif($file_fill1==true&&$file_fill2==false){
	    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml');
	    		$this->db->insert("INSERT INTO backup_data SET backup_name='backup2.xml', backup_time='".$time."'");
	    	}elseif ($file_fill2==true&&$file_fill3==false) {
	    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml');
	    		$this->db->insert("INSERT INTO backup_data SET backup_name='backup3.xml', backup_time='".$time."'");
	    	}
			
		}
    	msg::success ( gm('Data successfully saved'),'success');
    	$in['step']=2;
    	return true;
	}
	function uploadify(&$in)
	{
		$response=array();
		// Define a destination
		$targetFolder = 'upload/'.DATABASE_NAME.'/crm/'; // Relative to the root
		@mkdir($targetFolder,0755,true);
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

			// Validate the file type
			$fileTypes = array('xml','xls','xlsx'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			if (in_array($fileParts['extension'],$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
				// $response['success'] = $_FILES['Filedata']['name'];
				msg::success($_FILES['Filedata']['name'],'success');
				$in['xls_name'] = $_FILES['Filedata']['name'];

				json_out($in);
				// $this->import_company($in);
				/*echo json_encode($response);*/
			} else {
				// $response['error'] = 'Invalid file type.';
				msg::error( 'Invalid file type.','error');
				// echo json_encode($response);
			}

		}
	}

	
	
	function delete_import_company(&$in)
	{
		if(!$this->delete_import__company_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM company_import_log WHERE company_import_log_id='".$in['company_import_log_id']."' ");

		msg::success ( gm('Import Log deleted').'.','success');

		return true;
	}
	/****************************************************************
	* function delete_import_validate(&$in)                         *
	****************************************************************/
	function delete_import__company_validate(&$in)
	{
		$is_ok = true;
		if(!$in['company_import_log_id']){
			msg::error ( gm('Invalid ID').'.','error');
			return false;
		}
		else{
			$this->db->query("SELECT company_import_log_id FROM company_import_log WHERE company_import_log_id='".$in['company_import_log_id']."'");
			if(!$this->db->move_next()){
				msg::error ( gm('Invalid ID').'.','error');
				return false;
			}
		}

		return $is_ok;
	}
	

	function addContactToCustomer(&$in){
		if(!$this->addContactToCustomer_validate($in)){
			return false;
		}
		$this->db->query("INSERT INTO customer_contactsIds SET customer_id='".$in['customer_id']."', contact_id='".$in['contact_id']."' ");
		$this->updateContactCustomerName($in);
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function addContactToCustomer_validate(&$in){
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]:unique[customer_contactsIds.customer_id.contact_id=\''.$in['contact_id'].'\']', "Invalid Id");
		return $v->run();
	}

	function removeContactFromCustomer(&$in){
		if(!$this->removeContactFromCustomer_validate($in)){
			return false;
		}else{
			$count = $this->db->field("SELECT count(customer_id) FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' ");
			if($count<=1){
				msg::notice ( gm("There must be at least one account."),'notice');
				return false;
			}
			$primary = $this->db->field("SELECT `primary` FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND customer_id='".$in['customer_id']."'  ");
			if($primary == 1){
				msg::notice ( gm("You cannot delete the primary account"),'notice');
				return false;	
			}
		}
		$this->db->query("DELETE FROM customer_contactsIds WHERE customer_id='".$in['customer_id']."' AND contact_id='".$in['contact_id']."' ");
		$this->updateContactCustomerName($in);
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function removeContactFromCustomer_validate(&$in){
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customer_contactsIds.customer_id.contact_id=\''.$in['contact_id'].'\']', "Invalid Id");
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function updateContactCustomerName(&$in)
	{
		$name = $this->db->field("SELECT name FROM customers INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id WHERE customer_contactsIds.contact_id='".$in['contact_id']."' AND customer_contactsIds.`primary`=1");
		$info = $this->db->query("SELECT phone, email,customer_id FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND `primary`=1 ");
		
		$count = $this->db->field("SELECT count(customer_id) FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."'  ");
		
		// $name .= " (".$count.")";
		$this->db->query("UPDATE customer_contacts SET company_name='".addslashes($name)."', phone='".$info->f('phone')."', email='".$info->f('email')."', customer_id='".$info->f('customer_id')."' WHERE contact_id='".$in['contact_id']."' ");
		return true;
	}

	/****************************************************************
	* function address_delete(&$in)                                          *
	****************************************************************/
	function address_delete(&$in)
	{
		// $in['failure']=false;
		if(!$this->address_delete_validate($in)){
			// $in['failure']=true;
			return false;
		}

		$this->db->query("DELETE FROM customer_addresses
					  WHERE customer_id	=	'".$in['customer_id']."'
					  AND 	address_id	=	'".$in['address_id']."' ");
		$this->db->query("DELETE FROM article_threshold_dispatch where address_id='".$in['address_id']."' AND customer_id	=	'".$in['customer_id']."'");

		msg::success ( gm("Address deleted."),'success');
		insert_message_log($this->pag,'{l}Customer address deleted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		return true;
	}
	/****************************************************************
	* function address_delete_validate(&$in)                                          *
	****************************************************************/
	function address_delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[customer_addresses.address_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		return $v->run();

	}

	/****************************************************************
	* function address_update(&$in)                                          *
	****************************************************************/
	function address_update(&$in)
	{
		if(!$this->CanEdit($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			return false;
		}
		$in['failure']=false;
		if(!$this->address_update_validate($in)){
			http_response_code(400);
			json_out( array( 'error' => msg::get_errors() ) );
			// $in['failure']=true;
			return false;
		}
		if(!$in['primary']) {
			$exist_primary = $this->db->field("SELECT address_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' AND address_id!= '".$in['address_id']."' AND is_primary = '1' ");
			if(!$exist_primary) {
				$in['primary'] =1 ;
			}
		}
		$country_n = get_country_name($in['country_id']);
		
		$this->db->query("UPDATE customer_addresses SET
											country_id		=	'".$in['country_id']."',
											state_id		=	'".$in['state_id']."',
											city			=	'".$in['city']."',
											zip				=	'".$in['zip']."',
											address			=	'".$in['address']."',
											billing			=	'".$in['billing']."',
											is_primary		=	'".$in['primary']."',
											delivery		=	'".$in['delivery']."'
										WHERE customer_id	=	'".$in['customer_id']."' AND address_id='".$in['address_id']."' ");

		if($in['billing']){
			$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
		}
		/*if($in['delivery']){
			$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
		}*/
		
		if($in['primary']){
			if($in['address'])
			{
				if(!$in['zip'] && $in['city'])
				{
					$address = $in['address'].', '.$in['city'].', '.$country_n;
				}elseif(!$in['city'] && $in['zip'])
				{
					$address = $in['address'].', '.$in['zip'].', '.$country_n;
				}elseif(!$in['zip'] && !$in['city'])
				{
					$address = $in['address'].', '.$country_n;
				}else
				{
					$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
				}
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
				$output= json_decode($geocode);
				$lat = $output->results[0]->geometry->location->lat;
				$long = $output->results[0]->geometry->location->lng;
			}
			$this->db->query("SELECT coord_id FROM addresses_coord WHERE customer_id='".$in['customer_id']."'");
			if($this->db->move_next())
			{
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
			}else
			{
				$this->db->query("INSERT INTO addresses_coord SET location_lat='".$lat."', location_lng='".$long."', customer_id='".$in['customer_id']."'");
			}
			$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
			$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
		}

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		json_out();
		return true;
	}
	/****************************************************************
	* function address_update_validate(&$in)                                          *
	****************************************************************/
	function address_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[customer_addresses.address_id]', "Invalid Id");
		if(!$v->run()){
			return false;
		} else {
			return $this->address_add_validate($in);
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function updateContactAccounts(&$in)
	{
		
		foreach ($in['accounts'] as $key => $value) {
			$this->db->query("UPDATE customer_contactsIds SET position='".$value['position']."',
			                 								department='".$value['department']."',
			                 								title='".$value['title']."',
			                 								e_title='".$value['e_title']."',
			                 								email='".$value['email']."',
			                 								phone='".$value['phone']."',
			                 								fax='".$value['fax']."',
			                 								s_email='".$value['s_email']."'
			                WHERE id='".$value['id']."' ");
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function setAccountAsPrimary(&$in)
	{
		if(!$this->removeContactFromCustomer_validate($in)){
			return false;
		}
		$this->db->query("UPDATE customer_contactsIds SET `primary`='0' WHERE contact_id='".$in['contact_id']."' ");
		$this->db->query("UPDATE customer_contactsIds SET `primary`='1' WHERE contact_id='".$in['contact_id']."' AND customer_id='".$in['customer_id']."' ");
		$this->updateContactCustomerName($in);
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function customer_notes(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		$this->db->query("UPDATE customers SET customer_notes='".$in['customer_notes']."' WHERE customer_id='".$in['customer_id']."'");
		// msg::$success = gm("Notes updated");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function delete_list(&$in)
	{
		$this->db->query("DELETE FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
		$this->db->query("DELETE FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' ");
		msg::success ( gm('List deleted').'.','success');
		return true;
	}

	function refresh(&$in){
		$contacts_list = $this->db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
		$contacts_list->next();
		$list_for = $contacts_list->f('list_for');
		$filtru = unserialize($contacts_list->f('filter'));
		if($filtru){
			$filter = '';
			foreach ($filtru as $field => $value){
				if($value){
					if(is_array($value)){
						$val = '';
						foreach ($value as $key){
							$val .= $key.',';
						}
						$value = $val;
					}
					$value = rtrim($value,',');
					if($value){
						if(substr(trim($field), 0, 13)=='custom_dd_id_'){
							$filter.= " AND ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "customer_field.value = '".addslashes($v2)."' OR ";
							}
							$filter = rtrim($filter,"OR ");
						}elseif($field == 'c_type '){
							$filter .= " AND ( ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
							}
							$filter = rtrim($filter,"OR ");
							$filter .= " ) ";
						}elseif ($field == 'c_type') {
							$filter .= " AND ( ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
							}
							$filter = rtrim($filter,"OR ");
							$filter .= " ) ";
						}
						elseif(trim($field) == 'position'){
							$filter .= " AND ( ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "position_n like '%".addslashes($v2)."%' OR ";
							}
							$filter = rtrim($filter,"OR ");
							$filter .= " ) ";
						}elseif(trim($field) == 'customer_contacts.email'){
							$filter.=" AND customer_contacts.email='' ";
						}
						elseif(trim($field) == 'customers.c_email'){
							$filter.=" AND customers.c_email='' ";
						}
						else{
							$filter .= " AND ".$field." IN (".addslashes($value).") ";
						}
					}elseif(trim($field) == 'customers.c_email'){
						$filter.= " AND customers.c_email!='' ";
					}elseif(trim($field) == 'customer_contacts.email'){
						$filter.= " AND customer_contacts.email!='' ";
					}elseif(trim($field) == 'customers.s_emails'){
						$filter.= " AND customers.s_emails='1' ";
					}
				}
			}
		}
		if(!$filter){
			$filter = '1=1';
		}
		$filter = ltrim($filter," AND");

		if(!$list_for){
			$filter .= " AND customer_contacts.active='1' ";
			$cont = $this->db->field("SELECT count(DISTINCT customer_contacts.contact_id)
						    FROM customer_contacts
						    LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
						    LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
						    LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
						    LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
						    LEFT JOIN customer_type ON customers.c_type=customer_type.id
						    LEFT JOIN customer_sector ON customers.sector=customer_sector.id
						    LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
						    LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
						    LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            		LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            		LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
						    WHERE ".$filter." ");
		}else{
			$filter .= " AND customers.active='1' AND is_admin='0' ";
			$cont =  $this->db->field("SELECT COUNT( DISTINCT customers.customer_id)
			        FROM customers
			        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
			        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
			        LEFT JOIN customer_type ON customers.c_type=customer_type.id
			        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
			        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
			        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
			        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
							LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            		WHERE ".$filter." ");
		}

		$this->db->query("UPDATE contact_list SET contacts='".$cont."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	function list_add(&$in){
		if(!$this->list_add_validate($in)){
			json_out($in);
			return false;
		}

		$in['contact_list_id'] = $this->db->insert("INSERT INTO contact_list SET name='".$in['list_name']."', list_type='".$in['list_type']."', contacts='".$in['nr_contacts']."', list_for='".$in['list_for']."' ");

		if($in['list_type'] == 1 && $in['list_for'] == 0){
			$contacts = $this->db->query("SELECT DISTINCT customer_contacts.contact_id
							        FROM customer_contacts
							        LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
							        LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
							        LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							        WHERE customer_contacts.active='1'");

			while ($contacts->next()) {
				$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('contact_id')."' ");
			}
		}
		if($in['list_type'] == 1 && $in['list_for'] == 1){
			$contacts = $this->db->query("SELECT DISTINCT customers.customer_id
							        FROM customers
							        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
							        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							        WHERE customers.active='1'");

			while ($contacts->next()) {
				$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('customer_id')."' ");
			}
		}
		$in['state'] = 'smartListEdit';
		if($in['list_for'] == 1){
			// $in['state'] = 'crm_lv';
			// ark::$controller = 'crm_lv';
		}

		msg::success ( gm('List added'),'success');
		json_out($in);
		return true;
	}

	function list_add_validate(&$in){
		$v = new validation($in);
		$v->field('list_name', 'List name', "required:unique[contact_list.name.(contact_list_id!='".$in['contact_list_id']."')]");
		return $v->run();
	}

	function list_update(&$in){
		if(!$this->list_add_validate($in)){
			return false;
		}
		/*if($in['list_type'])
		{
			$k =  $this->db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
		}else
		{*/
			$k = $in['nr_contacts'];
		// }
		$this->db->query("UPDATE contact_list SET contacts='".$k."',filter='".addslashes( serialize($in['filter']))."', name='".$in['list_name']."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		if($in['filter'] && $in['list_type'] && $in['list_for'] == 0){ //echo 'a';exit();
			$this->db->query("UPDATE contact_list_contacts SET is_part='0' WHERE contact_list_id='".$in['contact_list_id']."' ");
			$contacts = $this->db->query("SELECT DISTINCT customer_contacts.contact_id
							        FROM customer_contacts
							        LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
							        LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
							        LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            						LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
							        WHERE ".stripslashes($in['filter_a']));
			$i = 0;
			while ($contacts->next()) {
				$exist = $this->db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_id='".$contacts->f('contact_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				if($exist->next()){
					$this->db->query("UPDATE contact_list_contacts SET is_part='1' WHERE contact_id='".$contacts->f('contact_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				}
				else{
					$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('contact_id')."' ");
				}
				$i++;
			}
			$this->db->query("UPDATE contact_list SET contacts='".$i."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		}elseif ($in['filter'] && $in['list_type'] && $in['list_for'] == 1) { //echo 'b';exit();
			$this->db->query("UPDATE contact_list_contacts SET is_part='0' WHERE contact_list_id='".$in['contact_list_id']."' ");
			$contacts = $this->db->query("SELECT DISTINCT customers.customer_id
							        FROM customers
							        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
							        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            						LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
							        WHERE ".stripslashes($in['filter_a']));
			$i = 0;
			while ($contacts->next()) {
				$exist = $this->db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_id='".$contacts->f('customer_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				if($exist->next()){
					$this->db->query("UPDATE contact_list_contacts SET is_part='1' WHERE contact_id='".$contacts->f('customer_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				}
				else{
					$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('customer_id')."' ");
				}
				$i++;
			}
			$this->db->query("UPDATE contact_list SET contacts='".$i."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		} //print_r($in['list_for']);exit();
		// if($in['list_for'] == 1){
		// 	ark::$controller = 'crm_lv';
		// }


		$this->db->query("DELETE FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND is_part='0' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	function list_deactivate_c(&$in){
		$exist = $this->db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_id='".$in['id']."' AND contact_list_id='".$in['contact_list_id']."' ");
		if(!$exist->next()){
			$this->db->query("INSERT INTO contact_list_contacts SET active='".$in['active']."', contact_id='".$in['id']."', contact_list_id='".$in['contact_list_id']."' ");
		}else{
			$this->db->query("UPDATE contact_list_contacts SET active='".$in['active']."' WHERE contact_id='".$in['id']."' AND contact_list_id='".$in['contact_list_id']."' ");
		}
		$this->db->query("UPDATE contact_list SET contacts=(SELECT COUNT(contact_id) FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND active='1' ) WHERE contact_list_id='".$in['contact_list_id']."' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	function list_deactivate_co(&$in){
		$exist = $this->db->query("SELECT customer_id FROM company_reports_lists WHERE customer_id='{$in['id']}' AND list_id='{$in['list_id']}' ");
		if(!$exist->next()){
			$this->db->query("INSERT INTO company_reports_lists SET active='{$in['active']}', customer_id='{$in['id']}', list_id='{$in['list_id']}' ");
		}else{
			$this->db->query("UPDATE company_reports_lists SET active='{$in['active']}' WHERE customer_id='{$in['id']}' AND list_id='{$in['list_id']}' ");
		}
		$this->db->query("UPDATE company_report_list SET contacts=(SELECT COUNT(customer_id) FROM company_reports_lists WHERE list_id='{$in['list_id']}' AND active='1' ) WHERE list_id='{$in['list_id']}' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	function sendToGoogle(&$in)
	{
		### google calendar ###
		ark::loadLibraries(array('gCalendar'));
		$calendar = new gCalendar($in);
		$calendar->sendToGoogle($in);
		### google calendar ###
	}

}
?>