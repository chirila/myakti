<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps;
global $p_access;

perm::controller('all', 'admin',in_array($apps['customers'], $p_access));
perm::controller('all', 'user',in_array($apps['customers'], $p_access));
if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
	perm::controller(array('settings','customer_fields','restore_data','import_accounts'), 'user', false);
}
if(defined('NEW_SUBSCRIPTION') && ADV_CRM == 1 ){
	perm::controller(array('smartList'), 'all', false);	
}
