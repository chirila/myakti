<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($db_config);
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$output = array();	
$order_by_array = array('company_name','lastname','firstname');
$arguments_s = '';
$arguments='';
$arguments_o='';
$arguments_a='';
$l_r =28;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$output['view'] = 0;
if(CUSTOMER_VIEW_LIST){
	$output['view'] = 1;
}

$filter = " customer_contacts.front_register = 0 ";
if(empty($in['archived'])){
	$filter.= " AND customer_contacts.active=1";
}else{
	$in['archived'] = 1;
	$filter.= " AND customer_contacts.active=0";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}

//FILTER LIST
$order_by = " ORDER BY lastname ";
$escape_chars = array('(','?','*','+',')');
if(!empty($in['search'])){
	$filter_x = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['search']));
	foreach ($search_arr as $value) {
		if(in_array($value, $escape_chars)){
			$value = '\\'.$value;
		}
		$filter_x .= $value.".?";
	}

	$search_words = explode(' ', $in['search']);
	foreach ($search_words as $word_pos => $searched_word) {
		if(trim($searched_word)){
			
			if($word_pos==0){
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR lastname LIKE '%".$searched_word."%'
							OR company_name LIKE '%".$searched_word."%'
							OR phone LIKE '%".$searched_word."%'
							OR cell LIKE '%".$searched_word."%'
							OR email LIKE '%".$searched_word."%' )";
			}else{
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR lastname LIKE '%".$searched_word."%'
							OR company_name LIKE '%".$searched_word."%'
							OR phone LIKE '%".$searched_word."%'
							OR cell LIKE '%".$searched_word."%'
							OR email LIKE '%".$searched_word."%' )";
			}

		}
	}

	$arguments_s.="&search=".$in['search'];
}

if(!empty($in['order_by'])){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];				
	}
}

if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	$max_rows =  $db->field("SELECT COUNT( contact_id ) AS max_contact_rows 
							FROM customer_contacts
							LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter );
	$c = $db->query("SELECT customer_contacts.*, customers.customer_id
				FROM customer_contacts
				INNER JOIN 
				LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
				WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();

}else{
	$arguments = $arguments.$arguments_s;
	$max_rows =  $db->field("SELECT count(contact_id) FROM customer_contacts WHERE ".$filter );
	$c = $db->query("SELECT * FROM customer_contacts
				WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
}

$arguments = $arguments.$arguments_s;
$output['max_rows'] =  $max_rows;
$output['list'] = array();

foreach ($c as $key => $value) {
	/*$is_new_event = '';
	if(in_array($value['customer_id'], $customer_logs)){
		$is_new_event = 'new_event_timeline';
	}*/
	$output['list'][]=array(
		/*'name'						=> $value['name'],
		// 'serial_number_customer'	=> $value['serial_number_customer'],
		'country'					=> $value['country_name'] ? $value['country_name']: '&nbsp;',
		'city'						=> $value['city_name'] ? $value['city_name'] : '&nbsp;' ,
		'customer_id'				=> $value['customer_id'],
		'sales'						=> $value['c_type_name'] ? $value['c_type_name'] : '&nbsp;' ,
		// 'contacts'					=> get_contact_nr($value['customer_id']),
		'contacts'					=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '&nbsp;',
		// 'is_active'     			=> $value['ac']? true : false,
		// 'new_event'					=> $is_new_event,
		// 'edit_link'					=> 'index.php?do=company-customer&customer_id='.$value['customer_id'],
		// 'time_link'					=> 'index.php?do=company-customer_timelineNew&customer_id='.$value['customer_id'],
		// 'delete_link'				=> array('do'=>'customers-customer-customer-delete', 'id'=>$value['customer_id']),
		'archive_link'				=> array('do'=>'customers-customers-customers-archive', 'customer_id'=>$value['customer_id']),
		'activate_link'				=> array('do'=>'customers-customers-customers-activate', 'customer_id'=>$value['customer_id']),*/
		'indiv_created'			=> $value['indiv_created'] == 1 ? false : true,
		'cus_name'			    => $value['company_name'],
		'customer_id'		    => $value['customer_id'],
		'con_firstname'			=> $value['firstname'] ? $value['firstname'] : '&nbsp;',
		'con_lastname'			=> $value['lastname'] ? $value['lastname'] : '&nbsp;',
		'con_position'			=> $value['position_n'],
		'con_phone'				=> $value['phone'] ? $value['phone'] : '-',
		'con_cell'				=> $value['cell'] ? $value['cell'] : '-',
		'con_email'				=> $value['email'],
		// 'is_active'     		=> $value['active']? true : false,
		// 'delete_title'     		=> gm('Archive'),
		// 'delete_class'     		=> 'archive',
		// 'hide_delete_link'		=> $value['is_primary'] == 1 ? 'hide':'' ,
		// 'contact_edit_link'		=> 'index.php?do=company-xcustomer_contact&cont/act_id='.$value['contact_id'],
		// 'edit_link'				=> $in['archived'] ? 'index.php?do=company-contacts-customer-activate_contact&contact_id='.$value['contact_id'].$arguments.$arguments_o : 'index.php?do=company-xcustomer_contact&contact_id='.$value['contact_id'],
		// 'delete_link'			=> 'index.php?do=company-contacts-customer-archive_contact&contact_id='.$value['contact_id'].$arguments.$arguments_o,
		'archive_link'				=> array('do'=>'customers-contacts-customers-archive_contact', 'contact_id'=>$value['contact_id']),
		'activate_link'				=> array('do'=>'customers-contacts-customers-activate_contact', 'contact_id'=>$value['contact_id']),
		// 'view_link'				=> 'index.php?do=company-xcustomer_contact&contact_id='.$value['contact_id'].'&front_register='.$in['front_register'].'',
		'contact_id' 			=> $value['contact_id'],

		// 'activity_link'			=> 'index.php?do=company-customer_contact_activity&contact_id='.$value['contact_id'].'&customer_id='.$value['customer_id'].'&bar_data=1&contacts_ref=1',
		//'hide_activity_icon' 		=> check_if_activity_exists('',$value['contact_id']) == 1 ? '' : 'hide',		
		// 'edit_delete' 			=> check_if_activity_exists('',$value['contact_id']) == 1 ? 'edit_delete' : 'edit_delete_gray',
	);
}

$output['export_args'] = ltrim($arguments,'&');
$output['lr']=$l_r;
json_out($output);