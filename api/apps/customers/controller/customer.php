<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$view = new at(ark::$viewpath.$in['filename'].'.html');

$db->query("SELECT * FROM customise_field WHERE controller='company-customer' ");
while ($db->next()) {
	$view->assign(array(
		$db->f('field') => $db->f('value') == 1 ? true : false,
	));
}

unset($_SESSION['articles_id']);

$various1 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 1' ");
$various2 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 2' ");

$view->assign(array(
	'is_admin'				=> $_SESSION['access_level'] == 1 ? true : false,
	'is_front_register'		=> true,
	'not_front_register'	=> false,
	'various1_text'			=> $various1 ? $various1 : gm('Various 1'),
	'various2_text'			=> $various2 ? $various2 : gm('Various 2'),
	'are_extra'				=> $extra ? true : false,
	// 'company_relationship' 		=> ark::run('company-edit_comp_relationship'),
));

if($in['id']=='tmp'){
	$label = $db->query("SELECT * FROM customer_fields WHERE value = '1' AND creation_value=1  ORDER BY field_id ");
}else{
	$label = $db->query("SELECT * FROM customer_fields WHERE value = '1'  ORDER BY field_id ");
}




$all = $label->getAll();
foreach ($all as $key => $value) {
	$loop = 'extra_field';

	$f_value = $db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$value['field_id']."' ");
	if($in['id']==tmp){
		$creation=$value['creation_value']==1 ? true:false;
	}else{
		$creation=true;
	}
	if($value['is_dd']==1){
		$extra_field_dd_name = $db->field("SELECT name
										FROM customer_extrafield_dd
										INNER JOIN customer_field ON ( customer_extrafield_dd.extra_field_id = customer_field.field_id
										AND customer_extrafield_dd.id = customer_field.value )
										WHERE extra_field_id =  '".$value['field_id']."'
										AND customer_id='".$in['customer_id']."' ");
	}

	$view->assign(array(
		'field_id'			=> $value['field_id'],
		'label'				=> $value['label'],
		'label_if'			=> $creation,
		'value'				=> $f_value ? $f_value : '',
		'normal_input'		=> $value['is_dd']==1? false:true,
		'extra_field_dd'	=> $value['is_dd']==1? build_extra_field_dd($f_value,$value['field_id']):'',
		'is_admin'			=> $_SESSION['access_level'] == 1 ? true : false,
		'extra_field_name' 	=> $value['is_dd']==1 ? $extra_field_dd_name : ( $f_value ? $f_value : '' ),
	),$loop);	
	$view->loop($loop);
}

// return $view->fetch();