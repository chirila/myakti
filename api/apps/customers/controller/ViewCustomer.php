<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if(!$in['customer_id']){

	http_response_code(400);
    json_out();
}

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

// $users_auto = $db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
// $user_auto = array();
// foreach ($users_auto as $key => $value) {
// 	$user_auto[] = array('id'=>$value['user_id'], 'value'=>utf8_encode(strip_tags($value['first_name'] . ' ' . $value['last_name'])) );
// }

$result =array();


if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
$hide_fields = array('type','supplier','vat_id','payment_term','payment_term_type','fixed_discount','check_vat_number','vat_form','quote_reference','is_admin','various1','various2','first_letter','no_vat','front_register','attention_of_invoice','invoice_note2','external_id','payment_type_id','pickup_category_id','top_level_id','deliv_disp_note','unsubscribe','s_emails','pub_id','apply_fix_disc','apply_line_disc','line_discount','identity_id','bPaid_debtor_id','cat_id','delivery_home','currency_id','internal_language','credit','production_company','production_company_id','pickup_fee','pickup_method','machine_handling_fee','vat_regime_id','invoice_email_type','production_category_id');
$customer = $db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ")->getAll();
$customer = $customer[0];

$showFields = $db->query("SELECT * FROM customise_field WHERE controller='company-customer'")->getAll();
$showField = array();
foreach ($showFields as $key => $value) {
	$showField[$value['field']] = $value['value'];
}


foreach ($customer as $key => $value) {
	if(!in_array($key, $hide_fields) && $showField[$key] !== '0'){
		$result['account'][$key] = $value;
	}
}

$result['account']['is_customer'] = $result['account']['is_customer'] == 1 ? true : false;
$result['account']['is_supplier'] = $result['account']['is_supplier'] == 1 ? true : false;
$result['account']['AccountType'] = $result['account']['type'] == 0 ? 'Company' : 'Individual';
$result['account']['email'] 	  = $result['account']['c_email'];

$c_type = explode(',', $customer['c_type']);
if(!empty($c_type[0])){
	foreach ($c_type as $key ) {
		$c_types_name = $db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
		$c_types .= $c_types_name.', ';
		$hidden_c_type .= $c_types_name.':'.$key.',';
	}
}

$usernames = '';
if($result['account']['acc_manager']){
	// $usernames2 = '';
	$user_id = trim($customer['user_id'],',');
	if($user_id){
		$user = $db_users->query("SELECT CONCAT_WS(' ',first_name, last_name) AS username,user_id FROM users WHERE user_id IN (".$user_id.") ORDER BY FIELD(user_id, ".$user_id.")");
		while($user->next()){
			$usernames.= $user->f('username').', ';
			// $usernames2.= $user->f('username').':'.$user->f('user_id').',';
		}
		$usernames = rtrim($usernames,', ');
		// $usernames2 = rtrim($usernames2,',');
	}
	$result['account']['acc_manager']   			= utf8_encode($usernames);
}
$result['account']['extra'] = array();
$extra = $db->query("SELECT field_id,value FROM customer_field WHERE customer_id='".$in['customer_id']."' ")->getAll();
foreach ($extra as $key => $value) {
	$result['account']['extra'][$value['field_id']] = $value['value'];
}


// $customer['payment_term_start'] = $customer['payment_term_type'];

// if($customer['creation_date']){
// 	$customer['creation_date'] = date(ACCOUNT_DATE_FORMAT,$customer['creation_date']);
// }else{
// 	unset($customer['creation_date']);
// }
if(isset($result['account']['legal_type'])){
	$result['account']['legal_type_name']			= $db->field("SELECT name FROM customer_legal_type WHERE id = '".$result['account']['legal_type']."' ");
}
if(isset($result['account']['sales_rep'])){
	$result['account']['sales_rep_name']			= $db->field("SELECT name FROM sales_rep WHERE id = '".$customer['sales_rep']."' ");
}
if(isset($result['account']['sector'])){
	$result['account']['sector_name']				= $db->field("SELECT name FROM customer_sector WHERE id = '".$customer['sector']."' ");
}
if(isset($result['account']['language'])){
	$result['account']['language_name']				= $db->field("SELECT name FROM customer_contact_language WHERE id = '".$customer['language']."' ");
}
if(isset($result['account']['lead_source'])){
	$result['account']['lead_source_name'] 			= $db->field("SELECT name FROM customer_lead_source WHERE id = '".$customer['lead_source']."' ");
}
if(isset($result['account']['activity'])){
	$result['account']['activity_name'] 			= $db->field("SELECT name FROM customer_activity WHERE id = '".$customer['activity']."' ");
}
$result['account']['currency'] 					= get_commission_type_list($customer['currency_id']);
$result['account']['vat'] 						= $db->field("SELECT value FROM vats WHERE  vat_id = '".$customer['vat_id']."' ");
$result['account']['internal_language_txt']		= build_language_dd_new($customer['internal_language'],'txt');
$result['account']['vat_regime']				= build_vat_regime_dd($customer['vat_regime_id']);

$result['addresses']							= get_customerAddresses($in);

// $various1 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 1' ");
// if(!$various1){
// 	$various1 = gm('Variuos 1');
// }
// $various2 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 2' ");
// if(!$various2){
// 	$various2 = gm('Variuos 2');
// }

json_out($result);

function get_customerAddresses($in){
	$db = new sqldb();
	$result = array('list'=>array());
	$comp_addr_data = $db->query("SELECT customer_addresses.*,country.name AS country
			FROM customer_addresses
			LEFT JOIN country ON country.country_id=customer_addresses.country_id
			WHERE customer_id='".$in['customer_id']."'
			ORDER BY is_primary DESC,address_id ");
	
	$i=0;
	while($comp_addr_data->next()){
		$address_type = '';
		if($comp_addr_data->f('is_primary') == 1){
			$address_type .= gm('Primary addr').' ';
			$coords = $db->query("SELECT location_lat AS latitude, location_lng AS longitude FROM  `addresses_coord` WHERE  `customer_id` ='".$in['customer_id']."' ")->getAll();
			if(empty($coords[0])){
				$coords[0] =updateCustomerPrimaryAddressCoords($comp_addr_data,$in['customer_id']);
			}
		}
		elseif($comp_addr_data->f('billing') == 1){
			$address_type .= gm('Billing addr').' ';
		}else{
			$address_type .= gm('Delivery addr').' ';
		}

		$text_delivery_address = gm('Delivery addr');
		$text_billing_address = gm('Billing addr');
		$text_primary_address = gm('Primary addr');

		$address_type2 = '-';
		$comp_addr_data->f('delivery') == 1 ? $address_type2 = $text_delivery_address : '';
		$comp_addr_data->f('billing') == 1 ? $address_type2 = $text_billing_address : '';
		($comp_addr_data->f('delivery') == 1 && $comp_addr_data->f('billing') == 1) ? $address_type2 = $text_delivery_address.' / '.$text_billing_address : '';
		if($comp_addr_data->f('is_primary') == 1 && $comp_addr_data->f('delivery') == 1){
			$comp_addr_data->f('is_primary') == 1 ? $address_type2 .= ' / '.$text_primary_address : '';
		}elseif($comp_addr_data->f('is_primary') == 1 && $comp_addr_data->f('billing') == 1){
			$comp_addr_data->f('is_primary') == 1 ? $address_type2 .= ' / '.$text_primary_address : '';
		}elseif($comp_addr_data->f('is_primary') == 1){
			$comp_addr_data->f('is_primary') == 1 ? $address_type2 = $text_primary_address : '';
		}
		/*$comp_addr_data->f('is_primary') == 1 ? $address_type2 .= ' / '.$text_primary_address : '';*/
		
		// $address_type = rtrim($address_type,' / ');

		$line=array(
			'address_id'	=> $comp_addr_data->f('address_id'),
			'address' 		=> $comp_addr_data->f('address'),
			'city' 			=> $comp_addr_data->f('city'),
			'country' 		=> get_country_name($comp_addr_data->f('country_id')),
			'country_id' 	=> $comp_addr_data->f('country_id'),
			'customer_id'	=> $in['customer_id'],
			'is_primary'	=> $comp_addr_data->f('is_primary'),
			'is_billing'	=> $comp_addr_data->f('billing'),
			'is_delivery'	=> $comp_addr_data->f('delivery'),
			'zip' 			=> $comp_addr_data->f('zip'),
			// 'address_txt' 	=> nl2br($comp_addr_data->f('address')),
			// 'address_type'	=> $address_type,
			// 'address_type2'	=> $address_type2,
			// 'delete_link'	=> array('do'=>'customers-ViewCustomer-customers-address_delete', 'customer_id'=>$in['customer_id'], 'xget'=> 'customerAddresses', 'address_id'=> $comp_addr_data->f('address_id') ),
		);
		$result['list'][] = $line;
		$i++;
	}
	// $result['count'] = $i;
	$result['rows']=(int)$i;
	// $result['customer_id'] = $in['customer_id'];
	$result['coords'] = $coords[0] ? $coords[0] : array();
	return $result;
}

function get_accountAddress($in){
	$db = new sqldb();
	$result = array();
	
	if($in['id'] == 'tmp'){
		// $page_title 				= gm('Add Address');
		// $result['do_next']			='customers-ViewCustomer-customers-address_add';
		$result['country_id']		= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
	}else{

		// $result['do_next']='customers-ViewCustomer-customers-address_update';
		$db->query("SELECT * FROM customer_addresses 
					WHERE address_id='".$in['id']."' 
					AND customer_id='".$in['customer_id']."'");
		
		// $page_title = gm('Edit Address');
		
		$db->f('billing') 		== 1 ? $billing =true : false;
		$db->f('delivery') 		== 1 ? $delivery =true : false;
		$result['city']				= $db->f('city');
		$result['zip']            	= $db->f('zip');
		$result['address']			= $db->f('address');
		$result['state_id']			= $db->f('state_id');
		$result['contact_id']		= $db->f('contact_id');
		/*if($db->f('contact_id')){
			$in['contact'] = get_contact_name($db->f('contact_id'));
		}*/
		
		$result['billing']			= $billing;
		$result['delivery']			= $delivery;
		$result['primary']			= $db->f('is_primary') == 1 ? true :false;
		$result['country_id']		= $db->f('country_id');
	}
	// $result['country_dd']			= build_country_list($result['country_id']);
		
	// $view->assign(array(
	// $result['page_title']			= $page_title;
	$result['customer_id']			= $in['customer_id'];
	// $result['address_id']			= $in['address_id'];
	// $result['xget']					='accountAddress';
	// ));


	return $result;
}

function get_contact($in){
	$db = new sqldb();
	$result = array();

	if(isset($in['id'])){
		$in['contact_id'] = $in['id'];
	}

	$contact = $db->query("SELECT * FROM customer_contacts 
	                      INNER JOIN customer_contactsIds
	                      WHERE customer_contacts.contact_id='".$in['contact_id']."' AND customer_contactsIds.customer_id='".$in['customer_id']."' ")->getAll();
	$contact = $contact[0];

	// $contact_new = $db->query("SELECT * FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' ")->getAll();
	// $contact_new = $contact_new[0];
	// $contact['position'] = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$contact_new['position']."'");
	// $contact['department'] = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact_new['department']."'");
	// $contact['phone'] = $contact_new['phone'];
	// $contact['fax'] = $contact_new['fax'];

	// $contact['exact_title']=$contact_new['e_title'];
	// $contact['e_title']=$contact_new['e_title'];
	// $contact['title_txt'] = $db->field("SELECT name FROM customer_contact_title  WHERE id='".$contact_new['title']."' ");
	if(!is_numeric($contact['title'])){
		$contact['title']='0';
	}

	if(!is_numeric($contact['sex']) || !$contact['sex'] ){
		$contact['sex']='1';
	}
	// switch ($contact['sex']) {
	// 	case '2':
	// 		// $contact['gender_txt'] = gm('Female');
	// 		break;	
	// 	default:
	// 		// $contact['gender_txt'] = gm('Male');
	// 		break;
	// }
	$contact['gender'] = $contact['sex'];
	unset($contact['sex']);
	if(!is_numeric($contact['language'])){
		$contact['language']='1';
	}
	// $contact['language_txt'] = $db->field("SELECT name FROM customer_contact_language WHERE id='".$contact['language']."' ");
	// $contact['exact_title_txt'] = $db->field("SELECT e_title FROM customer_contactsIds  WHERE contact_id='".$in['contact_id']."' ");

	if(!is_numeric($contact['department'])){
		$contact['department']='1';
	}
	// $contact['department_txt'] = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact['department']."' ");

	// if(!is_numeric($contact['country_id'])){
	// 	$contact['country_id']=ACCOUNT_BILLING_COUNTRY_ID;
	// }

	if($contact['birthdate']){
		// $contact['birthdate_txt'] = date(ACCOUNT_DATE_FORMAT,$contact['birthdate']);
		$contact['birthdate'] = date("Y-m-d",$contact['birthdate']);
	}


	if($contact['s_email'] == '1'){
		$contact['s_email'] = true;
		// $contact['s_email_txt'] = gm('Yes');
	}else{
		$contact['s_email'] = false;
		// $contact['s_email_txt'] = gm('No');
	}


	// if($contact['create']){
	// 	$contact['create'] = date(ACCOUNT_DATE_FORMAT,$contact['create']);
	// }

	if( isset($contact['position']) && !is_numeric($contact['position']) ){
		$contact['position']='1';
	}
	// $contact['position_txt'] = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$contact['position']."' ");

	if($contact['position_n']){
		$contact['position_n'] = explode(',', $contact['position_n']);
	}

	/*$label = $db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY field_id ");
	$all = $label->getAll();
	$contact['extra']=array();
	$result['extra'] = array();
	$result['extra_name'] = array();
	foreach ($all as $key => $value) {
		$f_value = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$value['field_id']."' ");
		if($value['is_dd']==1){
			$result['extra'][$value['field_id']]=build_extra_contact_field_dd('',$value['field_id']);
			if($in['viewMod']){
				$f_value = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$value['field_id']."' ");
			}
			if(!$f_value){
				$f_value = '0';
			}
		}
		$result['extra_name'][$value['field_id']] = $value['label'];
		
		$contact['extra'][$value['field_id']]=$f_value;
	}*/

	
	/*$i=0;
	$default_show=array();
	$def=$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact' AND value=1 ");
	while ($def->next()) {
		$line = array();
		/*$line[$def->f('field')] = $def->f('value') == 1 ? true : false;* /
		$result['hide'][$def->f('field').'_if'] = $def->f('creation_value') == 1 ? true : false;
	}
	*/


	if($in['contact_id'] == 'tmp' || !$in['contact_id']){
		// $do_next ='customers-Contact-customers-contact_add';
		// $is_add = true;
		// $result['contact_id'] = 'tmp';
	}else{
		// $do_next ='customers-Contact-customers-contact_update';
		// $is_add = false;
		/*$address = $db->query("SELECT * FROM  customer_contact_address WHERE contact_id='".$in['contact_id']."' and is_primary=1 ")->getAll();
		foreach ($address[0] as $key => $value) {
			$contact[$key] = $value;
		}*/
		// $add_id = $db->query("SELECT * FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		// $title = $db->field("SELECT title FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' ");
		/*if($add_id->next()){
			$c_address = $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country  FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
							 		 LEFT JOIN state ON state.state_id=customer_addresses.state_id
							  		 WHERE address_id='".$add_id->f('address_id')."'");
			$contact['attachlocation'] = array(
				'caddress'			=> nl2br($c_address->f('address')),
		  		'czip'			    => $c_address->f('zip'),
		  		'ccity'			    => $c_address->f('city'),
		  		'cstate'			=> $c_address->f('state'),
		  		'ccountry'			=> $c_address->f('country')
			);
			
		}*/

		// $result['accounts'] = get_contactAccounts($in);

	}
	// $contact['do'] = $do_next;

	// $result['is_admin']					= $_SESSION['access_level'] == 1 ? true : false;
	// $contact['title_id']				= $title;
	// $contact['title']					= build_contact_title_type_dd('');
	// $result['department']				= build_contact_dep_type_dd('');
	// $result['language']					= build_language_dd('');
	// $result['country_dd']				= build_country_list('');
	// $result['position']					= build_contact_function('');
	// $result['customer_dd']				= get_customers($in);
	// $result['format']					= 'dd/MM/yyyy';
	// $result['showLinkAccount']			= false;
	// $result['dateOptions'] 				= array(
	// 							            'formatYear'	=> 'YYYY',
	// 							            'startingDay' 	=> 1
	// 							        );

	// $result['viewMod']				= $in['viewMod'];
	// if($in['viewMod']){
	// 	$result['contact_id']			= $in['contact_id'];
	// }

	$showFields = $db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'")->getAll();
	$showField = array();
	foreach ($showFields as $key => $value) {
		$showField[$value['field']] = $value['value'];
	}
	$hide_fields = array('is_primary','zintra_id','list_id','last_update','zintra_sync','email2','indiv_created','first_letter','company_name','reset_request','front_register','create','sub_level_id','api_key','admin_webshop','bPaid_debtor_id','nylas_contact_id');
	foreach ($contact as $key => $value) {
		if(!in_array($key, $hide_fields) && $showField[$key] !== '0'){
		// if(!in_array($key, $hide_fields)){
			$result['contact'][$key] = $value;
		}
	}
	$result['contact']['extra'] = array();
	$extra = $db->query("SELECT field_id,value FROM contact_field WHERE customer_id='".$in['contact_id']."' ")->getAll();
	foreach ($extra as $key => $value) {
		$result['contact']['extra'][$value['field_id']] = $value['value'];
	}
	
	// $result['contact']						= $contact;
	// if(!$in['viewMod']){
	// 	$result['obj']['customer_id']	= isset($in['customer_id']) ? $in['customer_id'] : '';
	// }

	// foreach ($in as $key => $value) {
	// 	if(!array_key_exists($key, $result['obj'])) {
	// 		$result['obj'][$key] = $in[$key];
	// 	}
	// }
	// $result['obj']['contact_name'] = $result['obj']['firstname'].' '.$result['obj']['lastname'];

	// $result['ADV_CRM']					= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;
	return $result;
}


?>