<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($db_config);
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$output = array();	
$order_by_array = array('company_name','lastname','firstname');
$arguments_s = '';
$arguments='';
$arguments_o='';
$arguments_a='';
$l_r =ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

/*$output['view'] = 0;
if(CUSTOMER_VIEW_LIST){
	$output['view'] = 1;
}*/

$filter = " customer_contacts.front_register = 0 AND customer_contactsIds.customer_id=".$in['customer_id']." ";

if(empty($in['archived'])){
	$filter.= " AND customer_contacts.active=1";
}else{
	$in['archived'] = 1;
	$filter.= " AND customer_contacts.active=0";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}

//FILTER LIST
$order_by = " ORDER BY lastname ";

if(!empty($in['search'])){
	$filter_x = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['search']));
	foreach ($search_arr as $value) {
		if(in_array($value, $escape_chars)){
			$value = '\\'.$value;
		}
		$filter_x .= $value.".?";
	}

	$search_words = explode(' ', $in['search']);
	foreach ($search_words as $word_pos => $searched_word) {
		if(trim($searched_word)){
			
			if($word_pos==0){
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR customer_contacts.lastname LIKE '%".$searched_word."%'
							OR customer_contacts.company_name LIKE '%".$searched_word."%'
							OR customer_contacts.phone LIKE '%".$searched_word."%'
							OR customer_contacts.cell LIKE '%".$searched_word."%'
							OR customer_contacts.email LIKE '%".$searched_word."%' )";
			}else{
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR customer_contacts.lastname LIKE '%".$searched_word."%'
							OR customer_contacts.company_name LIKE '%".$searched_word."%'
							OR customer_contacts.phone LIKE '%".$searched_word."%'
							OR customer_contacts.cell LIKE '%".$searched_word."%'
							OR customer_contacts.email LIKE '%".$searched_word."%' )";
			}

		}
	}

	$arguments_s.="&search=".$in['search'];
}

if(!empty($in['order_by'])){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];				
	}
}

if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	$max_rows =  $db->field("SELECT COUNT( customer_contacts.contact_id ) AS max_contact_rows 
							FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter );
	$c = $db->query("SELECT customer_contacts.*, customers.customer_id, customer_contactsIds.position, customer_contactsIds.department, customer_contactsIds.fax, customer_contactsIds.title, 
	                customer_contactsIds.e_title, customer_contactsIds.s_email
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
				WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
}else{
	$arguments = $arguments.$arguments_s;
	$max_rows =  $db->field("SELECT count( customer_contacts.contact_id ) FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							WHERE ".$filter );
	$c = $db->query("SELECT customer_contacts.*, customer_contactsIds.position, customer_contactsIds.department, customer_contactsIds.fax, customer_contactsIds.title, 
	                customer_contactsIds.e_title, customer_contactsIds.s_email FROM customer_contacts
	                INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
					WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
}

$arguments = $arguments.$arguments_s;

$output['rows'] =  $max_rows;
$output['offset'] = $in['offset'];
$output['list'] = array();

$position 				= build_contact_function('',true);
$department				= build_contact_dep_type_dd('',true);
$title					= build_contact_title_type_dd('',true);

$showFields = $db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'")->getAll();
$showField = array();
foreach ($showFields as $key => $value) {
	$showField[$value['field']] = $value['value'];
}

foreach ($c as $key => $value) {
	
	$output['list'][]=array(
		// 'account_name'		    => $value['company_name'],
		// 'account_id'		    => $value['customer_id'],
		'firstname'				=> $value['firstname'] ? $value['firstname'] : '',
		'lastname'				=> $value['lastname'] ? $value['lastname'] : '',
		'position'				=> $showField['position'] ? $position[$value['position']] : '',
		'department'			=> $showField['department'] ? $department[$value['department']] : '',
		'phone'					=> $value['phone'] && $showField['phone'] ? $value['phone'] : '',
		'cell'					=> $value['cell'] && $showField['cell'] ? $value['cell'] : '',
		'email'					=> $showField['email'] ? $value['email'] : '',
		'fax'					=> $showField['fax'] ? $value['fax'] : '',
		'title'					=> $showField['title'] ? $title[$value['title']] : '',
		// 'e_title'				=> $value['e_title'],
		// 's_email'				=> $value['s_email'] == 1 ? gm('Yes') : gm('No'),
		// 'archive_link'			=> array('do'=>'customers-contacts-customers-archive_contact', 'contact_id'=>$value['contact_id']),
		// 'activate_link'			=> array('do'=>'customers-contacts-customers-activate_contact', 'contact_id'=>$value['contact_id']),
		'contact_id' 			=> $value['contact_id'],
	);
}

// $output['export_args'] = ltrim($arguments,'&');
$output['rows_per_page']=$l_r;

json_out($output);