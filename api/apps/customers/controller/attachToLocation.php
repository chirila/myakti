<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$l_r =20;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$max_rows= $db->field("SELECT customer_addresses.address_id
						 FROM customer_addresses
						 LEFT JOIN country ON country.country_id=customer_addresses.country_id
						 LEFT JOIN state ON state.state_id=customer_addresses.state_id
						 WHERE customer_addresses.customer_id='".$in['customer_id']."' ");

$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
						 FROM customer_addresses
						 LEFT JOIN country ON country.country_id=customer_addresses.country_id
						 LEFT JOIN state ON state.state_id=customer_addresses.state_id
						 WHERE customer_addresses.customer_id='".$in['customer_id']."'
						 ORDER BY customer_addresses.address_id LIMIT ".$offset*$l_r.",".$l_r );

$db->move_to($offset*$l_r);

$addresses=array(
	'list'=>array(),
	'lr'=>$l_r,
	'max_rows'=>$max_rows,
	'do_next'=>'customers-Contact-customers-attackToAddress', 
	'contact_id'=> $in['contact_id']
);

while($address->next() ){
  	$a = array(
	  	'address_id'	    => $address->f('address_id'),
	  	'address'			=> nl2br($address->f('address')),
	  	'zip'			    => $address->f('zip'),
	  	'city'			    => $address->f('city'),
	  	'state'			    => $address->f('state'),
	  	'country'			=> $address->f('country')
  	);

	array_push($addresses['list'], $a);

}
$address = null;

json_out($addresses);