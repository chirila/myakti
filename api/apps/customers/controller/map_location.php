<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

// $country_id = 26;
global $database_config,$config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);

$country_id = get_country_by_code($in['country_code']);
// $country_n = get_country_name($country_id);
$filter = " AND 1=1 ";

// if we have a regular user and ONLY_IF_ACC_MANAG == 1, then we show to the user only the
// companies where he is account manager
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
	$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
//center position
// $address='Mechelsesteenweg, 2860 Sint-Katelijne-Waver, Belgium';
// if(!$in['zip'] && !$in['address'])
// {
// 	$address = $in['city'].', '.$country_n;
// }elseif(!$in['zip'] && $in['address'])
// {
// 	$address = $in['address'].', '.$in['city'].', '.$country_n;
// }elseif($in['zip'] && !$in['address'])
// {
// 	$address = $in['zip'].' '.$in['city'].', '.$country_n;
// }else
// {
// 	$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
// }

// if($in['acc_manager_name']){
// 	$filter .= " AND ( acc_manager_name LIKE '%".$in['acc_manager_name']."%' ) ";

// }
// if($in['our_reference']){
// 	$filter .= " AND ( our_reference LIKE '%".$in['our_reference']."%' ) ";
// }
// if($in['btw_nr']){
// 	$filter .= " AND ( btw_nr LIKE '%".$in['btw_nr']."%' ) ";
// }
// if($in['commercial_name']){
// 	$filter .= " AND ( commercial_name LIKE '%".$in['commercial_name']."%' ) ";
// }
// if($in['lead_source']){
// 	$filter .=" AND lead_source='".$in['lead_source']."' ";
// }  
// if($in['sector']){
// 	$filter .= " AND sector='".$in['sector']."' ";
// }
// if($in['c_type_name']){
// 	$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
// }
//$address='Sint-Katelijne-Waver, Belgium';
//$range = 25000;
$range = $in['range'];
if(!$range){
	$range = 5;
}
$enter_addr = true;
// $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
// $output= json_decode($geocode);
// $lat = $output->results[0]->geometry->location->lat;
// $long = $output->results[0]->geometry->location->lng;
list($in['lat'],$in['lng']) = explode(',', $in['latLng']);

$lat = $in['lat'];
$long = $in['lng'];
$data = array();
$i=0;
$result = array('list'=>array());
if($lat && $long)
{
	$found_address=true;
	// distance query is inspired from here https://developers.google.com/maps/articles/phpsqlsearch_v3?csw=1
	$customers = $db->query("SELECT customers.name, customers.customer_id as cust_id, addresses_coord.*, 
	                        ( 6371 * acos( cos( radians(".$lat.") ) * cos( radians( location_lat ) ) * cos( radians( location_lng ) - radians(".$long.") ) + sin( radians(".$lat.") ) * sin( radians( location_lat ) ) ) ) AS distance
	                        FROM customers
							INNER JOIN addresses_coord ON customers.customer_id=addresses_coord.customer_id
							WHERE customers.active='1' ".$filter." HAVING distance < ".$range." ORDER BY distance ");

	/*SELECT id, ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($long) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance FROM markers HAVING distance < 25 ORDER BY distance LIMIT 0 , 20;*/
	
	while($customers->next())
	{
		// $locations = $db->query("SELECT * FROM addresses_coord WHERE customer_id='".$customers->f('customer_id')."'");
		if($customers->f('coord_id'))
		{
			// $distance = $customers->f('location_lat'),$customers->f('location_lng'))*1000);

			// var_dump($customers->f('location_lat'),$customers->f('location_lng'),$lat+2,$long+2,$customers->f('location_lat') < $lat+2 && $customers->f('location_lat') > $lat-2 && $customers->f('location_lng') < $long+2 && $customers->f('location_lng') > $long -2 );
			// var_dump($customers->f('customer_id'), $distance,$range);
			if($customers->f('location_lat') < $lat+2 && $customers->f('location_lat') > $lat-2 && $customers->f('location_lng') < $long+2 && $customers->f('location_lng') > $long -2 )
			// $distance = round(calculate_distance($lat,$long,$customers->f('location_lat'),$customers->f('location_lng'))*1000);
			// if($distance<$range)
			{
				$result['list'][] = array(
					'lat'		=> $customers->f('location_lat'),
					'lng'		=> $customers->f('location_lng'),
					'name'		=> $customers->f('name'),
					'distance'	=> number_format($customers->f('distance'),2)
					// 'pos'				=> $i,
				);
				// $view->loop('known_markers');
				$i++;
			}
		}else
		{
			//$dest = address, zip city, country;
			if(!$customers->f('zip'))
			{
				$dest = $customers->f('address').', '.$customers->f('city').', '.$country_n;
			}elseif(!$customers->f('city'))
			{
				$dest = $customers->f('address').', '.$customers->f('zip').', '.$country_n;
			}else
			{
				$dest = $customers->f('address').', '.$customers->f('zip').' '.$customers->f('city').', '.$country_n;
			}
			$array = array('name'=> htmlentities(preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$customers->f('name'))), 'value' => htmlentities(preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$dest)), 'customer_id' => $customers->f('cust_id'));
			array_push($data, $array);
		}
	}
}else
{
	$found_address=false;
}
// if(!$in['city'] || !$in['country_id'] || !$in['range'])
// {
// 	$enter_addr = false;
// }
$view->assign(array(
	// 'center_lat'	=> $lat,
	// 'center_long'	=> $long,
	// 'address'		=> $address,
	'data'			=> json_encode($data),
	// 'range'			=> $range,
	// 'found_addr'	=> $found_address,
	// 'entered_addr'	=> $enter_addr,

));

// $view->assign(array(
// 	'do_next'	=> 'company-map_location-customer-range_select',
// 	'country_dd'				=> build_country_list($in['country_id']),
// 	'range_dd'					=> build_range_dd($in['range']),
// 	'account_manage_select' 	=> !empty($in['acc_manager_name']) ? build_acc_manager($in['acc_manager_name']) : build_acc_manager(),
// 	'l_source_dd'				=> !empty($in['lead_source']) ?  build_l_source_dd($in['lead_source']) : build_l_source_dd(),
// 	'sector_dd'					=> !empty($in['sector']) ? build_s_type_dd($in['sector']) : build_s_type_dd(),
// 	'c_type_name_dd'			=> !empty($in['c_type_name']) ? build_c_type_name_dd($in['c_type_name']) : build_c_type_name_dd(),
// ));
// global $main_menu_item;
// global $sub_menu_item;
// $main_menu_item='crm';
// $sub_menu_item='customers_list';

// return $view->fetch();

json_out($result);