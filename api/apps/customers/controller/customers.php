<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);

unset($_SESSION['articles_id']);

$output = array();	
$order_by_array = array('name','city_name','country_name','c_type_name','acc_manager_name','serial_number_customer');
$arguments_s = '';
$arguments='';
$arguments_o='';
$arguments_a='';
$l_r =ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

if($in['active']==1){
	$filter.= " AND active=1";
}

$filter = " is_admin='0' AND front_register=0 ";
if(empty($in['archived'])){
	$filter.= " AND active=1";
}else{
	$in['archived'] = 1;
	$filter.= " AND active=0";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}
// if we have a regular user and ONLY_IF_ACC_MANAG == 1, then we show to the user only the
// companies where he is account manager
$admin_licence = $db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
	$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}

//FILTER LIST
$filter_c=' ';
$order_by = " ORDER BY name ";
$ad_search_f = '';
$escape_chars = array('(','?','*','+',')');
if(!empty($in['search'])){
	$filter_x = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['search']));
	foreach ($search_arr as $value) {
		if(in_array($value, $escape_chars)){
			$value = '\\'.$value;
		}
		$filter_x .= $value."[^a-zA-Z0-9]?";
	}
	$filter .= " AND ( name regexp '".addslashes($filter_x)."' OR city_name LIKE '%".$in['search']."%' OR comp_phone LIKE '%".$in['search']."%' OR c_email LIKE '%".$in['search']."%' OR firstname LIKE '%".$in['search']."%' ) ";
	// $filter .= " AND ( name LIKE '%".$in['search']."%' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['acc_manager_name'])){
	$filter .= " AND ( acc_manager_name LIKE '%".$in['acc_manager_name']."%' ) ";
	$arguments_s.="&acc_manager_name=".$in['acc_manager_name'];
}elseif(!empty($in['acc'])){
	$filter .= " AND user_id='0' ";
	$arguments_s.="&acc=".$in['acc'];
}
if(!empty($in['zip_from']) && empty($in['zip_to'])){
	$zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
	$filter .= " AND ( zip_name LIKE '%".$zip_from."%' ) ";
	$arguments_s.="&zip_from=".$in['zip_from'];
}
if(!empty($in['zip_to'])){
	$zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
	$zip_to = preg_replace('/[^0-9]/', '', $in['zip_to']);
	if(empty($zip_from)){
		$zip_from = '0';
	}
	$filter .= " AND ( STRIP_NON_DIGIT(zip_name) BETWEEN '".$zip_from."' AND '".$zip_to."' ) ";
	$arguments_s.="&zip_to=".$zip_to."&zip_from=".$zip_from;
}
if(!empty($in['country_id'])){
	$filter .= " AND country_name = '".$in['country_id']."' ";
	$arguments_s.="&country_id=".$in['country_id'];
}
if(!empty($in['our_reference'])){
	$filter .= " AND ( our_reference LIKE '%".$in['our_reference']."%' ) ";
	$arguments_s.="&our_reference=".$in['our_reference'];
}
if(!empty($in['btw_nr'])){

	$escape_chars2 = array('.',' ','/',',');
	$filter_y = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['btw_nr']));
	
	foreach ($search_arr as $value) {
		if(!in_array($value, $escape_chars2)){
			$filter_y .= $value."\.?";	
		}
		
	}
	// console::log($filter_y);
	$filter .= " AND ( btw_nr regexp '".addslashes($filter_y)."' OR btw_nr='".$in['btw_nr']."') ";
	$arguments_s.="&btw_nr=".$in['btw_nr'];
}
if(!empty($in['commercial_name'])){
	$filter_x = '';
	$search_arr = str_split(stripslashes($in['commercial_name']));
	foreach ($search_arr as $value) {
		if($value == '('){
			$value = '\\(';
		}
		$filter_x .= $value.".?";
	}
	$filter .= " AND ( commercial_name regexp '".addslashes($filter_x)."' ) ";
	$arguments_s.="&commercial_name=".$in['commercial_name'];
}

if(!empty($in['lead_source'])){
	$filter .= " AND lead_source='".$in['lead_source']."' ";
	$arguments_s.="&lead_source=".$in['lead_source'];
}

if(!empty($in['sector'])){
	$filter .= " AND sector='".$in['sector']."' ";
	$arguments_s.="&sector=".$in['sector'];
}


if (!empty($in['c_type_name'])){
	$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
	$arguments.="&c_type_name=".$in['c_type_name'];
}
if (isset($in['type'])){
	// if(!$in['type']){
	// 	$in['type'] ='0';
	// }
	switch ($in['type']) {
		case '1':
			$filter .= " AND customers.type='".$in['type']."'  ";
	        $arguments.="&type=".$in['type'];
			break;
		case '2':
		    $filter .= " AND customers.type='0'  ";
	        $arguments.="&type=".$in['type'];
		    break;
	}
	
}
if (isset($in['is_supplier'])){
	switch ($in['is_supplier']) {
		case '1':
			$filter .= " AND customers.is_supplier='".$in['is_supplier']."'  ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		case '2':
			$filter .= " AND customers.is_customer='1'  ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		case '3':
			$filter .= " AND customers.is_supplier='1' AND customers.is_customer='1' ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		case '4':
			$filter .= " AND customers.is_supplier='' AND customers.is_customer='' ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		default:
			# code...
			break;
	}
	
}

if(!empty($in['order_by'])){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];				
	}
}
$selected =array();
$db->query("SELECT DISTINCT first_letter FROM customers WHERE ".$filter." ORDER BY name");
while($db->move_next()){
	array_push($selected, $db->f('first_letter'));
}

$arguments = $arguments.$arguments_s;

$output['rows'] = (int)$db->field("SELECT count(customer_id) FROM customers WHERE ".$filter );
$customer_logs = array();
$c_logs = $db->query("SELECT id, customer_id FROM logging_tracked WHERE seen='0' AND user_id='".$_SESSION['u_id']."' GROUP BY customer_id ")->getAll();

foreach ($c_logs as $key => $value) {
	$customer_logs[$value['id']] = $value['customer_id'];
}

// $output['lead_source_dd']	= build_l_source_dd();
// $output['sector_dd']		= build_s_type_dd();
// $output['type_dd']		= array( array('id'=>'0', 'value'=>gm('Company')), array('id'=>'1', 'value'=>gm('Individual')), array('id'=>'2', 'value'=>gm('Customer')), array('id'=>'3', 'value'=>gm('Supplier')) );
// $output['account_managers']	= build_acc_manager();
// $output['c_type_dd']		= build_c_type_name_dd();
// $output['countries'] 		= company_country_list();
$output['offset'] = $in['offset'];
$output['list'] = array();
$c = $db->query("SELECT is_supplier,is_customer,type,name,customer_id, country_name,active as ac, city_name, c_type_name, acc_manager_name, first_letter, serial_number_customer,firstname,comp_phone,c_email
			FROM customers
			WHERE ".$filter." GROUP BY customers.customer_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
foreach ($c as $key => $value) {
	
	if($value['is_supplier']==1 && $value['is_customer']==1) {
		$class='akti-icon akti-icon-16 o_akti_custopplier';
		$class_name = gm('Customer').' & '.gm('Supplier');
	}elseif($value['is_customer']==0 && $value['is_supplier']==0){
		$class='';
		$class_name='';
	}elseif($value['is_supplier']==1 && $value['is_customer']==0){
		$class='akti-icon akti-icon-16 o_akti_delivery';
		$class_name = gm('Supplier');
	}elseif($value['is_customer']==1 && $value['is_supplier']==0){
		$class='akti-icon akti-icon-16 o_akti_customer';
		$class_name = gm('Customer');
	}



	$output['list'][]=array(
		'name'						=> $value['name'],
		'firstname'					=> $value['firstname'],
		// 'type'						=> $class,
		'type'						=> $class_name,
		// 'type_name'					=> $class_name,
		// 'type2'						=> $value['type']==1 ? 'users' : 'building',
		'type2'						=> $value['type']==1 ? gm('Individual') : gm('Company'),
		// 'type2_name'				=> $value['type']==1 ? gm('Individual') : gm('Company'),
		// 'serial_number_customer'	=> $value['serial_number_customer'],
		'country'					=> $value['country_name'] ? $value['country_name']: '&nbsp;',
		'city'						=> $value['city_name'] ? $value['city_name'] : '&nbsp;' ,
		'customer_id'				=> $value['customer_id'],
		// 'sales'						=> $value['c_type_name'] ? $value['c_type_name'] : '&nbsp;' ,
		// 'contacts'					=> get_contact_nr($value['customer_id']),
		'account_manager'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '&nbsp;',
		'phone'						=> $value['comp_phone'],
		'email'						=> $value['c_email'],
		// 'is_active'     			=> $value['ac']? true : false,
		// 'new_event'					=> $is_new_event,
		// 'edit_link'					=> 'index.php?do=company-customer&customer_id='.$value['customer_id'],
		// 'time_link'					=> 'index.php?do=company-customer_timelineNew&customer_id='.$value['customer_id'],
		// 'delete_link'				=> array('do'=>'customers-customer-customer-delete', 'id'=>$value['customer_id']),
		// 'archive_link'				=> array('do'=>'customers-customers-customers-archive', 'customer_id'=>$value['customer_id']),
		// 'activate_link'				=> array('do'=>'customers-customers-customers-activate', 'customer_id'=>$value['customer_id']),
	);
}

// $output['export_args'] = ltrim($arguments,'&');
$output['rows_per_page']=(int)$l_r;
json_out($output);