<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$page = ark::run();

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	echo $page;
	exit();
}

foreach ($lang as $key => $val){
	$lang[$key] = utf8_encode($val);
}
$langjs = 'var LANG='.json_encode($lang);
// if we have a layout
if(ark::$layout){

	$main = new at(ark::$layout);
	$main->assign('page', $page);
	// $main->assign('ctrljs', '<script src="js/controllers/'.ark::$app.'_ctrl.js"></script>');
	// to change this in the future
	$main->assign('basehref',$config['site_url']);
	$main->assign('metatitle',$config['meta_title']);
	$main->assign('metakeywords',$config['meta_keywords']);
	$main->assign('metadescription',$config['meta_description']);
	$main->assign('langjs',$langjs);
	$main->out();
} else {
	echo $page;
}
