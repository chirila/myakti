<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Console
 *
 * It's a small tool to provide debugging related information to developers without needing
 * them to add a lot of programmatic overhead to their code. Originally inspired by Particletree's
 * PHP Quick Profiler (http://particletree.com/features/php-quick-profiler/)
 *
 * @package DooHoo
 * @subpackage Core
 * @author Bodi Zsolt <zsolt@medeeaweb.com>
 */
if(!class_exists('console')){
class console{

	/**
	 * Singleton instance
	 *
	 * @var Console
	 */
	private static $_instance;

	/**
	 * Array of log entries
	 *
	 * @var array
	 */
	protected $_logs = array();

	/**
	 * Number of log entries
	 *
	 * @var array
	 */
	protected $_logCount = 0;

	/**
	 * Array of formated data which will be sent to the template class
	 *
	 * @var array
	 */
	protected $_output = array();

	/**
	 * Should the debugger be open when it's rendered?
	 *
	 * @var bool
	 */
	protected $_open = 2;


	private $_headline = '';

	private $_benchmark = array();
	/**
	 * Start time used as a base of reference for speed calculations
	 *
	 * @var int
	 */
	private $_startTime;

	/**
	 * Prevent "cloning" of this class
	 *
	 */
	private function __clone(){}

	/**
	 * Constructor
	 *
	 */
	public function __construct(){}

	public static function start(){
		$me = self::getInstance();
		$me->_startTime = $me->getMicroTime();
	}

	public static function headline($do = ''){
		$me = self::getInstance();
		$me->_headline = !empty($do) ? 'do= '. $do : '';
	}

	/**
	 * Returns the current instance
	 *
	 * @return Console
	 */
	public static function getInstance(){
		if(! (self::$_instance instanceof self)){
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Use the log method to log any information you are interested in.
	 *
	 *
	 * @example Console::log("hello");
	 * @example Console::log(array('key'=>'value'));
	 * @param mixed $data
	 * @param bool $imediate Should echo the data?
	 */
	public static function log(){
		$x = debug_backtrace(true);
		$data = func_get_args();
		if(!$data){
			return;
		}
		$me = self::getInstance();
		foreach ($data as $key) {
			$logItem = array(
				'data' => $data,
				'type' => 'log',
				'controller'=> $x['0']['file'].':'.$x['0']['line'],
			);
		}
		$me->_logs[] = $logItem;
		$me->_logCount++;
		if($imediate){
			echo '<pre>',htmlentities(print_r($data, true)),'</pre>';
		}
	}

	public static function speed($name = 'Point in Time'){
		$me = self::getInstance();
		$logItem = array(
			"data" => $me->getMicroTime(),
			"type" => 'speed',
			"name" => $name
		);
		$me->_logs[] = $logItem;
		$me->_logCount++;
	}

	public static function benchmark($name = 'Benchmark'){
		$me = self::getInstance();
		$me->_benchmark[$name] = array('name' => $name,'time' => $me->getMicroTime());
	}

	public static function end($name = ''){
		$me = self::getInstance();
		if(empty($name)){
			$name = end($me->_benchmark);
			$name = $name['name'];
		}

		if(!isset($me->_benchmark[$name])){
			$me->benchmark($name);
		}

		$logItem = array(
			"data" => ($me->getMicroTime() - $me->_benchmark[$name]['time'])*1000,
			"type" => 'benchmark',
			"name" => $name
		);
		$me->_logs[] = $logItem;
		$me->_logCount++;
		unset($me->_benchmark[$name]);
	}

	/**
	 * Use this method to log any errors that the sistem has encountered but which are not fatal errors.
	 * You can optionally pass in the file and line number using the PHP constants __FILE__ and __LINE__
	 * to make debugging easier.
	 *
	 * @example Console:error(array('message'=>'Wrong param count','file'=>__FILE__,'line'=>__LINE__));
	 * @param array $data
	 */
	public static function error($data){
		if(!$data){
			return;
		}

		if(!is_array($data)){
	        $data = array('message' => $data);
	    }

        if(!array_key_exists('message', $data)){
        	$data = array('message' => $data);
        }

		$me = self::getInstance();
		$logItem = array(
			"data" => $data['message'],
			"type" => 'error',
			"file" => isset($data['file']) ? $data['file'] : '',
			"line" => isset($data['line']) ? $data['line'] : '',
		);

		$me->_logs[] = $logItem;
		$me->_logCount++;
	}

	/**
	 * Use this method see how much memory a given object takes up
	 *
	 * @param mixed $object
	 * @param string $name
	 */
	public static function memory($object = false, $name = 'PHP',$output = false) {
		$me = self::getInstance();
		if(function_exists('memory_get_usage')){
			$memory = memory_get_usage();
		}
		if($object) $memory = strlen(serialize($object));
		#if you want the size
		if($output === true){
			return $me->getReadableFileSize($memory);
		}
		$logItem = array(
			"data" => $memory,
			"type" => 'memory',
			"name" => $name,
			"dataType" => gettype($object)
		);

		$me->_logs[] = $logItem;
		$me->_logCount++;
	}

	/**
	 * Opens the debugger when it's rendered
	 */
	public static function show($maximize = false){
		$me = self::getInstance();
		$me->_open = $maximize ? 2 : 1;
	}

	/**
	 * Closes the debugger when it's rendered
	 */
	public static function close(){
		$me = self::getInstance();
		$me->_open = false;
	}

	/**
	 * Internal method used for logging the enviroment variables the sistem is running in.
	 *
	 * @param mixed $data
	 */
	protected function logEnviroment($data,$log = 'log'){
		unset($data['innerHTML']);
		$logItem = array(
			'data' => $data,
			'type' => $log
		);
		array_unshift($this->_logs,$logItem);
		$this->_logCount++;
	}

	/**
	 * Internal method which parses the logged information and formats it for output
	 */
	protected function parseLogData(){
		$logs = $this->_logs;
		$counters = array('log'=>0,'error'=>0,'memory'=>0,'speed' => 0);
		if(!empty($logs)){
			foreach($logs as $key => $log){
				switch ($log['type']){
					case 'in':
						ksort($log['data']);
					case 'session':
						ksort($log['data']);
					case 'data':
					case 'log':
						$logs[$key]['data'] = var_export($log['data'], true);
						if(isset($log['controller'])){
							$logs[$key]['controller'] = $log['controller'];
						}
						$counters['log']++;
						break;
					case 'error':
						$logs[$key]['data'] = var_export($log['data'], true);
						$counters['error']++;
						break;
					case 'memory':
						$counters['memory']++;
						$logs[$key]['data'] = $this->getReadableFileSize($log['data']);
						break;
					case 'speed':
						$counters['speed']++;
						$logs[$key]['data'] = $this->getReadableTime(($log['data'] - $this->_startTime)*1000);
						$logs[$key]['speed'] = true;
						break;
					case 'benchmark':
						$counters['speed']++;
						$logs[$key]['type'] = 'speed';
						$logs[$key]['data'] = $this->getReadableTime($log['data']);
						$logs[$key]['speed'] = true;
						break;
				}
			}
		}
		$this->_output['logs'] = $logs;
		//normalize the output array so that they all have the same keys
		foreach ($this->_output['logs'] as $key => $log){
			foreach (array('data','type','name','dataType','file','line','speed') as $v){
				if(isset($this->_output['logs'][$key][$v])){
					continue;
				}
				$this->_output['logs'][$key][$v] = '';
			}
		}
		$this->_output['counters'] = array($counters);
		$this->_output['logsCount'] = $this->_logCount;
	}

	/**
	 * Gathers a list of files that have been included.
	 */
	public function gatherFileData(){
		$files = get_included_files();
        $fileList = array();
		$fileTotals = array(
			"size" => 0,
			"largest" => 0,
		);
		$fileCount = 0;
		foreach($files as $key => $file) {
			$fileCount++;
			$size = filesize($file);
			$fileList[] = array(
					'name' => $file,
					'size' => $this->getReadableFileSize($size)
				);
			$fileTotals['size'] += $size;
			if($size > $fileTotals['largest']) $fileTotals['largest'] = $size;
		}

		$fileTotals['size'] = $this->getReadableFileSize($fileTotals['size']);
		$fileTotals['largest'] = $this->getReadableFileSize($fileTotals['largest']);
		$this->_output['files'] = $fileList;
		$this->_output['fileTotalsSize'] = $fileTotals['size'];
		$this->_output['fileTotalsLargest'] = $fileTotals['largest'];
		$this->_output['fileCount'] = $fileCount;
	}

	/*--------------------------------------------------------
	     QUERY DATA -- DATABASE OBJECT WITH LOGGING REQUIRED
	----------------------------------------------------------*/

	/**
	 * Gathers query information and formats it for output
	 */
	public function gatherQueryData(){

		$queryTotals = array();
		$queryCount = 0;
		$queryTotals['time'] = 0;
		$queryTotals['duplicates'] = 0;
		$queries = array();
		$duplicates = array();

		$executedQueries = database_store::getInstance()->getQueries();

		$queryCount = count($executedQueries);
		foreach($executedQueries as $key => $query) {
			array_push($duplicates, $query['sql']);
			$queryTotals['time'] += $query['time'];
			$query['timeFloat'] = $query['time'];
			$query['time'] = $this->getReadableTime($query['time']);
			$queries[] = $query;
		}
		$queryTotals['duplicates'] = $queryCount - count(array_unique($duplicates));
		$queryTotals['timeFloat'] = $queryTotals['time'];
		$queryTotals['time'] = $this->getReadableTime($queryTotals['time']);
		$this->_output['queries'] = $queries;
		$this->_output['queryTotalsTime'] = $queryTotals['time'];
		$this->_output['queryTotalsTimeFloat'] = $queryTotals['timeFloat'];
		$this->_output['queryTotalsDuplicates'] = $queryTotals['duplicates'];
		$this->_output['queryCount'] = $queryCount;
	}

	/*--------------------------------------------------------
	     CALL SQL EXPLAIN ON THE QUERY TO FIND MORE INFO
	----------------------------------------------------------*/
	/**
	 * Attempts to explain a query by running EXPLAIN on it and formatting the returned results.
	 *
	 * @param array $query
	 * @return array
	 */
	public function attemptToExplainQuery($q) {
		$query = array();
		$db = new sqldb();
		$rs = $db->explain($q);
		$query['sql'] = nl2br($q);
		if($rs === false){
			return $query;
		}
		while ($rs->next()) {
			$query['explain'][] = $rs->getAll();
		}
		return $query;
	}

    public function gatherActionData(){
        $actions = ark::$dolist;
        $times = ark::$dotime;
        $entries = array();
        $totalTime = 0;
        foreach($actions as $index => $entry){
            $entry['time'] = $this->getReadableTime($times[$entry['controller']]);
            $totalTime += $times[$entry['controller']];
            array_push($entries, $entry);
        }
        $this->_output['actions'] = $entries;
        $this->_output['actionsCount'] = count($entries);
        $this->_output['actionsTotalTimeFloat'] = $totalTime;
        $this->_output['actionsTotalTime'] = $this->getReadableTime($totalTime);
        unset($actions,$times,$entries, $totalTime);
    }


	/**
	 * Converts file sizes
	 *
	 * @param int $size
	 * @param int $retstring
	 * @return string
	 */
	public function getReadableFileSize($size, $retstring = null) {
        	// adapted from code at http://aidanlister.com/repos/v/function.size_readable.php
	       $sizes = array('bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

	       if ($retstring === null) { $retstring = '%01.2f %s'; }

		$lastsizestring = end($sizes);

		foreach ($sizes as $sizestring) {
	       	if ($size < 1024) { break; }
	           if ($sizestring != $lastsizestring) { $size /= 1024; }
	       }
	       if ($sizestring == $sizes[0]) { $retstring = '%01d %s'; } // Bytes aren't normally fractional
	       return sprintf($retstring, $size, $sizestring);
	}

	public static function getMicroTime(){
		$time = microtime();
		$time = explode(' ', $time);
		return $time[1] + $time[0];
	}

   public static function getEndMicroTime($start){
       return (self::getMicroTime() - $start)*1000;
   }

	/**
	 * Formats microtimes into a human readable format
	 *
	 * @param int $time
	 * @return string
	 */
	public function getReadableTime($time) {
		$ret = $time;
		$formatter = 0;
		$formats = array('ms', 's', 'm');
		if($time >= 1000 && $time < 60000) {
			$formatter = 1;
			$ret = ($time / 1000);
		}
		if($time >= 60000) {
			$formatter = 2;
			$ret = ($time / 1000) / 60;
		}
		$ret = number_format($ret,3,'.','') . ' ' . $formats[$formatter];
		return $ret;
	}

	/**
	 * Renders the debugger console
	 *
	 * @param dhEvent $e
	 */
	public function render(){
	    global $in;

	    // $this->gatherFileData();
        $this->gatherQueryData();
        // if(isset($_SESSION['u_id'])){
            $this->logEnviroment($_SESSION,'session');
        // }

        if(!empty($in)){
            $this->logEnviroment($in, 'in');
        }

        $this->gatherActionData();

        $this->parseLogData();
        switch ($this->_open){
            case 1:
                $this->_output['open'] = '';
                break;
            case 2:
            $this->_output['open'] = 'maximizeDHBUG';
                break;
            default:
                $this->_output['open'] = 'smallDHBUG';
        }
        // $this->_output['path'] = INSTALLPATH;
		// $this->_output['headline'] = $this->_headline;
		return $this->_output;

        //load in the template parser and exect
        include_once(INSTALLPATH.'core/library/template.php');
        $debugger = new dhTemplate();

        $debugger->setDebug(true);
        return $debugger->parse(INSTALLPATH.'debug/debugger.html',$this->_output);
	}
}
}
/* End of file console.php */
