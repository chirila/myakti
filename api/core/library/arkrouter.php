<?php

class arkRouter {
	// to do - accept multiple request types for the same route

	protected $routes = array();
	protected $base = '';


	/**
	  * Create router in one call from config.
	  *
	  * @param array $routes
	  * @param string $base - useful when the application is installed in a subdirectory
	  */
	public function __construct($routes = array(), $base = '') {
		if($base != ''){
			$this->setbase($base);
		}

		if(!empty($routes)){
			$this->addRoutes($routes);
		}
	}

	/**
	 * Retrieves all routes.
	 * Useful if you want to process or display existing routes.
	 * @return array All routes.
	 */
	public function getRoutes() {
		return $this->routes;
	}

	/**
	 * Add multiple routes at once from array in the following format:
	 *
	 *   $routes = array(
	 *      'name' => array(
	 *			'method' => "GET",
				'do' => 'app-controller-model-method',
				'params' => 'cat_id/url'
				),
	 *   );
	 *
	 * @param array $routes
	 * @return void
	 * @author Koen Punt
	 */
	public function addRoutes($routes){
		if(!is_array($routes)) {
			return false;
		}

		foreach($routes as $name => $route) {
			if(array_key_exists('do', $route)){
				$this->add($name, $route['method'], $route['do'], $route['params']);
			}else{
				$this->addRoutes($routes[$name]);
			}
		}
		return true;
	}

	/**
	 * Set the base path.
	 * Useful if you are running your application from a subdirectory.
	 */
	public function setbase($base) {
		$this->base = $base;
	}


	/**
	 * add a route to the list
	 *
	 * @param string $name The first segment in a URL, that will be asociated with 'do'
	 * @param string $method One of 5 HTTP Methods, or a pipe-separated list of multiple HTTP Methods (GET|POST|PATCH|PUT|DELETE)
	 * @param string $do The target app-controlled-model-method where this route should point to
	 * @param string $params the variable names aspected in the controller
	 */
	public function add($name, $method, $do, $params) {
		if(isset($this->routes[$name])){
			return false;
		}
		$this->routes[$name] = array(
			'method'=> $method,
			'do' => $do,
			'params' => $params
		);
		return 'true';
	}

	/**
	 * Transfers the requested URL into 'in' values
	 * @param string $requestUrl
	 * @param string $requestMethod
	 */

	public function run($requestUrl = null, $requestMethod = null) {
		global $in;

		// set Request Url if it isn't passed as parameter
		if($requestUrl === null) {
			$requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
		}

		// strip base path from request url
		$requestUrl = substr($requestUrl, strlen($this->base));

		// Strip query strings from Request Url
		if (($strpos = strpos($requestUrl, '?')) !== false) {
			$requestUrl = substr($requestUrl, 0, $strpos);
		}
		if (($strpos = strpos($requestUrl, '&')) !== false) {
			$requestUrl = substr($requestUrl, 0, $strpos);
		}

		// set Request Method if it isn't passed as a parameter
		if($requestMethod === null) {
			$requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
		}
		if($requestMethod == "OPTIONS"){ # for CORS
			http_response_code(200);
			header('Allow: GET,POST,PUT,DELETE,OPTIONS');
			header('Access-Control-Allow-Headers: token');
			json_out();
		}
		
		// Force request_order to be GET, POST
		// $_REQUEST = array_merge($_GET, $_POST);
		$request = array_values(array_filter(explode('/', $requestUrl)));
		$parsedRequest = $this->checkRoute($request);
		
		if(empty($request) ){
			// no route
			http_response_code(400);
    		json_out();
		}elseif ( !$parsedRequest ) {
			// no route is matching the request
			http_response_code(404);
    		json_out();
			return false;
		} else {
			$route = $this->routes[$parsedRequest[0]];
			
			// check the methos to see if its accepted
			if(is_array($route['method'])){
				if( !in_array($requestMethod, $route['method']) ){
					http_response_code(405);
	    			json_out();
	    		}
			}else{
				if( $route['method'] != $requestMethod ){ 
					http_response_code(405);
	    			json_out();
				}
			}
			
			$in['do'] = $route['do'];

			//now that we have mapped the 'do', we map the segments # do we use this ? O:o 
			/*foreach($parsedRequest as $key => $value){
				$in['segments'][$key] = $value;
			}*/
			$params = array_values(array_filter(explode('/', $route['params'])));

		}
		
		if(is_array($params)){
			foreach($params as $key=>$value){
				$in[$value] = $parsedRequest[($key+1)];
			}
		}
		file_put_contents('cucu.txt', 'am reusit: '.print_r($in,true));
		return true;
	}

	/**
	 * Tries to match a route
	 * @param array $request
	 */

	private function checkRoute($request){
		if(!is_array($request)) {
			return false;
		}
		if( !array_key_exists($request[0], $this->routes) ){
			if(count($request) > 1 ){
				$newRequest = array($request[0] . '/' . $request[1]); // merge the first two key;
				$oldRequest = array_slice($request, 2);  // get the rest of the request array except the first two keys
				$request = array_merge($newRequest,$oldRequest); // merged the two arrays
				return $this->checkRoute($request);
			}else{
				return false;
			}
		}else{
			return $request;
		}
	}

}
