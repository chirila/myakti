<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
$data = explode(',', $in['data']);
foreach ($data as $value) {
	$func = 'get_'.$value;
	if(function_exists($func)){
		$result[$value] = $func();
	}
}

json_out($result);

function get_country_dd($in){
    return build_country_list();
}

function get_user_dd($in){
    return getAccountManager();
}

function get_sales_dd($in){
    return getSales();
}

function get_c_type_dd($in){
    return getCustomerType();
}

function get_lead_source_dd($in){
    return build_l_source_dd();
}

function get_legal_type_dd($in){
    return build_l_type_dd();
}

function get_sector_dd($in){
    return build_s_type_dd();
}

function get_activity_dd($in){
    return build_c_activity_dd();
}

function get_language_dd($in){
    return build_language_dd_new();
}

function get_contact_title_dd($in){
    return build_contact_title_type_dd();
}

function get_language_contact_dd($in){
    return build_language_dd();
}

function get_function_dd($in){
    return build_contact_function();
}

function get_department_dd(){
    return build_contact_dep_type_dd();
}

function get_contact_fields(){
    $db = new sqldb();
    $showFields = $db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'")->getAll();
    $showField = array();
    foreach ($showFields as $key => $value) {
        $showField[$value['field']] = $value['creation_value'] == '1' ? true : false;
    }
    return $showField;
}

function get_customer_fields(){
    $db = new sqldb();
    $showFields = $db->query("SELECT * FROM customise_field WHERE controller='company-customer'")->getAll();
    $showField = array();
    foreach ($showFields as $key => $value) {
        $showField[$value['field']] = $value['creation_value'] =='1' ?  true :  false;
    }
    return $showField;
}

function get_customer_extra_fields(){
    $db = new sqldb();
    $a = $db->query("SELECT field_id,label,is_dd FROM customer_fields ORDER BY field_id")->getAll();
    foreach ($a as $key => $value) {
        if($value['is_dd'] == '1'){
            $val = $db->query("SELECT id,name FROM customer_extrafield_dd  WHERE extra_field_id='".$value['field_id']."' ORDER BY sort_order ")->getAll();
            $a[$key]['dd_items'] = $val;
        }
    }
    return $a;
}

function get_contact_extra_fields(){
    $db = new sqldb();
    $a = $db->query("SELECT field_id,label,is_dd FROM contact_fields ORDER BY field_id")->getAll();
    return $a;
}