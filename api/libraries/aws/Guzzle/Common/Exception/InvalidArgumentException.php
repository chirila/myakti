<?php

namespace Guzzle\Common\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements GuzzleException {
	public $message;

	function __construct($foo) {
		$this->message = $foo;
	}
}
