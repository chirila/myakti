<?php
use \Dropbox as dbx;

class drop{

	private $db;
	public $is_connect = false;
	public $accountInfo;
	private $path;
	private $customer_folder;
	private $contact_folder=null;
	private $item_folder;
	private $dropbox; #dropbox object
	private $field_id = 'customer_id';
	private $table_name = 'customers';
	private $field_name = 'name';

	function __construct( $path = null, $id = null, $item_id=null, $check = true, $db = '', $is_contact = 0, $serial_number = null, $contact_id=null,$is_batch=null ) {
		//console::log($path, $id, $item_id, $check, $db, $is_contact, $serial_number, $contact_id,$is_batch);
		if($is_contact) {
			$this->field_id = 'contact_id';
			$this->table_name = 'customer_contacts';
			$this->field_name = "CONCAT_WS(' ',firstname, lastname)";
		}
		#this is for uploadify
		if($db){
			global $database_config;
			$database_up = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $db,
			);
			$this->db = new sqldb($database_up);
		}else{
			$this->db = new sqldb();
		}
		if(defined('DROPBOX') && DROPBOX != ''){

			$this->dropbox = new dbx\Client(DROPBOX, "PHP-Example/1.0");
			$this->accountInfo = $this->dropbox->getAccountInfo();
	
			if(!is_array($this->accountInfo)){
				console::error($this->accountInfo);
				msg::$error = gm('Please go through Dropbox instalation proccess again').'.';
				$this->is_connect = false;
			}else{
				$this->is_connect = true;
				$this->path = '/'.$path;
				$c = $this->isFolder( $this->path );
				if(empty($c)){
					$r = $this->dropbox->createFolder( $this->path );
				}
				$exists = $this->db->query("SELECT * FROM dropbox_folder WHERE cId='".$id."' AND folder='".addslashes($path)."' AND is_contact='".$is_contact."' ");
				if($exists->f('name')){
					$this->customer_folder = '/'.trim($exists->f('name'));
				}

				if($is_batch){ 

							$this->customer_folder = '/batches';
							if($item_id){
								$this->item_folder = '/'.$item_id;
									}
							 $c = $this->isFolder('batches');
								if(empty($c)){
									$r = $this->dropbox->createFolder($this->path.'/batches');
								}else{
									//$this->dropbox->delete($this->path.'/batches');
									$r = $this->dropbox->createFolder($this->path.$this->customer_folder.$this->item_folder);

								}

				}elseif($id && $path &&($path!='invoices')){
					if($check){
						$name = $this->db->field("SELECT ".$this->field_name." FROM ".$this->table_name." WHERE ".$this->field_id."='".$id."'  ");
						if(!$exists->f('id')) {
							$this->customer_folder = '/'.trim($name);
							$r = $this->dropbox->createFolder($this->path.$this->customer_folder);
							if(is_array($r)){
								$this->db->query("INSERT INTO dropbox_folder SET folder='".addslashes($path)."', cId='".$id."', name='".addslashes($name)."', is_contact='".$is_contact."' ");
							}
						}
						$c = $this->isFolder( $this->path.$this->customer_folder );
						if(empty($c)){
							$r = $this->dropbox->createFolder( $this->path.$this->customer_folder );
						}
					}
					if($item_id){
						$this->item_folder = '/'.$serial_number;
						if($check){
							switch ($path) {
								case 'quotes':
									$table = 'tblquote';
									$field = 'id';
									break;
								case 'projects':
									$table = 'projects';
									$field = 'project_id';
									break;
								case 'orders':
									$table = 'pim_orders';
									$field = 'order_id';
									break;
								case 'customers':
									$table = 'customers';
									$field = 'customer_id';
									break;
								case 'contracts':
									$table = 'contracts';
									$field = 'customer_id';
									break;
								default:
									$table = 'tblquote';
									$field = 'id';
									break;
							}
							$item_f = $this->db->field("SELECT dropbox_folder FROM ".$table." WHERE ".$field."='".$item_id."' ");
							if(!$item_f){
								// $c = $this->isFolder( $item_id, $this->path.$this->customer_folder );
								// if(empty($c)){
									$r = $this->dropbox->createFolder($this->path.$this->customer_folder.$this->item_folder);
									$this->db->field("UPDATE ".$table." SET dropbox_folder='1' WHERE ".$field."='".$item_id."' ");
								// }
							}
						}
					}
				}else{
						if($contact_id==='temp'){
						$c = $this->isFolder( 'temp' );
						if(empty($c)){
							$r = $this->dropbox->createFolder($this->path.'/temp');
						}else{
							$this->dropbox->delete($this->path.'/temp');
							$r = $this->dropbox->createFolder($this->path.'/temp');
						}

					}elseif(!$id&&isset($contact_id)){

						$this->db->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$contact_id."'");
						$this->contact_folder= $this->db->f('firstname').'_'.$this->db->f('lastname');
						$c = $this->isFolder( $this->path.'/'.$this->contact_folder);
						if(empty($c)){
							$r = $this->dropbox->createFolder($this->path.'/'.$this->contact_folder);
						}
					}elseif(isset($id)){
						$this->customer_folder=trim($this->db->field("SELECT name FROM customers WHERE customer_id='".$id."'"));
						$c = $this->isFolder( $this->path.'/'.$this->customer_folder );
						if(empty($c)){
							$r = $this->dropbox->createFolder($this->path.'/'.$this->customer_folder);
						}
					}


				}
				console::log('Connected to Dropbox');
			}
		}else{
			$this->is_connect = false;
		}
	}

	public function isFolder( $name, $path = '' ){
		if(!$name){
			console::log('Please provide a name');
			return null;
		}
		if($this->is_connect){
			$p = $path;
			if(!$p){
				$p = $this->path;
			}
			if($p=='/'){
				return "/";
			}
			$e = $this->dropbox->searchFileNames( $p , $name );
			return $e;
		}
		return $this->not_connected();
	}

	public function createFolder( $name ){
		if(!$name){
			console::log('Please provide a name');
			return null;
		}
		if($this->is_connect){
			$e = $this->dropbox->createFolder( '/' . $name );
			return $e;
		}
		return $this->not_connected();
	}

	private function not_connected(){
		console::log('Could not connect');
		return null;
	}

	public function getContent( $path = '' ){
		if($this->is_connect){
			$p = $path;
			if(!$p){
				$p = $this->path.$this->customer_folder.$this->item_folder;
			}
			$e = $this->dropbox->getMetadataWithChildren( $p );
			return $e;
		}
		return $this->not_connected();
	}
	public function getLink( $path = '' ){
		if($this->is_connect){
			$p = $path;
			if(!$p){
				$p = $this->path.$this->customer_folder.$this->item_folder;
			}
			$e = $this->dropbox->createShareableLink( $p );
			return $e;
		}
		return $this->not_connected();
	}

	public function deleteF($path){
		if(!$path){
			console::log('Please provide a path');
			return null;
		}
		if($this->is_connect){
			$e = $this->dropbox->delete( $path );
			return $e;
		}
		return $this->not_connected();
	}

	public function upload( $name, $file, $size ){
		if($this->is_connect){
			$p = $this->path.$this->customer_folder.$this->item_folder.'/'.$name;
			console::log($p);
			$f = fopen($file, "rb");
			$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
			console::log($e);
			fclose($f);
			return $e;
		}
		return $this->not_connected();
	}
	public function moveFromTemp( $from ){
		if($this->is_connect){
			// echo $from;
			// echo $this->path.'/'.$this->customer_folder;
			// exit();

			$name=explode('/', $from);
			if($name[1]=='upload_invoices'){
				$i=2;
			}else{
				$i=3;
			}
			// print_r($name);
			if($this->contact_folder){
				$e = $this->dropbox->move( $from, $this->path.'/'.$this->contact_folder.'/'.$name[$i] );
			}else{
				$e = $this->dropbox->move( $from, $this->path.'/'.$this->customer_folder.'/'.$name[$i] );
			}
			if(!$e){
				msg::$error_big = "Could not move the file. A file with the same name already exists!";
				return false;
			}
			return $e;
		}
		return $this->not_connected();
	}

	public function upload_invoice( $name, $file, $size, $temp='' ){
		if($this->is_connect){

			if($temp){
				$p = $this->path.'/temp/'.$name;
				$f = fopen($file, "rb");
				$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
				fclose($f);
			}else{
				if($this->contact_folder){
					$p = $this->path.'/'.$this->contact_folder.'/'.$name;
					$f = fopen($file, "rb");
					$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
					fclose($f);
				}else{
					$p = $this->path.'/'.$this->customer_folder.'/'.$name;
					$f = fopen($file, "rb");
					$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
					fclose($f);
				}
			}
			return $e;
		}
		return $this->not_connected();
	}

	public function getFile($path, $out){
		if(!$path){
			console::log('Please provide a path');
			return null;
		}
		if(!$out){
			console::log('Please provide a out');
			return null;
		}
		if($this->is_connect){
			$e = $this->dropbox->getFile( $path, $out );
			return $e;
		}
		return $this->not_connected();
	}


}

?>