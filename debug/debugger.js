(function(){
window.DHBUG = {
	'changeTab':function(newTabObj){
		var container = $('DHBUG');
		if(container.className == 'smallDHBUG'){			
			container.className = '';
		}		
		var srcParent = newTabObj.parentNode;
		 	if(hasClass(srcParent,'active')){
				srcParent = null;
				container = null;
				return false;	
			}		
		//we need to find the currently active element
		var tabs = $('DHBUG-tabs').getElementsByTagName('LI');
			for(var i = 0, tabsCount = tabs.length; i < tabsCount; i++){
				if(hasClass(tabs[i],'active')){
					removeClassName(tabs[i],'active');
					addClassName($(tabs[i].firstChild.rel),'dh-hide');
					break;
				}
			}
		addClassName(srcParent, 'active');
		removeClassName($(newTabObj.rel),'dh-hide');
		tabs = null;
		srcParent = null;
		container = null;
		return false;
	},
	'toggleDisplay':function(){
		var container = $('DHBUG');
		if(container.className == 'smallDHBUG'){
			container.className = '';//remove it dh-hide it
		}else{
			container.className = 'smallDHBUG';//we always toggle betwen smallDHBUG and nothing
		}
		container = null;
		return false;//prevent link from going anywhere :)	
	},
	'toggleSize':function(e){
		var container = $('DHBUG');
		if(container.className == 'maximizeDHBUG'){
			container.className = '';//remove it dh-hide it
		}else{
			container.className = 'maximizeDHBUG';
		}
		container = null;
		return false;//prevent link from going anywhere :)			
	},
	'close':function(){
		$('DHBUG').style.display = 'none';
		$('DHBUG_SPACER').style.display = 'none';
		return false;
	}
};
	//http://www.bigbold.com/snippets/posts/show/2630
	function addClassName(objElement, strClass, blnMayAlreadyExist){
	   if ( objElement.className ){
	      var arrList = objElement.className.split(' ');
	      if ( blnMayAlreadyExist ){
	         var strClassUpper = strClass.toUpperCase();
	         for ( var i = 0; i < arrList.length; i++ ){
	            if ( arrList[i].toUpperCase() == strClassUpper ){
	               arrList.splice(i, 1);
	               i--;
	             }
	           }
	      }
	      arrList[arrList.length] = strClass;
	      objElement.className = arrList.join(' ');
	   }
	   else{  
	      objElement.className = strClass;
	      }
	}

	//http://www.bigbold.com/snippets/posts/show/2630
	function removeClassName(objElement, strClass){
	   if ( objElement.className ){
	      var arrList = objElement.className.split(' ');
	      var strClassUpper = strClass.toUpperCase();
	      for ( var i = 0; i < arrList.length; i++ ){
	         if ( arrList[i].toUpperCase() == strClassUpper ){
	            arrList.splice(i, 1);
	            i--;
	         }
	      }
	      objElement.className = arrList.join(' ');
	   }
	}
	//http://snippets.dzone.com/posts/show/2630
	function hasClass(objElement, strClass){
	   // if there is a class
	   if(objElement.className){
		  // the classes are just a space separated list, so first get the list
		  var arrList = objElement.className.split(' ');
		  // get uppercase class for comparison purposes
		  var strClassUpper = strClass.toUpperCase();
		  // find all instances and remove them
		  for ( var i = 0; i < arrList.length; i++ ){
			 // if class found
			 if ( arrList[i].toUpperCase() == strClassUpper ){
				// we found it
				return true;
			 }
		  }
	   }
	   // if we got here then the class name is not there
	   return false;
	}
	
	function $() {
	  var elements = new Array();
	
	  for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
		  element = document.getElementById(element);
	
		if (arguments.length == 1) 
		  return element;
	
		elements.push(element);
	  }
	  return elements;
	} 
})();	