<?php
function json_out($out, $strict = false,$exit = true){

    if($_SESSION['remember_url'] && ark::$controller == 'isLoged'){
        unset($_SESSION['remember_url']);
    }
    // unset($_SESSION['articles_id']);
    global $in,$config;
    $json = new Services_JSON();
    if(is_array($out)){
        unset($out['do']);
    }
    $tmp['data'] = $out;
    if(!$strict){
        if($in != $out){
            $tmp['in'] = $in;
        }
        $tmp['error'] = msg::get_errors();
        $tmp['notice'] = msg::get_notices();
        $tmp['success'] = msg::get_success();
        ark::$dotime[ark::$controller] = console::getEndMicroTime(ark::$starttime[ark::$controller]);
        console::memory();
        console::speed('Load time');
        $tmp_console=console::getInstance()->render();
        doQueryLog($tmp_console);
        if($config['debug']){
            //ark::$dotime[ark::$controller] = console::getEndMicroTime(ark::$starttime[ark::$controller]);
            // var_dump(ark::$controller);            
            //console::memory();
            //console::speed('Load time');
            $tmp['console'] = $tmp_console;
        }
    }    

    if($exit===true){
        global $startTime;
        $tmp['timp'] = microtime(true)- $startTime;
        header('Content-Type: application/json');
        echo $json->encode($tmp);
        exit();
    }else{
        return $out;
    }
}

// for php version < 5.5
if( !function_exists( 'array_column' ) ){
    function array_column( array $input, $column_key, $index_key = null ) {

        $result = array();
        foreach( $input as $k => $v )
            $result[ $index_key ? $v[ $index_key ] : $k ] = $v[ $column_key ];

        return $result;
    }
}
function get_article_tax_type_dd($term = false)
{   
    $q = strtolower($term);

    $filter = '1=1';

    if($q!='pim_article_tax_type'){
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();

    $s = $db->query("SELECT * FROM pim_article_tax_type  ORDER BY sort_order ")->getAll();
    $users =array();
    foreach ($s as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>htmlspecialchars($value['name'])));
    }
    if($_SESSION['access_level'] == 1){
        if($term=='pim_article_tax_type'){
            
        }else{
            array_push($users,array('id'=>'0','name'=>''));
        }
    }
    return $users;
}
function build_article_tax_type_dd($selected = false){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, id FROM pim_article_tax_type ORDER BY sort_order");
    while($db->move_next()){
        $array[$db->f('id')] = $db->f('name');
    }
    return build_simple_dropdown($array,$selected);
}
function get_article_tax_application_type($selected)
{   
    $opt=array(
    0   => gm('Sales & Purchase Documents'),
    1   => gm('Sales Documents'),
    2   => gm('Purchase Documents')
    );
    $out_str=$opt[$selected];
    return $out_str;
}

function build_article_tax_application_type_dd($selected = false){

    $array = array(
        array('id' => "1" , 'name' => gm('Sales & Purchase Documents')),
        array('id' => "2" , 'name' => gm('Sales Documents')),
        array('id' => "3" , 'name' =>  gm('Purchase Documents')),
    );
    return $array;

}
function backupcrm($selected = false){
    $db = new sqldb();
    $array = array();
    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    //$tz=array('1'=> $file1, '2'=> $file2, '3'=> $file3);
    $tz=array(
        array('id' => "1" , 'name' => $file1),
        array('id' => "2" , 'name' => $file2),
        array('id' => "3" , 'name' => $file3),

    );
    return $tz;
}
function backupcrm2($selected = false){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='3'");
    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);
    if(!$backup1){
        $date1  = '';
    }
    if(!$backup2){
        $date2  = '';
    }
    if(!$backup3){
        $date3  = '';
    }
    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array(
        array('id' => "1" , 'name' => $file1.'    '.$date1),
        array('id' => "2" , 'name' => $file2.'    '.$date2),
        array('id' => "3" , 'name' => $file3.'    '.$date3),

    );

    return $tz;
}
function backupcrm3($selected = false,$is_restore=false){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='3'");
    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='Backup 1';
    $file2 ='Backup 2';
    $file3 ='Backup 3';
    $tz=array();

    if(!$backup1){
        $date1  = 'No backup created yet';
    }
    if(!$is_restore || ($is_restore && $backup1)){
        array_push($tz, array('id' => "1" , 'name' => $file1.'  -  '.$date1));
    }

    if(!$backup2){
        $date2  = 'No backup created yet';
    }
    if(!$is_restore || ($is_restore && $backup2)){
        array_push($tz, array('id' => "2" , 'name' => $file2.'  -  '.$date2));
    }

    if(!$backup3){
        $date3  = 'No backup created yet';
    }
    if(!$is_restore || ($is_restore && $backup3)){
        array_push($tz, array('id' => "3" , 'name' => $file3.'  -  '.$date3));
    }
    return $tz;
}
function backuparticle2($selected = false){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='3'");
    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);
    if(!$backup1){
        $date1  = '';
    }
    if(!$backup2){
        $date2  = '';
    }
    if(!$backup3){
        $date3  = '';
    }
    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array(
        array('id' => "1" , 'name' => $file1.'    '.$date1),
        array('id' => "2" , 'name' => $file2.'    '.$date2),
        array('id' => "3" , 'name' => $file3.'    '.$date3),

    );
    return $tz;
}
function backuparticle3($selected = false,$is_restore=false){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='3'");
    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='Backup 1';
    $file2 ='Backup 2';
    $file3 ='Backup 3';

    $tz=array();

    if(!$backup1){
        $date1  = 'No backup created yet';
    }
    if(!$is_restore || ($is_restore && $backup1)){
        array_push($tz, array('id' => "1" , 'name' => $file1.'  -  '.$date1));
    }

    if(!$backup2){
        $date2  = 'No backup created yet';
    }
    if(!$is_restore || ($is_restore && $backup2)){
        array_push($tz, array('id' => "2" , 'name' => $file2.'  -  '.$date2));
    }

    if(!$backup3){
        $date3  = 'No backup created yet';
    }
    if(!$is_restore || ($is_restore && $backup3)){
        array_push($tz, array('id' => "3" , 'name' => $file3.'  -  '.$date3));
    }

    return $tz;
}
function backupinvoice2($selected = false){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='3'");
    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);
    if(!$backup1){
        $date1  = '';
    }
    if(!$backup2){
        $date2  = '';
    }
    if(!$backup3){
        $date3  = '';
    }
    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array(
        array('id' => "1" , 'name' => $file1.'    '.$date1),
        array('id' => "2" , 'name' => $file2.'    '.$date2),
        array('id' => "3" , 'name' => $file3.'    '.$date3),

    );
    return $tz;
}
function backupinvoice3($selected = false ){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='3'");
    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);
    if(!$backup1){
        $date1  = '';
    }
    if(!$backup2){
        $date2  = '';
    }
    if(!$backup3){
        $date3  = '';
    }

    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array(
        array('id' => "1" , 'name' => $file1.'    '.$date1),
        array('id' => "2" , 'name' => $file2.'    '.$date2),
        array('id' => "3" , 'name' => $file3.'    '.$date3),

    );
    return $tz;
}
function backupcatalog($selected = false){
    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1, '2'=> $file2, '3'=> $file3);
    return build_simple_dropdown($tz,$selected);
}

function build_time_zone_dropdown($selected=0){
    /*$tz=array(
    '-12' => '(GMT-12:00) International Date Line West',
    '-11' => '(GMT-11:00) Midway Island, Samoa',
    '-10' => '(GMT-10:00) Hawaii',
    '-9' => '(GMT-09:00) Alaska',
    '-8' => '(GMT-08:00) Pacific Time (US & Canada); Tijuana',
    '-7' => '(GMT-07:00) Mountain Time (US & Canada), Chihuahua',
    '-6' => '(GMT-06:00) Saskatchewan, Guadalajara, Mexico City',
    '-5' => '(GMT-05:00) Eastern Time (US & Canada), Bogota, Lima, Quito',
    '-4' => '(GMT-04:00) Santiago,Caracas, La Paz,Atlantic Time (Canada)',
    '-3.3' => '(GMT-03:30) Newfoundland',
    '-3' => '(GMT-03:00) Greenland, Buenos Aires, Georgetown, Brasilia',
    '-2' => '(GMT-02:00) Mid-Atlantic',
    '-1' => '(GMT-01:00) Cape Verde Is., Azores',
    '0' => '(GMT) Casablanca, Monrovia, Greenwich Mean Time : Dublin',
    '1' => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm',
    '2' => '(GMT+02:00) Athens, Beirut, Istanbul, Minsk ',
    '3' => '(GMT+03:00) Kuwait, Riyadh, Moscow, St. Petersburg',
    '3.3' => '(GMT+03:30) Tehran',
    '4' => '(GMT+04:00) Abu Dhabi, Muscat, Baku, Tbilisi, Yerevan',
    '4.3' => '(GMT+04:30) Kabul',
    '5' => '(GMT+05:00) Ekaterinburg, Islamabad, Karachi, Tashkent',
    '5.3' => '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
    '6' => '(GMT+06:00) Almaty, Novosibirsk, Astana, Dhaka, Sri Jayawardenepura',
    '7' => '(GMT+07:00) Bangkok, Hanoi, Jakarta, Krasnoyarsk',
    '8' => '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
    '9' => '(GMT+09:00) Osaka, Sapporo, Tokyo, Seoul, Yakutsk',
    '9.3' => '(GMT+09:30) Adelaide, Darwin',
    '10' => '(GMT+10:00) Sydney,Brisbane, Canberra, Melbourne',
    '11' => '(GMT+11:00) Magadan, Solomon Is., New Caledonia',
    '12' => '(GMT+12:00) Auckland, Wellington, Fiji, Kamchatka, Marshall Is.'
    );*/
    $tz = array(
        '-12' => '(GMT-12:00) International Date Line West',
'-11' => '(GMT-11:00) Midway Island, Samoa',
'-10' => '(GMT-10:00) Hawaii',
'-9' => '(GMT-09:00) Alaska',
'-8' => '(GMT-08:00) Pacific Time (US & Canada), Tijuana, Baja California',
'-7' => '(GMT-07:00) Arizona, Chihuahua, La Paz, Mazatlan, Mountain Time (US & Canada)',
'-6' => '(GMT-06:00) Central America, Central Time (US & Canada), Guadalajara, Mexico City, Monterrey, Saskatchewan',
'-5' => '(GMT-05:00) Bogota, Lima, Quito, Rio Branco, Eastern Time (US & Canada), Indiana (East)',
'-4' => '(GMT-04:00) Atlantic Time (Canada), Caracas, La Paz, Manaus, Santiago',
'-3.5' => '(GMT-03:30) Newfoundland',
'-3' => '(GMT-03:00) Brasilia, Buenos Aires, Georgetown, Greenland, Montevideo',
'-2' => '(GMT-02:00) Mid-Atlantic',
'-1' => '(GMT-01:00) Cape Verde Is., Azores',
'0' => '(GMT+00:00) Casablanca, Monrovia, Reykjavik, Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
'1' => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna, Belgrade, Bratislava, Budapest, Ljubljana, Prague, Brussels, Copenhagen, Madrid, Paris, Sarajevo, Skopje, Warsaw, Zagreb, West Central Africa',
'2' => '(GMT+02:00) Amman, Athens, Bucharest, Istanbul, Beirut, Cairo, Harare, Pretoria, Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius, Jerusalem, Minsk, Windhoek',
'3' => '(GMT+03:00) Kuwait, Riyadh, Baghdad, Moscow, St. Petersburg, Volgograd, Nairobi, Tbilisi',
'3.5' => '(GMT+03:30) Tehran',
'4' => '(GMT+04:00) Abu Dhabi, Muscat, Baku, Yerevan',
'4.5' => '(GMT+04:30) Kabul',
'5' => '(GMT+05:00) Yekaterinburg, Islamabad, Karachi, Tashkent',
'5.5' => '(GMT+05:30) Sri Jayawardenapura, Chennai, Kolkata, Mumbai, New Delhi',
'5.75' => '(GMT+05:45) Kathmandu',
'6' => '(GMT+06:00) Almaty, Novosibirsk, Astana, Dhaka',
'6.5' => '(GMT+06:30) Yangon (Rangoon)',
'7' => '(GMT+07:00) Bangkok, Hanoi, Jakarta, Krasnoyarsk',
'8' => '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi, Kuala Lumpur, Singapore, Irkutsk, Ulaan Bataar, Perth, Taipei',
'9' => '(GMT+09:00) Osaka, Sapporo, Tokyo, Seoul, Yakutsk',
'9.5' => '(GMT+09:30) Adelaide, Darwin',
'10' => '(GMT+10:00) Brisbane, Canberra, Melbourne, Sydney, Hobart, Guam, Port Moresby, Vladivostok',
'11' => '(GMT+11:00) Magadan, Solomon Is., New Caledonia',
'12' => '(GMT+12:00) Auckland, Wellington, Fiji, Kamchatka, Marshall Is.',
'13' => '(GMT+13:00) Nuku\'alofa,'
);

    return build_simple_dropdown_timezone($tz,$selected);
}
function build_price_type($selected = false){
    return build_simple_dropdown(array('1' =>  gm('Use Base Price'),'2' =>  gm('Use Purchase Price')),$selected);
}
function build_price_category_type($selected = false){
    return build_simple_dropdown(array('1' => gm('Discount'),'2' => gm('Mark-up'),'3' => gm('Profit Margin'),'4' => gm('Manually set price')),$selected);
}

function build_price_category_type_2($selected = false){
    return build_simple_dropdown(array('1' => gm('Discount'), '2' => gm('Mark-up'), '3' => gm('Profit Margin')),$selected);
}
function build_price_value_type_list($selected = false){
    return build_simple_dropdown(array('1' => '%','2' => gm('fixed value')),$selected);
}
function get_price_category_type($selected = false){

    $opt=array(
    1   => gm('Discount'),
    2   => gm('Mark-up'),
    3   => gm('Profit Margin'),
    4   => gm('Manually set price')
    );
    $out_str=$opt[$selected];
    return $out_str;
}
function get_price_type($selected = false){

    $opt=array(
    1   => gm('Use Base Price'),
    2   => gm('Use Purchase Price')
    );
    $out_str=$opt[$selected];
    return $out_str;
}
function get_price_value_type($selected = false){
    $currency = get_currency(ACCOUNT_CURRENCY_TYPE);
    $opt=array(
    1   => '%',
    2   => $currency
    );
    $out_str=$opt[$selected];
    return $out_str;
}
function build_language_dd_user($selected = false){

    $db=new sqldb;
    $array = array();
    $db->query("SELECT * FROM lang ORDER BY name");
    while($db->move_next()){
        $array[$db->f('lang_id')] = $db->f('name');
    }
    return build_simple_dropdown_timezone($array,$selected);
}
function build_group_list($selected = ''){
    $db=new sqldb;
    $db->query("select * from groups WHERE group_id <> 2");
     $array = array();
    $no_timesheets = false;
    if (ACCOUNT_TYPE=='goods')
    {
        $no_timesheets = true;
    }
    if(!$selected){
        if($no_timesheets)
        {
            $selected='1';
        }else
        {
            $selected = '2';
        }
    }
    $out_str="";
    while($db->move_next()){
        if($no_timesheets==false || $db->f('group_id')!=2)
        {
            $array[$db->f('group_id')] = $db->f('name');
        }
    }
    return build_simple_dropdown_timezone($array,$selected);
}
function build_month_list($selected=0)
{

    $opt=array(
        array('id' => "1" , 'name' => gm("January")),
        array('id' => "2" , 'name' => gm("February")),
        array('id' => "3" , 'name' => gm("March")),
        array('id' => "4" , 'name' => gm("April")),
        array('id' => "5" , 'name' => gm("May")),
        array('id' => "6" , 'name' => gm("June")),
        array('id' => "7" , 'name' => gm("July")),
        array('id' => "8" , 'name' => gm("August")),
        array('id' => "9" , 'name' => gm("September")),
        array('id' => "10" , 'name' => gm("October")),
        array('id' => "11" , 'name' => gm("November")),
        array('id' => "12" , 'name' => gm("December")),

    );
    /*while (list ($key, $val) = each ($opt))
    {
        $out_str.="<option value=\"".$key."\" ";//options values
        $out_str.=($key==$selected?" SELECTED ":"");//if selected
        $out_str.=">".$val."</option>";//options names
    }*/
    return $opt;
}

function build_activity_list($selected=0)
{

    $opt=array(
        array('id' => "1" ,  'name' => gm("Agro-food")),
        array('id' => "2" ,  'name' => gm("Banking/Insurance")),
        array('id' => "3" ,  'name' => gm("Building and Public Works/Building Material")),
        array('id' => "4" ,  'name' => gm("Business/Trade/Distribution")),
        array('id' => "5" ,  'name' => gm("Craftsmen")),
        array('id' => "6" ,  'name' => gm("Chemistry/Parachemistry")),
        array('id' => "7" ,  'name' => gm("IT/Telecoms")),
        array('id' => "8" ,  'name' => gm("Healthcare")),
        array('id' => "9" ,  'name' => gm("Pharmaceutical Industry")),
        array('id' => "10" , 'name' => gm("Publishing/Communication/Multimedia")),
        array('id' => "11" , 'name' => gm("Machines and Equipment/Automotive")),
        array('id' => "12" , 'name' => gm("Transport/Logistics")),
        array('id' => "13" , 'name' => gm("Services for Businesses")),
        array('id' => "14" , 'name' => gm("Services for Individuals")),
        array('id' => "15" , 'name' => gm("Studies and Consultancy")),
        array('id' => "16" , 'name' => gm("Wood/Paper/Cardboard/Printing")),

    );

    return $opt;
}
function build_company_list($selected=0)
{

    $opt=array(
        array('id' => "1" ,  'name' => '1'),
        array('id' => "2" ,  'name' => '2-5'),
        array('id' => "3" ,  'name' => '6-10'),
        array('id' => "4" ,  'name' => '11-15'),
        array('id' => "5" ,  'name' => '16-20'),
        array('id' => "6" ,  'name' => '21-25'),
        array('id' => "7" ,  'name' => '+25'),
    );
    return $opt;
}

function display_number($number,$decimals=2){
    if(!$number){
        $number = 0;
    }
    $separator = ACCOUNT_NUMBER_FORMAT;
    $out = number_format(floatval($number),$decimals,substr($separator, -1),substr($separator, 0, 1));
    return $out;
}
function display_number_exclude_thousand($number,$decimals=2){
    if(!$number){
        $number = 0;
    }
    $separator = ACCOUNT_NUMBER_FORMAT;
    $out = number_format(floatval($number),$decimals,substr($separator, -1),'');
    return $out;
}
function build_publish_lang_dd($selected = false,$setting = ''){
    $db = new sqldb();
    $array = array();
    $filter = " AND active='1' ";
    if($setting){
        $filter = "";
    }
    // $db->query("SELECT * FROM pim_lang WHERE language!='German' {$filter} ORDER BY sort_order");
    $db->query("SELECT * FROM pim_lang WHERE 1 = 1 {$filter} ORDER BY sort_order");
    while($db->move_next()){
        $array[$db->f('lang_id')] = gm($db->f('language'));
    }
    return build_simple_dropdown_timezone($array,$selected);
}
function updateUsersInfo()
{
    $used_const = array('ACCOUNT_DELIVERY_ADDRESS','ACCOUNT_DELIVERY_ZIP','ACCOUNT_DELIVERY_CITY','ACCOUNT_DELIVERY_STATE_ID','ACCOUNT_BILLING_ADDRESS','ACCOUNT_DELIVERY_COUNTRY_ID','ACCOUNT_BILLING_ZIP','ACCOUNT_BILLING_CITY','ACCOUNT_BILLING_STATE_ID','ACCOUNT_BILLING_COUNTRY_ID','ACCOUNT_VAT_NUMBER','ACCOUNT_VAT','ACCOUNT_CURRENCY_TYPE','ACCOUNT_COMPANY','ACCOUNT_BANK_NAME','ACCOUNT_BIC_CODE','ACCOUNT_IBAN','ACCOUNT_PHONE','ACCOUNT_FAX','ACCOUNT_TYPE');
    $db = new sqldb();
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    $const = $db->query("SELECT * FROM settings");
    while ($const->next()) {
        if(in_array($const->f('constant_name'), $used_const) ){
            $value = $const->f('value');
            if($const->f('type')  != 1){
                $value = $const->f('long_value');
            }
            $dbs->query("update users set ".$const->f('constant_name')."='".addslashes(utf8_decode($value))."' where database_name='".DATABASE_NAME."'");
        }
    }
}

function updateCustomersInfo()
{  if(strpos($config['site_url'],'my.akti')){
    //if(strpos($config['site_url'],'akti')){
        global $database_config;
        $database_new = array(
                'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
                'username' => 'admin',
                'password' => 'hU2Qrk2JE9auQA',
                'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
                //'database' => '32ada082_5cf4_1518_3aa09bca7f29'

        );
            
        $db_user_new = new sqldb($database_new);
         $db = new sqldb();

        $select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
        if($select_contact){
            $select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
        }       
        if($select_customer){
            $account_billing_address = $db->field("SELECT long_value FROM settings WHERE constant_name='ACCOUNT_BILLING_ADDRESS'");
            $account_billing_zip = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_ZIP'");
            $account_billing_city = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_CITY'");
            $billing_country = $db->field("SELECT value from settings where  constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
            $account_billing_state = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_STATE_ID'");

            $account_phone = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PHONE'");
            $account_fax = $db->field("SELECT value from settings where  constant_name='ACCOUNT_FAX'");
            $account_email = $db->field("SELECT value from settings where  constant_name='ACCOUNT_EMAIL'");
            
            $address = $db_user_new->field("SELECT address_id FROM customer_addresses WHERE customer_id='".$select_customer."' AND `is_primary`='1'");
            if($address){
                $db_user_new->query("UPDATE customer_addresses SET country_id='".$billing_country."',state_id='".$account_billing_state."',city='".$account_billing_city."',zip='".$account_billing_zip."',address='".$account_billing_address."', WHERE address_id='".$address."'");
            }else{
                $db_user_new->query("INSERT INTO customer_addresses SET customer_id='".$select_customer."', address_id='".$address."', country_id='".$billing_country."', state_id='".$account_billing_state."', city='".$account_billing_city."', zip='".$account_billing_zip."', address='".$account_billing_address."', is_primary='1', billing='1'");
            }
        }           
        
    }

}

function get_country_name($country_id){
    $name = '';
    if($country_id){
        $db=new sqldb();
        $db->query("SELECT country.* FROM country WHERE country_id='".$country_id."'");
        $db->move_next();
        $name=addslashes($db->f('name'));
    }
    return $name;
}

function get_country_code($country_id)
{
    $db=new sqldb;
    $db->query("SELECT code FROM country WHERE country_id='".$country_id."'");
    $db->move_next();
    $name=$db->f('code');
    return $name;
}

function number_as_hour($number){
    $negative = false;
    $min = round($number * 60);
    if($min < 0){
        $min = $min*(-1);
        $negative = true;
    }
    if( $min >= "60" ){
        $n = floor( $min / 60 );
        $min = $min - ( 60 * $n );
        if(strlen($min) == 1){
            $min = "0" . $min;
        }
        if($negative){
            $n = $n*(-1);
        }
        $out = $n . ":" . $min;
    }else{
        if(strlen($min) == 1){
            $min = "0" . $min;
        }
        $out = "0:". $min;
    }
    return $out;
}

function number_as_hour2($number){
    $negative = false;
    $min = round($number * 60);
    if($min < 0){
        $min = $min*(-1);
        $negative = true;
    }
    if( $min >= "60" ){
        $n = floor( $min / 60 );
        $min = $min - ( 60 * $n );
        if(strlen($min) == 1){
            $min = "0" . $min;
        }
        if(strlen($n) == 1){
            $n = "0" . $n;
        }
        if($negative){
            $n = $n*(-1);
        }
        $out = $n . ":" . $min;
    }else{
        if(strlen($min) == 1){
            $min = "0" . $min;
        }
        $out = "00:". $min;
    }
    $out = $out.':00';
    return $out;
}

function place_currency($out,$currency = '',$position='',$currency_font_face=''){

    if(!$currency){
        $currency = get_currency(ACCOUNT_CURRENCY_TYPE);
    }
    if($currency_font_face){
        $currency = '<font face="'.$currency_font_face.'">'.$currency.'</font>';
    }
    if($position==1){
        $out = $out.' '.$currency;
    }else{
        if(ACCOUNT_CURRENCY_FORMAT == 0){
            $out = $out.' '.$currency;
        }
        else {
            $out = $currency.' '.$out;
        }
    }
    return $out;

}

function get_currency($selected = false)
{
    return currency::get_currency($selected);
}

function get_currency_value_by_code($code)
{   
     $db=new sqldb;
    if(!$code){
        $ACCOUNT_CURRENCY_TYPE = ACCOUNT_CURRENCY_TYPE;
        if(!$ACCOUNT_CURRENCY_TYPE){
            $currency_value=$db->field("SELECT value FROM currency WHERE id='1' ");
        }else{
            $currency_value=$db->field("SELECT value FROM currency WHERE id='".$ACCOUNT_CURRENCY_TYPE."' ");
        }
    }else{
        $currency_value=$db->field("SELECT value FROM currency WHERE code='".$code."' ");
    }
    return $currency_value;
}

function build_language_dd_new($selected='',$select_text=false) {
    $db_1 = new sqldb();
    $select = array();
    $country_codes = array();
    $db_1->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
    while($db_1->next()) {
        $row = array('id'=>$db_1->f('lang_id'),'name'=>gm($db_1->f('language')));
        // $select[$db_1->f('lang_id')] = gm($db_1->f('language'));
            array_push($select, $row);
        switch ($db_1->f('code')) {
            case 'du':
                $country_codes[$db_1->f('lang_id')] = 'nl';
                break;
            case 'en':
                $country_codes[$db_1->f('lang_id')] = 'gb';
                break;
            default:
                $country_codes[$db_1->f('lang_id')] = $db_1->f('code');
                break;
        }
    }
    $db_1->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
    while($db_1->next()) {
        $row = array('id'=>$db_1->f('lang_id'),'name'=>gm($db_1->f('language')));
        // $select[$db_1->f('lang_id')] = gm($db_1->f('language'));
        array_push($select, $row);
        switch ($db_1->f('code')) {
            case 'cs':
                $country_codes[$db_1->f('lang_id')] = 'cz';
                break;
            case 'sv':
                $country_codes[$db_1->f('lang_id')] = 'se';
                break;
            case 'da':
                $country_codes[$db_1->f('lang_id')] = 'dk';
                break;
            default:
                $country_codes[$db_1->f('lang_id')] = $db_1->f('code');
                break;
        }
    }
    if($select_text){
        if($select_text=='country_code'){
            return strtoupper($country_codes[$selected]);
        }else{
            return $select[$selected];
        }
    }else{
        return $select;
    }
}
function get_custom_right($user_cred,$group){
    $db = new sqldb();
    // global $database_config;

    // $database_users = array(
    //     'hostname' => $database_config['mysql']['hostname'],
    //     'username' => $database_config['mysql']['username'],
    //     'password' => $database_config['mysql']['password'],
    //     'database' => $database_config['user_db'],
    // );
    // $dbs = new sqldb($database_users);
    // $user_cred = $dbs->field("SELECT credentials FROM users WHERE user_id='".$user."'");
    $user_cred = explode(';',$user_cred);
    $rezult = count($user_cred);
    $img = "";
    if($rezult == 1 && $user_cred[0] == 8){
        $img = 'time';
    }else{
            $img = 'power';
            if($group['group_id'] == 1){
                $img = 'admin';
            }
    }
    $out[2] = $img;
    // $group = $db->query("SELECT credentials, name FROM groups WHERE group_id='".$group."' ");
    // $group->next();
    $out[0] = $group['name'];
    $cred = explode(';',$group['credentials']);
    $result = array_diff($user_cred, $cred);
    $text = "";
    $i=0;
    if(!empty($result)){
        foreach ($user_cred as $key){
            if($i != 0 && !empty($key)){
                $text .=', ';
            }
            switch ($key){
                case '1':
                    $text .='CRM';
                    break;
                case '2':
                    $text .='Reports';
                    break;
                case '3':
                    $text .='Projects';
                    break;
                case '4':
                    $text .='Billing';
                    break;
                case '5':
                    $text .='Quotes';
                    break;
                case '6':
                    $text .='Orders';
                    break;
                case '7':
                    $text .='Manage';
                    break;
                case '8':
                    $text .='Timesheet only';
                    break;
                case '11':
                    $text .='Contracts';
                    break;
                case '12':
                    $text .='Articles';
                    break;
                case '13':
                    $text .='Interventions';
                    break;
            }
            $i++;
        }
    }

    $out[1] = $text;

    return $out;
}
function get_user_name($id){
    if(!$id){
        return '';
    }
    global $database_config;

    $db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

    $db = new sqldb($db_config);

    $db->query("SELECT first_name,last_name FROM users WHERE user_id='".$id."'");
    $db->move_next();
    return htmlspecialchars_decode(stripslashes($db->f('first_name').' '.$db->f('last_name')));
}

function split_lines($str, $l=40){
    $str_len = strlen($str);
    $new_str ='';
    if($str_len>$l){
        $i=0;
        $strs = str_split($str,$l);
        while($i<count($strs)){
            $new_str .= $strs[$i]."<br/>";
            $i++;
        }
    }else{
        $new_str = $str;
    }
    return $new_str;
}
function get_group_name($id){
    $db = new sqldb();
    return $db->field("SELECT name FROM groups WHERE group_id='".$id."' ");
}
function pick_date_format($format = ACCOUNT_DATE_FORMAT){
    switch ($format){
        case 'm/d/Y':
            return 'MM/dd/yyyy';
            break;
        case 'd/m/Y':
            return 'dd/MM/yyyy';
            break;
        case 'Y-m-d':
            return 'yyyy-MM-dd';
            break;
        case 'd.m.Y':
            return 'dd.MM.yyyy';
            break;
        case 'Y.m.d':
            return 'yyyy.MM.dd';
            break;
        case 'Y/m/d':
            return 'yyyy/MM/dd';
            break;
    }
}

function get_query_date_format($format = ACCOUNT_DATE_FORMAT){
    switch ($format){
        case 'm/d/Y':
            return '%m/%d/%Y';
            break;
        case 'd/m/Y':
            return '%d/%m/%Y';
            break;
        case 'Y-m-d':
            return '%Y-%m-%d';
            break;
        case 'd.m.Y':
            return '%d.%m.%Y';
            break;
        case 'Y.m.d':
            return '%Y.%m.%d';
            break;
        case 'Y/m/d':
            return '%Y/%m/%d';
            break;
    }
}

function return_value($number){
    $separator = ACCOUNT_NUMBER_FORMAT;
    $out=str_replace(substr($separator, 0, -1), '', $number);
    // var_dump(substr($separator, 0, -1));
    $out=str_replace(substr($separator, -1), '.', $out);
    if(!is_numeric($out)){
        $out =0;
    }
    return (float)$out;

}
function build_currency_list($selected=''){
    $currency = currency::get_currency($selected,'arrays');
    $array = array();
    foreach ($currency as $key => $value) {
        array_push($array, array('id'=>$key,'name'=>$value,'code'=>currency::get_currency($key)));
    }
    return $array;
}
function build_currency_list_translated($selected=''){
    $currency = currency::get_currency($selected,'arrays');
    $array = array();
    foreach ($currency as $key => $value) {
        array_push($array, array('id'=>$key,'name'=>gm($value),'code'=>currency::get_currency($key)));
    }
    return $array;
}
function build_currency_name_list($selected = false){
    return currency::get_currency($selected,'code');
}
function get_sys_message($name,$lang='',$useHtml=true){
    $_db=new sqldb();
    $filter = ' ';
    if(!empty($lang) && is_numeric($lang)){
        if($lang>=1000) {
            $code = $_db->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$lang."' ");
            if($code=='nl'){
                $code='du';
            }
            $filter = " AND lang_code = '".$code."' ";
        } else {
            $code = $_db->field("SELECT code FROM pim_lang WHERE lang_id='".$lang."' ");
            if($code=='nl'){
                $code='du';
            }
            $filter = " AND lang_code='".$code."' ";
        }
    }else if(!empty($lang)){
        $filter = " AND lang_code='".$lang."' ";
    }

    $_db->query("SELECT * FROM sys_message WHERE name='".$name."' ".$filter);
    $_db->move_next();
    $msg['text']=$_db->f('text');
    $msg['from_email']=$_db->f('from_email');
    $msg['from_name']=$_db->f('from_name');
    $msg['subject']=$_db->f('subject');
    $msg['footnote']=$_db->f('footnote');
    $msg['use_html']=$_db->f('use_html');
    if($msg['use_html'] == 1 && $useHtml === true && $_db->f('html_content')){
        $msg['text']=$_db->f('html_content');
    }
    return $msg;
}

function generate_chars(){
    $num_chars = rand(20,25); //max length of random chars
    $i = 0;
    $my_keys = "123456789bcdfghjklmnpqrstvwxyz"; //keys to be chosen from
    $keys_length = strlen($my_keys);
    $url  = "";
    while($i<$num_chars)
    {
        $rand_num = mt_rand(1, $keys_length-1);
        $url .= $my_keys[$rand_num];
        $i++;
    }
    return $url;
}
function isUnique($chars){
    global $database_config;

    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb($database_users);
    global $link;
    $q = $db->query("SELECT * FROM `urls` WHERE `url_code`='".$chars."'");
    if($q->next()){
        return false;
    }else{
        return true;
    }
}
function get_commission_type_list($selected = false)
{
    return currency::get_currency($selected);
}

function get_signature(){
    global $database_config,$config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    if($_SESSION['u_id']){
        $dbs->query("SELECT * FROM users where user_id='".$_SESSION['u_id']."'");
        $dbs->move_next();
        if($dbs->gf('signature')){
            $info.= "<img src=".$config['site_url'].'../pim/'.$dbs->f('signature')." style='float:left;margin-right:15px;'>";
        }
        $info.="<br/>".$dbs->f('title') .", <strong>".$dbs->f('first_name').' '.$dbs->f('last_name')."</strong>";
        $info.="<br/>".$dbs->f('mobile');
        $info.="<br/>".$dbs->f('phone');
        $info.="<br/>".$dbs->f('fax');
        return $info;
    }
    return '';
}
function get_signature_new(){
    global $database_config,$config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    if($_SESSION['u_id']){
        $text=$dbs->field("SELECT text_signature FROM users where user_id='".$_SESSION['u_id']."'");
/*        $dbs->move_next();*/
/*        if($dbs->gf('signature')){
            $info.= "<img src=".$config['site_url'].'../pim/'.$dbs->f('signature')." style='float:left;margin-right:15px;'>";
        }
        $info.="<br/>".$dbs->f('title') .", <strong>".$dbs->f('first_name').' '.$dbs->f('last_name')."</strong>";
        $info.="<br/>".$dbs->f('mobile');
        $info.="<br/>".$dbs->f('phone');
        $info.="<br/>".$dbs->f('fax');*/
        //$info=htmlspecialchars_decode(stripslashes($text));
        $info=stripslashes($text);
        return $info;
    }
    return '';
}
function build_identity_dd($selected = false, $none = false){
    $db= new sqldb();
    $db->query("SELECT identity_id, identity_name FROM multiple_identity ");
    $out_str=array();
    if($none){
        array_push($out_str, array(
            'id'=>'999999',
            'name'=>gm('None')
        ));
    }

    while($db->move_next()){
        array_push($out_str, array(
            'id'=>$db->f('identity_id'),
            'name'=>$db->f('identity_name')
        ));
    }
    array_push($out_str, array(
        'id'=>'0',
        'name'=>gm('Main Identity')
    ));


    return $out_str;
}

function get_user_identity($user_id){
    $user_identity =0;

    if($user_id){
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $dbs = new sqldb($database_users);
      
        $user_identity=$dbs->field("SELECT default_identity_id FROM user_info where user_id='".$user_id."'");
    }
    return $user_identity;
}

function get_user_customer_identity($user_id, $customer_id = 0){
    $user_customer_identity =0;

    if($user_id){
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $dbs = new sqldb($database_users);
      
        $user_customer_identity=$dbs->field("SELECT default_identity_id FROM user_info where user_id='".$user_id."'");
    }

    if($user_customer_identity=='999999' && $customer_id){
        //if default in "none" (999999), default will not overrule the customer set identity
        $db = new sqldb();
        $user_customer_identity=$db->field("SELECT identity_id FROM customers where customer_id='".$customer_id."'");
    }elseif($user_customer_identity=='999999' && !$customer_id){
        //if default in "none" (999999) and we don't have customer_id, the main identity is used (0)
        $user_customer_identity= 0 ;
    }
    return $user_customer_identity;
}

function generate_service_number($database=''){
    if($database){
        global $database_config,$cfg;
        $db_conf = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );
        $db = new sqldb($db_conf);
    }else{
        $db = new sqldb();
    }

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_SERVICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_SERVICE_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT service_id,serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM servicing_support
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}

function generate_deal_number($database=''){
    if($database){
        global $database_config,$cfg;
        $db_conf = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );
        $db = new sqldb($db_conf);
    }else{
        $db = new sqldb();
    }

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DEAL_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DEAL_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT opportunity_id,serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblopportunity
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}

function build_state_list($selected ='',$view_mode=false,$state_ids=''){
    $db=new sqldb;
    if(!$selected)
    {
        $selected = ACCOUNT_BILLING_COUNTRY_ID;
    }

    $filter = " 1=1  ";
    if($view_mode === true){

        $filter.= " AND state_id='".$selected."' ";

    }
     $filter_state_ids = " ";
    if($state_ids){
        $filter_state_ids = " AND state_id in (".$state_ids.") ";
    }
    $db->query("SELECT * FROM state WHERE ".$filter.$filter_state_ids." ORDER BY name");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    $out_str=array();
    while($db->move_next()){
        array_push($out_str, array(
            'id'=>$db->f('state_id'),
            'name'=>$db->f('name')
        ));
    }
    return $out_str;
}
//Build Country DD
function build_country_list($selected ='',$view_mode=false,$country_ids=''){
    $db=new sqldb;
    if(!$selected)
    {
        $selected = ACCOUNT_BILLING_COUNTRY_ID;
    }

    $filter = " 1=1  ";
    if($view_mode === true){

        $filter.= " AND country_id='".$selected."' ";

    }
     $filter_country_ids = " ";
    if($country_ids){
        $filter_country_ids = " AND country_id in (".$country_ids.") ";
    }
    $db->query("SELECT * FROM country WHERE ".$filter.$filter_country_ids." ORDER BY name");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    $out_str=array();
    while($db->move_next()){
        array_push($out_str, array(
            'id'=>$db->f('country_id'),
            'name'=>$db->f('name')
        ));
    }
    return $out_str;
}

function build_user_dd_service($selected = false, $access){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    $db->query("SELECT user_id, first_name, last_name,group_id FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ");
    $db->move_next();

    $array[$db->f('user_id')] = $db->f('last_name')." ".$db->f('first_name');

    $filter = ' 1=1 ';
    $group_id = $db->f('group_id');
    if($group_id != 2){
        /*if($access > 1 ){
            $filter = " users.user_role > '".$_SESSION['access_level']."' OR users.user_id='".$selected."' ";
        }*/
        /*$service_manager=$db->field("SELECT value FROM user_meta WHERE name='intervention_admin' AND user_id='".$_SESSION['u_id']."' ");
        $is_manager = $db2->field("SELECT service_id FROM servicing_support_users WHERE pr_m='1' AND user_id='".$_SESSION['u_id']."' ");
 
        if($_SESSION['group'] != 'admin' && !empty($is_manager) && !$_SESSION['admin_sett']['admin_13'] && !$service_manager){
            $u = '';
            $users = $db2->query("SELECT user_id FROM  `servicing_support_users` WHERE service_id IN
                                    ( SELECT service_id FROM `servicing_support_users` WHERE pr_m='1' AND user_id ='".$_SESSION['u_id']."' )
                                  GROUP BY user_id ");
            while ($users->next()) {
                $u .= $users->f('user_id').',';
            }
            $filter =" users.user_id IN ( ".rtrim($u,',')." ) ";
        }else */if($_SESSION['team_int']){
            $filter = " users.user_id='".$selected."' ";
        }
        $db->query("SELECT users.user_id, users.first_name, users.last_name FROM users 
            LEFT JOIN user_meta ON users.user_id=user_meta.user_id AND user_meta.name='active'

            WHERE database_name='".DATABASE_NAME."' AND users.active<>1000 AND user_meta.value IS NULL AND $filter ORDER BY last_name");

        while ($db->move_next()) {

            $array[$db->f('user_id')] = $db->f('last_name')." ".$db->f('first_name');

        }
    }
    asort($array,SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
    $new_array = array();
    foreach ($array as $key => $value) {
        array_push($new_array, array('id'=>$key,'value'=>htmlspecialchars_decode(stripslashes($value))));
    }

    // $out[0] = $array[$selected];
    // $out[1] = $array;
    return $new_array;
}

function getItemPerm($perm){
    if(!is_array($perm)){
        return false;
    }
    #we check if the user has access to the module
    $module_access=false;
    if($_SESSION['group']=='admin') {
        $module_access=true;
    } else {
        if(in_array($perm['module'], $_SESSION['admin_sett'])) {
            $module_access=true;
        } else {
            $module_access=false;
        }
    }
    $permission = $module_access;
    if($permission === false) {
        #the user does not have admin access for module
        #we check to see if he has access to the item
        if(!is_array($perm['item'])){
            $perm['item'] = (array)$perm['item'];
        }
        foreach ($perm['item'] as $key => $value) {
            if($_SESSION['u_id'] == $value) {
                $permission = true;
                break;
            }
        }
    }
    return $permission;

}

function getHasAdminRights($perm){
    if(!is_array($perm)){
        return false;
    }
    #we check if the user has access to the module
    $module_access=false;
    if($_SESSION['group']=='admin') {
        $module_access=true;
    } else {
        if(in_array($perm['module'], $_SESSION['admin_sett'])) {
            $module_access=true;
        } else {
            $module_access=false;
        }
    }
    
    return $module_access;

}

function display_number_var_dec($number){
    if(!$number){
        $number = 0;
    }
    $separator = ACCOUNT_NUMBER_FORMAT;
    $decimals_after_comma = ARTICLE_PRICE_COMMA_DIGITS;
    $out = number_format(floatval($number),$decimals_after_comma,substr($separator, -1),substr($separator, 0, 1));
    return $out;
}

//with display number function
function remove_zero_decimals_dn($number,$decimals=2){
    $separator = ACCOUNT_NUMBER_FORMAT;
    if(!$number){
        $number = display_number(0);
        return $number;
    }
    $first_separator = substr($separator, -2, 1);
    $second_separator = substr($separator, -1);

    if(substr($number, -2,2)== '00'){
        $number = rtrim($number,'00');
        $number = rtrim($number,$second_separator);
    }
    return $number;
}

function remove_zero_decimals($number,$decimals=2){
    $separator = ACCOUNT_NUMBER_FORMAT;
    if(!$number){
        $number = '0.00';
    }
    if(substr($number, -2,2)== '00'){
        $out = intval($number);
    }else{
        $pos = strpos($number, '.');
        if($pos === false){
            return $number;
        }
        $dec_part = substr($number, -2,2);
        $int_part = intval($number);
        // console::log($int_part,substr($separator, -1),$dec_part);
        if( substr($number,0,1) == '-' && $int_part == 0 ){
            $minus_sign = '-';
        }
        $out = $minus_sign.$int_part.substr($separator, -1).$dec_part;

    }
    return $out;
}

function build_pdf_language_dd($selected='',$active){

    $db = new sqldb();
    $array = array();
    $filter="1=1";
    if($active){
        $filter.=" and active=1";
    }

    $db->query("SELECT * FROM pim_lang where  ".$filter." ORDER BY sort_order ");
    while($db->move_next()){
        $row = array('id'=>$db->f('lang_id'),'name'=>gm($db->f('language')));
        array_push($array, $row);
    }
    $lng = $db->query("SELECT * FROM pim_lang where  ".$filter." ORDER BY sort_order ");
    while($lng->next()){
        $l = $db->query("SELECT * FROM label_language WHERE lang_code='".$lng->f('lang_id')."'");
        if($l->next())
        {
            $row = array('id'=>$lng->f('label_language_id'),'name'=>gm($lng->f('language')));
            array_push($array, $row);
        }
    }
    return $array;

}
function build_simple_dropdown_timezone($opt, $selected=0,$view=0){
    $out_str = array();
    if($view==0){
        if($opt)

        while (list ($key, $val) = each ($opt))
        {
            array_push($out_str, array('id'=>$key,'name'=>$val));
        }
    }elseif($view==1){
        if($opt)
            while (list ($key, $val) = each ($opt))
            {
                $out_str.="<option name=\"".$key."\" ";//options values
                $out_str.=($key==$selected?" SELECTED ":"");//if selected
                $out_str.=">".$val."</option>";//options names            
            }
    }
    else{
        $out_str = '';
        if($opt)
        while (list ($key, $val) = each ($opt))
        {
            if($key==$selected){
                  $out_str=  $val;
            }

        }
    }
    return $out_str;
}

function build_simple_dropdown($opt, $selected=0,$view=0){
    $out_str = array();
    if($view==0){
        if($opt)

        while (list ($key, $val) = each ($opt))
        {
            array_push($out_str, array('id'=>$key,'value'=>$val));
        }
    }elseif($view==1){
        if($opt)
            while (list ($key, $val) = each ($opt))
            {
                $out_str.="<option value=\"".$key."\" ";//options values
                $out_str.=($key==$selected?" SELECTED ":"");//if selected
                $out_str.=">".$val."</option>";//options names            
            }
    }
    else{
        $out_str = '';
        if($opt)
        while (list ($key, $val) = each ($opt))
        {
            if($key==$selected){
                  $out_str=  $val;
            }

        }
    }
    return $out_str;
}

function wrap($string, $width=75, $break="\n", $cut=true){
  if($cut) {
    // Match anything 1 to $width chars long followed by whitespace or EOS,
    // otherwise match anything $width chars long
    $search = '/(.{1,'.$width.'})(?:\s|$)|(.{'.$width.'})/uS';
    $replace = '$1$2'.$break;
  } else {
    // Anchor the beginning of the pattern with a lookahead
    // to avoid crazy backtracking when words are longer than $width
    $pattern = '/(?=\s)(.{1,'.$width.'})(?:\s|$)/uS';
    $replace = '$1'.$break;
  }
  return preg_replace($search, $replace, $string);
}

function wrap2($string){
    $string = utf8_decode($string);
    $string2='';
    $q = strlen($string);
    for ($i=0; $i <= $q; $i++) {
    $char = substr( $string, $i, 1 );
        $string2 .= $char.' &nbsp;';
    }
    return utf8_encode($string2);
}
/* o functie care s-a uitat de mult ce trebuia sa faca, dar nu o putem scoate ca este peste tot in proiect */
function insert_message_log($pag,$message,$field_name,$field_value,$type = 0,$user_id = '', $track_log=false, $customer_id=0, $the_time='0'){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    if(!$the_time)
    {
        $the_time=time();
    }
    $db = new sqldb();
    $db_users = new sqldb($db_config);
    //$customer_id = '';
    $message = stripslashes($message);
    $id = $db->insert("INSERT INTO logging SET pag='".$pag."', message='".addslashes($message)."', field_name='".$field_name."', field_value='".$field_value."', date='".$the_time."', type='".$type."', user_id='".$user_id."', reminder_date='".$the_time."' ");
    if($track_log==true)
    {
        $users = $db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();
        foreach ($users as $key => $value) {
            $db->query("INSERT INTO logging_tracked SET
                                log_id='".$id."',
                                customer_id='".$customer_id."',
                                user_id='".$value['user_id']."'");
        }
        $db->query("UPDATE logging_tracked SET seen='1' WHERE user_id='".$_SESSION['u_id']."' AND log_id='".$id."'");
    }
    return $id;
}
/* end of CMS MENU functions */
function build_dispach_location_dd($selected = '',$article_ids='',$own=''){
    $db=new sqldb();
    //first we select all our address
    $array=array();
    $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC");

    while($db->move_next()){
       array_push($array, array('id'=>'0-'.$db->f('address_id'),'name'=>$db->f('naming').' / '.$db->f('address').' '.$db->f('zip').' '.$db->f('city').' '.$db->f('to_country')));
    }
    
if(!$own){
    //we selct customer addreses
    $db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_naming,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
                 FROM  dispatch_stock
                    INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id and pim_stock_disp.to_address_id=dispatch_stock.address_id
                 WHERE dispatch_stock.customer_id!=0
                 GROUp BY dispatch_stock.customer_id,dispatch_stock.address_id");


     while($db->move_next()){
       array_push($array, array('id'=>$db->f('customer_id').'-'.$db->f('address_id'),'name'=>$db->f('to_naming').' / '.$db->f('to_address').' '.$db->f('to_zip').' '.$db->f('to_city').' '.$db->f('to_country')));
    }
}
    return $array;
}

function build_picking_location_dd($selected = '', $view=0){
    $db=new sqldb();
    $array=array();
    if(!$view){
        array_push($array, array('id'=>'0','name'=>gm('All locations')));
    }
    
    $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC");

    while($db->move_next()){
       array_push($array, array('id'=>$db->f('address_id'),'name'=>$db->f('naming')));
    }
    
    return $array;
}

function generate_purchase_price($article_id,$first=0){

    $db = new sqldb();
    $db2 = new sqldb();
    $wac_act_date=WAC_ACT_DATE;
    $price=0;
    $total_stock_value=0;
    $total_quantity_purchase=0;
    $wac=0;
    $stocks_value_entry=array();
    $stock_exit=0;

    $stocks_value=array();
    if($first){
        $wac=$db->field("SELECT pim_p_order_articles.price FROM pim_p_order_articles WHERE article_id='".$article_id."' ORDER BY order_articles_id DESC LIMIT 1");
        if($wac){
            $db->query("UPDATE pim_article_prices SET purchase_price='".$wac."' WHERE base_price=1 AND article_id='".$article_id."'");
        }
        return true;
    }
    //first we select all  entries from purchase order
    $db->query("SELECT pim_p_order_deliveries.date,pim_p_order_deliveries.delivery_id,pim_p_order_articles.article_id,pim_p_order_articles.order_articles_id,pim_p_order_articles.price,pim_p_orders_delivery.quantity,pim_p_order_deliveries.date
                FROM pim_p_order_articles
                INNER JOIN pim_p_orders_delivery ON pim_p_orders_delivery.order_articles_id= pim_p_order_articles.order_articles_id
                INNER JOIN  pim_p_order_deliveries ON  pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id
                WHERE pim_p_order_articles.article_id='".$article_id."' AND pim_p_order_deliveries.date > '".$wac_act_date."'
                ORDER BY pim_p_order_deliveries.date ASC
              ");

    while($db->move_next()) {
        $stocks_value_entry[$db->f('delivery_id')] = $db->f('quantity');
    }

   //the magic now we selelct all exit form order
    $db->query("SELECT pim_order_deliveries.date,pim_order_deliveries.delivery_id,pim_order_articles.article_id,pim_order_articles.order_articles_id,pim_order_articles.price,sum(pim_orders_delivery.quantity) as qty,pim_order_deliveries.date
                FROM pim_order_articles
                INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id= pim_order_articles.order_articles_id
                INNER JOIN  pim_order_deliveries ON  pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
                WHERE pim_order_articles.article_id='".$article_id."' AND pim_order_deliveries.date > '".$wac_act_date."'
                 ORDER BY pim_order_deliveries.date ASC
              ");
    $db->move_next();
    $returned = $db2->field("SELECT sum(quantity) as qty_p FROM  pim_articles_return WHERE article_id='".$article_id."'");
    $stock_exit=$db->f('qty')-$returned;
    $db->query("SELECT sum(quantity) FROM  project_articles WHERE delivered=1 AND article_id='".$article_id."'");
    $db->move_next();
    $stock_exit=$db->f('qty_p')+$stock_exit;
    $i=0;
    //we have to take  entries

    foreach ($stocks_value_entry as $key => $q) {
        if(($q-$stock_exit)>=0){
            $stocks_value_entry[$key]=$q-$stock_exit;
            break;
        }else{
            $stocks_value_entry[$key]=0;
            $stock_exit= $stock_exit-$q;
        }
        $i++;
    }

      //calculate wac
    foreach ($stocks_value_entry as $key => $q) {
        $price=$db->field("SELECT pim_p_order_articles.price
                              FROM pim_p_order_articles
                              INNER JOIN pim_p_order_deliveries ON pim_p_order_deliveries.p_order_id=pim_p_order_articles.p_order_id
                              WHERE pim_p_order_articles.article_id='".$article_id."' AND  pim_p_order_deliveries.delivery_id='".$key."'");

        $total_stock_value+=$price*$q;
        $total_quantity_purchase+=$q;
    }

    $wac=@($total_stock_value/$total_quantity_purchase);

    $db2->query("UPDATE pim_article_prices SET purchase_price='".$wac."' WHERE base_price=1 AND article_id='".$article_id."'");
}

function build_language_admin_dd($selected = false){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($db->move_next()){
        array_push($array,array('id'=>$db->f('lang_id'), 'name' => gm($db->f('language'))));
    }
    return $array;
}

function build_pdf_type_dd($selected = false,$invoice = false,$quote = false,$timesheet = false,$order = false, $p_order= false){
    $db= new sqldb();

    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);

    $out_str=array();
    $opt=array(
    "Layout 1"      => '1',
    "Layout 2"      => '2',
    "Layout 3"      => '3',
    "Layout 4"      => '4',
    );
    if($timesheet){
        $opt['Layout 5'] = '5';
    }
    if($order){
        $opt['Layout 5'] = '5';

    }if($p_order){
        $opt['Layout 5'] = '5';

    }
    if($invoice){
        $opt['Layout 5'] = '5';
        $opt['Layout 6'] = '6';

        $main_user_id = $db_users->field("SELECT user_id FROM users WHERE main_user_id='0' AND database_name='".DATABASE_NAME."' ");
        $custom_pdf = $db_users->query("SELECT * FROM user_meta WHERE name='custom_pdf_template' AND user_id='".$main_user_id."' ");
        $i=0;
        while($custom_pdf->next()){
            $i++;
            $count = 6+$i;
            $opt['Custom Layout '.$i] = $count;
        }

    }if ($quote) {
        $opt['Layout 5'] = '5';

        // if is custom quote template
        $main_user_id = $db_users->field("SELECT user_id FROM users WHERE main_user_id='0' AND database_name='".DATABASE_NAME."' ");
        $custom_pdf = $db_users->query("SELECT * FROM user_meta WHERE name='custom_pdf_quote_template' AND user_id='".$main_user_id."' ");
        $i=0;
        while($custom_pdf->next()){
            $i++;
            $count = 5+$i;
            $opt['Custom Layout '.$i] = $count;
        }

    } else {
        # nothing to see here
    }

    foreach ($opt as $key=>$val)
    {
        array_push($out_str, array('id'=>$val,'name'=>$key));
    }
    return $out_str;
}

function generate_order_serial_number($database) {
    global $database_config;
    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $account_order_start=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_START' ");
    $account_digit_nr=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DIGIT_NR' ");
    $account_order_digit_nr=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_DIGIT_NR' ");

    $prefix_lenght=strlen($account_order_start)+1;
    $left = $account_digit_nr ? $account_order_digit_nr : '';
    if(!$left){
        $left=1; // ? la invoice $left =0;
    }

    $serial_number_length = $left + strlen($account_order_start);
    $db->query("SELECT pim_orders.order_id,pim_orders.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM pim_orders
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($account_order_start)."' AND LENGTH(serial_number)=".$serial_number_length."
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==$account_order_start){
        $next_serial=$account_order_start.str_pad($db->f('invoice_number')+1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $order_nr = $account_order_start ? $account_order_start : '';
        $next_serial= $order_nr.str_pad(1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}

function build_vat_dd($selected = false){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT value, vat_id FROM vats ORDER BY value");
    while($db->move_next()){
        array_push($array,array('id'=>$db->f('vat_id'),'name' => display_number($db->f('value')).' %', 'vat'=>display_number($db->f('value')) ));
    }
    return $array;
}
function build_vat_regime_old(){
    $db = new sqldb();
    $array = array(
        array('id' => "1" , 'name' => gm("Regular")),
        array('id' => "2" , 'name' => gm("Intra-EU")),
        array('id' => "3" , 'name' => gm("Export")),
        array('id' => "4" , 'name' => gm("Contracting Partner")),
    );
    return $array;    
}
function build_vat_regime_dd(){
    $db = new sqldb();
    $array=array();
    $vat_reg=$db->query("SELECT vat_new.*,vats.value FROM vat_new
        LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
        ORDER BY vat_new.name_id ASC")->getAll();
    foreach($vat_reg as $key=>$value){
        array_push($array,array(
            'id'=>$value['id'],
            'name'=>stripslashes($value['description']),
            'value'=>is_null($value['value']) ? '0.00' : $value['value'],
            'no_vat'=>$value['no_vat'] ? true:false,
            'regime_type'=>$value['regime_type'],
            'regime_id' =>$value['vat_regime_id']));
    }
    return $array;    
}

function get_vat_regime_name($vat_regime_id){
    $db = new sqldb();

    $vat_regime_name = '';

    if($vat_regime_id){
            $vat_regime_name=$db->field("SELECT vat_new.description FROM vat_new
            LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
            WHERE vat_new.id ='".$vat_regime_id."' ");
    }

    return $vat_regime_name;    
}

function build_payment_type_dd($selected='',$txt = false){
    $db = new sqldb();
    $array = array();
    if($txt){
        if($selected){
            return $db->field("SELECT name FROM customer_payment_type WHERE id='".$selected."' ORDER BY sort_order");
        }
        return '';
    }
    $db->query("SELECT name, id FROM customer_payment_type ORDER BY sort_order");
    while($db->move_next()){
        array_push( $array , array('id'=>$db->f('id'), 'name' => $db->f('name')));
    }
    return $array;
}

function build_apply_discount_list($selected ,$text = false){
    if($text === true){
        switch ($selected) {
            case '1':
                $t = gm('Apply at line level');
                break;
            case '2':
                $t = gm('Apply at global level');
                break;
            case '3':
                $t = gm('Apply at both levels');
                break;
            default:
                $t = gm('No Discount');
                break;
        }
        return $t;
    }
    $array = array( array('id'=>'0', 'name' => gm('No Discount')),
                           array('id'=>'1', 'name' => gm('Apply at line level')),
                           array('id'=>'2', 'name' => gm('Apply at global level')),
                           array('id'=>'3', 'name' => gm('Apply at both levels')));
    return $array;
}

function gfn($value){
    return htmlspecialchars(html_entity_decode($value));
}

function gfns($value){
    return html_entity_decode(htmlspecialchars_decode(stripslashes($value)));
}

function get_article_calc_price($article_id,$price_type=1){
   $db = new sqldb();

   $db->query("SELECT pim_article_prices.price,pim_article_prices.total_price,pim_article_prices.purchase_price FROM pim_article_prices
                                 WHERE pim_article_prices.article_id='".$article_id."' and base_price=1

                                 LIMIT 1
                                 ");
    if($price_type==1){
       return $db->f('price');
    }
    if($price_type==2){
       return $db->f('total_price');
    }
     if($price_type==3){
       return $db->f('purchase_price');
    }

}

function return_value2($number){
    $separator = ACCOUNT_NUMBER_FORMAT;
    $out=str_replace(substr($separator, -1), '.', $number);

    return $out;
}

function doManageLog($action='',$db=false,$hub=array()){
    if(!$db){
        $db = DATABASE_NAME;
    }
    global $database_config,$cfg, $config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    if($action){
        //$dbs->query("INSERT INTO users_manage_logs SET database_name='".$db."', action='".$action."', dates='".date('Y-m-d H:i:s')."' ");
    }

    $database_2 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $db,
    );
    $db2 = new sqldb($database_2);
    $hub_vid = $db2->field("SELECT value FROM settings WHERE constant_name='HUBSPOT_VID' ");
    $ac_vid = $db2->field("SELECT value FROM settings WHERE constant_name='ACTIVECAMPAIGN_VID' ");

    if(!empty($hub) && !empty($hub_vid) ){

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        if($hub[0]['property'] == 'first_module_usage'){
            $host_contact = 'https://api.hubapi.com//contacts/v1/contact/vid/'.$hub_vid.'/profile?hapikey='.$cfg['hubspot_api_key'];
            curl_setopt($ch, CURLOPT_URL, $host_contact);
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            $result = json_decode($result);
            $hub[0]['value'] .= ';'.$result->properties->first_module_usage->value;
        }

        $host_update = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$hub_vid.'/profile?hapikey='.$cfg['hubspot_api_key'];

        $post = array('properties'=>$hub);
        $post = json_encode($post);


        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        curl_setopt($ch, CURLOPT_URL, $host_update);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        $result = json_decode($result);
        if($result){
            $dbs->query("INSERT INTO cron SET time=NOW(), db='hubspot', extra='".addslashes(serialize($result))."' ");
        }*/

    }

    if(!empty($hub) && !empty($ac_vid) ){
            $headers=array('Content-Type: application/json','Api-Token: '.$config['activecampaign_api_key']);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            /*if($hub[0]['property'] == 'first_module_usage'){
                $host_contact = 'https://api.hubapi.com//contacts/v1/contact/vid/'.$hub_vid.'/profile?hapikey='.$config['hubspot_api_key'];
                curl_setopt($ch, CURLOPT_URL, $host_contact);
                $result = curl_exec($ch);
                $info = curl_getinfo($ch);
                $result = json_decode($result);
                $hub[0]['value'] .= ';'.$result->properties->first_module_usage->value;
            }*/
            foreach($hub as $key =>$value){

                switch ($value['property']) {
                    case 'trial_activation_link':
                        $field_id = 2;
                    break;
                    case 'referral_name':
                        $field_id = 21;
                    break;
                    case 'referral_code':
                        $field_id = 20;
                    break;
                    case 'referral_snap_id':
                        $field_id = 1;
                    break;
                    case 'subscription_type':
                        $field_id = 22;
                    break;
                    case 'onboarding_completed':
                        $field_id = 34;
                    break;
                }

                $host_update = 'https://akti.api-us1.com/api/3/fieldValues';

                $post = array('fieldValue'=>array(
                    'contact'=> $ac_vid,
                    'field'=> $field_id,
                    'value' =>$value['value'])
                    );
                $post = json_encode($post);

                 //$dbs->query("INSERT INTO activcamp_response SET serialized_glob='".addslashes(serialize($post))."', date='".date("d/m/Y H:i",time())."' ");
                // $dbs->query("INSERT INTO activcamp_response SET serialized_glob='".addslashes($config['activecampaign_api_key'])."', date='".date("d/m/Y H:i",time())."' ");

                //curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

                curl_setopt($ch, CURLOPT_URL, $host_update);
                $result = curl_exec($ch);
                $info = curl_getinfo($ch);
                $err = curl_error($curl);
                // $dbs->query("INSERT INTO activcamp_response SET serialized_glob='".addslashes(serialize($info))."', date='".date("d/m/Y H:i",time())."' ");
                // $dbs->query("INSERT INTO activcamp_response SET serialized_glob='".addslashes(serialize($err))."', date='".date("d/m/Y H:i",time())."' ");

                $result = json_decode($result);
                if($result){
                    $dbs->query("INSERT INTO cron SET time=NOW(), db='active_campaign', extra='".addslashes(serialize($result))."' ");
                }


            }
            
     }




}

function generate_invoice_number($database,$exit_on_error=true){
    global $database_config,$cfg;
    $db_conf = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_conf);

    $total_invoices_nr=$db->field("SELECT COUNT(id) FROM tblinvoice WHERE type='0'");
    $invoice_starting_number=$db->field("SELECT value FROM settings WHERE constant_name='INVOICE_STARTING_NUMBER' ");
    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    if(!$exit_on_error){
        $db->exitOnError=false;
    }   
    $uniqueSN=false;
    //add this to stop infinite loop if reaches 10
    $loop_counter_stop=0;
    while(!$uniqueSN){
        $loop_counter_stop++;
        if(!$exit_on_error && $loop_counter_stop == 10){
            $db->exitOnError=true;
        }
        $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."' AND type!='2'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
        if(!$digit_nr){
            $digit_nr = 0;
        }

        if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
            $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        }else{
            $first_inv_nr=!$total_invoices_nr && !is_null($invoice_starting_number) && (int)$invoice_starting_number>0 ? $invoice_starting_number : 1;
            $next_serial=$invoice_start.str_pad($first_inv_nr,$digit_nr,"0",STR_PAD_LEFT);
        }
        if($exit_on_error){
            $uniqueSN=true;
        }else{
            $serial_number_id=$db->insert("INSERT INTO tblinvoice_serial_numbers SET serial_number='".$next_serial."',type='0' ");
            if($serial_number_id === false){
                sleep(1);
                //continue loop 
            }else{            
                $uniqueSN=true;
            }
        }       
    }
    if(!$exit_on_error){
        $db->exitOnError=true;
    }    
    return $next_serial;
}

function generate_credit_invoice_number($database,$exit_on_error=true){
    if($database){
        global $database_config;
        $db_conf = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );
        $db = new sqldb($db_conf);
    }else{
        $db = new sqldb();
    }   

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CREDIT_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CREDIT_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    if(!$exit_on_error){
        $db->exitOnError=false;
    }    
    $uniqueSN=false;
    //add this to stop infinite loop if reaches 10
    $loop_counter_stop=0;
    while(!$uniqueSN){
        $loop_counter_stop++;
        if(!$exit_on_error && $loop_counter_stop == 10){
            $db->exitOnError=true;
        }
        $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."' AND (type=2 || type=4)
                    ORDER BY invoice_number DESC
                    LIMIT 1");

        if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
            $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        }else{
            $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        }
        if($exit_on_error){
            $uniqueSN=true;
        }else{
            $serial_number_id=$db->insert("INSERT INTO tblinvoice_serial_numbers SET serial_number='".$next_serial."',type='2' ");
            if($serial_number_id === false){
                sleep(1);
                //continue loop 
            }else{            
                $uniqueSN=true;
            }
        }      
    }
    if(!$exit_on_error){
        $db->exitOnError=true;
    }   
    return $next_serial;
}

function generate_proforma_invoice_number($database,$exit_on_error=true){
    global $database_config,$cfg;
    $db_conf = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_conf);

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROFORMA_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROFORMA_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    if(!$exit_on_error){
        $db->exitOnError=false;
    }    
    $uniqueSN=false;
    //add this to stop infinite loop if reaches 10
    $loop_counter_stop=0;
    while(!$uniqueSN){
        $loop_counter_stop++;
        if(!$exit_on_error && $loop_counter_stop == 10){
            $db->exitOnError=true;
        }
        $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."' AND type='1'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
        if(!$digit_nr){
            $digit_nr = 0;
        }
        if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
            $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        }else{
            $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        }
        if($exit_on_error){
            $uniqueSN=true;
        }else{
            $serial_number_id=$db->insert("INSERT INTO tblinvoice_serial_numbers SET serial_number='".$next_serial."',type='1' ");
            if($serial_number_id === false){
                sleep(1);
                //continue loop 
            }else{            
                $uniqueSN=true;
            }
        }
        
    }
    if(!$exit_on_error){
        $db->exitOnError=true;
    }   
    return $next_serial;
}


/**
 * Get the account company name
 *
 * @return void
 * @author
 **/
function get_company_byUser($db,$id){
    $name = $db->field("SELECT ACCOUNT_COMPANY FROM users WHERE user_id='".$id."' ");
    return $name;
}

function build_article_brand_dd($selected='',$view=0){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $e = $db->query("SELECT id,name FROM pim_article_brands ORDER BY name ASC ")->getAll();
    /*array_unshift($e, array('id'=>'','name'=>gm('Select brand')));
    return $e;*/
    $article_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_12' ");
    if($_SESSION['access_level'] == 1 || $article_admin=='article'){
        console::log('eee');
        if($selected=='pim_article_brands'){
            
        }else{
            array_push($e,array('id'=>'0','name'=>""));
        }
    }
    return $e;
}
function build_article_ledger_account_dd($selected='',$view=0){
    $db = new sqldb();
    $e = $db->query("SELECT id,name,description FROM  pim_article_ledger_accounts ORDER BY sort_order ASC")->getAll();
    //console::log($e);
    array_unshift($e, array('id'=>'','name'=>gm('Select Ledger Account'),'description'=>''));
    return $e;
}
function build_article_category_dd($selected='',$view=0){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);  
    $db = new sqldb();
    $e=$db->query("SELECT id,name FROM pim_article_categories ORDER BY name ASC")->getAll();
    /*array_unshift($e, array('id'=>'','name'=>gm('Select article category')));
    return $e;*/
    $article_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_12' ");
    if($_SESSION['access_level'] == 1 || $article_admin=='article'){
        if($selected=='pim_article_categories'){
            
        }else{
            array_push($e,array('id'=>'0','name'=>""));
        }
    }
    return $e;
}

/**
 * undocumented function
 *
 * @return void
 * @author
 **/
function update_articles($art,$db){
    global $database_config;

    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $db,
    );
    $db = new sqldb($database_users);
    if(!is_array($art)){
        $art = array($art);
    }
    foreach ($art as $key => $value) {
        $price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$value."' AND base_price='1' ");
        $article_category = $db->field("SELECT name FROM pim_article_categories WHERE id=(SELECT article_category_id FROM pim_articles WHERE article_id='".$value."') ");
        $photo = $db->field("SELECT parent_id FROM pim_article_photo WHERE parent_id='".$value."' ");
        $update = $db->query("UPDATE pim_articles SET price='".$price."',article_category='".addslashes($article_category)."',parent_id='".$photo."' WHERE article_id='".$value."' ");
    }
}
function insert_product_attribute_value($item_id, $attribute_array, $in,$apply_to){
    if($apply_to==1) //product
    {
        $product_id=$item_id;
        $article_id=0;
    }else{
        $product_id=0;
        $article_id=$item_id;
    }

    $db=new sqldb();

    switch($attribute_array['type'])
    {

        //text_field
        case 1:
            {
                if($in[$attribute_array['field_name']])
                {
                    $db->query("INSERT INTO pim_product_attribute_value ( product_id, article_id, product_attribute_id, product_attribute_set_id, product_attribute_option_id, value, short_text, long_text )
                                                           VALUES
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '',
                                                           '".$in[$attribute_array['field_name']]."',
                                                           '".$in[$attribute_array['field_name']]."',
                                                           ''
                                                           )
                           ");
                }
                return true;
            }break;
            //text_area
        case 2:
            {  if($in[$attribute_array['field_name']])
            {
                $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '',
                                                           '',
                                                           '',
                                                           '".$in[$attribute_array['field_name']]."'
                                                           )
                           ");
            }

            }break;
            //select_box
        case 3:
            {
                if($in[$attribute_array['field_name']])
                {
                    $product_attribute_option_id=$in[$attribute_array['field_name']];
                    $db->query("select value from pim_product_attribute_option where product_attribute_option_id='".$product_attribute_option_id."'");
                    $db->move_next();
                    $value=$db->f('value');
                    $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                            '".$attribute_array['product_attribute_set_id']."',
                                                           '".$product_attribute_option_id."',
                                                           '".$value."',
                                                           '',
                                                           ''
                                                           )
                           ");
                }


            }break;
            //radio_buttons
        case 4:
            {
                if($in[$attribute_array['field_name']])
                {
                    $product_attribute_option_id=$in[$attribute_array['field_name']];
                    $db->query("select value from pim_product_attribute_option where product_attribute_option_id='".$product_attribute_option_id."'");
                    $db->move_next();
                    $value=$db->f('value');
                    $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '".$product_attribute_option_id."',
                                                           '".$value."',
                                                           '',
                                                           ''
                                                           )
                           ");
                }
                return true;

            }break;
            //single_checbox
        case 5:
            {
                if($in[$attribute_array['field_name']])
                {
                    $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '',
                                                           '1',
                                                           '',

                                                           ''
                                                           )
                           ");
                }

            }break;
            //checboxes_block
        case 6:
            {
                if($in[$attribute_array['field_name']])
                {
                    foreach ($in[$attribute_array['field_name']] as $product_attribute_option_id => $value )
                    {
                        $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '".$product_attribute_option_id."',
                                                           '".$value."',
                                                           '',
                                                           ''
                                                           )
                           ");
                    }
                }
                return true;

            }
            break;


    }

}

function build_some_dd($t,$e = false,$d = false){
    switch ($t) {
         case 'tblquote_lost_reason':
            return build_data_qq();
            break;
        case 'customer_payment_type':
            return build_payment_type_dd();
            break;
        case 'pim_article_brands':
            return build_article_brand_dd();
            break;
        case 'pim_article_ledger_accounts':
            return build_article_ledger_account_dd();
            break;
        case 'pim_article_categories':
            return build_article_category_dd();
            break;
        case 'customer_sector':
            return build_s_type_dd($e);
            break;
        case 'customer_legal_type':
            return build_l_type_dd($e);
            break; 
        case 'customer_type':
            return getCustomerType($e);
            break;
        case 'customer_contact_job_title':
            return build_contact_function($e);
            break;
        case 'customer_contact_language':
            return build_language_dd($e);
            break;
        case 'sales_rep':
            return getSales($e);
            break;
        case 'pim_article_tax_type':
            return get_article_tax_type_dd($e);
            break;
        case 'customer_lead_source':
            return build_l_source_dd($e);
            break;
        case 'customer_activity':
            return build_c_activity_dd($e);
            break;
        case 'customer_various1':
            return build_various_dd('','1');
            break;
        case 'customer_various2':
            return build_various_dd('','2');
            break;
        case 'customer_extrafield_dd':
            return build_extra_field_dd('',$e,$d);
            break;
        case 'customer_contact_title':
            return build_contact_title_type_dd($e);
            break;
        case 'customer_contact_job_title':
            return build_contact_job_title_type_dd();
            break;
        case 'customer_contact_dep':
            return build_contact_dep_type_dd($e);
            break;
        case 'contact_extrafield_dd':
            return build_extra_contact_field_dd('',$e,$d);
            break;
        case 'tblinvoice_expense_categories':
            return build_expense_category_dd('',$e,$d);
            break;
        case 'pim_article_variant_types':
            return build_article_variant_types_dd($e);
            break;
        case 'pim_stock_edit_reasons':
            return build_stock_edit_reason_dd();
            break;
    }
}

function build_extra_field_dd($selected = false,$field_id,$d = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $q = strtolower($d);

    if($q!='customer_extrafield_dd'){
      $filter .=" AND name LIKE '".$q."%'";
    }
    $db->query("SELECT * FROM customer_extrafield_dd WHERE extra_field_id='".$field_id."' ".$filter." ORDER BY sort_order ASC");

    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }    
    //$crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
    $crm_adv = aktiUser::get('is_admin_company');
    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($d=='customer_extrafield_dd'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$q));
        }

    }
    // return build_simple_dropdown($array,$selected,'1');
    return $array;

}

function get_extra_dd_value($field_id){
    $db = new sqldb();
    return $db->field("SELECT name FROM customer_extrafield_dd WHERE id='".$field_id."' ");
}

function build_language_dd($selected = false,$view_mode=false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $filter = "1=1";
    $q = strtolower($selected);
/*    if($q!='customer_contact_language'){
        $filter = " AND name LIKE '".$q."%'";
    }*/
    $db->query("SELECT * FROM customer_contact_language WHERE ".$filter." ORDER BY sort_order");
/*    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }*/
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }
    //$crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_contact_language'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$q));
        }
    }

    return $array;
    // return build_simple_dropdown($array,$selected);
}

function build_l_source_dd($selected = false,$join = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $filter = '';
    if($join){
        $filter = " INNER JOIN customers ON customers.lead_source = customer_lead_source.id WHERE customers.active='1' ";
    }
   // $crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    $db->query("SELECT customer_lead_source.* FROM customer_lead_source ".$filter." ORDER BY customer_lead_source.sort_order ASC");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_lead_source'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$q));
        }

    }

    return $array;

}

function build_c_activity_dd($selected = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $q = strtolower($selected);

    $filter = ' 1=1 ';
    if($q!='customer_activity'){
      $filter .=" AND name LIKE '".$q."%'";
    }
    $db->query("SELECT * FROM customer_activity WHERE ".$filter." ORDER BY sort_order ASC");
    //$crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_activity'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$q));
        }

    }

    return $array;

}

function build_various_dd($selected='',$type = '1'){

    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM customer_various".$type." ORDER BY sort_order ASC");
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;

}

function build_l_type_dd($selected = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $q = strtolower($selected);

    $filter = ' 1=1 ';
    if($q!='customer_legal_type'){
      $filter .=" AND name LIKE '".$q."%'";
    }
    //$crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    $db->query("SELECT name, id FROM customer_legal_type WHERE ".$filter." ORDER BY sort_order ASC");
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_legal_type'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$q));
        }

    }

    return $array;
}

function build_s_type_dd($selected = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $q = strtolower($selected);

    $filter = ' 1=1 ';
    if($q!='customer_sector'){
      $filter .=" AND name LIKE '".$q."%'";
    }
    $db->query("SELECT name, id FROM customer_sector WHERE ".$filter." ORDER BY sort_order ASC");
   // $crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_sector'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$q));
        }

    }

    return $array;
}

function getCustomerType($term = false)
{
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $q = strtolower($term);

    $filter = ' 1=1 ';

    if($q!='customer_type'){
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();


    $customer_type = $db->query("SELECT * FROM customer_type WHERE  ".$filter." ORDER BY sort_order ")->getAll();
   // $crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    $users =array();
    foreach ($customer_type as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>htmlspecialchars_decode($value['name'])));
    }
    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($term=='customer_type'){
            
        }else{
            array_push($users,array('id'=>'0','name'=>$q));
        }
    }
    return $users;

}
function getAccountManager($term='')
{
    $q = strtolower($term);

    global $database_config;

    $database_users = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

    $db_users = new sqldb($database_users);

    $filter = '';

    if($q){
        $q = trim($q);
      $filter .=" AND users.first_name LIKE '".$q."%'";
    }

    $u = $db_users->query("SELECT CONCAT_WS(' ',users.first_name, users.last_name) AS name, users.user_id AS id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY name ASC")->getAll();
    $users =array();
    foreach ($u as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>htmlspecialchars_decode(stripslashes($value['name']))));
    }
    
    return $users;
}
function getSales($term = false)
{   
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $q = strtolower($term);

    $filter = '1=1';

    if($q!='sales_rep'){
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();

    $s = $db->query("SELECT * FROM sales_rep WHERE  ".$filter." ORDER BY sort_order ")->getAll();
    //$crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    $users =array();
    foreach ($s as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>htmlspecialchars_decode($value['name'])));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($term=='sales_rep'){
            
        }else{
            array_push($users,array('id'=>'0','name'=>$q));
        }

    }

    return $users;
}

function set_first_letter($table,$value,$field,$id)
{
    $db = new sqldb();
    $value = addslashes(substr(strtoupper($value), 0,1));
    $db->query("UPDATE ".$table." SET first_letter='".$value."' WHERE ".$field."='".$id."' ");
}
function gender_dd(){
    $db = new sqldb();
    $array=array(
/*            array('id' => "1" , 'name' => gm("Select gender")),*/
            array('id' => "1" , 'name' => gm("Male")),
            array('id' => "2" , 'name' => gm("Female")),
       );

    return $array;
}


function build_cat_dd(){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, category_id FROM pim_article_price_category ORDER BY category_id");
    while($db->move_next()){
        // $array[$db->f('category_id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('category_id'),'name'=> $db->f('name') ) );
    }
    return $array;
}

function build_contact_title_type_dd($selected = false,$view_mode=false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $q = strtolower($selected);

    $filter = '1=1';

/*    if($q!='customer_contact_title'){
      $filter .=" AND name LIKE '".$q."%'";
    }*/
    $db->query("SELECT name, id FROM customer_contact_title WHERE ".$filter." ORDER BY sort_order");
/*    if($view_mode === true ){
        while($db->move_next()){
            $array[$db->f('id')]=$db->f('name');
        }
        return $array;
    }*/
    //$crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
           array_push($array, array('id'=>$db->f('id'),'name'=> htmlspecialchars_decode($db->f('name') ) ));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_contact_title'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$selected));
        }

    }

    return $array;
}

function build_contact_function($selected='',$view_mode=false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db= new sqldb();
    $array = array();
    $q = strtolower($selected);

    $filter = '1=1';

/*    if($q!='customer_contact_job_title'){
      $filter .=" AND name LIKE '".$q."%'";
    }*/
    $db->query("SELECT name, id FROM customer_contact_job_title WHERE ".$filter." ORDER BY sort_order ");
/*    if($view_mode === true ){
        while($db->move_next()){
            $array[$db->f('id')]=$db->f('name');
        }
        return $array;
    }*/
   // $crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
           array_push($array, array('id'=>$db->f('id'),'name'=> htmlspecialchars_decode($db->f('name') ) ));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_contact_job_title'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$selected));
        }

    }

    return $array;
}
function build_contact_customers($selected,$view_mode=false){

    $db= new sqldb();
    $array = array();
    $filter = "";
    if($view_mode == true){
        $filter = " WHERE customer_id='".$selected."' AND customers.active=1 ";
    }
    $db->query("SELECT customers.name,customers.customer_id FROM customers ".$filter." ORDER BY customers.name ");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
           array_push($array, array('id'=>$db->f('customer_id'),'name'=> $db->f('name') ) );
    }
    return $array;
}

function build_contact_dep_type_dd($selected='',$view_mode=false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
    $q = strtolower($selected);

    $filter = '1=1';


    // $filter = "";
    // if($view_mode == true){
    //     $filter = " WHERE id='".$selected."' ";
    // }
    //$crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    $db->query("SELECT name, id FROM customer_contact_dep WHERE ".$filter." ORDER BY sort_order");
/*    if($view_mode === true ){
        while($db->move_next()){
            $array[$db->f('id')]=$db->f('name');
        }
        return $array;
    }*/
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=> htmlspecialchars_decode($db->f('name') ) ));
    }

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_contact_dep'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$selected));
        }

    }

    return $array;
}

function build_extra_contact_field_dd($selected = false,$field_id = false,$d = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $db = new sqldb();
    $array = array();
     $q = strtolower($d);

    if($q!='contact_extrafield_dd'){
      $filter .=" AND name LIKE '".$q."%'";
    }
   // $crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    $db->query("SELECT * FROM contact_extrafield_dd WHERE extra_field_id='".$field_id."' ".$filter." ORDER BY sort_order ");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }    

    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='contact_extrafield_dd'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$selected));
        }

    }

    // return build_simple_dropdown($array,$selected,'1');
    return $array;

}

function build_contact_job_title_type_dd($view_mode=false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);

    $q = strtolower($selected);

    $db = new sqldb();
    $array = array();
    $filter = "";
    $values = explode(',', $selected);
    $selected = '';
    foreach ($values as $key) {
        if(is_numeric($key)){
            $selected .=$key.',';
        }
    }
    $selected = rtrim($key,',');
    if($view_mode === true && $selected && $selected != "undefined"){

        $filter = " WHERE id IN (".$selected.") ";
    }
   // $crm_adv = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
      $crm_adv = aktiUser::get('is_admin_company');
    $db->query("SELECT name, id FROM customer_contact_job_title ".$filter." ORDER BY sort_order");
    if($view_mode === true && $selected){
        $name = '';
        while ($db->next()) {
            $name .= $db->f('name').',';
        }
        return rtrim($name,',');
    }
    if($view_mode === true && !$selected){
        $name = '';
        return $name;
    }
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
       // array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
        array_push($array, array('id'=>$db->f('id'),'name'=> htmlspecialchars_decode($db->f('name') ) ));
    }
    if($_SESSION['access_level'] == 1 || $crm_adv=='company'){
        if($selected=='customer_contact_job_title'){
            
        }else{
            array_push($array,array('id'=>'0','name'=>$selected));
        }

    }
    return $array;
    // return build_simple_dropdown($array,$selected);
}

function build_customer_dd(){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, customer_id FROM customers ");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('customer_id'),'name'=>$db->f('name')));
    }
    return $array;
}

function get_state_name($state_id)
{
    $db=new sqldb();
    $db->query("SELECT state.*  FROM state WHERE state_id='".$state_id."'");
    $db->move_next();
    $name=$db->f('name');
    return $name;
}

function unique_id($l = 4) {
    return substr(md5(uniqid(mt_rand(), true)), 0, $l);
}


function build_data_qq($term='')
{

    $q = strtolower($term);

    $filter = '1=1';

    if($q){
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();
    $s = $db->query("SELECT * FROM tblquote_lost_reason WHERE  ".$filter." ORDER BY name ASC ")->getAll();
    $users =array();
    foreach ($s as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>htmlspecialchars_decode($value['name'])));
    }
    
    return $users;
}
function build_data_dd($table_name,$selected='',$field='name',$opt = array()){
    $data = get_data($table_name,'',$field,$opt);
    $array= array();
    if(is_array($data)){
        foreach ($data as $key => $value) {
            array_push($array, array('id'=>$key,'name'=>$value));
        }
    }
    return $array;
}

function get_data($table_name='', $selected = '', $field ='name', $opt = array()){
    if($table_name == ''){
        return array();
    }
    $dbu = new sqldb();
    $id = isset($opt['id']) ? $opt['id'] : $table_name.'_id';
    $cond = isset($opt['cond']) ? $opt['cond'] : '';
    $selfield = isset($opt['field']) ? $opt['field'] : $field;

    $dbu->query('SELECT '.$id.','.$field.' FROM '.$table_name.' '.$cond);
    $ret_val = array();
    while ($dbu->move_next()){
        $ret_val[$dbu->f($id)] = $dbu->f($selfield);
    }
    return $selected!='' ? $ret_val[$selected] : $ret_val;
}

function build_quote_lost_reason_dd($selected = false){
    $db = new sqldb();
    $lost_reasons = $db->query("SELECT id, name FROM tblquote_lost_reason ORDER BY sort_order ASC ")->getAll();
    array_push($lost_reasons, array('id' => "0" , 'name' =>  ''));
    return $lost_reasons;
}

function get_customer_vat($customer_id,$daba='')
{
    $db=new sqldb();
    if(!empty($daba)){
        global $database_config;
        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $daba,
        );
        $db=new sqldb($db_config);
    }
    $vat_value='';
    if($customer_id){
            $vat_regime_id =  $db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$customer_id."'");
            if(!$vat_regime_id){
                $vat_regime_id = $db->field("SELECT id FROM vat_new WHERE `default`='1'");
                if(empty($vat_regime_id)){
                  $vat_regime_id = 1;
                }               
            }
        /*$db->query("SELECT customers.customer_id,customers.vat_id,vats.value
                    FROM customers
                    INNER JOIN vats ON vats.vat_id=customers.vat_id
                    WHERE customers.customer_id='".$customer_id."'");*/
         $vat_value=$db->field("SELECT vats.value FROM vat_new 
                        LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
                        WHERE vat_new.id='".$vat_regime_id."' ");
        /*$db->move_next();
        $vat_value=$db->f('value');*/
    }
 /*   if(!$vat_value){
        $vat_value = $db->field("SELECT value FROM vats WHERE value='".ACCOUNT_VAT."' ");
    }*/
    return display_number($vat_value);
}

function get_custom_width($all_custom_widths,$can_be_hidden_custom_widths,$custom_line_1,$custom_line_2,$custom_line_3=false,$custom_line_4=false,$custom_line_5=false,$custom_line_6=false){
    // $all_custom_widths - array with all the columns widths that can be shown on the same time (except the variable one)
    // $can_be_hidden_custom_widths - array with all the columns widths that can be hidden
    // $custom_line_1 , $custom_line_2, $custom_line_3, $custom_line_4 - the state of the column (shown = true , hidden = false)
    $total_width = 100;
    $total_fixed_width = 0;
    foreach ($all_custom_widths as $key => $value) {
        $total_fixed_width += $value;
    }
    $fixed_diff = $total_width - $total_fixed_width;
    $variable_diff = 0;
    for ($i=1; $i <= 6; $i++) {
        if(func_get_arg($i+1) == false){
            $variable_diff += $can_be_hidden_custom_widths['custom_line_'.$i];
        }
    }
    $custom_width = $fixed_diff+$variable_diff;
    return $custom_width;
}

function place_currency_new($out,$currency = '',$position='',$currency_font_face=''){


    if(!$currency){
        $currency = get_currency(ACCOUNT_CURRENCY_TYPE);
    }
    if($currency_font_face){
        $currency = '<font face="'.$currency_font_face.'">'.$currency.'</font>';
    }
    if($position==1){
        $out = $out.' '.$currency;
    }else{
        if(ACCOUNT_CURRENCY_FORMAT == 0){
            $out = $out.' '.$currency;
        }
        else {
            $out = $out.' '.$currency;
        }
    }
    return $out;


}

function generate_quote_number($database)
{
    global $database_config;

    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $prefix_lenght=strlen(ACCOUNT_QUOTE_START)+1;
    $left = ACCOUNT_QUOTE_DIGIT_NR;
    $db->query("SELECT tblquote.id,tblquote.serial_number, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS quote_number,LEFT( serial_number, $prefix_lenght-1 ) AS quote_prefix, LENGTH(SUBSTRING(serial_number,$prefix_lenght)) as length
                    FROM tblquote
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes(ACCOUNT_QUOTE_START)."'
                    HAVING length = '".$left."'
                    ORDER BY quote_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('quote_prefix')==ACCOUNT_QUOTE_START){
        $next_serial=ACCOUNT_QUOTE_START.str_pad($db->f('quote_number')+1,$left,"0",STR_PAD_LEFT);
    }else{
        $next_serial=ACCOUNT_QUOTE_START.str_pad(1,$left,"0",STR_PAD_LEFT);
    }
    return $next_serial;

}

function build_user_dd($selected=''){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb($db_config);

    $db->query("SELECT users.user_id, user_meta.value, user_meta.name FROM users LEFT JOIN user_meta ON users.user_id=user_meta.user_id AND user_meta.name='active' WHERE database_name='".DATABASE_NAME."' AND active='1' ORDER BY users.first_name");
    while ($db->next()) {
        $active = $db2->field("SELECT name FROM  user_meta WHERE user_id='".$db->f('user_id')."' AND name='active' AND ( user_meta.value =  '1' OR user_meta.value IS NULL )");
        if(!$active){
            $array[$db->f('user_id')] = get_user_name($db->f('user_id'));
        }
    }
    if(!$selected){
        $selected = 0;
    }
    return build_simple_dropdown($array,$selected);
}
function time_ago($timestamp,$now){
    $dif = $now - $timestamp;
    if($dif < 60 ){
        $out = gm('a few seconds ago');
    }else if($dif < 3600){
        $time = floor($dif/60);
        $out = $time.' '.gm('minutes ago');
        if($time == 1){
            $out = $time.' '.gm('minute ago');
        }
    }else if($dif < 86400){
        $time = floor($dif/3600);
        $out = $time.' '.gm('hours ago');
        if($time == 1){
            $out = $time.' '.gm('hours ago');
        }
    }/*else if($dif < 2592000){
        $time = floor($dif/86400);
        $out = $time.' '.gm('days ago');
        if($time == 1){
            $out = $time.' '.gm('days ago');
        }
    }*/else{
        /*$time = floor($dif/2592000);
        $out = $time.' '.gm('months ago');
        if($time == 1){
            $out = $time.' '.gm('months ago');
        }*/
        $out = date(ACCOUNT_DATE_FORMAT,$timestamp);
    }
    return $out;
}

function get_last_invoice_number($only_serial = false,$dbs='')
{
    if($dbs){
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $dbs,
        );
        $db = new sqldb($database_users);
    }else{
        $db=new sqldb();
    }

    /*$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;

    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number FROM tblinvoice WHERE f_archived='0' AND serial_number!='' AND type !='1' AND type !='2' ORDER BY tblinvoice.id DESC LIMIT 1");*/

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_START' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."' AND type!='2' AND type !='1' AND f_archived='0' AND serial_number!='' 
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next()){
        $last_serial='('.gm("Last Used").': '.$db->f('serial_number').')';
    }else{
        $last_serial='';
    }
    if($only_serial===true){
        $last_serial=str_replace($invoice_start, '', $db->f('serial_number'));
    }
    return $last_serial;
}

function build_sepa_mandate_dd($selected = false, $buyer, $active = false)
{
    $db= new sqldb();

    $array=array();
    $active_filter = '';
    $i=0;

    if($active){
        $active_filter = " AND active_mandate ='1' ";
    }
    $db->query("SELECT * FROM mandate WHERE buyer_id='".$buyer."' ".$active_filter." ");

    while($db->move_next()){
       $i++;
       array_push($array, array('id'=>$db->f('mandate_id'),'name'=>$db->f('sepa_number')));
    }
    if($i==0 && $active){
         array_push($array, array('id'=>0,'name'=>gm("No mandate configured yet") ));
    }

    return $array;
}

function update_log_message($id,$message = '',$replace = false)
{
    if(!$message){
        return $id;
    }
    $db = new sqldb();
    if($replace){
        $oldMessage = '';
    }else{
        $oldMessage = $db->field("SELECT message FROM logging WHERE log_id='".$id."' ").',';
    }
    $db->query("UPDATE logging SET message='".addslashes($oldMessage.$message)."' WHERE log_id='".$id."' ");
    return $id;
}

function get_last_credit_invoice_number()
{
    $db=new sqldb();

    /*$prefix_lenght=strlen(ACCOUNT_CREDIT_INVOICE_START)+1;

    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number FROM tblinvoice WHERE f_archived='0' AND type ='2' ORDER BY tblinvoice.id DESC LIMIT 1");*/

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CREDIT_INVOICE_START' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."' AND f_archived='0' AND type ='2'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next()){
        $last_serial='('.gm("Last Used").': '.$db->f('serial_number').')';
    }else{
        $last_serial='';
    }

    return $last_serial;
}

function get_article_label($article_id,$language_id=DEFAULT_LANG_ID,$module, $is_tax = false){

    $db=new sqldb();
    if(!$language_id){
        $language_id=1;
    }
    if($module=='invoice'){
        $fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='INVOICE_FIELD_LABEL'");
    }
    if($module=='order'){
        $fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='ORDER_FIELD_LABEL'");
    }
   
    $filter.=" AND pim_articles_lang.lang_id='".$language_id."'";
   
    $table = 'pim_articles INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
                           LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id  '.$filter.'
                           LEFT JOIN pim_article_brands ON pim_article_brands.id = pim_articles.article_brand_id';
    $columns = 'pim_articles.*,pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
                pim_article_prices.price, pim_articles.internal_name,
                pim_article_brands.name AS article_brand,
                pim_articles_lang.description AS description,
                pim_articles_lang.name2 AS item_name2,
                pim_articles_lang.name AS item_name,
                pim_articles_lang.lang_id,
                pim_articles.vat_id,
                pim_articles.block_discount';

   

//echo "SELECT $columns FROM $table WHERE $filter AND  pim_articles.article_id='".$article_id."' ";
    if ($is_tax){
         $article = $db->query("SELECT pim_article_tax.code as item_code, pim_article_tax_type.name as internal_name FROM pim_article_tax
         INNER JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id WHERE  pim_article_tax.tax_id='".$article_id."' ");

    }else{
         $article = $db->query("SELECT $columns FROM $table WHERE  pim_articles.article_id='".$article_id."' ");

    }
   

    while($article->next()){

    //$article->move_next();
    $values = $article->next_array();


        $tags = array_map(function($field){
            return '/\[\!'.strtoupper($field).'\!\]/';
        },array_keys($values));

        $label = preg_replace($tags, $values, $fieldFormat);

    return gfn($label);
    }
}

function generate_ogm($db='',$invoice_id=0)
{
    if(!$db){
        $dbs = new sqldb();
    }else{
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $db,
        );
        $dbs = new sqldb($database_users);
    }
    $result = '';
    $o = $dbs->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION' ");
    $i = $dbs->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION_NR' ");
    if($o){
        switch ($o) {
            case 1:
                $nr = time();
                if($invoice_id){
                    $nr+=$invoice_id;
                }
                // $first_part = substr($nr,0,4);$second_part = substr($nr,4,4); $last_part = substr($nr,8,2);
                $first_part = substr($nr,0,3);$second_part = substr($nr,3,4); $last_part = substr($nr,7,3);
                $calc =$first_part.$second_part.$last_part;
                $calc = $calc%97;
                if($calc < 10){
                    if($calc==0){
                        $calc='97';
                    }else{
                        $calc = '0'.$calc;
                    }
                }
                // $s = strpos($calc,'.');
                // $calc = substr($calc,$s+1,2);
                $result = $first_part.'/'.$second_part.'/'.$last_part.$calc;
                break;
            case 2:
                $nr = $i;$nr2=get_last_invoice_number(true,$db);$nr3='';$calc=0;
                if(strlen($nr) > 3){
                    $nr2 = substr($nr,3).$nr2;
                    $nr= substr($nr,0,3);
                }else{
                    $l = 3-strlen($nr);
                    $nr.=substr($nr2,0,$l);
                    $nr2 = substr($nr2,$l);
                }
                if(strlen($nr2) > 4){
                    $nr3 = substr($nr2,4);
                    $nr2 = substr($nr2,0,4);
                }else{
                    $l = 4-strlen($nr2);
                    for($i=1;$i<=$l;$i++){
                        $nr2 .= '0';
                    }
                }
                if(strlen($nr3) < 3){
                    $l = 3-strlen($nr3);
                    for($i=1;$i<=$l;$i++){
                        $nr3 .= '0';
                    }
                }
                $calc= $nr.$nr2.$nr3;
                $calc = $calc%97;
                if($calc < 10){
                    if($calc==0){
                        $calc='97';
                    }else{
                        $calc = '0'.$calc;
                    }
                }
                console::log($calc);
                // $s = strpos($calc,'.');
                // $calc = substr($calc,$s+1,2);
                $result = $nr.'/'.$nr2.'/'.$nr3.$calc;

                break;
        }
    }
    return $result;
}
function build_language_ecom_dd($selected = false){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM pim_lang WHERE active='1' and front_active='1' ORDER BY sort_order");
    while($db->move_next()){
        $array[$db->f('lang_id')] = gm($db->f('language'));

    }
    return build_simple_dropdown($array,$selected);
}

function build_acc_manager(){
    global $database_config;
    $database_users = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );
    $users = array();
    $db_users = new sqldb($database_users);
    $db_users->query("SELECT CONCAT_WS(' ',first_name, last_name) as name FROM users WHERE database_name='".DATABASE_NAME."' and active!='1000' order by first_name ");
    while ($db_users->move_next()) {
         array_push($users, array('id'=>stripslashes($db_users->f('name')), 'value'=>stripslashes($db_users->f('name')) ));   
    }
    // ksort($users);
    return $users;
}    

function build_c_type_name_dd(){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, id FROM customer_type ORDER BY sort_order");
    while($db->move_next()){
         array_push($array, array('id'=>$db->f('name'),'value'=>$db->f('name')));   
    }
    return $array;    
}

// country exists
function company_country_list($view_mode=false)
{
    $db=new sqldb();
     
    $filter = " WHERE 1=1 ";
    if($view_mode === true){
        $filter = " WHERE country_id='".$selected."' ";
    }
    $db->query("SELECT country.* FROM country
                INNER JOIN customer_addresses ON country.country_id=customer_addresses.country_id
                INNER JOIN customers ON customers.customer_id = customer_addresses.customer_id
                ".$filter." AND customers.active='1' AND front_register='0' GROUP by country.country_id ORDER BY name");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    $array=array();
    while($db->move_next()){
         array_push($array, array('id'=>$db->f('name'),'value'=>$db->f('name')));           
    }
    return $array;

}

function get_contact_nr($customer){
    $db=new sqldb();
    $nr = $db->field("SELECT COUNT(customer_contactsIds.contact_id) FROM customer_contactsIds
        INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id WHERE customer_contactsIds.customer_id='".$customer."' and customer_contacts.active='1' ");
    return $nr;

}

function build_payment_methods_list($selected = false){
    $array=array();
    $db=new sqldb();
    $stripe_active = $db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
    $allow_sepa = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_SEPA' ");

   /* $adv_sepa=false;
    if(defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1){
        if(defined('ADV_SEPA') && ADV_SEPA==1 && defined('SHOW_INVOICE_SEPA') && SHOW_INVOICE_SEPA==1){
            $adv_sepa=true;
        }
    }else if(defined('SHOW_INVOICE_SEPA') && SHOW_INVOICE_SEPA==1){
        $adv_sepa=true;
    }*/

    $texts=array(1=>gm('Direct Transfer'),2=>gm('Stripe'),3=>gm('SEPA'));
    foreach($texts as $key => $val){
        if($key==1 || ($key==2 && $stripe_active) || ($key==3 && $allow_sepa) ){
            array_push($array, array('id'=>$key,'name'=>$val));
        }
       
    }  
    return $array;
}

function build_frequency_list($selected = false){
    $array=array();
    $texts=array(1=>gm('Weekly'),2=>gm('Monthly'),3=>gm('Quarterly'),4=>gm('Yearly'),5=>gm('Custom'));
    foreach($texts as $key => $val){
        array_push($array, array('id'=>$key,'name'=>$val));
    }
    
    return $array;
}
function build_frequency_list_new($selected = false){
    $array=array();
    $texts=array(1=>gm('Weekly'),2=>gm('Monthly'),3=>gm('Quarterly'),6=>gm('Bi-Annual'),4=>gm('Yearly'),7=>gm('Once every 2 years'),5=>gm('Custom'));
    foreach($texts as $key => $val){
        array_push($array, array('id'=>$key,'name'=>$val));
    }
    
    return $array;
}
function generate_customer_number($database,$first_reference=false)
{
    global $database_config,$cfg;
    $db_conf = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_conf);

    //$company_start = 0;

    /*$company_start=$db->field("SELECT value FROM settings WHERE constant_name='".$first_reference."' ");*/
    $company_start=$first_reference;
    if($company_start){
        $db->query("SELECT customers.customer_id,customers.our_reference
                    FROM customers
                    WHERE our_reference='".addslashes($company_start)."'
                    ORDER BY our_reference DESC
                    LIMIT 1
                   ");
        if(!$db->move_next()){
            //return $company_start;
        }else{
            $customer_main_id=$db->field("SELECT customers.customer_id
                    FROM customers
                    WHERE our_reference='".addslashes($company_start)."'

                   ");
            $last_customer_id=$db->field("SELECT customers.customer_id
                    FROM customers
                    ORDER BY customer_id DESC
                    LIMIT 1

                   ");

            // return ($last_customer_id-$customer_main_id)+$company_start+1;
            $company_start=($last_customer_id-$customer_main_id)+$company_start+1;
        }
    }

    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_COMPANY_DIGIT_NR' ");

    $prefix_lenght=strlen($company_start)+1;
    $db->query("SELECT customers.customer_id,customers.our_reference,CAST(SUBSTRING(our_reference,$prefix_lenght)AS UNSIGNED) AS company_number,LEFT( our_reference, $prefix_lenght-1 ) AS company_prefix
                    FROM customers
                    WHERE SUBSTRING(our_reference,1,$prefix_lenght-1)='".addslashes($company_start)."'
                    ORDER BY company_number DESC
                    LIMIT 1");

    if(!$digit_nr){
        $digit_nr = 0;
    }

    // console::log($db->f('invoice_prefix'),$invoice_start,$db->f('invoice_number'),$digit_nr);
    if($db->move_next() && $db->f('company_prefix')==$company_start){
        $next_serial=str_pad($db->f('company_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;//+1;
    }else{
        if($company_start){
             $next_serial=str_pad($company_start,$digit_nr,"0",STR_PAD_LEFT);
        }else{
             $next_serial=str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        }   
        return $next_serial;
    }
}

function update_q_price($article_id,$price,$vat_id){
       $db = new sqldb();
       $db2 = new sqldb();
      $vat_value   = $db->field("SELECT value from vats WHERE vat_id='".$vat_id."'");
      $db->query("SELECT * FROM  pim_article_prices WHERE article_id='".$article_id."' AND base_price=0 and percent!='0.00'");
        while ($db->move_next()) {
               $total_price=return_value($price) - (return_value($price)*$db->f('percent')/100);
               $total_with_vat = $total_price+($total_price*$vat_value/100);
               $db2->query("UPDATE pim_article_prices SET
                                                            price           = '".$total_price."',
                                            total_price     = '".$total_with_vat."'
                          WHERE article_price_id='".$db->f('article_price_id')."'

                            ");
        }

}
function generate_serial_number($database)
{
    global $database_config;

    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $prefix_lenght=strlen(ACCOUNT_P_ORDER_START)+1;
    $left = defined('ACCOUNT_DIGIT_NR') ? ACCOUNT_P_ORDER_DIGIT_NR : '';
    if(!$left){
        $left=1;
    }
    $db->query("SELECT pim_p_orders.p_order_id,pim_p_orders.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS SIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM pim_p_orders
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes(ACCOUNT_P_ORDER_START)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==ACCOUNT_P_ORDER_START){
        $next_serial=ACCOUNT_P_ORDER_START.str_pad($db->f('invoice_number')+1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $order_nr = defined('ACCOUNT_P_ORDER_START') ? ACCOUNT_P_ORDER_START : '';
        $next_serial= $order_nr.str_pad(1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function generate_aac_price($article_id,$first=0){
 
   $db = new sqldb();
   $db2 = new sqldb();

   //we select al entries for these article
   $db->query("SELECT pim_p_orders.add_cost,pim_p_orders.amount,pim_p_order_articles.article_id,pim_p_order_articles.quantity,pim_p_order_articles.price
               FROM pim_p_order_articles
               INNER JOIN pim_p_orders on pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
               WHERE article_id='".$article_id."'

    ");
    
 
  
   while($db->move_next()) {
       
       $ratio=($db->f("quantity")*$db->f("price")/($db->f("amount")- $db->f("add_cost")))*100;
       $distributed_cost+=$db->f("add_cost")*$ratio/100;
       $total_q+=$db->f("quantity");
      

      
       //$db2->query("UPDATE  pim_articles SET aac_price='".$aac_price."' WHERE  article_id='".$article_id."'");
       
   }
   /*$aac_price=$distributed_cost/$total_q;*/
    $aac_price=number_format($aac_price, 2, '.', '');
   $db2->query("UPDATE  pim_articles SET aac_price='".$aac_price."' WHERE  article_id='".$article_id."'");


}

function check_if_activity_exists($customer_id='',$contact_id=''){
    $db = new sqldb();
    $now = time();
    if(!$in['user_id'])
    {
        $in['user_id'] = $_SESSION['u_id'];
    }
    $limit = "";
    $filter_c = '';
    $filter_c2 = '';
    $filter_l = '';
    $filter_q = '';
    if($customer_id){
        $in['customer_id'] = $customer_id;
    }
    if($contact_id){
        $in['contact_id'] = $contact_id;
    }
    if($in['customer_id'])
    {
        $filter_c = "AND customer_id='".$in['customer_id']."'";
        $filter_c2 = " AND customer_id='".$in['customer_id']."'";
    }
    if($in['contact_id2']){
        $in['contact_id'] = $in['contact_id2'];
    }
    if($in['contact_id']){
        $filter_c .= " AND contact_id='".$in['contact_id']."' ";
    }
    $i=0;
    $comments = $db->query("SELECT customer_contact_activity.* FROM customer_contact_activity WHERE email_to='{$in['user_id']}' {$filter_c} ORDER BY date DESC {$limit} ");
    // console::log("SELECT customer_contact_activity.* FROM customer_contact_activity WHERE email_to='{$in['user_id']}' {$filter_c} ORDER BY date DESC {$limit} ");
    $in['has_activities'] = false;
    if($comments->next()){
        // console::log('first');
        $in['has_activities'] = true;
    }else{
        if($in['contact_id']){
            $comments2 = $db->query("SELECT customer_contact_activity.*, customer_contact_activity_contacts.contact_id AS contact_id2
                FROM customer_contact_activity
                LEFT JOIN customer_contact_activity_contacts
                ON customer_contact_activity.customer_contact_activity_id = customer_contact_activity_contacts.activity_id
                WHERE email_to='{$in['user_id']}' {$filter_c2} AND customer_contact_activity_contacts.contact_id = '".$in['contact_id']."' ORDER BY date DESC {$limit} ");
            if($comments2->next()){
                // console::log('second');
                $in['has_activities'] = true;
            }
        }
    }
    return $in['has_activities'];
}
function payment_method_dd($selected = false){
    $array = array(
        array('id' => "0" , 'name' => gm("Select")),
        array('id' => "1" , 'name' => gm("Cash")),
        array('id' => "2" , 'name' => gm("Bank transfer")),
        array('id' => "3" , 'name' => gm("Credit Card")),
        array('id' => "4" , 'name' => gm("Cheque")),
    );
    return $array;    
}
function payment_method_dd_pi($selected = false){
    $array = array(
        array('id' => "0" , 'name' => gm("Select")),
        array('id' => "1" , 'name' => gm("Cash")),
        array('id' => "2" , 'name' => gm("Bank transfer")),
        array('id' => "3" , 'name' => gm("Credit Card")),
        array('id' => "4" , 'name' => gm("Cheque")),
        array('id' => "5" , 'name' => gm("Automatic payment")),
    );
    return $array;    
}

function get_vat_id($value){
    $db= new sqldb();
    $vat = $db->field("SELECT vat_id FROM vats WHERE value='".number_format($value, 2, '.', '')."'");
    return $vat;
}
function get_article_vat($id){
    $db= new sqldb();
    $vat_id = $db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$id."' ");
    return get_vat_val($vat_id);
}

function get_vat_val($id){
    $db= new sqldb();
    $vat = $db->field("SELECT value FROM vats WHERE vat_id='".$id."'");
    return $vat;
}

function generate_binvoice_number($db_name='')
{
    if($db_name){
        global $database_config;
        $database_3 = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $db_name,
        );
        $db = new sqldb($database_3);
    }else{
        $db = new sqldb();
    }

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT tblinvoice_incomming.invoice_id,tblinvoice_incomming.booking_number,CAST(SUBSTRING(booking_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( booking_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice_incomming
                    WHERE SUBSTRING(booking_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    // console::log($db->f('invoice_prefix'),$invoice_start,$db->f('invoice_number'),$digit_nr);
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function generate_binvoice_credit_number($db_name='')
{
    if($db_name){
        global $database_config;
        $database_3 = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $db_name,
        );
        $db = new sqldb($database_3);
    }else{
        $db = new sqldb();
    }   

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT tblinvoice_incomming.invoice_id,tblinvoice_incomming.booking_number,CAST(SUBSTRING(booking_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( booking_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice_incomming
                    WHERE SUBSTRING(booking_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    // console::log($db->f('invoice_prefix'),$invoice_start,$db->f('invoice_number'),$digit_nr);
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function build_incinv_type_dd(){
    
    $array = array(
        array('id' => "0" , 'name' => gm("Invoice")),
        array('id' => "1" , 'name' => gm("Proforma")),
        array('id' => "2" , 'name' => gm("Credit Invoice")),
    );
    return $array;    
}

function build_invoice_type_dd(){
    
    $array = array(
        array('id' => "-1" , 'name' => gm("All types")),
        array('id' => "0" , 'name' => gm("Regular Invoice")),
        array('id' => "1" , 'name' => gm("Proforma Invoice")),
        array('id' => "2" , 'name' => gm("Credit Invoice")),
    );
    return $array;    
}
function twoDecNumber($number,$separator = '.'){
    $number = str_replace(',', '', $number);
    return number_format($number,2,'.','');
    return substr($number, 0, strpos($number,$separator)+3);
}
function varDecNumber($number,$var){
    $number = str_replace(',', '', $number);
    return number_format($number,$var,'.','');
}
function build_coda_dd($selected = false){
    $array = array(
        array('id'=>'00','name'=>'0%'),
        array('id'=>'01','name'=>'6%'),
        array('id'=>'02','name'=>'12%'),
        array('id'=>'03','name'=>'21%'),
        array('id'=>'45','name'=>'0% Contractor'),
        array('id'=>'44','name'=>'0% ICD Services B2B'),
        array('id'=>'NA','name'=>'0% Sundries excluded from VTA'),
        array('id'=>'00/44','name'=>'0% Clause 44'),
        array('id'=>'46/GO','name'=>'0% ICD Goods'),
        array('id'=>'47/TO','name'=>'0% ICD Manufacturing cost'),
        array('id'=>'47/AS','name'=>'0% ICD Assembly'),
        array('id'=>'47/DI','name'=>'0% ICD Distance'),
        array('id'=>'47/SE','name'=>'0% ICD Services'),
        array('id'=>'46/TR','name'=>'0% ICD Triangle a-B-c'),
        array('id'=>'47/EX','name'=>'0% Export non E.U.'),
        array('id'=>'47/EI','name'=>'0% Indirect export'),
        array('id'=>'47/EE','name'=>'0% Export via E.U.'),
        array('id'=>'03/SE','name'=>'0% Standard exchange'));
    return $array;
}
function build_yuki_dd($selected = false){
    $array = array(
        array('id'=>'100','name'=>'0% VAT exempt'),
        array('id'=>'1','name'=>'High VAT'),
        array('id'=>'2','name'=>'Low VAT'),
        array('id'=>'4','name'=>'VAT 0%'),
        array('id'=>'6','name'=>'0% Exports outside the EU'),
        array('id'=>'7','name'=>'0% Exports within the EU'),
        array('id'=>'8','name'=>'Country Specific Installation / distance sales within the EU'),
        array('id'=>'17','name'=>'0% VAT shifted'),
        array('id'=>'19','name'=>'0% EU exports of goods ( delivery in 2010 )'),
        array('id'=>'20','name'=>'0% EU export services ( services in 2010 )'));
    return $array;
}

function build_exact_dd($selected = false){
    $array = array(
        array('id'=>'0','name'=>'Not on the VAT return'),
        array('id'=>'0%','name'=>'National 0% on VAT'),
        array('id'=>'1','name'=>'National exclude 6%'),
        array('id'=>'1I','name'=>'National inclusive 6%'),
        array('id'=>'3','name'=>'National exclude 12%'),
        array('id'=>'3I','name'=>'National inclusive 12%'),
        array('id'=>'5','name'=>'National exclude 21%'),
        array('id'=>'5I','name'=>'National inclusive 21%'),
        array('id'=>'A','name'=>'Sales contractant 0%'),
        array('id'=>'B','name'=>'Intracom delivery 0%'),
        array('id'=>'E','name'=>'Export 0%'),
        array('id'=>'F','name'=>'Sales service EC 0%'),
        array('id'=>'MAR','name'=>'VAT on the margin 0%')
    );
    return $array;
}
function build_match_field_dd_contacts($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array = array();
    $final_array=array();
    $db->query("SELECT label_name, import_label_id,custom_field,field_name,group_name,field_name FROM import_labels WHERE type = 'contact' ORDER BY import_label_id ASC");
    // $array['9998'] = gm('Contact');
     $out_str=array();
    while($db->move_next()){
        if($db->f('field_name')=='firstname'){
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm('First Name Contact'),
                'field_name'=>$db->f('field_name')
            ));
        }elseif($db->f('field_name')=='lastname'){
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm('Last Name Contact'),
                'field_name'=>$db->f('field_name')
            ));
        }
        elseif($db->f('custom_field')==1 || $db->f('field_name')=='various1' || $db->f('field_name')=='various2')
        {
            /*$array[$db->f('import_label_id')] = $db->f('label_name');*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name')),
                'field_name'=>$db->f('field_name')
            ));
        }else
        {
            /*$array[$db->f('import_label_id')] = gm($db->f('label_name'));*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name')),
                'field_name'=>$db->f('field_name')
            ));
        }
    }

    foreach($out_str as $key=>$value){
        if($value['field_name'] == 'contact_id'){
            array_unshift($final_array,$value);
        }else{
            array_push($final_array,$value);
        }         
    }
    //return $out_str;
    return $final_array;
}
function build_match_field_dd_company($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name,field_name FROM import_labels WHERE type = 'customer' AND field_name!='firstname' AND label_name!='Last Name' AND field_name!='vat_id' AND field_name!='internal_language' ORDER BY import_label_id ASC");
    $out_str=array();
    $final_array=array();
    while($db->move_next()){
        if($db->f('custom_field')==1)
        {
            /*$array2[$db->f('import_label_id')] = $db->f('label_name');*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name')),
                'field_name'   =>$db->f('field_name')
            ));
        }else
        {
            /*$array2[$db->f('import_label_id')] = gm($db->f('label_name'));*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name')),
                'field_name'   =>$db->f('field_name')
            ));
        }
    }
    foreach($out_str as $key=>$value){
        if($value['name'] == 'Account Id'){
            array_unshift($final_array,$value);
        }else{
            array_push($final_array,$value);
        }         
    }
    //return $out_str;
    return $final_array;
}
function build_match_field_dd_article($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $lng_arr= array('1'=>array('13','14','15'),'2'=>array('16','17','18'),'3'=>array('19','20','21'),'4'=>array('22','23','24'));
    $db = new sqldb;
    $db2 = new sqldb;
    $adv_prod=$db2->field("SELECT value FROM settings WHERE constant_name='ADV_PRODUCT'");
    $array2 = array();
    if($adv_prod=='1'){
        $db->query("SELECT label_name, import_label_id,custom_field,group_name,is_language_field FROM import_labels_articles  ORDER BY import_label_id ASC");
    }else{
        $acc_reduced="'internal_name','item_code','base_price','purchase_price','vat_id','value','family_name','supplier_name','supplier_reference'";
        $db->query("SELECT label_name, import_label_id,custom_field,group_name,is_language_field FROM import_labels_articles WHERE field_name IN (".$acc_reduced.") ORDER BY import_label_id ASC");
    }
    $default_lang = $db2->query("SELECT lang_id FROM pim_lang WHERE  active='1'")->getAll();
    $default_lang2=array();
    foreach ($default_lang as $key => $value) {
        array_push($default_lang2, $value['lang_id']);
    }
    $out_str=array();
    $final_array=array();
    while($db->move_next()){
        if(in_array($db->f('is_language_field'), $default_lang2) && $db->f('is_language_field')!=0 && in_array($db->f('import_label_id'), $lng_arr[$db->f('is_language_field')]) ){
            
            // if(in_array($db->f('import_label_id'), $lng_arr[$default_lang->f('lang_id')])){
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
            
        }elseif ($db->f('is_language_field')==0) {
           
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
        }

        /*if($default_lang->f('lang_id')==1){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12  && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12  && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }*/


  
    }

    foreach($out_str as $key=>$value){
        if($value['name'] == 'Article Id'){
            array_unshift($final_array,$value);
        }else{
            array_push($final_array,$value);
        }         
    }
    return $final_array;
    //return $out_str;
}
function build_match_field_dd_article_language($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name FROM import_labels_articles  ORDER BY import_label_id ASC");
    $out_str=array();
    while($db->move_next()){
        if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 )
        {
            if($db->f('custom_field')==1)
            {
                /*$array2[$db->f('import_label_id')] = $db->f('label_name');*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }else
            {
                /*$array2[$db->f('import_label_id')] = gm($db->f('label_name'));*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }
        }
    }


      return $out_str;
}
function build_match_field_dd_article_prices(){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $lng_arr= array('1'=>array('13','14','15'),'2'=>array('16','17','18'),'3'=>array('19','20','21'),'4'=>array('22','23','24'));
    $db = new sqldb;
    $db2 = new sqldb;
    $db->query("SELECT label_name, import_label_id,custom_field,group_name,is_language_field FROM import_labels_articles WHERE field_name='item_code' ORDER BY import_label_id ASC");

    $default_lang = $db2->query("SELECT lang_id FROM pim_lang WHERE  active='1'")->getAll();
    $default_lang2=array();
    foreach ($default_lang as $key => $value) {
        array_push($default_lang2, $value['lang_id']);
    }
    $out_str=array();
    $final_array=array();
    while($db->move_next()){
        if(in_array($db->f('is_language_field'), $default_lang2) && $db->f('is_language_field')!=0 && in_array($db->f('import_label_id'), $lng_arr[$db->f('is_language_field')]) ){
            array_push($out_str, array(
                'id'=>'main_'.$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
            
        }elseif ($db->f('is_language_field')==0) {       
            array_push($out_str, array(
                'id'=>'main_'.$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
        }
    }    
    $db->query("SELECT pim_article_price_category.* FROM pim_article_price_category ORDER BY name ASC");
    while($db->move_next()){
        array_push($out_str, array(
            'id'=>'price_'.$db->f('category_id'),
            'group'=>gm('Article Price Categories'),
            'name'=>$db->f('name')
        ));
    }
    return $out_str;
}
function build_match_field_dd_individual($selected = false){
    
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name,field_name FROM import_labels WHERE type = 'customer' AND label_name!='Last Name' AND field_name!='vat_id' AND field_name!='internal_language' ORDER BY import_label_id ASC");
    $out_str=array();
    $final_array=array();
    while($db->move_next()){
        if($db->f('custom_field')==1)
        {
            if($db->f('label_name')!='Name'){
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name')),
                    'field_name'=>$db->f('field_name')
                ));
            }
        }else
        {
            if($db->f('label_name')!='Name'){
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name')),
                    'field_name'=>$db->f('field_name')
                ));
            }
        }
    }
    foreach($out_str as $key=>$value){
        if($value['name'] == 'Account Id'){
            array_unshift($final_array,$value);
        }else{
            array_push($final_array,$value);
        }         
    }
    //return $out_str;
    return $final_array;
}

function build_simple_dropdown_sorted($opt, $selected=0,$view=0)
{
    $out_str = '';
    if($view==1){
    if($opt)
    while (list ($key, $val) = each ($opt))
    {   if($key=='9998'||$key=='9999'){
            $out_str.="<option value=\"".$key."\" ";//options values
            $out_str.=($key==$selected?" SELECTED ":"");//if selected
            $out_str.=">".$val."</option>";//options names
        }else{
            $out_str.="<option value=\"".$key."\" ";//options values
            $out_str.=($key==$selected?" SELECTED ":"");//if selected
            $out_str.=">".$val."</option>";//options names
        }
    }
  }else{
    if($opt)
        while (list ($key, $val) = each ($opt))
        {
            array_push($out_str, array('id'=>$key,'value'=>$val));
        }
  }
    return $out_str;
}
function build_match_field_dd_invoice($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name FROM import_labels_invoice  ORDER BY import_label_id ASC");
    $out_str=array();
    while($db->move_next()){
            if($db->f('custom_field')==1)
            {
                /*$array2[$db->f('import_label_id')] = $db->f('label_name');*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }else
            {
                /*$array2[$db->f('import_label_id')] = gm($db->f('label_name'));*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }
        
    }


      return $out_str;
}
function build_match_field_dd_invoice_line($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT name,field_id,field FROM tblinvoice_fields WHERE type=2 ");
    $out_str=array();
    while($db->move_next()){
            array_push($out_str, array(
                'id'=>$db->f('field_id'),
                'name'=>$db->f('name')
            ));

    }


      return $out_str;
}

function build_budget_type_list($selected = false,$hide='',$dd = true){
    $array = array('1' => gm('No budget'),'2' => gm('Total project hours'),'3' => gm('Total project fees'),'4' => gm('Hours per task'),'5' => gm('Hours per person'));
    if($dd === false){

        return $array[$selected];
    }
    if($hide!=''){
        unset($array[$hide]);
    }
    $new_array=array();
    foreach($array as $key => $value){
        array_push($new_array,array('id'=>$key,'name'=>$value));
    } 
    return $new_array;
}
function build_budget_type_list_daily($selected = false,$hide='',$dd = true){
    $array = array('1' => gm('No budget'),'2' => gm('Total project days'),'3' => gm('Total project fees'),'4' => gm('Days per task'),'5' => gm('Days per staff'));
    if($dd === false){

        return $array[$selected];
    }
    if($hide!=''){
        unset($array[$hide]);
    }
    $new_array=array();
    foreach($array as $key => $value){
        array_push($new_array,array('id'=>$key,'value'=>$value,'name'=>$value));
    } 
    return $new_array;
}
function generate_project_number($database)
{
    global $database_config;
        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );

    $db = new sqldb($db_config);
    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM projects
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function build_timesheet_week_from_project($project_id){
    $start = microtime(true);
    $db = new sqldb();
    $weeks = array();
    $dates  = $db->query("SELECT `date` FROM `task_time` WHERE `project_id`='".$project_id."' AND approved='1' AND billed='0' ");
    while ($dates->next()) {
        $week  = date('W',$dates->f('date'));
        if(!array_key_exists($week, $weeks)){
            $now                = $dates->f('date');
            $today_of_week      = date("N", $now);
            $month              = date('n',$now);
            $year               = date('Y',$now);
            $week_start             = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
            $week_end               = $week_start + 604799;
            $weeks[$week] = array($week_start,$week_end);
        }
    }
    // console::log($weeks);
    return $weeks;

}
function build_custom_hours_list($selected=1)
{
    $array=array('1'=>gm('This Month'),'2'=>gm('Last Month'),'3'=>gm('Custom'));

    return build_simple_dropdown($array,$selected,$hide);
}
function build_task_list($selected = false,$customer_id)
{
    $db=new sqldb;
    $i = 0;
    $is_contact = $db->field("SELECT is_contact FROM projects WHERE project_id='".$selected."' ");
    if(!$is_contact){
        $projects = $db->query("SELECT projects.project_id, customers.is_admin AS is_admin
                FROM   projects
                INNER  JOIN customers ON customers.customer_id=projects.customer_id
                WHERE  projects.customer_id='".$customer_id."' AND project_id='".$selected."' AND customers.is_admin!=1 ");
    }else{
        $projects = $db->query("SELECT projects.project_id FROM projects
                INNER JOIN customer_contacts ON projects.customer_id = customer_contacts.contact_id
                WHERE  projects.customer_id='".$customer_id."' AND project_id='".$selected."' ");
    }

    $array=array();
    while ($projects->next()) {
        $tasks = $db->query("SELECT task_id, task_name FROM tasks WHERE project_id='".$projects->f('project_id')."' AND closed='1' AND billable='1' ");
        while ($tasks->next()) {
            array_push($array,array('id'=>$tasks->f('task_id'),'value'=>$tasks->f('task_name')));
            //$out_str .= "<input type=\"checkbox\" name=\"tasks_id[]\" value=\"".$tasks->f('task_id')."\" style=\"width: auto; vertical-align: middle;\"><label style=\"font-weight: normal\">".$tasks->f('task_name')."</label><br />";
            $i++;
        }
    }

    if($i == 0){

        msg::notice(gm('No tasks to be invoiced'),"notice");

    }

    return $array;
}
function build_purchase_list($selected = false,$customer_id = false)
{
    $db=new sqldb;
    $i = 0;
    $projects = $db->query("SELECT project_purchase.project_purchase_id, project_purchase.description, customers.is_admin AS is_admin
                FROM project_purchase
                INNER JOIN projects ON project_purchase.project_id=projects.project_id
                LEFT JOIN customers ON customers.customer_id=project_purchase.supplier_id AND projects.customer_id='".$customer_id."' AND customers.is_admin!=1
                WHERE project_purchase.delivered='1' AND project_purchase.billable='1' AND project_purchase.invoiced != '1' AND project_purchase.project_id='".$selected."'");
    $array=array();
    while ($projects->next()) {
        array_push($array,array('id'=>$projects->f('project_purchase_id'),'value'=>$projects->f('description')));
        //$out_str .= "<div><input type=\"checkbox\" name=\"project_purchases_id[]\" value=\"".$projects->f('project_purchase_id')."\" style=\"width: auto; float:left; vertical-align: middle;\"><label style=\"font-weight: normal\">".$projects->f('description')."</label></div><br />";
    $i++;
    }
    if($i == 0){

        msg::notice(gm('No purchase to be invoiced'),"notice");

    }

    return $array;
}

function build_article_list($selected = false,$customer_id = false)
{
    $db=new sqldb;
    $i = 0;
    $projects = $db->query("SELECT service_delivery.a_id,project_articles.name FROM service_delivery
            INNER JOIN projects ON service_delivery.project_id=projects.project_id
            INNER JOIN project_articles ON service_delivery.project_id=project_articles.project_id AND service_delivery.a_id=project_articles.article_id
            WHERE service_delivery.project_id='".$selected."' AND projects.invoice_method>0 AND service_delivery.invoiced='0' ");

    $array=array();
    while ($projects->next()) {
        array_push($array,array('id'=>$projects->f('a_id').'-'.$selected,'value'=>$projects->f('name')));
        //$out_str .= "<input type=\"checkbox\" name=\"project_a_id[]\" value=\"".$projects->f('a_id')."\" style=\"width: auto; float:left; vertical-align: middle;\"><label style=\"font-weight: normal\">".$projects->f('name')."</label><br />";
    $i++;
    }
    if($i == 0){

        msg::notice(gm('No article to be invoiced'),"notice");

    }

    return $array;
}
function projectHourReport($p)
{
    if(!is_object($p)){
        return;
    }
    if(get_class($p) != 'sqldbResult'){
        return;
    }
    $pbt = $p->f('billable_type');
    $pId = $p->f('project_id');
    if(!$pId){
        return;
    }
    $db = new sqldb();
    global $database_config;

    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    $result = array('user_cost'=>0,'teoreticalA'=>0,'invoicedA'=>0);
    $ph = $db->query("SELECT SUM( hours ) as h , user_id FROM  `task_time` WHERE project_id='".$pId."' GROUP BY user_id ");
    while ($ph->next()) {
        if($ph->f('h')){
            //$uCost = $dbs->field("SELECT h_cost FROM users WHERE user_id='".$ph->f('user_id')."' ");
            if($p->f('status_rate')==0){
                $uCost = $db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$ph->f('user_id')."' AND project_id='".$pId."'");
            }else{
                $uCost = $db->field("SELECT p_daily_cost FROM project_user WHERE user_id='".$ph->f('user_id')."' AND project_id='".$pId."'");
            }
            $result['user_cost'] += $ph->f('h') * $uCost;
        }
    }

    $result['total_purchases_cost'] = $db->field("SELECT SUM(unit_price) FROM `project_purchase` WHERE project_id='".$pId."' ");
    $result['teo_puchases_in'] = $db->field("SELECT SUM(unit_price+ (unit_price*margin/100) ) FROM `project_purchase` WHERE project_id='".$pId."' AND billable='1' ");
    $result['puchases_in'] = $db->field("SELECT SUM(unit_price+ (unit_price*margin/100) ) FROM `project_purchase` WHERE project_id='".$pId."' AND billable='1' AND invoiced='1' ");
    $result['exp_can_bill'] = 0;
    $result['exp_bill'] = 0;
    $result['exp_cost'] = 0;
    $exp_can_bill = $db->query("SELECT SUM( amount ) AS am, expense.unit_price, expense.expense_id
                                        FROM  `project_expenses`
                                        INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                                        LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                                        WHERE  `project_expenses`.`project_id` ='".$pId."' AND  `project_expences`.`project_id` IS NOT NULL
                                        GROUP BY project_expenses.expense_id");
    while ($exp_can_bill->next()) {
        $result['exp_can_bill'] += $exp_can_bill->f('unit_price') ? $exp_can_bill->f('unit_price')*$exp_can_bill->f('am') : $exp_can_bill->f('am');
    }
    $exp_bill = $db->query("SELECT SUM( amount ) AS am, expense.unit_price, expense.expense_id
                                        FROM  `project_expenses`
                                        INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                                        LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                                        WHERE  `project_expenses`.`project_id` ='".$pId."' AND billed='1' AND  `project_expences`.`project_id` IS NOT NULL
                                        GROUP BY project_expenses.expense_id");
    while ($exp_bill->next()) {
        $result['exp_bill'] += $exp_bill->f('unit_price') ? $exp_bill->f('unit_price')*$exp_bill->f('am') : $exp_bill->f('am');
    }
    $exp_cost = $db->query("SELECT SUM( amount ) AS am, expense.unit_price, expense.expense_id
                                        FROM  `project_expenses`
                                        INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                                        LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                                        WHERE  `project_expenses`.`project_id` ='".$pId."'
                                        GROUP BY project_expenses.expense_id");
    while ($exp_cost->next()) {
        $result['exp_cost'] += $exp_cost->f('unit_price') ? $exp_cost->f('unit_price')*$exp_cost->f('am') : $exp_cost->f('am');
    }

    switch ($pbt) {
        case 1:
        case 7:
            $t = $db->query("SELECT SUM( hours ) as h, task_id FROM task_time WHERE project_id='".$pId."' AND billable='1' GROUP BY task_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT t_h_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT t_daily_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }
                    $result['teoreticalA'] += $tCost * $t->f('h');
                }
            }
            $t = $db->query("SELECT SUM( hours ) as h, task_id FROM task_time WHERE project_id='".$pId."' AND billable='1' AND billed='1' GROUP BY task_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT t_h_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT t_daily_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }
                    $result['invoicedA'] += $tCost * $t->f('h');
                }
            }
            break;
        case 2:
            $t = $db->query("SELECT SUM( hours ) as h, user_id FROM task_time WHERE project_id='".$pId."' AND billable='1' GROUP BY user_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }
                    $result['teoreticalA'] += $tCost * $t->f('h');
                }
            }
            $t = $db->query("SELECT SUM( hours ) as h, user_id FROM task_time WHERE project_id='".$pId."' AND billable='1' AND billed='1' GROUP BY user_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }
                    $result['invoicedA'] += $tCost * $t->f('h');
                }
            }
            break;
        case 3:
            $t = $db->field("SELECT SUM( hours ) as h FROM task_time WHERE project_id='".$pId."' AND billable='1' GROUP BY user_id ");
            $tCost = $db->field("SELECT pr_h_rate FROM projects WHERE project_id='".$pId."' ");
            $result['teoreticalA'] += $tCost * $t;
            $t = $db->field("SELECT SUM( hours ) as h FROM task_time WHERE project_id='".$pId."' AND billable='1' AND billed='1' GROUP BY user_id ");
            $tCost = $db->field("SELECT pr_h_rate FROM projects WHERE project_id='".$pId."' ");
            $result['invoicedA'] += $tCost * $t;
            break;
        case 5:
            $t = $db->field("SELECT SUM( task_budget ) as h FROM tasks WHERE project_id='".$pId."' AND billable='1' ");
            $result['teoreticalA'] += $t;
            $t = $db->field("SELECT SUM( task_budget ) as h FROM tasks WHERE project_id='".$pId."' AND billable='1' AND closed='2' ");
            $result['invoicedA'] += $t;
            break;
        case 6:
            $rate = $p->f('retainer_budget');
            $startDate = new DateTime(date(DATE_W3C,$p->f('start_date')));
            $now = new DateTime();
            $diff = $startDate->diff($now)->format('%m');
            $result['teoreticalA'] += $rate * $diff;
            $next = new DateTime(date(DATE_W3C,$p->f('next_date')));
            $diff = $startDate->diff($next)->format('%m');
            $result['invoicedA'] += $rate * $diff;
        default:
            # I don't know what I'm doing here
            break;
    }

    return $result;
}
function get_billable_amount($customer_id, $start, $end, $project = 0, $task = 0,$user = 0 ){
    if($project == 0){
        $and = ' AND 1=1 ';
    }
    else {
        if($task == 0){
            $and_task = ' AND 1=1 ';
        }
        else {
            $and_task = " AND task_id='".$task."'";
        }

        if($user == 0){
            $and_user = ' AND 1=1 ';
        }
        else{
            $and_user = " AND user_id='".$user."'";
        }
        $and = " AND project_id='".$project."' ";
    }
    $filter_date = " date BETWEEN '".$start."' AND '".$end."' ";
    if(!$start && !$end){
        $filter_date = " 1=1 ";
    }

    $db = new sqldb();
    $db2 = new sqldb();
    $db3 = new sqldb();
    $db4 = new sqldb();
    $total_amount = 0;
    // select all project for the customer
    $db->query("SELECT project_id, billable_type, pr_h_rate, status_rate FROM projects WHERE customer_id='".$customer_id."' $and ");
    while($db->move_next()){
        // select all blillable tasks for the customers projects
        $db2->query("SELECT task_id, t_h_rate, t_daily_rate, task_budget FROM tasks WHERE project_id='".$db->f('project_id')."' AND billable='1' $and_task ");
        while($db2->move_next()){
            // sums the hours for the users for the customers projects tasks
            $db3->query("SELECT user_id, SUM(hours) AS total_h FROM task_time WHERE customer_id='".$customer_id."'  AND project_id='".$db->f('project_id')."' AND task_id='".$db2->f('task_id')."' AND $filter_date $and_user GROUP BY user_id ");
            while ($db3->move_next()) {
                $total_h = $db3->f('total_h');
                //console::log($total_h,$db->f('billable_type'));
                // gets the rate for the specific taks
                switch ($db->f('billable_type')){
                    case '1':
                    case '7':
                        if($db->f('status_rate')==0){
                            $db4->query("SELECT t_h_rate FROM tasks WHERE task_id='".$db2->f('task_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('t_h_rate');
                        }else{
                            $db4->query("SELECT t_daily_rate FROM tasks WHERE task_id='".$db2->f('task_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('t_daily_rate');
                        }
                        break;
                    case '2':
                        if($db->f('status_rate')==0){
                            $db4->query("SELECT p_h_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('p_h_rate');
                        }else{
                            $db4->query("SELECT p_daily_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('p_daily_rate');
                        }
                        break;
                    case '3':
                        $rate = $db->f('pr_h_rate');
                        break;
                    case '4':
                        $rate = 0;
                        break;
                    case '5':
                        if($user == 0){
                            if($db->f('status_rate')==0){
                                $db4->query("SELECT p_h_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                                $db4->move_next();
                                $rate = $db4->f('p_h_rate');
                            }else{
                                uery("SELECT p_daily_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                                $db4->move_next();
                                $rate = $db4->f('p_daily_rate');
                            }
                        }else{
                            $rate = 1;
                            $total_h = $db2->f('task_budget');
                        }
                        break;
                }
                $total_amount += $total_h*$rate;
            }
        }
    }
    return $total_amount;
}
function billableAmountToInvoice($filter,$filter_task){
    $db = new sqldb();
    $total_amount = 0;
    $array = array();
    //To know without add hoc tasks
    // INNER JOIN project_user ON task_time.project_id=project_user.project_id AND task_time.user_id=project_user.user_id
    $tasks = $db->query("SELECT task_time.user_id, task_time.task_id, task_time.project_status_rate, task_time.customer_id, task_time.project_id, task_time.approved, tasks.t_h_rate, tasks.t_daily_rate, project_user.p_h_rate, project_user.p_daily_rate, tasks.task_budget,
                                projects.billable_type, projects.pr_h_rate,tasks.task_name as t_name, projects.company_name as c_name, projects.name as p_name,task_time.hours
                        FROM task_time
                        LEFT JOIN projects on task_time.project_id = projects.project_id
                        LEFT JOIN project_user ON task_time.project_id=project_user.project_id AND task_time.user_id=project_user.user_id
                        LEFT JOIN tasks ON task_time.task_id = tasks.task_id AND task_time.project_id = tasks.project_id
                        WHERE $filter AND projects.invoice_method>0 AND projects.active!='0' ");


    while ($tasks->next()) {

        $total_h = 0;
        $total_h_approved = 0;
        $total_h = $tasks->f('hours');
        if($tasks->f('approved') == 1){
            $total_h_approved = $tasks->f('hours');
        }
        if($tasks->f('billable_type') == '5'){
            $total_h = 0;
            $total_h_approved = 0;
        }
        switch ($tasks->f('billable_type')) {
            case '1':
            case '7':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('t_h_rate');
                }else{
                    $rate = $tasks->f('t_daily_rate');
                }
                break;
            case '2':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('p_h_rate');
                }else{
                    $rate = $tasks->f('p_daily_rate');
                }
                break;
            case '3':
                $rate = $tasks->f('pr_h_rate');
                break;
            case '4':
                $rate = 0;
                break;
            case '5':
                # here we only invoice the hours not the bugdet of the tasks
                $rate = 0;
                $total_h= 0;
                // $rate = 1;
                // $total_h = $tasks->f('task_budget');
                break;
            default:
                $rate = 0;
                break;
        }
        $total_amount += $total_h*$rate;
        #creating the data for the fake invoices
        if(!array_key_exists($tasks->f('customer_id'), $array)){
            $array[$tasks->f('customer_id')] = array();
        }
        if(!array_key_exists($tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id'), $array[$tasks->f('customer_id')])) {
            $detail = array('name'          => $tasks->f('c_name'),
                            'project_id'        => $tasks->f('project_id'),
                            'description'       => $tasks->f('p_name').": ".$tasks->f('t_name')." ",
                            'quantity'          => $total_h,
                            'unit_price'        => $rate,
                            'project_name'      => $tasks->f('p_name'),
                            'task_name'         => $tasks->f('t_name'),
                            'task_approved'     => $total_h_approved ? 1 : $tasks->f('approved'),
                            'quantity_approved' => $total_h_approved,
                            );
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')] = $detail;
        }else{
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity']+=$total_h;
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity_approved']+=$total_h_approved;
        }
        // array_push($array[$tasks->f('customer_id')], $detail);
    }

    $tasks_budget = $db->query("SELECT tasks.*,projects.customer_id,projects.company_name as c_name,projects.name as p_name FROM tasks
        LEFT JOIN projects ON tasks.project_id=projects.project_id
        WHERE projects.billable_type='5' AND tasks.billable='1' AND tasks.closed='1' AND projects.active>'0' AND projects.stage>'0' $filter_task ");
    while($tasks_budget->next()){
        $total_amount += $tasks_budget->f('task_budget');
        if(!array_key_exists($tasks_budget->f('customer_id'), $array)){
            $array[$tasks_budget->f('customer_id')] = array();
        }
        if(!array_key_exists($tasks_budget->f('project_id').'-'.$tasks->f('task_id').'-0', $array[$tasks_budget->f('customer_id')])) {
            $detail = array('name'          => $tasks_budget->f('c_name'),
                            'project_id'        => $tasks_budget->f('project_id'),
                            'description'       => $tasks_budget->f('p_name').": ".$tasks_budget->f('task_name')." ",
                            'quantity'          => 1,
                            'unit_price'        => $tasks_budget->f('task_budget'),
                            'project_name'      => $tasks_budget->f('p_name'),
                            'task_name'         => $tasks_budget->f('task_name'),
                            'task_approved'         => 1,
                            'quantity_approved' => 1,
                            );
            $array[$tasks_budget->f('customer_id')][$tasks_budget->f('project_id').'-'.$tasks_budget->f('task_id').'-0'] = $detail;
        }else{
            $array[$tasks_budget->f('customer_id')][$tasks_budget->f('project_id').'-'.$tasks_budget->f('task_id').'-0']['quantity']=1;
            $array[$tasks_budget->f('customer_id')][$tasks_budget->f('project_id').'-'.$tasks_budget->f('task_id').'-0']['quantity_approved']=1;
        }
    }


    $result = array('total' => $total_amount, 'details' => $array);
    return $result;
}
function get_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language WHERE label_language_id = '".$language_id."' ");
    } else {
        $db->query("SELECT * FROM label_language WHERE lang_code ='".$language_id."'");
    }
    $db->move_next();
    if($db->f($label)){
        $label_val=$db->f($label);
    }else{
        $label_val='';
    }
    return $label_val;
}
function build_user_dd_timesheet($selected = false, $access = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    $db->query("SELECT user_id, first_name, last_name,group_id FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ");
    $db->move_next();
    $array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
    $filter = ' 1=1 ';
    $group_id = $db->f('group_id');
    if($group_id != 2){
        if($access > 1  ){
            $filter = " user_role > '".$_SESSION['access_level']."' OR user_id='".$selected."' ";
        }
        $is_manager = $db2->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");

        if($_SESSION['group'] != 'admin' && !empty($is_manager)){
            $u = '';
            $users = $db2->query("SELECT user_id FROM  `project_user` WHERE project_id IN
                                    ( SELECT project_id FROM project_user WHERE manager =1 AND user_id ='".$_SESSION['u_id']."' )
                                  GROUP BY user_id ");
            while ($users->next()) {
                $u .= $users->f('user_id').',';
            }
            $filter =" user_id IN ( ".rtrim($u,',')." ) ";
        }
        $db->query("SELECT user_id, first_name, last_name FROM users WHERE database_name='".DATABASE_NAME."' AND $filter AND active='1' ");
        while ($db->move_next()) {
            $array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
        }
    }

    asort($array);
    $out=array();
    foreach($array as $key => $value){
        array_push($out, array('id'=>$key,'name'=>htmlspecialchars_decode(stripslashes($value))));
    }
    return $out;
}
function build_user_dd_timetracker($selected = false, $access = false){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    $perm_admin = $db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_19' ");
    $perm_manager = $db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timetracker_admin' ");
    $db->query("SELECT user_id, first_name, last_name,group_id,CONCAT(first_name,' ',last_name) as fullname FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ORDER BY TRIM(fullname) ASC");
    $db->move_next();
    $array[] = gm('All Users');

    $array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
    $filter = ' 1=1 ';
    $group_id = $db->f('group_id');
    if($group_id != 2 || $perm_admin || $perm_manager){
        $db->query("SELECT user_id, first_name, last_name,CONCAT(first_name,' ',last_name) as fullname FROM users WHERE database_name='".DATABASE_NAME."' AND user_id!='".$_SESSION['u_id']."' AND active='1' ORDER BY TRIM(fullname) ASC ");
        while ($db->move_next()) {
            $array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
        }
    }

    // asort($array);
    $out=array();
    foreach($array as $key => $value){
        array_push($out, array('id'=>$key,'name'=>htmlspecialchars_decode(stripslashes($value))));
    }
    return $out;
}
function build_how_many_dropdown($selected=0){
    $sell=array(
    '0' => gm('Personal'),
    '1' => gm('Startup'),
    '2' => gm('Small'),
    '3' => gm('Business'),
    '4' => gm('Entreprise'),
    );
    $out=array();
    foreach($sell as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;
}
function build_period_dropdown($selected=1){
    $sell=array(
    '1' => gm('Monthly'),
    '2' => gm('Yearly'),
    );
    $out=array();
    foreach($sell as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;
}
function build_you_sell_dropdown($selected=1){
    $sell=array(
    // '1' => gm('Free'),
    '5' => gm('Services'),
    '4' => gm('Products'),
    '3' => gm('Products & Services'),
    );
    $out=array();
    foreach($sell as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;
}
function send_attention_mail($user_id)
{
    global $database_config;
    $mail = new PHPMailer();
    $mail->WordWrap = 50;
    $database_2 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_user= new sqldb($database_2);
    $fromMail='support@akti.com';
    $user_info = $db_user->query("SELECT * FROM users WHERE user_id='".$user_id."' ");
    if($user_info->next()){
        $email = $user_info->f('email');
        switch ($user_info->f('lang_id')) {
            case '1':
                $body='Hallo,

Uw kredietkaart voor uw <a href="my.akti.com">Akti</a> abonnement kon niet verwerkt worden.
We blijven je kaart opnieuw te proberen in de komende
dagen. Wij begrijpen dat creditcards verlopen, dus geven we je een paar extra dagen om alles in orde te brengen.

Als uw kaart is vervallen, kunt u inloggen op uw<a href="my.akti.com">Akti</a> account
en werk deze zo spoedig mogelijk bij.

Met vriendelijke groet,

Het Akti Team';

                $subject = 'Akti � Maandelijkse betaling geweigerd';
                break;
            case '2':
                $body='Hi,

Your credit card for your <a href="my.akti.com">Akti</a> subscription failed to
process recently. We\'ll continue to try your card again over the next few
days. We understand that credit cards expire, so we\'re
glad to give you  a few extra days to straighten things out.

If your card has expired, please login to your <a href="my.akti.com">Akti</a> account
and update it as soon as possible.

Regards,

The Akti Team';
                $subject = 'Akti - Recurring Payment Declined';
                break;
            case '3':
                $body='Bonjour,

Votre carte de cr�dit pour votre abonnement <a href="my.akti.com">Akti</a> n�a pas pu �tre trait�e.
Nous allons r�essayer de traiter votre paiement les prochains jours.
Nous comprenons que les cartes de cr�dit viennent � �ch�ance, et nous vous donnons donc quelques jours suppl�mentaires pour arranger cela.

Si votre carte a expir�, veuillez vous connecter � votre compte <a href="my.akti.com">Akti</a> et mettez-le � jour d�s que possible.

Cordialement,

L\'�quipe Akti';
                $subject = 'Akti � Paiement mensuel refus�';
                break;

            default:
                $body='Hi,

Your credit card for your <a href="my.akti.com">Akti</a> subscription failed to
process recently. We\'ll continue to try your card again over the next few
days. We understand that credit cards expire, so we\'re
glad to give you  a few extra days to straighten things out.

If your card has expired, please login to your <a href="my.akti.com">Akti</a> account
and update it as soon as possible.

Regards,

The Akti Team';
                $subject = 'Akti - Recurring Payment Declined';
                break;
        }
        $xxx = '<table style="background:#f6f6f6; width:100%; margin:0; padding:0; " border="0" cellspacing="0" cellpadding="50" >
                <tbody>
                    <tr ><td style="border-collapse:collapse; " >
                        <table style="margin:0 auto; padding:0; background:#FFFFFF;" width="600" border="0" cellspacing="0" cellpadding="0" >
                            <tbody>
                                <tr style="background:#FFF;"><td style="padding: 0px;"><img src="http://www.akti.com/images/logo_newsletter.png"></td></tr>
                                <tr style=""><td style="width=100%; padding:20px; border-bottom:1px solid #dcdcdc; border-right:1px solid #dcdcdc; border-left:1px solid #dcdcdc; border-collapse: collapse;">'.nl2br($body).'
                                </td></tr>
                            </tbody>
                        </table>
                    </tr></td>
                </tbody>
            </table>';
        $body = $xxx;
        $body = str_replace("\n", '', $body);
        $body = str_replace("\t", '', $body);

        $mail->Subject = $subject;

        $mail->SetFrom($fromMail, $fromMail);

        $mail->MsgHTML(nl2br($body));

        $mail->AddAddress($email);

        $mail->Send();
    }
    return true;

}

function build_s_n_status_dd($selected = false){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT serial_number_status.* FROM serial_number_status ");
    while($db->move_next()){
        $array[$db->f('id')] = gm($db->f('name'));
    }
    return build_simple_dropdown($array,$selected);

}
function build_batch_nr_status_dd($selected = false){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT batch_number_status.* FROM batch_number_status ");
    while($db->move_next()){
        $array[$db->f('id')] = $db->f('name');
    }
    return build_simple_dropdown($array,$selected);
}
function getpo_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language_p_order WHERE label_language_id='".$language_id."'");
    } else {
        $db->query("SELECT * FROM label_language_p_order WHERE lang_code = '".$language_id."'");
    }

    $db->move_next();
    $label_val = $db->f($label) ? $db->f($label) : '';
    return $label_val;
}

function get_customer_name($id){
    $db = new sqldb();
    return $db->field("SELECT name FROM customers WHERE customer_id='".$id."' ");
    // $db->next();
    // return $db->f('name');
}
function get_customer_name_with_legal($id){
    $db = new sqldb();
    $c_data=$db->query("SELECT customer_legal_type.name as l_name,customers.name FROM customers LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id WHERE customers.customer_id='".$id."' ");
    return ($c_data->f('l_name') ? stripslashes($c_data->f('name')).' '.$c_data->f('l_name') : stripslashes($c_data->f('name')));
}
function get_contact_first_and_name($id){
    $db = new sqldb();
    return $db->field("SELECT CONCAT_WS(' ',lastname, firstname) FROM customer_contacts WHERE contact_id='".$id."' ");
    // $db->next();
    // return $db->f('firstname').' '.$db->f('lastname');
}

function strtotimeActivity($str){
    $tmstmp = strtotime($str);
    $offset = 2; // we set the timezone to europe/bucharest;
    $hour = 3600;
    $tmstmp = $tmstmp-2*$hour;
    if(defined('ACCOUNT_TIME_ZONE')){
        // console::log('erere',ACCOUNT_TIME_ZONE);
        $tmstmp = $tmstmp + (ACCOUNT_TIME_ZONE*$hour);
    }
    return $tmstmp;
}
function build_timesheet_customer_dd($selected='',$time_start,$time_end,$user_id)
{
    $db=new sqldb();
    $array = array();
    $db->query("SELECT task_time.*, projects.company_name AS c_name, projects.customer_id
            FROM task_time
            INNER JOIN projects ON task_time.project_id=projects.project_id
            WHERE task_time.user_id='".$user_id."' AND task_time.date BETWEEN '".$time_start."' AND '".$time_end."' AND hours!='0'
            GROUP BY c_name
            ORDER BY task_time.date
           ");
    while($db->move_next()){
        array_push($array,array('id'=>$db->f('customer_id'), 'name'=>$db->f('c_name')));
    }
    return $array;
}
function gettime_label_txt($label,$language_id)
{
    $db=new sqldb();
    $db->query("SELECT * FROM label_language_time WHERE label_language_id='".$language_id."'");
    $db->move_next();
    if($db->f($label)){
        $label_val=$db->f($label);
    }else{
        $label_val='';
    }
    return $label_val;
}
function build_days_of_the_week($start_week,$selected = false){

    $k = 0;
    $out = array();
    for ($i = 0; $i<7; $i++){
        $out[$start_week+$k] = date(ACCOUNT_DATE_FORMAT,$start_week+$k);
        $k += 86400;
    }

    return build_simple_dropdown($out,$selected);

}

function updateCustomerPrimaryAddressCoords($address,$customer_id){
    $db = new sqldb();
    if($address->f('address') )
    {
        $country_n = get_country_name($address->f('country_id') );
        if(!$address->f('zip') && $address->f('city'))
        {
            $address = $address->f('address').', '.$address->f('city').', '.$country_n;
        }elseif(!$address->f('city') && $address->f('zip'))
        {
            $address = $address->f('address').', '.$address->f('zip').', '.$country_n;
        }elseif(!$address->f('zip') && !$address->f('city'))
        {
            $address = $address->f('address').', '.$country_n;
        }else
        {
            $address = $address->f('address').', '.$address->f('zip').' '.$address->f('city').', '.$country_n;
        }
        $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
        $output= json_decode($geocode);
        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;
        $db->query("INSERT INTO addresses_coord SET location_lat='".$lat."', location_lng='".$long."', customer_id='".$customer_id."'");
        return array('latitude'=>$lat,'longitude'=>$long);
    }
    return array();
   

}
function build_user_dd_calendar($selected = false,$user_ids='0')
{
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    $db->query("SELECT user_id, first_name, last_name FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."' ");
    $db->move_next();
    $array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
    if($user_ids){
        $db->query("SELECT user_id, first_name, last_name FROM users WHERE user_id IN (".$user_ids.")");
        while ($db->move_next()) {
            $array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
        } 
    }

    asort($array);
    $out=array();
    foreach($array as $key => $value){
        array_push($out, array('id'=>$key,'name'=>htmlspecialchars_decode(stripslashes($value))));
    }
    return $out;

}
function geto_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language_order WHERE label_language_id='".$language_id."'");
    } else {
        $db->query("SELECT * FROM label_language_order WHERE lang_code = '".$language_id."' ");
    }

    $db->move_next();
    $label_val = $db->f($label) ? $db->f($label) : '';
    return $label_val;
}
function getc_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language_certificate WHERE label_language_id='".$language_id."'");
    } else {
        $db->query("SELECT * FROM label_language_certificate WHERE lang_code = '".$language_id."' ");
    }

    $db->move_next();
    $label_val = $db->f($label) ? $db->f($label) : '';
    return $label_val;
}
function getint_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language_int WHERE label_language_id='".$language_id."'");
    } else {
        $db->query("SELECT * FROM label_language_int WHERE lang_code = '".$language_id."' ");
    }

    $db->move_next();
    $label_val = $db->f($label) ? $db->f($label) : '';
    return $label_val;
}
function generate_stock_disp_serial_number($database)
{
    global $database_config;

    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $prefix_lenght=strlen(ACCOUNT_STOCK_DISP_START)+1;
    $left = defined('ACCOUNT_DIGIT_NR') ? ACCOUNT_STOCK_DISP_DIGIT_NR : '';
    if(!$left){
        $left=1;
    }
    $db->query("SELECT  pim_stock_disp.stock_disp_id,pim_stock_disp.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM pim_stock_disp
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes(ACCOUNT_STOCK_DISP_START)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==ACCOUNT_STOCK_DISP_START){
        $next_serial=ACCOUNT_STOCK_DISP_START.str_pad($db->f('invoice_number')+1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $order_nr = defined('ACCOUNT_STOCK_DISP_START') ? ACCOUNT_STOCK_DISP_START : '';
        $next_serial= $order_nr.str_pad(1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }

}

function build_pdf_language_code_dd($selected='',$active){

    $db = new sqldb();
    $array = array();
    $filter="1=1";
    if($active){
        $filter.=" and active=1";
    }
    $db->query("SELECT * FROM pim_lang where  ".$filter." ");
    while($db->move_next()){
        $array[$db->f('code')] = $db->f('language');
        /*array_push($array,array('id'=>$db->f('code'), 'name'=>$db->f('language')));*/
    }
    return build_simple_dropdown($array,$selected,0);


}

function generate_sepa_number($database)
{
    global $database_config,$cfg;
    $db_conf = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_conf);
    $sepa_id=$db->field("SELECT sepa_settings_id FROM sepa_settings ");
    $last_number=$db->field("SELECT value FROM settings WHERE constant_name='SEPA_START_NUMBER' ");
    $last_number=$last_number+1;

    if(!$sepa_id){
        $sepa_number='334-'.$last_number;
    }else{
        $sepa_data = $db->query("SELECT sepa_invoice_start,sepa_inc_part FROM sepa_settings WHERE sepa_settings_id='".$sepa_id."' ");
        $sepa_number=$sepa_data->f('sepa_invoice_start').''.str_pad($last_number, $sepa_data->f('sepa_inc_part'), "0", STR_PAD_LEFT );
    }
    return $sepa_number;
}


function get_not_paid_invoice($invoice_id,$with_vat=0){

   $db = new sqldb();
   $db->query("SELECT sum(amount) as amount,sum(amount_vat) as amount_vat
               FROM tblinvoice
               WHERE id='".$invoice_id."' and not_paid=1");
    return array($db->f('amount')-$db->f('amount_paid'), $db->f('amount_vat')-$db->f('amount_paid'));
}

function billableAmount($filter){
    $db = new sqldb();
    $total_amount = 0;
    $array = array();
    //To know without add hoc tasks
    // INNER JOIN project_user ON task_time.project_id=project_user.project_id AND task_time.user_id=project_user.user_id
    $tasks = $db->query("SELECT task_time.user_id, task_time.task_id, task_time.project_status_rate, task_time.customer_id, task_time.project_id, task_time.approved, tasks.t_h_rate, tasks.t_daily_rate, project_user.p_h_rate, project_user.p_daily_rate, tasks.task_budget,
                                projects.billable_type, projects.pr_h_rate,tasks.task_name as t_name, projects.company_name as c_name, projects.name as p_name,task_time.hours
                        FROM task_time
                        INNER JOIN projects on task_time.project_id = projects.project_id
                        INNER JOIN project_user ON task_time.project_id=project_user.project_id
                        INNER JOIN tasks ON task_time.task_id = tasks.task_id AND task_time.project_id = tasks.project_id
                        WHERE $filter AND projects.invoice_method>0 AND projects.active!='0' ");

    while ($tasks->next()) {
        $total_h = 0;
        $total_h_approved = 0;
        $total_h = $tasks->f('hours');
        if($tasks->f('approved') == 1){
            $total_h_approved = $tasks->f('hours');
        }
        if($tasks->f('billable_type') == '5'){
            $total_h = 0;
            $total_h_approved = 0;
        }
        switch ($tasks->f('billable_type')) {
            case '1':
            case '7':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('t_h_rate');
                }else{
                    $rate = $tasks->f('t_daily_rate');
                }
                break;
            case '2':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('p_h_rate');
                }else{
                    $rate = $tasks->f('p_daily_rate');
                }
                break;
            case '3':
                $rate = $tasks->f('pr_h_rate');
                break;
            case '4':
                $rate = 0;
                break;
            case '5':
                # here we only invoice the hours not the bugdet of the tasks
                $rate = 0;
                $total_h= 0;
                // $rate = 1;
                // $total_h = $tasks->f('task_budget');
                break;
            default:
                $rate = 0;
                break;
        }
        $total_amount += $total_h*$rate;
        #creating the data for the fake invoices
        if(!array_key_exists($tasks->f('customer_id'), $array)){
            $array[$tasks->f('customer_id')] = array();
        }
        if(!array_key_exists($tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id'), $array[$tasks->f('customer_id')])) {
            $detail = array('name'          => $tasks->f('c_name'),
                            'project_id'        => $tasks->f('project_id'),
                            'description'       => $tasks->f('p_name').": ".$tasks->f('t_name')." ",
                            'quantity'          => $total_h,
                            'unit_price'        => $rate,
                            'project_name'      => $tasks->f('p_name'),
                            'task_name'         => $tasks->f('t_name'),
                            'task_approved'     => $total_h_approved ? 1 : $tasks->f('approved'),
                            'quantity_approved' => $total_h_approved,
                            );
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')] = $detail;
        }else{
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity']+=$total_h;
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity_approved']+=$total_h_approved;
        }
        // array_push($array[$tasks->f('customer_id')], $detail);
    }
    $result = array('total' => $total_amount, 'details' => $array);
    return $result;

}

function send_someone_mail($email, $body, $subject)
{
    require_once (__DIR__.'/class.phpmailer.php');
    include_once (__DIR__.'/class.smtp.php');
    $mail = new PHPMailer();

    $fromMail='noreply@akti.com';

    $mail->Subject = $subject;

    $mail->SetFrom($fromMail, $fromMail);


    $mail->MsgHTML(nl2br($body));

    if(is_array($email)){
        foreach ($email as $key) {
            $mail->AddAddress($key);
        }
    }else{
        $mail->AddAddress($email);
    }

    $mail->Send();
    return true;
}
function generatePdfFtomPim($database,$id)
{
    global $config,$database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_user= new sqldb($db_config);
    $user_id = $db_user->field("SELECT user_id FROM users WHERE database_name='".$database."' AND active='1' AND default_admin='1' LIMIT 1");

    $db_user->query("INSERT INTO gpasta SET user_id='".$user_id."', `database`='".$database."',invoice_id='".$id."' ");

    $vars = array();
    $vars['username'] = 'admin:'.$user_id;
    $vars['do'] = 'auth--auth-login';
    $vars['password'] = 'Umckt%78AXw1E6Fqv!Do';
    $opt = array(CURLOPT_COOKIEJAR => "my_cookies_".$database.".txt",
                 CURLOPT_COOKIEFILE => "my_cookies_".$database.".txt",
                 CURLOPT_COOKIESESSION => true );
    $c = new clientREST();
    $c->setOpt($opt);
    $response = $c->execRequest($config['site_url'].'index.php','post',$vars);

    $vars = array();
    $vars['do'] = 'invoice--invoice-gpasta';
    $vars['invoice_id'] = $id;
    $vars['attachAsString'] = 1;
    $response = $c->execRequest($config['site_url'].'index.php','get',$vars);
    return $response;
}
function generateXMLtomPim($database,$id)
{
    global $config,$database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_user= new sqldb($db_config);
    $user_id = $db_user->field("SELECT user_id FROM users WHERE database_name='".$database."' AND active='1' AND default_admin='1' LIMIT 1");

    $db_user->query("INSERT INTO gpasta SET user_id='".$user_id."', `database`='".$database."',invoice_id='".$id."' ");

    $vars = array();
    $vars['username'] = 'admin:'.$user_id;
    $vars['do'] = 'auth--auth-login';
    $vars['password'] = 'Umckt%78AXw1E6Fqv!Do';
    $opt = array(CURLOPT_COOKIEJAR => "my_cookies_".$database.".txt",
                 CURLOPT_COOKIEFILE => "my_cookies_".$database.".txt",
                 CURLOPT_COOKIESESSION => true );
    $c = new clientREST();
    $c->setOpt($opt);
    $response = $c->execRequest($config['site_url'].'index.php','post',$vars);

    $vars = array();
    $vars['do'] = 'invoice--invoice-gpastaxml';
    $vars['invoice_id'] = $id;
    $response = $c->execRequest($config['site_url'].'index.php','get',$vars);
    return $response;
}

  function default_email($database)
  {  global $database_config;

    $db_usr = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    ); 
    $db=new sqldb($db_usr);

    $array = array();
    $ACCOUNT_COMPANY=utf8_decode($db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' "));
    $ACCOUNT_EMAIL=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_EMAIL' ");
    $MAIL_SETTINGS_PREFERRED_OPTION=$db->field("SELECT value FROM settings WHERE `constant_name`='MAIL_SETTINGS_PREFERRED_OPTION' ");
    $MAIL_SETTINGS_EMAIL=$db->field("SELECT value FROM settings WHERE `constant_name`='MAIL_SETTINGS_EMAIL' ");
    $ACCOUNT_SEND_EMAIL=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_SEND_EMAIL' ");

    $array['from']['name'] = $ACCOUNT_COMPANY;
    $array['from']['email'] = 'noreply@akti.com';
    $array['reply']['name'] = $ACCOUNT_COMPANY;
    $array['reply']['email'] = $ACCOUNT_EMAIL;
    $array['bcc']['name'] = '';
    $array['bcc']['email'] = '';

    if($MAIL_SETTINGS_PREFERRED_OPTION){
      if($MAIL_SETTINGS_PREFERRED_OPTION=='2'){
        if($MAIL_SETTINGS_EMAIL!=''){
          $array['from']['email'] = $MAIL_SETTINGS_EMAIL;
        }
      }else{
        if($ACCOUNT_SEND_EMAIL=='1' && $ACCOUNT_EMAIL!=''){
          $array['from']['email'] = $ACCOUNT_EMAIL;
        }
      }
    }
    $db->query("SELECT * FROM default_data WHERE type='invoice_email' ");

    if($db->move_next() && $db->f('value') && $db->f('default_name')){
      $array['reply']['name'] = $db->f('default_name');
      $array['reply']['email'] = $db->f('value');
      $array['from']['name'] = $db->f('default_name');
      if(($ACCOUNT_SEND_EMAIL=='1' || $MAIL_SETTINGS_PREFERRED_OPTION=='2') && $db->f('value')!=''){
        $array['from']['email'] = $db->f('value');
      }
    }
    //bcc email
    $db->query("SELECT * FROM default_data WHERE type='bcc_invoice_email' ");
    if($db->move_next() && $db->f('value')){
      $array['bcc']['email'] = $db->f('value'); // pot fi valori multiple, delimitate de ;
    }

    return $array;
  } 

function send_invoice_email($recurring_invoice_id,$language_id,$invoice_id,$database,$attach='')
{
    global $in,$database_config,$config;

    $in['type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    $in['id']=$invoice_id;
    $in['lid']=$language_id;
    $in['db']=$database;
    include_once(__DIR__.'/../core/library/msg.php');
    // include(__DIR__.'/../php/invoice_print.php');
    $in['time'] = time();
    require_once(__DIR__.'/class.phpmailer.php');
    include_once(__DIR__.'/class.smtp.php'); // optional, gets called from within class.phpmailer.php if not already loaded
    $db_usr = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    ); 
    $db=new sqldb($db_usr);
    $db2=new sqldb($db_usr);
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    ); 
    $dbu= new sqldb($db_config);

    $body=$db->field("SELECT email_body FROM recurring_invoice WHERE recurring_invoice_id='".$recurring_invoice_id."' ");
    $subject=$db->field("SELECT email_subject FROM recurring_invoice WHERE recurring_invoice_id='".$recurring_invoice_id."' ");
    $use_html = $db->field("SELECT use_html FROM recurring_invoice WHERE recurring_invoice_id='".$recurring_invoice_id."' ");
    $include_xml=$db->field("SELECT include_xml FROM recurring_invoice WHERE recurring_invoice_id='".$recurring_invoice_id."' ");
    $copy=$db->field("SELECT copy FROM recurring_invoice WHERE recurring_invoice_id='".$recurring_invoice_id."' ");
    $created_by=$db->field("SELECT created_by FROM recurring_invoice WHERE recurring_invoice_id='".$recurring_invoice_id."' ");
    $email_created_by=$dbu->field("SELECT email from users where user_id ='".$created_by."'");

    $ACCOUNT_VAT_NUMBER=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
    $ACCOUNT_EMAIL=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_EMAIL' ");
    $ACCOUNT_COMPANY=utf8_decode($db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' "));
    $ACCOUNT_DATE_FORMAT=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DATE_FORMAT' ");
    $ACCOUNT_LOGO=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_LOGO' ");
    $USE_INVOICE_WEB_LINK=$db->field("SELECT value FROM settings WHERE `constant_name`='USE_INVOICE_WEB_LINK' ");
    $ALLOW_WEB_PAYMENT=$db->field("SELECT value FROM settings WHERE `constant_name`='ALLOW_WEB_PAYMENT' ");

    $db->query("SELECT tblinvoice.due_date,tblinvoice.id,tblinvoice.email_subject,tblinvoice.email_body,tblinvoice.serial_number,tblinvoice.discount,tblinvoice.vat,tblinvoice.invoice_date,tblinvoice.buyer_name,
                       tblinvoice.currency_type,tblinvoice.include_pdf,tblinvoice.copy, tblinvoice.email_language, tblinvoice.buyer_id,tblinvoice.contact_id,tblinvoice.your_ref,tblinvoice.amount_vat,tblinvoice.apply_discount,tblinvoice.seller_bwt_nr,
                                  SUM(tblinvoice_line.amount) AS total,
                                  SUM(tblinvoice_payments.amount) AS total_payed
                           FROM tblinvoice
                           LEFT JOIN tblinvoice_line ON tblinvoice_line.invoice_id=tblinvoice.id
                           LEFT JOIN tblinvoice_payments ON tblinvoice_payments.invoice_id=tblinvoice.id
                           WHERE tblinvoice.id='".$invoice_id."'");
    $db->move_next();
    $b_name=$db->f('buyer_name');
    $e_lang = $db->f('email_language');
    $buyer_id=$db->f('buyer_id');
    $contact_id=$db->f('contact_id');
    $seller_bwt_nr=$db->f('seller_bwt_nr');

    if($contact_id){
     $contact = $db2->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$contact_id."'");
     $title_cont = $db2->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
    }
    

    $factur_date = date($ACCOUNT_DATE_FORMAT,$db->f('invoice_date'));
    $total=$db->f('total');
    $currency=get_commission_type_list($db->f('currency_type'));

    $discount_value = $total*$db->f('discount')/100;
    $total -= $discount_value;

    $vat_value = $total*$db->f('vat')/100;
    $total += $vat_value;

    $total_payed = $db->f('total_payed');
    $amount_due = $db->f('amount_vat');

    $mail = new PHPMailer();
    
    $mail->database_name = $database;
    /*$mail->IsSMTP(); // telling the class to use SMTP
    $mail->SMTPDebug = 1; // enables SMTP debug information (for testing)
    // 1 = errors and messages
    // 2 = messages only
    $mail->SMTPAuth = true; // enable SMTP authentication
    $mail->Host = SMTP_HOST; // sets the SMTP server
    $mail->Port = SMTP_PORT; // set the SMTP port for the GMAIL server
    $mail->Username = SMTP_USERNAME; // SMTP account username
    $mail->Password = SMTP_PASSWORD; // SMTP account password
*/
    if($db->f('discount') == 0){
        $discount_procent=0;
    } else {
        $discount_procent = $db->f('discount');
    }

    $$discount_value = 0;
    $lines = $db2->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$invoice_id."' ");
    while ($lines->next()) {
        $line_discount = $lines->f('discount');
        if($db->f('apply_discount') ==0 || $db->f('apply_discount') == 2){
            $line_discount = 0;
        }
        $amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
        $amount_line_disc = $amount_line*$discount_procent/100;
        $discount_value += $amount_line_disc;
    }

    $body=utf8_decode(stripslashes($body));
    $subject=str_replace('[!ACCOUNT_COMPANY!]',$ACCOUNT_COMPANY, $subject );
    $subject=str_replace('[!SERIAL_NUMBER!]',$db->f('serial_number'), $subject);
    $subject=str_replace('[!INVOICE_DATE!]',$factur_date, $subject);
    $subject=str_replace('[!CUSTOMER!]',utf8_decode($b_name), $subject);
    $subject=str_replace('[!SUBTOTAL!]',$currency.' '.display_number($db->f('total')), $subject);
    $subject=str_replace('[!DISCOUNT!]',$db->f('discount').'%', $subject);
    $subject=str_replace('[!DISCOUNT_VALUE!]',$currency.' -'.display_number($discount_value), $subject);
    $subject=str_replace('[!DUE_DATE!]',date( $ACCOUNT_DATE_FORMAT,$db->f('due_date')), $subject);
    $subject=str_replace('[!VAT!]',$db->f('vat').'%', $subject);
    $subject=str_replace('[!VAT_VALUE!]',$currency.' '.display_number($vat_value), $subject);
    $subject=str_replace('[!PAYMENTS!]',$currency.' '.display_number($total_payed), $subject);
    $subject=str_replace('[!AMOUNT_DUE!]',$currency.' '.display_number($amount_due), $subject);
    $subject=str_replace('[!INVOICE_TYPE!]', $invoice_type_txt, $subject);

    $body=str_replace('[!ACCOUNT_COMPANY!]',$ACCOUNT_COMPANY, $body );
    $body=str_replace('[!SERIAL_NUMBER!]',$db->f('serial_number'), $body);
    $body=str_replace('[!INVOICE_DATE!]',$factur_date, $body);
    $body=str_replace('[!CUSTOMER!]',utf8_decode($b_name), $body);
    $body=str_replace('[!SUBTOTAL!]',$currency.' '.display_number($db->f('total')), $body);
    $body=str_replace('[!DISCOUNT!]',$db->f('discount').'%', $body);
    $body=str_replace('[!DISCOUNT_VALUE!]',$currency.' -'.display_number($discount_value), $body);
    $body=str_replace('[!DUE_DATE!]',date( $ACCOUNT_DATE_FORMAT,$db->f('due_date')), $body);
    $body=str_replace('[!VAT!]',$db->f('vat').'%', $body);
    $body=str_replace('[!VAT_VALUE!]',$currency.' '.display_number($vat_value), $body);
    $body=str_replace('[!PAYMENTS!]',$currency.' '.display_number($total_payed), $body);
    $body=str_replace('[!AMOUNT_DUE!]',$currency.' '.display_number($amount_due), $body);
    $body=str_replace('[!INVOICE_TYPE!]', $invoice_type_txt, $body);

    if($contact_id){
        $body=str_replace('[!CONTACT_FIRST_NAME!]',"".$contact->f('firstname')."",$body);
        $body=str_replace('[!CONTACT_LAST_NAME!]',"".$contact->f('lastname')."",$body);
    }else{
        $body=str_replace('[!CONTACT_FIRST_NAME!]',"",$body);
        $body=str_replace('[!CONTACT_LAST_NAME!]',"",$body);
    } 

    $body=str_replace('[!SALUTATION!]',"".$title_cont."",$body);
    $body=str_replace('[!YOUR_REFERENCE!]',"".$db->f('your_ref')."",$body);
    $body=str_replace('[!OWN_REFERENCE!]',"".$db->f('our_ref')."",$body);
    $body=str_replace('[!SERIAL_NUMBER!]',"".$db->f('serial_number')."",$body);
    $body=str_replace('[!OGM!]',"".$db->f('ogm')."",$body);
    $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].$ACCOUNT_LOGO."\">",$body);
    $body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);

    $mail->Subject = utf8_decode($subject);

    $def_email = default_email($database);
    $fromMail='noreply@akti.com';

    if($def_email['from']['email']){
          $mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']));
        }else{
            $mail->SetFrom($fromMail, $fromMail);
        }
    if($def_email['reply']['email']){
          $mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
        }else{
             $mail->AddReplyTo($fromMail, $fromMail);
        }

    if($db->f('include_pdf')){
        $in['include_pdf'] = $db->f('include_pdf');
        if($attach){
           $mail->AddStringAttachment($attach,"invoice_".$in['time'].".pdf");
        }

        // $mail->AddAttachment(__DIR__."/../pdf/invoice_".$in['time'].".pdf", 'invoice_'.$db->f('serial_number').'.pdf'); // attach files/invoice-user-1234.pdf, and rename it to invoice.pdf
    }
    if($include_xml){
        $in['include_xml'] = $include_xml;
          $xml_tmp_file_name="eff_".$database."_".$db->f('serial_number').".xml";
          $mail->AddAttachment(__DIR__."/../".$xml_tmp_file_name, "eff_".str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER)."_".substr(str_replace(array('.',' ','-','_'), '', $ACCOUNT_COMPANY),0,10)."_".$db->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $seller_bwt_nr)."_".substr(str_replace(array('.',' ','-','_'), '', $b_name),0,10).".xml");
    }

    if(!$e_lang){
        $e_lang=1;
    }
    /*$text_array = array('1' => array('simple' => array('1' => 'INVOICE WEB LINK', '2'=> 'You can download your invoice HERE','3'=>"HERE"),
                                       'pay' => array('1' => 'INVOICE WEB LINK', '2'=> 'You can download your invoice HERE and pay it online','3'=>"HERE")
                                       ),
                        '2' => array('simple' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Vous pouvez t�l�charger votre facture ICI','3'=>'ICI'),
                                       'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Vous pouvez t�l�charger votre facture ICI et la payer en ligne','3'=>'ICI')
                                       ),
                        '3' => array('simple' => array('1' => 'WEB LINK FACTUUR', '2'=> 'U kan uw factuur HIER downloaden','3'=>'HIER'),
                                       'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'U kan uw factuur HIER downloaden en online betalen','3'=>'HIER')
                                       ),
                        '4' => array('simple' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie k�nnen Ihre Rechnung HIER downloaden','3'=>'HIER'),
                                       'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie k�nnen Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER')
                                       )
    );*/
    $text_array = array('1' => array('simple' => array('1' => 'INVOICE', '2'=> 'You can download your invoice HERE','3'=>"HERE",'4'=>'Web Link'),
                       'pay' => array('1' => 'INVOICE', '2'=> 'Check it by clicking on the link above','3'=>"HERE",'4'=>'Web Link')
                       ),
              '2' => array('simple' => array('1' => 'FACTURE', '2'=> 'Vous pouvez télécharger votre facture ICI','3'=>'ICI','4'=>'Lien web'),
                       'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Cliquez sur le lienweb pour télécharger votre facture.','3'=>'ICI','4'=>'Lien web')
                       ),
              '3' => array('simple' => array('1' => 'FACTUUR', '2'=> 'U kan uw factuur HIER downloaden','3'=>'HIER','4'=>'Weblink'),
                       'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'Klik op de link hierboven om de factuur te bekijken.','3'=>'HIER','4'=>'Weblink')
                       ),
              '4' => array('simple' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
                       'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER','4'=>'WEB-LINK')
                       )
    );
    $config['web_link_url'] = 'https://my.akti.com/weblink/';
    $use_html=1;
    if($use_html==1){
        $mail->IsHTML(true);
        $head = '<style>p { margin: 0px; padding: 0px; }</style>';
        $body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 35px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 30px;">'. $body.'</div>';
        $body = stripslashes($body_style);

        if($USE_INVOICE_WEB_LINK == '1'){

            $url = generate_chars();
                while (!isUnique($url)) {
                    $url = generate_chars();
                }
                $dbu->query("INSERT INTO urls SET `url_code`='".$url."', `database`='".$database."', `item_id`='".$invoice_id."', `type`='i'  ");

            // $exist_url = $dbu->field("SELECT url_code FROM urls WHERE `database`='".$database."' AND `item_id`='".$invoice_id."' AND `type`='i' ");
            //$extra = "<p style=\"background: #dedede; width: 500px; text-align:center; padding-bottom: 15px;\"><br />";
            //$extra .="<b>".$text_array[$e_lang]['simple']['1']."</b><br />";

            /*$extra = "<p style=\"background: #f7f9fa; width: 500px; text-align:center; padding-bottom: 15px; border: 2px solid #deeaf0; color:#868d91;\"><br />";
            $extra .="<img src=\"https://app.akti.com/pim/img/email_arrow_left.png\" width=\"18\" height=\"8\" />&nbsp;&nbsp;";
            $extra .="<b style=\"color: #5199b7;\"><a href=\"".$config['web_link_url']."?q=".$url."\">".$text_array[$e_lang]['simple']['1']."</a></b>";
            $extra .="&nbsp;&nbsp;<img src=\"https://app.akti.com/pim/img/email_arrow_right.png\" width=\"18\" height=\"8\" /><br />";*/

            $extra = "<p style=\"background: #e6e6e6; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #e6e6e6; color:#868d91; border-radius:8px;\">";
            
            $extra .="<b style=\"color: #5199b7; text-align:center; font-size:32px;\"><a style=\"text-decoration:none; text-transform: lowercase; color:#6399c6;\" href=\"".$config['web_link_url']."?q=".$url."\">".utf8_decode($text_array[$e_lang]['simple']['1'])."</a></b><br />";
            /*if($const['ALLOW_WEB_PAYMENT']== 1){
                $extra .= str_replace($text_array[$e_lang]['pay']['3'], "<a href=\"".$config['web_link_url']."?q=".$url."\">".$text_array[$e_lang]['pay']['3']."</a>", $text_array[$e_lang]['pay']['2'])."<br /></p>";
            }else{
                $extra .= str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2'])."<br /></p>";
            }*/
            if($ALLOW_WEB_PAYMENT== '1'){
                    $extra .=utf8_decode($text_array[$e_lang]['pay']['2'])."<br /></p>";
            }else{
                    $extra .=utf8_decode($text_array[$e_lang]['simple']['2'])."<br /></p>";
            }
            //$body .=$extra;
            $body=str_replace('[!WEB_LINK!]', $extra, $body);
            $body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$url."\">".utf8_decode($text_array[$e_lang]['simple']['4'])."</a>", $body);
            $body=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$url, $body);
        }else{
            $body=str_replace('[!WEB_LINK!]', '', $body);
            $body=str_replace('[!WEB_LINK_2!]', '', $body);
            $body=str_replace('[!WEB_LINK_URL!]','', $body);
        }
        $mail->Body    = $head.$body;
    }else{
        if($USE_INVOICE_WEB_LINK == '1'){
            $url = generate_chars();
                while (!isUnique($url)) {
                    $url = generate_chars();
                }
                $dbu->query("INSERT INTO urls SET `url_code`='".$url."', `database`='".$database."', `item_id`='".$invoice_id."', `type`='i'  ");
            // $exist_url = $dbu->field("SELECT url_code FROM urls WHERE `database`='".$database."' AND `item_id`='".$invoice_id."' AND `type`='i' ");
            $extra = "\n\n-----------------------------------";
            $extra .="\n".$text_array[$e_lang]['simple']['1'];
            $extra .="\n-----------------------------------";
            if($ALLOW_WEB_PAYMENT== '1'){
                $extra .="\n". str_replace($text_array[$e_lang]['pay']['3'], "<a href=\"".$config['web_link_url']."?q=".$url."\">".$text_array[$e_lang]['pay']['3']."</a>", $text_array[$e_lang]['pay']['2']);
            }else{
                $extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
            }
            //$body .=$extra;
            $body=str_replace('[!WEB_LINK!]', $extra, $body);
            $body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
            $body=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$url, $body);
        }else{
            $body=str_replace('[!WEB_LINK!]', '', $body);
            $body=str_replace('[!WEB_LINK_2!]', '', $body);
            $body=str_replace('[!WEB_LINK_URL!]','', $body);
        }
        $mail->MsgHTML(nl2br(($body)));
    }
    // $mail->MsgHTML(nl2br($body));
    $send_to = '';
    $send_to_arr = array();
    if($db->f('copy')){
        $mail->AddBCC($ACCOUNT_EMAIL,$ACCOUNT_COMPANY);
    }
    //$mail->AddBCC('cristina@medeeaweb.com','cristina@medeeaweb.com');

    /*$db2->query("SELECT recurring_email_contact.*, recurring_email_contact.email AS r_email,customer_contacts.email,customer_contacts.firstname,customer_contacts.lastname
             FROM recurring_email_contact
             LEFT JOIN customer_contacts ON customer_contacts.contact_id=recurring_email_contact.contact_id
             WHERE recurring_email_contact.recurring_invoice_id ='".$recurring_invoice_id."'");
    while ($db2->move_next()) {
        if($db2->f('contact_id')){
            $mail->AddAddress($db2->f('email'),$db2->f('firstname').' '.$db2->f('lastname'));
            if(!in_array($db2->f('email'),$send_to_arr) && filter_var($db2->f('email'), FILTER_VALIDATE_EMAIL) ){
                array_push($send_to_arr, $db2->f('email'));
            }
        }else{
            $mail->AddAddress($db2->f('r_email'));
            if(!in_array($db2->f('r_email'),$send_to_arr) && filter_var($db2->f('r_email'), FILTER_VALIDATE_EMAIL) ){
                array_push($send_to_arr, $db2->f('r_email'));
            }
        }
    };
    if($buyer_id){
        $db->query("SELECT invoice_email FROM customers WHERE customer_id='{$buyer_id}' ");
        if($db->move_next()) {
            if(trim($db->f('invoice_email')) !=""){
                $mail->AddAddress($db->f('invoice_email'),$db->f('invoice_email'));
                if(!in_array($db->f('invoice_email'),$send_to_arr) && filter_var($db->f('invoice_email'), FILTER_VALIDATE_EMAIL) ){
                    array_push($send_to_arr, $db->f('invoice_email'));
                }
            }     
        }
    }*/
    $recipient_addresses=$db2->query("SELECT * FROM recurring_email_contact
             WHERE recurring_invoice_id ='".$recurring_invoice_id."'")->getAll();
    if(!empty($recipient_addresses)){
        foreach($recipient_addresses as $k1=>$v1){
            $mail->AddAddress($v1['email']);
            if(!in_array($v1['email'],$send_to_arr) && filter_var($v1['email'], FILTER_VALIDATE_EMAIL) ){
                array_push($send_to_arr, $v1['email']);
            }
        }  
        $mail_setting_option = $db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
        $sendgrid_selected = false;
        if($mail_setting_option==3){
            $sendgrid_selected =true;
        }
        if($sendgrid_selected && $copy){  
           if(!in_array($email_created_by,$send_to_arr) && filter_var($email_created_by, FILTER_VALIDATE_EMAIL) ){
                array_push($send_to_arr, $email_created_by);
            }
        }  
        $sent_date= time();
        $send_to=implode(', ', $send_to_arr);
      
        if($sendgrid_selected){
          //SendGrid Send
          $in['e_subject'] = utf8_encode(utf8_decode($subject));
          $body = '<html>'.utf8_encode($head.$body).'</html>';

          include_once(__DIR__."/../apps/misc/model/sendgrid.php");
          foreach($send_to_arr as $key=>$value){
                $in['new_email'] = $value;
                $sendgrid = new sendgrid($in, 'invoice', 'invoice_id',$invoice_id, $body,$def_email, $database, $attach);
                $sendgrid_data = $sendgrid->get_sendgrid($in);        
          }
          

        }else{
          $mail->Send();  
          
        }
        if($attach){
            $db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}This invoice was automatically sent to{endl} ').$send_to."', field_name='invoice_id', field_value='".$invoice_id."', date='".$sent_date."', type='0', reminder_date='".$sent_date."' "); 
        }
        
        
        $db->query("UPDATE tblinvoice SET sent='1',sent_date='".$sent_date."' WHERE id='".$invoice_id."'");
            
    }   
    //unlink(__DIR__."/../invoice_".$in['time'].".pdf");
    if($include_xml){
        unlink(__DIR__."/../".$xml_tmp_file_name);
    }
    return $send_to;
}
function send_reminder_email($id,$database)
{
    global $database_config;
    require_once (__DIR__.'/class.phpmailer.php');
    include_once (__DIR__.'/class.smtp.php'); // optional, gets called from within class.phpmailer.php if not already loaded
    $db_usr = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db= new sqldb($db_usr);
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_u= new sqldb($db_config);

    $activities = $db->query("SELECT * FROM customer_contact_activity WHERE customer_contact_activity_id='".$id."' ");
    $activities->next();
    $email_id= $activities->f('email_to');
    if($activities->f('contact_id')){
        $who = 'contact';
        $name = $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$activities->f('contact_id')."' ");
    }else{
        $who = 'company';
        $name = $db->field("SELECT name FROM customers WHERE customer_id='".$activities->f('customer_id')."' ");

    }
    $contacts='';
    $contact_ids = $db->query("SELECT contact_id FROM customer_contact_activity_contacts WHERE activity_id = '".$id."'");
    while ($contact_ids->next()) {
        $contacts.= $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$contact_ids->f('contact_id')."' ").',';
    }
    $contacts=rtrim($contacts,",");

    $d = $db->query("SELECT * FROM logging_tracked WHERE activity_id='".$id."' ");
    if($d->next() && $d->f('log_id')){
        $new_email = $db->field("SELECT to_user_id FROM logging WHERE log_id='".$d->f('log_id')."' ");
        if($new_email){
            $email_id = $new_email;
        }
    }

    $email = $db_u->field("SELECT email FROM users WHERE user_id='".$email_id."' ");

    $mail = new PHPMailer();

    $fromMail='noreply@akti.com';

    $subject = $activities->f('contact_activity_note');

    $body ='Subject: '.$subject.'

    Reminder: '.$activities->f('log_comment').'

    For '.$who.': '.$name.'

    '.($contacts ? $contacts.'.' : '');

    $mail->Subject = $subject;

    $mail->SetFrom($fromMail, $fromMail);
    $mail->AddReplyTo($fromMail, $fromMail);

    $mail->MsgHTML(nl2br($body));

    if(!$email){
        return false;
    }
    $mail->AddAddress($email);

    $mail->Send();
    return true;
}
function send_notification_email($id,$due=false){
     global $database_config,$config;

    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb($db_config);

    $id_exists = $db->field("SELECT users.user_id FROM users WHERE user_id='".$id."' ");
    if(!$id_exists){
        return true;  
    }

    $db_name = $db->query("SELECT users.database_name, users.email, lang.code FROM users
                           INNER JOIN lang ON users.lang_id=lang.lang_id WHERE user_id='".$id."' ");
    $db_name->next();
    $db_user = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $db_name->f('database_name'),
    ); 
    $db1= new sqldb($db_user);
    require_once (__DIR__.'/class.phpmailer.php');
    include_once (__DIR__.'/class.smtp.php'); // optional, gets called from within class.phpmailer.php if not already loaded
    $today = mktime(0, 0, 0, date('m'), date('d')-1, date('Y'));
    $today_end = mktime( 23, 59, 59, date('m'), date('d')-1, date('Y'));
    $i = 0;

    $user_info = $db->query("SELECT first_name,last_name FROM users where user_id='".$id."'")->getAll();
    $user_info = $user_info[0];    

    $log = $db1->query("SELECT * FROM logging WHERE date BETWEEN '".$today."' AND '".$today_end."' AND sent='0' AND type='1' AND to_user_id='".$id."' ");
    if($due){
        $today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $today_end = mktime( 23, 59, 59, date('m'), date('d'), date('Y'));
        $log = $db1->query("SELECT * FROM logging WHERE due_date BETWEEN '".$today."' AND '".$today_end."' AND to_user_id='".$id."' AND type='1' AND finished='0' ");
    }
    $message_log = '';
    $message_log_f = array();

    while($log->next()){
        global $site_url;
        switch ($log->f('pag')) {
            case 'order':
                $link = '<a href="'.$config['site_url'].'order/view/'.$log->f('field_value').'">order</a>';
                $pim_order = $db1->query("SELECT serial_number,customer_name  FROM pim_orders WHERE order_id='".$log->f('field_value')."'");
                $number_of= $pim_order->f('serial_number');
                $link_name =$pim_order->f('customer_name');
                break;
            case 'p_order':
                $link = '<a href="'.$config['site_url'].'purchase_order/view/'.$log->f('field_value').'">p_order</a>';
                $pim_p_order= $db1->query("SELECT serial_number, customer_name FROM pim_p_orders WHERE p_order_id='".$log->f('field_value')."'");
                $number_of= $pim_p_order->f('serial_number');
                $link_name =$pim_p_order->f('customer_name');
                break;
            case 'invoice':
                $link = '<a href="'.$config['site_url'].'invoice/view/'.$log->f('field_value').'">invoice</a>';
                $invoicetbl=$db1->query("SELECT serial_number, buyer_name FROM tblinvoice WHERE id='".$log->f('field_value')."'");
                $number_of= $invoicetbl->f('serial_number');
                $link_name =$invoicetbl->f('buyer_name');
                break;
            case 'quote':
                $link = '<a href="'.$config['site_url'].'quote/view/'.$log->f('field_value').'">quote</a>';
                $quote_name =$db1->query("SELECT serial_number, buyer_id FROM tblquote WHERE id='".$log->f('field_value')."'");
                $number_of= $quote_name->f('serial_number');
                $link_name =$quote_name->f('buyer_id');
                break;
            case 'project':
                $link = '<a href="'.$config['site_url'].'project/edit/'.$log->f('field_value').'">project</a>';
                $project =$db1->query("SELECT serial_number, company_name FROM projects WHERE project_id='".$log->f('field_value')."'");
                $number_of= $project->f('serial_number');
                $link_name =$project->f('company_name');
                break;
            case 'customer':
                $link = '<a href="'.$config['site_url'].'customerView/'.$log->f('field_value').'">customer</a>';
                $number_of= '';
                $link_name =$db1->field("SELECT name FROM customers WHERE customer_id='".$log->f('field_value')."'");
                break;
            case 'xcustomer_contact':
                $link = '<a href="'.$config['site_url'].'contactView/'.$log->f('field_value').'">contact</a>';
                $number_of= '';
                $link_name =$db1->field("SELECT company_name FROM customer_contacts WHERE customer_id='".$log->f('field_value')."'");
                break;
            default:
                $link = $log->f('pag');
                break;
        }

        $message_log = '<tr> <td style="line-height: 25px;font-size: 9pt; color: #383436;border: 0.25pt solid #B3B3B3;text-align: center;">'.utf8_decode($link_name).'</td><td style="line-height: 25px;font-size: 9pt; color: #383436;border: 0.25pt solid #B3B3B3;text-align: center;">'.$link.' '.$number_of.'</td><td style="line-height: 25px;font-size: 9pt; color: #383436;border: 0.25pt solid #B3B3B3;text-align: center;">'.utf8_decode($log->f('message')).'</td></tr>';

        array_push($message_log_f, $message_log);

        $i++;
    }

    $lang_name = $db1->field("SELECT language FROM pim_lang WHERE sort_order = '1' ");
    $content_em=$db->query("SELECT * FROM task_content WHERE task_content_id=1 ");
    switch ($lang_name) {
        case 'English':
            $body=$content_em->f('task_content_en');
            $subject=$content_em->f('subject_en');
            break;

        case 'French':
            $body=$content_em->f('task_content_fr');
            $subject=$content_em->f('subject_fr');
            break;

        case 'Dutch':
            $body=$content_em->f('task_content_nl');
            $subject=$content_em->f('subject_nl');
            break;

        case 'German':
            $body=$content_em->f('task_content_ge');
            $subject=$content_em->f('subject_ge');
            break;

        default:
            $body=$content_em->f('task_content_en');
            $subject=$content_em->f('subject_en');
            break;
    }

    $body_general='<body style="background-color: #f2f2f2;"><table style="margin: 40px auto; width:600px; border-collapse: collapse; border-spacing: 0; font: inherit;vertical-align: baseline; background-color: #fff;"><tbody><tr style=" padding-bottom: 20px; background-color: #f2f2f2; text-align: center"><td colspan="2" style="padding-bottom: 40px; text-align: center; width: 100%;  background-color: #f2f2f2;"><h1 style="text-align: center; width: 168px; height: auto; margin: 0 auto;"><a href="https://akti.com"><img src="https://my.akti.com/images/akti-logo.png" style="width: 168px; height: auto; margin: 0 auto;"></a></h1></td></tr><tr><td>';

    $cont = '<table width="90%" cellpadding="0" style="margin: 0 auto;border-collapse: collapse;"><thead><tr><th width="30%" style="line-height: 25px;font-size: 10pt; color: #808080;border: 0.25pt solid #B3B3B3;text-align: center;">'.gm('Customer name').'</th><th width="25%"style="line-height: 25px;font-size: 10pt; color: #808080;border: 0.25pt solid #B3B3B3;text-align: center;">'.gm('Item').'</th><th width="45%"style="line-height: 25px;font-size: 10pt; color: #808080;border: 0.25pt solid #B3B3B3;text-align: center;">'.gm('Description').'</th></tr></thead><tbody>';            
    foreach($message_log_f as $data){
        $cont .= utf8_decode($data);
    }
    $cont .= '</body></table>';

    $body=str_replace('[!CONTENT!]',$cont,$body);
    $body=str_replace('[!FIRSTNAME!]',utf8_decode($user_info['first_name']),$body);
    $body=str_replace('[!LASTNAME!]',utf8_decode($user_info['last_name']),$body);

    $body_general.=$body;
    $body_general.='</td>
           </tr>     
          </tbody>
    </table>
</body>';   
    
    if($i == 0 ){
        return false;
    }
    $code = $db_name->f('code');
    if($code == 'nl'){$code = 'du';}
    $mail = new PHPMailer();
    
    $fromMail='noreply@akti.com';

    $mail->Subject = $subject;

    $mail->SetFrom($fromMail, $fromMail);
    $mail->AddReplyTo($fromMail, $fromMail);

    $mail->MsgHTML(nl2br($body_general));
    if(!$db_name->f('email')){
        return false;
    }
    $mail->AddAddress($db_name->f('email'));
    echo "notification sent2 \n";
    if(!$due){
        // $log = $db1->query("UPDATE logging SET sent='1' WHERE date BETWEEN '".$today."' AND '".$today_end."' ");
    }
    $mail->Send();
    return true;
}

function send_inv_manag_email($inv_manag_email,$created_rec_inv,$database)
{
    global $config,$database_config;
    $today = mktime(5, 0, 0, date('m'), date('d'), date('Y'));
    $today = date(ACCOUNT_DATE_FORMAT,  $today);
    /*foreach ($created_rec_inv as $key => $value) {
        $href = 'href="'.$config['site_url'].'invoice/view/'.$value['invoice_id'].'"';
        $invoice_data .=$value['serial_number']." - client name: ".stripslashes(htmlentities($value['buyer_name'],ENT_COMPAT | ENT_HTML401,'UTF-8')).",  "."\n";
    }
    $invoice_data = rtrim(rtrim($invoice_data,"\n"),', ');*/
    require_once (__DIR__.'/class.phpmailer.php');
    include_once (__DIR__.'/class.smtp.php');
    $mail = new PHPMailer();

    $fromMail='noreply@akti.com';
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_user= new sqldb($db_config);
    $db_usr = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_usr);
    $lang_name = $db->field("SELECT language FROM pim_lang WHERE sort_order = '1' ");
    $content_em=$db_user->query("SELECT * FROM emails WHERE type='recurring_mail' ");
    switch ($lang_name) {
        case 'English':
            $body=$content_em->f('mail_content_en');
            $subject=$content_em->f('subject_en');
            $inv_draft='Invoice created in draft';
            $inv_sent='Invoice has been sent to';
            $inv_sent_no_email='No invoicing email address defined. Invoice created as draft';
            /*$body='Dear customer,

Today, '.$today.', Akti created the following draft invoices based on the recurrence setting:

'.$invoice_data.'.

You receive this notification as a manager for the invoices.
Log in to Akti to manage your invoices: my.akti.com

Best regards,
Akti Customer Care
<a href="https://my.akti.com" >my.akti.com</a>';

    $subject = 'Invoices created at '.$today;*/
            break;

        case 'French':
            $body=$content_em->f('mail_content_fr');
            $subject=$content_em->f('subject_fr');
            $inv_draft='Facture cr��e en brouillon';
            $inv_sent='Facture a �t� envoy�e �';
            $inv_sent_no_email="Pas d'adresse e-mail de facturation d�finit. Facture cr�� en status brouillon";
            /*$invoice_data = str_replace('client name', 'le nom du client', $invoice_data);
            $body='Cher client,

Aujourd�hui, '.$today.', Akti a cr�� les factures suivantes en fonction du param�tre de r�currence:

'.$invoice_data.'.

Vous recevez cette notification en tant que responsable de la facturation.
Connectez-vous � Akti pour g�rer vos factures: my.akti.com

Cordialement
L��quipe Akti
<a href="https://my.akti.com" >my.akti.com</a>';

    $subject = 'Factures cr��es � '.$today;*/
            break;

        case 'Dutch':
            $body=$content_em->f('mail_content_nl');
            $subject=$content_em->f('subject_nl');
            $inv_draft='Factuur gemaakt in ontwerp';
            $inv_sent='Factuur werd verzonden naar';
            $inv_sent_no_email="Geen facturatie e-mailadres gedefinieerd. Factuur aangemaakt als draft";
            /*$invoice_data = str_replace('client name', 'klantnaam', $invoice_data);
            $body='Beste klant,

Vandaag, '.$today.', heeft Akti de volgende facturen aangemaakt volgens de instelling van terugkerende facturen:

'.$invoice_data.'.

U ontvangt deze mededeling omdat u beheerder bent van de facturen.
Meldt u aan in Akti om uw facturen te beheren: my.akti.com

Met vriendelijke groeten,
Akti Customer Care
<a href="https://my.akti.com" >my.akti.com</a>';

    $subject = 'Facturen aangemaakt op '.$today;
            break;*/

        case 'German':
            $body=$content_em->f('mail_content_ge');
            $subject=$content_em->f('subject_ge');
            $inv_draft='Invoice created in draft';
            $inv_sent='Invoice has been sent to';
            $inv_sent_no_email='No invoicing email address defined. Invoice created as draft';
            /*$body='Dear customer,

Today, '.$today.', Akti created the following draft invoices based on the recurrence setting:

'.$invoice_data.'.

You receive this notification as a manager for the invoices.
Log in to Akti to manage your invoices: my.akti.com

Best regards,
Akti Customer Care
<a href="https://my.akti.com" >my.akti.com</a>';

    $subject = 'Invoices created at '.$today;*/
            break;

        default:
            $body=$content_em->f('mail_content_en');
            $subject=$content_em->f('subject_en');
            $inv_draft='Invoice created in draft';
            $inv_sent='Invoice has been send to';
            $inv_sent_no_email='No invoicing email address defined. Invoice created as draft';
            /*$body='Dear customer,

Today, '.$today.', Akti created the following draft invoices based on the recurrence setting:

'.$invoice_data.'.

You receive this notification as a manager for the invoices.
Log in to Akti to manage your invoices: my.akti.com

Best regards,
Akti Customer Care
<a href="https://my.akti.com" >my.akti.com</a>';

    $subject = 'Invoices created at '.$today;*/
            break;
    }
    $body_general='<body style="background-color: #f2f2f2;">
    <table style="margin: 40px auto; width:600px; border-collapse: collapse; border-spacing: 0; font: inherit;vertical-align: baseline; background-color: #fff;">
        <tbody>
          <tr style=" padding-bottom: 20px; background-color: #f2f2f2; text-align: center">
                    <td colspan="2" style="padding-bottom: 40px; text-align: center; width: 100%;  background-color: #f2f2f2;"><h1 style="text-align: center; width: 168px; height: auto; margin: 0 auto;"><a href="https://akti.com"><img src="https://my.akti.com/images/akti-logo.png" style="width: 168px; height: auto; margin: 0 auto;"></a></h1></td>
           </tr>
           <tr>
             <td>';

    $cont='<table border="1" width="100%" cellpadding="1"><thead><tr><th style="text-align: center" width="40%">'.gm('Company').'</th><th style="text-align: center" width="30%">'.gm('Invoice').'</th><th style="text-align: center" width="40%">'.gm('Status').'</th></tr></thead><tbody>';
    foreach($created_rec_inv as $key => $value){
        //$inv_status=$value['status'] ? $inv_sent.' '.rtrim($value['emails'],',') : $inv_draft;
        $inv_status=$value['status'] && $value['emails'] ? $inv_sent.' '.rtrim($value['emails'],',') : ($value['status'] && !$value['emails'] ? $inv_sent_no_email : $inv_draft);
        $cont.='<tr><td style="text-align: center">'.stripslashes(htmlentities($value['buyer_name'],ENT_COMPAT | ENT_HTML401,'UTF-8')).'</td><td style="text-align: center">'.$value['serial_number'].'</td><td style="text-align: center">'.$inv_status.'</td></tr>';
    }
    $cont.='</tbody></table>';
    $body=str_replace('[!TODAY!]',$today,$body);
    $body=str_replace('[!CONTENT!]',$cont,$body);

    $body_general.=$body;
    $body_general.='</td>
           </tr>     
          </tbody>
    </table>
</body>';

    $subject=str_replace('[!TODAY!]',$today,$subject);
    $mail->Subject = $subject;

    $mail->SetFrom($fromMail, $fromMail);
    // $mail->AddReplyTo($fromMail, $fromMail);

    //$mail->MsgHTML(nl2br($body));
    $mail->MsgHTML($body_general);

    $mail->AddAddress($inv_manag_email);

    $mail->Send();
    return true;
}


function correct_val($number){
    $value=explode(":",$number);
    $nr_string1=(int)$value[0];
    $nr_string2=((int)$value[1])*100/60;
    $final_string=$nr_string1.'.'.((int)$value[1] < 6 ? '0'.$nr_string2 : $nr_string2);
    return (float)$final_string;
}
function get_contact_name($id){
    $db = new sqldb();
    $db->query("SELECT lastname, firstname FROM customer_contacts WHERE contact_id='".$id."' ");
    $db->next();
    return $db->f('lastname').' '.$db->f('firstname');
}

function get_contact_title($id){
    $db = new sqldb();
    $db->field("SELECT name FROM customer_contact_title WHERE id = '".$id."'  ");
    $db->next();
    return $db->f('name');
}

function insert_user_activity($db){
   global $config;
    if(isset($_SESSION['logedAsAdmin']) && $_SESSION['logedAsAdmin'] == 1){
        return;
    }
/*    global $config;
    if(!$config['debug']){
            $db->query("UPDATE user_activity SET expired='1' WHERE user_id='".$_SESSION['u_id']."' AND HTTP_USER_AGENT='".$_SERVER['HTTP_USER_AGENT']."' AND activity_id<>'".$id."' ");
    }*/
    if($_SESSION['u_id']){
        $session = session_id();
        $info = $db->query("SELECT activity_id,expired FROM user_activity WHERE session_id='".$session."' AND user_id='".$_SESSION['u_id']."' ");
        $id = $info->f('activity_id');
        if($id){
            $extra = ",
                                    QUERY_STRING='".addslashes($_SERVER['QUERY_STRING'])."',
                                    REQUEST_URI='".addslashes($_SERVER['REQUEST_URI'])."'";
            if($_SERVER['QUERY_STRING']=='do=misc-is_notice'){
                $extra ="";
            }
            $db->query("UPDATE user_activity SET
                                    REQUEST_TIME_FLOAT='".(isset($_SERVER['REQUEST_TIME_FLOAT']) ? $_SERVER['REQUEST_TIME_FLOAT'] : $_SERVER['REQUEST_TIME'])."'
                                    ".$extra."
                                    WHERE activity_id='".$id."' ");
            if($info->f('expired') == '1'){
                unset($_SESSION['u_id']);
            }
        }else{
            $id = $db->insert("INSERT INTO user_activity SET
                                user_id='".$_SESSION['u_id']."',
                                HTTP_USER_AGENT='".$_SERVER['HTTP_USER_AGENT']."',
                                HTTP_REFERER='".$_SERVER['HTTP_REFERER']."',
                                REMOTE_ADDR='".$_SERVER['REMOTE_ADDR']."',
                                REMOTE_PORT='".$_SERVER['REMOTE_PORT']."',
                                REQUEST_METHOD='".$_SERVER['REQUEST_METHOD']."',
                                REQUEST_TIME_FLOAT='".$_SERVER['REQUEST_TIME_FLOAT']."',
                                QUERY_STRING='".addslashes($_SERVER['QUERY_STRING'])."',
                                REQUEST_URI='".addslashes($_SERVER['REQUEST_URI'])."',
                                session_id='".$session."' ");
        }
       

         if(!$config['debug']){
            $db->query("UPDATE user_activity SET expired='1' WHERE user_id='".$_SESSION['u_id']."' AND HTTP_USER_AGENT='".$_SERVER['HTTP_USER_AGENT']."' AND activity_id<>'".$id."' ");
        }
    }

}

/**
 * Used to log actions and allow tracebility between crm,quotes,projects,interventions,orders, p orders,invoices
 * It accepts 2 parameters: 1 associative array parameter with the REQUIRED following keys:
 * - target_id (INT - ex. invoice_id number)
 * - target_type (INT - values: 1-invoice,2-quote,3-project,4-order,5-intervention,6-p order,7-proforma,8-credit invoice,9-contracts,10-purchase invoice,11-deals,12-installation,13-recurring invoice)
 * - target_buyer_id (INT - destination buyer id)
 * - lines(optional): array containing multiple associative arrays with the following keys:
 *          - origin_id (INT - the origin module id, ex. invoice_id number)
 *          - origin_type (INT - same values as target_type)
 * AND 2 parameter trace_id if needed to update the lines
 * @return void
 * @author 
 **/
function addTracking($data,$trace_id=0,$database=null){
    global $database_config;
    if($database){
        $database_1 = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
            );
        $db = new sqldb($database_1);
    }else{
        $db = new sqldb();
    } 
    /*if(empty($data['lines'])){
        return;
    }*/
    if(!$trace_id){
        if(!empty($data['lines'])){
            switch($data['lines'][0]['origin_type']){
                case '1':
                case '7':
                case '8':
                    $table="tblinvoice";
                    $table_id="id";
                    $customer_id="buyer_id";
                    break;
                case '2':
                    $table="tblquote";
                    $table_id="id";
                    $customer_id="buyer_id";
                    break;
                case '3':
                    $table="projects";
                    $table_id="project_id";
                    $customer_id="customer_id";
                    break;
                case '4':
                    $table="pim_orders";
                    $table_id="order_id";
                    $customer_id="customer_id";
                    break;
                case '5':
                    $table="servicing_support";
                    $table_id="service_id";
                    $customer_id="customer_id";
                    break;
                case '6':
                    $table="pim_p_orders";
                    $table_id="p_order_id";
                    $customer_id="customer_id";
                    break;
                case '9':
                    $table="contracts";
                    $table_id="contract_id";
                    $customer_id="customer_id";
                    break;
                case '10':
                    $table="tblinvoice_incomming";
                    $table_id="invoice_id";
                    $customer_id="supplier_id";
                    break;
                case '11':
                    $table='tblopportunity';
                    $table_id="opportunity_id";
                    $customer_id="buyer_id";
                    break;
                case '12':
                    $table='installations';
                    $table_id="id";
                    $customer_id="customer_id";
                    break;
                case '13':
                    $table='recurring_invoice';
                    $table_id="recurring_invoice_id";
                    $customer_id="buyer_id";
                    break;
            }
            $origin_buyer_id=$db->field("SELECT ".$customer_id." FROM ".$table." WHERE ".$table_id."='".$data['lines'][0]['origin_id']."' ");
        }else{
            $origin_buyer_id=$data['target_buyer_id'];
        }    
        $trace_id=$db->insert("INSERT INTO tracking SET
                                origin_buyer_id  ='".$origin_buyer_id."',
                                target_id        ='".$data['target_id']."',             
                                target_type      ='".$data['target_type']."',
                                target_buyer_id  ='".$data['target_buyer_id']."',
                                creation_date    ='".time()."',
                                creation_user_id ='".$_SESSION['u_id']."',
                                archived         ='0' ");
        switch($data['target_type']){
            case '1':
            case '7':
            case '8':
                $table_target="tblinvoice";
                $table_target_id="id";
                break;
            case '2':
                $table_target="tblquote";
                $table_target_id="id";
                break;
            case '3':
                $table_target="projects";
                $table_target_id="project_id";
                break;
            case '4':
                $table_target="pim_orders";
                $table_target_id="order_id";
                break;
            case '5':
                $table_target="servicing_support";
                $table_target_id="service_id";
                break;
            case '6':
                $table_target="pim_p_orders";
                $table_target_id="p_order_id";
                break;
            case '9':
                $table_target="contracts";
                $table_target_id="contract_id";
                break;
             case '10':
                $table_target="tblinvoice_incomming";
                $table_target_id="invoice_id";
                break;
            case '11':
                $table_target='tblopportunity';
                $table_target_id="opportunity_id";
                break;
            case '12':
                $table_target='installations';
                $table_target_id="id";
                break;
            case '13':
                $table_target='recurring_invoice';
                $table_target_id="recurring_invoice_id";
                break;
        }
        $db->query("UPDATE ".$table_target." SET trace_id='".$trace_id."' WHERE ".$table_target_id."='".$data['target_id']."' ");
    }
    foreach($data['lines'] as $key=>$value){
        $db->query("INSERT INTO tracking_line SET
                            origin_id             ='".$value['origin_id']."',
                            origin_type           ='".$value['origin_type']."',
                            trace_id              ='".$trace_id."' ");
    }
}

function build_supplier_dd($selected= false,$txt = false){
    $db = new sqldb();
    $array = array();
    if($txt){
        if($selected){
            return $db->field("SELECT name FROM customers WHERE id='".$selected."'");
        }
        return '';
    }
    $db->query("SELECT name, customer_id FROM customers WHERE is_supplier='1'");
    while($db->move_next()){
        $array[$db->f('customer_id')] = $db->f('name');
    }
    return build_simple_dropdown($array,$selected);
}
function build_contract_budget_type_list($selected = false,$hide='',$dd = true){
    $array = array('1' => gm('No budget'),'2' => gm('Total contract hours'));
    if($dd === false){
        return $array[$selected];
    }
    $opt=array();
    foreach ($array as $key => $value) {
        array_push($opt,array('id'=>$key,'name'=>$value));
    }
    return $opt;
}
function generate_contract_number($database)
{
    global $database_config;
        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );

    $db = new sqldb($db_config);
    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CONTRACT_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CONTRACT_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM contracts
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function get_last_contract_number()
{
    $db=new sqldb();

    $prefix_lenght=strlen(ACCOUNT_CONTRACT_START)+1;
    $last_serial='';
    $db->query("SELECT serial_number FROM contracts  ORDER BY contract_id DESC LIMIT 1");
    $db->move_next();
    if($db->f('serial_number')){
        $last_serial='('.gm("Last Used").': <span>'.$db->f('serial_number').'</span>)';
    }

    return $last_serial;
}
function build_contract_list($selected = false){
    $array=array('1' => gm('Product Contract'),'2' => gm('Service Contract'));
    $opt=array();
    foreach ($array as $key => $value) {
        array_push($opt,array('id'=>$key,'name'=>$value));
    }
    return $opt;
}
function build_reminder_type_list($selected = false,$hide='',$dd = true){
    $array = array('0' => gm('Never'),'1' => gm('1 month before End Date'),'2' => gm('2 months before End Date'),'3'=>gm('Custom Date'));
    if($dd === false){

        return $array[$selected];
    }
    $opt=array();
    foreach ($array as $key => $value) {
        array_push($opt,array('id'=>$key,'name'=>$value));
    }
    return $opt;
}
function build_invoicing_type_list($selected = false,$hide='',$dd = true){
    $array = array('0' => gm('Manual Invoice'),'1' => gm('Recurring Invoices'));
    if($dd === false){

        return $array[$selected];
    }
    $opt=array();
    foreach ($array as $key => $value) {
        array_push($opt,array('id'=>$key,'name'=>$value));
    }
    return $opt;
}
function build_data_cc($term='')
{

    $q = strtolower($term);

    $filter = '1=1';

    if($q){
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();
    $s = $db->query("SELECT * FROM contract_lost_reason WHERE  ".$filter." ORDER BY name ASC ")->getAll();
    $users =array();
    foreach ($s as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>utf8_encode($value['name'])));
    }
    
    return $users;
}
function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid =substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12);
        return $uuid;
    }
}

function generate_installation_number($database=''){
    if($database){
        global $database_config,$cfg;
        $db_conf = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );
        $db = new sqldb($db_conf);
    }else{
        $db = new sqldb();
    }

    $installation_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INSTALLATION_START' ");
    if(!$installation_start){
        $installation_start='inst-';
    }
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INSTALLATION_DIGIT_NR' ");
    $prefix_lenght=strlen($installation_start)+1;
    $db->query("SELECT id,serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS installation_number,LEFT( serial_number, $prefix_lenght-1 ) AS installation_prefix
                    FROM installations
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($installation_start)."'
                    ORDER BY installation_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    if($db->move_next() && $db->f('installation_prefix')==$installation_start){
        $next_serial=$installation_start.str_pad($db->f('installation_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$installation_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}

// functie de sortare a credentialelor unui user pt. a fi afisate corect pe pagina de my account
// functie de sortare a credentialelor unui user pt. a fi afisate corect pe pagina de my account
function sortCred ($arr){
    $sorter = array(1,12,5,6,14,16,3,19,13,17,4,15,11,2,7,8,9,10,100,18);
    $flipped = array_flip($sorter);
    $newArray=array();
    foreach ($arr as $key => $value) {
        $newArray[$flipped[$value]]=$value;
    }
    ksort($newArray);
    return $newArray;
}

function insert_error_call($database,$app,$module,$module_id,$message){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db=new sqldb($db_config);
    $db->query("INSERT INTO error_call SET
                    app         = '".$app."',
                    module      = '".$module."',
                    module_id   = '".$module_id."',
                    date        = '".time()."',
                    message     = '".addslashes($message)."' ");
    $allowed_apps=['yuki','codabox','clearfacts','billtobox','exact_online','winbooks','btb','jefacture'];

    if(in_array($app, $allowed_apps)){
        //add notification to admin users
        $db_config_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $db_users = new sqldb($db_config_users);

        $admin_users=$db_users->query("SELECT users.user_id FROM users LEFT JOIN user_meta ON users.user_id=user_meta.user_id AND user_meta.name='active' WHERE users.database_name='".$database."' AND users.active<>1000 AND users.group_id='1' AND user_meta.value IS NULL GROUP BY users.user_id");
        while($admin_users->next()){
            $db_users->query("INSERT INTO `notifications` SET 
                    db_name='".$database."',
                    user_id='".$admin_users->f('user_id')."',
                    module_name='".$module."',
                    module_id='".$module_id."',
                    action_type='export-fail.".$app."',
                    created_at='".time()."' ");
        }
    }

    return true;
}

function str_replace_last( $search , $replace , $str ) {
    if( ( $pos = strrpos( $str , $search ) ) !== false ) {
        $search_length  = strlen( $search );
        $str    = substr_replace( $str , $replace , $pos , $search_length );
    }
    return $str;
}

function get_currency_sign($number){
    $db=new sqldb();
    $currency_code=$db->field("SELECT value FROM currency WHERE id='ACCOUNT_CURRENCY_TYPE' ");
    if(!$currency_code){
        $currency_code='&euro;';
    }
    $account_currency = $db->field("SELECT value from settings where constant_name='ACCOUNT_CURRENCY_FORMAT'");
    if(is_null($account_currency) || $account_currency=='0'){
        $new_number=$number.' '.html_entity_decode($currency_code, ENT_COMPAT, 'UTF-8');
    }else{
        $new_number=html_entity_decode($currency_code, ENT_COMPAT, 'UTF-8').' '.$number;
    }
    return $new_number;
}

function get_customer_article_data($article_id,$customer_id='',$contact_id='',$old_currency_type=ACCOUNT_CURRENCY_TYPE){
    $db=new sqldb;
    $db2=new sqldb;
    $article_data = array();
    // echo $article_id.'-'.$customer_id.'-'.$contact_id.'<br>';
    if(!$article_id){
        $article_data['content'] = 1;
        return $article_data;
    }
    $article_data['default_currency_type'] = ACCOUNT_CURRENCY_TYPE;

    $article_data['old_currency_type'] = $old_currency_type;
    $article_data['old_currency_name'] = build_currency_name_list($old_currency_type);

    $article_data['currency_type'] = ACCOUNT_CURRENCY_TYPE;
    $article_data['currency_name'] = build_currency_name_list(ACCOUNT_CURRENCY_TYPE);

    if($customer_id){
        $customer_data = $db->query("SELECT * FROM customers WHERE customer_id = '".$customer_id."' ");
        while($customer_data->next()){
            $def_lang   = $customer_data->f('internal_language');
            $cat_id     = $customer_data->f('cat_id');

            $global_discount = $customer_data->f('fixed_discount');
            $line_discount = $customer_data->f('line_discount');


            if($customer_data->f('apply_fix_disc') && $customer_data->f('apply_line_disc')){
                $apply_discount = 3;
            }else{
                if($customer_data->f('apply_fix_disc')){
                    $apply_discount = 2;
                }
                if($customer_data->f('apply_line_disc')){
                    $apply_discount = 1;
                }
            }
            $article_data['customer_name'] = $customer_data->f('name');
            $article_data['customer_id'] = $customer_id;
            if($customer_data->f('no_vat')){
                $article_data['remove_vat'] = 1;
            }else{
                $article_data['remove_vat'] = 0;
            }
            if($customer_data->f('currency_id')){
                $article_data['currency_type'] = $customer_data->f('currency_id');
                $article_data['currency_name'] = build_currency_name_list($customer_data->f('currency_id'));
            }


        }
        if(!$def_lang){
            $def_lang   = DEFAULT_LANG_ID;
        }
        $buyer = $db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$customer_id."' ORDER BY is_primary DESC ");
        $buyer->next();
        $article_data['customer_address'] = $buyer->f('address');
        $article_data['customer_zip'] = $buyer->f('zip');
        $article_data['customer_city'] = $buyer->f('city');
        $article_data['customer_country'] = get_country_name($buyer->f('country_id'));
        $article_data['customer_country_id'] = $buyer->f('country_id');
$article_data['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').' '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));
        if($contact_id){
            $contact = $db->query("SELECT firstname, lastname, title FROM customer_contacts WHERE contact_id = '".$contact_id."' ");
            $contact->next();
            $contact_title = $db->field("SELECT name FROM customer_contact_title WHERE id = '".$contact->f('title')."' ");
            if($contact_title){
                $contact_title .= " ";
            }
            $article_data['contact_name'] = $contact_title.$contact->f('firstname').' '.$contact->f('lastname');
            $article_data['contact_id'] = $contact_id;
$article_data['address_info'] = $article_data['contact_name'].'
'.$article_data['address_info'];
        }
    }else{
        $def_lang       = DEFAULT_LANG_ID;
        $cat_id         = 0;
        $global_discount    = 0;
        $line_discount  = 0;
        $apply_discount     = 0;
        $article_data['remove_vat'] = 0;
        if($contact_id){
            $buyer = $db->query("SELECT * FROM customer_contact_address WHERE contact_id = '".$contact_id."' ORDER BY is_primary DESC ");
            $article_data['contact_address'] = $buyer->f('address');
            $article_data['contact_zip'] = $buyer->f('zip');
            $article_data['contact_city'] = $buyer->f('city');
            $article_data['contact_country'] = get_country_name($buyer->f('country_id'));
            $article_data['contact_country_id'] = $buyer->f('country_id');
            $contact = $db->query("SELECT firstname, lastname, title FROM customer_contacts WHERE contact_id = '".$contact_id."' ");
            $contact->next();
            $contact_title = $db->field("SELECT name FROM customer_contact_title WHERE id = '".$contact->f('title')."' ");
            if($contact_title){
                $contact_title .= " ";
            }
            $article_data['contact_name'] = $contact_title.$contact->f('firstname').' '.$contact->f('lastname');
            $article_data['contact_id'] = $contact_id;
$article_data['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').' '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));
        }
    }

    $article = $db->query(" SELECT      pim_articles.*,pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
                                pim_article_prices.price, pim_articles.internal_name,
                                pim_article_brands.name AS article_brand,
                                pim_articles_lang.description AS description,
                                pim_articles_lang.name2 AS item_name2,
                                pim_articles_lang.name AS item_name,
                                pim_articles_lang.lang_id,
                                pim_articles.vat_id,
                                pim_articles.block_discount
                    FROM            pim_articles
                    INNER JOIN      pim_article_prices
                    ON          pim_articles.article_id = pim_article_prices.article_id
                    INNER JOIN      pim_articles_lang
                    ON          pim_articles.article_id = pim_articles_lang.item_id
                    LEFT JOIN       pim_article_brands
                    ON          pim_article_brands.id = pim_articles.article_brand_id
                    WHERE       pim_articles_lang.lang_id='".$def_lang."'
                    AND             pim_articles.active='1'
                    AND             pim_articles.article_id = '".$article_id."'
                    GROUP BY        pim_articles.article_id ");

    $article->move_next();

    $values = $article->next_array();
    $tags = array_map(function($field){
        return '/\[\!'.strtoupper($field).'\!\]/';
    },array_keys($values));

    $fieldFormat = $db2->field("SELECT      long_value
                    FROM            settings
                    WHERE       constant_name='ORDER_FIELD_LABEL'");

    $article_content = preg_replace($tags, $values, $fieldFormat);

    $vat = $db2->field("    SELECT      value
                    FROM            vats
                    WHERE       vat_id = '".$article->f('vat_id')."'");
    if($article_data['remove_vat']){
        $vat=0;
    }
    $stock = $db2->field("  SELECT      stock
                    FROM            pim_articles
                    WHERE       article_id='".$article_id."' ");

    if($article->f('price_type')==1){
        $price = $db2->field("SELECT        pim_article_prices.price
                    FROM            pim_article_prices
                    WHERE       pim_article_prices.price_category_id='".$cat_id."'
                    AND             pim_article_prices.article_id='".$article->f('article_id')."'");


            if(!$price || $article->f('block_discount')==1 ){
                $price = $db2->field("SELECT    pim_article_prices.price
                        FROM            pim_article_prices
                        WHERE       pim_article_prices.price_category_id=0
                        AND             pim_article_prices.article_id='".$article->f('article_id')."'
                        AND             base_price=1");
            }

        }else{
            $price = $db->field("SELECT         pim_article_prices.price
                        FROM            pim_article_prices
                        WHERE       pim_article_prices.from_q='1'
                        AND             pim_article_prices.article_id='".$article->f('article_id')."'");

            if(!$price || $article->f('block_discount')==1 ){
                $price = $db2->field("SELECT    pim_article_prices.price
                        FROM            pim_article_prices
                        WHERE       pim_article_prices.price_category_id=0
                        AND             pim_article_prices.article_id='".$article->f('article_id')."'
                        AND base_price=1");
            }

        }

        $pending_articles = $db2->field("SELECT     SUM(pim_order_articles.quantity) AS pending_articles
                        FROM        pim_order_articles
                        WHERE       pim_order_articles.article_id='".$article->f('article_id')."'
                        AND             delivered=0 ");

    $base_price = $db2->field("SELECT       price
                    FROM            pim_article_prices
                    WHERE       article_id='".$article->f('article_id')."'
                    AND             base_price='1' ");
    // echo $price.'<br>';
    if(!$price){
       $price = $base_price; 
    }
    $price_with_vat = $price+($price*$vat/100);

    if($article_data['old_currency_type'] && $article_data['old_currency_type']!=$article_data['currency_type']){
        $currency_rate = json_encode(currency::getCurrency($article_data['old_currency_name'], $article_data['currency_name'], 1,ACCOUNT_NUMBER_FORMAT));;
        // echo $article_data['old_currency_name'].' - '.$article_data['currency_name'].' - '.return_value($currency_rate).'<br/>';
        $price = $price*$currency_rate;
        $price_with_vat = $price+($price*$vat/100);
    }

    $article_data['article_id']             = $article->f('article_id');
    $article_data['article_code']       = $article->f('item_code');
    $article_data['price']              = $price;
    $article_data['vat_value']          = $vat;
    $article_data['price_with_vat']     = $price_with_vat;
    $article_data['sale_unit']          = $article->f('sale_unit');
    $article_data['stock']              = $stock;
    $article_data['packing']            = $article->f('packing');
    $article_data['pending_articles']       = $pending_articles;
    $article_data['threshold_value']        = $article->f('article_threshold_value');
    $article_data['hide_stock']         = $article->f('hide_stock');
    $article_data['article']            = $article_content;
    $article_data['global_discount']        = $global_discount;
    $article_data['line_discount']      = $line_discount;
    $article_data['apply_discount']     = $apply_discount;
    $article_data['languages']          = $def_lang;



    return $article_data;
}

function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {

            if (is_array($v)) {

                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
function synctoZendesk($data){
    $db=new sqldb();
    $ch = curl_init();
    $zen_data=$db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
    $zen_api=$zen_data->f('api');
    $zen_email=$db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
    $zen_pass=$db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
    if($data['contact_id']){
        $zen_org_id=$db->field("SELECT zen_organization_id FROM customers WHERE customer_id='".$data['customer_id']."' ");
        $zen_contact_id=$db->field("SELECT zen_contact_id FROM customer_contacts WHERE contact_id='".$data['contact_id']."' ");
        if($data['customer_id'] && !$zen_org_id){   
            $organization_data=array('organization'=>array('name'=>trim($data['customer_name'])));
            $organization_data=json_encode($organization_data);

            $params=urlencode('type:organization name:'.trim($data['customer_name']));
            $headers=array('Content-Type: application/json');
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
            curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $zen_api.'/api/v2/search.json?query='.$params);

            $put = curl_exec($ch);
            $info = curl_getinfo($ch);

            if($info['http_code']>300 || $info['http_code']==0){
                    //
            }else{
                $org_data=json_decode($put);
                if(empty($org_data->results)){
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                    curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $organization_data);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_URL, $zen_api.'/api/v2/organizations.json');

                    $put = curl_exec($ch);
                    $info = curl_getinfo($ch);

                    if($info['http_code']>300 || $info['http_code']==0){
                            //
                    }else{
                        $data_c=json_decode($put);
                        $db->query("UPDATE customers SET zen_organization_id='".$data_c->organization->id."' WHERE customer_id='".$data['customer_id']."' ");
                        $db->query("INSERT INTO zen_accounts SET 
                                    zen_id='".$data_c->organization->id."',
                                    customer_id='".$data['customer_id']."',
                                    name='".addslashes(trim($data['customer_name']))."' ");
                        }
                        $zen_org_id=$data_c->organization->id;
                }else{
                    $db->query("UPDATE customers SET zen_organization_id='".$org_data->results[0]->id."' WHERE customer_id='".$data['customer_id']."' ");
                    $zen_org_id=$org_data->results[0]->id;
                }
            }
        }
        if($data['op']=='add'){
            $organization_user=array(
                'user'=>array(
                    'name'          => trim($data['firstname']).' '.trim($data['lastname']),
                    'email'         => $data['email'],
                    'verified'      => true,
                    'role'          => 'end-user',
                )
            );
            if($data['customer_name']){
                $organization_user['user']['organization']=array('name'=>$data['customer_name']);
            }
            if($data['customer_id']){
                $comp_phone=$db->field("SELECT comp_phone FROM customers WHERE customer_id='".$data['customer_id']."' ");
                if($comp_phone){
                    $organization_user['user']['shared_phone_number']=$comp_phone;
                }
                $contact_data=$db->query("SELECT customer_contacts.cell,customer_contactsIds.phone FROM customer_contactsIds
                    INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
                    WHERE customer_contactsIds.contact_id='".$data['contact_id']."' AND customer_contactsIds.customer_id='".$data['customer_id']."' ");
                if($contact_data->f('phone')){
                    $organization_user['user']['phone']=$contact_data->f('phone');
                }else if($contact_data->f('cell')){
                    $organization_user['user']['phone']=$contact_data->f('cell');
                }
            }else{
                $contact_mobile=$db->field("SELECT cell FROM customer_contacts WHERE contact_id='".$data['contact_id']."' ");
                if($contact_mobile){
                    $organization_user['user']['phone']=$contact_mobile;
                }
            }
            $l_code=$db->field("SELECT lang.code FROM customer_contacts
                INNER JOIN lang ON customer_contacts.language=lang.lang_id 
                WHERE customer_contacts.contact_id='".$data['contact_id']."' ");
            if($l_code){
                switch($l_code){
                    case 'nl':
                        $lang_code="nl";
                        break;
                    case 'en':
                        $lang_code="en-US";
                        break;
                    case 'fr':  
                        $lang_code="fr";
                        break;
                    case 'de':
                        $lang_code="de";
                        break;
                }
                $organization_user['user']['locale']=$lang_code;
            }
            $custom_fields=$db->query("SELECT * FROM zen_fields WHERE `master`='contact' ")->getAll();
            if(!empty($custom_fields)){
                foreach($custom_fields as $key=>$value){
                    $key_data=$db->query("SELECT label_name,field_name,tabel_name,is_custom_dd FROM import_labels WHERE import_label_id='".$value['our_field']."' ");
                    if($key_data->f('tabel_name')=='contact_field'){
                        $field_id=$db->field("SELECT field_id FROM contact_fields WHERE label='".addslashes($key_data->f('label_name'))."' ");
                        if($field_id){
                            $is_drop=$db->field("SELECT is_dd FROM contact_fields WHERE field_id='".$field_id."' ");
                            if($is_drop){
                                $val=$db->field("SELECT contact_extrafield_dd.name FROM contact_field
                                            INNER JOIN contact_extrafield_dd ON contact_field.value=contact_extrafield_dd.id
                                            WHERE contact_field.customer_id='".$data['contact_id']."' AND contact_field.field_id='".$field_id."' ");
                                if($val){
                                    $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                                }                              
                            }else{
                                $val=$db->field("SELECT value FROM contact_field WHERE contact_field.customer_id='".$data['contact_id']."' AND contact_field.field_id='".$field_id."' ");                  
                                if($val){
                                    $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                                }
                            }
                        }                
                    }else{
                        if($key_data->f('is_custom_dd')){
                            if(($key_data->f('field_name')=='department' || $key_data->f('field_name')=='position') && $data['customer_id']){
                                $val=$db->field("SELECT ".$key_data->f('tabel_name').".name FROM customer_contactsIds
                                            INNER JOIN ".$key_data->f('tabel_name')." ON customer_contactsIds.".$key_data->f('field_name')."=".$key_data->f('tabel_name').".id
                                            WHERE customer_contactsIds.contact_id='".$data['contact_id']."' AND customer_contactsIds.customer_id='".$data['customer_id']."'  ");
                            }else{
                                $val=$db->field("SELECT ".$key_data->f('tabel_name').".name FROM customer_contacts
                                            INNER JOIN ".$key_data->f('tabel_name')." ON customer_contacts.".$key_data->f('field_name')."=".$key_data->f('tabel_name').".id
                                            WHERE customer_contacts.contact_id='".$data['contact_id']."'  ");
                            }   
                            if($val){
                                $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                            }
                        }else{
                            if(($key_data->f('field_name')=='e_title' || $key_data->f('field_name')=='fax') && $data['customer_id']){
                                 $val=$db->field("SELECT ".$key_data->f('field_name')." FROM customer_contactsIds WHERE contact_id='".$data['contact_id']."' AND customer_id='".$data['customer_id']."' ");
                            }else{
                                $val=$db->field("SELECT ".$key_data->f('field_name')." FROM customer_contacts WHERE contact_id='".$data['contact_id']."'");
                            }                       
                            if($val){
                                $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                            }
                        }
                    }                                                                                           
                }
            }      
            $organization_user=json_encode($organization_user);
            $headers=array('Content-Type: application/json');
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
            curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $organization_user);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $zen_api.'/api/v2/users.json'); 

            $put = curl_exec($ch);
            $info = curl_getinfo($ch);

            if($info['http_code']>300 || $info['http_code']==0){
                //
            }else{
                $data_d=json_decode($put);      
                $db->query("UPDATE customer_contacts SET zen_contact_id='".$data_d->user->id."' WHERE contact_id='".$data['contact_id']."' ");
                $db->query("INSERT INTO zen_contacts SET
                        contact_id='".$data_d->user->id."',
                        customer_id='".$zen_org_id."',
                        email='".$data['email']."',                          
                        first_name='".addslashes($data['firstname'])."',
                        last_name='".addslashes($data['lastname'])."',
                        our='1'");
            }
        }else if($data['op']=='update'){
            $c_name=$db->field("SELECT customers.name FROM customer_contactsIds
                INNER JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
                WHERE customer_contactsIds.contact_id='".$data['contact_id']."' AND `primary`='1' ");
            $organization_user=array(
                'user'=>array(
                    'name'          => trim($data['firstname']).' '.trim($data['lastname']),
                    'email'         => $data['email'],
                    'verified'      => true,
                    'role'          => 'end-user',
                )
            );
            if($c_name){
                $organization_user['user']['organization']=array('name'=>$c_name);
            }
            if($data['customer_id']){
                $comp_phone=$db->field("SELECT comp_phone FROM customers WHERE customer_id='".$data['customer_id']."' ");
                if($comp_phone){
                    $organization_user['user']['shared_phone_number']=$comp_phone;
                }
                $contact_data=$db->query("SELECT customer_contacts.cell,customer_contactsIds.phone FROM customer_contactsIds
                    INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
                    WHERE customer_contactsIds.contact_id='".$data['contact_id']."' AND customer_contactsIds.customer_id='".$data['customer_id']."' ");
                if($contact_data->f('phone')){
                    $organization_user['user']['phone']=$contact_data->f('phone');
                }else if($contact_data->f('cell')){
                    $organization_user['user']['phone']=$contact_data->f('cell');
                }
            }else{
                $contact_mobile=$db->field("SELECT cell FROM customer_contacts WHERE contact_id='".$data['contact_id']."' ");
                if($contact_mobile){
                    $organization_user['user']['phone']=$contact_mobile;
                }
            }
            $l_code=$db->field("SELECT lang.code FROM customer_contacts
                INNER JOIN lang ON customer_contacts.language=lang.lang_id 
                WHERE customer_contacts.contact_id='".$data['contact_id']."' ");
            if($l_code){
                switch($l_code){
                    case 'nl':
                        $lang_code="nl";
                        break;
                    case 'en':
                        $lang_code="en-US";
                        break;
                    case 'fr':  
                        $lang_code="fr";
                        break;
                    case 'de':
                        $lang_code="de";
                        break;
                }
                $organization_user['user']['locale']=$lang_code;
            }
            $custom_fields=$db->query("SELECT * FROM zen_fields WHERE `master`='contact' ")->getAll();
            if(!empty($custom_fields)){
                foreach($custom_fields as $key=>$value){
                    $key_data=$db->query("SELECT label_name,field_name,tabel_name,is_custom_dd FROM import_labels WHERE import_label_id='".$value['our_field']."' ");
                    if($key_data->f('tabel_name')=='contact_field'){
                        $field_id=$db->field("SELECT field_id FROM contact_fields WHERE label='".addslashes($key_data->f('label_name'))."' ");
                        if($field_id){
                            $is_drop=$db->field("SELECT is_dd FROM contact_fields WHERE field_id='".$field_id."' ");
                            if($is_drop){
                                $val=$db->field("SELECT contact_extrafield_dd.name FROM contact_field
                                            INNER JOIN contact_extrafield_dd ON contact_field.value=contact_extrafield_dd.id
                                            WHERE contact_field.customer_id='".$data['contact_id']."' AND contact_field.field_id='".$field_id."' ");
                                if($val){
                                    $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                                }                              
                            }else{
                                $val=$db->field("SELECT value FROM contact_field WHERE contact_field.customer_id='".$data['contact_id']."' AND contact_field.field_id='".$field_id."' ");                  
                                if($val){
                                    $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                                }
                            }
                        }                
                    }else{
                        if($key_data->f('is_custom_dd')){
                            if(($key_data->f('field_name')=='department' || $key_data->f('field_name')=='position') && $data['customer_id']){
                                $val=$db->field("SELECT ".$key_data->f('tabel_name').".name FROM customer_contactsIds
                                            INNER JOIN ".$key_data->f('tabel_name')." ON customer_contactsIds.".$key_data->f('field_name')."=".$key_data->f('tabel_name').".id
                                            WHERE customer_contactsIds.contact_id='".$data['contact_id']."' AND customer_contactsIds.customer_id='".$data['customer_id']."'  ");
                            }else{
                                $val=$db->field("SELECT ".$key_data->f('tabel_name').".name FROM customer_contacts
                                            INNER JOIN ".$key_data->f('tabel_name')." ON customer_contacts.".$key_data->f('field_name')."=".$key_data->f('tabel_name').".id
                                            WHERE customer_contacts.contact_id='".$data['contact_id']."'  ");
                            }   
                            if($val){
                                $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                            }
                        }else{
                            if(($key_data->f('field_name')=='e_title' || $key_data->f('field_name')=='fax') && $data['customer_id']){
                                 $val=$db->field("SELECT ".$key_data->f('field_name')." FROM customer_contactsIds WHERE contact_id='".$data['contact_id']."' AND customer_id='".$data['customer_id']."' ");
                            }else{
                                $val=$db->field("SELECT ".$key_data->f('field_name')." FROM customer_contacts WHERE contact_id='".$data['contact_id']."'");
                            }                       
                            if($val){
                                $organization_user['user']['user_fields'][$value['zen_field']]=stripslashes($val);
                            }
                        }
                    }                                                                                           
                }
            }  
            $organization_user=json_encode($organization_user);
            $headers=array('Content-Type: application/json');
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
            curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $organization_user);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $zen_api.'/api/v2/users/'.$zen_contact_id.'.json');

            $put = curl_exec($ch);
            $info = curl_getinfo($ch);

            if($info['http_code']>300 || $info['http_code']==0){
                //
            }else{   
                $db->query("UPDATE zen_contacts SET
                                customer_id='".$zen_org_id."',
                                email='".$data['email']."',                          
                                first_name='".addslashes($data['firstname'])."',
                                last_name='".addslashes($data['lastname'])."' 
                                WHERE contact_id='".$zen_contact_id."'");
                $memberships=$db->query("SELECT * FROM customer_contactsIds WHERE contact_id='".$data['contact_id']."' ");
                if($memberships->records_count()>1){
                    $orgs_m=array();
                    $orgs=$db->query("SELECT customers.zen_organization_id FROM customer_contactsIds 
                            LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
                            WHERE customer_contactsIds.contact_id='".$data['contact_id']."' ")->getAll();
                    foreach($orgs as $key=>$value){
                        if($value['zen_organization_id']){
                            array_push($orgs_m, array('user_id'=>(int)$zen_contact_id,'organization_id'=>(int)$value['zen_organization_id']));               
                        }
                    }

                    $organization_user=array(
                        'organization_memberships' => $orgs_m
                    );
                    $organization_user=json_encode($organization_user);
                    $headers=array('Content-Type: application/json');
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                    curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $organization_user);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_URL, $zen_api.'/api/v2/organization_memberships/create_many.json'); 

                    $put = curl_exec($ch);
                    $info = curl_getinfo($ch);

                }
            }
        }
    }
}

//Set email language as account / contact language
//Use logged in user as default language if are not set
function get_email_language($emailMessageData){ 
    
    $db = new sqldb();

    //$email_language = DEFAULT_LANG_ID;
    $email_language = $db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
    if(empty($email_language)){
        $email_language = DEFAULT_LANG_ID;
    }

    if($emailMessageData['param'] == 'add'){
        if($emailMessageData['buyer_id']){
        $buyerLang = $db->field("SELECT language FROM customers WHERE customer_id='".$emailMessageData['buyer_id']."' ");
            if($buyerLang !='0'){
                $email_language = $buyerLang;
            }
        }

        if($emailMessageData['contact_id']){
            $contactLang = $db->field("SELECT language FROM customer_contacts WHERE contact_id='".$emailMessageData['contact_id']."' ");
            if($contactLang !='0'){
                $email_language = $contactLang;
            }
        }

        if($email_language == '0'){
           $default_email_language = $db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
           $email_language = $default_email_language;
        }    
    } else if($emailMessageData['param'] == 'edit' || $emailMessageData['param'] == 'update_customer_data'){

        if($emailMessageData['email_language']){

            //Get the document language
            $email_language = $emailMessageData['email_language'];

            if(($emailMessageData['item_id'] && $emailMessageData['item_id'] != 'tmp') && $emailMessageData['buyer_id']){
                $currentBuyerId = $db->field("SELECT ".$emailMessageData['table_buyer_label']." FROM ".$emailMessageData['table']." WHERE ".$emailMessageData['table_label']." = ".$emailMessageData['item_id']);

                $buyerLang = $db->field("SELECT language FROM customers WHERE customer_id='".$emailMessageData['buyer_id']."' ");

                if(($emailMessageData['buyer_id'] != $currentBuyerId) && $buyerLang !='0'){
                    $email_language = $buyerLang;
                }
            } else if($emailMessageData['buyer_id']){
                $buyerLang = $db->field("SELECT language FROM customers WHERE customer_id='".$emailMessageData['buyer_id']."' ");

                if($buyerLang !='0'){
                    $email_language = $buyerLang;
                }
            }

            // if($emailMessageData['contact_id']){
            //     $currentContactId = $db->field("SELECT ".$emailMessageData['table_contact_label']." FROM ".$emailMessageData['table']." WHERE ".$emailMessageData['table_label']." = ".$emailMessageData['item_id']);

            //     $contactLang = $db->field("SELECT language FROM customer_contacts WHERE contact_id='".$emailMessageData['contact_id']."' ");

            //     if(($emailMessageData['contact_id'] != $currentContactId) && $contactLang !='0'){
            //         $email_language = $contactLang;
            //     }
            // }    
            
            if($email_language == '0'){
                $default_email_language = $db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
                $email_language = $default_email_language;
            }
                
        } else {
            if($emailMessageData['item_id']=='tmp' || !$emailMessageData['item_id']){
                $buyer_lang = $db->field("SELECT language FROM customers WHERE customer_id = '".$emailMessageData['buyer_id']."' ");
                if($buyer_lang){
                    $email_language=$buyer_lang;
                }
            }else{
                $itemLang = $db->field("SELECT email_language 
                            FROM ".$emailMessageData['table']
                            ." WHERE ".$emailMessageData['table_label']
                            ." = ".$emailMessageData['item_id']);
                if($itemLang){
                    $email_language = $itemLang;
                }
            }
        }
    }

    return $email_language;
}

function get_email_message_default_notes($data){
    $db = new sqldb();

    $terms = '';
    if($data['email_language'] != 1){      
        $terms = '_'.$data['email_language'];
    }

    if($data['default_name_note'] != 'contract_note' && $data['default_name_note'] != 'p_order_note'){
        $note = $db->field("SELECT value FROM default_data
                                               WHERE default_name = '".$data['default_name_note']."'
                                               AND type = '".$data['default_type_note'].$terms."'  ");    
    } else {
        $note = $db->field("SELECT value FROM default_data
                                               WHERE default_main_id='0'
                                               AND type = '".$data['default_type_note'].$terms."'  ");
    }
    
    $free_text_content = $db->field("SELECT value FROM default_data
                                               WHERE default_name = '".$data['default_name_free_text_content']."'
                                               AND type = '".$data['default_type_free_text_content'].$terms."'  ");
    
    return array('notes' => $note,'free_text_content' => $free_text_content);

}    


function get_default_page(){ 
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $data_name = $db_users->field("SELECT database_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

    $db_user = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $data_name,
    );
    $db=new sqldb($db_user);
    $res = array();

    $is_new_subscription = $db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");
    $WIZZARD = $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");
   
    $res['must_pay']=false;
    $showGdpr=false;
    $user_info = $db_users->query("SELECT user_info.*, users.active,users.two_factor,users.gdpr_status FROM user_info JOIN users ON user_info.user_id = users.user_id WHERE user_info.user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    if(time()>$user_info->f('end_date') && ($user_info->f('is_trial') =='0' || $user_info->f('active') =='2') ){

        $res['must_pay']=true;
        if(is_null($is_new_subscription)){
            //$res['pag']='subscription';
            $res['pag']='subscription_new';
        }else{
            $res['pag']='subscription_new';
        }
        if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
            $showGdpr=true;
        }
    }
    //$group_id=$db_users->field("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
    $group_id=$db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    //$credential=$db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
    $credential=$db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    $credentials=explode(';', $credential);
    if($group_id==2 || (in_array('1', $credentials) === false)){
        $res['default_pag']='calendar';
        if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
            $showGdpr=true;
        }
    }else{
        $is_admin_company=aktiUser::get('is_admin_company');
        $select_company=false;
        if($is_admin_company=='company' && $_SESSION['group']=='user'){
            //
        }else if($_SESSION['group']=='user'){
            $select_company=true;
        }        
        $res['default_pag']=$select_company ? 'customers' : 'dash';
        if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
            $showGdpr=true;
        }
    }
    if($WIZZARD=='0'){
        $res['default_pag']='board';
        $showGdpr=false;
    }
    //$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
    $easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`= :constant_name ",['constant_name'=>'EASYINVOICE']);
    if($easyinvoice=='1'){
        $res['default_pag']='invoices';
        if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
            $showGdpr=true;
        }
    }
    
    $old_default_pag=$res['default_pag'];
    $res['two_done']=true;
    if($user_info->f('two_factor')){
        $res['two_done']=false;
        if($_SESSION['logedAsAdmin']){
            $res['two_done']=true;
            $_SESSION['two_done']=true;
        }else if($_COOKIE["trust".$_SESSION['u_id']] && $_COOKIE["trust".$_SESSION['u_id']]!='canci'){
            //$user_trust=$db_users->field("SELECT id FROM user_trust WHERE user_id='".$_SESSION['u_id']."' AND trust_code='".$_COOKIE["trust".$_SESSION['u_id']]."' ");
            $user_trust=$db_users->field("SELECT id FROM user_trust WHERE user_id= :user_id AND trust_code= :trust_code ",['user_id'=>$_SESSION['u_id'],'trust_code'=>$_COOKIE["trust".$_SESSION['u_id']]]);
            if($user_trust){
                $res['two_done']=true;
                $_SESSION['two_done']=true;
            }
        }else if($_SESSION['two_done']===true){
            $res['two_done']=true;
        }else if($_SESSION['two_done']===false){
            $res['default_pag']='login_two';
            $showGdpr=false;
        }
    }
    $res['showGdpr']=$showGdpr;

    return $res;
}

function build_site_field_dropdown($val){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT * FROM site_fields_drops WHERE field_id='".$val."' ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    if($_SESSION['access_level'] == 1){
        array_push($res,array('id'=>'0','name'=>''));
    }
    return $res;
}
function get_categorisation_segment(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT id, name FROM tblquote_segment ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}
function get_categorisation_type(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT id, name FROM tblquote_type ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}
function get_categorisation_source(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT id, name FROM tblquote_source ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}

function get_price_categories(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT category_id, name FROM pim_article_price_category  ORDER BY name ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('category_id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}
function getCompanyInfo($in){
    $db=new sqldb();
    if($in['identity_id']){
        $q = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$in['identity_id']."' ");
        if($q->next()){
            $array = array(         
                    'name'      => $q->f('identity_name'),
                    'address'      => nl2br($q->f('company_address')),
                    'zip'           => $q->f('company_zip'),
                    'city'      => $q->f('city_name'),
                    'country'       => get_country_name($q->f('country_id')),
                    'phone'     => $q->f('company_phone'),
                    'fax'           => $q->f('company_fax'),
                    'email'     => $q->f('company_email'),
                    'url'           => $q->f('company_url'),
                    'logo'      => $q->f('company_logo'),   
                    'identity_id'   => $q->f('identity_id'),        
            );
        }else{
            $array = array(         
                    'name'      => ACCOUNT_COMPANY,
                    'address'       => nl2br(ACCOUNT_DELIVERY_ADDRESS),
                    'zip'           => ACCOUNT_DELIVERY_ZIP,
                    'city'      => ACCOUNT_DELIVERY_CITY,
                    'country'       => get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
                    'phone'     => ACCOUNT_PHONE,
                    'fax'           => ACCOUNT_FAX,
                    'email'     => ACCOUNT_EMAIL,
                    'url'           => ACCOUNT_URL,
                    'logo'      => defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',           
            );
        }
    }else{
        $array = array(         
                'name'      => ACCOUNT_COMPANY,
                'address'       => nl2br(ACCOUNT_DELIVERY_ADDRESS),
                'zip'           => ACCOUNT_DELIVERY_ZIP,
                'city'      => ACCOUNT_DELIVERY_CITY,
                'country'       => get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
                'phone'     => ACCOUNT_PHONE,
                'fax'           => ACCOUNT_FAX,
                'email'     => ACCOUNT_EMAIL,
                'url'           => ACCOUNT_URL,
                'logo'      => defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',           
        );
    }           
    return $array;
}
function list_content($arrayname, $tab="&nbsp&nbsp&nbsp&nbsp", $indent=0) { // recursively displays contents of the array and sub-arrays:
    // This function (c) Peter Kionga-Kamau (http://www.pmkmedia.com)
    // Free for unrestricted use, except sale - do not resell.
    // use: echo LIST_CONTENTS(array $arrayname, string $tab, int $indent);
    // $tab = string to use as a tab, $indent = number of tabs to indent result
    $retval = '';
    $currenttab = '';
    while (list($key, $value) = each($arrayname)) {
        for ($i = 0; $i < $indent; $i++)
        $currenttab .= $tab;
        if (is_array($value)) {
            $retval .= "$currenttab$key : Array: <BR>$currenttab{<BR>";
            $retval .= list_content($value, $tab, $indent + 1) . "$currenttab}<BR>";
        }
        else
        $retval .= "$currenttab$key => $value<BR>";
        $currenttab = NULL;
    }
    return $retval;
}
function build_instal_field_dropdown($val){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT * FROM installation_fields_drops WHERE field_id='".$val."' ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    if($_SESSION['access_level'] == 1){
        array_push($res,array('id'=>'0','name'=>''));
    }
    return $res;
}
function error_post($data){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb($database_users);

    if($_SESSION['main_u_id']!='0'){
       $snap_id=$_SESSION['main_u_id'];
    }else{
      $snap_id=$_SESSION['u_id'];
    }

    $message = $data['error_message'];

    error_log("Customer Id:".$snap_id." ".date('m/d/y H:i')."-".$message."\n","3",INSTALLPATH."error_post.txt");

    $db->query("INSERT INTO postgreen_logs SET user_id ='".$data['user_id']."', 
                    database_name = '".$data['database_name']."',
                    item_id = '".$data['item_id']."',
                    module = '".$data['module']."',
                    date = '".$data['date']."',
                    action = '".$data['action']."',
                    content = '".$data['content']."',
                    login_user = '".$data['login_user']."',
                    error_message = '".$data['error_message']."'"
                );
}

function invoice_payment_method_dd($selected = ''){
    //Yuki Payment methods
    $array = array(
        array('id' => "0" , 'name' => gm("Select")),       
        array('id' => "4" , 'name' => gm("Cash")),       
        array('id' => "14" , 'name' => gm('Credit invoice')),
        array('id' => "6" , 'name' => gm("CreditCard")),
        array('id' => "5" , 'name' => gm("DebitCard")),
        array('id' => "3" , 'name' => gm("DirectCollection")),
        array('id' => "2" , 'name' => gm("ElectronicTransfer")),
        array('id' => "10" , 'name' => gm("iDeal")),
        array('id' => "11" , 'name' => gm("Online")),
        array('id' => "13" , 'name' => gm("iDeal")),      
        array('id' => "8" , 'name' => gm("ReceivedCash")),
        array('id' => "7" , 'name' => gm("ReceivedElectronically")),
        array('id' => "9" , 'name' => gm("ToSettle")),  
        array('id' => "1" , 'name' => gm("Unspecified")),     
    );
    return $array;    
}
function get_invoice_payment_method($array, $selected ){
    //$array = invoice_payment_method_dd();
    if($selected){
        $key = array_search($selected, array_column($array,'id'));
        if($key || $key==0){
            $method = $array[$key]['name'];
        }else{
            $method = gm("Unspecified");
        }
        
    }else{
        $method = gm("Unspecified");
    }
    
    return $method;    
}

function invoice_payment_method_dd_en($selected = ''){
    //Yuki Payment methods
    $array = array(
        array('id' => "0" , 'name' => "Unspecified"),
        array('id' => "1" , 'name' => "Unspecified"),
        array('id' => "2" , 'name' => "ElectronicTransfer"),
        array('id' => "3" , 'name' => "Directcollection"),
        array('id' => "4" , 'name' => "Cash"),
        array('id' => "5" , 'name' => "DebitCard"),
        array('id' => "6" , 'name' => "CreditCard"),
        array('id' => "7" , 'name' => "ElectronicTransfer"),
        array('id' => "8" , 'name' => "Cash"),
        array('id' => "9" , 'name' => "Unspecified"),
        array('id' => "10" , 'name' => "iDeal"),
        array('id' => "11" , 'name' => "Online"),
        array('id' => "13" , 'name' => "iDeal"),
    );
    return $array;    
}
function get_invoice_payment_method_en($array, $selected ){
    //$array = invoice_payment_method_dd();
    if($selected){
        $key = array_search($selected, array_column($array, 'id'));
        if($key){
            $method = $array[$key]['name'];
        }else{
            $method = "Unspecified";
        }
        
    }else{
        $method = "Unspecified";
    }
    
    return $method;    
}


function get_payment_method($selected ){
    $array = array(
        array('id' => "0" , 'name' => ''),
        array('id' => "1" , 'name' => gm("Direct Transfer")),
        array('id' => "2" , 'name' => gm("Stripe")),
        array('id' => "3" , 'name' => gm("SEPA")),
    ); 

    if($selected){
        $key = array_search($selected, array_column($array, 'id'));
        if($key){
            $method = $array[$key]['name'];
        }else{
            $method = gm("Unspecified");
        }
        
    }else{
        $method = gm("Unspecified");
    }
    
    return $method;    
}

function pc_next_permutation($p, $size) {
    // slide down the array looking for where we're smaller than the next guy
    for ($i = $size - 1; $p[$i] >= $p[$i+1]; --$i) { }

    // if this doesn't occur, we've finished our permutations
    // the array is reversed: (1, 2, 3, 4) => (4, 3, 2, 1)
    if ($i == -1) { return false; }

    // slide down the array looking for a bigger number than what we found before
    for ($j = $size; $p[$j] <= $p[$i]; --$j) { }

    // swap them
    $tmp = $p[$i]; $p[$i] = $p[$j]; $p[$j] = $tmp;

    // now reverse the elements in between by swapping the ends
    for (++$i, $j = $size; $i < $j; ++$i, --$j) {
         $tmp = $p[$i]; $p[$i] = $p[$j]; $p[$j] = $tmp;
    }

    return $p;
}

function get_location_dd(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT * FROM `locations` ORDER BY name ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}

function get_depot_dd($loc_id){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT dispatch_stock_address.* FROM dispatch_stock_address 
                    INNER JOIN locations ON locations.id = dispatch_stock_address.location_id
                    WHERE locations.id = '".$loc_id."' 
        ORDER BY naming ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('address_id'),
                                'name'=> stripslashes($inf->f('naming')).' '.stripslashes($inf->f('address')).'  
                                '.stripslashes($inf->f('zip')).'  '.stripslashes($inf->f('city')).'  
                                '.stripslashes(get_country_name($inf->f('country_id'))) 
                               )
                    );
    }
    return $res;
}

function get_depot_dd_no_location(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT dispatch_stock_address.* FROM dispatch_stock_address ORDER BY naming ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('address_id'),
                                'name'=> stripslashes($inf->f('naming')) 
                               )
                    );
    }
    return $res;
}

function locationTracking($data){
    $db=new sqldb();
    if($data['action']=='delete'){
        $db->query("DELETE FROM locations_tracking WHERE module='".$data['module']."' AND document_id='".$data['document_id']."' ");
    }else{
        if(!$data['location_id'] || $data['location_id']==''){
            return true;
        } 
        $location_attached=$db->field("SELECT id FROM locations_tracking WHERE module='".$data['module']."' AND document_id='".$data['document_id']."' ");
        if($location_attached){
            $db->query("UPDATE locations_tracking SET location_id='".$data['location_id']."' WHERE id='".$location_attached."' ");
        }else{
            $db->query("INSERT INTO locations_tracking SET location_id='".$data['location_id']."',module='".$data['module']."',document_id='".$data['document_id']."'");
        }
    } 
}
function generate_financial_code($data){
    $db=new sqldb();
    $cost_center=$db->field("SELECT financial_code FROM locations WHERE id='".$data['location_id']."' ");
    if(!$cost_center){
        return "";
    }
    $cost_center.="1000/";
    if($data['segment_id']){
        $segment_id=$db->field("SELECT `code` FROM tblquote_segment WHERE id='".$data['segment_id']."' ");
        if($segment_id){
            $cost_center.=$segment_id;
        }else{
            $cost_center.="0";
        }
    }else{
        $cost_center.="0";
    }
    if($data['source_id']){
        $source_id=$db->field("SELECT `code` FROM tblquote_source WHERE id='".$data['source_id']."' ");
        if($source_id){
            $cost_center.=$source_id;
        }else{
            $cost_center.="0";
        }
    }else{
        $cost_center.="0";
    }
    if($data['type_id']){
        $type_id=$db->field("SELECT `code` FROM tblquote_type WHERE id='".$data['type_id']."' ");
        if($type_id){
            $cost_center.=$type_id;
        }else{
            $cost_center.="0";
        }
    }else{
        $cost_center.="0";
    }
    return $cost_center;
}
function get_stock_labels_dd(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT * FROM `pim_article_labels` ORDER BY name ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}

function build_stock_edit_reason_dd($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb();
    $db_users = new sqldb($database_users);

    $user_id = $_SESSION['u_id'];
    $group_id = $db_users->field("SELECT group_id FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ");

    $edit_reasons = $db->query("SELECT id,name FROM pim_stock_edit_reasons ORDER BY sort_order ASC ")->getAll();

    //Manage Edit Stock Reason only for admins
    if($group_id == 1){
        array_push($edit_reasons, array('id' => "0" , 'name' =>  ''));
    }    
    return $edit_reasons;
}


function build_article_quantity_edit_reason_dd($selected = false){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb();
    $db_users = new sqldb($database_users);

    $user_id = $_SESSION['u_id'];
    $group_id = $db_users->field("SELECT group_id FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ");

    $edit_reasons = $db->query("SELECT id,name FROM servicing_article_quantity_edit_reasons ORDER BY sort_order ASC ")->getAll();

    //Manage Edit Stock Reason only for admins
    // if($group_id == 1){
        array_push($edit_reasons, array('id' => "0" , 'name' =>  ''));
    // }    
    return $edit_reasons;
}

function check_customer($customer_id){
    $db=new sqldb();

    $is_facq = $db->field("SELECT facq FROM customers WHERE customer_id='".$customer_id."' ");
    if($is_facq){
        return false;
    }
   

    //check if the customer is linked to any document

    $quotes = $db->query("SELECT id FROM tblquote WHERE buyer_id = '".$customer_id."'" );
    if($quotes->move_next()){
        return false;
    }

    $invoices = $db->query("SELECT id FROM tblinvoice WHERE buyer_id = '".$customer_id."'" );
    if($invoices->move_next()){
        return false;
    }

    $invoices_inc = $db->query("SELECT invoice_id FROM tblinvoice_incomming WHERE supplier_id = '".$customer_id."'" );
    if($invoices_inc->move_next()){
        return false;
    }

    $invoices_rec = $db->query("SELECT recurring_invoice_id FROM recurring_invoice WHERE buyer_id = '".$customer_id."'" );
    if($invoices_rec->move_next()){
        return false;
    }

    $orders = $db->query("SELECT order_id FROM pim_orders WHERE customer_id = '".$customer_id."'" );
    if($orders->move_next()){
        return false;
    }

    $p_orders = $db->query("SELECT p_order_id FROM pim_p_orders WHERE customer_id = '".$customer_id."' OR second_customer_id = '".$customer_id."'" );
    if($p_orders->move_next()){
        return false;
    }

    $dispatch_stock = $db->query("SELECT dispatch_stock_id FROM dispatch_stock WHERE customer_id = '".$customer_id."'" );
    if($dispatch_stock->move_next()){
        return false;
    }

    $dispatch_stock_addr = $db->query("SELECT address_id FROM dispatch_stock_address WHERE customer_id = '".$customer_id."'" );
    if($dispatch_stock_addr->move_next()){
        return false;
    }


    $contracts = $db->query("SELECT contract_id FROM contracts WHERE customer_id = '".$customer_id."'" );
    if($contracts->move_next()){
        return false;
    }

    $contracts_inv = $db->query("SELECT contract_invoice_id FROM contract_invoice WHERE buyer_id = '".$customer_id."'" );
    if($contracts_inv->move_next()){
        return false;
    }

    $deals = $db->query("SELECT opportunity_id FROM tblopportunity WHERE buyer_id = '".$customer_id."'" );
    if($deals->move_next()){
        return false;
    }

   $projects = $db->query("SELECT project_id FROM projects WHERE customer_id = '".$customer_id."'" );
    if($projects->move_next()){
        return false;
    }

    $int = $db->query("SELECT service_id FROM servicing_support WHERE customer_id = '".$customer_id."'" );
    if($int->move_next()){
        return false;
    }

    $inst = $db->query("SELECT id FROM installations WHERE customer_id = '".$customer_id."'" );
    if($inst->move_next()){
        return false;
    }

    $stock_disp = $db->query("SELECT stock_disp_id FROM pim_stock_disp WHERE from_customer_id = '".$customer_id."' OR to_customer_id = '".$customer_id."'");
    if($stock_disp->move_next()){
        return false;
    }

    $mandate = $db->query("SELECT mandate_id FROM mandate WHERE buyer_id = '".$customer_id."'" );
    if($mandate->move_next()){
        return false;
    }

    $tracking = $db->query("SELECT trace_id FROM tracking WHERE origin_buyer_id = '".$customer_id."' OR target_buyer_id = '".$customer_id."'" );
    if($tracking->move_next()){
        return false;
    }

    return true;


}

function customer_credit_limit($customer_id){
    $db=new sqldb();
    $credit_limit=$db->field("SELECT SUM(amount_vat) FROM tblinvoice WHERE buyer_id='".$customer_id."' AND (type='0' OR type='3') AND f_archived='0' and not_paid = '0' AND status = '0' and sent = '1' and due_date < ".time()." ");
    if(!$credit_limit){
        $credit_limit=0;
    }
    return $credit_limit;
}

function get_managers_dd($in){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb();
    $db_users = new sqldb($database_users);

    $q = strtolower($in["term"]);

    $filter = '';

    if($q){
        $filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
    }
    if($in['manager_id']){
        $filter .=" AND users.user_id='".$in['manager_id']."' ";
    }

    $data=$db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY last_name ");
    while($data->move_next()){
      $users[$data->f('last_name').' '.$data->f('first_name')]=$data->f('user_id');
    }

    $result = array('list'=>array());
    if($users){
        foreach ($users as $key=>$value) {
            $result_line=array(
                'id'        => $value,
                'value'     => htmlspecialchars_decode(stripslashes($key))
            );
            array_push($result['list'], $result_line);
        }
    }
    return $result;
}

function default_selected_columns_dd($in){
    switch($in['list']){
        case 'accounts':
            $array=array('type' => true, 'name' => true, 'city' => true, 'country' => false, 'sales' => false, 'contacts' => false, 'comp_phone' => true, 'c_email' => true, 'our_reference' => false, 'language' => false, 'last_update' => false, 'invoice_email' => false, 'vat_nr' => true, 'identity_name' => false, 'company_type' => false, 'legal_type' => false, 'vat_regime' => true);
            break;
        case 'articles':
            $array=array('main_code' => false, 'code' => true, 'name' => true, 'article_category' => true, 'customer_name' => true, 'base_price' => true, 'total_price' => true, 'stock' => false, 'ant_stock' => false, 'name_lang_en' => false, 'name_lang_fr' => false, 'name_lang_du' => false, 'stock_pack' => false, 'ant_stock_pack' => false, 'brand' => false, 'name_lang2_en' => false, 'name_lang2_fr' => false, 'name_lang2_du' => false);
            break;
        case 'invoices':
            $array=array("s_number" => true, "doc_type_title" => true, "created" => true, "due_date" => true, "buyer_name" => true, "mandate_reference" => false, "our_reference" => true, "amount" => true, "amount_vat" => true, 'balance' => false, 'status' => true, 'days_expired' => false, 'identity_name' => false, 'vat_regime' => true, 'acc_manager_name' => false, 'subject' => false, "your_ref" => false);
            break;
        case 'quotes':
            $array=array("serial_number" => true, "created" => true, "buyer_name" => true, "subject" => true, "amount" => true, 'status_customer' => true, "own_reference" => false, "author" => false, "acc_manager_name" => false, "amount_vat" => true, 'identity_name' => false, 'vat_regime' => false, 'deal_stage' => false);
            break;
        case 'recinvoices':
            $array=array("buyer_name" => true, "next_date" => true, "title" => true, "contract_reference" => false, "mandate_reference" => false, "amount" => true, "start_date" => true, "end_date" => true, 'invoice_email' => true, 'payment_method' => false, 'frequency' => true, 'your_ref' => false, 'our_ref' => false, 'mrr' => true, "last_upd" => false, 'vat_regime' => true);
            break;
        case 'orders':
            $array=array("serial_number"=>true,"created"=>true,"delivery_date"=>true,"our_ref"=>true,"your_ref"=>true,"buyer_name"=>true,"amount"=>true,"status2"=>true,'identity_name'=>false,'author_name'=>false,'acc_manager_name'=>false,'vat_regime'=>false,'subject'=>false,'delivery_country'=>false,'deal_stage'=>false);
            break;
        case 'purchase_orders':
            $array=array("serial_number"=>true,"created"=>true,"delivery_date"=>true,"our_ref"=>true,"your_ref"=>false,"buyer_name"=>true,"amount"=>true,"status2"=>true,'associated_customer'=>false, 'business_unit' => false);
            break;
        case 'interventions':
            $array=array("serial_number"=>true,"planeddate"=>true,"customer_name"=>true,"manager"=>true,"subject"=>true,"type_status"=>true,"account_phone"=>true,"contact_phone"=>true,'city'=>false,'invoicing_type'=>false,'color_code'=>false, 'deal_stage'=>false);
            break;
        case 'stock_transactions':
            $array=array("date"=>true,"selling_reference"=>true,"article_name"=>true,"item_code"=>true,"movement_type"=>true,"delivery_date"=>true,"created_by"=>true,"stock"=>true,"new_stock"=>true,"buyer_name"=>false,"line_price"=>false);
            break;
        case 'reminders':
            $array=array("buyer_name"=>true,"acc_manager_name"=>true,"date_sent"=>true,"amount"=>true);
            break;
        case 'purchase_invoices':
            $array=array("b_number"=>true,'s_number'=>true,'created'=>true,'due_date'=>true,'buyer_name'=>true,'expense_category'=>true,'amount'=>true,'amount_vat'=>true,'balance'=>false,'status'=>true,'ogm'=>false,'reference'=>false,'last_update'=>false, 'doc_type'=>false);
            break;
    }
    return $array;
}

function default_columns_dd($in){
    switch($in['list']){
        case 'deals':
            $array=array("serial_number"=>gm('Deal Number'),"created"=>gm('Date'),"buyer_name"=>gm('Account Name'),"amount"=>gm('Value'),"subject"=>gm('Subject'),"author"=>gm('Account manager'),"board_name"=>gm('Deal board'),"name"=>gm('Deal stage'));
            break;
        case 'accounts':
            $array=array('type'=>gm('Icons'),'name'=>gm('Name'),'city'=>gm('City'),'country'=>gm('Country'),'sales'=>gm('Type of relationship'),'contacts'=>gm('Account Manager'),'comp_phone'=>gm('Phone'),'c_email'=>gm('Company email'),'our_reference'=>gm('Our reference'),'language'=>gm('Language'),'last_update'=>gm('Date of last update'), 'invoice_email'=>gm('Invoice email'), 'vat_nr'=>gm('VAT number'),'identity_name'=>gm('Multiple Identity'),'company_type'=>gm('Account Type'),'legal_type'=>gm('Legal Type'),'firstname'=>gm('First Name'),'vat_regime'=>gm('VAT Regime'));
            break;
        case 'contacts':
            $array=array('con_lastname'=>gm('Last Name'),'con_firstname'=>gm('First Name'),'cus_name'=>gm('Company Name'),'function'=>gm('Function'),'info'=>gm('Info'),'last_update'=>gm('Date of last update'));
            break;
        case 'articles':
            $array=array('main_code' => gm('Main Code'), 'code'=>gm('Code'),'name'=>gm('Internal Name'),'article_category'=>gm('Article Category'),'customer_name'=>gm('Preferred Supplier'),'base_price'=>gm('Base Price'),'total_price'=>gm('Price + VAT'),'stock'=>gm('Stock'),'ant_stock'=>gm('Anticipated Stock'),'name_lang_en'=>gm('Commercial Name').' '.gm('English'),'name_lang_fr'=>gm('Commercial Name').' '.gm('French'),'name_lang_du'=>gm('Commercial Name').' '.gm('Dutch'),'stock_pack'=>gm('Stock per package'),'ant_stock_pack'=>gm('Anticipated stock per package'),'brand'=>gm('Brand'),'name_lang2_en'=>gm('Commercial Name2').' '.gm('English'),'name_lang2_fr'=>gm('Commercial Name2').' '.gm('French'),'name_lang2_du'=>gm('Commercial Name2').' '.gm('Dutch'));
            break;
        case 'purchase_orders':
            $array=array("serial_number"=>gm('Purchase Order'),"created"=>gm('Date'),"delivery_date"=>gm('Expected delivery date'),"our_ref"=>gm('Our reference'),"your_ref"=>gm('Your Reference'),"buyer_name"=>gm('Supplier'),"amount"=>gm('Amount'),"status2"=>gm('Status'),'associated_customer'=>gm('Associated Customer'),'business_unit'=>gm('Business Unit'),);
            break;
        case 'orders':
            $array=array("serial_number"=>gm('Order'),"created"=>gm('Date'),"delivery_date"=>gm('Expected delivery date'),"our_ref"=>gm('Our reference'),"your_ref"=>gm('Your Reference'),"buyer_name"=>gm('Account Name'),"amount"=>gm('Amount'),"status2"=>gm('Status'),'identity_name'=>gm('Multiple Identity'),'author_name'=>gm('Author'),'acc_manager_name'=>gm('Account Manager'),'vat_regime'=>gm('VAT Regime'),'subject'=>gm('Subject'),'delivery_country'=>gm('Delivery country'),'deal_stage'=>gm('Deal stage'));
            break;
        case 'interventions':
            $array=array("serial_number"=>gm('Intervention No'),"planeddate"=>gm('Planned date'),"customer_name"=>gm('Account Name'),"manager"=>gm('Intervention manager'),"subject"=>gm('Subject'),"type_status"=>gm('Status'),"account_phone"=>gm('Account phone number'),"contact_phone"=>gm('Contact phone number'),'city'=>gm('Site address location'),'invoicing_type'=>gm('Type of invoicing'),'color_code'=>gm('Color code'), 'deal_stage'=>gm('Deal stage'));
            break;
        case 'invoices':
            $array=array("s_number"=>gm('Document number'),"doc_type_title"=>gm('Document type'),"created"=>gm('Date'),"due_date"=>gm('Due Date'),"buyer_name"=>gm('Account Name'),"mandate_reference"=>gm('Mandate Reference'),"our_reference"=>gm('Our Reference'),"amount"=>gm('Amount'),"amount_vat"=>gm('Amount + VAT'),'balance'=>gm('Balance'),'status'=>gm('Status'),'days_expired'=>gm('Days expired'),'identity_name'=>gm('Multiple Identity'),'vat_regime'=>gm('VAT Regime'),'acc_manager_name'=>gm('Account Manager'),'subject'=>gm('Subject'),"your_ref"=>gm('Your Reference'));
            break;
        case 'quotes':
            $array=array("serial_number"=>gm('Quote').' ['.gm('Version').']',"created"=>gm('Date'),"buyer_name"=>gm('Account'),"subject"=>gm('Subject'),"amount"=>gm('Amount'),'status_customer'=>gm('Status'),"own_reference"=>gm('Your Reference'), "author"=>gm('Author'),"acc_manager_name"=>gm('Account manager'),"amount_vat"=>gm('Amount + VAT'),'identity_name'=>gm('Multiple Identity'),'vat_regime'=>gm('VAT Regime'),'deal_stage'=>gm('Deal stage'));
            break;
        case 'recinvoices':
            $array=array("buyer_name"=>gm('Company'),"next_date"=>gm('Next Invoice'),"title"=>gm('Subject'),"contract_reference"=>gm('Contract reference'),"mandate_reference"=>gm('Mandate reference'),"amount"=>gm('Amount'),"start_date"=>gm('Start Date'),"end_date"=>gm('End Date'),'invoice_email'=>gm('Invoice email'),'payment_method'=>gm('Payment Method'),'frequency'=>gm('Frequency'),'your_ref'=>gm('Your reference'),'our_ref'=>gm('Our reference'),'mrr'=>'MRR', "last_upd"=>gm('Date Last Update'),'vat_regime'=>gm('VAT Regime'));
            break;
        case 'purchase_invoices':
            $array=array("b_number"=>gm('Booking number'),'s_number'=>gm('Invoice'),'created'=>gm('Date'),'due_date'=>gm('Due Date'),'buyer_name'=>gm('Company'),'expense_category'=>gm('Expense category'),'amount'=>gm('Amount'),'amount_vat'=>gm('Amount + VAT'),'balance'=>gm('Balance'),'status'=>gm('Status'),'ogm'=>gm('O.G.M.'),'reference'=>gm('Reference'),'last_update'=>gm('Date of last update'),  'doc_type'=>gm('Document type'));
            break;
        case 'reminders':
            $array=array("buyer_name"=>gm('Account'),"acc_manager_name"=>gm('Account manager'),"date_sent"=>gm("Date sent"),"amount"=>gm('Amount'));
            break;
        case 'stock_transactions':
            $array=array("date"=>gm('Log Date'),"selling_reference"=>gm('Reference'),"article_name"=>gm("Article Name"),"item_code"=>gm('Article Code'),"movement_type"=>gm('Action made'),"delivery_date"=>gm('Delivery Date'),"created_by"=>gm('Updated by'),"stock"=>gm('Previous Stock'),"new_stock"=>gm('Actual Stock'),"buyer_name"=>gm('Account'),"line_price"=>gm('Line price'));
            break;
    }
    return $array;
}
function default_columns_order_dd($in){
    switch($in['list']){
        case 'deals':
            $array=array("serial_number"=>"serial_number","created"=>"opportunity_date","buyer_name"=>"buyername","amount"=>"expected_revenue","subject"=>"subject","author"=>"","board_name"=>"","name"=>"");
            break;
        case 'accounts':
            $array=array('type'=>'','name'=>'name','city'=>'city_name','country'=>'country_name','sales'=>'c_type_name','contacts'=>'acc_manager_name','comp_phone'=>'comp_phone','c_email'=>'c_email','our_reference'=>'our_reference','language'=>'language','last_update'=>'last_update','invoice_email'=>'invoice_email','vat_nr'=>'btw_nr','identity_name'=>'','company_type'=>'','legal_type'=>'legal_type_name','firstname'=>'firstname');
            break;
        case 'contacts':
            $array=array('con_lastname'=>'lastname','con_firstname'=>'firstname','cus_name'=>'cus_name','function'=>'function','info'=>'','last_update'=>'last_update');
            break;
        case 'articles':
            $array=array('code'=>'item_code','name'=>'internal_name','article_category'=>'article_category','customer_name'=>'customer_name','base_price'=>'price','total_price'=>'total_price','stock'=>'stock','ant_stock'=>'stock_ant','name_lang_en'=>'name_lang_en','name_lang_fr'=>'name_lang_fr','name_lang_du'=>'name_lang_du','stock_pack'=>'stock_pack','ant_stock_pack'=>'ant_stock_pack','brand'=>'article_brand','name_lang2_en'=>'name_lang2_en','name_lang2_fr'=>'name_lang2_fr','name_lang2_du'=>'name_lang2_du');
            break;
        case 'purchase_orders':
            $array=array("serial_number"=>"serial_number","created"=>"t_date","delivery_date"=>"del_date","our_ref"=>"our_ref","your_ref"=>"your_ref","buyer_name"=>"customer_name","amount"=>"amount","status2"=>"","associated_customer"=>"");
            break;
        case 'orders':
            $array=array("serial_number"=>"serial_number","created"=>"t_date","delivery_date"=>"del_date","our_ref"=>"our_ref","your_ref"=>"your_ref","buyer_name"=>"customer_name","amount"=>"amount","status2"=>"",'identity_name'=>'','author_name'=>'author_name','acc_manager_name'=>'acc_manager_name','subject'=>'subject', 'delivery_country'=>'','deal_stage'=>'');
            break;
        case 'interventions':
            $array=array("serial_number"=>'serial_number',"planeddate"=>'planeddate',"customer_name"=>'customer_name',"manager"=>'manager',"subject"=>'subject',"type_status"=>"","account_phone"=>"account_phone","contact_phone"=>"contact_phone",'city'=>"city","invoicing_type"=>"","color_code"=>"");
            break;
        case 'invoices':
            $array=array("s_number"=>'serial_number',"doc_type_title"=>'doc_type_title',"created"=>'t_date',"due_date"=>'due_date',"buyer_name"=>'buyer_name',"mandate_reference"=>'sepa_number',"our_reference"=>'our_ref',"amount"=>'amount',"amount_vat"=>'amount_vat','balance'=>'balance', "status"=>"",'days_expired'=>'days_expired','identity_name'=>'','acc_manager_name'=>'acc_manager_name','subject'=>'subject',"your_ref"=>"your_ref");
            break;
        case 'quotes':
            $array=array("serial_number"=>'serial_number',"created"=>'created',"buyer_name"=>'buyer_name',"subject"=>'subject',"amount"=>'amount','status_customer'=>'',"own_reference"=>'own_reference', "author"=>'author',"acc_manager_name"=>'acc_manager_name',"amount_vat"=>'amount_vat','identity_name'=>'','deal_stage'=>'' );
            break;
        case 'recinvoices':
            $array=array("buyer_name"=>"buyer_name","next_date"=>"next_date","title"=>"title","contract_reference"=>"our_ref","mandate_reference"=>"sepa_number","amount"=>"amount","start_date"=>"start_date","end_date"=>"end_date",'invoice_email'=>"",'payment_method'=>"payment_method",'frequency'=>"frequency",'your_ref'=>"your_ref",'our_ref'=>"our_ref",'mrr'=>'mrr','last_upd'=>'last_upd');
            break;
        case 'purchase_invoices':
            $array=array("b_number"=>'booking_number','s_number'=>'invoice_number','created'=>'t_date','due_date'=>'due_date','buyer_name'=>'buyer_name','expense_category'=>'expense_category','amount'=>'total','amount_vat'=>'total_with_vat','balance'=>'balance','status'=>'','ogm'=>'','reference'=>'reference','last_update'=>'last_update');
            break;
        case 'reminders':
            $array=array("buyer_name"=>"buyer_name","acc_manager_name"=>"","date_sent"=>"sent_date","amount"=>"amount");
            break;
        case 'stock_transactions':
            $array=array("date"=>"t_date","selling_reference"=>"serial_number","article_name"=>"article_name","item_code"=>"item_code","movement_type"=>"","delivery_date"=>"delivery_Date","created_by"=>"","stock"=>"","new_stock"=>"","buyer_name"=>"","line_price"=>"");
            break;
    }
    return $array;
}

function build_expense_category_dd($selected='',$view=0){
    $db = new sqldb();
    $e=$db->query("SELECT id,name FROM tblinvoice_expense_categories ORDER BY sort_order ASC")->getAll();
    if(!$view){
        array_push($e, array('id'=>"0",'name'=>gm('Select expense category')));
    }else{
        array_unshift($e, array('id'=>"0",'name'=>gm('All expense categories')));
    }

    return $e;

}

function order_array_by_column($array, $column, $order,$offset=0,$l_r=0){
    //console::log($array,$column, $order,$offset,$l_r);
    if($order ==' ASC '){
        $exo = array_sort($array, $column.'_ord', SORT_ASC);    
    }else{
        $exo = array_sort($array, $column.'_ord', SORT_DESC);
    }
    if($l_r){
       $exo = array_slice($exo, $offset*$l_r, $l_r);       
    }
    $array=array();
    foreach ($exo as $key => $value) {
           array_push($array, $value);
       } 
   //  console::log($array);
   return $array;
}

function get_subscription_plans($selected_plan){
    $currency_type = ACCOUNT_CURRENCY_TYPE;
    $currency = get_commission_type_list($currency_type);
    //nu schimba ordinea id-urilor din urmatorul array!!
    $result =array(
                 array(     "id" =>'1',
                            "sort_order" =>'2',
                            "name" => gm("Starter"),  
                            "value"  => 30,
                            "value_annual" =>30*0.9,
                            "value_currency" =>place_currency(30,$currency),
                            "value_currency_annual" =>place_currency(30*0.9,$currency),
                            "nr_users" => 1, 
                            "selected" => $selected_plan =='1'? true:false,
                            "select_text" => $selected_plan =='1'? gm("Selected"):gm("Select"),
                            "can_be_selected" => $selected_plan < 1? true:false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Devis"),
                                               gm("Facturation")
                                        ),
                            "additionals" => array('0','2'),
                            "akti_code" => 'AKTI-001'
                             ),
                 array(     "id" =>'2',
                            "sort_order" =>'3',
                            "name" => gm("Essential"),  
                            "value"  => 60,
                            "value_annual" =>60*0.9,
                            "value_currency" =>place_currency(60,$currency),
                            "value_currency_annual" =>place_currency(60*0.9,$currency),
                            "nr_users" => 2,
                            "selected" => $selected_plan =='2'? true:false,
                            "select_text" => $selected_plan =='2'? gm("Selected"):gm("Select"),
                            "can_be_selected" => $selected_plan < 2? true:false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Devis"),
                                               gm("Facturation"),
                                               gm("Marketplace")
                                               ),
                            "additionals" => array('0','2','12'),
                            "akti_code" => 'AKTI-002'
                        ),
                 array(     "id" =>'3',
                            "sort_order" =>'4',
                            "name" => gm("Growth Stock"),  
                            "value"  => 110,
                            "value_annual" =>110*0.9,
                            "value_currency" =>place_currency(110,$currency),
                            "value_currency_annual" =>place_currency(110*0.9,$currency),
                            "nr_users" => 2,
                            "selected" => $selected_plan =='3'? true:false,
                            "select_text" => $selected_plan =='3'? gm("Selected"):gm("Select"),
                            "can_be_selected" => ($selected_plan < 3 && $selected_plan !=4 )? true:false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Products+"),
                                               gm("Devis"),
                                               gm("Orders"),
                                               gm("Purchase orders"),
                                               gm("Facturation"),
                                               gm("Marketplace")
                                               ),
                            "additionals" => array('0','2','1','3','4','13','12'),
                            "akti_code" => 'AKTI-003'
                        ),
                 array(     "id" =>'4',
                            "sort_order" =>'5',
                            "name" => gm("Growth Interventions"),  
                            "value"  => 110,
                            "value_annual" =>110*0.9,
                            "value_currency" =>place_currency(110,$currency),
                            "value_currency_annual" =>place_currency(110*0.9,$currency),
                            "nr_users" => 5,
                            "selected" => $selected_plan =='4'? true:false,
                            "select_text" => $selected_plan =='4'? gm("Selected"):gm("Select"),
                            "can_be_selected" => ($selected_plan < 4 && $selected_plan !=3 )? true:false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Products+"),
                                               gm("Devis"),
                                               gm("Interventions"),
                                               gm("Contracts"),
                                               gm("Installations"),
                                               gm("Facturation"),
                                               gm("Marketplace")
                                               ),
                            "additionals" => array('0','2','1','7','8','12'),
                            "akti_code" => 'AKTI-004'
                             ),
                 array(     "id" =>'5',
                            "sort_order" =>'6',
                            "name" => gm("Advanced"),  
                            "value"  => 160,
                            "value_annual" =>160*0.9,
                            "value_currency" =>place_currency(160,$currency),
                            "value_currency_annual" =>place_currency(160*0.9,$currency),
                            "nr_users" => 5,
                            "selected" => $selected_plan =='5'? true:false,
                            "select_text" => $selected_plan =='5'? gm("Selected"):gm("Select"),
                            "can_be_selected" => ($selected_plan < 5 )? true:false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Products+"),
                                               gm("Devis"),
                                               gm("Orders"),
                                               gm("Purchase orders"),
                                               gm("Interventions"),
                                               gm("Contracts"),
                                               gm("Installations"),
                                               gm("Facturation"),
                                               gm("Marketplace"),
                                               gm("Barcode scanning")
                                               ),
                            "additionals" => array('0','2','1','3','4','13','7','8','11','12','13','14'),
                            "akti_code" => 'AKTI-005'
                              ),
                 array(     "id" =>'6',
                            "sort_order" =>'7',
                            "name" => gm("Custom"),  
                            "value"  => 0,
                            "value_annual" =>0,
                            "value_currency" =>place_currency(0,$currency),
                            "value_currency_annual" =>place_currency(0,$currency),
                            "nr_users" => 0,
                            "selected" => $selected_plan =='6'? true:false,
                            "select_text" => $selected_plan =='6'? gm("Selected"):gm("Select"),
                            "can_be_selected" => ($selected_plan < 6 )? true:false,
                            "options" => array(),
                            "additionals" => array('0','2','1','3','4','13','7','8','11','12'),
                              ),
                 array(     "id" =>'7',
                            "sort_order" =>'1',
                            "name" => gm("Easy invoice"),  
                            "value"  => 5,
                            "value_annual" =>5*0.9,
                            "value_currency" =>place_currency(5,$currency),
                            "value_currency_annual" =>place_currency(5*0.9,$currency),
                            "nr_users" => 1, 
                            "selected" => $selected_plan =='7'? true:false,
                            "select_text" => $selected_plan =='7'? gm("Selected"):gm("Select"),
                            "can_be_selected" => false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Facturation")
                                        ),
                            "additionals" => array('0','2'),
                            "akti_code" => 'AKTI-007'
                             ),
                 array(     "id" =>'8',
                            "sort_order" =>'8',
                            "name" => gm("Easy Invoice - Facq"),  
                            "value"  => 0,
                            "value_annual" =>0,
                            "value_currency" =>place_currency(0,$currency),
                            "value_currency_annual" =>place_currency(0,$currency),
                            "nr_users" => 2, 
                            "selected" => $selected_plan =='7'? true:false,
                            "select_text" => $selected_plan =='7'? gm("Selected"):gm("Select"),
                            "can_be_selected" => false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Facturation"),
                                                gm("Purchase orders"),
                                        ),
                            "additionals" => array('0','2'),
                            "akti_code" => 'AKTI-008' 
                             ),
                array(     "id" =>'9',
                            "sort_order" =>'9',
                            "name" => gm("Easy Invoice - DIY"),  
                            "value"  => 0,
                            "value_annual" =>0,
                            "value_currency" =>place_currency(0,$currency),
                            "value_currency_annual" =>place_currency(0,$currency),
                            "nr_users" => 1,
                            "selected" => $selected_plan =='9'? true:false,
                            "select_text" => $selected_plan =='9'? gm("Selected"):gm("Select"),
                            "can_be_selected" => $selected_plan < 9? true:false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Facturation")
                                        ),
                            "additionals" => array('0','2'),
                            "akti_code" => 'AKTI-009' 
                        ),
                array(     "id" =>'10',
                            "sort_order" =>'10',
                            "name" => gm("Easy Invoice - Billtobox"),  
                            "value"  => 10,
                            "value_annual" =>10*0.9,
                            "value_currency" =>place_currency(10,$currency),
                            "value_currency_annual" =>place_currency(10*0.9,$currency),
                            "nr_users" => 1,
                            "selected" => $selected_plan =='10'? true:false,
                            "select_text" => $selected_plan =='10'? gm("Selected"):gm("Select"),
                            "can_be_selected" => $selected_plan < 10? true:false,
                            "options" => array(gm("Gestion Contacts"),
                                               gm("Catalog"),
                                               gm("Devis"),
                                               gm("Facturation")
                                               ),
                            "additionals" => array('0','2'),
                        ),
            );
    return $result;

}

function get_name_subscription_plan_ac($subscription_plan=''){
    //subsription type for Active Campaign
    $subscription_type ='None';
    switch ($subscription_plan) {
                    case '1':
                        $subscription_type = 'Starter';
                    break;
                    case '2':
                        $subscription_type = 'Essential';
                    break;
                    case '3':
                        $subscription_type = 'Growth - Stock';
                    break;
                    case '4':
                        $subscription_type = 'Growth - Interventions';
                    break;
                    case '5':
                        $subscription_type = 'Advanced';
                    break;
                    case '6':
                        $subscription_type = 'Custom';
                    break;
                    case '7':
                        $subscription_type = 'Easy Invoice';
                    break;
                    case '8':
                        $subscription_type = 'Easy Invoice - Facq';
                    break;
                    case '9':
                        $subscription_type = 'Easy Invoice - DIY';
                    break;
                    case '10':
                        $subscription_type = 'Easy Invoice - Billtobox';
                    break;
                    default:
                        $subscription_type = 'None';
                    break;
                }
    return $subscription_type;
}
function build_article_variant_types_dd(){
    $db = new sqldb();
    $array = array();

    $db->query("SELECT name, id FROM pim_article_variant_types ORDER BY sort_order ASC");
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
    }

    if($_SESSION['access_level'] == 1){
        array_push($array,array('id'=>'0','name'=>""));
    }

    return $array;
}

function send_email_won_customer($id, $period)
{
    global $database_config;
    require_once (__DIR__.'/class.phpmailer.php');
    include_once (__DIR__.'/class.smtp.php'); // optional, gets called from within class.phpmailer.php if not already loaded

    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_u= new sqldb($db_config);

    $user_info = $db_u->query("SELECT ACCOUNT_COMPANY, ACCOUNT_BILLING_COUNTRY_ID, valid_vat, last_payment, first_paid,pricing_plan_id, base_plan_id, users, period FROM users WHERE user_id='".$id."' ");
    $account_name = $user_info->f('ACCOUNT_COMPANY');
    $won_date = date(ACCOUNT_DATE_FORMAT,$user_info->f('first_paid'));
    
    $currency_type = ACCOUNT_CURRENCY_TYPE;
    $currency = get_commission_type_list($currency_type);

    $in['period']  = $period;
    $selected_plan  = $user_info->f('pricing_plan_id');
    $pricing_plans= get_subscription_plans($selected_plan);
    $base_plan_id = $user_info->f('base_plan_id');
     $custom_pricing_plans =['1','2','3','4','5','7','8'];
     if(in_array($selected_plan, $custom_pricing_plans)){
        $total_price=$in['period']=='1'? $pricing_plans[$selected_plan-1]['value'] : $pricing_plans[$selected_plan-1]['value_annual'];
        $nr_users= $pricing_plans[$selected_plan-1]['nr_users'];
     }else{
        if ($base_plan_id=='6'){
            $total_price=$start_price;
            $nr_users=2;
            foreach($aditional as $key=>$value){
                if($value[$value['model']]){
                    $total_price+=$value['price'];
                }
            }
        }else{
            $total_price=$in['period']=='1'? $pricing_plans[$base_plan_id-1]['value'] : $pricing_plans[$base_plan_id-1]['value_annual'];
            //console::log($total_price);
            $nr_users = $pricing_plans[$base_plan_id-1]['nr_users'];
            foreach($pricing_plans[$base_plan_id-1]['additionals'] as $key => $value){
                    $aditional[$value][$aditional[$value]['model']]=true;
                }
            foreach($aditional as $key=>$value){
                if($value[$value['model']] && !in_array($key, $pricing_plans[$base_plan_id-1]['additionals'])){
                    //console::log($value['price']);
                    $total_price+=$value['price'];                          
                }
            }
        }
     }

        if($in['period']=='1'){
            $total_price+=(($user_info->f('users')-$nr_users)*12);
        }else{
            //$total_price-=$total_price*10/100;
            $total_price+=(($user_info->f('users')-$nr_users)*10);
        }

    //$value_subscription = place_currency($user_info->f('last_payment'),$currency);
       if($in['period']!='1'){
        $total_price = 12*$total_price;
       } 

       $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
        $vat_value=0;
        if(in_array(get_country_code($user_info->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries)){
            if(get_country_code($user_info->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE'){
                    $vat_value=21;
            }else{
                if(!$user_info->f('valid_vat')){
                    $vat_value=21;
                }
            }
        } 
        $total_price += $total_price*$vat_value/100;
        $value_subscription = place_currency($total_price,$currency);

    $mail = new PHPMailer();

    $fromMail='noreply@akti.com';
    $email='sales@akti.com';
    //$email='cristina@medeeaweb.com';

    $subject = 'Won customer: '.$account_name;

    $body = $account_name.' has activated his subscription on '.$won_date.' for '.$value_subscription.'.';

    $mail->Subject = $subject;

    $mail->SetFrom($fromMail, $fromMail);
    $mail->AddReplyTo($fromMail, $fromMail);

    $mail->MsgHTML(nl2br($body));

    if(!$email){
        return false;
    }
    $mail->AddAddress($email);

    $mail->Send();
    return true;
}

function send_email_update_subscription($id, $pricing_plan, $period, $users)
{
    global $database_config;
    require_once (__DIR__.'/class.phpmailer.php');
    include_once (__DIR__.'/class.smtp.php'); // optional, gets called from within class.phpmailer.php if not already loaded

    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_u= new sqldb($db_config);

    $user_info = $db_u->query("SELECT ACCOUNT_COMPANY, ACCOUNT_BILLING_COUNTRY_ID, users FROM users WHERE user_id='".$id."' ");
    $account_name = $user_info->f('ACCOUNT_COMPANY');
    $update_date = date(ACCOUNT_DATE_FORMAT,time());
    
    $currency_type = ACCOUNT_CURRENCY_TYPE;
    $currency = get_commission_type_list($currency_type);

    $in['period']  = $period;
    $selected_plan  = $pricing_plan;
    $pricing_plans= get_subscription_plans($selected_plan);
    $base_plan_id =  $pricing_plan;
     $custom_pricing_plans =['1','2','3','4','5','7','8'];
     if(in_array($selected_plan, $custom_pricing_plans)){
        $total_price=$in['period']=='1'? $pricing_plans[$selected_plan-1]['value'] : $pricing_plans[$selected_plan-1]['value_annual'];
        $nr_users= $pricing_plans[$selected_plan-1]['nr_users'];
     }else{
        if ($base_plan_id=='6'){
            $total_price=$start_price;
            $nr_users=2;
            foreach($aditional as $key=>$value){
                if($value[$value['model']]){
                    $total_price+=$value['price'];
                }
            }
        }else{
            $total_price=$in['period']=='1'? $pricing_plans[$base_plan_id-1]['value'] : $pricing_plans[$base_plan_id-1]['value_annual'];
            $nr_users = $pricing_plans[$base_plan_id-1]['nr_users'];
            foreach($pricing_plans[$base_plan_id-1]['additionals'] as $key => $value){
                    $aditional[$value][$aditional[$value]['model']]=true;
                }
            foreach($aditional as $key=>$value){
                if($value[$value['model']] && !in_array($key, $pricing_plans[$base_plan_id-1]['additionals'])){
                    $total_price+=$value['price'];                          
                }
            }
        }
     }

        if($in['period']=='1'){
            $total_price+=(($users-$nr_users)*12);
        }else{
            $total_price+=(($users-$nr_users)*10);
        }

       if($in['period']!='1'){
        $total_price = 12*$total_price;
       } 

       $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
        $vat_value=0;
        if(in_array(get_country_code($user_info->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries)){
            if(get_country_code($user_info->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE'){
                    $vat_value=21;
            }else{
                if(!$user_info->f('valid_vat')){
                    $vat_value=21;
                }
            }
        } 
        $total_price += $total_price*$vat_value/100;
        $value_subscription = place_currency($total_price,$currency);

    $mail = new PHPMailer();

    $fromMail='noreply@akti.com';
    $email='sales@akti.com';
    //$email='cristina@medeeaweb.com';

    $subject = 'Update payment subscription - customer: '.$account_name;

    $body = $account_name.' has updated his subscription on '.$update_date.' for '.$value_subscription.'.';

    $mail->Subject = $subject;

    $mail->SetFrom($fromMail, $fromMail);
    $mail->AddReplyTo($fromMail, $fromMail);

    $mail->MsgHTML(nl2br($body));

    if(!$email){
        return false;
    }
    $mail->AddAddress($email);

    $mail->Send();
    return true;
}

function get_account_reference($id){
    $db = new sqldb();
    $account_ref = $db->field("SELECT our_reference FROM customers WHERE customer_id = '".$id."'");
    return $account_ref;
}


function get_sendgrid_full_contact_name($id){
    $db = new sqldb();
    $db->query("SELECT lastname, firstname FROM customer_contacts WHERE contact_id='".$id."' ");
    $db->next();
    return array('first_name' => $db->f('firstname'),
                'last_name' => $db->f('lastname')
            );
}

function sendgridCall($data,$database_name=false){
    global $database_config,$config;

    if($database_name){
        $database_1 = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_name,
            );
        $db = new sqldb($database_1);
    }else{
        $db = new sqldb();
    }

    $sendgrid_url= "https://api.sendgrid.com/v3";

    $database_akti = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $config['db_akti'],
            );
   $db_akti = new sqldb($database_akti);

    /*$app_id = $db_akti->field("SELECT app_id FROM apps WHERE name='SendGrid' AND type='main' AND main_app_id='0' ");
    $sendgrid_api_key = $db_akti->field("SELECT api FROM apps WHERE main_app_id='".$app_id."' AND type='api_key' ");*/
    $sendgrid_api_key = $config['sendgrid_api_key'];

    $data_curl=array();
     
    $headers=array('Content-Type: application/json','Authorization: Bearer '.$sendgrid_api_key);
    $data_curl['headers']=$headers;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if($data['method'] == "POST"){
        curl_setopt($ch, CURLOPT_POST, true);
    }else if($data['method'] == "PUT" || $data['method'] == "DELETE"){
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $data['method']);
    } 

    if($data['method'] != "GET" || $data['method'] != "DELETE"){ 
        $data_curl['postfields']=$data['webservice_outgoing'];
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data['webservice_outgoing'])); 
    }
    $data_curl['method']=$data['method'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    if($data['method'] == "POST" && $data['call_link'] == '/mail/send'){
        curl_setopt($ch,CURLOPT_HEADER, 1);
    }
    
    curl_setopt($ch, CURLOPT_PORT, 443);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_URL, $sendgrid_url.$data['call_link']);
    $data_curl['url']=$sendgrid_url.$data['call_link'];

    $sendgrid_history_id=sendgrid_insert_log(array(
                            'sent_date'         => time(),
                            'webservice_name'   => $data['webservice_name'],
                            'webservice_id'     => $data['webservice_id'],
                            'module'            => $data['module'],
                            'module_id'         => $data['module_id'],
                            'webservice_outgoing'   => $data_curl,
                            'module_multiple_ids'   => $data['module_multiple_ids'],
                            ),0,($database_name?$database_name:false)
                        );

    $put = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);

    $x_message_id = '';

    if($data['method'] == "POST" && $data['call_link'] == '/mail/send'){ 
        $resp_headers = array();
        $data_headers = explode("\n",$put);
        $data_headers['status'] = $data_headers[0];
        array_shift($resp_headers);

        foreach($data_headers as $h){
          $middle=explode(":",$h);
          $resp_headers[trim($middle[0])] = trim($middle[1]);
        }

        if(!empty($resp_headers['X-Message-Id'])){
          $x_message_id = $resp_headers['X-Message-Id'];
        }
    }

    sendgrid_insert_log(array('webservice_response_code'=>$info['http_code'],
                        'webservice_incomming'=>$put,'x_message_id'=>$x_message_id),$sendgrid_history_id,($database_name?$database_name:false));

    return array('response'=>$put,'headers'=>$info);   
}

/**
 * Used to log actions for sendgrid
 * It accepts 3 parameters: 1 associative array parameter:
 * - sent_date (INT)
 * - webservice_name (varchar - values: mail/send )
 * - webservice_id (INT - in case of sending the company - its database id)
 * - module (VARCHAR - company,contact,invoice,po)
 * - webservice_response_code (INT)
 * - webservice_incomming (TEXT - what has been received from the webservice)
 * - webservice_outgoing (TEXT - what has been sent to the webservice)
 * - from_webhook (TINYINT) 
 * AND 2 parameter id if needed to update the line
 * AND 3 parameter database_name if needed for database instantiation
 * @return void
 * @author 
 **/

function sendgrid_insert_log($data,$id=0,$database_name=false){
    global $database_config;
    if($database_name){
        $database_1 = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_name,
            );
        $db = new sqldb($database_1);
        $database= $database_name;
    }else{
        $db = new sqldb();
        $database= DATABASE_NAME;
    }

    $data['webservice_outgoing'] = str_replace("'",'',$data['webservice_outgoing']);

    if($id){
        $module = $db->query("SELECT module, module_id, module_multiple_ids FROM sendgrid_history WHERE id='".$id."' ");
        $db->query("UPDATE sendgrid_history SET webservice_response_code='".$data['webservice_response_code']."',webservice_incomming='".addslashes(serialize($data['webservice_incomming']))."',x_message_id='".$data['x_message_id']."' WHERE id='".$id."' ");
        sendgrid_xmessage_database($data['x_message_id'], $database, $module->f('module'), $module->f('module_id'), $module->f('module_multiple_ids') );
        return $id;
    }else{
        $sent_date=new DateTime();
        if($data['sent_date']){
            $sent_date->setTimestamp($data['sent_date']);
        }      
        $sent_date->setTimezone(new DateTimeZone('Europe/Brussels'));
        $sentDate=$sent_date->format('Y-m-d H:i:s');

        //Remove base64_encoded content
        if(isset($data['webservice_outgoing']['postfields']['attachments'])){
            foreach ($data['webservice_outgoing']['postfields']['attachments'] as $key => $value) {      
              unset($data['webservice_outgoing']['postfields']['attachments'][$key]['content']);
            }
        }

        $id=$db->insert("INSERT INTO sendgrid_history SET 
            `sent_date` = '".$sentDate."',
            webservice_name = '".$data['webservice_name']."',
            webservice_id ='".$data['webservice_id']."',
            `module`       ='".$data['module']."',  
            `module_id`    ='".$data['module_id']."',     
            webservice_outgoing='".addslashes(serialize($data['webservice_outgoing']))."',
            from_webhook='".$data['from_webhook']."',
            module_multiple_ids= '".$data['module_multiple_ids']."' ");
        return $id;
    }
}

function sendgrid_xmessage_database($x_message_id, $database, $module, $module_id, $module_multiple_ids){
    global $database_config,$config;
    if($x_message_id){
       $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $db_users = new sqldb($db_config);

        $query_id=$db_users->insert("INSERT INTO sendgrid_database SET
                x_message_id                = '".$x_message_id."',
                database_name               = '".$database."',
                module                      = '".$module."',
                module_id                   = '".$module_id."',
                module_multiple_ids         = '".$module_multiple_ids."',
                `created_at`                = '".time()."'
            "); 
    }
    
    return true;
}

function addToExceptionList($data,$database_name=false){
    global $database_config;
    if($database_name){
        $database_1 = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_name,
            );
        $db = new sqldb($database_1);
    }else{
        $db = new sqldb();
    }
    $sent_date=new DateTime('now',new DateTimeZone('Europe/Brussels'));     
    $sentDate=$sent_date->format('Y-m-d H:i:s');
    $db->query("INSERT INTO exception_list SET `date`='".$sentDate."',
                destination='".addslashes($data['destination'])."',
                location_id='".$data['location_id']."',
                `module`='".$data['module']."',
                module_id='".$data['module_id']."',
                `source`='".$data['source']."',
                `message`='".addslashes($data['message'])."' ");
}

function doQueryLog($console=false){
    global $originalIn,$database_config,$config;
    if(!$config['elastic_url'] || $_SERVER['REMOTE_ADDR'] == '188.26.120.86' || !$_SESSION['u_id'] || $originalIn['do'] == 'auth-getVersion' || $originalIn['do'] == 'auth-isLoged'){
        return true;
    }
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    $dataContent=array();    
    if(!$console){
        ark::$dotime[ark::$controller] = console::getEndMicroTime(ark::$starttime[ark::$controller]);
        console::memory();
        console::speed('Load time');
        $console=console::getInstance()->render();
    }
    $dataContent['totalTime']=$console['actionsTotalTimeFloat']>$console['queryTotalsTimeFloat'] ? round($console['actionsTotalTimeFloat'],2) : round($console['queryTotalsTimeFloat'],2);
    $dataContent['totalTimeQueries']=round($console['queryTotalsTimeFloat'],2);
    $dataContent['requestedAction']=$originalIn['xget'] ? $originalIn['do'].'&xget='.$originalIn['xget'] : $originalIn['do'];
    unset($originalIn['do']);
    $dataContent['params']=$originalIn;
    $dataContent['method']=$_SERVER['REQUEST_METHOD'];
    $dataContent['userId']=$_SESSION['u_id'];
    $query_log_id=$db_users->insert("INSERT INTO query_logs SET
            userId      = '".$dataContent['userId']."',
            `ip`        = '".$_SERVER['REMOTE_ADDR']."',
            `method`      = '".$dataContent['method']."',
            requestedAction  = '".$dataContent['requestedAction']."',
            totalTime       =   '".$dataContent['totalTime']."',
            totalTimeQueries = '".$dataContent['totalTimeQueries']."',
            `created_at`      = '".time()."'
        ");
    $db_users->insert("INSERT INTO query_logs_data SET
            log_id      = '".$query_log_id."',
            `params`    = '".base64_encode(serialize($dataContent['params']))."'
        ");
    unset($dataContent);
}

//when an account is won, akti creates a recurring invoice in 04ed5b47_ database
function create_reccuring_invoice(&$in){
     global $config, $database_config;
    if(strpos($config['site_url'],'my.akti')){
   // if(strpos($config['site_url'],'akti')){ //pt testare

        $db = new sqldb();
        $database ='04ed5b47_e424_5dd4_ad024bcf2e1d';
        //$database ='32ada082_5cf4_1518_3aa09bca7f29';

        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $db_u= new sqldb($db_config);

        $database_new = array(
                'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
                'username' => 'admin',
                'password' => 'hU2Qrk2JE9auQA',
                'database' => $database
        );
                            
        $db_user_new = new sqldb($database_new);

        $contact_id =0;
        $customer_id=0;
        $main_user_id = 0;
        $main_user_id = $db_u->field("SELECT main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
        $contact_id = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");

        if(!$contact_id && $main_user_id){      
            $contact_id = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$main_user_id."' AND field_id='1' ");
        }

        if($contact_id){
            $customer_id = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$contact_id."' AND `primary`='1' ");
        }
        if(!$customer_id){
            $customer_id = $db_user_new->field("SELECT customer_id FROM customer_field WHERE value='".$_SESSION['u_id']."' AND field_id='7' ");
        }
        if(!$customer_id && $main_user_id){
            $customer_id = $db_user_new->field("SELECT customer_id FROM customer_field WHERE value='".$main_user_id."' AND field_id='7' ");
        }
         
        if($customer_id){
        //set the user as customer in AKTI account
          $db_user_new->query("UPDATE customers SET is_customer = '1' WHERE customer_id = '".$customer_id."' ");

          $time = $in['time'];
          $account_date_format = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DATE_FORMAT' ");
          $created_date= date($account_date_format,$time);
          $next_date = $in['next_date'];

          if($in['period']=='1'){
            $in['frequency'] =2;
            $discount=0;
            $apply_discount=0;
            $qty = 1;
          }else{
            $in['frequency'] =4;
            $discount=10;
            $apply_discount=2;
            $qty = 12;
          }
     
            if(date('I',$next_date)=='1'){
              $next_date+=3600;
            }
            $strt_date=$time;
            if(date('I',$strt_date)=='1'){
              //$strt_date+=3600;
            }



       // $seller_info = $db_u->query("SELECT * FROM users WHERE database_name='04ed5b47_e424_5dd4_ad024bcf2e1d' AND main_user_id='0'");
       //$seller_info->move_next();
        $seller_name = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_COMPANY'");
        $seller_billing_address = $db_user_new->field("SELECT long_value FROM settings WHERE constant_name='ACCOUNT_BILLING_ADDRESS'");
        $seller_billing_zip = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_ZIP'");
        $seller_billing_city = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_CITY'");
        $seller_billing_country = $db_user_new->field("SELECT value from settings where  constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
        $seller_billing_state = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_STATE_ID'");

        $seller_delivery_address = $db_user_new->field("SELECT long_value FROM settings WHERE constant_name='ACCOUNT_DELIVERY_ADDRESS'");
        $seller_delivery_zip = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DELIVERY_ZIP'");
        $seller_delivery_city = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DELIVERY_CITY'");
        $seller_delivery_country = $db_user_new->field("SELECT value from settings where  constant_name='ACCOUNT_DELIVERY_COUNTRY_ID'");
        $seller_delivery_state = $db_user_new->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DELIVERY_STATE_ID'");
    

        $user_address = $db_user_new->query("SELECT * FROM customer_addresses WHERE customer_id='".$customer_id."' and `is_primary`=1 ");
      /*  $address_info = $user_address->f('address')."\n".$user_address->f('zip').'  '.$user_address->f('city')."\n".get_country_name($user_address->f('country_id'));*/
        $main_address_id = $user_address->f('address_id');
        /*$address_info = $user_info->f('ACCOUNT_BILLING_ADDRESS')."\n".$user_info->f('ACCOUNT_BILLING_ZIP').'  '.$user_info->f('ACCOUNT_BILLING_CITY')."\n".get_country_name($user_info->f('ACCOUNT_BILLING_COUNTRY_ID'));*/
        

        $user_info = $db_u->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."'");
        $user_info->move_next();
        $lang = $user_info->f('lang_id');
        $lang_code = $db_u->field("SELECT code FROM lang WHERE lang_id='".$user_info->f('lang_id')."'");
        if($lang_code=='nl'){
            $lang_code='du';
        }
        $lang_id = $db_u->field("SELECT lang_id FROM pim_lang WHERE code='".$lang_code."'");

        $customer = $db_user_new->query("SELECT * FROM customers WHERE customer_id='".$customer_id."'");
        $account_company = $customer->f('name');
        $account_billing_address = $db->field("SELECT long_value FROM settings WHERE constant_name='ACCOUNT_BILLING_ADDRESS'");
        //$account_billing_address = $user_address->f('address');
        $account_vat_number = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT_NUMBER'");
        //$account_vat_number = $customer->f('btw_nr');
        $account_billing_zip = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_ZIP'");
        // $account_billing_zip = $user_address->f('zip');
        $account_billing_city = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_CITY'");
        //$account_billing_city = $user_address->f('city');
        $billing_country = $db->field("SELECT value from settings where  constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
        //$billing_country = $user_address->f('country_id');
        $account_billing_state = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_STATE_ID'");

        $account_phone = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PHONE'");
        //$account_phone = $customer->f('comp_phone');
        $account_fax = $db->field("SELECT value from settings where  constant_name='ACCOUNT_FAX'");
         //$account_fax = $customer->f('comp_fax');
        $account_email = $db->field("SELECT value from settings where  constant_name='ACCOUNT_EMAIL'");
        //$account_email = $customer->f('c_email');

        $address_info = $account_billing_address."\n".$account_billing_zip.'  '.$account_billing_city."\n".get_country_name($billing_country);
        $free_field=$address_info;
        $same_address=0;

        $vat_regime_id = $db_user_new->field("SELECT vat_new.id FROM vat_new 
                                    LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
                                    WHERE vats.value=0 ");
        $vat=0;
        $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
        
        if(in_array(get_country_code($billing_country), $eu_countries)){
            if(get_country_code($billing_country) == 'BE'){
                    $vat_regime_id = $db_user_new->field("SELECT vat_new.id FROM vat_new 
                                    LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
                                    WHERE vats.value=21 ");
                    $vat=21;
            }else{
                if(!$user_info->f('valid_vat')){
                    $vat_regime_id = $db_user_new->field("SELECT vat_new.id FROM vat_new 
                                    LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
                                    WHERE vats.value=21 ");
                    $vat=21;
                }
            }
        }

         $in['recurring_invoice_id']=$db_user_new->insert("INSERT INTO recurring_invoice SET
                                                        title                   =   '".gm('Subscription')."',
                                                        start_date              =   '".$strt_date."',
                                                        frequency               =   '".$in['frequency']."',
                                                        days                    =   '0',
                                                        next_date               =   '".$next_date."',
                                                        end_date                =   '',
                                                        invoice_delivery        =   '1',                                                       
                                                        req_payment             =   '0',
                                                        currency_type           =   '1',
                                                        vat_regime_id           =   '".$vat_regime_id."',
                                                        type                    =   '0',
                                                        status                  =   '0',
                                                        f_archived              =   '0',
                                                        paid                    =   '0',
                                                        notes2                  =   '',
                                                        buyer_id                =   '".$customer_id."',
                                                        buyer_name              =   '".addslashes($account_company)."',
                                                        buyer_address           =   '".addslashes($account_billing_address)."',
                                                        buyer_email             =   '".$account_email."',
                                                        buyer_phone             =   '".$account_phone."',
                                                        buyer_fax               =   '".$account_fax."',
                                                        buyer_zip               =   '".$account_billing_zip."',
                                                        buyer_city              =   '".addslashes($account_billing_city)."',
                                                        buyer_country_id        =   '".$billing_country."',
                                                        buyer_state_id          =   '".$account_billing_state."',
                                                        seller_id               =   '',
                                                        seller_name             =   '".addslashes($seller_name)."',
                                                        seller_d_address        =   '".addslashes($seller_delivery_address)."',
                                                        seller_d_zip            =   '".$seller_delivery_zip."',
                                                        seller_d_city           =   '".addslashes($seller_delivery_city)."',
                                                        seller_d_country_id     =   '".$seller_delivery_country."',
                                                        seller_d_state_id       =   '".$seller_delivery_state."',
                                                        seller_b_address        =   '".addslashes($seller_billing_address)."',
                                                        seller_b_zip            =   '".$seller_billing_zip."',
                                                        seller_b_city           =   '".addslashes($seller_billing_city)."',
                                                        seller_b_country_id     =   '".$seller_billing_country."',
                                                        seller_b_state_id       =   '".$seller_billing_state."',
                                                        seller_bwt_nr           =   '".$account_vat_number."',
                                                        created                 =   '".$created_date."',
                                                        created_by              =   '".$_SESSION['u_id']."',
                                                        last_upd                =   '".$created_date."',
                                                        last_upd_by             =   '".$_SESSION['u_id']."',
                                                        vat                     =   '".display_number($vat)."',
                                                        email_language          =   '".$lang_id."',
                                                        lid                     =   '".$lang_id."',
                                                        contact_id              =   '',
                                                        apply_discount          =   '".$apply_discount."',
                                                        discount                =   '".$discount."',
                                                        use_html                =   '1',
                                                        currency_rate           =   '',
                                                        remove_vat              =   '0',
                                                        mandate_id              =   '0',
                                                        free_field              =   '".addslashes($free_field)."',
                                                        same_address            =   '".$same_address."',
                                                        main_address_id         =   '".$main_address_id."',
                                                        discount_line_gen       =   '0',
                                                        our_ref                 =   '',
                                                        your_ref                =   '',
                                                        contract_id             =   '',
                                                        acc_manager_id          =   '',
                                                        end_contract            =   '',
                                                        source_id               =   '',
                                                        type_id                 =   '',
                                                        segment_id              =   '',
                                                        payment_method          =   '2' ");
           

            // add multilanguage order notes in note_fields
                $in['NOTES'] = $db_user_new->field("SELECT value FROM default_data WHERE default_name='invoice_terms' AND type = 'invoice_terms_".$lang_id."' ");
                    $db_user_new->insert("INSERT INTO note_fields SET
                                    item_id         = '".$in['recurring_invoice_id']."',
                                    lang_id         =   '".$lang_id."',
                                    active          =   '1',
                                    item_type         =   'recurring_invoice',
                                    item_name         =   'notes',
                                    item_value        =   '".$in['NOTES']."'
                    ");
              
                //INSERT EMAIL SETTINGS
                $filter = ' ';
                if(!empty($lang_id) && is_numeric($lang_id)){
                    if($lang_id>=1000) {
                        $code = $db_user_new->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$lang_id."' ");
                        if($code=='nl'){
                            $code='du';
                        }
                        $filter = " AND lang_code = '".$code."' ";
                    } else {
                        $code = $db_user_new->field("SELECT code FROM pim_lang WHERE lang_id='".$lang_id."' ");
                        if($code=='nl'){
                            $code='du';
                        }
                        $filter = " AND lang_code='".$code."' ";
                    }
                }else if(!empty($lang_id)){
                    $filter = " AND lang_code='".$lang_id."' ";
                }

                $message_data=$db_user_new->query("SELECT * FROM sys_message WHERE name='invmess' ".$filter);
                $message_data->move_next();
                $msg=array();
                $msg['text']=$message_data->f('text');
                $msg['from_email']=$message_data->f('from_email');
                $msg['from_name']=$message_data->f('from_name');
                $msg['subject']=$message_data->f('subject');
                $msg['footnote']=$message_data->f('footnote');
                $msg['use_html']=$message_data->f('use_html');
                if($msg['use_html'] == 1 && $message_data->f('html_content')){
                    $msg['text']=$message_data->f('html_content');
                }

                $db_user_new->query("UPDATE recurring_invoice SET
                                                        email_subject       =   '".addslashes($msg['subject'])."',
                                                        email_body          =   '".addslashes($msg['text'])."',
                                                        include_pdf         =   '1',
                                                        include_xml         =   '1',
                                                        copy                =   '0',
                                                        lid                 =   '".$lang_id."'
                                         WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."' ");

  
                 $db_user_new->query("INSERT INTO recurring_email_contact SET
                                                    recurring_invoice_id   =   '".$in['recurring_invoice_id']."',
                                                    email         =   '".$account_email."' ");

                //INSERT LINES
                $line_vat = 0;
      
                $created_date_line=  date("Y-m-d H:i:s");
                $invoice_total = 0;
              
                $cred_ids = array();
                $akti_codes = array();
                $credentials = $user_info->f('credentials');
                $pricing_plan_id = $user_info->f('pricing_plan_id');
                $selected_plan = $user_info->f('pricing_plan_id');
                $base_plan_id = $user_info->f('base_plan_id');
                $users = $user_info->f('users');
                $pricing_plans= get_subscription_plans($selected_plan);
                $aditional = get_aditional();
                if($user_info->f('credentials_serial')!=''){
                    $stored_aditional=unserialize($user_info->f('credentials_serial'));
                    foreach($aditional as $key=>$value){
                        foreach($stored_aditional as $key1=>$value1){
                            if($value['model'] == $value1['model'] && $value1[$value1['model']]=='1'){
                                $aditional[$key][$value['model']]=true;
                                break;
                            }
                        }
                    }
                }

                 $custom_pricing_plans =['1','2','3','4','5','7','8'];
                 if(in_array($selected_plan, $custom_pricing_plans)){
                    $akti_code =$pricing_plans[$selected_plan-1]['akti_code'];
                    array_push($akti_codes, $akti_code);
                    $nr_users= $pricing_plans[$selected_plan-1]['nr_users'];
                 }else{
                    if ($base_plan_id=='6'){
                        array_push($akti_codes, 'AKTI-001');
                        $nr_users=2;
                        foreach($aditional as $key=>$value){
                            if($value[$value['model']]){
                                array_push($akti_codes, $value['akti_code']);
                            }
                        }
                    }else{
                        $akti_code =$pricing_plans[$base_plan_id-1]['akti_code'];
                        array_push($akti_codes, $akti_code);
                        $nr_users = $pricing_plans[$base_plan_id-1]['nr_users'];
                        foreach($pricing_plans[$base_plan_id-1]['additionals'] as $key => $value){
                                $aditional[$value][$aditional[$value]['model']]=true;
                            }
                        foreach($aditional as $key=>$value){
                            if($value[$value['model']] && !in_array($key, $pricing_plans[$base_plan_id-1]['additionals'])){ 
                               array_push($akti_codes, $value['akti_code']);                        
                            }
                        }
                    }
                 }
                 $extra_users = 0;
                 if($users > $nr_users){
                    $extra_users = $users - $nr_users;
                    array_push($akti_codes, 'AKTI-010');
                 }

                $i = 0;
                foreach($akti_codes as $key => $value){
                    $line_total = 0;
                    
                    
                    $akti_article=$db_user_new->query("SELECT pim_articles.*, vats.value FROM pim_articles
                                                        LEFT JOIN vats on pim_articles.vat_id = vats.vat_id WHERE item_code='".$value."'");
                    $akti_article->move_next();
                    $price = $akti_article->f('price');
                    if($value == 'AKTI-010'){
                        $qty = $qty * $extra_users;
                        if($in['period']=='1'){
                            $price = 12;
                          }else{                            
                            $price = 11.111; 
                          }
                    }
                    $line_total=$qty * $price;
                    //$vat_value = $akti_article->f('value');
                    $vat_value = $vat;
                    //$line_total_vat= $line_total + $line_total * $vat_value;
                    $invoice_total += $line_total;
                    $db_user_new->query("INSERT INTO recurring_invoice_line SET
                                                            name                    =   '".html_entity_decode($akti_article->f('internal_name'))."',
                                                            recurring_invoice_id    =   '".$in['recurring_invoice_id']."',
                                                            quantity                =   '".$qty."',
                                                            price                   =   '".$price."',
                                                            amount                  =   '".$line_total."',
                                                            f_archived              =   '0',
                                                            created                 =   '".$created_date_line."',
                                                            created_by              =   '".$_SESSION['u_id']."',
                                                            last_upd                =   '".$created_date_line."',
                                                            vat                     =   '".return_value(display_number($vat_value))."',
                                                            discount                =   '0',
                                                            last_upd_by             =   '".$_SESSION['u_id']."',
                                                            article_id              =   '".$akti_article->f('article_id')."',
                                                            content                 =   '',
                                                            content_title           =   '',
                                                            item_code               =   '".$value."',
                                                            sort_order              =   '".$i."',
                                                            tax_id                  =   '',
                                                            tax_for_article_id      =   '' ");
               
                $i++;
                              

                }  
                if($apply_discount){
                    $invoice_total = $invoice_total - ($discount * $invoice_total/100);
                }
                $db_user_new->query("UPDATE recurring_invoice SET amount='".$invoice_total."' WHERE recurring_invoice_id='".$in['recurring_invoice_id']."'");   
               create_first_invoice($in);
               if($in['invoice_id']){
                // $database is '04ed5...'
                // push_invoice_to_stripe($in['invoice_id'], $database, true, true);
               }
               return true;
        }           
                        
     }

    }
    
     function create_first_invoice(&$in){
        $database ='04ed5b47_e424_5dd4_ad024bcf2e1d';
        //$database ='32ada082_5cf4_1518_3aa09bca7f29';
         $database_new = array(
                'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
                'username' => 'admin',
                'password' => 'hU2Qrk2JE9auQA',
                'database' => $database
        );
                            
        $db_user_new = new sqldb($database_new);
        $constants=$db_user_new->query("select constant_name, value, long_value from settings");
        while($constants->move_next()){
            if($constants->f('constant_name')){
                $const[$constants->f('constant_name')] = $constants->f('value') ? $constants->f('value') : utf8_decode($constants->f('long_value'));
            }
        }

        $DRAFT_INVOICE_NO_NUMBER=$db_user_new->field("SELECT value FROM settings WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
        $today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $today_end = mktime( 23, 59, 59, date('m'), date('d'), date('Y'));
        $created_date= date('Y-m-d');
        $invoice_date=$today;
        
        $rec_invoices=$db_user_new->query("SELECT recurring_invoice.* FROM recurring_invoice
            WHERE recurring_invoice.recurring_invoice_id ='".$in['recurring_invoice_id']."'");

        while($rec_invoices->move_next()){
            if($DRAFT_INVOICE_NO_NUMBER=='1'){
                $serial_number='';
            }else{
                $serial_number=generate_invoice_number($database);
            }      
            if($rec_invoices->f('buyer_id')){
                $buyer_details = $db_user_new->query("SELECT customers.customer_id as buyer_id, customers.name, customers.payment_term, customer_addresses . *, customers.btw_nr, customers.c_email, customer_legal_type.name AS l_name, customers.invoice_note2, customers.internal_language
                    FROM customers
                    LEFT JOIN customer_addresses ON customer_addresses.customer_id = customers.customer_id
                    LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                    AND customer_addresses.billing =1
                    WHERE customers.customer_id = '".$rec_invoices->f('buyer_id')."'");
                if(!$buyer_details->next()){
                    $buyer_details = $db_user_new->query("SELECT customers.customer_id as buyer_id, customers.name, customers.payment_term, customer_addresses . *, customers.btw_nr, customers.c_email, customer_legal_type.name AS l_name, customers.invoice_note2, customers.internal_language
                    FROM customers
                    LEFT JOIN customer_addresses ON customer_addresses.customer_id = customers.customer_id
                    LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                    AND customer_addresses.is_primary =1
                    WHERE customers.customer_id = '".$rec_invoices->f('buyer_id')."'");
                }
            }else{
                $buyer_details = $db_user_new->query("SELECT CONCAT_WS(' ',firstname, lastname) AS name, phone AS comp_phone, email AS c_email,customer_contact_address.*
                                            FROM customer_contacts
                                            LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
                                            WHERE customer_contacts.contact_id='".$rec_invoices->f('contact_id')."' ");
                $buyer_details->next();
            }
            $email_lg = $rec_invoices->f('email_language');
            if(!$email_lg){
                $email_lg = $buyer_details->f('internal_language');
            }
            if(!$email_lg){
                $email_lg = $db_user_new->field("SELECT * FROM `pim_lang` WHERE `default_lang`=1 ");
            }
            //$invoice_due_date = $invoice_date + ( $buyer_details->f('payment_term') * (60*60*24) ) ;
            $invoice_due_date = $invoice_date + ( 15 * (60*60*24) ) ;

            $free_field = $buyer_details->f('address').'
            '.$buyer_details->f('zip').' '.$buyer_details->f('city').'
            '.get_country_name($buyer_details->f('country_id'));

            $invoice_id=$db_user_new->insert("INSERT INTO tblinvoice SET
                serial_number               = '".addslashes($serial_number)."',
                invoice_date                = '".$invoice_date."',
                due_date                    = '".$invoice_due_date."',
                due_days                    = '15',
                discount                    = '".$rec_invoices->f('discount')."',
                vat                         = '".$rec_invoices->f('vat')."',
                currency_type               = '".$rec_invoices->f('currency_type')."',
                email_subject               = '".addslashes($rec_invoices->f('email_subject'))."',
                email_body                  = '".addslashes($rec_invoices->f('email_body'))."',
                include_pdf                 = '".addslashes($rec_invoices->f('include_pdf'))."',
                copy                        = '".$rec_invoices->f('copy')."',
                notes                       = '".addslashes($rec_invoices->f('notes'))."',
                notes2                      = '".addslashes($buyer_details->f('invoice_note2'))."',
                buyer_id                    = '".$rec_invoices->f('buyer_id')."',
                buyer_name                  = '".addslashes($buyer_details->f('name').' '.$buyer_details->f('l_name'))."',
                buyer_address               = '".addslashes($buyer_details->f('address'))."',
                buyer_email                 = '".$buyer_details->f('c_email')."',
                buyer_phone                 = '".$buyer_details->f('comp_phone')."',
                buyer_fax                   = '".$buyer_details->f('comp_fax')."',
                buyer_zip                   = '".$buyer_details->f('zip')."',
                buyer_city                  = '".addslashes($buyer_details->f('city'))."',
                buyer_country_id            = '".$buyer_details->f('country_id')."',
                buyer_state_id              = '".addslashes($buyer_details->f('state_id'))."',
                seller_id                   = '".$rec_invoices->f('seller_id')."',
                seller_name                 = '".addslashes($const['ACCOUNT_COMPANY'])."',
                seller_d_address            = '".addslashes(utf8_decode($const['ACCOUNT_DELIVERY_ADDRESS']))."',
                seller_d_zip                = '".$const['ACCOUNT_DELIVERY_ZIP']."',
                seller_d_city               = '".addslashes($const['ACCOUNT_DELIVERY_CITY'])."',
                seller_d_country_id         = '".$const['ACCOUNT_DELIVERY_COUNTRY_ID']."',
                seller_d_state_id           = '".addslashes($const['ACCOUNT_DELIVERY_STATE_ID'])."',
                seller_b_address            = '".addslashes(utf8_encode($const['ACCOUNT_BILLING_ADDRESS']))."',
                seller_b_zip                = '".$const['ACCOUNT_BILLING_ZIP']."',
                seller_b_city               = '".addslashes($const['ACCOUNT_BILLING_CITY'])."',
                seller_b_country_id         = '".$const['ACCOUNT_BILLING_COUNTRY_ID']."',
                seller_b_state_id           = '".addslashes($const['ACCOUNT_BILLING_STATE_ID'])."',
                seller_bwt_nr               = '".$buyer_details->f('btw_nr')."',
                created                     = '".$created_date."',
                created_by                  = 'System',
                last_upd                    = '".$created_date."',
                contact_id                  = '".$rec_invoices->f('contact_id')."',
                currency_rate               = '".$rec_invoices->f('currency_rate')."',
                apply_discount              = '".$rec_invoices->f('apply_discount')."',
                email_language              = '".$email_lg."',
                identity_id                 = '".$rec_invoices->f('identity_id')."',
                mandate_id                  = '".$rec_invoices->f('mandate_id')."',
                same_address                = '".$rec_invoices->f('same_address')."',
                free_field                  = '".addslashes($free_field)."',
                main_address_id             = '".$rec_invoices->f('main_address_id')."',
                last_upd_by                 = '".$rec_invoices->f('last_upd_by')."',
                our_ref                     = '".addslashes($rec_invoices->f('our_ref'))."',
                your_ref                    = '".addslashes($rec_invoices->f('your_ref'))."',
                acc_manager_id              = '".addslashes($rec_invoices->f('acc_manager_id'))."',
                vat_regime_id              = '".addslashes($rec_invoices->f('vat_regime_id'))."',
                source_id                   = '".$rec_invoices->f('source_id')."',
                type_id                   = '".$rec_invoices->f('type_id')."',
                segment_id                  = '".$rec_invoices->f('segment_id')."',
                payment_method              = '".$rec_invoices->f('payment_method')."'
                 ");
      
                //copy invoice line]
            $tracking_data=array(
                  'target_id'         => $invoice_id,
                  'target_type'       => '1',
                  'target_buyer_id'   => $rec_invoices->f('buyer_id'),
                   'lines'            => array()
                   );
            addTracking($tracking_data,0,$database);
            if($DRAFT_INVOICE_NO_NUMBER!='1'){
                $ogm = generate_ogm($database,$invoice_id);
                $db_user_new->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$invoice_id."' ");
            }

            $buyer_name = $buyer_details->f('name').' '.$buyer_details->f('l_name');

            $invoice_note_data = $db_user_new->query("SELECT item_value, lang_id, active FROM note_fields WHERE (item_type = 'r_invoice' OR item_type = 'recurring_invoice') AND item_id = '".$rec_invoices->f('recurring_invoice_id')."' ");
            while($invoice_note_data->next()){
                $db_user_new->query("INSERT INTO note_fields SET
                item_id                     = '".$invoice_id."',
                item_type                   = 'invoice',
                item_name                   = 'notes',
                item_value                  = '".addslashes($invoice_note_data->f('item_value'))."',
                version_id                  = '0',
                active                      = '".$invoice_note_data->f('active')."',
                lang_id                     = '".$invoice_note_data->f('lang_id')."'
                ");
            }
            $discount = $rec_invoices->f('discount');
            if($rec_invoices->f('apply_discount') < 2){
                $discount = 0;
            }
            $rec_line=$db_user_new->query("SELECT recurring_invoice_line.*
                FROM  recurring_invoice_line
                WHERE recurring_invoice_line.recurring_invoice_id='".$rec_invoices->f('recurring_invoice_id')."' ");
            //$total = 0;
            $invoice_total=0;
            $invoice_total_vat = 0 ;
            while($rec_line->move_next()){
                $line_total = $rec_line->f('quantity')*$rec_line->f('price');
                $line_discount = $rec_line->f('discount');
                if($rec_invoices->f('apply_discount') == 0 || $rec_invoices->f('apply_discount') == 2){
                    $line_discount = 0;
                }
                $disc_line = $line_total * $line_discount / 100;
                $discount_vat = ( $line_total - $disc_line ) * ( $discount / 100);
                $invoice_total += $line_total - $disc_line - $discount_vat;
                $invoice_total_vat += ($line_total - $disc_line - $discount_vat) * ($rec_line->f('vat') / 100);
             
                $db_user_new->query("INSERT INTO tblinvoice_line SET
                    name                = '".addslashes($rec_line->f('name'))."',
                    invoice_id          = '".$invoice_id."',
                    quantity            = '".$rec_line->f('quantity')."',
                    price               = '".$rec_line->f('price')."',
                    amount              = '".$rec_line->f('amount')."',
                    f_archived          = '0',
                    created             = '".$created_date."',
                    created_by          = '".$rec_line->f('created_by')."',
                    last_upd            = '".$created_date."',
                    vat                 = '".$rec_line->f('vat')."',
                    discount            = '".$rec_line->f('discount')."',
                    article_id          = '".$rec_line->f('article_id')."',
                    item_code           = '".$rec_line->f('item_code')."',
                    last_upd_by         = '".$rec_line->f('last_upd_by')."',
                    content             = '".$rec_line->f('content')."',
                    content_title       = '".$rec_line->f('content_title')."', 
                    tax_id              = '".$rec_line->f('tax_id')."',
                    tax_for_article_id          = '".$rec_line->f('tax_for_article_id')."' ");
            }
            
            $invoice_total_vat += $invoice_total;
            if($rec_invoices->f('currency_type') != $const['ACCOUNT_CURRENCY_TYPE']){
                if($rec_invoices->f('currency_type')){
                    $invoice_total = $invoice_total*return_value($rec_invoices->f('currency_rate'));
                    $invoice_total_vat = $invoice_total_vat*return_value($rec_invoices->f('currency_rate'));
                }
            }
            $db_user_new->query("UPDATE tblinvoice SET amount='".$invoice_total."', amount_vat='".$invoice_total_vat."' WHERE id='".$invoice_id."' ");
            $db_user_new->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Invoice created by recurring tool{endl}')."', field_name='invoice_id', field_value='".$invoice_id."', date='".time()."', type='0', reminder_date='".time()."' ");

              // send email
            $status_email=false;
            $sent_to='';
            // changed for task AKTI 3929
           /* if($rec_invoices->f('invoice_delivery')){
                if($DRAFT_INVOICE_NO_NUMBER=='1'){
                    $serial_number=generate_invoice_number($database);
                    $db_user_new->query("UPDATE tblinvoice SET serial_number = '".addslashes($serial_number)."' WHERE id='".$invoice_id."' ");
                    $db_user_new->query("UPDATE tblinvoice SET ogm = '".generate_ogm($database,$invoice_id)."' WHERE id='".$invoice_id."' ");
                }
                $attach = generatePdfFtomPim($database,$invoice_id);
                if($rec_invoices->f('include_xml')){
                    generateXMLtomPim($database,$invoice_id);
                }
                $sent_to=send_invoice_email($rec_invoices->f('recurring_invoice_id'),$rec_invoices->f('lid'),$invoice_id,$database,$attach);
                $status_email=true;
                
            }else{*/
                $attach = generatePdfFtomPim($database,$invoice_id);
            //}
            $created_rec_inv[] = array('buyer_name'   => $buyer_name,
                                       'invoice_id'   => $invoice_id,
                                       'serial_number'=> $serial_number,
                                       'status'     => $status_email,
                                       'emails'     => $sent_to);
            $acc_comp=$db_user_new->field("SELECT value FROM settings WHERE `constant_name`= 'ACCOUNT_COMPANY' ");
            $g_inv[$database][]=array('buyer_name'   => $acc_comp,
                                                            'invoice_id'   => $invoice_id,
                                                            'serial_number'=> $serial_number);
        }
        $in['invoice_id']=$invoice_id;
     }

    function push_invoice_to_stripe($invoice_id, $database, $from_sub, $auto){
    global $config, $database_config;

       $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $db_u= new sqldb($db_config);

        $result=array();

        ark::loadLibraries(array('stripe/init'));

        $db_conf = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );
        $db_user_new= new sqldb($db_conf);

        $invoice = $db_user_new->query("SELECT * FROM tblinvoice WHERE id='".$invoice_id."' ");
        $invoice_lines = $db_user_new->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$invoice_id."' ");
        $contact_id =0;
        $customer_id=0;
        if($database == '04ed5b47_e424_5dd4_ad024bcf2e1d' ){
            $stripe_api_key = $config['stripe_api_key'];
        }else{
            $stripe_api_key = $db_user_new->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
            //$stripe_api_key = 'sk_test_frrSZiyO3TUXxh7jU00pK89100Ns1shFw4';
        }

        if(!$from_sub){
            //push to stripe for regular users, or to AKTI account from cron, when auto is true

            $customer_id = $invoice ->f('buyer_id');
            if($auto){
                //cron invoice
                $message = '{l}Invoice was pushed to Stripe by automated action on periodic invoice {endl}';
            }else{
                $user_name = get_user_name($_SESSION['u_id']);
                $message = '{l}Invoice was pushed to Stripe by{endl} '.$user_name;
            }
        }else{
            //push to stripe from subscription: database is 04ed
            //$auto is true

            $contact_id = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");

            if($contact_id){
                $customer_id = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$contact_id."' AND `primary`='1' ");
            }
            if(!$customer_id){
                $customer_id = $db_user_new->field("SELECT customer_id FROM customer_field WHERE value='".$_SESSION['u_id']."' AND field_id='7' ");
            }
            $message = '{l}Invoice was pushed to Stripe by automated action on periodic invoice {endl}';
        }

        $new_customer_stripe =0;
        $stripe_cust_id = 0;

        if($customer_id){
            $stripe_cust_id = $db_user_new->field("SELECT stripe_cust_id FROM customers WHERE customer_id='".$customer_id."' ");
            $customer_name = $db_user_new->field("SELECT name FROM customers WHERE customer_id='".$customer_id."' ");
            $customer_email = $db_user_new->field("SELECT c_email FROM customers WHERE customer_id='".$customer_id."' ");

            if(!$stripe_cust_id && $database == '04ed5b47_e424_5dd4_ad024bcf2e1d'){
                //for AKTI account, search first if it's not filled in the custom field from customers
                $stripe_cust_id = $db_user_new->field("SELECT value FROM customer_field WHERE customer_id='".$customer_id."' AND field_id='1' ");
                //for AKTI account, search  if it's not filled in the users table/database
                if(!$stripe_cust_id){
                    $stripe_cust_id = $db_u->field("SELECT stripe_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
                }
                /*msg::error("Stripe customer ID is not filled in for this customer","error");
                return false;*/
            }
        }else {

            $result['error']= gm("Buyer ID is not filled in for this invoice");
            return $result;
        }       

        \Stripe\Stripe::setApiKey($stripe_api_key); 

        if(!$stripe_cust_id){
                 
                try{
                $new_cust=\Stripe\Customer::create(array(
                  "description" => $customer_name,
                  "email"=> $customer_email,       
                ));
                }catch(Exception $e){
                   $result['error']=$e->getMessage();
                    return $result;
                }

                if($new_cust['id']){
                    $db_user_new->query("UPDATE customers SET stripe_cust_id='".$new_cust['id']."' WHERE customer_id='".$invoice->f('buyer_id')."'");
                    if($database == '04ed5b47_e424_5dd4_ad024bcf2e1d' ){
                        $db_user_new->query("INSERT INTO customer_field SET
                                                        field_id='1',
                                                        customer_id='".$customer_id."',
                                                        value='".$new_cust['id']."'");
                    }
                    $stripe_cust_id = $new_cust['id'];
                    $new_customer_stripe =1;
                }   
        }

        $collection_method="charge_automatically";
        $due_date =null;

        if($new_customer_stripe){
            $collection_method="send_invoice";
            $due_date =$invoice->f('due_date');
        }

        if($stripe_cust_id && $stripe_api_key){
                
             if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 1){
                    $global_discount = 0;
                }else{
                    $global_discount = $invoice->f('discount');
                }       

            while($invoice_lines->move_next()){
                $currency = $db_user_new->field("SELECT code FROM currency WHERE id='".$invoice->f('currency_type')."' ");
                if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
                    $line_discount = 0;
                }else{
                    $line_discount = $invoice_lines->f('discount');
                }


                $price = $invoice_lines->f('price') - $invoice_lines->f('price') * $line_discount/100;
                $price = $price - $price * $global_discount/100;
                $unit_amount = $price + ( $price * $invoice_lines->f('vat')/100 );
                $amount = $invoice_lines->f('quantity') * $unit_amount;
                try{
                    $inv_item = \Stripe\InvoiceItem::create([
                        "customer" => $stripe_cust_id,
                        //"amount" => $amount,
                        "quantity" => $invoice_lines->f('quantity'),
                        "unit_amount" => round($unit_amount,2)*100,
                        "currency" => $currency,
                        "description" => $invoice_lines->f('name'),
                    ]);

                }catch(Exception $e){
                   $result['error']=$e->getMessage();
                    return $result;
                }
                
            }


           try{
                $inv = \Stripe\Invoice::create([
                    "customer" => $stripe_cust_id,
                    "collection_method" => $collection_method,
                    "auto_advance" => true,
                    "due_date" => $due_date,
                ]); 
           }catch(Exception $e){
                    $result['error']=$e->getMessage();
                    return $result;
           }

           if($inv['id']){
                    $db_user_new->query("UPDATE tblinvoice SET stripe_invoice_id = '".$inv['id']."', our_ref = '".$inv['id']."' WHERE id='".$invoice_id."' ");
                    insert_message_log('invoice',$message,'invoice_id',$invoice_id);
                    $result['success']=1;
                }
            return $result;
        }
 
        return $result;
    }

    function get_aditional(){
            $aditional=array(
            array(
                'txt'       => gm('CRM'),
                'price'     => 20,
                'model'     => 'crm_adv',
                'crm_adv'   => false,
                'credential'=> 0,
                'akti_code' => ''
                ),
            array(
                'txt'       => gm('Products'),
                'price'     => 10,
                'model'     => 'products',
                'products'  => false,
                'credential'=> 0,
                'akti_code' => ''
                ),
            array(
                'txt'       => gm('Quotes'),
                'price'     => 10,
                'model'     => 'quotes_adv',
                'quotes_adv'=>false,
                'credential'=> 0,
                'akti_code' => ''
                ),
            array(
                'txt'       => gm('Orders'),
                'price'     => 10,
                'model'     => 'orders',
                'orders'    => false,
                'credential'=> 6,
                'akti_code' => 'AKTI-009'
                ),
            array(
                'txt'       => gm('Stocks'),
                'price'     => 30,
                'model'     => 'stocks',
                'stocks'    => false,
                'credential'=> 16,
                'akti_code' => ''
                ),
            array(
                'txt'       => gm('Cashregister'),
                'price'     => 50,
                'model'     => 'cashregister',
                'cashregister'  => false,
                'credential'=> 15,
                'akti_code' => 'INT-001'
                ),
            array(
                'txt'       => gm('Projects'),
                'price'     => 20,
                'model'     => 'projects',
                'projects'  => false,
                'credential'=> 3,
                'akti_code' => 'AKTI-011'
                ),
            array(
                'txt'       => gm('Interventions'),
                'price'     => 20,
                'model'     => 'intervention',
                'intervention'=>false,
                'credential'=> 13,
                'akti_code' => 'AKTI-012'
                ),
            array(
                'txt'       => gm('Contrats'),
                'price'     => 20,
                'model'     => 'contracts',
                'contracts' => false,
                'credential'=> 11,
                'akti_code' => 'AKTI-013'
                ),
            array(
                'txt'       => gm('SEPA'),
                'price'     => 10,
                'model'     => 'sepa',
                'sepa'      => false,
                'credential'=> 0,
                'akti_code' => 'AKTI-014'
                ),
            array(
                'txt'       => gm('Reports'),
                'price'     => 10,
                'model'     => 'report_adv',
                'report_adv'=> false,
                'credential'=> 0,
                'akti_code' => ''
                ),
            array(
                'txt'       => gm('API and webhooks'),
                'price'     => 0,
                'model'     => 'api_webhooks',
                'api_webhooks'=> false,
                'credential'=> 0,
                'akti_code' => 'AKTI-015'
                ),
            array(
                'txt'       => gm('Market place'),
                'price'     => 0,
                'model'     => 'marketplace',
                'marketplace'=> false,
                'credential'=> 0,
                'akti_code' => 'AKTI-016'
                ),
            array(
                'txt'       => gm('Purchase Orders'),
                'price'     => 0,
                'model'     => 'p_orders',
                'p_orders'    => false,
                'credential'=> 14,
                'akti_code' => ''
                ),
            array(
                'txt'       => gm('Barcode scanning'),
                'price'     => 0,
                'model'     => 'barcode_scan',
                'barcode_scan'    => false,
                'credential'=> 0,
                'akti_code' => ''
                )
             );
            return $aditional;
    }

    function get_user_color($user_id)
{  
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    
    $user_color = $db_users->field("SELECT user_info.color FROM user_info WHERE user_info.user_id= :user_id ",['user_id'=>$user_id]);
    if(!$user_color){
        $color = '#';
        $colorHexLighter = array("9","A","B","C","D","E","F" );
        for($x=0; $x < 6; $x++){
            $color .= $colorHexLighter[array_rand($colorHexLighter, 1)]  ;
        }
        $user_color = substr($color, 0, 7);
        $db_users->query("UPDATE user_info SET color='".$user_color."' WHERE user_info.user_id= :user_id ",['user_id'=>$user_id]);
    }

    return $user_color;
}

function check_vat_regime($vat_regime_id){
    $db = new sqldb();

    $exists = $db->field("SELECT id FROM vat_new WHERE id='".$vat_regime_id."'");
    if($exists){
        return true;
    }

    return false;
}
function generate_img_with_cid($var,$cid){
    $img=$var;
    $src=strpos($var,"src=");
    if($src !== false){
        $left_part=substr($var,0,$src);
        $end_src=strpos($var,'"',$src+8);
        if($end_src !== false){
            $right_part=substr($var,$end_src+1,strlen($var)-$end_src);
            if(!is_null($left_part) && !is_null($right_part)){
                $img=$left_part.' src="cid:sign_'.$cid.'" '.$right_part;
            }
        }
    }
    return $img;
}
function customerInvoicingRecurringInvoiceUpdate($data){
    if($data['db']){
        global $database_config;
        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $data['db'],
        );
        $db = new sqldb($db_config);
    }else{
        $db = new sqldb();
    }
    if($data['customer_id']){
        $db->query("DELETE FROM recurring_email_contact WHERE recurring_invoice_id IN (SELECT recurring_invoice_id FROM recurring_invoice WHERE buyer_id='".$data['customer_id']."')");
        $c_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$data['customer_id']."' ");
        $invoice_email_type = $db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$data['customer_id']."' "); 
        $matched_recurring=$db->query("SELECT * FROM recurring_invoice WHERE buyer_id='".$data['customer_id']."'");
        while($matched_recurring->next()){
            if($invoice_email_type=='1'){
                if($matched_recurring->f('contact_id')){
                    $contacts = $db->query("SELECT customer_contactsIds.email,customer_contacts.firstname,customer_contacts.lastname, customers.name
                                    FROM customer_contacts
                                    INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
                                    LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
                                    WHERE customer_contactsIds.customer_id='".$data['customer_id']."' AND customer_contactsIds.contact_id='".$matched_recurring->f('contact_id')."' ");
                    while($contacts->next()){
                        if($contacts->f('email')){  
                            $db->query("INSERT INTO recurring_email_contact SET
                                        recurring_invoice_id   =   '".$matched_recurring->f('recurring_invoice_id')."',
                                        email         =   '".$contacts->f('email')."' ");
                        }
                    }
                }
            }else if($invoice_email_type == '2'){
                $c_email = str_replace(' ', '', $c_email);
                $email_arr = array();
                $email_arr = explode(";", $c_email);
                foreach($email_arr as $key=> $value){
                    $db->query("INSERT INTO recurring_email_contact SET
                                        recurring_invoice_id   =   '".$matched_recurring->f('recurring_invoice_id')."',
                                        email         =   '".$value."' ");
                }
            }
        }
    }
}
function get_export_auto_invoice_options(){
    return array(
        array('id'=>'0','name'=>gm('Manual')),
        array('id'=>'2','name'=>gm('Immediately')),
        array('id'=>'3','name'=>gm('Daily')),
        array('id'=>'1','name'=>gm('Once a week'))
    );
}

function get_export_purchase_invoice_options(){
    return array(
        array('id'=>'0','name'=>gm('Disabled')),
        array('id'=>'2','name'=>gm('Manual Export')),
        array('id'=>'1','name'=>gm('Automatic Import'))
    );
}

function get_quantity_component($parent_article_id, $component_id){
     $db = new sqldb();
    $quantity_component=0; 
    if($parent_article_id && $component_id){
          $quantity_component = $db->field("SELECT quantity FROM pim_articles_combined WHERE parent_article_id='".$parent_article_id."' AND article_id='".$component_id."'");  
    }

    return $quantity_component;
}

function get_subscription_plan($user_id){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);

    $main_user_id = $db_users->field("SELECT main_user_id FROM users WHERE users.user_id= :user_id ",['user_id'=>$user_id]);

    if($main_user_id){
        $pricing_plan_id = $db_users->field("SELECT base_plan_id FROM users WHERE users.user_id= :user_id ",['user_id'=>$main_user_id]);
    }else{
        $pricing_plan_id = $db_users->field("SELECT base_plan_id FROM users WHERE users.user_id= :user_id ",['user_id'=>$user_id]);
    }
          
    return $pricing_plan_id;

}
//retrieve address data from an input
function retrieve_address_data($input){
    $data=array('address'=>'','zip'=>'','city'=>'','country'=>'');
    if(empty($input)){
        return $data;
    }
    $explodeAr=explode("\n",$input);
    if(empty($explodeAr) || count($explodeAr)<3){
        return $data;
    }
    //first line should be the address
    $data['address']=trim($explodeAr[0]);  
    //second line should be zip and city
    $zipCity=explode(" ",trim($explodeAr[1]));
    if(count($zipCity)){
        switch(count($zipCity)){
            case 1:
                //either zip or city
                if(is_numeric(trim($zipCity[0]))){
                    $data['zip']=trim($zipCity[0]);
                }else if(trim($zipCity[0])){
                    $data['city']=trim($zipCity[0]);
                }
                break;
            case 2:
                //both zip and city
                if(trim($zipCity[0])){
                    $data['zip']=trim($zipCity[0]);   
                }
                if(trim($zipCity[1])){
                    $data['city']=trim($zipCity[1]);
                }         
            default:
                //first is zip and rest is city
                $data['zip']=trim($zipCity[0]);
                $data['city']=trim(str_replace($zipCity[0], "", trim($explodeAr[1])));
        }
    }  
    //third line should be the country
    $data['country']=trim($explodeAr[2]);
    return $data;
}


function check_facq_reference($article_code)
{  
    global $database_config;
    
    if(strlen($article_code)=='6' && ctype_digit($article_code)){
        return true;
    }else{
        return false;
    }

}

function get_yuki_projects(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT yuki_project_id, description FROM yuki_projects ORDER BY description ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('yuki_project_id'),'name'=>stripslashes($inf->f('description'))));
    }
    return $res;
}

function get_yuki_project_name($yuki_project_id){
    $db=new sqldb();
    $yuki_project_name ='';
    if($yuki_project_id){
        $yuki_project_name = $db->field("SELECT description FROM yuki_projects WHERE yuki_project_id='".$yuki_project_id."' ");
    }  
    return $yuki_project_name;
}
function get_yuki_project_code($yuki_project_id){
    $db=new sqldb();
    $yuki_project_code ='';
    if($yuki_project_id){
        $yuki_project_code = $db->field("SELECT yuki_code FROM yuki_projects WHERE yuki_project_id='".$yuki_project_id."' ");
    }  
    return $yuki_project_code;
}

function get_location_name($address_id){
    $db=new sqldb();
    $location_name ='';
    if($address_id){
        $location_name = $db->field("SELECT naming FROM dispatch_stock_address WHERE address_id='".$address_id."' ");
    }  
    return $location_name;
}

function get_article_photo($article_id){
    $db=new sqldb();
    global $config;
    $link ='';
    $image_size = 0;
    if($article_id){
       $file = $db->query("SELECT file_name, folder, upload_amazon, amazon_path, photo_id FROM pim_article_photo WHERE parent_id='".$article_id."' ORDER BY pim_article_photo.photo_id ASC LIMIT 1");

       while($file->next()){
            if($file->f('upload_amazon')){
                $image_size = getimagesize($file);
                if($image_size){
                    return $file->f('file_name');
                }else{
                    ark::loadLibraries(array('aws'));
                    $a = new awsWrap(DATABASE_NAME);
                    $link =  $a->getLink($file->f('amazon_path'));
                    if($link){
                        $db->query("UPDATE pim_article_photo SET file_name='".$link."' WHERE photo_id='".$file->f('photo_id')."' ");
                        $db->query("UPDATE pim_articles SET article_photo='".$link."' WHERE article_id ='".$article_id."' ");
                    }
                    return $link;
                }

            }else{
                $image = ltrim($file->f('folder').'/'.$file->f('file_name'),'../');
                return $image;
            }
            
        }

    }  
    return $link;
}

function get_contact_photo($contact_id){
    $db=new sqldb();
    global $config;
    $link ='';
    $image_size = 0;
    if($contact_id){
       $file = $db->query("SELECT file_name, folder, upload_amazon, amazon_path, photo_id FROM pim_contact_photo WHERE parent_id='".$contact_id."' ORDER BY pim_contact_photo.photo_id ASC LIMIT 1");
       $image = ltrim($file->f('folder').'/'.$file->f('file_name'),'../');
       return $image;
       while($file->next()){
            if($file->f('upload_amazon')){
                $image_size = getimagesize($file);
                if($image_size){
                    return $file->f('file_name');
                }else{
                    ark::loadLibraries(array('aws'));
                    $a = new awsWrap(DATABASE_NAME);
                    $link =  $a->getLink($file->f('amazon_path'));
                    if($link){
                        $db->query("UPDATE pim_contact_photo SET file_name='".$link."' WHERE photo_id='".$file->f('photo_id')."' ");
                    }
                    return $link;
                }

            }else{
                $image = ltrim($file->f('folder').'/'.$file->f('file_name'),'../');
                return $image;
            }
            
        }

    }  
    return $link;
}

function get_delivery_addresses($customer_id, $selected_delivery_address_id){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
                                 FROM customer_addresses
                                 LEFT JOIN country ON country.country_id=customer_addresses.country_id
                                 LEFT JOIN state ON state.state_id=customer_addresses.state_id
                                 WHERE customer_addresses.customer_id='".$customer_id."' 
                                 ORDER BY customer_addresses.address_id");
    while($inf->next()){
        array_push($res, array(
                'id'=>$inf->f('address_id'),
                'delivery_address_id'=>$inf->f('address_id'),
                'address'            => $inf->f('address'),
               
                'zip'               => $inf->f('zip'),
                'city'              => $inf->f('city'),
                'state'             => $inf->f('state'),
                'country'           => $inf->f('country'),
                'address_more'      => trim($inf->f('address').' '.$inf->f('zip').' '.$inf->f('city'))
            ));
    }
    return $res;
}

function get_status_order($order_id){
    $db=new sqldb();
    $res=array();
    if(!$order_id){
        return $res;
    }
    $order=$db->query("SELECT * FROM pim_orders WHERE order_id='".$order_id."'"); 

    if($order->f('sent')==0 ){
        $status = gm('Draft');
    }
    if($order->f('invoiced')){
        if($order->f('rdy_invoice') == 1){
            $status = gm('Delivered');
        }else{
            $status = gm('Confirmed');
        }
    }else{
        if($order->f('rdy_invoice') == 2){
            $status = gm('Confirmed');
        }else{
            if($order->f('rdy_invoice') == 1){
                $status = gm('Delivered');
            }else{
                if($order->f('sent') == 1){
                    $status = gm('Confirmed');
                }else{
                    $status = gm('Draft');
                }
            }
        }
    }

    if(!$order->f('active')){
        $status = gm('Archived');
    }

    $is_delivery_planned=false;
   if(ORDER_DELIVERY_STEPS == '2'){
        $is_partial_deliver = false;
        if($order->f('rdy_invoice')==2){
            $dels=$db->query("SELECT * FROM pim_order_deliveries WHERE order_id='".$order->f('order_id')."'");
            while($dels->next()){
                //daca cel putin una din livrarile partiale sunt confirmate, atunci is_partial_deliver = true
                if($dels->f('delivery_done')=='1'){
                    $is_partial_deliver = true;
                    $is_delivery_planned=false;
                    break;
                }else{
                    $is_delivery_planned=true;
                }
            }
        }
        

    }else{
         $is_partial_deliver= $order->f('rdy_invoice')==2?true:false;
    }
    
    if($is_partial_deliver && !$is_delivery_planned){
        $status = gm('Partially Delivered');
    }else if($is_delivery_planned){
        $status = gm('Delivery planned');
    }

    $res['status'] = $status;
    $res['color_status'] = $order->f('rdy_invoice') ? ($order->f('rdy_invoice')==1 ? 'green' : 'yellow') : ($order->f('sent') == 1 ? 'yellow' : 'gray');

    return $res;
}

function get_status_p_order($p_order_id){
    $db=new sqldb();
    $res=array();
    if(!$p_order_id){
        return $res;
    }
    $p_order=$db->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$p_order_id."'"); 

    if($p_order->f('sent')==0 ){
        $status = gm('Draft');
    }
    if($p_order->f('invoiced')){
        if($p_order->f('rdy_invoice') == 1){
            $status = gm('Received');
        }else{
            $status = gm('Final');
        }
    }else{
        if($p_order->f('rdy_invoice') == 2){
            $status = gm('Final');
        }else{
            if($p_order->f('rdy_invoice') == 1){
                $status = gm('Received');
            }else{
                if($p_order->f('sent') == 1){
                    $status = gm('Final');
                }else{
                    $status = gm('Draft');
                }
            }
        }
    }

    if(!$p_order->f('active')){
        $status = gm('Archived');
    }

    $is_p_delivery_planned=false;
    if(P_ORDER_DELIVERY_STEPS==2){
        if($p_order->f('rdy_invoice') == 2){
            $dels_p=$db->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id = '".$p_order->f('p_order_id')."' ");
            while($dels_p->next()){
                if(!$dels_p->f('delivery_done')){
                    $is_p_delivery_planned=true;
                }
            }
        }
    }
    if($is_p_delivery_planned){
        $status=gm('Delivery planned');
    }

    $res['status'] = $status;
    $res['color_status'] = $p_order->f('rdy_invoice') ? ($p_order->f('rdy_invoice')==1 ? 'green' : 'yellow') : 'gray';

    return $res;
}

function get_status_quote($version_id){
    $db=new sqldb();
    $res=array();
    if(!$version_id){
        return $res;
    }
    $quote=$db->query("SELECT * FROM tblquote_version WHERE version_id='".$version_id."'"); 

    $opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
    $opts_color = array('gray','red','green','green','green','yellow');
    $field = 'status_customer';
    
    $res[strtolower($field)]=$opts[$quote->f('version_status_customer')];
    $res['color_status']=$opts_color[$quote->f('version_status_customer')];
    
    if($quote->f('sent') == 1 && $quote->f('version_status_customer') == 0){
        $res['status_customer']=$opts[5];
        $res['color_status']='yellow';
    }

    if($quote->f('f_archived')=='1'){
        $res['status_customer']=gm('Archived');
        $res['color_status']='gray';
    }

    $res['status'] =  $res['status_customer'];

    return $res;
}

function get_status_invoice($invoice_id){
    $db=new sqldb();
    $res=array();
    if(!$invoice_id){
        return $res;
    }
    $invoice=$db->query("SELECT * FROM tblinvoice WHERE id='".$invoice_id."'"); 

     if($invoice->f('sent') == '0' ){
        $res['status'] = gm('Draft');
        $res['color_status']='gray';

    }else if($invoice->f('status')=='1' || ($invoice->f('status')=='0' && $invoice->f('paid')=='2' ) ){
        $res['status'] = $invoice->f('paid')=='2' ? gm('Partially Paid') : gm('Paid');
        $res['color_status']='green';
    }else if($invoice->f('sent')!='0' && $invoice->f('not_paid')=='0'){
        $res['status'] = $invoice->f('status')=='0' && $invoice->f('due_date')< time() && $invoice->f('type')!='2' && $invoice->f('type')!='4'? gm('Late') : gm('Final');
        $res['color_status']=$invoice->f('status')=='0' && $invoice->f('due_date')< time() && $invoice->f('type')!='2' && $invoice->f('type')!='4' ? 'red' : 'yellow';
    } else if($invoice->f('sent')!='0' && $invoice->f('not_paid')=='1'){
        $res['status'] = gm('No Payment');
        $res['color_status']='yellow';
    }

    if($invoice->f('f_archived')=='1'){
        $res['status'] = gm('Archived');
        $res['color_status']='gray';
    }

    return $res;
}

function get_status_project($project_id){
    $db=new sqldb();
    $res=array();
    if(!$project_id){
        return $res;
    }
    $project=$db->query("SELECT * FROM projects WHERE project_id='".$project_id."'"); 

    switch ($project->f('stage')) {
        case '1':
            $res['status'] = gm('Active');
            $res['color_status'] = 'green';
            break;
        case '2':
            $res['status'] = gm('Closed');
            $res['color_status'] = 'gray';
            break;
        default:
            $res['status'] = gm('Draft');
            $res['color_status'] = 'gray';
            break;
    }

    return $res;
}

function get_status_intervention($intervention_id){
    $db=new sqldb();
    $res=array();
    if(!$intervention_id){
        return $res;
    }
    $intervention=$db->query("SELECT * FROM servicing_support WHERE service_id='".$intervention_id."'"); 

    if($intervention->f('status')=='1'){
        $res['status'] = gm('Planned');
        $res['color_status'] = 'yellow';
    }elseif($intervention->f('status')=='2'){
        if($intervention->f('is_recurring')=='0'&&$intervention->f('active')=='1'&&$intervention->f('accept')=='1'){
            $res['status'] = gm('Accepted');
        }else{
            $res['status'] = gm('Closed');
        }
        $res['color_status'] = 'green';
    }else{
        $res['status'] = gm('Draft');
        $res['color_status'] = 'grey';
    }

    return $res;
}

function get_status_contract($contract_id){
    $db=new sqldb();
    $res=array();
    if(!$contract_id){
        return $res;
    }
    $contracts=$db->query("SELECT * FROM contracts WHERE contract_id='".$contract_id."'"); 

    if(!$contracts->f('sent')){
            $res['status']=gm('Draft');
        }else{
            if($contracts->f('status_customer')==0){
                $res['status']=gm('Sent');
            }else if($contracts->f('status_customer')==1){
                $res['status']=gm('Rejected');
            }else if($contracts->f('status_customer')==2){
                $res['status']=gm('Active');
            }else{
                $res['status']=gm('Closed');
            }
        }
        if($contracts->f('closed')==1){
            $res['status']=gm('Closed');
        }

        if($contracts->f('sent')=='1' && $contracts->f('status_customer')=='0'){
            $res['color_status']='orange';
        }else if($contracts->f('sent')=='1' && $contracts->f('status_customer')=='2'){
            $res['color_status']='green';
        }else if($contracts->f('status_customer')=='3'){
            $res['color_status']='yellow';
        }else if($contracts->f('sent')=='1' && $contracts->f('status_customer')=='1'){
            $res['color_status']='red';
        }else{
            $res['color_status']='gray';
        }

    return $res;
}

function get_status_purchase_invoice($invoice_id){
    $db=new sqldb();
    $res=array();
    if(!$invoice_id){
        return $res;
    }
    $invoice=$db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$invoice_id."'"); 

     switch($invoice->f('status')){
        case '0':
            $obj['color_status']=$invoice->f('paid') ? 'green' : 'gray';
            $obj['status']=$invoice->f('paid') ? gm('Paid') : gm('Uploaded');
            break;
        case '1':
            $obj['color_status']=$invoice->f('paid') ? 'green' : 'yellow';
            $obj['status']=$invoice->f('paid') ? gm('Paid') : gm('Approved');
            break;
        default:
            $obj['status']='gray';
            $obj['status']=gm('Uploaded');
            break;
    }

    return $res;
}

function column_selected($needle, $haystack) {
    foreach($haystack as $key =>$element) {
        if($element['column_name'] == $needle){
            return true;
        }
    }
    return false;
}

function build_cert_language_dd($selected,$active){

    $db = new sqldb();
    $db2 = new sqldb();
    $array = array();
    $filter="1=1";
    if($active){
        $filter.=" and active=1";
    }
    
    $db->query("SELECT * FROM pim_lang where  ".$filter." ORDER BY sort_order ");
    while($db->move_next()){
        $code= $db->f('code');
        $filter_code = " lang_code='".$code."'";
        if($code =='du' || $code == 'nl'){
            $filter_code=  " lang_code='du' OR lang_code='nl' ";
        }
        $db2->query("SELECT * FROM label_language_certificate WHERE $filter_code");
        if($db2->move_next())
        {
            $array[$db2->f('label_language_id')] = $db->f('language');
        }
    }
    return build_simple_dropdown($array,$selected);

}

function build_deliveries_dd($order, $selected){

    $db = new sqldb();
    
    $array = array();
        
    $db->query("SELECT * FROM pim_order_deliveries WHERE order_id='".$order."' ORDER BY delivery_id DESC");
    while($db->move_next()){
        $array[$db->f('delivery_id')] = date(ACCOUNT_DATE_FORMAT,$db->gf('date'));
    }
    return build_simple_dropdown($array, $selected);

}

function mark_invoices_as_exported($app, $export_purchase_inv = false){
    $db = new sqldb();
        if($app){
            $invoices=$db->query("SELECT tblinvoice.id FROM tblinvoice WHERE tblinvoice.sent=1 AND (tblinvoice.type='0' OR tblinvoice.type='2' OR tblinvoice.type='3' OR tblinvoice.type='4') AND tblinvoice.f_archived='0' ");
            while($invoices->next()){
                $exist =$db->field("SELECT invoice_id FROM tblinvoice_exported WHERE tblinvoice_exported.invoice_id='".$invoices->f('id')."' AND  tblinvoice_exported.type='0' AND ".$app."=1 ");
                if(!$exist){
                    $db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$invoices->f('id')."', type='0', ".$app."=1 ");
                }
                
            }

            if($export_purchase_inv){
                $p_invoices=$db->query("SELECT tblinvoice_incomming.invoice_id FROM tblinvoice_incomming WHERE tblinvoice_incomming.f_archived='0' ");
                while($p_invoices->next()){
                    $exist =$db->field("SELECT invoice_id FROM tblinvoice_exported WHERE tblinvoice_exported.invoice_id='".$p_invoices->f('invoice_id')."' AND  tblinvoice_exported.type='1' AND ".$app."=1 ");
                    if(!$exist){
                        $db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$p_invoices->f('invoice_id')."', type='1', ".$app."=1 ");
                    }
                    
                }
            }

            msg::success(gm('Invoices successfully exported'),'success');
        }
        return true;

    }
function build_color_ball_dd(){
    $array=array();

    $color_data=['D50000','F4511E','33B679','039BE5','F6BF26','322B78'];

    foreach($color_data as $color){
        array_push($array, array('id'=>'#'.$color,'name'=>'#'.$color));
    }
    return $array;
}
function get_payment_term_type_dd(){
    $array=array();

    $input_data=array('1'=>gm('Payment term(days) from the invoice date'),'2'=>gm('Payment term(days) from the first day of the next month'));

    foreach($input_data as $key=>$value){
        array_push($array, array('id'=>$key,'name'=>$value,'value'=>$value));
    }
    return $array;
}
function build_reminder_dd(){
    return array(
    array('id'=>'1','name'=>gm('None'),'value'=>gm('None')),
        array('id'=>'2','name'=>gm('At event time'),'value'=>gm('At event time')),
        array('id'=>'3','name'=>gm('5 minutes before'),'value'=>gm('5 minutes before')),
        array('id'=>'4','name'=>gm('10 minutes before'),'value'=>gm('10 minutes before')),
        array('id'=>'5','name'=>gm('15 minutes before'),'value'=>gm('15 minutes before')),
        array('id'=>'6','name'=>gm('30 minutes before'),'value'=>gm('30 minutes before')),
        array('id'=>'7','name'=>gm('1 hour before'),'value'=>gm('1 hour before')),
        array('id'=>'8','name'=>gm('1 day before'),'value'=>gm('1 day before'))
    );
}
function ublContactEmail($data){
    if($data['db']){
        global $database_config;
        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $data['db'],
        );
        $db = new sqldb($db_config);
    }else{
        $db = new sqldb();
    }
    $xml_contact_email="";
    $invoice_email_type = $db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$data['customer_id']."' "); 
    if($invoice_email_type == '2'){
        $c_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$data['customer_id']."' ");
        $c_email = str_replace(' ', '', $c_email);
        $email_arr = array();
        $email_arr = explode(";", $c_email);
        if(!empty($email_arr) && trim($email_arr[0])){
            $xml_contact_email=trim($email_arr[0]);
        }
    }
    if(empty($xml_contact_email) && $data['contact_id']){
        $contact_email=$db->field("SELECT email FROM customer_contactsIds WHERE customer_id='".$data['customer_id']."' AND contact_id='".$data['contact_id']."' "); 
        if(trim($contact_email)){
            $xml_contact_email=trim($contact_email);
        }
    }
    if(empty($xml_contact_email)){
        $company_email=$db->field("SELECT c_email FROM customers WHERE customer_id='".$data['customer_id']."' "); 
        if(trim($company_email)){
            $xml_contact_email=trim($company_email);
        }
    }
    return $xml_contact_email;
}
function syncDataToActiveCampaign($db=false,$data=array()){
    if(!$db){
        $db = DATABASE_NAME;
    }
    global $database_config,$config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    $database_2 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $db,
    );
    $db2 = new sqldb($database_2);

    $headers=array('Content-Type: application/json','Api-Token: '.$config['activecampaign_api_key']);

    $ch = curl_init();

    $host_update = 'https://akti.api-us1.com/api/3/accounts';
    $post = array(
        'account' => array(
            'name'  => $data['account_name']
        )
    );
    $post = json_encode($post);

    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_URL, $host_update);
    $result = curl_exec($ch);
    $info = curl_getinfo($ch);

    $result = json_decode($result);
    if($result){
        $dbs->query("INSERT INTO cron SET time=NOW(), db='active_campaign', extra='".addslashes(serialize($result))."' ");
    }

    $is_ok=false;
    $active_campaign_account_id=0;
    $active_campaign_contact_id=0;
    if($info['http_code']>=200 && $info['http_code']<300){
        $is_ok=true;
        $active_campaign_account_id=$result->account->id;
    }
    if($is_ok){
        $host_update = 'https://akti.api-us1.com/api/3/contacts';
        $post = array(
            'contact' => array(
                'email'         => $data['contact_email'],
                'firstName'     => $data['contact_first_name'],
                'lastName'      => $data['contact_last_name'],
                "fieldValues"   => array(
                    array(
                        "field" => "23",
                        "value" => $data['contact_lang']
                    ),
                    array(
                        "field" => "35",
                        "value" => "Yes"
                    )
                )
            )
        );
        $post = json_encode($post);

        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $host_update);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        $result = json_decode($result);
        if($result){
            $dbs->query("INSERT INTO cron SET time=NOW(), db='active_campaign', extra='".addslashes(serialize($result))."' ");
        }
        if($info['http_code']>=200 && $info['http_code']<300){
            $is_ok=true;
            $active_campaign_contact_id=$result->contact->id;
        }else{
            $is_ok=false;
        }
    }
    if($is_ok && $active_campaign_contact_id > 0){
        $host_update = 'https://akti.api-us1.com/api/3/accountContacts';
        $post = array(
            'accountContact' => array(
                'contact'   => $active_campaign_contact_id,
                'account'   => $active_campaign_account_id
            )
        );
        $post = json_encode($post);

        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $host_update);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        $result = json_decode($result);
        if($result){
            $dbs->query("INSERT INTO cron SET time=NOW(), db='active_campaign', extra='".addslashes(serialize($result))."' ");
        }

        $ACTIVECAMPAIGN_VID=$db2->field("SELECT value FROM settings WHERE constant_name='ACTIVECAMPAIGN_VID'");
        if(is_null($ACTIVECAMPAIGN_VID)){
            $db2->query("INSERT INTO settings SET value='".$active_campaign_contact_id."', constant_name='ACTIVECAMPAIGN_VID'");
        }else{
            $db2->query("UPDATE settings SET value='".$active_campaign_contact_id."' WHERE constant_name='ACTIVECAMPAIGN_VID'");
        }
    }
    return true;
}
