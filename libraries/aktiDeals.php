<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class aktiDeals
{

    private static $instance = null;

    private function __construct(){}

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new aktiDeals();
        }
        return self::$instance;
    }

    public function get($param,$id =''){
        if($_SESSION['aktiDeals']){
            if(array_key_exists($param, $_SESSION['aktiDeals']['list']) ) {
                return $_SESSION['aktiDeals']['list'][$param];
            } else {
                return self::$param($id);
            }
        }else{
            return self::$param($id);
        }
    }

    public function setList($list){
        $_SESSION['aktiDeals']=array('list'=>$list);
    }

    private function prev($searched_id){
        $prev = array_search($searched_id,$_SESSION['aktiDeals']['list'])-1;
        return $_SESSION['aktiDeals']['list'][$prev];
    }

    private function next($searched_id){        
        $next = array_search($searched_id,$_SESSION['aktiDeals']['list'])+1;
        return $_SESSION['aktiDeals']['list'][$next];
    }

    private function total_items(){         
        return count($_SESSION['aktiDeals']['list']);
    }

    private function start_item($searched_id){
        return array_search($searched_id,$_SESSION['aktiDeals']['list'])+1;
    }

}


?>