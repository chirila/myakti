<?php
//chflags -R nouchg .
if(!class_exists('Sync')){
class Sync{

	private static $stack = array();

	private static $action = '';

	public static $db = '';

	public static function start($action){

		self::$action = $action;
		self::$stack = database_store::getInstance()->getQueries();
		// console::log('started sync for '.$action );

	}

	public static function cache($query){


		return true;
	}

	public static function end($id = 0,$query=0){
		$exit = database_store::getInstance()->getQueries();
	    $commands = array();


	    if($query){
	    	for($i = 0; $i < count($query); $i++){
	    		array_push($commands, $query[$i]);

	    	}

	    }
		for($i = count(self::$stack); $i < count($exit); $i++){
	    	if(!sqldb::is_write_type($exit[$i]['sql'])){
	    		continue;
	    	}
	    	array_push($commands, $exit[$i]['sql']);
	    }



	    unset($exit, $i);



	    if(empty($commands)){
	    	return;
	    }

    	$sql = new sqldb();
    	if(self::$db){
    		global $database_config;
			$database_2 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => self::$db,
			);

    		$sql = new sqldb($database_2);

    	}


    	$sql->query("INSERT INTO pim_sync SET act = '".self::$action."',
    										    link_id = '".$id."',
    											command = '".base64_encode(serialize($commands))."',
    											created_on = '".time()."'");


    	unset($sql,$commands);
    	console::log('Sync ended for '.self::$action);

	}

	public static function send($url = '', $data = array(),$can_sync = ''){
		if(!CAN_SYNC && $can_sync == ''){
			return false;
		}
		if(empty($url) || empty($data)){
			return false;
		}
		//check to see that it's an url
		if(!filter_var($url, FILTER_VALIDATE_URL)){
			return false;
		}


		$ch = curl_init();
		$postFields = 'do=api-sync&';
		foreach ($data as $key => $value) {
			$postFields .= $key.'='.urlencode($value).'&';
		}
		rtrim($postFields,'&');
                curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X_REQUESTED_WITH: xmlhttprequest'));
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
   //$sql = new sqldb();
//$sql->query("INSERT INTO zintra_sync set query='".addslashes($postFields)."' ");
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
 $info = curl_getinfo($ch);
		curl_close ($ch);

		return $server_output;
	}

	public static function endSend($homeUrl, $api_key = ''){
		$exit = database_store::getInstance()->getQueries();
	    $commands = array();
	    for($i = count(self::$stack); $i < count($exit); $i++){
	    	if(!sqldb::is_write_type($exit[$i]['sql'])){
	    		continue;
	    	}
	    	array_push($commands, $exit[$i]['sql']);
	    }
	    unset($exit, $i);

	    if(empty($commands)){
	    	return;
	    }



	    $data = array(
	    	'command' => base64_encode(serialize($commands)),
	    	'do' => 'api--api-client',
	    	'key' => $api_key
 	    );
	    return self::send($homeUrl, $data);
	}

}
}