<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class aktiTimesheet
{

    private static $navigator_list;

    private static $instance = null;

    private function __construct()
    {

    }

    public static function getInstance()
    {

        if (self::$instance === null) {
            self::$instance = new aktiTimesheet();
        }
        return self::$instance;
    }



public function setList($list){

       self::$navigator_list = $list;
       $_SESSION['aktitimesheets']['list']=$list;
       // var_dump(self::$navigator_list); 
       return self::$navigator_list;
}

public  function get($param,$id){
        //var_dump(self::$navigator_list, $param, $id);exit();
        if($_SESSION['aktitimesheets']){
           if(array_key_exists($param, $_SESSION['aktitimesheets']['list']) ) {

                return $_SESSION['aktitimesheets']['list'][$param];
             } else {
                 return self::$param($id);
             }
          }else{
            return self::$param($id);
          }

    }

private function prev($searched_id){

        $prev = array_search($searched_id,$_SESSION['aktitimesheets']['list'])-1;
        //var_dump(self::$navigator_list, $prev, $searched_id);exit();
         return $_SESSION['aktitimesheets']['list'][$prev];
     }

private function next($searched_id){
        
        $next = array_search($searched_id,$_SESSION['aktitimesheets']['list'])+1;
         return $_SESSION['aktitimesheets']['list'][$next];
     }

private function total_items(){
        
        $total_items = count($_SESSION['aktitimesheets']['list']);
         return $total_items;
     }

private function start_item($searched_id){
        $start_item = array_search($searched_id,$_SESSION['aktitimesheets']['list'])+1;
         return $start_item;
     }


}


?>