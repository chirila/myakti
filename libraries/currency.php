<?php
class currency{
	private static $sql;
	private static $currency = array();

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author your's truly
	 **/
	function __construct(){}

	/**
	 * use this function to return the currency as you want
	 *
	 * @return multiple
	 * string - can return the value or the code
	 * array - return an array (used for example on drop-downs)
	 * @author me
	 **/
	static public function get_currency($selected,$code=''){
		if(empty(self::$currency)){
			self::get();
		}
		if(!$selected){
			$selected = 1;
		}
		switch ($code) {
			case 'code': #code
				return self::$currency[$selected]->code;
				break;
			case 'arrays': #code and value all
				$currency_as_array = array();
				foreach (self::$currency as $key => $value) {
					$currency_as_array[$key] = $value->code.' '.$value->value;
				}
				return $currency_as_array;
				break;
			case 'name': #code and value
				return self::$currency[$selected]->code.' '.self::$currency[$selected]->value;
				break;
			case 'array': #code
				$currency_as_array = array();
				foreach (self::$currency as $key => $value) {
					$currency_as_array[$key] = $value->code;
				}
				return $currency_as_array;
				break;
			case 'entity':
				return self::$currency[$selected]->entity;
				break;
			default: #value
				return self::$currency[$selected]->value;
				break;
		}
		// return self::$currency[$selected]->value;
	}

	static private function get(){
		$db = self::set_sql();
		$db->query("SELECT * FROM currency");
		while ($db->next()) {
			self::$currency[$db->f('id')] = new stdClass();
			self::$currency[$db->f('id')]->code = $db->f('code');
			self::$currency[$db->f('id')]->value = $db->f('value');
			self::$currency[$db->f('id')]->name = $db->f('nume');
			self::$currency[$db->f('id')]->entity = $db->f('entity');
		}
	}

	/**
	 * use your own class to connect to the database
	 *
	 * @return object that contains the connection to the database
	 * @author me
	 **/
	static private function set_sql(){
		return self::$sql = new sqldb();
	}

	static public function getCurrency($from_Currency, $to_Currency, $amount, $separator){
		$amount = urlencode($amount);
	    $from_Currency = urlencode($from_Currency);
	    $to_Currency = urlencode($to_Currency);

	    $url = "https://finance.google.co.uk/bctzjpnsun/converter?a=$amount&from=$from_Currency&to=$to_Currency"; 
		//"https://finance.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

	    $ch = curl_init();
	    $timeout = 0;
	    curl_setopt ($ch, CURLOPT_URL, $url);
	    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

	    curl_setopt ($ch, CURLOPT_USERAGENT,
	                 "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
	    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	    $rawdata = curl_exec($ch);

	    curl_close($ch);
	    $data = explode('bld>', $rawdata);
	    $data = explode($to_Currency, $data[1]);

	  /*  if(!is_numeric(trim($data[0]))){
	    	return '1';
	    }

	    $out = number_format($data[0],4,substr($separator, -1),substr($separator, 0));

	    return $out;*/

	    //varianta noua ar trebui sa fie:

	    global $database_config,$config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);	

	    $now=time();
		$day=date('j',$now);
		$month = date('n',$now);
		$year = date('Y',$now);
		$day_start = mktime(0,0,0,$month,$day,$year);
		$day_end = mktime(23,59,59,$month,$day,$year);
	    $rates_data=array();
		$rates_query=$db_users->query("SELECT * FROM conversion_rates WHERE `code`='".$from_Currency."' AND `date` BETWEEN ".$day_start." AND ".$day_end." LIMIT 1")->getAll();
		if(!empty($rates_query)){
			$decoded_data=json_decode(unserialize($rates_query[0]['response']));
			if(is_object($decoded_data) && $decoded_data->result == 'success'){
				foreach($decoded_data->conversion_rates as $key=>$value){
					$rates_data[$key]=$value;
				}		
			}
		}else{
			$c_rest = new clientREST();	
			$return_data = $c_rest->execRequest("https://prime.exchangerate-api.com/v5/".$config['currency_api_key']."/latest/".$from_Currency,'get',null);
			if($return_data){
				$db_users->query("INSERT INTO conversion_rates SET `code`='".$from_Currency."',`response`='".addslashes(serialize($return_data))."',`date`='".time()."' ");
				$decoded_data=json_decode($return_data);
				if(is_object($decoded_data) && $decoded_data->result == 'success'){
					foreach($decoded_data->conversion_rates as $key=>$value){
						$rates_data[$key]=$value;
					}		
				}
			}
		}

		$currency_rate = array_key_exists($to_Currency, $rates_data) ? $rates_data[$to_Currency] : '1';

		//$out = number_format($currency_rate,4,substr($separator, -1),substr($separator, 0));



		if(!is_numeric(trim($data[0]))){
	    	return '1';
	    }

	    $out = number_format($data[0],4,substr($separator, -1),substr($separator, 0));

	    return $out;

		return $out;
	}
}
?>