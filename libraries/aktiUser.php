<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class aktiUser
{

    public static $db;

    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new aktiUser();
        }
        return $inst;
    }

    /**
     * Private ctor so nobody else can instantiate it
     *
     */
    private function __construct()
    {

        //  return  new sqldb($db_config);

    }

public  function get($param, $get_anyway=false){

      if(!$_SESSION['u_id']){

          return false;
      }

      global $database_config;
        $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
        );

        if(!self::$db){
            self::$db = new sqldb($db_config);
         }

       // if(isset($_SESSION['aktiuser'][$param])){
         if($get_anyway){
            return self::$param();
         }else{
             if($_SESSION['aktiuser']){
             if(array_key_exists($param, $_SESSION['aktiuser']) ) {

                  return $_SESSION['aktiuser'][$param];
               } else {
                   return self::$param();
               }
            }else{
              return self::$param();
            }
         }
   

    }

private  function databasename(){

         $dbname=self::$db->field("SELECT database_name FROM users WHERE user_id ='".$_SESSION['u_id']."' ");
         $_SESSION['aktiuser']['databasename']=$dbname;
         return $dbname;
     }

private  function userplan(){

        $user_plan=self::$db->field("SELECT plan FROM users WHERE user_id='".$_SESSION['u_id']."'");


        $_SESSION['aktiuser']['userplan']=$user_plan;
       return $user_plan;
}
private  function credentials(){

    $credentials=self::$db->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."'");

      //check if user is an admin with group id 1 and not main user to allow max credentials same as main user
      $user_data=self::$db->query("SELECT group_id,main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."'")->getAll();
      $user_data=$user_data[0];
      if($user_data['group_id'] == 1 && $user_data['main_user_id'] > 0){
        $credentials=self::$db->field("SELECT credentials FROM users WHERE user_id='".$user_data['main_user_id']."'");
      }
        $_SESSION['aktiuser']['credentials']=$credentials;
        return $credentials;


        }



private  function user_role(){

    $user_role=self::$db->field("SELECT user_role FROM users WHERE user_id='".$_SESSION['u_id']."'");


    $_SESSION['aktiuser']['user_role']=$user_role;
    return $user_role;
}
private  function user_active(){

    $user_active=self::$db->field("SELECT active FROM users WHERE user_id='".$_SESSION['u_id']."'");


    $_SESSION['aktiuser']['user_active']=$user_active;
    return $user_active;
}
private  function payment_type(){

    $payment_type=self::$db->field("SELECT payment_type FROM users WHERE user_id='".$_SESSION['u_id']."'");


        $_SESSION['aktiuser']['payment_type']=$payment_type;
        return $payment_type;
    }

    private  function u_type(){

        $u_type=self::$db->field("SELECT u_type FROM users WHERE user_id='".$_SESSION['u_id']."'");


        $_SESSION['aktiuser']['u_type']=$u_type;
        return $u_type;
    }
    private  function user_type(){

        $user_type=self::$db->field("SELECT user_type FROM users WHERE user_id='".$_SESSION['u_id']."'");


        $_SESSION['aktiuser']['user_type']=$user_type;
        return $user_type;
    }
    private  function is_trial(){

        $is_trial=self::$db->field("SELECT is_trial FROM user_info WHERE user_id='".$_SESSION['u_id']."'");


        $_SESSION['aktiuser']['is_trial']=$is_trial;
        return $is_trial;
    }
    private  function end_date(){

        $end_date=self::$db->field("SELECT end_date FROM user_info WHERE user_id='".$_SESSION['u_id']."'");


        $_SESSION['aktiuser']['end_date']=$end_date;
        return $end_date;
    }
    private  function start_date(){

        $start_date=self::$db->field("SELECT start_date FROM user_info WHERE user_id='".$_SESSION['u_id']."'");


        $_SESSION['aktiuser']['start_date']=$start_date;
        return $start_date;
    }

    private  function is_admin_company(){

    $is_admin_company=self::$db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1'");


    $_SESSION['aktiuser']['is_admin_company']=$is_admin_company;
    return $is_admin_company;
}
   private  function YodaDashboardGraph(){

    $YodaDashboardGraph=self::$db->field("SELECT value FROM user_meta WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");


    $_SESSION['aktiuser']['YodaDashboardGraph']=$YodaDashboardGraph;
    return $YodaDashboardGraph;
}
    private function is_easy_invoice_diy(){

      $is_easy_invoice_diy=true;
      $user_data=self::$db->query("SELECT pricing_plan_id,main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
      if($user_data->f('main_user_id')){
        $pricing_plan_id=self::$db->field("SELECT pricing_plan_id FROM users WHERE main_user_id='".$_SESSION['u_id']."'");
        if($pricing_plan_id != 9){
          $is_easy_invoice_diy=false;
        }
      }else{
        if($user_data->f('pricing_plan_id') != 9){
          $is_easy_invoice_diy=false;
        }
      }
      $_SESSION['aktiuser']['is_easy_invoice_diy']=$is_easy_invoice_diy;
      return $is_easy_invoice_diy;
    }

    private function is_easy_invoice_billtobox(){

      $is_easy_invoice_billtobox=true;
      $user_data=self::$db->query("SELECT pricing_plan_id,main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
      if($user_data->f('main_user_id')){
        $pricing_plan_id=self::$db->field("SELECT pricing_plan_id FROM users WHERE main_user_id='".$_SESSION['u_id']."'");
        if($pricing_plan_id != 10){
          $is_easy_invoice_billtobox=false;
        }
      }else{
        if($user_data->f('pricing_plan_id') != 10){
          $is_easy_invoice_billtobox=false;
        }
      }
      $_SESSION['aktiuser']['is_easy_invoice_billtobox']=$is_easy_invoice_billtobox;
      return $is_easy_invoice_billtobox;
    }

    private function subscription_pricing_plan_id(){
      $user_data=self::$db->query("SELECT pricing_plan_id,main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
      $pricing_plan_id = $user_data->f('pricing_plan_id');
      if($user_data->f('main_user_id')){
        $pricing_plan_id=self::$db->field("SELECT pricing_plan_id FROM users WHERE main_user_id='".$_SESSION['u_id']."'");
      }
      $_SESSION['aktiuser']['subscription_pricing_plan_id']=$pricing_plan_id;
      return $pricing_plan_id;
    }

}







?>