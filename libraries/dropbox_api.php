<?php
//use \Dropbox as dbx;

class drop{

	private $db;
	public $is_connect = false;
	public $accountInfo;
	private $path;
	private $customer_folder;
	private $contact_folder=null;
	private $item_folder;
	private $dropbox; #dropbox object
	private $field_id = 'customer_id';
	private $table_name = 'customers';
	private $field_name = 'name';
	private $access_token;
	private $base_url='/APP/Akti';

	function __construct( $path = null, $id = null, $item_id=null, $check = true, $db = '', $is_contact = 0, $serial_number = null, $contact_id=null,$is_batch=null,$is_custom_folder=false,$parent_files=false ) {
		//console::log($path, $id, $item_id, $check, $db, $is_contact, $serial_number, $contact_id,$is_batch);
		global $config;
		if($is_contact) {
			$this->field_id = 'contact_id';
			$this->table_name = 'customer_contacts';
			$this->field_name = "CONCAT_WS(' ',firstname, lastname)";
		}
		#this is for uploadify
		if($db){
			global $database_config;
			$database_up = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $db,
			);
			$this->db = new sqldb($database_up);
		}else{
			$this->db = new sqldb();
		}
		$this->access_token=$this->db->field("SELECT api FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
		$drop_connected=$this->db->field("SELECT active FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
		if($drop_connected && $this->access_token){

			//$this->dropbox = new dbx\Client(DROPBOX, "PHP-Example/1.0");
			//$this->accountInfo = $this->dropbox->getAccountInfo();
			$app_id=$this->db->field("SELECT app_id FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");

			$refresh_token=$this->db->field("SELECT api FROM apps WHERE type='refresh_token' AND main_app_id='".$app_id."' ");
			$expires_in=$this->db->field("SELECT api FROM apps WHERE type='expires_in' AND main_app_id='".$app_id."' ");
			
			if($refresh_token && $expires_in){
				if($expires_in +120 < time()){
					$this->refresh_token($refresh_token);
					$this->access_token=$this->db->field("SELECT api FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
				}
			}

			$ch = curl_init();
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        	curl_setopt($ch, CURLOPT_POST, true);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(null));
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        	curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/users/get_current_account');

        	$put = curl_exec($ch);
    		$info = curl_getinfo($ch);
			//if(!is_array($this->accountInfo)){
        	if($info['http_code']>300 || $info['http_code']==0){
				//console::error($this->accountInfo);
				msg::error(gm('Please go through Dropbox instalation proccess again').'.','error');
				$this->is_connect = false;
			}else{
				$this->is_connect = true;
				//$this->path = '/'.$path;
				if($is_custom_folder){
					$this->path=$path;
					$old_path=$path;
					/*$c = $this->isFolder("", $this->path );
					if(empty($c)){
						$this->createFolder( $this->path );
					}*/
				}else{
					if(strpos($config['site_url'], 'my.akti')!==false && $_SESSION['u_id']<10000){
						$path=str_replace('APP/Akti/','',$path);
						$this->path='/'.$path;
						/*$c = $this->isFolder("", $this->path );
						if(empty($c)){
							$this->createFolder( $this->path );
						}*/
					}else{
						$this->path = strpos($path, 'APP')!==false ? '/'.$path : $this->base_url.'/'.$path;
						/*$c = $this->isFolder($this->base_url, str_replace($this->base_url,"",$this->path) );
						if(empty($c)){
							$this->createFolder( $this->path );
						}*/
					}
					$old_path=str_replace('APP/Akti/','',$path);
				}		
				/*$c = $this->isFolder("", $this->path );
				if(empty($c)){
					$this->createFolder( $this->path );
				}*/
				/*$exists = $this->db->query("SELECT * FROM dropbox_folder WHERE cId='".$id."' AND folder='".addslashes($path)."' AND is_contact='".$is_contact."' ");*/
				/*if($exists->f('name')){
					$this->customer_folder = '/'.trim($exists->f('name'));
				}*/

				if($parent_files){
					$this->customer_folder = '/files';
					if($item_id){
						$this->item_folder = '/'.$item_id;
					}
					$this->createFolder($this->path.$this->customer_folder.$this->item_folder);
				}else if($is_batch){ 

							$this->customer_folder = '/batches';
							if($item_id){
								$this->item_folder = '/'.$item_id;
									}
							 $c = $this->isFolder($this->path,'/batches');
								if(empty($c)){
									//$r = $this->dropbox->createFolder($this->path.'/batches');
									$this->createFolder($this->path.'/batches');
								}else{
									//$this->dropbox->delete($this->path.'/batches');
									//$r = $this->dropbox->createFolder($this->path.$this->customer_folder.$this->item_folder);
									$this->createFolder($this->path.$this->customer_folder.$this->item_folder);
								}

				//}elseif($id && $path &&($path!='invoices')){
				}elseif($id && $path && ($old_path!='invoices')){
					if($check){
						$name = $this->db->field("SELECT ".$this->field_name." FROM ".$this->table_name." WHERE ".$this->field_id."='".$id."'  ");
						//if(!$exists->f('id')) {
							$this->customer_folder = '/'.str_replace('/','_',trim($name));
							//$r = $this->dropbox->createFolder($this->path.$this->customer_folder);
							/*$r=$this->createFolder($this->path.$this->customer_folder);
							if(is_array($r)){
								$this->db->query("INSERT INTO dropbox_folder SET folder='".addslashes($path)."', cId='".$id."', name='".addslashes($name)."', is_contact='".$is_contact."' ");
							}*/
						//}
						/*$c = $this->isFolder( $this->path,$this->customer_folder );
						if(empty($c)){
							$r=$this->createFolder( $this->path.$this->customer_folder );
							if(is_array($r)){
								$this->db->query("INSERT INTO dropbox_folder SET folder='".addslashes($path)."', cId='".$id."', name='".addslashes($name)."', is_contact='".$is_contact."' ");
							}
						}*/
					}
					if($item_id){
						$this->item_folder = '/'.$serial_number;
						if($check){
							switch ($old_path) {
								case 'quotes':
									$table = 'tblquote';
									$field = 'id';
									break;
								case 'projects':
									$table = 'projects';
									$field = 'project_id';
									break;
								case 'orders':
									$table = 'pim_orders';
									$field = 'order_id';
									break;
								case 'customers':
									$table = 'customers';
									$field = 'customer_id';
									break;
								case 'contracts':
									$table = 'contracts';
									$field = 'customer_id';
									break;
								default:
									$table = 'tblquote';
									$field = 'id';
									break;
							}
							$item_f = $this->db->field("SELECT dropbox_folder FROM ".$table." WHERE ".$field."='".$item_id."' ");
							if(!$item_f){
								// $c = $this->isFolder( $item_id, $this->path.$this->customer_folder );
								// if(empty($c)){
									//$r = $this->dropbox->createFolder($this->path.$this->customer_folder.$this->item_folder);
									/*$c = $this->isFolder( $this->path.$this->customer_folder,$this->item_folder );
									if(empty($c)){*/
										$this->createFolder($this->path.$this->customer_folder.$this->item_folder);
									//}				
									$this->db->field("UPDATE ".$table." SET dropbox_folder='1' WHERE ".$field."='".$item_id."' ");
								// }
							}else{
								//$c = $this->isFolder( $this->path.$this->customer_folder,$this->item_folder );
								//if(empty($c)){
									$this->createFolder( $this->path.$this->customer_folder.$this->item_folder );
								//}
							}
						}
					}
				}else{
						if($contact_id==='temp'){
						$c = $this->isFolder($this->path, 'temp' );
						if(empty($c)){
							//$r = $this->dropbox->createFolder($this->path.'/temp');
							$this->createFolder($this->path.'/temp');
						}else{
							//$this->dropbox->delete($this->path.'/temp');
							//$r = $this->dropbox->createFolder($this->path.'/temp');
							$this->deleteF($this->path.'/temp');
							$this->createFolder($this->path.'/temp');
						}

					}elseif(!$id && isset($contact_id)){

						$this->db->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$contact_id."'");
						$this->contact_folder= $this->db->f('firstname').'_'.$this->db->f('lastname');
						/*$c = $this->isFolder( $this->path,'/'.$this->contact_folder);
						if(empty($c)){*/
							//$r = $this->dropbox->createFolder($this->path.'/'.$this->contact_folder);
							$this->createFolder($this->path.'/'.$this->contact_folder);
						//}
					}elseif(isset($id)){
						$this->customer_folder='/'.str_replace('/','_',trim($this->db->field("SELECT name FROM customers WHERE customer_id='".$id."'")));
						if ($serial_number){
							$this->item_folder = '/'.$serial_number;
							/*$c = $this->isFolder( $this->path.$this->customer_folder,$this->item_folder );
							if(empty($c)){*/
								$this->createFolder($this->path.$this->customer_folder.$this->item_folder);
							//}						
						}else{
							/*$c = $this->isFolder( $this->path,$this->customer_folder );
							if(empty($c)){*/
								//$r = $this->dropbox->createFolder($this->path.$this->customer_folder);
								$this->createFolder($this->path.$this->customer_folder);
							//}
							
						}
					}


				}
				console::log('Connected to Dropbox');
			}
		}else{
			$this->is_connect = false;
		}
	}

	public function refresh_token($refresh_token)
	{
		global $config;
		$ch = curl_init();

		$data=array(
			'client_id'		 	=> $config['dropbox_client_id'],
			'client_secret' 	=> $config['dropbox_client_secret'],
			'grant_type' 		=> 'refresh_token',
			'refresh_token'		=> $refresh_token,
			);

    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_URL, $config['dropbox_baseurl'].'oauth2/token');
        	
        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
    		msg::error(gm("Error on refreshing token"),"error");
	    }else{
	    	$token=json_decode($put);
    		
	    	$app = $this->db->query("SELECT * FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
			if($app->next()){
				$this->db->query("UPDATE apps SET active = '1', api ='".$token->access_token."' WHERE app_id= '".$app->f('app_id')."'");

				$access_token = $this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='access_token' ");
				if(!$access_token){
					$this->db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='access_token', api='".$token->access_token."' ");
				}else{
					$this->db->query("UPDATE apps SET api ='".$token->access_token."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='access_token'");	
				}

				$expires_in = $this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='expires_in' ");
				$expires_time = time() + $token->expires_in;
				if(!$expires_in){
					$this->db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='expires_in', api='".$expires_time."' ");
				}else{
					$this->db->query("UPDATE apps SET api ='".$expires_time."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='expires_in'");	
				}			
	    		 										 								    
			}

		}
	}

	public function isFolder( $path , $name ){
		if(!$name){
			console::log('Please provide a name');
			return null;
		}
		if($this->is_connect){
			$p = $path;
			/*if(!$p){
				$p = $this->path;
			}
			if($p=='/'){
				return "/";
			}*/
			//$e = $this->dropbox->searchFileNames( $p , $name );
			//return $e;
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			$ch = curl_init();
			/*$drop_data=array(
				'path'			=> $p,
				'query'			=> $name,
				'start'			=> 0,
				'max_results'	=> 100,
				'mode'			=> 'filename'
				);*/
			$options = array(
				'path'			=> $p,
				'max_results'	=> 100,
				'file_status' 	=> 'active',
        		'file_categories' => 'folder'
			);
			$drop_data=array(
				'query'			=> $name,
				'options'		=> $options,
				
				);
			$drop_data=json_encode($drop_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
	        //curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/search');
	        curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/search_v2');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

	    	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Check folder failed');
	    		return null;
	    	}else{
	    		$resp=json_decode($put);
	    		return $resp->matches;
	    	}
		}
		return $this->not_connected();
	}

	public function createFolder( $name ){
		if(!$name){
			console::log('Please provide a name');
			return null;
		}
		if($this->is_connect){
			//$e = $this->dropbox->createFolder( '/' . $name );
			//return $e;
			$ch = curl_init();
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			$drop_data=array(
					"path" 				=> $name,
    				"autorename"		=> false
					);
			$drop_data=json_encode($drop_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/create_folder');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);

		   	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Create folder failed');
	    		return null;
	    	}else{
	    		return true;
	    	}
		}
		return $this->not_connected();
	}

	private function not_connected(){
		console::log('Could not connect');
		return null;
	}

	public function getContent( $path = '' ){
		if($this->is_connect){
			$p = $path;
			if(!$p){
				$p = $this->path.$this->customer_folder.$this->item_folder;
			}
			//$e = $this->dropbox->getMetadataWithChildren( $p );
			//return $e;
			$ch = curl_init();
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			$drop_data=array(
					"path" 				=> $p,
					);
			$drop_data=json_encode($drop_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/list_folder');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);

		   	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Get content failed');
	    		return null;
	    	}else{
	    		return json_decode($put);
	    	}
		}
		return $this->not_connected();
	}
	public function getLink( $path = '' ){
		if($this->is_connect){
			$p = $path;
			if(!$p){
				$p = $this->path.$this->customer_folder.$this->item_folder;
			}
			//$e = $this->dropbox->createShareableLink( $p );
			//return $e;
			$ch = curl_init();
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			$drop_data=array(
					"path" 				=> $p,
					"short_url"			=> false
					);
			$drop_data=json_encode($drop_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/sharing/create_shared_link');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);

		   	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Get link failed');
	    		return null;
	    	}else{
	    		return json_decode($put)->url;
	    	}

		}
		return $this->not_connected();
	}

	public function deleteF($path){
		if(!$path){
			console::log('Please provide a path');
			return null;
		}
		if($this->is_connect){
			//$e = $this->dropbox->delete( $path );
			//return $e;
			$ch = curl_init();
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			$drop_data=array(
					"path" 				=> $path,
					);
			$drop_data=json_encode($drop_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/delete');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);
		   	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Delete file failed');
	    		return null;
	    	}else{
	    		return array();
	    	}
		}
		return $this->not_connected();
	}

	public function upload( $name, $file, $size ){
		if($this->is_connect){
			$p = $this->path.$this->customer_folder.$this->item_folder.'/'.$name;
			//$f = fopen($file, "rb");
			//$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
			//console::log($e);
			$ch = curl_init();
			$drop_data=array(
					"path" 				=> $p,
					'mode'				=> 'add',
					'autorename'		=> false,
					'mute'				=> false,		
					);
			$drop_data=json_encode($drop_data);
			$headers=array("Content-Type: application/octet-stream",
				"Authorization: Bearer ".$this->access_token,
				"Dropbox-API-Arg: ".$drop_data);		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($file));
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://content.dropboxapi.com/2/files/upload');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);

		   	
		   /*	if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '188.26.120.86'){
						    var_dump($put, $info);exit();
						}*/
		   	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Upload file failed');
	    		return null;
	    	}else{
	    		return json_decode($put);
	    	}
			//fclose($f);
			//return $e;
		}
		return $this->not_connected();
	}
	public function moveFromTemp( $from, $name ){
		if($this->is_connect){
			// echo $from;
			// echo $this->path.'/'.$this->customer_folder;
			// exit();

			/*$name=explode('/', $from);
			//if($name[1]=='upload_invoices'){
			if(strpos($name[1], 'upload_invoices')!==false){
				$i=2;
			}else{
				$i=3;
			}*/
			// print_r($name);
			if($this->contact_folder){
				//$e = $this->dropbox->move( $from, $this->path.'/'.$this->contact_folder.'/'.$name[$i] );
				$full_path=$this->path.$this->contact_folder.'/'.$name;
			}else{
				//$e = $this->dropbox->move( $from, $this->path.'/'.$this->customer_folder.'/'.$name[$i] );
				$full_path=$this->path.$this->customer_folder.'/'.$name;
			}
			$ch = curl_init();
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			$drop_data=array(
					"from_path" 				=> $from,
    				"to_path"					=> str_replace("\\","",$full_path),
    				"allow_shared_folder"		=> false,
    				"autorename"				=> false,
    				"allow_ownership_transfer"	=> true,
					);
			$drop_data=json_encode($drop_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/move');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);
		   	if($info['http_code']>300 || $info['http_code']==0){
	    		msg::error(gm("Could not move the file. A file with the same name already exists!"),"error");
				return false;
	    	}else{
	    		return json_decode($put);
	    	}
			/*if(!$e){
				msg::error(gm("Could not move the file. A file with the same name already exists!"),"error");
				return false;
			}
			return $e;*/
		}
		return $this->not_connected();
	}

	public function renameFile( $from, $to ){
		if($this->is_connect){
			$ch = curl_init();
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$this->access_token);
			$drop_data=array(
					"from_path" 				=> $from,
    				"to_path"					=> str_replace("\\","",$to),
    				"allow_shared_folder"		=> false,
    				"autorename"				=> false,
    				"allow_ownership_transfer"	=> true,
					);
			$drop_data=json_encode($drop_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/move');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);
		   	if($info['http_code']>300 || $info['http_code']==0){
	    		msg::error(gm("Could not move the file. A file with the same name already exists!"),"error");
				return false;
	    	}else{
	    		return json_decode($put);
	    	}
			/*if(!$e){
				msg::error(gm("Could not move the file. A file with the same name already exists!"),"error");
				return false;
			}
			return $e;*/
		}
		return $this->not_connected();
	}

	public function upload_invoice( $name, $file, $size, $temp='' ){
		if($this->is_connect){

			if($temp){
				$p = $this->path.'/temp/'.$name;
				//$f = fopen($file, "rb");
				//$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
				//fclose($f);
			}else{
				if($this->contact_folder){
					$p = $this->path.'/'.$this->contact_folder.'/'.$name;
					//$f = fopen($file, "rb");
					//$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
					//fclose($f);
				}else{
					$p = $this->path.'/'.$this->customer_folder.'/'.$name;
					//$f = fopen($file, "rb");
					//$e = $this->dropbox->uploadFile( $p, dbx\WriteMode::force(), $f, $size );
					//fclose($f);
				}
			}
			$ch = curl_init();
			$drop_data=array(
					"path" 				=> $p,
					'mode'				=> 'add',
					'autorename'		=> false,
					'mute'				=> false,		
					);
			$drop_data=json_encode($drop_data);
			$headers=array("Content-Type: application/octet-stream",
				"Authorization: Bearer ".$this->access_token,
				"Dropbox-API-Arg: ".$drop_data);		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($file));
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://content.dropboxapi.com/2/files/upload');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);
		   	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Upload invoice failed');
	    		return null;
	    	}else{
	    		return json_decode($put);
	    	}
			//return $e;
		}
		return $this->not_connected();
	}

	public function getFile($path, $out){
		$path=stripslashes($path);
		if(!$path){
			console::log('Please provide a path');
			return null;
		}
		if(!$out){
			console::log('Please provide a out');
			return null;
		}

		if($this->is_connect){
			//$e = $this->dropbox->getFile( $path, $out );
			//return $e;
			$ch = curl_init();
			$drop_data=array(
					"path" 				=> $path,	
					);
			$drop_data=json_encode($drop_data);
			$headers=array("Content-Type: ", "Authorization: Bearer ".$this->access_token,
				"Dropbox-API-Arg: ".$drop_data);		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, '' );
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		    curl_setopt($ch, CURLOPT_URL, 'https://content.dropboxapi.com/2/files/download');

		    $put = curl_exec($ch);
		   	$info = curl_getinfo($ch);
		   	if($info['http_code']>300 || $info['http_code']==0){
	    		console::log('Download file failed');
	    		return null;
	    	}else{
	    		file_put_contents($out, $put);
	    	}
		}
		return $this->not_connected();
	}


}

?>