<?php   if(!defined('BASEPATH')) exit('No direct script access allowed');
/*if(DEBUG){
    ini_set('display_errors', 1);
    error_reporting(E_ERROR);
}*/

date_default_timezone_set('Europe/Bucharest');
session_start();
// var_dump($_POST);

// build the $in array
if(is_array($_GET)){
    $in = cleanInputs($_GET);
    unset($_GET);
}

if(is_array($_POST)){
    $in = array_merge($in,cleanInputs($_POST));
    unset($_POST);
}

require(__DIR__.'/common_cron.php');
if(php_sapi_name() === 'cli'){
    define('LOG_QUERYS','NO');
}else{
    define('LOG_QUERYS','yes');
}
// $json_vars = file_get_contents("php://input");
$json_vars = json_decode( @file_get_contents( "php://input" ), true );

if($json_vars){
    // $json = new Services_JSON();
    // $json_vars = (array)$json->decode($json_vars);
    $in = array_merge($in,cleanInputs($json_vars));
}

if(isset($in['lang_id'])){
    $_SESSION['lang_id'] = $in['lang_id'];
}

function cleanInputs($data){
    $clean_input = array();
    if(is_array($data)){
        foreach($data as $k => $v){
            $clean_input[$k] = cleanInputs($v);
        }
    }elseif(is_object($data)){
        $data = (array)$data;
        foreach($data as $k => $v){
            $clean_input[$k] = cleanInputs($v);
        }
    }
    else{
        if(get_magic_quotes_gpc()){
            $data = trim(stripslashes($data));
        }
        $clean_input = addslashes(trim($data));
    }
    return $clean_input;
}
// define("LOG_QUERYS", 'NO');

$_db=new sqldb();
$_db2=new sqldb();
$_db->query("SET names utf8");
$_db->query("SET sql_mode= ''");
$_db->query("SELECT constant_name, value, type, long_value FROM settings");
while($_db->move_next())
{
    if($_db->f('constant_name') && $_db->f('type')  == 1)
    {
        if(!defined($_db->f('constant_name'))){
            if($_db->f('value')==''){
                define($_db->f('constant_name'),'0');
            }else{
                define($_db->f('constant_name'),$_db->f('value'));
            }           
        }
    }else{
        if(!defined($_db->f('constant_name'))){
            define($_db->f('constant_name'),$_db->f('long_value'));
        }
    }
}
$_db->query("SELECT dispatch_stock_address.* FROM dispatch_stock_address WHERE address_id='1'");
$_db->move_next();
if(!$_db->f('address_id')){
       $_db2->query("INSERT INTO dispatch_stock_address SET naming='Main',
                                                            address_id='1',
                                                            country_id='".ACCOUNT_DELIVERY_COUNTRY_ID."', 
                                                            zip='".ACCOUNT_DELIVERY_ZIP."', 
                                                            city='".ACCOUNT_DELIVERY_CITY."',  
                                                            address='".ACCOUNT_DELIVERY_ADDRESS."', 
                                                            is_default='1'
                                                               ");
}else{
    if($_db->f('is_default') !='1' ){
        $_db2->query("UPDATE dispatch_stock_address SET naming='Main',
                                                                country_id='".ACCOUNT_DELIVERY_COUNTRY_ID."', 
                                                                zip='".ACCOUNT_DELIVERY_ZIP."', 
                                                                city='".ACCOUNT_DELIVERY_CITY."',  
                                                                address='".ACCOUNT_DELIVERY_ADDRESS."', 
                                                                is_default='1'
                                                                WHERE address_id='1' ");
    }
}


$def_lang = $_db->field("SELECT lang_id FROM pim_lang WHERE default_lang=1");
if(!$def_lang){
    $user_lang = $_SESSION['l'];
    if ($user_lang=='nl')
    {
        $user_lang='du';
    }
    switch ($user_lang) {
        case 'en':
            $user_lang_id=1;
            break;
        case 'du':
            $user_lang_id=3;
            break;
        case 'fr':
            $user_lang_id=2;
            break;
        default: 
            $user_lang_id=2;
            break;
    }
    $def_lang = $user_lang_id;
}

// $_db->move_next();
define('DEFAULT_LANG_ID',$def_lang);


# load the language translations 
switch (true) {
    case isset($_SESSION['l']):
        $language = $_SESSION['l'];
        break;
    case isset($_COOKIE["l"]):
        $language = $_COOKIE["l"];
        break;
    default:
        $language = LANGUAGE_TR;
        break;
}
$allowed_langs = array('en','fr','nl','de');
if(!in_array($language, $allowed_langs)){
    $language = 'en';
}
$lang = array();
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);
$languageLabels = $db_users->query("SELECT id, ".strtolower($language)." FROM langs ")->getAll();
foreach ($languageLabels as $key => $value) {
    $lang[$value['id']] = $value[strtolower($language)];
}

$p_access = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,100);
perm::$allow_apps = $p_access;
//JWT start
ark::loadCronLibraries(array('jwt/JWT'));
$JWT_PSWD=$db_users->field("SELECT value FROM settings WHERE constant_name='JWT_PSWD'");
$AWS_FACTORY_DATA=$db_users->field("SELECT long_value FROM settings WHERE constant_name='AWS_FACTORY'");
try{
    $AWS_FACTORY=JWT::decode($AWS_FACTORY_DATA, $JWT_PSWD, array('HS256'));
    define('AWS_FACTORY_KEY',$AWS_FACTORY->data->key);
    define('AWS_FACTORY_SECRET',$AWS_FACTORY->data->secret);
}catch(ExpiredException $e){
    //
}catch(Exception $e){
    //    
}
//JWT end
// print_r($in['do']);
/* End of file startup.php */