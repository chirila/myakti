<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('display_errors', 0);
ini_set('error_reporting', 0);


include_once(__DIR__.'/library/console.php');
include_once(__DIR__.'/../config/config.php');

include_once(__DIR__.'/../config/apps.php');


include_once(__DIR__.'/library/arkrouter.php');
include_once(__DIR__.'/../config/routes.php');

include_once(__DIR__.'/library/validation.php');

include_once(__DIR__.'/library/perm.php');

include_once(__DIR__.'/library/msg.php');

include_once(__DIR__.'/library/ark.php');

include_once(__DIR__.'/library/at.php');
include_once(__DIR__.'/library/database_store.php');
include_once(__DIR__.'/library/sqldb.php');
include_once(__DIR__.'/library/sqldbResult.php');

include_once(__DIR__.'/../config/database.php');

ark::loadCronLibraries();
define('DEBUG', $config['debug']);

//if (DEBUG) {
  ini_set('display_errors', 1);
  ini_set('error_reporting', 1);
 // error_reporting(E_ALL);
 // error_reporting(E_ALL & ~E_NOTICE);
//}

/* End of file common.php */