<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$page = ark::run();

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	echo $page;
	exit();
}

$user_name = get_user_name($_SESSION['u_id']);
$account_name = ACCOUNT_COMPANY;

foreach ($lang as $key => $val){
	$lang[$key] = utf8_encode($val);
}
$langjs = 'var LANG='.json_encode($lang);
// if we have a layout
if(ark::$layout){

	$main = new at(ark::$layout);
	$main->assign('page', ($page==1 ? '' : $page));
	// $main->assign('ctrljs', '<script src="js/controllers/'.ark::$app.'_ctrl.js"></script>');
	// to change this in the future
	$main->assign('basehref',$config['site_url']);
	$main->assign('metatitle', ($_SESSION['u_id'] ? (aktiUser::get('is_easy_invoice_diy') ? 'Mcf' : $config['meta_title']) : ($in['source'] && $in['source'] == 'mcf' ? 'Mcf' : $config['meta_title'])));
	$main->assign('version','?ver='.$config['version']);
	$main->assign('versionNr',$config['version']);
	$main->assign('metakeywords',$config['meta_keywords']);
	$main->assign('metadescription',$config['meta_description']);
	$main->assign('langjs',$langjs);
	$main->assign('lang',$_SESSION['l']);
	$main->assign('is_first',(ark::$controller=='first'? 1:0));
	$main->assign('is_board',strpos($_SERVER['REQUEST_URI'], 'board/'));
	//$main->assign('uid',$_SESSION['u_id']);
	$main->assign('identity', addslashes($account_name.' - '.$user_name));
	$main->assign('not_easy_invoice_diy', ($_SESSION['u_id'] ? (aktiUser::get('is_easy_invoice_diy') ? false : true) : ($in['source'] && $in['source'] == 'mcf' ? false : true)));
	$main->assign('easy_invoice_diy', ($_SESSION['u_id'] ? (aktiUser::get('is_easy_invoice_diy') ? true : false) : ($in['source'] && $in['source'] == 'mcf' ? true : false)));
	$main->out();
	
} else {
	echo $page;
}
