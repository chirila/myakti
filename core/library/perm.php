<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if(!isset($_SESSION['group'])){
    $_SESSION['group'] = 'public';
}

class perm {
    private static $ctrl = array();
    private static $model = array();
    public static $apps = array('company'=>'1','article'=>'12','auth'=>'100','home'=>'10','invoice'=>'4','misc'=>'10','order'=>'6','pim'=>'10','project'=>'3','quote'=>'5','settings'=>'7','api'=>'10','maintenance'=>'13');
    public static $allow_apps = array();

    /**
     * Set permissions for a resources and a given group
     *
     * @param $type use perm::$CONTROLLER or perm::$MODEL
     * @param $object the object to have access to, for models use model-method(all can be used in any position)
     * @param $group the group that should have access
     * @param $permission should have permission or not
     */
    private static function set($type = 'controller', $object, $group, $permission = true){

        if(empty($group)){
            console::error(array('message' => 'Group can not be empty for permissions','file'=>__FILE__));
            return;
        }elseif(is_array($group)){
            foreach($group as $member){
                self::set($type, $object, $member, $permission);
            }
            return;
        }

        if(empty($object)){
            console::error(array('message' => 'Object can not be empty for permissions','file'=>__FILE__));
            return;
        }

        switch($type){
            case 'controller':
                $perms = self::$ctrl;
                $perm_type = 'ctrl';
                break;
            case 'model':
                $perms = self::$model;
                $perm_type = 'model';
                break;
            default:
                console::error(array('message' => 'Invalid permission type','file'=>__FILE__,'line'=>__LINE__));
                return;
        }

        //check to see if we have that group
        if(!isset($perms[$group])){
            $perms[$group] = array();
            if(defined('USE_APPS') && USE_APPS == true){
                $perms[$group] = array(ark::$app => array($object => $permission));
            }else{
                $perms[$group] = array($object => $permission);
            }
        }else{
            //we have the group
            if(defined('USE_APPS') && USE_APPS == true){
                $perms[$group][ark::$app][$object] = $permission;
            }else{
                $perms[$group][$object] = $permission;
            }
        }
        self::$$perm_type = $perms;
    }


    /**
     * Checks if the current users group is allowed to perm a given action on the current resource(model, controller)
     *
     * @param $type string resource type perm::$MODEL or perm::$CONTROLLER
     * @uses ark
     * @return boolean (Yes it is allowed, No it's not allowed')
     */
    public static function isAllowed($type = 'controller'){

        $perms = self::loadPermissions($type);
        if(!$perms){
            return false;
        }

        switch($type){
            case 'controller':
                $object = ark::$controller;
                $object_all = 'all';
                $perms = self::$ctrl;
                break;
            case 'model':
                $object = ark::$model.'-'.ark::$method;
                $object_all = ark::$model.'-all';
                $perms = self::$model;
                break;
            default:
                console::error(array('message' => 'Invalid permission type','file'=>__FILE__,'line' => __LINE__));
                return false;
        }
        $entry = array();
        //check to see if we have exact permissions on the controller
        if(defined('USE_APPS') && USE_APPS == true) {
            if(array_key_exists($_SESSION['group'], $perms)){
                $entry = $perms[$_SESSION['group']][ark::$app];
            }
            if(!$entry){
                $entry = $perms['all'][ark::$app];
            }
        }else{
            $entry = $perms[$_SESSION['group']];
            if(!$entry){
                $entry = $perms['all'];
            }
        }



        switch(true){
            case isset($entry[$object]):
                return $entry[$object];//permission on the object go for it

            case isset($entry[$object_all]):
                return $entry[$object_all];//permission on all object in this app

            case $type == 'model' && isset($entry['all-'.ark::$method]):
                return $entry['all-'.ark::$method];

            case $type == 'model' && isset($entry['all-all']):
                return $entry['all-all'];

            case isset($perms['all'])://permission for group all ?
                $entry = defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true ? $perms['all'][ark::$app] : $perms['all'];
                //check the all groups requires some extra handling for models
                if($type == 'model'){
                    //we need to check all of the above so model-method, model-all, all-method,all-all
                    foreach (array($object, $object_all, 'all-'.ark::$method, 'all-all') as $value) {
                        if(!isset($entry[$value])){
                            continue;
                        }
                        return $entry[$value];
                    }
                }
                $key = isset($entry[$object]) ? $object : $object_all;//permission for controller or for all controllers
                return isset($entry[$key]) ? $entry[$key] : false;//return the perms for all if we have
        }
        //My great concern is not whether you have failed, but whether you are content with your failure. Abraham Lincoln
        return false;
    }

    /**
     * Loads a permission file for the current resource
     *
     * @param $type string resource type perm::$MODEL or perm::$CONTROLLER
     * @uses ark
     * @return boolean Managed to load file or not
     */
    private static function loadPermissions($type = 'controller'){

       if(defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
            $permpath = ark::$apppath.ENVIRONMENT.'/perm/';
        }else{
            $permpath = ark::$apppath.'perm/';
        }

        switch ($type) {
            case 'controller':
                $permpath = $permpath.'controller.php';
                break;
            case 'model':
                $permpath = $permpath.'model.php';
                break;
            default:
                return false;
        }

        if(file_exists($permpath)){
            include_once($permpath);
            return true;
        }
        return false;
    }

    /**
     * Sets allow for controller and group
     *
     * @param $controller
     * @param $group
     * @param $permission boolean Allow or Deny
     *
     */
    public static function controller($controller, $group, $permission = true){
        if(is_array($controller) && !empty($controller)){
            foreach ($controller as $value) {
                self::set('controller', $value, $group, $permission);
            }
            return;
        }
        self::set('controller', $controller, $group, $permission);
    }

    /**
     * Sets allow for model and group
     *
     * @param $model string
     * @param $method string
     * @param $group string
     * @param $permission boolean Allow Or Deny
     */
    public static function model($model, $method, $group, $permission = true){
        self::set('model', $model.'-'.$method, $group, $permission);
    }

    /**
     * Checks permission for given resource type
     *
     * @param $type string resource type perm::$MODEL or perm::$CONTROLLER
     * @deprecated use perm::isAllowed($type) instead
     */
    public static function get($type = 'controller'){
        return self::isAllowed($type);
    }

    public static function user_cred($allow){
        self::$allow_apps = $allow;
    }
    
}
