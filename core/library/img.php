<?php

/* server side prepare image for output with resize */
if(!isset($req)){
	$req = array();
}

foreach($_REQUEST as $k => $v )
{
	$req[$k] = urldecode($v);
}



if(!$req['src']) die('no image');
$req['src'] = realpath($req['src']);
//print_r($req['src']);die();
if(!$req['w']) $new_width=0;
else if($req['w']!='') $new_width = $req['w'];
if(!$req['h']) $new_height=0;
else if($req['h']!='') $new_height = $req['h'];
if(!$req['w'] && !$req['h']) $new_width=138;
$f_type = substr($req['src'],-3,3);
$original_image=$req['src'];



if($f_type == "jpg") $type = "JPEG";
else if($f_type == "gif") $type = "GIF";
else if($f_type == "png") $type = "PNG";
$create = 'ImageCreateFrom'.$type;
$do = 'Image'.$type;
		$original_image=@$create($original_image);
		$aspect_ratio = imagesx($original_image) / imagesy($original_image); 
		if (empty($new_width)) 
		{ 
			$new_width = $aspect_ratio * $new_height; 
		}
		elseif (empty($new_height)) 
		{ 
			$new_height= $new_width / $aspect_ratio; 
		}
		if (imageistruecolor($original_image))	
		{ 
			$image = imagecreatetruecolor($new_width, $new_height);
			if($type=='PNG') $image = imagealphablending($image, false);
		} 
		else 
		{ 
			$image = imagecreate($new_width, $new_height); 
		} 
		// copy the original image onto the smaller blank 
		imagecopyresampled($image, $original_image, 0, 0, 0, 0, $new_width, $new_height, imagesx($original_image), imagesy($original_image));
//header('Content-Disposition: Attachment;filename=image.'.$f_type);
header('Content-Type: image/'.$f_type);
		
    if($type=='PNG') $do($image, NULL, $quality=9);
		else $do($image, NULL, $quality=100); 
		imagedestroy($image);

//fpassthru($fp);

?>