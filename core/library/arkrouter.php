<?php

class arkRouter {
	// to do - accept multiple request types for the same route

	protected $routes = array();
	protected $base = '';


	/**
	  * Create router in one call from config.
	  *
	  * @param array $routes
	  * @param string $base - useful when the application is installed in a subdirectory
	  */
	public function __construct($routes = array(), $base = '') {

		if($base != ''){
			$this->setbase($base);
		}

		if(!empty($routes)){
			$this->addRoutes($routes);
		}
	}

	/**
	 * Retrieves all routes.
	 * Useful if you want to process or display existing routes.
	 * @return array All routes.
	 */
	public function getRoutes() {
		return $this->routes;
	}

	/**
	 * Add multiple routes at once from array in the following format:
	 *
	 *   $routes = array(
	 *      'name' => array(
	 *			'method' => "GET",
				'do' => 'app-controller-model-method',
				'params' => 'cat_id/url'
				),
	 *   );
	 *
	 * @param array $routes
	 * @return void
	 * @author Koen Punt
	 */
	public function addRoutes($routes){
		if(!is_array($routes)) {
			return false;
		}
		foreach($routes as $name => $route) {
			$this->add($name, $route['method'], $route['do'], $route['params']);

		}
		return true;
	}

	/**
	 * Set the base path.
	 * Useful if you are running your application from a subdirectory.
	 */
	public function setbase($base) {
		$this->base = $base;
	}


	/**
	 * add a route to the list
	 *
	 * @param string $name The first segment in a URL, that will be asociated with 'do'
	 * @param string $method One of 5 HTTP Methods, or a pipe-separated list of multiple HTTP Methods (GET|POST|PATCH|PUT|DELETE)
	 * @param string $do The target app-controlled-model-method where this route should point to
	 * @param string $params the variable names aspected in the controller
	 */
	public function add($name, $method, $do, $params) {
		if(isset($this->routes[$name])){
			return false;
		}
		$this->routes[$name] = array(
			'method'=> $method,
			'do' => $do,
			'params' => $params
		);
		return 'true';
	}

	/**
	 * Transfers the requested URL into 'in' values
	 * @param string $requestUrl
	 * @param string $requestMethod
	 */

	public function run($requestUrl = null, $requestMethod = null) {
		global $in;

		// set Request Url if it isn't passed as parameter
		if($requestUrl === null) {
			$requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
		}

		// strip base path from request url
		$requestUrl = substr($requestUrl, strlen($this->base));

		// Strip query strings from Request Url
		if (($strpos = strpos($requestUrl, '?')) !== false) {
			$requestUrl = substr($requestUrl, 0, $strpos);
		}
		if (($strpos = strpos($requestUrl, '&')) !== false) {
			$requestUrl = substr($requestUrl, 0, $strpos);
		}

		// set Request Method if it isn't passed as a parameter
		if($requestMethod === null) {
			$requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
		}
		// Force request_order to be GET, POST
		// $_REQUEST = array_merge($_GET, $_POST);
		$request = array_values(array_filter(explode('/', $requestUrl)));
		
		if(empty($request) || !array_key_exists($request[0], $this->routes)){
			// no route is matching the request
			return false;
		} else {
			$route = $this->routes[$request[0]];
			if($route['method'] != $requestMethod){
				return false;
			}
			$in['do'] = $route['do'];

			//now that we have mapped the 'do', we map the segments
			foreach($request as $key => $value){
				$in['segments'][$key] = $value;
			}
			$params = array_values(array_filter(explode('/', $route['params'])));

		}

		if(is_array($params)){
			foreach($params as $key=>$value){
				$in[$value] = $request[($key+1)];
			}
		}
		return true;
	}
}
