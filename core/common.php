<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('display_errors', 0);
ini_set('error_reporting', 0);

include_once(BASEPATH.'/library/console.php');
console::start();
include_once(INSTALLPATH.'config/config.php');
include_once(INSTALLPATH.'libraries/aktiDeals.php');

include_once(INSTALLPATH.'config/apps.php');
include_once(INSTALLPATH.'libraries/aktiUser.php');

include_once(BASEPATH.'/library/arkrouter.php');
include_once(INSTALLPATH.'config/routes.php');

include_once(BASEPATH.'/library/validation.php');

include_once(BASEPATH.'/library/perm.php');

include_once(BASEPATH.'/library/msg.php');

include_once(BASEPATH.'/library/ark.php');

include_once(BASEPATH.'/library/at.php');
include_once(BASEPATH.'/library/database_store.php');
include_once(BASEPATH.'/library/sqldb.php');
include_once(BASEPATH.'/library/sqldbResult.php');

include_once(INSTALLPATH.'config/database.php');

ark::loadLibraries();

define('DEBUG', $config['debug']);

if (DEBUG) {
  ini_set('display_errors', 1);
  ini_set('error_reporting', 1);
 // error_reporting(E_ALL);
 // error_reporting(E_ALL & ~E_NOTICE);
}

/* End of file common.php */