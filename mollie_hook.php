<?php
define('INSTALLPATH',__DIR__.'/');
//define('ENVIRONMENT','admin');
define('LAYOUT',__DIR__.'/layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_project.log');
set_time_limit(0);
include_once(__DIR__."/core/startup.php");
global $config,$database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
); 
$db_users= new sqldb($db_config);
$response=file_get_contents("php://input");
$db_users->query("INSERT INTO mollie_log SET response='".addslashes(serialize($response))."', date='".time()."' ");
	
	$id=str_replace("id=","",file_get_contents("php://input"));
	$user_db=$db_users->field("SELECT `database` FROM mollie_payments WHERE mollie_id='".$id."' ");
	if($user_db){

		$db_user = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $user_db,
		); 
		$db= new sqldb($db_user);

		$api_key=$db->field("SELECT api FROM apps WHERE name='Mollie' AND type='main' AND main_app_id='0'");
		$headers=array('Content-Type: application/json','Authorization: Bearer '.$api_key);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['mollie_base_url'].'/payments/'.$id);

        $put = curl_exec($ch);
    	$info = curl_getinfo($ch);
		
    	if($info['http_code']>=200 && $info['http_code']<300){
    		$resp=json_decode($put);
    		if($resp->status == 'paid'){
				$payment_date = date("Y-m-d",time());
				$payment_view_date=date(ACCOUNT_DATE_FORMAT,time());
				$info=$payment_view_date.'  -  '.place_currency(display_number($resp->amount),'&euro;').' Mollie';
				$db->query("UPDATE tblinvoice SET icepay_payment = '".$id."',
            				icepay_status =  'OK',
                                    stripe_paid= '2'
           					WHERE id= '".$resp->metadata->invoice_id."'");
				$db->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id= '".$resp->metadata->invoice_id."' ");
				$db->query("INSERT INTO tblinvoice_payments SET
                              invoice_id  = '".$resp->metadata->invoice_id."',
                              date    = '".$payment_date."',
                              amount    = '".$resp->amount."',
                              info        = '".$info."' ");
				$db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Payment has been successfully recorded by {endl} Mollie')."', field_name='invoice_id', field_value='".$resp->metadata->invoice_id."', date='".time()."', type='0', reminder_date='".time()."' ");
    		}
    	}
	}
	
	header('X-PHP-Response-Code: 200', true, 200);
	exit();
?>