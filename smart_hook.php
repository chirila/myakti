<?php
define('INSTALLPATH',__DIR__.'/');
//define('ENVIRONMENT','admin');
define('LAYOUT',__DIR__.'/layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_project.log');
set_time_limit(0);
include_once(__DIR__."/core/startup.php");
global $config,$database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
); 
$db_users= new sqldb($db_config);
$response = json_decode( preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', @file_get_contents( "php://input" )), true );
$db_users->query("INSERT INTO smart_log SET response='".addslashes(serialize($response))."', date='".time()."' ");
	
	if(count($response)==1 && $response['webhookId']){
		$str='Smartsheet-Hook-Response: '.$_SERVER['HTTP_SMARTSHEET_HOOK_CHALLENGE'];
		header($str, true, 200);
		exit();
	}else{
		if($response['newWebhookStatus'] =='DISABLED_SCOPE_INACCESSIBLE'){
			$user_db=$db_users->field("SELECT `database` FROM smart_data WHERE sheet_id='".$response['scopeObjectId']."' ");
			if($user_db){
				$db_user = array(
				    'hostname' => $database_config['mysql']['hostname'],
				    'username' => $database_config['mysql']['username'],
				    'password' => $database_config['mysql']['password'],
				    'database' => $user_db,
				); 
				$db= new sqldb($db_user);
				$project_id=$db->field("SELECT project_id FROM projects WHERE smart_id='".$response['scopeObjectId']."' ");
				if($project_id){
					$db->query("UPDATE projects SET smart_id='' WHERE project_id='".$project_id."' ");
				}
			}

		}else if(!$response['events'][0]['changeAgent']){
			$user_db=$db_users->field("SELECT `database` FROM smart_data WHERE sheet_id='".$response['events'][0]['id']."' ");
			if($user_db){
				$db_user = array(
				    'hostname' => $database_config['mysql']['hostname'],
				    'username' => $database_config['mysql']['username'],
				    'password' => $database_config['mysql']['password'],
				    'database' => $user_db,
				); 
				$db= new sqldb($db_user);	
				if($response['events'][1] && $response['events'][1]['objectType'] == 'row'){
					switch($response['events'][1]['eventType']){
						case 'updated':
							if($response['events'][2] && $response['events'][2]['objectType']=='row' && $response['events'][2]['eventType']=='deleted'){
								$db->query("DELETE FROM tasks WHERE row_id='".$response['events'][1]['id']."'");
							}else if($response['events'][2] && $response['events'][2]['objectType']=='cell'){
								$colls=array();
								for($i=2;$i < count($response['events']); $i++){
									array_push($colls, $response['events'][$i]['columnId']);
								}
								$app = $db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
								$headers=array('Content-Type: application/json','Authorization: Bearer '.$app->f('api'));
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        					curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        					curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$response['events'][0]['id'].'/rows/'.$response['events'][1]['id']);
							
								$put = curl_exec($ch);
	    						$info = curl_getinfo($ch);
	    						if($info['http_code']>=200 && $info['http_code']<300){
	    							$task_id=$db->field("SELECT task_id FROM tasks WHERE row_id='".$response['events'][1]['id']."'");
	    							$row_data=json_decode(preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', $put));
	    							$final_data=array();
	    							foreach($row_data->cells as $key => $value){
	    								if(in_array($value['columnId'],$colls)){
	    									$final_data[$value['columnId']]=$value['value'];
	    								}
	    							}
	    							if(count($final_data)>0 && $task_id){
	    								$sql="UPDATE tasks SET ";
	    								foreach ($final_data as $key => $value) {
	    									$col_name=$db->field("SELECT col_name FROM smart_cols WHERE col_id='".$key."' ");
	    									if($col_name == 'start_date' || $col_name == 'end_date'){
	    										$sql.="".$col_name."='".strtotime($value)."',";
	    									}else{
	    										$sql.="".$col_name."='".$value."',";
	    									}
	    								}
	    								$sql=rtrim($sql,",");
	    								$sql.=" WHERE task_id='".$task_id."' ";
	    								$db->query($sql);
	    							}
	    						}
							}	
							break;
						case 'created':
							$app = $db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
							$headers=array('Content-Type: application/json','Authorization: Bearer '.$app->f('api'));
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        					curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        					curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$response['events'][0]['id'].'/rows/'.$response['events'][1]['id']);
						
							$put = curl_exec($ch);
    						$info = curl_getinfo($ch);
    						if($info['http_code']>=200 && $info['http_code']<300){
    							$project_id=$db->field("SELECT project_id FROM projects WHERE smart_id='".$response['scopeObjectId']."' ");
    							$row_data=json_decode(preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', $put));
    							$final_data=array();
    							foreach($row_data->cells as $key => $value){
    								$final_data[$value['columnId']]=$value['value'];			
    							}
    							if(count($final_data)>0 && $project_id){
    								$sql="INSERT INTO tasks SET ";
    								foreach ($final_data as $key => $value) {
    									$col_name=$db->field("SELECT col_name FROM smart_cols WHERE col_id='".$key."' ");
    									if($col_name == 'start_date' || $col_name == 'end_date'){
	    									$sql.="".$col_name."='".strtotime($value)."',";
	    								}else{
	    									$sql.="".$col_name."='".$value."',";
	    								}
    								}
    								$sql.="project_id='".$project_id."',row_id='".$response['events'][1]['id']."'";
    								$db->query($sql);
    							}
    						}
							break;
					}
				}
			}
		}
		header('X-PHP-Response-Code: 200', true, 200);
		exit();
	}
?>