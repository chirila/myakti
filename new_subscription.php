<?php
define('INSTALLPATH',__DIR__.'/');
define('LAYOUT',__DIR__.'/layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/

set_time_limit(0);
include_once(__DIR__."/core/startup.php");
include_once(__DIR__."/apps/settings/model/payment.php");
ark::loadLibraries(array('stripe/init'));
global $config,$database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
); 
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_project.log');
$db_users= new sqldb($db_config);

	\Stripe\Stripe::setApiKey($config['stripe_api_key']);
	$response = json_decode( @file_get_contents( "php://input" ), true );
	$db_users->query("INSERT INTO stripe_log SET response='".addslashes(serialize($response))."', date='".time()."' ");

	// $response = unserialize($db_users->field("SELECT response FROM stripe_log WHERE stripe_id='2028' "));
	// $response = unserialize('a:9:{s:2:"id";s:28:"evt_19zkIwDZsP34V8VZDLLS6Fos";s:6:"object";s:5:"event";s:11:"api_version";s:10:"2016-03-07";s:7:"created";i:1490092410;s:4:"data";a:1:{s:6:"object";a:35:{s:2:"id";s:27:"ch_19zkIuDZsP34V8VZ5QtKcbMl";s:6:"object";s:6:"charge";s:6:"amount";i:5900;s:15:"amount_refunded";i:0;s:11:"application";N;s:15:"application_fee";N;s:19:"balance_transaction";s:28:"txn_19zkIwDZsP34V8VZnoMORvc2";s:8:"captured";b:1;s:7:"created";i:1490092408;s:8:"currency";s:3:"eur";s:8:"customer";s:18:"cus_AKP7uoCzIhhIIl";s:11:"description";N;s:11:"destination";N;s:7:"dispute";N;s:12:"failure_code";N;s:15:"failure_message";N;s:13:"fraud_details";a:0:{}s:7:"invoice";s:27:"in_19zkIuDZsP34V8VZNq88HhS4";s:8:"livemode";b:1;s:8:"metadata";a:0:{}s:12:"on_behalf_of";N;s:5:"order";N;s:7:"outcome";a:5:{s:14:"network_status";s:19:"approved_by_network";s:6:"reason";N;s:10:"risk_level";s:6:"normal";s:14:"seller_message";s:17:"Payment complete.";s:4:"type";s:10:"authorized";}s:4:"paid";b:1;s:13:"receipt_email";N;s:14:"receipt_number";N;s:8:"refunded";b:0;s:7:"refunds";a:5:{s:6:"object";s:4:"list";s:4:"data";a:0:{}s:8:"has_more";b:0;s:11:"total_count";i:0;s:3:"url";s:47:"/v1/charges/ch_19zkIuDZsP34V8VZ5QtKcbMl/refunds";}s:6:"review";N;s:8:"shipping";N;s:6:"source";a:23:{s:2:"id";s:29:"card_19zkImDZsP34V8VZQbHsQg7E";s:6:"object";s:4:"card";s:12:"address_city";N;s:15:"address_country";N;s:13:"address_line1";N;s:19:"address_line1_check";N;s:13:"address_line2";N;s:13:"address_state";N;s:11:"address_zip";N;s:17:"address_zip_check";N;s:5:"brand";s:4:"Visa";s:7:"country";s:2:"FR";s:8:"customer";s:18:"cus_AKP7uoCzIhhIIl";s:9:"cvc_check";s:4:"pass";s:13:"dynamic_last4";N;s:9:"exp_month";i:2;s:8:"exp_year";i:2019;s:11:"fingerprint";s:16:"SCsCoLb9gSgfttH1";s:7:"funding";s:5:"debit";s:5:"last4";s:4:"9435";s:8:"metadata";a:0:{}s:4:"name";s:23:"tomlejeune.dj@gmail.com";s:19:"tokenization_method";N;}s:15:"source_transfer";N;s:20:"statement_descriptor";N;s:6:"status";s:9:"succeeded";s:14:"transfer_group";N;}}s:8:"livemode";b:1;s:16:"pending_webhooks";i:2;s:7:"request";s:18:"req_AKP7IiTa0MBnnu";s:4:"type";s:16:"charge.succeeded";}');
	// echo "<pre>";
// var_dump($response);
	$next_date_2_days = mktime(0,0,1,date('n'),date('j')+2,date('Y'));
	$stripe_ips=array(
		"54.187.174.169",
		"54.187.205.235",
		"54.187.216.72",
		"54.241.31.99",
		"54.241.31.102",
		"54.241.34.107"
	);
	if(in_array($_SERVER['REMOTE_ADDR'], $stripe_ips) === false){
		header('X-PHP-Response-Code: 200', true, 200);
		exit();
	}
	$payment=new payment(); 
	switch($response['data']['object']['object']){
		case 'charge':
		echo "charge - ";
			if($response['data']['object']['status']=='succeeded' && $response['data']['object']['customer']!=''){
				$inv_exist=$db_users->field("SELECT id FROM tblinvoice WHERE our_ref='".$response['data']['object']['invoice']."' ");
				$cust_exist=$db_users->field("SELECT user_id FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."' ");
				$stripe_subscr_id=$db_users->field("SELECT stripe_subscr_id FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."' ");
				if($stripe_subscr_id){
					$stripe_inv=\Stripe\Invoice::all(array("customer" => $response['data']['object']['customer'],"subscription"=>$stripe_subscr_id,"limit" => 3));
				}			
				if($cust_exist && $stripe_subscr_id && $stripe_inv && count($stripe_inv['data'])>1){
					$user_data=$db_users->query("SELECT user_id,database_name,payment_fail,payment_fail_next FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."'");
					$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $response['data']['object']['customer']));
					$next_date = $inv_incoming['date'];
					$time = time();
					$update = $db_users->query("UPDATE users SET payed='1', active ='1', pay_id='".$response['data']['object']['invoice']."', next_date='".$next_date."', alias='".$user_data->f('user_id')."'
																	WHERE database_name='".$user_data->f('database_name')."' AND active!='1000' ");
					$update = $db_users->query("UPDATE users SET payed='1', pay_id='".$response['data']['object']['invoice']."', next_date='".$next_date."', alias='".$user_data->f('user_id')."'
																	WHERE database_name='".$user_data->f('database_name')."' AND active='1000' ");
					$payment->payment_invoice_stripe($user_data->f('user_id'),$response['data']['object']['invoice']);
					echo "invoice created";

					$user_update = $db_users->query("SELECT user_id FROM users WHERE database_name='".$user_data->f('database_name')."' ");
					while ($user_update->next()) {
						$update = $db_users->query("UPDATE user_info SET end_date='".$next_date."', is_trial='1' WHERE user_id='".$user_update->f('user_id')."' ");
					}
					$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".$user_data->f('database_name')."', `action`='Recurring payment', `date`='".$time."' ");
					
					$update = $db_users->query("UPDATE users SET payment_fail_next='0',payment_fail='0'
																	WHERE database_name='".$user_data->f('database_name')."'");
					if($user_data->f('payment_fail') > 0){
			                $db_users->query("UPDATE users SET next_date='".$user_data->f('payment_fail_next')."', payment_fail_next='0',payment_fail='0' WHERE database_name='".$user_data->f('database_name')."' ");
			                $db_users->query("UPDATE user_info SET end_date='".$user_data->f('payment_fail_next')."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name = '".$user_data->f('database_name')."')  ");
			            }else{
			                $db_users->query("UPDATE users SET next_date='".$next_date."', payment_fail_next='0',payment_fail='0' WHERE database_name='".$user_data->f('database_name')."' ");
			                $db_users->query("UPDATE user_info SET end_date='".$next_date."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name = '".$user_data->f('database_name')."')  ");
			            }
				}
				if($stripe_subscr_id){
					$cu = \Stripe\Customer::retrieve($response['data']['object']['customer']);
					$subscription = $cu->subscriptions->retrieve($stripe_subscr_id);
					$subscription->prorate = false;
					$subscription->save();
				}	
			}else if($response['data']['object']['status']=='failed' && $response['data']['object']['customer']!=''){
				$cust_exist=$db_users->field("SELECT user_id FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."' ");
				$user_data=$db_users->query("SELECT stripe_cust_id,user_id,payment_fail_next,payment_fail,database_name,next_date,first_name,last_name FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."' ");
				if($cust_exist){
					$db_users->query("UPDATE `users` SET next_date='".$next_date_2_days."', `payment_fail`='".($user_data->f('payment_fail')+1)."' WHERE database_name='".$user_data->f('database_name')."' ");
				      /*if(!$user_data->f('payment_fail_next')){
				            $db_users->query("UPDATE `users` SET payment_fail_next='".$user_data->f('next_date')."' WHERE database_name='".$user_data->f('database_name')."' ");
				       }*/
			       	$db_users->query("INSERT INTO crons SET date=NOW(), extra='Started informing client of failed payment',cron='cron_invoice.php' ");
			       	send_attention_mail($user_data->f('user_id'));
			       	$db_users->query("INSERT INTO crons SET date=NOW(), extra='Informed client of failed payment',cron='cron_invoice.php' ");
			       	$mail = new PHPMailer();
					$mail->WordWrap = 50;
					$fromMail='notification@akti.com';
					$mail->SetFrom($fromMail, $fromMail);
		
			        	$body ="Dear Celine,

					       subscription payment to Akti for ".utf8_encode($user_data->f('first_name'))." ".utf8_encode($user_data->f('last_name'))." with stripe id ".$response['data']['object']['customer']." has failed.

					        Your can see acount info <a href=\"https://dashboard.stripe.com/customers/".$response['data']['object']['customer']."\"> here</a>

							Best regards,
							Akti Customer Care
							<a href=\"http://akti.com\" >www.akti.com</a>";
							$subject= 'Subscription payment to Akti has failed';

					$mail->Subject = $subject;
			        	$mail->MsgHTML(nl2br($body));
					$mail->AddAddress('office@akti.com');

					$mail->Send();
					$db_users->query("INSERT INTO crons SET date=NOW(), extra='Informed Gaetan of failed payment',cron='cron_invoice.php' ");
				}
			}
			break;
		case 'subscription':
			if($response['data']['object']['status']=='canceled'){
				$cust_exist=$db_users->field("SELECT user_id FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."' ");
				$not_canceled=$db_users->field("SELECT stripe_subscr_id FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."' ");
				if($cust_exist && $not_canceled){
					$db_users->query("UPDATE users SET stripe_subscr_id='' WHERE stripe_cust_id='".$response['data']['object']['customer']."'");
					$user_data=$db_users->query("SELECT user_id,database_name,first_name,last_name FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."'");

					$db_users->query("UPDATE users SET payed='2', active='2',payment_fail_next='0',payment_fail='0' WHERE database_name='".$user_data->f('database_name')."' AND active!='1000' ");
					$db_users->query("UPDATE users SET payed='2', payment_fail_next='0',payment_fail='0' WHERE database_name='".$user_data->f('database_name')."' AND active='1000' ");
					$time = time();
					$db_users->query("INSERT INTO `user_logs` SET `database_name`='".$user_data->f('database_name')."', `action`='Subscription canceled', `date`='".$time."' ");

					$mail = new PHPMailer();
					$mail->WordWrap = 50;
					$fromMail='notification@akti.com';
					$mail->SetFrom($fromMail, $fromMail);
		
			        $body ="Dear Celine,

					       subscription to Akti for ".utf8_encode($user_data->f('first_name'))." ".utf8_encode($user_data->f('last_name'))." has been canceled.

					        Your can see acount info <a href=\"http://manage.akti.com/index.php?pag=user&user_id=".$user_data->f('user_id')."\"> here</a>

							Best regards,
							Akti Customer Care
							<a href=\"http://akti.com\" >www.akti.com</a>";
							$subject= 'Subscription to Akti has been canceled';

					$mail->Subject = $subject;
			        $mail->MsgHTML(nl2br($body));
					$mail->AddAddress('office@akti.com');

					$mail->Send();
					return true;
				}
			}
			break;
		case 'invoice':
			$stripe_inv=\Stripe\Invoice::all(array("customer" => $response['data']['object']['customer'],"limit" => 3));
			$cu = \Stripe\Customer::retrieve($response['data']['object']['customer']);
			$cust_exist=$db_users->field("SELECT user_id FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."' ");
			$user_data=$db_users->query("SELECT user_id,stripe_subscr_id,users,time_users,stripe_discount_id,credentials_serial,resseler_id,accountant_id FROM users WHERE stripe_cust_id='".$response['data']['object']['customer']."'");
			if(!$user_data->f('stripe_subscr_id')){
				break;
			}
			$subscription = $cu->subscriptions->retrieve($user_data->f('stripe_subscr_id'));
			if($cust_exist && count($stripe_inv['data'])>1 && $subscription['plan']['id'] && $subscription['plan']['id'] == 3){
				$aditional=unserialize($user_data->f('credentials_serial'));
				if($user_data->f('accountant_id')){
					$total=20;				
				}else{
					$total=20;
					$aditional[0]['crm_adv']='1';
					$aditional[1]['quotes_adv']='1';
				}
				foreach($aditional as $key=>$value){
					if($value[$value['model']]=='1'){
						$total+=((int)$value['price']);
					}
				}
				$users=$user_data->f('users')*15;
				$t_users=$user_data->f('time_users')*5;
				$total += ($users+$t_users);
				if($total > 0){
					if($user_data->f('stripe_discount_id')!='0' && $user_data->f('stripe_discount_id')!=''){
						$user_discount=\Stripe\Coupon::retrieve($user_data->f('stripe_discount_id'));
						if($user_discount['amount_off']==null){
							$total_init=$total-20;
							$total=$total_init-($total_init*$user_discount['percent_off']/100);
							$cu->account_balance = number_format(($total*$subscription['tax_percent']/100+$total),2)*100;
						}else{
							$total_nedeed=($total-$user_discount['amount_off']/100)*$subscription['tax_percent']/100+($total-$user_discount['amount_off']/100);
							$total_discount_no_vat=20-$user_discount['amount_off']/100;
							$total_account_with_discount=(20-$user_discount['amount_off']/100)*$subscription['tax_percent']/100+(20-$user_discount['amount_off']/100);
							if($total_discount_no_vat<0){
								$cu->account_balance = number_format($total_nedeed,2)*100;
							}else{
								$cu->account_balance = number_format(($total_nedeed-$total_account_with_discount),2)*100;
							}
						}
					}else{
						$total=$total-20;
						$cu->account_balance = number_format(($total*$subscription['tax_percent']/100+$total),2)*100;
					}
					$cu->save();
				}
			}
			break;
	}

	header('X-PHP-Response-Code: 200', true, 200);
	exit();

?>



