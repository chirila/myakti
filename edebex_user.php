<?php
define('INSTALLPATH',__DIR__.'/');
define('LAYOUT',__DIR__.'/layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_project.log');
set_time_limit(0);
include_once(__DIR__."/core/startup.php");
global $config,$database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
); 
$db_users= new sqldb($db_config);
$response = json_decode( @file_get_contents( "php://input" ), true );

	$db_users->query("INSERT INTO edebex_log SET response='".serialize($response)."', date='".time()."', type='0' ");

	$user=$db_users->field("SELECT user_id FROM users WHERE email='".$response['email']."' AND main_user_id='0'");
	if($user){
		$user_db=$db_users->field("SELECT `database_name` FROM users WHERE user_id='".$user."' ");

		$db_user = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $user_db,
		); 
		$db= new sqldb($db_user);

		$edebex_id=$db->field("SELECT app_id FROM apps WHERE name='Edebex' AND type='main' AND main_app_id='0' ");
		$db->query("UPDATE apps SET api='".$response['token']."' WHERE app_id='".$edebex_id."' ");
		$db->query("INSERT INTO apps SET api='".$response['email']."', main_app_id='".$edebex_id."', type='email' ");

		$mail = new PHPMailer();
		$mail->WordWrap = 50;
		$fromMail='support@akti.com';
		$email = $response['email'];
		$body='Hi,

			Your registration on Edebex has been successfully completed.
			Your personal token associated with your email address is '.$response['token'].'. 

			You can now login to your <a href="my.akti.com">Akti</a> account and use Edebex services.

			Regards,

			The Akti Team';
		$subject = 'Akti - Edebex registration complete';

		$table = '<table style="background:#f6f6f6; width:100%; margin:0; padding:0; " border="0" cellspacing="0" cellpadding="50" >
				<tbody>
					<tr ><td style="border-collapse:collapse; " >
						<table style="margin:0 auto; padding:0; background:#FFFFFF;" width="600" border="0" cellspacing="0" cellpadding="0" >
							<tbody>
								<tr style="background:#FFF;"><td style="padding: 0px;"><img src="http://www.akti.com/images/logo_newsletter.png"></td></tr>
								<tr style=""><td style="width=100%; padding:20px; border-bottom:1px solid #dcdcdc; border-right:1px solid #dcdcdc; border-left:1px solid #dcdcdc; border-collapse: collapse;">'.nl2br($body).'
								</td></tr>
							</tbody>
						</table>
					</tr></td>
				</tbody>
			</table>';
		$body = $table;
		$body = str_replace("\n", '', $body);
    	$body = str_replace("\t", '', $body);

    	$mail->Subject = $subject;
		$mail->SetFrom($fromMail, $fromMail);
		$mail->MsgHTML(nl2br($body));
		$mail->AddAddress($email);
		$mail->Send();

	}

	header('X-PHP-Response-Code: 200', true, 200);
	exit();

?>