<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * -------------------------------------------------------------------
 * 	DATABASE CONNECTION SETTINGS
 * -------------------------------------------------------------------
 * This file contains the settings needed for you to access your database.
 * For more information read the documentation.
 *
 * -------------------------------------------------------------------
 * 	HELP
 * -------------------------------------------------------------------
 * All the settings are kept in an associative array.
 * The $database_config['default'] key contains the name of the default database driver,
 * by default this is mysql.
 *
 * The $database_config['allow_multiple_drivers'] keys determines if multiple drivers
 * are allowed.
 *
 * The $database_config also contains all the settings required to connect to the database
 * grouped by the driver name. The expected keys in this array are:
 * 		[hostname]			The hostname of your database server.
 * 		[username]			The username used to connect to the database
 * 		[password]			The password used to connect to the database
 * 		[database]			The name of the database you want to connect to
 * 		[tbl_prefix]		You can add an optional prefix, which will be added
 *							to the table name
 * 		[exit_on_error]     TRUE/FALSE - Wheather to exit if an sql error occures or not
 * 		[create_new]        TRUE/FALSE - Wheather to recycle the existing connection or always
 * 							start with a new one, by default the active connection is reused.
 * Depending on what database platform you are using (MySQL, Postgres, etc.) not all values will be needed.
 */
$database = $config['db_base'];

$database_config['mysql'] = array(
 	'hostname'   => $config['db_host'],
 	'username'   => $config['db_user'],
 	'password'   => $config['db_pass'],
 	'database'   => $database,
 	'tbl_prefix' => '',
);
$database_config['user_db'] = $config['db_users'];
$db_users = array(
 	'hostname'   => $config['db_host'],
 	'username'   => $config['db_user'],
 	'password'   => $config['db_pass'],
	'database'   => $database_config['user_db'],
	'tbl_prefix' => '',
);
if($_COOKIE['remember_token'] && !empty($_COOKIE['remember_token']) && !$_SESSION['u_id']){
	setcookie("username",$in['username'],time()-60*60*24*365);
	$conn = new sqldb($db_users);
	$token_e=$conn->field("SELECT user_id FROM remember_token WHERE `token`='".$_COOKIE['remember_token']."' AND expire_date>'".time()."' ");
	if($token_e){
		require_once(APPSPATH.'auth/model/auth.php');
		$auth = new auth($in);
		$auth->login_token($in,$token_e);
	}
	$conn->query("DELETE FROM remember_token WHERE expire_date<'".time()."' ");
}
if(isset($_SESSION['u_id'])){






	$db = new sqldb($db_users);
	$db->query("DELETE FROM remember_token WHERE expire_date<'".time()."' ");
//$dt=aktiUser::Instance();
//$database=$dt->get('databasename');
	$uinfo = $db->query("SELECT database_name,`ssl` FROM users WHERE user_id ='".$_SESSION['u_id']."' ");
	$database = $uinfo->f('database_name');

	if($database){
		$database_config['mysql']['database'] = $database;
		/*if($uinfo->f('ssl') == 1){
			$database_config['mysql']['hostname'] = 'salesassist-encrypted.cknyx1utyyc1.eu-west-1.rds.amazonaws.com';
			$database_config['mysql']['username'] = 'admin';
			$database_config['mysql']['password'] = 'hU2Qrk2JE9auQA';
		}*/
	}else{
		// $in['do'] = $default_app[ENVIRONMENT].'-'.$default_controller[ENVIRONMENT];
	}
}
define('DATABASE_NAME',$database);

$database_config['default'] = 'mysql';
$database_config['allow_multiple_drivers'] = false;
/* End of file database.php */