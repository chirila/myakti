<?php

return array(
    // Bootstrap the configuration file with AWS specific features
    'includes' => array('_aws'),
    'services' => array(
        // All AWS clients extend from 'default_settings'. Here we are
        // overriding 'default_settings' with our default credentials and
        // providing a default region setting.
        'default_settings' => array(
            'params' => array(
                'key'    => (defined('AWS_FACTORY_KEY') ? AWS_FACTORY_KEY : ''),
                'secret' => (defined('AWS_FACTORY_SECRET') ? AWS_FACTORY_SECRET : ''),
                // 'region' => 'us-west-1'
            )
        ),
        // 'cloudfront' => array(
        //     'extends' => 'cloudfront',
        //     'params'  => array(
        //         'private_key' => 'pk-APKAJ2HJL5KBT6Q3KTRA.pem',
        //         'key_pair_id' => 'APKAJ2HJL5KBT6Q3KTRA'
        //     )
        // )
    )
);
# 8{NZ^m230T8b
?>