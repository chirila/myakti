<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * Website Routes
 * --------------------------------------------------------------------------
  */

$routes = array(
    'template' => array(
        'method' => "GET",
        'do' => 'maintenance-template',
        'params' => 'filename'
    ),
    'otemplate' => array(
        'method' => "GET",
        'do' => 'order-template',
        'params' => 'filename'
    ),
    'potemplate' => array(
        'method' => "GET",
        'do' => 'po_order-template',
        'params' => 'filename'
    ),
    'stocktemplate' => array(
        'method' => "GET",
        'do' => 'stock-template',
        'params' => 'filename'
    ),
    /* deprecated
    'pager' => array(
        'method' => "GET",
        'do' => 'misc-pager',
        'params' => ''
    ),*/
    'planning_gb' => array(
        'method' => "GET",
        'do' => 'maintenance-serviceTs',
        'params' => ''
    ),
    'miscTemplate' => array(
        'method' => "GET",
        'do' => 'misc-template',
        'params' => 'filename'
    ),
    'authTemplate' => array(
        'method' => "GET",
        'do' => 'auth-template',
        'params' => 'filename'
    ),
    'iTemplate' => array(
        'method' => "GET",
        'do' => 'invoice-template',
        'params' => 'filename'
    ),
    'EditCustomerModal' => array(
        'method' => "GET",
        'do' => 'misc-EditCustomerModal',
        'params' => 'filename'
    ),
    'EditContactModal' => array(
        'method' => "GET",
        'do' => 'misc-EditContactModal',
        'params' => 'filename'
    ),
    'qTemplate' => array(
        'method' => "GET",
        'do' => 'quote-template',
        'params' => 'filename'
    ),
    'atemplate' => array(
        'method' => "GET",
        'do' => 'article-template',
        'params' => 'filename/loadController/form_type'
    ),
    'cTemplate' => array(
        'method' => "GET",
        'do' => 'customers-template',
        'params' => 'filename/loadController/id'
    ),
    'sTemplate' => array(
        'method' => "GET",
        'do' => 'settings-template',
        'params' => 'filename'
    ),
    'settingsEdit' => array(
        'method' => "GET",
        'do' => 'settings-settingsEdit',
        'params' => 'filename'
    ),
    'customer' => array(
        'method' => "GET",
        'do' => 'customers-customer',
        'params' => 'filename'
    ),
    'identityEdit' => array(
        'method' => "GET",
        'do' => 'settings-identityEdit',
        'params' => 'filename'
    ),
    'pTemplate' => array(
        'method' => "GET",
        'do' => 'project-template',
        'params' => 'filename'
    ),
    'cashTemplate' => array(
        'method' => "GET",
        'do' => 'cashregister-template',
        'params' => 'filename'
    ),
    'confirm' => array(
        'method' => "GET",
        'do' => 'auth-confirm',
        'params' => 'code'
    ),
    'insTemplate' => array(
        'method' => "GET",
        'do' => 'installation-template',
        'params' => 'filename'
    ),
    'conTemplate' => array(
        'method' => "GET",
        'do' => 'contract-template',
        'params' => 'filename'
    ),
    'rTemplate' => array(
        'method' => "GET",
        'do' => 'report-template',
        'params' => 'filename'
    ),
    'tTemplate' => array(
        'method' => "GET",
        'do' => 'timetracker-template',
        'params' => 'filename'
    ),

);

if(INSTALLPATH == '../'){
    $base = $config['install_path'].ENVIRONMENT.'/';
} else {
    $base = $config['install_path'];
}

$router = new arkRouter($routes,$base);

/*
$router->add('myaccount', 'GET', 'home-my_account', 'user_id');
$router->add('user', 'GET', 'home-user', '/user_id/test/');
*/

$router->run();