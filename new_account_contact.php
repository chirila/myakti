<?php
define('INSTALLPATH',__DIR__.'/');
define('BASEPATH',1);
ini_set('memory_limit','512M');
ignore_user_abort(true);
set_time_limit(0);

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/
include_once(__DIR__."/core/startup_cron.php");
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_error.log');
	
	if(!$in['lang_code']){
		$in['lang_code']='en';
	}
	$v = new validation($response);
	$v->field('contact-firstname', gm('Firstname'), 'required');
	$v->field('contact-lastname', gm('Lastname'), 'required');
	$v->field('contact-email', gm('Email'), 'required:min_length[6]');
	$v->field('contact-textarea', gm('Content'), 'required');

	if(!$v->run()){
        $data=array('data'=>$response,'error'=>msg::get_errors());
		echo json_encode($data);
		exit();
	};
	require_once (__DIR__.'/libraries/class.phpmailer.php');
    include_once (__DIR__.'/libraries/class.smtp.php');
	$mail = new PHPMailer();
	$mail->WordWrap = 50;
	$fromMail=$in['contact-email'];
	$mail->SetFrom($fromMail, utf8_decode($in['contact-lastname'].' '.$in['contact-firstname']));	
	$body=utf8_decode($in['contact-textarea']);
	$mail->Subject = 'Contact from website';
	$mail->MsgHTML($body);
	$mail->AddAddress('sales@akti.com');
	$mail->Send();
	switch($in['lang_code']){
		case 'nl':
		case 'du':
			$success_msg='Bericht verzonden, we contacteren u zo spoedig mogelijk.';
			break;
		case 'fr':
			$success_msg='Message envoyé. Nous vous contacterons aussi vite que possible.';
			break;
		default:
			$success_msg='Message sent, we will contact you shortly.';
			break;
	}
	$data=array('data'=>$response,'success'=>array('success'=>$success_msg));
	echo json_encode($data);
	exit();

?>