var app = angular
    .module('akti', [
        'ui.router',
        'ngAnimate',
        'ui.bootstrap',
    ])
    .config(['$urlRouterProvider', '$stateProvider','$locationProvider', function($urlRouterProvider, $stateProvider, $locationProvider){
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('services', {
                url: '/',
                templateUrl: 'template/services',
                controller: 'servicesCtrl'
            }).state('service', {
                url: '/service',
                templateUrl: 'template/service',
                controller: 'serviceCtrl'
            });

        $locationProvider.html5Mode(true);
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }])
    .run(['$http', '$rootScope',
        function($http, $rootScope) {
            /*$http.get('index.php?do=languages').then(function (response) {
                $rootScope.languages = response.data.data.languages;
                $rootScope.lang_id = response.data.data.lang_id;
            }, function (err) {
                console.log(err);
                return false;
            });*/

            /*$rootScope.summerNoteConfig = {
                height: 150,
                focus: false,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['misc', ['fullscreen', 'codeview', 'undo', 'redo']],

                ]
            };*/

    }]);
