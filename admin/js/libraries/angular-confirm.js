/*
 * angular-confirm
 * https://github.com/Schlogen/angular-confirm
 * @version v1.2.2 - 2015-12-08
 * @license Apache
 */
!function(n,e){"use strict";if("function"==typeof define&&define.amd)
// AMD. Register as an anonymous module.
  define(["angular"],e);else{if("undefined"==typeof module||"object"!=typeof module.exports)
// in the case of no module loading system
// then don't worry about creating a global
// variable like you would in normal UMD.
// It's not really helpful... Just call your factory
  return e(n.angular);
// CommonJS support (for us webpack/browserify/ComponentJS folks)
  module.exports=e(require("angular"))}}(this,function(n){n.module("angular-confirm",["ui.bootstrap.modal"]).controller("ConfirmModalController",["$scope","$uibModalInstance","data",function(e,t,o){e.data=n.copy(o),e.ok=function(){t.close()},e.cancel=function(){t.dismiss("cancel")}}]).value("$confirmModalDefaults",{template:'<div class="modal-header"><h3 class="modal-title">{{data.title}}</h3></div><div class="modal-body">{{data.text}}</div><div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">{{data.ok}}</button><button class="btn btn-default" ng-click="cancel()">{{data.cancel}}</button></div>',controller:"ConfirmModalController",defaultLabels:{title:"Confirm",ok:"OK",cancel:"Cancel"}}).factory("$confirm",["$uibModal","$confirmModalDefaults",function(e,t){return function(o,i){var c=n.copy(t);return i=n.extend(c,i||{}),o=n.extend({},i.defaultLabels,o||{}),"templateUrl"in i&&"template"in i&&delete i.template,i.resolve={data:function(){return o}},e.open(i).result}}]).directive("confirm",["$confirm",function(e){return{priority:1,restrict:"A",scope:{confirmIf:"=",ngClick:"&",confirm:"@",confirmSettings:"=",confirmTitle:"@",confirmOk:"@",confirmCancel:"@"},link:function(t,o,i){o.unbind("click").bind("click",function(o){if(o.preventDefault(),n.isUndefined(t.confirmIf)||t.confirmIf){var i={text:t.confirm};t.confirmTitle&&(i.title=t.confirmTitle),t.confirmOk&&(i.ok=t.confirmOk),t.confirmCancel&&(i.cancel=t.confirmCancel),e(i,t.confirmSettings||{}).then(t.ngClick)}else t.$apply(t.ngClick)})}}}])});