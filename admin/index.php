<?php

ini_set('post_max_size', '15M');
ini_set('upload_max_filesize', '15M');

define('INSTALLPATH','../');
define('ENVIRONMENT','admin');
define('LAYOUT',INSTALLPATH.'layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

require(BASEPATH.'startup.php');

require(BASEPATH.'engine.php');

/* End of file index.php */