<?php

define('INSTALLPATH','../');
define('ENVIRONMENT','admin');
define('LAYOUT',INSTALLPATH.'layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

require(BASEPATH.'startup.php');
require(BASEPATH.'engine.php'); 
