<?php
define('INSTALLPATH','');
define('ENVIRONMENT','front');
define('LAYOUT','layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');
ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('error_log','pim_error.log');

include_once(BASEPATH.'startup.php');
require(BASEPATH.'engine.php');

/* End of file index.php */