<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');

include_once(BASEPATH.'/library/console.php');
include_once(INSTALLPATH.'config/config.php');

include_once(INSTALLPATH.'config/apps.php');
include_once(INSTALLPATH.'config/database.php');

include_once(BASEPATH.'/library/validation.php');

include_once(BASEPATH.'/library/perm.php');

include_once(BASEPATH.'/library/msg.php');

include_once(INSTALLPATH.'instal/ark.php');

include_once(BASEPATH.'/library/at.php');
include_once(BASEPATH.'/library/database_store.php');
include_once(BASEPATH.'/library/sqldb.php');
include_once(BASEPATH.'/library/sqldbResult.php');

ark::loadLibraries();
define('DEBUG', $config['debug']);
    
if (DEBUG) {
    error_reporting (E_ALL);
    ini_set('display_errors', 1);
    ini_set('error_reporting', 1);
}
/* End of file common.php */