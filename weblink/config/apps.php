<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * Website Apps
 * --------------------------------------------------------------------------
 * Every app in the apps folder needs to be registered here, so the framework know where to look for it
 */

define("USE_APPS", true);
define("USE_ENVIRONMENT", true);

$apps['auth'] = 1;
$apps['instal']=1;
$apps['invoice'] = 1;
$apps['misc'] = 1;
$apps['quote'] = 1;
$apps['project'] = 1;
$apps['order'] = 1;

$auth_app = 'auth';
$auth_controller = 'login';

$default_app['front'] = 'invoice';
$default_controller['front'] = 'invoice';

$default_app['admin'] = 'invoice';
$default_controller['admin'] = 'invoice';

/*Pagination settings **/
// define("ROW_PER_PAGE", 10);

/* Table prefix */
define("TABLE_PREFIX", "pim_");
/* End of file apps.php */