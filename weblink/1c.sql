﻿/*
SQLyog Enterprise - MySQL GUI v6.03
Host - 5.1.61 : Database - 40245361_e0e8_4746_8154_bd39b79b47a5
*********************************************************************
Server version : 5.1.61
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `cms_content_box` */

DROP TABLE IF EXISTS `cms_content_box`;

CREATE TABLE `cms_content_box` (
  `content_box_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `headline` text NOT NULL,
  `content` text NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `tag` varchar(255) NOT NULL DEFAULT '',
  `content_template_id` int(11) NOT NULL DEFAULT '0',
  `mode` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`content_box_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_content_template` */

DROP TABLE IF EXISTS `cms_content_template`;

CREATE TABLE `cms_content_template` (
  `content_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  PRIMARY KEY (`content_template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_menu` */

DROP TABLE IF EXISTS `cms_menu`;

CREATE TABLE `cms_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `template_file_v` varchar(255) NOT NULL DEFAULT '',
  `template_file_h` varchar(255) NOT NULL DEFAULT '',
  `css_file_h` varchar(255) NOT NULL DEFAULT '',
  `css_file_v` varchar(255) NOT NULL DEFAULT '',
  `tag_h` varchar(255) NOT NULL DEFAULT '',
  `tag_v` varchar(255) NOT NULL DEFAULT '',
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `h_version` tinyint(4) NOT NULL DEFAULT '0',
  `v_version` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_menu_link` */

DROP TABLE IF EXISTS `cms_menu_link`;

CREATE TABLE `cms_menu_link` (
  `menu_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `sort_order` tinyint(4) NOT NULL DEFAULT '0',
  `target` varchar(255) DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_link_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_menu_submenu` */

DROP TABLE IF EXISTS `cms_menu_submenu`;

CREATE TABLE `cms_menu_submenu` (
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `menu_link_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `cms_menu_template_file` */

DROP TABLE IF EXISTS `cms_menu_template_file`;

CREATE TABLE `cms_menu_template_file` (
  `menu_template_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `file_name` varchar(40) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`menu_template_file_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_page_type` */

DROP TABLE IF EXISTS `cms_page_type`;

CREATE TABLE `cms_page_type` (
  `page_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `template_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  PRIMARY KEY (`page_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_page_type_czone` */

DROP TABLE IF EXISTS `cms_page_type_czone`;

CREATE TABLE `cms_page_type_czone` (
  `page_type_czone_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_type_id` int(11) NOT NULL DEFAULT '0',
  `template_czone_id` int(11) NOT NULL DEFAULT '0',
  `default_data` text NOT NULL,
  `mode` tinyint(4) NOT NULL DEFAULT '1',
  `prefilled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_type_czone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_tag_library` */

DROP TABLE IF EXISTS `cms_tag_library`;

CREATE TABLE `cms_tag_library` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `file_name` varchar(255) DEFAULT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_template` */

DROP TABLE IF EXISTS `cms_template`;

CREATE TABLE `cms_template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_template_czone` */

DROP TABLE IF EXISTS `cms_template_czone`;

CREATE TABLE `cms_template_czone` (
  `template_czone_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  PRIMARY KEY (`template_czone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_web_page` */

DROP TABLE IF EXISTS `cms_web_page`;

CREATE TABLE `cms_web_page` (
  `web_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_type_id` int(11) NOT NULL DEFAULT '0',
  `template_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  `date` varchar(20) NOT NULL DEFAULT '',
  `no_delete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`web_page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=258 DEFAULT CHARSET=latin1;

/*Table structure for table `cms_web_page_content` */

DROP TABLE IF EXISTS `cms_web_page_content`;

CREATE TABLE `cms_web_page_content` (
  `web_page_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `web_page_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` tinyint(4) NOT NULL DEFAULT '0',
  `date` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `subtitle` varchar(255) DEFAULT NULL,
  `headline` text,
  `content` longtext CHARACTER SET utf8 NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `template_czone_id` int(11) NOT NULL DEFAULT '0',
  `content_template_id` int(11) NOT NULL DEFAULT '0',
  `mode` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`web_page_content_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1125 DEFAULT CHARSET=latin1;

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `msg` varchar(255) NOT NULL DEFAULT '',
  `text_en` varchar(255) NOT NULL DEFAULT '',
  `text_nl` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`msg`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `constant_name` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) DEFAULT NULL,
  `long_value` text,
  `module` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `sys_message` */

DROP TABLE IF EXISTS `sys_message`;

CREATE TABLE `sys_message` (
  `sys_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `from_email` varchar(255) NOT NULL DEFAULT '',
  `from_name` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`sys_message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_level` tinyint(4) NOT NULL DEFAULT '2',
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;