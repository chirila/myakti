$(function(){

	$(window).scroll(function () {
    var y = $(window).scrollTop();
    if(y>z.top){
     $('.poweredby').addClass('addfloat');

    }else{
      $('.poweredby').removeClass('addfloat');
    }
  });

  $('.activity').on('focus','.textarea_logging',function(){
    setTimeout(function(){ setX(); }, 300);
  }).on('click','.comment_cancel',function(e){
    setTimeout(function(){ setX(); }, 300);
  }).on('click','.add_c',function(e){
    setTimeout(function(){ setX(); }, 300);
  });
  setX();


	$('#accept').on('click',function(e){

		$( "#dialog" ).dialog({
			modal: true,
			width: "400px"
		});

		e.preventDefault();
		$('.stars').removeClass('hide');
	});

	$('#comm').on('click',function(e){

		$( "#dialog1" ).dialog({
			modal: true,
			width: "400px"
		});

	});

	$('.star').click(function(){
		var a = $('#accepted').val();
		if(a==1){
			$('.star').removeClass('blue_star');
			$(this).addClass('blue_star').prevAll().addClass('blue_star');
			$('#stars').val( $(this).index() );
		}
	});

	$('.cancel_star').on('click',function(e){
		e.preventDefault();
		$('.stars').addClass('hide');
		$('.star').removeClass('blue_star');
		$( "#dialog" ).dialog('close');
		$('#stars').val( 0 );
	});

	$('.add_star').on('click',function(){
		var star_c = $('#star_c').val(), stars = $('#stars').val(),txt = $('#stars_textarea').val();
		if(star_c>0 && star_c >= stars && txt==""){
			alert("Comment required");
			return false;
		}
		if(!confirm('This will close the maintenance sheet, are you sure?')){
			return false;
		}
	});

	$('.wlink_options').on('click',function(e){
		e.preventDefault();
		$('.wlink_controls').toggleClass("open-menu");
		// $('.wlink_main').toggleClass("make_room");
		$('.buttons-main li').removeClass('modal_pos')
		$('#buttons_modal').css({top: '-1000px'});
    });

});