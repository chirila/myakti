<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $config;
$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.time());

$service = $db->query("SELECT * FROM servicing_support WHERE service_id='".$in['invoice_id']."'");
if(!$service->move_next()){
	$view->assign(array('data'=>false));
	return $view->fetch();
}
$date = date(ACCOUNT_DATE_FORMAT,$service->f('planeddate'));
if($service->f('startdate')){
	$date .= ' - '.number_as_hour($service->f('startdate'));
}
$currency = '';
if($service->f('project_id')){
	$currency = get_currency($db->field("SELECT currency_type FROM projects WHERE project_id='".$service->f('project_id')."' "));
}

switch ($service->f('status')) {
	case '1':
		$status = 'In progress';
		$classbg = 'quote_sent';
		break;
	case '2':
		$status = 'Done';
		$classbg = '';
		break;
	default:
		$status = 'Draft';
		$classbg = '';
		break;
}
$u = array();
$users = $db->query("SELECT * FROM servicing_support_users WHERE service_id='".$in['service_id']."' ORDER BY name ASC ");
while($users->next()){
	$u[$users->f('user_id')] = $users->f('name');
	$view->assign(array(
		'name' 		=> $users->f('name'),
		'id'		=> $users->f('u_id'),
		'pr_m'		=> $users->f('pr_m') == 1 ? 'CHECKED' : '' ,
		'sign'		=> count($u) <= 1 ? '' : ' / ',
	),'users');
	$view->loop('users');
}
$total_h = 0;
$sheet = $db->query("SELECT servicing_support.*,servicing_support_sheet.*, servicing_support_sheet.date AS date2,servicing_support_sheet.user_id AS u_id
					FROM servicing_support_sheet
					INNER JOIN servicing_support ON servicing_support_sheet.service_id=servicing_support.service_id
					WHERE servicing_support_sheet.service_id='".$in['invoice_id']."' ");
while ($sheet->next()) {
	$view->assign(array(
		'DATE'						=> date(ACCOUNT_DATE_FORMAT,$sheet->f('date2')),
		'hours'						=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break')),
		'break'						=> number_as_hour($sheet->f('break')),
		'id'						=> $sheet->f('id'),
		'service_id'				=> $sheet->f('service_id'),
		'tmsmp'						=> $sheet->f('date2'),
		'start_time'				=> number_as_hour($sheet->f('start_time')),
		'end_time'					=> number_as_hour($sheet->f('end_time')),
		'user'						=> $u[$sheet->f('u_id')]
	),'intervention');
	$view->loop('intervention');
	$total_h += $sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break');
}
$total_exp = 0;
$is_exp = false;
$expense = $db->query("SELECT project_expenses.*, expense.name AS e_name,
							expense.unit_price, expense.unit
					   FROM project_expenses
					   INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
					   INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
					   WHERE project_expenses.service_id='".$in['invoice_id']."' ");
while ($expense->next()) {
	$is_exp = true;
	$amount = place_currency(display_number($expense->f('amount')));
	$a = $expense->f('amount');
	if($expense->f('unit_price')){
		$amount = place_currency(display_number(($expense->f('amount') * $expense->f('unit_price'))))." (".$expense->f('amount')." ".$expense->f('unit').")";
		$a = $expense->f('amount')* $expense->f('unit_price');
	}
	console::log($amount);
	$view->assign(array(
		'DATE'						=> date(ACCOUNT_DATE_FORMAT,$expense->f('date')),
		'name'						=> $expense->f('e_name'),
		'hours'						=> $amount,
	),'intervention_exp');
	$view->loop('intervention_exp');
	$total_exp += $a;
}
$allow_star_rating = $db->field("SELECT value FROM settings WHERE constant_name ='ALLOW_STARS_RATING' ");

$lgs=$db->query("SELECT * FROM pim_lang WHERE active=1 ORDER BY sort_order ");
 while($lgs->move_next()){
 	if($lgs->f('default_lang')){
 		$default_lang = $lgs->f('lang_id');
 	}
}
$lg = $default_lang;
if($service->f('email_language')){
	$lg = $service->f('email_language');
}
$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id='".$lg."'")->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
	if( ($j % 2) == 0){
		$j++;
		continue;
	}
	$labels[$key] = $value;
	$j++;
}
$language = $db->field("SELECT * FROM pim_lang WHERE active=1 AND default_lang='1'");
if($service->f('email_language')){
	$language = $service->f('email_language');
}

$view->assign(array(
	'is_exp'				=> $is_exp,
	'total_h'				=> number_as_hour($total_h),
	'total_exp'				=> display_number($total_exp),
	'data'=>true,
	'invoice_seller_name'	=>$service->f('customer_name'),
	'factur_nr'				=>$service->f('serial_number'),
	'factur_date'			=>$date,
	'invoice_seller_b_address'=>$service->f('project_name'),
	'invoice_seller_b_zip'	=>$service->f('task_name'),
	'invoice_seller_b_country'=>$service->f('contract_name'),
	'invoice_vat_no'		=> $service->f('subject'),
	'STATUS'				=> $status,
	'class_lost'			=>$classbg,
	'intervention_report'	=> $service->f('report'),
	'hide_stars'			=> $service->f('accept') == 1 ? '' : 'hide',
	'star_c'				=> defined('STARS_COMMENTS') ? STARS_COMMENTS : 0,
	'accepted'				=> $service->f('accept') == 1 ? false : true,
	'show_comm'				=> obj::get('allow_comments') == '1' ? true : false,
	'allow_star_rating'		=> $allow_star_rating == 1 ? true : false,
	'bankDetails_txt'			=>	getq_weblink_label_text('bankDetails_webl', $language),
	'bank_txt'			  	=> $labels['bank_name'],
	'bicCode_txt'			=> 	getq_weblink_label_text('bicCode_webl', $language),
	'ibanCode_txt'			=> 	getq_weblink_label_text('ibanCode_webl', $language),
	'bank'				 => ACCOUNT_BANK_NAME,
	'bic'				  	=> ACCOUNT_BIC_CODE,
	'iban'				 => ACCOUNT_IBAN,
));

for ($i=1; $i <6 ; $i++) {
	$view->assign(array(
		'blue' => $service->f('rating') >= $i ? 'blue_star' : '',
	),'stars');
	$view->loop('stars');
}



$tasks =0;
	$task = $db->query("SELECT * FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' ORDER BY task_name ASC ");
	while($task->next()){
		$view->assign(array(
			'name' 			=> $task->f('task_name'),
			'id'			=> $task->f('task_id'),
			'checked'		=> $task->f('closed') == 1 ? "CHECKED" : '',
			'comment'		=> nl2br($task->f('comment')),
			'closed'		=> $task->f('closed') == 1 ? true : false,
		),'task');
		$view->loop('task');
		$tasks++;
	}

	$expens=0;
	$task = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY name ASC ");
	while($task->next()){
		$view->assign(array(
			'name' 			=> $task->f('name'),
			'id'			=> $task->f('a_id'),
			'quantity'		=> display_number($task->f('quantity')),
			'to_deliver'	=> display_number($task->f('to_deliver')),
			'delivered'		=> display_number($task->f('delivered')),
			'price'			=> place_currency(display_number($task->f('price')*$task->f('margin')/100+$task->f('price')),$currency),
		),'expenses');
		$view->loop('expenses');
		$expens++;
	}

	$article=0;
	$task = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY name ASC ");
	while($task->next()){
		$view->assign(array(
			'name' 			=> $task->f('name'),
			'id'				=> $task->f('a_id'),
			'quantity'			=> display_number($task->f('quantity')),
			'to_deliver'		=> display_number($task->f('to_deliver')),
			//'delivered'			=> display_number($task->f('delivered')),
			'price'			=> place_currency(display_number($task->f('price')),$currency),
		),'articles');
		$view->loop('articles');
		$article++;
	}

return $view->fetch();