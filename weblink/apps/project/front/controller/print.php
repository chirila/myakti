<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $config;
$db5 = new sqldb();
$quote_data = $db5->query("SELECT serial_number FROM servicing_support WHERE service_id='".$in['service_id']."'  ");

ark::loadLibraries(array('aws'));
$aws = new awsWrap(obj::get('db'));
$link =  $aws->getLink($config['awsBucket'].obj::get('db').'/intervention/intervention_'.$in['service_id'].'.pdf');
$name = $quote_data->f('serial_number').'.pdf';
$content = @file_get_contents($link);
if($content){
	$file_len = strlen($content);
	header('Content-Type: application/pdf');
	header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
	header('Pragma: public');
	header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
	header("X-Robots-Tag: noindex,nofollow,noarchive");
	if($in['dw']==1){
		header('Content-Description: File Transfer');
		// force download dialog
		if (strpos(php_sapi_name(), 'cgi') === false) {
			header('Content-Type: application/force-download');
			header('Content-Type: application/octet-stream', false);
			header('Content-Type: application/download', false);
			header('Content-Type: application/pdf', false);
		} else {
			header('Content-Type: application/pdf');
		}
		header('Content-Disposition: attachment; filename="'.basename($name).'";');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.$file_len);
	}
	echo $content;
	exit();
}else{
	global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_user = new sqldb($db_config);
    $user_id = $db_user->field("SELECT user_id FROM users WHERE database_name='".obj::get('db')."' AND active='1' AND default_admin='1' LIMIT 1");
	ark::loadLibraries(array('clientREST'));
	$vars = array();
	$vars['username'] = 'admin:'.$user_id;
	$vars['do'] = 'auth--auth-login';
	$vars['password'] = 'Umckt%78AXw1E6Fqv!Do';
	$c = new clientREST();
	$response = $c->execRequest($config['admin_site_url'].'pim/admin/index.php','post',$vars);

	$vars = array();
	$vars['do'] = 'maintenance--maintenance-gpasta';
	$vars['service_id'] = $in['service_id'];
	$response = $c->execRequest($config['admin_site_url'].'pim/admin/index.php','get',$vars);
	$file_len = strlen($response);
	header('Content-Type: application/pdf');
	header("X-Robots-Tag: noindex,nofollow,noarchive");
	if($in['dw']==1){
		header('Content-Description: File Transfer');
		// force download dialog
		if (strpos(php_sapi_name(), 'cgi') === false) {
			header('Content-Type: application/force-download');
			header('Content-Type: application/octet-stream', false);
			header('Content-Type: application/download', false);
			header('Content-Type: application/pdf', false);
		} else {
			header('Content-Type: application/pdf');
		}
		header('Content-Disposition: attachment; filename="'.basename($name).'";');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.$file_len);
	}
	echo $response;
	exit();
}
?>