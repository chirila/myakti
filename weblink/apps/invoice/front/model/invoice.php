<?php
	/**
	 * the quote class
	 *
	 */
class invoice{

	var $db;
	var $db_users;
	var $pag = 'invoice'; 						# used for log messages
	var $field_n = 'invoice_id';					# used for log messages

	/**
	 * constructor
	 */
	function __construct(){
		global $database_config;
		$this->db = new sqldb();

		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		); 
		$this->db_users= new sqldb($db_config);
	}

	function mollie_pay(&$in){
		global $config;
		global $database_config;

		$ch = curl_init();
		$api_key=$this->db->field("SELECT api FROM apps WHERE name='Mollie' AND type='main' AND main_app_id='0'");
		$headers=array('Content-Type: application/json','Authorization: Bearer '.$api_key);
		$inv_data=$this->db->query("SELECT amount_vat,serial_number FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

		$mollie_data=array(
			'amount'		=> $inv_data->f('amount_vat'),
			'description'	=> gm('Mollie payment for invoice').' '.$inv_data->f('serial_number'),
			'redirectUrl'	=> $config['site_url'].'?do=invoice-mollie_ok&q='.$in['q'],
			'webhookUrl'	=> $config['admin_site_url'].'mollie_hook.php',
			'metadata'		=> array(
									'invoice_id' 	=> $in['invoice_id'],
									'serial_number'	=> $inv_data->f('serial_number')
							   )
		);

		$mollie_data = json_encode($mollie_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $mollie_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['mollie_base_url'].'/payments');

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	/*console::log($put);
    	console::log('space');
    	console::log($info);*/

    	if($info['http_code']>300 || $info['http_code']==0){
    		msg::$error=gm("Invalid credentials");
    	}else{  		
    		$resp=json_decode($put);
    		$in['redirect']=$resp->links->paymentUrl;
    		$this->db->query("UPDATE tblinvoice SET mollie_id='".$resp->id."' WHERE id='".$in['invoice_id']."' ");
    		$this->db_users->query("INSERT INTO mollie_payments SET mollie_id='".$resp->id."', `database`='".$database_config['mysql']['database']."' ");
    		msg::$success=gm("Success");
    	}
    	return true;
	}
	  function pay(&$in){
		$tblinvoice_new=$this->db->query("SELECT * FROM tblinvoice WHERE  id='".$in['invoice_id']."'");
		$in['payment_date'] = $tblinvoice_new->f('sent_date');
		
    $lines = $this->db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
    while ($lines->next()) {
          $line_discount = $lines->f('discount');
          if($tblinvoice_new->f('apply_discount') ==0 || $tblinvoice_new->f('apply_discount') == 2){
            $line_discount = 0;
          }
          $amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
          $amount_line_disc = $amount_line*$tblinvoice_new->f('discount')/100;
          $discount_value += $amount_line_disc;
          $subtotal_vat +=  ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
          $total_n += $amount_line - $amount_line_disc;
    }
      $total_all = $total_n+$subtotal_vat;
      $in['amount']   = $total_all;
/*    if(!empty($in['payment_date'])){
      $in['payment_date'] =strtotime($in['payment_date']);
      $in['payment_date'] = mktime(23,59,59,date('n',$in['payment_date']),date('j',$in['payment_date']),date('y',$in['payment_date']));
    }*/
    $total = 0;
    $tblinvoice=$this->db->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE  id='".$in['invoice_id']."'");
    $tblinvoice->next();
    $currency = get_commission_type_list($this->db->f('currency_type'));
    $big_discount = $tblinvoice->f("discount");
    if($tblinvoice->f('apply_discount') < 2){
      $big_discount =0;
    }
    $lines = $this->db->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
    while ($lines->next()) {
      $line_disc = $lines->f('discount');
      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
        $line_disc = 0;
      }
      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
      $line = $line - $line * $big_discount / 100;

      $total += $line + ($line * ( $lines->f('vat') / 100));

    }

    //already payed
    if($tblinvoice->f('proforma_id')){
      $filter = " OR invoice_id='".$tblinvoice->f('proforma_id')."' ";
    }
    $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' {$filter}  AND credit_payment='0' ");
    $this->db->move_next();
    $total_payed = $this->db->f('total_payed');

    $negative = 1;
    if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
      $negative = -1;
    }
    $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' {$filter}  AND credit_payment='1' ");
    $this->db->move_next();
    $total_payed += ($negative*$this->db->f('total_payed'));

    $req_payment_value = $total;
    if($tblinvoice->f('type')==1){
      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
    }

    $amount_due = round($req_payment_value - $total_payed,2);
 //console::log($req_payment_value,$total_payed);
    $in['amount'] = return_value($in['amount']);

    if($in['cost']){
      $in['amount'] += return_value($in['cost']);
    }

    /*if(($in['amount'] > $amount_due) || $in['amount'] == 0){
      msg::$error =  gm('Invalid Amount');
      return false;
    }*/

    $payment_date=date("Y-m-d",$in['payment_date']);

    //Mark as payed
    if($in['amount'] == $amount_due || $in['amount']>$amount_due){
      $this->db->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$in['invoice_id']."'");
      $p = $this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($p){
        $this->db->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
      }
    }
    else {
      $this->db->query("UPDATE tblinvoice SET paid='2' WHERE id='".$in['invoice_id']."'");
    }

    //$info='{l}Payment of{endl}'.'@/'.place_currency(display_number($in['amount']),$currency).'@/'.'{l}successfully recorded by{endl}'.'@/'.get_user_name($_SESSION['u_id']);
    if($in['notes']){
      $in['notes']='  -  '.$in['notes'];
    }

    $no_cost="";
    if($in['cost'] == '0,00'){
      $in['cost']=$no_cost;
    }else{
       $in['cost']='  -  '.$in['cost'];
    }

    $payment_view_date=date(ACCOUNT_DATE_FORMAT,$in['payment_date']);
    $info=$payment_view_date.'  -  '.place_currency(display_number($in['amount']),$currency).$in['notes'].$in['cost'];

    //INSERT PAYMENT
    $coda_pay=0;
    if($in['coda_pay']){
      $coda_pay=$in['coda_pay'];
    }
    $new_pay_id=$this->db->insert("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$in['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$in['amount']."',
                          info        = '".$info."',
                          codapayment_id='".$coda_pay."' ");
    if($tblinvoice->f('bPaid_debt_id') != ''){
      $payment_row=$this->db->query("SELECT * FROM tblinvoice_payments WHERE payment_id='".$new_pay_id."' ")->getAll();
      foreach($payment_row as $key => $value){
        $this->bPaid_payment($value,$tblinvoice->f('bPaid_debt_id'),$tblinvoice->f('serial_number'),1);
      }  
    }

    msg::success(gm('Payment has been successfully recorded'),'success');

    insert_message_log($this->pag,'{l}Payment has been successfully recorded by {endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
    $in['pagl'] = $this->pag;
    if($in['method_pay']==2){
      # this is for import;
    }else if($in['coda_pay']){
      //this is payment from coda so nothing
    }
    elseif($in['from_list'] == 1){
      // $in['do'] = 'invoice-invoices';
      ark::$controller='invoices';
    }else{
      ark::$controller='invoice';
    }
    return true;
  }


}