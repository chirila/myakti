<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');

$path = __DIR__.'/../view/';

if($in['t_type']){
	$html='timesheet_print_'.$in['t_type'].'.html';
}else{
	$html='timesheet_print_'.ACCOUNT_TIMESHEET_PDF_FORMAT.'.html';
}
$view_html = new at($path.$html);
$db = new sqldb();


$img = '../img/no-logo.png';

if($in['logo']){
	$print_logo = $in['logo'];
}else {
	$print_logo=ACCOUNT_LOGO_TIMESHEET;
}
if($print_logo){
	$img = '../'.$print_logo;
	$size = getimagesize($img);
	$ratio = 250 / 77;
	if($size[0]/$size[1] > $ratio ){
		$attr = 'width="250"';
	}else{
		$attr = 'height="77"';
	}
}

if($in['t_preview']){
	$pdf->setQuotePageLabel(gettime_label_txt('page',$in['lid']));
	$all_hours = 480;
	$time_start = 1420434000; # 05/01/2015
	$time_end = 1420952400; # 11/01/2015
	$customer_name = '[CUSTOMER NAME]';
	$user_name = '[CONSULTANT NAME]';



	$view_html->assign(array(
		'account_logo'        		=> $img,
		'attr'				=> $attr,
		'all_hours'				=> number_as_hour($all_hours),
		'timeframe'				=> date(ACCOUNT_DATE_FORMAT,$time_start)." - ".date(ACCOUNT_DATE_FORMAT,$time_end),
		'timeframe_txt'			=> gettime_label_txt('timeframe',$in['lid']),
		'hours_txt'				=> gettime_label_txt('hours',$in['lid']),
		'total_txt'				=> gettime_label_txt('total',$in['lid']),
		'client_txt'			=> gettime_label_txt('client',$in['lid']),
		'project_txt'			=> gettime_label_txt('project',$in['lid']),
		'task_txt'				=> gettime_label_txt('task',$in['lid']),
		'person_txt'			=> gettime_label_txt('person',$in['lid']),
		'customer_txt'			=> gettime_label_txt('customer',$in['lid']),
		'consultant_txt'			=> gettime_label_txt('consultant',$in['lid']),
		'comment_txt'			=> gettime_label_txt('comment',$in['lid']),
		'customer_name'			=> $customer_name,
		'consultant_name'			=> $user_name,
		'is_customer'			=> $is_customer,
		'consultant'			=> $consultant,
		'project_name'			=> '[PROJECT NAME]',
		'is_client'				=> true,
		'is_project'			=> true,
		'is_task'				=> true,
		'width_cp'				=> '30%',
		'width'				=> '30%',
		'consultant'			=> true,
	));

	for ($i=0; $i < 3; $i++) {

		for($j=1;$j<5;$j++){
			$view_html->assign(array(
				'client'		=> '[COMPANY NAME]',
				'project'		=> '[PROJECT NAME]',
				'task'		=> '[TASK NAME]',
				'hours'			=> number_as_hour(40),
				'comment'		=> nl2br('[Notes]'),
				'person'		=> '[CONSULTANT NAME]',
				'colspan_comm'	=> 4,
			),'project_row');
			$view_html->loop('project_row','day_row');
		}

		$view_html->assign(array(
			'date'		=> date("d/m/Y",$time_start+($i*(60*60*24))),
			'total_hours'	=> number_as_hour(160),
			'colspan'		=> 3,
		),'day_row');
		$view_html->loop('day_row');
	}

}else{
	$time_start = $in['time_start'];
	$time_end = $in['time_end'];

	$filter = " AND user_id='".$in['user_id']."' ";
	$is_customer = false;
	$consultant = true;
	$default_name = 'timesheet_print';
	if($in['customer_id'])
	{
		$filter .= " AND customer_id='".$in['customer_id']."' ";
		$customer_name = $db->field("SELECT name FROM customers WHERE customer_id='".$in['customer_id']."'");
		$is_customer = true;
	}
	if($in['project_id']){
		$filter = " AND project_id='".$in['project_id']."' AND approved='1' AND billable=1  ";
		$customer_name = $db->field("SELECT company_name FROM projects WHERE project_id='".$in['project_id']."'");
		$is_customer = true;
		$consultant = false;
		$default_name .="_inv";
	}
	$task_time = $db->query("SELECT task_time.*
	            FROM task_time
	            WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND hours!='0' ".$filter."
	            ORDER BY task_time.date
			   ");
	$all_hours = 0;
	$comment_active = false;
	$nr_cols = $db->field("SELECT COUNT(default_name) FROM default_data WHERE default_name='".$default_name."' AND active='1' AND value!='comment' and value!='person'");
	// $nr_cols = $nr_cols;
	$active_cols = $db->query("SELECT * FROM default_data WHERE default_name='".$default_name."'");
	while ($active_cols->next()) {
		if($active_cols->f('active')=='1')
		{
			$view_html->assign(array(
				'is_'.$active_cols->f('value')		=> true,
			));
			if($active_cols->f('value')=='comment')
			{
				$comment_active = true;
			}
		}else
		{
			$view_html->assign(array(
				'is_'.$active_cols->f('value')		=> false,
			));
		}
	}
	switch ($nr_cols) {
		case '1':
			$view_html->assign(array(
				'width_cp'		=> '90%',
				'width'			=> '90%',
			));
			break;
		case '2':
			$view_html->assign(array(
				'width_cp'		=> '45%',
				'width'			=> '45%',
			));
			break;
		case '3':
			$view_html->assign(array(
				'width_cp'		=> '30%',
				'width'			=> '30%',
			));
			break;
		case '4':
			$view_html->assign(array(
				'width_cp'		=> '23%',
				'width'			=> '22%',
			));
			break;
		default:
			$view_html->assign(array(
				'width_cp'		=> '90%',
				'width'			=> '90%',
				'is_nada'		=> true,
			));
			$nr_cols++;
			break;
	}

	while($task_time->next())
	{
		$all_hours = $all_hours + $task_time->f('hours');
		$total_hours = $db->field("SELECT SUM(hours) FROM task_time WHERE `date`='".$task_time->f('date')."' ".$filter."");
		$project_info = $db->query("SELECT projects.*, tasks.*
									FROM projects
									INNER JOIN tasks ON projects.project_id=tasks.project_id
									WHERE projects.project_id = '".$task_time->f('project_id')."' AND tasks.task_id='".$task_time->f('task_id')."' ");
		$user_name = $db->field("SELECT user_name FROM project_user WHERE user_id='".$task_time->f('user_id')."'");
		if($project_info->next())
		{
			$view_html->assign(array(
				'client'		=> $project_info->f('company_name'),
				'project'		=> $project_info->f('name'),
				'task'			=> $project_info->f('task_name'),
				'hours'			=> number_as_hour($task_time->f('hours')),
				'comment'		=> nl2br($task_time->f('notes')),
				'person'		=> $user_name,
				'colspan_comm'	=> $nr_cols+1,
			),'project_row');
			if($comment_active==true)
			{
				if(trim($task_time->f('notes'))!='')
				{
					$view_html->assign('is_comment_line',true,'project_row');
				}else
				{
					$view_html->assign('is_comment_line',false,'project_row');
				}
			}
			$view_html->loop('project_row','day_row');
		}
		if($last_date!=$task_time->f('date'))
		{
			$view_html->assign(array(
				'date'			=> date("d/m/Y",$task_time->f('date')),
				'total_hours'	=> number_as_hour($total_hours),
				'colspan'		=> $nr_cols,
			),'day_row');
			$last_date = $task_time->f('date');
		}else
		{
			$view_html->assign(array(
				'hide_date'			=> 'hide',
			),'day_row');
		}
		$view_html->loop('day_row');

	}

	$pdf->setQuotePageLabel(gettime_label_txt('page',$in['lid']));

	$view_html->assign(array(
		'account_logo'        		=> $img,
		'attr'				=> $attr,
		'all_hours'				=> number_as_hour($all_hours),
		'timeframe'				=> date(ACCOUNT_DATE_FORMAT,$time_start)." - ".date(ACCOUNT_DATE_FORMAT,$time_end),
		'timeframe_txt'		=> gettime_label_txt('timeframe',$in['lid']),
		'hours_txt'				=> gettime_label_txt('hours',$in['lid']),
		'total_txt'				=> gettime_label_txt('total',$in['lid']),
		'client_txt'			=> gettime_label_txt('client',$in['lid']),
		'project_txt'			=> gettime_label_txt('project',$in['lid']),
		'task_txt'				=> gettime_label_txt('task',$in['lid']),
		'person_txt'			=> gettime_label_txt('person',$in['lid']),
		'customer_txt'		=> gettime_label_txt('customer',$in['lid']),
		'consultant_txt'	=> gettime_label_txt('consultant',$in['lid']),
		'comment_txt'			=> gettime_label_txt('comment',$in['lid']),
		'customer_name'		=> $customer_name,
		'consultant_name'	=> $user_name,
		'is_customer'			=> $is_customer,
		'consultant'			=> $consultant,
		'project_name'		=> $in['project_id'] ? $db->field("SELECT name FROM projects WHERE project_id='{$in['project_id']}' ") : '',
	));
}



return $view_html->fetch();