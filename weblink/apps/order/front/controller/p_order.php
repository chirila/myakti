<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
global $config;
$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.time());
$db2 = new sqldb();
$db3 = new sqldb();

if(!$in['p_order_id'])
{
	$view->assign(array('data'=>false,'noData_txt'=>gm('No data to display')));
	return $view->fetch();
}
$page_title= gm("Purchase Order Information");

$db->query("SELECT pim_p_orders.* FROM pim_p_orders
			LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
			WHERE pim_p_orders.p_order_id='".$in['p_order_id']."'");

$email_lg=$db->f('email_language');
// echo "SELECT pim_p_orders.* FROM pim_p_orders
// 			LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
// 			WHERE pim_p_orders.p_order_id='".$in['p_order_id']."'";
if(!$db->move_next()){
	$view->assign(array('data'=>false,'noData_txt'=>gm('No data to display')));
	return $view->fetch();
}

/*if($db->f('sent') == '0'){
	$view->assign(array('data'=>false,'noData_txt'=>gm('No data to display')));
	return $view->fetch();
}*/

$the_time=time();
//$db->insert("INSERT INTO logging SET pag='p_order', message='".addslashes($db->f('buyer_ref'))." opened the weblink', field_name='p_order_id', field_value='".$in['p_order_id']."', date='".$the_time."', type='0' ");

$in['lid'] = $db->f('email_language');
if(!$in['lid']){
	$in['lid'] = 1;
}

$link_b_text = 'contact_id';
$link_c_text = 'contact_name';
$customer_id=0;
$is_contacts = false;
if($db->f('field') == 'customer_id'){
	$customer_id = $db->f('customer_id');
	$link_b_text = 'buyer_id';
	$link_c_text = 'customer_name';
}else{
	$is_contacts = true;
	$contact_id = $db->f('contact_id');
}


$rate = return_value($db->f('currency_rate'));

$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
$factur_del_date = date(ACCOUNT_DATE_FORMAT,  $db->f('del_date'));
$currency = get_commission_type_list($db->f('currency_type'));
$currency_type = $db->f('currency_type');
$serial_number = $db->f('serial_number');
$customer_name = $db->f('customer_name');
$delivery_status = $db->f('rdy_invoice');

// $class_lost='';
// if($delivery_status == 2){
// 	$class_lost = 'quote_sent';
// }

$is_ref = false;
if($db->f('buyer_ref')){
	$is_ref = true;
}

$ref = '';
if(ACCOUNT_P_ORDER_REF && $db->f('buyer_ref')){
	$ref = $db->f('buyer_ref');
}else{
	$is_ref = false;
}

 // ark::run('order--order-external_id');

$selectedDraft = "";
$selectedWon = '';
$selectedSent = '';
if($db->f('invoiced')){
	if($db->f('rdy_invoice') == 1){
		$selectedWon = 'selected';
	}else{
		$selectedSent = 'selected';
	}
}else{
	if($db->f('rdy_invoice') == 2){
		$selectedSent = 'selected';
	}elseif($db->f('rdy_invoice') == 1){
		$selectedWon = 'selected';
	}else{
		$selectedDraft = "selected";
	}
}
$default_lang = $db->field("SELECT * FROM pim_lang WHERE active=1 AND default_lang='1'");
if($db->f('email_language')){
	$default_lang = $db->f('email_language');
}


$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'p_order'
								   AND item_name = 'notes'
								   And active = '1'
								   AND item_id = '".$in['p_order_id']."' ");

$view->assign(array(
	'selectedDraft'					=> $selectedDraft,
	'selectedWon'					=> $selectedWon,
	'selectedSent'					=> $selectedSent,
	//'link_c_text'										=> $link_c_text,
	//'link_b_text'										=> $link_b_text,
	'factur_nr' 					=> $serial_number ? $ref.$serial_number: '',
	'ref' 							=> $ref,
	'is_ref'						=> $is_ref,
	'factur_date' 					=> $factur_date,
	'factur_del_date' 				=> $factur_del_date,
	'due_date'						=> date(ACCOUNT_DATE_FORMAT,$db->f('due_date')),
	'delete_link'   				=> 'index.php?do=order-po_orders-order-delete_po_order&p_order_id='.$db->f('p_order_id'),
	'duplicate_link'    			=> 'index.php?do=order-norder&duplicate_order_id='.$db->f('order_id'),
	'order_buyer_name'              => $db->f('field') == 'customer_id' ? $db->f('customer_name') : $db->f('customer_name')." > " ,
	'order_contact_name'            => $db->f('contact_name'),
	'field_customer_id'             => $db->f('field')=='customer_id'?true:false,
	'order_buyer_country'   		=> get_country_name($db->f('customer_country_id')),
	'order_buyer_state'     		=> $db->f('customer_state'),
	'order_buyer_city'      		=> $db->f('customer_city'),
	'order_buyer_zip'       		=> $db->f('customer_zip'),
	'order_buyer_address'   		=> $db->f('customer_address'),
	'buyer_email'                   => $db->f('customer_email'),
	'buyer_fax'                     => $db->f('customer_fax'),
	'buyer_phone'                   => $db->f('customer_phone'),
	'order_seller_name'             => utf8_decode($db->f('seller_name')),
	'order_seller_d_address'        => utf8_decode($db->f('seller_d_address')),
	'seller_d_city'                 => utf8_decode($db->f('seller_d_city')),
	'seller_d_zip'                  => $db->f('seller_d_zip'),
	'seller_d_country'              => get_country_name( $db->f('seller_d_country_id')),
	'order_vat_no'                  => $db->f('seller_bwt_nr'),
	'serial_number'			 		=> $db->f('serial_number'),
  	'status'						=> $delivery_status == 2 ? gm('Ready to received') : ($delivery_status == 1 ? gm("Received") : gm('Draft') ),
	'img'							=> $db->f('rdy_invoice') ? ($db->f('rdy_invoice')==1 ? 'green' : 'orange') : 'gray',
	'is_editable'					=> $db->f('rdy_invoice') > 0 ? false : true,

  	'notes'	 	  		            => $notes,
	'buyer_id'						=> $db->f('customer_id'),
	'p_order_id'					=> $in['p_order_id'],
	'your_ref'       				=> $db->f('your_ref'),
	'our_ref' 				  		=> $db->f('our_ref'),
	'is_delivered'					=> $delivery_status > 0 ? true : false ,
	'address_info'					=> nl2br($db->f('address_info')),
	'language_dd'	      			=> build_pdf_language_dd($in['lid'],1),
	'not_adveo'						=> $db->f('is_adveo') == 1 ? false : true ,
	'sent_adveo'                    => $db->f('sent_adveo') == 1 ? true : false ,
	'sent_adveo_date'               => date('d/m/Y',$db->f('sent_adveo_date')-3600),
	'sent_adveo_time'               => date('H:i',$db->f('sent_adveo_date')-3600),
	'data'							=> true,
	'class_lost'					=> $delivery_status == 2 ? 'quote_sent' : '',

));


if($notes == ''){
	$view->assign('hide_notes','hide');
}






$lg = $default_lang;
if($email_lg){
	$lg = $email_lg;
}

if($lg>4) {
	$labels_query = $db->query("SELECT * FROM label_language_p_order WHERE lang_code ='".$lg."'")->getAll();
}else{
	$labels_query = $db->query("SELECT * FROM label_language_p_order WHERE label_language_id='".$lg."'")->getAll();
}

$labels = array();
$j=1;

foreach ($labels_query['0'] as $key => $value) {
	if( ($j % 2) == 0){
		$j++;
		continue;
	}
	$labels[$key] = $value;
	$j++;
}


//ini_set('display_errors', 1);
   // ini_set('error_reporting', 1);
   // error_reporting (E_ALL);

$view->assign(array(
	'bank'				  			=> ACCOUNT_BANK_NAME,
	'bic'				  			=> ACCOUNT_BIC_CODE,
	'iban'				  			=> ACCOUNT_IBAN,
	'bankd_txt'			  			=> $labels['bank_details'],
	'bank_txt'			  			=> $labels['bank_name'],
	'bic_txt'			  			=> $labels['bic_code'],
	'iban_txt'			  			=> $labels['iban'],

	'invoice_txt'					=> 	$labels['p_order_webl'],
	'date_txt'						=> 	$labels['date_webl'],
	'accept_txt'					=>  $labels['accept_webl'],
	'request_new_version_txt'		=>  $labels['requestNewVersion_webl'],
	'reject_txt'					=>  $labels['reject_webl'],
	'status_txt'					=>  $labels['status_webl'],
	'regularInvFromProforma_txt'	=> $labels['regularInvFromProforma_webl'],
	'bankDetails_txt'				=> $labels['bankDetails_webl'],
	'bankName_txt'					=> $labels['bankName_webl'],
	'bicCode_txt'					=> $labels['bicCode_webl'],
	'ibanCode_txt'					=> $labels['ibanCode_webl'],
	'noData_txt'					=> $labels['noData_webl'],
	'PDF_LINK'					    => 'index.php?do=order-p_order_print&p_order_id='.$in['p_order_id'].'&lid='.$email_lg.'&type='.ACCOUNT_P_ORDER_PDF_FORMAT.'&q='.$in['q'],
	'endhrf'						=>'&p_order_id='.$in['p_order_id'].'&lid='.$email_lg.'&type='.ACCOUNT_P_ORDER_PDF_FORMAT.'&q='.$in['q'],
));

global $main_menu_item;
global $sub_menu_item;
$main_menu_item='po_orders';
$sub_menu_item='po_orders_list';


return $view->fetch('CONTENT');