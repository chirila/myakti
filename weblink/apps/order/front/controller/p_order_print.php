<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf_type =array(4,5);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Purchase Order');
$pdf->SetSubject('Purchase Order');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

if(in_array($in['type'], $pdf_type)){
	$pdf->SetMargins(10, 5, 10);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, 50);
	$pdf->setPrintFooter(false);
	$hide_all = 2;
}
$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();

$html = include('p_order_print_body.php');
// print($html);exit();
if(isset($in['print'])){
	print_r($html);exit();
}
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->SetY($height);
if(in_array($in['type'], $pdf_type)){
	$hide_all = 1;
	$pdf->SetAutoPageBreak(TRUE, 0);

	$htmls = include('p_order_print_body.php');

	$pdf->writeHTML($htmls, true, false, true, false, '');
}
$pdf->lastPage();

if($in['dw'] == 1){
	$pdf->Output('p_order.pdf', 'D');
	exit();
}

if($in['do']=='order-p_order_print'){
   $pdf->Output($serial_number.'.pdf','I');
}else{
   $pdf->Output(__DIR__.'/../../../../admin/p_order.pdf', 'F');
}