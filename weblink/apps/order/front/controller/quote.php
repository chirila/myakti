<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.time());
global $cfg;
$db = new sqldb();
$in['quote_preview'] = 1;
// $tips = $page_tip->getTip('quote-quote');
if(!$in['quote_id'])
{
	$view->assign(array('data'=>false,'noData_txt'=>gm('No data to display')));
	return $view->fetch();
}
$page_title=gm("Quote Information");

$filter = '';
if(obj::get('version_id')){
	$in['version_id'] = obj::get('version_id');
}
if(isset($in['version_id']) && is_numeric($in['version_id'])){
	$filter = 'AND tblquote_version.version_id = '.$in['version_id'];
}else{
	$filter = 'AND tblquote_version.active = 1';
}

$quote = $db->query("SELECT tblquote.*, tblquote.sent as sent_q,
					tblquote_version.active,tblquote_version.version_code,tblquote_version.version_id,tblquote_version.active AS version_active,
					tblquote_version.version_date, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date,tblquote.pdf_logo,tblquote.pdf_layout
            		FROM tblquote
            		INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
            		WHERE tblquote.id='".$in['quote_id']."' AND 1=1 ".$filter);
if(!$quote->next()){
	$view->assign(array('data'=>false,'noData_txt'=>gm('No data to display')));
	return $view->fetch();
}
if($quote->f('sent') == '0'){
	$view->assign(array('data'=>false,'noData_txt'=>gm('No data to display')));
	return $view->fetch();
}
$the_time=time();
//$db->insert("INSERT INTO logging SET pag='quote', message='".addslashes($quote->f('buyer_name'))." opened the weblink', field_name='quote_id', field_value='".$in['quote_id']."', date='".$the_time."', type='0' ");
// switch ($_SESSION['group']) {
// 	case 'user':
// 		$can_revert = false;
// 		if(in_array(ark::$app, $_SESSION['admin_sett'])){
// 			$can_revert = true;
// 		}elseif($quote->f('created_by') == $_SESSION['u_id']){
// 			$can_revert = true;
// 		}
// 		break;
// 	case 'admin':
// 		$can_revert = true;
// 		break;
// 	default:
// 		$can_revert = false;
// 		break;
// }

$in['use_custom'] = 0;
if($quote->f('pdf_layout') && $quote->f('use_custom_template')==0){
	$in['logo'] = $quote->f('pdf_logo');
	$link_end='&type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
	$in['template_type'] = $quote->f('pdf_layout');
}elseif($quote->f('pdf_layout') && $quote->f('use_custom_template')==1){
	$link_end = '&custom_type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
	$in['template_type'] = $quote->f('pdf_layout');
	$in['use_custom'] = 1;
}else{
	$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
	$in['template_type'] = ACCOUNT_QUOTE_PDF_FORMAT;
}
#if we are using a customer pdf template
if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $quote->f('pdf_layout') == 0){
	$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT;
	$in['use_custom'] = 1;
}
// $in['show_vat'] = $quote->f('show_vat');
// $in['show_grand'] = $quote->f('show_grand');

// $show_total=$quote->f('show_grand');

// $apply_discount=$quote->f('apply_discount');
/*
}else{
	$apply_discount=false;
}*/
$filter_db2 = '';
if($quote->f('contact_id'))
{
	$contact_title = $db->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$quote->f('contact_id')."'");
	$filter_db2 = " AND customer_contacts.contact_id='".$quote->f('contact_id')."' ";
	// if(!$quote->f('buyer_id'))
	// {
	// 	$view->assign(array('HIDE_CUSTOMER'=>'hide'));
	// }else
	// {
	// 	$view->assign(array('HIDE_CUSTOMER'=>''));
	// }
}
if($contact_title)
{
	$contact_title .= " ";
}
$opts = array('gray','red','green','green','green','orange');
$fields = array('status_customer');
$class_lost = '';
// if($quote->f('sent')){
	foreach ($fields as $field){
		$view->assign(array(strtoupper($field)=>$opts[$quote->f($field)]));
		switch ($opts[$quote->f($field)]) {
			case 'orange':
				$status = gm('Sent');
				$status = gm('Quote Pending');
				break;
			case 'red':
				$green_add = '';
				$version_codes = $db->field("SELECT version_code FROM tblquote_version WHERE quote_id='".$in['quote_id']."' AND active='1' ");
				if($version_codes){
					$green_add = ' ['.$version_codes.']';
				}
				$status = gm('Rejected').$green_add;
				$status = gm('Quote Rejected').$green_add;
				$class_lost = 'quote_lost';
				break;
			case 'green':
				$green_add = '';
				$version_codes = $db->field("SELECT version_code FROM tblquote_version WHERE quote_id='".$in['quote_id']."' AND active='1' ");
				if($version_codes){
					$green_add = ' ['.$version_codes.']';
				}
				$status = gm('Accepted').$green_add;
				$status = gm('Quote Accepted').$green_add;
				break;
			case 'gray':
				$status = gm('Draft');
				$status = gm('Quote Draft');
				break;
			default:
				$status = gm('Draft');
				$status = gm('Quote Draft');
				break;
		}
	}
/*}else{
	$view->assign(array('STATUS_CUSTOMER'=>$opts[5]));
	$status = 'Draft';
}*/
$was_sent = $db->field("SELECT count(version_id) FROM tblquote_version WHERE sent='1' AND quote_id='".$in['quote_id']."' ");
if($was_sent && $quote->f('status_customer') == 0){
	$view->assign(array('STATUS_CUSTOMER'=>$opts[5]));
	$status = gm('Sent');
	$status = gm('Quote Pending');
	$class_lost = 'quote_sent';
}
$q_status = $quote->f('status_customer');
$version_code = $quote->f('version_code');
$vat = $quote->f('vat');
$quote_date = date(ACCOUNT_DATE_FORMAT,$quote->f('version_date'));
$serial_number = $quote->f('serial_number');
$quote_status = $quote->f('status');
// if (ACCOUNT_TYPE=='goods')
// {													//Accepted ( for Invoice )'),4=>gm('Accepted ( for Order )')
// 	$opts = array(0=>gm('Sent'),1=>gm('Rejected'), 2=>gm('Accepted'));
// }else if (ACCOUNT_TYPE=='services')
// {													// ('Accepted ( for Invoice )'),3=>gm('Accepted ( for Project )')
// 	$opts = array(0=>gm('Sent'),1=>gm('Rejected'), 2=>gm('Accepted'));
// }else
// {													// ( for Invoice )'),3=>gm('Accepted ( for Project )'),4=>gm('Accepted ( for Order )'));
// 	$opts = array(0=>gm('Sent'),1=>gm('Rejected'), 2=>gm('Accepted'));
// }
$ref = '';
if(ACCOUNT_QUOTE_REF && $quote->f('buyer_reference')){
	$ref = $quote->f('buyer_reference');
}

#stages
// $k=0;
// $stages = array();
// $stage = $db->query("SELECT * FROM tblquote_stage");
// while ($stage->next()) {
// 	$view->assign(array(
// 		'STAGE'	        => $stage->f('name'),
// 		'STAGE_LINK'    => 'index.php?do=quote-quote-quote-change_stage&quote_id='.$in['quote_id'].'&version_id='.$quote->f('version_id').'&stage_id='.$stage->f('id'),
// 	),'stage');
// 	$view->loop('stage');
// 	$stages[$stage->f('id')] = $stage->f('name');
// 	// $k++;
// }
/*if($k == 0){
	$view->assign(array('HIDE_STAGE'=>'hide'));
}*/

// $inv_nr = $db->field("SELECT serial_number FROM tblinvoice WHERE quote_id='".$in['quote_id']."' order by id desc ");
// $order_nr = $db->field("SELECT serial_number FROM pim_orders WHERE quote_id='".$in['quote_id']."' ");
// $project_nr = $db->field("SELECT serial_number FROM projects WHERE quote_id='".$in['quote_id']."' order by project_id desc ");
if($quote->f('source_id')){
	$source = $db->field("SELECT name FROM tblquote_source WHERE id='".$quote->f('source_id')."' ");
}
if($quote->f('t_id')){
	$type = $db->field("SELECT name FROM tblquote_type WHERE id='".$quote->f('t_id')."' ");
}
$author = '';
if($quote->f('author_id')){
	$author = get_user_name($quote->f('author_id'));
}
if($quote->f('lost_id')){
	$lost = $db->field("SELECT name FROM tblquote_lost_reason WHERE id='".$quote->f('lost_id')."' ");
}

$notes = $db->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'quote'
								   AND item_name = 'notes'
								   AND version_id = '".$quote->f('version_id')."'
								   AND active = '1'
								   AND item_id = '".$in['quote_id']."' ");
$free_text_content = $db->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'quote'
								   AND item_name = 'free_text_content'
								   AND version_id = '".$quote->f('version_id')."'
								   AND item_id = '".$in['quote_id']."' ");

$language = $db->field("SELECT * FROM pim_lang WHERE active=1 AND default_lang='1'");
if($quote->f('email_language')){
	$language = $quote->f('email_language');
}

$view->assign(array(
	'invoice_txt'				=> 	getq_weblink_label_text('quote_webl', $language),
	'date_txt'					=> 	getq_weblink_label_text('date_webl', $language),
	'accept_txt'				=>  getq_weblink_label_text('accept_webl', $language),
	'request_new_version_txt'	=>  getq_weblink_label_text('requestNewVersion_webl', $language),
	'reject_txt'				=>  getq_weblink_label_text('reject_webl', $language),
	'status_txt'				=>  getq_weblink_label_text('status_webl', $language),
	'regularInvoiceProForma_txt'=>  getq_weblink_label_text('regularInvoiceProForma_webl', $language),
	'bankDetails_txt'			=>	getq_weblink_label_text('bankDetails_webl', $language),
	'bicCode_txt'				=> 	getq_weblink_label_text('bicCode_webl', $language),
	'ibanCode_txt'				=> 	getq_weblink_label_text('ibanCode_webl', $language),
	'bank_txt_wl'				=>  getq_weblink_label_text('bank_webl', $language),
	'noData_txt'				=>  getq_weblink_label_text('noData_webl', $language)
));

$view->assign(array(
	'STATUS_LEASELINE' 			=> build_simple_dropdown($opts,$quote->gf('status_customer')),
	// 'Q_LOST'					=> build_lost_dd($quote->f('lost_id')),
	'STATUS_C_DATE'	 			=> date(ACCOUNT_DATE_FORMAT,$quote->f('status_c_date')),
	'SENT_DATED'				=> $quote->f('sent')==1 && $quote->f('sent_date') ? date(ACCOUNT_DATE_FORMAT,$quote->f('sent_date')):'',
	// 'HIDE_SENTD'				=> $quote->f('sent')==1 ? '':'hide',
	// 'HIDE_SOURCE'				=> $quote->f('source_id') ? '':'hide',
	// 'HIDE_TYPE'					=> $quote->f('t_id') ? '':'hide',
	// 'HIDE_AUTHOR'				=> $quote->f('author_id') ? '':'hide',
	'QUOTE_SOURCE'				=> $quote->f('source_id') ? $source : '-',
	'QUOTE_TYPE'				=> $quote->f('t_id') ? $type : '-',
	'QUOTE_AUTHOR'				=> $quote->f('author_id') ? $author : '-',
	'QUOTE_LOST'				=> $quote->f('lost_id') ? $lost : '-',
	// 'AUTHOR_DD'					=> build_author_dd($quote->f('author_id')),

	'VERSION_ID'                => $quote->f('version_id'),
	'VIEW_VERSION_ACTION'       => $quote->f('version_active') ? 'hide' : '',
	// 'HIDE_SENT'                 => $quote->f('sent')==0?'':'hide',
	'QUOTE_NR' 	        		=> $ref.$quote->f('serial_number'),
	'QUOTE_VERSION'     	    => $quote->f('version_code'),
	'QUOTE_DATE' 	    		=> $quote_date,
	// 'QUOTE_STAGE' 	    		=> $quote->f('stage_id') ? $stages[$quote->f('stage_id')] : '-',
	'HIDE_STAGE'				=> $quote->f('stage_id') ? '' : 'hide',
	'INVOICE_NR'				=> $inv_nr,
	'HIDE_INV_NR'				=> $inv_nr ? '' : 'hide',
    'ORDER_NR'					=> $order_nr,
	'HIDE_ORDER_NR'				=> $order_nr ? '' : 'hide',
	'PROJECT_NR'				=> $project_nr,
	'HIDE_PROJECT_NR'			=> $project_nr ? '' : 'hide',
	'SQUOTE_DATE'				=> date(ACCOUNT_DATE_FORMAT,$quote->f('quote_date')),

	//'DELETE_LINK'       		=> get_link('index.php?pag=quotes&act=quote-delete&id='.$db->f('id')),
	'DUPLICATE_LINK'    		=> 'index.php?do=quote-nquote&duplicate_quote_id='.$quote->f('id'),
	'ADD_VERSION_LINK'  		=> 'index.php?do=quote-nquote-quote_version-next&quote_id='.$quote->f('id').'&current_version_id='.$quote->f('version_id'),

	'QUOTE_BUYER_REFERENCE'  	=> $ref,
	'QUOTE_BUYER_NAME'       	=> $quote->f('buyer_name'),
	'QUOTE_BUYER_NAME_URL'      => urlencode($quote->f('buyer_name')),
	'QUOTE_BUYER_COUNTRY'    	=> get_country_name($quote->f('buyer_country_id')),
	'QUOTE_BUYER_CITY'       	=> $quote->f('buyer_city'),
	'QUOTE_BUYER_ZIP'        	=> $quote->f('buyer_zip'),
	'QUOTE_BUYER_ADDRESS'    	=> $quote->f('buyer_address'),
    'VIEW_DELIVERY'    			=> $quote->f('delivery_address')?'':'hide',
    'DELIVERY_ADDRESS'    		=> nl2br($quote->f('delivery_address')),


	'QUOTE_SELLER_NAME'         => $quote->f('seller_name'),
	'QUOTE_SELLER_D_COUNTRY'    => get_country_name($quote->f('seller_d_country_id')),
	'QUOTE_SELLER_D_CITY'       => $quote->f('seller_d_city'),
	'QUOTE_SELLER_D_ZIP'        => $quote->f('seller_d_zip'),
	'QUOTE_SELLER_D_ADDRESS'    => $quote->f('seller_d_address'),
	'QUOTE_SELLER_B_CITY'       => $quote->f('seller_b_city'),
	'QUOTE_SELLER_B_ZIP'        => $quote->f('seller_b_zip'),
	'QUOTE_SELLER_B_ADDRESS'    => $quote->f('seller_b_address'),
	'QUOTE_VAT_NO'              => $quote->f('seller_bwt_nr'),
	'NOTES'						=> nl2br($notes),
	'BUYER_ID'					=> $quote->f('buyer_id'),
    'CONTACT_ID'				=> $quote->f('contact_id'),
	'HIDE_PROJECT'				=> $quote->f('status_customer') == 3 ? '' : 'hide',
	// 'HIDE_PROJECT'				=> $quote->f('buyer_id')  && $quote->f('status_customer') == 3 ? '' : 'hide',
	'HIDE_INVOICE'				=> $quote->f('status_customer') == 2 ? '' : 'hide',
    // 'HIDE_ORDER'				=> $quote->f('status_customer') == 4 ? '' : 'hide',
	'HIDE_OPTIONS'				=> ($quote->f('status_customer') < 2) ? 'disable_option' : '',
	'HIDE_DETAILS'				=> ($quote->f('status_customer') > 1) ? 'hide' : '',
	'HIDE_REF'					=> $quote->f('own_reference') ? '' : 'hide',
	'OWN_REFERENCE'             => $quote->f('own_reference'),
	'VIEW_APPROVAL'				=> $quote->f('sent_q') == 0  ? 'hide' : ($q_status > 0 && !$quote->f('version_active') ? 'hide' : ''),
	'STATUS'					=> $status,
	'is_note'					=> $notes ? true : false,
	'is_sent'					=> $quote->f('sent') == 1 ? false : true,
	// 'STAGE_DD'                  => build_data_dd('tblquote_stage',$quote->gf('stage_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id')),
	// 'SOURCE_DD'                 => build_data_dd('tblquote_source',$quote->gf('source_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id')),
	// 'TYPE_DD'                  	=> build_data_dd('tblquote_type',$quote->gf('t_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id')),
	// 'LOST_DD'                  	=> build_data_dd('tblquote_lost_reason',$quote->gf('lost_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id')),
	'AUTHOR_ID'					=> $quote->gf('author_id') ? $quote->gf('author_id') : '',
	'ACC_MANAGER'				=> $quote->gf('author_id') ? get_user_name($quote->gf('author_id')) : '',
	'status_c'					=> $quote->f('status_customer'),
	'BUYER_EMAIL'				=> $quote->f('buyer_email'),
	'SHOW_TOTAL'				=> $show_total,
	'is_lost'					=> $quote->f('status_customer') == 1 ? true : false,
	'hide_lost'					=> $quote->f('status_customer') == 1 ? '' : 'hide',
	'quote_to_project_link'		=> 'index.php?do=project-project&source_id='.$quote->f('source_id').'&customer_id='.($quote->f('buyer_id') ? $quote->f('buyer_id') : $quote->f('contact_id') ).'&customer='.urlencode($quote->f('buyer_name')).'&quote_id='.$in['quote_id'].'&currency_type='.$quote->f('currency_type').'&budget_type=4&is_contact='.($quote->f('buyer_id') ? '0' : '1'),
	'quote_to_project_string'	=> '&customer_id='.($quote->f('buyer_id') ? $quote->f('buyer_id') : $quote->f('contact_id') ).'&customer='.urlencode($quote->f('buyer_name')).'&quote_id='.$in['quote_id'].'&currency_type='.$quote->f('currency_type').'&budget_type=4&is_contact='.($quote->f('buyer_id') ? '0' : '1'),
	'is_free_text_content'		=> $free_text_content ? true : false,
	'free_text_content'			=> $free_text_content,
	'approved'					=> $q_status < 1 ? true : false,
	'can_revert'				=> $can_revert,
	'q_subject'					=> $quote->f('subject'),
	'checked_yes'				=> $in['show_vat'] == 1 ? 'CHECKED' : '',
	'checked_no'				=> $in['show_vat'] == 1 ? '' : 'CHECKED',
	'checked_yes_g'				=> $in['show_grand'] == 1 ? 'CHECKED' : '',
	'checked_no_g'				=> $in['show_grand'] == 1 ? '' : 'CHECKED',
	'hide_grand_t'				=> $in['show_grand'] == 1 ? '' : 'hide',
	'show_vat_inp'				=> $in['show_grand'] == 1 ? '' : 'hide',
	'hide_vat_input'			=> $in['show_vat'] == 1 ? '' : 'hide',


	'data'								=> true,
	'class_lost'					=> $class_lost,

));
$in['validity_d'] = $quote->f('validity_date') ? date(ACCOUNT_DATE_FORMAT,$quote->f('validity_date')) : '';
$in['validity_date'] = $quote->f('validity_date');
$in['lid'] = $quote->f('email_language');
if(!$in['lid']){
	$in['lid'] = 1;
}
if($quote->f('type')==1){ // proforma
	$view->assign(array('SERIAL_NUMBER'=>generate_quote_number(obj::get('db'))));
}else{
	$view->assign(array('SERIAL_NUMBER'=>$quote->f('serial_number')));
}

$discount_procent = $quote->f('discount') ? $quote->f('discount') : 0;
/*$contact_name = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$quote->f('contact_id')."'");
$contact_name->next();*/

$free_field = '<p>'.$quote->f('buyer_address').'</p><p>'.$quote->f('buyer_zip').' '.$quote->f('seller_d_city').'</p><p>'.get_country_name($quote->f('buyer_country_id')).'</p>';
$in['id'] = $quote->f('id');	# needed for quote_print_web
$in['version'] = $quote->f('version_id');  # needed for quote_print_web
$view->assign(array(
	// 'QUOTE_CONTACT_NAME'	=> $contact_title.$contact_name->f('firstname').' '.$contact_name->f('lastname'),
	// 'QUOTE_CONTACT_EMAIL'	=> $contact_name->f('email'),
	// 'QUOTE_CONTACT_TEL'		=> $contact_name->f('phone'),
	// 'QUOTE_CONTACT_CELL'	=> $contact_name->f('cell'),
	'free_field'			=> $quote->f('free_field') ? '<p>'.nl2br($quote->f('free_field')).'</p>' : $free_field,
	// 'print'						=> ark::run('quote-quote_print_web'),
));

$amount = $db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$quote->f('id')."' AND version_id='".$quote->f('version_id')."' AND show_block_total='0' ");

if($quote->f('discount') && $quote->f('apply_discount') > 1){
	$amount = $amount - ($amount*$quote->f('discount')/100);
}
$amount2 = 0;
if($quote->f('currency_rate')){
	$amount2 = $amount*return_value($quote->f('currency_rate'));
}

$view->assign(array(
	'total'			=> place_currency(display_number($amount),get_commission_type_list($quote->f('currency_type'))),
	'total2'		=> place_currency(display_number($amount2)),
	'is_total2'		=> $amount2 ? true : false,
));

//sent mail
if(!$in['include_pdf'])
{
	$in['include_pdf'] = 1;
}

// global $database_config;
// $database_users = array(
// 	'hostname' => $database_config['mysql']['hostname'],
// 	'username' => $database_config['mysql']['username'],
// 	'password' => $database_config['mysql']['password'],
// 	'database' => $database_config['user_db'],
// );
// $db_users = new sqldb($database_users);
// if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
// 	$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`='".obj::get('db')."' AND `item_id`='".$quote->f('id')."' AND `type`='q'  ");
// 	if(defined('WEB_INCLUDE_PDF_Q') && WEB_INCLUDE_PDF_Q == 0){
// 		$in['include_pdf'] = 0;
// 	}
// }

// $db->query("SELECT tblquote.id,tblquote.serial_number,tblquote.discount,tblquote.email_language,tblquote.quote_date,tblquote.buyer_name,tblquote.currency_type, customer_contacts.firstname,customer_contacts.lastname,
//             SUM(tblquote_line.amount) AS total
//             FROM tblquote
//             LEFT JOIN tblquote_line ON tblquote_line.quote_id=tblquote.id
//             INNER JOIN customer_contacts ON customer_contacts.customer_id=tblquote.buyer_id
//             WHERE tblquote.id='".$in['quote_id']."' ".$filter_db2);
// $db->move_next();

// $message_data=get_sys_message('quomess',$db->f('email_language'));
// $subject=$message_data['subject'];
// $external_web_link = '<a href="'.$cfg['web_link_url'].'?q='.$exist_url.'">'.$cfg['web_link_url'].'?q='.$exist_url.'</a>';
// $subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
// $subject=str_replace('[!SERIAL_NUMBER!]',$db->f('serial_number').' ['.$version_code.']', $subject);
// $subject=str_replace('[!QUOTE_DATE!]',$quote_date, $subject);
// $subject=str_replace('[!CUSTOMER!]',$db->f('buyer_name'), $subject);
// $subject=str_replace('[!SUBTOTAL!]',place_currency(display_number($db->f('total')),get_commission_type_list($quote->f('currency_type'))), $subject);
// $subject=str_replace('[!DISCOUNT!]',$db->f('discount').'%', $subject);
// $subject=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($in['discount_value_web']),get_commission_type_list($quote->f('currency_type'))), $subject);
// //$subject=str_replace('[!VAT!]',$db->f('vat').'%', $subject);
// $subject=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),get_commission_type_list($quote->f('currency_type'))), $subject);
// $subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($in['grand_total_web']),get_commission_type_list($quote->f('currency_type'))), $subject);
// $subject=str_replace('[!CONTACT_FIRST_NAME!]',$db->f('firstname'), $subject);
// $subject=str_replace('[!CONTACT_LAST_NAME!]',$db->f('lastname'), $subject);
// $subject=str_replace('[!WEB_LINK!]', $external_web_link, $subject);


// $body=$message_data['text'];
// $body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
// $body=str_replace('[!SERIAL_NUMBER!]',$db->f('serial_number').' ['.$version_code.']', $body);
// $body=str_replace('[!QUOTE_DATE!]',$quote_date, $body);
// $body=str_replace('[!CUSTOMER!]',$db->f('buyer_name'), $body);
// $body=str_replace('[!SUBTOTAL!]',place_currency(display_number($db->f('total')),get_commission_type_list($quote->f('currency_type'))), $body);
// $body=str_replace('[!DISCOUNT!]',$db->f('discount').'%', $body);
// $body=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($in['discount_value_web']),get_commission_type_list($quote->f('currency_type'))), $body);
// //$body=str_replace('[!VAT!]',$db->f('vat').'%', $body);
// $body=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),get_commission_type_list($quote->f('currency_type'))), $body);
// $body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($in['grand_total_web']),get_commission_type_list($quote->f('currency_type'))), $body);
// $body=str_replace('[!CONTACT_FIRST_NAME!]',$db->f('firstname'), $body);
// $body=str_replace('[!CONTACT_LAST_NAME!]',$db->f('lastname'), $body);
// $body=str_replace('[!WEB_LINK!]', $external_web_link, $body);

// $view->assign(array(
// 	'INCLUDE_PDF_CHECKED'  => $in['include_pdf'] ? 'checked="checked"' : '',
// 	'COPY_CHECKED'         => $in['copy'] ? 'checked="checked"' : '',
// 	'SUBJECT'              => $in['subject']?$in['subject']:$subject,
// 	'E_MESSAGE'            => $in['e_message']?$in['e_message']:($body),
// 	'LANGUAGE_DD'	       => build_pdf_language_dd($quote->f('email_language'),1),
// 	'use_html'			   => $message_data['use_html'],
// ));
$in['languages'] = $quote->f('email_language');

$recipient = $db->query("SELECT customer_contacts.*,tblquote.id,tblquote.buyer_id,tblquote.serial_number
             FROM tblquote
             INNER JOIN customer_contacts ON customer_contacts.customer_id=tblquote.buyer_id
             WHERE tblquote.id='".$in['quote_id']."' ".$filter_db2." ");

$j = 0;
while ($recipient->next()){
	$in['recipients'][$recipient->f('contact_id')]=1;
	$view->assign(array(
		'RECIPIENT_NAME'  			=> $recipient->f('firstname').' '.$recipient->f('lastname'),
		'RECIPIENT_EMAIL' 			=> $recipient->f('email'),
		'RECIPIENT_ID'    			=> $recipient->f('contact_id'),
		'RECIPIENTS_CHECKED'    	=> $in['recipients'][$recipient->f('contact_id')] ? 'checked="checked"' : '',
	),'recipient_row');
	$j++;
	$view->loop('recipient_row');
}
if($j == 0){
	$view->assign(array("HIDE_SEND_TO"=>""));
}
else{
	$view->assign(array("HIDE_SEND_TO"=>"hide"));
}

$l=0;
//print languages


$db->query("SELECT * FROm pim_lang WHERE active=1 ORDER BY sort_order ");
 while($db->move_next()){
 	if($db->f('default_lang')){
 		$default_lang = $db->f('lang_id');
 	}
 	$view->assign(array(
		'LANGUAGE'	        => gm($db->f('language')),
		'PDF_LINK'          => 'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$quote->f('version_id').'&lid='.$db->f('lang_id').$link_end,
	),'languages_pdf');
	$l++;
	$view->loop('languages_pdf');
}
$custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active=1 ORDER BY sort_order ");
while($custom_lang->next()){
 	$custom_label = $db->query("SELECT * FROM label_language_quote WHERE lang_code='".$custom_lang->f('lang_id')."'");
 	if($custom_label->move_next())
 	{
	 	$view->assign(array(
			'LANGUAGE'	        => $custom_label->f('name'),
			'PDF_LINK'          => 'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$quote->f('version_id').'&lid='.$custom_label->f('label_language_id').$link_end,
		),'languages_pdf');
		$l++;
	}
	$view->loop('languages_pdf');
}
if($l==1){
	   $view->assign(array(
	       'GENERATE_PDF_ID'  	=> '',
	       'WEB_QUOTE_ID'   	=> '',
	       'HIDE_LANGUAGE'	  	=> 'hide',
	       'pdf_link'			=>'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$quote->f('version_id').'&lid='.$default_lang.$link_end,
	   ));
}else{
	 $view->assign(array(
	       'GENERATE_PDF_ID'  => 'generate_pdf',
	       'WEB_QUOTE_ID'   	=> 'web_quote',
	       'HIDE_LANGUAGE'	  => ''
	   ));
}
$lg = $default_lang;
if($quote->f('email_language')){
	$lg = $quote->f('email_language');
}
//attached files
// $f = $db->query("SELECT * FROM attached_files WHERE type='1'");
// while ($f->next()) {
// 	$view->assign(array(
// 		'file'		=> $f->f('name'),
// 		'checked'	=> $f->f('default') == 1 ? "CHECKED" : "",
// 		'file_id'	=> $f->f('file_id'),
// 		'path'		=> '../../'.$f->f('path'),
// 		),'files_row');
// 	$view->loop('files_row');
// }

//versions
//  $db->query("SELECT * FROM tblquote_version WHERE quote_id='".$in['quote_id']."' ");
//  while($db->move_next()){
//    $view->assign(array(
// 		'VERSION_CODE'	    => $db->f('version_code'),
// 		'ACTIVE'	        => ($q_status < 2) ? ($in['version_id'] ? ($db->f('version_id') == $in['version_id'] ? 'selected' : '') : ($db->f('active') ? 'selected':'')) : ($db->f('active') ? 'active' : ($db->f('version_id') == $in['version_id'] ? 'selected' : '')),
// 		'VERSION_LINK'      => ('index.php?do=quote-quote&quote_id='.$db->f('quote_id').'&version_id='.$db->f('version_id')),
// 		),'version_row');
// 	$view->loop('version_row');
// }

//sent box
if($in['act']=='quote-mark_sent' ){
	$view->assign(array("VIEW_SENT"=>""));
}else{
	$view->assign(array("VIEW_SENT"=>"style='display:none;'"));
}
//send box
if($in['act']=='quote-send' ){
	$view->assign(array("VIEW_SEND"=>""));
}else{
	$view->assign(array("VIEW_SEND"=>"style='display:none;'"));
}

if(!$in['s_date']){
	$in['s_date'] = time();
}

if($quote->f('buyer_id')){
	$view->assign(array(
		'HIDE_BACK'		=>  '',
		'CUSTOMER_ID'	=> $quote->f('buyer_id')
	));
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id='".$quote->f('buyer_id')."' ");
	$c_details->next();
	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<h4 >'.gm('Bank Details').'</h4>
	<p><strong>'.gm('Name').': </strong> <span>'.$c_details->f('bank_name').'</span></p>
	<p><strong>'.gm('BIC Code').':</strong> <span>'.$c_details->f('bank_bic_code').'</span></p>
	<p><strong>'.gm('IBAN: ').'</strong> <span>'.$c_details->f('bank_iban').'</span></p>
	';
	}
	if($extra_info){
		$extra_info = '<div style="width: inherit;" >'.$extra_info.'</div>';
	}
}else{
	$view->assign(array(
		'HIDE_BACK'		=>  'hide'
	));
}

// $in['drop_folder'], $in['customer_id'], $in['item_id']
$id= $quote->f('buyer_id');
$is_contact = 0;
if(!$id){
	$id = $quote->f('contact_id');
	$is_contact = 1;
}

$can_do = $db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
									   INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
									   WHERE tblquote_line.quote_id='".$in['quote_id']."' AND tblquote_line.content_type='1' AND tblquote_version.active='1' AND tblquote_line.article_id='0' ORDER BY id ASC  ")->records_count();

// $drop_info = array('drop_folder' => 'quotes', 'customer_id' => $id, 'item_id' => $in['quote_id'], 'isConcact' => $is_contact, 'serial_number' => $serial_number);

// $view->assign(array(
//   'QUOTE_ID'    						=> $in['quote_id'],
//   'PAGE_TITLE'	    				=> $page_title,
//   'S_DATE'									=> $in['s_date'],
//   'SENT_DATE'								=> date(ACCOUNT_DATE_FORMAT,$in['s_date']),
//   'SEARCH'									=> $in['search']? '&search='.$in['search'] : '',
//   'HIDE_ACTIVITY'						=> $_SESSION['access_level'] == 1 ? '' : 'hide',
//   'PICK_DATE_FORMAT'				=> pick_date_format(),
//  	'DEFAULT_CURRENCY_NAME'		=> build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
// 	'HIDE_TOTAL_CURRENCY'			=> $quote->f('currency_type') ? ($quote->f('currency_type') == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide',
// 	'CURRENCY_TYPE'						=> $quote->f('currency_type'),
// 	'EXTRA_INFO'							=> $extra_info,
// 	'is_extra'								=> $extra_info ? true : false,
// 	'page_tip'								=> $tips,
// 	// 'print'						=> ark::run('quote-quote_print_web'),
// 	'drop_info'								=> htmlentities(json_encode($drop_info)),
// 	// 'is_customer'				=> $quote->f('buyer_id') ? true : false,
// 	'is_customer'							=> true,
// 	'is_drop'									=> defined('DROPBOX') && (DROPBOX != '') ? true : false,
// 	'use_weblink'							=> defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1 ? true : false,
// 	'web_url'									=> $cfg['web_link_url'].'?q=',
// 	'can_do'									=> $can_do,
// 	'preview'									=> $quote->f('preview')
// ));

$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id='".$lg."'")->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
	if( ($j % 2) == 0){
		$j++;
		continue;
	}
	$labels[$key] = $value;
	$j++;
}
$can_acc = true;
$can_rej = true;
$new_version = false;

	$quote = $db->query("SELECT * FROM tblquote WHERE id='".$in['quote_id']."'");
	if($quote->next()){
		switch ($quote->f('status_customer')) {
			case 0:
				$new_version = true;
				break;
			case 1:
				$can_acc = false;
				break;
			case 2:
				$can_acc = false;
				break;

			default:
				# code...
				break;
		}
	}
$status_ver = '';
$version_codess = $db->field("SELECT version_code FROM tblquote_version WHERE quote_id='".$in['quote_id']."' AND active='1' ");
if($version_codess){
	$status_ver = ' ['.$version_codess.']';
}

$view->assign(array(
	'bank'				  		=> ACCOUNT_BANK_NAME,
	'bic'				  			=> ACCOUNT_BIC_CODE,
	'iban'				  		=> ACCOUNT_IBAN,
	'bankd_txt'			  	=> $labels['bank_details'],
	'bank_txt'			  	=> $labels['bank_name'],
	'bic_txt'			  		=> $labels['bic_code'],
	'iban_txt'			  	=> $labels['iban'],
	'accept_link'				=> 'index.php?q='.$in['q'].'&do=quote-quote-quote-status&status_customer=2',
	'reject_link'				=> 'index.php?q='.$in['q'].'&do=quote-quote-quote-status&status_customer=1',
	'can_acc'						=> $can_acc,
	'can_rej'						=> $can_rej,
	'new_version'				=> $new_version,
	'acc_txt'						=> gm('Quote Accepted').$status_ver,
	'rej_txt'						=> gm('Quote Rejected').$status_ver,
	'allow_acc'					=> defined('ALLOW_QUOTE_ACCEPT') && ALLOW_QUOTE_ACCEPT == 1 ? true :false,
	'allow_new'					=> defined('ALLOW_QUOTE_NEWV') && ALLOW_QUOTE_NEWV == 1 ? true :false,
	'allow_rej'					=> defined('ALLOW_QUOTE_REJECT') && ALLOW_QUOTE_REJECT == 1 ? true :false,
	'PDF_LINK2'					=> 'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$quote->f('version_id').'&lid='.$quote->f('email_language').$link_end.'&q='.$in['q'],
	'endhrf'						=>'&id='.$in['quote_id'].'&version_id='.$quote->f('version_id').'&lid='.$quote->f('email_language').$link_end.'&q='.$in['q'],
));

global $main_menu_item;
global $sub_menu_item;
$main_menu_item='quotes';
$sub_menu_item='quotes_list';

return $view->fetch('CONTENT');