<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

@session_start();

if(!isset($in['db'])){
	$db =  new sqldb();

}else{
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $in['db'],
		);
	$db =  new sqldb($database_2); // recurring billing
}

$path = ark::$viewpath;


if($in['type']){
	$html='p_order_print_'.$in['type'].'.html';
}else{
	$html='p_order_print_1.html';
}
$db2=new sqldb();
$db3 = new sqldb();
$db4 = new sqldb();
$db_date=new sqldb();

$view = new at(ark::$viewpath.$html);
if(!$in['p_order_id']){ //sample
	//assign dummy data	
$height = 251;
	$factur_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));

	$pdf->setQuotePageLabel(getpo_label_txt('page',$in['lid']));
	
	$view->assign(array(
	'account_logo'        => '../../img/no-logo.png',
	'billing_address_txt' => getpo_label_txt('billing_address',$in['lid']),
	'p_order_note_txt'      => getpo_label_txt('p_order_note',$in['lid']),
	'p_order_txt'         => $in['entry_preview'] ? getpo_label_txt('entry_note',$in['lid']) : getpo_label_txt('p_order',$in['lid']),	
	'invoice_txt'         => getpo_label_txt('invoice',$in['lid']),
	'date_txt'            => getpo_label_txt('date',$in['lid']),
	'customer_txt'        => getpo_label_txt('customer',$in['lid']),
	'article_txt'         => getpo_label_txt('article',$in['lid']),
	'unitmeasure_txt'     => getpo_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => getpo_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => getpo_label_txt('unit_price',$in['lid']),
	'amount_txt'          => getpo_label_txt('amount',$in['lid']),
	'subtotal_txt'        => getpo_label_txt('subtotal',$in['lid']),
	'discount_txt'        => getpo_label_txt('discount',$in['lid']),
	'vat_txt'             => getpo_label_txt('vat',$in['lid']),
	'payments_txt'        => getpo_label_txt('payments',$in['lid']),
	'amount_due_txt'      => getpo_label_txt('amount_due',$in['lid']),
	'notes_txt'           => getpo_label_txt('notes',$in['lid']),
	//	'duedate'			  => getpo_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => getpo_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => getpo_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => getpo_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => getpo_label_txt('iban',$in['lid']),
	'our_ref_txt'		  => getpo_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => getpo_label_txt('your_ref',$in['lid']),
	'phone_txt'			  => getpo_label_txt('phone',$in['lid']),
	'fax_txt'			  => getpo_label_txt('fax',$in['lid']),
	'url_txt'			  => getpo_label_txt('url',$in['lid']),
	'email_txt'			  => getpo_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => getpo_label_txt('vat_number',$in['lid']),
	'article_code_txt'    => getpo_label_txt('article_code',$in['lid']),
	'entry_date_txt'   	  => getpo_label_txt('entry_date',$in['lid']),		
	'entry_note_txt'   	  => getpo_label_txt('entry_note',$in['lid']),
	'delivery_date'		  => date(ACCOUNT_DATE_FORMAT, time()),
	'order_date'          		=> date(ACCOUNT_DATE_FORMAT, time()),
	'is_delivery_date'	  => true,
	'address_info'	      => '[Address]<br>[Zip][City]<br>[Country]',
	'delivery_note'		=> '[Entry note]',
	'notes'			=> '[Notes]',
	
	'seller_name'         => '[Account Name]',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_CITY'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'seller_b_country'    => '[Billing Country]',
	'seller_b_state'      => '[Billing State]',
	'seller_b_city'       => '[Billing City]',
	'seller_b_zip'        => '[Billing Zip]',
	'seller_b_address'    => '[Billing Address]',
	'serial_number'       => '####',
	'invoice_date'        => $factur_date,
	'invoice_vat'         => '##',
	'buyer_name'       	  => '[Customer Name]',
	'buyer_country'    	  => '[Customer Country]',
	'buyer_state'      	  => '[Customer State]',
	'buyer_city'       	  => '[Customer City]',
	'buyer_zip'           => '[Customer ZIP]',
	'buyer_address'    	  => '[Customer Address]',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_city'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	//  'notes'            	  => '[NOTES]',
	'bank'				  => ACCOUNT_BANK_NAME,
	'bic'				  => ACCOUNT_BIC_CODE,
	'iban'				  => ACCOUNT_IBAN,
	// 'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'seller_b_fax'        => '[Account Fax]',
	'seller_b_email'      => '[Account Email]',
	'seller_b_phone'      => '[Account Phone]',
	'seller_b_url'        => '[Account URL]',
	'our_ref'			=> '[Our Reference]',
	'your_ref'			=> '[Your Reference]',
	'hide_f'			  => true,
	'hide_e'			  => true,
	'hide_p'			  => true,
	'hide_u'			  => true,
	'is_delivery'		  => true,
	'sh_discount'		  => 'hide',
	'sh_vat'			  => 'hide',
	'delivery'				=> $in['entry_preview'] ? false : true,		
	));
	

	$i=0;
	while ($i<3)
	{
		$vat_total = 4;
		$total = 120;
		$total_with_vat = 120;
		$view->assign(array(
		'article'  			    	=> 'Article Name',
		'quantity'      			=> display_number(2),
		'price_vat'      			=> display_number(40),
		'price'         			=> display_number_var_dec(20),
		'percent'					=> display_number(5),
		'vat_value'					=> display_number(1),
		'is_article_code'		  => true,
		'content'			=> true,
		'article_width'		=> 40,
		'delivery'				=> $in['entry_preview'] ? false : true,
		'q_class'			  => $in['entry_preview'] ? 'last right' : '',
		),'order_row');

		if($in['type']==4 &&$in['entry_preview']){
			$view->assign('article_width',68,'order_row');
		}

		$view->loop('order_row');
		$i++;

	}
	$view->assign(array(
	'vat_total'				=> display_number($vat_total),
	'total'					=> display_number($total),
	'total_vat'				=> display_number($total_with_vat),
	'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
	'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
	'currency_type'			=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
	));


}else{
	$db = new sqldb();
	$db->query("SELECT pim_p_orders.* FROM pim_p_orders
			LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
			WHERE pim_p_orders.p_order_id='".$in['p_order_id']."'");
	$db->move_next();
	$serial_number = $db->f('serial_number');
    $allow_article_packing=$db->f('use_package');
    $allow_article_sale_unit=$db->f('use_sale_unit');
	$currency = get_commission_type_list($db->f('currency_type'));
	$vat = $db->f('vat_percent');

	$currency_type = $db->f('currency_type');
	$currency_rate = $db->f('currency_rate');

	
	$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
	$delivery_date = date(ACCOUNT_DATE_FORMAT,  $db->f('del_date'));
	$deliv_date = $db_date->field("SELECT date FROM pim_p_order_deliveries WHERE p_order_id = '".$in['p_order_id']."' AND delivery_id = '".$in['delivery_id']."' ");
	$db_date->query("SELECT minute,hour,pm FROM pim_p_order_deliveries WHERE p_order_id = '".$in['p_order_id']."' AND delivery_id = '".$in['delivery_id']."' ");
	$delivery_del_date = date(ACCOUNT_DATE_FORMAT,  $deliv_date);
	//	$due_date = date(ACCOUNT_DATE_FORMAT,  $db->f('due_date'));
	if($db_date->f('hour')){
		$delivery_del_date.= ' ('.$db_date->f('hour').':'.$db_date->f('minute').' '.$db_date->f('pm').')';
	}

	$img = '../img/no-logo.png';
	$attr = '';
	if(ACCOUNT_LOGO_PO_ORDER){
		$img = '../'.ACCOUNT_LOGO_PO_ORDER;
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}

	$notes = $db3->field("SELECT item_value FROM note_fields 
								   WHERE item_type = 'p_order' 
								   AND item_name = 'notes' 
								   AND active = '1' 
								   AND item_id = '".$in['p_order_id']."' ");	

	$pdf->setQuotePageLabel(getpo_label_txt('page',$in['lid']));

	$view->assign(array(
	'account_logo'        => $img,
	'attr'				  => $attr,
	'billing_address_txt' => getpo_label_txt('billing_address',$in['lid']),
	'p_order_note_txt'    => getpo_label_txt('p_order_note',$in['lid']),
	'p_order_txt'         => $in['delivery_id'] ? getpo_label_txt('entry_note',$in['lid']) : getpo_label_txt('p_order',$in['lid']),	
	'date_txt'            => getpo_label_txt('date',$in['lid']),
	'customer_txt'        => getpo_label_txt('customer',$in['lid']),
	'article_txt'         => getpo_label_txt('article',$in['lid']),
	'unitmeasure_txt'     => getpo_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => getpo_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => getpo_label_txt('unit_price',$in['lid']),
	'amount_txt'          => getpo_label_txt('amount',$in['lid']),
	'subtotal_txt'        => getpo_label_txt('subtotal',$in['lid']),
	'discount_txt'        => getpo_label_txt('discount',$in['lid']),
	'vat_txt'             => getpo_label_txt('vat',$in['lid']),
	'payments_txt'        => getpo_label_txt('payments',$in['lid']),
	'total_txt'      	  => getpo_label_txt('total',$in['lid']),
	'notes_txt'           => $notes ? getpo_label_txt('notes',$in['lid']) : '',
	'duedate_txt'		  => getpo_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => getpo_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => getpo_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => getpo_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => getpo_label_txt('iban',$in['lid']),
	'gov_taxes_txt'       => getpo_label_txt('gov_taxes',$in['lid']),
	'gov_taxes_code_txt'  => getpo_label_txt('gov_taxes_code',$in['lid']),
	'gov_taxes_type_txt'  => getpo_label_txt('gov_taxes_type',$in['lid']),
	'sale_unit_txt'		  => getpo_label_txt('sale_unit',$in['lid']),
	'package_txt'		  => getpo_label_txt('package',$in['lid']),
	'delivery_note_txt'	  => getpo_label_txt('delivery_note',$in['lid']),
	'sale_unit_txt'		  => getpo_label_txt('sale_unit',$in['lid']),
	'package_txt'		  => getpo_label_txt('package',$in['lid']),
	'our_ref_txt'		  => getpo_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => getpo_label_txt('your_ref',$in['lid']),
	'phone_txt'			  => get_label_txt('phone',$in['lid']),
	'fax_txt'			  => get_label_txt('fax',$in['lid']),
	'url_txt'			  => get_label_txt('url',$in['lid']),
	'email_txt'			  => get_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => get_label_txt('vat_number',$in['lid']),
	'entry_date_txt'   	  => getpo_label_txt('entry_date',$in['lid']),		
	'entry_note_txt'   	  => getpo_label_txt('entry_note',$in['lid']),
	'article_code_txt'    => getpo_label_txt('article_code',$in['lid']),

	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',

	'serial_number'		  => $serial_number,
	'order_date'          => $factur_date,
	'buyer_name'       	  => $db->f('customer_name'),
	'buyer_country'    	  => get_country_name($db->f('customer_country_id')),
	'buyer_state'      	  => $db->f('customer_state'),
	'buyer_city'       	  => $db->f('customer_city'),
	'buyer_zip'        	  => $db->f('customer_zip'),
	'buyer_address'    	  => $db->f('customer_address'),
	'seller_name'         => $db->f('seller_name'),
	'seller_d_country'    => get_country_name($db->f('seller_d_country_id')),
	'seller_d_state'      => get_state_name($db->f('seller_d_state_id')),
	'seller_d_city'       => utf8_decode($db->f('seller_d_city')),
	'seller_d_zip'        => $db->f('seller_d_zip'),
	'seller_d_address'    => utf8_decode($db->f('seller_d_address')),
	'p_order_contact_name'=> $db->f('contact_name'),
	'field_customer_id'   => $db->f('field')=='customer_id'?true:false,
	//	'INVOICE_BUYER_VAT'   => ACCOUNT_VAT_NUMBER,
	'bank'				  => ACCOUNT_BANK_NAME,
	'bic'				  => ACCOUNT_BIC_CODE,
	'iban'				  => ACCOUNT_IBAN,
	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'sh_discount'		  => ($db->f('discount')==0.00)?'hide':'',
	'discount_percent'    => ' ( '.$db->f('discount').'% )',
	'notes'               =>  $notes ? nl2br($notes) : '',
	'view_notes'          => $notes ? '' : 'hide' ,
	'hide_default_total'  => 'hide',//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide', 
	'def_currency'		  => build_currency_name_list(ACCOUNT_CURRENCY_TYPE),	
	'your_ref'       	  => $db->f('your_ref'),
	'our_ref' 			  => $db->f('our_ref'),
	'hide_our_ref'		  => $db->f('our_ref')? '':'hide',
	'hide_your_ref'		  => $db->f('your_ref')? '':'hide',

	'seller_b_fax'        => ACCOUNT_FAX,
	'seller_b_email'      => ACCOUNT_EMAIL,
	'seller_b_phone'      => ACCOUNT_PHONE,
	'seller_b_url'        => ACCOUNT_URL,
	
	'hide_f'			  => defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? false : true,
	'hide_e'			  => defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? false : true,
	'hide_p'			  => defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? false : true,
	'hide_u'			  => defined('ACCOUNT_URL') && !ACCOUNT_URL ? false : true,
	'address_info'	      => nl2br($db->f('address_info')),
	'delivery_date'		  => $delivery_date,
	'is_delivery_date'	  => ($in['delivery_id'] && $delivery_date) ? false : true,
	'delivery_del_date'	  => $delivery_del_date,
	'is_delivery_del_date'=> ($in['delivery_id'] && $delivery_del_date) ? true : false,
	));
$height = 251;
	$discount_percent= $db->f('discount');
	if($db->f('discount') == 0){
		$view->assign('sh_discount','hide');
		$discount_procent=0;
	} else {
		$discount_procent = $db->f('discount');
		$view->assign('total_discount_procent',$db->f('discount'));
	}

 if($db->f('delivery_address')){
	$view->assign(array(
				'delivery_address'=>nl2br($db->f('delivery_address')),
				'is_delivery'=>true
			));	
}else{
	$view->assign(array(
				'delivery_address'=>'',
				'is_delivery'=>false
			));	
}

	$i=0;
	$discount_total = 0;
	$show_discount = true;

	$vat_total = 0;
	$total = 0;
	$total_with_vat = 0;
	if($in['delivery_id']){
		$first_line_content_data = $db4->query("SELECT article, content FROM pim_p_order_articles WHERE p_order_id = '".$in['p_order_id']."' ORDER BY order_articles_id ASC LIMIT 1");
		$first_line_content_data->next();
		$is_first_content_line = false;
		if($first_line_content_data->f('content')=='1'){
			$is_first_content_line = true;
			$view->assign(array(
			'is_content_line2'			=> true,		
			'content_line2'				=> $first_line_content_data->f('article'),
			'is_first_content_line'		=> $is_first_content_line,
			));
			
		}	
		$db->query("SELECT pim_p_order_articles.*, pim_p_orders_delivery.* FROM pim_p_order_articles 
					INNER JOIN pim_p_orders_delivery ON pim_p_orders_delivery.order_articles_id=pim_p_order_articles.order_articles_id 
					WHERE pim_p_orders_delivery.p_order_id='".$in['p_order_id']."' AND pim_p_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");				
		$content_line_data = $db4->query("SELECT order_articles_id, article FROM pim_p_order_articles WHERE p_order_id = '".$in['p_order_id']."' AND content = '1' ");
		$is_content_line = false;
		$content_line = array();
		while($content_line_data->next()){
			$content_line[$content_line_data->f('order_articles_id')] = $content_line_data->f('article');
			$is_content_line = true;
		}	
	}else{
		$db->query("SELECT pim_p_order_articles.* FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order ASC");	
		$is_content_line = false;	
	}
	while ($db->move_next())
	{
		if($discount_percent != $db->f('discount')){
			$show_discount = false;
		}
		if ($in['type']==4 && $in['delivery_id'])
		{
			if($delivery_date || $delivery_del_date){
				$view->assign('width_if_del','40');
			}else{
				$view->assign('width_if_del','68');
			}
			$view->assign('article_width','68','order_row');
		}
		elseif($in['type']==4 && !$in['delivery_id'] && $db->f('content')){	
			$view->assign('article_width','94','order_row');			
		}
		else{

			$view->assign('article_width','38','order_row');

			if($delivery_date || $delivery_del_date){
				$view->assign('width_if_del','40');
			}else{
				$view->assign('width_if_del','68');
			}
		}

		if($is_content_line == true){			
			foreach ($content_line as $key => $value) {
				$after_order_articles_id = $db->f('order_articles_id')+1;
				// echo $after_order_articles_id; echo " / "; echo $key; echo "<br>";
				if($key == $after_order_articles_id){
					$is_spec_content_line = true;
					$spec_content_line = $value;
					goto found;
				}else{
					$is_spec_content_line = false;
				}
			}			
		}
		found:
		// var_dump($is_content_line);
		// var_dump($is_spec_content_line);
		$discount_total += ($db->f('price')*$db->f('discount')/100)*$db->f('quantity')*($db->f('packing')/$db->f('sale_unit'));
		$price_line = $db->f('price') ;
		$vat_total += ($price_line*$db->f('vat_percent')/100)*$db->f('quantity')* ($db->f('packing')/$db->f('sale_unit'));
		$total += $db->f('price')*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));
		// $total_with_vat += $db->f('price_with_vat')*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));
			

		//if show serial numbers 
		if($in['delivery_id'] && $is_content_line == false && (SHOW_ART_S_N_ORDER_PDF==1)){			
			$art_serial_number_list = '';
			$use_serial_no = $db3->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$db->f('article_id')."' ");
			if($use_serial_no){				
				$serial_number_data = $db3->query("SELECT serial_number FROM serial_numbers WHERE p_delivery_id = '".$in['delivery_id']."' AND article_id = '".$db->f('article_id')."' ");
				$k=0;
				while($serial_number_data->next()){
					$art_serial_number_list .= "<br/>".$serial_number_data->f('serial_number');
					$k++;
				}						
				$art_serial_number_list = "<br/>".gm("Serial Numbers").':'.$art_serial_number_list;
				$view->assign(array(
					'is_serial_number'		=> $k > 0 ? true : false,	
					// 'count_s_n'				=> $k+1,	
					// 'serial_nr_txt'			=> wrap2(gm('Serial Numbers')),
					'art_serial_number_list'	=> $art_serial_number_list,
				),'order_row');									
			}
		}	

		//if show batch numbers 
		if($in['delivery_id'] && $is_content_line == false && (SHOW_ART_B_N_ORDER_PDF==1)){			
			$art_batch_number_list = '';
			$use_batch_no = $db3->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$db->f('article_id')."' ");
			if($use_batch_no){				
				$batch_number_data = $db3->query("SELECT * FROM batches WHERE p_delivery_id = '".$in['delivery_id']."' AND article_id = '".$db->f('article_id')."' ");
				$k=0;
				while($batch_number_data->next()){									
					$art_batch_number_list .= "<br/>".$batch_number_data->f('batch_number');
					$k++;
				}						
				$art_batch_number_list = "<br/>".gm("Batches").':'.$art_batch_number_list;
				$view->assign(array(
					'is_batch_number'		=> $k > 0 ? true : false,						
					'art_batch_number_list'	=> $art_batch_number_list,
				),'order_row');				
			}
		}


		$view->assign(array(
		'article'  			    	=> $db->f('article'),
		'quantity'      			=> display_number($db->f('quantity')),
		'price_vat'      			=> display_number($db->f('quantity') * $price_line * ($db->f('packing') / $db->f('sale_unit'))), 
		'price'         			=> display_number_var_dec($db->f('price')),
		'percent'					=> display_number($db->f('vat_percent')),
		'vat_value'					=> display_number($db->f('vat_value')),
		'discount'					=> display_number($db->f('discount')),
		'sale_unit'					=> $db->f('sale_unit'),
		'price_with_vat'            => display_number($db->f('price')+$db->f('vat_value')),
		'packing'					=> display_number($db->f('packing')),
		'q_class'					=> $in['delivery_id'] ? 'last' : '',
		'colspan'					=> $db->f('content') ? ' colspan="8" ' : '',
		'content'					=> $db->f('content') ? false : true,
		'content_class'				=> $db->f('content') ? ' last_nocenter ' : '',
		'is_article_code'			=> $db->f('article_code') ? true : false,
		'article_code'				=> $db->f('article_code'),
		'is_content_line'			=> ($is_content_line && $is_spec_content_line) ? true : false,		
		'content_line'				=> $spec_content_line,
		),'order_row');

		$view->loop('order_row');

		$view->assign(array('delivery_note'	=> nl2br($db->f('delivery_note'))));
		$delivery_note = $db->f('delivery_note');

		$i++;

	}	
	$view->assign('delivery',$in['delivery_id'] ? false : true);

// $discount_total=$total*(($discount_percent)/100);
	
	$total=$total-$discount_total;
	
	$total_with_vat=$vat_total+$total;
    
	if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){ 
			$total_default = $total_with_vat*return_value($currency_rate);
		}

	$view->assign(array(
	'discount_total'        		=> place_currency(display_number(number_format($discount_total,2)),get_commission_type_list($currency_type)), 
	'vat_total'						=> place_currency(display_number($vat_total),get_commission_type_list($currency_type)),
	'total'							=> place_currency(display_number($total),get_commission_type_list($currency_type)),
	'total_vat'						=> place_currency(display_number($total_with_vat + $total_gov_taxes),get_commission_type_list($currency_type)),
	'gov_taxes_value'       		=> place_currency(display_number($total_gov_taxes),get_commission_type_list($currency_type)),
	'total_amount_due_default'		=> place_currency(display_number($total_default)),
	'hide_currency2'				=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
	'hide_currency1'				=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
	'currency'						=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
	'discount_percent'				=> $show_discount ? ' ( '.$discount_percent.'% )' : "",	
	));

	if($vat_total == 0){
		$view->assign('sh_vat','hide');
	}

}

if($allow_article_sale_unit && $allow_article_packing){
	$view->assign('article_width',14);
}elseif($allow_article_packing && !$allow_article_sale_unit){
	$view->assign('article_width',14);
}else{
	$view->assign('article_width',27);
}

if($in['delivery_id']){
	$view->assign(array(
		'article_width'			=> 69,
		'show_delivery_note'	=> $delivery_note ? true : false ));
}else{
	$view->assign(array(
		'article_width'			=> 47,
		'show_delivery_note'	=> false ));
}
if($in['entry_preview']){
	$view->assign('show_delivery_note'	, $in['entry_preview'] ? true : false);	
}

$view->assign(array(
    	'allow_article_packing'	=> $allow_article_packing,
       	'allow_article_sale_unit'=> $allow_article_sale_unit
));

if($in['entry_preview']){
	$view->assign('article_width'	, 69 );	
}

$is_table = true;
$is_data = false;

#for layout nr 4.
if($hide_all == 1){
	// $view->assign('hide_all','hide');
	$is_table = false;
	$is_data = true;
}else if($hide_all == 2){
	$is_table = true;
	$is_data = false;
	// $view->assign('hide_t','hide');
}

$view->assign(array(
	'is_table'=> $is_table,
	'is_data'=>$is_data,
));
return $view->fetch();