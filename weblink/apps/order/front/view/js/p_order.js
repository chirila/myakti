$(function(){


  $(window).scroll(function () {
    var y = $(window).scrollTop();
    if(y>x.top && y< x.top+970){
      $('.bx-controls-direction').addClass('addfloat');
    }else{
      $('.bx-controls-direction').removeClass('addfloat');
    }
    if(y>z.top){
     $('.poweredby').addClass('addfloat');

    }else{
      $('.poweredby').removeClass('addfloat');
    }
  });



  $('.activity').on('focus','.textarea_logging',function(){
    setTimeout(function(){ setX(); }, 300);
  }).on('click','.comment_cancel',function(e){
    setTimeout(function(){ setX(); }, 300);
  }).on('click','.add_c',function(e){
    setTimeout(function(){ setX(); }, 300);
  });
  setX();

  var slider;
  function build_slider(){
    if(slider){
      slider.destroySlider();
    }
    slider = $('.bxslider').bxSlider({
      nextText: '',
      prevText: '',
      pager: false,
      infiniteLoop: false,
      show_pages: true,
      onSlideAfter: function($slideElement, oldIndex, newIndex){
        $('.selected_bx_page').val(slider.getCurrentSlide()+1);
      },
      onSliderLoad: function(){
        setX();
        $(window).scroll();
      }
    });
    return slider;
  }
  if(m===false){
    $.getJSON('index.php?do=quote-quote_print_web'+$('#qs').val(),function(o){
        $('.wlink_content').html(o.innerHTML);
        build_slider();
    });
  }



   $('#reject').on('click',function(e){
      e.preventDefault();
	  $('.buttons-main li').removeClass('modal_pos');
	  var m = $(this);
    var mPos = m.position();
	  callModal(m, mPos);
      $('#buttons_modal .textarea_logging').val('').focus();
      $('.status_customer').val('1');
      $('.new_version').val('0');
    });
    $('#newV').on('click',function(e){
      e.preventDefault();
	  $('.buttons-main li').removeClass('modal_pos');
	  var m = $(this);
      var mPos = m.position();
	  callModal(m, mPos);
      $('#buttons_modal .textarea_logging').val('').focus();
      $('.new_version').val('1');
      $('.status_customer').val('0');
    });
    $('#accept').on('click',function(e){
      e.preventDefault();
	  $('.buttons-main li').removeClass('modal_pos');
	  var m = $(this);
      var mPos = m.position();
	  callModal(m, mPos);
      $('#buttons_modal .textarea_logging').focus();
      $('.status_customer').val('2');
      $('.new_version').val('0');
    });

	$('.wlink_options').on('click',function(e){
		e.preventDefault();
		$('.wlink_controls').toggleClass("open-menu");
		// $('.wlink_main').toggleClass("make_room");
		$('.buttons-main li').removeClass('modal_pos')
		$('#buttons_modal').css({top: '-1000px'});
    });

	function setWidth() {
    if($('.iefix').length){
      // do nothing
    }else{
      if ($(window).width()<=1009) {
  			$('.activity').css('width', $(window).width());
  		}
  		else if ($(window).width()>1009) {
  			$('.activity').css('width', $('.wlink_right').width());
  		}
    }
	}
	function callModal(m, mPos) {
    if($('.iefix').length){
      // we are in IE and everything is fucked up
      $('#buttons_modal').css({top:(mPos.top-29)+'px'}).show();
      $(m).parent().addClass('modal_pos');
    }else if ($(window).width()<769) {
    	$('#buttons_modal').css({top:(mPos.top+110)+'px'}).show();
			$(m).parent().addClass('modal_pos');
		}
		else if ($(window).width()<1025)  {
    	$('#buttons_modal').css({top:(mPos.top+132)+'px'}).show();
			$(m).parent().addClass('modal_pos');
		}
		else if ($(window).width()>1025) {
    	$('#buttons_modal').css({top:(mPos.top-29)+'px'}).show();
		}
	}

	$(window).scroll(function () {
		setWidth();
	});

	$(window).resize(function () {
		setWidth();
	});

	$(window).load(function () {
		setWidth();
	});

});
