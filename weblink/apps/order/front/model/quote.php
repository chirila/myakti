<?php
	/**
	 * the quote class
	 *
	 */
class quote{

	var $db;
	// var $dbu_users;
	var $pag = 'quote'; 						# used for log messages
	var $field_n = 'quote_id';					# used for log messages

	/**
	 * constructor
	 */
	function __construct(){
		$this->db = new sqldb();
	}

	function status(&$in){
		$q = $this->db->query("SELECT id,buyer_name from tblquote where id=".obj::get('id'));
		$cust_id = $this->db->field("SELECT buyer_id FROM tblquote WHERE id='".obj::get('id')."'");
		if(!$q->next()){
			$in['error'] = gm("Invalid ID");
			return false;
		}
		$time = time();

		$this->db->query("UPDATE tblquote SET status_customer='".$in['status_customer']."', status_c_date='".$time."',sent='1' WHERE id=".obj::get('id'));
		$this->db->query("UPDATE tblquote_version SET active = 0 WHERE quote_id='".obj::get('id')."'");
		$this->db->query("UPDATE tblquote_version SET active = 1 WHERE quote_id='".obj::get('id')."' AND version_id = '".obj::get('version_id')."'");

		if ($in['status_customer'] == 1) {
			$this->db->query("UPDATE tblquote SET lost_id='".$in['lost_id']."' WHERE id=".obj::get('id'));
		}

		$in['date'] = date(ACCOUNT_DATE_FORMAT,$time);
		$opts = array('{l}Pending{endl}','{l}Rejected{endl}', '{l}Accepted{endl}', '{l}Accepted{endl}', '{l}Accepted{endl}', '{l}Draft{endl}');
		$the_time = time();
		$this->db->query("INSERT INTO tblquote_history SET quote_id='".obj::get('id')."', date='".$the_time."', message=(SELECT version_code FROM tblquote_version WHERE version_id='".obj::get('version_id')."' ) ");
		insert_message_log($this->pag,'{l}Approval status changed to{endl} '.$opts[$in['status_customer']].' {l}by{endl} '.addslashes($q->f('buyer_name')),$this->field_n,obj::get('id'),false,$_SESSION['u_id'],true,$cust_id,$the_time);
		msg::$success = gm("Quote has been updated");
		return true;
	}


}