<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

switch ($in['error']) {
	case '404':
		$text = "Error: 404. Page not found.<br> <a href=\"index.php\">Home page</a>";
		break;
	
	default:
		$text = "We have encountered an error. The technical staff has been notified about this issue.<br> <a href=\"index.php\">Home page</a>"; 
		break;
}

$view->assign(array(
	'error'	=> $text
));

return $view->fetch();