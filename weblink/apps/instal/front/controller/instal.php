<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

switch ($_SESSION['step']) {
	case '2':
		$instalpage = ark::run('instal-first_time');
		break;	
	case '1':
		$instalpage = ark::run('instal-second_step');
		break;
	default:
		$instalpage = ark::run('instal-second_step');
		break;
}

$view->assign(array('instal_page'=>$instalpage));


return $view->fetch();