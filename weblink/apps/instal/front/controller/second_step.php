<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$view->assign(array(
	'do_next'		=> 'instal-second_step-instal-set_config',
));
$errors = msg::get_all_messages();

foreach ($errors['msg'] as $key => $value) {
	$view->assign(array('show_'.$key => 'error'));
}

global $config;
foreach ($config as $key => $value) {
	if(is_bool($value)){
		if($value === true){
			$value = 'true';
		}else{
			$value = 'false';
		}
	}
	$view->assign(array(
		'name' 	=> $key,
		'value'	=> $value,
	),'config');
	$view->loop('config');
}
$is_success = false;
if(msg::$success){
	$is_success = true;
}
$view->assign(array('is_success'=>$is_success));
return $view->fetch();