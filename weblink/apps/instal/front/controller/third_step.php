<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$errors = msg::get_all_messages();

foreach ($errors['msg'] as $key => $value) {
	$view->assign(array('show_'.$key => 'error'));
}

$is_success = false;
if(msg::$success){
	$is_success = true;
}
$view->assign(array(
	'is_success'	=> $is_success,
	'do_next'		=> 'instal-third_step-instal-set_defaults',
));

return $view->fetch();