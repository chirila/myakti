<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$view->assign(array(
	'do_next'		=> 'instal-first_step-instal-db_settings',
));
$errors = msg::get_all_messages();

if($errors['msg']){
	foreach ($errors['msg'] as $key => $value) {
		$view->assign(array('show_'.$key => 'error'));
	}
}
$is_error = false;
if($errors['error'] && !$errors['msg']){
	
	$is_error = true;

}
$view->assign(array(
	'is_error'=>$is_error,
	'big_error'=>$errors['error'],
));

return $view->fetch();