<?php

/**
* instalation class
*/
class instal
{
	var $dirs = array();  # here we keep the folders of the new created application
	var $folders = array('controller','layout','model','perm','view');  # the folders to be created when creating a new application
	
	/**
	* the contructor
	* we dont use it for now
	**/
	function __construct()
	{
		# code...
	}

	/**
	 * set up the database configuration file to connect to the database
	 *
	 * @return true
	 * @param array in
	 * @author Marius Pop
	 **/
	function db_settings(&$in)
	{
		if(!$this->db_settings_valid($in)){
			return false;
		}
		
		$app = file_get_contents(INSTALLPATH.'config/database.php');

		$app = preg_replace("/'hostname'(\s+)=>(\s+)'(\w+)'/", "'hostname'   => '".$in['hostname']."'", $app);
		$app = preg_replace("/'username'(\s+)=>(\s+)'(\w+)'/", "'username'   => '".$in['username']."'", $app);
		$app = preg_replace("/'password'(\s+)=>(\s+)'(\w+)'/", "'password'   => '".$in['password']."'", $app);
		$app = preg_replace("/'database'(\s+)=>(\s+)'(\w+)'/", "'database'   => '".$in['database']."'", $app);
		
		file_put_contents(INSTALLPATH.'config/database.php',$app);
		ark::run('instal-second_step');
		return true;
	}

	/**
	 * we need to check if all the field are filed
	 *
	 * @return void
	 * @author 
	 **/
	function db_settings_valid(&$in)
	{
		$is_ok = true;
		$v=new validation();
		$v->f('hostname','Username','required');
		$v->f('username','Password','required');
		// $v->f('password','Password','required');
		$v->f('database','Password','required');
		if($v->run()){
			if(!$this->test_connection($in)){
				$is_ok = false;
			}
		}else{
			$is_ok = false;
		}		
		return $is_ok;
	}

	/**
	 * the the sql connection
	 *
	 * @return true
	 * @author Marius Pop
	 **/
	function test_connection(&$in)
	{
		try {
			$db = new PDO("mysql:host=".$in['hostname'], $in['username'], $in['password']);
			$db->exec("CREATE DATABASE `".$in['database']."`;
					   USE `".$in['database']."` ");
			$error = $db->errorInfo();
			if(empty($error[2])){
				$this->create_tables($db);
			}else{
				msg::error($error[2]);
				return false;
			}			
		}
		catch  (PDOException $e) {
			msg::error($e->getMessage());

			return false;
		}
		return true;
	}

	/**
	 * create the table for the new database
	 *
	 * @return void
	 * @author Marius Pop
	 **/
	function create_tables($db)
	{
		$db->exec("DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `constant_name` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) DEFAULT NULL,
  `long_value` text,
  `module` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_level` tinyint(4) NOT NULL DEFAULT '2',
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1; ");
	}

	/**
	 * Set up the config file
	 *
	 * @return bool 
	 * @author Marius Pop
	 **/
	function set_config(&$in)
	{
		global $config;
		$app = file_get_contents(INSTALLPATH.'config/config.php');
		foreach ($config as $key => $value) {
			if(is_bool($value)){
				if($value === true){
					$value = 'true';
				}else{
					$value = 'false';
				}
			}
			$app = preg_replace("/config\['".$key."'\] = '(.*)'/", "config['".$key."'] = '".$in[$key]."'", $app);
			$app = preg_replace("/config\['".$key."'\] = (\w+)/", "config['".$key."'] = ".$in[$key]."", $app);
		}

		file_put_contents(INSTALLPATH.'config/config.php',$app);
		ark::run('instal-third_step');
		return true;
	}

	/**
	 * helper function to create controllers
	 *
	 * @return true
	 * @author Marius Pop
	 **/
	function creat_controller(&$in)
	{
		if(!$this->creat_controller_validate($in)){
			return false;
		}
		$data_php = '<?php if(!defined(\'BASEPATH\')) exit(\'No direct script access allowed\');

#code here...

return $view->fetch();';
		$data_html = 'Hello world!';

		if(file_put_contents(APPSPATH.$in['app'].'/'.$in['enviroment'].'/controller/'.$in['controller'].'.php', $data_php) === false){
			msg::set('controller','Failed to create controller');
			return false;
		}

		if(file_put_contents(APPSPATH.$in['app'].'/'.$in['enviroment'].'/view/'.$in['controller'].'.html', $data_html) === false){
			msg::set('controller','Failed to create view');
			return false;
		}
		msg::$success = "Files created";

		return true;
	}

	/**
	 * here we validate the creation of controllers
	 * we check if we already have the files
	 * @return void
	 * @author 
	 **/
	function creat_controller_validate(&$in)
	{
		$is_ok = true;
		$v=new validation();
		$v->f('controller','Username','required');
		$v->f('app','Password','required');
		if($v->run()){
			if(file_exists(APPSPATH.$in['app'].'/'.$in['enviroment'].'/controller/'.$in['controller'].'.php')){
				msg::set('controller','Controller already exists');
				$is_ok = false;
			}else if(file_exists(APPSPATH.$in['app'].'/'.$in['enviroment'].'/view/'.$in['controller'].'.html')){
				msg::set('controller','Template already exists');
				$is_ok = false;
			}
		}else{
			$is_ok = false;
		}
		return $is_ok;
	}
	
	/**
	 * helper to create new application
	 *
	 * @return true
	 * @author Marius Pop
	 **/
	function add_app(&$in)
	{
		if(!$this->add_app_validate($in)){
			return false;
		}
		
		$perm_data_controller = '<?php if(!defined(\'BASEPATH\')) exit(\'No direct script access allowed\');
perm::controller(\'all\', \'all\');';
		$perm_data_model = '<?php if(!defined(\'BASEPATH\')) exit(\'No direct script access allowed\');
perm::model(\'all\',\'all\', \'all\');';
		$file = 'apps/index.html';
		#create the app folder
		if(mkdir(APPSPATH.$in['apps_name']) === false){
			msg::set('apps_name','Failed to create application folder');
			return false;
		}else{
			# create the folder structure of the application
			mkdir(APPSPATH.$in['apps_name'].'/admin');
			mkdir(APPSPATH.$in['apps_name'].'/front');
			foreach($this->folders as $key){
				mkdir(APPSPATH.$in['apps_name'].'/admin/'.$key);
			}
			foreach($this->folders as $key){
				mkdir(APPSPATH.$in['apps_name'].'/front/'.$key);
			}
			mkdir(APPSPATH.$in['apps_name'].'/front/view/js');
			mkdir(APPSPATH.$in['apps_name'].'/admin/view/js');
			# add the necesery files
			file_put_contents(APPSPATH.$in['apps_name'].'/front/perm/controller.php',$perm_data_controller);
			file_put_contents(APPSPATH.$in['apps_name'].'/front/perm/model.php',$perm_data_model);
			file_put_contents(APPSPATH.$in['apps_name'].'/admin/perm/controller.php',$perm_data_controller);
			file_put_contents(APPSPATH.$in['apps_name'].'/admin/perm/model.php',$perm_data_model);
			$this->show_tree(APPSPATH.$in['apps_name'].'/*');
			foreach($this->dirs as $key){
				copy($file, $key.'/index.html');
			}
			# and last but not least we register our new application
			$app = file_get_contents(INSTALLPATH.'config/apps.php');

			$pos_app = strrpos($app,'$apps[\'');
			$last_app = strpos($app,';',$pos_app)+1;
			$string1 = substr($app,0,$last_app);
			$string2 = substr($app,$last_app);
			$apps_config = $string1."\n".'$apps[\''.$in['apps_name'].'\'] = 1;'.$string2;




			file_put_contents(INSTALLPATH.'config/apps.php',$apps_config);
		}
		
		msg::$success = "Application created";
		global $apps;
		$apps[$in['apps_name']] = 1;
		// header("Location: index.php?do=instal-third_step");
		return true;
	}
	
	/**
	 * validate create app
	 * we check to see if we already have the application 
	 * @return true
	 * @author Marius Pop
	 **/
	function add_app_validate(&$in)
	{
		$is_ok = true;
		$v=new validation();
		$v->f('apps_name','Username','required');
		if($v->run()){
			if(file_exists(APPSPATH.$in['apps_name'])){
				msg::set('apps_name','Application already exists');
				$is_ok = false;
			}
		}else{
			$is_ok = false;
		}
		
		return $is_ok;
	}
	
	/**
	 * helper function to travers tree
	 *
	 * @return true
	 * @author Marius Pop
	 **/
	function show_tree($dir)
	{
		$search_in = array();
		foreach(glob($dir) as $file){
			/*if(filetype($file) == 'file'){
				if(!array_key_exists($dir,$files)){
					$files[$dir] = array();
				}
				array_push($files[$dir],$file);
			}
			else*/if(filetype($file) == 'dir'){
				array_push($search_in,$file);
				array_push($this->dirs,$file);
			}
		}
		foreach($search_in as $dir){
			$this->show_tree($dir.'/*');
		}

	}
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function set_defaults(&$in)
	{

		if(!$this->set_defaults_valid($in)){
			return false;
		}
		$perm_data_controller = '<?php if(!defined(\'BASEPATH\')) exit(\'No direct script access allowed\');
perm::controller(\'all\', \'all\');';
		$perm_data_model = '<?php if(!defined(\'BASEPATH\')) exit(\'No direct script access allowed\');
perm::model(\'all\',\'all\', \'all\');';
		$file = 'apps/index.html';
		#create the app folder
		if(mkdir(APPSPATH.$in['apps_name']) === false){
			msg::set('apps_name','Failed to create application folder');
			return false;
		}else{
			# create the folder structure of the application
			mkdir(APPSPATH.$in['apps_name'].'/admin');
			mkdir(APPSPATH.$in['apps_name'].'/front');
			foreach($this->folders as $key){
				mkdir(APPSPATH.$in['apps_name'].'/admin/'.$key);
			}
			foreach($this->folders as $key){
				mkdir(APPSPATH.$in['apps_name'].'/front/'.$key);
			}
			mkdir(APPSPATH.$in['apps_name'].'/front/view/js');
			mkdir(APPSPATH.$in['apps_name'].'/admin/view/js');
			# add the necesery files
			file_put_contents(APPSPATH.$in['apps_name'].'/front/perm/controller.php',$perm_data_controller);
			file_put_contents(APPSPATH.$in['apps_name'].'/front/perm/model.php',$perm_data_model);
			file_put_contents(APPSPATH.$in['apps_name'].'/admin/perm/controller.php',$perm_data_controller);
			file_put_contents(APPSPATH.$in['apps_name'].'/admin/perm/model.php',$perm_data_model);
			$this->show_tree(APPSPATH.$in['apps_name'].'/*');
			foreach($this->dirs as $key){
				copy($file, $key.'/index.html');
			}
			# and last but not least we register our new application
			$app = file_get_contents(INSTALLPATH.'config/apps.php');

			$pos_app = strrpos($app,'$apps[\'');
			$last_app = strpos($app,';',$pos_app)+1;
			$string1 = substr($app,0,$last_app);
			$string2 = substr($app,$last_app);
			$apps_config = $string1."\n".'$apps[\''.$in['apps_name'].'\'] = 1;'.$string2;

			$apps_config = preg_replace("/default_app\['admin'\](\s+)=(\s+)'(\w+)'/", "default_app['admin'] = '".$in['apps_name']."'", $apps_config);
			$apps_config = preg_replace("/default_app\['front'\](\s+)=(\s+)'(\w+)'/", "default_app['front'] = '".$in['apps_name']."'", $apps_config);

			#add the controller
			$data_php = '<?php if(!defined(\'BASEPATH\')) exit(\'No direct script access allowed\');

#code here...

return $view->fetch();';
			$data_html = 'Hello world!';

			file_put_contents(APPSPATH.$in['apps_name'].'/admin/controller/'.$in['controller'].'.php', $data_php);
			file_put_contents(APPSPATH.$in['apps_name'].'/front/controller/'.$in['controller'].'.php', $data_php);
			
			file_put_contents(APPSPATH.$in['apps_name'].'/admin/view/'.$in['controller'].'.html', $data_html);
			file_put_contents(APPSPATH.$in['apps_name'].'/front/view/'.$in['controller'].'.html', $data_html);

			#and register it
			$apps_config = preg_replace("/default_controller\['admin'\](\s+)=(\s+)'(\w+)'/", "default_controller['admin'] = '".$in['controller']."'", $apps_config);
			$apps_config = preg_replace("/default_controller\['front'\](\s+)=(\s+)'(\w+)'/", "default_controller['front'] = '".$in['controller']."'", $apps_config);
			file_put_contents(INSTALLPATH.'config/apps.php',$apps_config);
		}
		
		#set up the index
		$index = file_get_contents(INSTALLPATH.'index.php');
		$index = str_replace("INSTALLPATH.'instal/startup.php')", "BASEPATH.'startup.php')", $index);
		file_put_contents(INSTALLPATH.'index.php',$index);

		#and copy the last file needed
		$template = APPSPATH.'instal/front/layout/template.html';
		copy($template, APPSPATH.$in['apps_name'].'/front/layout/template.html');
		
		msg::$success = "Application created";
		
		header('Location: index.php');
		return true;
	}
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function set_defaults_valid(&$in)
	{
		$is_ok = true;
		$v=new validation();
		$v->f('apps_name','Username','required');
		$v->f('controller','Username','required');
		if($v->run()){
			if(file_exists(APPSPATH.$in['apps_name'])){
				msg::set('apps_name','Application already exists');
				$is_ok = false;
			}
		}else{
			$is_ok = false;
		}
		
		return $is_ok;
	}
}
?>