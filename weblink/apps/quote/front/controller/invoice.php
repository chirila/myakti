<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.time());
$db2 = new sqldb();
$db3 = new sqldb();

$tblinvoice = $db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."'");
if(!$tblinvoice->move_next()){
	$view->assign(array('data'=>false));
	return $view->fetch();

}
if($tblinvoice->f('sent') == '0'){
	$view->assign(array('data'=>false));
	return $view->fetch();
}
$the_time=time();
//$db->insert("INSERT INTO logging SET pag='invoice', message='".addslashes($tblinvoice->f('buyer_name'))." opened the weblink', field_name='invoice_id', field_value='".$in['invoice_id']."', date='".$the_time."', type='0' ");
$currency_rate = $tblinvoice->f('currency_rate');
$discount = $tblinvoice->f('discount');

$vat = $tblinvoice->f('vat');

if($tblinvoice->f('type')=='2'){
	$page_title= gm('Credit Invoice');
}
else{
	$page_title= gm('Invoice Information');
}

$factur_date = date(ACCOUNT_DATE_FORMAT,  $tblinvoice->f('invoice_date'));
$currency_type = $tblinvoice->f('currency_type');
$currency = get_commission_type_list($tblinvoice->f('currency_type'));
$serial_number = $tblinvoice->f('serial_number');
$invoice_status = $tblinvoice->f('status');
$invoice_sent = $tblinvoice->f('sent');

switch ($tblinvoice->f('status')){
	case '0':
		if($tblinvoice->f('sent') == '0'){
			$status = 'draft';
		}
		else{
			if($tblinvoice->f('paid') == '2' ){
				$status = 'partial';
			}else {
				$status = 'final';
			}
			if($tblinvoice->f('due_date') < time()){
				$status = 'late';
			}
		}
		break;
	case '1':
		$tblinvoice->f('type')=='2'? $status = 'final':$status = 'fully';
		break;
	default:
		$status = 'draft';
		break;
}
$buyer_id=$tblinvoice->f('buyer_id');
$contact_id = $tblinvoice->f('contact_id');
$contact_title = '';
if($db->f('contact_id') && !$buyer_id)
{
	$contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$db->f('contact_id')."'");
}
if(!empty($contact_title)){
	$contact_title .= " ";
}
$in['type'] = $tblinvoice->f('type');
$remove_vat = $tblinvoice->f('remove_vat');
$free_field = '<p>'.$tblinvoice->f('buyer_address').'</p><p>'.$tblinvoice->f('buyer_zip').' '.$tblinvoice->f('buyer_city').'</p><p>'.get_country_name($tblinvoice->f('buyer_country_id')).'</p>';

$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'invoice'
								   AND item_name = 'notes'
								   And active = '1'
								   AND item_id = '".$in['invoice_id']."' ");

$view->assign(array(
	'bg' 							=> $status,
	'sent_dated'					=> $tblinvoice->f('sent')==1 ? date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('sent_date')):'',
	'hide_sentd'					=> $tblinvoice->f('sent_date') ? '':'hide',
	'hide_proforma' 				=> $tblinvoice->f('serial_number')=='' ? 'hide' : '',
	'hide_serial'   				=> $tblinvoice->f('serial_number')=='' ? '' : 'hide',
	'hide_sent'     				=> $tblinvoice->f('sent')==0?'':'hide',
	'factur_nr' 					=> $tblinvoice->f('serial_number') ? $tblinvoice->f('serial_number'): '',
	'factur_date' 					=> $factur_date,
	'due_date'						=> date(ACCOUNT_DATE_FORMAT,$db->f('due_date')),
	'invoice_buyer_name'       		=> $contact_title.$tblinvoice->f('buyer_name'),
	'invoice_buyer_country'    		=> get_country_name($tblinvoice->f('buyer_country_id')),
	'invoice_buyer_state'      		=> get_state_name($tblinvoice->f('buyer_state_id')),
	'invoice_buyer_city'       		=> $tblinvoice->f('buyer_city'),
	'invoice_buyer_zip'        		=> $tblinvoice->f('buyer_zip'),
	'invoice_buyer_address'    		=> nl2br($tblinvoice->f('buyer_address')),
	'invoice_buyer_vat'		   		=> $tblinvoice->f('seller_bwt_nr'),
	'invoice_seller_name'         	=> $tblinvoice->f('seller_name'),
	'invoice_seller_d_country'    	=> get_country_name($tblinvoice->f('seller_d_country_id')),
//	'invoice_seller_d_state'      		=> get_state_name($db->f('seller_d_state_id')),
	'invoice_seller_d_city'       	=> $tblinvoice->f('seller_d_city'),
	'invoice_seller_d_zip'        	=> $tblinvoice->f('seller_d_zip'),
	'invoice_seller_d_address'    	=> $tblinvoice->f('seller_d_address'),
	'invoice_seller_b_country'    	=> get_country_name($tblinvoice->f('seller_b_country_id')),
//	'invoice_seller_b_state'   		=> get_state_name($db->f('seller_b_state_id')),
	'invoice_seller_b_city'       	=> $tblinvoice->f('seller_b_city'),
	'invoice_seller_b_zip'        	=> $tblinvoice->f('seller_b_zip'),
	'invoice_seller_b_address'    	=> $tblinvoice->f('seller_b_address'),
	'invoice_vat_no'              	=> ACCOUNT_VAT_NUMBER,
	'notes'						  	=> nl2br($notes),
	'notes2'						=> nl2br(utf8_decode($tblinvoice->f('notes2'))),
//	'hide_sync'				  					=> $status == 'draft' ? '':'hide',
	'hide_vat'					 	=> $remove_vat ? 'hide' : '',
	'hide_notes'					=> $notes == '' ? 'hide' : '',
	'hide_notes2'					=> $tblinvoice->f('notes2') == '' ? 'hide' : '',
	'serial_number'					=> $tblinvoice->f('type')==1 ? generate_invoice_number(obj::get('db')) : $tblinvoice->f('serial_number'),
	'our_ref'						=> $tblinvoice->f('our_ref'),
	'your_ref'						=> $tblinvoice->f('your_ref'),
	'buyer_id'						=> $buyer_id,
	'free_field'					=> $tblinvoice->f('free_field') ? '<p style="line-height: 17px;">'.nl2br($tblinvoice->f('free_field')).'</p>' : $free_field,
	'invoice_label'					=> $tblinvoice->f('type')== 2 ? gm('Credit Note Nr:') : gm('Invoice Nr').':',
	'no_credit'						=> $tblinvoice->f('type')== 2 ? false : true,
	'red_if_credit'					=> $tblinvoice->f('type')== 2 ? 'red_text' : '',
	'buyer_email'					=> $tblinvoice->f('buyer_email'),
	'hide_disc_line'				=> ($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2) ? 'hide' : '',
));
if(!$buyer_id){
	if($tblinvoice->f('buyer_email')){
		$view->assign('contact_email',$tblinvoice->f('buyer_email'));
	}
}
if($tblinvoice->f('discount') == 0){
	$discount_procent=0;
} else {
	$discount_procent = $tblinvoice->f('discount');
	$view->assign('total_discount_procent',display_number($tblinvoice->f('discount')),'vat_line');
}

if($tblinvoice->f('apply_discount') < 2){
	$discount_procent = 0;
}

if($tblinvoice->f('req_payment') == 100){
	$view->assign('view_req_payment','hide');

} else {
	$view->assign('view_req_payment','');
}
$payments_nr=0;
//payments_info
$payments_info = $db->query("SELECT info,payment_id FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' ORDER BY payment_id desc");
while($payments_info->move_next()){
	$view->assign(array(
		'payment_info'	=> $payments_info->f('info'),
	),'payments_row');
	$view->loop('payments_row');
	$payments_nr++;
}
if($payments_nr){
	$view->assign('is_payment',true);
}else{
	$view->assign('is_payment',false);
}
$amount_due = 0;
//PAID
if($tblinvoice->f('paid') == 1){
	$subtotal_vat = 0;
	$discount_value = 0;
	$total_n = 0;
	$lines = $db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
	while ($lines->next()) {
		$line_discount = $lines->f('discount');
		if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
			$line_discount = 0;
		}
		$amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
		$amount_line_disc = $amount_line*$discount_procent/100;
		$discount_value += $amount_line_disc;
		$subtotal_vat += ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
		$total_n += $amount_line - $amount_line_disc;
	}

	if($discount_procent != 0){
		$view->assign('show_disc',$show_disc);
	}
	$total = $total_n+$subtotal_vat;

    $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
	if($tblinvoice->f('req_payment') != 100){
		$view->assign('sh_pay','hide');
	}

	//select last payment
	$date = $db->query("SELECT date FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' ORDER BY date DESC LIMIT 1");
	$date->move_next();

    if($date->f('date')){
	   $payment_date = date(ACCOUNT_DATE_FORMAT, strtotime($date->f('date')));
	}else{    //proforma fully paid
        $date = $db->query("SELECT date FROM tblinvoice_payments WHERE invoice_id='".$tblinvoice->f('proforma_id')."' ORDER BY date DESC LIMIT 1");
	    $date->move_next();
	    $payment_date = date(ACCOUNT_DATE_FORMAT, strtotime($date->f('date')));
	}

	$total_default = $total * return_value($currency_rate);
	$view->assign(array(
		'req_payment'				=> 100 - $tblinvoice->f('req_payment'),
		's_paid'					=> '',
		'e_paid'					=> '',
		's_notpaid'					=> '<!--',
		'e_notpaid'					=> '-->',
		'paid_observation'			=> $tblinvoice->f('type')==1?'Proforma Paid in full on'.' '.$payment_date:'Invoice Paid in full on'.' '.$payment_date,
		'currency'					=> $currency,
		'total_sum'			        => place_currency(display_number(0),$currency),
		'total_amount_due'			=> place_currency(display_number(0),$currency),
		'total_amount_due2'			=> place_currency(display_number($total),$currency),
		'total_amount_due2_default'	=> place_currency(display_number($total_default)),
		'total_amount_due1'			=> display_number(0),
		'total_payments'			=> place_currency(display_number(0),$currency),
		'sh_edit'					=> 'hide',
		'req_payment_value'         => place_currency(display_number($total-$req_payment_value),$currency),
		'is_not_credit'				=> $tblinvoice->f('type')=='2'? '':'1',
	));

}//NOT PAID / NOT FULLY PAID
else {
	$subtotal_vat = 0;
	$discount_value = 0;
	$total_n = 0;
	$show_disc = false;
	$lines = $db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
	while ($lines->next()) {
		$line_discount = $lines->f('discount');
		if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
			$line_discount = 0;
		}
		$amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
		$amount_line_disc = $amount_line*$discount_procent/100;
		$discount_value += $amount_line_disc;
		$subtotal_vat +=  ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
		$total_n += $amount_line - $amount_line_disc;
	}

	if($discount_procent != 0 || $show_disc === true){
		$view->assign('show_disc',$show_disc);
	}

	$total = $total_n+$subtotal_vat;

	//already payed
	$proforma_id=$db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
    $payments_filter=" invoice_id='".$in['invoice_id']."'";
    if($proforma_id){
      $payments_filter.=" OR invoice_id='".$proforma_id."'";
    }

	$already_payed = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter."  ");
	$already_payed->move_next();
	$total_payed = $already_payed->f('total_payed');

	$req_payment_value=$total* $tblinvoice->f('req_payment')/100;
	if($tblinvoice->f('req_payment') == 100){
		$amount_due = round($total - $total_payed,2);
	}else{
		$amount_due = round($req_payment_value - $total_payed ,2);
	}

	// SHOW/HIDE PAYMENT FORM
	if(isset($in['act']) && $in['act'] == 'invoice-invoice-invoice-pay'){
		$view->assign('sh_payment_receive','hide');
	}else {
		$view->assign('sh_payment_form','hide');
	}

	if($invoice_status == 1){
		$view->assign('sh_edit','hide');
	}
	else {
		if($invoice_sent == 1){
			$view->assign('sh_edit','hide');
		}
	}

	if(!isset($in['payment_date'])){
		$in['payment_date'] = date(ACCOUNT_DATE_FORMAT);
	}else {
		$in['payment_date'] = date(ACCOUNT_DATE_FORMAT,$in['payment_date']);
	}
	$total_default = $total*return_value($currency_rate);

	$view->assign('req_payment',$tblinvoice->f('req_payment'));

	$view->assign(array(
	's_paid'					=>	'<!--',
	'e_paid'					=>	'-->',
	's_notpaid'					=>	'',
	'e_notpaid'					=>	'',
	'currency'					=> $currency,
	'total_payments'			=> place_currency(display_number($amount_due),$currency),
	'payment_date'  			=> $in['payment_date'],
	'p_date'  					=> time(),
	'total_amount_due'			=> place_currency(display_number($amount_due),$currency),
	'total_amount_due1'			=> display_number($amount_due),
	'total_amount_due2'			=> place_currency(display_number($total),$currency),
	'total_amount_due2_default'	=> place_currency(display_number($total_default)),
	'vat'						=> $vat,
	'sh_pay'					=> $total_payed ? '' : 'hide',
	'is_not_credit'				=> $tblinvoice->f('type')=='2'? '':'1',
	'req_payment_value'         => place_currency(display_number($req_payment_value-$total_payed),$currency)
	));
	if($tblinvoice->f('req_payment') != 100){
		$view->assign('sh_pay','hide');
	}
}

//GET invoice rows
$get_invoice_rows = $db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' order by sort_order ASC ");

$i = 0;
$total_amount = 0;
$vat_percent = array();
$sub_t = array();
$sub_disc = array();
$vat_percent_val = 0;
while ($get_invoice_rows->move_next()){
	$line_d = $get_invoice_rows->f('discount');
	if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
		$line_d = 0;
	}
	$amount = $get_invoice_rows->f('amount') - $get_invoice_rows->f('amount') * $line_d /100;

	$view->assign(array(
		'row_nr'				=> $i+1,
		'row_description'		=> nl2br($get_invoice_rows->f('name')),
		'row_unitmeasure'		=> $get_invoice_rows->f('unitmeasure'),
		'row_quantity'			=> display_number($get_invoice_rows->f('quantity')),
		'row_unit_price'		=> display_number_var_dec($get_invoice_rows->f('price')),
		'row_amount'			=> place_currency(display_number($amount),$currency),
		'vat'					=> display_number($get_invoice_rows->f('vat')),
		'disc_line'				=> display_number($get_invoice_rows->f('discount')),
		'content'               => $get_invoice_rows->f('content'),
		'colspan'			    =>  $get_invoice_rows->f('content') ? 'colspan="7" class="first-of-type  last_nocenter"' : '',
	),'invoice_row');

	$total_amount += $get_invoice_rows->f('amount');
	$i++;
	$view->loop('invoice_row');

	$amount_d = $amount * $discount/100;
	if($tblinvoice->f('apply_discount') < 2){
		$amount_d = 0;
	}
	$vat_percent_val = ($amount - $amount_d) * $get_invoice_rows->f('vat')/100;

	$vat_percent[$get_invoice_rows->f('vat')] += $vat_percent_val;
	$sub_t[$get_invoice_rows->f('vat')] += $amount;
	$sub_disc[$get_invoice_rows->f('vat')] += $amount_d;
}

foreach ($vat_percent as $key => $val){
	 if($sub_t[$key] - $sub_disc[$key]){
		$view->assign(array(
			'vat_percent'   			=> display_number($key),
			'vat_value'	   				=> place_currency(display_number($val),$currency),
			'total_novat'	   	 		=> place_currency(display_number($sub_t[$key]),$currency),
			'discount_value'	   	 	=> place_currency(display_number($sub_disc[$key]),$currency),
			'sh_discount'				=> ($discount_procent != 0 || $show_disc === true) ? '' : "hide",
			'sh_vat'					=> $tblinvoice->f('remove_vat') == 1 ? 'hide' : '',
			'net_amount'				=> place_currency(display_number($sub_t[$key] - $sub_disc[$key]),$currency),
		),'vat_line');
		$view->loop('vat_line');
	}
}

$total_payments = 0;
$total_amount_due = $total_amount - $total_payments;

//sent mail
// if(!$in['include_pdf'])
// {
// 	$in['include_pdf'] = 1;
// }

$tblinvoice123 = $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,tblinvoice.discount, tblinvoice.invoice_date,tblinvoice.buyer_name,
									tblinvoice.currency_type,tblinvoice.pdf_logo,tblinvoice.pdf_layout,
                                  SUM(tblinvoice_line.amount) AS total,
                                  SUM(tblinvoice_payments.amount) AS total_payed
                           FROM tblinvoice
                           LEFT JOIN tblinvoice_line ON tblinvoice_line.invoice_id=tblinvoice.id
                           LEFT JOIN tblinvoice_payments ON tblinvoice_payments.invoice_id=tblinvoice.id
                           WHERE tblinvoice.id='".$in['invoice_id']."'");
$tblinvoice123->move_next();

// $message_data=get_sys_message('invmess',$tblinvoice->f('email_language'));
// $subject=$message_data['subject'];
// $subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
// $subject=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $subject);
// $subject=str_replace('[!INVOICE_DATE!]',$factur_date, $subject);
// $subject=str_replace('[!CUSTOMER!]',$tblinvoice123->f('buyer_name'), $subject);
// $subject=str_replace('[!SUBTOTAL!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $subject);
// $subject=str_replace('[!DISCOUNT!]',$tblinvoice123->f('discount').'%', $subject);
// $subject=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_value),$currency), $subject);
// $subject=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $subject);
// $subject=str_replace('[!PAYMENTS!]',place_currency(display_number($total_payed),$currency), $subject);
// $subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount_due),$currency), $subject);

// $body=$message_data['text'];
// $body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
// $body=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $body);
// $body=str_replace('[!INVOICE_DATE!]',$factur_date, $body);
// $body=str_replace('[!CUSTOMER!]',$tblinvoice123->f('buyer_name'), $body);
// $body=str_replace('[!SUBTOTAL!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $body);
// $body=str_replace('[!DISCOUNT!]',$tblinvoice123->f('discount').'%', $body);
// $body=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_value),$currency), $body);
// $body=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $body);
// $body=str_replace('[!PAYMENTS!]',place_currency(display_number($total_payed),$currency), $body);
// $body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount_due),$currency), $body);

// $view->assign(array(
// 	'include_pdf_checked'  => $in['include_pdf'] ? 'checked="checked"' : '',
// 	'copy_checked'         => $in['copy'] ? 'checked="checked"' : '',
// 	'subject'              => $in['subject'] ? $in['subject'] : $subject,
// 	'e_message'            => $in['e_message'] ? stripslashes($in['e_message']) : $body,
// 	'language_dd'	       => build_pdf_language_dd($tblinvoice->f('email_language'),1),
// ));
if($buyer_id){
	$invoice_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$buyer_id."' ");
	if($invoice_email){
		$view->assign(array(
			'inv_email'		=> true,
			'invoice_email'	=> $invoice_email,
		));
	}
	$view->assign(array(
		'isbuyer'	=> true,
	));
}
if($tblinvoice123->f('pdf_layout')){
	$link_end='&type='.$tblinvoice123->f('pdf_layout').'&logo='.$tblinvoice123->f('pdf_logo');
}else{
	$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
}
#if we are using a customer pdf template
if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $tblinvoice123->f('pdf_layout') == 0){
	$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
}

$l=0;
//print languages
$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active=1 ORDER BY sort_order ");
while($pim_lang->move_next()){
	if($pim_lang->f('default_lang')){
 		$default_lang = $pim_lang->f('lang_id');
 	}
  	$view->assign(array(
		'language'	        	=> gm($pim_lang->f('language')),
		'pdf_link_loop'         => 'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$pim_lang->f('lang_id').$link_end,
		'web_invoice_link'  	=> 'index_blank.php?do=invoice-invoice_web_print&invoice_id='.$in['invoice_id'].'&lid='.$pim_lang->f('lang_id').$link_end,
	),'languages_pdf');
	$l++;
	$view->loop('languages_pdf');
}
$db->query("select * from pim_custom_lang where active=1 ORDER BY sort_order ");
 while($db->move_next()){
 	$db2->query("SELECT * FROM label_language WHERE lang_code='".$db->f('lang_id')."'");
 	if($db2->move_next())
 	{
	 	$view->assign(array(
			'language'	        	=> $db2->f('name'),
			'pdf_link_loop'         => 'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$db2->f('label_language_id').$link_end,
			'web_invoice_link'  	=> 'index_blank.php?do=invoice-invoice_web_print&invoice_id='.$in['invoice_id'].'&lid='.$db2->f('label_language_id').$link_end,
		),'languages_pdf');
		$l++;
	}
	$view->loop('languages_pdf');
}
if($l==1){
	$view->assign(array(
	   'generate_pdf_id'  		=> '',
	   'web_invoice_id'   		=> '',
	   'hide_language'	  		=> 'hide',
	   'pdf_link'	         	=> 'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$default_lang.$link_end,
	));
}else{
	$view->assign(array(
	   'generate_pdf_id'  		=> 'generate_pdf',
	   'web_invoice_id'   		=> 'web_invoice',
	   'hide_language'	  		=> '',
	   'pdf_link'	      		=> '#',
	));
}

$lg = $default_lang;
if($tblinvoice->f('email_language')){
	$lg = $tblinvoice->f('email_language');
}
$view->assign(array( 'pdf_link'	=> 'index.php?q='.$in['q'].'&do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$lg.$link_end));
// //add_nr box
// if($in['do']=='invoice-invoice-invoice-add_nr' ){
// 	$view->assign("view_add_nr","");
// }else{
// 	$view->assign("view_add_nr","style='display:none;'");
// }

//sent box

// $view->assign("view_sent","style='display:none;'");

//send box
// $view->assign("view_send","style='display:none;'");
// if(!$in['s_date']){
// 	$in['s_date'] = time();
// }
$rel = $buyer_id ? 'customer_addresses.customer_id.'.$buyer_id : 'customer_contact_address.contact_id.'.$contact_id;

// $proforma_id=$db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
// if($proforma_id){
//     $total_payed_proforma = $db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$proforma_id."'");
// 	$proforma_date=$db->field("SELECT invoice_date FROM tblinvoice WHERE id='".$proforma_id."'");
// 	if($total_payed_proforma){
// 	 	$is_proforma_payments=true;
// 	}else{
// 	 	$is_proforma_payments=false;
// 	}
// }

$is_invoice_for_proforma=$db->field("SELECT proforma_id FROM tblinvoice WHERE proforma_id='".$in['invoice_id']."'");
if($is_invoice_for_proforma){
  $view->assign('no_regular_invoice',false);
  $view->assign('has_regular_invoice',true);
}else{
  $view->assign('no_regular_invoice',true);
  $view->assign('has_regular_invoice',false);
}

$view->assign(array(
	'invoice_id'    			=> $in['invoice_id'],
	// 'c_invoice_id'    			=> $tblinvoice->f('c_invoice_id'),
	// 's_date'					=> $in['s_date'],
	// 'sent_date'					=> date(ACCOUNT_DATE_FORMAT,$in['s_date']),
	'regular_invoice_serial_number'=> $db->field("SELECT serial_number FROM tblinvoice WHERE proforma_id='".$in['invoice_id']."'"),
	'regular_invoice_id'        => $db->field("SELECT id FROM tblinvoice WHERE proforma_id='".$in['invoice_id']."'"),
	// 'has_proforma'              => $has_proforma,
	// 'is_proforma_payments'      => $is_proforma_payments,
	// 'proforma_date'             => date(ACCOUNT_DATE_FORMAT,$proforma_date),
	// 'total_payed_proforma'      => place_currency(display_number($total_payed_proforma),$currency),
	'page_title'	    		=> $page_title,
	// 'pick_date_format'			=> pick_date_format(),
	// 'search'					=> $in['search']? '&search='.$in['search'] : '',
	// 'view'						=> $in['view']? '&view='.$in['view'] : '',
	// 'default_currency_name'		=> build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
	// 'hide_total_currency'		=> $currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide',
	// 'style'     				=> ACCOUNT_NUMBER_FORMAT,
	'data'						=> true,
	'show_payment_box'			=> defined('SHOW_INVOICE_BALANCE') && SHOW_INVOICE_BALANCE == 1 ? true : false,
));
require_once 'icepay/icepay_api_basic.php';
global $database_config;
/* Apply logging rules */

if($amount_due && (defined('ICEPAY_ACTIVE2') && ICEPAY_ACTIVE2 == 1) && (defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT == 1)){
	$logger = Icepay_Api_Logger::getInstance();
	$logger->enableLogging(false)
	        ->setLoggingLevel(Icepay_Api_Logger::LEVEL_ALL)
	        ->logToFile(true)
	        ->setLoggingDirectory(realpath("icepay/logs"))
	        ->setLoggingFile($database_config['mysql']['database'].'.txt')
	        ->logToScreen(false);
	$country_code = get_country_code($tblinvoice->f('buyer_country_id'));
	if(!$country_code){
		$country_code = 'BE';
	}
	$icepay_id = $db->field("SELECT id FROM icepay_orders WHERE item='".$in['invoice_id']."' AND type='1' ORDER BY `id` DESC LIMIT 1 ");
	if(!$icepay_id){
		$icepay_id = $db->insert("INSERT INTO icepay_orders SET item='".$in['invoice_id']."', type='1' ");
	}
	/* Set the paymentObject */
	$paymentObj = new Icepay_PaymentObject();
	$paymentObj->setAmount(number_format($amount_due, 2, '.', '') *100)
	            ->setCountry($country_code)
	            ->setLanguage("EN")
	            ->setReference($in['q'])
	            ->setDescription($tblinvoice->f('seller_name').' Invoice Id # '.$in['invoice_id'])
	            ->setCurrency(currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code'))
	            ->setOrderID($icepay_id);

    // Merchant Settings
    $basicmode = Icepay_Basicmode::getInstance();
    if($basicmode->setMerchantID(ICEPAY_MERCHANTID2)){
    	if($basicmode->setSecretCode(ICEPAY_SECRETCODE2)){
    		if($basicmode->validatePayment($paymentObj)){
				$view->assign('ice_pay_link',$basicmode->getURL());
				$view->assign('icepay_hide',true);
    		}
    	}
    }

}else{
	$view->assign('icepay_hide',false);
}
if(isset($in['Status']) && $in['Status'] == 'ERR'){
	$view->assign('error',true);
}
if(isset($in['Status']) && $in['Status'] == 'OPEN'){
	$view->assign('pending',true);
}
if(isset($in['Status']) && $in['Status'] == 'OK'){
	$view->assign('ok',true);
}

$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id='".$lg."'")->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
	if( ($j % 2) == 0){
		$j++;
		continue;
	}
	$labels[$key] = $value;
	$j++;
}
$view->assign(array(
	'bank'				  			=> ACCOUNT_BANK_NAME,
	'bic'				  			=> ACCOUNT_BIC_CODE,
	'iban'				  			=> ACCOUNT_IBAN,
	'bankd_txt'			  			=> $labels['bank_details'],
	'bank_txt'			  			=> $labels['bank_name'],
	'bic_txt'			  			=> $labels['bic_code'],
	'iban_txt'			  			=> $labels['iban'],

));

return $view->fetch();