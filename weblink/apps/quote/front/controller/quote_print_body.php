<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
//session_start();
$db = new sqldb();
$db2 = new sqldb();
$db3 = new sqldb();
$path = ark::$viewpath;
global $config,$database_config;
if($in['type']){
	$html='quote_print_'.$in['type'].'.html';
}elseif ($in['custom_type']) {
	$path = INSTALLPATH.'upload/'.obj::get('db').'/custom_pdf/';

	$html='custom_quote_print-'.$in['custom_type'].'.html';
}else{
	$html='quote_print_1.html';
}
// echo $path.$html; exit();
$view = new at($path.$html);
// echo "<pre>";
// 	print_r(realpath($path.$html));
// 	exit();
$source = '[Source]';
$type = '[Type]';
if(!$in['id']){ //sample
	//assign dummy data
	$quote_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));

	$pdf->setQuotePageLabel(getq_label_txt('page',$in['lid']));
	//for SIESQO
	if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
		$pdf->setQuotePageLabel('c656cd7e_7144_757f_92b34246c311');
	}
	//for salesasssist_2
	if(obj::get('db') == 'salesassist_2'){
		$pdf->setQuotePageLabel('c656cd7e_7144_757f_92b34246c311');
	}

	$view->assign(array(
//	'ACCOUNT_LOGO_Q'      => ACCOUNT_LOGO_QUOTE?$upload_path.ACCOUNT_LOGO_QUOTE:$upload_path.'img/no-logo.png',
	'ACCOUNT_LOGO_Q'      => '../../img/no-logo.png',
	'BILLING_ADDRESS_TXT' => getq_label_txt('billing_address',$in['lid']),
	'QUOTE_NOTE_TXT'      => getq_label_txt('quote_note',$in['lid']),
	'QUOTE_TXT'           => getq_label_txt('quote',$in['lid']),
	'REFERENCE_TXT'       => getq_label_txt('reference',$in['lid']),
	'DATE_TXT'            => getq_label_txt('date',$in['lid']),
	'CUSTOMER_TXT'        => getq_label_txt('customer',$in['lid']),
	'ITEM_TXT'            => getq_label_txt('item',$in['lid']),
    'ARTICLE_CODE_TXT'    => getq_label_txt('article_code',$in['lid']),
	'UNITMEASURE_TXT'     => getq_label_txt('unitmeasure',$in['lid']),
	'QUANTITY_TXT'        => getq_label_txt('quantity',$in['lid']),
	'UNIT_PRICE_TXT'      => getq_label_txt('unit_price',$in['lid']),
	'AMOUNT_TXT'          => getq_label_txt('amount',$in['lid']),
	'SUBTOTAL_TXT'        => getq_label_txt('subtotal',$in['lid']),
	'DISCOUNT_TXT'        => getq_label_txt('discount',$in['lid']),
	'VAT_TXT'             => getq_label_txt('vat',$in['lid']),
	'PAYMENTS_TXT'        => getq_label_txt('payments',$in['lid']),
	'AMOUNT_DUE_TXT'      => getq_label_txt('amount_due',$in['lid']),
	'NOTES_TXT'           => getq_label_txt('notes',$in['lid']),
	'SALE_UNIT_TXT'       => getq_label_txt('sale_unit',$in['lid']),
	'PACKAGE_TXT'         => getq_label_txt('package',$in['lid']),
	'YOUR_REFERENCE_TXT'  => getq_label_txt('your_ref',$in['lid']),
	'AUTHOR_TXT'		  => getq_label_txt('author',$in['lid']),
	'GRAND_TOTAL_TXT'     => getq_label_txt('grand_total',$in['lid']),
	'PHONE_TXT'			  => get_label_txt('phone',$in['lid']),
	'FAX_TXT'			  => get_label_txt('fax',$in['lid']),
	'URL_TXT'			  => get_label_txt('url',$in['lid']),
	'EMAIL_TXT'			  => get_label_txt('email',$in['lid']),
	'SOURCE_TXT'		  => getq_label_txt('source',$in['lid']),
	'TYPE_TXT'			  => getq_label_txt('type',$in['lid']),
	'GENERAL_CONDITIONS_TXT' => getq_label_txt('general_conditions',$in['lid']),

	'SELLER_NAME'         => '[Account Name]',
	'SELLER_D_COUNTRY'    => '[Country]',
	'SELLER_D_STATE'      => '[State]',
	'SELLER_D_CITY'       => '[City]',
	'SELLER_D_ZIP'        => '[Zip]',
	'SELLER_D_ADDRESS'    => '[Address]',
	'SELLER_B_COUNTRY'    => '[Billing Country]',
	'SELLER_B_STATE'      => '[Billing State]',
	'SELLER_B_CITY'       => '[Billing City]',
	'SELLER_B_ZIP'        => '[Billing Zip]',
	'SELLER_B_ADDRESS'    => '[Billing Address]',
	'SERIAL_NUMBER'       => '####',
	'BUYER_REFERENCE'     => '####',
	'QUOTE_DATE'          => $quote_date,
	'QUOTE_VAT'           => '##',
	'BUYER_NAME'       	  => '[Customer Name]',
	'QUOTE_CONTACT_NAME'  => '[Contact Name]',
	'BUYER_COUNTRY'    	  => '[Customer Country]',
	'BUYER_STATE'      	  => '[Customer State]',
	'BUYER_CITY'       	  => '[Customer City]',
	'BUYER_ZIP'           => '[Customer ZIP]',
	'BUYER_ADDRESS'    	  => '[Customer Address]',
    'NOTES'            	  => '[NOTES]',
    'SOURCE'			  => getq_label_txt('source',$in['lid']).': [Source]',
	'TYPE'			 	  => getq_label_txt('type',$in['lid']).': [Type]',
	'AUTHOR'			  => '[Author]',
	'OWN_REFERENCE'		  => '[Your Reference]' ,
	'HIDE_DEFAULT_TOTAL'  => 'hide',
	'SOURCE'			  => $source ? getq_label_txt('source',$in['lid']).": ".$source : '',
	'TYPE'			 	  => $type ? getq_label_txt('type',$in['lid']).': '.$type : '',
	'SOURCE4'			  => $source? $source : '',
	'TYPE4'			 	  => $type ? $type : '',
	'HIDE_DELIVERY'		  => 'hide',
	'DELIVERY_ADDRESS'	  => '[Delivery Address]',

	'SELLER_B_FAX'        => '[Fax]',
	'SELLER_B_EMAIL'      => '[Email]',
	'SELLER_B_PHONE'      => '[Phone]',
	'SELLER_B_URL'        => '[URL]',
	));
	$item_width=60;
	if($in['type']==4){
		$item_width = 50;
	}
	$quote_data = Array(
		"3326" => Array(
			"title" => "Title",
			"table" => Array(
	            "9wv8" => Array(
	                "description" => Array(
	                        "0" => "Article 1",
	                        "1" => "Article 2"
	                    ),
	                "sale_unit" => Array(
	                        "0" => 1,
	                        "1" => 1
	                    ),
	                "article_code" => Array(
	                        "0" => 1234,
	                        "1" => 4321
	                    ),
	                "line_discount" => Array(
	                        "0" => 0,
	                        "1" => 0
	                    ),
	                "package" => Array(
	                        "0" => 1,
	                       	"1" => 1
	                    ),
	                "quantity" => Array(
	                        "0" => 5,
	                        "1" => 6
	                    ),
	                "price" => Array(
	                        "0" => 20,
	                        "1" => 320
	                    )
	            )
	        )
		)
	);
	$quote_order =Array(
	    "3326" => Array(
	        "9wv8" => 1
	    )
	);
	$grand_total = 2020;
	$currency_type = 1;
	$version_code="A";
	$discount_procent = 10;
	$serial_number = '2011-014';
	foreach ($quote_order as $group_id => $group_items) {
		foreach($group_items as $item => $sort_order){
			//check to see if it's a table or content
			$content_type = 0;//don't know what it is yet
			$table = array(); $content = '';
			if(isset($quote_data[$group_id]['table']) && isset($quote_data[$group_id]['table'][$item])){
				$content_type = 1;//it's a table; congrats
				$table = $quote_data[$group_id]['table'][$item];
			}

			if(isset($quote_data[$group_id]['content']) && isset($quote_data[$group_id]['content'][$item])){
				$content_type = 2;//it's content
				$content = $quote_data[$group_id]['content'][$item];
			}
			if(isset($quote_data[$group_id]['pagebreak']) && isset($quote_data[$group_id]['pagebreak'][$item])){
				$content_type = 3;//page break; yeye
				$content = $quote_data[$group_id]['pagebreak'][$item];
			}
			if($content_type == 0){
				//could not find out what it is so just ignore it;
				continue;
			}
			if(!empty($table) && $content_type == 1){
				$i = 0;
				$tableTotal = 0;
				foreach($table['description'] as $index => $val){
					// $view->assign(array('QUOTE_GROUP_ID'=>$group_id));
					if( $table['article_code'][$index]){
                        $view->assign(array(
                        	'S_VIEW_ARTICLE'      => '',
                            'E_VIEW_ARTICLE'      => ''
                        ),'quote_table');

                        $is_article_code=true;
					}else{
                        $view->assign(array(
                        	'S_VIEW_ARTICLE'      => '<!---',
                            'E_VIEW_ARTICLE'      => '-->'
                        ),'quote_table');

                        $is_article_code=false;
     				}
                    if( $use_discount){
                        $view->assign(array(
                            'S_VIEW_DISCOUNT'      => '',
                            'E_VIEW_DISCOUNT'      => ''
                        ),'quote_table');
                        $is_discount=true;
					}else{
                        $view->assign(array(
                            'S_VIEW_DISCOUNT'      => '<!---',
                            'E_VIEW_DISCOUNT'      => '-->'
                        ),'quote_table');
                       	$is_discount=false;
     				}
                    $line_total=$table['quantity'][$index] * $table['price'][$index] * ($table['package'][$index]/ $table['sale_unit'][$index]);
                    $view->assign(array(
						'TR_ID'         			=> $item,
						'DESCRIPTION'  				=> nl2br($val),
            'SALE_UNIT'                 => $table['sale_unit'][$index],
						'ARTICLE_CODE'      		=> $table['article_code'][$index],
            'PACKAGE'      	            => remove_zero_decimals($table['package'][$index]),
            'DISCOUNT'      			=> display_number($table['line_discount'][$index]),
						'QUANTITY'      			=> display_number($table['quantity'][$index]),
						'PRICE'         			=> display_number_var_dec($table['price'][$index]),
						'LINE_TOTAL'                => place_currency(display_number(($line_total - ($line_total* $table['line_discount'][$index]/100))) ,get_commission_type_list($currency_type)),
					),'quote_line');
				    $tableTotal += $line_total - ($line_total* $table['line_discount'][$index]/100);
					$view->loop('quote_line','quote_table');
					$i++;
				}
				if($i > 0){
					$view->assign(array(
						'ITEM_TXT'            => getq_label_txt('item',$in['lid']),
            'ARTICLE_CODE_TXT'    => getq_label_txt('article_code',$in['lid']),
            'UNITMEASURE_TXT'     => getq_label_txt('unitmeasure',$in['lid']),
            'QUANTITY_TXT'        => getq_label_txt('quantity',$in['lid']),
            'UNIT_PRICE_TXT'      => getq_label_txt('unit_price',$in['lid']),
            'DISCOUNT_TXT'        => getq_label_txt('discount',$in['lid']),
            'AMOUNT_TXT'          => getq_label_txt('amount',$in['lid']),
            'AMOUNT_DUE_TXT'      => getq_label_txt('amount_due',$in['lid']),
						'TOTAL' 			  => place_currency(display_number($tableTotal),get_commission_type_list($currency_type)),
						'HIDE_CURRENCY2' 	  => ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
						'HIDE_CURRENCY1' 	  => ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
						'GROUP_TABLE_ID' 	  => $item,
						'SALE_UNIT_TXT'       => getq_label_txt('sale_unit',$in['lid']),
						'PACKAGE_TXT'         => getq_label_txt('package',$in['lid']),
					),'quote_table');
					$view->assign(array('ITEM_WIDTH'=>$item_width-23),'quote_table');
                    $view->loop('quote_table','quote_group');
				}
			}
			if(!empty($content) && $content_type == 2){
				$view->assign(array('QUOTE_CONTENT'=>$content),'quote_content');
				$view->loop('quote_content','quote_group');
			}
			if($content_type == 3){
				$view->assign(array('PAGE_BREAK'=>$content),'quote_pagebreak');
				$view->loop('quote_pagebreak','quote_group');
			}
		}
		$view->assign(array(
			'QUOTE_GROUP_HAS_TITLE' => empty($quote_data[$group_id]['title']) ? 'hide' : '',
			'QUOTE_GROUP_TITLE'		=> $quote_data[$group_id]['title'],
		),'quote_group');
		$view->loop('quote_group');
	}

  	$height = 262;
	if($discount_procent){
		$height -= 5;
		$discount_value = ($grand_total * $discount_procent) / 100;
		$grand_total -= $discount_value;
		$view->assign(array('TOTAL_DISCOUNT_PROCENT'=>display_number($discount_procent)));
		$view->assign(array('DISCOUNT_VALUE'=>$discount_value ? display_number($discount_value) : display_number(0)));
		$view->assign(array('VIEW_DISCOUNT'	=>''));
	}else{
		$view->assign(array('VIEW_DISCOUNT'	=>'hide'));
	}

	if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
		$total_default = $grand_total*return_value($currency_rate);
	}

	$view->assign(array('GRAND_TOTAL' 		=> place_currency(display_number($grand_total),get_commission_type_list($currency_type))));
	$view->assign(array('GRAND_TOTAL_DEF' 	=> place_currency(display_number($total_default),get_commission_type_list(ACCOUNT_CURRENCY_TYPE))));
}else{

	$filter = '';
	if(isset($in['version_id']) && is_numeric($in['version_id'])){
		$filter = 'AND tblquote_version.version_id = '.$in['version_id'];
	}else{
		$filter = 'AND tblquote_version.active = 1';
	}

	$db->query("SELECT tblquote.*,tblquote_version.active,tblquote_version.version_code,tblquote_version.version_id,tblquote_version.active AS version_active
    	        FROM tblquote
    	        INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
    	        WHERE tblquote.id='".$in['id']."' AND 1=1 ".$filter);
	$db->move_next();
	$show_total=$db->f('show_grand');
	$show_vat = $db->f('show_vat');
	$source = $db2->field("SELECT name FROM tblquote_source WHERE id='".$db->f('source_id')."' ");
	$type = $db2->field("SELECT name FROM tblquote_type WHERE id='".$db->f('t_id')."' ");
	$author = get_user_name($db->f('author_id'));
	$currency_type = $db->f('currency_type');
	$currency_rate = $db->f('currency_rate');
	$item_width=60;
	if($in['type']==4 || $in['type']==5)
	{
		$item_width = 54;
	}
	#if we are using a customer pdf template
	$pdf_layout = $db->f('pdf_layout');
	$use_custom_template = $db->f('use_custom_template');

	if($in['custom_type']){
		if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
			//for DAS MEDIA
			$item_width=90;
			//for SIESQO
			if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
				$item_width=52;
			}
			//for salesassist_2
			if(obj::get('db') == 'salesassist_2'){
				$item_width=52;
			}
		}
	}
	$initial_item_width = $item_width;
	if($db->f('use_package') ){
		$use_package=true;
	}else{
		$use_package=false;
	}

	if($db->f('use_sale_unit')){
		$use_sale_unit=true;
	}else{
		$use_sale_unit=false;
	}
	$use_discount = $db->f('apply_discount');
	/*if($db->f('apply_discount')){
		$use_discount=true;
		$view->assign('USE_DISCOUNT','');
	}else{
		$use_discount=false;
		$view->assign('USE_DISCOUNT','class="hide"');
	}*/

	$version_code=$db->f('version_code');
	$quote_id = $db->f('id');
	$version_id = $db->f('version_id');
	$discount_procent = $db->f('discount') ? $db->f('discount') : 0;
	if($use_discount < 2){
		$discount_procent = 0;
	}
	$serial_number = $db->f('serial_number');
	$buyer_reference = $db->f('buyer_reference');

//$currency = get_commission_type_list($db->f('currency_type'));
	$vat=$db->f('vat');
	$quote_date = date(ACCOUNT_DATE_FORMAT,  $db->f('quote_date'));

	$img = '../pim/img/no-logo.png';
	$attr = '';
	if($in['logo'])	{
		$print_logo = $in['logo'];
	}else {
		$print_logo=ACCOUNT_LOGO_QUOTE;
	}
	if($print_logo){
		$img = '../'.$print_logo;
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}
	if($db->f('contact_id'))
	{
		$contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
									INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
									WHERE customer_contacts.contact_id='".$db->f('contact_id')."'");
		if(!$db->f('buyer_id'))
		{
			$view->assign(array('HIDE_CUSTOMER'=>'hide'));
		}else
		{
			$view->assign(array('HIDE_CUSTOMER'=>''));
		}
	}
	if($contact_title)
	{
		$contact_title .= " ";
	}
	$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'quote'
								   AND item_name = 'notes'
								   AND version_id = '".$version_id."'
								   AND active = '1'
								   AND item_id = '".$quote_id."' ");

	$free_text_content = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'quote'
								   AND 	item_name 	= 'free_text_content'
								   AND 	version_id 	= '".$version_id."'
								   AND 	item_id 	= '".$quote_id."' ");

	$free_field = $db->f('buyer_address').'<br/>'.$db->f('buyer_zip').' '.$db->f('seller_d_city').'<br/>'.get_country_name($db->f('buyer_country_id')).'<br/>';
	$pdf->setQuotePageLabel(getq_label_txt('page',$in['lid']));
	//for SIESQO
	if((obj::get('db') == 'c656cd7e_7144_757f_92b34246c311') && ((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1)){
		$pdf->setQuotePageLabel('c656cd7e_7144_757f_92b34246c311');
	}
	//for salesasssist_2
	if((obj::get('db') == 'salesassist_2') && ((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1)){
		$pdf->setQuotePageLabel('c656cd7e_7144_757f_92b34246c311');
	}
	$view->assign(array(
		'ACCOUNT_LOGO_Q'      => $img,
		'ATTR'				  => $attr,
		'BILLING_ADDRESS_TXT' => getq_label_txt('billing_address',$in['lid']),
		'QUOTE_NOTE_TXT'      => getq_label_txt('quote_note',$in['lid']),
		'QUOTE_TXT'           => getq_label_txt('quote',$in['lid']),
		'DATE_TXT'            => getq_label_txt('date',$in['lid']),
		'REFERENCE_TXT'       => getq_label_txt('reference',$in['lid']),
		'YOUR_REFERENCE_TXT'  => getq_label_txt('your_ref',$in['lid']),
		'CUSTOMER_TXT'        => getq_label_txt('customer',$in['lid']),
		'DISCOUNT_TXT'        => getq_label_txt('discount',$in['lid']),
		'GRAND_TOTAL_TXT'     => getq_label_txt('grand_total',$in['lid']),
		'NOTES_TXT'           => $notes ? getq_label_txt('notes',$in['lid']) : '' ,
		'SALE_UNIT_TXT'       => getq_label_txt('sale_unit',$in['lid']),
		'PACKAGE_TXT'         => getq_label_txt('package',$in['lid']),
		'AUTHOR_TXT'				  => getq_label_txt('author',$in['lid']),
		'SOURCE_TXT'				  => getq_label_txt('source',$in['lid']),
		'TYPE_TXT'					  => getq_label_txt('type',$in['lid']),
		'PHONE_TXT'					  => getq_label_txt('phone',$in['lid']),
		'FAX_TXT'			  			=> getq_label_txt('fax',$in['lid']),
		'URL_TXT'			  			=> getq_label_txt('url',$in['lid']),
		'EMAIL_TXT'					  => getq_label_txt('email',$in['lid']),
		'subtotal_txt'			  => getq_label_txt('subtotal',$in['lid']),
		'GENERAL_CONDITIONS_TXT' => getq_label_txt('general_conditions',$in['lid']),

		'SELLER_NAME'         => $db->f('seller_name'),
		'SELLER_D_COUNTRY'    => get_country_name($db->f('seller_d_country_id')),
		'SELLER_D_STATE'      => get_state_name($db->f('seller_d_state_id')),
		'SELLER_D_CITY'       => $db->f('seller_d_city'),
		'SELLER_D_ZIP'        => $db->f('seller_d_zip'),
		'SELLER_D_ADDRESS'    => $db->f('seller_d_address'),
		'SELLER_B_COUNTRY'    => get_country_name($db->f('seller_b_country_id')),
		'SELLER_B_STATE'      => get_state_name($db->f('seller_b_state_id')),
		'SELLER_B_CITY'       => $db->f('seller_b_city'),
		'SELLER_B_ZIP'        => $db->f('seller_b_zip'),
		'SELLER_B_ADDRESS'    => $db->f('seller_b_address'),
		'SERIAL_NUMBER'       => $db->f('serial_number').' ['.$version_code.']',
		'QUOTE_DATE'          => $quote_date,
		'QUOTE_VAT'           => $db->f('vat'),
		'BUYER_NAME'          => $db->f('buyer_name'),
		'BUYER_REFERENCE'     => $db->f('buyer_reference') ? $db->f('buyer_reference') : '' ,
		'OWN_REFERENCE'			  => $db->f('own_reference') ? $db->f('own_reference') : '' ,
		'HIDE_YOUR_REF'			  => $db->f('own_reference') ? '' : 'hide' ,
		'BUYER_COUNTRY'       => get_country_name($db->f('buyer_country_id')),
		'BUYER_STATE'         => get_state_name($db->f('buyer_state_id')),
		'BUYER_CITY'          => $db->f('buyer_city'),
		'BUYER_ZIP'           => $db->f('buyer_zip'),
		'DELIVERY_ADDRESS'	  => $db->f('delivery_address')? nl2br($db->f('delivery_address')):'',
		'HIDE_DELIVERY'		 	  => $db->f('delivery_address')? '':'hide',
		'BUYER_ADDRESS'       => $db->f('buyer_address'),
		'NOTES'               => $notes ? nl2br($notes) : '' ,
		'VIEW_NOTES'          => $notes ? '' : 'hide' ,
		'DEF_CURRENCY'			  => build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
		'HIDE_DEFAULT_TOTAL'  => 'hide',//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide',
		'SOURCE'						  => $source ? getq_label_txt('source',$in['lid']).": ".$source : '',
		'SOURCE4'						  => $source? $source : '',
		'HIDE_SOURCE'		 			=> $source ? '' : 'hide',
		'TYPE'			 				  => $type ? getq_label_txt('type',$in['lid']).': '.$type : '',
		'TYPE4'			 				  => $type ? $type : '',
		'HIDE_TYPE'					  => $type ? '': 'hide',
		'AUTHOR'						  => trim($author) ? $author : '' ,
		'HIDE_AUTHOR'				  => trim($author) ? '' : 'hide' ,

		'SELLER_B_FAX'        => ACCOUNT_FAX,
		'SELLER_B_EMAIL'      => ACCOUNT_EMAIL,
		'SELLER_B_PHONE'      => ACCOUNT_PHONE,
		'SELLER_B_URL'        => ACCOUNT_URL,

		'HIDE_F'			  => defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? 'hide' : '',
		'HIDE_E'			  => defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? 'hide' : '',
		'HIDE_P'			  => defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? 'hide' : '',
		'HIDE_U'			  => defined('ACCOUNT_URL') && !ACCOUNT_URL ? 'hide' : '',
		'free_field'		  => $db->f('free_field') ? nl2br($db->f('free_field')) : $free_field,
		'sh_vat'			  => $show_vat ? '' : 'hide',
		'SHOW_TOTAL'		  => $show_total,
		'show_table'		  =>($show_total||$show_vat||$discount_procent)? '1':'',
		'is_free_text_content'=> $free_text_content ? true : false,
		// 'free_text_content'	  => preg_replace('/<br/', '<div', $free_text_content),
		'free_text_content'	  => $free_text_content,
		'free_text_br'	 	  => $db->f('general_conditions_new_page') == 1 ? '<br pagebreak="true"/>' : '',
		'account_vat_number'  => ACCOUNT_VAT_NUMBER,
		'VALID_UNTILL_TXT'    => getq_label_txt('valid_until',$in['lid']),
		'validity_date'		  => date(ACCOUNT_DATE_FORMAT,  $db->f('validity_date')),
		'is_validity_date'    => $db->f('validity_date') ? true : false,
	));

	$contact_name = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$db->f('contact_id')."'");
	$contact_name->next();

	$view->assign(array(
		'QUOTE_CONTACT_NAME'	=>	$contact_title.$contact_name->f('firstname').' '.$contact_name->f('lastname'),
		'QUOTE_CONTACT_EMAIL'	=>	$contact_name->f('email'),
		'QUOTE_CONTACT_TEL'		=>	$contact_name->f('phone'),
		'QUOTE_CONTACT_CELL'	=>	$contact_name->f('cell'),
	));
	$quote_data = array();
	$quote_order = array();
	$vat_val = array();

	$groups = $db->query("SELECT * FROM tblquote_group WHERE quote_id = '".$quote_id."' AND version_id = '".$version_id."' ORDER BY sort_order ASC");
	$grand_total = 0;
	while ($groups->next()) {
		$group_code = unique_id();
		$db->query("SELECT * FROM tblquote_line WHERE quote_id='".$quote_id."' AND group_id = '".$groups->f('group_id')."' ORDER BY sort_order,line_order ASC");

	 	while($db->move_next()){
	 		if(!isset($quote_data[$group_code])){
	 			$quote_data[$group_code] = array('title' => $groups->f('title'),'show_chapter_total'=>$groups->f('show_chapter_total'));
	 		}
	 		if($db->f('content_type') == 1 ){
		 		if(!isset($quote_data[$group_code]['table'])){
		 			$quote_data[$group_code]['table'] = array(
		 				$db->f('table_id') => array(
		 					'description' => array($db->f('name')),
		 					'sale_unit' => array($db->f('sale_unit')),
              'article_code' => array($db->f('article_code')),
              'line_discount' => array($db->f('line_discount')),
              'package' => array($db->f('package')),
              'article_id' => array($db->f('article_id')),
		 					'quantity' => array($db->f('quantity')),
		 					'vat_val' => array($db->f('vat')),
							'price' => array($db->f('price')),
							'show_block_total' => array($db->f('show_block_total')),
							'hide_QC' => array($db->f('hide_QC')),
							'hide_AC' => array($db->f('hide_AC')),
							'show_block_vat' => array($db->f('show_block_vat')),
							'service_bloc_title' => $db->f('service_bloc_title')
		 				)
		 			);
		 		}else{
		 			//if it's set we check for the table if it's set
		 			if(!isset($quote_data[$group_code]['table'][$db->f('table_id')])){
		 				$quote_data[$group_code]['table'][$db->f('table_id')] = array(
		 					'description' => array($db->f('name')),
		 					'sale_unit' => array($db->f('sale_unit')),
              'article_code' => array($db->f('article_code')),
              'line_discount' => array($db->f('line_discount')),
              'package' => array($db->f('package')),
              'article_id' => array($db->f('article_id')),
		 					'quantity' => array($db->f('quantity')),
		 					'vat_val' => array($db->f('vat')),
							'price' => array($db->f('price')),
							'show_block_total' => array($db->f('show_block_total')),
							'hide_QC' => array($db->f('hide_QC')),
							'hide_AC' => array($db->f('hide_AC')),
							'show_block_vat' => array($db->f('show_block_vat')),
							'service_bloc_title' => $db->f('service_bloc_title')
		 				);
		 			}else{
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['description'],$db->f('name'));
            array_push($quote_data[$group_code]['table'][$db->f('table_id')]['article_code'],$db->f('article_code'));
            array_push($quote_data[$group_code]['table'][$db->f('table_id')]['sale_unit'],$db->f('sale_unit'));
            array_push($quote_data[$group_code]['table'][$db->f('table_id')]['line_discount'],$db->f('line_discount'));
            array_push($quote_data[$group_code]['table'][$db->f('table_id')]['package'],$db->f('package'));
            array_push($quote_data[$group_code]['table'][$db->f('table_id')]['article_id'],$db->f('article_id'));
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['quantity'],$db->f('quantity'));
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['price'],$db->f('price'));
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['vat_val'],$db->f('vat'));
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['show_block_total'],$db->f('show_block_total'));
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['hide_AC'],$db->f('hide_AC'));
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['hide_QC'],$db->f('hide_QC'));
		 				array_push($quote_data[$group_code]['table'][$db->f('table_id')]['show_block_vat'],$db->f('show_block_vat'));
		 			}
		 		}
		 		// $grand_total += $db->f('amount');
		 		$line_discount = $db->f('line_discount');
				if($use_discount == 0 || $use_discount == 2){
					$line_discount = 0;
				}
				$price = ($db->f('price')-($db->f('price')*$line_discount/100));
				if($use_discount > 1){
					$price = $price - $price*$discount_procent/100;
				}
				if($db->f('show_block_total') == 0){
			 		$grand_total += ($db->f('price')-($db->f('price')*$db->f('line_discount')/100))*$db->f('quantity')*$db->f('package')/$db->f('sale_unit');
					if($show_vat){
						$vat_val[$db->f('vat')] += $price*($db->f('quantity')*$db->f('package')/$db->f('sale_unit'))*$db->f('vat')/100;
					}
				}
	 		}

	 		if($db->f('content_type') == 2){
	 			if(!isset($quote_data[$group_code]['content'])){
					$quote_data[$group_code]['content'] = array($db->f('table_id') => nl2br($db->f('content')));
	 			}else{
					$quote_data[$group_code]['content'][$db->f('table_id')] = nl2br($db->f('content'));
	 			}
			}
			if($db->f('content_type') == 3){
	 			if(!isset($quote_data[$group_code]['pagebreak'])){
					$quote_data[$group_code]['pagebreak'] = array($db->f('table_id') => $db->f('content'));

	 			}else{
					$quote_data[$group_code]['pagebreak'][$db->f('table_id')] = $db->f('content');
	 			}
			}
			$quote_order[$group_code][$db->f('table_id')] = $db->f('sort_order');
 		}
	}
	// echo "<pre>";
	// print_r($quote_order);
	// echo "<pre>";
	// print_r($quote_data);
	foreach ($quote_order as $group_id => $group_items) {
		$chapter_total = 0;
		foreach($group_items as $item => $sort_order){
			$item_width_ac = false;
			$item_width_qc = false;
			$item_width_p = false;
			$item_width = $initial_item_width;
			//check to see if it's a table or content
			$content_type = 0;//don't know what it is yet
			$table = array(); $content = '';
			if(isset($quote_data[$group_id]['table']) && isset($quote_data[$group_id]['table'][$item])){
				$content_type = 1;//it's a table; congrats
				$table = $quote_data[$group_id]['table'][$item];
			}
			if(isset($quote_data[$group_id]['content']) && isset($quote_data[$group_id]['content'][$item])){
				$content_type = 2;//it's content
				$content = $quote_data[$group_id]['content'][$item];
			}
			if(isset($quote_data[$group_id]['pagebreak']) && isset($quote_data[$group_id]['pagebreak'][$item])){
				$content_type = 3;//page break; yeye
				$content = $quote_data[$group_id]['pagebreak'][$item];
			}
			if($content_type == 0){
				//could not find out what it is so just ignore it;
				continue;
			}
			if(!empty($table) && $content_type == 1){
				$i = 0;
				$tableTotal = 0;
				$vat_line =0;
				foreach($table['description'] as $index => $val){
					$image = '';
					$show_block_total = true;
					if($table['show_block_total'][$index] == 1){
						$show_block_total = false;
					}
					$show_block_vat = false;
					if($table['show_block_vat'][$index] == 1){
						$show_block_vat = true;
					}
					$hide_QC=false;
					$hide_AC=false;
					if($table['hide_AC'][$index] == '1'){
						$hide_AC = true;
						if($item_width_ac === false){
							if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									//for DAS MEDIA
									// $item_width+=14;
									$e = 11;
																		//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										// $item_width+=11;
										$e=11;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										// $item_width+=11;
										$e=11;
									}
									$item_width += $e;
								}
							}else{
								$item_width +=14;
							}
							$item_width_ac = true;
						}
					}
					if($table['hide_QC'][$index] == '1'){
						$hide_QC = true;
						if($item_width_qc === false){
							if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									//for DAS MEDIA
									$e = 8;
									// $item_width+=13;
																		//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										// $item_width+=8;
										$e=8;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										// $item_width+=8;
										$e=8;
									}
									$item_width += $e;
								}
							}else{
								$item_width += 13;
							}
							$item_width_qc = true;
						}
					}
					// $view->assign('QUOTE_GROUP_ID',$group_id);
					if( $table['article_code'][$index]){
					  $view->assign(array(
              'S_VIEW_ARTICLE'      => '',
              'E_VIEW_ARTICLE'      => ''
            ),'quote_table');
            $is_article_code=true;
      //       $image_q = $db->query("SELECT pim_article_photo.file_name,pim_article_photo.name FROM pim_article_photo
      //       											INNER JOIN pim_articles ON pim_article_photo.parent_id=pim_articles.article_id
      //        WHERE pim_article_photo.parent_id='".$table['article_id'][$index]."' AND pim_articles.show_img_q='1' ORDER BY pim_article_photo.photo_id ASC LIMIT 1 ");
						// if($image_q->next()){
						// 	$image ='<br/><img src="'.$config['real_path'].'phpthumb/phpThumb.php?src=../upload/'.$database_config['mysql']['database'].'/pim_article_photo/'.$image_q->f('file_name').'&w=50&h=50'.'"  />';
						// }
					}else{
            $view->assign(array(
              'S_VIEW_ARTICLE'      => '<!---',
              'E_VIEW_ARTICLE'      => '-->'
            ),'quote_table');
            $is_article_code=false;
           	if(($in['type'] == 4 || $in['type'] == 5) && $item_width_p === false) {
           		$item_width += 13;
           		$item_width_p=true;
           	}
           	if($in['custom_type'] && $item_width_p === false){
							if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
								//for DAS MEDIA
								$e = 8;
								// $item_width+=13;
																	//for SIESQO
								if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
									// $item_width+=8;
									$e=8;
								}
								//for salesassist_2
								if(obj::get('db') == 'salesassist_2'){
									// $item_width+=8;
									$e=8;
								}
								$item_width += $e;
							}
							$item_width_p=true;
						}
          }

          if( $use_discount == 1 || $use_discount == 3 ){
            $view->assign(array(
              'S_VIEW_DISCOUNT'      => '',
              'E_VIEW_DISCOUNT'      => ''
           	),'quote_table');
          	$is_discount=true;
					}else{
          	$view->assign(array(
              'S_VIEW_DISCOUNT'      => '<!---',
              'E_VIEW_DISCOUNT'      => '-->'
            ),'quote_table');
            $is_discount=false;
     			}
     			if(!$use_package){
     				$view->assign(array(
     					'USE_PACKAGE2'		  =>'hide'),'quote_table'
     				);
     				$view->assign(array(
     					'USE_PACKAGE'		  =>'class="hide"'),'quote_line'
     				);
     				$view->assign(array(
     					'USE_PACKAGE'		  =>'class="hide"'),'quote_table'
     				);
     			}
     			if(!$use_sale_unit){
     				$view->assign(array(
     					'USE_SALE_UNIT2'		  =>'class="hide"'),'quote_table'
     				);
     				$view->assign(array(
     					'USE_SALE_UNIT'		  =>'class="hide"'),'quote_line'
     				);
     			}
          $line_total=$table['quantity'][$index] * $table['price'][$index] * ($table['package'][$index]/ $table['sale_unit'][$index]);
          $view->assign(array(
						'TR_ID'         			=> $item,
						'DESCRIPTION'  				=> nl2br($val),
            'SALE_UNIT'           => $table['sale_unit'][$index],
						'ARTICLE_CODE'      	=> $table['article_code'][$index],
            'PACKAGE'      	      => remove_zero_decimals($table['package'][$index]),
            'DISCOUNT'      			=> display_number($table['line_discount'][$index]),
						'QUANTITY'      			=> display_number($table['quantity'][$index]),
						'PRICE'         			=> display_number_var_dec($table['price'][$index]),
						'LINE_TOTAL'          => place_currency(display_number(($line_total - ($line_total* $table['line_discount'][$index]/100))) ,get_commission_type_list($currency_type)),
						'is_article'					=> $is_article_code ? true : false,
						'art_img'							=> $image,
						'show_block_total'		=> $table['show_block_total'][$index],
						'hide_QC'							=> $hide_QC ? 'hide' : "",
						'hide_AC'							=> $hide_AC ? 'hide' : "",
					),'quote_line');

				  $tableTotal += $line_total - ($line_total* $table['line_discount'][$index]/100);
				  if($use_discount > 1){
						$p = ($line_total - ($line_total* $table['line_discount'][$index]/100));
						$p = $p - $p * $discount_procent / 100;
						$vat_line += $p * $table['vat_val'][$index]/100;
					}else{
						$vat_line += ($line_total - ($line_total* $table['line_discount'][$index]/100))*$table['vat_val'][$index]/100;
					}
					$view->loop('quote_line','quote_table');
					$i++;
				}

				if($i > 0){
					$view->assign(array(
						'ITEM_TXT'            => getq_label_txt('item',$in['lid']),
            'ARTICLE_CODE_TXT'    => getq_label_txt('article_code',$in['lid']),
            'UNITMEASURE_TXT'     => getq_label_txt('unitmeasure',$in['lid']),
            'QUANTITY_TXT'        => getq_label_txt('quantity',$in['lid']),
            'UNIT_PRICE_TXT'      => getq_label_txt('unit_price',$in['lid']),
            'PACKAGE_TXT'         => getq_label_txt('package',$in['lid']),
            'SALE_UNIT_TXT'       => getq_label_txt('sale_unit',$in['lid']),
            'DISCOUNT_TXT'        => getq_label_txt('discount',$in['lid']),
            'AMOUNT_TXT'          => getq_label_txt('amount',$in['lid']),
            'AMOUNT_DUE_TXT'      => getq_label_txt('amount_due',$in['lid']),
						'TOTAL' 						  => place_currency(display_number($tableTotal),get_commission_type_list($currency_type)),
						'HIDE_CURRENCY2' 	  	=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
						'HIDE_CURRENCY1' 	  	=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
						'SALE_UNIT_TXT'       => getq_label_txt('sale_unit',$in['lid']),
						'PACKAGE_TXT'         => getq_label_txt('package',$in['lid']),
						'GROUP_TABLE_ID' 	  	=> $item,
						'q_table'			  			=> true,
						'block_total_hide'		=> $show_block_total ? '' : 'hide',
						'checkedBAD'					=> $show_block_total ? '' : 'CHECKED',
						'checkedQC'						=> $hide_QC ? 'CHECKED' : '',
						'checkedAC'						=> $hide_AC ? 'CHECKED' : '',
						'hide_QC'							=> $hide_QC ? 'hide' : "",
						'hide_AC'							=> $hide_AC ? 'hide' : "",
						'show_lVAT'						=> $show_block_vat ? '' : 'hide',
						'VAT_TOTAL'						=> display_number($vat_line),
						'VAT_TXT'							=> getq_label_txt('vat',$in['lid']),
					),'quote_table');
					//if we have service bloc title we show it
	        $service_bloc_title = $table['service_bloc_title'];
	        $view->assign(array('SERVICE_BLOC_TITLE'=>$service_bloc_title),'quote_table');
          if($service_bloc_title){
          	$view->assign(array('SERVICE_BLOC_HAS_TITLE' => ''),'quote_table');
          }else{
          	$view->assign(array('SERVICE_BLOC_HAS_TITLE' => 'hide'),'quote_table');
          }

					$item_width_dif=0;
          if(!$is_article_code){   //service
            if($in['type']==4 || $in['type']==5){
							// if($use_package){
								$item_width_dif=13;
							// }
						}
         		if($is_discount){
         			#if we are using a customer pdf template
         			if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									//for DAS MEDIA
									// $item_width_dif=20;
									// if($use_package){
									// 	$item_width_dif=28;
									// }
									$item_width_dif=-11;
									//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										$item_width_dif=0;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										$item_width_dif=0;
									}
								}
							}
              $view->assign(array('ITEM_WIDTH'=>$item_width-11-$item_width_dif),'quote_table');
            }else{
            	// echo $initial_item_width."<br>";
             	#if we are using a customer pdf template
             	if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									// $item_width_dif=22;
									// if($use_package){
									// 	$item_width_dif=30;
									// }
									$item_width_dif=-8;
									//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										$item_width_dif=0;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										$item_width_dif=0;
									}
								}
							}
              $view->assign(array('ITEM_WIDTH'=>$item_width-$item_width_dif),'quote_table');
            }
          }elseif($use_package){  //article
            if($is_discount){
            	if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									//for DAS MEDIA
									$item_width_dif=-38;
									//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										$item_width_dif=-8;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										$item_width_dif=-8;
									}
								}
							}
              $view->assign(array('ITEM_WIDTH'=>$item_width-38-$item_width_dif),'quote_table');
            }else{
             	#if we are using a customer pdf template
             	if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									//for DAS MEDIA
									$item_width_dif=-35;
									//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										$item_width_dif=-8;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										$item_width_dif=-8;
									}
								}
							}
							// $item_width_dif;
              $view->assign(array('ITEM_WIDTH'=>$item_width-27-$item_width_dif),'quote_table');
            }
          }else{
            if($is_discount){
               		#if we are using a customer pdf template
            	if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									//for DAS MEDIA
									$item_width_dif=-33;
									//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										$item_width_dif=-8;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										$item_width_dif=-8;
									}
								}
							}
              $view->assign(array('ITEM_WIDTH'=>$item_width-25-$item_width_dif),'quote_table');
            }else{
            	$item_width_dif = -1;
             	#if we are using a customer pdf template
             	if($in['custom_type']){
								if((defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0)||$use_custom_template==1){
									//for DAS MEDIA
									$item_width_dif=-30;
									//for SIESQO
									if(obj::get('db') == 'c656cd7e_7144_757f_92b34246c311'){
										$item_width_dif=-9;
									}
									//for salesassist_2
									if(obj::get('db') == 'salesassist_2'){
										$item_width_dif=-9;
									}
								}
							}
              $view->assign(array('ITEM_WIDTH'=>$item_width-15-$item_width_dif),'quote_table');
            }
          }
          $view->loop('quote_table','quote_group');
				}
				if($show_block_total === true){
					$chapter_total += $tableTotal;
				}
			}
			if(!empty($content) && $content_type == 2){
				// $content = str_replace('?', '', $content);
   			$content = utf8_decode($content);
				$content = str_replace('?', '', $content);
				$content = utf8_encode($content);
				$content = str_replace('<sub>', '<small>', $content);
        $content = str_replace('</sub>', '</small>', $content);
        $content = str_replace('<sup>', '<small>', $content);
        $content = str_replace('</sup>', '</small>', $content);

				$view->assign(array(
					'QUOTE_CONTENT'	=> $content,
					'q_content'		=> true,
				),'quote_table');
				$view->loop('quote_table','quote_group');
			}
			if($content_type == 3){
				$view->assign(array(
					'PAGE_BREAK'	=> $content,
					'pagebreak'		=> true,
				),'quote_table');
				$view->loop('quote_table','quote_group');
			}
		}
		$view->assign(array('QUOTE_GROUP_HAS_TITLE'=>empty($quote_data[$group_id]['title']) ? 'hide' : ''),'quote_group');
		$view->assign(array('QUOTE_GROUP_TITLE'=>$quote_data[$group_id]['title']),'quote_group');

		//chapter total
		$show_chapter_subtotal = false;
		if( $quote_data[$group_id]['show_chapter_total'] == 1 && !empty($quote_data[$group_id]['title']) ) {
			$show_chapter_subtotal = true;
		}
		$view->assign(array('SHOW_CHAPTER_TOTAL'=> $show_chapter_subtotal),'quote_group');
		$view->assign(array('CHAPTER_TOTAL_TXT' => getq_label_txt('chapter_total',$in['lid']),),'quote_group');
		$view->assign(array('CHAPTER_TOTAL' => place_currency(display_number($chapter_total),get_commission_type_list($currency_type))),'quote_group');


		$view->loop('quote_group');
	}

	if($discount_procent){
		$discount_value = ($grand_total * $discount_procent) / 100;
		// $grand_total -= $discount_value;
		$view->assign(array(
			'TOTAL_DISCOUNT_PROCENT'=>display_number($discount_procent),
			'DISCOUNT_VALUE'		=>$discount_value ? display_number($discount_value) : display_number(0),
			'VIEW_DISCOUNT'			=>''
		));
	}else{
		$view->assign(array('VIEW_DISCOUNT'=>'hide'));
	}

	$vat_total = 0;
	$nr_of_vats = 0;
	foreach ($vat_val as $key => $value) {
		if($value){
			$nr_of_vats ++;
			$view->assign(array(
				'vat_percent'			=> display_number($key),
				'vat_value'				=> place_currency(display_number($value),get_commission_type_list($currency_type)),
				'vat_txt'				=> getq_label_txt('vat',$in['lid']),
			),'vat_loop');
			$view->loop('vat_loop');
			$vat_total += $value;
		}
	}


	// set the height of the page depending by several params
	if($show_total){
		$height = 262;
		if($discount_procent){
			$height -= 5;
		}
		if(!$show_vat){
			$height +=5;
		}else{
			$height = $height - $nr_of_vats*5;
		}
		if(!USE_QUOTE_PAGE_NUMBERING){
			$height +=8;
		}
	}else{
		$height = 280;
	}


	// $grand_total_vat = $grand_total + $vat_total;
	$grand_total_vat = ($grand_total - $grand_total * $discount_procent / 100) + $vat_total;
	if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
			$total_default = $grand_total_vat*return_value($currency_rate);
	}
	$view->assign(array(
		'GRAND_TOTAL' 		=> place_currency(display_number($grand_total_vat),get_commission_type_list($currency_type)),
		'GRAND_TOTAL_DEF' 	=> place_currency(display_number($total_default),get_commission_type_list(ACCOUNT_CURRENCY_TYPE)),
		'total_novat'		=> place_currency(display_number($grand_total),get_commission_type_list($currency_type)),
	));
}

if($hide_all == 1){
	$view->assign(array('HIDE_ALL'=>'hide'));
}else if($hide_all == 2){
	$view->assign(array('HIDE_T'=>'hide'));
}

return $view->fetch('CONTENT');