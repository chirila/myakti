<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'icepay/icepay_api_basic.php';
global $database_config;


        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $db_user = new sqldb($db_config);
if(defined('ICEPAY_MERCHANTID2') && defined('ICEPAY_SECRETCODE2')){
    /* Apply logging rules */
    $logger = Icepay_Api_Logger::getInstance();
    $logger->enableLogging(true)
            ->setLoggingLevel(Icepay_Api_Logger::LEVEL_ALL)
            ->logToFile(true)
            ->setLoggingDirectory(realpath("icepay/logs"))
            ->setLoggingFile($database_config['mysql']['database'].'.txt')
            ->logToScreen(false);

    class ICE_Order  {
        protected $status = "OPEN";
        public $data;
        public function loadByOrderID($id) {}
        public function getStatus() {return $this->status;}
        public function getResultData() {return $this->data;}
        public function setData($d){ $this->data = $d; }
    }

    /* Start the postback class */
    $icepay = new Icepay_Postback();
    $icepay->setMerchantID(ICEPAY_MERCHANTID2)
            ->setSecretCode(ICEPAY_SECRETCODE2)
            ->doIPCheck();

    $order  = new ICE_Order();

    if($icepay->validate()){
        $order->loadByOrderID($icepay->getOrderID());
        $order->setData($icepay->getPostback());
        if ($icepay->canUpdateStatus($order->getStatus())){
            $inv_id = $db->field("SELECT item FROM icepay_orders WHERE id='".$icepay->getOrderID()."' ");
            // $db->query("INSERT INTO settings SET value='ma-ta-i curva3.".$inv_id.".-.".$icepay->getOrderID()."' ");
            $payment_date = date("Y-m-d",time());
            $payment_view_date=date(ACCOUNT_DATE_FORMAT,time());
            $info=$payment_view_date.'  -  '.place_currency(display_number($in['Amount']/100),'&euro;');
            $db->query("UPDATE tblinvoice SET icepay_payment = '".$order->data->paymentID."',
            icepay_status =  '".$order->data->status."',
                                    icepay_id =  '".$order->data->transactionID."'
            WHERE id= '".$inv_id."'");
            if($order->data->status == 'OK'){
                $db->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id= '".$inv_id."' ");
                $db->query("INSERT INTO tblinvoice_payments SET
                              invoice_id  = '".$inv_id."',
                              date    = '".$payment_date."',
                              amount    = '".($in['Amount']/100)."',
                              info        = '".$info."' ");
            }else{
                $db->query("INSERT INTO icepay_orders SET item='".$inv_id."' , type='1' ");
            }
        }
    }
}