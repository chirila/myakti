<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $config;
$db5 = new sqldb();
$quote_data = $db5->query("SELECT serial_number,preview, show_grand, show_vat, discount, apply_discount, vat FROM tblquote WHERE id = '".$in['id']."' ");

$in['version_id'] = obj::get('version_id');
ark::loadLibraries(array('aws'));
$aws = new awsWrap(obj::get('db'));
$link =  $aws->getLink($config['awsBucket'].obj::get('db').'/quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf');
$content = file_get_contents($link);
$ver_code = $db5->field("SELECT version_code FROM tblquote_version WHERE version_id='".$in['version_id']."' ");
$name = 'quote_'.$quote_data->f('serial_number').'['.$ver_code.'].pdf';

$file_len = strlen($content);
header('Content-Type: application/pdf');
header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
header('Pragma: public');
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header("X-Robots-Tag: noindex,nofollow,noarchive");
if($in['dw']==1){
	header('Content-Description: File Transfer');
	// force download dialog
	if (strpos(php_sapi_name(), 'cgi') === false) {
		header('Content-Type: application/force-download');
		header('Content-Type: application/octet-stream', false);
		header('Content-Type: application/download', false);
		header('Content-Type: application/pdf', false);
	} else {
		header('Content-Type: application/pdf');
	}
	header('Content-Disposition: attachment; filename="'.basename($name).'";');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: '.$file_len);
}
echo $content;
exit();