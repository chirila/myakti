<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$im = new imagick();
$im->setResolution(110,110);
$im->readImage(INSTALLPATH.ENVIRONMENT.'/'.$in['name'].'['.$in['page'].']');

$im->setImageCompression(Imagick::COMPRESSION_JPEG);
$im->setImageCompressionQuality(100);

$im->setImageFormat('png');

mkdir(INSTALLPATH.'upload/'.obj::get('db').'/quote_cache',0755);
$im->writeImage(INSTALLPATH.'upload/'.obj::get('db').'/quote_cache/quote_'.$in['id'].'_'.$in['version'].'_'.$in['page'].".png");

$im->clear();
$in['pag'] = ++$in['page'];
if($in['pag'] == $in['pages'] ){
	$db->query("UPDATE tblquote SET preview='1' WHERE id='".$in['id']."' ");
	unlink(INSTALLPATH.ENVIRONMENT.'/'.$in['name']);
}
return $view->fetch('CONTENT');