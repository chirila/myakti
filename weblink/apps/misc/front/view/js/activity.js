$(function(){
	var clicked_strike_ex = false;
	var clicked = false;
	var style_of_comment='.comment_logging';
	var q = $('.q').val();
	$('.activity').on('click','.add_comment',function(e){
		e.preventDefault();
		$(this).hide(0,function(){
			$(this).next().slideDown();
		});
	}).on('click','.add_c',function(e){
		e.preventDefault();
		var elem = $(this);
		var fields = elem.parents('.comment_form').serializeArray(), params = {};
		$.each(fields,function(i,field){
			params[field.name] = field.value;
		});
		if(validateForm(params["email"])){
			if(params["comment"] && params["name"] && params["email"] && !clicked){
				show_loader();
				$.post('index.php',params,function(o){
					elem.parents(style_of_comment).find('textarea').val('').end().find('input[type="text"]').val('');
					show_loader(true);
					if(elem.parents('.activity').attr('id') == 'dialog1'){
						$( "#dialog1" ).dialog('close');
					}else{
						$('.comment_cancel').click();
					}
				},'json');
			}else{
				alert('Please fill all the fields.');
				// show_alert('Alert',getLanguage('Message cannot be empty.'));
			}
		}
	}).on('click','.show_comment',function(e){
		e.preventDefault();
		var elems = $(this).parents('.comment_list_ul').find('li.hide').removeClass('hide');
		$('.dont_show_comment').parent().removeClass('hide');
		$(this).parent().addClass('hide');
	}).on('click','.dont_show_comment',function(e){
		e.preventDefault();
		$(this).parents('.comment_list_ul').find('li').filter(':not(:lt(3))').addClass('hide');
		$('.show_comment').parent().removeClass('hide');
	}).on('focus','.textarea_logging',function(){
		$('.activity').removeClass('hide_activity');
		$('.dont_hide').addClass('hide');
	}).on('click','.comment_cancel',function(e){
		e.preventDefault();
		if($(this).parents('.activity').attr('id') == 'dialog1'){
			$( "#dialog1" ).dialog('close');
		}else{
			//$('.buttons-main li').removeClass('modal_pos')
			$('.dont_hide').removeClass('hide');
			$('.activity').addClass('hide_activity');
		}
		$('.activity .textarea_logging').val('');
		$('.activity').find('input[type="text"]').val('');
	});

function show_loader(hide){
	if(hide){
		clicked = false;
		$('.show_loading').slideUp(function(){
			$('.activity').addClass('hide_activity');
			$('.dont_hide').removeClass('hide');
		});
	}else{
		clicked = true;
		$('.show_loading').slideDown();
	}
}

});


function getLanguage(text) {
	var base64 = base64_encode(text);
	if(!LANG[base64]) {
		//save it for future reference
		$.post('index.php', {
			'do' : 'misc--lang-add',
			'word' : text,
			'debug' : 1
		});
		LANG[base64] = text;
		return text;
	}
	return LANG[base64];
}

function base64_encode(data) {
	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, enc = "", tmp_arr = [];

	if(!data) {
		return data;
	}
	data = utf8_encode(data + '');

	do {// pack three octets into four hexets
		o1 = data.charCodeAt(i++);
		o2 = data.charCodeAt(i++);
		o3 = data.charCodeAt(i++);
		bits = o1 << 16 | o2 << 8 | o3;
		h1 = bits >> 18 & 0x3f;
		h2 = bits >> 12 & 0x3f;
		h3 = bits >> 6 & 0x3f;
		h4 = bits & 0x3f;

		// use hexets to index into b64, and append result to encoded string
		tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	} while (i < data.length);
	enc = tmp_arr.join('');

	switch (data.length % 3) {
		case 1:
			enc = enc.slice(0, -2) + '==';
			break;
		case 2:
			enc = enc.slice(0, -1) + '=';
			break;
	}

	return enc;
}

function utf8_encode(argString) {
	if(argString === null || typeof argString === "undefined") {
		return "";
	}
	var string = (argString + '');
	var utftext = "", start, end, stringl = 0;
	start = end = 0;
	stringl = string.length;
	for(var n = 0; n < stringl; n++) {
		var c1 = string.charCodeAt(n);
		var enc = null;
		if(c1 < 128) {
			end++;
		} else if(c1 > 127 && c1 < 2048) {
			enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
		} else {
			enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
		}
		if(enc !== null) {
			if(end > start) {
				utftext += string.slice(start, end);
			}
			utftext += enc;
			start = end = n + 1;
		}
	}

	if(end > start) {
		utftext += string.slice(start, stringl);
	}
	return utftext;
}
function show_alert(title,text,select_target,current_lang_id,lang_id){
		$(select_target+' #lang').val(current_lang_id);
		$('.alert').attr('title',title);
		$('#ui-dialog-title-alert').html(title);
		$(".alert").dialog({
					autoOpen: false,
					position: 'center',
					width: '450px',
					resizable: false,
					draggable: false ,
					modal: true,
					buttons: [{
						text: 'OK',
						click: function() { $(select_target+' #not_save').val(0);
						                    $(this).dialog("close");
											$(select_target+' #current_lang_id').val(lang_id);
											$(select_target+' #lang').val(lang_id);
											$(select_target+' #lang').change(); }
					},
					{
						text: "Cancel",
						click: function(){
							$(this).dialog("close");

						}
					}]
		});
		$(".alert").html(text).dialog('open');
}

function validateForm(x) {
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
        return false;
    }
    return true;
}
