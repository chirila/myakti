<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $config;
	$view->assign(array(
		'base_url'					=> $config['site_url'],
	));
return $view->fetch();