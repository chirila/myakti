<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
session_start();

if(!$in['db']){
	$db =  new sqldb();

}else{
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $in['db'],
		);
	$db =  new sqldb($database_2); // recurring billing
}
$db2=new sqldb();
$db3 = new sqldb();
$path = ark::$viewpath;
if($in['type']){
	$html='invoice_print_'.$in['type'].'.html';
}elseif ($in['custom_type']) {
	$path = INSTALLPATH.'upload/'.obj::get('db').'/custom_pdf/';
	$html='custom_invoice_print-'.$in['custom_type'].'.html';
}
else{
	$html='invoice_print_1.html';
}
// $html='invoice_print_6.html';
$view_html = new at($path.$html);

$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id='".$in['lid']."'")->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
	if( ($j % 2) == 0){
		$j++;
		continue;
	}
	$labels[$key] = $value;
	$j++;
}

if(!$in['id'] && !$in['reminder']){
//sample
	//assign dummy data
	$height = 250;

    $factur_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));
	$view_html->assign(array(
//		'account_logo'        => ACCOUNT_LOGO?$upload_path.ACCOUNT_LOGO:$upload_path.'img/no-logo.png',
		'account_logo'        => $upload_path.'../img/no-logo.png',
		// 'account_logo'        => ACCOUNT_LOGO ? '../'.ACCOUNT_LOGO:'../img/no-logo.png',
		'billing_address_txt' => $labels['billing_address'],
		'invoice_note_txt'    => $labels['invoice_note'],
		'invoice_txt'         => $labels['invoice'],
		'date_txt'            => $labels['date'],
		'customer_txt'        => $labels['customer'],
		'item_txt'            => $labels['item'],
		'unitmeasure_txt'     => $labels['unitmeasure'],
		'quantity_txt'        => $labels['quantity'],
		'unit_price_txt'      => $labels['unit_price'],
		'amount_txt'          => $labels['amount'],
		'subtotal_txt'        => $labels['subtotal'],
		'discount_txt'        => $labels['discount'],
		'vat_txt'             => $labels['vat'],
		'payments_txt'        => $labels['payments'],
		'amount_due_txt'      => $labels['amount_due'],
		'notes_txt'           => $labels['notes'],
		'notes2_txt'          => $labels['invoice_note2'],
		'duedate_txt'		  => $labels['duedate'],
		'bankd_txt'			  => $labels['bank_details'],
		'bank_txt'			  => $labels['bank_name'],
		'bic_txt'			  => $labels['bic_code'],
		'iban_txt'			  => $labels['iban'],
		'phone_txt'			  => $labels['phone'],
		'fax_txt'			  => $labels['fax'],
		'url_txt'			  => $labels['url'],
		'email_txt'			  => $labels['email'],
		'our_ref_txt'		  => $labels['our_ref'],
		'your_ref_txt'		  => $labels['your_ref'],
		'vat_number_txt'      => $labels['vat_number'],
		'payment_inst_txt'	  => $labels['pay_instructions'],
		'net_txt'			  => $labels['net_amount'],

		'nr'				  => $in['custom_type'],
		'our_ref' 		      => '[Our reference]' ,
		'your_ref'     		  => '[Your reference]',

		'hide_f'			  => true,
		'hide_e'			  => true,
		'hide_p'			  => true,
		'hide_u'			  => true,


		'seller_b_fax'        => '[FAX]',
		'seller_b_email'      => '[Email]',
		'seller_b_phone'      => '[Phone]',
		'seller_b_url'        => '[URL]',

		'seller_name'         => '[Account Name]',
		'seller_d_country'    => '[Country]',
		'seller_d_state'      => '[State]',
		'seller_d_city'       => '[City]',
		'seller_d_zip'        => '[Zip]',
		'seller_d_address'    => '[Address]',
		'seller_b_country'    => '[Billing Country]',
		'seller_b_state'      => '[Billing State]',
		'seller_b_city'       => '[Billing City]',
		'seller_b_zip'        => '[Billing Zip]',
		'seller_b_address'    => '[Billing Address]',
		'serial_number'       => '####',
		'invoice_date'        => $factur_date,
		'invoice_due_date'	  => $factur_date,
		'invoice_vat'         => '##',
		'buyer_name'       	  => '[Customer Name]',
		'buyer_country'    	  => '[Customer Country]',
		'buyer_state'      	  => '[Customer State]',
		'buyer_city'       	  => '[Customer City]',
		'buyer_zip'           => '[Customer ZIP]',
		'buyer_address'    	  => '[Customer Address]',
    	'notes'            	  => '[NOTES]',
    	'notes2'              => '[COMPANY NOTES]',
    	'bank'				  => '[Bank name]',
    	'bic'				  => '[BIC]',
    	'iban'				  => '[IBAN]',
    	'invoice_buyer_vat'   => '[Account VAT]',
    	'invoice_vat_no'      => '[Customer VAT]',
    	//'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
    	'session'			  => obj::get('db'),
    	'reminder'			  => false,
	));

   $i=0;
   while ($i<3){

		$view_html->assign(array(
			'row_nr'				=> $i+1,
			'row_description'		=> 'Article name',
			'row_unitmeasure'		=> 'MT',
			'row_quantity'			=> display_number(5),
			'row_unit_price'		=> display_number_var_dec(3),
			'row_amount'			=> place_currency(display_number(15)),
		),'invoice_row');

		$i++;
		 $view_html->loop('invoice_row');
	}

  $view_html->assign(array(
		'total_payments'			=> place_currency(display_number(0)),
		'total_discount_procent'	=> place_currency(display_number(10)),
		'discount_value'			=> place_currency(display_number(4.5)),
		'total_vat_procent'     	=> display_number(5).'%',
		'vat_value'		        	=> place_currency(display_number(5)),
		'total_novat'	       		=> place_currency(display_number(45)),
		'total_amount_due'	    	=> place_currency(display_number(40.5)),
	));

}elseif($in['id'] && !$in['reminder']){
	$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['lid']."' ");
	$db->query("SELECT * FROM tblinvoice WHERE id='".$in['id']."'");
	$db->move_next();

	$apply_discount = $db->f('apply_discount');
	$serial_number = $db->f('serial_number');
	$vat = $db->f('vat');
	$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('invoice_date'));
	$due_date = date(ACCOUNT_DATE_FORMAT,  $db->f('due_date'));
	$currency_type = $db->f('currency_type');
	$currency_rate = $db->f('currency_rate');
	$img = '../img/no-logo.png';
	$attr = '';
	if($in['logo'])
	{
		/*switch($in['logo']){
		case 1: $print_logo=ACCOUNT_LOGO;break;
		case 3: $print_logo=ACCOUNT_LOGO_QUOTE;break;
		case 2: $print_logo=ACCOUNT_LOGO_ORDER;break;*/
		$print_logo = $in['logo'];
		// }
	}else {
		$print_logo=ACCOUNT_LOGO;
	}
	if($print_logo){
		$img = 'http://10.0.0.1/marius/salesA/thinkweb-salesassist/branches/188/'.$print_logo;
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}
	if($db->f('contact_id') && !$db->f('buyer_id'))
	{
		$contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
									INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
									WHERE customer_contacts.contact_id='".$db->f('contact_id')."'");
	}
	if($contact_title)
	{
		$contact_title .= " ";
	}
	$cr_inv = gm('Credit Invoice',true);
	$cr_nr = gm('Credit Note Nr:',true);

	if(!$code){$code = 'en';}
	if($code == 'du'){$code = 'nl';}
	$free_field = $db->f('buyer_address').'<br/>'.$db->f('buyer_zip').' '.$db->f('buyer_city').'<br/>'.get_country_name($db->f('buyer_country_id')).'<br/>';

	$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'invoice'
								   AND item_name = 'notes'
								   AND active = '1'
								   AND item_id = '".$in['id']."' ");
	$modif_free_field = '<p>'.nl2br($db->f('free_field')).'</p>';
	if($db->f('type')==6){
		$modif_free_field = '<span>'.nl2br($db->f('free_field')).'</span>';
	}else{
		if(!$db->f('type') && $in['type']==6){
			$modif_free_field = '<span>'.nl2br($db->f('free_field')).'</span>';
		}
	}
	$width = 56;
	$view_html->assign(array(
		'account_logo'        => $img,
		'attr'				  => $attr,
		'billing_address_txt' => $labels['billing_address'],
		'invoice_note_txt'    => $db->f('type') == '2' ? utf8_encode($cr_inv['0'][$code]) : ($db->f('type') == '1' ? $labels['pro_invoice'] : $labels['invoice_note']),
		'invoice_txt'         => $db->f('type') == '2' ? utf8_encode($cr_nr['0'][$code]) : $labels['invoice'].':',
		'date_txt'            => $labels['date'],
		'customer_txt'        => $labels['customer'],
		'item_txt'            => $labels['item'],
		'unitmeasure_txt'     => $labels['unitmeasure'],
		'quantity_txt'        => $labels['quantity'],
		'unit_price_txt'      => $labels['unit_price'],
		'amount_txt'          => $labels['amount'],
		'subtotal_txt'        => $labels['subtotal'],
		'discount_txt'        => $labels['discount'],
		'vat_txt'             => $labels['vat'],
		'payments_txt'        => $labels['payments'],
		'amount_due_txt'      => $labels['amount_due'],
		'notes_txt'           => $notes ? $labels['notes'] : '',
		'notes2_txt'          => $db->f('notes2') ? $labels['invoice_note2'] : '',
		'duedate_txt'		  => $labels['duedate'],
		'bankd_txt'			  => $labels['bank_details'],
		'bank_txt'			  => $labels['bank_name'],
		'bic_txt'			  => $labels['bic_code'],
		'iban_txt'			  => $labels['iban'],
		'phone_txt'			  => $labels['phone'],
		'fax_txt'			  => $labels['fax'],
		'url_txt'			  => $labels['url'],
		'email_txt'			  => $labels['email'],
		'our_ref_txt'		  => $labels['our_ref'],
		'your_ref_txt'		  => $labels['your_ref'],
		'vat_number_txt' 	  => $labels['vat_number'],
		'payment_inst_txt'	  => $labels['pay_instructions'],
		'net_txt' 			  => $labels['net_amount'],

		'seller_name'         => $db->f('seller_name'),
		'seller_d_country'    => get_country_name($db->f('seller_d_country_id')),
		'seller_d_state'      => get_state_name($db->f('seller_d_state_id')),
		'seller_d_city'       => $db->f('seller_d_city'),
		'seller_d_zip'        => $db->f('seller_d_zip'),
		'seller_d_address'    => $db->f('seller_d_address'),
		'seller_b_country'    => get_country_name($db->f('seller_b_country_id')),
		'seller_b_state'      => get_state_name($db->f('seller_b_state_id')),
		'seller_b_city'       => $db->f('seller_b_city'),
		'seller_b_zip'        => $db->f('seller_b_zip'),
		'seller_b_address'    => $db->f('seller_b_address'),
		'invoice_vat_no'      => $db->f('seller_bwt_nr'),
		'serial_number'       => $db->f('serial_number') ? $db->f('serial_number') : '',

		'our_ref' 		      => $db->f('our_ref') ,
		'your_ref'     		  => $db->f('your_ref'),
		'hide_our'			  => $db->f('our_ref')? '':'hide',
		'hide_your'			  => $db->f('your_ref')? '':'hide',
		'nr'				  => $in['custom_type'],

		'seller_b_fax'        => ACCOUNT_FAX,
		'seller_b_email'      => ACCOUNT_EMAIL,
		'seller_b_phone'      => ACCOUNT_PHONE,
		'seller_b_url'        => ACCOUNT_URL,

		'hide_f'			  => defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? false : true,
		'hide_e'			  => defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? false : true,
		'hide_p'			  => defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? false : true,
		'hide_u'			  => defined('ACCOUNT_URL') && !ACCOUNT_URL ? false : true,
		'is_proforma'		  => $db->f('type')==1? true:false,
		'not_proforma'		  => $db->f('type')==1? false: true,

		'invoice_date'        => $factur_date,
		'invoice_due_date'    => $due_date,
		'invoice_vat'         => $db->f('vat'),
		'buyer_name'       	  => $contact_title.$db->f('buyer_name'),
		'buyer_email'      	  => $db->f('buyer_email'),
		'buyer_country'    	  => get_country_name($db->f('buyer_country_id')),
		'buyer_state'      	  => get_state_name($db->f('buyer_state_id')),
		'buyer_city'       	  => $db->f('buyer_city'),
		'buyer_zip'        	  => $db->f('buyer_zip'),
		'buyer_address'    	  => nl2br(utf8_encode($db->f('buyer_address'))),
		'free_field'		  => $db->f('free_field') ? $modif_free_field : $free_field,
		'invoice_buyer_vat'   => ACCOUNT_VAT_NUMBER,
		'bank'				  => ACCOUNT_BANK_NAME,
	  	'bic'				  => ACCOUNT_BIC_CODE,
	  	'iban'				  => ACCOUNT_IBAN,
	  	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',

		'notes'            	  => $notes ? nl2br($notes) : '',
		'notes2'              => $db->f('notes2') ? nl2br(utf8_decode($db->f('notes2'))) : '',
		'view_notes'          => $notes ? '' : 'hide' ,
		'view_notes2'         => $db->f('notes2') ? '' : 'hide' ,
		'default_total'  	  => false,//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? false : true) : false,
		'def_currency'		  => build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
		'session'			  => obj::get('db'),
		'inv_type'			  => $db->f('type'),
		'red_if_credit'		  => $db->f('type')==2 ? 'color:#ff0000;' : '',
		'reminder'			  => false,
		'hide_disc_line'	  => ($apply_discount ==0 || $apply_discount == 2) ? 'hide' : '',
		'item_w'			  => ($apply_discount ==0 || $apply_discount == 2) ? $width : $width-15,
	));
	$height = 251;
	if($db->f('discount') == 0){
		$view_html->assign('sh_discount','hide');
		$discount_procent=0;
	} else {
		$height -= 5;
		$discount_procent = $db->f('discount');
		$view_html->assign('total_discount_procent',display_number($db->f('discount')));
	}

	if($apply_discount < 2){
		$discount_procent = 0;
		$view_html->assign('sh_discount','hide');
	}

	if($db->f('remove_vat') == 1){
		$view_html->assign('sh_vat','hide');
		$vat_procent=0;
	} else {

		$vat_procent = $db->f('vat');
		$view_html->assign('total_vat_procent',$db->f('vat'));
	}

//PAID
	if($db->f('paid') == 1){
		$discount_value = 0;
		$sub_total = 0;
		$sub_total2 = 0;
		$vat_line = 0;
		$subtotal_vat = 0;
		$req_payment = $db->f('req_payment');
		//total
		// $db->query("SELECT SUM(amount) AS total, vat FROM tblinvoice_line WHERE invoice_id='".$in['id']."' GROUP BY vat ");
		$db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['id']."' ");
		while ($db->move_next()) {
			$line_discount = $db->f('discount');
			if($apply_discount ==0 || $apply_discount == 2){
				$line_discount = 0;
			}
			// $discount_value += $db->f('total')*$discount_procent/100;
			// $sub_total += $db->f('total')-($db->f('total')*$discount_procent/100);
			// $sub_total2 += $db->f('total');
			// $vat_line += ($db->f('total')-($db->f('total')*$discount_procent/100))*($db->f('vat')/100);

			$amount_line = $db->f('amount') - $db->f('amount') * $line_discount / 100;
			$amount_line_disc = $amount_line*$discount_procent/100;
			$discount_value += $amount_line_disc;
			$subtotal_vat += ( $amount_line - $amount_line_disc ) * $db->f('vat') / 100;

			$sub_total += $amount_line - $amount_line_disc;
			$sub_total2 += $amount_line;
		}
		if($discount_procent != 0){
			$view_html->assign(array(
				'discount_value' 	=> place_currency(display_number($discount_value)),
				'net_value'			=> place_currency(display_number($sub_total2-$discount_value))
			));
		}

		$total = $sub_total+$subtotal_vat;

		if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
			$total_default = $total*return_value($currency_rate);
		}

		$req_payment_value=$total* $req_payment/100;
		if($req_payment != 100){
			$view_html->assign('sh_pay','hide');
		}
		$view_html->assign('req_payment',100 - $req_payment);
		$view_html->assign(array(
			'total_amount_due'				=> place_currency(display_number($total),get_commission_type_list($currency_type)),
			'total_amount_due_default'		=> place_currency(display_number($total_default)),
			'total_payments'				=> place_currency(display_number(0),get_commission_type_list($currency_type)),
			'sh_edit'						=> 'hide',
			'total_novat'	   				=> place_currency(display_number($sub_total2),get_commission_type_list($currency_type)),
			'req_payment_value'         	=> place_currency(display_number($total-$req_payment_value),get_commission_type_list($currency_type)),
			'vat'							=> $vat,
		));

	}//NOT PAID / NOT FULLY PAID
	else {
		$discount_value = 0;
		$sub_total = 0;
		$sub_total2 = 0;
		$vat_line = 0;
		$subtotal_vat = 0;
		$req_payment = $db->f('req_payment');
		//total
		$db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['id']."' ");
		while($db->move_next()){
			$line_discount = $db->f('discount');
			if($apply_discount ==0 || $apply_discount == 2){
				$line_discount = 0;
			}
			// $discount_value += $db->f('total')*$discount_procent/100;
			// $sub_total += $db->f('total')-($db->f('total')*$discount_procent/100);
			// $sub_total2 += $db->f('total');
			// $vat_line += ($db->f('total')-($db->f('total')*$discount_procent/100))*($db->f('vat')/100);

			$amount_line = $db->f('amount') - $db->f('amount') * $line_discount / 100;
			$amount_line_disc = $amount_line*$discount_procent/100;
			$discount_value += $amount_line_disc;
			$subtotal_vat += ( $amount_line - $amount_line_disc ) * $db->f('vat') / 100;

			$sub_total += $amount_line - $amount_line_disc;
			$sub_total2 += $amount_line;
		}

		if($discount_procent != 0){
			$view_html->assign(array(
				'discount_value'	=> place_currency(display_number($discount_value),get_commission_type_list($currency_type)),
				'net_value'			=> place_currency(display_number($sub_total2-$discount_value),get_commission_type_list($currency_type))
			));
		}

		$total = $sub_total+$subtotal_vat;
		if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
			$total_default = $total*return_value($currency_rate);
		}

		//already payed
		$db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['id']."' ");
		$db->move_next();
		$total_payed = $db->f('total_payed');
		$req_payment_value=$total* $req_payment/100;
		if($req_payment == 100){
			$amount_due = round($total - $total_payed,2);
		}else{
			$amount_due = round($req_payment_value - $total_payed ,2);
		}


		$view_html->assign('req_payment',$req_payment);

		$view_html->assign(array(
			'total_payments'				=> place_currency(display_number($amount_due),get_commission_type_list($currency_type)),
			'payment_date'  				=> $in['payment_date'],
			'total_amount_due'				=> place_currency(display_number($total),get_commission_type_list($currency_type)),
			'total_amount_due_default'		=> place_currency(display_number($total_default)),
			'total_novat'	   				=> place_currency(display_number($sub_total2),get_commission_type_list($currency_type)),
			'vat'							=> $vat,
			'sh_pay'						=> $total_payed ? '' : 'hide',
			'req_payment_value'         	=> place_currency(display_number($req_payment_value-$total_payed),get_commission_type_list($currency_type))
		));
		if($db->f('req_payment') != 100){
			$view_html->assign('sh_pay','hide');
		}
	}

	$vr = $db->field("SELECT COUNT(DISTINCT vat) FROM tblinvoice_line WHERE invoice_id='".$in['id']."' ");

	//GET invoice rows
	$db->query("SELECT tblinvoice_line.*,tblinvoice.id,tblinvoice.currency_type, tblinvoice_line.discount as disc_line
            FROM tblinvoice_line
            INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
            WHERE tblinvoice_line.invoice_id='".$in['id']."'
            order by tblinvoice_line.sort_order ASC ");

	$i = 0;
	$total_amount = 0;
	$vat_percent = array();
	$vat_percent_val = 0;

	while ($db->move_next()){
		$line_d = $db->f('discount');
		if($apply_discount ==0 || $apply_discount == 2){
			$line_d = 0;
		}
		$amount = $db->f('amount') - $db->f('amount') * $line_d / 100;
		$view_html->assign(array(
			'row_nr'				=> $i+1,
			'row_description'		=> nl2br(htmlentities($db->f('name'),null,'UTF-8')),
			'row_unitmeasure'		=> $db->f('unitmeasure'),
			'row_quantity'			=> display_number($db->f('quantity')),
			'row_unit_price'		=> display_number_var_dec($db->f('price')),
			'row_vat'				=> display_number($db->f('vat')),
			'row_amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
			'row_discount'			=> display_number($db->f('disc_line')),
			'hide_disc_line'	  	=> ($apply_discount ==0 || $apply_discount == 2) ? 'hide' : '',
			'if_disc_line'			=> ($apply_discount ==0 || $apply_discount == 2) ? ($vr > 1 ? '39' : '49') : ($vr > 1 ? '29' : '39'),
			'content'               => $db->f('content'),
			'colspan'               => ($apply_discount ==0 || $apply_discount == 2) ? ' colspan="5" ': 'colspan="6"',
			'i_w'					=> ($apply_discount ==0 || $apply_discount == 2) ? ($vr > 1 ? '35' : '45'): ($vr > 1 ? '25' : '35'),
			'hide_row_vat'			=> $vr > 1 ? '' : 'hide',
		),'invoice_row');

		$total_amount += $db->f('amount');
		$i++;
		$view_html->loop('invoice_row');
		//$view_html->parse('INVOICE_ROW_OUT','.invoice_row');

		$vat_percent_val = ( $amount - ($amount * ($discount_procent / 100) )) * $db->f('vat')/100;
		$vat_percent[$db->f('vat')] += $vat_percent_val;
	}
	$i=0;
	foreach ($vat_percent as $key => $val){
		if($val){
			$height -= 7;
			$view_html->assign(array(
				'vat_percent'   			=> display_number($key),
				'vat_value'	   				=> place_currency(display_number($val),get_commission_type_list($currency_type)),
				'vat_txt'             		=> get_label_txt('vat',$in['lid']),

			),'vat_line');
			$view_html->loop('vat_line');
		}
		$i++;
	}
	if($i > 1){
		$view_html->assign(array(
			'hide_row_vat'		=> '',
			'item_w'			=> ($apply_discount ==0 || $apply_discount == 2) ? $width-15 : $width-30,
		));
	}else{
		$view_html->assign(array(
			'hide_row_vat'		=> 'hide',
		));
	}

}else
{
	//$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['lid']."' ");
	list($buyer_id,$contact_id) = explode('-', $in['customer_id']);
	if($contact_id && !$buyer_id)
	{
		$contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
									INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
									WHERE customer_contacts.contact_id='".$contact_id."'");
	}
	if($contact_title)
	{
		$contact_title .= " ";
	}
	$customer_info = $db->query("SELECT customers.name, customers.invoice_email,customers.btw_nr FROM customers WHERE customers.customer_id='".$buyer_id."'");
	$customer_info->next();
	$address = $db->query("SELECT * FROM customer_addresses WHERE customer_id='".$buyer_id."' AND billing='1'");
	$address->next();
	$free_field = $address->f('address').'<br/>'.$address->f('zip').' '.$address->f('city').'<br/>'.get_country_name($address->f('country_id')).'<br/>';
	$print_logo=ACCOUNT_LOGO;
	if($print_logo){
		$img = '../../'.$print_logo;
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}
	if(!$img){
		$img = 	'../../img/no-logo.png';
	}
	$factur_date = date(ACCOUNT_DATE_FORMAT,  time());
	$view_html->assign(array(
		'account_logo'        => $img,
		'attr'				  => $attr,
		'billing_address_txt' => get_label_txt('billing_address',$in['lid']),
		//'invoice_note_txt'    => $db->f('type') == '2' ? utf8_encode($cr_inv['0'][$code]) : ($db->f('type') == '1' ? get_label_txt('pro_invoice',$in['lid']) : get_label_txt('invoice_note',$in['lid'])),
		'invoice_txt'         => get_label_txt('invoice',$in['lid']).':',
		'date_txt'            => get_label_txt('date',$in['lid']),
		'customer_txt'        => get_label_txt('customer',$in['lid']),
		'item_txt'            => get_label_txt('item',$in['lid']),
		'unitmeasure_txt'     => get_label_txt('unitmeasure',$in['lid']),
		'quantity_txt'        => get_label_txt('quantity',$in['lid']),
		'unit_price_txt'      => get_label_txt('unit_price',$in['lid']),
		'amount_txt'          => get_label_txt('amount',$in['lid']),
		'subtotal_txt'        => get_label_txt('subtotal',$in['lid']),
		'discount_txt'        => get_label_txt('discount',$in['lid']),
		'vat_txt'             => get_label_txt('vat',$in['lid']),
		'payments_txt'        => get_label_txt('payments',$in['lid']),
		'amount_due_txt'      => get_label_txt('amount_due',$in['lid']),
		//'notes_txt'           => $notes ? get_label_txt('notes',$in['lid']) : '',
		//'notes2_txt'          => $db->f('notes2') ? get_label_txt('invoice_note2',$in['lid']) : '',
		'duedate_txt'		  => get_label_txt('duedate',$in['lid']),
		'bankd_txt'			  => get_label_txt('bank_details',$in['lid']),
		'bank_txt'			  => get_label_txt('bank_name',$in['lid']),
		'bic_txt'			  => get_label_txt('bic_code',$in['lid']),
		'iban_txt'			  => get_label_txt('iban',$in['lid']),
		'phone_txt'			  => get_label_txt('phone',$in['lid']),
		'fax_txt'			  => get_label_txt('fax',$in['lid']),
		'url_txt'			  => get_label_txt('url',$in['lid']),
		'email_txt'			  => get_label_txt('email',$in['lid']),
		'our_ref_txt'		  => get_label_txt('our_ref',$in['lid']),
		'your_ref_txt'		  => get_label_txt('your_ref',$in['lid']),
		'vat_number_txt' 	  => get_label_txt('vat_number',$in['lid']),
		'payment_inst_txt'	  => get_label_txt('pay_instructions',$in['lid']),
		'hide_our'			  => 'hide',
		'hide_your'			  => 'hide',

		'seller_name'         => ACCOUNT_COMPANY,
		'seller_d_country'    => get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
		'seller_d_state'      => get_state_name(ACCOUNT_DELIVERY_STATE_ID),
		'seller_d_city'       => ACCOUNT_DELIVERY_CITY,
		'seller_d_zip'        => ACCOUNT_DELIVERY_ZIP,
		'seller_d_address'    => ACCOUNT_DELIVERY_ADDRESS,
		'seller_b_country'    => get_country_name(ACCOUNT_BILLING_COUNTRY_ID),
		'seller_b_state'      => get_state_name(ACCOUNT_BILLING_STATE_ID),
		'seller_b_city'       => ACCOUNT_BILLING_CITY,
		'seller_b_zip'        => ACCOUNT_BILLING_ZIP,
		'seller_b_address'    => ACCOUNT_BILLING_ADDRESS,
		'invoice_vat_no'      => $customer_info->f('btw_nr'),

		'seller_b_fax'        => ACCOUNT_FAX,
		'seller_b_email'      => ACCOUNT_EMAIL,
		'seller_b_phone'      => ACCOUNT_PHONE,
		'seller_b_url'        => ACCOUNT_URL,
		'hide_f'			  => defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? false : true,
		'hide_e'			  => defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? false : true,
		'hide_p'			  => defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? false : true,
		'hide_u'			  => defined('ACCOUNT_URL') && !ACCOUNT_URL ? false : true,
		'buyer_name'       	  => $contact_title.$customer_info->f('name'),
		'buyer_email'      	  => $customer_info->f('invoice_email'),
		// 'buyer_country'    	  => get_country_name($db->f('buyer_country_id')),
		// 'buyer_state'      	  => get_state_name($db->f('buyer_state_id')),
		// 'buyer_city'       	  => $db->f('buyer_city'),
		// 'buyer_zip'        	  => $db->f('buyer_zip'),
		// 'buyer_address'    	  => nl2br(utf8_encode($db->f('buyer_address'))),
		'free_field'		  => $free_field,
		'invoice_buyer_vat'   => ACCOUNT_VAT_NUMBER,
		'bank'				  => ACCOUNT_BANK_NAME,
	  	'bic'				  => ACCOUNT_BIC_CODE,
	  	'iban'				  => ACCOUNT_IBAN,
	  	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	  	'def_currency'		  => build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
	  	'invoice_date'        => $factur_date,
	  	'reminder'			  => true,
	  	'border_bank'		  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? '':'border_bottom',
	  	'border_info'		  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'border_bottom':'',
	));
	$code = '';
	if($buyer_id != '0' ){
	 	$filter =" AND buyer_id='".$buyer_id."' ";
	 	$language = $db->field("SELECT internal_language FROM customers WHERE customer_id='".$buyer_id."' ");
 		$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$language."' ");
 		if($code == 'du'){$code = 'nl';}
	}else if($contact_id != '0'){
		$filter =" AND contact_id='".$contact_id."' ";
	}else{
		return false;
	}

	$invoice_text = gm('Invoice',true);
	$days_text = gm('days',true);
	$late_text = gm('Late',true);
	$first_grade = gm('First grade',true);
	$second_grade = gm('Second grade',true);
	$third_grade = gm('Third grade',true);
	$invoices = $db->query("SELECT * FROM tblinvoice WHERE sent='1' AND status ='0' AND f_archived='0' ".$filter." AND due_date < ".time()." ORDER BY invoice_grade DESC");
	$text = '<table width="100%" cellspacing="0" cellpadding="5" class="invoice_rows">';
	$old_grade='';
	$invoice_ids = '';
	$penalty_value = '';
	if(USE_GRADES=='0')
	{
		$text.='<tr><td colspan=\'4\' class="no_grade">&nbsp;</td></tr>';
	}
	while ($invoices->next()) {
		if(!$code){
			$language = $invoices->f('email_language');
			if(!$language){$language=1;}
			$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$language."' ");
			if(!$code){$code = 'en';}
			if($code == 'du'){$code = 'nl';}
		}
		$t1 = utf8_encode($invoice_text['0'][$code]);
		$t2 = utf8_encode($days_text['0'][$code]);
		$t3 = utf8_encode(strtolower($late_text['0'][$code]));
		switch ($invoices->f('invoice_grade')) {
			case '1':
				$grade='first';
				$grade_txt = utf8_encode($first_grade['0'][$code]);
				break;
			case '2':
				$grade='second';
				$grade_txt = utf8_encode($second_grade['0'][$code]);
				break;
			case '3':
				$grade='third';
				$grade_txt = utf8_encode($third_grade['0'][$code]);
				break;
		}
		$buyer_name = $invoices->f('buyer_name');
		$buyer_id = $invoices->f('buyer_id');
		$days = round((time() - $invoices->f('due_date'))/(60*60*24));
		$space = 15-strlen($invoices->f('serial_number'));
		if($old_grade!=$grade && USE_GRADES==1)
		{
			$penalty_value = '';
			$grade_info = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='".$grade."' AND lang_code='".$code."'");
			if($grade_info->f('value')!='0.00')
			{
				if($grade_info->f('type')==1)
				{
					$penalty_value = ' + '.number_format($grade_info->f('value'),0).'%';
				}else
				{
					$penalty_value = ' + '.place_currency(display_number($grade_info->f('value')));
				}
			}
			$text.='<tr><td class="th last" style="text-align:left;" colspan="4">'.$grade_txt.' '.$grade_info->f('message').'</td></tr>';
		}
		if(USE_GRADES=='0')
		{
			$penalty_value = '';
		}
		// $text .=$t1.": ".$invoices->f('serial_number').$nbsp.date(ACCOUNT_DATE_FORMAT,$invoices->f('invoice_date'))." : ".place_currency(display_number($invoices->f('amount'))).$nbsp2.$days." ".$t2." ".$t3.". \n";
		$text .='<tr>
		';
		$text .='<td width="25%" style="text-align:left;">'.$t1.': '.$invoices->f('serial_number').'</td>
		';
		$text .='<td width="25%" style="text-align:left;">'.date(ACCOUNT_DATE_FORMAT,$invoices->f('invoice_date')).'</td>
		';
		$text .='<td width="25%" style="text-align:left;">'.place_currency(display_number($invoices->f('amount_vat'))).$penalty_value.'</td>
		';
		$text .='<td width="25%" style="text-align:left;" class="last" >'.$days.' '.$t2.' '.$t3.'</td>
		</tr>
		';
		$old_grade = $grade;

		/*$view->assign(array(
			'invoice_id'	=> $invoices->f('id'),
		),'invoice_ids');
		$view->loop('invoice_ids');*/
	}
	$text .="</table>";
	$message_data=get_sys_message('invmess_late',$language);
	$body='<br />'.nl2br($message_data['text']);
	$body=str_replace('[!CUSTOMER!]',$buyer_name, $body);
	$body=str_replace('[!INVOICE_SUMMARY!]',$text, $body);
	$view_html->assign('reminder_table',$body);
	//$view_html->assign('buyer_name',$contact_title.$buyer_name);
}
$is_table = true;
$is_data = false;

#for layout nr 4.
if($hide_all == 1){
	$is_table = false;
	$is_data = true;
}else if($hide_all == 2){
	$is_table = true;
	$is_data = false;
}

$view_html->assign(array(
	'is_table'=> $is_table,
	'is_data'=>$is_data,
));
return $view_html->fetch();
?>