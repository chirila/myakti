<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if(!obj::get('table')){
	$view->assign(array('data'=>false));
	return $view->fetch();
}
$tblinvoice = $db->query("SELECT * FROM ".obj::get('table')." WHERE ".obj::get('main_field')."='".$in['invoice_id']."'");
if(!$tblinvoice->move_next()){
	$view->assign(array('show_comm'=>false));
	return $view->fetch();
}
$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?1');

if($tblinvoice->f('sent') == '0' && obj::get('app') != 'project' && obj::get('app') != 'order' ){
	$view->assign(array('show_comm'=>false));
	return $view->fetch();
}

$pag = obj::get('pag');
$field_name = obj::get('item');
$field_value = obj::get('id');
if(!$field_name || !$field_value || !$pag){
	return;
}
$i = 0;
$now = time();
$is_loging =false;

$count = $db->field("SELECT count(log_id) FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='0' ORDER BY date DESC ");
	$is_loging =true;
	$count_comment = $db->field("SELECT count(log_id) FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='1' ORDER BY date DESC ");
	$logs = $db->query("SELECT * FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='0' ORDER BY date ASC LIMIT 0,1 ");
	$logs->next();
	$patt = '/\[!L!\](.*)\[!\/L!\]\s/';
	if(!strstr($logs->f('message'),'[!L!]')){
		$patt = '/\{l\}(.*)\{endl\}\s/';
	}
	if(strpos($logs->f('message'), "Invoice was automatically created by") !== false){
		$names = gm('Recurring tool');
	}else{
		$names = preg_replace($patt, '', $logs->f('message'));
		if($names == '[!L!]Invoice created by recurring tool[!/L!]'){
			$names = gm('Recurring tool');
		}
	}	
	$counts = ($count-3).' '.gm('more actions');
	if(($count-3) == 1 ){
		$counts = ($count-3).' '.gm(' more action');
	}
	$in['field_value'] = $field_value;
	$in['field_name'] = $field_name;
	$in['pag'] = $pag;
	if($count_comment){
		$has_comment = true;
	}
	$view->assign(array(
		'ago'					=> $logs->f('date') ? date(ACCOUNT_DATE_FORMAT,$logs->f('date')) : '',
		'user_name'				=> $names,
		'field_name'			=> $field_name,
		'field_value'			=> $field_value,
		'page'					=> $pag,
		// 'user_id'				=> $_SESSION['u_id'],
		// 'user_dd'				=> build_user_dd(),
		'count'					=> $counts,
		'more'					=> $count > 4 ? true : false,
		'created'				=> $names ? gm('created this item') : gm('Item created'),
	));
#pim-customer_activity_comment_list
	$comment = $db->query("SELECT * FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='0' ORDER BY date DESC LIMIT 3 ");
	while ($comment->next()) {
		$replace = '[!/L!]';
		$replace_first = '[!L!]';
		if(!strstr($comment->f('message'),$replace)){
			$replace = '{endl}';
			$replace_first = '{l}';
		}
		$message = str_replace($replace_first,'',str_replace($replace,"",(substr(str_replace('.','',$comment->f('message')),0,strrpos($comment->f('message'), $replace)+strlen($replace)))));
		if($message){
			$names = substr(str_replace('.','',$comment->f('message')),strrpos($comment->f('message'), $replace)+strlen($replace));
			$text = $message.' <b>'.$names.'</b>';
		}else{
			$message = $comment->f('message');
			$text = $message;
		}
		$view->assign(array(
			'message'				=> $text,
			'dates'					=> date(ACCOUNT_DATE_FORMAT,$comment->f('date'))
		),'messages');
		$view->loop('messages');
		$i++;
	}

global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($db_config);

$view->assign(array(
	'comment_style'		=> 'comment_logging',
	'history_style' 	=> 'logging_side',
	'textarea_style'	=> 'width: 797px;',
	'comment_style_id'	=> '1'
));

$view->assign(array(
	'style_comment'	=> pick_date_format(ACCOUNT_DATE_FORMAT),
	'is_loging' 		=> $is_loging,
	'show_comm'			=> obj::get('allow_comments') == '1' ? true : false,
));

$lg = $db->field("SELECT lang_id FROM pim_lang WHERE active=1 AND default_lang='1'");
if($tblinvoice->f('email_language')){
	$lg = $tblinvoice->f('email_language');
}

$view->assign(array(
	'name_txt'		=> getq_weblink_label_text('name_webl', $lg),
	'email_txt'		=> getq_weblink_label_text('email_webl', $lg),
	'commnent_txt'	=> getq_weblink_label_text('addCommentLabel_webl', $lg),
	'submit_txt'	=> getq_weblink_label_text('submit_webl', $lg),
	'cancel_txt'	=> getq_weblink_label_text('cancel_webl', $lg)
));

return $view->fetch();