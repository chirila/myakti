<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if(!obj::get('table')){
	$view->assign(array('data'=>false));
	return $view->fetch();
}
$tblinvoice = $db->query("SELECT * FROM ".obj::get('table')." WHERE ".obj::get('main_field')."='".$in['invoice_id']."'");
if(!$tblinvoice->move_next()){
	$view->assign(array('show_comm'=>false));
	return $view->fetch();

}
$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js');

$pag = obj::get('pag');
$field_name = obj::get('item');
$field_value = obj::get('id');
if(!$field_value || !$field_value || !$pag){
	return;
}

$logs = $db->query("SELECT * FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='0' ORDER BY date ASC LIMIT 0,1 ");
$logs->next();
$patt = '/\[!L!\](.*)\[!\/L!\]\s/';
if(!strstr($logs->f('message'),'[!L!]')){
	$patt = '/\{l\}(.*)\{endl\}\s/';
}
$names = preg_replace($patt, '', $logs->f('message'));
if($names == '[!L!]Invoice created by recurring tool[!/L!]'){
	$names = gm('Recurring tool');
}

$in['field_value'] = $field_value;
$in['field_name'] = $field_name;
$in['pag'] = $pag;
$allow_comments_after_all = 0;
if($pag == 'quote' && ( defined('ALLOW_QUOTE_ACCEPT') && ALLOW_QUOTE_ACCEPT == 1)){
	$allow_comments_after_all = 1;
}

$view->assign(array(
	'user_name'				=> $names,
	'field_name'			=> $field_name,
	'field_value'			=> $field_value,
	'page'					=> $pag,
	'show_comm'				=> obj::get('allow_comments') == '1' || $allow_comments_after_all == 1 ? true : false,
));

$lg = $db->field("SELECT * FROM pim_lang WHERE active=1 AND default_lang='1'");
if($tblinvoice->f('email_language')){
	$lg = $tblinvoice->f('email_language');
}
$view->assign(array(
	'name_txt'		=> getq_weblink_label_text('name_webl', $lg),
	'email_txt'		=> getq_weblink_label_text('email_webl', $lg),
	'commnent_txt'	=> getq_weblink_label_text('addCommentLabel_webl', $lg),
	'submit_txt'	=> getq_weblink_label_text('submit_webl', $lg),
	'cancel_txt'	=> getq_weblink_label_text('cancel_webl', $lg)
));


return $view->fetch();