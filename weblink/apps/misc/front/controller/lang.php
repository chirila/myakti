<?php
  ini_set('display_errors', 1);
    ini_set('error_reporting', 1);
    // error_reporting (E_ALL);
error_reporting(0);
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}

session_start();
include_once (INSTALLPATH . 'config/config.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
//include_once (BASEPATH . 'startup.php');

$_db=new sqldb();

$_db->query("select constant_name, value from settings where constant_name='LANGUAGE_TR'");

$_db->move_next();
define($_db->f('constant_name'),$_db->f('value'));
$language = isset($_SESSION['l']) ? $_SESSION['l'] : LANGUAGE_TR;

header('Content-type:text/javascript');
include("../../../../../lang/lang_".$language.".php");
foreach ($lang as $key => $val){
	$lang[$key] = utf8_encode($val);
}
echo 'var LANG='.json_encode($lang);
