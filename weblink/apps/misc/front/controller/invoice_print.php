<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ark::loadLibraries(array('tcpdf/tcpdf','tcpdf/config/lang/eng'));
$special_template = array(4,5,6); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Salesassist');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// $pdf->setLanguageArray($l);
if(in_array($in['type'], $special_template) || ($in['custom_type'] == 1 && obj::get('db') == '6522a2ec_2904_313e_fd99d29e57b4')){
	if($in['type']==6){
		$pdf->SetMargins(6, 43, 10);
		$pdf->SetAutoPageBreak(TRUE, 10);
	}else{
		$pdf->SetMargins(10, 5, 10);
		$pdf->SetAutoPageBreak(TRUE, 50);
	}
	$pdf->SetFooterMargin(0);
	$pdf->setPrintFooter(false);
	$hide_all = 2;
}

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();

$html = include('invoice_print_body.php');
if($in['print']){
	die($html);
}
$pdf->writeHTML($html, true, false, true, false, '');
#250 vat si discount
#255 numai discount
#267 fara vat si discount
$pdf->SetY($height);
if(in_array($in['type'], $special_template) || ($in['custom_type'] == 1 && obj::get('db') == '6522a2ec_2904_313e_fd99d29e57b4') ){
	$hide_all = 1;
	$pdf->SetAutoPageBreak(TRUE, 0);

	$htmls = include('invoice_print_body.php');

	$pdf->writeHTML($htmls, true, false, true, false, '');
}

$pdf->lastPage();
if($in['do']=='invoice-invoice_print'){
   $pdf->Output($serial_number.'.pdf','I');
}else{
   $pdf->Output('invoice.pdf', 'F');
}
?>