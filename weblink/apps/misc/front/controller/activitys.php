<?php if (!defined('BASEPATH'))	exit('No direct script access allowed');

$now = time();
$pag = $in['pag'];
if($in['old_frame']){
	$pag = $in['pag_old'];
}

$max = $db->field("SELECT count(log_id) FROM logging
						WHERE pag='".$pag."' AND field_name='".$in['field_name']."' AND field_value='".$in['field_value']."' AND type='1'
						ORDER BY date DESC ");

$comments = $db->query("SELECT * FROM logging
						WHERE pag='".$pag."' AND field_name='".$in['field_name']."' AND field_value='".$in['field_value']."' AND type='1'
						ORDER BY date DESC ");
$i=0;

while ($comments->next()) {
	$view->assign(array(
		'name' 			=> get_user_name($comments->f('user_id')),
		'message'		=> nl2br($comments->f('message')),
		'time'			=> time_ago($comments->f('date'),$now),
		'id'			=> $comments->f('log_id'),
		'hide_comment'	=> $i > 2 ? 'hide' : '',
		'field_value' 	=> $in['field_value'],
		'field_name' 	=> $in['field_name'],
		'pag' 			=> $pag,
		'assign_to'		=> $comments->f('to_user_id') ? true : false,
		'to'			=> $comments->f('to_user_id') ? get_user_name($comments->f('to_user_id')) : '',
		'strike'		=> $comments->f('done') ? 'strike' : '',
		'checked'		=> $comments->f('done') ? 'checked' : '',
		'due_date'		=> $comments->f('due_date')? date(ACCOUNT_DATE_FORMAT,$comments->f('due_date')):'',
	),'comments');
	$view->loop('comments');
	$i++;
}

if($i > 3){
	$view->assign(array(
		'comment' 	=> true,
		'count'		=> $max-3,
	));
}

return $view->fetch();
