<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
require_once 'icepay/icepay_api_basic.php';
if(!obj::get('table')){
	$view->assign(array('data'=>false));
	return $view->fetch();
}
$tblinvoice = $db->query("SELECT  ".obj::get('table').".*,multiple_identity.* FROM ".obj::get('table')."
LEFT JOIN multiple_identity ON multiple_identity.identity_id=".obj::get('table').".identity_id
 WHERE ".obj::get('main_field')."='".$in['invoice_id']."'");

if($in['invoice_id'] && $in['do']=='invoice-invoice'){
	$tbl = $db->field("SELECT identity_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
	if($tbl!=0){
		$company_name = $db->field("SELECT company_name FROM multiple_identity WHERE identity_id='".$tbl."'");
		$title=gm('Invoice from');
	}else{
		$company_name = ACCOUNT_COMPANY;
		$title=gm('Invoice from');
	}
}else if($in['quote_id'] && $in['do']=='quote-quote'){
	$tbl = $db->field("SELECT identity_id FROM tblquote WHERE id='".$in['quote_id']."'");
	if($tbl!=0){
		$company_name = $db->field("SELECT company_name FROM multiple_identity WHERE identity_id='".$tbl."'");
		$title=gm('Quote from');
	}else{
		$company_name = ACCOUNT_COMPANY;
		$title=gm('Quote from');
	}
}else if($in['service_id'] && $in['do'] == 'project-service'){
	$tbl = $db->field("SELECT identity_id FROM servicing_support WHERE service_id='".$in['service_id']."'");
	if($tbl!=0){
		$company_name = $db->field("SELECT company_name FROM multiple_identity WHERE identity_id='".$tbl."'");
		$title=gm('Intervention from');
	}else{
		$company_name = ACCOUNT_COMPANY;
		$title=gm('Intervention from');
	}
}


// $tblinvoice = $db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."'");
if(!$tblinvoice->move_next()){
	$view->assign(array('data'=>false));
	return $view->fetch();

}

if($tblinvoice->f('sent') == '0' && obj::get('table') != 'pim_p_orders'){
	$view->assign(array('data'=>false));
	return $view->fetch();
}

$view->assign(array('data'=>true));
$icepay_hide = false;
if((defined('ICEPAY_ACTIVE2') && ICEPAY_ACTIVE2 == 1) && (defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT == 1)){
	$logger = Icepay_Api_Logger::getInstance();
	$logger->enableLogging(false)
	        ->setLoggingLevel(Icepay_Api_Logger::LEVEL_ALL)
	        ->logToFile(true)
	        ->setLoggingDirectory(realpath("icepay/logs"))
	        ->setLoggingFile($database_config['mysql']['database'].'.txt')
	        ->logToScreen(false);
	$country_code = get_country_code($tblinvoice->f('buyer_country_id'));
	if(!$country_code){
		$country_code = 'BE';
	}
	$icepay_id = $db->field("SELECT id FROM icepay_orders WHERE item='".$in['invoice_id']."' AND type='1' ORDER BY `id` DESC LIMIT 1 ");
	if(!$icepay_id){
		$icepay_id = $db->insert("INSERT INTO icepay_orders SET item='".$in['invoice_id']."', type='1' ");
	}
	/* Set the paymentObject */
	$paymentObj = new Icepay_PaymentObject();
	$paymentObj->setAmount(number_format($amount_due, 2, '.', '') *100)
	            ->setCountry($country_code)
	            ->setLanguage("EN")
	            ->setReference($in['q'])
	            ->setDescription($tblinvoice->f('seller_name').' Invoice Id # '.$in['invoice_id'])
	            ->setCurrency(currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code'))
	            ->setOrderID($icepay_id);

    // Merchant Settings
    $basicmode = Icepay_Basicmode::getInstance();
    if($basicmode->setMerchantID(ICEPAY_MERCHANTID2)){
    	if($basicmode->setSecretCode(ICEPAY_SECRETCODE2)){
    		if($basicmode->validatePayment($paymentObj)){

				$view->assign('ice_pay_link',$basicmode->getURL());
				$icepay_hide = true;
    		}
    	}
    }
}else{
	$icepay_hide = false;
}

if((defined('STRIPE_ACTIVE2') && STRIPE_ACTIVE2 == 1) && (defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT == 1)){
	$stripe_show = true;
}else{
	$stripe_show = false;
}

if((defined('MOLLIE_ACTIVE2') && MOLLIE_ACTIVE2 == 1) && (defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT == 1)){
	$mollie_show = true;
}else{
	$mollie_show = false;
}

if(isset($in['quote_id'])){
	$icepay_hide = false;
	$stripe_show = false;
	$mollie_show = false;
}
if(isset($in['p_order_id'])){
	$icepay_hide = false;
	$stripe_show = false;
	$mollie_show = false;
}
$view->assign(array(
	'icepay_hide'	=> $icepay_hide,
	'stripe'		=> $stripe_show,
	'mollie'		=> $mollie_show
  ));
if($tblinvoice->f('pdf_layout')){
	$link_end='&type='.$tblinvoice->f('pdf_layout').'&logo='.$tblinvoice->f('pdf_logo');
}else{
	$link_end = '&type='.obj::get('pdf_format');
}

#if we are using a customer pdf template
if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $tblinvoice->f('pdf_layout') == 0){
	$link_end = '&custom_type='.obj::get('pdf_format');
}

$lg = $db->field("SELECT lang_id FROM pim_lang WHERE active=1 AND default_lang='1'");
if($tblinvoice->f('email_language')){
	$lg = $tblinvoice->f('email_language');
}

$stripe_app_id=$db->field("SELECT app_id FROM apps WHERE name='Stripe' and type='main' AND main_app_id='0' ");

// $img = '../'.ACCOUNT_LOGO;
// $size = getimagesize($img);

// $attr = 'width="200"';
// if($size[1] >80 || $size[0] >200){
// 	if($size[1] >= $size[0]){
// 		$attr = 'height="80"';
// 	}else{
// 		if($size[1] > 80){
// 			$attr = 'height="80"';
// 		}else{
// 			$attr = 'width="200"';
// 		}

// 	}
// }

$img = '../images/no-logo.png';
	$attr = '';


	if($in['logo'])
	{
		/*switch($in['logo']){
		case 1: $print_logo=ACCOUNT_LOGO;break;
		case 3: $print_logo=ACCOUNT_LOGO_QUOTE;break;
		case 2: $print_logo=ACCOUNT_LOGO_ORDER;break;*/
		$print_logo = $in['logo'];
		// }
	}else {

		$print_logo=ACCOUNT_LOGO;
	}
	if($tblinvoice->f('company_logo')){
		$print_logo_idnt=$tblinvoice->f('company_logo');
	}
	if($print_logo){
		$img = '../'.$print_logo;
		if($print_logo_idnt){
			$img='../'.$print_logo_idnt;
		}
		$size = getimagesize($img);
		if($size === false){
			$img=ACCOUNT_LOGO ? '../'.ACCOUNT_LOGO :'../images/no-logo.png';
			$size = getimagesize($img);
		}
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}
	// var_dump($img);
	// exit();


$view->assign(array( 'pdf_link'	=> 'index.php?q='.$in['q'].'&do='.obj::get('app').'-'.obj::get('print').'&id='.$in['invoice_id'].'&lid='.$lg.$link_end));

$view->assign(array(
	'title'						=> $title,
	'company_name'				=> $company_name,
	'logo'							=> $img,
	'attr'							=> $attr,
	'accept_link'				=> 'index.php?q='.$in['q'].'&do=quote-quote-quote-status&status_customer=2',
	'reject_link'				=> 'index.php?q='.$in['q'].'&do=quote-quote-quote-status&status_customer=1',
	'download_txt'				=>  getq_weblink_label_text('download_webl', $lg),
	'print_pdf_txt'				=> 	getq_weblink_label_text('pdfPrint_webl', $lg),
	'payWithIcepay_txt'			=> 	getq_weblink_label_text('payWithIcepay_webl', $lg),
	'project'								=> obj::get('app') == 'project' ? true : false,
	'stripe_locale'				=> $db->field("SELECT code FROM lang WHERE lang_id='".$lg."' "),
	'stripe_pub_key'				=> $db->field("SELECT api FROM apps WHERE type='publish_key' AND main_app_id='".$stripe_app_id."' "),
	'account_name'				=> ACCOUNT_COMPANY,
	'serial_number'				=> $tblinvoice->f('serial_number'),
));

return $view->fetch();