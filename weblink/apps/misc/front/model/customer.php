<?php
class customer
{
	var $db;								# holds the database connection
	var $db_users;							# holds the database connection

	function customer()
	{
		$this->db = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_user = new sqldb($db_config);
		$this->db_users = new sqldb($db_config);
		$this->db_users2 = new sqldb($db_config);
	}

	/**
	 * Delete the selected logging entrie
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function delete_logging(&$in){
		$this->db->query("DELETE FROM logging WHERE date='".$in['date']."'");
		return true;
	}

	/**
	 * Add activity for contacts
	 *
	 * @param array $in
	 * @return true
	 * @author Marius Pop
	 **/
	function add_activity(&$in){
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['type_a']."',
																	contact_activity_note='".$in['comment']."',
																	date='".time()."',
																	subject='".$in['subject']."',
																	customer_id='".$in['customer_id']."',
																	contact_id ='".$in['contact_act_id']."',
																	email_to='".$_SESSION['u_id']."' ");
		if($in['is_reminder'] || $in['user_to_id'] ){
			$user_id = $_SESSION['u_id'];
			if($in['user_to_id']){
				$user_id = $in['user_to_id'];
			}
			$this->db->query("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$_SESSION['u_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$in['reminder_date']."',
														log_comment='".$in['subject']."',
														message='".$in['comment']."' ");
		}
		msg::$success = gm("Activity added").".";
		return true;
	}

	/**
	 * Add activity for companies
	 *
	 * @return void
	 * @param array $in
	 * @author Marius Pop
	 **/
	function add_company_activity(&$in)
	{
		$in['use_customer'] = true;
		if($in['contact_act_id']){
			$in['contact_id'] = $in['contact_act_id'];
			$in['use_customer'] = false;
		}
		return $this->add_activity($in);
	}

	/**
	 * delete actiivity
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function delete_activity(&$in)
	{
		$this->db->query("DELETE FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['id']."' ");
		msg::$success = gm('Activity deleted');
		return true;
	}

	/**
	 * update an activity
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function update_company_activity(&$in)
	{
		$v =  new validation($in);
		$v->f('type_a',"type_a","required");
		if($v->run() === false){

			return false;
		}
		$field = 'contact_id';
		$value = $in['contact_act_id'];
		if($in['use_customer']){
			$field = 'customer_id';
			$value = $in['customer_id'];
		}
		$this->db->query("UPDATE customer_contact_activity SET contact_activity_type='".$in['type_a']."',
																contact_activity_note='".$in['note_a']."',
																date='".time()."',
																subject='".$in['subject']."',
																".$field."='".$value."'
															WHERE customer_contact_activity_id='".$in['customer_contact_activity_id']."' ");
		if($in['is_reminder']){
			$this->db->query("UPDATE customer_contact_activity SET reminder_date = '".$in['reminder_date']."', email_to='".$_SESSION['u_id']."' WHERE customer_contact_activity_id='".$in['customer_contact_activity_id']."' ");
		}
		$in['customer_contact_activity_id'] =null;
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function insert_comment(&$in)
	{
		if(ark::$model=='customer'){
			if(!$in['name']){
				return false;
			}
			if(!$in['email']){
				return false;
			}
		}
		if(!$in['comment']){
			return false;
		}
		$in['comment2'] = $in['comment'];
		$in['comment'] = $in['name']." <a href=\"mailto:".$in['email']."\" >".$in['email']."</a><br>".$in['comment'];
		$in['log_id'] = insert_message_log($in['pag'],$in['comment'],$in['field_name'],$in['field_value'],1,$in['user_id']);
		if(obj::get('app') == 'project'){
			$user_id = $this->db->field("SELECT user_id FROM ".obj::get('table')." WHERE service_id='".$in['field_value']."' ");
		}else if (obj::get('app') == 'invoice') {
			$user_id = $this->db->field("SELECT created_by FROM ".obj::get('table')." WHERE id='".$in['field_value']."' ");
		}else if (obj::get('app') == 'order') {
			return true;
		}
		else{
			$user_id = $this->db->field("SELECT author_id FROM ".obj::get('table')." WHERE id='".$in['field_value']."' ");
		}
		if($in['status_customer']){
			ark::run('quote-quote-quote-status');
		}
		if(obj::get('app') == 'quote'){
			//add notification
			$notification_users=array();
			$quote_users = $this->db->query("SELECT author_id,acc_manager_id FROM ".obj::get('table')." WHERE id='".$in['field_value']."' ");

			$quote_users->next();

			if($quote_users->f('author_id') && !in_array($quote_users->f('author_id'), $notification_users)){
				$notification_users[]=$quote_users->f('author_id');
			}
			if($quote_users->f('acc_manager_id') && !in_array($quote_users->f('acc_manager_id'), $notification_users)){
				$notification_users[]=$quote_users->f('acc_manager_id');
			}
			if(!empty($notification_users)){
				add_notification(obj::get('db'),'quote',$in['field_value'],$notification_users,'version-won.'.obj::get('version_id'));
			}
			//end add notification
			$this->send_email($in);
			$this->db->query("UPDATE logging SET to_user_id='".$user_id."', log_comment='New task' WHERE log_id='".$in['log_id']."' ");
			$this->send_email_customer($in);
		}else{
			#do somehting
			$this->db->query("UPDATE logging SET to_user_id='".$user_id."', log_comment='New task' WHERE log_id='".$in['log_id']."' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$user_id."' AND name='send_email_type' ");
			if($this->db_users->next()){
				$send_email_type = $this->db_users->f('value');
				$send_email_type = explode(',',$send_email_type);
				if($send_email_type[0] == '1'){
					$this->send_notification_email($in);
				}
				$today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
				$today_end = mktime( 23, 59, 59, date('m'), date('d'), date('Y'));
				$due_date = $in['comment_due_date'];
				if($send_email_type[0] != '1' && $send_email_type[1] == '2' && $due_date > $today && $due_date < $today_end){
					$this->send_notification_email($in);
				}
			}
		}

		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function send_email_customer(&$in){
		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		$stuff = $this->db->query("SELECT * FROM tblquote WHERE id=".obj::get('id'));
    	$log = $this->db->query("SELECT * FROM logging WHERE log_id='".$in['log_id']."' ");
    	$author_id = $this->db->field("SELECT author_id FROM ".obj::get('table')." WHERE id='".obj::get('id')."' ");
    	$user_email = $this->db_users->query("SELECT * FROM users WHERE user_id='".$author_id."' ");
		$user_email->next();

		global $config;
		$link = '<a href="'.$config['admin_site_url'].'quote/view/'.$log->f('field_value').'">Your quote</a>';

		#Actions
		#1 #accepted
		#2 #new version
		#3 #rejected
		#4 #commentary

		$weblink_message_name = '';

		if($in['new_version'] == 1){
			$action = 2;
			$weblink_message_name = 'quowebnvermess';
		}
		if($in['status_customer'] == 2){
			$action = 1;#accepted
			$weblink_message_name = 'quowebaccmess';
		}
		if($in['status_customer'] == 1){
			$action = 3;#rejected
			$weblink_message_name = 'quowebrefmess';
		}
		if($in['commentary_email'] == 1){
			$action = 4;#commentary
			$weblink_message_name = 'quowebcommmess';
		}

		$set_default_message = false;

		//Get weblink messages set from AKTI
		$message_data=get_weblink_sys_message($weblink_message_name,$stuff->f('email_language'));
		if(!empty($message_data)){
			$set_default_message = true;
		}

		$email_subject = '';
		$email_body = '';

		if(!$action){
			return;
		}
	    $mail = new PHPMailer();
	    $mail->WordWrap = 50;
	    $weblink = '<a href="'.$config['site_url'].'?q='.$in['q'].'">'.$stuff->f('serial_number').'</a>';
	
	    if($set_default_message){

			$email_subject = $message_data['subject'];	

			$email_subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $email_subject );
			$email_subject=str_replace('[!SERIAL_NUMBER!]',$stuff->f('serial_number'), $email_subject);
			$email_subject=str_replace('[!CUSTOMER!]',$in['name'], $email_subject);
			$email_subject=str_replace('[!CONTACT_FIRST_NAME!]',htmlspecialchars_decode(stripslashes($user_email->f('first_name'))), $email_subject);
			$email_subject=str_replace('[!CONTACT_LAST_NAME!]',htmlspecialchars_decode(stripslashes($user_email->f('last_name'))), $email_subject);

			$email_body= $message_data['text'];
			$email_body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $email_body );
			$email_body=str_replace('[!SERIAL_NUMBER!]',$stuff->f('serial_number'), $email_body);
			$email_body=str_replace('[!CUSTOMER!]',$in['name'], $email_body);
			$email_body=str_replace('[!CONTACT_FIRST_NAME!]',htmlspecialchars_decode(stripslashes($user_email->f('first_name'))), $email_body);
			$email_body=str_replace('[!CONTACT_LAST_NAME!]',htmlspecialchars_decode(stripslashes($user_email->f('last_name'))), $email_body);
			$email_body=str_replace('[!WEB_LINK!]',$weblink, $email_body);
			
		} else {

			 # dutch
	    $subject[1][3] = 'Offerte '.$stuff->f('serial_number').' van '.ACCOUNT_COMPANY.' is aanvaard';
	    $text[1][3]='Beste '.$in['name'].',

Bedankt voor uw reactie op onze offerte '.$weblink.'. We zijn verheugd om te zien dat u onze offerte heeft aanvaard, en kunnen niet wachten om aan onze samenwerking te beginnen!

Als u nog vragen of feedback heeft, aarzel aub niet om ons te contacteren. We zullen binnenkort contact met u opnemen om onze deal te bevestigen.

Met vriendelijke groet,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[2][3] = 'Offerte '.$stuff->f('serial_number').' van '.ACCOUNT_COMPANY.' nieuwe versie';
		$text[2][3]='Beste '.$in['name'].',

Bedankt voor uw reactiee op onze offerte '.$weblink.'. We hebben begrepen dat onze huidige offerte niet voldoet aan uw verwachtingen, en dat u graag een nieuwe versie van ons wilt ontvangen. We zullen hier zo snel mogelijk voor zorgen!

Indien de informatie die we op uw offerte gebruikt hebben niet correct is, gelieve ons dit te laten weten. We maken dit dan meteen in orde.

Met vriendelijke groet,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[3][3] = 'Offerte '.$stuff->f('serial_number').' van '.ACCOUNT_COMPANY.' is geweigerd';
		$text[3][3]='Beste '.$in['name'].',

Bedankt voor uw reactie op onze offerte '.$weblink.'. Helaas blijkt deze offerte niet aan uw eisen voldaan te hebben, en heeft u ze moeten weigeren. Als u verder nog feedback heeft over onze aanbieding, horen we dat graag!

Indien u graag een nieuwe versie van deze offerte ontvangt, bezorgen we die u graag op simpel verzoek. We blijven hoopvol over een samenwerking in de toekomst.

Met vriendelijke groet,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;

		$subject[4][3] = ' Feedback op offerte '.$stuff->f('serial_number');
	    $text[4][3]='Beste '.$in['name'].', 

Bedankt voor uw feedback aangaande onze offerte '.$weblink.'. Wij komen hier zo snel mogelijk op terug.

Met vriendelijke groeten,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;

		# english
		$subject[1][1] = 'Quote '.$stuff->f('serial_number').' from '.ACCOUNT_COMPANY.' is accepted';
		$text[1][1]='Dear '.$in['name'].',

thank you for your reaction to our quote proposal '.$weblink.'. We are delighted to see that you have accepted our quote, and are eager to start our collaboration!
If you have any more questions or feedback, please don’t hesitate to reach out.
We will contact you soon to confirm our deal.

Kind regards,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[2][1] = 'Quote '.$stuff->f('serial_number').' from '.ACCOUNT_COMPANY.' new version';
		$text[2][1]='Dear '.$in['name'].',

thank you for your reaction to our quote proposal '.$weblink.'. We understand that the current quote does not match your expectations, and that you would like us to make a new proposal. We will get on it as soon as possible!
In case the information we used on our quote is incorrect, please let us know and we’ll correct it immediately.

Kind regards,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[3][1] = 'Quote '.$stuff->f('serial_number').' from '.ACCOUNT_COMPANY.' is refused';
		$text[3][1]='Dear '.$in['name'].',

thank you for your reaction to our quote proposal '.$weblink.'. Unfortunately, it seems that our quote has not met your standards, and you had to refuse our proposal. If you have any comments you want to share with us, we would be most grateful to hear them.
If you would like us to make a new version of this quote, please let us know. In the mean while, we remain hopeful for a collaboration in the future.

Kind regards,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;

		$subject[4][1] = 'Feedback on '.$stuff->f('serial_number');
		$text[4][1]='Dear '.$in['name'].',

		Thank you for your feedback regarding our quote '.$weblink.'. We will get back to you as soon as possible.

		Best regards, 

		'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
		'.ACCOUNT_COMPANY;
		

		# french
		$subject[1][2] = 'Offre '.$stuff->f('serial_number').' de '.ACCOUNT_COMPANY.' est acceptée';
		$text[1][2]='Cher '.$in['name'].',

Nous vous remercions de votre réaction à notre offre '.$weblink.'. Nous sommes ravis que vous avez acceptez notre offre, et impatients d\'entamer notre collaboration !
Surtout n\'hésitez pas à nous contacter si vous avez des questions ou des suggestions.
Nous vous contacterons très bientôt pour confirmer le tout.

Bien à vous,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[2][2] = 'Offre '.$stuff->f('serial_number').' de '.ACCOUNT_COMPANY.' nouvelle version';
		$text[2][2]='Cher '.$in['name'].',

Nous vous remercions de votre réaction à notre offre '.$weblink.'. Nous comprenons que l\'offre actuelle ne correspond pas à vos attentes, et que vous souhaitez nous faire une nouvelle proposition. Nous répondons à cette proposition au plus vite !
Si les données que nous avons utilisées sur notre offre sont incorrectes, merci de nous le signaler. Nous les corrigerons immédiatement.

Bien à vous,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[3][2] = 'Offre '.$stuff->f('serial_number').' de '.ACCOUNT_COMPANY.' est refusée';
		$text[3][2]='Cher '.$in['name'].',

Nous vous remercions de votre réaction à notre offre '.$weblink.'. Malheureusement, il semble que notre offre n\'a pas répondu à vos attentes, et vous avez dû la refuser. Surtout n\'hésitez pas à nous transmettre vos commentaires ou suggestions.
Veuillez nous indiquer si vous souhaitez une nouvelle version de cette offre. Dans l\'espoir d\'une collaboration fructueuse, nous restons à votre service pour toute question.

Bien à vous,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;

		$subject[4][2] = 'Commentaires sur '.$stuff->f('serial_number');
		$text[4][2]='Bonjour '.$in['name'].',
Merci pour vos commentaires concernant notre devis '.$weblink.'. Nous vous reviendrons le plus vite possible. 

Bien à vous,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;

		# german;
		$subject[1][4] = 'Quote '.$stuff->f('serial_number').' from '.ACCOUNT_COMPANY.' is accepted';
		$text[1][4]='Dear '.$in['name'].',

thank you for your reaction to our quote proposal '.$weblink.'. We are delighted to see that you have accepted our quote, and are eager to start our collaboration!
If you have any more questions or feedback, please don’t hesitate to reach out.
We will contact you soon to confirm our deal.

Kind regards,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[2][4] = 'Quote '.$stuff->f('serial_number').' from '.ACCOUNT_COMPANY.' new version';
		$text[2][4]='Dear '.$in['name'].',

thank you for your reaction to our quote proposal '.$weblink.'. We understand that the current quote does not match your expectations, and that you would like us to make a new proposal. We will get on it as soon as possible!
In case the information we used on our quote is incorrect, please let us know and we’ll correct it immediately.

Kind regards,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;
		$subject[3][4] = 'Quote '.$stuff->f('serial_number').' from '.ACCOUNT_COMPANY.' is refused';
		$text[3][4]='Dear '.$in['name'].',

thank you for your reaction to our quote proposal '.$weblink.'. Unfortunately, it seems that our quote has not met your standards, and you had to refuse our proposal. If you have any comments you want to share with us, we would be most grateful to hear them.
If you would like us to make a new version of this quote, please let us know. In the mean while, we remain hopeful for a collaboration in the future.

Kind regards,

'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
'.ACCOUNT_COMPANY;

		$subject[4][4] = 'Feedback on '.$stuff->f('serial_number');
		$text[4][4]='Dear '.$in['name'].',

		Thank you for your feedback regarding our quote '.$weblink.'. We will get back to you as soon as possible.

		Best regards, 

		'.htmlspecialchars_decode(stripslashes($user_email->f('last_name').' '.$user_email->f('first_name'))).'
		'.ACCOUNT_COMPANY;

		$email_subject = $subject[$action][$stuff->f('email_language')];
		$email_body = $text[$action][$stuff->f('email_language')];

		}
   

    $body =
    '<table style="background:#f2f8fa; width:100%; margin:0; padding:30px; " border="0" cellspacing="0" cellpadding="50">
	<tbody>
		<tr><td style="border-collapse:collapse; ">
			<table style="margin:0 auto; padding:0; background:#FFFFFF;" width="600" border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr style="background:#FFF;"><td style="padding: 20px; max-width: 153px;"><img src="https://my.akti.com/images/akti-logo.png"></td></tr>
					<tr style=""><td style="width=100%; padding:20px; border-bottom:1px solid #c3d3dd; border-right:1px solid #c3d3dd; border-left:1px solid #c3d3dd; border-collapse: collapse;">
					'.$email_body.'
					</td></tr>
				</tbody>
			</table>
		</td></tr>
	</tbody>
</table>';

		$def_email = $this->default_email();

        /*$mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
        $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
        $mail->set('Sender',$def_email['reply']['email']);*/

        $mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='quote_email_type' ");
    	
    	if($mail_sender_type == 1 || $mail_sender_type == 3) {
    		if($mail_sender_type == 1) {
    			
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
    			
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

    				$mail->SetFrom($this->db_users->f('email'), utf8_decode($mail_name ),0);
    			
    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
    				
    				$mail->set('Sender',$this->db_users->f('email'));
    				
    			}else{
    				$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
    			
    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
    			
    				$mail->set('Sender',$def_email['reply']['email']);
    			}		
    		} else {
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$author."' ");
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

    				$mail->SetFrom($this->db_users->f('email'), utf8_decode( $mail_name),0);

    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));

    				$mail->set('Sender',$this->db_users->f('email'));
    			}else{
    				$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
    			
    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));
    			
    				$mail->set('Sender',$def_email['reply']['email']);
    			}		
    		}
    	}else{
    		$mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']),0);
    		$mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
    		$mail->set('Sender',$def_email['reply']['email']);
    	}

    	$mail->CharSet = 'UTF-8';
        $mail->Subject = $email_subject;

        $mail->MsgHTML(nl2br(($body)));
        // console::log($from_user->f('email'),$user_email->f('email'));
        $mail->AddAddress($in['email']);

   		$mail->Send();

		return true;
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function send_email(&$in){
		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		$created= $this->db->field("SELECT created_by FROM ".obj::get('table')." WHERE id='".$in['field_value']."' ");
		$author_id = $this->db->field("SELECT author_id FROM ".obj::get('table')." WHERE id='".$in['field_value']."' ");
		$stuff = $this->db->query("SELECT * FROM ".obj::get('table')." WHERE id='".$in['field_value']."' ");

    $log = $this->db->query("SELECT * FROM logging WHERE log_id='".$in['log_id']."' ");
		$log->next();
		global $config;
		$link = '<a href="'.$config['admin_site_url'].'quote/view/'.$log->f('field_value').'">Your quote</a>';
		$action = 0;
		if($in['new_version'] == 1){
			$action = 2;
		}
		if($in['status_customer'] == 2){
			$action = 1;
		}
		if($in['status_customer'] == 1){
			$action = 3;
		}


		$from_user = $this->db_users->query("SELECT * FROM users WHERE user_id='".$created."' ");
		$from_user->next();
		$user_email = $this->db_users->query("SELECT * FROM users WHERE user_id='".$author_id."' ");
		$user_email->next();
    $mail = new PHPMailer();

    $mail->WordWrap = 50;
    //$weblink = '<a href="'.$config['site_url'].'?q='.$in['q'].'">'.$config['site_url'].'?q='.$in['q'].'</a>';
    $weblink = '<a href="'.$config['admin_site_url'].'quote/view/'.$log->f('field_value').'">'.$config['admin_site_url'].'quote/view/'.$log->f('field_value').'</a>';
    # dutch
    $subject[0][1] = 'Offerte '.$stuff->f('serial_number').' van '.$stuff->f('buyer_name');
    $subject[1][1] = 'Offerte '.$stuff->f('serial_number').' aanvaard door '.$stuff->f('buyer_name');
    $subject[2][1] = 'Offerte '.$stuff->f('serial_number').' van '.$stuff->f('buyer_name').' nieuwe versie';
    $subject[3][1] = 'Offerte '.$stuff->f('serial_number').' van '.$stuff->f('buyer_name').' is geweigerd';
    $text[0][1]='Hallo '.ACCOUNT_COMPANY.',

Uw potentiële klant '.$stuff->f('buyer_name').' heeft uw offerte '.$stuff->f('serial_number').' bestudeerd, en heeft volgende opmerkingen toegevoegd:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

Uw klant heeft deze offerte voorlopig nog niet aanvaard of geweigerd. De offerte blijft de status ‘hangende’ behouden, tot u of uw klant verdere actie ondernemen.

Een nieuwe versie van deze offerte maken kan hier! (link: '.$link.')

Veel succes,

Het Akti Team';
/*		$text[1][1]='Hallo '.ACCOUNT_COMPANY.',

Goed nieuws! Uw potentiële klant '.$stuff->f('buyer_name').' is nu een échte klant! Ze hebben uw offerte '.$stuff->f('serial_number').' goedgekeurd, en hebben volgende opmerkingen toegevoegd:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

En nu, aan de slag!

'.$weblink.'

U kan uw offerte '.$stuff->f('serial_number').' meteen omvormen tot een nieuw project, en later tot een factuur! Maar eerst: drie hoeraatjes voor deze nieuwe klant!

Veel succes,

Het Akti Team';*/
		$text[1][1]='Dag '.ACCOUNT_COMPANY.',

Uw offerte '.$stuff->f('serial_number').' werd zojuist aanvaard door '.$stuff->f('buyer_name').'.

'.$weblink.'

Ga meteen aan de slag in Akti en zet de volgende stap in uw verkoopsproces.

Veel succes,

Het Akti Team';
		$text[2][1]='Hallo '.ACCOUNT_COMPANY.',

Uw potentiële klant '.$stuff->f('buyer_name').' heeft uw offerte '.$stuff->f('serial_number').' bestudeerd, en vraagt u om een nieuwe versie te maken. Ze voegden er deze opmerkingen aan toe:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

U kan dus meteen aan de slag om een nieuwe versie van deze offerte te maken! (link: '.$link.')

Veel succes,

Het Akti Team';
		$text[3][1]='Hallo '.ACCOUNT_COMPANY.',

Uw potentiële klant '.$stuff->f('buyer_name').' heeft uw offerte '.$stuff->f('serial_number').' bestudeerd, maar heeft deze helaas geweigerd. Ze voegden er deze opmerkingen aan toe:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

Maar blijf vooral niet bij de pakken zitten! We zijn er zeker van dat u meer geluk zal hebben bij uw volgende offerte! (link: '.$link.')

Veel succes,

Het Akti Team';
		# english
		$subject[0][2] = 'Quote '.$stuff->f('serial_number').' from '.$stuff->f('buyer_name');
		$subject[1][2] = 'Quote '.$stuff->f('serial_number').' accepted by '.$stuff->f('buyer_name');
		$subject[2][2] = 'Quote '.$stuff->f('serial_number').' from '.$stuff->f('buyer_name').' new version';
		$subject[3][2] = 'Quote '.$stuff->f('serial_number').' from '.$stuff->f('buyer_name').' is refused';
		$text[0][2]='Hi '.ACCOUNT_COMPANY.',

Your potential client '.$stuff->f('buyer_name').' has reviewed your quote '.$stuff->f('serial_number').', and added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

Please note that your quote is still pending, and has not yet been accepter nor rejected.

You can make a new version of your quote here: (link: '.$link.')

Good luck,

Akti Team';
/*		$text[1][2]='Hi '.ACCOUNT_COMPANY.',

Good news! Your potential client '.$stuff->f('buyer_name').' is now a real customer! They accepted your quote '.$stuff->f('serial_number').', and added this comment:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

Time to go to work!

'.$link.'

You can immediately transform quote '.$stuff->f('serial_number').' into a project, and later into an invoice! But first, celebrate your new customer!


Akti Team' ;*/
		$text[1][2]='Hi '.ACCOUNT_COMPANY.',

Your quote '.$stuff->f('serial_number').' was accepted by '.$stuff->f('buyer_name').'.

'.$link.'

Go to Akti and take the next steps in your sales proces.

Good luck,

The Akti Team';


		$text[2][2]='Hi '.ACCOUNT_COMPANY.',

Your potential client '.$stuff->f('buyer_name').' has reviewed your quote '.$stuff->f('serial_number').', and would like you to make a new version. They added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

So don’t hesitate and make a new version of your quote here: (link: '.$link.')

Good luck,

Akti Team';
		$text[3][2]='Hi '.ACCOUNT_COMPANY.',

After careful consideration, your potential client '.$stuff->f('buyer_name').' has unfortunately decided to reject your quote '.$stuff->f('serial_number').'. They added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

But don’t give up now! We’re sure you’ll have better luck with your next quote! (link: '.$link.')

Good luck,

Akti Team';
		# french
		$subject[0][3] = 'Offre '.$stuff->f('serial_number').' de '.$stuff->f('buyer_name');
		$subject[1][3] = 'Devis '.$stuff->f('serial_number').' accepté par '.$stuff->f('buyer_name');
		$subject[2][3] = 'Offre '.$stuff->f('serial_number').' de '.$stuff->f('buyer_name').' nouvelle version';
		$subject[3][3] = 'Offre '.$stuff->f('serial_number').' de '.$stuff->f('buyer_name').' est refusée';
		$text[0][3]='Hi '.ACCOUNT_COMPANY.',

Your potential client '.$stuff->f('buyer_name').' has reviewed your quote '.$stuff->f('serial_number').', and added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

Please note that your quote is still pending, and has not yet been accepter OR rejected.

You can make a new version of your quote here: (link: '.$link.')

Good luck,

Akti Team';
/*		$text[1][3]='Hi '.ACCOUNT_COMPANY.',

Good news! Your potential client '.$stuff->f('buyer_name').' has accepted your quote '.$stuff->f('serial_number').', and added this comment:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

Time to go to work!

'.$link.'

You can immediately transform the quote into a project, and later an invoice! But first, celebrate your new customer!



Akti Team';*/
		$text[1][3]="Bonjour ".ACCOUNT_COMPANY.",

Votre devis ".$stuff->f('serial_number')." vient d'être accepté par ".$stuff->f('buyer_name').".

".$link."

Ouvrez Akti et faites le prochain pas dans votre processus de vente. 

Bonne chance,

L'équipe Akti";
		$text[2][3]='Hi '.ACCOUNT_COMPANY.',

Your potential client '.$stuff->f('buyer_name').' has reviewed your quote '.$stuff->f('serial_number').', and would like you to make a new version. They added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

You can make a new version of your quote here: (link: '.$link.')

Good luck,

Akti Team';
		$text[3][3]='Hi '.ACCOUNT_COMPANY.',

After careful consideration, your potential client '.$stuff->f('buyer_name').' has unfortunately decided to reject your quote '.$stuff->f('serial_number').'. They added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

But don’t give up now! We’re sure you’ll have better luck with your next quote! (link: '.$link.')

Good luck,

Akti Team';
		# german;
		$subject[0][4] = 'Quote '.$stuff->f('serial_number').' from '.$stuff->f('buyer_name');
		$subject[1][4] = 'Quote '.$stuff->f('serial_number').' accepted by '.$stuff->f('buyer_name');
		$subject[2][4] = 'Quote '.$stuff->f('serial_number').' from '.$stuff->f('buyer_name').' new version';
		$subject[3][4] = 'Quote '.$stuff->f('serial_number').' from '.$stuff->f('buyer_name').' is refused';
		$text[0][4]='Hi '.ACCOUNT_COMPANY.',

Your potential client '.$stuff->f('buyer_name').' has reviewed your quote '.$stuff->f('serial_number').', and added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

Please note that your quote is still pending, and has not yet been accepter OR rejected.

You can make a new version of your quote here: (link: '.$link.')

Good luck,

Akti Team';
		$text[1][4]='Hi '.ACCOUNT_COMPANY.',

Your quote '.$stuff->f('serial_number').' was accepted by '.$stuff->f('buyer_name').'.

'.$link.'

Go to Akti and take the next steps in your sales proces.

Good luck,

The Akti Team';
		$text[2][4]='Hi '.ACCOUNT_COMPANY.',

Your potential client '.$stuff->f('buyer_name').' has reviewed your quote '.$stuff->f('serial_number').', and would like you to make a new version. They added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

You can make a new version of your quote here: (link: '.$link.')

Good luck,

Akti Team';
		$text[3][4]='Hi '.ACCOUNT_COMPANY.',

After careful consideration, your potential client '.$stuff->f('buyer_name').' has unfortunately decided to reject your quote '.$stuff->f('serial_number').'. They added these comments:

'.$in['name'].' - '.$in['email'].'
'.$in['comment2'].'

'.$weblink.'

But don’t give up now! We’re sure you’ll have better luck with your next quote! (link: '.$link.')

Good luck,

Akti Team';

    $body =
    '<table style="background:#f2f8fa; width:100%; margin:0; padding:30px; " border="0" cellspacing="0" cellpadding="50">
	<tbody>
		<tr><td style="border-collapse:collapse; ">
			<table style="margin:0 auto; padding:0; background:#FFFFFF;" width="600" border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr style="background:#FFF;"><td style="padding: 20px; max-width: 153px;"><img src="https://my.akti.com/images/akti-logo.png"></td></tr>
					<tr style=""><td style="width=100%; padding:20px; border-bottom:1px solid #c3d3dd; border-right:1px solid #c3d3dd; border-left:1px solid #c3d3dd; border-collapse: collapse;">
					'.$text[$action][$from_user->f('lang_id')].'
					</td></tr>
				</tbody>
			</table>
		</td></tr>
	</tbody>
</table>';

  /*  $body='Dear User,

You have received this internal message:
-------------------------------------------------------------------
<table width="100%"><tbody><tr><td width="10%" style="text-align:right">From :</td><td>&nbsp;&nbsp;'.$in['name'].'</td></tr><tr><td style="text-align:right">Subject :</td><td>&nbsp;&nbsp;New task</td></tr><tr><td style="text-align:right">Date :</td><td>&nbsp;&nbsp;'.date(ACCOUNT_DATE_FORMAT,$log->f('date')).'</td></tr><tr><td style="text-align:right">To :</td><td>&nbsp;&nbsp;'.utf8_encode($user_email->f('first_name')).' '.utf8_encode($user_email->f('last_name')).'</td></tr><tr><td style="text-align:right">Item :</td><td>&nbsp;&nbsp;'.$link.'</td></tr></tboby></table>


'.$in['comment2'].'

-------------------------------------------------------------------

Best regards,
SalesAssist Customer Care
<a href="http://saleassist.eu" >www.saleassist.eu</a>';
*/
		$def_email = $this->default_email();

       /* $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
        $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
        $mail->set('Sender',$def_email['reply']['email']);*/


        $mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='quote_email_type' ");
    	
    	if($mail_sender_type == 1 || $mail_sender_type == 3) {
    		if($mail_sender_type == 1) {
    			
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
    			
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

    				$mail->SetFrom($this->db_users->f('email'), utf8_decode($mail_name ),0);
    			
    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
    				
    				$mail->set('Sender',$this->db_users->f('email'));
    				
    			}else{
    				$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
    			
    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
    			
    				$mail->set('Sender',$def_email['reply']['email']);
    			}		
    		} else {
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$author."' ");
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

    				$mail->SetFrom($this->db_users->f('email'), utf8_decode( $mail_name),0);

    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));

    				$mail->set('Sender',$this->db_users->f('email'));
    			}else{
    				$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
    			
    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));
    			
    				$mail->set('Sender',$def_email['reply']['email']);
    			}		
    		}
    	}else{
    		$mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']),0);
    		$mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
    		$mail->set('Sender',$def_email['reply']['email']);
    	}

        // $subject="New internal message";
    	$mail->CharSet = 'UTF-8';
        $mail->Subject = '=?utf-8?B?'.base64_encode($subject[$action][$from_user->f('lang_id')]).'?=';

        $mail->MsgHTML(nl2br(($body)));
        // console::log($from_user->f('email'),$user_email->f('email'));
        $mail->AddAddress($from_user->f('email'));
        if($user_email->f('email')){
        	$mail->AddAddress($user_email->f('email'));
        }

		$sent_date= time();
    	$this->db->query("UPDATE logging SET sent='1' WHERE log_id='".$in['log_id']."' ");

   		$mail->Send();
   		// console::log($mail);
		// msg::$success =gm('Invoice has been successfully sent');
		// $in['act'] = '';
		// $in['do']='invoice-invoice';
		// ark::run('invoice-invoice');
		return true;
	}

	function toggle_done(&$in)
	{
		$this->db->query("UPDATE logging SET done='".$in['val']."' WHERE log_id='".$in['log_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function log_comment(&$in)
	{
		$this->db->query("UPDATE customer_contact_activity SET log_comment='".$in['log_comment']."' WHERE customer_contact_activity_id='".$in['log_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function delete_comment(&$in)
	{
		$this->db->query("DELETE FROM logging WHERE log_id='".$in['log_id']."' ");
		return true;
	}

	function send_notification_email(&$in)
	{
		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		// require_once ('../../classes/class.phpmailer.php');
        // include_once ("../../classes/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

        $log = $this->db->query("SELECT * FROM logging WHERE log_id='".$in['log_id']."' ");
		$log->next();
		global $config;
		switch ($log->f('pag')) {
			case 'order':
				$link = '<a href="'.$config['admin_site_url'].'order/view/'.$log->f('field_value').'">order</a>';
				break;
			case 'invoice':
				$link = '<a href="'.$config['admin_site_url'].'invoice/view/'.$log->f('field_value').'">invoice</a>';
				break;
			case 'quote':
				$link = '<a href="'.$config['admin_site_url'].'quote/view/'.$log->f('field_value').'">quote</a>';
				break;
			case 'project':
				$link = '<a href="'.$config['admin_site_url'].'project/edit/'.$log->f('field_value').'">project</a>';
				break;
			case 'customer':
				$front_register  = $this->db->field("SELECT front_register FROM customers WHERE customer_id='".$log->f('field_value')."'");
				if($front_register==1){
					$link = '<a href="'.$config['admin_site_url'].'customerView/'.$log->f('field_value').'&front_register=1">customer</a>';
				}else{
					$link = '<a href="'.$config['admin_site_url'].'customerView/'.$log->f('field_value').'">customer</a>';
				}
				break;
			case 'xcustomer_contact':
				$front_register  = $this->db->field("SELECT front_register FROM customer_contacts WHERE contact_id='".$log->f('field_value')."'");
				if($front_register==1){
					$link = '<a href="'.$config['admin_site_url'].'contactView/'.$log->f('field_value').'&front_register=1">contact</a>';
				}else{
					$link = '<a href="'.$config['admin_site_url'].'contactView/'.$log->f('field_value').'">contact</a>';
				}
				break;
			case 'maintenance':
				$link = '<a href="'.$config['admin_site_url'].'service/edit/'.$log->f('field_value').'">intervention</a>';
				break;
			default:
				$link = $log->f('pag');
				break;
		}

		$from_user = $this->db_users->query("SELECT * FROM users WHERE user_id='".$log->f('user_id')."' ");
		$from_user->next();
		$user_email = $this->db_users->query("SELECT * FROM users WHERE user_id='".$log->f('to_user_id')."' ");
		$user_email->next();
        $mail = new PHPMailer();
       /*
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug = 1; // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true; // enable SMTP authentication
        $mail->Host = SMTP_HOST; // sets the SMTP server
        $mail->Port = SMTP_PORT; // set the SMTP port for the GMAIL server
        $mail->Username = SMTP_USERNAME; // SMTP account username
        $mail->Password = SMTP_PASSWORD; // SMTP account password*/

        $body='Dear User,

You have received this internal message:
-------------------------------------------------------------------
<table width="100%"><tbody><tr><td width="10%" style="text-align:right">From :</td><td>&nbsp;&nbsp;'.htmlspecialchars_decode(stripslashes($from_user->f('first_name').' '.$from_user->f('last_name'))).'</td></tr><tr><td style="text-align:right">Subject :</td><td>&nbsp;&nbsp;New task</td></tr><tr><td style="text-align:right">Date :</td><td>&nbsp;&nbsp;'.date(ACCOUNT_DATE_FORMAT,$log->f('date')).'</td></tr><tr><td style="text-align:right">To :</td><td>&nbsp;&nbsp;'.htmlspecialchars_decode(stripslashes($user_email->f('first_name'))).' '.htmlspecialchars_decode(stripslashes($user_email->f('last_name'))).'</td></tr><tr><td style="text-align:right">Item :</td><td>&nbsp;&nbsp;'.$link.'</td></tr></tboby></table>


'.$log->f('message').'

-------------------------------------------------------------------

Best regards,
Akti Customer Care
<a href="http://saleassist.eu" >www.saleassist.eu</a>';

		$def_email = $this->default_email();

        $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
        $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);

        $subject="New internal message";

        $mail->Subject = $subject;

        $mail->MsgHTML(nl2br(($body)));

        $mail->AddAddress($user_email->f('email'));

		$sent_date= time();
    	$this->db->query("UPDATE logging SET sent='1' WHERE log_id='".$in['log_id']."' ");

   		$mail->Send();

		// msg::$success =gm('Invoice has been successfully sent');
		// $in['act'] = '';
		// $in['do']='invoice-invoice';
		// ark::run('invoice-invoice');
		return true;
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		/*$this->dbu->query("SELECT * FROM default_data WHERE type='invoice_email' ");
		if($this->dbu->move_next()){
			$array['reply']['name'] = $this->dbu->f('default_name');
			$array['reply']['email'] = $this->dbu->f('value');
		}*/

		if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
			if(MAIL_SETTINGS_PREFERRED_OPTION==2){
				if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
					$array['from']['email'] = MAIL_SETTINGS_EMAIL;
				}
			}else{
				if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
					$array['from']['email'] = ACCOUNT_EMAIL;
				}
			}
		}
		$this->db->query("SELECT * FROM default_data WHERE type='quote_email' ");
		//if($this->db->move_next()){
		if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
			$array['from']['name'] = $this->db->f('default_name');
			if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)) && $this->db->f('value')!=''){
				$array['from']['email'] = $this->db->f('value');
			}
		}

		$this->db->query("SELECT * FROM default_data WHERE type='bcc_quote_email' ");
          if($this->db->move_next() && $this->db->f('value')){
            $array['bcc']['email'] = $this->db->f('value'); // pot fi valori multiple, delimitate de ;
          }
		return $array;
	}


}//end class
?>