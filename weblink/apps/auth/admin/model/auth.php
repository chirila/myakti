<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class auth {

	function auth() {
		global $database_config;
		$this -> db = new sqldb;

		$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);

		$this -> db_users = new sqldb($database_users);

	}

	/****************************************************************
	* function login(&$in)                                          *
	****************************************************************/
	function login(&$in) {
	
		if (!$this -> auth_validate($in)) {
			return false;
		}
		$this -> db_users -> query("SELECT password,user_id,main_user_id,user_role,database_name, lang.code
                FROM users 
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = '".$in['username']."'");    
		if($this -> db_users -> move_next()){


			if(md5($in['password']) == $this -> db_users ->f('password'))
			{
				
				session_start();
				$u_id = $this -> db_users ->f('user_id');
				// $_SESSION['db'] = $this -> db_users ->f('database_name');
				$_SESSION['lang_id'] = $this -> db -> field("SELECT lang_id FROM pim_lang WHERE default_lang='1'");
				
				$_SESSION['group']='admin';
				
				$q = $this->db_users->query("SELECT * FROM users_settings WHERE user_id = '".$u_id."'");
				if($q->move_next()){
					$_SESSION['regional']['user_id']=$q->f('user_id');
					$_SESSION['regional']['timezone']=$q->f('timezone');
					$_SESSION['regional']['date_format']=$q->f('date_format');
					$_SESSION['regional']['startday']=$q->f('startday');
					$_SESSION['regional']['time_format']=$q->f('time_format');
					$_SESSION['regional']['time_display']=$q->f('time_display');
				}

			}
			else {
				msg::set('password', "Wrong Username or Password!"."<br>");
				return false;
			}

		}else{
			msg::set('username', "Wrong Username or Password!"."<br>");
			return false;

		}
		header("Location: ../../index.php");
//		header("Location: ".$_SERVER['PHP_SELF']);
		return true;
	}
	/****************************************************************
	* function logout(&$ld)										    *
	****************************************************************/

	function logout(&$in)
	{
		
		unset($_SESSION['regional']);
		unset($_SESSION['db']);
		unset($_SESSION['lang_id']);
		unset($_SESSION['group']);
		unset ($_SESSION['l']);
    unset ($_SESSION['db']);
    unset ($ld['act']);
    session_unset();
    session_destroy();
		
		header("Location: ../../");
		return true;
	}


	/****************************************************************
	* function auth_validate(&$in)                                   *
	****************************************************************/
	function auth_validate(&$in) {
		$v=new validation();
		$v->f('username','Username','required');
		$v->f('password','Password','required');

		return $v->run();
	}

}
