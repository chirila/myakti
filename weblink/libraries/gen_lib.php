<?php

function get_link ($link)
{
	global $rewrite_url, $_SESSION;

	$link = str_replace("'","",$link);
	$_db=new sqldb();
    $_db->query("select mask_lang_".$_SESSION['front']['lang_id']." as mask from url_mask where url='".$link."'");
    if($_db->move_next())
    {
    		return $_db->f('mask');
    }else{
        return $link;
    }
}

function page_redirect ($page)
{
	echo '
<script language="javascript">
<!-- 

location.replace("'.$page.'");

-->
</script>
';
}
/**
 * @return unknown
 * @param $str string
 * @desc Enter description here...
 */
function generateUniqueLink($str,$item_id,$lang=1)
{
	$out_str='';
	$out_str.=checkUniqueLink(str_replace("'","",str_replace(" ","-", strtolower($str))),$item_id,$lang);
//	$out_str.='.html';
	return $out_str;
}

function checkUniqueLink($url,$item_id,$lang=1,$i=0)
{

	global $_db;
	if($i==0)
		$tmp_link = $url;
	else
		$tmp_link = $url.'-'.$i;

	$_db->query("select * from url_mask where mask_lang_".$lang." = '".$tmp_link."' and item_id!='".$item_id."'");
	if($_db->move_next())
	{
		$i++;
		return checkUniqueLink($url,$item_id, $lang,$i);
	}
	else
	{
		 return $tmp_link;
	}

}
function removeAccents($str)
{
  $a = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '�', '�', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '�', '�', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '�', '?', '?', '?', '?', '�', '�', '?', '�', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str);
}

/**
 * @return unknown
 * @param $str string
 * @desc Enter description here...
 */
function str_to_filename_mask($str)
{
        $str = removeAccents(strtolower(utf8_decode(rtrim($str))));


	$allowed['1']=1;
	$allowed['2']=1;
	$allowed['3']=1;
	$allowed['4']=1;
	$allowed['5']=1;
	$allowed['6']=1;
	$allowed['7']=1;
	$allowed['8']=1;
	$allowed['9']=1;
	$allowed['0']=1;
	$allowed['q']=1;
	$allowed['w']=1;
	$allowed['e']=1;
	$allowed['r']=1;
	$allowed['t']=1;
	$allowed['y']=1;
	$allowed['u']=1;
	$allowed['i']=1;
	$allowed['o']=1;
	$allowed['p']=1;
	$allowed['a']=1;
	$allowed['s']=1;
	$allowed['d']=1;
	$allowed['f']=1;
	$allowed['g']=1;
	$allowed['h']=1;
	$allowed['j']=1;
	$allowed['k']=1;
	$allowed['l']=1;
	$allowed['z']=1;
	$allowed['x']=1;
	$allowed['c']=1;
	$allowed['v']=1;
	$allowed['b']=1;
	$allowed['n']=1;
	$allowed['m']=1;
	$allowed[' ']=1;
	$allowed['-']=1;

	$newstr='';
	for($i=0; $i<=strlen($str); $i++)
	{
		if($allowed[$str[$i]])
		{
			$newstr.=$str[$i];
		}
	}
	$str = $newstr;
	$out_str='';
	$out_str=str_replace(".", "", $str);
	$out_str=str_replace("'", "-", $str);
  $out_str=str_replace(" ", "-", $out_str);
	$out_str=str_replace("---", "-", $out_str);
	$out_str=str_replace("--", "-", $out_str);

	return $out_str;
}
function str_to_filename($str)
{
	$allowed['1']=1;
	$allowed['2']=1;
	$allowed['3']=1;
	$allowed['4']=1;
	$allowed['5']=1;
	$allowed['6']=1;
	$allowed['7']=1;
	$allowed['8']=1;
	$allowed['9']=1;
	$allowed['0']=1;
	$allowed['q']=1;
	$allowed['w']=1;
	$allowed['e']=1;
	$allowed['r']=1;
	$allowed['t']=1;
	$allowed['y']=1;
	$allowed['u']=1;
	$allowed['i']=1;
	$allowed['o']=1;
	$allowed['p']=1;
	$allowed['a']=1;
	$allowed['s']=1;
	$allowed['d']=1;
	$allowed['f']=1;
	$allowed['g']=1;
	$allowed['h']=1;
	$allowed['j']=1;
	$allowed['k']=1;
	$allowed['l']=1;
	$allowed['z']=1;
	$allowed['x']=1;
	$allowed['c']=1;
	$allowed['v']=1;
	$allowed['b']=1;
	$allowed['n']=1;
	$allowed['m']=1;
	$allowed[' ']=1;
	$str = strtolower($str);
	$newstr='';
	for($i=0; $i<=strlen($str); $i++)
	{
		if($allowed[$str[$i]])
		{
			$newstr.=$str[$i];
		}
	}
	$str = $newstr;
	$out_str='';
	$out_str=str_replace(" ", "-", $str);
	$out_str=str_replace("---", "-", $out_str);
	$out_str=str_replace("--", "-", $out_str);
	return $out_str;
}

function limitare($str,$limit=200)
{
	$array = preg_split('/ /', $str, -1, PREG_SPLIT_OFFSET_CAPTURE);
	$return = '';
	foreach($array as $k => $val)
	{
		$return .= $val[0]." ";
		if($val[1]>$limit) {$return .= "..."; break;}
	}
	return $return;
}
function pick_date_format($format = ACCOUNT_DATE_FORMAT){
	switch ($format){
		case 'm/d/Y':
			return 'mm/dd/yy';
			break;
		case 'd/m/Y':
			return 'dd/mm/yy';
			break;
		case 'Y-m-d':
			return 'yy-mm-dd';
			break;
		case 'd.m.Y':
			return 'dd.mm.yy';
			break;
		case 'Y.m.d':
			return 'yy.mm.dd';
			break;
		case 'Y/m/d':
			return 'yy/mm/dd';
			break;
	}
}

function pick_date_timestamp($birthdate){
	switch ($_SESSION['regional']['date_format']){
		case 'm/d/Y':
			$bp = split('/',$birthdate);
			return mktime(0,0,0,$bp[0],$bp[1],$bp[2]);
			break;
		case 'd/m/Y':
			$bp = split('/',$birthdate);
			return mktime(0,0,0,$bp[1],$bp[0],$bp[2]);
			break;
		case 'Y-m-d':
			$bp = split('-',$birthdate);
			return mktime(0,0,0,$bp[1],$bp[2],$bp[0]);
			break;
		case 'd.m.Y':
			$bp = split('.',$birthdate);
			return mktime(0,0,0,$bp[1],$bp[0],$bp[2]);
			break;
		case 'Y.m.d':
			$bp = split('.',$birthdate);
			return mktime(0,0,0,$bp[1],$bp[2],$bp[1]);
			break;
		case 'Y/m/d':
			$bp = split('/',$birthdate);
			return mktime(0,0,0,$bp[1],$bp[2],$bp[0]);
			break;
	}
}

function build_simple_dropdown($opt, $selected=0)
{
	if($opt)
	while (list ($key, $val) = each ($opt))
	{
		$out_str.="<option value=\"".$key."\" ";//options values
		$out_str.=($key==$selected?" SELECTED ":"");//if selected
		$out_str.=">".$val."</option>";//options names
	}


	return $out_str;
}


function build_language_ecom_dd($selected){
	$db = new sqldb();
	$array = array();
	$db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY lang_id");
	while($db->move_next()){
		$array[$db->f('lang_id')] = $db->f('language');

	}
	return build_simple_dropdown($array,$selected);
}

function str_to_safe($str)
{
	$out_str='';
	$out_str.=str_replace(" ", "_", strtolower($str));
	return $out_str;
}


function array_to_json( $array ){

	if( !is_array( $array ) ){
		return false;
	}

	$associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
	if( $associative ){

		$construct = array();
		foreach( $array as $key => $value ){

			// We first copy each key/value pair into a staging array,
			// formatting each key and value properly as we go.

			// Format the key:
			if( is_numeric($key) ){
				$key = "key_$key";
			}
			$key = "\"".addslashes($key)."\"";

			// Format the value:
			if( is_array( $value )){
				$value = array_to_json( $value );
			} else if( !is_numeric( $value ) || is_string( $value ) ){
				$value = "\"".str_replace('&amp;nbsp;','&nbsp;',htmlspecialchars($value))."\"";
			}

			// Add to staging array:
			$construct[] = "$key: $value";
		}

		// Then we collapse the staging array into the JSON form:
		$result = "{ " . implode( ", ", $construct ) . " }";

	} else { // If the array is a vector (not associative):

		$construct = array();
		foreach( $array as $value ){

			// Format the value:
			if( is_array( $value )){
				$value = array_to_json( $value );
			} else if( !is_numeric( $value ) || is_string( $value ) ){
				$value = "'".addslashes($value)."'";
			}

			// Add to staging array:
			$construct[] = $value;
		}

		// Then we collapse the staging array into the JSON form:
		$result = "[ " . implode( ", ", $construct ) . " ]";
	}

	return $result;
}

function duplicate_data($table,$item,$item_id,$new_item_id,$primary_key=''){

	$db=new sqldb();

	$db->query("CREATE TEMPORARY TABLE temp ENGINE=MyISAM SELECT * FROM $table WHERE $item ='".$item_id."'");

	$db->query("UPDATE temp SET $item  ='".$new_item_id."'");
	if($primary_key){
		$last_primary_key=$db->field("SELECT $primary_key FROM $table ORDER BY $primary_key DESC LIMIT 1");
		$temp=$db->query("SELECT * FROM temp ORDER BY $primary_key DESC");
		while($temp->next()){
			$last_primary_key++;
			$db->query("UPDATE temp SET $primary_key  ='".$last_primary_key."' WHERE $primary_key='".$temp->f($primary_key)."'");
		}
	}

	$db->query("INSERT INTO $table SELECT * FROM temp");
	$db->query("DROP TABLE temp");

}
function get_commission_type_list($selected)
{
	return currency::get_currency($selected);
}
function get_country_name($country_id)
{
	$name = '';
	if($country_id){
		$db=new sqldb();
		$db->query("SELECT country.* FROM country WHERE country_id='".$country_id."'");
		$db->move_next();
		$name=$db->f('name');
	}
	return $name;
}
function get_state_name($state_id)
{
	$db=new sqldb();
	$db->query("SELECT state.*  FROM state WHERE state_id='".$state_id."'");
	$db->move_next();
	$name=$db->f('name');
	return $name;
}
function return_value($number){
	$separator = ACCOUNT_NUMBER_FORMAT;
	$out=str_replace(substr($separator, 0, -1), '', $number);
	// var_dump(substr($separator, 0, -1));
	$out=str_replace(substr($separator, -1), '.', $out);
	return $out;

}
function place_currency($out,$currency = ''){


	if(!$currency){
		$currency = get_currency(ACCOUNT_CURRENCY_TYPE);
	}
	if(ACCOUNT_CURRENCY_FORMAT == 0){
		$out = $out.' '.$currency;
	}
	else {
		$out = $currency.' '.$out;
	}
	return $out;

}
function display_number($number){
	if(!$number){
		$number = 0;
	}
	$separator = ACCOUNT_NUMBER_FORMAT;
	$out = number_format(floatval($number),2,substr($separator, -1),substr($separator, 0, 1));
	return $out;
}
function get_currency($selected)
{
	return currency::get_currency($selected);
}
function display_number_var_dec($number){
	if(!$number){
		$number = 0;
	}
	$separator = ACCOUNT_NUMBER_FORMAT;
	$decimals_after_comma = ARTICLE_PRICE_COMMA_DIGITS;
	$out = number_format(floatval($number),$decimals_after_comma,substr($separator, -1),substr($separator, 0, 1));
	return $out;
}
function get_sys_message($name,$lang='')
{
	$_db=new sqldb();
	$filter = ' ';
	if(!empty($lang) && is_numeric($lang)){
		$code = $_db->field("SELECT code FROM pim_lang WHERE lang_id='".$lang."' ");
		$filter = " AND lang_code='".$code."' ";
	}else if(!empty($lang)){
		$filter = " AND lang_code='".$lang."' ";
	}
	$_db->query("SELECT * FROM sys_message WHERE name='".$name."' ".$filter);
	$_db->move_next();
	$msg['text']=$_db->f('text');
	$msg['from_email']=$_db->f('from_email');
	$msg['from_name']=$_db->f('from_name');
	$msg['subject']=$_db->f('subject');
	return $msg;
}

function get_weblink_sys_message($name,$lang='',$useHtml=true){
    $_db=new sqldb();
    $filter = ' ';
    if(!empty($lang) && is_numeric($lang)){
        if($lang>=1000) {
            $code = $_db->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$lang."' ");
            if($code=='nl'){
                $code='du';
            }
            $filter = " AND lang_code = '".$code."' ";
        } else {
            $code = $_db->field("SELECT code FROM pim_lang WHERE lang_id='".$lang."' ");
            if($code=='nl'){
                $code='du';
            }
            $filter = " AND lang_code='".$code."' ";
        }
    }else if(!empty($lang)){
        $filter = " AND lang_code='".$lang."' ";
    }

    $result = $_db->query("SELECT * FROM sys_message WHERE name='".$name."' ".$filter)->getAll();

    if(empty($result)){
    	return $result;
    } else {
    	$result = $result[0];
    }

    $msg['text']=$result['text'];
    $msg['from_email']=$result['from_email'];
    $msg['from_name']=$result['from_name'];
    $msg['subject']=$result['subject'];
    $msg['footnote']=$result['footnote'];
    $msg['use_html']=$result['use_html'];
    if($msg['use_html'] == 1 && $useHtml === true && $result['html_content']){
        $msg['text']=$result['html_content'];
    }
    return $msg;
}

function build_pdf_language_dd($selected,$active){

	$db = new sqldb();
	$db2 = new sqldb();
	$array = array();
	$filter="1=1";
	if($active){
		$filter.=" and active=1";
	}
	$db->query("SELECT * FROM pim_lang where  ".$filter." ORDER BY sort_order ");
	while($db->move_next()){
		$array[$db->f('lang_id')] = gm($db->f('language'));
	}
	$db->query("SELECT * FROM pim_custom_lang where  ".$filter." ORDER BY sort_order ");
	while($db->move_next()){
		$db2->query("SELECT * FROM label_language WHERE lang_code='".$db->f('lang_id')."'");
		if($db2->move_next())
		{
			$array[$db2->f('label_language_id')] = $db2->f('name');
		}
	}
	return build_simple_dropdown($array,$selected);

}
function build_currency_name_list($selected){
	return currency::get_currency($selected,'code');
}
function build_user_dd($selected=''){
	global $database_config;
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$array = array();
	$db = new sqldb($db_config);
	$db->query("SELECT * FROM users WHERE database_name='".obj::get('db')."' AND active='1' ");
	while ($db->next()) {
		$array[$db->f('user_id')] = utf8_encode($db->f('last_name').' '.$db->f('first_name'));
	}
	if(!$selected){
		$selected = 0;
	}
	return build_simple_dropdown($array,$selected);
}
function get_user_name($id)
{
	global $database_config;

	$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);

	$db = new sqldb($db_config);

	$db->query("SELECT first_name,last_name FROM users WHERE user_id='".$id."'");
	$db->move_next();
	return htmlspecialchars_decode(stripslashes($db->f('first_name').' '.$db->f('last_name')));
}
function time_ago($timestamp,$now){
	$dif = $now - $timestamp;
	if($dif < 60 ){
		$out = gm('a few seconds ago');
	}else if($dif < 3600){
		$time = floor($dif/60);
		$out = $time.' '.gm('minutes ago');
		if($time == 1){
			$out = $time.' '.gm('minute ago');
		}
	}else if($dif < 86400){
		$time = floor($dif/3600);
		$out = $time.' '.gm('hours ago');
		if($time == 1){
			$out = $time.' '.gm('hours ago');
		}
	}else{
		$out = date(ACCOUNT_DATE_FORMAT,$timestamp);
	}
	return $out;
}
function insert_message_log($pag,$message,$field_name,$field_value,$type = 0,$user_id = '', $track_log=false, $customer_id=0, $the_time='0'){
	global $database_config;
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	if(!$the_time)
	{
		$the_time=time();
	}
	$db = new sqldb();
	$field_select = 'buyer_name';
	if(obj::get('table') == 'servicing_support'){
		$field_select = 'customer_name';
	}
	if(obj::get('table') == 'pim_p_orders'){
		$field_select = 'customer_name';
	}
	$customer_name = $db->field("SELECT $field_select FROM ".obj::get('table')." WHERE ".obj::get('main_field')."='".$field_value."' ");
	$db_users = new sqldb($db_config);
	$message = stripslashes($message);
	$id = $db->insert("INSERT INTO logging SET pag='".$pag."', message='".addslashes($message)."', field_name='".$field_name."', field_value='".$field_value."', date='".$the_time."', type='".$type."', customer_name='".addslashes($customer_name)."' ");

	return $id;
}
function get_country_code($country_id)
{
	$db=new sqldb;
	$db->query("SELECT code FROM country WHERE country_id='".$country_id."'");
	$db->move_next();
	$name=$db->f('code');
	return $name;
}

function list_content($arrayname, $tab="&nbsp&nbsp&nbsp&nbsp", $indent=0) { // recursively displays contents of the array and sub-arrays:
	// This function (c) Peter Kionga-Kamau (http://www.pmkmedia.com)
	// Free for unrestricted use, except sale - do not resell.
	// use: echo LIST_CONTENTS(array $arrayname, string $tab, int $indent);
	// $tab = string to use as a tab, $indent = number of tabs to indent result
	while (list($key, $value) = each($arrayname)) {
		for ($i = 0; $i < $indent; $i++)
		$currenttab .= $tab;
		if (is_array($value)) {
			$retval .= "$currenttab$key : Array: <BR>$currenttab{<BR>";
			$retval .= list_content($value, $tab, $indent + 1) . "$currenttab}<BR>";
		}
		else
		$retval .= "$currenttab$key => $value<BR>";
		$currenttab = NULL;
	}
	return $retval;
}
function getq_label_txt($label,$language_id)
{
	$db=new sqldb;
	$db->query("SELECT * FROM label_language_quote WHERE label_language_id='".$language_id."'");
	$db->move_next();
	if($db->f($label)){
		$label_val=$db->f($label);
	}else{
		$label_val='';
	}
	return $label_val;
}
function get_label_txt($label,$language_id)
{
	$db=new sqldb();
	if($language_id<=4) {
		$db->query("SELECT * FROM label_language WHERE label_language_id = '".$language_id."' ");
	} else {
		$db->query("SELECT * FROM label_language WHERE lang_code ='".$language_id."'");
	}
	$db->move_next();
	if($db->f($label)){
		$label_val=$db->f($label);
	}else{
		$label_val='';
	}
	return $label_val;
}
function getpo_label_txt($label,$language_id)
{
	$db=new sqldb();
	if($language_id<=4) {
		$db->query("SELECT * FROM label_language_p_order WHERE label_language_id='".$language_id."'");
	} else {
		$db->query("SELECT * FROM label_language_p_order WHERE lang_code = '".$language_id."'");
	}

	$db->move_next();
	$label_val = $db->f($label) ? $db->f($label) : '';
	return $label_val;
}
function unique_id($l = 4) {
	return substr(md5(uniqid(mt_rand(), true)), 0, $l);
}
function remove_zero_decimals($number,$decimals=2){
	$separator = ACCOUNT_NUMBER_FORMAT;
	if(!$number){
		$number = '0.00';
	}
	if(substr($number, -2,2)== '00'){
		$out = intval($number);
	}else{
		$dec_part = substr($number, -2,2);
		$int_part = intval($number);
		// console::log($int_part,substr($separator, -1),$dec_part);
		$out = $int_part.substr($separator, -1).$dec_part;
	}
	return $out;
}

function getq_weblink_label_text($label, $language_id) {
	$db = new sqldb();

	if($language_id<=4) {
		$db->query("SELECT * FROM label_language_quote WHERE label_language_id = '".$language_id."' ");
	} else {
		$db->query("SELECT * FROM label_language_quote WHERE lang_code = '".$language_id."' ");
	}
	$db->move_next();
	if($db->f($label)) {
		$label_val = $db->f($label);
	} else {
		$label_val = '';
	}
	return ($label_val);
}


function number_as_hour($number){
		$negative = false;
		$min = round($number * 60);
		if($min < 0){
			$min = $min*(-1);
			$negative = true;
		}
		if( $min >= "60" ){
			$n = floor( $min / 60 );
			$min = $min - ( 60 * $n );
			if(strlen($min) == 1){
				$min = "0" . $min;
			}
			if($negative){
				$n = $n*(-1);
			}
			$out = $n . ":" . $min;
		}else{
			if(strlen($min) == 1){
				$min = "0" . $min;
			}
			$out = "0:". $min;
		}
		return $out;
	}
	function gettime_label_txt($label,$language_id)
{
	$db=new sqldb();
	$db->query("SELECT * FROM label_language_time WHERE label_language_id='".$language_id."'");
	$db->move_next();
	if($db->f($label)){
		$label_val=$db->f($label);
	}else{
		$label_val='';
	}
	return $label_val;
}
function generate_ogm($db='')
{
	if(!$db){
		$dbs = new sqldb();
	}else{
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $db,
		);
		$dbs = new sqldb($database_users);
	}
	$result = '';
	$o = $dbs->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION' ");
	$i = $dbs->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION_NR' ");
	if($o){
		switch ($o) {
			case 1:
				$nr = time();
				// $first_part = substr($nr,0,4);$second_part = substr($nr,4,4); $last_part = substr($nr,8,2);
				$first_part = substr($nr,0,3);$second_part = substr($nr,3,4); $last_part = substr($nr,7,3);
				$calc =$first_part.$second_part.$last_part;
				$calc = $calc/97;
				$s = strpos($calc,'.');
				$calc = substr($calc,$s+1,2);
				$result = $first_part.'/'.$second_part.'/'.$last_part.$calc;
				break;
			case 2:
				$nr = $i;$nr2=get_last_invoice_number(true,$db);$nr3='';$calc=0;
				if(strlen($nr) > 3){
					$nr2 = substr($nr,3).$nr2;
					$nr= substr($nr,0,3);
				}else{
					$l = 3-strlen($nr);
					$nr.=substr($nr2,0,$l);
					$nr2 = substr($nr2,$l);
				}
				if(strlen($nr2) > 4){
					$nr3 = substr($nr2,4);
					$nr2 = substr($nr2,0,4);
				}else{
					$l = 4-strlen($nr2);
					for($i=1;$i<=$l;$i++){
						$nr2 .= '0';
					}
				}
				if(strlen($nr3) < 3){
					$l = 3-strlen($nr3);
					for($i=1;$i<=$l;$i++){
						$nr3 .= '0';
					}
				}
				$calc= $nr.$nr2.$nr3;
				$calc = $calc/97;
				$s = strpos($calc,'.');
				$calc = substr($calc,$s+1,2);
				$result = $nr.'/'.$nr2.'/'.$nr3.$calc;

				break;
		}
	}
	return $result;
}

function get_custom_width($all_custom_widths,$can_be_hidden_custom_widths,$custom_line_1,$custom_line_2,$custom_line_3,$custom_line_4){
	// $all_custom_widths - array with all the columns widths that can be shown on the same time (except the variable one)
	// $can_be_hidden_custom_widths - array with all the columns widths that can be hidden
	// $custom_line_1 , $custom_line_2, $custom_line_3, $custom_line_4 - the state of the column (shown = true , hidden = false)
	$total_width = 100;
	$total_fixed_width = 0;
	foreach ($all_custom_widths as $key => $value) {
		$total_fixed_width += $value;
	}
	$fixed_diff = $total_width - $total_fixed_width;
	$variable_diff = 0;
	for ($i=1; $i <= 4; $i++) {
		if(func_get_arg($i+1) == false){
			$variable_diff += $can_be_hidden_custom_widths['custom_line_'.$i];
		}
	}
	$custom_width = $fixed_diff+$variable_diff;
	return $custom_width;
}

function add_notification($database,$module,$module_id,$users,$action_type){
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$dbs = new sqldb($database_users);
	if(is_array($users) && !empty($users)){
		foreach($users as $user_id){
			$dbs->query("INSERT INTO `notifications` SET 
	            db_name='".$database."',
	            user_id='".$user_id."',
	            module_name='".$module."',
	            module_id='".$module_id."',
	            action_type='".$action_type."',
	            created_at='".time()."' ");
		}
	}
	return true;
}
?>