<?php
class obj{
	private static $obj=array();
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author your's truly
	 **/
	function __construct(){}

	public static function set($key,$value){
		self::$obj[$key]=$value;
	}

	public static function get($key){
		if(isset(self::$obj[$key])){
			return self::$obj[$key];
		}
		return null;
	}


}
?>