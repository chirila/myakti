 <?php if(!defined('BASEPATH')) exit('No direct script access allowed');
// global $config;
$page = ark::run();

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	$json = new Services_JSON();
	$in['arkmsg'] = msg::get_all_messages();
	$in['innerHTML'] = $page;
	if(!isset($in['debug']) && DEBUG == true){
		console::memory();
		console::headline($in['do']);
		console::speed('Load time');
		$in['console'] = console::getInstance()->render();
	}
	echo ' '.$json->encode($in);
	exit();
}
date_default_timezone_set('UTC');
$time = time();
$browser = new Browser();
$browser_name = $browser->getBrowser();
$browser_version = $browser->getVersion();
//console::log($_SERVER);
if(!$_SERVER['HTTP_REFERER']){
	$type = obj::get('app');
	$in['log_id'] = $_db->insert("INSERT INTO weblink_log  SET
										code  		= '".$in['q']."',
										ip 	  		= '".$_SERVER['REMOTE_ADDR']."',
										browser 	= '".$browser_name.' - '.$browser_version."',
										time 		= '".$time."',
										item_id 	= '".$in['invoice_id']."',
										type 		= '".$type."' ");
}
/*
if($in['old_frame']){
	return $page;
}*/

if(ark::$layout){
	$main = new at(ark::$layout);
	$main->assign('SITE_URL', $config['site_url']);
	$main->assign('page', $page);
	$main->out();
} else {
	echo $page;
}