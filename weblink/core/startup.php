<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Europe/Helsinki');

session_start();
// build the $in array
if(is_array($_GET)){
	foreach($_GET as $key => $value){
		if(!is_array($value))
	    {
	        $in[$key] = addslashes($value);
	    }else{

		   $in[$key]=$value;
	    }
	}
	unset($_GET);
}

if(is_array($_POST)){
	foreach($_POST as $key => $value){
		if(!is_array($value))
	    {
	        $in[$key] = addslashes($value);
	    }else{

		   $in[$key]=$value;
	    }
	}
	unset($_POST);
}

require(BASEPATH.'common.php');
$log_query = 'YES';
if(isset($in['debug'])){
	$log_query = 'NO';
}
define('LOG_QUERYS',$log_query);

$_db=new sqldb();


$_db->query("INSERT INTO cron set extra='".serialize($in)."',time=NOW(), db='weblink' ");
if(isset($in['Reference'])){
	$in['q'] = $in['Reference'];
}

if(isset($in['q'])){

	$data = $_db->query("SELECT * FROM `urls` WHERE `url_code`='".$in['q']."' ");
	if($data->next()){
		global $database_config;


		$database_config['mysql']['database'] = $data->f('database');
		obj::set('db',$data->f('database'));
		$_db=new sqldb();
		$_db->query("SET names utf8");
		$_db->query("SET sql_mode= ''");
		$_db->query("SELECT * FROM settings");
		while($_db->move_next())
		{
			if($_db->f('constant_name') && $_db->f('type')  == 1){
				define($_db->f('constant_name'),$_db->f('value'));
			}else{
				define($_db->f('constant_name'),$_db->f('long_value'));
			}
		}
		$in['invoice_id'] = $data->f('item_id');
		switch ($data->f('type')) {
			case 'q':
				$table = 'tblquote';
				$id='id';
				if(!$in['do']){
					$in['do'] = 'quote-quote';
				}
				$in['quote_id'] = $in['invoice_id'];
				obj::set('table','tblquote');
				obj::set('quote_id',$in['invoice_id']);
				obj::set('app','quote');
				obj::set('print','quote_print');
				obj::set('pag','quote');
				obj::set('item','quote_id');
				obj::set('id',$in['invoice_id']);
				obj::set('version_id',$data->f('version_id'));
				obj::set('allow_comments',ALLOW_QUOTE_COMMENTS);
				obj::set('pdf_format',ACCOUNT_QUOTE_PDF_FORMAT);
				obj::set('main_field','id');
				break;
			case 'm':
				$table = 'servicing_support';
				$id='service_id';
				if(!$in['do']){
					$in['do'] = 'project-service';
				}
				$in['service_id'] = $in['invoice_id'];
				obj::set('table','servicing_support');
				obj::set('service_id',$in['invoice_id']);
				obj::set('app','project');
				obj::set('print','print');
				obj::set('pag','maintenance');
				obj::set('item','service_id');
				obj::set('id',$in['invoice_id']);
				obj::set('service_id',$in['invoice_id']);
				obj::set('allow_comments',ALLOW_MAINTENANCE_COMMENTS);
				obj::set('pdf_format',ACCOUNT_MAINTENANCE_PDF_FORMAT);
				obj::set('main_field','service_id');
				break;
			case 'p':

				$table = 'pim_p_orders';
				$id='p_order_id';
				if(!$in['do']){
					$in['do'] = 'order-p_order';
				}
				$in['p_order_id'] = $in['invoice_id'];
				obj::set('table','pim_p_orders');
				obj::set('p_order_id',$in['invoice_id']);
				obj::set('app','order');
				obj::set('print','p_order_print');
				obj::set('pag','p_order');
				obj::set('item','p_order_id');
				obj::set('id',$in['invoice_id']);
				obj::set('p_order_id',$in['invoice_id']);
				obj::set('allow_comments',ALLOW_PO_ORDER_COMMENTS);
				obj::set('pdf_format',ACCOUNT_P_ORDER_PDF_FORMAT);
				obj::set('main_field','p_order_id');
				break;
			default:
				obj::set('table','tblinvoice');
				$table = 'tblinvoice';
				$id='id';
				obj::set('invoice_id',$in['invoice_id']);
				obj::set('app','invoice');
				obj::set('print','invoice_print');
				obj::set('pag','invoice');
				obj::set('item','invoice_id');
				obj::set('id',$in['invoice_id']);
				obj::set('allow_comments',ALLOW_INVOICE_COMMENTS);
				obj::set('pdf_format',ACCOUNT_INVOICE_PDF_FORMAT);
				obj::set('main_field','id');
				break;
		}
		$languages = 'en';
		if($table){
			$languages = $_db->field("SELECT code FROM pim_lang WHERE lang_id = (SELECT email_language FROM ".$table." WHERE ".$id."='".$in['invoice_id']."') ");
		}
		if($languages){
			if($languages == 'du'){
				$languages = 'nl';
			}
			$_SESSION['lg'] = $languages;
		}
		switch (true) {
			case isset($_SESSION['lg']):
				$language = $_SESSION['lg'];
				break;
			default:
				$language = LANGUAGE_TR;
				break;
		}
		
		$lang = array();
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($db_config);
		$languageLabels = $db_users->query("SELECT id, ".strtolower($language)." FROM langs ")->getAll();
		foreach ($languageLabels as $key => $value) {
		    $lang[$value['id']] = $value[strtolower($language)];
		}
		//JWT start
		ark::loadLibraries(array('jwt/JWT'));
		$JWT_PSWD=$db_users->field("SELECT value FROM settings WHERE constant_name='JWT_PSWD'");
		$AWS_FACTORY_DATA=$db_users->field("SELECT long_value FROM settings WHERE constant_name='AWS_FACTORY'");
		try{
		    $AWS_FACTORY=JWT::decode($AWS_FACTORY_DATA, $JWT_PSWD, array('HS256'));
		    define('AWS_FACTORY_KEY',$AWS_FACTORY->data->key);
		    define('AWS_FACTORY_SECRET',$AWS_FACTORY->data->secret);
		}catch(ExpiredException $e){
		    //
		}catch(Exception $e){
		    //    
		}
		//JWT end
	}

}else{
	$in['do'] = 'quote-quote';
}
/* End of file startup.php */