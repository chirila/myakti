<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * DooHoo
 * An open-source PHP framework.
 * 
 * @package DooHoo
 * @copyright Copyright (c) Medeeaweb Works
 * @since Version 1.0
 */

/**
 * DooHoo PDO Wrapper Class

 */

class sqldbResult{
	/**
	 * holds the results of a query
	 * @access private
	 * @var PDOStatement
	 */
	private $results;
	
	/**
	 * holds the current row
	 * @access private
	 * @var PDOStatement
	 */
	private $row;
	
	/**
	 * in case the 'next' method has been used. 
	 * The first time a 'next' method is used we skip it because we position our self on the first row in the constructor
	 * @access private
	 * @var PDOStatement
	 */
	private $next_used;
	
	public function __construct($results){
		try{
			$this->results = $results->fetchAll();
		}
		catch(PDOException $e){
			$this->results = array();
		}
		$this->row = 0;
		$this->next_used = 0;
	}
	
	public function getAll(){
		return $this->results;
	}
	
	public function next(){
		if($this->next_used == 0){
			if(!empty($this->results)){
				$this->next_used = 1;
				return true;
			}
			else{
				return false;
			}
		}
		$this->row++;
		if(!empty($this->results[$this->row])){
			return true;
		}
		else{
			return false;
		}
	}


	public function next_array(){
		if(empty($this->results)){
		    $this->next();
		}
		$ret = array();
		foreach($this->results[$this->row] as $key => $value){
		    if(is_numeric($key)){
		        continue;
		    }
		    $ret[$key] = $value;
		}        
		return $ret;		
	}
	
	public function move_next(){
		return $this->next();
	}
	
	public function getField($field){
		if(isset($this->results[$this->row][$field])){
			return $this->results[$this->row][$field];
		}
		return;
	}
	
	public function f($field){
		return $this->getField($field);
	}
	
	public function gf($field){
		global $in;
		if($in[$field]){
			return  htmlspecialchars(stripslashes($in[$field]));
		}
		return $this->getField($field);
	}
	
	public function records_count(){
		return count($this->results);
	}
	public function field_count(){
		return (count($this->results[$this->row]) / 2);
	}
	
	public function move_to($row_number=0){
		$this->row = $row_number;
		
		if($this->results[$this->row]){
			return true;
		}
		else{
			return false;
		}
		return count($this->results);
	}
	
}
/* End of file sqldbResult.php */