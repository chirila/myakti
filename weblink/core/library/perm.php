<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class perm {

    public static $CONTROLLER = 'controller';
    public static $MODEL = 'model';

    private static $cache = array();

    private static $ctrl = array();
    private static $model = array();

    /**
     * Set permissions for a resources and a given group
     *
     * @param $type use perm::$CONTROLLER or perm::$MODEL
     * @param $object the object to have access to, for models use model-method(all can be used in any position)
     * @param $group the group that should have access
     * @param $permission should have permission or not
     */
    private static function set($type = 'controller', $object, $group, $permission = true){

        if(empty($group)){
            console::error(array('message' => 'Group can not be empty for permissions','file'=>__FILE__));
            return;
        }elseif(is_array($group)){
            foreach($group as $member){
                self::set($type, $object, $member, $permission);
            }
            return;
        }

        if(empty($object)){
            console::error(array('message' => 'Object can not be empty for permissions','file'=>__FILE__));
            return;
        }

        switch($type){
            case self::$CONTROLLER:
                $perms = self::$ctrl;
                $perm_type = 'ctrl';
                break;
            case self::$MODEL:
                $perms = self::$model;
                $perm_type = 'model';
                break;
            default:
                console::error(array('message' => 'Invalid permission type','file'=>__FILE__,'line'=>__LINE__));
                return;
        }

        //check to see if we have that group
        if(!isset($perms[$group])){
            $perms[$group] = array();
            if(defined('USE_APPS') && USE_APPS == true){
                $perms[$group] = array(ark::$app => array($object => $permission));
            }else{
                $perms[$group] = array($object => $permission);
            }
        }else{
            //we have the group
            if(defined('USE_APPS') && USE_APPS == true){
                $perms[$group][ark::$app][$object] = $permission;
            }else{
                $perms[$group][$object] = $permission;
            }
        }
        self::$$perm_type = $perms;
    }


    /**
     * Checks if the current users group is allowed to perm a given action on the current resource(model, controller)
     *
     * @param $type string resource type perm::$MODEL or perm::$CONTROLLER
     * @uses ark
     * @return boolean (Yes it is allowed, No it's not allowed')
     */
    public static function isAllowed($type = 'controller'){

        $perms = self::loadPermissions($type);
        if(!$perms){
            return false;
        }
        switch($type){
            case self::$CONTROLLER:
                $object = ark::$controller;
                $object_all = 'all';
                $perms = self::$ctrl;
                break;
            case self::$MODEL:
                $object = ark::$model.'-'.ark::$method;
                $object_all = ark::$model.'-all';
                $perms = self::$model;
                break;
            default:
                console::error(array('message' => 'Invalid permission type','file'=>__FILE__,'line' => __LINE__));
                return;
        }

        //check to see if we have exact permissions on the controller
        /*if(defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
            $entry = $perms[$_SESSION['group']][ark::$app];
        }else{
            $entry = $perms[$_SESSION['group']];
        }*/

        switch(true){
            case isset($entry[$object]):
                return $entry[$object];//permission on the object go for it
            case isset($entry[$object_all]):
                return $entry[$object_all];//permission on all object in this app
            case $type == self::$MODEL && isset($entry['all-'.ark::$method]):
                return $entry['all-'.ark::$method];
            case $type == self::$MODEL && isset($entry['all-all']):
                return $entry['all-all'];
            case isset($perms['all'])://permission for group all ?
                $entry = defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true ? $perms['all'][ark::$app] : $perms['all'];
                //check the all groups requires some extra handling for models
                if($type == self::$MODEL){
                    //we need to check all of the above so model-method, model-all, all-method,all-all
                    foreach (array($object, $object_all, 'all-'.ark::$method, 'all-all') as $value) {
                        if(!isset($entry[$value])){
                            continue;
                        }
                        return $entry[$value];
                    }
                }
                $key = isset($entry[$object]) ? $object : $object_all;//permission for controller or for all controllers
                return isset($entry[$key]) ? $entry[$key] : false;//return the perms for all if we have
        }
        //My great concern is not whether you have failed, but whether you are content with your failure. Abraham Lincoln
        return false;
    }

    /**
     * Loads a permission file for the current resource
     *
     * @param $type string resource type perm::$MODEL or perm::$CONTROLLER
     * @uses ark
     * @return boolean Managed to load file or not
     */
    private static function loadPermissions($type = 'controller'){

       if(defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
            $permpath = ark::$apppath.ENVIRONMENT.'/perm/';
        }else{
            $permpath = 'perm/';
        }

        switch ($type) {
            case 'controller':
                $permpath = $permpath.'controller.php';
                break;
            case 'model':
                $permpath = $permpath.'model.php';
                break;
            default:
                return false;
        }

        if(isset(self::$cache[$permpath])){
            return self::$cache[$permpath];
        }

        if(file_exists($permpath)){
            include_once($permpath);
            self::$cache[$permpath] = true;
            return true;
        }
        return false;
    }

    /**
     * Sets allow for controller and group
     *
     * @param $controller
     * @param $group
     * @param $permission boolean Allow or Deny
     *
     */
    public static function controller($controller, $group, $permission = true){
        if(is_array($controller) && !empty($controller)){
            foreach ($controller as $value) {
                self::set(self::$CONTROLLER, $value, $group, $permission);
            }
            return;
        }
        self::set(self::$CONTROLLER, $controller, $group, $permission);
    }

    /**
     * Sets allow for model and group
     *
     * @param $model string
     * @param $method string
     * @param $group string
     * @param $permission boolean Allow Or Deny
     */
    public static function model($model, $method, $group, $permission = true){
        self::set(self::$MODEL, $model.'-'.$method, $group, $permission);
    }

    /**
     * Checks permission for given resource type
     *
     * @param $type string resource type perm::$MODEL or perm::$CONTROLLER
     * @deprecated use perm::isAllowed($type) instead
     */
    public static function get($type = 'controller'){
        return self::isAllowed($type);
    }
}
