<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class msg {
	
	// Field Related Messages
	public static $msg = array();
	
	// Type of Field Related Messages - error, notice, success
	public static $msg_type = array();
	
	// All Error Messages occured during execution
	public static $error;
	
	// All Notice Messages occured during execution
	public static $notice;
	
	// All Success Messages occured during execution
	public static $success;
	
	public static function error($msg) {
		self::$error .= $msg;
	}

	public static function notice($msg) {
		self::$notice .= $msg;
	}

	public static function success($msg) {
		self::$success .= $msg;
	}
	
	public static function set($field, $msg, $type='error') {
		self::$msg[$field] = $msg;
		self::$msg_type[$field] = $type;
		switch ($type) {
			case 'error': self::error($msg); break;
			case 'notice': self::notice($msg); break;
			case 'success': self::success($msg); break;
		}
	}
	
	public static function get_all_messages(){
		$tmp = array();
		if(count(self::$msg) > 0)
		{
			$tmp['msg'] = self::$msg;
			$tmp['msg_type'] = self::$msg_type;
		}
		
		if(strlen(self::$error)>1){
			$tmp['error'] = self::$error;
		}
		if(strlen(self::$notice)>1){
			$tmp['notice'] = self::$notice;
		}
		if(strlen(self::$success)>1){
			$tmp['success'] = self::$success;
		}
		return $tmp;
	}
	
	public static function get_messages(){
		$tmp = array();
		if(strlen(self::$error) > 1){
			$tmp['msg'] = self::$error;
			$tmp['msg_type'] = 'error';
			return $tmp;
		}
		if(strlen(self::$success) > 1){
			$tmp['msg'] = self::$error;
			$tmp['msg_type'] = 'error';
			return $tmp;
		}
	}
	
}
