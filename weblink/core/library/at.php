<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');
class at {
	private $template;
	//holds the template files (one or more)
	private $content;
	//template content

	private $in_loop;

	private $compile_dir;

	private $vars;
	private $tmp_row;

	private $app;
	private $controller;
	private $in;

	/**
	 * @return object mt
	 * @param string template filename (and path)
	 * @desc Class constructor.
	 */
	function at($template = '') {
		if ($template == '') {
			$template = ark::$viewpath . ark::$controller . '.html';
			$this -> app = ark::$app;
			$this -> controller = ark::$controller;
		} else {
			$this -> app = false;
			$this -> controller = false;
		}

		$this -> template = $template;
		$this -> in_loop = 0;

		$this -> loop_map = array();

		if (FALSE === ($fp = @fopen($this -> template, 'r'))) {
			trigger_error("at: - Can not open template file: " . $this -> template);
			console::error("at: - Can not open template file: " . $this -> template);
			return false;
		}
		if (-1 === ($content = @fread($fp, @filesize($this -> template)))) {
			trigger_error("at: - Can not read template file: " . $this -> template);
			return false;
		}

		$this -> content = $content;

		$this -> compile_dir = INSTALLPATH . "cache/";
	}

	private function at_compile($html_content) {
		$count = preg_match_all('/\{([^{}]+)\}/', $html_content, $tags);
		if ($count > 0) {
			for ($i = 0; $i < $count; $i++) {
				if(!strstr($tags[0][$i],' ') && !strstr($tags[0][$i],'(') && !strstr($tags[0][$i],')')){
					$html_content = str_replace($tags[0][$i], $this -> convert($tags[1][$i]), $html_content);
				}elseif(strstr($tags[0][$i],'{if:')){
					$html_content = str_replace($tags[0][$i], $this -> convert($tags[1][$i]), $html_content);
				}
			}
		}

		return $html_content;
	}

	private function convert($tag) {
		if (!is_object($this -> vars)) {
	   		$this -> vars = new stdClass();
		}
		$tag = str_replace('/', 'end', $tag);
		$to = '$this->vars->';
		$args = explode(':', $tag, 2);

		if (!method_exists($this, $args[0] . '_convert')) {
			$func_name = 'default_convert';
		} else {
			$func_name = $args[0] . "_convert";
		}
		return '<?php ' . $this -> $func_name($to, $args) . ' ?>';
	}

	private function tag2var($to, $args) {
		if (!$this -> in_loop) {
			$string = $to . $args;
		} else {
			$string = '$v' . $this -> in_loop . "['" . $args . "']";
		}
		return $string;
	}

	private function default_convert($to, $args) {
		return 'echo ' . $this -> tag2var($to, $args[0]) . '; ';
	}

	private function l_convert($to, $args) {
		return 'ob_start();';
	}

	private function endl_convert($to, $args) {
		return '$mllabel = ob_get_clean();

		echo gm($mllabel)';
	}

	private function loop_convert($to, $args) {
		$this -> in_loop++;
		if ($this -> in_loop == 1) {
			$string = 'if(is_array(' . $to . 'loop_' . $args[1] . ')){ foreach(' . $to . 'loop_' . $args[1] . ' as $k1 => $v1 ) {';
		} else {
			$string = 'if(is_array($v' . ($this -> in_loop - 1) . "['" . 'loop_' . $args[1] . "']" . ')){ foreach( $v' . ($this -> in_loop - 1) . "['" . 'loop_' . $args[1] . "']" . ' as $k' . $this -> in_loop . ' => $v' . $this -> in_loop . ' ) {';
		}
		return $string;
	}

	private function elseloop_convert($to, $args) {
		return '} } else { {';
	}

	private function endloop_convert($to, $args) {
		$string = '} }';
		$this -> in_loop--;
		return $string;
	}

	private function if_convert($to, $args) {
		list($var, $operator, $var2) = explode(' ', $args[1], 3);
		$string = 'if(isset(' . $this -> tag2var($to, $var) . ') && ' . $this -> tag2var($to, $var);
		// $string = 'if(' . $this -> tag2var($to, $var);
		if ($operator) {
			if ($operator == '=') {
				$operator = '==';
			}
			$string .= ' ' . $operator;
		}

		if ($var2) {
			if ($var2[0] == "'") {
				$string .= ' ' . $var2;
			} else {
				$string .= ' ' . $this -> tag2var($to, $var2);
			}
		}
		$string .= ') {';

		return $string;
	}

	private function else_convert($to, $args) {
		return ' } else { ';
	}

	private function end_convert($to, $args) {
		return ' } ';
	}

	private function include_convert($to, $args) {
		$file = $args[1];
		$string = '
$data = include("' . $file . '");
if(is_string($data)){
	echo $data;
	unset($data);
}';
		return $string;
	}

	private function do_convert($to, $args) {
		$string = '
$data = ark::run("' . $args[1] . '");
if(is_string($data)){
	echo $data;
	unset($data);
}';
		return $string;
	}

	private function msg_convert($to, $args) {

		$string = '';
		switch($args[1]){
			case 'error': {
				$string = '	echo msg::$error; ';
			} break;

			case 'error-out': {
				$string = '	if(strlen(msg::$error)>1){ echo \'<div class="error">\'.msg::$error.\'</div>\'; }';
			} break;

			case 'notice': {
				$string = '	echo msg::$notice; ';
			} break;

			case 'notice-out': {
				$string = '	if(strlen(msg::$notice)>1){ echo \'<div class="notice">\'.msg::$notice.\'</div>\'; }';
			} break;

			case 'success': {
				$string = '	echo msg::$success; ';
			} break;

			case 'success-out': {
				$string = '	if(strlen(msg::$success)>1){ echo \'<div class="success">\'.msg::$success.\'</div>\'; }';
			} break;

			default: {
				if(strstr($args[1],'[')){
					$field = str_replace('[','',$args[1]);
					$field = str_replace(']','',$field);
					if(strstr($field, '-out')){
						$field = str_replace('-out','',$field);
						$string = '	if(strlen(msg::$msg[\''.$field.'\'])>1){ echo \'<div class="\'.msg::$msg_type[\''.$field.'\'].\'"><span class="msg-arrow">&nbsp;</span><p>\'.msg::$msg[\''.$field.'\'].\'</p></div>\'; }';
					}
					else{
						$string = '	echo msg::$msg[\''.$field.'\']; ';
					}

				}
			}
		}
		return $string;
	}
	private function ifmsg_convert($to, $args) {
		$string = '';
		switch($args[1]){
			case 'error': {
				$string = '	if(strlen(msg::$error)>1){ ';
			} break;

			case 'notice': {
				$string = '	if(strlen(msg::$notice)>1){ ';
			} break;

			case 'success': {
				$string = '	if(strlen(msg::$success)>1){ ';
			} break;

			default: {
				if(strstr($args[1],'[')){
					$field = str_replace('[','',$args[1]);
					$field = str_replace(']','',$field);
					$string = '	if(strlen(msg::$msg[\''.$field.'\'])>1){ ';
				}
			}
		}
		return $string;
	}
	private function msgtype_convert($to, $args) {
		$string = '';
		if(strstr($args[1],'[')){
			$field = str_replace('[','',$args[1]);
			$field = str_replace(']','',$field);
			$string = '	echo msg::$msg_type[\''.$field.'\']; ';
		}
		return $string;
	}

	private function in_convert($to, $args) {
		$args[1] = str_replace(']','',$args[1]);
		$tmp=explode('[',$args[1]);
		$val='$in[\''.$tmp[0].'\']';
		if(count($tmp)>1){
			foreach($tmp as $k=>$v){
				if($k!=0){
					if(substr($v, 0, 1)=="'"){
						$v=str_replace("'",'',$v);
						$val.='['.$this->tag2var($to, $v).']';
					} else {
						$val.='[\''.$v.'\']';
					}

				}
			}
		}
		$string = 'echo htmlspecialchars(html_entity_decode(' .$val. ')); ';
		return $string;
	}

	public function assign($val1, $val2 = null, $val3 = null) {
		//florin vars 3 rows added
		if (!is_object($this -> vars)) {
	   		$this -> vars = new stdClass();
		}
		$lp = 'loop_';
		if ($val3 == null) {
			if ($val2 != null) {
				if (!is_array($val1)) {
					// we have the simplest case: assign('key', value);
					$this -> vars -> $val1 = $val2;
					return true;
				} else {
					/* we have:
					* assign(array('key1'=>'value1',
					* 				'key2'=>'val2'
					*               .............), 'row_name');
					*/
					if (empty($this -> tmp_row[$lp . $val2]) || !is_array($this -> tmp_row[$lp . $val2])) {
						$this -> tmp_row[$lp . $val2] = $val1;
						return true;
					}
					$this -> tmp_row[$lp . $val2] = array_merge($this -> tmp_row[$lp . $val2], $val1);
					return true;
				}
			} else {
				if (is_array($val1)) {
					/* we have:
					* assign(array('key1'=>'value1',
					* 				'key2'=>'val2'
					*               .............));
					*/
					foreach ($val1 as $k => $v) {
						$this -> vars -> $k = $v;
					}
					return true;
				}
				return false;
			}
		} else {
			if (!is_array($val1)) {
				// we have this case: assign('key', 'value', 'row_name');
				$arr = array($val1 => $val2);
				if (!is_array($this -> tmp_row[$lp . $val3])) {
					$this -> tmp_row[$lp . $val3] = $arr;
					return true;
				}
				$this -> tmp_row[$lp . $val3] = array_merge($this -> tmp_row[$lp . $val3], $arr);
				return true;
			}
		}

		return false;
	}

	public function loop($val1, $val2 = null, $val3 = null) {
		//florin vars 3 rows added
		if (!is_object($this -> vars)) {
	   		$this -> vars = new stdClass();
		}
		$lp = 'loop_';
		if ($val3 != null && is_array($val1)) {
			// we have loop( array(), loop_name, parent);
			$this -> assign($val1, $val2);
			$this -> loop($val2, $val3);
			return true;
		}
		if ($val2 == null) {
			if (!is_array($val1)) {
				// we have loop( loop_name);
				$loop = $lp . $val1;
				if (empty($this-> vars -> $loop) || !is_array($this -> vars -> $loop)) {
					$this -> vars -> $loop = array();
				}
				array_push($this -> vars -> $loop, $this -> tmp_row[$loop]);
				unset($this -> tmp_row[$loop]);
				return true;
			} else {
				return false;
			}
		} elseif (is_array($val1)) {
			// we have loop( array(), loop_name);
			$loop = $lp . $val2;
			if (!is_array($this -> vars -> $loop)) {
				$this -> vars -> $loop = array();
			}
			array_push($this -> vars -> $loop, $val1);
			return true;
		} else {
			// we have loop( loop_name, parent);
			$this -> tmp_row[$lp . $val2][$lp . $val1][] = $this -> tmp_row[$lp . $val1];
			unset($this -> tmp_row[$lp . $val1]);
		}
	}

	public function fetch() {
		global $in;


		if (!file_exists($this -> compile_dir))
		mkdir($this -> compile_dir);

		if (!$this -> controller) {
			$compiled_file = $this -> compile_dir . str_replace('/', '-', $this -> template . '.php');
			$compiled_file = str_replace('..-', '', $compiled_file);
			$compiled_file = str_replace('apps-', '', $compiled_file);
			$compiled_file = str_replace('view-', '', $compiled_file);
		} else {
			$compiled_file = $this -> compile_dir;
			if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) { $compiled_file .= ENVIRONMENT . '-';
			}
			if (defined('USE_APPS') && USE_APPS == true) { $compiled_file .= $this -> app . '-';
			}
			$compiled_file .= $this -> controller . '.html.php';

		}

		if (!file_exists($compiled_file) || filemtime($compiled_file) < filemtime($this -> template)) {
			$compiled = $this -> at_compile($this -> content);
			$cf = fopen($compiled_file, 'w');
			fwrite($cf, $compiled);
			fclose($cf);
		}
		ob_start();
		include ($compiled_file);
		$out = ob_get_clean();
		return $out;
	}

	public function out() {
		$page = $this->fetch();
		if(defined('DEBUG') && DEBUG == '1'){
			console::error(msg::$error);
			console::memory();
			console::speed('Load time');
			$debug = console::getInstance()->render();
			$page=str_replace('</body>', $debug.'</body>', $page);
		}
		echo $page;
	}

}
function  gm($str,$out=false){
	# for development improvement we will leave multilanguage for now
	// return utf8_encode($str);
	global $lang,$in,$cfg;
	global $database_config;
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$dbu = new sqldb($db_config);
	$db = new sqldb();
	$base64_key = base64_encode($str);
	if($out){
		$dbu->query("SELECT * FROM langs WHERE id='".$base64_key."' ");
		$response = $dbu->getAll();
		return $response;
	}
	if(!empty($lang[$base64_key])) {
		return utf8_encode($lang[$base64_key]);
	}
	else{
		$dbu->query("SELECT * FROM langs WHERE id='".$base64_key."' ");
		if(!$dbu->move_next()){
			$data = '/* '.$str.' */';
			$data .= "\n"."\$lang['".$base64_key."'] = '".addslashes($str)."';\n";
			$dbu->query('SELECT code FROM lang');
			while ($dbu->move_next()) {
				file_put_contents('../lang/lang_'.$dbu->f('code').'.php',$data,FILE_APPEND);
			}
			$dbu->query("INSERT INTO langs set id='".$base64_key."', value='".addslashes($str)."', en='".addslashes($str)."', pag='".ark::$controller."' ");
		}
		return utf8_encode($str);

	}
//ENVIRONMENT
}

?>
