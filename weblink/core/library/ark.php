<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ark {

	// for debugging purposes only
	public static $dolist = array();
	public static $dotime = array();

	// layout
	public static $layout;

	// currently executed app / controller / model / method
	public static $app;
	public static $controller;
	public static $model;
	public static $method;

	// model view controller filepaths
	public static $apppath;
	public static $modelpath;
	public static $controllerpath;
	public static $viewpath;

	private static $_loadedLibs = array();

	public static function run($do = '') {

		global $in, $default_app, $default_controller;
		$start = console::getMicroTime();

		if ($do == '') {
			if (empty($in['do'])) {
				if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
					$dctrl = $default_controller[ENVIRONMENT];
					if (defined('USE_APPS') && USE_APPS == true) {
						$dapp = $default_app[ENVIRONMENT];
					} else {
						$dapp = false;
					}
				} else {
					$dctrl = $default_controller;
					if (defined('USE_APPS') && USE_APPS == true) {
						$dapp = $default_app;
					} else {
						$dapp = false;
					}
				}

				if ($dapp) {
					$in['do'] = $dapp . '-' . $dctrl;
				} else {
					$in['do'] = $dctrl;
				}
			}
			$do = $in['do'];
		}

		self::get_acmm($do);

		self::$apppath = APPSPATH . self::$app . '/';

		if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
			self::$modelpath = self::$apppath . ENVIRONMENT . '/model/';
			self::$controllerpath = self::$apppath . ENVIRONMENT . '/controller/';
			self::$viewpath = self::$apppath . ENVIRONMENT . '/view/';
		} else {
			self::$modelpath = self::$apppath . 'model/';
			self::$controllerpath = self::$apppath . 'controller/';
			self::$viewpath = self::$apppath . 'view/';
		}

		if(DEBUG==true){
			self::debug($do);
		}

		if (self::$model) {
			if(perm::get('model')){
				$mod = self::load_model(self::$model, $in);
				if (self::$method) {
					$met = self::$method;
					if(method_exists($mod, $met)){
					    $mod->$met($in);
					} else {
						// method does not exist
						console::error('Method does not exist');
					}

				}
			} else {
				// msg::$error = gm("You don't have access");
				// error: you do not have permissions to run this action
				console::error("You don't have permissions to run this action");
			}

		}

		if (self::$controller) {
			global $auth_app, $auth_controller;
			if(defined('USE_APPS') && USE_APPS == true){
				$auth = $auth_app.'-'.$auth_controller;
			} else {
				$auth = $auth_controller;
			}

			if(perm::get('controller')){
				$page = self::load_controller(self::$controller, $in);
			} elseif($do == $in['do']){				
				// return login form
				$in['do'] = $auth;
				self::$dotime[]=console::getEndMicroTime($start);
				return ark::run();
			} else {
				// msg::$notice = "You don't have permission to access this page";
				self::$dotime[]=console::getEndMicroTime($start);
				return '';
			}
		} else {
			return '';
		}


		if($do == $in['do']){
			self::$layout = self::get_layout($in['do']);
		}
		self::$dotime[]=console::getEndMicroTime($start);
		return $page;
	}

	public static function loadLibraries($libraries = array(),$libPath =''){
		$libraryPath = INSTALLPATH.'libraries/';
		if($libPath){
			$libraryPath = $libPath;
		}
		if(empty($libraries)){
			include(INSTALLPATH.'config/startup.php');
			if(!isset($load)){
				return false;
			}
		}else{
			$load = array('libraries' => $libraries);
		}
		foreach ($load['libraries'] as $library) {
			if(isset(self::$_loadedLibs[$library])){
				continue;
			}
			if(!file_exists($libraryPath.$library.'.php')){
				console::error('Could not load library '.$library);
			}else{
				require($libraryPath.$library.'.php');
				self::$_loadedLibs[$library] = true;
			}
		}

	}

	private static function get_layout($do){

		if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
	      	$layout_path = self::$apppath . ENVIRONMENT . '/layout/';
		} else {
			$layout_path = self::$apppath . ENVIRONMENT . '/layout/';
		}

		/*
		if(file_exists($layout_path.self::$controller.'.html')){
			return $layout_path.self::$controller.'.html';
		}
		*/

		$act = explode('-', $do);

		if(file_exists($layout_path.$act[1].'.html')){
			return $layout_path.$act[1].'.html';
		}

		if(file_exists($layout_path.'template.html')){
			return $layout_path.'template.html';
		}

		if(file_exists(LAYOUT.ENVIRONMENT.'_template.html')){
			return LAYOUT.ENVIRONMENT.'_template.html';
		}
	}

	private static function get_acmm($do) {
		global $apps;
		$actions = explode('-', $do);
		

		if ($actions[0] == 'this') {
			global $in;
			$act = explode('-', $in['do']);
			$actions[0] = $act[0];
		}
		// if the first parameter is a registered app
		if (defined('USE_APPS') && USE_APPS == true && $apps[$actions[0]]) {
			self::$app = $actions[0];
			if(strlen($actions[1])>1){
				self::$controller = $actions[1];
			}else{
				self::$controller = false;
			}
			self::$model = null;
			self::$method = null;
			if(!empty($actions[2])){
				self::$model = $actions[2];
			}
			if(!empty($actions[3])){
				self::$method = $actions[3];
			}
		} else {
			// we are in No Apps mode
			self::$app = '';
			if(strlen($actions[0])>1){
				self::$controller = $actions[0];
			}else{
				self::$controller = false;
			}
			self::$model = null;
			self::$method = null;
			if(!empty($actions[1])){
				self::$model = $actions[1];
			}
			if(!empty($actions[2])){
				self::$method = $actions[2];
			}
		}
	}

	private static function load_model($m, &$in) {
		if(!empty($in['skip_action'])){
		   return false;
		}

		if (file_exists(self::$modelpath . $m . '.php')) {
			include_once (self::$modelpath . $m . '.php');
		} else {
			// error can not find file
			console::error('Cannot find model file');
			return false;
		}
		if(class_exists($m)){
			$m = new $m($in);
			return $m;
		}
		console::error('Model class does not exist');
		// model class does not exist
		return false;
	}

	private static function load_controller($c, &$in) {
		if (file_exists(self::$controllerpath . $c . '.php')) {
			$view=new at();
			$db = new sqldb();
			$c = include (self::$controllerpath . $c . '.php');
		} else {
			if(ENVIRONMENT=="admin"){
				global $default_app;
				if(defined('USE_APPS') && USE_APPS == true){
					$home = $default_app['admin'].'-error';
				} else {
					$home = 'error';
				}
				$in['do'] = $home;
				$in['error'] = '404';
				// error can not find file
				// return false;
				# error can not find file => redirect the user to ther 404 page
				console::error('Page not found');
				return ark::run();
			}
			console::error('Page not found');
			return false;
		}
		return $c;
	}

	private static function debug($do){
        $entry = array(
            'do' => str_replace('this','this('.self::$app.')',$do),
            'app' => self::$app,
            'apppath' => self::$apppath,
            'controller' => self::$controller,
            'controllerpath' => self::$controllerpath . self::$controller . '.php',
            'model' => self::$model,
            'modelpath' =>  self::$modelpath. self::$model . '.php',
            'method' => self::$method
        );
        array_push(self::$dolist,$entry);
    }
}
