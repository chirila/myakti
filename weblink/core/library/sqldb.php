<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * DooHoo
 * An open-source PHP framework.
 *
 * @package DooHoo
 * @copyright Copyright (c) Medeeaweb Works
 * @since Version 1.0
 */

/**
 * DooHoo PDO Wrapper Class
 */

class sqldb{
	/**
	 * hold the host where the MySql server is located
	 * @var string
	 * @access private
	 */
	private $host;
	/**
	 * store de user name required for connection
	 * @access private
	 * @var string
	 */
	private $user;
	/**
	 * password required by the username
	 * @access private
	 * @var string
	 */
	private $pass;
	/**
	 * database to connect
	 * @access private
	 * @var unknown_type
	 */
	private $database;

	/**
	 * database server type
	 * @access private
	 * @var unknown_type
	 */
	private $db_type;

	/**
	 * database server pot
	 * @access private
	 * @var unknown_type
	 */
	private $db_port;

	/**
     * If set to true it will stop the execution of sql's onerror
     *
     * @var bool
     */
    public $exitOnError = true;

	/**
	 * placeholder for prepared statements
	 * @access private
	 * @var string
	 */
	private $p_statements;

	/**
	 * static variable that stores the database connection to avoid creating multiple connections
	 * @access private static
	 * @var unknown_type
	 */
	private static $pdo = array();

    /**
     * static variable that stores the database store, which is used for debugging
     */
    private $store;

	/**
	 * holds the execution times of all executions
	 * @access private
	 * @var sqldbResult
	 */
	 	private $results;

	/**
	 * Transaction flag. Values: true - if transaction is started. false - if there is no transaction.
	 * @access private
	 * @var sqldbResult
	 */
	private $in_transaction;

	public function __construct($db_config = ""){
		global $database_config;
		if($db_config == ""){
	    	$this->db_type=$database_config['default'];
	    	$this->host=$database_config[$this->db_type]['hostname'];
	    	$this->user=$database_config[$this->db_type]['username'];
	    	$this->pass=$database_config[$this->db_type]['password'];
	    	$this->database=$database_config[$this->db_type]['database'];
		}else{
	    	$this->db_type=$database_config['default'];
	    	$this->host=$db_config['hostname'];
	    	$this->user=$db_config['username'];
	    	$this->pass=$db_config['password'];
	    	$this->database=$db_config['database'];
		}

		if(empty(self::$pdo[$this->database]) && !isset(self::$pdo[$this->database])){
			try{
			    self::$pdo[$this->database] = new PDO("mysql:host=".$this->host.";dbname=".$this->database, $this->user, $this->pass);;
				self::$pdo[$this->database]->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				# this should be mandatory for the new projects
				# but if i change it for sa it will just mess up everything
				// self::$pdo[$this->database]->query("SET names utf8");
				// self::$pdo[$this->database]->query("SET sql_mode= ''");
			}
			catch(PDOException $e){
				/*if(defined('INSTALED') === false){
					$this->is_connect = false;
					return false;
				}*/
				$this->raiseError($e);
			}

		}
		$this->in_transaction = false;
        if(!class_exists('database_store')){
            include('database_store.php');
        }
        $this->store = database_store::getInstance();
	}

	public function field($statement, array $params=array()){
		$res = $this->query($statement,$params);
		return $res->f(0);
	}

	public function query($statement, array $params=array()){
		if(!empty($params)){
			//if we have prepared statements
			if(!$this->p_statements[$statement]){
				// if this statement hasn't been prepared before
				try{
					$prepared_statement = self::$pdo[$this->database]->prepare($statement);
				}
				catch(PDOException $e){
					if($this->exitOnError){
			    		$this->raiseError($e, $statement.' : '.implode(';',$params));
			    	}else{
			    		return false;
			    	}				}
				$this->p_statements[$statement] = $prepared_statement;
			}
			else{
				$prepared_statement = $this->p_statements[$statement];

			}

			foreach($params as $k => $v){
				if(!get_magic_quotes_gpc()){
				    $params[$k] = mysql_real_escape_string($v);
                }
			}
            $start = $this->getTime();
			try{
				$prepared_statement->execute($params);
			}
			catch(PDOException $e){
				if($this->exitOnError){
					$this->raiseError($e, $statement.' : '.implode(';',$params));
				}else{
					return false;
				}
			}
            $this->logQuery($statement, $params, $start);
			$this->results = new sqldbResult($prepared_statement);
			return $this->results;
		}
		else{
		    $start = $this->getTime();
			try{
				$results = self::$pdo[$this->database]->query($statement);
			}
			catch(PDOException $e){
				if($this->exitOnError){
			    	$this->raiseError($e, $statement);
				}else{
					return false;
				}			}
			$this->logQuery($statement, $params, $start);

			$this->results = new sqldbResult($results);
			return $this->results;
		}
	}
	public function logQuery($statement, array $params = array(), $start){
		if(LOG_QUERYS == 'NO'){
			return true;
		}
		$x = debug_backtrace(true);
		if( isset($x['2']) && ($x['2']['function'] == 'field' || $x['2']['function'] == 'insert') ) {
			$f = $x['2']['file'];
			$l = $x['2']['line'];
		}else{
			$f = $x['1']['file'];
			$l = $x['1']['line'];
		}

		$query = array(
            'sql' 		=> $statement,
            'time'		=> ($this->getTime() - $start)*1000,
            'database'	=> $this->database,
            // 'file'		=> '<pre>'.print_r($x,true).'<pre>',
            'file'		=> $f,
            'line'		=> $l,        );
        $this->store->push($query);
	}

    public function explain($strQuery){
        if($this->is_write_type($strQuery)){
            return false;
        }
        return $this->query('EXPLAIN '.$strQuery);
    }

    /**
     * Determines if a query is a "write" type.
     *
     * @param   string  An SQL query string
     * @return  boolean
     */
    public static function is_write_type($sql){
        if(!preg_match('/^\s*"?(SET|INSERT|UPDATE|DELETE|REPLACE|CREATE|DROP|LOAD DATA|COPY|ALTER|GRANT|REVOKE|LOCK|UNLOCK)\s+/i', $sql)){
            return false;
        }
        return true;
    }

    protected function getTime() {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
        return $start;
    }

	public function insert($statement, array $params=array()){
		$this->query($statement, $params);
		return self::$pdo[$this->database]->lastInsertId();
	}

	# use it when you wanna know how many entries you fucked up
	public function affected_rows($statement, array $params=array()){
		$start = $this->getTime();
		$this->logQuery($statement, $params, $start);
		return self::$pdo[$this->database]->exec($statement);
	}

	public function beginTransaction(){
		$this->in_transaction = true;
		return self::$pdo[$this->database]->beginTransaction();
	}

	public function commit(){
		$this->in_transaction = false;
		return self::$pdo[$this->database]->commit();
	}

	public function rollBack(){
		$this->in_transaction = false;
		return self::$pdo[$this->database]->rollBack();
	}

	public function getAll(){
		return $this->results->getAll();
	}

	public function next(){
		return $this->results->next();
	}

	public function move_next(){
		return $this->results->next();
	}

	public function f($field){
		return $this->results->f($field);
	}

	public function gf($field){
		return $this->results->gf($field);
	}

	public function records_count(){
		return $this->results->records_count();
	}

	public function move_to($row_number=0){
		return $this->results->move_to($row_number);
	}

	private function raiseError($error, $statement=''){
		if($this->in_transaction)
		{
			$this->in_transaction = false;
			$this->rollBack();
		}
		global $site_url;
    global $config;
    global $in;

    if(!$config['debug']){
    	ark::loadLibraries(array('class.phpmailer','class.smtp'));
      	$mail = new PHPMailer();
		$fromMail='support@akti.com';
	    $mail->SetFrom($fromMail, 'SalesAssist');

	   	$body = 'SQLSTATE: '.$error->errorInfo[0].'<br>';
			$body.= 'Syntax error or access violation: '.$error->errorInfo[1].'<br>';
			$body.= '<span>'.$error->errorInfo[2].'</span>';
			if($statement != ''){
				$body.= '<p>SLQ: <span><strong>'.$statement.'</strong></span>';
			}
			$body .="<br>Post and Get:<pre>".list_content($in)."<br>Session:<br>".list_content($_SESSION)."</pre>";

	    $subject= ' [SalesAssist] Database error - '.date('d/m/Y H:m:s');
	    $mail->Subject = $subject;
	    $mail->MsgHTML(nl2br($body));

	    $mail->AddAddress(TEHNICAL_SUPPORT);
	    $mail->AddAddress('tibi@medeeaweb.com');
	    //$mail->AddCC('support@akti.com');

	    $mail->Send();
	    header('Location: index.php');
	  }else{		if(empty($error->errorInfo[0])){
			$error_message = $error->getMessage();
		}
		else{
			$error_message = 'SQLSTATE: '.$error->errorInfo[0].'<br>';
			$error_message.= 'Syntax error or access violation: '.$error->errorInfo[1].'<br>';
			$error_message.= '<span>'.$error->errorInfo[2].'</span>';
			if($statement != ''){
				$error_message.= '<p>SLQ: <span><strong>'.$statement.'</strong></span>';
			}
		}
		$html = '<style type="text/css">
#container{
	background-color:#F5F5F5;
	border:1px solid #990000;
	width:90%;
	margin:0 auto;
	margin-top:20px;
	padding:10px;
}
	#container h1{
		font-size:28px;
		font-family:Arial, Helvetica, sans-serif;
		color:#990000;
		border-bottom:1px solid;
		padding:0px;
		margin:0px;
	}
	#container p{
		font-size:12px;
		font-family:Arial, Helvetica, sans-serif;
		color: #000000;
	}
	#container p span{
		color: #990000;
	}
	</style>
<div id="container">
<h1>sqlDB Error:</h1>
<p>'.$error_message.'</p>
</div>';
		echo $html;
		exit();
	}
	}

	/**
	 *
	 * attempt to kill the connection
	 *
	 */
	function killConn(){
		self::$pdo[$this->database] = null;
	}

}

/* End of file sqldb.php */
