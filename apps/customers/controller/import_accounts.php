<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}


	function get_import($in,$showin=true,$exit=true){
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
				set_time_limit(0);
    ini_set('memory_limit', '-1');   
		$db = new sqldb();
		$db2 = new sqldb();
		$data = array('crm'=>array());
		$in['crm'] = '0';
/*		if($in['step']==3){
			$data['step']=$in['step1'];
		}*/
		if(!$in['step']){
			$step=1;
		}else{
			$step=$in['step'];
		}
		if($step==1){
			$class1="text-primary";
			$class2="text-muted";
			$class3="text-muted";
		}elseif($step==2){
			$class1="text-muted";
			$class2="text-primary";
			$class3="text-muted";
		}elseif($step==3){
			$class1="text-muted";
			$class2="text-muted";
			$class3="text-primary";
		}
		$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml';
		$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml';
		$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml';
		$file_fill1 = file_get_contents($file1);
		$file_fill2 = file_get_contents($file2);
		$file_fill3 = file_get_contents($file3);

		if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
			$show_file=1;
		}
		$zen_active=$db->field("SELECT active FROM apps WHERE name='Zendesk' AND main_app_id='0' and type='main' ");
		$nylas_active=$db->field("SELECT active FROM apps WHERE name='Nylas' AND main_app_id='0' and type='main' ");
		//$user_nylas=$db_users->field("SELECT active FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$user_nylas=$db_users->field("SELECT active FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$data['crm'] = array(
			'file'		=>    backupcrm2($in['crm']),
			'file_id'	=>	  $in['crm'],
			'show_file'	=>    $show_file==1?true:false,
			'class1'	=> 	  $class1,
			'class2'	=> 	  $class2,
			'class3'	=> 	  $class3,
			'step'		=>    $in['step']==3 ? 3 : $step,
			'history'	=>    array(),
			'is_nylas'	=> ($nylas_active && $user_nylas) ? true : false,
			'is_zendesk' => $zen_active==1? true : false,
		);

		$db->query("SELECT company_import_log.*	FROM company_import_log	ORDER BY company_import_log.company_import_log_id DESC ");

		$z=0;
		while($db->move_next()){
			$data['crm']['history'][] = array(
			'date'	        	=> date(ACCOUNT_DATE_FORMAT,$db->f('date')),
			'id'				=> $db->f('company_import_log_id'),
			'filename'	    	=> $db->f('filename'),
			'company_added'		=> $db->f('company_added'),
			'add_company'		=> 'companies_add',
			'company_updated' 	=> $db->f('company_updated'),
			'update_company'	=> 'companies_update',
			'contact_added'		=> $db->f('contact_added'),
			'add_contact'		=> 'contacts_add',
			'contact_updated'	=> $db->f('contact_updated'),
			'update_contact'	=> 'contacts_update',
			'username'			=> $db->f('username'),
			'delete_link'	  	=> 'index.php?do=company-import_accounts-customer-delete_import_company&company_import_log_id='.$db->f('company_import_log_id').'&tab=0',
			'type'				=> $db->f('log_type') ? gm('Export') : gm('Import')
			);
			$z++;
		}

		$data['multiple_addresses']=true;
		$data['delete_existent_addresses'] =0;


			
		return json_out($data, $showin,$exit);
	}
	function get_history_import($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db2 = new sqldb();
		$data = array('list_history'=>array());

		if($in['import_id']){
		
			if((!$in['offset']) || (!is_numeric($in['offset'])))
			{
			    $offset=0;
			    $in['offset']=1;
			}
			else
			{
			    $offset=$in['offset']-1;
			}

			$l_r = 10;

			$all_items = $db2->field("SELECT ".$in['field']."
						FROM company_import_log
						WHERE company_import_log_id='".$in['import_id']."'
					   ");

			if($in['field']=='companies_add' || $in['field']=='companies_update')
			{
				$table = 'customers';
				if($in['field']=='companies_add')
				{
					$header_name = 'Companies added';
				}else
				{
					$header_name = 'Companies updated';
				}
			}else
			{
				$table = 'customer_contacts';
				if($in['field']=='contacts_add')
				{
					$header_name = 'Contacts added';
				}else
				{
					$header_name = 'Contacts updated';
				}
			}
				$order_by = " ORDER BY sort_order ASC";
				if($in['order_by']){
					$order = " ASC ";
					if($in['desc']){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
					$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
					$view_list->assign(array(
						'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
					));
				}
			$all_items= unserialize($all_items);

			$j=$l_r*$offset;
			$max_rows = count($all_items);
			$data['max_rows'] =  $max_rows;
			$data['lr']=$l_r;
			while ($j>=$l_r*$offset && $j<($l_r*$offset)+$l_r )
			{

				if($all_items[$j])
				{

					if($table == 'customers')
					{

						$item_name = $db2->field("SELECT name FROM $table WHERE customer_id='".$all_items[$j]."'");
					}else
					{
						$db2->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$all_items[$j]."'  LIMIT ".$offset*$l_r.",".$l_r);
						$db2->move_next();
						$item_name = $db2->f('firstname')." ".$db2->f('lastname');
					}

					$data['list_history'][] = array(
							'item_name' => $item_name,
					);
					$j++;
				}else
				{
					break;
				}
			}
		}

		return json_out($data, $showin,$exit);
	}
		$result = array(
		'import'				=> get_import($in,true,false),
		'history_import'		=> get_history_import($in,true,false),

	);
json_out($result);
?>