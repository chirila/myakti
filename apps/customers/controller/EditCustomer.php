<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();
global $database_config;
$database_users = array(
'hostname' => $database_config['mysql']['hostname'],
'username' => $database_config['mysql']['username'],
'password' => $database_config['mysql']['password'],
'database' => $database_config['user_db'],
);
$db2 = new sqldb($database_users);

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
if($in['contact_id']){
	$customer = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ")->getAll();
	$customer[0]['name']		=$customer[0]['lastname'];
	$customer[0]['comp_phone']	=$customer[0]['phone'];
	$customer[0]['comp_fax']	=$customer[0]['fax'];
	$customer[0]['c_email']		=$customer[0]['email'];
}else{
	$customer = $db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ")->getAll();
}

if(!$customer[0]['customer_lead_source']){
	$customer[0]['customer_lead_source']=undefined;
}
if(!$customer[0]['sector']){
	$customer[0]['sector']=undefined;
}
if(!$customer[0]['language']){
	$customer[0]['language']=undefined;
}
if(!$customer[0]['lead_source']){
	$customer[0]['lead_source']=undefined;
}
if(!$customer[0]['activity']){
	$customer[0]['activity']=undefined;
}
if(!$customer[0]['various1']){
	$customer[0]['various1']=undefined;
}
if(!$customer[0]['various2']){
	$customer[0]['various2']=undefined;
}
if(!$customer[0]['legal_type']){
	$customer[0]['legal_type']=undefined;
}
if($customer[0]['is_customer']==1){
	$customer[0]['is_customer']=true;
}else{
	$customer[0]['is_customer']=false;
}

if($customer[0]['is_supplier']==1){
	$customer[0]['is_supplier']=true;
}else{
	$customer[0]['is_supplier']=false;
}
if($customer[0]['acc_manager_name']){
	$customer[0]['acc_manager'] = explode(',', $customer[0]['acc_manager_name']);
	$customer[0]['user_id'] = explode(',', $customer[0]['user_id']);
}
if($customer[0]['c_type']){
	$customer[0]['c_type'] = explode(',', $customer[0]['c_type']);
}
// var_dump($customer[0]['vat_id']);
if(!$customer[0]['vat_id']){
	$vat = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."' ");
	$customer[0]['vat_id'] = $vat;
}

/*if(!$customer[0]['credit_limit']){
	$customer[0]['credit_limit']='';
}*/

if(!$customer[0]['currency_id']){
	$customer[0]['currency_id'] = ACCOUNT_CURRENCY_TYPE;
}
$customer[0]['currency_code']=currency::get_currency($customer[0]['currency_id']);

if($customer[0]['creation_date']){
	$customer[0]['creation_date'] = date(ACCOUNT_DATE_FORMAT,$customer[0]['creation_date']);
}

if($customer[0]['apply_fix_disc']=='1'){
	$customer[0]['apply_fix_disc']=true;
}

if($customer[0]['apply_line_disc']=='1'){
	$customer[0]['apply_line_disc']=true;
}

$customer[0]['payment_term_start'] = $customer[0]['payment_term_type'];

$is_siret = false;
	$country_id = $db->field("SELECT country_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' ");
	if( $country_id=='79' || $country_id=='132' ){ //france and luxembourg
		$is_siret = true;
	}	


$label = $db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY field_id ");
$all = $label->getAll();
$customer[0]['extra']=array();
$result['extra'] = array();
foreach ($all as $key => $value) {
	$f_value = $db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$value['field_id']."' ");	
	if($value['is_dd']==1){
		$result['extra'][$value['field_id']]= build_extra_field_dd('',$value['field_id']);
		if(!$f_value){
			$f_value = undefined;
		}
	}
	$customer[0]['extra'][$value['field_id']]=$f_value;
}
if($in['customer_id'] == 'tmp'){
	$do_next ='customers-EditCustomer-customers-addCustomer';
	$is_add = true;
}else{
	$do_next ='customers-EditCustomer-customers-updateCustomer';
	$is_add = false;
	$address = $db->query("SELECT * FROM  customer_addresses WHERE customer_id='".$in['customer_id']."' and is_primary=1 ")->getAll();
	foreach ($address[0] as $key => $value) {
		$customer[0][$key] = $value;
	}
}
if($in['contact_id']){
	$address = $db->query("SELECT * FROM  customer_contact_address WHERE contact_id='".$in['contact_id']."'")->getAll();
		$customer[0]['address'] = $address[0]['address'];
		$customer[0]['zip'] 	= $address[0]['zip'];
		$customer[0]['city'] 	= $address[0]['city'];
}


$result['vat_0'] 					= $db->field("SELECT vat_id FROM vats where value='0.00'");
if($customer[0]['internal_language']>1000){
  $result['internal_language_table']='pim_custom_lang';
}else{
  $result['internal_language_table']='pim_lang';
}

$ADV_PRODUCT = $db->field("SELECT value FROM  settings WHERE constant_name='ADV_PRODUCT'");
$multiple_identity = $db->field("SELECT COUNT(identity_id) FROM multiple_identity");

if($multiple_identity==0){
	$hide_identity = false;
}else{
	$hide_identity = true;
}

if($in['exist_company']){
	$result['exist_company']=$in['exist_company'];
}

$result['invoice_email_type']		= $customer[0]['invoice_email_type'];
$result['invoice_email']			= $customer[0]['invoice_email'];
$result['attention_of_invoice']		= $customer[0]['attention_of_invoice'];
$result['contact_id']				= $in['contact_id'];
$result['customer_contact_language']= build_language_dd();
$result['customer_lead_source']		= build_l_source_dd();
$result['customer_activity']		= build_c_activity_dd();
$result['various1']					= build_various_dd($in['various1'],'1');
$result['various2']					= build_various_dd($in['various2'],'2');
$result['customer_legal_type']		= build_l_type_dd();
$result['customer_sector']			= build_s_type_dd();
$result['customer_type']			= getCustomerType();
$result['accountManager']			= getAccountManager();
$result['sales_rep']				= getSales();
$result['vat_dd']					= build_vat_dd();
$result['vat_regim_dd']				= build_vat_regime_dd();
$result['currency_dd']				= build_currency_list();
$result['cat_dd']					= build_cat_dd();
$result['lang_dd']					= build_language_dd_new();
$result['multiple_dd'] 				= build_identity_dd();
$result['is_siret']					= $is_siret;
$result['is_add']					= $is_add;
$result['is_admin']					= $_SESSION['access_level'] == 1 ? true : false;
$result['ADV_PRODUCT']				= $ADV_PRODUCT == 1 ? true : false;
$result['hide_identity']			= $hide_identity;
$result['obj']						= $customer[0];
if(!$result['obj']['type']){
	$result['obj']['type'] 			= $in['types']==1 ? '1' : '0';
}
if($result['obj']['language'] == 'undefined'){
	$result['obj']['language'] 	= $db->field("SELECT lang_id FROM pim_lang where pdf_default=1");
}
	$result['atachment'] = array();
	$atachment = $db->query("SELECT * FROM attached_files WHERE type='0' AND REPLACE(REPLACE(path,'".INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/customer/',''),'/','')='".$in['customer_id']."' ");
	while ($atachment->next()) {
		array_push($result['atachment'], array(
			'file'		=> $atachment->f('name'),
			'checked'	=> $atachment->f('default') == 1 ? true : false,
			'file_id'	=> $atachment->f('file_id'),
			//'path'		=> INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/customer/'.$in['customer_id'].'/'.$atachment->f('name'),
			'path'		=> $atachment->f('path').$atachment->f('name'),
			//'checkeddrop'		=> $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_DROP_BOX' ") == '1' ? true : false,
		));
	}
/*if($in['updateFromFinancial']==false){
	$in['updateFromFinancial']=false;
}elseif($in['updateFromFinancial']==""){
	$in['updateFromFinancial']=true;
}elseif($in['updateFromFinancial']==true){
	$in['updateFromFinancial']=true;
}*/

$result['obj']['add_customer']		= $in['updateFromFinancial'] == false ? true : false;
$result['obj']['add_contact']		= $in['updateFromFinancial1'] == true ? true : false;
$result['obj']['add_notes']			= $in['updateFromFinancial2'] == true ? true : false;
$result['obj']['country_dd']		= build_country_list(0);
//$result['obj']['country_id']		= $result['obj']['country_id'] ? $result['obj']['country_id'] : ACCOUNT_BILLING_COUNTRY_ID;
$result['obj']['country_id']		= $result['obj']['country_id'] ? $result['obj']['country_id'] : ACCOUNT_DELIVERY_COUNTRY_ID;
$result['obj']['do']				= $do_next;
$result['obj']['vies_ok'] 			= $customer[0]['check_vat_number'] && ($customer[0]['check_vat_number'] ==  $customer[0]['btw_nr']) && !$customer[0]['vat_form'] ? true : false;
$result['obj']['do_financial']		= 'customers-EditCustomer-customers-update_financial';
$result['obj']['do_notes']					= 'customers-EditCustomer-customers-customer_notes';
$result['obj']['customer_notes']			= $customer[0]['customer_notes'];

$i=0;
$default_show=array();
if($in['customer_id']=='tmp'){
	$def=$db->query("SELECT * FROM customise_field WHERE controller='company-customer' AND value=1 ");
	while ($def->next()) {
		$line = array();
		/*$line[$def->f('field')] = $def->f('value') == 1 ? true : false;*/
		$result['obj'][$def->f('field').'_if'] = $def->f('creation_value') == 1 ? true : false;
	}
}else{
	$def=$db->query("SELECT * FROM customise_field WHERE controller='company-customer' ");
	while ($def->next()) {
		$line = array();
		/*$line[$def->f('field')] = $def->f('value') == 1 ? true : false;*/
		/*$result['obj'][$def->f('field').'_if'] =  true;*/
		$result['obj'][$def->f('field').'_if'] = $def->f('value') == 1 ? true : false;
	}
}

/*$default_show[$def->f('field')] = $line;*/

/*$result['obj']['acc_manager']      		=  ($def->f("field")=='acc_manager' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['language_if']      		=  ($def->f("field")=='language' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['sales_rep_if']      	=  ($def->f("field")=='sales_rep' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['lead_source_if']    	=  ($def->f("field")=='lead_source' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['comp_size_if'] 		    =  ($def->f("field")=='comp_size' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['activity_if']      		=  ($def->f("field")=='activity' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['various1_if']   	   	=  ($def->f("field")=='various1' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['various2_if'] 	     	=  ($def->f("field")=='various2' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['is_supplier_if']    	=  ($def->f("field")=='is_supplier' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['website_if']    	  	=  ($def->f("field")=='website' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['phone']      			=  ($def->f("field")=='phone' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['fax']      				=  ($def->f("field")=='fax' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['c_email_if']      		=  ($def->f("field")=='c_email' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['our_reference_if']  	=  ($def->f("field")=='our_reference' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['sector_if']     		=  ($def->f("field")=='sector' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['legal_type_if']     	=  ($def->f("field")=='legal_type' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['commercial_name_if']    =  ($def->f("field")=='legal_type' &&  $def->f("creation_value")==1) ? true : false;*/




/*$label = $db->query("SELECT * FROM customer_fields WHERE value = '1' AND is_dd = 1 ORDER BY field_id ");
$all = $label->getAll();

foreach ($all as $key => $value) {
	
}*/

//Build Sorted Field Section
if($in['customer_id']=='tmp'){
	$queryF = $db->query("SELECT * FROM customise_field WHERE controller='company-customer' AND value=1  AND creation_value=1 ORDER BY sort_order ASC");
	$queryExtraF = $db->query("SELECT * FROM customer_fields WHERE value = '1' AND creation_value='1'  ORDER BY sort_order ASC ");
}else{
	$queryF = $db->query("SELECT * FROM customise_field WHERE controller='company-customer'  AND value=1 ORDER BY sort_order ASC");
	$queryExtraF = $db->query("SELECT * FROM customer_fields WHERE value = '1'  ORDER BY sort_order ASC ");
}

while ($queryF->next()) {
$fieldInfo = customerStaticFieldsInfo($queryF);
	if(!empty($fieldInfo['label'])){
		$result['obj']['linesObj'][] = $fieldInfo; 
	}	
}

while ($queryExtraF->next()) {
	$f_value = $db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$queryExtraF->f('field_id')."' ");
	if($in['customer_id']=='tmp'){
		$creation = $queryExtraF->f('creation_value') == 1 ? true : false;
	}else{
		$creation= true;
	}

	if($queryExtraF->f('is_dd')==1){
		$extra_field_dd_name = $db->field("SELECT name
										FROM customer_extrafield_dd
										INNER JOIN customer_field ON ( customer_extrafield_dd.extra_field_id = customer_field.field_id
										AND customer_extrafield_dd.id = customer_field.value )
										WHERE extra_field_id =  '".$queryExtraF->f('field_id')."'
										AND customer_id='".$in['customer_id']."' ");
	}

	$result['obj']['linesObj'][] = array(
		'field_id'			=> $queryExtraF->f('field_id'),
		'label'				=> $queryExtraF->f('label'),
		'label_if'			=> $creation,
		'value'				=> $f_value ? $f_value : '',
		'normal_input'		=> $queryExtraF->f('is_dd')== 1 ? false:true,
		'extra_field_dd'	=> $queryExtraF->f('is_dd')== 1 ? build_extra_field_dd($f_value,$queryExtraF->f('field_id')):'',
		'extra_field_name' 	=> $queryExtraF->f('is_dd') == 1 ? $extra_field_dd_name : ( $f_value ? $f_value : '' ),
		'is_custom'			=> true,
		'sort_order'		=> $queryExtraF->f('sort_order'),
		);
}

usort($result['obj']['linesObj'], "myCustomSort");
//End Build Sorted Field Section

json_out($result);

function get_message_intra_eu($in){
	$db=new sqldb();
	$c = $db->query("SELECT message_intra_eu FROM  ".$in['table']."  WHERE lang_id='".$in['lang_id']."'");
	return array('message'=>$c->f('message_intra_eu'));
}

function get_check_for_btw($in){
	$db=new sqldb();
	$db->query("SELECT btw_nr, name FROM customers WHERE customer_id!='".$in['customer_id']."' AND btw_nr='".$in['btw_nr']."' ");
	if($db->next()){
		msg::notice ( $db->f('name').' '.gm("has the same VAT number"),'notice');
		return false;
	}
	return true;
}

function customerStaticFieldsInfo($query){
	$fieldLabelName = '';
	$ifCondition = '';
	$isDD = false;
	$selectizeConfig = '';
	$selectizeOptions = '';
	$model = '';
	$isCustom = false;
	$sortOrder = $query->f('sort_order');
	$hasTable = false;
	$table = '';
	$isCheckbox = false;

	switch($query->f('field')){
		case 'legal_type':
			$fieldLabelName = gm('Legal Type');
			$ifCondition = 'legal_type_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_legal_type';
			$model = 'legal_type';
			$hasTable = true;
			$table = 'customer_legal_type';
			break;
		case 'commercial_name':
			$fieldLabelName = gm('Commercial Name');
			$ifCondition = 'commercial_name_if';
			$isDD = false;
			$model = 'commercial_name'; 
			break;
		case 'our_reference':
			//$fieldLabelName = gm('Our reference');
			$fieldLabelName = gm('Account Number');
			$ifCondition = 'our_reference_if';
			$model = 'our_reference';
			break;
		case 'sector':
			$fieldLabelName = gm('Activity Sector');
			$ifCondition = 'sector_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_sector';
			$hasTable = true;
			$table = 'customer_sector';
			$model = 'sector';
			break;
		case 'c_type':
			$fieldLabelName = gm('Type of relationship');
			$ifCondition = 'c_type_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_type';
			$hasTable = true;
			$table = 'customer_type';
			$model = 'c_type';
			break;
		case 'language':
			$fieldLabelName = gm('Language');
			$ifCondition = 'language_if';
			$isDD = true;
			$selectizeConfig = 'cfg';
			$selectizeOptions = 'lang_dd';
			$model = 'language';
			break;
		case 'acc_manager':
			$fieldLabelName = gm('Account Manager');
			$ifCondition = 'acc_manager_if';
			$isDD = true;
			$selectizeConfig = 'customerTypeCfg';
			$selectizeOptions = 'accountManager';
			$model = 'user_id';
			break;
		case 'sales_rep':
			$fieldLabelName = gm('Sales representative');
			$ifCondition = 'sales_rep_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'sales_rep';
			$model = 'sales_rep';
			$hasTable = true;
			$table = 'sales_rep';
			break;
		case 'lead_source':
			$fieldLabelName = gm('Lead source');
			$ifCondition = 'lead_source_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_lead_source';
			$model = 'lead_source';
			$hasTable = true;
			$table = 'customer_lead_source';
			break;
		case 'comp_size':
			$fieldLabelName = gm('Company size');
			$ifCondition = 'comp_size_if';
			$model = 'comp_size';
			break;
		case 'activity':
			$fieldLabelName = gm('Activity');
			$ifCondition = 'activity_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_activity';
			$model = 'activity';
			$hasTable = true;
			$table = 'customer_activity';
			break;
		case 'is_supplier':
			$fieldLabelName = gm('Supplier');
			$ifCondition = 'is_supplier_if';
			$model = 'is_supplier';
			$isCheckbox = true;
			break;
		case 'is_customer':
			$fieldLabelName = gm('Customer');
			$ifCondition = 'is_customer_if';
			$model = 'is_customer';
			$isCheckbox = true;
			break;
		case 'first_name':
			$fieldLabelName = gm('First Name');
			$ifCondition = 'first_name_if';
			$model = 'firstname';
			break;	
		case 'stripe_cust_id':
			$fieldLabelName = gm('Stripe ID');
			$ifCondition = 'stripe_cust_id_if';
			$isDD = false;
			$model = 'stripe_cust_id'; 
			break;	
	}

	return array('label' 			=> $fieldLabelName,
			'if_condition' 		=> $ifCondition,
			'is_dd'		   		=> $isDD,
			'selectize_config' 	=> $selectizeConfig,
			'selectize_options' => $selectizeOptions,
			'model'				=> $model,
			'is_custom'			=> $isCustom,
			'sort_order'		=> $sortOrder,
			'has_table'			=> $hasTable,
			'table'				=> $table,
			'is_checkbox'		=> $isCheckbox,
			);
}

function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

function buildCustomFieldsArray($result,$value,$info){

	$result[$info['section']]['linesObj'][] = array(
					'field_id'				=> $value['field_id'],
					'label'					=> $value['label'],
					'is_custom'				=> true,
					'is_dd'					=> $value['is_dd'] == 1 ? true : false,
					'value'					=> $value['value'] == 1 ? true : false,
					'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
					'value_creation'		=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
					'checkbox_id'			=> $info['j'],
					'checkbox_id_d'			=> $info['j_d'],
					'checkbox_id_c'			=> $info['j_c'],
					'save_value'			=> $info['save_value'],
					'save_value_d'			=> $info['save_value_d'],
					'save_value_c'	 		=> $info['save_value_c'],
					'edit_parameter'		=> $info['edit_parameter'],
					'sort_order'			=> $value['sort_order']
				);
	return $result;
}

?>