<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);

unset($_SESSION['articles_id']);
if($in['reset_list']){
	if (isset($_SESSION['tmp_add_account_to_archive'])) {
	    unset($_SESSION['tmp_add_account_to_archive']);
	}
	if(isset($_SESSION['add_account_to_archive'])){
		unset($_SESSION['add_account_to_archive']);
	}
}


$output = array();
$order_by_array = array('name','city_name','country_name','c_type_name','acc_manager_name','serial_number_customer','comp_phone','c_email','our_reference','language', 'invoice_email', 'btw_nr','legal_type_name','firstname');
$arguments_s = '';
$arguments='';
$arguments_o='';
$arguments_a='';
$l_r =ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

if($in['active']==1){
	$filter.= " AND active=1";
}

$filter = " is_admin='0' AND front_register=0 ";
if(empty($in['archived'])){
	$filter.= " AND active=1";
}else{
	$in['archived'] = 1;
	$filter.= " AND active=0";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}
// if we have a regular user and ONLY_IF_ACC_MANAG == 1, then we show to the user only the
// companies where he is account manager
//$admin_licence = $db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence=aktiUser::get('user_type');

if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
	$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}

//FILTER LIST
$filter_c=' ';
$order_by = " ORDER BY customers.name ";
$ad_search_f = '';
$escape_chars = array('(','?','*','+',')');
if(!empty($in['search'])){
	$filter_x = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['search']));
	foreach ($search_arr as $value) {
		if(in_array($value, $escape_chars)){
			$value = '\\'.$value;
		}
		$filter_x .= $value."[^a-zA-Z0-9]?";
	}
	$filter .= " AND ( customers.name regexp '".addslashes($filter_x)."' OR customers.name LIKE '%".$in['search']."%' ) ";
	// $filter .= " AND ( name LIKE '%".$in['search']."%' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['acc_manager_name'])){
	$filter .= " AND ( acc_manager_name LIKE '%".$in['acc_manager_name']."%' ) ";
	$arguments_s.="&acc_manager_name=".$in['acc_manager_name'];
}/*elseif(!empty($in['acc'])){
	$filter .= " AND user_id='0' ";
	$arguments_s.="&acc=".$in['acc'];
}*/
if(!empty($in['zip_from']) && empty($in['zip_to'])){
	$zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
	$filter .= " AND ( zip LIKE '%".$zip_from."%' ) ";
	$arguments_s.="&zip_from=".$in['zip_from'];
}
if(!empty($in['zip_to'])){
	$db->query("DROP FUNCTION IF EXISTS STRIP_NON_DIGIT;
CREATE FUNCTION STRIP_NON_DIGIT(input VARCHAR(255))
   RETURNS VARCHAR(255)
BEGIN
   DECLARE output   VARCHAR(255) DEFAULT '';
   DECLARE iterator INT          DEFAULT 1;
   WHILE iterator < (LENGTH(input) + 1) DO
      IF SUBSTRING(input, iterator, 1) IN ( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ) THEN
         SET output = CONCAT(output, SUBSTRING(input, iterator, 1));
      END IF;
      SET iterator = iterator + 1;
   END WHILE;
   RETURN output;
END;");
	$zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
	$zip_to = preg_replace('/[^0-9]/', '', $in['zip_to']);
	if(empty($zip_from)){
		$zip_from = '0';
	}
	$filter .= " AND ( STRIP_NON_DIGIT(zip) BETWEEN '".$zip_from."' AND '".$zip_to."' ) ";
	$arguments_s.="&zip_to=".$zip_to."&zip_from=".$zip_from;
}
if(!empty($in['country_id'])){
	$filter .= " AND country_name = '".$in['country_id']."' ";
	$arguments_s.="&country_id=".$in['country_id'];
}
if(!empty($in['city_name'])){
	$filter .= " AND ( customers.city_name LIKE '%".$in['city_name']."%' ) ";
	$arguments_s.="&city_name=".$in['city_name'];
}
if(!empty($in['our_reference'])){
	$filter .= " AND ( our_reference LIKE '%".$in['our_reference']."%' ) ";
	$arguments_s.="&our_reference=".$in['our_reference'];
}

if(!empty($in['btw_nr'])){

	$escape_chars2 = array('.',' ','/',',');
	$filter_y = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['btw_nr']));

	foreach ($search_arr as $value) {
		if(!in_array($value, $escape_chars2)){
			$filter_y .= $value."\.?";
		}

	}
	//console::log($filter_y);
	$filter .= " AND ( btw_nr regexp '".addslashes($filter_y)."' OR btw_nr='".$in['btw_nr']."') ";
	$arguments_s.="&btw_nr=".$in['btw_nr'];
}
if(!empty($in['commercial_name'])){
	$filter_x = '';
	$search_arr = str_split(stripslashes($in['commercial_name']));
	foreach ($search_arr as $value) {
		if($value == '('){
			$value = '\\(';
		}
		$filter_x .= $value.".?";
	}
	$filter .= " AND ( commercial_name regexp '".addslashes($filter_x)."' ) ";
	$arguments_s.="&commercial_name=".$in['commercial_name'];
}

if(!empty($in['lead_source'])){
	$filter .= " AND lead_source='".$in['lead_source']."' ";
	$arguments_s.="&lead_source=".$in['lead_source'];
}

if(!empty($in['sector'])){
	$filter .= " AND sector='".$in['sector']."' ";
	$arguments_s.="&sector=".$in['sector'];
}

if (!empty($in['c_type_name'])){
	$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
	$arguments.="&c_type_name=".$in['c_type_name'];
}
if (isset($in['type'])){
	// if(!$in['type']){
	// 	$in['type'] ='0';
	// }
	switch ($in['type']) {
		case '1':
			$filter .= " AND customers.type='".$in['type']."'  ";
	        $arguments.="&type=".$in['type'];
			break;
		case '2':
		    $filter .= " AND customers.type='0'  ";
	        $arguments.="&type=".$in['type'];
		    break;
	}

}
if (isset($in['is_supplier'])){
	switch ($in['is_supplier']) {
		case '1':
			$filter .= " AND customers.is_supplier='".$in['is_supplier']."'  ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		case '2':
			$filter .= " AND customers.is_customer='1'  ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		case '3':
			$filter .= " AND customers.is_supplier='1' AND customers.is_customer='1' ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		case '4':
			$filter .= " AND customers.is_supplier='' AND customers.is_customer='' ";
            $arguments.="&is_supplier=".$in['is_supplier'];
			break;
		default:
			# code...
			break;
	}

}
if(!empty($in['first']) && $in['first'] == '[0-9]'){
	$filter.=" AND SUBSTRING(LOWER(customers.name),1,1) BETWEEN '0' AND '9'";
	$arguments.="&first=".$in['first'];
} elseif (!empty($in['first'])){
	$filter .= " AND customers.name LIKE '".$in['first']."%'  ";
	$arguments.="&first=".$in['first'];
}

if(!empty($in['order_by'])){
	$order = " ASC ";
		if($in['desc'] == '1' || $in['desc']=='true'){
			$order = " DESC ";
		}
	if(in_array($in['order_by'], $order_by_array)){	
		if($in['order_by']=='name'){
	        $in['order_by']='customers.name';
	    }
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	}
}
$selected =array();
/*$db->query("SELECT DISTINCT first_letter FROM customers
			LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
			WHERE ".$filter." GROUP BY customers.customer_id ORDER BY name");
while($db->move_next()){
	array_push($selected, $db->f('first_letter'));
}*/

if(!empty($in['address'])){
	$filter .= " AND ( customer_addresses.address LIKE '%".$in['address']."%' ) ";
	$arguments_s.="&address=".$in['address'];
}

$arguments = $arguments.$arguments_s;

$output['max_rows'] =  $db->field("SELECT count(DISTINCT customers.customer_id) FROM customers
								LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
								LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
								 WHERE ".$filter." " );
$customer_logs = array();
$c_logs = $db->query("SELECT id, customer_id FROM logging_tracked WHERE seen='0' AND user_id='".$_SESSION['u_id']."' GROUP BY customer_id ")->getAll();

foreach ($c_logs as $key => $value) {
	$customer_logs[$value['id']] = $value['customer_id'];
}

$output['lead_source_dd']	= build_l_source_dd();
$output['sector_dd']		= build_s_type_dd();
$output['type_dd']		= array( array('id'=>'0', 'value'=>gm('Company')), array('id'=>'1', 'value'=>gm('Individual')), array('id'=>'2', 'value'=>gm('Customer')), array('id'=>'3', 'value'=>gm('Supplier')) );
$output['account_managers']	= build_acc_manager();
$output['c_type_dd']		= build_c_type_name_dd();
$output['countries'] 		= company_country_list();
$output['list'] = array();

$output['nav'] =  $db->query("SELECT customers.customer_id FROM customers
			LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
			LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
			WHERE ".$filter." GROUP BY customers.customer_id ".$order_by)->getAll();


$c = $db->query("SELECT is_supplier,is_customer,type,customers.name,customers.customer_id, country_name,active as ac, customers.city_name, c_type_name, acc_manager_name, first_letter, serial_number_customer, comp_phone, c_email, our_reference, customers.language, CASE WHEN invoice_email_type = 2 THEN invoice_email ELSE '' END as invoice_email, customer_addresses.city,customer_addresses.country_id, customers.btw_nr,multiple_identity.identity_name,customer_legal_type.name as legal_type_name,customers.firstname,vat_new.description AS vat_regime
			FROM customers
			LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
			LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
			LEFT JOIN multiple_identity ON customers.identity_id=multiple_identity.identity_id
			LEFT JOIN vat_new ON customers.vat_regime_id=vat_new.id
			WHERE ".$filter." GROUP BY customers.customer_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();

foreach ($c as $key => $value) {
    foreach ($value as $key2 => $value2) {
        if ($key2 == 'customer_id') {
            $_SESSION['tmp_add_account_to_archive'][$value2] = 1;
        }
    }

    /*$is_new_event = '';
    if(in_array($value['customer_id'], $customer_logs)){
        $is_new_event = 'new_event_timeline';
    }*/
	if($value['is_supplier']==1 && $value['is_customer']==1) {
		$class='akti-icon akti-icon-16 o_akti_custopplier';
		$class_name = gm('Customer').' & '.gm('Supplier');
	}elseif($value['is_customer']==0 && $value['is_supplier']==0){
		$class='';
		$class_name='';
	}elseif($value['is_supplier']==1 && $value['is_customer']==0){
		$class='akti-icon akti-icon-16 o_akti_delivery';
		$class_name = gm('Supplier');
	}elseif($value['is_customer']==1 && $value['is_supplier']==0){
		$class='akti-icon akti-icon-16 o_akti_customer';
		$class_name = gm('Customer');
	}

	$can_be_deleted = check_customer($value['customer_id']);
	$last_update = $db->field("SELECT date FROM  `logging` WHERE  `field_name` =  'customer_id' AND  `field_value` =  '".$value['customer_id']."' ORDER BY  `logging`.`date` DESC" );

	$output['list'][]=array(
		'name'						=> $value['name'],
		'type'						=> $class,
		'type_name'					=> $class_name,
		'type2'						=> $value['type']==1 ? 'users' : 'building',
		'type2_name'				=> $value['type']==1 ? gm('Individual') : gm('Company'),
		'company_type'				=> $value['type']==1 ? gm('Individual') : gm('Company'),
		// 'serial_number_customer'	=> $value['serial_number_customer'],
		'country'					=> $value['country_name'] ? $value['country_name']: ($value['country_id']? get_country_name($value['country_id']): '&nbsp;' ),
		'city'						=> $value['city_name'] ? $value['city_name'] : ($value['city']? $value['city']: '&nbsp;' ),
		'customer_id'				=> $value['customer_id'],
		'sales'						=> $value['c_type_name'] ? $value['c_type_name'] : '&nbsp;' ,
		// 'contacts'					=> get_contact_nr($value['customer_id']),
		'contacts'					=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '&nbsp;',
		'confirm'					=> gm('Confirm'),
		'ok'						=> gm('Ok'),
		'cancel'					=> gm('Cancel'),
		// 'is_active'     			=> $value['ac']? true : false,
		// 'new_event'					=> $is_new_event,
		// 'edit_link'					=> 'index.php?do=company-customer&customer_id='.$value['customer_id'],
		// 'time_link'					=> 'index.php?do=company-customer_timelineNew&customer_id='.$value['customer_id'],
		'can_be_deleted'			=> $can_be_deleted,
		'delete_link'				=> array('do'=>'customers-customers-customers-delete', 'customer_id'=>$value['customer_id']),
		'archive_link'				=> array('do'=>'customers-customers-customers-archive', 'customer_id'=>$value['customer_id']),
		'activate_link'				=> array('do'=>'customers-customers-customers-activate', 'customer_id'=>$value['customer_id']),
		'comp_phone'				=> $value['comp_phone'],
		'c_email'					=> $value['c_email'],
		'our_reference'				=> $value['our_reference'],
		'language'					=> $value['language']<10000? (gm($db->field("SELECT language FROM pim_lang WHERE lang_id = '".$value['language']."' "))) : (gm($db->field("SELECT language FROM pim_custom_lang WHERE lang_id = '".$value['language']."' "))),
		'last_update'				=> date(ACCOUNT_DATE_FORMAT,$last_update),
		'last_update_ord'			=> $last_update,
		'invoice_email'				=> $value['invoice_email'],
		'vat_nr'					=> $value['btw_nr'],
		'identity_name'				=> $value['identity_name'] ? stripslashes($value['identity_name']) : gm('Main Identity'),
		'legal_type'                => $value['legal_type_name'] ? $value['legal_type_name'] : '',
		'firstname'					=> $value['type']==1 ? $value['firstname'] : '-',
		'vat_regime'				=> stripslashes($value['vat_regime']),
        'check_add_to_product'      => $_SESSION['add_account_to_archive'][$value['customer_id']] == 1 ? true : false,
        'id'                        => $value['customer_id']
	);
}

$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_account_to_archive']){
    if($output['max_rows'] && count($_SESSION['add_account_to_archive']) == $output['max_rows']){
        $all_pages_selected=true;
    }else if(count($_SESSION['add_account_to_archive'])){
        $minimum_selected=true;
    }
}
$output['all_pages_selected'] = $all_pages_selected;
$output['minimum_selected'] = $minimum_selected;

/*
console::log( sprintf('%04x%04_%04x_%03x4_%04x_%04x%04x%04x',
       mt_rand(0, 65535), mt_rand(0, 65535), // 32 bits for "time_low"
       mt_rand(0, 65535), // 16 bits for "time_mid"
       mt_rand(0, 4095),  // 12 bits before the 0100 of (version) 4 for "time_hi_and_version"
       bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
           // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
           // (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
           // 8 bits for "clk_seq_low"
       mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) // 48 bits for "node"
   ));
*/

if($in['order_by']=='last_update'){
		$output['list']=order_array_by_column($output['list'], $in['order_by'], $order);	    
	}

$output['export_args'] = ltrim($arguments,'&');
$output['lr']=$l_r;
json_out($output);