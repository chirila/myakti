<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($db_config);
//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
$output = array();	
$order_by_array = array('department','position','lastname','firstname');
$arguments_s = '';
$arguments='';
$arguments_o='';
$arguments_a='';
$l_r =28;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$output['view'] = 0;
if(CUSTOMER_VIEW_LIST){
	$output['view'] = 1;
}

$filter = " customer_contacts.front_register = 0 AND customer_contactsIds.customer_id=".$in['customer_id']." ";

if(empty($in['search'])){
	if(empty($in['archived'])){
		$filter.= " AND customer_contacts.active=1";
	}else{
		$in['archived'] = 1;
		$filter.= " AND customer_contacts.active=0";
		$arguments.="&archived=".$in['archived'];
		$arguments_a = "&archived=".$in['archived'];
	}
}

//FILTER LIST
$order_by = " ORDER BY lastname ";

if(!empty($in['search'])){
	$filter_x = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['search']));
	foreach ($search_arr as $value) {
		if(in_array($value, $escape_chars)){
			$value = '\\'.$value;
		}
		$filter_x .= $value.".?";
	}

	$search_words = explode(' ', $in['search']);
	foreach ($search_words as $word_pos => $searched_word) {
		if(trim($searched_word)){
			
			if($word_pos==0){
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR customer_contacts.lastname LIKE '%".$searched_word."%'
							OR customer_contactsIds.department LIKE '%".$searched_word."%'
							OR customer_contactsIds.position LIKE '%".$searched_word."%'
							OR customer_contacts.phone LIKE '%".$searched_word."%'
							OR customer_contacts.cell LIKE '%".$searched_word."%'
							OR customer_contacts.email LIKE '%".$searched_word."%' )";
			}else{
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR customer_contacts.lastname LIKE '%".$searched_word."%'
							OR customer_contactsIds.department LIKE '%".$searched_word."%'
							OR customer_contactsIds.position LIKE '%".$searched_word."%'
							OR customer_contacts.phone LIKE '%".$searched_word."%'
							OR customer_contacts.cell LIKE '%".$searched_word."%'
							OR customer_contacts.email LIKE '%".$searched_word."%' )";
			}

		}
	}

	$arguments_s.="&search=".$in['search'];
}

if(!empty($in['order_by'])){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		if($in['order_by']=='department'){
			$in['order_by']='customer_contactsIds.department';
		}elseif($in['order_by']=='position'){
			$in['order_by']='customer_contactsIds.position';
		}else{

		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];				
	}
}

if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	$max_rows =  $db->field("SELECT COUNT( customer_contacts.contact_id ) AS max_contact_rows 
							FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter );
	$c = $db->query("SELECT customer_contacts.*, customer_contacts.email as main_email, customer_contactsIds.position, customer_contactsIds.department, customer_contactsIds.fax, customer_contactsIds.title, 
	                customer_contactsIds.e_title, customer_contactsIds.s_email,customer_contactsIds.primary,customer_contactsIds.email as sec_email  
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
				LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id 
				WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
}else{
	$arguments = $arguments.$arguments_s;
	$max_rows =  $db->field("SELECT count( customer_contacts.contact_id ) FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							WHERE ".$filter );
	$c = $db->query("SELECT customer_contacts.*, customer_contacts.email as main_email,customer_contactsIds.position, customer_contactsIds.department, customer_contactsIds.fax,customer_contactsIds.phone,
	                customer_contactsIds.e_title, customer_contactsIds.s_email,customer_contactsIds.primary,customer_contactsIds.email as sec_email FROM customer_contacts
	                INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
					WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
}

$arguments = $arguments.$arguments_s;

$output['max_rows'] =  $max_rows;

$output['list'] = array();


$position 				= build_contact_function('',true);
$department				= build_contact_dep_type_dd('',true);
$title					= build_contact_title_type_dd('',true);

foreach ($c as $key => $value) {
	$department_name = $db->field("SELECT name FROM customer_contact_dep WHERE id=".$value['department']."");
	$position_con = $db->field("SELECT name FROM customer_contact_job_title WHERE id=".$value['position']."");
	$title_name = $db->field("SELECT name FROM customer_contact_title WHERE id='".$value['title']."'");
	$output['list'][]=array(
		'cus_name'			    => $value['company_name'],
		'department_name'		=> $department_name,
		'con_position'			=> $position_con,
		'customer_id'		    => $value['customer_id'],
		'con_firstname'			=> $value['firstname'] ? $value['firstname'] : '&nbsp;',
		'con_lastname'			=> $value['lastname'] ? $value['lastname'] : '&nbsp;',
		'position_id'			=> $position_con,
		'department_id'			=> $position_con,
		'position'				=> $value['position'],
		'department'			=> $value['department'],
		'con_phone'				=> $value['phone'] ? $value['phone'] : '-',
		'con_cell'				=> $value['cell'] ? $value['cell'] : '-',
		'con_email'				=> $value['primary'] ? $value['sec_email'] : $value['sec_email'],
		'fax'					=> $value['fax'],
		'title'					=> $title_name,
		'e_title'				=> $value['e_title'],
		's_email'				=> $value['s_email'] == 1 ? gm('Yes') : gm('No'),
		'archive_link'			=> array('do'=>'customers-customer_contacts-customers-archive_contact', 'contact_id'=>$value['contact_id'],'offset'=>$in['offset'], 'search'=>$in['search']),
		'activate_link'			=> array('do'=>'customers-customer_contacts-customers-activate_contact', 'contact_id'=>$value['contact_id']),
		'contact_id' 			=> $value['contact_id'],
		'zen_id'				=> $value['zen_contact_id'],
		'active_status'			=> $value['active'] ? true : false
	);
}

$output['export_args'] = ltrim($arguments,'&');
$output['lr']=$l_r;
json_out($output);