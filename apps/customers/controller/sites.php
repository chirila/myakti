<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class InstallSite extends Controller
{
	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);
	}

    public function get_Data(){
        global $config;
        $in=$this->in;
		$result = array('list'=>array());
		$addresses_list=array();
		$customer_addresses=$this->db->query("SELECT address_id,address,city,zip,country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND site='1' ORDER BY address ASC")->getAll();
		foreach($customer_addresses as $key=>$value){
			$item_addr='';
			if(!empty($value['address'])){
				$item_addr.=$value['address'].',';
			}
			if(!empty($value['city'])){
				$item_addr.=$value['city'].' ';
			}
			if(!empty($value['zip'])){
				$item_addr.=$value['zip'].' ';
			}
			if($value['country_id']){
				$item_addr.=get_country_name($value['country_id']);
			}
			$item_addr=rtrim($item_addr,' ');
			$item_addr=rtrim($item_addr,',');
			$item=array(
				'address_id'	=> $value['address_id'],
				'name'		=> stripslashes($item_addr),
			);
			array_push($addresses_list,$item);
		}
		$result['addresses_list']=$addresses_list;
		if(!$in['address_id']){
			$main_address=$this->db->query("SELECT site,address_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
			if($main_address->f('site')){
				$in['address_id']=$main_address->f('address_id');
				$result['address_id']=$main_address->f('address_id');
			}else{
				if(!empty($customer_addresses)){
					$in['address_id']=$customer_addresses[0]['address_id'];
					$result['address_id']=$customer_addresses[0]['address_id'];
				}
			}
		}else{
			$result['address_id']=$in['address_id'];
		}
		$result['address']='';
		$result['zip']='';
		$result['city']='';
		$result['country']='';
		if($in['address_id']){
			$addr_data=$this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['address_id']."' ");
			while($addr_data->next()){
				$result['address']=$addr_data->f('address');
				$result['zip']=$addr_data->f('zip');
				$result['city']=$addr_data->f('city');
				$result['country']=get_country_name($addr_data->f('country_id'));
				if($addr_data->f('address') ){
			        $country_n = get_country_name($addr_data->f('country_id') );
			        if(!$addr_data->f('zip') && $addr_data->f('city'))
			        {
			            $geo_address = $addr_data->f('address').', '.$addr_data->f('city').', '.$country_n;
			        }elseif(!$addr_data->f('city') && $addr_data->f('zip'))
			        {
			            $geo_address = $addr_data->f('address').', '.$addr_data->f('zip').', '.$country_n;
			        }elseif(!$addr_data->f('zip') && !$addr_data->f('city'))
			        {
			            $geo_address = $addr_data->f('address').', '.$country_n;
			        }else
			        {
			            $geo_address = $addr_data->f('address').', '.$addr_data->f('zip').' '.$addr_data->f('city').', '.$country_n;
			        }
			        /*$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.urlencode($geo_address).'&key=AIzaSyAB-QkaXPK9zZoAlEnVUN7L03OlABkUGSI&sensor=false');
			        $map_data= json_decode($geocode);
			        $lat = $map_data->results[0]->geometry->location->lat;
			        $long = $map_data->results[0]->geometry->location->lng;
			        $coords=array('latitude'=>$lat,'longitude'=>$long);*/
			        $result['full_address_map_src']	= $geo_address ? 'https://www.google.com/maps/embed/v1/place?key=' . $config['GOOGLE_MAPS_API_KEY'] . '&q=' . $geo_address : '';
			    }
			}
			$result['list']=$this->get_Fields($in);
		}
		$result['coords'] = ($coords && $coords['latitude'] && $coords['longitude']) ? $coords : array();
		$result['customer_id']=$in['customer_id'];	
		$this->out = $result;
	}

	public function get_Fields(&$in){
		$data=array();
		$site_fields_address=$this->db->query("SELECT * FROM site_fields_address WHERE address_id='".$in['address_id']."' ")->getAll();
		$site_fields=$this->db->query("SELECT * FROM site_fields ORDER BY sort_order ASC");
		while($site_fields->next()){
			$val='';
			$drop_opts=array();
			if($site_fields->f('type')=='1'){
				$drop_opts=build_site_field_dropdown($site_fields->f('field_id'));
			}
			foreach($site_fields_address as $key=>$value){
				if($value['field_id']==$site_fields->f('field_id')){
					switch($site_fields->f('type')){
						case '1':
							if($in['view']){
								$val=$this->db->field("SELECT name FROM site_fields_drops WHERE id='".$value['field_value']."' ");
							}else{
								$val=$value['field_value'];
							}				
							break;
						case '3':
							if($in['view']){
								$val=$value['field_value'] ? date(ACCOUNT_DATE_FORMAT,$value['field_value']) : '';
							}else{
								$val=$value['field_value'] ? $value['field_value']*1000 : '';
							}		
							break;
						case '4':
							$val=$in['view'] ? nl2br(stripslashes($value['field_text'])) : stripslashes($value['field_text']);
							break;
						case '5':
							if($in['view']){
								$val=$value['field_value'] ? gm('Yes') : gm('No');
							}else{
								$val=$value['field_value'] ? true : false;
							}				
							break;
						default:
							$val=stripslashes($value['field_text']);
							break;
					}			
					break;
				}
			}
			$elem=array(
				'field_id'		=> $site_fields->f('field_id'),
				'field_type'	=> $site_fields->f('type'),
				'name'			=> stripslashes($site_fields->f('name')),
				'value'			=> $val,
				'options'		=> $drop_opts,
			);
			array_push($data,$elem);
		}
		return $data;
	}

	public function get_Installations(&$in){
		$result=array('list'=>array());
		$filter_instal="";
		if($in['search'] && !empty($in['search'])){
			$filter_instal=" AND name LIKE '%".$in['search']."%' ";
		}
		$installations=$this->db->query("SELECT * FROM installations WHERE address_id='".$in['address_id']."' ".$filter_instal." ORDER BY name ASC ");
		while($installations->next()){
			$icon=$this->db->field("SELECT icon_url FROM installation_templates WHERE template_id='".$installations->f('template_id')."'");
			$is_left=0;
			$is_right=0;
			$left_f=array();
			$right_f=array();
			$left_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$installations->f('id')."' AND position='1' ORDER BY id ASC");
			while($left_fields->next()){
				switch($left_fields->f('p_type')){
					case '10':
					case '11':
						$name_final=htmlspecialchars_decode($this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$left_fields->f('p_id')."' "));
						break;
					default:						
						if($left_fields->f('type')==3){
							$name_final=(int)$left_fields->f('name') ? date(ACCOUNT_DATE_FORMAT,(int)$left_fields->f('name')): '';
						}else{
							if($left_fields->f('type')==1){
								$name_final=htmlspecialchars_decode(stripslashes($this->db->field("SELECT name FROM installation_fields_drops WHERE id='".$left_fields->f('name')."' ")));
							}else{
								$name_final=stripslashes($left_fields->f('name'));
							}
						}
						break;
				}
				$temp_left=array(
						'name'			=> stripslashes($left_fields->f('label_name')),
						'name_final'		=> $name_final,
					);
				array_push($left_f, $temp_left);
				$is_left++;
			}
			$right_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$installations->f('id')."' AND position='2' ORDER BY id ASC");
			while($right_fields->next()){
				switch($right_fields->f('p_type')){
					case '10':
					case '11':
						$name_final=htmlspecialchars_decode($this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$right_fields->f('p_id')."' "));
						break;
					default:
						if($right_fields->f('type')==3){
							$name_final=(int)$right_fields->f('name') ? date(ACCOUNT_DATE_FORMAT,(int)$right_fields->f('name')): '';
						}else{
							if($right_fields->f('type')==1){
								$name_final=htmlspecialchars_decode(stripslashes($this->db->field("SELECT name FROM installation_fields_drops WHERE id='".$right_fields->f('name')."' ")));
							}else{
								$name_final=stripslashes($right_fields->f('name'));
							}
						}
						break;
				}
				$temp_right=array(
						'name'			=> stripslashes($right_fields->f('label_name')),
						'name_final'		=> $name_final,
					);
				array_push($right_f, $temp_right);
				$is_right++;
			}
			$ints=array();
			$attached_ints=$this->db->query("SELECT servicing_support.*,tracking.creation_date,tracking.creation_user_id FROM servicing_support
				LEFT JOIN tracking ON servicing_support.service_id=tracking.target_id AND tracking.target_type='5'
			 	WHERE installation_id='".$installations->f('id')."' ORDER BY tracking.creation_date DESC");
			while($attached_ints->next()){
				$item=array(
					'id'				=> $attached_ints->f('service_id'),
					'serial_number'		=> $attached_ints->f('serial_number'),
					'img'				=> 'interventionsok',
					'time'				=> date(ACCOUNT_DATE_FORMAT,$attached_ints->f('creation_date')),
					'hour'				=> date('H:i',$attached_ints->f('creation_date')),
					'type'				=> gm('Intervention'),
					'pdf_link'			=> 'index.php?do=maintenance-print&service_id='.$attached_ints->f('service_id'),
					'author'			=> get_user_name($attached_ints->f('creation_user_id')),
					);
				array_push($ints,$item);
			}

			global $config;
			ark::loadLibraries(array('aws'));
			$a = new awsWrap(DATABASE_NAME);
			$in['s3_folder']='installation';
			$info=$this->db->query("SELECT * FROM amazon_files WHERE master='".$in['s3_folder']."' AND parent_id='".$installations->f('id')."' ");
			$item_img=array();
			$item_files=array();
			while($info->next()){
				if($info->f('expire') > (time()+60*60*24*3)){
					$link=$info->f('link');		
				}else{
					$expire = time() + (60*60*24*365);
					$link = $a->getLink($config['awsBucket'].DATABASE_NAME.'/'.$info->f('item'));
					$db->query("UPDATE amazon_files SET `link`='".$link."',`expire`='".$expire."' WHERE id='".$info->f('id')."' ");
				}
				$tmp_data=array(
					'id'				=> $info->f('id'),		
					'url'				=> $link,
					'name'				=> htmlspecialchars_decode(stripslashes($info->f('name'))),
					'delete_file_link'	=> array('do'=>'misc--misc-deleteS3File','id'=>$info->f('id')),
					'down_file_link'	=> 'index.php?do=misc-download_s3_file&id='.$info->f('id'),
					'item'				=> $info->f('item'),
				);
				if($info->f('type')=='1'){
					//file
					array_push($item_files,$tmp_data);
				}else{
					//image
					array_push($item_img,$tmp_data);
				}			
			}
			
			$item=array(
				'id'				=> $installations->f('id'),
				'name'				=> stripslashes($installations->f('name')),
				'serial_number'		=> $installations->f('serial_number'),
				'icon'				=> $icon,
				'left_fields'		=> $left_f,
				'right_fields'		=> $right_f,
				'show_more'			=> ($is_left>3 || $is_right>3) ? true : false,
				'showMoreField'		=> false,
				'interventions'		=> $ints,
				'intervention_more'	=> false,
				'images'			=> $item_img,
				'images_show'		=> !empty($item_img)? true : false,
				'files'				=> $item_files,
				'files_show'		=> !empty($item_files)? true : false,
			);
			array_push($result['list'],$item);
		}
		return $result;
	}

}

	$install_site = new InstallSite($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$install_site->output($install_site->$fname($in));
	}

	$install_site->get_Data();
	$install_site->output();
?>