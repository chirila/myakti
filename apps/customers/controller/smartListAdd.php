<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in,$list));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

$output = array();

$output['do_next'] = 'customers-smartListAdd-customers-list_add';

$items = get_smartListContacts($in);
foreach ($items as $key => $value) {
	$output[$key] = $value;
}

json_out($output);

function get_smartListContacts($in){
	
	$db=new sqldb();

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$result = array('list'=>array());
	$l_r = ROW_PER_PAGE;
	$type = $in['list_type'];
	$list_for = $in['list_for'];

	if(!$list_for){
		$order_by = " ORDER BY firstname ";
	}else{
		$order_by = " ORDER BY customers.name ";
	}
	
	if($in['filter'] ){
		$filter = '';
		foreach ($in['filter'] as $field => $value){
			if($value){
				if(is_array($value)){
					$val = '';
					foreach ($value as $key){
						$val .= $key.',';
					}
					$value = $val;
				}
				$value = rtrim($value,',');
				if($value){
					if(substr(trim($field), 0, 13)=='custom_dd_id_'){

						$filter.= " AND ( ";

						$v = explode(',', $value);
						foreach ($v as $k => $v2) {

							$filter .= "customers.customer_id IN (SELECT customers.customer_id FROM customers
												LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	 											WHERE customer_field.value = '".addslashes($v2)."') OR ";

							//$filter .= "customer_field.value = '".$v2."' OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(substr(trim($field), 0, 15)=='c_custom_dd_id_'){
						
						$filter.= " AND ( ";

						$v = explode(',', $value);

						foreach ($v as $k => $v2) {

							$filter .= " customer_contacts.contact_id IN (SELECT customer_contacts.contact_id FROM customer_contacts
										 LEFT JOIN contact_field ON customer_contacts.contact_id=contact_field.customer_id
										 WHERE contact_field.value = '".addslashes($v2)."') OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(trim($field) == 'c_type'){
						$filter .= " AND ( ";
						$v = explode(',', $value);
						foreach ($v as $k => $v2) {
							$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(trim($field) == 'position'){
						$filter .= " AND ( ";
						$v = explode(',', $value);
						foreach ($v as $k => $v2) {
							$filter .= "position_n like '%".addslashes($v2)."%' OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(trim($field) == 'customer_contacts.email'){
						$filter.=" AND customer_contacts.email='' ";
					}elseif(trim($field) == 'customers.c_email'){
						$filter.=" AND customers.c_email='' ";
					}elseif(trim($field) == 'customers.type'){

						$filter.=" AND customers.active='1' AND customers.type='".($value-1)."' ";
					}elseif(trim($field) == 'type'){
							switch ($value) {
								    case 1:
								        $filter.=" AND customers.is_customer='1' ";
								        break;
								    case 2:
								        $filter.=" AND customers.is_supplier='1' ";
								        break;
								    case 3:
								        $filter.=" AND customers.is_customer='1' AND customers.is_supplier='1' ";
								        break;
								    case 4:
								        $filter.=" AND customers.is_customer='' AND customers.is_supplier='' ";
								        break;
								}
						
					}					
					else{
						$filter .= " AND ".$field." IN (".addslashes($value).") ";
					}
				}elseif(trim($field) == 'customers.c_email'){
					$filter.= " AND customers.c_email!='' ";
				}elseif(trim($field) == 'customer_contacts.email'){
					$filter.= " AND customer_contacts.email!='' ";
				}elseif(trim($field) == 'customers.s_emails'){
					$filter.= " AND customers.s_emails='1' ";
				}
			}
		}
	}
	if(!$filter){
		$filter = '1=1';
	}
	$filter = ltrim($filter," AND");
	if(!$list_for){
		$filter .= " AND customer_contacts.active='1' ";
	}else{
		$filter .= " AND customers.active='1' AND is_admin='0' ";
	}

	$result['filter_a']=$filter;
	if(!$list_for){//console::log('a');
	// if($type == 0){
		$k =  $db->field("SELECT COUNT( DISTINCT customer_contacts.contact_id)
	            FROM customer_contacts
	            LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
	            LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
	            LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
	            LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
	            LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
	            LEFT JOIN customer_type ON customers.c_type=customer_type.id
	            LEFT JOIN customer_sector ON customers.sector=customer_sector.id
	            LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
	            LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
	            
	            LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	            WHERE ".$filter." ");
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
	            // LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	// }
		$max_rows=$k;
		if($type){
			$k = $db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
		}

		$query_list = $db->query("SELECT customer_contacts.firstname, customer_contacts.lastname, customer_contacts.contact_id, customer_contacts.email, customers.name AS company_name,
			customer_contactsIds.customer_id AS cus_id
		            FROM customer_contacts
		            LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
		            LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
		            LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
		            LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
		            LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
		            LEFT JOIN customer_type ON customers.c_type=customer_type.id
		            LEFT JOIN customer_sector ON customers.sector=customer_sector.id
		            LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
		            LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
		            
		            LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
		            WHERE ".$filter." GROUP BY customer_contacts.contact_id
					$order_by  limit ".$offset*$l_r.", ".$l_r);
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
		            // LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	}else{//console::log('b');
		$k =  $db->field("SELECT DISTINCT COUNT( DISTINCT customers.customer_id)
				        FROM customers
				        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
				        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
				        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
				        LEFT JOIN customer_type ON customers.c_type=customer_type.id
				        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
				        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
				        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
				        
						LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	            WHERE ".$filter."  ");
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
						// LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	// }
		$max_rows=$k;
		if($type){
			$k = $db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
		}

		$query_list = $db->query("SELECT customers.c_email AS email, customers.name AS company_name, customers.customer_id AS contact_id, customers.customer_id AS cus_id
		            FROM customers
		            LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
			        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
			        LEFT JOIN customer_type ON customers.c_type=customer_type.id
			        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
			        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
			        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
			        
					LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
		            WHERE ".$filter." GROUP BY customers.customer_id
					$order_by  limit ".$offset*$l_r.", ".$l_r);
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
					// LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	}

	while ($query_list->next()){
		if($type){
			$active = $db->query("SELECT active FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND contact_id='".$query_list->f('contact_id')."' ");
			if($active->next()){
				$ac = $active->f('active');
			}else{
				$ac = 1;
			}
		}
		$item = array(
		 	'CON_FIRSTNAME'			=> $query_list->f('firstname'),
		 	'CON_COMP'				=> $query_list->f('company_name'),
			'CON_LASTNAME'			=> $query_list->f('lastname'),
			'EMAIL'					=> $query_list->f('email'),
			'REL' 					=> $query_list->f('contact_id'),
			'customer_id' 			=> $query_list->f('cus_id'),
			// 'CONTACT_EDIT_LINK'		=> $list_for ? 'index.php?do=company-customer&customer_id='.$query_list->f('contact_id').$arguments : 'index.php?do=company-xcustomer_contact&contact_id='.$query_list->f('contact_id').$arguments,
			// 'CHECKED'				=> $in['contact_list_id'] ? (in_array($query_list->f('contact_id'),$in['contact_id']) ? 'CHECKED': ($type ? '' : 'CHECKED')) : ($type ? (in_array($query_list->f('contact_id'),$in['contact_id']) ? 'CHECKED': '') : 'CHECKED' ) ,
			'CHECKED'				=> $in['contact_list_id'] ? ($type ? ($ac == '0' ? false : true) : true) : ($type ? (in_array($query_list->f('contact_id'),$in['contact_id']) ? true: false) : true ) ,
			// 'hide_action'			=> $type ? true : false,
			// 'hide_comp'				=> $list_for ? false : true,
			// 'last'					=> $type ? '' : 'last',
		);
	    $result['list'][] = $item;
		$i++;
	}
	$result["nr_contacts"] = $k>=0? $k : 0;
	$result['max_rows'] = $max_rows;
	$result['hide_comp'] = $list_for ? false : true;
	$result['lr'] = $l_r;
	$result['TOTAL_SEL_C'] = $k>=0? $k : 0;
	return $result;
}

?>