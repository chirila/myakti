<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class DealEdit extends Controller{
	function __construct($in,$db = null){
		parent::__construct($in,$db = null);		
	}

	public function getDeal(){
		if($this->in['opportunity_id'] == 'tmp'){ //add
			$this->getAddDeal();
		}else{ # edit
			$this->getEditDeal();
		}
	}

	public function getAddDeal(){
		$in = $this->in;
		$output=array();

		$user_lang = $_SESSION['l'];
		if ($user_lang=='nl')
		{
			$user_lang='du';
		}
		switch ($user_lang) {
			case 'en':
				$user_lang_id='1';
				break;
			case 'du':
				$user_lang_id='3';
				break;
			case 'fr':
				$user_lang_id='2';
				break;
		}

		$details = array('table'	 => $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field' 	 => $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'	 => $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],					 
					 'filter'	 => $in['buyer_id'] ? ' AND is_primary =1' : '',
					);

		$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
		if($in['buyer_id'] ){
			$buyer_info = $this->db->query("SELECT customers.payment_term,customers.payment_term_type, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name,
					customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.invoice_note2, customers.internal_language,
					customers.attention_of_invoice,customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc, customers.name AS company_name, customers.vat_regime_id,customers.identity_id,
					customers.bank_iban, customers.bank_bic_code
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$in['buyer_id']."' ");
			$buyer_info->next();
			$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');

			$text = gm('Company Name').':';
			$c_email = $buyer_info->f('c_email');
			$c_fax = $buyer_details->f('comp_fax');
			$c_phone = $buyer_details->f('comp_phone');
					
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			$main_address_id			= $buyer_details->f('address_id');
			$sameAddress = false;
			if($buyer_details->f('is_primary')==1){
				$sameAddress = true;
			}
			$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
			$name = stripslashes($name);
			$in['email_language']=$buyer_info->f('internal_language');
		}else{
			$buyer_info = $this->db->query("SELECT phone, cell, email, CONCAT_WS(' ',firstname, lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
			$buyer_info->next();
			$c_email = $buyer_info->f('email');
			$c_fax = '';
			$c_phone = $buyer_info->f('phone');
			$text =gm('Name').':';
			$name = $buyer_info->f('name');
		}

		if(!$in['delivery_address_id']){
			$in['delivery_address_id']=($in['main_address_id'] ? $in['main_address_id'] : $main_address_id);
		}
		if($in['delivery_address_id'] && $in['delivery_address_id']!=$main_address_id){
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND address_id='".$in['delivery_address_id']."' ");
			$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
			$name = stripslashes($name);
		}
		$output['country_dd']			= $in['country_id']? build_country_list($in['country_id']):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['customer']			= 'customer';
		$output['contact']			= 'contact';
		$output['main_address_id']		= $in['main_address_id'] ? $in['main_address_id'] : $main_address_id;
		$output['sameAddress']			= $in['sameAddress'] ? ($in['sameAddress'] == '1' ? true:false) : ($in['main_address_id']==$in['delivery_address_id'] ? true : false);
		$output['buyer_id']             	= $in['buyer_id'];
		$output['customer_id']			= $in['buyer_id'];
		$output['contact_id']           	= $in['contact_id'];
		$output['buyer_name']           	= $name;
		$output['field']				= $in['buyer_id'] ? 'customer_id' : 'contact_id';
		$output['free_field']			= $free_field;
		$output['free_field_txt']		= nl2br($free_field);
		$output['delivery_address_id']	= $in['delivery_address_id'];
		if($in['contact_id']){
			$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
		}
		$output['do_request']		= 'customers-deal_edit-customers-updateCustomerDataDeal';
		$output['cc']			= $this->get_cc($in);
		$output['contacts']		= $this->get_contacts($in);
		$output['addresses']		= $this->get_addresses($in);		
		$output['opportunity_id']		= $in['opportunity_id'];
		//$output['add_customer']		= !$in['buyer_id'] && !$in['contact_id'] ? true : false;
		$output['add_customer']		= false;
		$output['board_id']=$in['board_id'];
		$output['user_id']=$in['user_id'];
		$output['stage_id']=$in['stage_id'];
		$output['subject']=$in['subject'];
		$output['serial_number'] = $in['serial_number'] ? $in['serial_number'] : generate_deal_number();
		$output['amount']=$in['amount'] ? $in['amount'] : display_number(0);
		$output['success_rate']=$in['success_rate'] ? $in['success_rate'] : display_number(0);
		$output['close_date']=$in['close_date'] ? strtotime($in['close_date'])*1000 : time()*1000;
		$output['currency_type']=ACCOUNT_CURRENCY_TYPE;
		if(!$in['email_language']){
			$in['email_language'] = $user_lang_id;
		}
		$output['email_language']	= $in['email_language'];

		$output['language_dd']=build_language_dd_new(0);
		$output['gender_dd']= gender_dd();
		$output['title_dd']= build_contact_title_type_dd(0);
		$output['main_comp_info']= getCompanyInfo($in);
		$output['segment_dd']= get_categorisation_segment();
		$output['source_dd']= get_categorisation_source();
		$output['type_dd']= get_categorisation_type();
		$output['identity_id']=$in['identity_id'] ? $in['identity_id'] : get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);
		$output['multiple_identity_dd']=build_identity_dd($in['identity_id']);
		$output['type_id']=$in['type_id'];
		$output['source_id']=$in['source_id'];
		$output['segment_id']=$in['segment_id'];
		$output['user_list'] = $this->get_UserList($in);
		$output['created_by'] = $in['user_id'] ? $in['user_id'] : $_SESSION['u_id'];
		$output['board_list']=$this->get_BoardsList($in);
		$output['stages_list']=array();
		if($in['board_id']){
			$output['stages_list']=$this->get_StagesList($in);
		}else{
			if(!empty($output['board_list'])){
				$output['board_id']=$output['board_list'][0]['id'];
				$in['board_id']=$output['board_id'];
				$output['stages_list']=$this->get_StagesList($in);
			}
		}
		$output['not_redirect_deal']=$in['not_redirect_deal'];
		
		$this->out = $output;
	}

	public function getEditDeal(){
		$in = $this->in;
		$output=array();

		$deal =	$this->db->query("SELECT * FROM tblopportunity WHERE opportunity_id='".$in['opportunity_id']."' ");
		if(!$deal->f('opportunity_id')){
			msg::error('Deal does not exist','error');
			$in['item_exists']= false;
		    json_out($in);
		}
		$output['item_exists']			= true;
		$output['country_dd']			= $in['country_id']? build_country_list($in['country_id']):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['customer']			= 'customer';
		$output['contact']			= 'contact';
		$output['main_address_id']		= $deal->f('main_address_id');
		$output['sameAddress']			= !$deal->f('same_address') ? true : false;
		$output['buyer_id']             	= $deal->f('buyer_id');
		$in['buyer_id']				= $deal->f('buyer_id');
		$output['customer_id']			= $deal->f('buyer_id');
		$output['contact_id']           	= $in['contact_id']? $in['contact_id'] : $deal->f('contact_id');
		$output['buyer_name']           	= get_customer_name($deal->f('buyer_id'));
		$output['field']				= $deal->f('buyer_id') ? 'customer_id' : 'contact_id';
		if($deal->f('buyer_id')){
			$buyer_email_phone = $this->db->query("SELECT comp_phone,c_email FROM customers WHERE customer_id='".$deal->f('buyer_id')."' ");
			$output['company_phone'] = $buyer_email_phone->f('comp_phone');
			$output['company_email'] = $buyer_email_phone->f('c_email');
		}
		$output['free_field']			= $deal->f('free_field');
		$output['free_field_txt']		= nl2br($deal->f('free_field'));
		$output['delivery_address_id']	= $deal->f('same_address');
		if($deal->f('contact_id')){
			$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$deal->f('contact_id')."' ");
			$output['contact_phone'] = $this->db->field("SELECT cell FROM customer_contacts WHERE contact_id='".$deal->f('contact_id')."' ");
			$output['contact_email'] = $this->db->field("SELECT email FROM customer_contacts WHERE contact_id='".$deal->f('contact_id')."' ");
		}
		
		$output['do_request']		= 'customers-deal_edit-customers-updateCustomerDataDeal';
		$output['cc']			= $this->get_cc($in);
		$output['contacts']		= $this->get_contacts($in);
		$output['addresses']		= $this->get_addresses($in);		
		$output['opportunity_id']		= $deal->f('opportunity_id');
		$output['add_customer']		= false;
		$output['subject']=$deal->f('subject');
		$output['serial_number']=$deal->f('serial_number');
		$output['amount']=display_number($deal->f('expected_revenue'));
		$output['amount_currency']= place_currency(display_number($deal->f('expected_revenue')));
		$output['success_rate']=display_number($deal->f('success_rate'));
		$output['close_date']=$deal->f('exp_close_date')*1000;
		$output['exp_close_date']=$deal->f('exp_close_date')?date(ACCOUNT_DATE_FORMAT,$deal->f('exp_close_date')) :'';
		$output['exp_close_date_js']=$deal->f('exp_close_date')*1000;
		$output['currency_type']=ACCOUNT_CURRENCY_TYPE;
		$output['stage_id'] = $deal->f('stage_id');
		$output['stage_name'] = $this->db->field("SELECT name FROM tblopportunity_stage WHERE id='".$output['stage_id']."' ");

		$output['board_id']=$in['board_id']? $in['board_id']:($this->db->field("SELECT board_id FROM tblopportunity_stage WHERE id='".$output['stage_id']."' "));
		$output['board_name'] = $this->db->field("SELECT name FROM tblopportunity_board WHERE id='".$output['board_id']."' ");
		$output['user_id']=$in['user_id'];
		if(!$in['email_language']){
			$in['email_language'] = $user_lang_id;
		}
		$output['email_language']	= $in['email_language'];
		$in['identity_id']=$deal->f('identity_id');
		$output['language_dd']=build_language_dd_new(0);
		$output['gender_dd']= gender_dd();
		$output['title_dd']= build_contact_title_type_dd(0);
		$output['main_comp_info']= getCompanyInfo($in);
		$output['source_dd']= get_categorisation_source();
		$output['type_dd']= get_categorisation_type();
		$output['identity_id']=$deal->f('identity_id');
		$output['multiple_identity_dd']=build_identity_dd($deal->f('identity_id'));
		$output['type_id']=$deal->f('type_id');
		$output['source_id']=$deal->f('source_id');
		$output['segment_dd']= get_categorisation_segment();
		$output['segment_id']=$deal->f('segment_id');
		$output['user_list'] = $this->get_UserList($in);
		$output['created_by'] = $deal->f('created_by');
		//$output['stage_dd'] = $this->get_AllStagesList($in);
		$output['board_dd']=$this->get_BoardsList($in);
		$in['board_id']=$output['board_id'];
		$output['stage_dd'] = $this->get_StagesList($in);
		$output['author'] = get_user_name($deal->f('created_by'));
		$output['avatar'] = $this->db_users->field("SELECT avatar FROM users WHERE user_id='".$deal->f('created_by')."' ");
		$output['deal_notes']=$deal->f('notes');

		$timeline_docs=['2-1','4','5','1-1','8','7','6'];
		if($in['view']=='0' || !$in['view']){
			$output['list']=$this->get_List($in);
			$output['list_activities']=$this->get_Activity($in);
			foreach($output['list_activities']['activities']['item'] as $value){
				array_push($output['list'],$value);
			}
		}elseif($in['view']=='1' || in_array($in['view'], $timeline_docs)){
			switch($in['view']){
				case '1':	
					$in['target_d']='0';
					break;
				case '1-1':
					$in['target_d']='1';
					break;
				case '2-1':
					$in['target_d']='2';
					break;
				default:
					$in['target_d']=$in['view'];
			}		
			$output['list']=$this->get_List($in);
		}elseif($in['view']=='2'){
			$output['list']=array();
			$output['list_activities']=$this->get_Activity($in);
			foreach($output['list_activities']['activities']['item'] as $value){
				array_push($output['list'],$value);
			}
		}
		
		$exo = array_sort($output['list'], 'creation_date', SORT_DESC);
   
	   // $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $output['list']=array();
	       foreach ($exo as $key => $value) {
	           array_push($output['list'], $value);
	       }
		
		$output['ADV_CRM']= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;

		for ($j = 1; $j<=14; $j++){
				$is = false;
				if(in_array($j,perm::$allow_apps)){
					$is = true;
				}
				
				//$val =$this->db_users->field("SELECT value FROM user_meta WHERE name='MODULE_".$j."' AND user_id='".$_SESSION['u_id']."' ");
				$val =$this->db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$j,'user_id'=>$_SESSION['u_id']]);
				if($val===NULL){
					if(defined('MODULE_'.$j)){
						$val = constant('MODULE_'.$j);
					}
				}
				if($val == 0){
					$is = false;
				}
				
				$output['is_'.$j]=$is;
			}
		$drop_info = array('drop_folder' => 'deals', 'customer_id' => $deal->f('buyer_id'), 'item_id' => $in['opportunity_id'], 'isConcact' => '0', 'serial_number' => ($deal->f('serial_number') ? $deal->f('serial_number') : $in['opportunity_id']));
        $output['is_drop']        = defined('DROPBOX') && (DROPBOX != '') ? true : false;
        $output['drop_info']        = $drop_info;

 		$this->out = $output;
	}

	public function get_List(&$in){
		//$in=$this->in;
	
		$filter= " 1=1 "; 
		if(!empty($in['start_date']) && empty($in['end_date'])){
			$filter .= " AND tracking.creation_date>='".strtotime($in['start_date'])."' ";
		}else if(empty($in['start_date']) && !empty($in['end_date'])){
			$filter .= " AND tracking.creation_date<='".strtotime($in['end_date'])."' ";
		}else if(!empty($in['start_date']) && !empty($in['end_date'])){
			$filter .= " AND tracking.creation_date BETWEEN '".strtotime($in['start_date'])."' AND '".strtotime($in['end_date'])."' ";
		}
		if($in['target_d'] && $in['target_d']!='0'){
                $filter .=" AND tracking.target_type='".$in['target_d']."' ";
		}
		if ($in['target_d'] == 6) { //purchase order - another query approach
		    $all_po_trace = array();
            $all_tracking_lines = $this->db->query("SELECT trace_id FROM tracking_line WHERE origin_id ='". $in['opportunity_id'] ."' ");
            while($all_tracking_lines->next()){
                $orders_tracking = $this->db->query("SELECT target_id FROM tracking WHERE trace_id = '" . $all_tracking_lines->f('trace_id') ."' AND target_type = '4'");
                while($orders_tracking->next()) {
                    $po_trace = $this->db->query("SELECT tracking_line.trace_id FROM tracking_line 
                                LEFT JOIN tracking ON tracking.trace_id = tracking_line.trace_id
                                WHERE tracking_line.origin_id = '". $orders_tracking->f('target_id') ."' AND tracking_line.origin_type = '4' AND tracking.archived = '0' ORDER BY tracking.creation_date DESC")->getAll();
                    $all_po_trace = array_merge($all_po_trace, $po_trace);
                }
            }
            $trace_id = $all_po_trace;
        } else {
            $trace_id = $this->db->query("SELECT tracking.trace_id FROM tracking 
		        LEFT JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
		        WHERE " . $filter . " AND (tracking_line.origin_id='" . $in['opportunity_id'] . "' AND tracking_line.origin_type='11') AND tracking.archived='0' ORDER BY tracking.creation_date DESC")->getAll();
        }
        $tmp_array=array();
		 $trace_ids=array();
		  foreach($trace_id as $val){ 
	         array_push($tmp_array,$val['trace_id']);
		  }
		   	$last_key=0;
		     $loops=0;
		     $do_loop=true;
		     while($do_loop && !$in['target_d']){

		        $sub_docs=$this->db->query("SELECT tracking_line.trace_id
					FROM  `tracking` 
					LEFT JOIN tracking_line ON tracking.target_id = tracking_line.origin_id
					AND tracking.target_type = tracking_line.origin_type
					WHERE tracking.`trace_id` ='".$tmp_array[$last_key]."' ")->getAll();
		          if(count($sub_docs)){
		          		 
		                 foreach($sub_docs as $key=>$value){

		                    if($value['trace_id'] && (in_array($value['trace_id'], $tmp_array) === false)){
		                      array_push($tmp_array,$value['trace_id']);
		                     } 
		                 } 
		                 	
		          }
		         /* console::log($last_key, count($tmp_array));*/
		          if($last_key<count($tmp_array)){
		           $last_key++;
		          }    
		          $loops++;
		         
		          if($last_key==count($tmp_array) && $loops%2==0){
		           $do_loop=false;
		          }
		          
		     }
		 	    
		  foreach($tmp_array as $value){
		   array_push($trace_ids,$value);
		  }

		   $items=array();

		 // var_dump($trace_ids);exit();

		 $cases=array(
			'1'=>gm('Invoice'),
			'2'=>gm('Quote'),
			'3'=>gm('Project'),
			'4'=>gm('Order'),
			'5'=>gm('Intervention'),
			'6'=>gm('Purchase Order'),
			'7'=>gm('Proforma'),
			'8'=>gm('Credit Invoice'),
			'9'=>gm('Contract'),
			'10'=>gm('Purchase Invoice'),
			'11'=>gm('Deal'),
			// '12'=>gm('Installation'),
			);
		$type_mach=array(
			'1'=>'invoice',
			'2'=>'quote',
			'3'=>'project',
			'4'=>'order',
			'5'=>'service',
			'6'=>'purchase_order',
			'7'=>'invoice',
			'8'=>'invoice',
			'9'=>'contract',
			'10'=>'purchase_invoice',
			'11'=>'opportunity',
			// '12'=>'installation',
			);
		$type_img=array(
			'1'=>'fas fa-file-invoice',
			'2'=>'fas fa-hand-holding-usd',
			'3'=>'fas fa-user-cog',
			'4'=>'fas fa-shipping-fast',
			'5'=>'fas fa-wrench',
			'6'=>'fas fa-shipping-fast',
			'7'=>'fas fa-file-invoice',
			'8'=>'fas fa-file-invoice',
			'9'=>'fas fa-file-contract',
			'10'=>'fas fa-file-invoice',
			// '12'=>'interventionsok',
			);

		 foreach($trace_ids as $val){ 
	    	$data= $this->db->query("SELECT * FROM  `tracking` WHERE  `trace_id` = '".$val."'");
	    	$version_ids=array();
	    	if($data->f('target_type') == 2){
	    		$q_version_nr = $this->db->query("SELECT version_id FROM tblquote_version WHERE quote_id='".$data->f('target_id')."' ")->getAll();
	    		if($q_version_nr > 1){
	    			foreach($q_version_nr as $vrs){
	    				$version_ids[]=$vrs['version_id'];
	    			}
	    		}
	    	}
	    	if(count($version_ids)>1 && $data->f('target_type') == 2){
	    		foreach($version_ids as $vrs){
	    			$more_data=$this->get_Details($data->f('target_id'),$data->f('target_type'), $in['search'], $vrs);
			    	if(!empty($more_data)){
			    		
			    		$last='.view';
						if($type_mach[$data->f('target_type')]=='project'){
							$last='.edit';
						}elseif($type_mach[$data->f('target_type')]=='service'){
							$last='.edit';
						}else{
							$last='.view';
						}

						$origins=array();
						$source_doc= $this->db->query("SELECT origin_id, origin_type FROM  `tracking_line` WHERE  `trace_id` = '".$val."'");
						while($source_doc->next()){
							$orig_last='.view';
							if($type_mach[$source_doc->f('origin_type')]=='project'){
								$orig_last='.edit';
							}elseif($type_mach[$source_doc->f('origin_type')]=='service'){
								$orig_last='.edit';
							}else{
								$orig_last='.view';
							}

							$more_data_source=$this->get_Details($source_doc->f('origin_id'),$source_doc->f('origin_type'));
							$origin_edit = $source_doc->f('origin_type')=='11'?'dealView' : strtolower($type_mach[$source_doc->f('origin_type')]).$orig_last;
							$origin_type_id = strtolower($type_mach[$source_doc->f('origin_type')]).'_id';
							$temp_origin_item=array(	
								'origin_id'				=> $source_doc->f('origin_id'),		
								'origin_serial_nr' 		=> $more_data_source['serial_number'],
								'origin_type'  			=> $cases[$source_doc->f('origin_type')],
								'origin_edit'  			=> $origin_edit,
								'origin_type_id' 		=> $origin_type_id,
								'origin_sref'			=> $origin_edit."({ '".$origin_type_id."':".$source_doc->f('origin_id')." })"
								);
							array_push($origins,$temp_origin_item);	
							}
			    		

				    	$temp_item=array(
							'id'			=> $data->f('target_id'),
							'type_nr'		=> $data->f('target_type'),
							'type'  		=> $cases[$data->f('target_type')],
							'hour'			=> date('H:i',$more_data['version_creation_date']),
							'time'  		=> date(ACCOUNT_DATE_FORMAT,$more_data['version_creation_date']),
							'creation_date'	=> $more_data['version_creation_date'],
							'created'		=> get_user_name($data->f('creation_user_id')),
							'date'  		=> $more_data['version_creation_date'],
					       'img'  			=> $type_img[$data->f('target_type')],
					       'amount' 		=> $more_data['amount'],
					       'serial_nr' 		=> $more_data['serial_number'].' ['.$more_data['version_code'].']',
					       'message' 		=> $more_data['serial_number'],
					       'pdf_link' 		=> $more_data['pdf_link'],
					       'sref' 			=> $more_data['sref'],
					       'danger' 		=> $more_data['danger'],
					       'danger_corner'	=>$more_data['danger_corner'],
					       'edit'  			=> strtolower($type_mach[$data->f('target_type')]).$last,
							'type_id' 		=> strtolower($type_mach[$data->f('target_type')]).'_id',
							'is_activity'	=>0,
							'origins'		=>$origins,
							'status' 		=> $more_data['status'],
							'color_status' 	=> $more_data['color_status'],
							'your_ref' 		=> $more_data['your_ref'],
							'subject' 		=> $more_data['subject'],
							'supplier' 		=> $more_data['supplier'],
							'version_id'	=> $vrs
							);
					    
						 array_push($items,$temp_item);
				    }
	    		}
	    	}else{
	    		$more_data=$this->get_Details($data->f('target_id'),$data->f('target_type'), $in['search']);
		    	if(!empty($more_data)){
		    		
		    		$last='.view';
					if($type_mach[$data->f('target_type')]=='project'){
						$last='.edit';
					}elseif($type_mach[$data->f('target_type')]=='service'){
						$last='.edit';
					}else{
						$last='.view';
					}

					$origins=array();
					$source_doc= $this->db->query("SELECT origin_id, origin_type FROM  `tracking_line` WHERE  `trace_id` = '".$val."'");
					while($source_doc->next()){
						$orig_last='.view';
						if($type_mach[$source_doc->f('origin_type')]=='project'){
							$orig_last='.edit';
						}elseif($type_mach[$source_doc->f('origin_type')]=='service'){
							$orig_last='.edit';
						}else{
							$orig_last='.view';
						}

						$more_data_source=$this->get_Details($source_doc->f('origin_id'),$source_doc->f('origin_type'));

						$origin_edit = $source_doc->f('origin_type')=='11'?'dealView' : strtolower($type_mach[$source_doc->f('origin_type')]).$orig_last;
						$origin_type_id = strtolower($type_mach[$source_doc->f('origin_type')]).'_id';
						$temp_origin_item=array(	
							'origin_id'				=> $source_doc->f('origin_id'),		
							'origin_serial_nr' 		=> $more_data_source['serial_number'],
							'origin_type'  			=> $cases[$source_doc->f('origin_type')],
							'origin_edit'  			=> $origin_edit,
							'origin_type_id' 		=> $origin_type_id,
							'origin_sref'			=> $origin_edit."({ '".$origin_type_id."':".$source_doc->f('origin_id')." })"
							);
						array_push($origins,$temp_origin_item);	

						}
		    		

			    	$temp_item=array(
						'id'			=> $data->f('target_id'),
						'type_nr'		=> $data->f('target_type'),
						'type'  		=> $cases[$data->f('target_type')],
						'hour'			=> date('H:i',$data->f('creation_date')),
						'time'  		=> date(ACCOUNT_DATE_FORMAT,$data->f('creation_date')),
						'creation_date'	=> $data->f('creation_date'),
						'created'		=> get_user_name($data->f('creation_user_id')),
						'date'  		=> $more_data['date'],
				       'img'  			=> $type_img[$data->f('target_type')],
				       'amount' 		=> $more_data['amount'],
				       'serial_nr' 		=> $more_data['serial_number'],
				       'message' 		=> $more_data['serial_number'],
				       'pdf_link' 		=> $more_data['pdf_link'],
				       'sref' 			=> $more_data['sref'],
				       'danger' 		=> $more_data['danger'],
				       'danger_corner'	=>$more_data['danger_corner'],
				       'edit'  			=> strtolower($type_mach[$data->f('target_type')]).$last,
						'type_id' 		=> strtolower($type_mach[$data->f('target_type')]).'_id',
						'is_activity'	=>0,
						'origins'		=>$origins,
						'status' 		=> $more_data['status'],
						'color_status' 	=> $more_data['color_status'],
						'your_ref' 		=> $more_data['your_ref'],
						'subject' 		=> $more_data['subject'],
						'supplier' 		=> $more_data['supplier'],
						'version_id'	=> ''
						);
				    
					 array_push($items,$temp_item);
			    }
	    	}
	    	
		}
		usort($items, function($a, $b) {return strcmp($a['creation_date'], $b['creation_date']);});
		return $items;
	}

	private function get_Details($id,$type, $search = false, $version=0){
		$obj=array();
		switch($type){
			case '1':
				//Invoice
			case '7':
				//Proforma
			case '8':
				//Credit Invoice
				if($search){
					$filter_s = " AND tblinvoice.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT * FROM tblinvoice WHERE id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['date']=$data->f('invoice_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date')) : '';
					$obj['amount']=display_number($data->f('amount'));
					$obj['serial_number']=$data->f('serial_number');
					$obj['your_ref']=stripslashes($data->f('your_ref'));
					//$obj['subject']=stripslashes($data->f('our_ref'));
					$obj['subject']=stripslashes($data->f('subject'));

					if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
						$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
					}
					$link_credit_end = '&type='.ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
					if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $data->f('pdf_layout') == 0){
						$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
					}
					if($data->f('type')==2){
						$obj['pdf_link']= 'index.php?do=invoice-invoice_credit_print&id='.$id.'&lid='.$data->f('email_language').$link_credit_end;
					}else{
						$obj['pdf_link']= 'index.php?do=invoice-invoice_print&id='.$id.'&lid='.$data->f('email_language').$link_end;
					}
					if($data->f('not_paid') == '0' && $data->f('status') == '0' && $data->f('sent') == '1' && $data->f('due_date') < time()){
						$obj['danger']='ak_activities-danger' ;
						$obj['danger_corner']='border_right_danger';
				    }

				   	$invoice_status_info=get_status_invoice($id);
					$obj['status'] = $invoice_status_info['status'];
					$obj['color_status'] = $invoice_status_info['color_status'];
					$obj['sref']= "invoice.view({ 'invoice_id':".$id." })";
					
				}
				
				break;
			case '2':
				//Quote
				if($search){
					$filter_s = " AND tblquote.serial_number LIKE '%".$search."%' ";
				}
				if($version){
					$filter_s.= " AND tblquote_version.version_id='".$version."' ";
				}
				$data=$this->db->query("SELECT tblquote.*,tblquote_version.version_id,tblquote_version.version_code,tblquote_version.version_date,tblquote_version.version_subject,tblquote_version.version_own_reference,tblquote_version.version_status_customer FROM tblquote 
					INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
					WHERE tblquote.id='".$id."' ".$filter_s);
				if($data->next()){
					$amount = $this->db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$id."' AND version_id='".$data->f('version_id')."' AND show_block_total='0' ");

					if($data->f('discount') && $data->f('apply_discount') > 1){
						$amount = $amount - ($amount*$data->f('discount')/100);
					}

					if($data->f('currency_rate')){
						$amount = $amount*return_value($data->f('currency_rate'));
					}
					$obj['date']=$data->f('quote_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('quote_date')) : '';
					$obj['amount']=display_number($amount);
					$obj['serial_number']=$data->f('serial_number');
					$obj['your_ref']=stripslashes($data->f('version_own_reference'));
					$obj['subject']=stripslashes($data->f('version_subject'));

					if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
						$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
					}
					#if we are using a customer pdf template
					if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $data->f('pdf_layout') == 0){
						$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT;
					}
					$obj['pdf_link']='index.php?do=quote-print&id='.$id.'&version_id='.$data->f('version_id').'&lid='.$data->f('email_language').$link_end;
					$obj['sref']= "quote.view({ 'quote_id':".$id.", version_id:".$data->f('version_id')." })";
					$today = mktime(0,0,0,date('n'),date('j'),date('Y'));
					if($data->f('version_status_customer') == 0){
						$valid = mktime(0,0,0,date('n',$data->f('validity_date')),date('j',$data->f('validity_date')),date('Y',$data->f('validity_date')));
						if($valid < $today || $data->f('sent') == 1){
							$obj['danger']='ak_activities-danger' ;
							$obj['danger_corner']='border_right_danger';
						}
					}

					$quote_status_info=get_status_quote($version ? $version : $data->f('version_id'));
					$obj['status'] = $quote_status_info['status'];
					$obj['color_status'] = $quote_status_info['color_status'];
					$obj['version_code']=$data->f('version_code');
					$obj['version_creation_date']=$data->f('version_date');
				}

				break;
			case '3':
				//Project
				if($search){
					$filter_s = " AND projects.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT * FROM projects WHERE project_id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['date']=$data->f('start_date') ? '<span>'.gm('Start Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('start_date')) : '';
					$obj['amount']=display_number(0);
					$obj['serial_number']=$data->f('serial_number');
					$obj['pdf_link']='';
					$obj['sref']= "project.edit({ 'project_id':".$id." })";
					$obj['your_ref']='';
					$obj['subject']=stripslashes($data->f('name'));

					$project_status_info=get_status_project($id);
					$obj['status'] = $project_status_info['status'];
					$obj['color_status'] = $project_status_info['color_status'];
				}
				break;
			case '4':
				//Order
				if($search){
					$filter_s = " AND pim_orders.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT * FROM pim_orders WHERE order_id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['date']=$data->f('del_date') ? '<span>'.gm('Delivery Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('del_date')) : '';
					$obj['amount']=display_number($data->f('amount'));
					$obj['serial_number']=$data->f('serial_number');
					$obj['your_ref']=stripslashes($data->f('your_ref'));
					//$obj['subject']=stripslashes($data->f('our_ref'));
					$obj['subject']=stripslashes($data->f('subject'));

					$default_lang = $data->f('email_language');
					if(!$default_lang){
						$default_lang = 1;
					}
					if($data->f('pdf_layout')){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						$link_end = '&type='.ACCOUNT_ORDER_PDF_FORMAT;
					}
					$obj['pdf_link']= 'index.php?do=order-order_print&order_id='.$id.'&lid='.$default_lang.$link_end;
					$obj['sref']= "order.view({ 'order_id':".$id." })";

					$order_status_info=get_status_order($id);
					$obj['status'] = $order_status_info['status'];
					$obj['color_status'] = $order_status_info['color_status'];

				}
				break;
			case '5':
				//Intervention
				if($search){
					$filter_s = " AND servicing_support.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['date']=$data->f('planneddate') ? '<span>'.gm('Planned Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('planneddate')) : '';
					$obj['amount']=display_number(0);
					$obj['serial_number']=$data->f('serial_number');
					$obj['pdf_link']='index.php?do=maintenance-print&service_id='.$id;
					$obj['sref']= "service.view({ 'service_id':".$id." })";
					$obj['your_ref']=stripslashes($data->f('your_ref'));
					$obj['subject']=stripslashes($data->f('subject'));

					$intervention_status_info=get_status_intervention($id);
					$obj['status'] = $intervention_status_info['status'];
					$obj['color_status'] = $intervention_status_info['color_status'];
				}
				break;
			case '6':
				//Purchase Order
				if($search){
					$filter_s = " AND pim_p_orders.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['date']=$data->f('del_date') ? '<span>'.gm('Delivery Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('del_date')) : '';
					$obj['amount']=display_number($data->f('amount'));
					$obj['serial_number']=$data->f('serial_number');
					$obj['your_ref']=stripslashes($data->f('your_ref'));
					$obj['subject']=stripslashes($data->f('our_ref'));
					$obj['supplier']=$data->f('customer_name');

					$lid = $data->f('email_language');
					$pdf_del_type = $data->f('p_order_type')? $data->f('p_order_type'):ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
					if(!$lid){
						$lid = 1;
					}
					$obj['pdf_link']='index.php?do=po_order-p_order_print&p_order_id='.$id.'&lid='.$lid.'&type='.$pdf_del_type;
					$obj['sref']= "purchase_order.view({ 'p_order_id':".$id." })";

					$p_order_status_info=get_status_p_order($id);
					$obj['status'] = $p_order_status_info['status'];
					$obj['color_status'] = $p_order_status_info['color_status'];
				}
				break;
			case '9':
				//Contracts
				if($search){
					$filter_s = " AND contracts.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT contracts.*,contracts_version.version_id FROM contracts 
					INNER JOIN contracts_version ON contracts.contract_id=contracts_version.contract_id AND contracts_version.active=1
					WHERE contracts.contract_id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['contract_active']= $data->f('active');
					$amount = $this->db->field("SELECT SUM(amount) FROM contract_line WHERE contract_id='".$id."' AND version_id='".$data->f('version_id')."' AND show_block_total='0' ");
					if($data->f('discount') && $data->f('apply_discount') > 1){
						$amount = $amount - ($amount*$data->f('discount')/100);
					}
					if($data->f('currency_rate')){
						$amount = $amount*return_value($data->f('currency_rate'));
					}
					$obj['date']=$data->f('start_date') ? '<span>'.gm('Start Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('start_date')) : '';
					$obj['amount']=display_number($amount);
					$obj['serial_number']=$data->f('serial_number');
					$obj['your_ref']=stripslashes($data->f('your_ref'));
					$obj['subject']=stripslashes($data->f('subject'));

					if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
						$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						if(ACCOUNT_CONTRACT_PDF_FORMAT){
							$link_end = '&type='.ACCOUNT_CONTRACT_PDF_FORMAT;
						}else{
							$link_end = '&type=1';
						}	
					}
					$obj['pdf_link']='index.php?do=contract-print&contract_id='.$id.'&version_id='.$data->f('version_id').'&lid='.$data->f('email_language').$link_end;
					$obj['sref']= "contract.view({ 'contract_id':".$id.", version_id:".$data->f('version_id')." })";
					$contract_status_info=get_status_contract($id);
					$obj['status'] = $contract_status_info['status'];
					$obj['color_status'] = $contract_status_info['color_status'];
				}
				break;
			case '10':
				//Purchase Invoice
				if($search){
					$filter_s = " AND tblinvoice_incomming.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['date']=$data->f('invoice_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date')) : '';
					$obj['amount']=display_number($data->f('total'));
					$obj['serial_number']=$data->f('booking_number');
					$obj['pdf_link']=$data->f('file_name') ? 'index.php?do=invoice-purchase_print&id='.$id : '';
					$obj['sref']= "purchase_invoice.view({ 'invoice_id':".$id." })";
					$obj['your_ref']='';
					$obj['subject']='';

					$purchase_invoice_status_info=get_status_purchase_invoice($id);
					$obj['status'] = $purchase_invoice_status_info['status'];
					$obj['color_status'] = $purchase_invoice_status_info['color_status'];
				}
				break;
			case '11':
				//Deal
				if($search){
					$filter_s = " AND tblopportunity.serial_number LIKE '%".$search."%' ";
				}
				$data=$this->db->query("SELECT * FROM tblopportunity WHERE opportunity_id='".$id."' ".$filter_s);
				if($data->next()){
					$obj['date']=$data->f('opportunity_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('opportunity_date')) : '';
					$obj['amount']=display_number($data->f('expected_revenue'));
					$obj['serial_number']=$data->f('serial_number');
					$obj['subject']=stripslashes($data->f('subject'));
					$obj['your_ref']='';
					$obj['pdf_link']= '';
					$obj['sref']= "dealView({ 'opportunity_id':".$id." })";
				}
				break;
			// case '12':
			// 	//Installation
			// 	$data=$this->db->query("SELECT * FROM installations WHERE id='".$id."' ");
			// 	$obj['date']=$data->f('date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('date')) : '';
			// 	$obj['amount']='';
			// 	$obj['serial_number']=$data->f('serial_number');
			// 	$obj['pdf_link']='';
			// 	break;
		}
		return $obj;
	}

	function get_Activity($in)
	{
		//get_ZenTickets($in);
		$reminder_ids_data=array_column(build_reminder_dd(), 'name', 'id');
		$db = new sqldb();
		global $config;
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		
		if($in['from_date']){
			$in['from_date'] = strtotime($in['from_date']);
		}
		if($in['to_date']){
			$in['to_date'] = strtotime($in['to_date']);
		}

		if(!$in['user_id'])
		{
		  	$in['user_id'] = $_SESSION['u_id'];
		}
		
		$result = array( 'item' => array() );

		$now = time();
		
		$limit = "";

		if( isset($in['offset']) ){
			$limit = " LIMIT ".($in['offset']*5)." ,5";
		}

		$filter_u = ' 1=1 ';
		$filter_u2 = ' ';
		if($in['user_id']){
			$filter_u2 = " AND user_id='".$in['user_id']."' ";
		}
		$filter_c = '';
		$filter_q = '';
		$join = '';


		if($in['customer2_id']){
			$in['customer_id'] = $in['customer2_id'];
		}
		
		switch ($in['filter_type']) {
			case 'date':
				if($in['from_date'] && !$in['to_date'])
				{
					$filter_c = "AND logging.due_date>'".$in['from_date']."'";
				}elseif(!$in['from_date'] && $in['to_date'])
				{
					$filter_c = "AND logging.due_date<'".$in['to_date']."'";
				}elseif($in['from_date'] && $in['to_date'])
				{
					$filter_c = "AND logging.due_date<'".$in['to_date']."' AND logging.due_date>'".$in['from_date']."'";
				}else
				{
					$filter_c = '';
				}
				if($in['customer_id']){
					$filter_c .= " AND customer_contact_activity.customer_id='".$in['customer_id']."'";
				}
				break;
			case 'event':
				if($in['event_type'])
				{
					$filter_c = "AND contact_activity_type='".$in['event_type']."'";
					if($in['event_type']!=7)
					{
					}
				}
				if($in['c_id']){
					$filter_c .= "AND customer_contact_activity.customer_id='".$in['c_id']."'";
				}
				break;
			case 'customer':
				if($in['customer_id'])
				{
					$filter_c = "AND customer_contact_activity.customer_id='".$in['customer_id']."'";
				}
				
				break;
			case 'user_timeline':
				if($in['from_date'] && !$in['to_date']){
					$filter_c = " AND logging.due_date>'".$in['from_date']."' ";
				}elseif(!$in['from_date'] && $in['to_date']){
					$filter_c = " AND logging.due_date<'".$in['to_date']."' ";
				}elseif($in['from_date'] && $in['to_date']){
					$filter_c = " AND logging.due_date<'".$in['to_date']."' AND logging.due_date>'".$in['from_date']."' ";
				}
				if($in['customer_id']){
					$filter_c .= " AND customer_contact_activity.customer_id='".$in['customer_id']."' ";
				
					if($in['activity_id']){
						$filter_c .= " AND customer_contact_activity.contact_activity_type='".$in['activity_id']."' ";
					}
				}
				$filter_u = " email_to='".$in['user_id']."' ";

				if($in['activity_id']){
						$filter_c .= " AND customer_contact_activity.contact_activity_type='".$in['activity_id']."' ";
				}

				break;
		}

		if($in['contact_id2']){
			$in['contact_id'] = $in['contact_id2'];
		}

		if($in['contact_id']){
			$filter_c .= " AND customer_contact_activity.contact_id='".$in['contact_id']."' OR (
						customer_contact_activity.contact_id IS NULL
						AND customer_contact_activity_contacts.contact_id =".$in['contact_id']."
						) ";
			$join = ' LEFT JOIN customer_contact_activity_contacts ON customer_contact_activity.`customer_contact_activity_id` = customer_contact_activity_contacts.activity_id AND action_type = 0';
		}
		if($in['opportunity_id'])
		{
			$filter_c = " AND customer_contact_activity.opportunity_id='".$in['opportunity_id']."' ";
		}

		if($in['search']){
			$filter_c = " AND customer_contact_activity.contact_activity_note LIKE '%".$in['search']."%' ";
		}

		// $result['page_title']=get_user_name($in['user_id']);
		$zen_base=$db->field("SELECT api FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");

		$users_auto = $db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
		$user_auto = array();
		foreach ($users_auto as $key => $value) {
			$user_auto[] = array('id'=>$value['user_id'], 'value'=>htmlspecialchars(strip_tags($value['first_name'] . ' ' . $value['last_name'])) );
		}

		$i=0;
		$customers_id = array();
		if( isset($in['offset']) ){
			$result['max_rows'] = $db->field("SELECT COUNT( DISTINCT customer_contact_activity_id )  FROM customer_contact_activity
							{$join}
							INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
							LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
							WHERE {$filter_u} {$filter_c} ");
		}
		$comments = $db->query("SELECT customer_contact_activity.* FROM customer_contact_activity
						{$join}
						INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
						LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
						WHERE {$filter_u} {$filter_c}
						GROUP BY customer_contact_activity.customer_contact_activity_id ORDER BY logging.due_date DESC {$limit} ")->getAll();

		$days = array();
		$contact_name = array();
		// $user_name = array();
		$users_id = array();
		$w = 0;
		foreach ($comments as $key => $value) {
			$new_event = '';
			$source='-';
			$stage = '-';
			$type='-';
			$side = 'left';
			$activity_status='';
			$color='';
			$assigned = '';
			$assigned_id = '';
			$type_of_call='';
			$type_of_call_id='';
			$status_change = 0;
			if($value['customer_contact_activity_id'])
			{
				$db->query("SELECT * FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' ".$filter_u2." ");
				$db->move_next();
				$un_log_id = $db->f('log_id');
				if($db->f('seen')=='0')
				{
					$new_event = 'new_event';
					$db->query("UPDATE logging_tracked SET seen='1' WHERE activity_id='".$value['customer_contact_activity_id']."' ".$filter_u2." ");
				}
					$log_assigned=$db->query("SELECT finished, due_date, to_user_id FROM logging WHERE log_id='".$un_log_id."' ");
					if(!array_key_exists($log_assigned->f('to_user_id'), $users_id)){
						$users_id[$log_assigned->f('to_user_id')] = get_user_name($log_assigned->f('to_user_id'));
					}
					$assigned = $users_id[$log_assigned->f('to_user_id')];	
					$assigned_id = $log_assigned->f('to_user_id');				
			}
			elseif($value['log_id']){
				$db->query("SELECT * FROM logging_tracked WHERE log_id='".$value['log_id']."' ".$filter_u2." ");
				$db->move_next();
				if($db->f('seen')=='0')
				{
					$new_event = 'new_event';
					$db->query("UPDATE logging_tracked SET seen='1' WHERE log_id='".$value['log_id']."' ".$filter_u2." ");
				}
			}
			else{
				if($value['sent_date']){
					$log_id = $db->field("SELECT log_id FROM logging WHERE field_name='quote_id' AND field_value='".$value['quote_id']."' AND date='".$value['sent_date']."'");
					if($log_id){
						$db->query("SELECT * FROM logging_tracked WHERE log_id='".$log_id."' ".$filter_u2." ");
						$db->move_next();
						if($db->f('seen')=='0')
						{
							$new_event = 'new_event';
							$db->query("UPDATE logging_tracked SET seen='1' WHERE log_id='".$log_id."' ".$filter_u2." ");
						}
					}
				}
			}
			
			switch ($value['contact_activity_type']) {
				case '1':
					$action = 'email';
					$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
					$status_values->move_next();
					$db->query("SELECT log_id,finished, due_date,status_other,type_of_call FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
					if($db->f('finished') == 1){
						$activity_status = gm('Completed');
						$color = 'green_status';
						$status_change = 1;
					}else{
						$activity_status = gm('Scheduled');
						$color = 'opened_status';
						$status_change = 0;
					}
					$log_code=$db->f('log_id');

					if($db->f('type_of_call') == 1){
						$type_of_call = gm('incoming email');
						$type_of_call_id = 1;
						
					}else{
						$type_of_call = gm('outgoing email');
						$type_of_call_id = 0;
						
					}
					break;
				case '2':
					$action = 'events';
					$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
					$status_values->move_next();
					$db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
					if($db->f('finished') == 1){
						$activity_status = gm('Completed');
						$color = 'green_status';
						$status_change = 1;
					}else{
						$activity_status = gm('Scheduled');
						$color = 'opened_status';
						$status_change = 0;
					}
					$log_code=$db->f('log_id');
					break;
				case '3':
					$action = 'fax';
					break;
				case '4':
					$action = 'meeting';
					$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
					$status_values->move_next();
					$db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
					if($db->f('finished') == 1){
						$activity_status = gm('Completed');
						$color = 'green_status';
						$status_change = 1;
					}else{
						$activity_status = gm('Scheduled');
						$color = 'opened_status';
						$status_change = 0;
					}
					$log_code=$db->f('log_id');
					break;
				case '5':
					$action = 'phone';
					$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
					$status_values->move_next();
					$db->query("SELECT log_id,finished, due_date,status_other,type_of_call FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
					if($db->f('finished') == 1){
						$activity_status = gm('Completed');
						$color = 'green_status';
						$status_change = 1;
					}else{
						$activity_status = gm('Scheduled');
						$color = 'opened_status';
						$status_change = 0;
					}
					if($db->f('type_of_call') == 1){
						$type_of_call = gm('incoming call');
						$type_of_call_id = 1;
						
					}else{
						$type_of_call = gm('outgoing call');
						$type_of_call_id = 0;
						
					}
					$log_code=$db->f('log_id');
					break;
				default:
					$action = $value['zen_ticket_id'] ? 'zen' : ($value['nylas_email_id'] ? 'nylas': 'other');
					$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
					$db->query("SELECT log_id, finished, reminder_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
					$status_change = 0;
					if($db->f('finished') == 1){
						$activity_status = gm('Completed');
						$color = 'green_status';
						$status_change = 1;
					}else if($db->f('reminder_date') < time()){
						$activity_status = gm('Late');
						$color = 'red_status';
					}else if($db->f('status_other') == 3){
						$activity_status = gm('On hold');
						$color = 'opened_status';
					}else if($db->f('status_other') == 2){
						$activity_status = gm('In progress');
						$color = 'opened_status';
					}else{
						$activity_status = gm('New');
						$color = 'opened_status';
					}
					$log_code=$db->f('log_id');
					break;
			}

			$message = nl2br($value['contact_activity_note']);
			// $long_message = $value['contact_activity_note'];
			if($value['email_to'] && !array_key_exists($value['email_to'], $user_id)) {
				$user_id[$value['email_to']] = get_user_name($value['email_to']);
			}
			$name = $value['zen_ticket_id'] && !$value['is_our_zen'] ? 'Zendesk API': ($value['nylas_email_id'] ? 'Nylas API' : $user_id[$value['email_to']]);

			$contacts_array = array();
			$contact_ids=array();
			$c = array();
			$assign_to_str = '';
			if($value['contact_id'])
			{
				if(!array_key_exists($value['contact_id'], $contact_name)){
					$contact_name[$value['contact_id']] = get_contact_first_and_name($value['contact_id']);
				}

				$assign_to_str	= $value['contact_id'];

				$c['contact_name'] = $contact_name[$value['contact_id']];
				$c['contact_id']= $value['contact_id'];
				array_push($contacts_array, $c);
				$contact_ids[]=$c['contact_id'];

			}else
			{
				$contacts= '';
				$db->query("SELECT * FROM customer_contact_activity_contacts WHERE activity_id='".$value['customer_contact_activity_id']."' AND action_type='0'");
				while($db->move_next())
				{
					if(!array_key_exists($db->f('contact_id'), $contact_name))
					{
						$contact_name[$db->f('contact_id')] = get_contact_first_and_name($db->f('contact_id'));
					}
					$contacts = $contact_name[$db->f('contact_id')];
					$assign_to_str	= $db->f('contact_id');
					$c['contact_name'] =  $contact_name[$db->f('contact_id')];
					$c['contact_id']= $db->f('contact_id');

					array_push($contacts_array, $c);
					$contact_ids[]=$c['contact_id'];

				}
				
			}
			
			if(!$value['contact_activity_type'])
			{
				if($value['quote_id'])
				{
					$customer = $q[$value['quote_id']]['customer'];
					$customer_id = $q[$value['quote_id']]['customer_id'];
					if(!in_array($q[$value['quote_id']]['customer_id'], $customers_id) && $q[$value['quote_id']]['customer_id']!='0')
					{
						array_push($customers_id, $q[$value['quote_id']]['customer_id']);
					}
				}else
				{
					$customer = $q[$value['field_value']]['customer'];
					$customer_id = $q[$value['field_value']]['customer_id'];
					if(!in_array($q[$value['field_value']]['customer_id'], $customers_id) && $q[$value['field_value']]['customer_id']!='0')
					{
						array_push($customers_id, $q[$value['field_value']]['customer_id']);
					}
				}
				$events['7'] = 'quotes';
			}else
			{
				if($value['customer_id'])
				{
					$customer = get_customer_name($value['customer_id']);
					$customer_id = $value['customer_id'];
					if(!in_array($value['customer_id'], $customers_id))
					{
						array_push($customers_id, $value['customer_id']);
					}
				}
				$events[$value['contact_activity_type']]=$action;
			}
			
			$user_assigned = $db->query("SELECT logging.user_id, logging.to_user_id, logging.due_date FROM logging_tracked
				INNER JOIN logging ON logging_tracked.log_id=logging.log_id WHERE activity_id='".$value['customer_contact_activity_id']."' ");
			$date = $user_assigned->f('due_date');
			if(is_null($date)){
				$date = $value['date'];
			}
			$day = date(ACCOUNT_DATE_FORMAT,$date);

			$value['date'] = $value['date']+$_SESSION['user_timezone_offset'];

			$meeting_end_date='';
			$location_meet='';
			if($value['contact_activity_type'] == '4'){
				$meeting_data = $db->query("SELECT * FROM customer_meetings WHERE activity_id='".$value['customer_contact_activity_id']."' ");

				$meeting_end_date=$meeting_data->f('end_date');
				$location_meet=stripslashes($meeting_data->f('location'));
			}
		
			$item_line = array(
			    'contacts'				=> $contacts_array, 
			    'assign_to'				=> $assign_to_str,
				// "side"				=> $side,
				'i'						=> $value['message'],
				'action'				=> $action,
				'message'				=> strlen(strip_tags($message)) > 30 ? substr(strip_tags($message),0,30).'...' : strip_tags($message),
				'time'					=> date('H:i',$date),
				'time2'					=> date('G:i',$date),
				'day_tmpstmp'			=> $date,
				'creation_date'			=> $date,
				'day'					=> $day,
				'type_of_call'			=> $type_of_call,
				'customer'				=> $customer,
				'customer_id'			=> $customer_id,
				'show_day'				=> in_array($day, $days) ? false : true,
				'commented'				=> $value['log_comment'] ? 'commented' : '',
				'add_note'				=> $value['log_comment'] ? gm('Edit note') : gm('Add Note'),
				'hide_note_if_exist'	=> $value['log_comment'] ? 'hide' : '',
				'comments'				=> $value['zen_ticket_id'] && !$value['is_our_zen'] ? nl2br(stripslashes(htmlspecialchars($value['log_comment']))) : $value['log_comment'],
				'is_comments'			=> $value['log_comment'] ? '': 'hide',
				'by'					=> $name,
				'name'					=> $name,
				// 'contact_name'		=> get_contact_name($value['contact_id']),
				//'assign_to'			=> $value['contact_id'],
				'log_id'				=> $value['customer_contact_activity_id'],
				'stage'					=> $stage,
				'source'				=> $source,
				'type'					=> $type,
				'new_event'				=> $new_event,
				'activity_status'		=> $activity_status,
				'color'					=> $color,
				'to_user'				=> $assigned,
				'to_user_id'			=> $assigned_id,
				// 'assigned'			=> $assigned,
				'to'					=> $assigned ? true : false,
				'reminder_tmpstmp'		=> $value['reminder_date'],
				'reminder_time'			=> date('G:i',$value['reminder_date']),
				'reminder_date' 		=> date(ACCOUNT_DATE_FORMAT,$value['reminder_date']).', '.date('H:i',$value['reminder_date']),
				'is_reminder_date' 		=> $value['reminder_date'] > 0 ? true : false,
				'not_task'				=> $value['contact_activity_type']  < 6 ? true : false,
				'log_comment_date' 		=> date(ACCOUNT_DATE_FORMAT,$value['log_comment_date']).', '.date('H:i',$value['log_comment_date']),
				'is_log_comment_date' 	=> $value['log_comment_date'] ? true : false,
				'log_code'				=> $log_code ? $log_code : '',
				'reminder_assign'		=> $value['reminder_id'] > 1 ?true : false,
				'reminder_assign_data'	=> $value['reminder_id'] ? $reminder_ids_data[$value['reminder_id']] : '',
				'assigned_to_me'		=> ($user_assigned->f('user_id') == $_SESSION['u_id'] || $user_assigned->f('to_user_id')==$_SESSION['u_id'] || $_SESSION['group']=='admin') ? true : false,
				'allowed_delete'		=> ($user_assigned->f('user_id') == $_SESSION['u_id'] ||  $_SESSION['group']=='admin') ? true : false,
				// 'day_hours_time'			=> build_day_hours(date('H:i',$date)),
				'type_of_call_dd'		=> build_simple_dropdown(array(0=>gm('outgoing'),1=>gm('incoming')),$type_of_call_id),
				'type_of_call_id'		=> $type_of_call_id,
				// 'reminder_date_day_hours_time'			=> build_day_hours(date('H:i',$value['reminder_date'])),
				'reminder_date_day'		=> date(ACCOUNT_DATE_FORMAT,$value['reminder_date']),
				'status_change'			=> $status_change,
				'is_nylas'				=> $value['nylas_email_id'] ? true : false,
				'zen_link'				=> $value['zen_ticket_id'] ? $zen_base.'/tickets/'.$value['zen_ticket_id'] : '',
				'zen_ticket_id'			=> $value['zen_ticket_id'],
				'is_activity'			=>1,
				'user_auto' 			=> $user_auto,
				'reminder_id'			=> $value['reminder_id'],
				'task_type'				=> $value['contact_activity_type'],
				'comment'				=> $value['contact_activity_note'],
				'log_comment'			=> $value['log_comment'],
				'customer_id'			=> $value['customer_id'],
				'contact_ids'			=> $contact_ids,
				'end_d_meet'			=> $meeting_end_date,
				'end_d_meet_day'		=> $meeting_end_date ? date('j F, l, Y',$meeting_end_date) : '',
				'end_d_meet_time'		=> $meeting_end_date ? date('H:i',$meeting_end_date) : '',
				'day_full'				=> date('j F, l, Y',$date),
				'location_meet'			=> $location_meet,
				'user_to_id'			=> $assigned_id

			);
			$result['item'][] = $item_line;

			// $view->loop('item');
			if(!in_array($day, $days)){
				array_push($days, $day);
			}
			$w++;
		}
		// console::log($days);
		ksort($events);
		$in['ev_type']	= '<option value=0>'.gm('Select type').'</option>'.build_simple_dropdown($events,$selected);
		// $in['customer_dd'] = '<option value=0>'.gm('Select type').'</option>'.build_customers_dd($customers_id);

		if($status_values){
			$status=$db->field("SELECT finished FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
		}
		if($in['filter_type'] == 'customer'){
			$customer_data=$db->query("SELECT customers.name,customers.country_name,customers.city_name,addresses_coord.location_lat,addresses_coord.location_lng FROM customers 
						LEFT JOIN addresses_coord ON customers.customer_id=addresses_coord.customer_id
						WHERE customers.customer_id='".$in['customer_id']."' AND customers.active=1 ORDER BY customers.name ");

			$result['location_meet'] = $customer_data->f('city_name').','.$customer_data->f('country_name');
			$result['coord_lat'] = $customer_data->f('location_lat');
			$result['coord_lang'] = $customer_data->f('location_lng');
		}
		$result['log_id'] = $status_values ? $status_values->f('log_id') : '';
		$result['scheduled_selected'] = $status == '0' ? 'selected' : '';
		$result['finished_selected'] = $status == '1' ? 'selected' : '';
		$result['is_data'] = $w > 0 ? true : false;
		$result['user_id'] = $in['user_id'];
		$result['opportunity_id'] = $in['opportunity_id'];
		$result['is_zen']= defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 ? true : false;
		if($in['customer_id']){
			global $database_config;
			$database_users = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
			);
			$db_users = new sqldb($database_users);
			$result['customer_id']=$in['customer_id'];
			$result['customer_name']=get_customer_name($in['customer_id']);
			/*$users_auto = $db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
			$user_auto = array();
			foreach ($users_auto as $key => $value) {
				$user_auto[] = array('id'=>$value['user_id'], 'value'=>htmlspecialchars(strip_tags($value['first_name'] . ' ' . $value['last_name'])) );
			}*/
			$result['user_auto'] = $user_auto;
		}
		$result2 = array('activities' => $result); 
		$result2['customer_dd'] = $db->query("SELECT customer_contact_activity.customer_id AS id, customers.name AS value FROM customer_contact_activity
							INNER JOIN customers ON customer_contact_activity.customer_id=customers.customer_id
							INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
							LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
							WHERE  email_to='".$in['user_id']."' GROUP BY customer_contact_activity.customer_id ORDER BY customers.name ")->getAll();
		$result2['account_date_format'] = pick_date_format();
		$result2['reminder_dd']=build_reminder_dd();
		$result2['user_auto'] = $user_auto;
		$result2['user_id'] = $in['user_id'];
		return $result2;

	}

		function get_ZenTickets($in){
		if(!$in['customer_id'] && !$in['contact_id']){
			return true;
		}
		global $database_config;
		$db = new sqldb();
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		$zen_data=$db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
		if($zen_data->f('active')){
			$zen_email=$db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
			$zen_pass=$db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
			$ch = curl_init();
			/*$comp_email=$db->field("SELECT c_email FROM customers WHERE customer_id='".$in['customer_id']."' ");
			if($comp_email){
				$params.='requester:'.$comp_email.' ';
			}*/
			if($in['customer_id']){
				$filter_m=" customer_contacts.customer_id='".$in['customer_id']."' ";
				$filter_left=" AND customer_contactsIds.customer_id='".$in['customer_id']."' ";
			}else{
				$filter_m=" customer_contacts.contact_id='".$in['contact_id']."' ";
				$filter_left=" AND customer_contactsIds.customer_id!='0'  ";
			}
			$contact_email=$db->query("SELECT customer_contacts.contact_id,customer_contacts.email as main_email,customer_contacts.zen_contact_id,customer_contactsIds.email,customer_contactsIds.customer_id FROM customer_contacts
				LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id ".$filter_left."
				 WHERE ".$filter_m);
			while($contact_email->next()){
				if($contact_email->f('email') || $contact_email->f('zen_contact_id')){
					$params='';
					if($contact_email->f('email')){
						$params.='requester:'.$contact_email->f('email').' ';
					}
					if($contact_email->f('zen_contact_id')){
						$params.='requester:'.$contact_email->f('zen_contact_id').' ';
					}
					$params=rtrim($params,' ');
					$params=urlencode('type:ticket '.$params);
			    	$headers=array('Content-Type: application/json');
			    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
				    curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
			    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			        curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json?query='.$params);

			    	$put = curl_exec($ch);
			    	$info = curl_getinfo($ch);

			    	if($info['http_code']>300 || $info['http_code']==0){
			    		//do nothing
			    	}else{
			    		$data_d=json_decode($put);
			    		if(!empty($data_d->results)){
			    			$users = $db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();
			    			foreach ($data_d->results as $key => $value) {
			    				$exist=$db->field("SELECT customer_contact_activity_id FROM customer_contact_activity WHERE zen_ticket_id='".$value->id."' ");
			    				if($exist){
			    					 					
			    				}else{
			    					$log_id = $db->insert("INSERT INTO logging SET field_name='customer_id',
										pag='customer',
										field_value='".$contact_email->f('customer_id')."',
										date='".time()."',
										type='2',
										user_id='0',
										to_user_id='0',
										done='0',
										due_date='".strtotime($value->created_at)."',
										log_comment='".addslashes($value->subject)."',
										message='".addslashes($value->description)."',
										status_other='0',
										not_task='1',
										zen_due_date='".strtotime($value->due_at)."',
										zen_requester_id='".$value->requester_id."',
										zen_submitter_id='".$value->submitter_id."',
										zen_assignee_id='".$value->assignee_id."',
										zen_organization_id='".$value->organization_id."',
										zen_group_id='".$value->group_id."' ");
									$some_id = $db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='7',
										contact_activity_note='".addslashes($value->subject)."',
										log_comment='".addslashes($value->description)."',
										log_comment_date='".time()."',
										date='".time()."',
										customer_id='".$contact_email->f('customer_id')."',
										email_to='0',
										zen_ticket_id='".$value->id."' ");
				    				foreach ($users as $key => $value) {
											$db->query("INSERT INTO logging_tracked SET
																activity_id='".$some_id."',
																log_id='".$log_id."',
																customer_id='".$contact_email->f('customer_id')."',
																user_id='".$value['user_id']."'");
									}
									$db->query("UPDATE logging_tracked SET seen='0' WHERE user_id='".$_SESSION['u_id']."'AND activity_id='".$some_id."' AND log_id='".$log_id."'");
				    				$is_added=$db->field("SELECT customer_activity_contacts_id FROM customer_contact_activity_contacts WHERE activity_id='".$some_id."' AND contact_id='".$contact_email->f('contact_id')."' ");
				    				if(!$is_added){
				    					$db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$some_id."',contact_id = '".$contact_email->f('contact_id')."', action_type = '0'");
				    				}
			    				}
			    			}
			    		}
			    	}
				}	
			}
		}
	}

	public function get_UserList(&$in){
		$users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role
									FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
									WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY users.last_name ASC");

		$items = array();
		array_push( $items, array( 'id'=>'0','value'=>gm('All') ));
		while($users->next()){
			array_push( $items, array( 'id'=>$users->f('user_id'),'value'=>htmlspecialchars_decode(stripslashes($users->f('last_name').' '.$users->f('first_name'))) ) );
		}
		return $items;
	}

	public function get_cc($in)
	{
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";

		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}
		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
		}

		//$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}

		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 50")->getAll();
		
		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				//"id"					=> $value['cust_id'].'-'.$value['contact_id'],
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip_name'] ? $value['zip_name'] : '',
				'city'					=> $value['city_name'] ? $value['city_name'] : '',
				//"bottom"				=> $value['zip_name'].' '.$value['city'].' '.$value['country_name'],
				//"right"					=> $value['acc_manager_name']
				"bottom"				=> "",
				"right"					=> ""
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts($in) {
		if(!$in['buyer_id']){
			return array();
		}
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['contact_id']){
			$filter .= " AND customer_contacts.contact_id ='".$in['contact_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		//$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
				'bottom' 				=> '',
			);

		}

		$added = false;
		if(count($result)==9){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in)
	{
		if(!$in['buyer_id']){
			return array();
		}
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		'symbol'				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags(($address->f('address')).' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

	public function get_BoardsList(&$in){
		$q = strtolower($in["term"]);
		$filter=" 1=1 ";
		if($q){
			$filter .=" AND tblopportunity_board.name LIKE '%".addslashes($q)."%'";
		}
		$result = array();

		$boards=$this->db->query("SELECT tblopportunity_board.* FROM tblopportunity_board WHERE ".$filter." ORDER BY tblopportunity_board.name ASC");
		while($boards->next()){
			array_push($result,array('id'=>$boards->f('id'),'name'=>stripslashes($boards->f('name'))));
		}
		return $result;
	}

	public function get_StagesList(&$in){
		$q = strtolower($in["term"]);
		$filter=" 1=1 ";
		if($q){
			$filter .=" AND tblopportunity_stage.name LIKE '%".addslashes($q)."%'";
		}
		if($in['board_id']){
			$filter .=" AND board_id='".$in['board_id']."' ";
		}
		$result = array();
		$stages=$this->db->query("SELECT * FROM tblopportunity_stage WHERE ".$filter." ORDER BY sort_order ASC");
		while($stages->next()){
			$item=array(
				'id' 	=>  $stages->f('id'),
				'name'	=> stripslashes($stages->f('name'))
				);
			array_push($result,$item);
		}
		return $result;
	}

	public function get_AllStagesList(&$in){
		$q = strtolower($in["term"]);
		$filter=" 1=1 ";
		if($q){
			$filter .=" AND tblopportunity_stage.name LIKE '%".addslashes($q)."%'";
		}

		$result = array();
		$stages=$this->db->query("SELECT * FROM tblopportunity_stage WHERE ".$filter." ORDER BY sort_order ASC");
		while($stages->next()){
			$item=array(
				'id' 	=>  $stages->f('id'),
				'name'	=> stripslashes($stages->f('name'))
				);
			array_push($result,$item);
		}

		return $result;
	}
}

	$deal = new DealEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $deal->output($deal->$fname($in));
	}

	$deal->getDeal();
	$deal->output();

?>