<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit','786M');
setcookie('Akti-Contact-Export','6-0-0',time()+3600,'/');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

global $database_config;
$db_config = array(
'hostname' => $database_config['mysql']['hostname'],
'username' => $database_config['mysql']['username'],
'password' => $database_config['mysql']['password'],
'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);

$filter = "1=1";
$filename ="contacts_report.xls";
$objPHPExcel = new PHPExcel();
// $objPHPExcel->getProperties()->setCreator("Akti")
//                ->setLastModifiedBy("Akti")
//                ->setTitle("Contacts report")
//                ->setSubject("Contacts report")
//                ->setDescription("Contacts export")
//                ->setKeywords("office PHPExcel php")
//                ->setCategory("Contacts");

if($in['search']){
  // $filter .= " AND (firstname LIKE '%".$in['search']."%' OR lastname LIKE '%".$in['search']."%' OR company_name LIKE '%".$in['search']."%' OR phone LIKE '%".$in['search']."%' OR cell LIKE '%".$in['search']."%' OR email LIKE '%".$in['search']."%')";

  $search_words = explode(' ', $in['search']);
  foreach ($search_words as $word_pos => $searched_word) {
    if(trim($searched_word)){
      
      if($word_pos==0){
        $filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
              OR lastname LIKE '%".$searched_word."%'
              OR company_name LIKE '%".$searched_word."%'
              OR customer_contactsIds.phone LIKE '%".$searched_word."%'
              OR cell LIKE '%".$searched_word."%'
              OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
      }else{
        $filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
              OR lastname LIKE '%".$searched_word."%'
              OR company_name LIKE '%".$searched_word."%'
              OR customer_contactsIds.phone LIKE '%".$searched_word."%'
              OR cell LIKE '%".$searched_word."%'
              OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
      }

    }
  }

  $arguments_s.="&search=".$in['search'];
}

if($in['first'] && $in['first'] == '[0-9]'){
  $filter.=" AND SUBSTRING(LOWER(lastname),1,1) BETWEEN '0' AND '9'";
  $arguments.="&first=".$in['first'];
} elseif ($in['first']){
  $filter .= " AND lastname LIKE '".$in['first']."%'  ";
  $arguments.="&first=".$in['first'];
}

if(!$in['archived']){
  $filter.= " AND customer_contacts.active=1";
}else{
  $filter.= " AND customer_contacts.active=0";
  $arguments.="&archived=".$in['archived'];
}
if($in['front_register']){
  $filter.= " AND customer_contacts.front_register=1";
}else{
  $filter.= " AND customer_contacts.front_register=0";
  $arguments.="&front_register=".$in['front_register'];
}
$arguments =$arguments.$arguments_s;
$language_array = array();
$job_title_array= array();
$job_dep_array= array();
$language='';
$c_types_name='';
$department_names='';
// $language = $db->query("SELECT * FROM lang ORDER BY name");
// while($language->move_next()){
//   $language_array[$language->f('lang_id')] = $language->f('name');
// }

$language = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
while($language->move_next()){
  $language_array[$language->f('lang_id')] = gm($language->f('language'));
}

$custom_language = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
while($custom_language->move_next()){
  $language_array[$custom_language->f('lang_id')] = gm($custom_language->f('language'));
}

$c_types_name = $db->query("SELECT * FROM customer_contact_job_title");
while($c_types_name->move_next()){
  $job_title_array[$c_types_name->f('id')] = $c_types_name->f('name');
}
$department_names = $db->query("SELECT * FROM customer_contact_dep");
while($department_names->move_next()){
  $job_dep_array[$department_names->f('id')] = $department_names->f('name');
}

$info = $db->query("SELECT customer_contactsIds.customer_id as customer_id,
                           customer_contacts.contact_id as contact_id, 
                           customer_contacts.firstname as first_name,
                           customer_contacts.lastname as last_name, 
                           customer_contact_title.name as salutation,
                           case customer_contacts.sex 
                            when 0 then ''
                            when 1 then 'male'
                            else 'female'
                           end as sex,
                           customer_contacts.birthdate as birthdate,
                           country.name as country_name,
                           customer_addresses.city as city_name,
                           customer_addresses.zip as zip_name,
                           customer_addresses.address AS customer_address,
                           customers.name as company_name,
                           customer_contactsIds.department as department,
                           customer_contactsIds.position as position, 
                           customer_contactsIds.e_title as exact_title,  
                           customer_contactsIds.email as email,
                           customer_contactsIds.phone as phone,
                           customer_contacts.cell as cell, 
                           customer_contacts.language as language,
                           customer_contacts.note as note
                    FROM customer_contacts
                    LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
                    LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
                    LEFT JOIN customer_addresses ON customer_addresses.customer_id = customers.customer_id AND customer_addresses.is_primary = '1'
                    LEFT JOIN customer_contact_title ON customer_contact_title.id = customer_contacts.title 
                    LEFT JOIN country ON customer_addresses.country_id = country.country_id
                    WHERE ".$filter." ORDER BY customer_contacts.contact_id")->getAll();

// $i=0;
// $objPHPExcel->getActiveSheet(0)->getStyle('A1:N1')->getFont()->setBold(true);
// $objPHPExcel->setActiveSheetIndex(0)
//           ->setCellValue('A1',gm("Contact id"))
//       ->setCellValue('B1',gm("First Name"))
//       ->setCellValue('C1',gm("Last Name"))
//       ->setCellValue('D1',gm("Gender"))
//       ->setCellValue('E1',gm("Birthdate"))
//       ->setCellValue('F1',gm("Address"))
//       ->setCellValue('G1',gm("Company"))
//       ->setCellValue('H1',gm("Department"))
//       ->setCellValue('I1',gm("Function"))
//       ->setCellValue('J1',gm("Email"))
//       ->setCellValue('K1',gm("Phone"))
//       ->setCellValue('L1',gm("Cell"))
//       ->setCellValue('M1',gm("Language"))
//       ->setCellValue('N1',gm("Note"));

//   //Custom Fields Values
//   $first_letter = 14;
//   $second_letter = 0;
//   $letters = range('A', 'Z');
//   $extra_field = $db->query("SELECT * FROM contact_fields ")->getAll();

//   $reset = false;

//     foreach ($extra_field as $key => $value) {

//       //Change the first set of rows to be AA1 ... AB1 .... BA1 
//       if($reset){
//         $cell_name = (string)$letters[$first_letter].$letters[$second_letter].'1'; 
//         $second_letter++;
//       } else {
//         $cell_name = (string)$letters[$first_letter].'1';
//       }

//       $objPHPExcel->getActiveSheet(0)->getStyle($cell_name)->getFont()->setBold(true);
//       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_name, utf8_decode($value['label']));
    
//       //Build the first set of rows
//       if($first_letter == 25){
//         $first_letter = 0;
//         $reset = true;
//       }  
    
//       if($second_letter == 25){
//         $second_letter = 0;
//         $first_letter++;
//       }

//       if(!$reset){
//         $first_letter++; 
//       }   
//     }
//     //End Custom Fields Values

// $xlsRow = 2;

$extra_field = $db->query("SELECT * FROM contact_fields ORDER BY sort_order ASC")->getAll();

$my_headers = array(gm("Account ID"),
                    gm("Contact id"),
                    gm("First Name"),
                    gm("Last Name"),
                    gm("Salutation"),
                    gm("Gender"),
                    gm("Birthdate"),
                    gm("Country"),
                    gm("City"),
                    gm("ZIP"),
                    gm("Address"),
                    gm("Company"),
                    gm("Department"),
                    gm("Function"),
                    gm("Exact Title"),
                    gm("Email"),
                    gm("Phone"),
                    gm("Cell"),
                    gm("Language"),
                    gm("Note")
                  );

  $id_array = '';
  foreach($extra_field as $fieldLabel){
      $id_array .= $fieldLabel['field_id'].',';
      array_push($my_headers, $fieldLabel['label']);
  }
  $id_array = substr($id_array, 0,-1);

$not_dd = $db->query("SELECT contact_field.customer_id,contact_field.field_id,contact_field.value as name FROM contact_field 
                      INNER JOIN contact_fields ON contact_field.field_id = contact_fields.field_id
                      WHERE contact_fields.is_dd = '0'
                      GROUP BY contact_field.customer_id, field_id
                      ORDER BY contact_fields.sort_order ASC")->getAll();

$is_dd = $db->query("SELECT customer_id,name,contact_fields.field_id
                     FROM contact_fields
                    INNER JOIN contact_field ON  contact_fields.field_id = contact_field.field_id                 
                     INNER JOIN contact_extrafield_dd ON contact_field.value = contact_extrafield_dd.id  
                     WHERE contact_fields.is_dd = '1'
                     GROUP BY customer_id,contact_fields.field_id 
                     ORDER BY contact_fields.sort_order ASC")->getAll();

$custom_fields = array(); 

foreach ($not_dd as $key => $value) {
    foreach ($extra_field as $field) {
       if($field['field_id'] == $value['field_id']){
           $custom_fields[$value['customer_id']][$value['field_id']] = preg_replace("/[\n\r]/","",$value['name']);     
       } else {
           if(!isset($custom_fields[$value['customer_id']][$field['field_id']])){
              $custom_fields[$value['customer_id']][$field['field_id']] = ''; 
           }    
       } 
    }
}

foreach ($is_dd as $key => $value) {
    foreach ($extra_field as $field) {
       if($field['field_id'] == $value['field_id']){
           $custom_fields[$value['customer_id']][$value['field_id']] = preg_replace("/[\n\r]/","",$value['name']);     
       } else {
           if(!isset($custom_fields[$value['customer_id']][$field['field_id']])){
              $custom_fields[$value['customer_id']][$field['field_id']] = ''; 
           }    
       } 
    }
}
$inserted_contacts_arr=array();
$inserted_contacts=0;

foreach($info as $key => $t){
  $inserted_contacts_arr[$inserted_contacts] = $t['contact_id'];
  $inserted_contacts++;

  if(isset($custom_fields[$t['contact_id']])) {
    foreach($custom_fields[$t['contact_id']] as $add => $value){
        $add_str = (string)$add;
        $info[$key][$add_str] = preg_replace("/[\n\r]/","",$value);
    }
  }
    $birthdate = date('d/m/Y',$info[$key]['birthdate']);
    $info[$key]['birthdate'] = $birthdate;
    $info[$key]['department'] = $job_dep_array[$info[$key]['department']];
    $info[$key]['position'] = $job_title_array[$info[$key]['position']];
    $info[$key]['language'] = $language_array[$info[$key]['language']];
}

$user_info = $db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$user_info->next();

$db->query("INSERT INTO company_import_log SET
          filename='contacts_report.csv',
          `date`='".time()."',
          contact_added='".$inserted_contacts."',
          contacts_add='".serialize($inserted_contacts_arr)."',
          username='".addslashes($user_info->f('first_name')." ".$user_info->f('last_name'))."',
          log_type='1' ");

doQueryLog();
header('Content-Type: application/excel');
//header('Content-Disposition: attachment; filename="contacts_report.xls"');
header('Content-Disposition: attachment; filename="contacts_report.csv"');

$fp = fopen('php://output', 'w');

//Add headers
fputcsv($fp, $my_headers);

//Add Information
foreach ( $info as $line ) {
    fputcsv($fp, $line);
}

fclose($fp);
exit();


while ($info->next())
{
  if($info->f('sex')==0)
  {
    $sex = '';
  }
  elseif ($info->f('sex')==1)
  {
    $sex = 'male';
  }
  else
  {
    $sex = 'female';
  }
  if($info->f('language')!=0)
  {
    /*$language = $db->field("SELECT name FROM lang WHERE lang_id=".$info->f('language'));*/
    $language = $lang_array[$info->f('language')];
  }
  $date = '';
  if($info->f('birthdate')){
    $date = date('d/m/Y',$info->f('birthdate'));
  }
  $c_types = '';
  if($info->f('position')){
    /*$pos = explode(',', $info->f('position'));*/
/*    foreach ($pos as $key) {
      
      $c_types .= $c_types_name.', ';
    }*/
    $c_types = $job_title_array[$info->f('position')];
  }
  $address_info = $db->query("SELECT * FROM customer_contact_address WHERE contact_id = '".$info->f('contact_id')."' AND is_primary = '1' ");
  $address = '';
  if($address_info->next()){
    $address = $address_info->f('address').'
'.$address_info->f('zip').'  '.$address_info->f('city').'
'.get_country_name($address_info->f('country_id'));
  }
  $department_name = '';
  if($info->gf('department')){
   /* $department_name = $db->field("SELECT name FROM customer_contact_dep WHERE id = '".$info->gf('department')."'");*/
   $department_name = $job_dep_array[$info->f('department')];
  }
  
  $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $info->f('contact_id'))
        ->setCellValue('B'.$xlsRow, $info->f('firstname'))
        ->setCellValue('C'.$xlsRow, $info->f('lastname'))
        ->setCellValue('D'.$xlsRow, $sex)
        ->setCellValue('E'.$xlsRow, $date)
        ->setCellValue('F'.$xlsRow, $address)
        ->setCellValue('G'.$xlsRow, $info->f('Cname'))
        ->setCellValue('H'.$xlsRow, $department_name)
        ->setCellValue('I'.$xlsRow, $c_types)
        ->setCellValue('J'.$xlsRow, $info->f('email'))
        ->setCellValue('K'.$xlsRow, $info->f('phone'))
        ->setCellValue('L'.$xlsRow, $info->f('cell'))
        ->setCellValue('M'.$xlsRow, $language)
        ->setCellValue('N'.$xlsRow, $info->f('note'));
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);

    // Custom Fields Values 
    $first_letter = 14;
    $second_letter = 0;
    $letters = range('A', 'Z');
    
    $reset = false;

    foreach ($extra_field as $key => $value) {

      $value_extra = $db->field("SELECT contact_field.value FROM contact_field 
                               INNER JOIN contact_fields ON contact_field.field_id = contact_fields.field_id
                               WHERE customer_id = '".$info->f('contact_id')."' AND contact_field.field_id = '".$value['field_id']."' ");

      
      //Change the set of rows values to be A2 ... AB2 .... BA2 
      if($reset){
        $cell_name = (string)$letters[$first_letter].$letters[$second_letter].$xlsRow; 
        $second_letter++;
      } else {
        $cell_name = (string)$letters[$first_letter].$xlsRow;  
      }

      if($value['is_dd']){
        $extra_field_dd_name = $db->field("SELECT name 
                      FROM contact_extrafield_dd 
                      INNER JOIN contact_field ON ( contact_extrafield_dd.extra_field_id = contact_field.field_id
                      AND contact_extrafield_dd.id = contact_field.value ) 
                      WHERE extra_field_id='".$value['field_id']."' 
                      AND customer_id='".$info->f('contact_id')."' ");

        $value_extra = $extra_field_dd_name ? $extra_field_dd_name : '';
      }

      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_name, utf8_decode($value_extra));

      //Build the first set of rows
      if($first_letter == 25){
        $first_letter = 0;
        $reset = true;
      }  
    
      if($second_letter == 25){
        $second_letter = 0;
        $first_letter++;
      }

      if(!$reset){
        $first_letter++; 
      }   
    }
    // End Custom Fields Values

    $xlsRow++;
}

// set column width to auto
foreach(range('A','N') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

@mkdir('upload/'.DATABASE_NAME."/",0755);

$objPHPExcel->getActiveSheet()->setTitle('Contacts report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);
define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Contact-Export','9-0-0',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();
?>