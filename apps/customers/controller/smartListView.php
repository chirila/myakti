<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$output = array();

$list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
$list->next();

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in,$list));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$l_r = ROW_PER_PAGE;

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

$output['list'] = array();
$output['list_name'] = $list->f('name');
$output['list_type'] = $list->f('list_type');
$output['list_for'] = $list->f('list_for');
$in['filter'] = unserialize($list->f('filter'));
/*$output['search']=array(
	'search' => $in['search'] ? $in['search']: '',
	'do' => 'customers-smartListView',
	'offset' => $in['offset'],
	'contact_list_id' => $in['contact_list_id'],
);*/

$type = $list->f('list_type');
$list_for = $list->f('list_for');
/*9if($list_for){
	$view = new at(ark::$viewpath.'crm_customers_list_view.html');
}*/

if($_SESSION['group']=='user')
{
	$perm_admin = $db_users->query("SELECT value, name FROM user_meta WHERE name LIKE 'admin_1' AND user_id='".$_SESSION['u_id']."' ");
	
	if($perm_admin->f('value'))
	{
		$output['admin_1']=true;
	}
	
}else
{
	$output['admin_1']= true;
}
if(!$list_for){
	$order_by = " ORDER BY firstname ";
}else{
	$order_by = " ORDER BY customers.name ";
	// $order_by_allow = array('name');
}
if($in['filter'] ){
	$filter = '';
	foreach ($in['filter'] as $field => $value){
		if($value){
			if(is_array($value)){
				$val = '';
				foreach ($value as $key){
					$val .= $key.',';
				}
				$value = $val;
			}
			$value = rtrim($value,',');
			if($value){
				if(substr(trim($field), 0, 13)=='custom_dd_id_'){
					$filter.= " AND ";
					$v = explode(',', $value);
					foreach ($v as $k => $v2) {
						//$filter .= "customer_field.value = '".addslashes($v2)."' OR ";
						$filter .= "customers.customer_id IN (SELECT customers.customer_id FROM customers
												LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	 											WHERE customer_field.value = '".addslashes($v2)."') OR ";
					}
					$filter = rtrim($filter,"OR ");
				}elseif(substr(trim($field), 0, 15)=='c_custom_dd_id_'){
					
					$filter.= " AND ( ";

					$v = explode(',', $value);

					foreach ($v as $k => $v2) {

						$filter .= " customer_contacts.contact_id IN (SELECT customer_contacts.contact_id FROM customer_contacts
									 LEFT JOIN contact_field ON customer_contacts.contact_id=contact_field.customer_id
									 WHERE contact_field.value = '".addslashes($v2)."') OR ";
					}
					$filter = rtrim($filter,"OR ");
					$filter .= " ) ";
				}elseif($field == 'c_type '){
					$filter .= " AND ( ";
					$v = explode(',', $value);
					foreach ($v as $k => $v2) {
						$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
						// $arguments .= "&filter[c_type][]=".$value;
					}
					$filter = rtrim($filter,"OR ");
					$filter .= " ) ";
				}elseif ($field == 'c_type') {
					$filter .= " AND ( ";
					$v = explode(',', $value);
					foreach ($v as $k => $v2) {
						$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
						// $arguments .= "&filter[c_type][]=".$value;
					}
					$filter = rtrim($filter,"OR ");
					$filter .= " ) ";
				}elseif(trim($field) == 'position'){
					$filter .= " AND ( ";
					$v = explode(',', $value);
					foreach ($v as $k => $v2) {
						$filter .= "position_n like '%".addslashes($v2)."%' OR ";
					}
					$filter = rtrim($filter,"OR ");
					$filter .= " ) ";
				}elseif(trim($field) == 'customer_contacts.email'){
					$filter.=" AND customer_contacts.email='' ";
				}elseif(trim($field) == 'customers.c_email'){
					$filter.=" AND customers.c_email='' ";
				}elseif(trim($field) == 'type'){
						switch ($value) {
							    case 1:
							        $filter.=" AND customers.is_customer='1' ";
							        break;
							    case 2:
							        $filter.=" AND customers.is_supplier='1' ";
							        break;
							    case 3:
							        $filter.=" AND customers.is_customer='1' AND customers.is_supplier='1' ";
							        break;
							    case 4:
							        $filter.=" AND customers.is_customer='' AND customers.is_supplier='' ";
							        break;
							}
						
					}					
				else{
					// $arguments .= "&filter['".$field."']=".$value;
					$filter .= " AND ".$field." IN (".addslashes($value).") ";
				}
			}elseif(trim($field) == 'customers.c_email'){
				$filter.= " AND customers.c_email!='' ";
			}elseif(trim($field) == 'customer_contacts.email'){
				$filter.= " AND customer_contacts.email!='' ";
			}elseif(trim($field) == 'customers.s_emails'){
				$filter.= " AND customers.s_emails='1' ";
			}
		}
	}
}
if(!$filter){
	$filter = '1=1';
}
$filter = ltrim($filter," AND");

if(!$list_for){
	$filter .= " AND customer_contacts.active='1' ";
	if($in['search']){
		$filter .= " AND (customer_contacts.firstname LIKE '%".$in['search']."%' OR customer_contacts.lastname LIKE '%".$in['search']."%' OR customer_contacts.company_name LIKE '%".$in['search']."%') ";
	}
}else{
	$filter .= " AND customers.active='1'  AND is_admin='0' ";
	if($in['search']){
		$filter .= " AND customers.name LIKE '%".$in['search']."%' ";
	}
}

if($in['order_by'] && in_array($in['order_by'],$order_by_allow)){
	$order = " ASC ";
	if($in['desc']){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	console::log('ON_'.strtoupper($in['order_by']));
	$view->assign(array(
		'ON_'.strtoupper($in['order_by']) 	=> $in['desc'] ? 'on_asc' : 'on_desc',
	));
}

if($list->f('list_type') == 1){
	$active_c = $db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND active='1' ");
	while ($active_c->next()) {
		$c_active .= $active_c->f('contact_id').',';
	}
	$c_active = rtrim($c_active,',');
	if($c_active){
		if(!$list_for){
			$filter .= " AND customer_contacts.contact_id IN (".$c_active.") ";
		}else{
			$filter .= " AND customers.customer_id IN (".$c_active.") ";
		}
	}
}

if(!$list_for){
// if($type == 0){
	$k =  $db->field("SELECT COUNT( DISTINCT customer_contacts.contact_id)
            FROM customer_contacts
            LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
            LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
            LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
            LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
            LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
            LEFT JOIN customer_type ON customers.c_type=customer_type.id
            LEFT JOIN customer_sector ON customers.sector=customer_sector.id
            LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
            LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
            LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            WHERE ".$filter." ");
// }
	if($type){
		$k = $db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
	}
	$max_rows=$k;

	$query_list = $db->query("SELECT customer_contacts.firstname, customer_contacts.lastname, customer_contacts.contact_id, customer_contacts.email, customer_contacts.company_name,customer_contactsIds.phone,customer_contacts.cell,customer_contactsIds.email as contact_email,
			customer_contactsIds.customer_id AS cus_id
	            FROM customer_contacts
	            LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
	            LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
            	LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
	            LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
	            LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
	            LEFT JOIN customer_type ON customers.c_type=customer_type.id
	            LEFT JOIN customer_sector ON customers.sector=customer_sector.id
	            LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
	            LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
	            LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
	            LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	            LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	            WHERE ".$filter." GROUP BY customer_contacts.contact_id
				$order_by  limit ".$offset*$l_r.", ".$l_r);
}else{
	$k =  $db->field("SELECT DISTINCT COUNT( DISTINCT customers.customer_id)
			        FROM customers
			        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
			        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
			        LEFT JOIN customer_type ON customers.c_type=customer_type.id
			        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
			        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
			        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
			        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
					LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
					LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            WHERE ".$filter."  ");
// }
	if($type){
		$k = $db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
	}
	$max_rows=$k;

	$query_list = $db->query("SELECT customers.c_email AS email, customers.name AS company_name, customers.customer_id AS contact_id, country_name, city_name,customers.comp_phone AS phone, customers.customer_id AS cus_id
	            FROM customers
	            LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
		        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
		        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
		        LEFT JOIN customer_type ON customers.c_type=customer_type.id
		        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
		        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
		        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
		        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
				LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
				LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	            WHERE ".$filter." GROUP BY customers.customer_id
				$order_by  limit ".$offset*$l_r.", ".$l_r);
}

$i=0;

while ($query_list->next()){
	if($type){
		$active = $db->query("SELECT active FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND contact_id='".$query_list->f('contact_id')."' ");
		if($active->next()){
			$ac = $active->f('active');
		}else{
			$ac = 1;
		}
	}
	$item = array(
	 	'CON_FIRSTNAME'			=> $query_list->f('firstname'),
	 	'CON_COMP'				=> $query_list->f('company_name'),
		'CON_LASTNAME'			=> $query_list->f('lastname'),
		//'EMAIL'					=> $query_list->f('email'),
		'EMAIL'					=> $query_list->f('contact_email') ? $query_list->f('contact_email') : $query_list->f('email'),
		'REL' 					=> $query_list->f('contact_id'),
		'PHONE'					=> $query_list->f('phone'),
		'CELL'					=> $query_list->f('cell'),
		'country'				=> $query_list->f('country_name'),
		'city'					=> $query_list->f('city_name'),
		'customer_id' 			=> $query_list->f('cus_id'),
		// 'CONTACT_EDIT_LINK' => $list_for ? 'index.php?do=company-customer&customer_id='.$query_list->f('contact_id').$arguments : 'index.php?do=company-xcustomer_contact&contact_id='.$query_list->f('contact_id').$arguments,
	);

	$output['list'][] = $item;
	$i++;
}
$output['max_rows'] = $max_rows;
$output['export_args'] = ltrim($arguments,'&');
$output['lr']=$l_r;
$output['list_for'] = $list_for;
$output['hide_refresh']	= $type ? false : true;
$output['pag_export'] = $list_for ? 'export_companies_list' : 'export_contacts_list';
$output['EXPORT'] = '&contact_list_id='.$in['contact_list_id'].'&search='.$in['search'];

if(!$in['withoutFilters']){
	if($output['list_for'] == 1){
		$in['xget']='customersFilters';
	}else{
		$in['xget']='contactsFilters';
	}
	$in['return_data']=true;
	$f = include(__DIR__.'/smartListEdit.php');
	if($f){
		$output['filters']=$f['filters'];
	}
}

json_out($output);




?>