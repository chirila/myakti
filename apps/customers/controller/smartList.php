<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$output = array('list'=>array());

$order_by_array = array('name', 'contacts_nr', 'list_type', 'list_for');

$arguments_s = '';
$arguments='';
$l_r = ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$filter = " contact_list.list_for='".$in['list_for']."' ";

//FILTER LIST
$selected =array();
$filter_u=' ';
$order_by = " ORDER BY name ";
if($in['search']){
	$filter .= " AND name LIKE '%".$in['search']."%'  ";
	$arguments_s.="&search=".$in['search'];
}

if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == '1' || $in['desc']=='true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		$view->assign(array(
			'ON_'.strtoupper($in['order_by']) 	=> $in['desc'] ? 'on_asc' : 'on_desc',
		));
	}
}

$args = $arguments;
$arguments = $arguments.$arguments_s;
$output['max_rows']=$db->field("SELECT count(contact_list_id) FROM contact_list WHERE ".$filter);
$list = $db->query("SELECT contact_list.*, CAST(contact_list.contacts AS UNSIGNED) as contacts_nr FROM contact_list WHERE ".$filter." ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();

$i=0;
foreach ($list as $key => $value) {
// while($db->move_next() && $i<$l_r){
	$edit_pag = 'crm_l';
	$based_type = gm('Contacts only');
	if($value['list_for'] == 1){
		$edit_pag = 'crm_lv';
		$based_type = gm('Companies only');
	}
	$item = array(
		'name'				=> $value['name'],
		'based_type'		=> $based_type,
		'type'				=> $value['list_type'] ? gm('Static') : gm('Dynamic'),
		'contacts'			=> $value['contacts_nr'] ? $value['contacts_nr'] : '&nbsp;',
		'delete_link'		=> array('do'=>'customers-smartList-customers-delete_list', 'contact_list_id'=>$value['contact_list_id']),
		'refresh_link'		=> array('do'=>'customers-smartList-customers-refresh', 'contact_list_id'=>$value['contact_list_id']),
		'contact_list_id'	=> $value['contact_list_id'],
		'hide_refresh'		=> $value['list_type'] ? false : true, 
	);

	$output['list'][] = $item;
	$i++;
}

$output['filters'] = $arguments.$arguments_s;
$output['export_args'] = ltrim($arguments,'&');
$output['lr']=$l_r;

json_out($output);
?>