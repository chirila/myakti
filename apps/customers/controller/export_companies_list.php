<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit','786M');
setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

$filter = "1=1";
/*$filename ="customers_report.xls";
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Customers report")
							 ->setSubject("Customers report")
							 ->setDescription("Customers export")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Customers");*/

$list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
$list->next();

if($list->f('list_type') == 1){
	$active_c = $db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND active='1' ");
	while ($active_c->next()) {
		$c_active .= $active_c->f('contact_id').',';
	}
	$c_active = rtrim($c_active,',');
	if($c_active){
		$filter .= " AND customers.customer_id IN (".$c_active.") ";
	}
}else{
	$in['filter'] = unserialize($list->f('filter'));

	if($in['filter'] ){
		$filter_c = '';
		foreach ($in['filter'] as $field => $value){
			if($value){
				if(is_array($value)){
					$val = '';
					foreach ($value as $key){
						$val .= $key.',';
					}
					$value = $val;
				}
				$value = rtrim($value,',');
				if($value){
					if(substr(trim($field), 0, 13)=='custom_dd_id_'){
          $filter_c.= " AND ";
          $v = explode(',', $value);
          foreach ($v as $k => $v2) {
            $filter_c .= "customer_field.value = '".$v2."' OR ";
          }
          $filter_c = rtrim($filter_c,"OR ");
        }elseif(trim($field) == 'c_type'){
						$filter_c .= " AND ( ";
						$v = explode(',', $value);
						foreach ($v as $k => $v2) {
							$filter_c .= "c_type_name like '%".$v2."%' OR ";
						}
						$filter_c = rtrim($filter_c,"OR ");
						$filter_c .= " ) ";
					}
					elseif(trim($field) == 'position'){
						$filter_c .= " AND ( ";
						$v = explode(',', $value);
						foreach ($v as $k => $v2) {
							$filter_c .= "position_n like '%".$v2."%' OR ";
						}
						$filter_c = rtrim($filter_c,"OR ");
						$filter_c .= " ) ";
					}
					else{
						$filter_c .= " AND ".$field." IN (".$value.") ";
					}
				}
			}
		}
	}
	if(!$filter_c){
		$filter_c = '1=1';
	}
	$filter_c = ltrim($filter_c," AND");

	$filter_c .= " AND customers.active='1' AND is_admin='0' ";

	$k =  $db->query("SELECT DISTINCT customers.customer_id
              FROM customers
              LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
              LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
              LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
              LEFT JOIN customer_type ON customers.c_type=customer_type.id
              LEFT JOIN customer_sector ON customers.sector=customer_sector.id
              LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
              LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
              LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
              LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
              LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            WHERE ".$filter_c."  ".$order_by." ");
	while ($k->next()) {
		$c_active .= $k->f('customer_id').',';
	}
	$c_active = rtrim($c_active,',');
	if($c_active){
		$filter .= " AND customers.customer_id IN (".$c_active.") ";
	}
}


if($in['search']){
	$filter .= " AND customers.name LIKE '%".$in['search']."%' ";
	$arguments_s.="&search=".$in['search'];
}

/*$objPHPExcel->setActiveSheetIndex(0)
	        ->setCellValue('A1',gm('Customer id'))
      		->setCellValue('B1',gm('Name'))
      		->setCellValue('C1',gm('Account Manager'))
        	->setCellValue('D1',gm('Type of Company'))
        	->setCellValue('E1',gm('VAT'))
        	->setCellValue('F1',gm('VAT number'))
          ->setCellValue('G1',gm('Language'))
          ->setCellValue('H1',gm('Country'))
          ->setCellValue('I1',gm('City'))
          ->setCellValue('J1',gm('Zip'))
          ->setCellValue('K1',gm('Address'))
        	->setCellValue('L1',gm('Size'))
          ->setCellValue('M1', gm("Our reference"))
          ->setCellValue('N1', gm("No. contacts"))
          ->setCellValue('O1', gm("Company phone"))
          ->setCellValue('P1', gm("Price Category"))
          ->setCellValue('Q1', gm("Legal Type"))
          ->setCellValue('R1', gm("Website"))
          ->setCellValue('S1', gm("Activity Sector"))
          ->setCellValue('T1', gm("Email"))
          ->setCellValue('U1', gm("Lead Source"))
          ->setCellValue('V1', gm("Comercial name"))
          ->setCellValue('W1', gm("Activity"));*/

  $headers = array(gm('Customer id'),
                    gm('Name'),
                    gm('Account Manager'),
                    gm('Type of Company'),
                    gm('VAT'),
                    gm('VAT number'),
                    gm('Language'),
                    gm('Country'),
                    gm('City'),
                    gm('Zip'),
                    gm('Address'),
                    gm('Size'),
                    gm("Our reference"),
                    gm("No. contacts"),
                    gm("Company phone"),
                    gm("Price Category"),
                    gm("Legal Type"),
                    gm("Website"),
                    gm("Activity Sector"),
                    gm("Email"),
                    gm("Lead Source"),
                    gm("Comercial name"),
                    gm("Activity")
      );

$final_data=array();
$arguments =$arguments.$arguments_s;
$info = $db->query("SELECT customers.*, customer_addresses.address,pim_lang.language as c_lang,vats.value as vat_value,pim_article_price_category.name as price_cat_name,
        customer_legal_type.name as c_legal_type_name,customer_sector.name AS c_sector_name,customer_lead_source.name as c_lead_source_name,customer_activity.name as c_activity_name
					FROM customers
					LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
          LEFT JOIN pim_lang ON pim_lang.lang_id=customers.language
          LEFT JOIN vats ON vats.vat_id=customers.vat_id
          LEFT JOIN pim_article_price_category ON pim_article_price_category.category_id=customers.cat_id
          LEFT JOIN customer_legal_type ON customer_legal_type.id=customers.legal_type
          LEFT JOIN customer_sector ON customer_sector.id=customers.sector
          LEFT JOIN customer_lead_source ON customer_lead_source.id=customers.lead_source
          LEFT JOIN customer_activity ON customer_activity.id=customers.activity
					WHERE ".$filter." GROUP BY customers.customer_id ORDER BY customers.name");
    $i=0;
    $xlsRow = 2;
while ($info->next())
{
	/*if($info->f('language')!=0)
	{
		$language = utf8_decode($db->field("SELECT language FROM pim_lang WHERE lang_id=".$info->f('language')));
	}
	$vat = '';
	if($info->f('vat_id')){
		$vat = $db->field("SELECT value FROM vats WHERE vat_id=".$info->f('vat_id'));
	}
  $cat_name = '';
  if($info->f('cat_id')){
    $cat_name = $db->field("SELECT name FROM pim_article_price_category WHERE category_id='".$info->f('cat_id')."' ");
  }
  $legal_type ='';
  if($info->f('legal_type')){
    $legal_type = $db->field("SELECT name FROM customer_legal_type WHERE id='".$info->f('legal_type')."' ");
  }
  $sector = '';
  if($info->f('sector')){
    $sector = $db->field("SELECT name FROM customer_sector WHERE id='".$info->f('sector')."' ");
  }
  $lead_source = '';
  if($info->f('lead_source')){
    $lead_source = $db->field("SELECT name FROM customer_lead_source WHERE id='".$info->f('lead_source')."' ");
  }
  $activity = '';
  if($info->f('activity')){
    $activity = $db->field("SELECT name FROM customer_activity WHERE id='".$info->f('activity')."' ");
  }
		  $objPHPExcel->setActiveSheetIndex(0)
	        ->setCellValue('A'.$xlsRow,$info->f('customer_id'))
      		->setCellValue('B'.$xlsRow,$info->f('name'))
      		->setCellValue('C'.$xlsRow,$info->f('acc_manager_name'))
        	->setCellValue('D'.$xlsRow,$info->f('c_type_name'))
        	->setCellValue('E'.$xlsRow,$vat)
        	->setCellValue('F'.$xlsRow,$info->f('btw_nr'))
        	->setCellValue('G'.$xlsRow,$language)
        	->setCellValue('H'.$xlsRow,$info->f('country_name'))
          ->setCellValue('I'.$xlsRow,$info->f('city_name'))
        	->setCellValue('J'.$xlsRow,$info->f('zip_name'))
        	->setCellValue('K'.$xlsRow,$info->f('address'))
        	->setCellValue('L'.$xlsRow,$info->f('comp_size'))
          ->setCellValue('M'.$xlsRow, $info->f('our_reference'))
          ->setCellValue('N'.$xlsRow, get_contact_nr($info->f('customer_id')))
          ->setCellValue('O'.$xlsRow, $info->f('comp_phone'))
          ->setCellValue('P'.$xlsRow, $cat_name)
          ->setCellValue('Q'.$xlsRow, $legal_type)
          ->setCellValue('R'.$xlsRow, $Website)
          ->setCellValue('S'.$xlsRow, $Sector)
          ->setCellValue('T'.$xlsRow, $info->f('c_email'))
          ->setCellValue('U'.$xlsRow, $lead_source)
          ->setCellValue('V'.$xlsRow, $info->f('commercial_name'))
          ->setCellValue('W'.$xlsRow, $activity);
	$i++;
	$xlsRow++;*/
  $tmp_item=array(
    $info->f('customer_id'),
    $info->f('name'),
    $info->f('acc_manager_name'),
    $info->f('c_type_name'),
    $info->f('vat_value'),
    $info->f('btw_nr'),
    $info->f('c_lang'),
    $info->f('country_name'),
    $info->f('city_name'),
    $info->f('zip_name'),
    $info->f('address'),
    $info->f('comp_size'),
    $info->f('our_reference'),
    get_contact_nr($info->f('customer_id')),
    $info->f('comp_phone'),
    $info->f('price_cat_name'),
    $info->f('c_legal_type_name'),
    '',
    $info->f('c_sector_name'),
    $info->f('c_email'),
    $info->f('c_lead_source_name'),
    $info->f('commercial_name'),
    $info->f('c_activity_name')
  );
  array_push($final_data,$tmp_item);
}

doQueryLog();
header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="customers_report.csv"');

$fp = fopen('php://output', 'w');

//Add headers
fputcsv($fp, $headers);

//Add Information
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}

fclose($fp);
exit();

$tmp_link=INSTALLPATH.'upload/'.DATABASE_NAME.'/customers_report_'.$_SESSION['u_id'].'_'.time().'.csv';
$fp = fopen($tmp_link, 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
  fputcsv($fp, $line);
}
fclose($fp);
$objReader = PHPExcel_IOFactory::createReader('CSV');

$objPHPExcel = $objReader->load($tmp_link);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
unlink($tmp_link);
doQueryLog();
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="customers_report.xls"');
header('Cache-Control: max-age=0');
$objWriter->save('php://output');   
exit();

$objPHPExcel->getActiveSheet()->setTitle('Customers list');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);
define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
  	 setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();
?>