<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit','786M');
setcookie('Akti-Customer-Export','6-0-0',time()+3600,'/');
$filename ="customers_report.xls";

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
global $database_config;
$db_config = array(
'hostname' => $database_config['mysql']['hostname'],
'username' => $database_config['mysql']['username'],
'password' => $database_config['mysql']['password'],
'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);

//Leave for now
// ark::loadLibraries(array('PHPExcel'));
/** Include PHPExcel */

//Leave for now
// global $database_config;

// $database_users = array(
//   'hostname' => $database_config['mysql']['hostname'],
//   'username' => $database_config['mysql']['username'],
//   'password' => $database_config['mysql']['password'],
//   'database' => $database_config['user_db'],
// );

// $dbu = new sqldb($database_users);

$filter = " customers.is_admin='0' ";
//Leave for now
// $objPHPExcel = new PHPExcel();
// $objPHPExcel->getProperties()->setCreator("Akti")
//                ->setLastModifiedBy("Akti")
//                ->setTitle("Customers report")
//                ->setSubject("Customers report")
//                ->setDescription("Customers export")
//                ->setKeywords("office PHPExcel php")
//                ->setCategory("Customers");
$filter_c = '';
// if($in['search']){
//   /*$filter .= " AND ( name LIKE '%".$in['search']."%'  OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%') ";
//   $arguments_s.="&search=".$in['search'];*/
//   $filter_x = '';
//   // $search_arr = str_split($in['search']);
//   $search_arr = str_split(stripslashes($in['search']));
//   foreach ($search_arr as $value) {
//     $filter_x .= $value.".?";
//   }
//   $filter .= " AND ( name regexp '".addslashes($filter_x)."' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
//   // $filter .= " AND ( name LIKE '%".$in['search']."%' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
//   $arguments_s.="&search=".$in['search'];
// }
$admin_licence=aktiUser::get('user_type');

if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
  $filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
$escape_chars = array('(','?','*','+');

if(!empty($in['search'])){
  $filter_x = '';
  // $search_arr = str_split($in['search']);
  $search_arr = str_split(stripslashes($in['search']));
  foreach ($search_arr as $value) {
    if(in_array($value, $escape_chars)){
      $value = '\\'.$value;
    }
    $filter_x .= $value."[^a-zA-Z0-9]?";
  }
 /* $filter .= " AND ( customers.name regexp '".addslashes($filter_x)."' OR customers.country_name LIKE '%".$in['search']."%' OR customers.c_type_name LIKE '%".$in['search']."%' OR customers.city_name LIKE '%".$in['search']."%' ) ";*/
 $filter .= " AND ( customers.name regexp '".addslashes($filter_x)."' OR customers.name LIKE '%".$in['search']."%' ) ";
  // $filter .= " AND ( name LIKE '%".$in['search']."%' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
  $arguments_s.="&search=".$in['search'];
}

if(!$in['archived']){
  $filter.= " AND customers.active=1";
}else{
  $filter.= " AND customers.active=0";
  $arguments.="&archived=".$in['archived'];
  $arguments_a = "&archived=".$in['archived'];
}

if(!empty($in['acc_manager_name'])){
  $filter .= " AND ( customers.name LIKE '%".$in['acc_manager_name']."%'  OR customers.country_name LIKE '%".$in['acc_manager_name']."%' OR customers.c_type_name LIKE '%".$in['acc_manager_name']."%' OR customers.city_name LIKE '%".$in['acc_manager_name']."%' OR customers.acc_manager_name LIKE '%".$in['acc_manager_name']."%' ) ";
  $arguments_s.="&acc_manager_name=".$in['acc_manager_name'];
}elseif(!empty($in['acc'])){
  $filter .= " AND customers.user_id='0' ";
  $arguments_s.="&acc=".$in['acc'];
}
if(!empty($in['zip_from']) && empty($in['zip_to'])){
  $zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
  $filter .= " AND ( customers.zip_name LIKE '%".$zip_from."%' ) ";
  $arguments_s.="&zip_from=".$in['zip_from'];
}
if(!empty($in['zip_to'])){
  $zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
  $zip_to = preg_replace('/[^0-9]/', '', $in['zip_to']);
  if(empty($zip_from)){
    $zip_from = '0';
  }
  $filter .= " AND ( STRIP_NON_DIGIT(customers.zip_name) BETWEEN '".$zip_from."' AND '".$zip_to."' ) ";
  $arguments_s.="&zip_to=".$zip_to."&zip_from=".$zip_from;
}
if(!empty($in['country_id'])){
  $filter .= " AND customers.country_name = '".$in['country_id']."' ";
  $arguments_s.="&country_id=".$in['country_id'];
}
if(!empty($in['our_reference'])){
  $filter .= " AND ( customers.our_reference LIKE '%".$in['our_reference']."%' ) ";
  $arguments_s.="&our_reference=".$in['our_reference'];
}
if(!empty($in['btw_nr'])){
  $filter .= " AND ( customers.btw_nr LIKE '%".$in['btw_nr']."%' ) ";
  $arguments_s.="&btw_nr=".$in['btw_nr'];
}
if(!empty($in['commercial_name'])){
  $filter_x = '';
  $search_arr = str_split(stripslashes($in['search']));
  foreach ($search_arr as $value) {
    $filter_x .= $value.".?";
  }
  $filter .= " AND ( customers.commercial_name regexp '".addslashes($filter_x)."' ) ";
  $arguments_s.="&commercial_name=".$in['commercial_name'];
}
if (isset($in['type'])){
  switch ($in['type']) {
    case '1':
      $filter .= " AND customers.type='".$in['type']."'  ";
          $arguments.="&type=".$in['type'];
      break;
    case '2':
        $filter .= " AND customers.type='0'  ";
          $arguments.="&type=".$in['type'];
        break;
  }
}
if (isset($in['is_supplier'])){
  /*$filter .= " AND customers.is_supplier='".$in['is_supplier']."'  ";
  $arguments.="&is_supplier=".$in['is_supplier'];*/
  switch ($in['is_supplier']) {
    case '1':
      $filter .= " AND customers.is_supplier='".$in['is_supplier']."'  ";
            $arguments.="&is_supplier=".$in['is_supplier'];
      break;
    case '2':
      $filter .= " AND customers.is_customer='1'  ";
            $arguments.="&is_supplier=".$in['is_supplier'];
      break;
    case '3':
      $filter .= " AND customers.is_supplier='1' AND customers.is_customer='1' ";
            $arguments.="&is_supplier=".$in['is_supplier'];
      break;
    case '4':
      $filter .= " AND customers.is_supplier='' AND customers.is_customer='' ";
            $arguments.="&is_supplier=".$in['is_supplier'];
      break;
    default:
      # code...
      break;
  }
}
if(!empty($in['lead_source'])){
  $filter .= " AND customers.lead_source='".$in['lead_source']."' ";
  $arguments_s.="&lead_source=".$in['lead_source'];
}
if($in['first'] && $in['first'] == '[0-9]'){
  $filter.=" AND SUBSTRING(LOWER(customers.name),1,1) BETWEEN '0' AND '9'";
  $arguments.="&first=".$in['first'];
} elseif ($in['first']){
  $filter .= " AND customers.name LIKE '".$in['first']."%'  ";
  $arguments.="&first=".$in['first'];
}
if(!isset($in['front_register'])){
  $in['front_register'] = 0;
}
  $filter .= " AND customers.front_register= '".$in['front_register']."'  ";
  $arguments.="&front_register=".$in['front_register'];

setcookie('Akti-Customer-Export','6-0-88',time()+3600,'/');
$arguments = $arguments.$arguments_s;
$info = $db->query("SELECT * FROM customers WHERE $filter ");

$i = 0;
//Leave for now
// $objPHPExcel->setActiveSheetIndex(0)
//             ->setCellValue('A1', gm("Company id"))
//       ->setCellValue('B1', gm("Name"))
//       ->setCellValue('C1', gm("Account Manager"))
//       ->setCellValue('D1', gm("Type of relationship"))
//       ->setCellValue('E1', gm("VAT"))
//       ->setCellValue('F1', gm("VAT number"))
//       ->setCellValue('G1', gm("Reference"))
//       ->setCellValue('H1', gm("Country"))
//       ->setCellValue('I1', gm("City"))
//       ->setCellValue('J1', gm("ZIP"))
//       ->setCellValue('K1', gm("Address"))
//       ->setCellValue('L1', gm("No. contacts"))
//       ->setCellValue('M1', gm("Phone"))
//       ->setCellValue('N1', gm("Price Category"))
//       ->setCellValue('O1', gm("Language"))
//       ->setCellValue('P1', gm("Legal Type"))
//       ->setCellValue('Q1', gm("Website"))
//       ->setCellValue('R1', gm("Activity Sector"))
//       ->setCellValue('S1', gm("Company email"))
//       ->setCellValue('T1', gm("Lead Source"))
//       ->setCellValue('U1', gm("Commercial Name"))
//       ->setCellValue('V1', gm("Activity"))
//       ->setCellValue('W1', gm("Payment Term"))
//       ->setCellValue('X1', gm("Discount"))
//       ->setCellValue('Y1', gm("Currency"))
//       ->setCellValue('Z1', gm("External id"))
//       ->setCellValue('AA1', gm("Delivery and dispatch notes"))
//       ->setCellValue('AB1', gm("Bank Name"))
//       ->setCellValue('AC1', gm("BIC Code"))
//       ->setCellValue('AD1', gm("IBAN"))
//       ->setCellValue('AE1', gm("Invoice email"))
//       ->setCellValue('AF1', gm("Document Language"))
//       ->setCellValue('AG1', gm("Company size"));
      
// $first_letter = 0;
// $second_letter = 7;
// $letters = range('A', 'Z');
// $extra_field = $db->query("SELECT * FROM customer_fields ")->getAll();
      // foreach ($extra_field as $key => $value) {
      //   $cell_name = (string)$letters[$first_letter].$letters[$second_letter].'1';
      //   $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_name, utf8_decode($value['label']));
      //   if($second_letter ==25){
      //     $second_letter = 0;
      //     $first_letter ++;
      //   }else{
      //     $second_letter++;
      //   }

      // }
 // $xlsRow = 2;

$extra_field = $db->query("SELECT * FROM customer_fields ORDER BY sort_order ASC")->getAll();

  $my_headers = array(gm("Company id"),
                    gm("Name"),
                    gm("Account Manager"),
                    gm("Type of relationship"),
                    gm("VAT"),
                    gm("VAT number"),
                    gm("Reference"),
                    gm("Country"),
                    gm("City"),
                    gm("ZIP"),
                    gm("Address"),
                    gm("No. contacts"),
                    gm("Phone"),
                    gm("Price Category"),
                    gm("Language"),
                    gm("Legal Type"),
                    gm("Website"),
                    gm("Activity Sector"),
                    gm("Company email"),
                    gm("Lead Source"),
                    gm("Commercial Name"),
                    gm("Activity"),
                    gm("Payment Term"),
                    gm("Discount"),
                    gm("Currency"),  
                    gm("External id"),
                    gm("Delivery and dispatch notes"),
                    gm("Bank Name"),
                    gm("BIC Code"),
                    gm("IBAN"),
                    gm("Invoice email"),
                    gm("Company size"),
                    gm("Credit limit"),
                    gm("Customer"),
                    gm("Supplier"),
                    gm("VAT Regime"),
                    gm("Created")
  );

  $id_array = '';
  foreach($extra_field as $fieldLabel){
      $id_array .= $fieldLabel['field_id'].',';
      array_push($my_headers, $fieldLabel['label']);
  }

  $id_array = substr($id_array, 0,-1);

/*$info = $db->query("SELECT customers.customer_id as customer_id,
                    customers.name as name,
                    customers.acc_manager_name as acc_manager_name,
                    customers.c_type_name as c_type_name,
                    vats.value as vat_value,
                    customers.btw_nr as btw_nr,
                    customers.our_reference as our_reference,
                    customers.country_name as country_name,
                    customers.city_name as city_name,
                    customers.zip_name as zip_name,
                    customer_addresses.address AS customer_address,
                    ( SELECT COUNT(contact_id) FROM customer_contacts WHERE customer_contacts.customer_id=customers.customer_id and active='1' )
                     as contacts_number,
                    customers.comp_phone as comp_phone,
                    pim_article_price_category.name as category_name,
                    customer_contact_language.name as customer_language_name,
                    customer_legal_type.name as legal_type_name,
                    customers.website as website,
                    customer_sector.name as sector_name,
                    customers.c_email as c_email,
                    customer_lead_source.name as customer_lead_source_name,
                    customers.commercial_name as commercial_name,
                    customer_activity.name as customer_activity_name,
                    customers.payment_term as payment_term,
                    customers.fixed_discount as fixed_discount,
                    currency.code as currency_code,
                    customers.external_id as external_id,
                    customers.deliv_disp_note as deliv_disp_note,
                    customers.bank_name as bank_name,
                    customers.bank_bic_code as bank_bic_code,
                    customers.bank_iban as bank_iban,
                    customers.invoice_email as invoice_email,
                    pim_lang.language as customer_internal_language,
                    customers.comp_size as comp_size
                    FROM customers 
                    LEFT JOIN customer_addresses ON customer_addresses.customer_id = customers.customer_id AND customer_addresses.is_primary = '1'
                    LEFT JOIN vats ON vats.vat_id = customers.vat_id
                    LEFT JOIN pim_article_price_category ON pim_article_price_category.category_id = customers.cat_id
                    LEFT JOIN customer_contact_language ON customer_contact_language.id = customers.language
                    LEFT JOIN customer_legal_type ON customer_legal_type.id = customers.legal_type
                    LEFT JOIN customer_sector ON customer_sector.id = customers.sector  
                    LEFT JOIN customer_lead_source ON customer_lead_source.id = customers.lead_source
                    LEFT JOIN customer_activity ON customer_activity.id = customers.activity
                    LEFT JOIN currency ON currency.id = customers.currency_id
                    LEFT JOIN pim_lang ON pim_lang.lang_id = customers.internal_language
                    WHERE $filter GROUP BY customers.customer_id")
                      ->getAll();*/
                     /* echo "SELECT customers.customer_id as customer_id,
                    customers.name as name,
                    customers.acc_manager_name as acc_manager_name,
                    customers.c_type_name as c_type_name,
                    vats.value as vat_value,
                    customers.btw_nr as btw_nr,
                    customers.our_reference as our_reference,
                    customers.country_name as country_name,
                    customer_addresses.city as city_name,
                    customer_addresses.zip as zip_name,
                    customer_addresses.address AS customer_address,
                    ( SELECT COUNT(contact_id) FROM customer_contacts WHERE customer_contacts.customer_id=customers.customer_id and active='1' )
                     as contacts_number,
                    customers.comp_phone as comp_phone,
                    pim_article_price_category.name as category_name,
                    customer_contact_language.name as customer_language_name,
                    customer_legal_type.name as legal_type_name,
                    customers.website as website,
                    customer_sector.name as sector_name,
                    customers.c_email as c_email,
                    customer_lead_source.name as customer_lead_source_name,
                    customers.commercial_name as commercial_name,
                    customer_activity.name as customer_activity_name,
                    customers.payment_term as payment_term,
                    customers.fixed_discount as fixed_discount,
                    currency.code as currency_code,
                    customers.external_id as external_id,
                    customers.deliv_disp_note as deliv_disp_note,
                    customers.bank_name as bank_name,
                    customers.bank_bic_code as bank_bic_code,
                    customers.bank_iban as bank_iban,
                    customers.invoice_email as invoice_email,
                    pim_lang.language as customer_internal_language,
                    customers.comp_size as comp_size,
                    customers.credit_limit as credit_limit,
                    customers.is_customer as is_customer,
                    customers.is_supplier as is_supplier,  
                    vat_new.description AS vat_regime
                            
                    FROM customers 
                    LEFT JOIN customer_addresses ON customer_addresses.customer_id = customers.customer_id AND customer_addresses.address!='' 
                    LEFT JOIN vats ON vats.vat_id = customers.vat_id
                    LEFT JOIN vat_new ON customers.vat_regime_id=vat_new.id
                    LEFT JOIN pim_article_price_category ON pim_article_price_category.category_id = customers.cat_id
                    LEFT JOIN customer_contact_language ON customer_contact_language.id = customers.language
                    LEFT JOIN customer_legal_type ON customer_legal_type.id = customers.legal_type
                    LEFT JOIN customer_sector ON customer_sector.id = customers.sector  
                    LEFT JOIN customer_lead_source ON customer_lead_source.id = customers.lead_source
                    LEFT JOIN customer_activity ON customer_activity.id = customers.activity
                    LEFT JOIN currency ON currency.id = customers.currency_id
                    LEFT JOIN pim_lang ON pim_lang.lang_id = customers.internal_language
                    
                    WHERE $filter GROUP BY customers.customer_id"; exit();*/
$info = $db->query("SELECT customers.customer_id as customer_id,
                    customers.name as name,
                    customers.acc_manager_name as acc_manager_name,
                    customers.c_type_name as c_type_name,
                    vats.value as vat_value,
                    customers.btw_nr as btw_nr,
                    customers.our_reference as our_reference,
                    customers.country_name as country_name,
                    customer_addresses.city as city_name,
                    customer_addresses.zip as zip_name,
                    customer_addresses.address AS customer_address,
                    ( SELECT COUNT(contact_id) FROM customer_contacts WHERE customer_contacts.customer_id=customers.customer_id and active='1' )
                     as contacts_number,
                    customers.comp_phone as comp_phone,
                    pim_article_price_category.name as category_name,
                    pim_lang.language as customer_language_name,
                    customer_legal_type.name as legal_type_name,
                    customers.website as website,
                    customer_sector.name as sector_name,
                    customers.c_email as c_email,
                    customer_lead_source.name as customer_lead_source_name,
                    customers.commercial_name as commercial_name,
                    customer_activity.name as customer_activity_name,
                    customers.payment_term as payment_term,
                    customers.fixed_discount as fixed_discount,
                    currency.code as currency_code,
                    customers.external_id as external_id,
                    customers.deliv_disp_note as deliv_disp_note,
                    customers.bank_name as bank_name,
                    customers.bank_bic_code as bank_bic_code,
                    customers.bank_iban as bank_iban,
                    customers.invoice_email as invoice_email,
                    customers.comp_size as comp_size,
                    customers.credit_limit as credit_limit,
                    customers.is_customer as is_customer,
                    customers.is_supplier as is_supplier,  
                    vat_new.description AS vat_regime,
                    customers.creation_date as created_at        
                    FROM customers 
                    LEFT JOIN customer_addresses ON customer_addresses.customer_id = customers.customer_id AND customer_addresses.address!='' 
                    LEFT JOIN vats ON vats.vat_id = customers.vat_id
                    LEFT JOIN vat_new ON customers.vat_regime_id=vat_new.id
                    LEFT JOIN pim_article_price_category ON pim_article_price_category.category_id = customers.cat_id
                    LEFT JOIN customer_contact_language ON customer_contact_language.id = customers.language
                    LEFT JOIN customer_legal_type ON customer_legal_type.id = customers.legal_type
                    LEFT JOIN customer_sector ON customer_sector.id = customers.sector  
                    LEFT JOIN customer_lead_source ON customer_lead_source.id = customers.lead_source
                    LEFT JOIN customer_activity ON customer_activity.id = customers.activity
                    LEFT JOIN currency ON currency.id = customers.currency_id
                    LEFT JOIN pim_lang ON pim_lang.lang_id = customers.language
                    
                    WHERE $filter GROUP BY customers.customer_id")
                      ->getAll();

$extra = 0;

$not_dd = $db->query("SELECT customer_field.customer_id,customer_field.value as name,customer_field.field_id FROM customer_field 
                      INNER JOIN customer_fields ON customer_field.field_id = customer_fields.field_id
                      WHERE customer_fields.is_dd = '0'
                      GROUP BY customer_field.customer_id, field_id
                      ORDER BY customer_fields.sort_order ASC")->getAll();

$is_dd = $db->query("SELECT customer_id,name,customer_fields.field_id
                     FROM customer_fields
                    INNER JOIN customer_field ON  customer_fields.field_id = customer_field.field_id                 
                     INNER JOIN customer_extrafield_dd ON customer_field.value = customer_extrafield_dd.id  
                     WHERE customer_fields.is_dd = '1'
                     GROUP BY customer_id,customer_fields.field_id 
                     ORDER BY customer_fields.sort_order ASC")->getAll();
 
$custom_fields = array(); 

foreach ($not_dd as $key => $value) {
    foreach ($extra_field as $field) {
       if($field['field_id'] == $value['field_id']){
           $custom_fields[$value['customer_id']][$value['field_id']] = preg_replace("/[\n\r]/","",$value['name']);     
       } else {
           if(!isset($custom_fields[$value['customer_id']][$field['field_id']])){
              $custom_fields[$value['customer_id']][$field['field_id']] = ''; 
           }    
       } 
    }
}

foreach ($is_dd as $key => $value) {
    foreach ($extra_field as $field) {
       if($field['field_id'] == $value['field_id']){
           $custom_fields[$value['customer_id']][$value['field_id']] = preg_replace("/[\n\r]/","",$value['name']);     
       } else {
           if(!isset($custom_fields[$value['customer_id']][$field['field_id']])){
              $custom_fields[$value['customer_id']][$field['field_id']] = ''; 
           }    
       } 
    }
}

$inserted_cust_arr=array();
$inserted_cust = 0;

foreach($info as $key => $t){
  $inserted_cust_arr[$inserted_cust] = $t['customer_id'];
  $inserted_cust++;

  if(isset($custom_fields[$t['customer_id']])) {
    foreach($custom_fields[$t['customer_id']] as $add => $value){
        $add_str = (string)$add;
        $info[$key][$add_str] = preg_replace("/[\n\r]/","",$value);
    }
  }
  
 // $info[$key]['customer_internal_language'] = gm($info[$key]['customer_internal_language']);
  $info[$key]['country_name'] = get_country_name($info[$key]['country_id']);
  $info[$key]['country_id'] = '';

  foreach($t as $key1=>$value1){
    if($key1 == 'created_at'){

      if($value1){
        $info[$key][$key1]= date(ACCOUNT_DATE_FORMAT,$value1);
      }else{
          $created_id =  $db->field("SELECT MIN(log_id) FROM logging WHERE  logging.pag='customer' AND logging.field_name='customer_id' AND  logging.field_value='".$value['customer_id']."'" );
        if($created_id){
            $created_at =  $db->field("SELECT date FROM logging WHERE  logging.log_id='".$created_id."'" );
            $info[$key][$key1]= $created_at? date(ACCOUNT_DATE_FORMAT,$created_at):'';
        }else{
            $info[$key][$key1]='';
        }
     }
      
    }else{
      $info[$key][$key1]=stripslashes(preg_replace("/[\n\r]/","",$value1));
    }
  }
/*echo '<pre>';
var_dump($info);
echo '</pre>';exit();*/
  // foreach ($extra_field as $value) {
      // $value_extra = $db->field("SELECT customer_field.value FROM customer_field 
      //                          INNER JOIN customer_fields ON customer_field.field_id = customer_fields.field_id
      //                          WHERE customer_id = '".$t['customer_id']."' AND customer_field.field_id = '".$value['field_id']."' ");
      // if($value['is_dd']){
      //     $extra_field_dd_name = $db->field("SELECT name
      //               FROM customer_extrafield_dd
      //               INNER JOIN customer_field ON ( customer_extrafield_dd.extra_field_id = customer_field.field_id
      //               AND customer_extrafield_dd.id = customer_field.value )
      //               WHERE extra_field_id =  '".$value['field_id']."'
      //               AND customer_id='".$t['customer_id']."' ");

      //   $value_extra = $extra_field_dd_name ? $extra_field_dd_name : '';
      // }
        
  // }

}

$user_info = $db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$user_info->next();

$db->query("INSERT INTO company_import_log SET
          filename='customers_report.csv',
          `date`='".time()."',
          company_added='".$inserted_cust."',
          companies_add='".serialize($inserted_cust_arr)."',
          username='".addslashes($user_info->f('first_name')." ".$user_info->f('last_name'))."',
          log_type='1' ");

doQueryLog();
header('Content-Type: application/excel');
//header('Content-Disposition: attachment; filename="customers_report.xls"');
header('Content-Disposition: attachment; filename="customers_report.csv"');

$fp = fopen('php://output', 'w');

//Add headers
fputcsv($fp, $my_headers);

//Add Information
foreach ( $info as $line ) {
    fputcsv($fp, $line);
}

fclose($fp);
exit();


while ($info->next())
{
  
  $add = $db->field("SELECT address FROM customer_addresses WHERE customer_id='".$info->f('customer_id')."' ");
  $vat = '';
  if($info->f('vat_id')){
    $vat = $db->field("SELECT value FROM vats WHERE vat_id=".$info->f('vat_id'));
  }
  $cat_name = '';
  if($info->f('cat_id')){
    $cat_name = $db->field("SELECT name FROM pim_article_price_category WHERE category_id='".$info->f('cat_id')."' ");
  }
  $language = '';
  if($info->f('language')){
    $language = $db->field("SELECT name FROM customer_contact_language WHERE id='".$info->f('language')."' ");
  }
  $legal_type ='';
  if($info->f('legal_type')){
    $legal_type = $db->field("SELECT name FROM customer_legal_type WHERE id='".$info->f('legal_type')."' ");
  }
  $sector = '';
  if($info->f('sector')){
    $sector = $db->field("SELECT name FROM customer_sector WHERE id='".$info->f('sector')."' ");
  }
  $lead_source = '';
  if($info->f('lead_source')){
    $lead_source = $db->field("SELECT name FROM customer_lead_source WHERE id='".$info->f('lead_source')."' ");
  }
  $activity = '';
  if($info->f('activity')){
    $activity = $db->field("SELECT name FROM customer_activity WHERE id='".$info->f('activity')."' ");
  }
  $currency_code = '';
  if($info->f('currency_id')){
    $currency_code = $db->field("SELECT code FROM currency WHERE id='".$info->f('currency_id')."' ");
  }
  $internal_language = '';
  if($info->f('internal_language')){
    $internal_language = $db->field("SELECT language FROM pim_lang WHERE lang_id='".$info->f('internal_language')."' GROUP BY lang_id ");
    if($internal_language){
      $internal_language = gm($internal_language);
    }
  }

  $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $info->f('customer_id'))
        ->setCellValue('B'.$xlsRow, $info->f('name'))
        ->setCellValue('C'.$xlsRow, $info->f('acc_manager_name'))
        ->setCellValue('D'.$xlsRow, $info->f('c_type_name'))
        ->setCellValue('E'.$xlsRow, $vat)
        ->setCellValue('F'.$xlsRow, $info->f('btw_nr'))
        ->setCellValue('G'.$xlsRow, $info->f('our_reference'))
        ->setCellValue('H'.$xlsRow, get_country_name($info->f('country_id')))
        ->setCellValue('I'.$xlsRow, $info->f('city_name'))
        ->setCellValue('J'.$xlsRow, $info->f('zip_name'))
        ->setCellValue('K'.$xlsRow, $add)
        ->setCellValue('L'.$xlsRow, get_contact_nr($info->f('customer_id')))
        ->setCellValue('M'.$xlsRow, $info->f('comp_phone'))
        ->setCellValue('N'.$xlsRow, $cat_name)
        ->setCellValue('O'.$xlsRow, $language)
        ->setCellValue('P'.$xlsRow, $legal_type)
        ->setCellValue('Q'.$xlsRow, $info->f('website'))
        ->setCellValue('R'.$xlsRow, $sector)
        ->setCellValue('S'.$xlsRow, $info->f('c_email'))
        ->setCellValue('T'.$xlsRow, $lead_source)
        ->setCellValue('U'.$xlsRow, $info->f('commercial_name'))
        ->setCellValue('V'.$xlsRow, $activity)

        ->setCellValue('W'.$xlsRow, $info->f('payment_term'))
        ->setCellValue('X'.$xlsRow, $info->f('fixed_discount'))
        ->setCellValue('Y'.$xlsRow, $currency_code)
        ->setCellValue('Z'.$xlsRow, $info->f('external_id'))
        ->setCellValue('AA'.$xlsRow, $info->f('deliv_disp_note'))
        ->setCellValue('AB'.$xlsRow, $info->f('bank_name'))
        ->setCellValue('AC'.$xlsRow, $info->f('bank_bic_code'))
        ->setCellValue('AD'.$xlsRow, $info->f('bank_iban'))
        ->setCellValue('AE'.$xlsRow, $info->f('invoice_email'))
        ->setCellValue('AF'.$xlsRow, $internal_language)
        ->setCellValue('AG'.$xlsRow, $info->f('comp_size'))
        ->setCellValue('AH'.$xlsRow, $info->f('credit_limit'));
    $first_letter = 0;
    $second_letter = 7;
    $letters = range('A', 'Z');
    foreach ($extra_field as $key => $value) {
      $value_extra = $db->field("SELECT customer_field.value FROM customer_field 
                               INNER JOIN customer_fields ON customer_field.field_id = customer_fields.field_id
                               WHERE customer_id = '".$info->f('customer_id')."' AND customer_field.field_id = '".$value['field_id']."' ");
      $cell_name = (string)$letters[$first_letter].$letters[$second_letter].$xlsRow;

      if($value['is_dd']){
          $extra_field_dd_name = $db->field("SELECT name
                    FROM customer_extrafield_dd
                    INNER JOIN customer_field ON ( customer_extrafield_dd.extra_field_id = customer_field.field_id
                    AND customer_extrafield_dd.id = customer_field.value )
                    WHERE extra_field_id =  '".$value['field_id']."'
                    AND customer_id='".$info->f('customer_id')."' ");

        $value_extra = $extra_field_dd_name ? $extra_field_dd_name : '';
      } 

      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_name, utf8_decode($value_extra));
      if($second_letter ==25){
        $second_letter = 0;
        $first_letter ++;
      }else{
        $second_letter++;
      }
    }

    $xlsRow++;
    $i++;
}

@mkdir('upload/'.DATABASE_NAME."/",0755);

$objPHPExcel->getActiveSheet()->setTitle('Customers report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Customer-Export','9-0-0',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();
?>