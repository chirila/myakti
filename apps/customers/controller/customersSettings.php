<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

json_out($result);

function get_customerFields($in){
	
	$result = array('customerFields'=>array('extraList'=>array(),'extraListDD'=>array()), 'extraFields'=>array('customer_fields'=>array(),'contact_fields'=>array()));
	$db = new sqldb();
	$label = $db->query("SELECT * FROM customer_fields WHERE is_dd='0' ORDER BY field_id ")->getAll();
	$all = $label;

	foreach ($all as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM customer_field WHERE field_id='".$value['field_id']."' ");

		$result['customerFields']['extraList'][] = array(
			'field_id'			=> $value['field_id'],
			'label'				=> $value['label'],
			'value'				=> $value['value'] == 1 ? true : false,
			'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			'value_creation'			=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			// 'value_def'			=> $value['default_value'] == 1 ? 'checked' :($value['value'] == 0 ? 'disabled' : ''),

		);		
	}
	$labeldd = $db->query("SELECT * FROM customer_fields WHERE is_dd='1' ORDER BY field_id ")->getAll();
	$alldd = $labeldd;

	foreach ($alldd as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM customer_field WHERE field_id='".$value['field_id']."' ");

		$result['customerFields']['extraListDD'][] = array(
			'field_id'			=> $value['field_id'],
			'label'				=> $value['label'],
			'value'				=> $value['value'] == 1 ? true : false,
			'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			'value_creation'			=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			// 'value_def'			=> $value['default_value'] == 1 ? 'checked' :($value['value'] == 0 ? 'disabled' : ''),

		);		
	}

	$db->query("SELECT * FROM customise_field WHERE controller='company-customer' ORDER BY sort_order ASC");

	$i = 1;
	while ($db->next()) {
		$result['customerFields'][$db->f('field')]	 		= $db->f('value') == 1 ? true : false;
		$result['customerFields'][$db->f('field').'_d']		= $db->f('default_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);
		$result['customerFields'][$db->f('field').'_c']		= $db->f('creation_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);

			$i_d = $i+1;
			$i_c = $i+2;	

			$fieldInfo = customerStaticFieldsInfo($db->f('field'));
			if(!empty($fieldInfo['label'])){
				$result['customerFields']['linesObj'][] = array(
					'label'					=> $fieldInfo['label'],
					'is_custom'				=> false,
					'model'					=> $db->f('field'),
					'model_d'				=> $db->f('field').'_d',
					'model_c'				=> $db->f('field').'_c',
					$db->f('field')			=> $db->f('value') == 1 ? true : false,
					$db->f('field').'_d'	=> $db->f('default_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false),
					$db->f('field').'_c'	=> $db->f('creation_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false),
					'checkbox_id'			=> $i,
					'checkbox_id_d'			=> $i_d,
					'checkbox_id_c'			=> $i_c,
					'save_field'			=> 'c_field',
					'save_value'			=> 'customise',
					'save_value_d'			=> 'default_customise',
					'save_value_c'	 		=> 'creation_customise',
					'save_attribute'		=> true,
					'editInfo'				=> $fieldInfo,
					'controller'			=> 'company-customer',
					'sort_order'			=> $db->f('sort_order')
				);
				$i_d = $i+1;
				$i_c = $i+2;
				$i = $i_c;
				$i++;
			}
	}

	$labeldd = $db->query("SELECT * FROM customer_fields ORDER BY sort_order ASC ")->getAll();
	$alldd = $labeldd;

	//Get Custom Fields Array
	$j=1;
	foreach ($alldd as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM customer_field WHERE field_id='".$value['field_id']."' ");
		$j_d = $j+1;
		$j_c = $j+2;

		$info = array('j' => $j, 'j_d' => $j_d, 'j_c' => $j_c, 'section' => 'customerFields', 'edit_parameter' => 'customer_extrafield_dd','save_value' => 'extra', 'save_value_d' => 'extra_d', 'save_value_c' => 'extra_c');
		$result = buildCustomFieldsArray($result,$value,$info);
		
		$j_d = $j+1;
		$j_c = $j+2;
		$j = $j_c;
		$j++;		
	}
	
	//Sort all the linesObj fields
	usort($result['customerFields']['linesObj'], "myCustomSort");
	
	$in['various1'] = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 1' ");
	$in['various2'] = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 2' ");

	$result['customerFields']['various1_text']			= $in['various1'] ? $in['various1'] : gm('Various 1');
	$result['customerFields']['various2_text']			= $in['various2'] ? $in['various2'] : gm('Various 2');
	$result['customerFields']['xget'] = 'customerFields';
	$result['customerFields']['controller'] = 'company-customer';

	// $result = array('extraFields'=>array('customer_fields'=>array(),'contact_fields'=>array()));
	// $db = new sqldb();
	$i=0;
	$j=0;
	$c_fields = $db->query("SELECT * FROM customer_fields ORDER BY field_id ");
	while ($c_fields->next()) {
		$i++;
		if($c_fields->f('is_dd'))
		{
			$has_values = false;
			$values = $db->query("SELECT name FROM customer_extrafield_dd WHERE extra_field_id='".$c_fields->f('field_id')."'");
			if($values->next())
			{
				$has_values=true;
			}
		}

		$result['extraFields']['customer_fields'][]=array(
			'field_id'	=> $c_fields->f('field_id'),
			'label'		=> gm('Field').' '.$i,
			'value'		=> $c_fields->f('label'),
			'checked'	=> $c_fields->f('is_dd')? true: false,
			'warn'		=> $has_values ? '1': '0',
		);		
	
	}
	$c_fields = $db->query("SELECT * FROM contact_fields ORDER BY field_id ");
	while ($c_fields->next()) {
		$j++;
		if($c_fields->f('is_dd'))
		{
			$has_values = false;
			$values = $db->query("SELECT name FROM customer_extrafield_dd WHERE extra_field_id='".$c_fields->f('field_id')."'");
			if($values->next())
			{
				$has_values=true;
			}
		}
		$result['extraFields']['contact_fields'][]=array(
			'field_id'			=> $c_fields->f('field_id'),
			'label'			=> gm('Field').' '.$j,
			'value'			=> $c_fields->f('label'),
			'checked'		=> $c_fields->f('is_dd')? true: false,
			'warn'			=> $has_values ? '1': '0',
		);
		
	}	
	$use = $db->field("SELECT value FROM settings WHERE constant_name = 'USE_COMPANY_NUMBER'");
	$customers = $db->field("SELECT count(customer_id) FROM customers WHERE our_reference='' ORDER BY `customers`.`creation_date` ");
	$deal_start =$db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_DEAL_START'");
	$deal_digit_nr = $db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_DEAL_DIGIT_NR'");
	$result['custRef']=array(
		'inc_part_c'			=> $db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_COMPANY_DIGIT_NR'"),
		'first_reference_c' 	=> $db->field("SELECT value FROM settings WHERE constant_name = 'FIRST_REFERENCE_C'"),
		'use_company_number' 	=> $use == 1 ? true : false,
		'totalcustomers'		=> $customers,
	);
	return $result;
}

function customerStaticFieldsInfo($field){
	$db = new sqldb();
	$fieldLabelName = '';
	$editStatus = false;
	$editParameter = '';

	switch($field){
		case 'legal_type':
			$fieldLabelName = gm('Legal Type');
			$editStatus = true;
			$editParameter = 'customer_legal_type';
			break;
		case 'commercial_name':
			$fieldLabelName = gm('Commercial Name'); 
			break;
		case 'our_reference':
			//$fieldLabelName = gm('Our reference');
			$fieldLabelName = gm('Account Number');
			break;
		case 'sector':
			$fieldLabelName = gm('Activity Sector');
			$editStatus = true;
			$editParameter = 'customer_sector';
			break;
		case 'c_type':
			$fieldLabelName = gm('Type of relationship');
			$editStatus = true;
			$editParameter = 'customer_type';
			break;
		case 'language':
			$fieldLabelName = gm('Language');
			$editStatus = true;
			$editParameter = 'customer_contact_language';
			break;
		case 'acc_manager':
			$fieldLabelName = gm('Account Manager');
			break;
		case 'sales_rep':
			$fieldLabelName = gm('Sales representative');
			$editStatus = true;
			$editParameter = 'sales_rep';
			break;
		case 'lead_source':
			$fieldLabelName = gm('Lead source');
			$editStatus = true;
			$editParameter = 'customer_lead_source';
			break;
		case 'comp_size':
			$fieldLabelName = gm('Company size');
			break;
		case 'activity':
			$fieldLabelName = gm('Activity');
			$editStatus = true;
			$editParameter = 'customer_activity';
			break;
		case 'is_supplier':
			$fieldLabelName = gm('Supplier');
			break;
		case 'is_customer':
			$fieldLabelName = gm('Customer');
			break;
		case 'first_name':
			$fieldLabelName = gm('First Name');
			break;	
		case 'stripe_cust_id':
			$stripe_active =$db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
			if($stripe_active){
				$fieldLabelName = gm('Stripe ID');
			}
			break;	
	}

	return array('label' => $fieldLabelName,'editStatus' => $editStatus, 'editParameter' => $editParameter);
}

function get_contactFields($in){
	
	$result = array('contactFields'=>array('extraList'=>array(),'extraListDD'=>array()));
	$db = new sqldb();

	//ORDER BY field_id
	$label = $db->query("SELECT * FROM contact_fields  WHERE is_dd='0' ORDER BY field_id ")->getAll();
	$all = $label;

	$buildFieldsArray = array();

	foreach ($all as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM contact_field WHERE field_id='".$value['field_id']."' ");
		
		$result['contactFields']['extraList'][] = array(
			'field_id'			=> $value['field_id'],
			'label'				=> $value['label'],
			'value'				=> $value['value'] == 1 ? true : false,
			'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			'value_creation'			=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			// 'value_def'			=> $value['default_value'] == 1 ? 'checked' :($value['value'] == 0 ? 'disabled' : ''),
		);
	}

	$labeldd = $db->query("SELECT * FROM contact_fields WHERE is_dd='1' ORDER BY field_id ")->getAll();
	$alldd = $labeldd;

	foreach ($alldd as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM contact_field WHERE field_id='".$value['field_id']."' ");

		$result['contactFields']['extraListDD'][] = array(
			'field_id'			=> $value['field_id'],
			'label'				=> $value['label'],
			'value'				=> $value['value'] == 1 ? true : false,
			'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			'value_creation'			=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			// 'value_def'			=> $value['default_value'] == 1 ? 'checked' :($value['value'] == 0 ? 'disabled' : ''),
		);	
	}

	$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact' ORDER BY sort_order ASC");

	$i=1;
	while ($db->next()) {
		//Remove Note field from contacts
		if($db->f('field') != 'note'){
			$result['contactFields'][$db->f('field')]	 		= $db->f('value') == 1 ? true : false;
			$result['contactFields'][$db->f('field').'_d']		= $db->f('default_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);
			$result['contactFields'][$db->f('field').'_c']		= $db->f('creation_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);	

		$i_d = $i+1;
		$i_c = $i+2;
		$fieldInfo = contactStaticFieldsInfo($db->f('field'));
		$accountRelatedfieldInfo = contactStaticAccountRelatedFieldsInfo($db->f('field'));

		$staticInfo = array('i' => $i,'i_d' => $i_d, 'i_c' => $i_c);

			if(!empty($fieldInfo['label'])){
				$result = buildStaticFieldsArray($result,$db,$fieldInfo,$staticInfo,'linesObj');

				$i_d = $i+1;
				$i_c = $i+2;
				$i = $i_c;
				$i++;
			}	

			if(!empty($accountRelatedfieldInfo['label'])){
				$result = buildStaticFieldsArray($result,$db,$accountRelatedfieldInfo,$staticInfo,'accountRelatedObj');

				$i_d = $i+1;
				$i_c = $i+2;
				$i = $i_c;
				$i++;
			}	
		}
	}

	$labeldd = $db->query("SELECT * FROM contact_fields ORDER BY sort_order ASC ")->getAll();
	$alldd = $labeldd;

	$j=1;
	foreach ($alldd as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM contact_field WHERE field_id='".$value['field_id']."' ");

		$j_d = $j+1;
		$j_c = $j+2;

		$section = 'contactFields';
		$info = array('j' => $j, 'j_d' => $j_d, 'j_c' => $j_c, 'section' => 'contactFields', 'edit_parameter' => 'contact_extrafield_dd', 'save_value' => 'cextra', 'save_value_d' => 'cextra_d', 'save_value_c' => 'cextra_c');
		$result = buildCustomFieldsArray($result,$value,$info);

		$j_d = $j+1;
		$j_c = $j+2;
		$j = $j_c;
		$j++;	
	}

	//Sort all the linesObj fields
	usort($result['contactFields']['linesObj'], "myCustomSort");

	return $result;
}

function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

function buildStaticFieldsArray($result,$db,$fieldInfo,$staticInfo,$objName){
	$result['contactFields'][$objName][] = array(
			'label'					=> $fieldInfo['label'],
			'is_custom'				=> false,
			'model'					=> $db->f('field'),
			'model_d'				=> $db->f('field').'_d',
			'model_c'				=> $db->f('field').'_c',
			$db->f('field')			=> $db->f('value') == 1 ? true : false,
			$db->f('field').'_d'	=> $db->f('default_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false),
			$db->f('field').'_c'	=> $db->f('creation_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false),
			'checkbox_id'			=> $staticInfo['i'],
			'checkbox_id_d'			=> $staticInfo['i_d'],
			'checkbox_id_c'			=> $staticInfo['i_c'],
			'save_field'			=> 'co_field',
			'save_value'			=> 'customise_contact',
			'save_value_d'			=> 'default_customise_contact',
			'save_value_c'	 		=> 'creation_customise_contact',
			'save_attribute'		=> true,
			'editInfo'				=> $fieldInfo,
			'controller'			=> 'company-xcustomer_contact',
			'sort_order'			=> $db->f('sort_order')
		);

	return $result;
}

function contactStaticAccountRelatedFieldsInfo($field){
	$fieldLabelName = '';
	$editStatus = false;
	$editParameter = '';
	$disable_global=false;

	switch($field){
		/*case 'email':
			$fieldLabelName = gm('Email');
			break;*/
		case 'exact_title':
			$fieldLabelName = gm('Exact Title');
			break;
		case 'function':
			$fieldLabelName = gm('Function');
			$editStatus = true;
			$editParameter = 'customer_contact_job_title';
			break;
		case 'department':
			$fieldLabelName = gm('Department');
			$editStatus = true;
			$editParameter = 'customer_contact_dep';
			break;
		case 'phone':
			$fieldLabelName = gm('Phone');
			break;
		case 'fax':
			$fieldLabelName = gm('Fax');
			break;
		case 's_email':
			$fieldLabelName = gm('Allow on e-mailing lists');
			$disable_global=true;
			break;
	}

	return array('label' => $fieldLabelName,'editStatus' => $editStatus, 'editParameter' => $editParameter, 'disable_global'=>$disable_global);
}

function contactStaticFieldsInfo($field){
	$fieldLabelName = '';
	$editStatus = false;
	$editParameter = '';

	switch($field){
		case 'title':
			$fieldLabelName = gm('Salutation');
			$editStatus = true;
			$editParameter = 'customer_contact_title';
			break;
		case 'gender':
			$fieldLabelName = gm('Gender'); 
			break;
		case 'language':
			$fieldLabelName = gm('Language');
			$editStatus = true;
			$editParameter = 'customer_contact_language';
			break;
		case 'mobile':
			$fieldLabelName = gm('Cell');
			break;
		case 'birthdate':
			$fieldLabelName = gm('Birthdate');
			break;
	}

	return array('label' => $fieldLabelName,'editStatus' => $editStatus, 'editParameter' => $editParameter);
}

function buildCustomFieldsArray($result,$value,$info){

	$result[$info['section']]['linesObj'][] = array(
					'field_id'				=> $value['field_id'],
					'label'					=> $value['label'],
					'is_custom'				=> true,
					'is_dd'					=> $value['is_dd'] == 1 ? true : false,
					'value'					=> $value['value'] == 1 ? true : false,
					'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
					'value_creation'		=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
					'checkbox_id'			=> $info['j'],
					'checkbox_id_d'			=> $info['j_d'],
					'checkbox_id_c'			=> $info['j_c'],
					'save_value'			=> $info['save_value'],
					'save_value_d'			=> $info['save_value_d'],
					'save_value_c'	 		=> $info['save_value_c'],
					'edit_parameter'		=> $info['edit_parameter'],
					'sort_order'			=> $value['sort_order']
				);
	return $result;
}

function get_extraFields($in){
	
	$result = array('extraFields'=>array('customer_fields'=>array(),'contact_fields'=>array()));
	$db = new sqldb();
	$i=0;
	$j=0;
	$c_fields = $db->query("SELECT * FROM customer_fields ORDER BY field_id ");
	while ($c_fields->next()) {
		$i++;
		if($c_fields->f('is_dd'))
		{
			$has_values = false;
			$values = $db->query("SELECT name FROM customer_extrafield_dd WHERE extra_field_id='".$c_fields->f('field_id')."'");
			if($values->next())
			{
				$has_values=true;
			}
		}

		$result['extraFields']['customer_fields'][]=array(
			'field_id'	=> $c_fields->f('field_id'),
			'label'		=> gm('Field').' '.$i,
			'value'		=> $c_fields->f('label'),
			'checked'	=> $c_fields->f('is_dd')? true: false,
			'warn'		=> $has_values ? '1': '0',
		);		
	
	}
	$c_fields = $db->query("SELECT * FROM contact_fields ORDER BY field_id ");
	while ($c_fields->next()) {
		$j++;
		if($c_fields->f('is_dd'))
		{
			$has_values = false;
			$values = $db->query("SELECT name FROM customer_extrafield_dd WHERE extra_field_id='".$c_fields->f('field_id')."'");
			if($values->next())
			{
				$has_values=true;
			}
		}
		$result['extraFields']['contact_fields'][]=array(
			'field_id'			=> $c_fields->f('field_id'),
			'label'			=> gm('Field').' '.$j,
			'value'			=> $c_fields->f('label'),
			'checked'		=> $c_fields->f('is_dd')? true: false,
			'warn'			=> $has_values ? '1': '0',
		);
		
	}	
	
	return $result;
}

function get_custRef($in){
	$db = new sqldb();
	$use = $db->field("SELECT value FROM settings WHERE constant_name = 'USE_COMPANY_NUMBER'");
	$customers = $db->field("SELECT count(customer_id) FROM customers WHERE our_reference='' ORDER BY `customers`.`creation_date` ");
	$deal_start =$db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_DEAL_START'");
	$deal_digit_nr = $db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_DEAL_DIGIT_NR'");
	$result = array('custRef'=>array(
		'inc_part_c'			=> $db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_COMPANY_DIGIT_NR'"),
		'first_reference_c' 	=> $db->field("SELECT value FROM settings WHERE constant_name = 'FIRST_REFERENCE_C'"),
		'use_company_number' 	=> $use == 1 ? true : false,
		'totalcustomers'		=> $customers,
	));

	return $result;
}

function get_dealsRef($in){
	$db = new sqldb();
	$deal_start =$db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_DEAL_START'");
	$deal_digit_nr = $db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_DEAL_DIGIT_NR'");
	$result = array('dealsRef'=>array(
		'ACCOUNT_DEAL_START'	=> $deal_start,
		'ACCOUNT_DEAL_DIGIT_NR' => $deal_digit_nr,
		'EXAMPLE_DEAL' 			=> $deal_start.str_pad(1,$deal_digit_nr,"0",STR_PAD_LEFT),
	));

	return $result;
}

function get_paymentTerm($in){
	$db = new sqldb();
	
	$result = array('paymentTerm'=>array(
		'payment_term'  			=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' "),
		'choose_payment_term_type' 	=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' "),
	));

	return $result;
}

function get_restore($in){
	$db = new sqldb();


	set_time_limit(0);
    ini_set('memory_limit', '-1');   
		$db = new sqldb();
		$db2 = new sqldb();
		$in['crm'] = '0';
		$in['crm2'] = '0';
		$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml';
		$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml';
		$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml';
		$file_fill1 = file_get_contents($file1);
		$file_fill2 = file_get_contents($file2);
		$file_fill3 = file_get_contents($file3);

		if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
			$show_file=1;
		}
		$result = array('erase'=>array(
			'file'				=>    backupcrm2($in['crm']),
			'filebackup'		=>    backupcrm3($in['crm']),
			'filerestore'		=>    backupcrm3($in['crm'],true),
			'file_id'			=>	  $in['crm'],
			'file2_id'			=>	  $in['crm2'],
			'show_file'			=>    $show_file==1?true:false,
			'class1'			=> 	  $class1,
			'class2'			=> 	  $class2,
			'class3'			=> 	  $class3,
			'history'			=>    array(),
			'confirm'			=>    gm('Confirm'),
			'ok'				=>    gm('Ok'),
			'cancel'			=> 	  gm('Cancel'),
		));


	return $result;
}


// INSERT INTO `customise_field` (`controller`, `field`, `value`, `default_value`, `creation_value`) VALUES ('company-xcustomer_contact', 'create', '1', '', '');

?>