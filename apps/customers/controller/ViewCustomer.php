<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
global $config;
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

$users_auto = $db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
$user_auto = array();
foreach ($users_auto as $key => $value) {
	$user_auto[] = array('id'=>$value['user_id'], 'value'=>htmlspecialchars(strip_tags($value['first_name'] . ' ' . $value['last_name'])) );
}

$result =array();

$result['user_auto'] = $user_auto;

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
$result['item_exists']= true;
$customer = $db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ")->getAll();
$customer = $customer[0];

if(!$customer['customer_id']){
	msg::error('Customer does not exist','error');
	$in['item_exists']= false;
    json_out($in);
}

$c_type = explode(',', $customer['c_type']);
if(!empty($c_type[0])){
	foreach ($c_type as $key ) {
		$c_types_name = $db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
		$c_types .= $c_types_name.', ';
		$hidden_c_type .= $c_types_name.':'.$key.',';
	}
}

$usernames = '';
// $usernames2 = '';
$user_id = trim($customer['user_id'],',');
if($user_id){
	$user = $db_users->query("SELECT CONCAT_WS(' ',first_name, last_name) AS username,user_id FROM users WHERE user_id IN (".$user_id.") ORDER BY FIELD(user_id, ".$user_id.")");
	while($user->next()){
		$usernames.= $user->f('username').', ';
		// $usernames2.= $user->f('username').':'.$user->f('user_id').',';
	}
	$usernames = rtrim($usernames,', ');
	// $usernames2 = rtrim($usernames2,',');
}

$all = $db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY field_id ")->getAll();
$customer['extra']=array();
// $result['extra'] = array();
foreach ($all as $key => $value) {
	$f_value = $db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$value['field_id']."' ");	
	if($value['is_dd']==1){
		$f_value = get_extra_dd_value($f_value);
	}
	$customer['extra'][$value['field_id']]=$f_value;
}

if($customer['vat_form']){
	$trends_value=$customer['check_vat_number'];
	$trends_value=trim($trends_value," ");
	$trends_value=str_replace(" ","",$trends_value);
	$trends_value=str_replace(".","",$trends_value);
	$trends_value=strtoupper($trends_value);
	if(substr($trends_value,0,2)=='BE'){
		$trends_value=str_replace("BE","",$trends_value);
	}
}

$customer['payment_term_start'] = $customer['payment_term_type'];

if($customer['creation_date']){
	$customer['creation_date'] = date(ACCOUNT_DATE_FORMAT,$customer['creation_date']);
}else{
	unset($customer['creation_date']);
}

if($customer['apply_fix_disc']=='1'){
	$customer['apply_fix_disc']=true;
}

if($customer['apply_line_disc']=='1'){
	$customer['apply_line_disc']=true;
}
$cc = $db->query("SELECT is_supplier,is_customer FROM customers WHERE customer_id = '".$customer['customer_id']."' ");
$is_supplier = $cc->f('is_supplier');
$is_customer = $cc->f('is_customer');

$primal_address = $db->query("SELECT customer_addresses.*,country.name AS country
			FROM customer_addresses
			LEFT JOIN country ON country.country_id=customer_addresses.country_id
			WHERE customer_id='".$in['customer_id']."' AND is_primary=1
			ORDER BY is_primary ASC,address_id ");

$ADV_PRODUCT = $db->field("SELECT value FROM  settings WHERE constant_name='ADV_PRODUCT'");
$multiple_identity = $db->field("SELECT COUNT(identity_id) FROM multiple_identity");

if($multiple_identity==0){
	$hide_identity = false;
}else{
	$hide_identity = true;
}

$full_address="";
if($primal_address->f('address')){
	$full_address.=urlencode($primal_address->f('address')).',';
}
if($primal_address->f('zip')){
	$full_address.=urlencode($primal_address->f('zip')).',';
}
if($primal_address->f('city')){
	$full_address.=urlencode($primal_address->f('city')).',';
}
if($primal_address->f('country_id')){
	$full_address.=urlencode(get_country_name($primal_address->f('country_id'))).',';
}
$full_address=rtrim($full_address,",");

$result['address_primary']					= $primal_address->f('address');
$result['zip_primary']						= $primal_address->f('zip');
$result['city_primary']						= $primal_address->f('city');
$result['country_primary']					= ' - '.get_country_name($primal_address->f('country_id'));
/*$result['full_address_map_src']				= $full_address ? 'https://www.google.com/maps/embed/v1/place?key=AIzaSyAwlmPeY64Lpr4QlwFdWR_gMF1ONK3OyTI&q='.$full_address : ''; */
$result['full_address_map_src']				= $full_address ? 'https://www.google.com/maps/embed/v1/place?key=' . $config['GOOGLE_MAPS_API_KEY'] . '&q=' . $full_address : '';
$result['obj']['invoice_email_type']		= $customer['invoice_email_type'];
$result['obj']['customer_notes']			= $customer['customer_notes'];
$result['obj']['invoice_email']				= $customer['invoice_email'];
$result['obj']['attention_of_invoice']		= $customer['attention_of_invoice'];
$result['obj']								= $customer;
$result['obj']['legal_type_name']			= $db->field("SELECT name FROM customer_legal_type WHERE id = '".$customer['legal_type']."' ");
$result['obj']['acc_manager']   			= htmlspecialchars_decode(stripslashes($usernames));
$result['obj']['sales_rep_name']			= $db->field("SELECT name FROM sales_rep WHERE id = '".$customer['sales_rep']."' ");
$result['obj']['is_supplier']				= $is_supplier==1 ? gm('Yes') : gm('No');
$result['obj']['is_customer']				= $is_customer==1 ? gm('Yes') : gm('No');
$result['obj']['exist_supplier']			= $is_supplier==1 ? true : false;
$result['obj']['exist_customer']			= $is_customer==1 ? true : false;
$result['obj']['sector_name']				= $db->field("SELECT name FROM customer_sector WHERE id = '".$customer['sector']."' ");
//$result['obj']['language_name']				= $db->field("SELECT name FROM customer_contact_language WHERE id = '".$customer['language']."' ");//
$result['obj']['language_name']				= $customer['language']<10000? (gm($db->field("SELECT language FROM pim_lang WHERE lang_id = '".$customer['language']."' "))) :
											(gm($db->field("SELECT language FROM pim_custom_lang WHERE lang_id = '".$customer['language']."' ")));
$result['obj']['lead_source_name'] 			= $db->field("SELECT name FROM customer_lead_source WHERE id = '".$customer['lead_source']."' ");
$result['obj']['activity_name'] 			= $db->field("SELECT name FROM customer_activity WHERE id = '".$customer['activity']."' ");
// $result['obj']['various1_name'] 			= $db->field("SELECT name FROM customer_various1 WHERE id = '".$customer['various1']."' ");
// $result['obj']['various2_name'] 			= $db->field("SELECT name FROM customer_various2 WHERE id = '".$customer['various2']."' ");
$result['obj']['trends_link']				= $customer['vat_form'] ? 'http://www.trendstop.be/'.$_SESSION['l'].'/'.$trends_value : '#';
$result['obj']['trends_link_hide']			= $customer['vat_form'] ? true : false;
$result['obj']['do_financial']				= 'customers-ViewCustomer-customers-update_financial';
$result['obj']['do_notes']					= 'customers-ViewCustomer-customers-customer_notes';
$result['obj']['vies_ok'] 					= $customer['check_vat_number'] && ($customer['check_vat_number'] ==  $customer['btw_nr']) && !$customer['vat_form'] ? true : false;
$result['addresses']						= get_customerAddresses($in);
$result['vat_regim_dd']						= build_vat_regime_dd();
$result['vat_id']							= $customer['vat_id'];
$result['vat_dd']							= build_vat_dd($customer['vat_id']);
$result['currency_dd']						= build_currency_list();
$result['cat_dd']							= build_cat_dd();
$result['lang_dd']							= build_language_dd_new();
$result['multiple_dd'] 						= build_identity_dd();
$result['ADV_CRM']							= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;
$result['custom_prices']					= get_customPrices($in);
$result['obj']['currency_id']				= $customer['currency_id'] ? $customer['currency_id'] : ACCOUNT_CURRENCY_TYPE;
$result['obj']['currency_code']				= currency::get_currency($result['obj']['currency_id']);
/*$result['obj']['credit_limit']				= !$result['obj']['credit_limit'] ? '' : $result['obj']['credit_limit'];*/
$subscription = $db_users->field("SELECT base_plan_id FROM users WHERE `user_id`='".$_SESSION['u_id']."' ");
$result['not_easyinv_subscription']=$subscription=='7'? false : true;

if($result['obj']['website']){
	if(strpos($result['obj']['website'],'http')===false){
		$result['obj']['website']='http://'.$result['obj']['website'];
	}
}

$result['ADV_PRODUCT']						= $ADV_PRODUCT == 1 ? true : false;
$result['hide_identity']					= $hide_identity;
$result['is_zen']							= defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 ? true : false;

	$result['atachment'] = array();
	$atachment = $db->query("SELECT * FROM attached_files WHERE type='0' AND REPLACE(REPLACE(path,'".INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/customer/',''),'/','')='".$in['customer_id']."' ");
	while ($atachment->next()) {
		array_push($result['atachment'], array(
			'file'		=> $atachment->f('name'),
			'checked'	=> $atachment->f('default') == 1 ? true : false,
			'file_id'	=> $atachment->f('file_id'),
			//'path'		=> INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/customer/'.$in['customer_id'].'/'.$atachment->f('name'),
			'path'		=> $atachment->f('path').$atachment->f('name'),
			//'checkeddrop'		=> $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_DROP_BOX' ") == '1' ? true : false,
		));
	}

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$result['families']=array();
while($db->move_next()){
    $families = array(
	'name'  => $db->f('name'),
	'id'=> $db->f('id')
    );
      
    array_push($result['families'], $families);
}



//$nylas_data=$db_users->field("SELECT active FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
$nylas_data=$db_users->field("SELECT active FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
$nylas_active=$db->field("SELECT active FROM apps WHERE name='Nylas' AND main_app_id='0' and type='main' ");
$result['is_nylas']= $nylas_active && $nylas_data ? true : false;
$various1 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 1' ");
if(!$various1){
	$various1 = gm('Variuos 1');
}
$various2 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 2' ");
if(!$various2){
	$various2 = gm('Variuos 2');
}

$default_front=array();
//Leave for now
// $default_show=array();
// $def=$db->query("SELECT * FROM customise_field WHERE controller='company-customer'  order by default_value desc,field ");
$relantionship=$db->field("SELECT value FROM customise_field WHERE controller='company-customer' AND field='c_type' ");
$result['type_if']=$relantionship==1 ? true : false;

// Leave for now
// while ($def->next()) {
// 	$line = array();
// 	$line[$def->f('field')] = $def->f('value') == 1 ? true : false;
	
// 	if($def->f('field')=='activity' ||$def->f('field')=='lead_source'  || $def->f('field')=='language' || $def->f('field')=='sector' || $def->f('field')=='legal_type' || $def->f('field') == 'various1' || $def->f('field')=='various2'){
// 		$var = $def->f('field');
// 		$val = $var.'_name';
		
// 		$line[$var.'_text']	= $$var;
// 		$line[$var.'_name'] = $result['obj'][$val];

// 	}
// 	$default_show[$def->f('field')] = $line;
// 	// array_push($default_show, $line);
// }


$def = $db->query("SELECT * FROM customise_field WHERE controller='company-customer'  order by default_value desc,sort_order ASC ");
$customiseFieldsAll = $def->getAll();
$customiseExtraFields = $db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY sort_order ASC ")->getALl();
$result_front =  array_merge($customiseFieldsAll, $customiseExtraFields);

//Sort all fields
usort($result_front, "myCustomSort");

foreach ($result_front as  $res) {
	$loop_name = $res['default_value'] == 1 ? 'default_show' : 'just_show';

	if(!empty($res['controller'])){
		$line = array($res['field']	=> $res['value'] == 1 ? true : false,
				'is_custom_field'	=> 'false',
			);
		$default_front[$loop_name][] = $line;	
	} else {
		$line = array('field_id'			=> $res['field_id'],	                    
						'label'				=> $res['label'],				
						'is_custom_field'	=> 'true',
			);
		$default_front[$loop_name][] = $line;
	}
}

$result['c_types']					= rtrim($c_types,', ');
$result['default_show']				= $default_front;
$allo_available=false;
$user_devices=array();
$allo_devices=$db->query("SELECT * FROM allo_devices WHERE user_id='".$_SESSION['u_id']."' ");
$nr_devices=0;
while($allo_devices->next()){
	$temp_devices=array(
		'id'		=> $allo_devices->f('device_id'),
		'value'		=> stripslashes($allo_devices->f('device_name'))
		);
	array_push($user_devices, $temp_devices);
	$nr_devices++;
}
$result['devices']=$user_devices;
$result['allo_available']=defined('ALLOCLOUD_ACTIVE') && ALLOCLOUD_ACTIVE==1 && $nr_devices ? true : false;

//Set Installation Inactive if there is not Intervention subscription
$check_intervention = in_array(13,perm::$allow_apps);
$result['is_active_MAINTENANCE'] = $check_intervention; 
//End Set Installation Inactive if there is not Intervention subscription

//Get permissions for customer quick add documents
$WIZZARD_COMPLETE = $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE'");

if($WIZZARD_COMPLETE==0){
    $hide_quick_add=false;
}else{
    $hide_quick_add=true;
}

$is = false;
$perm = array();
$i = 0;
for ($j = 1; $j<=19; $j++){
	
	if(in_array($j,perm::$allow_apps)){
		$is = true;
	}
	$perm['is_'.$j] = $is;
	$perm['is_only_'.$j] = $is;
	$i++;
	$is = false;
	//$val =$db_users->field("SELECT value FROM user_meta WHERE name='MODULE_".$j."' AND user_id='".$_SESSION['u_id']."' ");
	$val =$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$j,'user_id'=>$_SESSION['u_id']]);
	if($val===NULL){
		if(defined('MODULE_'.$j)){
			$val = constant('MODULE_'.$j);
		}
	}
	$perm['hide_'.$j] = $val == 0 ? 'hidden' : '';
}

$result['hide_quick_add'] = $hide_quick_add;
$result['quickAddOptions'] = $perm;
$result['deals_auto']=get_deals($in);
if($customer['third_party_id']){
	$in['third_party_id']=$customer['third_party_id'];
}
$result['third_party_dd']=get_third_customers($in);

json_out($result);

function get_customerAddresses($in){

	$db = new sqldb();
	$result = array('list'=>array(),'count'=>0);
	$comp_addr_data = $db->query("SELECT customer_addresses.*,country.name AS country
			FROM customer_addresses
			LEFT JOIN country ON country.country_id=customer_addresses.country_id
			WHERE customer_id='".$in['customer_id']."'
			ORDER BY is_primary DESC,address_id ");
	$number_addresses = $db->field("SELECT COUNT(address_id)
			FROM customer_addresses
			LEFT JOIN country ON country.country_id=customer_addresses.country_id
			WHERE customer_id='".$in['customer_id']."' AND customer_addresses.address!='' AND customer_addresses.zip!='' AND customer_addresses.city!=''
			ORDER BY is_primary DESC,address_id ");
	$i=$number_addresses;
	while($comp_addr_data->next()){
		$full_address="";
		$address_type = '';
		if($comp_addr_data->f('is_primary') == 1){
			$address_type .= gm('Primary addr').' ';
			$coords = $db->query("SELECT location_lat AS latitude, location_lng AS longitude FROM  `addresses_coord` WHERE  `customer_id` ='".$in['customer_id']."' ")->getAll();
			if(empty($coords[0])){
				$coords[0] =updateCustomerPrimaryAddressCoords($comp_addr_data,$in['customer_id']);
			}
		}
		elseif($comp_addr_data->f('billing') == 1){
			$address_type .= gm('Billing addr').' ';
		}elseif($comp_addr_data->f('delivery') == 1){
			$address_type .= gm('Delivery addr').' ';
		}elseif($comp_addr_data->f('site') == 1){
			$address_type .= gm('Site address').' ';
		}

		$text_delivery_address = gm('Delivery addr');
		$text_billing_address = gm('Billing addr');
		$text_primary_address = gm('Primary addr');
		$text_site_address = gm('Site address');

		$address_type2 = '-';
		$comp_addr_data->f('delivery') == 1 ? $address_type2 = $text_delivery_address : '';
		$comp_addr_data->f('billing') == 1 ? $address_type2 = $text_billing_address : '';
		$comp_addr_data->f('site') == 1 ? $address_type2 = $text_site_address : '';
		($comp_addr_data->f('delivery') == 1 && $comp_addr_data->f('billing') == 1) ? $address_type2 = $text_delivery_address.' / '.$text_billing_address : '';
		($comp_addr_data->f('delivery') == 1 && $comp_addr_data->f('site') == 1) ? $address_type2 = $text_delivery_address.' / '.$text_site_address : '';
		($comp_addr_data->f('billing') == 1 && $comp_addr_data->f('site') == 1) ? $address_type2 = $text_billing_address.' / '.$text_site_address : '';
		($comp_addr_data->f('billing') == 1 && $comp_addr_data->f('site') == 1 && $comp_addr_data->f('delivery') == 1) ? $address_type2 = $text_billing_address.' / '.$text_site_address.' / '.$text_delivery_address : '';
		if($comp_addr_data->f('is_primary') == 1 && $comp_addr_data->f('delivery') == 1){
			$comp_addr_data->f('is_primary') == 1 ? $address_type2 .= ' / '.$text_primary_address : '';
		}elseif($comp_addr_data->f('is_primary') == 1 && $comp_addr_data->f('billing') == 1){
			$comp_addr_data->f('is_primary') == 1 ? $address_type2 .= ' / '.$text_primary_address : '';
		}elseif($comp_addr_data->f('is_primary') == 1 && $comp_addr_data->f('site') == 1){
			$comp_addr_data->f('is_primary') == 1 ? $address_type2 .= ' / '.$text_primary_address : '';
		}elseif($comp_addr_data->f('is_primary') == 1){
			$comp_addr_data->f('is_primary') == 1 ? $address_type2 = $text_primary_address : '';
		}

		$full_address=$comp_addr_data->f('address')."\n";
		if($comp_addr_data->f('zip')){
            $full_address.= $comp_addr_data->f('zip')." ";
        }
        if($comp_addr_data->f('city')){
        	$full_address.= $comp_addr_data->f('city')." ";        
        }
        $full_address=rtrim($full_address);
        $full_address.=" - ".get_country_name($comp_addr_data->f('country_id') );

		/*$comp_addr_data->f('is_primary') == 1 ? $address_type2 .= ' / '.$text_primary_address : '';*/
		
		// $address_type = rtrim($address_type,' / ');

		$line=array(
			'address' 		=> $comp_addr_data->f('address'),
			'address_txt' 	=> nl2br($comp_addr_data->f('address')),
			'zip' 			=> $comp_addr_data->f('zip'),
			'city' 			=> $comp_addr_data->f('city'),
			'country' 		=> get_country_name($comp_addr_data->f('country_id')),
			'state_name' 	=> $comp_addr_data->f('state_name'),
			'address_type'	=> $address_type,
			'address_type2'	=> $address_type2,
			'customer_id'	=> $in['customer_id'],
			'address_id'	=> $comp_addr_data->f('address_id'),
			'delete_link'	=> array('do'=>'customers-ViewCustomer-customers-address_delete', 'customer_id'=>$in['customer_id'], 'xget'=> 'customerAddresses', 'address_id'=> $comp_addr_data->f('address_id') ),
			'full_address'	=> nl2br($full_address),
			'is_primary'	=> $comp_addr_data->f('is_primary') ? true : false,
			'primary_id'	=> $comp_addr_data->f('is_primary') ? $comp_addr_data->f('address_id') : '',
			'is_billing'	=> $comp_addr_data->f('billing') ? true : false,
			'billing_id'	=> $comp_addr_data->f('billing') ? $comp_addr_data->f('address_id') : '',
			'is_delivery'	=> $comp_addr_data->f('delivery') ? true : false,
			'is_site'		=> $comp_addr_data->f('site') ? true : false,
		);
		$result['list'][] = $line;
		/*$i++;*/
	}

	//Show add address on modal only on CRM
	$show_add = true;

	if($in['show_add']){
		$show_add = false;
	}

	$result['count'] = $i;
	$result['customer_id'] = $in['customer_id'];
	$result['coords'] = $coords[0] ? $coords[0] : array();
	$result['show_add'] = $show_add;
	return $result;
}

function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

function get_customerAddress($in){
	$db = new sqldb();
	$result = array();
	
	if($in['address_id'] == 'tmp'){
		$page_title 				= gm('Add Address');
		$result['do_next']			='customers-ViewCustomer-customers-address_add';
		$result['country_id']		= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$result['state_id']			= @constant("ACCOUNT_BILLING_STATE_ID") ? (string)@constant("ACCOUNT_BILLING_STATE_ID") : '26';
	}else{

		$result['do_next']='customers-ViewCustomer-customers-address_update';
		$db->query("SELECT * FROM customer_addresses 
					WHERE address_id='".$in['address_id']."' 
					AND customer_id='".$in['customer_id']."'");
		
		$page_title = gm('Edit Address');
		
		$db->f('billing') 		== 1 ? $billing =true : false;
		$db->f('delivery') 		== 1 ? $delivery =true : false;
		$result['city']				= $db->f('city');
		$result['zip']            	= $db->f('zip');
		$result['address']			= $db->f('address');
		$result['state_name']		= $db->f('state_name');
		$result['contact_id']		= $db->f('contact_id');
		/*if($db->f('contact_id')){
			$in['contact'] = get_contact_name($db->f('contact_id'));
		}*/
		
		$result['billing']			= $billing;
		$result['delivery']			= $delivery;
		$result['site']				= $db->f('site') == 1 ? true :false;;
		$result['primary']			= $db->f('is_primary') == 1 ? true :false;
		$result['country_id']		= $db->f('country_id');
	}
	$result['country_dd']			= build_country_list($result['country_id']);
	$result['state_dd']				= build_state_list($result['state_id']);	
	// $view->assign(array(
	$result['page_title']			= $page_title;
	$result['customer_id']			= $in['customer_id'];
	$result['address_id']			= $in['address_id'];
	$result['xget']					='customerAddress';
	// ));


	return $result;
}

function get_customPrices($in){
	$db = new sqldb();

	$result = array('list'=>array(),'count'=>0);
	$l_r =ROW_PER_PAGE;
	$order_by = " ORDER BY  pim_articles.item_code  ";

	$order_by_array = array('item_code','internal_name','article_category','price','customer_name','stock','stock_ant');

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	$filter = '1=1';

	//$filter.= " AND pim_articles.is_service='0' ";

	if($in['archived']==0){
	    $filter.= " AND pim_articles.active='1' ";
	}else{
	    $filter.= " AND pim_articles.active='0' ";

	}

	if(!empty($in['search'])){
    	$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
    
	}

	if(!empty($in['name'])){
	    $filter .= " AND ( supplier_name LIKE '%".$in['name']."%') ";
	 }
	if(!empty($in['supplier_reference'])){
	    $filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
	}

	if($in['article_category_id']){
	    $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
	}

	if($in['order_by']){
	    if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == '1'){
		    $order = " DESC ";
		}
		if($in['order_by']=='customer_name'){
		    $in['order_by']='supplier_name';
		}
	    if($in['order_by']=='stock_ant'){

	    }else{
		   $order_by =" ORDER BY ".$in['order_by']." ".$order;
		   $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	    }
		/*$view_list->assign(array(
		    'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
		));*/
	    }
	}

	$custom_prices_articles = $db->query("SELECT pim_articles.article_id,pim_articles.item_code,pim_articles.internal_name,pim_articles.supplier_id,pim_articles.supplier_name,pim_articles.supplier_reference,
			pim_articles.price,pim_articles.article_category, pim_articles_lang.description AS description, pim_articles.ean_code,pim_article_categories.name AS article_category_name, 
			customer_custom_article_price.price AS custom_price 
			FROM  customer_custom_article_price
			LEFT JOIN pim_articles ON customer_custom_article_price.article_id=pim_articles.article_id AND customer_id='".$in['customer_id']."' 
			LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
 			LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='1'
			WHERE $filter 
			GROUP by pim_articles.article_id 
			".$order_by."
			 LIMIT ".$offset*$l_r.",".$l_r."
			 ");


	$articles_number = $db->field("SELECT COUNT(DISTINCT pim_articles.article_id)
			FROM  customer_custom_article_price
			LEFT JOIN pim_articles ON customer_custom_article_price.article_id=pim_articles.article_id AND customer_id='".$in['customer_id']."' 
			LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
 			LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='1'
			WHERE  $filter 			
			".$order_by."
			
			");

	while($custom_prices_articles->next()){


		$line=array(
			'article_id' 	=> $custom_prices_articles->f('article_id'),
			'item_code' 			=> $custom_prices_articles->f('item_code'),
			'internal_name' 			=> $custom_prices_articles->f('internal_name'),
			'base_price' 	=> display_number($custom_prices_articles->f('price')),
			'custom_price' 	=> display_number($custom_prices_articles->f('custom_price')),
			'article_category' => $custom_prices_articles->f('article_category'),
			'supplier_id' 	=> $custom_prices_articles->f('supplier_id'),
			'supplier_name' => $custom_prices_articles->f('supplier_name'),
			'supplier_reference' => $custom_prices_articles->f('supplier_reference'),
			'customer_id' 	=> $in['customer_id'],
			'confirm'			=>    gm('Confirm'),
			'ok'				=>    gm('Ok'),
			'cancel'			=> 	  gm('Cancel'),
			
			'delete_link'	=> array('do'=>'customers-ViewCustomer-customers-delete_custom_price', 'customer_id'=>$in['customer_id'],  'article_id'=> $custom_prices_articles->f('article_id'),'xget'=>'customPrices'  ),
		);
		$result['list'][] = $line;
		/*$i++;*/
	}
	$result['count'] = $articles_number;
	$result['customer_id'] = $in['customer_id'];
	$result['l_r'] = $l_r;
	$result['max_rows'] = $articles_number;
	

	return $result;
}

/**
 * Activities
 *
 * @return array
 * @author 
 **/
function get_customerActivity($in)
{
	return false;
	$db = new sqldb();
	global $config;
	
	if($in['from_date']){
		$in['from_date'] = strtotime($in['from_date']);
	}
	if($in['to_date']){
		$in['to_date'] = strtotime($in['to_date']);
	}

	if(!$in['user_id'])
	{
	  	$in['user_id'] = $_SESSION['u_id'];
	}
	
	$result = array( 'item' => array() );

	$now = time();
	
	$limit = "";

	if( isset($in['offset']) ){
		$limit = " LIMIT ".($in['offset']*5)." ,5";
	}

	$filter_u = ' 1=1 ';
	$filter_u2 = ' ';
	if($in['user_id']){
		$filter_u2 = " AND user_id='".$in['user_id']."' ";
	}
	$filter_c = '';
	$filter_q = '';
	$join = '';
	
	switch ($in['filter_type']) {
		case 'date':
			if($in['from_date'] && !$in['to_date'])
			{
				$filter_c = "AND logging.due_date>'".$in['from_date']."'";
			}elseif(!$in['from_date'] && $in['to_date'])
			{
				$filter_c = "AND logging.due_date<'".$in['to_date']."'";
			}elseif($in['from_date'] && $in['to_date'])
			{
				$filter_c = "AND logging.due_date<'".$in['to_date']."' AND logging.due_date>'".$in['from_date']."'";
			}else
			{
				$filter_c = '';
			}
			if($in['customer_id']){
				$filter_c .= " AND customer_contact_activity.customer_id='".$in['customer_id']."'";
			}
			break;
		case 'event':
			if($in['event_type'])
			{
				$filter_c = "AND contact_activity_type='".$in['event_type']."'";
				if($in['event_type']!=7)
				{
				}
			}
			if($in['c_id']){
				$filter_c .= "AND customer_contact_activity.customer_id='".$in['c_id']."'";
			}
			break;
		case 'customer':
			if($in['customer_id'])
			{
				$filter_c = "AND customer_contact_activity.customer_id='".$in['customer_id']."'";
			}
			break;
		case 'user_timeline':
			if($in['from_date'] && !$in['to_date']){
				$filter_c = " AND logging.due_date>'".$in['from_date']."' ";
			}elseif(!$in['from_date'] && $in['to_date']){
				$filter_c = " AND logging.due_date<'".$in['to_date']."' ";
			}elseif($in['from_date'] && $in['to_date']){
				$filter_c = " AND logging.due_date<'".$in['to_date']."' AND logging.due_date>'".$in['from_date']."' ";
			}
			if($in['customer_id']){
				$filter_c .= " AND customer_contact_activity.customer_id='".$in['customer_id']."' ";
			}
			$filter_u = " email_to='".$in['user_id']."' ";
			break;
	}

	if($in['contact_id2']){
		$in['contact_id'] = $in['contact_id2'];
	}

	if($in['contact_id']){
		$filter_c .= " AND customer_contact_activity.contact_id='".$in['contact_id']."' OR (
					customer_contact_activity.contact_id IS NULL
					AND customer_contact_activity_contacts.contact_id =".$in['contact_id']."
					) ";
		$join = 'LEFT JOIN customer_contact_activity_contacts ON customer_contact_activity.`customer_contact_activity_id` = customer_contact_activity_contacts.activity_id AND action_type = 0';
	}

	// $result['page_title']=get_user_name($in['user_id']);

	$i=0;
	$customers_id = array();
	if( isset($in['offset']) ){
		$result['max_rows'] = $db->field("SELECT COUNT( DISTINCT customer_contact_activity_id )  FROM customer_contact_activity
						{$join}
						INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
						LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
						WHERE {$filter_u} {$filter_c} ");
	}
	$comments = $db->query("SELECT customer_contact_activity.* FROM customer_contact_activity
					{$join}
					INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
					LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
					WHERE {$filter_u} {$filter_c}
					GROUP BY customer_contact_activity.customer_contact_activity_id ORDER BY logging.due_date DESC {$limit} ")->getAll();

	// $q_ids = '';
	// 
	// $q = array(); //for quotes
	
	// $id = rtrim($id,',');
	
	/*function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
	    $sort_col = array();
	    foreach ($arr as $key=> $row) {
	        $sort_col[$key] = strtolower($row[$col]);
	    }
	    array_multisort($sort_col, $dir, $arr,SORT_STRING);
	}*/
	
	// array_sort_by_column($result,'date',SORT_DESC);

	$days = array();
	$contact_name = array();
	// $user_name = array();
	$users_id = array();
	$w = 0;
	foreach ($comments as $key => $value) {
		$new_event = '';
		$source='-';
		$stage = '-';
		$type='-';
		$side = 'left';
		$activity_status='';
		$color='';
		$assigned = '';
		$type_of_call='';
		$type_of_call_id='';
		$status_change = 0;
		if($value['customer_contact_activity_id'])
		{
			$db->query("SELECT * FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' ".$filter_u2." ");
			$db->move_next();
			$un_log_id = $db->f('log_id');
			if($db->f('seen')=='0')
			{
				$new_event = 'new_event';
				$db->query("UPDATE logging_tracked SET seen='1' WHERE activity_id='".$value['customer_contact_activity_id']."' ".$filter_u2." ");
			}
				$log_assigned=$db->query("SELECT finished, due_date, to_user_id FROM logging WHERE log_id='".$un_log_id."' ");
				if(!array_key_exists($log_assigned->f('to_user_id'), $users_id)){
					$users_id[$log_assigned->f('to_user_id')] = get_user_name($log_assigned->f('to_user_id'));
				}
				$assigned = $users_id[$log_assigned->f('to_user_id')];			
		}
		elseif($value['log_id']){
			$db->query("SELECT * FROM logging_tracked WHERE log_id='".$value['log_id']."' ".$filter_u2." ");
			$db->move_next();
			if($db->f('seen')=='0')
			{
				$new_event = 'new_event';
				$db->query("UPDATE logging_tracked SET seen='1' WHERE log_id='".$value['log_id']."' ".$filter_u2." ");
			}
		}
		else{
			if($value['sent_date']){
				$log_id = $db->field("SELECT log_id FROM logging WHERE field_name='quote_id' AND field_value='".$value['quote_id']."' AND date='".$value['sent_date']."'");
				if($log_id){
					$db->query("SELECT * FROM logging_tracked WHERE log_id='".$log_id."' ".$filter_u2." ");
					$db->move_next();
					if($db->f('seen')=='0')
					{
						$new_event = 'new_event';
						$db->query("UPDATE logging_tracked SET seen='1' WHERE log_id='".$log_id."' ".$filter_u2." ");
					}
				}
			}
		}
		
		switch ($value['contact_activity_type']) {
			case '1':
				$action = 'email';
				$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
				$status_values->move_next();
				$db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
				if($db->f('finished') == 1){
					$activity_status = gm('Completed');
					$color = 'green_status';
					$status_change = 1;
				}else{
					$activity_status = gm('Scheduled');
					$color = 'opened_status';
					$status_change = 0;
				}
				$log_code=$db->f('log_id');
				break;
			case '2':
				$action = 'events';
				$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
				$status_values->move_next();
				$db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
				if($db->f('finished') == 1){
					$activity_status = gm('Completed');
					$color = 'green_status';
					$status_change = 1;
				}else{
					$activity_status = gm('Scheduled');
					$color = 'opened_status';
					$status_change = 0;
				}
				$log_code=$db->f('log_id');
				break;
			case '3':
				$action = 'fax';
				break;
			case '4':
				$action = 'meeting';
				$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
				$status_values->move_next();
				$db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
				if($db->f('finished') == 1){
					$activity_status = gm('Completed');
					$color = 'green_status';
					$status_change = 1;
				}else{
					$activity_status = gm('Scheduled');
					$color = 'opened_status';
					$status_change = 0;
				}
				$log_code=$db->f('log_id');
				break;
			case '5':
				$action = 'phone';
				$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
				$status_values->move_next();
				$db->query("SELECT log_id,finished, due_date,status_other,type_of_call FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
				if($db->f('finished') == 1){
					$activity_status = gm('Completed');
					$color = 'green_status';
					$status_change = 1;
				}else{
					$activity_status = gm('Scheduled');
					$color = 'opened_status';
					$status_change = 0;
				}
				if($db->f('type_of_call') == 1){
					$type_of_call = gm('incoming call');
					$type_of_call_id = 1;
					
				}else{
					$type_of_call = gm('outgoing call');
					$type_of_call_id = 0;
					
				}
				$log_code=$db->f('log_id');
				break;
			default:
				$action = 'other';
				$status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
				$db->query("SELECT log_id, finished, reminder_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
				$status_change = 0;
				if($db->f('finished') == 1){
					$activity_status = gm('Completed');
					$color = 'green_status';
					$status_change = 1;
				}else if($db->f('reminder_date') < time()){
					$activity_status = gm('Late');
					$color = 'red_status';
				}else if($db->f('status_other') == 3){
					$activity_status = gm('On hold');
					$color = 'opened_status';
				}else if($db->f('status_other') == 2){
					$activity_status = gm('In progress');
					$color = 'opened_status';
				}else{
					$activity_status = gm('New');
					$color = 'opened_status';
				}
				$log_code=$db->f('log_id');
				break;
		}

		$message = nl2br($value['contact_activity_note']);
		// $long_message = $value['contact_activity_note'];
		if($value['email_to'] && !array_key_exists($value['email_to'], $user_id)) {
			$user_id[$value['email_to']] = get_user_name($value['email_to']);
		}
		$name = $user_id[$value['email_to']];
		/*if($value['version_id']){
			$name = '';
			$side = 'left';
			if($value['date'] == '0'){
				continue;
			}

			$action = 'quote_sent';
			$message = gm('Quote').': <a href="index.php?do=quote-quote&quote_id='.$value['quote_id'].'">'.$q[$value['quote_id']]['serial_number'].'['.$value['version_code'].']</a> '.gm('was sent');
			if($q[$value['quote_id']]['stage']){
				$stage = $db->field("SELECT name FROM tblquote_stage WHERE id = '".$q[$value['quote_id']]['stage']."' ");
			}
			if($q[$value['quote_id']]['source']){
				$source = $db->field("SELECT name FROM tblquote_source WHERE id = '".$q[$value['quote_id']]['source']."' ");
			}
			if($q[$value['quote_id']]['type']){
				$type = $db->field("SELECT name FROM tblquote_type WHERE id = '".$q[$value['quote_id']]['type']."' ");
			}
			if($q[$value['quote_id']]['autor']){
				$name =  get_user_name($q[$value['quote_id']]['autor']);
			}
		}*/
		
		/*if($value['log_id']){
			$name = '';
			$side = 'left';
			$action = 'quote_sent';
			$status = 'accepted';
			if(strpos($value['message'], 'Rejected') !== false ){
				$status = 'rejected';
			}
			$version_text = '';
			$version_code = $db->field("SELECT message FROM tblquote_history WHERE quote_id='".$value['field_value']."' AND date='".$value['date']."' ");
			if(!is_null($version_code)){
				$version_text = '['.$version_code.']';
			}
			$message = gm('Quote').': <a href="index.php?do=quote-quote&quote_id='.$value['field_value'].'">'.$q[$value['field_value']]['serial_number'].$version_text.'</a> '.gm('was '.$status);
			if($q[$value['field_value']]['stage']){
				$stage = $db->field("SELECT name FROM tblquote_stage WHERE id = '".$q[$value['field_value']]['stage']."' ");
			}
			if($q[$value['field_value']]['source']){
				$source = $db->field("SELECT name FROM tblquote_source WHERE id = '".$q[$value['field_value']]['source']."' ");
			}
			if($q[$value['field_value']]['type']){
				$type = $db->field("SELECT name FROM tblquote_type WHERE id = '".$q[$value['field_value']]['type']."' ");
			}
			if($q[$value['field_value']]['autor']){
				$name =  get_user_name($q[$value['field_value']]['autor']);
			}
		}*/
		$contacts_array = array();
		$assign_to_str = '';
		if($value['contact_id'])
		{
			if(!array_key_exists($value['contact_id'], $contact_name)){
				$contact_name[$value['contact_id']] = get_contact_first_and_name($value['contact_id']);
			}

			// $view->assign(array(
			$assign_to_str	= $value['contact_id'];
				// ),'item');
			// $view->assign(array(
			$contacts_array[]['contact_name']= $contact_name[$value['contact_id']];
				// ),'contacts');
			// $view->loop('contacts','item');
		}else
		{
			$contacts= '';
			$db->query("SELECT * FROM customer_contact_activity_contacts WHERE activity_id='".$value['customer_contact_activity_id']."' AND action_type='0'");
			while($db->move_next())
			{
				if(!array_key_exists($db->f('contact_id'), $contact_name))
				{
					$contact_name[$db->f('contact_id')] = get_contact_first_and_name($db->f('contact_id'));
				}
				$contacts = $contact_name[$db->f('contact_id')];
				// $view->assign(array(
					// 'assign_to'	=> $db->f('contact_id'),
					$assign_to_str	= $db->f('contact_id');
				// ),'item');
				$contacts_array[]['contact_name']= $contacts;
	  //     $view->assign(array(
			// 		'contact_name'=> $contacts,
			// 	),'contacts');

			// $view->loop('contacts','item');
			}
			
		}
		
		if(!$value['contact_activity_type'])
		{
			if($value['quote_id'])
			{
				$customer = $q[$value['quote_id']]['customer'];
				if(!in_array($q[$value['quote_id']]['customer_id'], $customers_id) && $q[$value['quote_id']]['customer_id']!='0')
				{
					array_push($customers_id, $q[$value['quote_id']]['customer_id']);
				}
			}else
			{
				$customer = $q[$value['field_value']]['customer'];
				if(!in_array($q[$value['field_value']]['customer_id'], $customers_id) && $q[$value['field_value']]['customer_id']!='0')
				{
					array_push($customers_id, $q[$value['field_value']]['customer_id']);
				}
			}
			$events['7'] = 'quotes';
		}else
		{
			if($value['customer_id'])
			{
				$customer = get_customer_name($value['customer_id']);
				if(!in_array($value['customer_id'], $customers_id))
				{
					array_push($customers_id, $value['customer_id']);
				}
			}
			$events[$value['contact_activity_type']]=$action;
		}
		
		$user_assigned = $db->query("SELECT logging.user_id, logging.to_user_id, logging.due_date FROM logging_tracked
			INNER JOIN logging ON logging_tracked.log_id=logging.log_id WHERE activity_id='".$value['customer_contact_activity_id']."' ");
		$date = $user_assigned->f('due_date');
		
		if(is_null($date)){
			$date = $value['date'];
		}
		$day = date('d-m-Y',$date);

		$value['date'] = $value['date']+$_SESSION['user_timezone_offset'];
	
		$item_line = array(
		    'contacts'				=> $contacts_array, 
		    'assign_to'				=> $assign_to_str,
			// "side"				=> $side,
			'i'						=> $value['message'],
			'action'				=> $action,
			'message'				=> $message,

			// 'long_message'		=> nl2br($long_message),
			// 'short_message'		=> $short_message,
			// 'show_show_more'		=> strlen($long_message) > 20 ? true : false,

			'time'					=> date('H:i',$date),
			'time2'					=> date('G:i',$date),
			'day_tmpstmp'			=> $date,
			'day'					=> $day,
			'type_of_call'			=> $type_of_call,
			'customer'				=> $customer,
			'show_day'				=> in_array($day, $days) ? false : true,
			'commented'				=> $value['log_comment'] ? 'commented' : '',
			'add_note'				=> $value['log_comment'] ? gm('Edit note') : gm('Add Note'),
			'hide_note_if_exist'	=> $value['log_comment'] ? 'hide' : '',
			'comments'				=> $value['log_comment'],
			'is_comments'			=> $value['log_comment'] ? '': 'hide',
			'by'					=> $name,
			'name'					=> $name,
			// 'contact_name'		=> get_contact_name($value['contact_id']),
			//'assign_to'			=> $value['contact_id'],
			'log_id'				=> $value['customer_contact_activity_id'],
			'stage'					=> $stage,
			'source'				=> $source,
			'type'					=> $type,
			'new_event'				=> $new_event,
			'activity_status'		=> $activity_status,
			'color'					=> $color,
			'to_user'				=> $assigned,
			// 'assigned'			=> $assigned,
			'to'					=> $assigned ? true : false,
			'reminder_tmpstmp'		=> $value['reminder_date'],
			'reminder_time'			=> date('G:i',$value['reminder_date']),
			'reminder_date' 		=> date(ACCOUNT_DATE_FORMAT,$value['reminder_date']).', '.date('H:i',$value['reminder_date']),
			'is_reminder_date' 		=> $value['reminder_date'] > 0 ? true : false,
			'not_task'				=> $value['contact_activity_type']  < 6 ? true : false,
			'log_comment_date' 		=> date(ACCOUNT_DATE_FORMAT,$value['log_comment_date']).', '.date('H:i',$value['log_comment_date']),
			'is_log_comment_date' 	=> $value['log_comment_date'] ? true : false,
			'log_code'				=> $log_code ? $log_code : '',
			'reminder_assign'		=> $value['reminder_date'] > 0 ?true : false,
			'assigned_to_me'		=> ($user_assigned->f('user_id') == $_SESSION['u_id'] || $user_assigned->f('to_user_id')==$_SESSION['u_id'] || $_SESSION['group']=='admin') ? true : false,
			'allowed_delete'		=> ($user_assigned->f('user_id') == $_SESSION['u_id'] ||  $_SESSION['group']=='admin') ? true : false,
			// 'day_hours_time'			=> build_day_hours(date('H:i',$date)),
			'type_of_call_dd'		=> build_simple_dropdown(array(0=>gm('outgoing'),1=>gm('incoming')),$type_of_call_id),
			'type_of_call_id'		=> $type_of_call_id,
			// 'reminder_date_day_hours_time'			=> build_day_hours(date('H:i',$value['reminder_date'])),
			'reminder_date_day'		=> date(ACCOUNT_DATE_FORMAT,$value['reminder_date']),
			'status_change'			=> $status_change,

		);
		$result['item'][] = $item_line;

		// $view->loop('item');
		if(!in_array($day, $days)){
			array_push($days, $day);
		}
		$w++;
	}
	// console::log($days);
	ksort($events);
	$in['ev_type']	= '<option value=0>'.gm('Select type').'</option>'.build_simple_dropdown($events,$selected);
	// $in['customer_dd'] = '<option value=0>'.gm('Select type').'</option>'.build_customers_dd($customers_id);

	if($status_values){
		$status=$db->field("SELECT finished FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
	}
	if($in['filter_type'] == 'customer'){
		$customer_data=$db->query("SELECT customers.name,customers.country_name,customers.city_name,addresses_coord.location_lat,addresses_coord.location_lng FROM customers 
					LEFT JOIN addresses_coord ON customers.customer_id=addresses_coord.customer_id
					WHERE customers.customer_id='".$in['customer_id']."' AND customers.active=1 ORDER BY customers.name ");

		$result['location_meet'] = $customer_data->f('city_name').','.$customer_data->f('country_name');
		$result['coord_lat'] = $customer_data->f('location_lat');
		$result['coord_lang'] = $customer_data->f('location_lng');
	}





	$result['log_id'] = $status_values ? $status_values->f('log_id') : '';
	$result['scheduled_selected'] = $status == '0' ? 'selected' : '';
	$result['finished_selected'] = $status == '1' ? 'selected' : '';
	$result['is_data'] = $w > 0 ? true : false;
	$result['user_id'] = $in['user_id'];
	$result2 = array('activities' => $result); 
	return $result2;

}

function get_deals($in)
{
	$db = new sqldb();
	$q = strtolower($in["term"]);

	$filter = '';

	if($q){
	  $filter .=" AND subject LIKE '".$q."%'";
	}
	$deals=$db->query("SELECT * FROM tblopportunity 
			WHERE buyer_id='".$in['customer_id']."' ".$filter." ORDER BY subject ASC");
	$items = array();
	while($deals->next()){
		array_push( $items, array('id'=>$deals->f('opportunity_id'),'value'=>stripslashes($deals->f('subject'))));
	}

	return $items;
}
function get_third_customers($in)
{
	$db=new sqldb();
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
	$q = strtolower($in["term"]);
				
	$filter =" is_admin='0' AND customers.active=1 ";
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
	}
	if($in['third_party_id']){
		$filter .=" AND customers.customer_id='".$in['third_party_id']."'";
	}

	$admin_licence = $db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}

	$cust = $db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type
		FROM customers
		
		WHERE $filter
		GROUP BY customers.customer_id			
		ORDER BY name
		LIMIT 9")->getAll();

	$items = array();
	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
		if($value['type']==0){
			$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
		}elseif($value['type']==1){
			$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
		}else{
			$symbol ='';
		}

		$address = $db->query("SELECT zip,city,address FROM customer_addresses
									WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");

		$items[]=array(
			"id"					=> $value['cust_id'],
			'symbol'				=> $symbol,
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id" 			=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
			'country'				=> $value['country_name'] ? $value['country_name'] : '',
			'zip'					=> $address->f('zip') ? $address->f('zip') : '',
			'city'					=> $address->f('city') ? $address->f('city') : '',
			"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
			"right"					=> $value['acc_manager_name']
		);
	}
	array_unshift($items,
		array(
			"id"					=> '0',
			'symbol'				=> '',
			"label"					=> gm('Not Activated'),
			"value" 				=> gm('Not Activated'),
			"top"	 				=> gm('Not Activated'),
			"ref" 					=> '',
			"currency_id"			=> '',
			"lang_id" 				=> '',
			"identity_id" 			=> '',
			'contact_name'			=> '',
			'country'				=> '',
			'zip'					=> '',
			'city'					=> '',
			"bottom"				=> '',
			"right"					=> ''
		)
	);
	
	return $items;
}

?>