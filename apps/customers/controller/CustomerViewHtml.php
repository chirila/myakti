<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$view = new at(ark::$viewpath.$in['filename'].'.html');
$i=0;
$def=$db->query("SELECT * FROM customise_field WHERE controller='company-customer'  order by default_value desc,sort_order ASC ");

$customiseFieldsAll = $def->getAll();

$customiseExtraFields = $db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY sort_order ASC ")->getALl();

$result =  array_merge($customiseFieldsAll, $customiseExtraFields);

//Sort all fields
usort($result, "myCustomSort");

foreach ($result as  $res) {
	
	$loop_name = $res['default_value'] == 1 ? 'default_show' : 'just_show';

	if(!empty($res['controller'])){
		$view->assign(array(
			$res['field']=> $res['value'] == 1 ? true : false,
			'itteration'	=> $i,
			'is_custom_field'	=> 'false',
		),$loop_name);
		if($res['field']=='activity' ||$res['field']=='lead_source' || $res['field']=='is_supplier' || $res['field']=='is_customer' || $res['field']=='sales_rep' ||$res['field']=='language' || $res['field']=='sector' || $res['field']=='legal_type' || $res['field'] == 'various1' || $res['field']=='various2'){
			$var = $res['field'];
			$val = $var.'_name';
			$view->assign(array(
				$var.'_text'	=> $$var,
				$var.'_name' 	=> $$val,
			),$loop_name);

		}
		$i++;
		$view->loop($loop_name);	
	} else {
		$view->assign(array(
			'field_id'			=> $res['field_id'],	                    
			'label'				=> $res['label'],				
			'is_custom_field'	=> 'true',
		),$loop_name);
		$view->loop($loop_name);
	}
}



// while ($def->next()) {

// 	$loop_name = $def->f('default_value') == 1 ? 'default_show' : 'just_show';
// 	$view->assign(array(
// 		$def->f('field')=> $def->f('value') == 1 ? true : false,
// 		'itteration'	=> $i,
// 	),$loop_name);
// 	if($def->f('field')=='activity' ||$def->f('field')=='lead_source' || $def->f('field')=='is_supplier' || $def->f('field')=='is_customer' || $def->f('field')=='sales_rep' ||$def->f('field')=='language' || $def->f('field')=='sector' || $def->f('field')=='legal_type' || $def->f('field') == 'various1' || $def->f('field')=='various2'){
// 		$var = $def->f('field');
// 		$val = $var.'_name';
// 		$view->assign(array(
// 			$var.'_text'	=> $$var,
// 			$var.'_name' 	=> $$val,
// 		),$loop_name);

// 	}
// 	$i++;
// 	$view->loop($loop_name);
// }

// $all = $db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY sort_order ASC ")->getALl();

// $loop = 'extra_field';	
// foreach ($all as $key => $value) {
// 	$view->assign(array(
// 		'field_id'			=> $value['field_id'],	                    
// 		'label'				=> $value['label'],
// 		'is_custom'			=> 'some value',
// 	),$loop);
// 	$view->loop($loop);
// }

function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

?>