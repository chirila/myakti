<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	if($in['return_data']){
    		return $fname($in);
    	}else{
    		$list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
			$list->next();
    		json_out($fname($in, $list));
    	}
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

$list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
$list->next();

$output = array();

$output['list_name'] = $list->f('name');
$output['list_type'] = $list->f('list_type');
$output['list_for'] = $list->f('list_for');
$in['filter'] = unserialize($list->f('filter'));

$output['search']=array(
	'filter' => $in['filter'],
	'search' => $in['search'],
	'do' => 'customers-smartListEdit',
	'xget' => 'smartListContacts',
	'offset' => '1',
	'contact_list_id' => $in['contact_list_id'],
);
$output['filters']=array();

$output['do_next'] = 'customers-smartListEdit-customers-list_update';

if($output['list_for'] == 1){
	$f = get_customersFilters($in);
	foreach ($f as $key => $value) {
		$output[$key] = $value;
	}
}else{
	$f = get_contactsFilters($in);
	foreach ($f as $key => $value) {
		$output[$key] = $value;
	}
}

$items = get_smartListContacts($in,$list);
foreach ($items as $key => $value) {
	$output[$key] = $value;
}

json_out($output);

function get_smartListContacts($in,$list){
	
	$db=new sqldb();

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$result = array('list'=>array());
	$l_r = ROW_PER_PAGE;
	$type = $list->f('list_type');
	$list_for = $list->f('list_for');

	if(!$list_for){
		$order_by = " ORDER BY firstname ";
	}else{
		$order_by = " ORDER BY customers.name ";
	}
	
	if($in['filter'] ){
		$filter = '';
		foreach ($in['filter'] as $field => $value){
			if($value){
				if(is_array($value)){
					$val = '';
					foreach ($value as $key){
						$val .= $key.',';
					}
					$value = $val;
				}
				$value = rtrim($value,',');
				if($value){
					if(substr(trim($field), 0, 13)=='custom_dd_id_'){

						$filter.= " AND ( ";

						$v = explode(',', $value);
						foreach ($v as $k => $v2) {

							$filter .= "customers.customer_id IN (SELECT customers.customer_id FROM customers
												LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	 											WHERE customer_field.value = '".addslashes($v2)."') OR ";

							//$filter .= "customer_field.value = '".$v2."' OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(substr(trim($field), 0, 15)=='c_custom_dd_id_'){
						
						$filter.= " AND ( ";

						$v = explode(',', $value);

						foreach ($v as $k => $v2) {

							$filter .= " customer_contacts.contact_id IN (SELECT customer_contacts.contact_id FROM customer_contacts
										 LEFT JOIN contact_field ON customer_contacts.contact_id=contact_field.customer_id
										 WHERE contact_field.value = '".addslashes($v2)."') OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(trim($field) == 'c_type'){
						$filter .= " AND ( ";
						$v = explode(',', $value);
						foreach ($v as $k => $v2) {
							$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(trim($field) == 'position'){
						$filter .= " AND ( ";
						$v = explode(',', $value);
						foreach ($v as $k => $v2) {
							$filter .= "position_n like '%".addslashes($v2)."%' OR ";
						}
						$filter = rtrim($filter,"OR ");
						$filter .= " ) ";
					}elseif(trim($field) == 'customer_contacts.email'){
						$filter.=" AND customer_contacts.email='' ";
					}elseif(trim($field) == 'customers.c_email'){
						$filter.=" AND customers.c_email='' ";
					}elseif(trim($field) == 'customers.type'){

						$filter.=" AND customers.active='1' AND customers.type='".($value-1)."' ";
					}elseif(trim($field) == 'type'){
							switch ($value) {
								    case 1:
								        $filter.=" AND customers.is_customer='1' ";
								        break;
								    case 2:
								        $filter.=" AND customers.is_supplier='1' ";
								        break;
								    case 3:
								        $filter.=" AND customers.is_customer='1' AND customers.is_supplier='1' ";
								        break;
								    case 4:
								        $filter.=" AND customers.is_customer='' AND customers.is_supplier='' ";
								        break;
								}
						
					}					
					else{
						$filter .= " AND ".$field." IN (".addslashes($value).") ";
					}
				}elseif(trim($field) == 'customers.c_email'){
					$filter.= " AND customers.c_email!='' ";
				}elseif(trim($field) == 'customer_contacts.email'){
					$filter.= " AND customer_contacts.email!='' ";
				}elseif(trim($field) == 'customers.s_emails'){
					$filter.= " AND customers.s_emails='1' ";
				}
			}
		}
	}
	if(!$filter){
		$filter = '1=1';
	}
	$filter = ltrim($filter," AND");
	if(!$list_for){
		$filter .= " AND customer_contacts.active='1' ";
	}else{
		$filter .= " AND customers.active='1' AND is_admin='0' ";
	}

	$result['filter_a']=$filter;
	if(!$list_for){//console::log('a');
	// if($type == 0){
		$k =  $db->field("SELECT COUNT( DISTINCT customer_contacts.contact_id)
	            FROM customer_contacts
	            LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
	            LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
	            LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
	            LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
	            LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
	            LEFT JOIN customer_type ON customers.c_type=customer_type.id
	            LEFT JOIN customer_sector ON customers.sector=customer_sector.id
	            LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
	            LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
	            
	            LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	            WHERE ".$filter." ");
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
	            // LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	// }
		$max_rows=$k;
		if($type){
			$k = $db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
		}

		$query_list = $db->query("SELECT customer_contacts.firstname, customer_contacts.lastname, customer_contacts.contact_id, customer_contacts.email, customers.name AS company_name,
			customer_contactsIds.customer_id AS cus_id
		            FROM customer_contacts
		            LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
		            LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
		            LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
		            LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
		            LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
		            LEFT JOIN customer_type ON customers.c_type=customer_type.id
		            LEFT JOIN customer_sector ON customers.sector=customer_sector.id
		            LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
		            LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
		            
		            LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
		            WHERE ".$filter." GROUP BY customer_contacts.contact_id
					$order_by  limit ".$offset*$l_r.", ".$l_r);
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
		            // LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	}else{//console::log('b');
		$k =  $db->field("SELECT DISTINCT COUNT( DISTINCT customers.customer_id)
				        FROM customers
				        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
				        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
				        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
				        LEFT JOIN customer_type ON customers.c_type=customer_type.id
				        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
				        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
				        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
				        
						LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
	            WHERE ".$filter."  ");
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
						// LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	// }
		$max_rows=$k;
		if($type){
			$k = $db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
		}

		$query_list = $db->query("SELECT customers.c_email AS email, customers.name AS company_name, customers.customer_id AS contact_id, customers.customer_id AS cus_id
		            FROM customers
		            LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
			        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
			        LEFT JOIN customer_type ON customers.c_type=customer_type.id
			        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
			        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
			        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
			        
					LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
		            WHERE ".$filter." GROUP BY customers.customer_id
					$order_by  limit ".$offset*$l_r.", ".$l_r);
		// LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
					// LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
	}

	while ($query_list->next()){
		if($type){
			$active = $db->query("SELECT active FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND contact_id='".$query_list->f('contact_id')."' ");
			if($active->next()){
				$ac = $active->f('active');
			}else{
				$ac = 1;
			}
		}
		$item = array(
		 	'CON_FIRSTNAME'			=> $query_list->f('firstname'),
		 	'CON_COMP'				=> $query_list->f('company_name'),
			'CON_LASTNAME'			=> $query_list->f('lastname'),
			'EMAIL'					=> $query_list->f('email'),
			'REL' 					=> $query_list->f('contact_id'),
			'customer_id' 			=> $query_list->f('cus_id'),
			// 'CONTACT_EDIT_LINK'		=> $list_for ? 'index.php?do=company-customer&customer_id='.$query_list->f('contact_id').$arguments : 'index.php?do=company-xcustomer_contact&contact_id='.$query_list->f('contact_id').$arguments,
			// 'CHECKED'				=> $in['contact_list_id'] ? (in_array($query_list->f('contact_id'),$in['contact_id']) ? 'CHECKED': ($type ? '' : 'CHECKED')) : ($type ? (in_array($query_list->f('contact_id'),$in['contact_id']) ? 'CHECKED': '') : 'CHECKED' ) ,
			'CHECKED'				=> $in['contact_list_id'] ? ($type ? ($ac == '0' ? false : true) : true) : ($type ? (in_array($query_list->f('contact_id'),$in['contact_id']) ? true: false) : true ) ,
			// 'hide_action'			=> $type ? true : false,
			// 'hide_comp'				=> $list_for ? false : true,
			// 'last'					=> $type ? '' : 'last',
		);
	    $result['list'][] = $item;
		$i++;
	}
	$result["nr_contacts"] = $k>=0? $k : $list->f('contacts');
	$result['max_rows'] = $max_rows;
	$result['hide_comp'] = $list_for ? false : true;
	$result['lr'] = $l_r;
	$result['TOTAL_SEL_C'] = $k>=0? $k : $list->f('contacts');
	return $result;
}

function get_contactsFilters($in){
/*	echo '<pre>';
	var_dump($in);exit();*/
	$db = new sqldb();
	$res = array('filters'=>array(),'appliedFilters'=>array());
	#language filter
	$i=0;
	$array = array('name'=>gm('Language'),'items'=>array(),'id'=>'targetLanguage');
	/*$language = $db->query("SELECT customer_contact_language.id, customer_contact_language.name, count(customer_contacts.language) AS total
							FROM customer_contact_language
							INNER JOIN customer_contacts ON customer_contact_language.id = customer_contacts.language
							WHERE customer_contacts.active='1'
							GROUP BY customer_contact_language.id ");*/
							$language = $db->query("SELECT pim_lang.lang_id, pim_lang.language, count(customer_contacts.contact_id) AS total
							FROM pim_lang
							INNER JOIN customer_contacts ON pim_lang.lang_id = customer_contacts.language
							WHERE customer_contacts.active='1'
							GROUP BY pim_lang.lang_id ");
	while ($language->next()) {
		$item = array(
			'id'						=> $language->f('lang_id'),
			'name'						=> $language->f('language'),
			'total'						=> $language->f('total'),
			'key'						=> 'customer_contacts.language',
			'CHECKED'					=> $in['filter']['customer_contacts.language'] && in_array($language->f('lang_id'),$in['filter']['customer_contacts.language']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['customer_contacts.language'][$language->f('lang_id')] = $language->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('customer_contacts.language', $in['filter']) && !empty($in['filter']['customer_contacts.language']) ? true : false;
	$res['filters'][] = $array;

	#company language filter
	$i=0;
	$array = array('name'=>gm('Company Language'),'items'=>array(),'id'=>'targetCompanyLanguage');
	/*$language_c = $db->query("SELECT customer_contact_language.id, customer_contact_language.name, count(customer_contacts.language) AS total
							FROM customer_contact_language
							INNER JOIN customers ON customer_contact_language.id=customers.language
							INNER JOIN customer_contacts ON customer_contacts.customer_id = customers.customer_id
							WHERE customer_contacts.active='1'
							GROUP BY customer_contact_language.id ");*/
	$language_c = $db->query("SELECT pim_lang.lang_id, pim_lang.language, count(customer_contacts.contact_id) AS total
							FROM pim_lang
							INNER JOIN customers ON pim_lang.lang_id=customers.language
							INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id
							INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
							WHERE customer_contacts.active='1'
							GROUP BY pim_lang.lang_id ");
	while ($language_c->next()) {
		$item = array(
			'id'						=> $language_c->f('lang_id'),
			'name'						=> $language_c->f('language'),
			'total'						=> $language_c->f('total'),
			'key'						=> 'customers.language',
			'CHECKED'					=> $in['filter']['customers.language'] && in_array($language_c->f('lang_id'),$in['filter']['customers.language']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['customers.language'][$language_c->f('lang_id')] = $language_c->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('customers.language', $in['filter']) && !empty($in['filter']['customers.language']) ? true : false;
	$res['filters'][] = $array;

	#country filter
	$i=0;
	$array = array('name'=>gm('Country'),'items'=>array(),'id'=>'targetCountry');
	$country = $db->query("SELECT country.country_id, country.name, count(DISTINCT customer_contact_address.contact_id) AS total FROM country
						   INNER JOIN customer_contact_address ON country.country_id=customer_contact_address.country_id
						   INNER JOIN customer_contacts ON customer_contact_address.contact_id=customer_contacts.contact_id
						   WHERE customer_contacts.active='1'
						   GROUP BY country.country_id ");
	while ($country->next()) {
		$item = array(
			'id'				=> $country->f('country_id'),
			'name'				=> $country->f('name'),
			'total'				=> $country->f('total'),
			'key'				=> 'customer_contact_address.country_id',
			'CHECKED'			=> $in['filter']['customer_contact_address.country_id'] && in_array($country->f('country_id'),$in['filter']['customer_contact_address.country_id']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['customer_contact_address.country_id'][$country->f('country_id')] = $country->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('customer_contact_address.country_id', $in['filter']) && !empty($in['filter']['customer_contact_address.country_id']) ? true : false;
	$res['filters'][] = $array;

	#company country filter
	$i=0;
	$array = array('name'=>gm('Company Country'),'items'=>array(),'id'=>'targetCompanyCountry');
	$country_c = $db->query("SELECT country.country_id, country.name, count(DISTINCT customer_contacts.contact_id) AS total FROM country
							INNER JOIN customer_addresses ON country.country_id = customer_addresses.country_id
						   INNER JOIN customer_contacts ON customer_addresses.customer_id=customer_contacts.customer_id
						   WHERE customer_contacts.active='1'
						   GROUP BY country.country_id ");
	while ($country_c->next()) {
		$item = array(
			'id'				=> $country_c->f('country_id'),
			'name'				=> $country_c->f('name'),
			'total'				=> $country_c->f('total'),
			'key'				=> 'customer_addresses.country_id',
			'CHECKED'			=> $in['filter']['customer_addresses.country_id'] && in_array($country_c->f('country_id'),$in['filter']['customer_addresses.country_id']) ? true : false,
			// 'ON'						=> $in['filter']['customer_addresses.country_id'] && in_array($country_c->f('country_id'),$in['filter']['customer_addresses.country_id']) ? 'on': '',
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['customer_addresses.country_id'][$country_c->f('country_id')] = $country_c->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('customer_addresses.country_id', $in['filter']) && !empty($in['filter']['customer_addresses.country_id']) ? true : false;
	$res['filters'][] = $array;

	#department filter
	$i=0;
	$array = array('name'=>gm('Department'),'items'=>array(),'id'=>'targetDepartment');
	$department = $db->query("SELECT customer_contact_dep.*, count(customer_contacts.contact_id) AS total 
		FROM customer_contact_dep
		INNER JOIN customer_contactsIds ON customer_contact_dep.id=customer_contactsIds.department
		INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id	
		WHERE customer_contacts.active='1'
		GROUP BY customer_contact_dep.id ");
	while ($department->next()) {
		$item = array(
			'id'						=> $department->f('id'),
			'name'						=> $department->f('name'),
			'total'						=> $department->f('total'),
			'key'						=> 'customer_contactsIds.department',
			'CHECKED'					=> $in['filter']['customer_contactsIds.department'] && in_array($department->f('id'),$in['filter']['customer_contactsIds.department']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['customer_contactsIds.department'][$department->f('id')] = $department->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('customer_contactsIds.department', $in['filter']) && !empty($in['filter']['customer_contactsIds.department']) ? true : false;
	$res['filters'][] = $array;

	#job filter
	$i=0;
	$array = array('name'=>gm('Function'),'items'=>array(),'id'=>'targetJob');
	/*$ctype = array();
	$c_type = $db->query("SELECT * FROM customer_contact_job_title");
	while ($c_type->next()) {
		$ctype[$c_type->f('name')] = array('id'=>$c_type->f('id'),'count'=>0);
	}
	$relationship_type = $db->query("SELECT contact_id, position_n
							  FROM customer_contacts
							  WHERE active = '1' AND position_n!=''
							  GROUP BY contact_id ");
	while ($relationship_type->next()) {
		$name = explode(',', $relationship_type->f('position_n'));
		foreach ($name as $key => $value) {
			if(array_key_exists(trim($value), $ctype)){
				$ctype[trim($value)]['count']++;
			}
		}
	}*/
	$ctype=$db->query("SELECT customer_contact_job_title.id,customer_contact_job_title.name,count(customer_contacts.contact_id) AS total 
		FROM customer_contact_job_title
		INNER JOIN customer_contactsIds ON customer_contact_job_title.id=customer_contactsIds.position
		INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
			WHERE customer_contacts.active='1'
			GROUP BY customer_contact_job_title.id 
		")->getAll();

	foreach ($ctype as $key => $value) {
		//if($ctype[$key]['count'] > 0 ){
			$item = array(
				'id'						=> $value['id'],
				'name'						=> $value['name'],
				'total'						=> $value['total'],
				'key'						=> 'customer_contactsIds.position',
				'CHECKED'					=> $in['filter']['customer_contactsIds.position'] && in_array($value['id'],$in['filter']['customer_contactsIds.position']) ? true : false,
			);
			$array['items'][]=$item;
			$i++;
			$filter_array['customer_contactsIds.position'][$value['id']] = $value['name'];
		//}
	}
	$array['show'] = $in['filter'] && array_key_exists('customer_contactsIds.position', $in['filter']) && !empty($in['filter']['customer_contactsIds.position']) ? true : false;
	$res['filters'][] = $array;

	#title filter
	/*$i=0;
	$array = array('name'=>gm('Title'),'items'=>array(),'id'=>'targetTitle');
	$title = $db->query("SELECT customer_contact_title.*, count(customer_contacts.title) AS total FROM customer_contact_title
							   INNER JOIN customer_contacts ON customer_contact_title.id=customer_contacts.title
							   WHERE customer_contacts.active='1'
							   GROUP BY customer_contact_title.id ");
	while ($title->next()) {
		$item = array(
			'id'						=> $title->f('id'),
			'name'						=> $title->f('name'),
			'total'						=> $title->f('total'),
			'key'						=> 'title',
			'CHECKED'					=> $in['filter']['title'] && in_array($title->f('id'),$in['filter']['title']) ? 'CHECKED': '',
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['title'][$title->f('id')] = $title->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('title', $in['filter']) ? true : false;
	$res['filters'][] = $array;*/

	#allow emailing list sum
	$array = array('name'=>gm('Allow on e-mailing lists'),'items'=>array(),'id'=>'targetAllowEmail');
	$item = array(
	     'id' => '1',
	     'name' => gm('Allow on e-mailing lists'),
	     'key' => 'customer_contactsIds.s_email',
	     'total' =>$db->field("SELECT count(DISTINCT(customer_contactsIds.contact_id)) FROM customer_contactsIds 
	     	INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
	     	WHERE customer_contactsIds.s_email='1' AND customer_contacts.active='1' "),
	     'CHECKED' => $in['filter']['customer_contactsIds.s_email'] ? true : false,
	);
	$array['items'][]=$item;
	$array['show'] = $in['filter'] && array_key_exists('customer_contactsIds.s_email', $in['filter']) && !empty($in['filter']['customer_contactsIds.s_email']) ? true : false;
	$res['filters'][] = $array;

	#legal type filter
	$i=0;
	$array = array('name'=>gm('Legal Type'),'items'=>array(),'id'=>'targetLegalType');
	$legal_type = $db->query("SELECT customer_legal_type . * , count(DISTINCT customer_contacts.contact_id ) AS total
							  FROM customer_legal_type
							  INNER JOIN customers ON customer_legal_type.id = customers.legal_type
							  INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id
							  INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
							  WHERE customer_contacts.active = '1'
							  GROUP BY customer_legal_type.id ");
	while ($legal_type->next()) {
		$item = array(
			'id'				=> $legal_type->f('id'),
			'name'				=> $legal_type->f('name'),
			'total'				=> $legal_type->f('total'),
			'key'				=> 'legal_type',
			'CHECKED'			=> $in['filter']['legal_type'] && in_array($legal_type->f('id'),$in['filter']['legal_type']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['legal_type'][$legal_type->f('id')] = $legal_type->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('legal_type', $in['filter']) && !empty($in['filter']['legal_type']) ? true : false;
	$res['filters'][] = $array;

	#relationship type filter
	$i=0;
	$array = array('name'=>gm('Relationship Type'),'items'=>array(),'id'=>'targetRelationship');
	$ctype = array();
	$c_type = $db->query("SELECT * FROM customer_type");
	while ($c_type->next()) {
		$ctype[$c_type->f('name')] = array('id'=>$c_type->f('id'),'count'=>0);
	}

	$relationship_type = $db->query("SELECT customer_contacts.contact_id, customers.c_type_name
							  FROM customer_contacts
							  INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							  INNER JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							  WHERE customer_contacts.active = '1' AND c_type_name!=''
							  GROUP BY customer_contacts.contact_id ");
	while ($relationship_type->next()) {
		$name = explode(',', $relationship_type->f('c_type_name'));
		foreach ($name as $key => $value) {
			if(array_key_exists(trim($value), $ctype)){
				$ctype[trim($value)]['count']++;
			}
		}
	}
	foreach ($ctype as $key => $value) {
		if($value['count'] > 0 ){
			$item = array(
				'id'				=> $value['id'],
				'name'				=> $key,
				'total'				=> $value['count'],
				'key'				=> 'c_type',
				'CHECKED'			=> $in['filter']['c_type'] && in_array($value['id'],$in['filter']['c_type']) ? true : false,
			);
			$array['items'][]=$item;
			$i++;
			$filter_array['c_type'][$value['id']] = $key;
		}
	}
	$array['show'] = $in['filter'] && array_key_exists('c_type', $in['filter']) && !empty($in['filter']['c_type']) ? true : false;
	$res['filters'][] = $array;

	#sector filter
	$i=0;
	$array = array('name'=>gm('Activity Sector'),'items'=>array(),'id'=>'targetSector');
	$sector = $db->query("SELECT customer_sector . * , count(DISTINCT customer_contacts.contact_id ) AS total
							  FROM customer_sector
							  INNER JOIN customers ON customer_sector.id = customers.sector
							  INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id
							  INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
							  WHERE customer_contacts.active = '1'
							  GROUP BY customer_sector.id ");
	while ($sector->next()) {
		$item = array(
			'id'						=> $sector->f('id'),
			'name'						=> $sector->f('name'),
			'total'						=> $sector->f('total'),
			'key'						=> 'sector',
			'CHECKED'					=> $in['filter']['sector'] && in_array($sector->f('id'),$in['filter']['sector']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['sector'][$sector->f('id')] = $sector->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('sector', $in['filter']) && !empty($in['filter']['sector']) ? true : false;
	$res['filters'][] = $array;

	#price categ filter
	$i=0;
	$array = array('name'=>gm('Price Category'),'items'=>array(),'id'=>'targetPriceCateg');
	$price_categ = $db->query("SELECT pim_article_price_category.category_id,pim_article_price_category.name , count(DISTINCT customer_contacts.contact_id ) AS total
							  FROM pim_article_price_category
							  INNER JOIN customers ON pim_article_price_category.category_id = customers.cat_id
							  INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id
							  INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
							  WHERE customer_contacts.active = '1'
							  GROUP BY pim_article_price_category.category_id ");
	while ($price_categ->next()) {
		$item = array(
			'id'						=> $price_categ->f('category_id'),
			'name'						=> $price_categ->f('name'),
			'total'						=> $price_categ->f('total'),
			'key'						=> 'cat_id',
			'CHECKED'					=> $in['filter']['cat_id'] && in_array($price_categ->f('category_id'),$in['filter']['cat_id']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['cat_id'][$price_categ->f('category_id')] = $price_categ->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('cat_id', $in['filter']) && !empty($in['filter']['cat_id']) ? true : false;
	$res['filters'][] = $array;

	#gender filter
	$i=0;
	$array = array('name'=>gm('Gender'),'items'=>array(),'id'=>'targetGender');
	$gender = $db->query("SELECT sex , count(DISTINCT customer_contacts.contact_id ) AS total
							  FROM customer_contacts
							  WHERE customer_contacts.active = '1' AND sex!='0'
							  GROUP BY sex ");
	while ($gender->next()) {
		$item = array(
			'id'						=> $gender->f('sex'),
			'name'						=> $gender->f('sex') == 1 ? gm('Male') : gm('Female'),
			'total'						=> $gender->f('total'),
			'key'						=> 'sex',
			'CHECKED'					=> $in['filter']['sex'] && in_array($gender->f('sex'),$in['filter']['sex']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['sex'][$gender->f('sex')] = $gender->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('sex', $in['filter']) && !empty($in['filter']['sex']) ? true : false;
	$res['filters'][] = $array;

	#account manager filter
	$i=0;
	$array = array('name'=>gm('Account Manager'),'items'=>array(),'id'=>'targetAccountManager');
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
	$acc_manag_arr = array();
	//$acc_manags = $db_users->query("SELECT CONCAT_WS(' ',first_name, last_name) AS acc_manager_name, user_id FROM users WHERE database_name = '".DATABASE_NAME."' ");
	$acc_manags = $db_users->query("SELECT CONCAT_WS(' ',first_name, last_name) AS acc_manager_name, user_id FROM users WHERE database_name = :d ",['d'=>DATABASE_NAME]);
	while ($acc_manags->next()) {
		$acc_manag_arr[$acc_manags->f('user_id')] = array('user_name'=>utf8_encode($acc_manags->f('acc_manager_name')),'count'=>0);
	}

	$acc_manager = $db->query("SELECT customer_contacts.contact_id, customers.acc_manager_name, customers.user_id
								FROM customer_contacts
								INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
								INNER JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
								WHERE customer_contacts.active = '1'
								AND customers.acc_manager_name != ''
								GROUP BY customer_contacts.contact_id ");
	while ($acc_manager->next()) {
		$name = explode(',', $acc_manager->f('user_id'));
		foreach ($name as $key => $value) {
			if(array_key_exists(trim($value), $acc_manag_arr)){
				$acc_manag_arr[trim($value)]['count']++;
			}
		}
	}

	foreach ($acc_manag_arr as $key => $value) {
		if($value['count'] > 0 ){
			$item = array(
				'id'						=> strval($key),
				'name'						=> $value['user_name'],
				'total'						=> $value['count'],
				'key'						=> 'customers.user_id',
				'CHECKED'					=> $in['filter']['customers.user_id'] && in_array($key,$in['filter']['customers.user_id']) ? true : false,
			);
			$array['items'][]=$item;
			$i++;
			$filter_array['customers.user_id'][$key] = $value['user_name'];
		}
	}
	$array['show'] = $in['filter'] && array_key_exists('customers.user_id', $in['filter']) && !empty($in['filter']['customers.user_id']) ? true : false;
	$res['filters'][] = $array;

	#lead_source
	$i=0;
	$array = array('name'=>gm('Lead Source'),'items'=>array(),'id'=>'targetLeadSource');
	$lead_source = $db->query("SELECT customer_lead_source.*, count(DISTINCT customer_contacts.contact_id ) AS total
							  FROM customer_lead_source
							  INNER JOIN customers ON customer_lead_source.id = customers.lead_source
							  INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id
							  INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
							  WHERE customer_contacts.active = '1'
							  GROUP BY customer_lead_source.id ");
	while ($lead_source->next()) {
		$item = array(
			'id'						=> $lead_source->f('id'),
			'name'						=> $lead_source->f('name'),
			'total'						=> $lead_source->f('total'),
			'key'						=> 'lead_source',
			'CHECKED'					=> $in['filter']['lead_source'] && in_array($lead_source->f('id'),$in['filter']['lead_source']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['lead_source'][$lead_source->f('id')] = $lead_source->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('lead_source', $in['filter']) && !empty($in['filter']['lead_source']) ? true : false;
	$res['filters'][] = $array;

	#various1
	/*$various1 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 1' ");

	$array = array('name'=>($various1 ? $various1 : gm('Various 1')),'items'=>array(),'id'=>'targetVarious1');
	$i=0;
	$various1 = $db->query("SELECT customer_various1.*, count(DISTINCT customer_contacts.contact_id ) AS total
							  FROM customer_various1
							  INNER JOIN customers ON customer_various1.id = customers.various1
							  INNER JOIN customer_contacts ON customers.customer_id = customer_contacts.customer_id
							  WHERE customer_contacts.active = '1'
							  GROUP BY customer_various1.id ");
	while ($various1->next()) {
		$item = array(
			'id'						=> $various1->f('id'),
			'name'						=> $various1->f('name'),
			'total'						=> $various1->f('total'),
			'key'						=> 'various1',
			'CHECKED'					=> $in['filter']['various1'] && in_array($various1->f('id'),$in['filter']['various1']) ? 'CHECKED': '',
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['various1'][$various1->f('id')] = $various1->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('various1', $in['filter']) ? true : false;
	$res['filters'][] = $array;

	#various2
	$i=0;
	$various2 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 2' ");
	$array = array('name'=>($various2 ? $various2 : gm('Various 2')),'items'=>array(),'id'=>'targetVarious2');
	$various2 = $db->query("SELECT customer_various2.*, count(DISTINCT customer_contacts.contact_id ) AS total
							  FROM customer_various2
							  INNER JOIN customers ON customer_various2.id = customers.various2
							  INNER JOIN customer_contacts ON customers.customer_id = customer_contacts.customer_id
							  WHERE customer_contacts.active = '1'
							  GROUP BY customer_various2.id ");
	while ($various2->next()) {
		$item = array(
			'id'						=> $various2->f('id'),
			'name'						=> $various2->f('name'),
			'total'						=> $various2->f('total'),
			'key'						=> 'various2',
			'CHECKED'					=> $in['filter']['various2'] && in_array($various2->f('id'),$in['filter']['various2']) ? 'CHECKED': '',
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['various2'][$various2->f('id')] = $various2->f('name');
	}
	$array['show'] = $in['filter'] && array_key_exists('various2', $in['filter']) ? true : false;
	$res['filters'][] = $array;*/

	#has email
	$i=0;
	$array = array('name'=>gm('Has email address'),'items'=>array(),'id'=>'targetHasEmail');
	$hasemail = $db->query("SELECT COUNT(customer_contacts.contact_id) AS total	FROM customer_contacts WHERE customer_contacts.active = '1' AND email!=''
							UNION
							SELECT COUNT(customer_contacts.contact_id) AS total	FROM customer_contacts WHERE customer_contacts.active = '1' AND email=''");
	while ($hasemail->next()) {
		$id = $i;
		$item = array(
			'id'	 					=> (string)$id,
			'name'					 	=> $i == 0 ? gm('Email') : gm('No').' '.gm('Email'),
			'total' 					=> $hasemail->f('total'),
			'key'						=> 'customer_contacts.email',
			'CHECKED'					=> $in['filter']['customer_contacts.email'] && in_array($id,$in['filter']['customer_contacts.email']) ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['customer_contacts.email'][$id] = ($i == 0 ? gm('Email') : gm('No').' '.gm('Email'));
	}
	$array['show'] = $in['filter'] && array_key_exists('customer_contacts.email', $in['filter']) && !empty($in['filter']['customer_contacts.email']) ? true : false;
	$res['filters'][] = $array;

	#nature - company or individual
	$i=0;
	$array = array('name'=>gm('Nature'),'items'=>array(),'id'=>'targetNature');

	$nature = $db->query("SELECT customers.type, count(DISTINCT customer_contacts.contact_id) AS total
							FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
  						    WHERE customers.active=1 AND customer_contacts.active=1 
							  GROUP BY customers.type ");
	while ($nature->next()) {
		$z_text = $nature->f('type') == 0 ? gm('Company') : gm('Individual');
		$checked=0;
		if($in['filter']['customers.type']){
			foreach ($in['filter']['customers.type'] as $k2 =>$val2) {

		        if ((int)($nature->f('type')) == (int)($val2-1)) {
		            $checked=1;
		        }
		    }
		}
			
		$item = array(
			'id'						=> (string)($nature->f('type')+1),
			'name'						=> $z_text,
			'total'						=> $nature->f('total'),
			'key'						=> 'customers.type',
			'CHECKED'					=> $checked ? true : false,
		);
		$array['items'][]=$item;
		$i++;
		$filter_array['customers.type'][$nature->f('type')+1] = $z_text;
	}
	
	$array['show'] = $in['filter'] && array_key_exists('customers.type', $in['filter']) && !empty($in['filter']['customers.type']) ? true : false;
	$res['filters'][] = $array;


	#type //is_suplier, is_customer
	$i=0;
	$array = array('name'=>gm('Type'),'items'=>array(),'id'=>'targetType');

	$queries= array('1' =>"SELECT count(DISTINCT customer_contacts.contact_id) AS total FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id 
							WHERE customer_contacts.active='1' AND customers.is_customer='1' ",
					'2' =>"SELECT count(DISTINCT customer_contacts.contact_id) AS total FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
							 WHERE customer_contacts.active='1' AND customers.is_supplier='1' ",
					'3' =>"SELECT count(DISTINCT customer_contacts.contact_id) AS total FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
							 WHERE customer_contacts.active='1' AND customers.is_customer='1' AND  customers.is_supplier='1'",
					'4' =>"SELECT count(DISTINCT customer_contacts.contact_id) AS total FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
							 WHERE customer_contacts.active='1' AND customers.is_supplier='' AND customers.is_customer='' ",);
	foreach($queries as $key => $value){
		switch ($key) {
			    case 1:
			        $z_text = gm('Customer');
			        break;
			    case 2:
			        $z_text = gm('Supplier');
			        break;
			    case 3:
			        $z_text = gm('Customer & Supplier');
			        break;
			    case 4:
			        $z_text = gm('Undefined');
			        break;
			}
		$type = $db->query($value);
		while ($type->next()) {
			$item = array(
				'id'						=> (string)$key,
				'name'						=> $z_text,
				'total'						=> $type->f('total'),
				'key'						=> 'type',
				'CHECKED'					=> $in['filter']['type'] && in_array($key,$in['filter']['type']) ? true : false,
			);
			$array['items'][]=$item;
			$i++;
			$filter_array['type'][$key] = $z_text;
		}
	}
		
	$array['show'] = $in['filter'] && array_key_exists('type', $in['filter']) && !empty($in['filter']['type']) ? true : false;
	$res['filters'][] = $array;
	//end type

	//Custom fields for companies / de aici
	$custom_field_dd_ids = $db->query("SELECT field_id,label FROM customer_fields WHERE is_dd = '1' ");
	$id_500 = 500;

	while($custom_field_dd_ids->next()){ //console::log($custom_field_dd_ids->f('field_id'));
		$count_customers = $db->field("SELECT COUNT(DISTINCT customer_contacts.contact_id ) AS total
										FROM customer_field
										INNER JOIN customer_contactsIds ON customer_field.customer_id=customer_contactsIds.customer_id
										INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
										WHERE customer_field.field_id = '".$custom_field_dd_ids->f('field_id')."'
										AND customer_contacts.active='1'
										AND customer_field.value NOT
										IN ('Select type','0','Sélectionner le type','undefined') ");
		$custom_title = $custom_field_dd_ids->f('label');
		$custom_dd_own_id = $db->query("SELECT DISTINCT value FROM customer_field
			INNER JOIN customer_contactsIds ON customer_field.customer_id=customer_contactsIds.customer_id
			INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
			WHERE field_id= '".$custom_field_dd_ids->f('field_id')."'
			AND customer_contacts.active='1'
			AND value NOT IN ('Select type','0','Sélectionner le type','undefined') ");
		$array = array('name'=>$custom_title,'items'=>array(),'id'=>'target'.$custom_title);
		while($custom_dd_own_id->next()){
			$count_vals = $db->field("SELECT COUNT( customer_field.value ) FROM  customer_field
			INNER JOIN customer_contactsIds ON customer_field.customer_id=customer_contactsIds.customer_id
			INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
			WHERE field_id= '".$custom_field_dd_ids->f('field_id')."' AND customer_contacts.active='1' AND value = '".$custom_dd_own_id->f('value')."' ");
			$value_name = $db->field("SELECT name FROM customer_extrafield_dd WHERE extra_field_id = '".$custom_field_dd_ids->f('field_id')."' AND id = '".$custom_dd_own_id->f('value')."' ");
			$item_mai_mic = array(
				'id'				=> $custom_dd_own_id->f('value'),
				'name'				=> $value_name,
				'total'				=> $count_vals,
				'key'				=> 'custom_dd_id_'.$custom_field_dd_ids->f('field_id'),
				'CHECKED'			=> $in['filter']['custom_dd_id_'.$custom_field_dd_ids->f('field_id')] && in_array($custom_dd_own_id->f('value'),$in['filter']['custom_dd_id_'.$custom_field_dd_ids->f('field_id')]) ? true : false,
				'field'				=> 'custom_dd_id_'.$custom_field_dd_ids->f('field_id'),
				'ids'				=> $id_500,
				);
			$array['items'][]=$item_mai_mic;
			$filter_array['custom_dd_id_'.$custom_field_dd_ids->f('field_id')][$custom_dd_own_id->f('value')] = $value_name;
			$array['show'] = $in['filter'] && array_key_exists('custom_dd_id_'.$custom_field_dd_ids->f('field_id'), $in['filter']) && !empty($in['filter']['custom_dd_id_'.$custom_field_dd_ids->f('field_id')]) ? true : false;
			$id_500++;
		}
		/*$item = array(
			'title'							=> $custom_title,
			'field'							=> 'custom_dd_id_'.$custom_field_dd_ids->f('field_id'),
			'HIDE_LANGUAGE12'  				=> ($in['filter'] && array_key_exists('custom_dd_id_'.$custom_field_dd_ids->f('field_id'), $in['filter'])) ? true : false,
			'COLLAPS_LANGUAGE12' 			=> $in['filter'] && array_key_exists('custom_dd_id_'.$custom_field_dd_ids->f('field_id'), $in['filter']) ? 'collaps' : '',
			'is_data'						=> $count_customers > 0 ? true : false,
		);*/
		$res['filters'][] = $array;
	}
	//pana aici


	//Custom fields for contacts / de aici
	$custom_field_dd_ids2 = $db->query("SELECT field_id,label FROM contact_fields WHERE is_dd = '1' ");
	$id_500 = 500;

	while($custom_field_dd_ids2->next()){
		$count_contacts = $db->field("SELECT COUNT(DISTINCT customers.customer_id ) AS total
										FROM contact_field
										INNER JOIN customers ON customers.customer_id = contact_field.customer_id
										WHERE contact_field.field_id = '".$custom_field_dd_ids2->f('field_id')."'
										AND customers.active='1'
										AND contact_field.value NOT
										IN ('Select type','0','Sélectionner le type','undefined') ");
		$custom_title2 = $custom_field_dd_ids2->f('label');
		$custom_dd_own_id2 = $db->query("SELECT DISTINCT value FROM contact_field
			INNER JOIN customers
			ON customers.customer_id = contact_field.customer_id
			WHERE field_id= '".$custom_field_dd_ids2->f('field_id')."'
			AND customers.active='1'
			AND value NOT IN ('Select type','0','Sélectionner le type','undefined') ");
		$array = array('name'=>$custom_title2,'items'=>array(),'id'=>'target'.$custom_title2);
		while($custom_dd_own_id2->next()){
			$count_vals2 = $db->field("SELECT COUNT( contact_field.value ) FROM  contact_field
										INNER JOIN customers
										ON customers.customer_id = contact_field.customer_id
										WHERE field_id= '".$custom_field_dd_ids2->f('field_id')."'
										AND customers.active='1'
										AND value = '".$custom_dd_own_id2->f('value')."' ");
			$value_name2 = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id = '".$custom_field_dd_ids2->f('field_id')."' AND id = '".$custom_dd_own_id2->f('value')."' ");
			$item_mai_mic2 = array(
				'id'				=> $custom_dd_own_id2->f('value'),
				'name'				=> $value_name2,
				'total'				=> $count_vals2,
				'key'				=> 'c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id'),
				'CHECKED'			=> $in['filter']['c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id')] && in_array($custom_dd_own_id2->f('value'),$in['filter']['c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id')]) ? true : false,
				'field'				=> 'c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id'),
				'ids'				=> $id_500,
				);
			$array['items'][]=$item_mai_mic2;
			$filter_array['c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id')][$custom_dd_own_id2->f('value')] = $value_name2;
			$array['show'] = $in['filter'] && array_key_exists('c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id'), $in['filter']) && !empty($in['filter']['c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id')]) ? true : false;
			$id_500++;
		}
		/*$item = array(
			'title'							=> $custom_title2,
			'field'							=> 'c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id'),
			'HIDE_LANGUAGE12'  				=> ($in['filter'] && array_key_exists('c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id'), $in['filter'])) ? true : false,
			'COLLAPS_LANGUAGE12' 			=> $in['filter'] && array_key_exists('c_custom_dd_id_'.$custom_field_dd_ids2->f('field_id'), $in['filter']) ? 'collaps' : '',
			'is_data'						=> $count_contacts > 0 ? true : false,
		);*/
		$res['filters'][] = $array;
	}
	//pana aici

		
	if($in['filter']){
		foreach ($in['filter'] as $key=>$value){
			if($key){
				if($key == 'c_type'){
					foreach ($value as $keys) {
						$res['appliedFilters'][$key][]=$keys;
					}
				}else{
					if(is_array($value)){
						foreach ($value as $keys){
							$res['appliedFilters'][$key][]=$filter_array[$key][$keys];
						}
					}else{
						$res['appliedFilters'][$key][]=$filter_array[$key][$value];
					}
				}
			}
		}
	}
	return $res;
}

function get_customersFilters($in){
	global $database_config;
	$db = new sqldb();
	$res = array('filters'=>array(),'appliedFilters'=>array());
	$filters = array(
		"Language"			=>array('query'	=> "SELECT pim_lang.lang_id AS id, pim_lang.language AS name, count(customers.customer_id) AS total
												FROM customers
												INNER JOIN pim_lang ON pim_lang.lang_id=customers.language
												WHERE customers.active='1'  AND is_admin='0'
												GROUP BY pim_lang.lang_id ",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customers.language'),
		"Country"			=>array('query' => "SELECT country.country_id, country.name, count(DISTINCT customers.customer_id) AS total FROM country
												INNER JOIN customer_addresses ON country.country_id = customer_addresses.country_id
								   				INNER JOIN customers ON customer_addresses.customer_id=customers.customer_id
								   				WHERE customers.active='1' AND is_admin='0'
								   				GROUP BY country.country_id ",
									'id'	=> 'country_id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customer_addresses.country_id'),
		"Legal Type"		=>array('query'	=> "SELECT customer_legal_type.* , count(DISTINCT customers.customer_id ) AS total
												FROM customer_legal_type
												INNER JOIN customers ON customer_legal_type.id = customers.legal_type
												WHERE customers.active = '1' AND is_admin='0'
												GROUP BY customer_legal_type.id ",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customers.legal_type'),
		"Relationship Type"	=>array('query'	=> "SELECT customers.customer_id, customers.c_type_name
												FROM customers
												WHERE customers.active = '1' AND c_type_name!='' AND is_admin='0'
												GROUP BY customers.customer_id ",
									'squery'=> "SELECT * FROM customer_type",
									'id'	=> 'id',
									'sid'	=> 'id',
									'name'	=> 'c_type_name',
									'sname'	=> 'name',
									'total'	=> 'total',
									'use_n'	=> 'yes',
									'field'	=> 'c_type'),
		"Sector"			=>array('query'	=> "SELECT customer_sector.* , count(DISTINCT customers.customer_id ) AS total
												FROM customer_sector
												INNER JOIN customers ON customer_sector.id = customers.sector
												WHERE customers.active = '1' AND is_admin='0'
												GROUP BY customer_sector.id",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customers.sector'),
		"Price Category"	=>array('query'	=> "SELECT pim_article_price_category.category_id,pim_article_price_category.name , count(DISTINCT customers.customer_id ) AS total
												FROM pim_article_price_category
												INNER JOIN customers ON pim_article_price_category.category_id = customers.cat_id
												WHERE customers.active = '1' AND is_admin='0'
												GROUP BY pim_article_price_category.category_id ",
									'id'	=> 'category_id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customers.cat_id'),
		"Account Manager"	=>array('query'	=> "SELECT customers.customer_id, customers.acc_manager_name, customers.user_id
												FROM customers
												WHERE customers.active = '1' AND customers.user_id != '' AND customers.user_id != '0' AND is_admin='0'
												GROUP BY customers.customer_id ",
									'squery'=> "SELECT CONCAT_WS(' ',first_name, last_name) AS acc_manager_name, user_id FROM users WHERE database_name = '".DATABASE_NAME."' ",
									'sdb'	=> 'yes',  # you can put the database just as easily
									'id'	=> 'user_id',
									'sid'	=> 'user_id',
									'name'	=> 'acc_manager_name',
									'sname'	=> 'acc_manager_name',
									'total'	=> 'total',
									'field'	=> 'customers.user_id'),
		"Lead source"		=>array('query'	=> "SELECT customer_lead_source.*, count(DISTINCT customers.customer_id ) AS total
												FROM customer_lead_source
												INNER JOIN customers ON customer_lead_source.id = customers.lead_source
												WHERE customers.active = '1' AND is_admin='0'
												GROUP BY customer_lead_source.id ",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customers.lead_source'),
		/*"Various 1"			=>array('query'	=> "SELECT customer_various1.*, count(DISTINCT customers.customer_id ) AS total
												FROM customer_various1
												INNER JOIN customers ON customer_various1.id = customers.various1
												WHERE customers.active = '1' AND is_admin='0'
												GROUP BY customer_various1.id ",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customers.various1'),
		"Various 2"			=>array('query'	=> "SELECT customer_various2.*, count(DISTINCT customers.customer_id ) AS total
												FROM customer_various2
												INNER JOIN customers ON customer_various2.id = customers.various2
												WHERE customers.active = '1' AND is_admin='0'
												GROUP BY customer_various2.id ",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'field'	=> 'customers.various2'),*/
		"Has email address"	=>array('query'	=> "SELECT COUNT(customers.customer_id) AS total FROM customers WHERE customers.active = '1' AND is_admin='0' AND c_email!=''
												UNION
												SELECT COUNT(customers.customer_id) AS total FROM customers WHERE customers.active = '1' AND is_admin='0' AND c_email=''",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'custom'=>  true,
									'field'	=> 'customers.c_email'),
		'Allow on e-mailing lists'
							=>array('query'	=> "SELECT count(customer_id) AS total FROM customers WHERE s_emails='1' AND active='1' AND is_admin='0' ",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'custom'=>  true,
									'field'	=> 'customers.s_emails'),
		'Nature'
							=>array('query'	=> "SELECT count(customer_id) AS total FROM customers WHERE active='1' AND is_admin='0'  GROUP BY customers.type",
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'custom'=>  true,
									'field'	=> 'customers.type'),
		'Type'
							=>array('query_new'	=> array('1' =>"SELECT count(customer_id) AS total FROM customers WHERE active='1' AND is_admin='0' AND is_customer='1' ",
														'2' =>"SELECT count(customer_id) AS total FROM customers WHERE active='1' AND is_admin='0' AND is_supplier='1' ",
														'3' =>"SELECT count(customer_id) AS total FROM customers WHERE active='1' AND is_admin='0' AND is_customer='1' AND  is_supplier='1'",
														'4' =>"SELECT count(customer_id) AS total FROM customers WHERE active='1' AND is_admin='0' AND customers.is_supplier='' AND customers.is_customer='' ",),
									'id'	=> 'id',
									'name'	=> 'name',
									'total'	=> 'total',
									'custom'=>  true,
									'field'	=> 'type'),
	);
	$i=0;
	foreach ($filters as $key => $value) {
		$j=0;
		$k=0;
		if(!$value['query_new']){

			$query = $db->query($value['query']);
			$t = gm($key);
			/*if($key == 'Various 1'){
				$various1 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 1' ");
				if($various1){ $t = $various1; }
			}
			if($key == 'Various 2'){
				$various2 = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 2' ");
				if($various2){ $t = $various2; }
			}*/
			$array2 = array('name'=>$t,'items'=>array(),'id'=>'target'.$key);
			if(!$value['squery']){
				while ($query->next()) {
					$id = $query->f($value['id']);
					if($value['custom']){
						$id=$k;
					}
					if($value['field']=='customers.s_emails'){
							$z_text = gm('Allow on e-mailing lists');
					}elseif($value['field']=='customers.type'){
							$id = $k+1;
							if($k==0){
									$z_text = gm('Company');
								}else{
									$z_text = gm('Individual');
								}
					}else{
						if($k==0){
							$z_text = gm('Email');
						}else{
							$z_text = gm('No').' '.gm('Email');
						}
					}
					$item = array(
						'id'						=> (string)$id,
						'name'						=> $value['custom'] ? $z_text : $query->f($value['name']),
						'total'						=> $query->f($value['total']),
						'key'						=> $value['field'],
						'CHECKED'					=> $in['filter'][$value['field']] && in_array($id,$in['filter'][$value['field']]) ? true : false,
					);
					$array2['items'][]=$item;
					$i++;
					$j++;
					$filter_array[$value['field']][$id] = $value['custom'] ? $z_text : $query->f($value['name']);
					if($value['custom']){
						$k++;
					}
				}
			}else{
				$array = array();
				if($value['sdb']){
					$database_users = array(
						'hostname' => $database_config['mysql']['hostname'],
						'username' => $database_config['mysql']['username'],
						'password' => $database_config['mysql']['password'],
						'database' => $database_config['user_db'],
					);
					$db2 = new sqldb($database_users);
					$sub_query= $db2->query($value['squery']);
				}else{
					$sub_query= $db->query($value['squery']);
				}
				while ($sub_query->next()) {
					$array[utf8_encode($sub_query->f($value['sname']))] = array('id'=>$sub_query->f($value['sid']),'count'=>0);
				}
				while ($query->next()) {
					$name = explode(',', $query->f($value['name']));
					foreach ($name as $k => $val) {
						if(array_key_exists(trim($val), $array)){
							$array[trim($val)]['count']++;
						}
					}
				}
				foreach ($array as $keys => $v) {
					if($array[$keys]['count'] > 0 ){
						$item = array(
							'id'						=> $value['use_n'] ? (string)$keys : (string)$array[$keys]['id'],
							'name'						=> $keys,
							'total'						=> $array[$keys]['count'],
							'key'						=> $value['field'],
							'CHECKED'					=> $in['filter'][$value['field']] && in_array($keys,$in['filter'][$value['field']]) ? true : false,
						);
						$array2['items'][]=$item;
						$i++;
						$j++;
						$filter_array[$value['field']][$array[$keys]['id']] = $keys;
					}
				}
			}
			$array2['show'] = $in['filter'] && array_key_exists($value['field'], $in['filter']) && !empty($in['filter'][$value['field']]) ? true : false;
			$res['filters'][] = $array2;



		}else{//for 'type' case
			$k=0;
			$t = gm($key);
			$array2 = array('name'=>$t,'items'=>array(),'id'=>'target'.$key);
			

			foreach($value['query_new'] as $key2 =>$value2){
				$query = $db->query($value2);
			
					while ($query->next()) {
						$id = $key2;
						if($value['field']=='type'){

								switch ($k) {
								    case 0:
								        $z_text = gm('Customer');
								        break;
								    case 1:
								        $z_text = gm('Supplier');
								        break;
								    case 2:
								        $z_text = gm('Customer & Supplier');
								        break;
								    case 3:
								        $z_text = gm('Undefined');
								        break;
								}
								
								
						}
						$item = array(
							'id'						=> (string)$id,
							'name'						=> $z_text ,
							'total'						=> $query->f($value['total']),
							'key'						=> $value['field'],
							'CHECKED'					=> $in['filter'][$value['field']] && in_array($id,$in['filter'][$value['field']]) ? true : false,
						);
						$array2['items'][]=$item;
						$i++;
						$j++;
						$filter_array[$value['field']][$id] =  $z_text ;
						
					}

					$k++;
					
				}
			
			
			$array2['show'] = $in['filter'] && array_key_exists($value['field'], $in['filter']) && !empty($in['filter'][$value['field']]) ? true : false;
			$res['filters'][] = $array2;

		} //end else 'type'



	




	}
	//de aici
	$custom_field_dd_ids = $db->query("SELECT field_id,label FROM customer_fields WHERE is_dd = '1' ");
	$id_500 = 500;
	while($custom_field_dd_ids->next()){

		$count_customers = $db->field("SELECT COUNT( customer_field.customer_id ) FROM customer_field WHERE field_id =  '".$custom_field_dd_ids->f('field_id')."' AND value NOT IN ('Select type','0','Sélectionner le type','undefined') ");
		// console::log($count_customers);
		$custom_title = $custom_field_dd_ids->f('label');
		$custom_dd_own_id = $db->query("SELECT DISTINCT value FROM customer_field WHERE field_id= '".$custom_field_dd_ids->f('field_id')."' AND value NOT IN ('Select type','0','Sélectionner le type','undefined') ");
		$array = array('name'=>$custom_title,'items'=>array(),'id'=>'target'.$custom_title);
		while($custom_dd_own_id->next()){
			$count_vals = $db->field("SELECT COUNT( customer_field.value ) FROM  customer_field WHERE field_id= '".$custom_field_dd_ids->f('field_id')."' AND value = '".$custom_dd_own_id->f('value')."' ");
			$value_name = $db->field("SELECT name FROM customer_extrafield_dd WHERE extra_field_id = '".$custom_field_dd_ids->f('field_id')."' AND id = '".$custom_dd_own_id->f('value')."' ");
			// console::log($custom_title,$value_name);
			$item = array(
				'id'						=> (string)$custom_dd_own_id->f('value'),
				'name'						=> $value_name,
				'total'						=> $count_vals,
				'key'						=> 'custom_dd_id_'.$custom_field_dd_ids->f('field_id'),
				'CHECKED'					=> $in['filter']['custom_dd_id_'.$custom_field_dd_ids->f('field_id')] && in_array($custom_dd_own_id->f('value'),$in['filter']['custom_dd_id_'.$custom_field_dd_ids->f('field_id')]) ? true : false,
			);
			$array['items'][]=$item;
			$id_500++;
		}
		$array['show'] = $in['filter'] && array_key_exists('custom_dd_id_'.$custom_field_dd_ids->f('field_id'), $in['filter']) && !empty($in['filter']['custom_dd_id_'.$custom_field_dd_ids->f('field_id')]) ? true : false;
		$res['filters'][] = $array;
		
	}
	if($in['filter']){
		foreach ($in['filter'] as $key=>$value){
			if($key){
				if($key == 'c_type'){
					foreach ($value as $keys) {
						$res['appliedFilters'][$key][]=$keys;
					}
				}else{
					if(is_array($value)){
						foreach ($value as $keys){
							$res['appliedFilters'][$key][]=$filter_array[$key][$keys];
						}
					}else{
						$res['appliedFilters'][$key][]=$filter_array[$key][$value];
					}
				}
			}
		}
	}
	return $res;
}
?>