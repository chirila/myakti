<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class StageModify extends Controller{
	function __construct($in,$db = null){
		parent::__construct($in,$db = null);		
	}

	public function getData(){
		$in = $this->in;
		$output=array();
	/*	console::log($in['items']);*/
		$in['items']=$_SESSION['selected_items'];
		$output['items']=$in['items'];
		$output['old_board_id']=$in['old_board_id'];
		$output['old_stage_id']=$in['old_stage_id'];
		$output['board_list']=$this->get_BoardsList($in);
		$output['stages_list']=array();
		if($in['board_id']){
			$output['stages_list']=$this->get_StagesList($in);
		}
		
		$this->out = $output;
	}

	
	public function get_BoardsList(&$in){
		$q = strtolower($in["term"]);
		$filter=" 1=1 ";
		if($q){
			$filter .=" AND tblopportunity_board.name LIKE '%".addslashes($q)."%'";
		}
		$result = array();

		$boards=$this->db->query("SELECT tblopportunity_board.* FROM tblopportunity_board WHERE ".$filter." ORDER BY tblopportunity_board.name ASC");
		while($boards->next()){
			array_push($result,array('id'=>$boards->f('id'),'name'=>stripslashes($boards->f('name'))));
		}
		return $result;
	}

	public function get_StagesList(&$in){
		$q = strtolower($in["term"]);
		$filter=" AND 1=1 ";
		if($q){
			$filter .=" AND tblopportunity_stage.name LIKE '%".addslashes($q)."%'";
		}
		$result = array();
		$stages=$this->db->query("SELECT * FROM tblopportunity_stage WHERE board_id='".$in['board_id']."' ".$filter." ORDER BY sort_order ASC");
		while($stages->next()){
			$item=array(
				'id' 	=>  $stages->f('id'),
				'name'	=> stripslashes($stages->f('name'))
				);
			array_push($result,$item);
		}
		return $result;
	}
}

	$deal = new StageModify($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $deal->output($deal->$fname($in));
	}

	$deal->getData();
	$deal->output();

?>