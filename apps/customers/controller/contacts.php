<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($db_config);
//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence=aktiUser::get('user_type');

$output = array();	
$order_by_array = array('lastname','firstname','cus_name','function');
$arguments_s = '';
$arguments='';
$arguments_o='';
$arguments_a='';
$l_r =28;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$output['view'] = 0;
if(CUSTOMER_VIEW_LIST==1){
	$output['view'] = 1;
}

$filter = " customer_contacts.front_register = 0 ";
if(empty($in['archived'])){
	$filter.= " AND customer_contacts.active=1";
}else{
	$in['archived'] = 1;
	$filter.= " AND customer_contacts.active=0";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}

//FILTER LIST
$order_by = " ORDER BY lastname ";
$escape_chars = array('(','?','*','+',')');
if(!empty($in['search'])){
	$filter_x = '';
	// $search_arr = str_split($in['search']);
	$search_arr = str_split(stripslashes($in['search']));
	foreach ($search_arr as $value) {
		if(in_array($value, $escape_chars)){
			$value = '\\'.$value;
		}
		$filter_x .= $value.".?";
	}

	$search_words = explode(' ', $in['search']);
	foreach ($search_words as $word_pos => $searched_word) {
		if(trim($searched_word)){
			
			if($word_pos==0){
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR lastname LIKE '%".$searched_word."%'
							OR customers.name LIKE '%".$searched_word."%'
							OR customer_contactsIds.phone LIKE '%".$searched_word."%'
							OR cell LIKE '%".$searched_word."%'
							OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
							//company_name
			}else{
				$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
							OR lastname LIKE '%".$searched_word."%'
							OR customers.name LIKE '%".$searched_word."%'
							OR customer_contactsIds.phone LIKE '%".$searched_word."%'
							OR cell LIKE '%".$searched_word."%'
							OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
							//company_name
			}

		}
	}

	$arguments_s.="&search=".$in['search'];
}

if(!empty($in['order_by'])){
	
	$order = " ASC ";
	//if($in['desc'] == 'true'){
	if($in['desc']=='1' || $in['desc'] == 'true') {
		$order = " DESC ";
	}
	if(in_array($in['order_by'], $order_by_array)){
		if($in['order_by']=='cus_name'){
			$in['order_by']='customers.name';
		}elseif($in['order_by']=='function'){
			$in['order_by']='customer_contactsIds.position';
		}else{

		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];				
	}
}

if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	$max_rows =  $db->field("SELECT COUNT( contact_id ) AS max_contact_rows 
							FROM customer_contacts
							LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
							LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter );
/*	$c = $db->query("SELECT customer_contacts.*, customers.customer_id, customer_contacts.email as main_email,customer_contact_title.name as display_title,customer_contact_job_title.name as display_function
				FROM customer_contacts
				LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
				LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contacts.title
				LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contacts.position
				WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();*/

	$c = $db->query("SELECT customer_contacts.*, customers.customer_id, customer_contacts.email as main_email,customer_contact_title.name as display_title,customer_contact_job_title.name as display_function
				FROM customer_contacts
				LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
				LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
				LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
				LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
				WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();

	$output['nav'] =  $db->query("SELECT customer_contacts.contact_id FROM customer_contacts 
		LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND `primary`='1' AND customer_contactsIds.customer_id!='0'
		LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
			WHERE ".$filter." GROUP BY customers.customer_id ".$order_by)->getAll();
	$output['nav_contacts'] =  $db->query("SELECT customer_contacts.contact_id
								FROM customer_contacts
								LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
								LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
								LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
								LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
								WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by )->getAll();

}else{
	$arguments = $arguments.$arguments_s;

	$max_rows =  $db->field("SELECT count(customer_contacts.contact_id) FROM customer_contacts 
		LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND `primary`='1' AND customer_contactsIds.customer_id!='0'
		LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
		WHERE ".$filter );
	/*$c = $db->query("SELECT customer_contacts.indiv_created,customers.name as comp_name,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customer_contacts.position_n,customer_contactsIds.phone,customer_contacts.cell,customer_contactsIds.email,customer_contacts.fax,customer_contacts.contact_id,customer_contactsIds.e_title,customer_contacts.title,customer_contactsIds.fax,customer_contactsIds.primary,customer_contactsIds.s_email,customer_contactsIds.department,customer_contactsIds.position,customer_contacts.email as main_email, customer_contactsIds.primary ,customer_contact_title.name as display_title,customer_contact_job_title.name as display_function
		  FROM customer_contacts
				LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
				LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contacts.title
				LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contacts.position
				WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();*/

	$c = $db->query("SELECT customer_contacts.indiv_created,customers.name as comp_name,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customer_contacts.position_n,customer_contactsIds.phone,customer_contacts.cell,customer_contactsIds.email,customer_contacts.fax,customer_contacts.contact_id,customer_contactsIds.e_title,customer_contacts.title,customer_contactsIds.fax,customer_contactsIds.primary,customer_contactsIds.s_email,customer_contactsIds.department,customer_contactsIds.position,customer_contacts.email as main_email, customer_contactsIds.primary ,customer_contact_title.name as display_title,customer_contact_job_title.name as display_function
		  FROM customer_contacts
				LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
				LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
				LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
				WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();


	$output['nav'] =  $db->query("SELECT customer_contacts.contact_id FROM customer_contacts 
		LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND `primary`='1' AND customer_contactsIds.customer_id!='0'
		LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
			WHERE ".$filter." GROUP BY customers.customer_id ".$order_by)->getAll();
	$output['nav_contacts'] =  $db->query("SELECT customer_contacts.contact_id
								FROM customer_contacts
								LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
								LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
								LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
								LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
								WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by )->getAll();
}

$arguments = $arguments.$arguments_s;
$output['max_rows'] =  $max_rows;
$output['list'] = array();

foreach ($c as $key => $value) {
	//$title =  $db->field("SELECT name FROM customer_contact_title WHERE id='".$value['title']."'");
	//$department = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$value['department']."'");
	//$function = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value['position']."'");
	/*$is_new_event = '';
	if(in_array($value['customer_id'], $customer_logs)){
		$is_new_event = 'new_event_timeline';
	}*/
	$last_update = $db->field("SELECT date FROM  `logging` WHERE  `field_name` =  'contact_id' AND  `field_value` =  '".$value['contact_id']."' ORDER BY  `logging`.`date` DESC" );

	$output['list'][]=array(
		/*'name'						=> $value['name'],
		// 'serial_number_customer'	=> $value['serial_number_customer'],
		'country'					=> $value['country_name'] ? $value['country_name']: '&nbsp;',
		'city'						=> $value['city_name'] ? $value['city_name'] : '&nbsp;' ,
		'customer_id'				=> $value['customer_id'],
		'sales'						=> $value['c_type_name'] ? $value['c_type_name'] : '&nbsp;' ,
		// 'contacts'					=> get_contact_nr($value['customer_id']),
		'contacts'					=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '&nbsp;',
		// 'is_active'     			=> $value['ac']? true : false,
		// 'new_event'					=> $is_new_event,
		// 'edit_link'					=> 'index.php?do=company-customer&customer_id='.$value['customer_id'],
		// 'time_link'					=> 'index.php?do=company-customer_timelineNew&customer_id='.$value['customer_id'],
		// 'delete_link'				=> array('do'=>'customers-customer-customer-delete', 'id'=>$value['customer_id']),
		'archive_link'				=> array('do'=>'customers-customers-customers-archive', 'customer_id'=>$value['customer_id']),
		'activate_link'				=> array('do'=>'customers-customers-customers-activate', 'customer_id'=>$value['customer_id']),*/
		'indiv_created'			=> $value['indiv_created'] == 1 ? false : true,
		'cus_name'			    => stripslashes($value['comp_name']),
		'customer_id'		    => $value['customer_id'],
		'con_firstname'			=> $value['firstname'] ? $value['firstname'] : '&nbsp;',
		'con_lastname'			=> $value['lastname'] ? $value['lastname'] : '&nbsp;',
		'con_position'			=> $value['position_n'],
		'con_phone'				=> $value['phone'] ? $value['phone'] : '-',
		'con_cell'				=> $value['cell'] ? $value['cell'] : '-',
		//'con_email'				=> $value['email'],
		'con_email'				=> ($value['primary'] ||  $value['email'] )? $value['email'] : $value['main_email'],
		//'e_email'				=> $value['e_email'],
		'fax'					=> $value['fax'],
		'e_title'				=> $value['e_title'],
		'title'					=> $value['display_title'],
		's_email'				=> $value['s_email']==1 ? gm('Yes') : gm('No'),
		//'department'			=> $department,
		'function'				=> $value['display_function'],
		// 'is_active'     		=> $value['active']? true : false,
		// 'delete_title'     		=> gm('Archive'),
		// 'delete_class'     		=> 'archive',
		// 'hide_delete_link'		=> $value['is_primary'] == 1 ? 'hide':'' ,
		// 'contact_edit_link'		=> 'index.php?do=company-xcustomer_contact&cont/act_id='.$value['contact_id'],
		// 'edit_link'				=> $in['archived'] ? 'index.php?do=company-contacts-customer-activate_contact&contact_id='.$value['contact_id'].$arguments.$arguments_o : 'index.php?do=company-xcustomer_contact&contact_id='.$value['contact_id'],
		// 'delete_link'			=> 'index.php?do=company-contacts-customer-archive_contact&contact_id='.$value['contact_id'].$arguments.$arguments_o,
		'archive_link'				=> array('do'=>'customers-contacts-customers-archive_contact', 'contact_id'=>$value['contact_id']),
		'activate_link'				=> array('do'=>'customers-contacts-customers-activate_contact', 'contact_id'=>$value['contact_id']),
		// 'view_link'				=> 'index.php?do=company-xcustomer_contact&contact_id='.$value['contact_id'].'&front_register='.$in['front_register'].'',
		'contact_id' 			=> $value['contact_id'],
		'confirm'				=> gm('Confirm'),
		'ok'					=> gm('Ok'),
		'cancel'				=> gm('Cancel'),
		// 'activity_link'			=> 'index.php?do=company-customer_contact_activity&contact_id='.$value['contact_id'].'&customer_id='.$value['customer_id'].'&bar_data=1&contacts_ref=1',
		//'hide_activity_icon' 		=> check_if_activity_exists('',$value['contact_id']) == 1 ? '' : 'hide',		
		// 'edit_delete' 			=> check_if_activity_exists('',$value['contact_id']) == 1 ? 'edit_delete' : 'edit_delete_gray',
		'last_update'		=> date(ACCOUNT_DATE_FORMAT,$last_update),
		'last_update_ord'	=> $last_update,
	);
}

	$allo_available=false;
	$user_devices=array();
	$allo_devices=$db->query("SELECT * FROM allo_devices WHERE user_id='".$_SESSION['u_id']."' ");
	$nr_devices=0;
	while($allo_devices->next()){
		$temp_devices=array(
			'id'		=> $allo_devices->f('device_id'),
			'value'		=> stripslashes($allo_devices->f('device_name'))
			);
		array_push($user_devices, $temp_devices);
		$nr_devices++;
	}
	$output['devices']=$user_devices;
	$output['allo_available']=defined('ALLOCLOUD_ACTIVE') && ALLOCLOUD_ACTIVE==1 && $nr_devices ? true : false;

	$output['confirm']	=    gm('Confirm');
	$output['ok']	=    gm('Ok');
	$output['cancel']= 	  gm('Cancel');

	if($in['order_by']=='last_update'){
		$output['list']=order_array_by_column($output['list'], $in['order_by'], $order);	    
	}


$output['export_args'] = ltrim($arguments,'&');
$output['lr']=$l_r;
$output['columns']=array();
$cols_default=default_columns_dd(array('list'=>'contacts'));
$cols_order_dd=default_columns_order_dd(array('list'=>'contacts'));
$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='contacts' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
if(!count($cols_selected)){
	$i=1;
	foreach($cols_default as $key=>$value){
		$tmp_item=array(
			'id'				=> $i,
			'column_name'		=> $key,
			'name'				=> $value,
			'order_by'			=> $cols_order_dd[$key]
		);
		array_push($output['columns'],$tmp_item);
		$i++;
	}
}else{
	foreach($cols_selected as $key=>$value){
		$tmp_item=array(
			'id'				=> $value['id'],
			'column_name'		=> $value['column_name'],
			'name'				=> $cols_default[$value['column_name']],
			'order_by'			=> $cols_order_dd[$value['column_name']]
		);
		array_push($output['columns'],$tmp_item);
	}
}
json_out($output);