<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class TrackingDoc extends Controller{
	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_List(){
		$in=$this->in;
		$result = array('list'=>array());
		$filter= " 1=1 "; 
		if(!empty($in['start_date']) && empty($in['end_date'])){
			$filter .= " AND tracking.creation_date>='".strtotime($in['start_date'])."' ";
		}else if(empty($in['start_date']) && !empty($in['end_date'])){
			$filter .= " AND tracking.creation_date<='".strtotime($in['end_date'])."' ";
		}else if(!empty($in['start_date']) && !empty($in['end_date'])){
			$filter .= " AND tracking.creation_date BETWEEN '".strtotime($in['start_date'])."' AND '".strtotime($in['end_date'])."' ";
		}
		$only_original=false;
		$only_target=false;
		if($in['original_d']!='0'){
			$only_original=true;
			$filter .=" AND tracking_line.origin_type='".$in['original_d']."' ";
		}
		if($in['target_d']!='0'){
			$only_target=true;
			$filter .=" AND tracking.target_type='".$in['target_d']."' ";
		}
		/*$all_main_data=$this->db->query("SELECT tracking.* FROM tracking 
				LEFT JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
				WHERE ".$filter." AND (tracking.origin_buyer_id='".$in['customer_id']."' OR tracking.target_buyer_id='".$in['customer_id']."') AND tracking.archived='0'  GROUP BY tracking.trace_id ORDER BY tracking.creation_date DESC")->getAll();
		$all_sub_data=$this->db->query("SELECT tracking_line.*,tracking.creation_date,tracking.creation_user_id FROM tracking_line
			INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
			WHERE ".$filter." AND (tracking.origin_buyer_id='".$in['customer_id']."' OR tracking.target_buyer_id='".$in['customer_id']."') AND tracking.archived='0' GROUP BY origin_id,origin_type ORDER BY tracking.creation_date DESC")->getAll();*/
		$all_main_data=$this->db->query("SELECT tracking.* FROM tracking 
				LEFT JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
				WHERE ".$filter." AND tracking.target_buyer_id='".$in['customer_id']."' AND tracking.archived='0' AND tracking.target_type != '13'  GROUP BY tracking.trace_id ORDER BY tracking.creation_date DESC")->getAll();
		$all_sub_data=$this->db->query("SELECT tracking_line.*,tracking.creation_date,tracking.creation_user_id FROM tracking_line
			INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
			WHERE ".$filter." AND tracking.target_buyer_id='".$in['customer_id']."' AND tracking.archived='0' AND tracking_line.origin_type != '13' GROUP BY origin_id,origin_type ORDER BY tracking.creation_date DESC")->getAll();
		$key_duplicate=array();
		$dates=array();
		foreach($all_main_data as $key=>$value){
			foreach($all_sub_data as $key1=>$value1){
				/*if($value1['origin_type']=='11'){
					continue;
				}*/
				if(($value['target_id'] == $value1['origin_id']) && ($value['target_type'] == $value1['origin_type']) ){
					array_push($key_duplicate,$key1);
				}
			}
			$item=array(
				'id'			=> $value['target_id'],
				'type'		=> $value['target_type'],
				'time'		=> date('H:i',$value['creation_date']),
				'created'	=> get_user_name($value['creation_user_id']),
				);
			if(array_key_exists($value['creation_date'], $dates) != false){
				array_push($dates[$value['creation_date']],$item);
			}else{
				$dates[$value['creation_date']]=array();
				array_push($dates[$value['creation_date']],$item);
			}
		}
		if($only_original && !$only_target){
			$dates=array();
		}
		if(!$only_target || ($only_original && $only_target)){
			foreach($all_sub_data as $key=>$value){
				/*if($value['origin_type']=='11'){
					continue;
				}*/
				if(in_array($key, $key_duplicate) === false){
					$item=array(
								'id'			=> $value['origin_id'],
								'type'			=> $value['origin_type'],
								'time'		=> date('H:i',$value['creation_date']),
								'created'	=> get_user_name($value['creation_user_id']),
								);
					if(array_key_exists($value['creation_date'], $dates) != false){
						array_push($dates[$value['creation_date']],$item);
					}else{
						$dates[$value['creation_date']]=array();
						array_push($dates[$value['creation_date']],$item);
					}
				}	
			}
		}	
		$result['list']=$this->make_List($dates);
		usort($result['list'], function($a, $b) {
		    return $b['document_timestamp'] - $a['document_timestamp'];
		});
		$result['customer_name']=get_customer_name($in['customer_id']);
		$this->out = $result;
	}

	public function get_More(){
		$in=$this->in;
		$result = array('list'=>array());
		/*$all_originals=$this->db->query("SELECT tracking_line.*,tracking.creation_date,tracking.creation_user_id FROM tracking_line
			INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
			WHERE (tracking.origin_buyer_id='".$in['customer_id']."' OR tracking.target_buyer_id='".$in['customer_id']."') AND tracking.archived='0' AND tracking.target_id='".$in['id']."' AND tracking.target_type='".$in['type']."' ORDER BY tracking.creation_date DESC")->getAll();
		$all_targets=$this->db->query("SELECT tracking.* FROM tracking 
				INNER JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
				WHERE (tracking.origin_buyer_id='".$in['customer_id']."' OR tracking.target_buyer_id='".$in['customer_id']."') AND tracking.archived='0' AND tracking_line.origin_id='".$in['id']."' AND tracking_line.origin_type='".$in['type']."' ORDER BY tracking.creation_date DESC")->getAll();*/
		$all_originals=$this->db->query("SELECT tracking_line.*,tracking.creation_date,tracking.creation_user_id FROM tracking_line
			INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
			WHERE tracking.target_buyer_id='".$in['customer_id']."' AND tracking.archived='0' AND tracking.target_id='".$in['id']."' AND tracking.target_type='".$in['type']."' ORDER BY tracking.creation_date DESC")->getAll();
		$all_targets=$this->db->query("SELECT tracking.* FROM tracking 
				INNER JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
				WHERE tracking.target_buyer_id='".$in['customer_id']."' AND tracking.archived='0' AND tracking_line.origin_id='".$in['id']."' AND tracking_line.origin_type='".$in['type']."' ORDER BY tracking.creation_date DESC")->getAll();
		$dates=array();
		foreach($all_originals as $key=>$value){
			/*if($value['origin_type']=='11'){
				continue;
			}*/
			$item=array(
				'id'			=> $value['origin_id'],
				'type'			=> $value['origin_type'],
				'time'			=> date('H:i',$value['creation_date']),
				'created'		=> get_user_name($value['creation_user_id']),
				);
			if(array_key_exists($value['creation_date'], $dates) != false){
				array_push($dates[$value['creation_date']],$item);
			}else{
				$dates[$value['creation_date']]=array();
				array_push($dates[$value['creation_date']],$item);
			}
		}
		foreach($all_targets as $key=>$value){
			$item=array(
				'id'			=> $value['target_id'],
				'type'			=> $value['target_type'],
				'time'			=> date('H:i',$value['creation_date']),
				'created'		=> get_user_name($value['creation_user_id']),
				);
			if(array_key_exists($value['creation_date'], $dates) != false){
				array_push($dates[$value['creation_date']],$item);
			}else{
				$dates[$value['creation_date']]=array();
				array_push($dates[$value['creation_date']],$item);
			}
		}
		$result['list']=$this->make_List($dates);
		$result['customer_name']=get_customer_name($in['customer_id']);
		$this->out = $result;
	}

	private function make_List($dates){
		$out=array();
		$cases=array(
			'1'=>gm('Invoice'),
			'2'=>gm('Quote'),
			'3'=>gm('Project'),
			'4'=>gm('Order'),
			'5'=>gm('Intervention'),
			'6'=>gm('Purchase Order'),
			'7'=>gm('Proforma'),
			'8'=>gm('Credit Invoice'),
			'9'=>gm('Contract'),
			'10'=>gm('Purchase Invoice'),
			'11'=>gm('Deal'),
//			'13'=>gm('Recurring Invoice'),
			// '12'=>gm('Installation'),
			);
		$type_mach=array(
			'1'=>'invoice',
			'2'=>'quote',
			'3'=>'project',
			'4'=>'order',
			'5'=>'service',
			'6'=>'purchase_order',
			'7'=>'invoice',
			'8'=>'invoice',
			'9'=>'contract',
			'10'=>'purchase_invoice',
			'11'=>'deal',
//			'13'=>'invoice',
			// '12'=>'installation',
			);
		$type_img=array(
			'1'=>'fas fa-file-invoice',
			'2'=>'fas fa-hand-holding-usd',
			'3'=>'fas fa-user-cog',
			'4'=>'fas fa-shipping-fast',
			'5'=>'fas fa-wrench',
			'6'=>'fas fa-shipping-fast',
			'7'=>'fas fa-file-invoice',
			'8'=>'fas fa-file-invoice',
			'9'=>'fas fa-file-contract',
			'10'=>'fas fa-file-invoice',
			'11'=>'fas fa-handshake',
//			'13'=>'invoice-lg',
			// '12'=>'interventionsok',
			);
		krsort($dates);
		foreach($dates as $key=>$value){
			if(is_array($value)){
				foreach($value as $key1=>$value1){
					$version_ids=array();
			    	if($value1['type'] == 2){
			    		$q_version_nr = $this->db->query("SELECT version_id FROM tblquote_version WHERE quote_id='".$value1['id']."' ")->getAll();
			    		if($q_version_nr > 1){
			    			foreach($q_version_nr as $vrs){
			    				$version_ids[]=$vrs['version_id'];
			    			}
			    		}
			    	}
			    	if(count($version_ids)>1 && $value1['type'] == 2){
			    		foreach($version_ids as $vrs){
			    			$more_data=$this->get_Details($value1['id'],$value1['type'],$vrs);
			    			$last='.view';
							if($type_mach[$value1['type']]=='project'){
								$last='.edit';
							}elseif($type_mach[$value1['type']]=='service'){
								$last='.edit';
							}else{
								$last='.view';
							}
							$item_temp=array(
						      'time'  => date(ACCOUNT_DATE_FORMAT,$key),
						      'id'  => $value1['id'],
						      'type'  => $cases[$value1['type']],
						      'type_nr' => $value1['type'],
						      'edit'  => $value1['type'] == '11' ? 'dealView' : strtolower($type_mach[$value1['type']]).$last,
						      'type_id' => strtolower($type_mach[$value1['type']]).'_id',
						      'hour'  => $value1['time'],
						      'author' => $value1['type'] == '11' ? $more_data['manager'] : $value1['created'],
						      'date'  => $more_data['version_creation_date'],
						      'img'  => $type_img[$value1['type']],
						      'amount' => $more_data['amount'],
						      'serial_nr' => $more_data['serial_number'].' ['.$more_data['version_code'].']',
						      'pdf_link' => $more_data['pdf_link'],
						      'sref' => $more_data['sref'],
						      'danger' => $more_data['danger'],
						      'danger_corner'=>$more_data['danger_corner'],
						      'document_timestamp'	=> $more_data['document_timestamp'],
							  'document_time'		=> $value1['type']==5 ? date(ACCOUNT_DATE_FORMAT,$key) : $more_data['document_time'],
							  'planned_date'		=> $value1['type']==5 && $more_data['document_time'] ?$more_data['document_time'] : '',
							  'type_title'		=> $more_data['type_title'] ? $more_data['type_title'] : '',
								'badge_class'		=>  $more_data['badge_class'] ? $more_data['badge_class'] : '',
								'subject'			=> $more_data['subject'],
								'stage_name'		=> $more_data['stage_name'] ? $more_data['stage_name'] : '',
								'your_ref'			=> $more_data['your_ref'],
								'version_id'	=> $vrs
						      );
						      if($value1['type']!='12'){
						       $out[]=$item_temp;
						      }
			    		}	    		
			    	}else{
			    		$more_data=$this->get_Details($value1['id'],$value1['type']);
			    		$last='.view';
						if($type_mach[$value1['type']]=='project'){
							$last='.edit';
						}elseif($type_mach[$value1['type']]=='service'){
							$last='.edit';
						}else{
							$last='.view';
						}

						/*$item_temp=array(
						'time'		=> date(ACCOUNT_DATE_FORMAT,$key),
						'id'		=> $value1['id'],
						'type'		=> $cases[$value1['type']],
						'type_nr'	=> $value1['type'],
						'edit'		=> strtolower($type_mach[$value1['type']]).$last,
						'type_id'	=> strtolower($type_mach[$value1['type']]).'_id',
						'hour'		=> $value1['time'],
						'author'	=> $value1['created'],
						'date'		=> $more_data['date'],
						'img'		=> $type_img[$value1['type']],
						'amount'	=> $more_data['amount'],
						'serial_nr'	=> $more_data['serial_number'],
						'pdf_link'	=> $more_data['pdf_link'],
						'danger'	=> $more_data['danger'],
						'danger_corner'=>$more_data['danger_corner'],
						);
					if($value1['type']!='12'){
						$out[]=$item_temp;
					}*/
					
					//For contracts, get only contracts and not contracts models
				     if($value1['type'] == '9'){
				      if($more_data['contract_active'] == 1 ){
				       $item_temp=array(
				       'time'  => date(ACCOUNT_DATE_FORMAT,$key),
				       'id'  => $value1['id'],
				       'type'  => $cases[$value1['type']],
				       'type_nr' => $value1['type'],
				       'edit'  => strtolower($type_mach[$value1['type']]).$last,
				       'type_id' => strtolower($type_mach[$value1['type']]).'_id',
				       'hour'  => $value1['time'],
				       'author' => $value1['created'],
				       'date'  => $more_data['date'],
				       'img'  => $type_img[$value1['type']],
				       'amount' => $more_data['amount'],
				       'serial_nr' => $more_data['serial_number'],
				       'pdf_link' => $more_data['pdf_link'],
				       'sref' => $more_data['sref'],
				       'danger' => $more_data['danger'],
				       'danger_corner'=>$more_data['danger_corner'],
				       'document_timestamp'		=> $more_data['document_timestamp'],
						'document_time'				=> $more_data['document_time'],
						'type_title'		=> $more_data['type_title'] ? $more_data['type_title'] : '',
						'badge_class'		=>  $more_data['badge_class'] ? $more_data['badge_class'] : '',
						'version_id'=>''
				       );
				       if($value1['type']!='12'){
				        $out[]=$item_temp;
				       } 
				      } 
				     } else {
				      $item_temp=array(
				      'time'  => date(ACCOUNT_DATE_FORMAT,$key),
				      'id'  => $value1['id'],
				      'type'  => $cases[$value1['type']],
				      'type_nr' => $value1['type'],
				      'edit'  => $value1['type'] == '11' ? 'dealView' : strtolower($type_mach[$value1['type']]).$last,
				      'type_id' => strtolower($type_mach[$value1['type']]).'_id',
				      'hour'  => $value1['time'],
				      'author' => $value1['type'] == '11' ? $more_data['manager'] : $value1['created'],
				      'date'  => $more_data['date'],
				      'img'  => $type_img[$value1['type']],
				      'amount' => $more_data['amount'],
				      'serial_nr' => $more_data['serial_number'],
				      'pdf_link' => $more_data['pdf_link'],
				      'sref' => $more_data['sref'],
				      'danger' => $more_data['danger'],
				      'danger_corner'=>$more_data['danger_corner'],
				      'document_timestamp'	=> $more_data['document_timestamp'],
					  'document_time'		=> $value1['type']==5 ? date(ACCOUNT_DATE_FORMAT,$key) : $more_data['document_time'],
					  'planned_date'		=> $value1['type']==5 && $more_data['document_time'] ?$more_data['document_time'] : '',
					  'type_title'		=> $more_data['type_title'] ? $more_data['type_title'] : '',
						'badge_class'		=>  $more_data['badge_class'] ? $more_data['badge_class'] : '',
						'subject'			=> $more_data['subject'],
						'stage_name'		=> $more_data['stage_name'] ? $more_data['stage_name'] : '',
						'your_ref'			=> $more_data['your_ref'],
						'version_id'		=> ''
				      );
				      if($value1['type']!='12'){
				       $out[]=$item_temp;
				      }
				     }
			    	}					
				}
			}else{
				$version_ids=array();
		    	if($value['type'] == 2){
		    		$q_version_nr = $this->db->query("SELECT version_id FROM tblquote_version WHERE quote_id='".$value['id']."' ")->getAll();
		    		if($q_version_nr > 1){
		    			foreach($q_version_nr as $vrs){
		    				$version_ids[]=$vrs['version_id'];
		    			}
		    		}
		    	}
		    	if(count($version_ids)>1 && $value['type'] == 2){
		    		foreach($version_ids as $vrs){
		    			$more_data=$this->get_Details($value['id'],$value['type'],$vrs);
						$item_temp=array(
							'time'		=> date(ACCOUNT_DATE_FORMAT,$key),
							'id'			=> $value['id'],
							'type'		=> $cases[$value['type']],
							'type_nr'	=> $value['type'],
							'hour'		=> $value['time'],
							'author'	=> $value['type'] == '11' ? $more_data['manager'] : $value['created'],
							'date'		=> $more_data['date'],
							'amount'	=> $more_data['amount'],
							'serial_nr'	=> $more_data['serial_number'],
							'pdf_link'	=> $more_data['pdf_link'],
							'sref' 		=> $more_data['sref'],
							'danger'	=> $more_data['danger'],
							'danger_corner'=>$more_data['danger_corner'],
							'document_time'		=> $value['type']==5 ? date(ACCOUNT_DATE_FORMAT,$key) : $more_data['document_time'],
							'planned_date'		=> $value['type']==5 && $more_data['document_time'] ? $more_data['document_time'] : '',
							'type_title'		=> $more_data['type_title'] ? $more_data['type_title'] : '',
							'badge_class'		=>  $more_data['badge_class'] ? $more_data['badge_class'] : '',
							'subject'			=> $more_data['subject'],
							'your_ref'			=> $more_data['your_ref'],
							'version_id'	=> $vrs
						);
						if($value['type']!='12'){
							$out[]=$item_temp;
						}
		    		}
		    	}else{
		    		$more_data=$this->get_Details($value['id'],$value['type']);
					$item_temp=array(
						'time'		=> date(ACCOUNT_DATE_FORMAT,$key),
						'id'			=> $value['id'],
						'type'		=> $cases[$value['type']],
						'type_nr'	=> $value['type'],
						'hour'		=> $value['time'],
						'author'	=> $value['type'] == '11' ? $more_data['manager'] : $value['created'],
						'date'		=> $more_data['date'],
						'amount'	=> $more_data['amount'],
						'serial_nr'	=> $more_data['serial_number'],
						'pdf_link'	=> $more_data['pdf_link'],
						'sref' 		=> $more_data['sref'],
						'danger'	=> $more_data['danger'],
						'danger_corner'=>$more_data['danger_corner'],
						'document_time'		=> $value['type']==5 ? date(ACCOUNT_DATE_FORMAT,$key) : $more_data['document_time'],
						'planned_date'		=> $value['type']==5 && $more_data['document_time'] ? $more_data['document_time'] : '',
						'type_title'		=> $more_data['type_title'] ? $more_data['type_title'] : '',
						'badge_class'		=>  $more_data['badge_class'] ? $more_data['badge_class'] : '',
						'subject'			=> $more_data['subject'],
						'your_ref'			=> $more_data['your_ref'],
						'version_id'	=> ''
					);
					if($value['type']!='12'){
						$out[]=$item_temp;
					}
		    	}			
			}	
		}
		return $out;
	}

	private function get_Details($id,$type,$version=0){
		$obj=array();
		switch($type){
			case '1':
				//Invoice
			case '7':
				//Proforma
			case '8':
				//Credit Invoice
				$data=$this->db->query("SELECT * FROM tblinvoice WHERE id='".$id."' ");
				$obj['date']=$data->f('invoice_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date')) : '';
				$obj['document_timestamp']=$data->f('invoice_date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date'));
				$obj['amount']=display_number($data->f('amount'));
				$obj['serial_number']=$data->f('serial_number');
				//$obj['subject']=stripslashes($data->f('our_ref'));
				$obj['subject']=stripslashes($data->f('subject'));
				if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
					$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
				}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
					$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
				}else{
					$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
				}
				$link_credit_end = '&type='.ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
				if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $data->f('pdf_layout') == 0){
					$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
				}
				if($data->f('type')==2){
					$obj['pdf_link']= 'index.php?do=invoice-invoice_credit_print&id='.$id.'&lid='.$data->f('email_language').$link_credit_end;
				}else{
					$obj['pdf_link']= 'index.php?do=invoice-invoice_print&id='.$id.'&lid='.$data->f('email_language').$link_end;
				}
				$obj['sref']= "invoice.view({ 'invoice_id':".$id." })";
				if($data->f('not_paid') == '0' && $data->f('status') == '0' && $data->f('sent') == '1' && $data->f('due_date') < time()){
					$obj['danger']='ak_activities-danger' ;
					$obj['danger_corner']='border_right_danger';
			    }
			    switch ($data->f('type')){
					case '0':
					    $obj['type_title'] = gm('Regular invoice');
					    break;
					case '1':
					    $obj['type_title'] = gm('Proforma invoice');
					    break;
					case '2':
					    $obj['type_title'] = gm('Credit Invoice');
					    break;
				}
			    if($data->f('sent') == '0' && $data->f('type')!='2'){
					$obj['type_title'] = gm('Draft');
					$obj['badge_class']='item-status-gray';
			    }else if($data->f('status')=='1'){
					$obj['type_title'] = gm('Paid');
					$obj['badge_class']='item-status-green';
			    }else if($data->f('sent')!='0' && $data->f('not_paid')=='0'){
			    	$obj['type_title'] = gm('Final');
					$obj['badge_class']='item-status-yellow';
			    } else if($data->f('sent')!='0' && $data->f('not_paid')=='1'){
			    	$obj['type_title'] = gm('No Payment');
					$obj['badge_class']='item-status-yellow';
			    }else{
			    	$obj['badge_class']='item-status-gray';
			    }
			    $obj['your_ref']=stripslashes($data->f('your_ref'));
				break;
			case '2':
				//Quote
				if($version){
					$filter_s.= " AND tblquote_version.version_id='".$version."' ";
				}
				$data=$this->db->query("SELECT tblquote.*,tblquote_version.version_id,tblquote_version.version_code,tblquote_version.version_date,tblquote_version.version_subject,tblquote_version.version_own_reference,tblquote_version.version_status_customer,tblquote_version.sent AS version_sent FROM tblquote 
					INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
					WHERE tblquote.id='".$id."' ".$filter_s);
				$amount = $this->db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$id."' AND version_id='".$data->f('version_id')."' AND show_block_total='0' ");

				if($data->f('discount') && $data->f('apply_discount') > 1){
					$amount = $amount - ($amount*$data->f('discount')/100);
				}

				if($data->f('currency_rate')){
					$amount = $amount*return_value($data->f('currency_rate'));
				}
				$obj['date']=$data->f('quote_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('quote_date')) : '';
				$obj['document_timestamp']=$data->f('quote_date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('quote_date'));
				$obj['amount']=display_number($amount);
				$obj['serial_number']=$data->f('serial_number');
				$obj['subject']=stripslashes($data->f('version_subject'));
				if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
					$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
				}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
					$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
				}else{
					$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
				}
				#if we are using a customer pdf template
				if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $data->f('pdf_layout') == 0){
					$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT;
				}
				$obj['pdf_link']='index.php?do=quote-print&id='.$id.'&version_id='.$data->f('version_id').'&lid='.$data->f('email_language').$link_end;
				$obj['sref']= "quote.view({ 'quote_id':".$id.", version_id:".$data->f('version_id')." })";
				$today = mktime(0,0,0,date('n'),date('j'),date('Y'));
				if($data->f('version_status_customer') == 0){
					$valid = mktime(0,0,0,date('n',$data->f('validity_date')),date('j',$data->f('validity_date')),date('Y',$data->f('validity_date')));
					if($valid < $today){
						$obj['danger']='ak_activities-danger' ;
						$obj['danger_corner']='border_right_danger';
					}
				}
				$opts=array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
				$opts_colors = array('item-status-gray','item-status-red','item-status-green','item-status-green','item-status-green','item-status-yellow');				
				$obj['type_title']=$opts[$data->f('version_status_customer')];
				$obj['badge_class']=$opts_colors[$data->f('version_status_customer')];
				if($data->f('version_sent') == 1 && $data->f('version_status_customer') == 0){
					$obj['type_title']=$opts[5];
					$obj['badge_class']=$opts_colors[5];
				}
				$obj['your_ref']=stripslashes($data->f('version_own_reference'));
				$obj['version_code']=$data->f('version_code');
				$obj['version_creation_date']=$data->f('version_date');
				break;
			case '3':
				//Project
				$data=$this->db->query("SELECT * FROM projects WHERE project_id='".$id."' ");
				$obj['date']=$data->f('start_date') ? '<span>'.gm('Start Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('start_date')) : '';
				$obj['document_timestamp']=$data->f('start_date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('start_date'));
				$obj['amount']=display_number(0);
				$obj['serial_number']=$data->f('serial_number');
				$obj['pdf_link']='';
				$obj['sref']= "project.edit({ 'project_id':".$id." })";
				$obj['subject']=stripslashes($data->f('name'));
				switch ($data->f('stage')) {
					case '1':
						$obj['type_title'] = gm('Active');
						$obj['badge_class'] = 'item-status-green';
						break;
					case '2':
						$obj['type_title'] = gm('Closed');
						$obj['badge_class'] = 'item-status-gray';
						break;
					default:
						$obj['type_title'] = gm('Draft');
						$obj['badge_class'] = 'item-status-gray';
						break;
				}
				$obj['your_ref']='';
				break;
			case '4':
				//Order
				$data=$this->db->query("SELECT * FROM pim_orders WHERE order_id='".$id."' ");
				$obj['date']=$data->f('del_date') ? '<span>'.gm('Delivery Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('del_date')) : '';
				$obj['document_timestamp']=$data->f('date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('date'));
				$obj['amount']=display_number($data->f('amount'));
				$obj['serial_number']=$data->f('serial_number');
				//$obj['subject']=stripslashes($data->f('our_ref'));
				$obj['subject']=stripslashes($data->f('subject'));
				$default_lang = $data->f('email_language');
				if(!$default_lang){
					$default_lang = 1;
				}
				if($data->f('pdf_layout')){
					$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
				}else{
					$link_end = '&type='.ACCOUNT_ORDER_PDF_FORMAT;
				}
				$obj['pdf_link']= 'index.php?do=order-order_print&order_id='.$id.'&lid='.$default_lang.$link_end;
				$obj['sref']= "order.view({ 'order_id':".$id." })";
				if($data->f('sent')==0 ){
					$obj['type_title'] = gm('Draft');
				}else if($data->f('invoiced')){
					if($data->f('rdy_invoice') == 1){
						$obj['type_title'] = gm('Delivered');
					}else{
						$obj['type_title'] = gm('Confirmed');
					}
				}else{
					if($data->f('rdy_invoice') == 2){
						$obj['type_title'] = gm('Confirmed');
					}else{
						if($data->f('rdy_invoice') == 1){
							$obj['type_title'] = gm('Delivered');
						}else{
							if($data->f('sent') == 1){
								$obj['type_title'] = gm('Confirmed');
							}else{
								$obj['type_title'] = gm('Draft');
							}
						}
					}
				}
				$obj['badge_class']=$data->f('sent') == 1 && $data->f('rdy_invoice') ? ($data->f('rdy_invoice')==1 ? 'item-status-green' : 'item-status-yellow') : ($data->f('sent') == 1 ? 'item-status-yellow' : 'item-status-gray');
				$obj['your_ref']=stripslashes($data->f('your_ref'));
				break;
			case '5':
				//Intervention
				$data=$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$id."' ");
				$obj['date']=$data->f('planeddate') ? '<span>'.gm('Planned Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('planeddate')) : '';
				$obj['document_timestamp']=$data->f('planeddate') ? $data->f('planeddate') : '0';
				$obj['document_time']=$data->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$data->f('planeddate')) : '';
				$obj['amount']=display_number(0);
				$obj['serial_number']=$data->f('serial_number');
				$obj['pdf_link']='index.php?do=maintenance-print&service_id='.$id;
				$obj['sref']= "service.view({ 'service_id':".$id." })";
				$obj['subject']=stripslashes($data->f('subject'));
				if($data->f('status')=='1'){
					$obj['type_title'] = gm('Planned');
				}elseif($data->f('status')=='2'){
					if($data->f('is_recurring')=='0' && $data->f('active')=='1'&&$data->f('accept')=='1'){
						$obj['type_title'] = gm('Accepted');
					}else{
						$obj['type_title'] = gm('Closed');
					}
				}else{
					$obj['type_title'] = gm('Draft');
				}
				$obj['badge_class']=$data->f('status') == 2 ? 'item-status-green' : ($data->f('status') == 1 ? 'item-status-orange' : 'item-status-gray');
				$obj['your_ref']=stripslashes($data->f('your_ref'));
				break;
			case '6':
				//Purchase Order
				$data=$this->db->query("SELECT pim_p_orders.*,ROUND(cast(pim_p_orders.amount AS decimal(12, 4))+0.0012,2) AS formatted_amount,pim_p_order_deliveries.delivery_id AS del_id FROM pim_p_orders LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id WHERE pim_p_orders.p_order_id='".$id."' ");
				$obj['date']=$data->f('del_date') ? '<span>'.gm('Delivery Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('del_date')) : '';
				$obj['document_timestamp']=$data->f('date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('date'));
				$obj['amount']=display_number($data->f('formatted_amount'));
				$obj['serial_number']=$data->f('serial_number');
				$obj['subject']=stripslashes($data->f('our_ref'));
				$lid = $data->f('email_language');
				$pdf_del_type = $data->f('p_order_type')? $data->f('p_order_type'):ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
				if(!$lid){
					$lid = 1;
				}
				$obj['pdf_link']='index.php?do=po_order-p_order_print&p_order_id='.$id.'&lid='.$lid.'&type='.$pdf_del_type;
				$obj['sref']= "purchase_order.view({ 'p_order_id':".$id." })";
				if($data->f('sent')==0 ){
					$obj['type_title'] = gm('Draft');
				}
				if($data->f('invoiced')){
					if($data->f('rdy_invoice') == 1){
						$obj['type_title'] = gm('Received');
					}else{
						$obj['type_title'] = gm('Final');
					}
				}else{
					if($data->f('rdy_invoice') == 2){
						if($data->f('sent')=='1' && $data->f('del_id') !=0){
							$obj['type_title'] = gm('Partially Delivered');
						}else{
							$obj['type_title'] = gm('Final');
						}				
					}else{
						if($data->f('rdy_invoice') == 1){
							$obj['type_title'] = gm('Received');
						}else{
							if($data->f('sent') == 1){
								$obj['type_title'] = gm('Final');
							}else{
								$obj['type_title'] = gm('Draft');
							}
						}
					}
				}
				$obj['badge_class']=$data->f('rdy_invoice') ? ($data->f('rdy_invoice')==1 ? 'item-status-green' : 'item-status-orange') : 'item-status-gray';
				$obj['your_ref']=stripslashes($data->f('your_ref'));
				break;
			case '9':
				//Contracts
				$data=$this->db->query("SELECT contracts.*,contracts_version.version_id FROM contracts 
					INNER JOIN contracts_version ON contracts.contract_id=contracts_version.contract_id AND contracts_version.active=1
					WHERE contracts.contract_id='".$id."' ");
				$obj['contract_active']= $data->f('active');
				$amount = $this->db->field("SELECT SUM(amount) FROM contract_line WHERE contract_id='".$id."' AND version_id='".$data->f('version_id')."' AND show_block_total='0' ");
				if($data->f('discount') && $data->f('apply_discount') > 1){
					$amount = $amount - ($amount*$data->f('discount')/100);
				}
				if($data->f('currency_rate')){
					$amount = $amount*return_value($data->f('currency_rate'));
				}
				$obj['date']=$data->f('start_date') ? '<span>'.gm('Start Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('start_date')) : '';
				$obj['document_timestamp']=$data->f('start_date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('start_date'));
				$obj['amount']=display_number($amount);
				$obj['serial_number']=$data->f('serial_number');
				$obj['subject']=stripslashes($data->f('subject'));
				if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
					$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
				}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
					$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
				}else{
					if(ACCOUNT_CONTRACT_PDF_FORMAT){
						$link_end = '&type='.ACCOUNT_CONTRACT_PDF_FORMAT;
					}else{
						$link_end = '&type=1';
					}	
				}
				$obj['pdf_link']='index.php?do=contract-print&contract_id='.$id.'&version_id='.$data->f('version_id').'&lid='.$data->f('email_language').$link_end;
				$obj['sref']= "contract.view({ 'contract_id':".$id.", version_id:".$data->f('version_id')." })";
				if(!$data->f('sent')){
					$obj['type_title']=gm('Draft');
				}else{
					if($data->f('status_customer')==0){
						$obj['type_title']=gm('Sent');
					}else if($data->f('status_customer')==1){
						$obj['type_title']=gm('Rejected');
					}else if($data->f('status_customer')==2){
						$obj['type_title']=gm('Active');
					}else{
						$obj['type_title']=gm('Closed');
					}
				}
				if($data->f('closed')==1){
					$obj['type_title']=gm('Closed');
				}
				if($data->f('sent')=='1' && $data->f('status_customer')=='0'){
					$obj['badge_class']='item-status-orange';
				}else if($data->f('sent')=='1' && $data->f('status_customer')=='2'){
					$obj['badge_class']='item-status-green';
				}else if($data->f('status_customer')=='3'){
					$obj['badge_class']='item-status-yellow';
				}else if($data->f('sent')=='1' && $data->f('status_customer')=='1'){
					$obj['badge_class']='item-status-red';
				}else{
					$obj['badge_class']='item-status-gray';
				}
				$obj['your_ref']=stripslashes($data->f('your_ref'));
				break;
			case '10':
				//Purchase Invoice
				$data=$this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$id."' ");
				$obj['date']=$data->f('invoice_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date')) : '';
				$obj['document_timestamp']=$data->f('invoice_date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date'));
				$obj['amount']=display_number($data->f('total'));
				$obj['serial_number']=$data->f('booking_number');
				$obj['pdf_link']=$data->f('file_name') ? 'index.php?do=invoice-purchase_print&id='.$id : '';
				$obj['sref']= "purchase_invoice.view({ 'invoice_id':".$id." })";
				$obj['subject']='';
				switch($data->f('status')){
					case '0':
						$obj['badge_class']=$data->f('paid') ? 'item-status-green' : 'item-status-gray';
						$obj['type_title']=$data->f('paid') ? gm('Paid') : gm('Uploaded');
						break;
					case '1':
						$obj['badge_class']=$data->f('paid') ? 'item-status-green' : 'item-status-yellow';
						$obj['type_title']=$data->f('paid') ? gm('Paid') : gm('Approved');
						break;
					default:
						$obj['type_title']='item-status-gray';
						$obj['type_title']=gm('Uploaded');
						break;
				}
				$obj['your_ref']='';
				break;
			case '11':
				//Deal
				$data=$this->db->query("SELECT tblopportunity.*,tblopportunity_stage.name AS stage_name FROM tblopportunity LEFT JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id WHERE tblopportunity.opportunity_id='".$id."' ");
				$obj['date']=$data->f('exp_close_date') ? date(ACCOUNT_DATE_FORMAT,$data->f('exp_close_date')) : '';
				$obj['document_timestamp']=$data->f('opportunity_date');
				$obj['document_time']=date(ACCOUNT_DATE_FORMAT,$data->f('opportunity_date'));
				$obj['amount']=display_number($data->f('expected_revenue'));
				$obj['serial_number']=$data->f('serial_number');
				$obj['pdf_link']='';
				$obj['sref']= "dealView({ 'opportunity_id':".$id." })";
				$obj['subject']=stripslashes($data->f('subject'));
				$obj['manager']=$data->f('created_by') ? get_user_name($data->f('created_by')) : '';
				$obj['stage_name']=stripslashes($data->f('stage_name'));
				$obj['your_ref']='';
				//
				break;
			// case '12':
			// 	//Installation
			// 	$data=$this->db->query("SELECT * FROM installations WHERE id='".$id."' ");
			// 	$obj['date']=$data->f('date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('date')) : '';
			// 	$obj['amount']='';
			// 	$obj['serial_number']=$data->f('serial_number');
			// 	$obj['pdf_link']='';
			// 	break;
//            case '13':
//                //Recurring Invoices
//                $invoices = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$id."'");
//                $data = $this->db->query("SELECT recurring_invoice.*, recurring_invoice_line.* FROM recurring_invoice LEFT JOIN recurring_invoice_line ON recurring_invoice.recurring_invoice_id = recurring_invoice_line.recurring_invoice_id WHERE recurring_invoice.recurring_invoice_id ='".$id."' ");
//                $obj['date']=$data->f('start_date') ? date(ACCOUNT_DATE_FORMAT, $data->f('start_date')) : '';
//                $obj['document_timestamp']= '';
//                $obj['document_time']= '';
//                $obj['amount']= display_number($data->f('price'));
//                $obj['serial_number'] = '';
//                $obj['pdf_link']= '';
//                $obj['sref']= "recinvoice.view({ 'recurring_invoice_id':".$id." })";
//                $obj['subject']=stripslashes($data->f('subject'));
//                $obj['badge_class']=$data->f('f_archived') ? 'item-status-gray' : 'item-status-green';
//                $obj['type_title']=$data->f('f_archived') ? gm('Archived') : gm('Active');
//                $obj['manager']=$data->f('created_by') ? get_user_name($data->f('created_by')) : '';
//                $obj['your_ref']=$data->f('your_ref');
//                break;
		}
		return $obj;
	}
}

	$track_data = new TrackingDoc($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$track_data->output($track_data->$fname($in));
	}

	$track_data->get_List();
	$track_data->output();
?>