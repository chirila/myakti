<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	if($in['return_data']){
    		return $fname($in);
    	}else{
    		json_out($fname($in));
    	}    	
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
if($in['viewMod']){
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);

	$users_auto = $db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
	$user_auto = array();
	foreach ($users_auto as $key => $value) {
		$user_auto[] = array('id'=>$value['user_id'], 'value'=>htmlspecialchars_decode(stripslashes($value['first_name'] . ' ' . $value['last_name'])) );
	}

	$result['user_auto'] = $user_auto;
}

$contact = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ")->getAll();
$contact = $contact[0];
$result['item_exists']= true;
if($in['contact_id']=='tmp'){
	$result['item_exists']= true;
}elseif(!$contact['contact_id']){
	msg::error('Contact does not exist','error');
	$in['item_exists']= false;
    json_out($in);
}

$contact_new = $db->query("SELECT * FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND `primary`='1' ")->getAll();
$contact_new = $contact_new[0];
$contact['position'] = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$contact_new['position']."'");
$contact['department_new'] = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact_new['department']."'");
// $contact['phone'] = $contact_new['phone'];
$contact['fax'] = $contact_new['fax'];
$contact['gender_dd'] = gender_dd();

$contact['exact_title']=$contact_new['e_title'];
$contact['function']=$contact_new['e_title'];
$contact['zen_id']=$contact['zen_contact_id'];
$contact['title_txt'] = $db->field("SELECT name FROM customer_contact_title  WHERE id='".$contact['title']."' ");
if(!is_numeric($contact['title'])){
	$contact['title']='1';
}

/*if(!is_numeric($contact['sex']) || !$contact['sex'] ){
	$contact['sex']='1';
}
switch ($contact['sex']) {
	case '2':
		$contact['sex_txt'] = gm('Female');
		break;	
	default:
		$contact['sex_txt'] = gm('Male');
		break;
}*/

/*if(!is_numeric($contact['language'])){
	$contact['language']='1';
}*/
if($contact['language']=='0'){
	$contact['language']=undefined;
}

$contact['language_txt'] = $contact['language']<1000? (gm($db->field("SELECT language FROM pim_lang WHERE lang_id = '".$contact['language']."' "))) :
											(gm($db->field("SELECT language FROM pim_custom_lang WHERE lang_id = '".$contact['language']."' ")));
 
// $db->field("SELECT name FROM customer_contact_language WHERE id='".$contact['language']."' ");

$contact['exact_title_txt'] = $db->field("SELECT e_title FROM customer_contactsIds  WHERE contact_id='".$in['contact_id']."' ");

switch ($contact['sex']) {
	case '1':
		$contact['sex_txt'] = gm('Male');
		break;
	case '2':
		$contact['sex_txt'] = gm('Female');
		break;	
	default:
		$contact['sex_txt'] = '';
		break;
}

if(!is_numeric($contact['department'])){
	$contact['department']='1';
}
$contact['department_txt'] = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact['department']."' ");

if(!is_numeric($contact['country_id']) || !$contact['country_id']){
	$contact['country_id']=ACCOUNT_DELIVERY_COUNTRY_ID;
}

if($contact['birthdate']){
	$contact['birthdate_txt'] = date(ACCOUNT_DATE_FORMAT,$contact['birthdate']);
	$contact['birthdate'] = $contact['birthdate']*1000;
}


if($contact['s_email'] == '1'){
	$contact['s_email'] = true;
	$contact['s_email_txt'] = gm('Yes');
}else{
	$contact['s_email'] = false;
	$contact['s_email_txt'] = gm('No');
}


if($contact['create']!=0){
	$contact['create'] = date(ACCOUNT_DATE_FORMAT,$contact['create']);
}

if( isset($contact['position']) && !is_numeric($contact['position']) ){
	$contact['position']='1';
}
$contact['position_txt'] = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$contact['position']."' ");

if($contact['position_n']){
	$contact['position_n'] = explode(',', $contact['position_n']);
}

$label = $db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY field_id ");
$all = $label->getAll();
$contact['extra']=array();
$result['extra'] = array();
$result['extra_ids'] = array();
$result['extra_name'] = array();
foreach ($all as $key => $value) {
	$f_value = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$value['field_id']."' ");
	if($value['is_dd']==1){
		$result['extra'][$value['field_id']]=build_extra_contact_field_dd('',$value['field_id']);
		$contact['extra_ids'][$value['field_id']] = $f_value;
		if($in['viewMod']){
			$f_value = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$value['field_id']."' AND id='".$f_value."' ");
		}
		/*if(!$f_value){
			$f_value = '0';
		}*/
	}
	$result['extra_name'][$value['field_id']] = $value['label'];
	
	$contact['extra'][$value['field_id']]=$f_value;
}

/*$label1 = $db->query("SELECT * FROM contact_fields WHERE value = '1' AND default_value='1' ORDER BY field_id ");
$all1 = $label1->getAll();
$contact['extra1']=array();
$result['extra1'] = array();
$result['extra_name1'] = array();
foreach ($all1 as $key1 => $value1) {
	$f_value1 = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$value1['field_id']."' ");
	if($value1['is_dd']==1){
		$result['extra1'][$value1['field_id']]=build_extra_contact_field_dd('',$value1['field_id']);
		if($in['viewMod']){
			$f_value1 = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$value1['field_id']."' ");
		}
		if(!$f_value1){
			$f_value1 = '0';
		}
	}
	$result['extra_name1'][$value1['field_id']] = $value1['label'];
	
	$contact['extra1'][$value1['field_id']]=$f_value1;
}
*/
$i=0;
$default_show=array();
$def=$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact' AND value=1 ");
while ($def->next()) {
	$line = array();
	/*$line[$def->f('field')] = $def->f('value') == 1 ? true : false;*/
	//$result['hide'][$def->f('field').'_if'] = $def->f('creation_value') == 1 ? true : false;
	if($in['contact_id'] == 'tmp' || !$in['contact_id']){
		$result['hide'][$def->f('field').'_if'] = $def->f('creation_value') == 1 ? true : false;
	} else {
	 	$result['hide'][$def->f('field').'_if'] = $def->f('value') == 1 ? true : false;			
	}
}



if($in['contact_id'] == 'tmp' || !$in['contact_id']){
	$do_next ='customers-Contact-customers-contact_add';
	$is_add = true;
	$result['contact_id'] = 'tmp';
}else{
	$do_next ='customers-Contact-customers-contact_update';
	$is_add = false;
	$address = $db->query("SELECT * FROM  customer_contact_address WHERE contact_id='".$in['contact_id']."' and is_primary=1 ")->getAll();
	foreach ($address[0] as $key => $value) {
		$contact[$key] = $value;
	}
	$add_id = $db->query("SELECT * FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
	$title = $db->field("SELECT title FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' ");
	$title_contact = $db->field("SELECT title FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	if($add_id->next()){
		$c_address = $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country  FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
						 		 LEFT JOIN state ON state.state_id=customer_addresses.state_id
						  		 WHERE address_id='".$add_id->f('address_id')."'");
		$contact['attachlocation'] = array(
			'caddress'			=> nl2br($c_address->f('address')),
	  		'czip'			    => $c_address->f('zip'),
	  		'ccity'			    => $c_address->f('city'),
	  		'cstate'			=> $c_address->f('state'),
	  		'ccountry'			=> $c_address->f('country')
		);
		
	}

	$result['accounts'] = get_contactAccounts($in);
	$result['account'] = get_contactAccount($in);
}
$contact['do'] = $do_next;

if($in['contact_id']){
	$is_admin = $db->field("SELECT admin_webshop FROM customer_contacts WHERE contact_id = '".$in['contact_id']."'");
	$contact_webshop = $db->query("SELECT * FROM customer_contacts WHERE contact_id = '".$in['contact_id']."'");
	$contact_webshop->move_next();
	$result['webshop']['email']						= $contact_webshop->gf('email');
	$result['webshop']['LANGUAGE_DD']				= build_language_dd_new($contact_webshop->gf('language'));
	$result['webshop']['lang_id']					= $contact_webshop->gf('language');
	$result['webshop']['password']					= $in['password'];
	$result['webshop']['input_value']				= $in['input_value'];
	$result['webshop']['USERNAME']					= $contact_webshop->gf('username');
	$result['webshop']['contact_id']				= $in['contact_id'];
	$result['webshop']['do']						= 'customers-Contact-customers-update_pass';
	$result['webshop']['is_admin']					= $is_admin == 1 ? true : false;
/*	$view_contact_webshop->assign(array(
		'EMAIL'           	=> $db->gf('email'),
		'LANGUAGE_DD'	    => build_language_dd_new($db->gf('language')),
		'USERNAME'	    	=> $db->gf('username'),
		'is_admin'			=> $is_admin == 1 ? "CHECKED" : '',
		'meyer'				=> DATABASE_NAME=='838e204c_ec74_7de6_1bdc5f79135d' || DATABASE_NAME=='SalesAssist_11' || DATABASE_NAME=='salesassist_2' ? true:false,
	));*/

}

$result['is_admin']						= $_SESSION['access_level'] == 1 ? true : false;
$contact['title_id']					= $title;
$contact['title_contact_id']			= $title_contact;
$result['customer_contact_title']		= build_contact_title_type_dd();
$result['customer_contact_dep']			= build_contact_dep_type_dd();
$result['lang_dd']						= build_language_dd_new();
$result['customer_contact_language']	= build_language_dd();
$result['country_dd']					= build_country_list('');
$result['customer_contact_job_title'] 	= build_contact_function();
$result['customer_dd']					= get_customers($in);
$result['customers_dd']					= get_customers($in);
$result['format']						= 'dd/MM/yyyy';
$result['showLinkAccount']				= false;
$result['dateOptions'] 					= 	array(
							            		'formatYear'	=> 'YYYY',
							            		'startingDay' 	=> 1
							        		);
$result['confirm']						= gm('Confirm');
$result['ok']							= gm('Ok');
$result['cancel']						= gm('Cancel');


$result['viewMod']						= $in['viewMod'];
if($in['viewMod']){
	$result['contact_id']				= $in['contact_id'];
}

$result['obj']							= $contact;
if(!$in['viewMod']){
	$result['obj']['customer_id']	= isset($in['customer_id']) ? $in['customer_id'] : '';
}
foreach ($in as $key => $value) {
	if(!array_key_exists($key, $result['obj'])) {
		$result['obj'][$key] = $in[$key];
	}
}
$result['obj']['contact_name'] = $result['obj']['firstname'].' '.$result['obj']['lastname'];
$result['obj']['email']=$contact_new['primary'] ? $contact_new['email'] : $contact['email'];

//TODO need a way to test this

// $result['obj']['default_image'] ='images/akti-picture.png';
// $artPhoto=get_contact_photo($result['obj']['contact_id']);

// if($artPhoto){
// 	$result['obj']['view_upload'] = false;
// 	$result['obj']['exist_image'] = true;
// }else{
// 	$result['obj']['view_upload'] = true;
// 	$result['obj']['exist_image'] = false;
// }

// $result['obj']['artPhoto']= $artPhoto ? $artPhoto : $result['obj']['default_image'];

if($in['contact_id'] == 'tmp' || !$in['contact_id']){
	$language_id = $db->field("SELECT language FROM customers WHERE customer_id = '".$in['customer_id']."' ");
	$result['obj']['language'] = $language_id;
}

$result['ADV_CRM']					= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;
$result['is_zen']							= defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 ? true : false;
$allo_available=false;
$user_devices=array();
$allo_devices=$db->query("SELECT * FROM allo_devices WHERE user_id='".$_SESSION['u_id']."' ");
$nr_devices=0;
while($allo_devices->next()){
	$temp_devices=array(
		'id'		=> $allo_devices->f('device_id'),
		'value'		=> stripslashes($allo_devices->f('device_name'))
		);
	array_push($user_devices, $temp_devices);
	$nr_devices++;
}
$result['devices']=$user_devices;
$result['allo_available']=defined('ALLOCLOUD_ACTIVE') && ALLOCLOUD_ACTIVE==1 && $nr_devices ? true : false;


//Build Contact fields sorting
if($in['contact_id'] == 'tmp' || !$in['contact_id']){
	$queryF = $db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact' AND value=1  AND creation_value=1 ORDER BY sort_order ASC")->getAll();
	$queryExtraF = $db->query("SELECT * FROM contact_fields WHERE value = '1' AND creation_value='1'  ORDER BY sort_order ASC ");
}else{
	$queryF = $db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'  AND value=1 ORDER BY sort_order ASC")->getAll();
	$queryExtraF = $db->query("SELECT * FROM contact_fields WHERE value = '1'  ORDER BY sort_order ASC ");
}
//add customer address till it will be put dynamic
array_unshift($queryF,array(
	'controller'	=> 'company-xcustomer_contact',
	'field'			=> 'customer_address_id',
	'value'			=> '1',
	'default_value'	=> '1',
	'creation_value'	=> '1',
	'sort_order'		=> null
));
//end
foreach ($queryF as $query) {
$fieldInfo = contactStaticFieldsInfo($query);
$accountRelatedfieldInfo = contactStaticAccountRelatedFieldsInfo($query);
	if(!empty($fieldInfo['label'])){
		$result['obj']['linesObj'][] = $fieldInfo; 
	}	
	if(!empty($accountRelatedfieldInfo['label'])){
		$result['obj']['accountRelatedObj'][] = $accountRelatedfieldInfo;
		$index++;
	}
}


while($queryExtraF->next()){
	$f_value = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$queryExtraF->f('field_id')."' ");
	if($in['contact_id']=='tmp' || !$in['contact_id']){
		$creation = $queryExtraF->f('creation_value') == 1 ? true : false;
	}else{
		$creation= true;
	}
	if($value['is_dd']==1){
		$extra_field_dd_name = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$queryExtraF->f('field_id')."' ");
	}

	$result['obj']['linesObj'][] = array(
		'field_id'			=> $queryExtraF->f('field_id'),
		'label'				=> $queryExtraF->f('label'),
		'label_if'			=> $creation,
		'value'				=> $f_value ? $f_value : '',
		'normal_input'		=> $queryExtraF->f('is_dd')== 1 ? false:true,
		// 'is_admin'			=> $_SESSION['access_level') == 1 ? true : false,
		'extra_field_name' 	=> $queryExtraF->f('is_dd') == 1 ? $extra_field_dd_name : ( $f_value ? $f_value : '' ),
		'is_custom'			=> true,
		'sort_order'		=> $queryExtraF->f('sort_order'),
		);
}

usort($result['obj']['linesObj'], "myCustomSort");
//End Build Sorted Field Section

// Contact View Fields
$default_front=array();

$def=$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'  order by default_value desc,sort_order ASC ");

$customiseFields = $def->getAll();

$customiseExtraFields = $db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY sort_order ASC ")->getALl();

$result_front =  array_merge($customiseFields, $customiseExtraFields);

//Sort all fields
usort($result_front, "myCustomSort");

foreach ($result_front as  $res) {
	$loop_name = $res['default_value'] == 1 ? 'default_show' : 'just_show';

	if(!empty($res['controller'])){
		$line = array($res['field'] 	=> $res['value'] == 1 ? true : false,
					'is_custom_field'	=> 'false',
				);
		$default_front[$loop_name][] = $line;	
	} else {
		$line = array('field_id'=> $res['field_id'],	                    
			'label'				=> $res['label'],				
			'is_custom_field'	=> 'true',
		);
		$default_front[$loop_name][] = $line;
	}
}
// End Contact View Fields

$result['default_show'] = $default_front;

// Get Activity and product type
$result['obj']['show_user_activity'] = false;
$result['obj']['user_activity_txt'] = '';
$result['obj']['show_user_product_type'] = false;
$result['obj']['user_product_type_txt'] = '';

if(DATABASE_NAME == '04ed5b47_e424_5dd4_ad024bcf2e1d'){
	$snap_id = $db->field("SELECT value FROM contact_field 
								WHERE field_id = '1'
								AND customer_id = '".$in['contact_id']."'
								");
	if(!empty($snap_id)){
		global $database_config;
		$database_usr = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_usr = new sqldb($database_usr);

		$client_data = $db_usr->query("SELECT users.last_name,users.first_name,users.database_name,users.user_id,user_info.is_trial,user_info.chargify_user_id,users.ACCOUNT_TYPE
				FROM users
				LEFT JOIN user_info ON users.user_id = user_info.user_id
				WHERE users.user_id = '".$snap_id."'" )->getAll();

		if(!empty($client_data)){
			$client_data = $client_data[0];

			$result['obj']['show_user_activity'] = true;
			$result['obj']['show_user_product_type'] = true;

			if(!empty($client_data['chargify_user_id'])){
				$result['obj']['user_activity_txt'] = date(ACCOUNT_DATE_FORMAT.' H:i',$client_data['chargify_user_id']);	
			} else {
				$result['obj']['user_activity_txt'] = '';	
			}
			 
			$product_type = $client_data['ACCOUNT_TYPE'];
			
			if($product_type == 'services'){
				$result['obj']['user_product_type_txt'] = gm('Services');
				} elseif ($product_type == 'goods'){
					$result['obj']['user_product_type_txt'] = gm('Goods');
				} elseif ($product_type == 'both'){
					$result['obj']['user_product_type_txt'] = gm('Both');
				} elseif ($product_type == 'free'){
					$result['obj']['user_product_type_txt'] = gm('Free');
			} else {
				$result['obj']['user_product_type_txt'] = gm('No Plan');
			}
		}
	}
}

// End Get Activity and product type

json_out($result);


function contactStaticAccountRelatedFieldsInfo($query){
	$fieldLabelName = '';
	$ifCondition = '';
	$isDD = false;
	$selectizeConfig = '';
	$selectizeOptions = '';
	$model = '';
	$isCustom = false;
	$sortOrder = $query['sort_order'];
	$hasTable = false;
	$table = '';
	$hasName = false;
	$is_checkbox=false;
	
	switch($query['field']){
		case 'email':
			$fieldLabelName = gm('Email');
			$model = 'email';
			$hasName = true;
			break;
		case 'exact_title':
			$fieldLabelName = gm('Exact Title');
			$model = 'e_title';
			$hasName = true; 
			break;
		case 'phone':
			$fieldLabelName = gm('Phone');
			$model = 'phone';
			break;
		case 'fax':
			$fieldLabelName = gm('Fax');
			$model = 'fax';
			break;
		case 'function':
			$fieldLabelName = gm('Function');
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_contact_job_title';
			$hasTable = true;
			$table = 'customer_contact_job_title';
			$model = 'position';
			break;
		case 'department':
			$fieldLabelName = gm('Department');
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_contact_dep';
			$hasTable = true;
			$table = 'customer_contact_dep';
			$model = 'department';
			break;	
		case 's_email':
			$fieldLabelName = gm('Allow on e-mailing lists');
			$model = 's_email';
			$hasName = true; 
			$is_checkbox=true;
			break;
		case 'customer_address_id':
			$fieldLabelName = gm('Location');
			$isDD = true;
			$selectizeConfig = 'addressCfg';
			$model = 'customer_address_id';
			break;
	}

	return array('label' 			=> $fieldLabelName,
			'if_condition' 		=> $ifCondition,
			'is_dd'		   		=> $isDD,
			'selectize_config' 	=> $selectizeConfig,
			'selectize_options' => $selectizeOptions,
			'model'				=> $model,
			'is_custom'			=> $isCustom,
			'sort_order'		=> $sortOrder,
			'has_table'			=> $hasTable,
			'table'				=> $table,
			'has_name'			=> $hasName,
			'is_checkbox'		=> $is_checkbox
			);
}


function contactStaticFieldsInfo($query){
	$fieldLabelName = '';
	$ifCondition = '';
	$isDD = false;
	$selectizeConfig = '';
	$selectizeOptions = '';
	$model = '';
	$isCustom = false;
	$sortOrder = $query['sort_order'];
	$hasTable = false;
	$table = '';
	$isDate = false;

	switch($query['field']){
		case 'title':
			$fieldLabelName = gm('Salutation');
			$ifCondition = 'title_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_contact_title';
			$model = 'title_contact_id';
			$hasTable = true;
			$table = 'customer_contact_title';
			break;
		case 'gender':
			$fieldLabelName = gm('Gender');;
			$ifCondition = 'gender_if';
			$isDD = true;
			$selectizeConfig = 'cfg';
			$selectizeOptions = 'gender_dd';
			$model = 'sex'; 
			break;
		case 'language':
			$fieldLabelName = gm('Language');
			$ifCondition = 'language_if';
			$isDD = true;
			$selectizeConfig = 'cfgLng';
			$selectizeOptions = 'lang_dd';
			$model = 'language';
			break;
		case 'mobile':
			$fieldLabelName = gm('Cell');
			$ifCondition = 'mobile_if';
			$model = 'cell';
			break;
		case 'birthdate':
			$fieldLabelName = gm('Birthdate');
			$ifCondition = 'birthdate_if';
			$isDate = true;
			$model = 'birthdate';
			break;
	}

	return array('label' 			=> $fieldLabelName,
			'if_condition' 		=> $ifCondition,
			'is_dd'		   		=> $isDD,
			'selectize_config' 	=> $selectizeConfig,
			'selectize_options' => $selectizeOptions,
			'model'				=> $model,
			'is_custom'			=> $isCustom,
			'sort_order'		=> $sortOrder,
			'has_table'			=> $hasTable,
			'table'				=> $table,
			'is_date'			=> $isDate,
			);
}


function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

function get_function($name){
	$q = strtolower(trim($in["term"]));
	$q = $name;
	$filter = " 1=1 ";
	if($q){
		$filter = " name LIKE '%".$q."%' ";
	}
	$result = array();
	$db= new sqldb();
	$func = $db->query("SELECT * FROM customer_contact_job_title WHERE $filter ORDER BY sort_order ")->getAll();
	foreach ($func as $key => $value) {
		$result[] = array('id'=>$value['id'], 'name'=>$value['name']);
	}
	return $result;
}

function get_customers($in){
	$db= new sqldb();
	$q = strtolower(trim($in["term"]));
	$filter =" AND is_admin='0' ";
	if($in['customer_id']){
		$c_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($c_name){
			$filter .= " AND name LIKE '%".addslashes($c_name)."%' ";
		}
	}
	if($q){
		$filter .= " AND name LIKE '%".$q."%' ";
	}
	$result = array();
	if($in['search']){
		$func = $db->query("SELECT customers.name,customers.customer_id,customers.type FROM customers
				WHERE customers.active=1 AND customers.name LIKE '%".$in['search']."%'  ORDER BY customers.name")->getAll();
	}else{
	$func = $db->query("SELECT customers.name,customers.customer_id,customers.type FROM customers
				WHERE customers.active=1 AND customers.customer_id NOT IN (SELECT customer_id FROM customer_contactsIds WHERE customer_id!='0' AND contact_id='".$in['contact_id']."' ) $filter ORDER BY customers.name LIMIT 50")->getAll();
	}
	foreach ($func as $key => $value) {
		$addresses=array();	
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
		 FROM customer_addresses
		 LEFT JOIN country ON country.country_id=customer_addresses.country_id
		 LEFT JOIN state ON state.state_id=customer_addresses.state_id
		 WHERE customer_addresses.customer_id='".$value['customer_id']."'   
		 ORDER BY customer_addresses.address_id");
		while($address->next()){
		  	$a = array(
		  		'symbol'				=> '',
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> ($address->f('address')),
			  	'top'				=> ($address->f('address')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
		}
        $add_new_location_button = array('address_id'=>'99999999999','top'=>'');
        array_push($addresses, $add_new_location_button);
		if($value['type']==0){
			$value['type'] = 'building';
			$value['type_name'] = 'Company';
		}elseif($value['type']==1){
			$value['type'] = 'user';
			$value['type_name'] = 'Individual';
		}else{
			$value['type'] ='';
			$value['type_name'] ='';
		}
        if ($in['from_contact']) {
            $model_value = $db->field("SELECT address_id FROM customer_addresses WHERE customer_addresses.customer_id = '". $in['customer_id'] ."' ORDER BY address_id DESC");
            $result[] = array('id'=>$value['customer_id'], 'name'=>$value['name'],  'type'=>$value['type'],  'type_name'=>$value['type_name'], 'addresses'=>$addresses, 'model_value' => $model_value);
        } else {
            $result[] = array('id'=>$value['customer_id'], 'name'=>$value['name'],  'type'=>$value['type'],  'type_name'=>$value['type_name'], 'addresses'=>$addresses);
        }
	}
	return $result;
}

function get_contactAccounts($in){
	$db= new sqldb();
/*	$position 				= build_contact_function('',true);
	$department				= build_contact_dep_type_dd('',true);
	$title					= build_contact_title_type_dd('',true);*/
	$result = $db->query("SELECT customers.name, customers.type, customer_contactsIds.* FROM customers INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id WHERE customer_contactsIds.contact_id='".$in['contact_id']."' ORDER BY customer_contactsIds.primary DESC , customers.name ASC  ")->getAll();
	$i = 0;

	foreach ($result as $key => $value) {
		$addresses=array();	
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
		 FROM customer_addresses
		 LEFT JOIN country ON country.country_id=customer_addresses.country_id
		 LEFT JOIN state ON state.state_id=customer_addresses.state_id
		 WHERE customer_addresses.customer_id='".$value['customer_id']."'   
		 ORDER BY customer_addresses.address_id");
		while($address->next()){
		  	$a = array(
		  		'symbol'				=> '',
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> ($address->f('address')),
			  	'top'				=> ($address->f('address')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
		}
		$result[$key]['addresses']=$addresses;
		if($result[$key]['type']==0){
			$result[$key]['type'] = 'building';
			$result[$key]['type_name'] = 'Company';
		}elseif($result[$key]['type']==1){
			$result[$key]['type'] = 'user';
			$result[$key]['type_name'] = 'Individual';
		}else{
			$result[$key]['type'] ='';
			$result[$key]['type_name'] ='';
		}
		$result[$key]['s_email'] = $result[$key]['s_email'] == 1 ? true : false;
		/*foreach ($position as $k => $v) {
			if($value['position'] == $v['id']){
				$result[$key]['position_txt'] = $v['name'];
			}
		}*/
		$result[$key]['position_txt'] 		= $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value['position']."'");
		$result[$key]['department_txt'] 	= $db->field("SELECT name FROM customer_contact_dep WHERE id='".$value['department']."'");
		$result[$key]['title_txt'] 			= $db->field("SELECT name FROM customer_contact_title WHERE id='".$value['title']."'");
		/*foreach ($department as $k => $v) {
			if($value['department'] == $v['id']){
				$result[$key]['department_txt'] = $v['name'];
			}
		}*/
		if(!$result[$key]['position']){
			$result[$key]['position']=undefined;
		}
		if(!$result[$key]['department']){
			$result[$key]['department']=undefined;
		}
		$result[$key]['s_email_txt'] = $result[$key]['s_email'] == 1 ? gm('Yes') : gm('No');
		// $result[$key]['s_email'] = $result[$key]['s_email'] == 1 ? true : false;
		if($i==0){
			$result[$key]['collapse'] = true;
		}
		$i++;
	}
	return $result;
}

function get_contactAccount($in){
	$db= new sqldb();
	$result = $db->query("SELECT customers.name, customer_contactsIds.* FROM customers INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id WHERE customer_contactsIds.contact_id='".$in['contact_id']."' ORDER BY customer_contactsIds.primary DESC , customers.name ASC LIMIT 1 ")->getAll();
	$i = 0;
	foreach ($result as $key => $value) {
		$addresses=array();	
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
		 FROM customer_addresses
		 LEFT JOIN country ON country.country_id=customer_addresses.country_id
		 LEFT JOIN state ON state.state_id=customer_addresses.state_id
		 WHERE customer_addresses.customer_id='".$value['customer_id']."'   
		 ORDER BY customer_addresses.address_id");
		while($address->next()){
		  	$a = array(
		  		'symbol'				=> '',
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> ($address->f('address')),
			  	'top'				=> ($address->f('address')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
		}
		$result[$key]['addresses']=$addresses;

		$result[$key]['s_email'] = $result[$key]['s_email'] == 1 ? true : false;
		/*foreach ($position as $k => $v) {
			if($value['position'] == $v['id']){
				$result[$key]['position_txt'] = $v['name'];
			}
		}*/
		$result[$key]['position_txt'] 		= $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value['position']."'");
		$result[$key]['department_txt'] 	= $db->field("SELECT name FROM customer_contact_dep WHERE id='".$value['department']."'");
		$result[$key]['title_txt'] 			= $db->field("SELECT name FROM customer_contact_title WHERE id='".$value['title']."'");
		/*foreach ($department as $k => $v) {
			if($value['department'] == $v['id']){
				$result[$key]['department_txt'] = $v['name'];
			}
		}*/
		if(!$result[$key]['position']){
			$result[$key]['position']=undefined;
		}
		if(!$result[$key]['department']){
			$result[$key]['department']=undefined;
		}
		$result[$key]['s_email_txt'] = $result[$key]['s_email'] == 1 ? gm('Yes') : gm('No');
		// $result[$key]['s_email'] = $result[$key]['s_email'] == 1 ? true : false;
		if($i==0){
			$result[$key]['collapse'] = true;
		}
		$i++;
	}
	return $result;
}


// pentru popularea noii tabele customer_contactsIds
// insert into customer_contactsIds (customer_id,contact_id) select customer_id, contact_id FROM customer_contacts WHERE customer_id<>'0'

?>