<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

	global $config,$database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
	$db=new sqldb();
	$result=array('list'=>array(),'customer_id'=>$in['customer_id']);
	$ch = curl_init();
	//$token_id=$db_users->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
	$token_id=$db_users->field("SELECT token FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

	$contact_email=$db->query("SELECT customer_contacts.contact_id,customer_contacts.email as main_email,customer_contactsIds.email,customer_contactsIds.primary FROM customer_contacts
				LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND customer_contactsIds.customer_id='".$in['customer_id']."'
				 WHERE customer_contacts.customer_id='".$in['customer_id']."'");
	while($contact_email->next()){
		if($contact_email->f('email')){
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/messages?any_email='.urlencode($contact_email->f('email')));

		    $put = curl_exec($ch);
			$info = curl_getinfo($ch);

			if($info['http_code']>300 || $info['http_code']==0){
	    		//do nothing
	    	}else{
	    		$obj=json_decode($put);
	    		foreach($obj as $key=>$value){
	    			$exist=$db->field("SELECT customer_contact_activity_id FROM customer_contact_activity WHERE nylas_email_id='".$value->id."' ");
	    			if(!$exist){
	    				$new_val=$value;
	    				$new_val->datef=date(ACCOUNT_DATE_FORMAT,$value->date);
	    				$new_val->customer_id=$in['customer_id'];
	    				$new_val->contact_id=$contact_email->f('contact_id');
	    				array_push($result['list'], $new_val);
	    			}		
	    		}
	    		
	    	}
		}else if($contact_email->f('main_email')){
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/messages?any_email='.urlencode($contact_email->f('main_email')));

		    $put = curl_exec($ch);
			$info = curl_getinfo($ch);
			
			if($info['http_code']>300 || $info['http_code']==0){
	    		//do nothing
	    	}else{
	    		$obj=json_decode($put);
	    		foreach($obj as $key=>$value){
	    			$exist=$db->field("SELECT customer_contact_activity_id FROM customer_contact_activity WHERE nylas_email_id='".$value->id."' ");
	    			if(!$exist){
	    				$new_val=$value;
	    				$new_val->datef=date(ACCOUNT_DATE_FORMAT,$value->date);
	    				$new_val->customer_id=$in['customer_id'];
	    				$new_val->contact_id=$contact_email->f('contact_id');
	    				array_push($result['list'], $new_val);
	    			}
	    		}
	    		
	    	}

		}	
	}

	json_out($result);
?>