<?php

	class DealsCtrl extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);
		}

		public function get_Data(){
			$in=$this->in;
			$output=array();
			if(!$in['user_id']){
				$in['user_id']='0';
			}
			$output['board_list']=$this->get_BoardsList($in);

			$output['view']= defined('DEAL_VIEW_LIST') && DEAL_VIEW_LIST == 1 ? 1 : 0;
			if($output['view']==1 && !$in['board_id']){
				$in['board_id']=$this->db->field("SELECT id FROM tblopportunity_board ORDER BY name ASC");
			}

			if($in['stage_id']){
				$output['stage_id']=$in['stage_id'];
			}
			if($in['user_id']){
				$output['user_id']=$in['user_id'];
			}
			if($in['search']){
				$output['search']=$in['search'];
			}
			if($in['manager_id']){
				$output['manager_id']=$in['manager_id'];
			}
			if($in['start_date_js']){
				$output['start_date_js']=$in['start_date_js'];
			}
			if($in['stop_date_js']){
				$output['stop_date_js']=$in['stop_date_js'];
			}

			if(!empty($in['start_date_js'])){
			    $in['start_date'] =strtotime($in['start_date_js']);
			    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
			    $output['start_date']=$in['start_date'];
			}
			if(!empty($in['stop_date_js'])){
			    $in['stop_date'] =strtotime($in['stop_date_js']);
			    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
			    $output['stop_date']=$in['stop_date'];
			}

			$output['board_id']=$in['board_id'];
			$output['stage_list']=$this->get_StagesList($in);
			$output['deals_list']=$this->get_DealsList($in);
			$output['user_list']=$this->get_UserList($in);
			$output['manager_list']=$this->get_ManagerList($in);
			//$output['user_id']=$_SESSION['u_id'];		
			for ($j = 1; $j<=14; $j++){
				$is = false;
				if(in_array($j,perm::$allow_apps)){
					$is = true;
				}
				
				$val =$this->db_users->field("SELECT value FROM user_meta WHERE name='MODULE_".$j."' AND user_id='".$_SESSION['u_id']."' ");
				if($val===NULL){
					if(defined('MODULE_'.$j)){
						$val = constant('MODULE_'.$j);
					}
				}
				if($val == 0){
					$is = false;
				}
				
				$output['is_'.$j]=$is;
			}
			$output['ADV_CRM']= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;
			$output['columns']=array();
			$cols_default=default_columns_dd(array('list'=>'deals'));
			$cols_order_dd=default_columns_order_dd(array('list'=>'deals'));
			$cols_selected=$this->db->query("SELECT * FROM column_settings WHERE list_name='deals' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
			if(!count($cols_selected)){
				$i=1;
				foreach($cols_default as $key=>$value){
					$tmp_item=array(
						'id'				=> $i,
						'column_name'		=> $key=='created' ? 'close_date' : $key,
						'name'				=> $value,
						'order_by'			=> $cols_order_dd[$key]
					);
					array_push($output['columns'],$tmp_item);
					$i++;
				}
			}else{
				foreach($cols_selected as $key=>$value){
					$tmp_item=array(
						'id'				=> $value['id'],
						'column_name'		=> $value['column_name']=='created' ? 'close_date' : $value['column_name'],
						'name'				=> $cols_default[$value['column_name']],
						'order_by'			=> $cols_order_dd[$value['column_name']]
					);
					array_push($output['columns'],$tmp_item);
				}
			}		
			
			$this->out = $output;
		}

		public function get_BoardsList(&$in){
			$q = strtolower($in["term"]);
			$filter=" 1=1 ";
			if($q){
				$filter .=" AND tblopportunity_board.name LIKE '%".addslashes($q)."%'";
			}
			$result = array();

			$boards=$this->db->query("SELECT tblopportunity_board.* FROM tblopportunity_board WHERE ".$filter." ORDER BY tblopportunity_board.name ASC");
			while($boards->next()){
				$stages_nr=$this->db->field("SELECT COUNT(id) FROM tblopportunity_stage WHERE board_id='".$boards->f('id')."' ");
				array_push($result,array('id'=>$boards->f('id'),'value'=>stripslashes($boards->f('name')),'stage_nr'=>$stages_nr));
			}
			$crm_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
			/*if(!$in['only_boards'] && ($_SESSION['group']=='admin' || $crm_admin)){
				if($q){
					array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
				}else{
					array_push($result,array('id'=>'99999999999','value'=>''));
				}
			}*/
			if(!$result && $in['xget']){
				json_out();
			}
			return $result;
		}

		public function get_Board(&$in){
			$result=array('stages'=>array(),'deals_dd'=>array());
			if($in['board_id']=='tmp'){
				$result['title']=gm('New Board');
			}else{
				$result['title']=gm('Edit Board');
				$result['name']=$this->db->field("SELECT name FROM tblopportunity_board WHERE id='".$in['board_id']."' ");
				$stages=$this->db->query("SELECT * FROM tblopportunity_stage WHERE board_id='".$in['board_id']."' ORDER BY sort_order ASC");
				while($stages->next()){
					$item=array(
						'id' 	=>  $stages->f('id'),
						'name'	=> stripslashes($stages->f('name')),
						'rate'	=> $stages->f('rate'),
						);
					array_push($result['stages'],$item);
				}
			}
			for($i=0;$i<=110;$i+=10){
				if($i==100){
					array_push($result['deals_dd'], array('id'=>$i, 'name'=> gm('Won')));
				}else if($i==110){
					array_push($result['deals_dd'], array('id'=>$i, 'name'=> gm('Lost')));
				}else{
					array_push($result['deals_dd'], array('id'=>$i, 'name'=> $i.'%'));
				}
			}
			$result['board_id']=$in['board_id'];
			return $result;
		}

		public function get_StagesList(&$in){
			$q = strtolower($in["term"]);
			$filter=" 1=1 ";
			if($q){
				$filter .=" AND tblopportunity_stage.name LIKE '%".addslashes($q)."%'";
			}
			if($in['board_id']){
				$filter .=" AND tblopportunity_stage.board_id ='".$in['board_id']."'";
			}
			$result = array();

			$stages=$this->db->query("SELECT tblopportunity_stage.* FROM tblopportunity_stage WHERE ".$filter." ORDER BY tblopportunity_stage.name ASC");
			while($stages->next()){
				array_push($result,array('id'=>$stages->f('id'),'value'=>stripslashes($stages->f('name'))));
			}
			
			if(!$result && $in['xget']){
				json_out();
			}
			return $result;
		}

		public function get_DealsList(&$in){
            if ($in['reset_list']) {
                unset($_SESSION['selected_items']);
            }
			$result=array('list'=>array());
/*			if(!$in['board_id']){
				return $result;
			}*/

			if(($in['view'] || $in['view'] == '0') && !$in['archived']){
				$in['do_continue']=true;
				ark::run('customers--deals-save_view_list');
				$result['view'] = $in['view'];
			} else {
				$result['view']= defined('DEAL_VIEW_LIST') && DEAL_VIEW_LIST == 1 ? 1 : 0;
			}

			$order_by_array = array('serial_number','opportunity_date','buyername','expected_revenue' ,'subject');
			$filter=" ";
			//$crm_user_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_1' ");
			//$crm_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
			$crm_admin=aktiUser::get('is_admin_company');
		//	$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$in['user_id']."' ");
			$l_r=ROW_PER_PAGE;

			if((!$in['offset']) || (!is_numeric($in['offset'])))
			{
			    $offset=0;
			    $in['offset']=1;
			}
			else
			{
			    $offset=$in['offset']-1;
			}

			if(!$result['view']){
				$order_by = " ORDER BY tblopportunity_stage.board_id ASC, tblopportunity_stage.id ASC, tblopportunity.opportunity_date DESC ";
			}else{
				$order_by = " ORDER BY tblopportunity.sort_order ASC ";
			}
			
			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					//if($in['desc'] == 'true'){
					if($in['desc']=='1' || $in['desc'] == 'true') {
						$order = " DESC ";
					}
					if($in['order_by']=='cus_name'){
						$in['order_by']='customers.name';
					}elseif($in['order_by']=='function'){
						$in['order_by']='customer_contactsIds.position';
					}else{

					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
					$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];				
				}
			}


			if(!empty($in['start_date_js'])){
			    $in['start_date'] =strtotime($in['start_date_js']);
			    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
			}
			if(!empty($in['stop_date_js'])){
			    $in['stop_date'] =strtotime($in['stop_date_js']);
			    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
			}	

			if(!$in['archived']){
				$filter.= " AND tblopportunity.f_archived=0 ";
			}else{
				$filter.= " AND tblopportunity.f_archived=1 ";
				$arguments.="&archived=".$in['archived'];
				$arguments_a = "&archived=".$in['archived'];
			}

			//Removed for now to show results only created by user 
			// if($_SESSION['group']=='admin' || $crm_admin){
			// 	if($admin_licence != '3' && !ONLY_IF_ACC_MANAG && !$crm_user_admin ){
			// 		//$filter.= " AND tblopportunity.created_by='".$in['user_id']."' ";
			// 	}else if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !$crm_user_admin && $in['user_id']){
			// 		$filter.= " AND (tblopportunity.created_by='".$in['user_id']."' OR CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$in['user_id'].",%' ) ";
			// 	}
			// }else{
			// 	if($in['user_id']==$_SESSION['u_id'] || $admin_licence == '3' || $crm_user_admin){
			// 		if(!ONLY_IF_ACC_MANAG){
			// 			//$filter.= " AND tblopportunity.created_by='".$_SESSION['u_id']."' ";
			// 		}else{
			// 			$filter.= " AND (tblopportunity.created_by='".$_SESSION['u_id']."' OR CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ) ";
			// 		}
			// 	}else{
			// 		if(!ONLY_IF_ACC_MANAG){
			// 			//$filter=" 1=2 ";
			// 		}else{
			// 			$filter.= " AND ( CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$in['user_id'].",%' AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%') ";
			// 		}
			// 	}
			// }

			//Get results based on the user that created the deals or deals subject
			if($in['user_id']){
				$filter.= " AND tblopportunity.created_by='".$in['user_id']."' ";
			}

			if($in['search']){
				$filter .= " AND (customers.name LIKE '%".$in['search']."%' OR tblopportunity.subject LIKE '%".$in['search']."%' OR tblopportunity.serial_number LIKE '%".$in['search']."%') "; 
			}
			if(!empty($in['start_date']) && !empty($in['stop_date'])){
				    $filter.=" AND tblopportunity.exp_close_date BETWEEN '".$in['start_date']."' AND '".$in['stop_date']."' ";
				    $arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
				}
				else if(!empty($in['start_date'])){
				    $filter.=" AND cast(tblopportunity.exp_close_date as signed) > ".$in['start_date']." ";
				    $arguments.="&start_date=".$in['start_date'];
				}
				else if(!empty($in['stop_date'])){
				    $filter.=" AND cast(tblopportunity.exp_close_date as signed) < ".$in['stop_date']." ";
				    $arguments.="&stop_date=".$in['stop_date'];
				}

			if($in['manager_id']){
				//$filter = " AND tblopportunity.assigned_to='".$in['manager_id']."' ";
				$filter .= " AND tblopportunity.created_by='".$in['manager_id']."' ";
			}

			$filter_b = ' WHERE 1=1 ';
			if($in['board_id']){
			 $filter_b .= " AND board_id='".$in['board_id']."' ";
			}
			if($in['stage_id']){
				$filter_b .= " AND id='".$in['stage_id']."' ";
			}

			$deals_nr=0;
			$navigator_list = array();
			

			if(!$result['view']){
				$navigator_list = $this->db->query("SELECT tblopportunity.opportunity_id FROM tblopportunity_stage
					LEFT JOIN tblopportunity ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
				 ".$filter_b."  ".$filter." ")->getAll();
				$deals_nr = count($navigator_list);

				$stages_ar=array();
				$deals_ar=array();
				$deals_list='';
				$stages_nr=$this->db->query("SELECT tblopportunity.stage_id, tblopportunity.opportunity_id FROM tblopportunity_stage
					LEFT JOIN tblopportunity ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
				 ".$filter_b." ".$filter." ORDER BY  tblopportunity_stage.board_id ASC, tblopportunity_stage.id ASC, tblopportunity.opportunity_date DESC LIMIT ".$offset*$l_r.",".$l_r." ");
				while($stages_nr->next()){
					$stages_ar[$stages_nr->f('stage_id')]=1;
					$deals_list .="'".$stages_nr->f('opportunity_id')."',";
				}
				$stages_nr_str="";
				foreach($stages_ar as $key=>$value){
					$stages_nr_str.="'".$key."',";
				}
				$stages_nr_str=rtrim($stages_nr_str,",");
				$deals_list=rtrim($deals_list,",");
				if($deals_list){
					$filter_d = " AND tblopportunity.opportunity_id IN (".$deals_list.") ";
				}

				if($stages_nr_str){
					$filter_b.=" AND id IN (".$stages_nr_str.") ";
				}
				//$stages=$this->db->query("SELECT * FROM tblopportunity_stage WHERE id IN (".$stages_nr_str.") ORDER BY sort_order ASC");
			}else{
				if(!$in['board_id']){
					$in['board_id']=$this->db->field("SELECT id FROM tblopportunity_board ORDER BY name ASC");
					$filter_b .= " AND board_id='".$in['board_id']."' ";
				}
				$filter_d ='';
			}
			
			if(!$result['view']){
				$stages=$this->db->query("SELECT * FROM tblopportunity_stage ".$filter_b." ORDER BY  tblopportunity_stage.board_id ASC, tblopportunity_stage.id ASC ");
			}else{
				$navigator_list = $this->db->query("SELECT tblopportunity.opportunity_id FROM tblopportunity_stage
					LEFT JOIN tblopportunity ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
				 ".$filter_b."  ".$filter." ")->getAll();
				$deals_nr = count($navigator_list);

				$stages=$this->db->query("SELECT * FROM tblopportunity_stage ".$filter_b." ORDER BY sort_order ASC");
			}
			
			$ind=0;
			//$navigator_list=array();
			$users_colors=array();
			while($stages->next()){
				$deals=$this->db->query("SELECT tblopportunity.*, customers.name as buyername FROM tblopportunity
					INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
					WHERE tblopportunity.stage_id='".$stages->f('id')."' ".$filter_d." ".$filter." ".$order_by);
				$tmp_deals=array();
				$count_o = 0;
				$total=0;
				
				while($deals->next()){

					$total+=$deals->f('expected_revenue');
					$count_o++;
					$has_doc=false;
					$linked_doc=$this->db->field("SELECT tracking_line.trace_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.origin_id='".$deals->f('opportunity_id')."' AND tracking.archived='0' ");
					if($linked_doc){
						$has_doc=true;
						$linked_data=$this->db->query("SELECT * FROM tracking WHERE trace_id='".$linked_doc."' ");
						while($linked_data->next()){
							$doc_data=$this->get_Details($linked_data->f('target_id'),$linked_data->f('target_type'));
						}
					}
					$user_avatar=$this->db_users->field("SELECT avatar FROM users WHERE user_id='".$deals->f('created_by')."' ");
					$author =get_user_name($deals->f('created_by'));
					$words = explode(' ', $author);
					$letters = $words[0][0]. $words[1][0];
					if(!array_key_exists($deals->f('created_by'),$users_colors)){
						$users_colors[$deals->f('created_by')] = get_user_color($deals->f('created_by'));
					}

					$tmp_item=array(
						'deal_id'			=> $deals->f('opportunity_id'),
						'id'				=> $deals->f('opportunity_id'),
						'subject'			=> $deals->f('subject'),
						'serial_number'		=> $deals->f('serial_number'),
						'buyer_id'			=> $deals->f('buyer_id'),
						'buyer_name'		=> get_customer_name($deals->f('buyer_id')),
						'buyername'			=> $deals->f('buyername'),
						'contact_id'		=> $deals->f('contact_id'),
						'close_date'		=> date(ACCOUNT_DATE_FORMAT,$deals->f('exp_close_date')),
						'deal_date'			=> date(ACCOUNT_DATE_FORMAT,$deals->f('opportunity_date')),
						'amount_free'		=> $deals->f('expected_revenue'),
						'created'			=> $deals->f('created'),
						'author'			=> $author,
						'amount'			=> place_currency(display_number($deals->f('expected_revenue'))),
						'can_edit'			=> ($_SESSION['group']=='admin' || $crm_admin || $deals->f('created_by')==$_SESSION['u_id']) ? true : false,
						'has_doc'			=> $has_doc,
						'doc_data'			=> $doc_data,
						'avatar'			=> $user_avatar,
						'check_add_to_product'	=> $_SESSION['selected_items'][$deals->f('opportunity_id')] == 1 ? true : false,
						'archive_link'      => array('do'=>'customers-deals-deals-archiveDeal', 'opportunity_id'=>$deals->f('opportunity_id')),
						'activate_link'      => array('do'=>'customers-deals-deals-activateDeal', 'opportunity_id'=>$deals->f('opportunity_id')),
						'is_archived'       => $in['archived'] ? true: false,
						'ind'				=> $ind,
						'letters'			=> strtoupper($letters),
						'color'				=> $users_colors[$deals->f('created_by')]
						
					);
					array_push($tmp_deals, $tmp_item);
					//array_push($navigator_list, $deals->f('opportunity_id'));
					$ind++;
				}

				if($result['view'] == 1){
					$exo = array_slice( $tmp_deals, $offset*$l_r, $l_r);
					$count_l = 0;
				    $tmp_a = array();
				       foreach ($exo as $key => $value) {
				           array_push($tmp_a, $value);
				           $count_l++;
				       }
				    $tmp_deals = $tmp_a;
				} 

				$list=array(
					'stage_id'		=> $stages->f('id'),
					'data'			=> $tmp_deals,
					'name'			=> stripslashes($stages->f('name')),
					'rate'			=> $stages->f('rate'),
					'total'			=> display_number($total),
					'count_o'		=> $count_o,
					'weighted_total'=> $total > 0 ? display_number(($total * ($stages->f('rate') / 100))) : '0,00',
					'currency'		=> get_currency(ACCOUNT_CURRENCY_TYPE),
					'board_name'	=> $this->db->field("SELECT tblopportunity_board.name FROM tblopportunity_stage
														LEFT JOIN tblopportunity_board ON tblopportunity_stage.board_id = tblopportunity_board.id
														WHERE tblopportunity_stage.id ='".$stages->f('id')."'"),
					);
				array_push($result['list'], $list);
			}
			//aktiDeals::getInstance();
			//aktiDeals::setList($navigator_list);
			$result['nav']=$navigator_list;
			$result['board_id']=$in['board_id'];
			$result['stage_id']=$in['stage_id'];
			$result['manager_id']=$in['manager_id'];
			$result['board_list']=$this->get_BoardsList($in);
			$result['stage_list']=$this->get_StagesList($in);
			$result['manager_list']=$this->get_ManagerList($in);

			$result['start_date']=$in['start_date'];
			$result['start_date_js']=$in['start_date_js'];
			$result['stop_date']=$in['stop_date'];
			$result['stop_date_js']=$in['stop_date_js'];
			
			$result['confirm']           = gm('Confirm');
			$result['ok']                = gm('Ok');
			$result['cancel']            = gm('Cancel');
			$result['max_rows']			 =(int)$deals_nr;
			$result['lr']			 	=(int)$l_r;
			return $result;
		}

		public function get_UserList(&$in){
			$users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY users.last_name ASC");

			$items = array();
			array_push( $items, array( 'id'=>'0','value'=>gm('All') ));
			while($users->next()){
				array_push( $items, array( 'id'=>$users->f('user_id'),'value'=>htmlspecialchars_decode(stripslashes($users->f('last_name').' '.$users->f('first_name'))) ) );
			}
			return $items;
		}

		public function get_ManagerList(&$in){
			$q = strtolower($in["term"]);
			$filter=" ";
		    if($q){
		        $filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
		    }
		    if($in['manager_id']){
		        $filter .=" AND users.user_id='".$in['manager_id']."' ";
		    }
			$result = array();

			$data=$this->db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY last_name ");
			while($data->next()){
				array_push($result,array('id'=>$data->f('user_id'),'value'=>htmlspecialchars_decode(stripslashes($data->f('last_name').' '.$data->f('first_name'))) ));
			}
			
			if(!$result && $in['xget']){
				json_out();
			}
			return $result;
		}
		
		private function get_Details($id,$type){
			$obj=array();
			switch($type){
				case '1':
					//Invoice
				case '7':
					//Proforma
				case '8':
					//Credit Invoice
					$data=$this->db->query("SELECT * FROM tblinvoice WHERE id='".$id."' ");
					$obj['date']=$data->f('invoice_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date')) : '';
					$obj['amount']=display_number($data->f('amount'));
					$obj['serial_number']=gm('Invoice').' '.$data->f('serial_number');
					if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
						$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
					}
					$link_credit_end = '&type='.ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
					if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $data->f('pdf_layout') == 0){
						$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
					}
					if($data->f('type')==2){
						$obj['pdf_link']= 'index.php?do=invoice-invoice_credit_print&id='.$id.'&lid='.$data->f('email_language').$link_credit_end;
					}else{
						$obj['pdf_link']= 'index.php?do=invoice-invoice_print&id='.$id.'&lid='.$data->f('email_language').$link_end;
					}
					$obj['do']='invoice.view';
					$obj['params']=array('invoice_id'=>$id);
					break;
				case '2':
					//Quote
					$data=$this->db->query("SELECT tblquote.*,tblquote_version.version_id FROM tblquote
						INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
						WHERE tblquote.id='".$id."' ");
					$amount = $this->db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$id."' AND version_id='".$data->f('version_id')."' AND show_block_total='0' ");

					if($data->f('discount') && $data->f('apply_discount') > 1){
						$amount = $amount - ($amount*$data->f('discount')/100);
					}

					if($data->f('currency_rate')){
						$amount = $amount*return_value($data->f('currency_rate'));
					}
					$obj['date']=$data->f('quote_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('quote_date')) : '';
					$obj['amount']=display_number($amount);
					$obj['serial_number']=gm('Quote').' '.$data->f('serial_number');
					if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
						$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
					}
					#if we are using a customer pdf template
					if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $data->f('pdf_layout') == 0){
						$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT;
					}
					$obj['pdf_link']='index.php?do=quote-print&id='.$id.'&version_id='.$data->f('version_id').'&lid='.$data->f('email_language').$link_end;
					$obj['do']='quote.view';
					$obj['params']=array('quote_id'=>$id);
					break;
				case '3':
					//Project
					$data=$this->db->query("SELECT * FROM projects WHERE project_id='".$id."' ");
					$obj['date']=$data->f('start_date') ? '<span>'.gm('Start Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('start_date')) : '';
					$obj['amount']=display_number(0);
					$obj['serial_number']=gm('Project').' '.$data->f('serial_number');
					$obj['pdf_link']='';
					$obj['do']='project.edit';
					$obj['params']=array('project_id'=>$id);
					break;
				case '4':
					//Order
					$data=$this->db->query("SELECT * FROM pim_orders WHERE order_id='".$id."' ");
					$obj['date']=$data->f('del_date') ? '<span>'.gm('Delivery Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('del_date')) : '';
					$obj['amount']=display_number($data->f('amount'));
					$obj['serial_number']=gm('Order').' '.$data->f('serial_number');
					$default_lang = $data->f('email_language');
					if(!$default_lang){
						$default_lang = 1;
					}
					if($data->f('pdf_layout')){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						$link_end = '&type='.ACCOUNT_ORDER_PDF_FORMAT;
					}
					$obj['pdf_link']= 'index.php?do=order-order_print&order_id='.$id.'&lid='.$default_lang.$link_end;
					$obj['do']='order.view';
					$obj['params']=array('order_id'=>$id);
					break;
				case '5':
					//Intervention
					$data=$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$id."' ");
					$obj['date']=$data->f('planneddate') ? '<span>'.gm('Planned Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('planneddate')) : '';
					$obj['amount']=display_number(0);
					$obj['serial_number']=gm('Intervention').' '.$data->f('serial_number');
					$obj['pdf_link']='index.php?do=maintenance-print&service_id='.$id;
					$obj['do']='service.view';
					$obj['params']=array('service_id'=>$id);
					break;
				case '6':
					//Purchase Order
					$data=$this->db->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$id."' ");
					$obj['date']=$data->f('del_date') ? '<span>'.gm('Delivery Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('del_date')) : '';
					$obj['amount']=display_number($data->f('amount'));
					$obj['serial_number']=$data->f('serial_number');
					$lid = $data->f('email_language');
					$pdf_del_type = $data->f('p_order_type')? $data->f('p_order_type'):ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
					if(!$lid){
						$lid = 1;
					}
					$obj['pdf_link']='index.php?do=po_order-p_order_print&p_order_id='.$id.'&lid='.$lid.'&type='.$pdf_del_type;
					break;
				case '9':
					//Contracts
					$data=$this->db->query("SELECT * FROM contracts WHERE contract_id='".$id."' ");
					$obj['date']=$data->f('start_date') ? '<span>'.gm('Start Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('start_date')) : '';
					$obj['amount']=display_number($data->f('amount'));
					$obj['serial_number']=gm('Contract').' '.$data->f('serial_number');
					if($data->f('pdf_layout') && $data->f('use_custom_template')==0){
						$link_end='&type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}elseif($data->f('pdf_layout') && $data->f('use_custom_template')==1){
						$link_end = '&custom_type='.$data->f('pdf_layout').'&logo='.$data->f('pdf_logo');
					}else{
						if(ACCOUNT_CONTRACT_PDF_FORMAT){
							$link_end = '&type='.ACCOUNT_CONTRACT_PDF_FORMAT;
						}else{
							$link_end = '&type=1';
						}
					}
					$obj['pdf_link']='index.php?do=contract-print&contract_id='.$id.'&lid='.$data->f('email_language').$link_end;
					$obj['do']='contract.view';
					$obj['params']=array('contract_id'=>$id);
					break;
				case '10':
					//Purchase Invoice
					$data=$this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$id."' ");
					$obj['date']=$data->f('invoice_date') ? '<span>'.gm('Date').'<span> '.date(ACCOUNT_DATE_FORMAT,$data->f('invoice_date')) : '';
					$obj['amount']=display_number($data->f('total'));
					$obj['serial_number']=$data->f('booking_number');
					$obj['pdf_link']='';
					break;
			}
			return $obj;
		}

	}

	$deal = new DealsCtrl($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $deal->output($deal->$fname($in));
	}

	$deal->get_Data();
	$deal->output();



?>