<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$view = new at(ark::$viewpath.$in['filename'].'.html');
$i=0;
$def=$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'  order by default_value desc,sort_order ASC ");

$customiseFields = $def->getAll();

$customiseExtraFields = $db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY sort_order ASC ")->getALl();

$result =  array_merge($customiseFields, $customiseExtraFields);

//Sort all fields
usort($result, "myCustomSort");

foreach ($result as  $res) {
	
	$loop_name = $res['default_value'] == 1 ? 'default_show' : 'just_show';

	if(!empty($res['controller'])){
		$view->assign(array(
			$res['field'] 		=> $res['value'] == 1 ? true : false,
			'itteration'		=> $i,
			'is_custom_field'	=> 'false',
		),$loop_name);
		if($res['field'] =='title' || $res['field'] =='function' || $res['field']=='department' || $res['field'] =='note' || $res['field'] =='phone' || $res['field'] =='mobile' || $res['field'] =='gender' || $res['field'] =='language' || $res['field'] =='email' || $res['field'] =='exact_title' || $res['field'] == 'various1' || $res['field'] =='various2'){
			$var = $res['field'];
			$val = $var.'_name';
			$view->assign(array(
				$var.'_text'	=> $$var,
				$var.'_name' 	=> $$val,
			),$loop_name);

		}
		$i++;
		$view->loop($loop_name);	
	} else {
		$view->assign(array(
			'field_id'			=> $res['field_id'],	                    
			'label'				=> $res['label'],				
			'is_custom_field'	=> 'true',
		),$loop_name);
		$view->loop($loop_name);
	}
}



// while ($def->next()) {
// 	$loop_name = $def->f('default_value') == 1 ? 'default_show' : 'just_show';
// 	$view->assign(array(
// 		$def->f('field')=> $def->f('value') == 1 ? true : false,
// 		'itteration'	=> $i,
// 	),$loop_name);
// 	if($def->f('field')=='title' || $def->f('field')=='function' || $def->f('field')=='department' || $def->f('field')=='note' || $def->f('field')=='phone' || $def->f('field')=='mobile' || $def->f('field')=='gender' || $def->f('field')=='language' || $def->f('field')=='email' || $def->f('field')=='exact_title' || $def->f('field') == 'various1' || $def->f('field')=='various2'){
// 		$var = $def->f('field');
// 		$val = $var.'_name';
// 		$view->assign(array(
// 			$var.'_text'	=> $$var,
// 			$var.'_name' 	=> $$val,
// 		),$loop_name);

// 	}
// 	$i++;
// 	$view->loop($loop_name);
// }




/*$j =0;
$array = array('mobile','language','birthdate','gender','create');
$def=$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'  order by default_value desc ");
while ($def->next()) {
	$value = $def->f('value') == 1 ? true : false;
	if($value == true && in_array($def->f('field'), $array)){
		$i++;
	}
	
	$view->assign(array(
		'is_'.$def->f('field')=> $value,
		'itteration'	=> $i==2  ? true: false,
		'itteration2'	=> $j,
	),'show_default');
	
	if($i >=2 ){
		$i=0;
	}
	if(in_array($def->f('field'), $array)){
		$j++;
	}
	$view->loop('show_default');
	
}*/

function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

?>