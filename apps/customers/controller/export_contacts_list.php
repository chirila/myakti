<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit','786M');
setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

//ark::loadLibraries(array('PHPExcel'));


$filter = "1=1";
$filename ="contacts_report.xls";
/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Contacts report")
               ->setSubject("Contacts report")
               ->setDescription("Contacts export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Contacts");

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(25);

*/

$list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
$list->next();

if($list->f('list_type') == 1){
  $active_c = $db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND active='1' ");
  while ($active_c->next()) {
    $c_active .= $active_c->f('contact_id').',';
  }
  $c_active = rtrim($c_active,',');
  if($c_active){
    $filter .= " AND customer_contacts.contact_id IN (".$c_active.") ";
  }
}else{
  $in['filter'] = unserialize($list->f('filter'));

  if($in['filter'] ){
    $filter_c = '';
    foreach ($in['filter'] as $field => $value){
      if($value){
        if(is_array($value)){
          $val = '';
          foreach ($value as $key){
            $val .= $key.',';
          }
          $value = $val;
        }
        $value = rtrim($value,',');
        if($value){
          if(substr(trim($field), 0, 13)=='custom_dd_id_'){
          $filter_c.= " AND ";
          $v = explode(',', $value);
          foreach ($v as $k => $v2) {
            $filter_c .= "customer_field.value = '".$v2."' OR ";
          }
          $filter_c = rtrim($filter_c,"OR ");
        }elseif(substr(trim($field), 0, 15)=='c_custom_dd_id_'){
           $filter_c.= " AND ";
          $v = explode(',', $value);
          foreach ($v as $k => $v2) {
            $filter_c .= "contact_field.value = '".$v2."' OR ";
          }
          $filter_c = rtrim($filter_c,"OR ");
        }elseif(trim($field) == 'c_type'){
            $filter_c .= " AND ( ";
            $v = explode(',', $value);
            foreach ($v as $k => $v2) {
              $filter_c .= "c_type_name like '%".$v2."%' OR ";
            }
            $filter_c = rtrim($filter_c,"OR ");
            $filter_c .= " ) ";
          }
          elseif(trim($field) == 'position'){
            $filter_c .= " AND ( ";
            $v = explode(',', $value);
            foreach ($v as $k => $v2) {
              $filter_c .= "position_n like '%".$v2."%' OR ";
            }
            $filter_c = rtrim($filter_c,"OR ");
            $filter_c .= " ) ";
          }
          else{
            $filter_c .= " AND ".$field." IN (".$value.") ";
          }
        }
      }
    }
  }
  if(!$filter_c){
    $filter_c = '1=1';
  }
  $filter_c = ltrim($filter_c," AND");

  $filter_c .= " AND customer_contacts.active='1' ";

  $k =  $db->query("SELECT DISTINCT customer_contacts.contact_id
        FROM customer_contacts
        LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
        LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
        LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
        LEFT JOIN customer_type ON customers.c_type=customer_type.id
        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
        LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
        LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
        LEFT JOIN contact_field ON customer_contacts.contact_id=contact_field.customer_id
        WHERE ".$filter_c." ".$order_by." ");
  while ($k->next()) {
    $c_active .= $k->f('contact_id').',';
  }
  $c_active = rtrim($c_active,',');
  if($c_active){
    $filter .= " AND customer_contacts.contact_id IN (".$c_active.") ";
  }
}


if($in['search']){
  $filter .= " AND (customer_contacts.firstname LIKE '%".$in['search']."%' OR customer_contacts.lastname LIKE '%".$in['search']."%' OR customer_contacts.company_name LIKE '%".$in['search']."%')";
  $arguments_s.="&search=".$in['search'];
}

/*$objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A1',gm('Contact id'))
          ->setCellValue('B1',gm('Title'))
          ->setCellValue('C1',gm('First Name'))
          ->setCellValue('D1',gm('Last Name'))
          ->setCellValue('E1',gm('Gender'))
          ->setCellValue('F1',gm('Birthdate'))
          ->setCellValue('G1',gm('Department'))
          ->setCellValue('H1',gm('Function'))
          ->setCellValue('I1',gm('Exact Title'))
          ->setCellValue('J1',gm('Email'))
          ->setCellValue('K1',gm('Phone'))
          ->setCellValue('L1',gm('Cell'))
          ->setCellValue('M1',gm('Language'))
          ->setCellValue('N1',gm('Note'))
         /* ->setCellValue('O1',gm('Address'))
          ->setCellValue('P1',gm('Zip'))
          ->setCellValue('Q1',gm('City'))
          ->setCellValue('R1',gm('Country'))*/
          /*->setCellValue('O1',gm('Company id'))
          ->setCellValue('P1',gm('Company Name'))
          ->setCellValue('Q1',gm('Company Account Manager'))
          ->setCellValue('R1',gm('Type of Company'))
          ->setCellValue('S1',gm('Company VAT'))
          ->setCellValue('T1',gm('Company VAT number'))
          ->setCellValue('U1',gm('Company Country'))
          ->setCellValue('V1',gm('Company City'))
          ->setCellValue('W1',gm('Company Zip'))
          ->setCellValue('X1',gm('Company Address'))
          ->setCellValue('Y1',gm('Company size'))
          ->setCellValue('Z1',gm('E-mailing').'?');*/

$headers = array(gm('Contact id'),
                gm('Title'),
                gm('First Name'),
                gm('Last Name'),
                gm('Gender'),
                gm('Birthdate'),
                gm('Department'),
                gm('Function'),
                gm('Exact Title'),
                gm('Email'),
                gm('Phone'),
                gm('Cell'),
                gm('Language'),
                gm('Note'),
                gm('Company id'),
                gm('Company Name'),
                gm('Company Account Manager'),
                gm('Type of Company'),
                gm('Company VAT'),
                gm('Company VAT number'),
                gm('Company Country'),
                gm('Company City'),
                gm('Company Zip'),
                gm('Company Address'),
                gm('Company size'),
                gm('E-mailing').'?'
  );

$final_data=array();

$arguments =$arguments.$arguments_s;
$list_for = $list->f('list_for');
$group_by = "";

if(!$list_for){
  $group_by = " GROUP BY customer_contacts.contact_id ";
} else {
  $group_by = " GROUP BY customers.customer_id ";
}
/*$info = $db->query("SELECT customer_contacts.*, customer_contact_address.address as c_address, customer_contact_address.city as c_city, customer_contact_address.zip as c_zip, customer_contact_address.country_id as c_country_id, customers.vat_id, customers.customer_id, customers.country_name, customers.city_name, customers.c_type_name, customers.acc_manager_name, customers.zip_name, customers.btw_nr, customers.quote_reference, customers.comp_size, customers.customer_id
          FROM customer_contacts
          LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
          LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
          WHERE ".$filter." ORDER BY customer_contacts.firstname");*/
$info = $db->query("SELECT customer_contacts.*, customer_contact_address.address as c_address, customer_contact_address.city as c_city, customer_contact_address.zip as c_zip, customer_contact_address.country_id as c_country_id, customers.vat_id, customers.customer_id, customers.country_name, customers.city_name, customers.c_type_name, customers.acc_manager_name, customers.zip_name, customers.btw_nr, customers.quote_reference, customers.comp_size, customers.customer_id
          FROM customer_contacts
          LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
          LEFT JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
          LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
          WHERE ".$filter.$group_by." ORDER BY customer_contacts.firstname");
    $i=0;
    $xlsRow = 2;
while ($info->next())
{
  if($info->f('sex')==0)
  {
    $sex = '';
  }
  elseif ($info->f('sex')==1)
  {
    $sex = gm('Male');
  }
  else
  {
    $sex = gm('Female');
  }
  if($info->f('language')!=0)
  {
    //$language = utf8_decode($db->field("SELECT name FROM customer_contact_language WHERE id=".$info->f('language')));
    $language = $info->f('language')<1000? (gm($db->field("SELECT language FROM pim_lang WHERE lang_id = '".$info->f('language')."' "))) :
                      (gm($db->field("SELECT language FROM pim_custom_lang WHERE lang_id = '".$info->f('language')."' ")));
  }
  $date = '';
  if($info->f('birthdate')){
    $date = date('d/m/Y',$info->f('birthdate'));
  }
  $add = '';
  if($info->f('customer_id')){
    $add = $db->field("SELECT address FROM customer_addresses WHERE customer_id='".$info->f('customer_id')."' ");
  }
  $vat = '';
  if($info->f('vat_id')){
    $vat = $db->field("SELECT value FROM vats WHERE vat_id=".$info->f('vat_id'));
  }
  $title='';
  if($info->f('title')){
    $title = $db->field("SELECT name from customer_contact_title WHERE id='".$info->f('title')."' ");
  }
  

  if($info->f('s_email') == 1){
    $email_txt = gm('Yes');
  }else{
    $email_txt = gm('No');
  }
/*  $department_name = '';
  if($info->gf('department')){
    $department_name = $db->field("SELECT name FROM customer_contact_dep WHERE id = '".$info->gf('department')."'");
  }*/

  $contact_new = $db->query("SELECT * FROM customer_contactsIds WHERE contact_id='".$info->f('contact_id')."' AND `primary`='1' ")->getAll();
  $contact_new = $contact_new[0];
 
  $department_new = '';
   if($contact_new['department']){
      $department_new = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact_new['department']."'");
    }
  $fax_new = $contact_new['fax'];
  $exact_title_new=$contact_new['e_title'];
 
  $position_name_new = '';
  if($contact_new['position']){
    $position_ids = explode(',', $contact_new['position']);
    foreach ($position_ids as $key => $position_id) {
      $position = $db->field("SELECT name FROM customer_contact_job_title WHERE id= '".$position_id."' ");
      $position_name_new .= utf8_decode($position).',';
    }
  }
  $position_name_new = rtrim(ltrim($position_name_new,','),',');

  //$address_new = $db->query("SELECT * FROM  customer_contact_address WHERE contact_id='".$contact_new['customer_id']."' and is_primary=1 ");

      /*$objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A'.$xlsRow,$info->f('contact_id'))
          ->setCellValue('B'.$xlsRow,$title)
          ->setCellValue('C'.$xlsRow,$info->f('firstname'))
          ->setCellValue('D'.$xlsRow,$info->f('lastname'))
          ->setCellValue('E'.$xlsRow,$sex)
          ->setCellValue('F'.$xlsRow,$date)
          ->setCellValue('G'.$xlsRow,$department_new)
          ->setCellValue('H'.$xlsRow,$position_name_new)
          ->setCellValue('I'.$xlsRow, $exact_title_new)
          ->setCellValue('J'.$xlsRow,$info->f('email'))
          ->setCellValue('K'.$xlsRow,$info->f('phone'))
          ->setCellValue('L'.$xlsRow,$info->f('cell'))
          ->setCellValue('M'.$xlsRow,$language)
          ->setCellValue('N'.$xlsRow,$info->f('note'))
          /*->setCellValue('O'.$xlsRow,$info->f('c_address'))
          ->setCellValue('P'.$xlsRow,$info->f('c_zip'))
          ->setCellValue('Q'.$xlsRow,$info->f('c_city'))
          ->setCellValue('R'.$xlsRow,get_country_name($info->f('c_country_id')))*/
          /*->setCellValue('O'.$xlsRow,$info->f('customer_id'))
          ->setCellValue('P'.$xlsRow,$info->f('company_name'))
          ->setCellValue('Q'.$xlsRow,$info->f('acc_manager_name'))
          ->setCellValue('R'.$xlsRow,$info->f('c_type_name'))
          ->setCellValue('S'.$xlsRow,$vat)
          ->setCellValue('T'.$xlsRow,$info->f('btw_nr'))
          ->setCellValue('U'.$xlsRow,$info->f('country_name'))
          ->setCellValue('V'.$xlsRow,$info->f('city_name'))
          ->setCellValue('W'.$xlsRow,$info->f('zip_name'))
          ->setCellValue('X'.$xlsRow,$add)
          ->setCellValue('Y'.$xlsRow,$info->f('comp_size'))
          ->setCellValue('Z'.$xlsRow,$email_txt);*/

  $i++;
  $xlsRow++;

  $tmp_item=array(
    $info->f('contact_id'),
    $title,
    $info->f('firstname'),
    $info->f('lastname'),
    $sex,
    $date,
    $department_new,
    $position_name_new,
    $exact_title_new,
    $info->f('email'),
    $info->f('phone'),
    $info->f('cell'),
    $language,
    $info->f('note'),
    $info->f('customer_id'),
    $info->f('company_name'),
    $info->f('acc_manager_name'),
    $info->f('c_type_name'),
    $vat,
    $info->f('btw_nr'),
    $info->f('country_name'),
    $info->f('city_name'),
    $info->f('zip_name'),
    $add,
    $info->f('comp_size'),
    $email_txt
  );
  array_push($final_data,$tmp_item);
}

doQueryLog();
header('Content-Disposition: attachment; filename="contacts_report.csv"');

$fp = fopen('php://output', 'w');

//Add headers
fputcsv($fp, $headers);

//Add Information
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}

fclose($fp);
exit();

$objPHPExcel->getActiveSheet()->setTitle('Contacts list');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
@mkdir('upload/'.DATABASE_NAME,0755);
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);
define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
     setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();
?>