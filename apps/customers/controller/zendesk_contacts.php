<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class ZenCustomer extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}

		/*public function get_list(){
			global $config;
			$in=$this->in;
			$result=array('list'=>array(),'data_incoming'=>array());
			$zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
			$zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
			$zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
			$ch = curl_init();
			$headers=array('Content-Type: application/json');
    		$time_filter='?query='.urlencode('type:user role:end-user');
    		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER' ");
	        if($const){
	            $time_filter.=urlencode(' created>='.date("Y-m-d",(int)$const));
	        }
	        if($in['view']!='1'){
	        	$headers=array('Content-Type: application/json');
		    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			    curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
		    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);

		    	$put = curl_exec($ch);
		    	$info = curl_getinfo($ch);

		    	$data=json_decode($put);
		        foreach($data->results as $key=>$value){
		        	$id2_e=$this->db->field("SELECT id FROM zen_contacts WHERE contact_id='".$value->id."' AND our='1' ");
		        	if(!$id2_e && $value->role=='end-user'){
		        		array_push($result['data_incoming'],$value);
		        	}   	
		        }
	        }        

	        $l_r = ROW_PER_PAGE;
			$order_by_array = array('id');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY id DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			if($in['view']=='1'){
				$filter.=" AND archived='1' ";
			}else{
				$filter.=" AND archived='0' AND created_customer='0' AND created_contact='0' AND our='0' ";
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;	
				}
			}

			$customer_data=$this->db->query("SELECT * FROM zen_contacts ".$filter.$order_by);
			$max_rows=$customer_data->records_count();
			$customer_data->move_to($offset*$l_r);
			$j=0;
			while($customer_data->next() && $j<$l_r){
				$line=array(
						'id'				=> $customer_data->f('id'),
						'contact_id'		=> $customer_data->f('contact_id'),
						'company'			=> $customer_data->f('customer_id') ? $this->db->field("SELECT name FROM customers WHERE zen_organization_id='".$customer_data->f('customer_id')."' ") : '',
						'email'				=> $customer_data->f('email'),
						'first_name'		=> $customer_data->f('first_name'),
						'last_name'			=> $customer_data->f('last_name'),					
						'our'				=> $customer_data->f('our') ? true : false,
						'country_id'		=> ACCOUNT_DELIVERY_COUNTRY_ID,
						'imported'			=> (!$customer_data->f('our') && ($customer_data->f('created_customer') || $customer_data->f('created_contact'))) ? true : false,
					);
				array_push($result['list'], $line);
			}

			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=1;
			$result['do_next']='customers--zendesk-saveContact';
			$result['country_dd']=build_country_list();
			$result['language_dd']=build_language_dd(0);
			$result['gender_dd']= gender_dd();
			$result['title_dd']= build_contact_title_type_dd(0);
			$result['customer_dd']=$this->get_buyers();
			$result['view']=$in['view'];
			$result['active']=$in['view']=='1' ? gm('Active') : gm('Archived');
			$result['step']=1;

			$this->out = $result;
		}*/

		public function get_list(){
			global $config;
			$in=$this->in;
			$result=array('list'=>array());
			$zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
			$zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
			$zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");

			$ch = curl_init();
			$headers=array('Content-Type: application/json');
			$time_filter='?query='.urlencode('type:organization');
    		$const_a=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_ACCOUNT_FILTER' ");
	        if($const_a){
	            $time_filter.=urlencode(' created>='.date("c",(int)$const_a));
	        }
		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);

		    $put = curl_exec($ch);
		    $info = curl_getinfo($ch);

		    $data_acc=json_decode($put);
		    $result['accounts_available']=$data_acc->count;
		    $result['accounts_imported']=$this->db->field("SELECT count(id) FROM zen_accounts");
		    $result['account_custom_fields']=array();
		    $i=0;
		    foreach($data_acc->results[0]->organization_fields as $key=>$value){
		    	$i++;
		    	array_push($result['account_custom_fields'],array('name'=>$key,'id'=>$i));
		    }
		    $result['account_fields']=array();
		    $account_fields=$this->db->query("SELECT label_name, import_label_id FROM import_labels WHERE type = 'customer' AND (tabel_name='customers' OR tabel_name='customer_field' OR tabel_name='customer_contact_language' OR tabel_name='customer_sector' OR tabel_name='customer_activity' OR tabel_name='customer_lead_source' OR tabel_name='sales_rep') AND field_name!='name' AND field_name!='acc_manager_name' ORDER BY import_label_id ASC");
		    while($account_fields->next()){
		    	$item=array(
		    		'id'		=> $account_fields->f('import_label_id'),
		    		'name'		=> stripslashes($account_fields->f('label_name')),
		    		);
		    	array_push($result['account_fields'], $item);
		    }

    		$time_filter='?query='.urlencode('type:user role:end-user');
    		$const_c=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER' ");
	        if($const_c){
	            $time_filter.=urlencode(' created>='.date("c",(int)$const_c));
	        }
		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);

		    $put = curl_exec($ch);
		    $info = curl_getinfo($ch);

		    $data_contact=json_decode($put);
		    $result['contacts_available']=$data_contact->count;
		    $result['contacts_imported']=$this->db->field("SELECT count(id) FROM zen_contacts WHERE our='0' AND created_customer='0' AND created_contact='0' ");
		    
		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/user_fields.json');

		    $put = curl_exec($ch);
		    $info = curl_getinfo($ch);

		    $user_f=json_decode($put);
		    $result['contact_custom_fields']=array();
		    $i=0;
		    foreach($user_f->user_fields as $key=>$value){
		    	$i++;
		    	array_push($result['contact_custom_fields'],array('name'=>$value->key,'id'=>$i));
		    }
		    $result['contact_fields']=array();
		    $contact_fields=$this->db->query("SELECT label_name, import_label_id FROM import_labels WHERE type = 'contact' AND tabel_name!='customer_contact_address' 
		    	AND tabel_name!='customer_contact_language' AND field_name!='phone' AND field_name!='cell'
		    	AND field_name!='firstname' AND field_name!='email' AND label_name!='Last Name' ORDER BY import_label_id ASC");
		    while($contact_fields->next()){
		    	$item=array(
		    		'id'		=> $contact_fields->f('import_label_id'),
		    		'name'		=> stripslashes($contact_fields->f('label_name')),
		    		);
		    	array_push($result['contact_fields'], $item);
		    }
		    $result['contact_custom_fields_matched']=$result['contact_custom_fields'];
		    $custom_fields=$this->db->query("SELECT * FROM zen_fields WHERE `master`='contact' ")->getAll();
		    if(!empty($custom_fields)){
		    	foreach($custom_fields as $key=>$value){
		    		foreach($result['contact_custom_fields_matched'] as $key1=>$value1){
		    			if($value1['name']==$value['zen_field']){
		    				$result['contact_custom_fields_matched'][$key1]['custom_field_id']=$value['our_field'];
		    				break;
		    			}
		    		}
		    	}
		    }
		    $const_c_mod=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER_MOD' ");
		    if(!$const_c_mod){
		    	if($const_c){
		    		$time_filter='?query='.urlencode('type:user role:end-user updated>='.date("c",(int)$const_c));
				    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
					curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
				    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);

				    $put = curl_exec($ch);
				    $info = curl_getinfo($ch);

				    $data_mod_contact=json_decode($put);
				    $result['contacts_changed']=$data_mod_contact->count;
		    	}
		    }else{
		    	$time_filter='?query='.urlencode('type:user role:end-user updated>='.date("c",(int)$const_c_mod));
			    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
				curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);

			    $put = curl_exec($ch);
			    $info = curl_getinfo($ch);

			    $data_mod_contact=json_decode($put);
			    $result['contacts_changed']=$data_mod_contact->count;
		    }
		    $result['step']=0;
		    $result['view']=$in['view'];
		    $result['zen_sync_new_c']=defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1 ? true : false;
		    $result['zen_sync_old_c']=defined('ZEN_SYNC_OLD_C') && ZEN_SYNC_OLD_C==1 ? true : false;
		    $result['zen_sync_inc_c']=defined('ZEN_SYNC_INC_C') && ZEN_SYNC_INC_C==1 ? true : false;
		    $result['can_sync']=($const_c_mod || $const_c) ? true : false;
		    $result['one_time']=(!$const_c_mod && !$const_c) ? true : false;
			$this->out = $result;
		}

		public function get_contacts(){
			$in=$this->in;
			$output=array('list'=>array());
			$l_r = ROW_PER_PAGE;
			$order_by_array = array('lastname','firstname','cus_name','function');
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}
			$filter = " customer_contacts.front_register = 0 ";
			if(!$in['contact_id']){
				$filter.=" AND customer_contacts.zen_contact_id='' ";
			}else{
				$filter.=" AND customer_contacts.zen_contact_id='".$in['contact_id']."' ";
			}
			if(empty($in['archived'])){
				$filter.= " AND customer_contacts.active=1";
			}else{
				$in['archived'] = 1;
				$filter.= " AND customer_contacts.active=0";
				$arguments.="&archived=".$in['archived'];
				$arguments_a = "&archived=".$in['archived'];
			}

			$order_by = " ORDER BY lastname ";
			$escape_chars = array('(','?','*','+',')');
			if(!empty($in['search'])){
				$filter_x = '';
				// $search_arr = str_split($in['search']);
				$search_arr = str_split(stripslashes($in['search']));
				foreach ($search_arr as $value) {
					if(in_array($value, $escape_chars)){
						$value = '\\'.$value;
					}
					$filter_x .= $value.".?";
				}

				$search_words = explode(' ', $in['search']);
				foreach ($search_words as $word_pos => $searched_word) {
					if(trim($searched_word)){
						
						if($word_pos==0){
							$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
										OR lastname LIKE '%".$searched_word."%'
										OR customers.name LIKE '%".$searched_word."%'
										OR customer_contactsIds.phone LIKE '%".$searched_word."%'
										OR cell LIKE '%".$searched_word."%'
										OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
						}else{
							$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
										OR lastname LIKE '%".$searched_word."%'
										OR customers.name LIKE '%".$searched_word."%'
										OR customer_contactsIds.phone LIKE '%".$searched_word."%'
										OR cell LIKE '%".$searched_word."%'
										OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
						}

					}
				}

				$arguments_s.="&search=".$in['search'];
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					if($in['order_by']=='cus_name'){
						$in['order_by']='customers.name';
					}elseif($in['order_by']=='function'){
						$in['order_by']='customer_contactsIds.position';
					}else{

					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;		
				}
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
				$max_rows =  $this->db->field("SELECT COUNT( contact_id ) AS max_contact_rows 
										FROM customer_contacts
										LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter );
				$c = $this->db->query("SELECT customer_contacts.*, customers.customer_id
							FROM customer_contacts
							LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
							WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();

			}else{
				$max_rows =  $this->db->field("SELECT count(customer_contacts.contact_id) FROM customer_contacts 
							INNER JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND customer_contactsIds.customer_id!='0'
							INNER JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							WHERE ".$filter );
				$c = $this->db->query("SELECT customers.name as comp_name,customer_contactsIds.customer_id,customer_contacts.firstname,customer_contacts.lastname,
							customer_contactsIds.email,customer_contacts.contact_id,
							customer_contactsIds.primary,customer_contacts.zen_contact_id,customers.zen_organization_id,customer_contacts.email as main_email FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
							INNER JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
			}
			$output['max_rows'] =  $max_rows;
			foreach ($c as $key => $value) {
				$item=array(
					'cus_name'			    => $value['comp_name'],
					'customer_id'		    => $value['customer_id'],
					'con_firstname'			=> $value['firstname'] ? $value['firstname'] : '',
					'con_lastname'			=> $value['lastname'] ? $value['lastname'] : '',
					//'con_email'				=> $value['primary'] ? $value['email'] : $value['main_email'],
					'con_email'				=> ($value['primary'] ||  $value['email'] )? $value['email'] : $value['main_email'],
					'contact_id' 			=> $value['contact_id'],
					'sync_done'				=> $value['zen_contact_id'] ? true : false,
					'no_address'			=> empty($addr) ? true : false,
					'zen_contact_id'		=> $value['zen_contact_id'],
					'zen_organization_id'	=> $value['zen_organization_id'],
				);
				array_push($output['list'], $item);
			}
			$output['lr'] = $l_r;
			$output['do_next'] = 'customers--zendesk-sendContact';
			$this->out = $output;
		}

		public function get_buyers($in){
			$q = strtolower(trim($in["term"]));
			$filter =" AND is_admin='0' ";
			if($q){
				$filter = " name LIKE '%".$q."%' ";
			}
			$result = array();
			$func = $this->db->query("SELECT customers.name,customers.customer_id FROM customers
						WHERE customers.active=1 $filter ORDER BY customers.name ")->getAll();
			foreach ($func as $key => $value) {
				$result[] = array('id'=>$value['customer_id'], 'name'=>$value['name']);
			}
			return $result;
		}

	}

	$zen_data = new ZenCustomer($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$zen_data->output($zen_data->$fname($in));
	}

	$zen_data->get_list();
	$zen_data->output();
?>