<?php

class zendesk {

	function __construct() {
		$this->db = new sqldb();
		$this->db2 = new sqldb();
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($database_users);
		$this->field_n='customer_id';
		$this->pag = 'customer';
		$this->pagc = 'xcustomer_contact';
		$this->field_nc = 'contact_id';
	}

	function sendContact(&$in){
		$ch = curl_init();
		$zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
		$in['zen_api']=$zen_data->f('api');
		$in['zen_email']=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
		$in['zen_pass']=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
        $in['zen_organization_id']=$this->db->field("SELECT zen_organization_id FROM customers WHERE customer_id='".$in['customer_id']."' ");

		if($in['zen_organization_id'] == ''){
			$organization_data=array('organization'=>array('name'=>$in['cus_name']));
			$organization_data=json_encode($organization_data);

            $params=urlencode('type:organization name:'.$in['cus_name']);
            $headers=array('Content-Type: application/json');
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
            curl_setopt($ch, CURLOPT_USERPWD, $in['zen_email']."/token:".$in['zen_pass']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $in['zen_api'].'/api/v2/search.json?query='.$params);

            $put = curl_exec($ch);
            $info = curl_getinfo($ch);

            if($info['http_code']>300 || $info['http_code']==0){
                msg::error($in['cus_name'].':'.gm('Failed to create organization'),'error');
                json_out($in);
            }else{
                $org_data=json_decode($put);
                if(empty($org_data->results)){
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                    curl_setopt($ch, CURLOPT_USERPWD, $in['zen_email']."/token:".$in['zen_pass']);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $organization_data);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_URL, $in['zen_api'].'/api/v2/organizations.json');

                    $put = curl_exec($ch);
                    $info = curl_getinfo($ch);

                    if($info['http_code']>300 || $info['http_code']==0){
                        msg::error($in['cus_name'].':'.gm('Failed to create organization'),'error');
                        json_out($in);
                    }else{
                        $data=json_decode($put);
                        $this->db->query("UPDATE customers SET zen_organization_id='".$data->organization->id."' WHERE customer_id='".$in['customer_id']."' ");
                        $in['zen_organization_id']=$data->organization->id;
                        $this->sendContactPlus($in);
                    }
                }else{
                    $this->db->query("UPDATE customers SET zen_organization_id='".$org_data->results[0]->id."' WHERE customer_id='".$in['customer_id']."' ");
                    $in['zen_organization_id']=$org_data->results[0]->id;
                    $this->sendContactPlus($in);
                }
            }
		}else{
			$this->sendContactPlus($in);
		}
	}

	function sendContactPlus(&$in){
		$ch = curl_init();
		$organization_user=array('user'=>array(
			'name'			=> $in['con_firstname'].' '.$in['con_lastname'],
			'email'			=> $in['con_email'],
			'verified'		=> true,
			'role'			=> 'end-user',
			'organization'  => array('name'=>$in['cus_name']),
		));
		$organization_user=json_encode($organization_user);
		$headers=array('Content-Type: application/json');
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
		curl_setopt($ch, CURLOPT_USERPWD, $in['zen_email']."/token:".$in['zen_pass']);
	   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    if($in['sync_done']!=''){
	    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	    }else{
	    	curl_setopt($ch, CURLOPT_POST, true);
	    }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $organization_user);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    if($in['sync_done']!=''){
	    	curl_setopt($ch, CURLOPT_URL, $in['zen_api'].'/api/v2/users/'.$in['zen_contact_id'].'.json');
	    }else{
	    	curl_setopt($ch, CURLOPT_URL, $in['zen_api'].'/api/v2/users.json');	
	    }

	    $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
	    	msg::error($in['con_firstname'].' '.$in['con_lastname'].':'.gm('Failed to send user'),'error');
	    }else{
	    	$data_d=json_decode($put);    	
	    	if($in['sync_done']==''){
                $this->db->query("UPDATE customer_contacts SET zen_contact_id='".$data_d->user->id."' WHERE contact_id='".$in['contact_id']."' ");
	    		$this->db->query("INSERT INTO zen_contacts SET
	    						contact_id='".$data_d->user->id."',
	    						customer_id='".$in['zen_organization_id']."',
                                email='".$in['con_email']."',                          
                                first_name='".$in['con_firstname']."',
                                last_name='".$in['con_lastname']."',
                                our='1'");
	    	}else{
	    		$this->db->query("UPDATE zen_contacts SET
	    						customer_id='".$in['zen_organization_id']."',
                                email='".$in['con_email']."',                          
                                first_name='".$in['con_firstname']."',
                                last_name='".$in['con_lastname']."',
                                our='1'
	    			WHERE contact_id='".$in['zen_contact_id']."'");
	    	}
	    }
	    json_out($in);
	}

	function saveContact(&$in){
		$zen_contact=$this->db->field("SELECT contact_id FROM zen_contacts WHERE contact_id='".$in['id']."' ");
        if($zen_contact){
            $this->db->query("UPDATE zen_contacts SET 
                                customer_id='".$in['organization_id']."',
                                email='".$in['email']."',                          
                                last_name='".$in['name']."'
                                WHERE contact_id='".$in['id']."'");
        }else{
            $last_id=$this->db->insert("INSERT INTO zen_contacts SET
                                contact_id='".$in['id']."',
                                customer_id='".$in['organization_id']."',
                                email='".$in['email']."',                          
                                last_name='".$in['name']."'  ");
            if($in['email']!=''){
              $customer_mail=$this->db->field("SELECT contact_id FROM customer_contactsIds WHERE email='".$in['email']."' ");
              if($customer_mail){
                  $this->db->query("UPDATE zen_contacts SET created_contact='".$customer_mail."' WHERE id='".$last_id."' ");
                  $this->db->query("UPDATE customer_contacts SET zen_contact_id='".$in['id']."' WHERE contact_id='".$customer_mail."' ");
              }
            }
        }
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='ZEN_CONTACT_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='ZEN_CONTACT_FILTER',value='".time()."'");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
	}

	/**
      * undocumented function
      *
      * @return void
      * @author PM
      **/
      function tryAddC(&$in){
        if(!$this->tryAddCValid($in)){ 
          json_out($in);
          return false; 
        }
        //$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
        $name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
        //Set account default vat on customer creation
        $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

        if(empty($vat_regime_id)){
          $vat_regime_id = 0;
        }

        $c_types = '';
        $c_type_name = '';
        if($in['c_type']){
            /*foreach ($in['c_type'] as $key) {
                if($key){
                    $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
                    $c_type_name .= $type.',';
                }
            }
            $c_types = implode(',', $in['c_type']);
            $c_type_name = rtrim($c_type_name,',');*/
            if(is_array($in['c_type'])){
                foreach ($in['c_type'] as $key) {
                    if($key){
                        $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
                        $c_type_name .= $type.',';
                    }
                }
                $c_types = implode(',', $in['c_type']);
                $c_type_name = rtrim($c_type_name,',');
            }else{
                $c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");                
                $c_types = $in['c_type'];
            }
        }


        if($in['user_id']==''){
            $in['user_id']=$_SESSION['u_id'];
        }


        if($in['user_id']){
            /*foreach ($in['user_id'] as $key => $value) {
                $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
                    $acc_manager .= $manager_id.',';
            }           
            $in['acc_manager'] = rtrim($acc_manager,',');
            $acc_manager_ids = implode(',', $in['user_id']);*/
            if(is_array($in['user_id'])){
                foreach ($in['user_id'] as $key => $value) {
                    $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
                        $acc_manager .= $manager_id.',';
                }           
                $in['acc_manager'] = rtrim($acc_manager,',');
                $acc_manager_ids = implode(',', $in['user_id']);
            }else{
                //$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
                $in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
                $acc_manager_ids = $in['user_id'];
            }
        }else{
            $acc_manager_ids = '';
            $in['acc_manager'] = '';
        }
        $vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
            $vat_default=str_replace(',', '.', $vat);
        $selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");

        if($in['add_customer']){
            $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
            $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
            if(SET_DEF_PRICE_CAT == 1){
                $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
            }
            $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                                name='".$in['name']."',
                                                btw_nr='".$in['btw_nr']."',
                                                vat_regime_id     = '".$vat_regime_id."',
                                                city_name = '".$in['city']."',
                                                c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
                                                user_id = '".$acc_manager_ids."',
                                                acc_manager_name = '".addslashes($in['acc_manager'])."',
                                                country_name ='".get_country_name($in['country_id'])."',
                                                active='1',
                                                payment_term      = '".$payment_term."',
                                                payment_term_type     = '".$payment_term_type."',
                                                zip_name  = '".$in['zip']."',
                                                commercial_name         = '".$in['commercial_name']."',
                                                legal_type              = '".$in['legal_type']."',
                                                c_type                  = '".$c_types."',
                                                website                 = '".$in['website']."',
                                                language                = '".$in['language']."',
                                                sector                  = '".$in['sector']."',
                                                comp_fax                = '".$in['comp_fax']."',
                                                activity                = '".$in['activity']."',
                                                comp_size               = '".$in['comp_size']."',
                                                lead_source             = '".$in['lead_source']."',
                                                various1                = '".$in['various1']."',
                                                various2                = '".$in['various2']."',
                                                c_type_name             = '".$c_type_name."',
                                                vat_id                  = '".$selected_vat."',
                                                invoice_email_type      = '1',
                                                sales_rep               = '".$in['sales_rep_id']."',
                                                internal_language       = '".$in['internal_language']."',
                                                is_supplier             = '".$in['is_supplier']."',
                                                is_customer             = '".$in['is_customer']."',
                                                stripe_cust_id          = '".$in['stripe_cust_id']."',
                                                cat_id                  = '".$in['cat_id']."'");
          
            $this->db->query("INSERT INTO customer_addresses SET
                            address='".$in['address']."',
                            zip='".$in['zip']."',
                            city='".$in['city']."',
                            country_id='".$in['country_id']."',
                            customer_id='".$in['buyer_id']."',
                            is_primary='1',
                            delivery='1',
                            billing='1' ");
            $in['customer_name'] = $in['name'];

            if($in['extra']){
                foreach ($in['extra'] as $key => $value) {
                    $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
                    if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
                        $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
                    }
                }
            }

          // include_once('../apps/company/admin/model/customer.php');
            $in['value']=$in['btw_nr'];
          // $comp=new customer();       
          // $this->check_vies_vat_number($in);
            if($this->check_vies_vat_number($in) != false){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
            }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
            }

            /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
            $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                    AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                    
            if(!$show_info->move_next()) {
              /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                name    = 'company-customers_show_info',
                                value   = '1' ");*/
                $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                name    = :name,
                                value   = :value ",
                            ['user_id'=>$_SESSION['u_id'],
                             'name'=>'company-customers_show_info',
                             'value'=>'1']
                        );                                
            } else {
              /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
                $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                    AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);
            }
            insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
            $this->db->query("UPDATE zen_contacts SET created_customer='".$in['buyer_id']."' WHERE contact_id='".$in['contact_id']."' ");
            $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
            if($count == 1){
              doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
            }
            msg::success (gm('Account created'),'success');
        }else if($in['add_individual']){
            $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
            $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
            if(SET_DEF_PRICE_CAT == 1){
                $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
            }
            $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                                name='".$in['lastname']."',
                                                firstname = '".$in['firstname']."',
                                                user_id = '".$acc_manager_ids."',
                                                acc_manager_name = '".addslashes($in['acc_manager'])."',
                                                btw_nr='".$in['btw_nr']."',
                                                vat_regime_id     = '".$vat_regime_id."',
                                                city_name = '".$in['city']."',
                                                c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
                                                type = 1,
                                                country_name ='".get_country_name($in['country_id'])."',
                                                active='1',
                                                payment_term      = '".$payment_term."',
                                                payment_term_type     = '".$payment_term_type."',
                                                zip_name  = '".$in['zip']."',
                                                commercial_name         = '".$in['commercial_name']."',
                                                legal_type              = '".$in['legal_type']."',
                                                c_type                  = '".$c_types."',
                                                website                 = '".$in['website']."',
                                                language                = '".$in['language']."',
                                                sector                  = '".$in['sector']."',
                                                comp_fax                = '".$in['comp_fax']."',
                                                activity                = '".$in['activity']."',
                                                comp_size               = '".$in['comp_size']."',
                                                lead_source             = '".$in['lead_source']."',
                                                various1                = '".$in['various1']."',
                                                various2                = '".$in['various2']."',
                                                c_type_name             = '".$c_type_name."',
                                                vat_id                  = '".$selected_vat."',
                                                invoice_email_type      = '1',
                                                sales_rep               = '".$in['sales_rep_id']."',
                                                internal_language       = '".$in['internal_language']."',
                                                is_supplier             = '".$in['is_supplier']."',
                                                is_customer             = '".$in['is_customer']."',
                                                stripe_cust_id          = '".$in['stripe_cust_id']."',
                                                cat_id                  = '".$in['cat_id']."'");
          
            $this->db->query("INSERT INTO customer_addresses SET
                            address='".$in['address']."',
                            zip='".$in['zip']."',
                            city='".$in['city']."',
                            country_id='".$in['country_id']."',
                            customer_id='".$in['buyer_id']."',
                            is_primary='1',
                            delivery='1',
                            billing='1' ");
            $in['customer_name'] = $in['name'];

            if($in['extra']){
                foreach ($in['extra'] as $key => $value) {
                    $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
                    if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
                        $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
                    }
                }
            }

          // include_once('../apps/company/admin/model/customer.php');
            $in['value']=$in['btw_nr'];
          // $comp=new customer();       
          // $this->check_vies_vat_number($in);
            if($this->check_vies_vat_number($in) != false){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
            }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
            }

            /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
            $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                    AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                    
            if(!$show_info->move_next()) {
              /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                name    = 'company-customers_show_info',
                                value   = '1' ");*/
                $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                name    = :name,
                                value   = :value ",
                            [ 'user_id' => $_SESSION['u_id'],
                              'name'    => 'company-customers_show_info',
                              'value'   => '1']
                        );                                
            }else{
              /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
                $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                    AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                    
            }
            insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
            $this->db->query("UPDATE zen_contacts SET created_customer='".$in['buyer_id']."' WHERE contact_id='".$in['contact_id']."' ");
            $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
            if($count == 1){
              doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
            }
            msg::success (gm('Account created'),'success');
        }
        if($in['add_contact']){
          $customer_name='';
          if($in['buyer_id']){
            $customer_name = $this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' ");
          }
            if($in['is_zen']){
                $firstname=$in['con_firstname'];
                $lastname=$in['con_lastname'];
                $email=$in['con_email'];
            }else{
                $firstname=$in['firstname'];
                $lastname=$in['lastname'];
                $email=$in['email'];
            }
            $contact_id = $this->db->insert("INSERT INTO customer_contacts SET
                                                  firstname='".$firstname."',
                                                  lastname='".$lastname."',
                                                  sex='".$in['gender_id']."',
                                                  title ='".$in['title_id']."',
                                                  language ='".$in['language_id']."',
                                                  `create`  = '".time()."',
                                                  customer_id = '".$in['buyer_id']."',
                                                  company_name= '".$customer_name."',
                                                  email='".$email."',
                                                  cell='".$in['cell']."',
                                                  zen_contact_id='".$in['contact_id']."' ");
            $in['contact_name']=$firstname.' '.$lastname.' >';
            if($in['country_id']){
              $this->db->query("INSERT INTO customer_contact_address SET
                              address='".$in['address']."',
                              zip='".$in['zip']."',
                              city='".$in['city']."',
                              country_id='".$in['country_id']."',
                              contact_id='".$contact_id."',
                              is_primary='1',
                              delivery='1' ");
            }
            /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                        AND name    = 'company-contacts_show_info'  ");*/
            $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                        AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);                                        
            if(!$show_info->move_next()) {
              /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                      name    = 'company-contacts_show_info',
                                      value   = '1' ");*/
                $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                      name    = :name,
                                      value   = :value ",
                                ['user_id' => $_SESSION['u_id'],
                                 'name'    => 'company-contacts_show_info',
                                 'value'   => '1']
                            );                                      
            } else {
              /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                          AND name    = 'company-contacts_show_info' ");*/
                $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                          AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);                                          
            }
            $this->db->query("UPDATE zen_contacts SET created_contact='".$contact_id."' WHERE contact_id='".$in['contact_id']."' ");
            if($in['buyer_id']){
                $this->db->query("INSERT INTO customer_contactsIds SET customer_id='".$in['buyer_id']."', contact_id='".$contact_id."', `primary`='1', email='".$in['email']."' ");
            }
            insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$contact_id);
            $count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
            if($count == 1){
              doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
            }
            msg::success (gm('Contact created'),'success');
        }
        return true;
      }

      /**
      * undocumented function
      *
      * @return void
      * @author PM
      **/
      function tryAddCValid(&$in)
      {
        if($in['add_customer']){
          $v = new validation($in);
          $v->field('name', 'name', 'required:unique[customers.name]');
          $v->field('country_id', 'country_id', 'required');
          if($in['is_zen']){
            $v->field('con_firstname', 'firstname', 'required');
            $v->field('con_firstname', 'lastname', 'required');
          }
          return $v->run();  
        }
        if($in['add_individual']){
          $v = new validation($in);
          $v->field('firstname', 'firstname', 'required');
          $v->field('lastname', 'lastname', 'required');
          $v->field('email', 'Email', 'required:email:unique[customers.c_email]');
          $v->field('country_id', 'country_id', 'required');
          if($in['is_zen']){
            $v->field('con_firstname', 'firstname', 'required');
            $v->field('con_firstname', 'lastname', 'required');
          }
          return $v->run();  
        }
        if($in['add_contact']){
          $v = new validation($in);
          $v->field('firstname', 'firstname', 'required');
          $v->field('lastname', 'lastname', 'required');
          $v->field('buyer_id', 'buyer_id', 'required');
/*        $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
          $v->field('country_id', 'country_id', 'required');*/
          return $v->run();  
        }
        return true;
      }

    function check_vies_vat_number(&$in){
        $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

        if(!$in['value'] || $in['value']==''){      
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }
        $value=trim($in['value']," ");
        $value=str_replace(" ","",$value);
        $value=str_replace(".","",$value);
        $value=strtoupper($value);

        $vat_numeric=is_numeric(substr($value,0,2));

        if(!in_array(substr($value,0,2), $eu_countries)){
          $value='BE'.$value;
        }
        if(in_array(substr($value,0,2), $eu_countries)){
          $search   = array(" ", ".");
          $vat = str_replace($search, "", $value);
          $_GET['a'] = substr($vat,0,2);
          $_GET['b'] = substr($vat,2);
          $_GET['c'] = '1';
          $dd = include('../valid_vat.php');
        }else{
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }
        if(isset($response) && $response == 'invalid'){
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }else if(isset($response) && $response == 'error'){
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }else if(isset($response) && $response == 'valid'){
          $full_address=explode("\n",$result->address);
          switch($result->countryCode){
            case "RO":
              $in['comp_address']=$full_address[1];
              $in['comp_city']=$full_address[0];
              $in['comp_zip']=" ";
              break;
            case "NL":
              $zip=explode(" ",$full_address[2],2);
              $in['comp_address']=$full_address[1];
              $in['comp_zip']=$zip[0];
              $in['comp_city']=$zip[1];
              break;
            default:
              $zip=explode(" ",$full_address[1],2);
              $in['comp_address']=$full_address[0];
              $in['comp_zip']=$zip[0];
              $in['comp_city']=$zip[1];
              break;
          }

          $in['comp_name']=$result->name;

          $in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
          $in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
          $in['full_details']=$result;
          $in['vies_ok']=1;
          if($in['customer_id']){
            $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
            if($in['comp_country'] != $country_id){
                    $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
                    $this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
                    $in['remove_v']=1;
                }else if($in['comp_country'] == $country_id){
                    $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
                    $this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
                    $in['add_v']=1;
                }
          }
          if(ark::$method == 'check_vies_vat_number'){
            msg::success ( gm('VAT Number is valid'),'success');
            json_out($in);
          }
          return true;
        }else{
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }
        if(ark::$method == 'check_vies_vat_number'){
          json_out($in);
        }
      }

      function setArchived(&$in){
          $this->db->query("UPDATE zen_contacts SET archived='".$in['archived']."' WHERE id='".$in['id']."' ");
          return true;
      }

      function importAccount(&$in){
        global $config;
        $zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
        $zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
        $zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");

        $ch = curl_init();
        $headers=array('Content-Type: application/json');      
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
        curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if($in['link']){
            curl_setopt($ch, CURLOPT_URL, $in['link']);
        }else{
            $time_filter='?query='.urlencode('type:organization');
            $const=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_ACCOUNT_FILTER' ");
            if($const){
                $time_filter.=urlencode(' created>='.date("c",(int)$const));
            }
            curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);
        }

        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        $data_ret=array();
        if($info['http_code']>300 || $info['http_code']==0){
            $data=json_decode($put);
            msg::error(gm('error'),'error');
            $data_ret['next']=false;
            $data_ret['nr']=$data->count;
            json_out($data_ret);
        }else{
            $data=json_decode($put);
            if($data->next_page){
                $data_ret['link']=$data->next_page; 
                $data_ret['next']=true;   
                $data_ret['nr']=$in['nr']+100;
            }else{
                $data_ret['next']=false;
                $data_ret['nr']=$data->count;
            }
            $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
            $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
            //$name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'"));
            $name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]));
            foreach($data->results as $key=>$value){
                $our_account=$this->db->field("SELECT customer_id FROM customers WHERE zen_organization_id='".$value->id."' ");
                if($our_account){
                    continue; 
                }
                $buyer_id = $this->db->insert("INSERT INTO customers SET
                                                name='".addslashes($value->name)."',
                                                user_id = '".$_SESSION['u_id']."',
                                                acc_manager_name = '".$name_user."',
                                                country_name ='".get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID)."',
                                                active='1',
                                                zen_organization_id='".$value->id."' ");
          
                $this->db->query("INSERT INTO customer_addresses SET
                                country_id='".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                customer_id='".$buyer_id."',
                                is_primary='1',
                                delivery='1',
                                billing='1' ");
                $this->db->query("INSERT INTO zen_accounts SET 
                    zen_id='".$value->id."',
                    customer_id='".$buyer_id."',
                    external_id='".$value->external_id."',
                    name='".addslashes($value->name)."' ");
                if($in['custom_fields'] && !empty($in['custom_fields'])){
                    foreach($in['custom_fields'] as $key1=>$value1){
                        if($value1){
                            $key_data=$this->db->query("SELECT label_name,field_name,tabel_name,is_custom_dd FROM import_labels WHERE import_label_id='".$in['custom_fields_id'][$key1]."' ");
                            if($key_data->f('tabel_name')=='customer_field'){
                                $field_id=$this->db->field("SELECT field_id FROM customer_fields WHERE label='".addslashes($key_data->f('label_name'))."' ");
                                if($field_id){
                                   $is_drop=$this->db->field("SELECT is_dd FROM customer_fields WHERE field_id='".$field_id."' ");
                                    if($is_drop){
                                        $last_sort=$this->db->field("SELECT sort_order FROM customer_extrafield_dd WHERE extra_field_id='".$field_id."' ORDER BY sort_order DESC");
                                        if(!$last_sort){
                                            $last_sort=0;
                                        }
                                        if($value->organization_fields->{$value1}){
                                            $cf=$this->db->insert("INSERT INTO customer_extrafield_dd SET name='".htmlspecialchars_decode($value->organization_fields->{$value1})."', sort_order='".($last_sort+1)."', extra_field_id='".$field_id."' ");
                                            $this->db->query("INSERT INTO customer_field SET customer_id='".$buyer_id."', value='".$cf."', field_id='".$field_id."' ");
                                        }                    
                                    }else{
                                        if($value->organization_fields->{$value1}){
                                            $this->db->query("INSERT INTO customer_field SET customer_id='".$buyer_id."', value='".addslashes($value->organization_fields->{$value1})."', field_id='".$field_id."' ");
                                        }                
                                    } 
                                }                        
                            }else{
                                if($key_data->f('is_custom_dd')){
                                    $last_sort=$this->db->field("SELECT sort_order FROM ".$key_data->f('tabel_name')." ORDER BY sort_order DESC");
                                    if(!$last_sort){
                                        $last_sort=0;
                                    }
                                    if($value->organization_fields->{$value1}){
                                        $cf=$this->db->insert("INSERT INTO ".$key_data->f('tabel_name')." SET name='".htmlspecialchars_decode($value->organization_fields->{$value1})."', sort_order='".($last_sort+1)."' ");
                                        $this->db->query("UPDATE customers SET ".$key_data->f('field_name')."='".$cf."' WHERE customer_id='".$buyer_id."' ");
                                    }
                                }else{
                                    if($value->organization_fields->{$value1}){
                                        $this->db->query("UPDATE customers SET ".$key_data->f('field_name')."='".addslashes($value->organization_fields->{$value1})."' WHERE customer_id='".$buyer_id."' ");
                                    }        
                                }
                            }
                        }           
                    }
                }
                insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$buyer_id);
            }
            json_out($data_ret);
        }
      }

      function importAccount_time(){
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_ACCOUNT_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='ZEN_ACCOUNT_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='ZEN_ACCOUNT_FILTER',value='".time()."'");
        }
      }

      function importContact_time(){
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='ZEN_CONTACT_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='ZEN_CONTACT_FILTER',value='".time()."'");
        }
        $constant_mod=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER_MOD' ");
        if(!$constant_mod){
            $this->db->query("INSERT INTO settings SET constant_name='ZEN_CONTACT_FILTER_MOD',value='".time()."'");
        }
      }

      function importMatchedContacts_time(){
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER_MOD' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='ZEN_CONTACT_FILTER_MOD'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='ZEN_CONTACT_FILTER_MOD',value='".time()."'");
        }
      }

      function updateChangedContacts_time(){
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER_MOD' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='ZEN_CONTACT_FILTER_MOD'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='ZEN_CONTACT_FILTER_MOD',value='".time()."'");
        }
      }

      function importContact(&$in){
        global $config;
        $zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
        $zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
        $zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
        //$name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'"));
        $name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]));

        $ch = curl_init();
        $headers=array('Content-Type: application/json');      
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
        curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if($in['link']){
            curl_setopt($ch, CURLOPT_URL, $in['link']);
        }else{
            $time_filter='?query='.urlencode('type:user role:end-user');
            $const=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER' ");
            if($const){
                $time_filter.=urlencode(' created>='.date("c",(int)$const));
            }
            curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);
        }

        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        $data_ret=array();
        if($info['http_code']>300 || $info['http_code']==0){
            $data=json_decode($put);
            msg::error(gm('error'),'error');
            $data_ret['next']=false;
            $data_ret['nr']=$data->count;
            json_out($data_ret);
        }else{
            $data=json_decode($put);
            if($data->next_page){
                $data_ret['link']=$data->next_page; 
                $data_ret['next']=true;   
                $data_ret['nr']=$in['nr']+100;
            }else{
                $data_ret['next']=false;
                $data_ret['nr']=$data->count;
            }
            foreach($data->results as $key=>$value){
                $our_contact=$this->db->field("SELECT contact_id FROM customer_contacts WHERE zen_contact_id='".$value->id."' ");
                if($our_contact){
                   continue; 
                }
                $full_name=explode(" ",$value->name);
                $firstname="";
                $lastname="";
                foreach($full_name as $key1=>$value1){
                    if($key1=='0'){
                        $firstname=$value1;
                    }else{
                        $lastname.=$value1." ";
                    }                 
                }
                $lastname=trim($lastname);
                $customer_name="";
                $customer_id=0;
                if($value->organization_id){
                    $customer_data = $this->db->query("SELECT name,customer_id FROM customers WHERE zen_organization_id ='".$value->organization_id."' ");
                    $customer_name=$customer_data->f('name');
                    $customer_id=$customer_data->f('customer_id');
                }
                if(strpos($value->locale,'-')!==false){
                  $l_ar=explode('-',$value->locale);
                  $l_code=$l_ar[0];
                }else{
                  $l_code=$value->locale;
                }
                $language_id=$this->db->field("SELECT lang_id FROM lang WHERE code='".$l_code."' ");
                $lang_sql="";
                if($language_id){
                    $lang_sql=", language='".$language_id."' ";
                }
                $last_id=$this->db->insert("INSERT INTO zen_contacts SET
                                contact_id='".$value->id."',
                                customer_id='".$value->organization_id."',
                                email='".$value->email."',                          
                                first_name='".addslashes($firstname)."',
                                last_name='".addslashes($lastname)."' ");
                if($value->email && $value->email!=''){
                  $customer_mail=$this->db->field("SELECT customer_contacts.contact_id FROM customer_contactsIds 
                    INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
                    WHERE customer_contactsIds.email='".$value->email."' ");
                  if($customer_mail){
                      $this->db->query("UPDATE zen_contacts SET created_contact='".$customer_mail."' WHERE id='".$last_id."' ");
                      $this->db->query("UPDATE customer_contacts SET zen_contact_id='".$value->id."' ".$lang_sql." WHERE contact_id='".$customer_mail."' ");
                      if(!$customer_id){
                          $customer_id=$this->db->field("SELECT customer_id FROM customer_contactsIds 
                            WHERE email='".$value->email."' AND contact_id='".$customer_mail."' ");
                          if($customer_id && $value->organization_id){
                            $this->db->query("UPDATE customers SET zen_organization_id ='".$value->organization_id."' WHERE customer_id='".$customer_id."' ");
                          }else if($value->organization_id){
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                            curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/organizations/'.$value->organization_id.'.json');

                            $put = curl_exec($ch);
                            $info = curl_getinfo($ch);

                            if($info['http_code']>300 || $info['http_code']==0){
                                    msg::error('Error','error');
                            }else{
                                $data_a=json_decode($put);
                                $customer_id = $this->db->insert("INSERT INTO customers SET
                                                        name='".addslashes($data_a->organization->name)."',
                                                        user_id = '".$_SESSION['u_id']."',
                                                        acc_manager_name = '".$name_user."',
                                                        country_name ='".get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID)."',
                                                        active='1',
                                                        zen_organization_id='".$data_a->organization->id."' ");
                                insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$customer_id);
                                $this->db->query("INSERT INTO customer_addresses SET
                                                country_id='".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                                customer_id='".$customer_id."',
                                                is_primary='1',
                                                delivery='1',
                                                billing='1' ");
                                $this->db->query("INSERT INTO zen_accounts SET 
                                    zen_id='".$data_a->organization->id."',
                                    customer_id='".$customer_id."',
                                    external_id='".$data_a->organization->external_id."',
                                    name='".addslashes($data_a->organization->name)."' ");
                                $this->db->query("INSERT INTO customer_contactsIds SET 
                                        customer_id='".$customer_id."', 
                                        contact_id='".$customer_mail."',
                                        `primary`='1',
                                        email='".$value->email."' ");              
                            }
                        }
                      }
                      continue;
                  }
                }
                $contact_id = $this->db->insert("INSERT INTO customer_contacts SET
                                                  firstname='".addslashes($firstname)."',
                                                  lastname='".addslashes($lastname)."',
                                                  `create`  = '".time()."',
                                                  customer_id = '".$customer_id."',
                                                  company_name= '".addslashes($customer_name)."',
                                                  email='".$value->email."',
                                                  zen_contact_id='".$value->id."' ".$lang_sql." ");
                if($value->organization_id && $customer_id){
                    $this->db->query("INSERT INTO customer_contactsIds SET 
                                customer_id='".$customer_id."', 
                                contact_id='".$contact_id."',
                                `primary`='1',
                                email='".$value->email."' ");              
                }else if($value->organization_id){
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                    curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/organizations/'.$value->organization_id.'.json');

                    $put = curl_exec($ch);
                    $info = curl_getinfo($ch);

                    if($info['http_code']>300 || $info['http_code']==0){
                        msg::error('Error','error');
                    }else{
                        $data_a=json_decode($put);
                        $customer_id = $this->db->insert("INSERT INTO customers SET
                                                        name='".addslashes($data_a->organization->name)."',
                                                        user_id = '".$_SESSION['u_id']."',
                                                        acc_manager_name = '".$name_user."',
                                                        country_name ='".get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID)."',
                                                        active='1',
                                                        zen_organization_id='".$data_a->organization->id."' ");
                        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$customer_id);
                        $this->db->query("INSERT INTO customer_addresses SET
                                                country_id='".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                                customer_id='".$customer_id."',
                                                is_primary='1',
                                                delivery='1',
                                                billing='1' ");
                        $this->db->query("INSERT INTO zen_accounts SET 
                                    zen_id='".$data_a->organization->id."',
                                    customer_id='".$customer_id."',
                                    external_id='".$data_a->organization->external_id."',
                                    name='".addslashes($data_a->organization->name)."' ");
                        $this->db->query("INSERT INTO customer_contactsIds SET 
                                        customer_id='".$customer_id."', 
                                        contact_id='".$contact_id."',
                                        `primary`='1',
                                        email='".$value->email."' ");              
                    }
                }
                if($in['custom_fields'] && !empty($in['custom_fields'])){
                    foreach($in['custom_fields'] as $key1=>$value1){
                        if($value1){
                            $key_data=$this->db->query("SELECT label_name,field_name,tabel_name,is_custom_dd FROM import_labels WHERE import_label_id='".$in['custom_fields_id'][$key1]."' ");
                            if($key_data->f('tabel_name')=='contact_field'){
                                $field_id=$this->db->field("SELECT field_id FROM contact_fields WHERE label='".addslashes($key_data->f('label_name'))."' ");
                                if($field_id){
                                    $is_drop=$this->db->field("SELECT is_dd FROM contact_fields WHERE field_id='".$field_id."' ");
                                    if($is_drop){
                                        $last_sort=$this->db->field("SELECT sort_order FROM contact_extrafield_dd WHERE extra_field_id='".$field_id."' ORDER BY sort_order DESC");
                                        if(!$last_sort){
                                            $last_sort=0;
                                        }
                                        if($value->user_fields->{$value1}){
                                            $is_value_link=$this->db->field("SELECT id FROM contact_extrafield_dd WHERE name='".htmlspecialchars_decode($value->user_fields->{$value1})."' ");
                                            $is_contact_link=$this->db->field("SELECT id FROM contact_field WHERE customer_id='".$contact_id."' AND field_id='".$field_id."' ");
                                            if($is_value_link){
                                                if($is_contact_link){
                                                    $this->db->query("UPDATE contact_field SET value='".$is_value_link."' WHERE id='".$is_contact_link."' ");
                                                }else{
                                                    $this->db->query("INSERT INTO contact_field SET customer_id='".$contact_id."', value='".$is_value_link."', field_id='".$field_id."'");
                                                }
                                            }else{
                                                if($is_contact_link){
                                                    $cf=$this->db->insert("INSERT INTO contact_extrafield_dd SET name='".htmlspecialchars_decode($value->user_fields->{$value1})."', sort_order='".($last_sort+1)."', extra_field_id='".$field_id."' ");
                                                    $this->db->query("UPDATE contact_field SET value='".$cf."' WHERE id='".$is_contact_link."' ");
                                                }else{
                                                    $cf=$this->db->insert("INSERT INTO contact_extrafield_dd SET name='".htmlspecialchars_decode($value->user_fields->{$value1})."', sort_order='".($last_sort+1)."', extra_field_id='".$field_id."' ");
                                                    $this->db->query("INSERT INTO contact_field SET customer_id='".$contact_id."', value='".$cf."', field_id='".$field_id."' ");
                                                }                               
                                            }
                                        }                     
                                    }else{
                                        if($value->user_fields->{$value1}){
                                            $is_contact_link=$this->db->field("SELECT id FROM contact_field WHERE customer_id='".$contact_id."' AND field_id='".$field_id."' ");
                                            if($is_contact_link){
                                                $this->db->query("UPDATE contact_field SET value='".addslashes($value->user_fields->{$value1})."' WHERE id='".$is_contact_link."' ");
                                            }else{
                                                $this->db->query("INSERT INTO contact_field SET customer_id='".$contact_id."', value='".addslashes($value->user_fields->{$value1})."', field_id='".$field_id."' ");
                                            }
                                        }                     
                                    }
                                }                
                            }else{
                                if($key_data->f('is_custom_dd')){
                                    $last_sort=$this->db->field("SELECT sort_order FROM ".$key_data->f('tabel_name')." ORDER BY sort_order DESC");
                                    if(!$last_sort){
                                        $last_sort=0;
                                    }
                                    if($value->user_fields->{$value1}){
                                        $is_value_link=$this->db->field("SELECT id FROM ".$key_data->f('tabel_name')." WHERE name='".htmlspecialchars_decode($value->user_fields->{$value1})."' ");                                                                                     
                                        if($is_value_link){
                                            if(($key_data->f('field_name')=='department' || $key_data->f('field_name')=='position') && $customer_id){
                                                $this->db->query("UPDATE customer_contactsIds SET ".$key_data->f('field_name')."='".$is_value_link."' WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."' ");
                                            }else{
                                                $this->db->query("UPDATE customer_contacts SET ".$key_data->f('field_name')."='".$is_value_link."' WHERE contact_id='".$contact_id."' ");
                                            }                                               
                                        }else{
                                            $cf=$this->db->insert("INSERT INTO ".$key_data->f('tabel_name')." SET name='".htmlspecialchars_decode($value->user_fields->{$value1})."', sort_order='".($last_sort+1)."' ");
                                            if(($key_data->f('field_name')=='department' || $key_data->f('field_name')=='position') && $customer_id){
                                                $this->db->query("UPDATE customer_contactsIds SET ".$key_data->f('field_name')."='".$cf."' WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."' ");
                                            }else{
                                                $this->db->query("UPDATE customer_contacts SET ".$key_data->f('field_name')."='".$cf."' WHERE contact_id='".$contact_id."' ");
                                            }           
                                        }                                         
                                    }
                                }else{
                                    if($value->user_fields->{$value1}){
                                        if(($key_data->f('field_name')=='e_title' || $key_data->f('fax')=='fax') && $customer_id){
                                            $this->db->query("UPDATE customer_contactsIds SET ".$key_data->f('field_name')."='".addslashes($value->user_fields->{$value1})."' WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."' ");
                                        }else{
                                            $this->db->query("UPDATE customer_contacts SET ".$key_data->f('field_name')."='".addslashes($value->user_fields->{$value1})."' WHERE contact_id='".$contact_id."' ");
                                        }                             
                                    }        
                                }
                            }                     
                        }                                                                       
                    }
                }
                insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$contact_id);
            }
            json_out($data_ret);
        }
      }

      function syncMembership(&$in){
        global $config;
        $zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
        $zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
        $zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");

        $org=$this->db->query("SELECT * FROM zen_accounts LIMIT ".$in['nr'].",1 ");

        $ch = curl_init();
        $headers=array('Content-Type: application/json');      
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
        curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/organizations/'.$org->f('zen_id').'/organization_memberships.json');

        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        if($info['http_code']>300 || $info['http_code']==0){
            msg::error(gm('error'),'error');
            json_out($in);
        }else{
            $data=json_decode($put);
            $org_contacts=$this->db->query("SELECT customer_contacts.zen_contact_id FROM customer_contactsIds 
                LEFT JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
                WHERE customer_contactsIds.customer_id='".$org->f('customer_id')."' ")->getAll();
            foreach($data->organization_memberships as $key=>$value){
                $is_linked=false;
                foreach($org_contacts as $key1=>$value1){
                    if($value->user_id==$value1['zen_contact_id']){
                        $is_linked=true;
                        break;
                    }
                }
                if(!$is_linked){
                    $contact_id=$this->db->field("SELECT contact_id FROM customer_contacts WHERE zen_contact_id='".$value->user_id."' ");
                    if($contact_id){
                        $primary=$value->default===true ? '1' : '0';
                        $this->db->query("INSERT INTO customer_contactsIds SET 
                                customer_id='".$org->f('customer_id')."', 
                                contact_id='".$contact_id."',
                                `primary`='".$primary."' ");
                    }               
                }
            }
            json_out($in);
        }
      }

      function applySync(&$in){
        $exists=$this->db->field("SELECT value FROM settings WHERE `constant_name`='".$in['name']."' ");
        if(is_null($exists)){
            $this->db->query("INSERT INTO settings SET `constant_name`='".strtoupper($in['name'])."',value='".$in['value']."' ");
        }else{
            $this->db->query("UPDATE settings SET value='".$in['value']."' WHERE `constant_name`='".strtoupper($in['name'])."' ");
        }
        msg::success(gm('Changes saved'),'success');
        json_out();
      }


      function importMatchedContacts(&$in){
        global $config;
        $zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
        $zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
        $zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
        //$name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'"));
        $name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]));

        $ch = curl_init();
        $headers=array('Content-Type: application/json');      
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
        curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if($in['link']){
            curl_setopt($ch, CURLOPT_URL, $in['link']);
        }else{
            $time_filter='?query='.urlencode('type:user role:end-user');
            curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);
        }

        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        $data_ret=array();
        if($info['http_code']>300 || $info['http_code']==0){
            $data=json_decode($put);
            msg::error(gm('error'),'error');
            $data_ret['next']=false;
            $data_ret['nr']=$data->count;
            json_out($data_ret);
        }else{
            $data=json_decode($put);
            if($data->next_page){
                $data_ret['link']=$data->next_page; 
                $data_ret['next']=true;   
                $data_ret['nr']=$in['nr']+100;
            }else{
                $data_ret['next']=false;
                $data_ret['nr']=$data->count;
            }
            foreach($data->results as $key=>$value){
                $our_contact=$this->db->field("SELECT contact_id FROM customer_contacts WHERE zen_contact_id='".$value->id."' ");
                if($our_contact){
                   continue; 
                }
                if(strpos($value->locale,'-')!==false){
                  $l_ar=explode('-',$value->locale);
                  $l_code=$l_ar[0];
                }else{
                  $l_code=$value->locale;
                }
                $language_id=$this->db->field("SELECT lang_id FROM lang WHERE code='".$l_code."' ");
                $lang_sql="";
                if($language_id){
                    $lang_sql=", language='".$language_id."' ";
                }
                if($value->email && $value->email!=''){
                    $customer_mail=$this->db->field("SELECT customer_contacts.contact_id FROM customer_contactsIds 
                        INNER JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
                        WHERE customer_contactsIds.email='".$value->email."' ");
                    if($customer_mail){
                        $last_id=$this->db->field("SELECT id FROM zen_contacts WHERE contact_id='".$value->id."' ");
                        if(!$last_id){
                            $this->db->query("INSERT INTO zen_contacts SET contact_id='".$last_id."',created_contact='".$customer_mail."' ");
                        }else{
                            $this->db->query("UPDATE zen_contacts SET created_contact='".$customer_mail."' WHERE id='".$last_id."' ");
                        }  
                        $this->db->query("UPDATE customer_contacts SET zen_contact_id='".$value->id."' ".$lang_sql." WHERE contact_id='".$customer_mail."' ");
                        $customer_id=$this->db->field("SELECT customer_id FROM customer_contactsIds 
                            WHERE email='".$value->email."' AND contact_id='".$customer_mail."' ");
                        if($customer_id && $value->organization_id){
                            $this->db->query("UPDATE customers SET zen_organization_id ='".$value->organization_id."' WHERE customer_id='".$customer_id."' ");
                        }else if($value->organization_id){
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                            curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/organizations/'.$value->organization_id.'.json');

                            $put = curl_exec($ch);
                            $info = curl_getinfo($ch);

                            if($info['http_code']>300 || $info['http_code']==0){
                                    msg::error('Error','error');
                            }else{
                                $data_a=json_decode($put);
                                $customer_id = $this->db->insert("INSERT INTO customers SET
                                                        name='".addslashes($data_a->organization->name)."',
                                                        user_id = '".$_SESSION['u_id']."',
                                                        acc_manager_name = '".$name_user."',
                                                        country_name ='".get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID)."',
                                                        active='1',
                                                        zen_organization_id='".$data_a->organization->id."' ");
                                insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$customer_id);
                                $this->db->query("INSERT INTO customer_addresses SET
                                                country_id='".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                                customer_id='".$customer_id."',
                                                is_primary='1',
                                                delivery='1',
                                                billing='1' ");
                                $this->db->query("INSERT INTO zen_accounts SET 
                                    zen_id='".$data_a->organization->id."',
                                    customer_id='".$customer_id."',
                                    external_id='".$data_a->organization->external_id."',
                                    name='".addslashes($data_a->organization->name)."' ");
                                $this->db->query("INSERT INTO customer_contactsIds SET 
                                        customer_id='".$customer_id."', 
                                        contact_id='".$customer_mail."',
                                        `primary`='1',
                                        email='".$value->email."' ");              
                            }
                        }
                    }
                }
            }
            json_out($data_ret);
        }
      }

      function updateChangedContacts(&$in){
        global $config;
        $zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
        $zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
        $zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
        //$name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'"));
        $name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]));
        $custom_fields=$this->db->query("SELECT * FROM zen_fields WHERE `master`='contact' ")->getAll();

        $ch = curl_init();
        $headers=array('Content-Type: application/json');      
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
        curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if($in['link']){
            curl_setopt($ch, CURLOPT_URL, $in['link']);
        }else{
            $const_c=$this->db->field("SELECT value FROM settings WHERE constant_name='ZEN_CONTACT_FILTER_MOD' ");
            $time_filter='?query='.urlencode('type:user role:end-user updated>='.date("c",(int)$const_c));
            curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/search.json'.$time_filter);
        }

        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        $data_ret=array();
        if($info['http_code']>300 || $info['http_code']==0){
            $data=json_decode($put);
            msg::error(gm('error'),'error');
            $data_ret['next']=false;
            $data_ret['nr']=$data->count;
            json_out($data_ret);
        }else{
            $data=json_decode($put);
            if($data->next_page){
                $data_ret['link']=$data->next_page; 
                $data_ret['next']=true;   
                $data_ret['nr']=$in['nr']+100;
            }else{
                $data_ret['next']=false;
                $data_ret['nr']=$data->count;
            }
            foreach($data->results as $key=>$value){
                $contact_id=$this->db->field("SELECT contact_id FROM customer_contacts WHERE zen_contact_id='".$value->id."' ");
                if($contact_id){
                    $full_name=explode(" ",$value->name);
                    $firstname="";
                    $lastname="";
                    foreach($full_name as $key1=>$value1){
                        if($key1=='0'){
                            $firstname=$value1;
                        }else{
                            $lastname.=$value1." ";
                        }                 
                    }
                    $lastname=trim($lastname);
                    if(strpos($value->locale,'-')!==false){
                      $l_ar=explode('-',$value->locale);
                      $l_code=$l_ar[0];
                    }else{
                      $l_code=$value->locale;
                    }
                    $language_id=$this->db->field("SELECT lang_id FROM lang WHERE code='".$l_code."' ");
                    $lang_sql="";
                    if($language_id){
                        $lang_sql=", language='".$language_id."' ";
                    }
                    $this->db->query("UPDATE customer_contacts SET
                                                  firstname='".addslashes($firstname)."',
                                                  lastname='".addslashes($lastname)."'
                                                  ".$lang_sql."
                                                  WHERE contact_id='".$contact_id."' ");

                        if($value->organization_id){
                            $c_id=$this->db->field("SELECT customer_id FROM customers WHERE zen_organization_id='".$value->organization_id."' ");
                        }
                        if(!empty($custom_fields)){
                            foreach($custom_fields as $key1=>$value1){
                                $key_data=$this->db->query("SELECT label_name,field_name,tabel_name,is_custom_dd FROM import_labels WHERE import_label_id='".$value1['our_field']."' ");
                                    if($key_data->f('tabel_name')=='contact_field'){
                                        $field_id=$this->db->field("SELECT field_id FROM contact_fields WHERE label='".addslashes($key_data->f('label_name'))."' ");
                                        if($field_id){
                                            $is_drop=$this->db->field("SELECT is_dd FROM contact_fields WHERE field_id='".$field_id."' ");
                                            if($is_drop){
                                                $last_sort=$this->db->field("SELECT sort_order FROM contact_extrafield_dd WHERE extra_field_id='".$field_id."' ORDER BY sort_order DESC");
                                                if(!$last_sort){
                                                    $last_sort=0;
                                                }
                                                if($value->user_fields->{$value1['zen_field']}){
                                                    $is_value_link=$this->db->field("SELECT id FROM contact_extrafield_dd WHERE name='".htmlspecialchars_decode($value->user_fields->{$value1['zen_field']})."' ");
                                                    $is_contact_link=$this->db->field("SELECT id FROM contact_field WHERE customer_id='".$contact_id."' AND field_id='".$field_id."' ");
                                                    if($is_value_link){
                                                        if($is_contact_link){
                                                            $this->db->query("UPDATE contact_field SET value='".$is_value_link."' WHERE id='".$is_contact_link."' ");
                                                        }else{
                                                            $this->db->query("INSERT INTO contact_field SET customer_id='".$contact_id."', value='".$is_value_link."', field_id='".$field_id."'");
                                                        }
                                                    }else{
                                                        if($is_contact_link){
                                                            $cf=$this->db->insert("INSERT INTO contact_extrafield_dd SET name='".htmlspecialchars_decode($value->user_fields->{$value1['zen_field']})."', sort_order='".($last_sort+1)."', extra_field_id='".$field_id."' ");
                                                            $this->db->query("UPDATE contact_field SET value='".$cf."' WHERE id='".$is_contact_link."' ");
                                                        }else{
                                                            $cf=$this->db->insert("INSERT INTO contact_extrafield_dd SET name='".htmlspecialchars_decode($value->user_fields->{$value1['zen_field']})."', sort_order='".($last_sort+1)."', extra_field_id='".$field_id."' ");
                                                            $this->db->query("INSERT INTO contact_field SET customer_id='".$contact_id."', value='".$cf."', field_id='".$field_id."' ");
                                                        }                               
                                                    }
                                                }                                                           
                                            }else{
                                                if($value->user_fields->{$value1['zen_field']}){
                                                    $is_contact_link=$this->db->field("SELECT id FROM contact_field WHERE customer_id='".$contact_id."' AND field_id='".$field_id."' ");
                                                    if($is_contact_link){
                                                        $this->db->query("UPDATE contact_field SET value='".addslashes($value->user_fields->{$value1['zen_field']})."' WHERE id='".$is_contact_link."' ");
                                                    }else{
                                                        $this->db->query("INSERT INTO contact_field SET customer_id='".$contact_id."', value='".addslashes($value->user_fields->{$value1['zen_field']})."', field_id='".$field_id."' ");
                                                    }
                                                }                   
                                            }
                                        }                
                                    }else{
                                        if($key_data->f('is_custom_dd')){
                                            $last_sort=$this->db->field("SELECT sort_order FROM ".$key_data->f('tabel_name')." ORDER BY sort_order DESC");
                                            if(!$last_sort){
                                                $last_sort=0;
                                            }
                                            if($value->user_fields->{$value1['zen_field']}){
                                                $is_value_link=$this->db->field("SELECT id FROM ".$key_data->f('tabel_name')." WHERE name='".htmlspecialchars_decode($value->user_fields->{$value1['zen_field']})."' ");                                                                                     
                                                if($is_value_link){
                                                    if(($key_data->f('field_name')=='department' || $key_data->f('field_name')=='position') && $c_id){
                                                        $this->db->query("UPDATE customer_contactsIds SET ".$key_data->f('field_name')."='".$is_value_link."' WHERE contact_id='".$contact_id."' AND customer_id='".$c_id."' ");
                                                    }else{
                                                        $this->db->query("UPDATE customer_contacts SET ".$key_data->f('field_name')."='".$is_value_link."' WHERE contact_id='".$contact_id."' ");
                                                    }                                               
                                                }else{
                                                    $cf=$this->db->insert("INSERT INTO ".$key_data->f('tabel_name')." SET name='".htmlspecialchars_decode($value->user_fields->{$value1['zen_field']})."', sort_order='".($last_sort+1)."' ");
                                                    if(($key_data->f('field_name')=='department' || $key_data->f('field_name')=='position') && $c_id){
                                                        $this->db->query("UPDATE customer_contactsIds SET ".$key_data->f('field_name')."='".$cf."' WHERE contact_id='".$contact_id."' AND customer_id='".$c_id."' ");
                                                    }else{
                                                        $this->db->query("UPDATE customer_contacts SET ".$key_data->f('field_name')."='".$cf."' WHERE contact_id='".$contact_id."' ");
                                                    }           
                                                }                                         
                                            }
                                        }else{
                                            if($value->user_fields->{$value1['zen_field']}){
                                                if(($key_data->f('field_name')=='e_title' || $key_data->f('fax')=='fax') && $c_id){
                                                    $this->db->query("UPDATE customer_contactsIds SET ".$key_data->f('field_name')."='".addslashes($value->user_fields->{$value1['zen_field']})."' WHERE contact_id='".$contact_id."' AND customer_id='".$c_id."' ");
                                                }else{
                                                    $this->db->query("UPDATE customer_contacts SET ".$key_data->f('field_name')."='".addslashes($value->user_fields->{$value1['zen_field']})."' WHERE contact_id='".$contact_id."' ");
                                                }                             
                                            }        
                                        }
                                    }                                                                                         
                            }
                        }

                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                    curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/users/'.$value->id.'/organization_memberships.json');

                    $put = curl_exec($ch);
                    $info = curl_getinfo($ch);

                    if($info['http_code']>300 || $info['http_code']==0){
                        //
                    }else{
                        $data_m=json_decode($put);
                        foreach($data_m->organization_memberships as $key2=>$value2){
                            $zen_org_id=$this->db->field("SELECT customer_id FROM customers WHERE zen_organization_id='".$value2->organization_id."' ");
                            if(!$zen_org_id){
                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
                                curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/organizations/'.$value2->organization_id.'.json');

                                $put = curl_exec($ch);
                                $info = curl_getinfo($ch);

                                if($info['http_code']>300 || $info['http_code']==0){
                                    //
                                }else{
                                    $data_acc=json_decode($put);
                                    $buyer_id = $this->db->insert("INSERT INTO customers SET
                                                name='".addslashes($data_acc->organization->name)."',
                                                user_id = '".$_SESSION['u_id']."',
                                                acc_manager_name = '".$name_user."',
                                                country_name ='".get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID)."',
                                                active='1',
                                                zen_organization_id='".$data_acc->organization->id."' ");
          
                                    $this->db->query("INSERT INTO customer_addresses SET
                                                    country_id='".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                                    customer_id='".$buyer_id."',
                                                    is_primary='1',
                                                    delivery='1',
                                                    billing='1' ");
                                    $this->db->query("INSERT INTO zen_accounts SET 
                                        zen_id='".$data_acc->organization->id."',
                                        customer_id='".$buyer_id."',
                                        external_id='".$data_acc->organization->external_id."',
                                        name='".addslashes($data_acc->organization->name)."' ");
                                }
                            }
                        }

                        $this->db->query("UPDATE customer_contactsIds SET `primary`='0' WHERE contact_id='".$contact_id."' ");
                        foreach($data_m->organization_memberships as $key2=>$value2){
                            $c_id=$this->db->field("SELECT customer_id FROM customers WHERE zen_organization_id='".$value2->organization_id."' ");
                            if($c_id){
                                $primary=$value2->default===true ? '1' : '0';
                                $c_entry=$this->db->field("SELECT id FROM customer_contactsIds WHERE customer_id='".$c_id."' AND contact_id='".$contact_id."' ");
                                if(!$c_entry){                         
                                    $this->db->query("INSERT INTO customer_contactsIds SET 
                                        customer_id='".$c_id."', 
                                        contact_id='".$contact_id."',
                                        `primary`='".$primary."' ");
                                }else{
                                    $this->db->query("UPDATE customer_contactsIds SET 
                                        customer_id='".$c_id."', 
                                        contact_id='".$contact_id."',
                                        `primary`='".$primary."'
                                        WHERE id='".$c_entry."' ");
                                }          
                            }                  
                        }
                        $orgs=$this->db->query("SELECT customers.zen_organization_id,customers.customer_id FROM customer_contactsIds 
                            LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
                            WHERE customer_contactsIds.contact_id='".$contact_id."' ")->getAll();
                        foreach($orgs as $key2=>$value2){
                            $is_linked=false;
                            foreach($data_m->organization_memberships as $key3=>$value3){
                                if($value2['zen_organization_id']==$value3->organization_id){
                                    $is_linked=true;
                                    break;
                                }
                            }
                            if($value2['zen_organization_id'] && !$is_linked){
                                $this->db->query("DELETE FROM customer_contactsIds WHERE customer_id='".$value2['customer_id']."' AND contact_id='".$contact_id."'");
                            }
                        }
                    }
                    insert_message_log('xcustomer_contact','{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$contact_id);  
                }
            }
            json_out($data_ret);
        }
      }

      function autoSync(&$in){
        if($in['custom_fields'] && !empty($in['custom_fields'])){
            $this->db->query("DELETE FROM zen_fields WHERE `master`='".$in['master']."' ");
            foreach($in['custom_fields'] as $key1=>$value1){
                if($value1){
                    $this->db->query("INSERT INTO zen_fields SET
                        master='".$in['master']."',
                        zen_field='".addslashes($value1)."',
                        our_field='".$in['custom_fields_id'][$key1]."'  ");                                 
                }                                                                       
            }
        }
        msg::success(gm('Changes saved'),'success');
        json_out($in);
      }

}

?>