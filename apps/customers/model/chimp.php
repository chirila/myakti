<?php
/**
 * Mailchimp class
 *
 * @author      Arkweb SRL
 * @link        https://go.salesassist.eu
 * @copyright   [description]
 * @package 	Mailchimp
 */
class chimp{

	var $db;
	/**
	 * constructor
	 *
	 * sets the database connection
	 *
	 * @return nothing
	 */
	function chimp(){
		$this->db= new sqldb();
		ark::loadLibraries(array('MCAPI.class'));
	}

	/**
	 * Activate Mailchimp app
	 *
	 * @param array() $in
	 * @return true
	 */
	function activate(&$in){
		if(!$this->activate_validate($in)){
			return  false;
		}
		$this->db->query("UPDATE apps SET api='".$in['api_key']."', active='1' WHERE name='Mail chimp' AND main_app_id='0' AND type='main' ");
		$app_id = $this->db->field("SELECT app_id FROM apps WHERE name='Mail chimp' AND main_app_id='0' AND type='main' ");
		$this->db->query("INSERT INTO apps SET api='".$in['user']."', main_app_id='".$app_id."', type='user' ");
		$this->db->query("INSERT INTO apps SET api='".$in['password']."', main_app_id='".$app_id."', type='password' ");
		msg::success ( gm('App activated'),'success');
		return true;

	}

	/**
	 * Validation for activate
	 * we check to see if we have all the fields necesery
	 * we need user, password and api_key fields to be filled
	 * if we have all we need we check to see if the credentials are ok
	 *
	 * @param array() $in
	 * @return true for no error(s) or false for error(s)
	 */
	function activate_validate(&$in){
		$is_ok = true;
		$v = new validation($in);
		$v->field('user', '[!L!]Name[!/L!]', 'required');
		$v->field('password', '[!L!]Name[!/L!]', 'required');
		$v->field('api_key', '[!L!]Name[!/L!]', 'required');
		if(!$v->run()){
			$is_ok = false;
		}else{
			$api = new MCAPI($in['api_key']);
			$retval = $api->apikeys($in['user'], $in['password'],false);
			if ($api->errorCode){
				$this->db->query("UPDATE apps SET active='2' WHERE  name='Mail chimp' AND main_app_id='0' AND type='main' ");
				$massaje = str_replace('href="/login/','target="_blank" href="https://login.mailchimp.com/login/',$api->errorMessage);
				msg::error ( $massaje,'error');
				$is_ok = false;
			}
		}
		return $is_ok;
	}

	/**
	 * Add subscribers to a list
	 *
	 * @param array() $in
	 * @return true
	 */
	function add_subscriber(&$in){

		#create an arrays with the new subscribers
		// $contacts = $this->db->field("SELECT contacts FROM contact_list WHERE contact_list_id='".$in['lists_id']."' ");
		// $contacts = explode(',',$contacts);
		$contacts_list = $this->db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['lists_id']."' ");
		$contacts_list->next();
		$list_for = $contacts_list->f('list_for');
		if($contacts_list->f('list_type')){
			$cont = $this->db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_list_id='".$in['lists_id']."' AND active='1' ");
		}else{
			$filtru = unserialize($contacts_list->f('filter'));
			if($filtru){
				$filter = '';
				foreach ($filtru as $field => $value){
					if($value){
						if(is_array($value)){
							$val = '';
							foreach ($value as $key){
								$val .= $key.',';
							}
							$value = $val;
						}
						$value = rtrim($value,',');
						if($value){
							if(substr(trim($field), 0, 13)=='custom_dd_id_'){
								$filter.= " AND ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									$filter .= "customer_field.value = '".addslashes($v2)."' OR ";
								}
								$filter = rtrim($filter,"OR ");
							}elseif(substr(trim($field), 0, 15)=='c_custom_dd_id_'){
					           $filter_c.= " AND ";
					          $v = explode(',', $value);
					          foreach ($v as $k => $v2) {
					            $filter_c .= "contact_field.value = '".$v2."' OR ";
					          }
					          $filter_c = rtrim($filter_c,"OR ");
					        }elseif($field == 'c_type '){
								$filter .= " AND ( ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
								}
								$filter = rtrim($filter,"OR ");
								$filter .= " ) ";
							}elseif ($field == 'c_type') {
								$filter .= " AND ( ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									// console::log($v2);
									// $c_v = $db->field("SELECT name FROM customer_type WHERE id='".$v2."' ");
									$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
								}
								$filter = rtrim($filter,"OR ");
								$filter .= " ) ";
							}
							elseif(trim($field) == 'position'){
								$filter .= " AND ( ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									$filter .= "position_n like '%".addslashes($v2)."%' OR ";
								}
								$filter = rtrim($filter,"OR ");
								$filter .= " ) ";
							}elseif(trim($field) == 'customer_contacts.email'){
								$filter.=" AND customer_contacts.email='' ";
							}elseif(trim($field) == 'customers.c_email'){
								$filter.=" AND customers.c_email='' ";
							}
							else{
								$filter .= " AND ".$field." IN (".addslashes($value).") ";
							}
						}elseif(trim($field) == 'customers.c_email'){
							$filter.= " AND customers.c_email!='' ";
						}elseif(trim($field) == 'customer_contacts.email'){
							$filter.= " AND customer_contacts.email!='' ";
						}elseif(trim($field) == 'customers.s_emails'){
							$filter.= " AND customers.s_emails='1' ";
						}
					}
				}
			}
			if(!$filter){
				$filter = '1=1';
			}
			$filter = ltrim($filter," AND");
			if(!$list_for){
				$filter .= " AND customer_contacts.active='1' ";

				$cont = $this->db->query("SELECT customer_contacts.contact_id
							    FROM customer_contacts
							    LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
							    LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
							    LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
							    LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							    LEFT JOIN customer_type ON customers.c_type=customer_type.id
							    LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							    LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							    LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							    LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            			LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            			LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            			LEFT JOIN contact_field ON customer_contacts.contact_id=contact_field.customer_id
							    WHERE ".$filter." ");
			}else{
				$filter .= " AND customers.active='1' AND is_admin='0' ";
				$cont =  $this->db->query("SELECT DISTINCT customers.customer_id AS contact_id
			        FROM customers
			        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
			        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
			        LEFT JOIN customer_type ON customers.c_type=customer_type.id
			        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
			        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
			        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
			        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
							LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            		WHERE ".$filter." ");
			}
		}
		while ($cont->next()) {
			if(!in_array($cont->f('contact_id'),$contacts)){
				$contacts[] = $cont->f('contact_id');
			}
		}
		$in['total_contacts'] = $contacts_list->f('contacts');
		$in['contacts_ids'] = $contacts;
		$in['chimp'] = '1';
		$in['list_name'] = $contacts_list->f('name');
		$i=0;
		foreach ($contacts as $key){
			if(!$list_for){
				$contact = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$key."' ");
				$contact->next();
				if($contact->f('email')){
					$batch[]	= array('EMAIL' => $contact->f('email'),
														'FNAME' => $contact->f('firstname'),
														'LNAME' => $contact->f('lastname')
													 );
				}else{
					//msg::$notice = gm('There were some contact with no email address that were not imported.');
				}
			}else{
				$contact = $this->db->query("SELECT * FROM customers WHERE customer_id='".$key."' ");
				$contact->next();
				if($contact->f('c_email')){
					$batch[]	= array('EMAIL' => $contact->f('c_email'),
														'FNAME' => $contact->f('name'),
														'LNAME' => $contact->f('name')
													 );
				}else{
					//msg::$notice = gm('There were some contact with no email address that were not imported.');
				}
			}
			$i++;
		}

		$apikey = $this->db->field("SELECT api FROM apps WHERE type='main' AND active='1' AND template_file='chimp' ");

		$api = new MCAPI($apikey);

		$retval = $api->listMembers($in['list_id'], 'subscribed', null, 0, 5000 );

		//print_r($retval);
		if ($api->errorCode){
			msg::error ( $api->errorMessage,'error');
			return false;
		} else {
			$subs = array();
			foreach($retval['data'] as $member){
			    array_push($subs,$member['email']);
			}
		}
		//print_r($subs);
		if($subs){
			$delete = true; //don't completely remove the emails
			$bye = false; // yes, send a goodbye email
			$notify = false; // no, don't tell me I did this

			$vals = $api->listBatchUnsubscribe($in['list_id'], $subs, $delete, $bye, $notify);
			//console::log($vals);
			//print_r($vals);
			//echo "success:".$vals['success_count']."\n";
			if ($api->errorCode){
				msg::error ( $api->errorMessage,'error');
				return false;
			}

		}
		if($in['refresh']=='1')
		{
			$optin = false; //yes, send optin emails
			$up_exist = true; // yes, update currently subscribed users
			$replace_int = false; // no, add interest, don't replace

			$vals = $api->listBatchSubscribe($in['list_id'],$batch,$optin, $up_exist, $replace_int);
			if ($api->errorCode){
			  msg::error ( $api->errorMessage,'error');
				return false;
			} else {
				foreach($vals['errors'] as $val){
					$mails .= $val['email_address']. ",";
				}

				/*foreach ($contacts as $key){
					$this->db->query("INSERT INTO customer_contact_activity SET contact_id='".$key."', contact_activity_type='6', contact_activity_note='Subscribed to ".$in['list_name'].".', date='".time()."' ");
			  }*/
			  $this->db->query("SELECT campaign_list_id FROM chimp_list WHERE campaign_list_id='".$in['list_id']."' ");
			  if(!$this->db->move_next()){
			  	$this->db->query("INSERT INTO chimp_list SET contact_list_id='".$in['lists_id']."', campaign_list_id='".$in['list_id']."' ");
			  }else{
			   	$this->db->query("UPDATE chimp_list SET contact_list_id='".$in['lists_id']."' WHERE campaign_list_id='".$in['list_id']."' ");
			  }
			  /*if($mails){
			  	msg::$notice = gm('The following email did not get imported:') ." ". $mails;
			  }*/
			  $in['pag'] = "get_chimp_lists";
				msg::success ( gm('List Updated'),'success');
			}
		}
		return true;
	}

	function insert_subscriber(&$in)
	{
		$contacts_list = $this->db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['lists_id']."' ");
		$contacts_list->next();
		$list_for = $contacts_list->f('list_for');
		$emailu='';
		if(!$list_for){
			$contact = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$contact->next();
			if($contact->f('email')){
				$emailu = $contact->f('email');
				$merge_vars	= array(//'EMAIL' => $contact->f('email'),
													'FNAME' => $contact->f('firstname'),
													'LNAME' => $contact->f('lastname')
												 );
			}else{
				// gm('The folowing contact was not imported: ').
				msg::notice ($contact->f('firstname')." ".$contact->f('lastname'),'notice');
				$in['failure']=true;
				json_out($in);
				return false;
			}
		}else{
			$contact = $this->db->query("SELECT * FROM customers WHERE customer_id='".$in['contact_id']."' ");
			$contact->next();
			if($contact->f('c_email')){
				$emailu = $contact->f('c_email');
				$merge_vars	= array(//'EMAIL' => $contact->f('email'),
													'FNAME' => $contact->f('name'),
													'LNAME' => ''
												 );
			}else{
				// gm('The folowing contact was not imported: ').
				msg::notice ( $contact->f('firstname')." ".$contact->f('lastname'),'notice');
				$in['failure']=true;
				json_out($in);
				return false;
			}
		}

		$apikey = $this->db->field("SELECT api FROM apps WHERE type='main' AND active='1' AND template_file='chimp' ");

		$api = new MCAPI($apikey);
		/*$retval = $api->listMembers($in['list_id'], 'subscribed', null, 0, 5000 );

		if ($api->errorCode){
			msg::$error = $api->errorMessage;
			return false;
		} else {
			$subs = array();
			foreach($retval['data'] as $member){
			    array_push($subs,$member['email']);
			}
		}

		if($subs){
			$delete = true; //don't completely remove the emails
			$bye = false; // yes, send a goodbye email
			$notify = false; // no, don't tell me I did this

			$vals = $api->listBatchUnsubscribe($in['list_id'], $subs, $delete, $bye, $notify);

			if ($api->errorCode){
				msg::$error = $api->errorMessage;
				return false;
			}
		}*/
		$optin = false; //yes, send optin emails
		$up_exist = true; // yes, update currently subscribed users
		$replace_int = false; // no, add interest, don't replace
		$email_type = 'html';
		//print_r($merge_vars);
		console::log($merge_vars);
		//$vals = $api->listBatchSubscribe($in['list_id'],$batch,$optin, $up_exist, $replace_int);
		$vals = $api->listSubscribe($in['list_id'],$emailu,$merge_vars,$email_type,$optin,$up_exist,$replace_int,false);
		//console::log($vals);
		if (!$vals)
		{
			$in["failure"]=true;
		}
		if ($api->errorCode){
		  msg::error ( $api->errorMessage,'error');
		  json_out($in);
			return false;
		} else {
			// console::log($vals);
			foreach($vals['errors'] as $val){
				$mails .= $val['email_address']. ",";
			}
			/*
			foreach ($contacts as $key){
				$this->db->query("INSERT INTO customer_contact_activity SET contact_id='".$key."', contact_activity_type='6', contact_activity_note='Subscribed to ".$in['list_name'].".', date='".time()."' ");
		  }*/
		  $this->db->query("SELECT campaign_list_id FROM chimp_list WHERE campaign_list_id='".$in['list_id']."' ");
		  if(!$this->db->move_next()){
		  	$this->db->query("INSERT INTO chimp_list SET contact_list_id='".$in['lists_id']."', campaign_list_id='".$in['list_id']."' ");
		  }else{
		   	$this->db->query("UPDATE chimp_list SET contact_list_id='".$in['lists_id']."' WHERE campaign_list_id='".$in['list_id']."' ");
		  }
		  /*if($mails){
		  	msg::$notice = gm('The following email did not get imported:') ." ". $mails;
		  }*/
		  //$in['pag'] = "get_chimp_lists";
			msg::success ( gm('List Updated'),'success');
		}
		json_out($in);
		return true;
	}

	function refresh(&$in){
		$list = $this->db->field("SELECT contact_list_id FROM chimp_list WHERE campaign_list_id='".$in['list_id']."' ");
		if($list){
			$in['lists_id'] =$list;
			$in['refresh']	= '1';
			$this->add_subscriber($in);
		}else {
			msg::notice("Please update your campaign manualy",'notice');
		}
		return true;
	}

}