<?php
	class deals {
		var $pag = 'opportunity'; 		
		var $field_n = 'opportunity_id';

		function __construct() {
			$this->db = new sqldb();
			global $database_config;
			$database_users = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
			);
			$this->database_users = $database_users;
			$this->db_users = new sqldb($database_users);
		}

		/**
		 * undocumented function
		 *
		 * @return void
		 * @author PM
		 **/
		function addBoard(&$in){
			if(!$this->addBoard_validate($in)){
				json_out($in);
				return false;
			}
			$board_id=$this->db->insert("INSERT INTO tblopportunity_board SET name='".addslashes($in['name'])."' ");
			$i=0;
			foreach($in['stages'] as $key=>$value){
				$this->db->query("INSERT INTO tblopportunity_stage
								SET name='".addslashes($value['name'])."',
								board_id='".$board_id."',
								sort_order='".$i."',
								rate='".$value['rate']."' ");
				$i++;
			}
			msg::success(gm("Board successfully created"),"success");
			return true;
		}

		function addBoard_validate(&$in){
			$v=new validation($in);
			$v->field('name', 'Name', 'required');
			$is_ok=$v->run();
			if($is_ok){
				if(empty($in['stages'])){
					msg::error(gm("At least one board stage required"),"error");
					$is_ok=false;
				}else{
					foreach($in['stages'] as $key=>$value){
						if(!$value['name']){
							msg::error(gm("This field is required"),'name_'.$key);
							$is_ok=false;
						}
					}
				}	
			}
			return $is_ok;
		}

		function editBoard(&$in){
			if(!$this->addBoard_validate($in)){
				json_out($in);
				return false;
			}
			$this->db->query("UPDATE tblopportunity_board SET name='".addslashes($in['name'])."' WHERE id='".$in['board_id']."' ");
			$i=0;
			foreach($in['stages'] as $key=>$value){
				if($value['id']){
					$this->db->query("UPDATE tblopportunity_stage
								SET name='".addslashes($value['name'])."',
								board_id='".$in['board_id']."',
								sort_order='".$i."',
								rate='".$value['rate']."' 
								WHERE id='".$value['id']."' ");
				}else{
					$this->db->query("INSERT INTO tblopportunity_stage
								SET name='".addslashes($value['name'])."',
								board_id='".$in['board_id']."',
								sort_order='".$i."',
								rate='".$value['rate']."' ");
				}	
				$i++;
			}
			msg::success(gm("Board successfully updated"),"success");
			return true;
		}

		function deleteBoard(&$in){
			$stages=$this->db->query("SELECT id FROM tblopportunity_stage WHERE board_id='".$in['board_id']."' ");
			while($stages->next()){
				$this->db->query("DELETE FROM tblopportunity WHERE stage_id='".$stages->f('id')."'");
			}
			$this->db->query("DELETE FROM tblopportunity_stage WHERE board_id='".$in['board_id']."'");
			$this->db->query("DELETE FROM tblopportunity_board WHERE id='".$in['board_id']."' ");
			msg::success(gm('Changes saved'),'success');
			return true;
		}

		function updateStage(&$in){
			if(!$in['stage_id']){
				json_out($in);
			}
			$i=0;
			foreach($in['deals'] as $key=>$value){
				$this->db->query("UPDATE tblopportunity SET stage_id='".$in['stage_id']."',sort_order='".$i."' WHERE opportunity_id='".$value['deal_id']."' ");
				$i++;
			}
			msg::success(gm("Changes done"),"success");
			json_out($in);
		}

		function deleteStage(&$in){
			if(!$this->deleteStage_validate($in)){
				msg::error(gm('There are deals on current stage'),'error');
				json_out($in);
			}
			$this->db->query("DELETE FROM tblopportunity_stage WHERE id='".$in['stage_id']."'");
			msg::success(gm('Changes saved'),'success');
			json_out($in);
		}

		function deleteStage_validate(&$in){
			$is_ok=true;
			$deals=$this->db->field("SELECT COUNT(opportunity_id) FROM tblopportunity WHERE stage_id='".$in['stage_id']."' AND f_archived='0' ");
			if($deals){
				$is_ok=false;
			}
			return $is_ok;
		}

		function addDeal_validate(&$in){
			$v = new validation($in);
			$v->f('subject', 'Subject', 'required');
			$v->f('buyer_id', 'Customer', 'required');
			$v->f('close_date', 'Close Date', 'required');
			if(ark::$method=='addDeal'){
				$v->f('board_id', 'Board', 'required');
				$v->f('stage_id', 'Stage', 'required');
				$v->field('serial_number', 'Deal Nr', 'unique[tblopportunity.serial_number]',gm('Deal Nr').' '.gm('is already in use'));
			}else{
				$v->field('serial_number', 'Deal Nr', "unique[tblopportunity.serial_number.( opportunity_id!='".$in['opportunity_id']."')]",gm('Deal Nr').' '.gm('is already in use'));
			}	

			$v->field('created_by', gm('ID'), "required:exist[users.user_id]", gm("Invalid ID"),$this->database_users);
			return $v->run();
		}

		function addDeal(&$in){
			$f_archived = '0';
			if(!$this->addDeal_validate($in))
			{
				json_out($in);
				return false;
			}

			$created_date= date(ACCOUNT_DATE_FORMAT);
			$opportunity_date=strtotime('now');

			$details = array('table' 		=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
						'field'			    => $in['buyer_id'] ? 'customer_id' : 'contact_id',
						'value'		 	    => $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],
						'name'			    => substr($in['s_customer_id'], 0, strpos($in['s_customer_id'],' >')),
						'filter'		    => $in['buyer_id'] ? " AND is_primary='1' " : ""
		        );
	        $c_name = $details['name'];
	        //get buyer details
		    $buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
		    $buyer_details->next();
		    $customer_id = $in['buyer_id'];

	        if($in['buyer_id']){
			   $buyer_info = $this->db->query("SELECT customer_legal_type.name as l_name, customers.c_email
									  FROM customer_legal_type
									  LEFT JOIN customers ON customers.legal_type = customer_legal_type.id
									  WHERE customers.customer_id = '".$in['buyer_id']."' ");
			   $buyer_info->next();
			   $name = $buyer_info->f('l_name') ? $in['s_buyer_id'].' '.$buyer_info->f('l_name') : $in['s_buyer_id'];

			}else{
			   $buyer_info = $this->db->query("SELECT phone, cell, email FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
			   $buyer_info->next();
			   $c_email = $buyer_info->f('email');
			   $c_name = '';
			   $name = $details['name'];
			}
			$assigned_user_fname='';
			$assigned_user_lname='';
			if($in['assigned_user']){
				//$assigned_data=$this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$in['assigned_user']."' ");
				$assigned_data=$this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$in['assigned_user']]);
				$assigned_user_fname=$assigned_data->f('first_name');
				$assigned_user_lname=$assigned_data->f('last_name');
			}
			$same_address=0;
		    if($in['sameAddress']!=1){
		    	$same_address=$in['delivery_address_id'];
		    }

		    /*$stage_id=$this->db->field("SELECT id FROM tblopportunity_stage WHERE board_id='".$in['board_id']."' ORDER BY sort_order ASC");*/
		    $stage_id=$in['stage_id'];
		    $last_sort=$this->db->field("SELECT sort_order FROM tblopportunity WHERE stage_id='".$stage_id."' ORDER BY sort_order DESC ");
		    if(is_null($last_sort)){
		    	$last_sort=0;
		    }

			$in['opportunity_id']=$this->db->insert("INSERT INTO tblopportunity SET
							serial_number           =   '".addslashes($in['serial_number'])."',
	                        subject                 =   '".addslashes($in['subject'])."',
	                        expected_revenue        =   '".return_value($in['amount'])."',
	                        opportunity_date        =   '".$opportunity_date."',
	                        currency_type       	=   '".$in['currency_type']."',
	                        currency_rate       	=   '1',
	                        status              	=   '0',
	                        f_archived          	=   '".$f_archived."',
	                        buyer_id       	      	=   '".$in['buyer_id']."',
	                        contact_id				=   '".$in['contact_id']."',
	                        buyer_name       		=   '".addslashes($name)."',
	                        buyer_email       		=   '".$c_email."',
	                        buyer_address       	=   '".addslashes($buyer_details->f('address'))."',
	                        buyer_zip       		=   '".addslashes($buyer_details->f('zip'))."',
	                        buyer_city       		=   '".addslashes($buyer_details->f('city'))."',
	                        buyer_country_id      	=   '".addslashes($buyer_details->f('country_id'))."',
	                        buyer_state_id       	=   '".addslashes($buyer_details->f('state_id'))."',
				            seller_name       		=   '".addslashes(ACCOUNT_COMPANY)."',
	                        seller_d_address     	=   '".addslashes(ACCOUNT_DELIVERY_ADDRESS)."',
	                        seller_d_zip       		=   '".addslashes(ACCOUNT_DELIVERY_ZIP)."',
	                        seller_d_city         	=   '".addslashes(ACCOUNT_DELIVERY_CITY)."',
	                        seller_d_country_id   	=   '".addslashes(ACCOUNT_DELIVERY_COUNTRY_ID)."',
	                        seller_d_state_id     	=   '".addslashes(ACCOUNT_DELIVERY_STATE_ID)."',
	                        seller_b_address     	=   '".addslashes(ACCOUNT_BILLING_ADDRESS)."',
	                        seller_b_zip       		=   '".addslashes(ACCOUNT_BILLING_ZIP)."',
	                        seller_b_city       	=   '".addslashes(ACCOUNT_BILLING_CITY)."',
	                        seller_b_country_id   	=   '".ACCOUNT_BILLING_COUNTRY_ID."',
	                        seller_b_state_id     	=   '".ACCOUNT_BILLING_STATE_ID."',
	                        seller_bwt_nr         	=   '".addslashes(ACCOUNT_VAT_NUMBER)."',
							created             	=   '".$created_date."',
	                        created_by          	=   '".$in['created_by']."',
	                        author_id          		=   '".$_SESSION['u_id']."',
	                        last_upd            	=   '".$created_date."',
	                        last_upd_by         	=   '".$_SESSION['u_id']."',
	                        assigned_to			=   '".$in['assigned_user']."',
	                        assigned_fname		= '".$assigned_user_fname."',
	                        assigned_lname		= '".$assigned_user_lname."',
	                        email_language			= 	'".$in['email_language']."',
							exp_close_date			= 	'".strtotime($in['close_date'])."',
	                        stage_id 				= 	'".$stage_id."',
	                        free_field				= '".$in['free_field']."',
	                        same_address					='".$same_address."',
							main_address_id				='".$in['main_address_id']."',
							sort_order					='".($last_sort+1)."',
							identity_id 			='".$in['identity_id']."',
							source_id 				='".$in['source_id']."',
							type_id 				='".$in['type_id']."',
							segment_id 				='".$in['segment_id']."'");


	        $count = $this->db->field("SELECT COUNT(opportunity_id) FROM tblopportunity ");
			if($count == 1){
				doManageLog('Created the first opportunity.');
			}
			$in['stage_id']='';
			msg::success(gm("Deal was created"),"success");
			insert_message_log($this->pag,'{l}Opportunity was created by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['opportunity_id'],false,$_SESSION['u_id']);

			$tracking_data=array(
		            'target_id'         => $in['opportunity_id'],
		            'target_type'       => '11',
		            'target_buyer_id'   => $in['buyer_id'],
		            'lines'             => array()
		          );
		    addTracking($tracking_data);

			json_out($in);
		}

		function updateDeal_validate(&$in){
			$is_ok = true;
			$v = new validation($in);
			$v->field('opportunity_id', 'ID', 'required:exist[tblopportunity.opportunity_id]', "Invalid Id");
			$v->field('created_by', gm('ID'), "required:exist[users.user_id]", gm("Invalid ID"),$this->database_users);

			$is_ok = $v->run();

			if (!$this->addDeal_validate($in))
			{
				$is_ok = false;
			}
			return $is_ok;
		}

		function updateDeal(&$in){

			if(!$this->updateDeal_validate($in)){
				json_out($in);
				return false;
			}

			$updatedd_date= date(ACCOUNT_DATE_FORMAT.' H:i:s');

			$this->db->query("UPDATE tblopportunity SET
	                subject                 =   '".$in['subject']."',
	                serial_number           =   '".$in['serial_number']."',
	                expected_revenue        =   '".return_value($in['amount'])."',
	                exp_close_date			= 	'".strtotime($in['close_date'])."',
	                buyer_id       	      	=   '".$in['buyer_id']."',
	                contact_id				=   '".$in['contact_id']."',
	                buyer_reference     	=   '".$in['buyer_reference']."',
	                buyer_name       		=   '".$in['buyer_name']."',
	                buyer_address       	=   '".$in['buyer_address']."',
	                buyer_zip       		=   '".$in['buyer_zip']."',
	                buyer_city       		=   '".$in['buyer_city']."',
	                buyer_country_id      	=   '".$in['buyer_country_id']."',
	                buyer_state_id       	=   '".$in['buyer_state_id']."',
	                created_by				=   '".$in['created_by']."',
				    seller_name       		=   '".addslashes(ACCOUNT_COMPANY)."',
	                seller_d_address     	=   '".addslashes(ACCOUNT_DELIVERY_ADDRESS)."',
	                seller_d_zip       		=   '".addslashes(ACCOUNT_DELIVERY_ZIP)."',
	                seller_d_city         	=   '".addslashes(ACCOUNT_DELIVERY_CITY)."',
	                seller_d_country_id   	=   '".addslashes(ACCOUNT_DELIVERY_COUNTRY_ID)."',
	                seller_d_state_id     	=   '".addslashes(ACCOUNT_DELIVERY_STATE_ID)."',
	                seller_b_address     	=   '".addslashes(ACCOUNT_BILLING_ADDRESS)."',
	                seller_b_zip       		=   '".addslashes(ACCOUNT_BILLING_ZIP)."',
	                seller_b_city       	=   '".addslashes(ACCOUNT_BILLING_CITY)."',
	                seller_b_country_id   	=   '".ACCOUNT_BILLING_COUNTRY_ID."',
	                seller_b_state_id     	=   '".ACCOUNT_BILLING_STATE_ID."',
	                seller_bwt_nr         	=   '".addslashes(ACCOUNT_VAT_NUMBER)."',
					last_upd_by         	=   '".$_SESSION['u_id']."',
					identity_id 			='".$in['identity_id']."',
					source_id 				='".$in['source_id']."',
					type_id 				='".$in['type_id']."',
					segment_id 				='".$in['segment_id']."'
					WHERE opportunity_id = '".$in['opportunity_id']."' ");

			msg::success(gm("Deal has been updated"),"success");
			insert_message_log($this->pag,'{l}Opportunity has been updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['opportunity_id']);
			json_out($in);
		}

		function deleteDeal(&$in){
			if(!$this->deleteDeal_validate($in)){
				json_out($in);
				return false;
			}
			$tracking_trace=$this->db->field("SELECT trace_id FROM tblopportunity WHERE opportunity_id='".$in['opportunity_id']."' ");
			$this->db->query("DELETE FROM tblopportunity WHERE opportunity_id = '".$in['opportunity_id']."'");
			if($tracking_trace){
		      $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
		      $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
		    }
			msg::success(gm("Deal has been deleted"),"success");
			return true;
		}

		function deleteDeal_validate(&$in)
		{
			if(!$in['opportunity_id']){
				msg::error( gm("Invalid ID"),'error');
				return false;
			}
			return true;
		}

		function archiveDeal(&$in)
		{
			if(!$this->deleteDeal_validate($in)){
				json_out($in);
				return false;
			}
			$tracking_trace=$this->db->field("SELECT trace_id FROM tblopportunity WHERE opportunity_id='".$in['opportunity_id']."' ");
			$this->db->query("UPDATE tblopportunity SET f_archived='1' WHERE opportunity_id='".$in['opportunity_id']."' ");
			if($tracking_trace){
			    $this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
			}
			msg::success ( gm('Changes saved'),'success');
			insert_message_log($this->pag,'{l}Opportunity has been archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['opportunity_id'],false);
			return true;
		}

		function archiveSelectedDeals(&$in)
		{
			if(empty($in['items'])){
				json_out($in);
				return false;
			}
			$items_list ='';
			foreach($in['items'] as $key => $value){
				$items_list .= $value.',';
			}
			$items_list=rtrim($items_list, ',');
			if($items_list){
				$this->db->query("UPDATE tblopportunity SET f_archived='1' WHERE opportunity_id  IN (".$items_list.") ");
				$this->db->query("UPDATE tracking SET archived='1' WHERE target_id IN (".$items_list.") AND target_type='11' ");
			}
			
			msg::success ( gm('Changes saved'),'success');
			foreach($in['items'] as $key => $value){
				unset($_SESSION['selected_items'][$value]);
				insert_message_log($this->pag,'{l}Opportunity has been archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$value,false);
			}
			unset($_SESSION['selected_items']);
			
			return true;
		}

		function modifyStage(&$in)
		{
			if(empty($in['items'])){
				json_out($in);
				return false;
			}
			$items_list ='';
			foreach($in['items'] as $key => $value){
				$items_list .= $key.',';
			}
			$items_list=rtrim($items_list, ',');
			if($items_list){
				$this->db->query("UPDATE tblopportunity SET stage_id='".$in['stage_id']."' WHERE opportunity_id  IN (".$items_list.") ");
			}
			msg::success ( gm('Changes saved'),'success');
			foreach($in['items'] as $key => $value){
				unset($_SESSION['selected_items'][$key]);
				insert_message_log($this->pag,'{l}Opportunity has been updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$key,false);
			}
			unset($_SESSION['selected_items']);
			$in['stage_id']=$in['old_stage_id'];
			$in['board_id']=$in['old_board_id'];
			
			return true;
		}

		function modifyDealStage(&$in)
		{
			if(!$this->modifyDealStage_validate($in)){
				json_out($in);
				return false;
			}
		
			$this->db->query("UPDATE tblopportunity SET stage_id='".$in['stage_id']."' WHERE opportunity_id  ='".$in['opportunity_id']."' ");
			msg::success ( gm('Changes saved'),'success');
	
			insert_message_log($this->pag,'{l}Opportunity has been updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['opportunity_id'],false);
			
			return true;
		}

		function modifyDealStage_validate(&$in){
			$v = new validation($in);
			$v->field('opportunity_id', 'ID', 'required:exist[tblopportunity.opportunity_id]', "Invalid Id");
			$v->field('stage_id', gm('ID'), "required:exist[tblopportunity_stage.id]", gm("Invalid ID"));

			return $v->run();
		}

		function updateDealNotes(&$in)
		{ 
			if(!$this->updateDealNotes_validate($in)){
				json_out($in);
				return false;
			}
		
			$this->db->query("UPDATE tblopportunity SET notes='".$in['deal_notes']."' WHERE opportunity_id  ='".$in['opportunity_id']."' ");
			msg::success ( gm('Changes saved'),'success');
	
			insert_message_log($this->pag,'{l}Opportunity has been updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['opportunity_id'],false);
			
			return true;
		}


		function updateDealNotes_validate(&$in){
			$v = new validation($in);
			$v->field('opportunity_id', 'ID', 'required:exist[tblopportunity.opportunity_id]', "Invalid Id");

			return $v->run();
		}

		function save_view_list(&$in){
			//0 - list view, 1- card board
/*			$view='';
			if($in['view']==0){
				$view = '1';
			}elseif($in['view']==2){
				$view = '2';
			}else{
				$view = '0';
			}*/
			$this->db->query("UPDATE settings SET value = '".$in['view']."' WHERE constant_name = 'DEAL_VIEW_LIST' ");
			msg::success (gm('Changes have been saved.'),'success');
			if(!$in['do_continue']){
				json_out($in);
			}
			return true;
		}

		function selectItems(&$in)
		  {
		  	if($in['all']){
		  		if($in['value'] == true){
		  			foreach ($in['item'] as $key => $value) {
					    $_SESSION['selected_items'][$value]= $in['value'];
			  		}
		  		}else{
		  			foreach ($in['item'] as $key => $value) {
					    // $_SESSION['selected_items'][$value]= $in['value'];
					    unset($_SESSION['selected_items'][$value]);
			  		}
			  		unset($_SESSION['selected_items']);
				  }
		  	}else{
			    if($in['value'] == true){
			    	$_SESSION['selected_items'][$in['item']]= $in['value'];
			    }else{
			    	unset($_SESSION['selected_items'][$in['item']]);
			    }
			  }
		    return true;
		  }


		function UpdateActivity(&$in)
		{
			if($in['reminder_id']){
				$original_date=strtotimeActivity($in['date']);
				switch($in['reminder_id']){
	                case '2':
	                    $in['reminder_event']=$in['date'];
	                    break;
	                case '3':
	                	$in['reminder_event']= date('c',$original_date-(5*60));
	                    break;
	                case '4':
	                	$in['reminder_event']= date('c',$original_date-(10*60));
	                    break;
	                case '5':
	                	$in['reminder_event']= date('c',$original_date-(15*60));
	                    break;
	                case '6':
	                	$in['reminder_event']= date('c',$original_date-(30*60));
	                    break;
	                case '7':
	                	$in['reminder_event']= date('c',$original_date-(60*60));
	                    break;
	                case '8':
	                	$in['reminder_event']= date('c',$original_date-(24*60*60));
	                    break;
	            }			
			}
			if($in['task_type'] != '2'){
				$this->modify_activity_date($in);
			}
			$this->db->query("UPDATE customer_contact_activity SET contact_activity_note='".addslashes($in['comment'])."', reminder_id='".$in['reminder_id']."',customer_id='".$in['customer_id']."' WHERE customer_contact_activity_id='".$in['log_id']."' ");
			$this->db->query("UPDATE logging SET field_value='".$in['customer_id']."',log_comment='".addslashes($in['comment'])."',to_user_id='".$in['to_user_id']."' WHERE log_id='".$in['log_code']."' ");
			$this->db->query("UPDATE logging_tracked SET customer_id='".$in['customer_id']."' WHERE activity_id='".$in['log_id']."' AND log_id='".$in['log_code']."'");
			$this->db->query("DELETE FROM customer_contact_activity_contacts WHERE activity_id='".$in['log_id']."' ");
			foreach ($in['contact_ids'] as $contact_id) {
				$this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['log_id']."',
					contact_id = '".$contact_id."',
					action_type = '0'");
			}
			$this->log_comment($in);
			$this->update_status($in);
			if($in['task_type'] == '4'){
				$this->db->query("UPDATE customer_meetings SET 
					subject='".addslashes(utf8_encode($in['comment']))."',
					location='".$in['location_meet']."',
					start_date='".strtotimeActivity($in['date'])."',
					end_date='".strtotime($in['end_d_meet'])."',
					message='".strip_tags($in['log_comment'])."',
					customer_id='".$in['customer_id']."'
					WHERE activity_id='".$in['log_id']."' ");
			}
			// json_out($in);
		}  

		function modify_activity_date(&$in){

			$date_activity=strtotimeActivity($in['date']);
			
			$this->db->query("UPDATE logging SET type_of_call='".$in['type_of_call_id']."' , due_date='".$date_activity."'  WHERE  log_id = '".$in['log_code']."' ");

			if($in['type_of_call_id']){
	            $in['type_of_call_view']=gm('incoming call');
			}else{
	            $in['type_of_call_view']=gm('outgoing call');
			} 
			return true;
		}

		function log_comment(&$in)
		{
			$date = time();
			$in['log_comment_date'] = date(ACCOUNT_DATE_FORMAT,$date).', '.date('H:i',$date);
			$this->db->query("UPDATE customer_contact_activity SET log_comment='".$in['log_comment']."', log_comment_date='".$date."' WHERE customer_contact_activity_id='".$in['log_id']."' ");
			$log_id=$this->db->field("SELECT log_id FROM logging_tracked WHERE activity_id='".$in['log_id']."' GROUP BY activity_id ");
			$this->db->query("UPDATE logging SET message='".$in['log_comment']."' WHERE log_id='".$log_id."' ");
			$is_zen=$this->db->field("SELECT zen_ticket_id FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['log_id']."'");
			if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && $is_zen){
				$zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
				$zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
				$zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
				$ch = curl_init();
				$headers=array('Content-Type: application/json');
				$zen_body=array();
				$zen_body['ticket']=array(
					'comment'		=> strip_tags($in['comments'])
				);
				$zen_body=json_encode($zen_body);
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
				curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
		    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	        	curl_setopt($ch, CURLOPT_POSTFIELDS, $zen_body);
		    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    	curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/tickets/'.$is_zen.'.json');
		    	$put = curl_exec($ch);
		    	$info = curl_getinfo($ch);
			}
			$in['comments'] = stripslashes($in['comments']);
			return true;
		}

		function update_status(&$in){
			$reminder_date=strtotimeActivity($in['reminder_event']);

	        if($in['reminder_event']){
	         	$this->db->query("UPDATE logging SET reminder_date='".$reminder_date."' WHERE log_id='".$in['log_code']."'");

	         	$activity_id=$this->db->field("SELECT logging_tracked.activity_id FROM logging INNER JOIN logging_tracked ON logging.log_id=logging_tracked.log_id 
	         		WHERE logging.log_id='".$in['log_code']."'
	                GROUP BY logging_tracked.activity_id
	         		");

	         	$this->db->query("UPDATE customer_contact_activity SET reminder_date='".$reminder_date."' WHERE customer_contact_activity_id='".$activity_id."'");
	        }
		    if(!$in['not_status_change']){
				if($in['status_change'] == '0'){
					$this->db->query("UPDATE logging SET status_other='1',finished='0',finish_date='0' WHERE log_id='".$in['log_code']."'");
				}else {
					$this->db->query("UPDATE logging SET finished='1',finish_date='".time()."' WHERE log_id='".$in['log_code']."'");
				}
			}	
	        
			msg::success ( gm("Status changed succesfully!"),'success');
			return true;
		}

	}
?>