<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

    ini_set('memory_limit','2000M');
    setcookie('Akti-Installations-Export','6-0-0',time()+3600,'/');
    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

    $in['installation_ids'] ='';
    if($_SESSION['add_to_pdf_inst']){
      foreach ($_SESSION['add_to_pdf_inst'] as $key => $value) {
        $in['installation_ids'] .= $key.',';
      }
    }
    $in['installation_ids'] = rtrim($in['installation_ids'],',');
    if(!$in['installation_ids']){
      $in['installation_ids']= '0';
    }

    $db=new sqldb();

    $headers=array(
            'ID',
            'NAME',
            'ACCOUNT NAME',
            'ADDRESS',
            'CITY',
            'ZIP',
            'COUNTRY',
            'TEMPLATE NAME'
    );

    $filename ="export_installations.csv";

    $final_data=array();

    $filter = " WHERE installations.id IN (".$in['installation_ids'].") ";
    $order_by = " ORDER BY name ASC ";
    /*$order_by_array=array('name', 'serial_number', 'customer_name', 'address', 'city', 'country_id');
    $order_by = " ORDER BY name ASC ";

    if(!empty($in['search'])){
        $filter.=" AND (installations.serial_number LIKE '%".$in['search']."%' OR installations.name LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%') ";
    }
    if(!empty($in['order_by'])){
        if(in_array($in['order_by'], $order_by_array)){
            $order = " ASC ";
            if($in['desc'] == 'true' || $in['desc'] == '1'){
                $order = " DESC ";
            }
            $order_by =" ORDER BY ".$in['order_by']." ".$order;
        }
    }*/

    $fields_headers=array();
    $dynamic_headers=$db->query("SELECT installations_data.*
        FROM installations 
        INNER JOIN installations_data ON installations.id=installations_data.installation_id
        LEFT JOIN customers ON installations.customer_id=customers.customer_id
        ".$filter." GROUP BY installations_data.field_id ORDER BY installations_data.label_name ASC");
    while($dynamic_headers->next()){
        array_push($fields_headers,$dynamic_headers->f('field_id'));
        array_push($headers,stripslashes($dynamic_headers->f('label_name')));
    }

    $installations=$db->query("SELECT installations.*, installation_templates.name AS template_name, customers.name as customer_name, customer_addresses.address, customer_addresses.city,customer_addresses.zip, customer_addresses.country_id,country.name AS country_name
        FROM installations
        LEFT JOIN installation_templates ON installations.template_id=installation_templates.template_id
        LEFT JOIN customers ON installations.customer_id=customers.customer_id
        LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id
        LEFT JOIN country ON customer_addresses.country_id=country.country_id
        ".$filter.$order_by);

    while($installations->next()){
        $item=array(
            $installations->f('serial_number'),
            stripslashes($installations->f('name')),
            stripslashes($installations->f('customer_name')),
            stripslashes(preg_replace("/[\n\r]/","",$installations->f('address'))),
            stripslashes(preg_replace("/[\n\r]/","",$installations->f('city'))),
            $installations->f('zip'),
            stripslashes($installations->f('country_name')),
            stripslashes(preg_replace("/[\n\r]/","",$installations->f('template_name')))
        );
        $fields_tmp=array();
        $installation_fields=$db->query("SELECT * FROM installations_data WHERE installation_id='".$installations->f('id')."' ORDER BY label_name ASC");
        while($installation_fields->next()){
            switch($installation_fields->f('p_type')){
                case '10':
                case '11':
                    $name_final=htmlspecialchars_decode($db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$installation_fields->f('p_id')."' "));
                    break;
                default:
                    if($installation_fields->f('type')==3){
                        $name_final=(int)$installation_fields->f('name') ? date(ACCOUNT_DATE_FORMAT,(int)$installation_fields->f('name')): '';
                    }else{
                        if($installation_fields->f('type')==1){
                            $name_final=htmlspecialchars_decode(stripslashes($db->field("SELECT name FROM installation_fields_drops WHERE id='".$installation_fields->f('name')."' ")));
                        }else{
                            $name_final=stripslashes($installation_fields->f('name'));
                        }
                    }
                    break;
            }
            $fields_tmp[$installation_fields->f('field_id')]=preg_replace("/[\n\r]/","",$name_final);
        }
        foreach($fields_headers as $value){
            if(array_key_exists($value, $fields_tmp) !== false){
                array_push($item,$fields_tmp[$value]);
            }else{
                array_push($item,'');
            }
        }
        $fields_tmp=array();
        unset($fields_tmp);
        array_push($final_data, $item);
        $item=array();
        unset($item);
    }
/*
    $from=INSTALLPATH.'upload/'.DATABASE_NAME.'/export_installations_'.time().'_'.$_SESSION['u_id'].'.csv';
    $fp = fopen($from, 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $installation_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    ark::loadLibraries(array('PHPExcel'));
    $objReader = PHPExcel_IOFactory::createReader('CSV');

    $objPHPExcel = $objReader->load($from);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    unlink($from);

    doQueryLog();

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="export_installations.xls"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');   
    exit();*/

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');

    $fp = fopen("php://output", 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    doQueryLog();
    exit();


    // set column width to auto
    foreach(range('A','W') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
    }

    $objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
    $rows_format='A1:A'.$xlsRow;
    $objPHPExcel->getActiveSheet()->getStyle($rows_format)
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    $objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('upload/'.$filename);





    define('ALLOWED_REFERRER', '');
    define('BASE_DIR','upload/');
    define('LOG_DOWNLOADS',false);
    define('LOG_FILE','downloads.log');
    $allowed_ext = array (

    // archives
    'zip' => 'application/zip',

    // documents
    'pdf' => 'application/pdf',
    'doc' => 'application/msword',
    'xls' => 'application/vnd.ms-excel',
    'ppt' => 'application/vnd.ms-powerpoint',
    'csv' => 'text/csv',

    // executables
    //'exe' => 'application/octet-stream',

    // images
    'gif' => 'image/gif',
    'png' => 'image/png',
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',

    // audio
    'mp3' => 'audio/mpeg',
    'wav' => 'audio/x-wav',

    // video
    'mpeg' => 'video/mpeg',
    'mpg' => 'video/mpeg',
    'mpe' => 'video/mpeg',
    'mov' => 'video/quicktime',
    'avi' => 'video/x-msvideo'
    );
    ####################################################################
    ###  DO NOT CHANGE BELOW
    ####################################################################

    // If hotlinking not allowed then make hackers think there are some server problems
    if (ALLOWED_REFERRER !== ''
    && (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
    ) {
    die("Internal server error. Please contact system administrator.");
    }
    $fname = basename($filename);
    function find_file ($dirname, $fname, &$file_path) {
    $dir = opendir($dirname);
    while ($file = readdir($dir)) {
        if (empty($file_path) && $file != '.' && $file != '..') {
        if (is_dir($dirname.'/'.$file)) {
            find_file($dirname.'/'.$file, $fname, $file_path);
        }
        else {
            if (file_exists($dirname.'/'.$fname)) {
            $file_path = $dirname.'/'.$fname;
            return;
            }
        }
        }
    }

    } // find_file
    // get full file path (including subfolders)
    $file_path = '';
    find_file('upload', $fname, $file_path);
    if (!is_file($file_path)) {
    die("File does not exist. Make sure you specified correct file name.");
    }
    // file size in bytes
    $fsize = filesize($file_path);
    // file extension
    $fext = strtolower(substr(strrchr($fname,"."),1));
    // check if allowed extension
    if (!array_key_exists($fext, $allowed_ext)) {
    die("Not allowed file type.");
    }
    // get mime type
    if ($allowed_ext[$fext] == '') {
    $mtype = '';
    // mime type is not set, get from server settings
    if (function_exists('mime_content_type')) {
        $mtype = mime_content_type($file_path);
    }
    else if (function_exists('finfo_file')) {
        $finfo = finfo_open(FILEINFO_MIME); // return mime type
        $mtype = finfo_file($finfo, $file_path);
        finfo_close($finfo);
    }
    if ($mtype == '') {
        $mtype = "application/force-download";
    }
    }
    else {
    // get mime type defined by admin
    $mtype = $allowed_ext[$fext];
    }
    doQueryLog();
    // set headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Type: $mtype");
    header("Content-Disposition: attachment; filename=\"$fname\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $fsize);
    // download
    //@readfile($file_path);
    $file = @fopen($file_path,"rb");
    if ($file) {
    while(!feof($file)) {
        print(fread($file, 1024*8));
        flush();
        if (connection_status()!=0) {
        @fclose($file);

        die();
        }
    }
    @fclose($file);
    }

?>