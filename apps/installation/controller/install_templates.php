<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

	$l_r = ROW_PER_PAGE;

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	$filter = 'WHERE 1=1 ';
	$order_by_array=array('name', 'nr');
	$order_by = " ORDER BY name ASC ";

	if(!empty($in['search'])){
		$filter.=" AND name LIKE '%".$in['search']."%' ";
	}
	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

	if(!empty($in['order_by'])){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == 'true' || $in['desc']=='1'){
				$order = " DESC ";
			}
			if($in['order_by']=='nr' ){
				$filter_limit =' ';
	   			$order_by ='';
		    }else{
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
				$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
			}
		}
	}

	$template=$db->query("SELECT * FROM installation_templates ".$filter.$order_by.$filter_limit);
	$max_rows=$db->query("SELECT * FROM installation_templates ".$filter)->records_count();
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	//$template->move_to($offset*$l_r);
	$j=0;
	while($template->move_next() ){
		$fields_nr=$db->field("SELECT COUNT(field_id) FROM installation_template_fields WHERE template_id='".$template->f('template_id')."' "); 
		$item=array(
			'template_id'		=> $template->f('template_id'),
			'name'				=> stripslashes($template->f('name')),
			'nr'				=> $fields_nr,
			'delete_link'		=> array('do'=>'installation-install_templates-ifields-deleteTemplate','template_id'=>$template->f('template_id'))
		);
		array_push($result['query'], $item);
		//$j++;
	}

	$result['lr']=$l_r;


	if($in['order_by']=='nr' ){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'], SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by'], SORT_DESC);
	    }

	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	}

json_out($result);
?>