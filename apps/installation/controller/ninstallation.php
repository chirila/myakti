<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class InstallationView extends Controller{
	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Data(){
	    global $config;
		$in = $this->in;
		$output=array();

		$installation_data=$this->db->query("SELECT * FROM installations WHERE id='".$in['installation_id']."' ");
		if(!$installation_data->f('id')){
			msg::error('Installation does not exist','error');
			$in['item_exists']= false;
		    json_out($in);
		}
		$output['item_exists']=true;
		$output['name']=stripslashes($installation_data->f('name'));
		$output['template_name']=$this->db->field("SELECT name FROM installation_templates WHERE template_id='".$installation_data->f('template_id')."' ");	
		$output['buyer_name']=$this->db->field("SELECT name FROM customers WHERE customer_id='".$installation_data->f('customer_id')."' ");
		$output['buyer_id']=$installation_data->f('customer_id');
		$output['address']='';
		$output['zip']='';
		$output['city']='';
		$output['country']='';
		$address=$this->db->query("SELECT customer_addresses.*,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 WHERE customer_addresses.address_id='".$installation_data->f('address_id')."' ");
		while($address->next()){
			$output['address']=$address->f('address');
			$output['zip']=$address->f('zip');
			$output['city']=$address->f('city');
			$output['country']=get_country_name($address->f('country_id'));
			if($address->f('address') ){
		        $country_n = get_country_name($address->f('country_id') );
		        if(!$address->f('zip') && $address->f('city'))
		        {
		            $geo_address = $address->f('address').', '.$address->f('city').', '.$country_n;
		        }elseif(!$address->f('city') && $address->f('zip'))
		        {
		            $geo_address = $address->f('address').', '.$address->f('zip').', '.$country_n;
		        }elseif(!$address->f('zip') && !$address->f('city'))
		        {
		            $geo_address = $address->f('address').', '.$country_n;
		        }else
		        {
		            $geo_address = $address->f('address').', '.$address->f('zip').' '.$address->f('city').', '.$country_n;
		        }
		        /*$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.urlencode($geo_address).'&key=AIzaSyAB-QkaXPK9zZoAlEnVUN7L03OlABkUGSI&sensor=false');
		        $map_data= json_decode($geocode);
		        $lat = $map_data->results[0]->geometry->location->lat;
		        $long = $map_data->results[0]->geometry->location->lng;
		        $coords=array('latitude'=>$lat,'longitude'=>$long);*/
		       /* $output['full_address_map_src']=$geo_address ? 'https://www.google.com/maps/embed/v1/place?key=AIzaSyAwlmPeY64Lpr4QlwFdWR_gMF1ONK3OyTI&q='.urlencode($geo_address) : '';*/
		          $output['full_address_map_src'] = $geo_address ? 'https://www.google.com/maps/embed/v1/place?key=' . $config['GOOGLE_MAPS_API_KEY'] . '&q=' . $geo_address : '';
		    }
		}
		$is_left=0;
		$is_right=0;
		$output['template']=array('left_fields'=>array(),'right_fields'=>array());
		$left_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$in['installation_id']."' AND position='1' ORDER BY id ASC");
		while($left_fields->next()){
			switch($left_fields->f('p_type')){
				case '10':
				case '11':
					$name_final=htmlspecialchars_decode($this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$left_fields->f('p_id')."' "));
					break;
				default:
					if($left_fields->f('type')==3){
						$name_final=(int)$left_fields->f('name') ? date(ACCOUNT_DATE_FORMAT,(int)$left_fields->f('name')): '';
					}else{
						if($left_fields->f('type')==1){
							$name_final=htmlspecialchars_decode(stripslashes($this->db->field("SELECT name FROM installation_fields_drops WHERE id='".$left_fields->f('name')."' ")));
						}else{
							$name_final=stripslashes($left_fields->f('name'));
						}
					}
					break;
			}
			$temp_left=array(
					'name'			=> stripslashes($left_fields->f('label_name')),
					'name_final'		=> $name_final,
				);
			array_push($output['template']['left_fields'], $temp_left);
			$is_left++;
		}
		$right_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$in['installation_id']."' AND position='2' ORDER BY id ASC");
		while($right_fields->next()){
			switch($right_fields->f('p_type')){
				case '10':
				case '11':
					$name_final=htmlspecialchars_decode($this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$right_fields->f('p_id')."' "));
					break;
				default:
					if($right_fields->f('type')==3){
						$name_final=(int)$right_fields->f('name') ? date(ACCOUNT_DATE_FORMAT,(int)$right_fields->f('name')): '';
					}else{
						if($right_fields->f('type')==1){
							$name_final=htmlspecialchars_decode(stripslashes($this->db->field("SELECT name FROM installation_fields_drops WHERE id='".$right_fields->f('name')."' ")));
						}else{
							$name_final=stripslashes($right_fields->f('name'));
						}
					}
					break;
			}
			$temp_right=array(
					'name'			=> stripslashes($right_fields->f('label_name')),
					'name_final'		=> $name_final,
				);
			array_push($output['template']['right_fields'], $temp_right);
			$is_right++;
		}
		$output['address_id']=$installation_data->f('address_id');
		$output['show_more']=($is_left>4 || $is_right>4) ? true : false;
		$output['coords'] = ($coords && $coords['latitude'] && $coords['longitude']) ? $coords : array();
		$output['installation_id']=$in['installation_id'];
		//$output['drop_info']= array('drop_folder' => 'installations', 'customer_id' => $installation_data->f('customer_id'), 'item_id' => $in['installation_id'], 'isConcact' => '0', 'serial_number' => $installation_data->f('serial_number'));
		//$drop_connected=$this->db->field("SELECT active FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
		//$output['drop_active']=$drop_connected ? true : false;
		$output['s3_info']=array('s3_folder'=>'installation','id'=>$in['installation_id']);
		$output['interventions']=$this->get_interventions($in);
		$this->out = $output;
	}

	public function get_interventions(&$in){
		$result=array('list'=>array());
		$ints=$this->db->query("SELECT servicing_support.*,tracking.creation_date,tracking.creation_user_id FROM servicing_support
				LEFT JOIN tracking ON servicing_support.service_id=tracking.target_id AND tracking.target_type='5'
			 	WHERE installation_id='".$in['installation_id']."' ORDER BY tracking.creation_date DESC");
		while($ints->next()){
			$enddate_txt='';
			if($ints->f('startdate') && $ints->f('duration')){
				if($ints->f('startdate')+$ints->f('duration')>24){
					$enddate_txt=date(ACCOUNT_DATE_FORMAT,$ints->f('planeddate')+86400).' '.number_as_hour($ints->f('startdate')+$ints->f('duration')-24);				
				}else{
					$enddate_txt=number_as_hour($ints->f('startdate')+$ints->f('duration'));
				}
			}
			$collaborators='';
			$service_users=$this->db->query("SELECT user_id FROM servicing_support_users WHERE service_id='".$ints->f('service_id')."' ");
			while($service_users->next()){
				$collaborators.=",".get_user_name($service_users->f('user_id'))."";
			}
			$collaborators=ltrim($collaborators,",");

			if($ints->f('status') == '1'){
				$status = gm('Planned');
			}elseif($ints->f('status')=='2'){
				if($ints->f('is_recurring')=='0' && $ints->f('active')=='1' && $ints->f('accept')=='1'){
					$status = gm('Accepted');
				}else{
					$status = gm('Closed');
				}
			}else{
				$status = gm('Draft');
			}

			$item=array(
				'id'				=> $ints->f('service_id'),
				'serial_number'		=> $ints->f('serial_number'),
				'img'				=> 'interventionsok',
				'time'				=> date(ACCOUNT_DATE_FORMAT,$ints->f('creation_date')),
				'hour'				=> date('H:i',$ints->f('creation_date')),
				'type'				=> gm('Intervention'),
				'pdf_link'			=> 'index.php?do=maintenance-print&service_id='.$ints->f('service_id'),
				'author'			=> get_user_name($ints->f('creation_user_id')),
				'planeddate'		=> $ints->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$ints->f('planeddate')) : '',
				'startdate_txt'		=> $ints->f('startdate') ? number_as_hour($ints->f('startdate')) : '',
				'enddate_txt'		=> $enddate_txt,
				'collaborators'		=> $collaborators,
				'status'			=> $ints->f('status'),
				'type_status'		=> $status
			);
			array_push($result['list'],$item);
		}
		$result['intervention_more']=false;
		return $result;
	}

}

	$install = new InstallationView($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $install->output($install->$fname($in));
	}

	$install->get_Data();
	$install->output();
?>