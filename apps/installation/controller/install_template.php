<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class InstallTempEdit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getInstall(){
		if($this->in['template_id'] == 'tmp'){ //add
			$this->getAddInstall();
		}elseif ($this->in['template']) { # create quote template
			$this->getTemplateInstall();
		}else{ # edit
			$this->getEditInstall();
		}
	}

	public function getAddInstall(){
		$in = $this->in;

		$output=array();

		$next_function = 'installation-install_template-ifields-addTemplate';

		$output['fields']=$this->get_activeFields();
		$output['template_id']=$in['template_id'];
		$output['fields1']=array();
		$output['fields2']=array();
		$output['do_next']=$next_function;

		$this->out = $output;
	}

	public function getEditInstall(){
		$in = $this->in;
		$output=array();
		$next_function = 'installation-install_template-ifields-updateTemplate';

		$output['name']=$this->db->field("SELECT name FROM installation_templates WHERE template_id='".$in['template_id']."'");
		$output['fields1']=array();
		$output['fields2']=array();
		$fields=$this->get_activeFields();
		$fields1=$this->db->query("SELECT installation_template_fields.*,installation_fields.name,installation_fields.sort as field_sort FROM installation_template_fields
			LEFT JOIN installation_fields ON installation_template_fields.field_id=installation_fields.field_id 
			WHERE installation_template_fields.template_id='".$in['template_id']."' AND installation_template_fields.field_position='1' ORDER BY sort ASC");
		while($fields1->next()){
			$temp1=array(
				'field_id'  		=> $fields1->f('field_id'),
				'name'				=> stripslashes($fields1->f('name')),
				'sort'				=> $fields1->f('field_sort'),
			);
			array_push($output['fields1'], $temp1);
			foreach($fields as $key=>$value){
				if($value['field_id'] == $fields1->f('field_id')){
					array_splice($fields, $key, 1);
					break;
				}
			} 
		}
		$fields2=$this->db->query("SELECT installation_template_fields.*,installation_fields.name,installation_fields.sort as field_sort FROM installation_template_fields
			LEFT JOIN installation_fields ON installation_template_fields.field_id=installation_fields.field_id 
			WHERE installation_template_fields.template_id='".$in['template_id']."' AND installation_template_fields.field_position='2' ORDER BY sort ASC");
		while($fields2->next()){
			$temp2=array(
				'field_id'  		=> $fields2->f('field_id'),
				'name'				=> stripslashes($fields2->f('name')),
				'sort'				=> $fields2->f('field_sort'),
			);
			array_push($output['fields2'], $temp2);
			foreach($fields as $key=>$value){
				if($value['field_id'] == $fields2->f('field_id')){
					array_splice($fields, $key, 1);
					break;
				}
			}
		}

		$output['icon']=$this->db->field("SELECT icon_url FROM installation_templates WHERE template_id='".$in['template_id']."'");
		$output['fields']=$fields;
		$output['template_id']=$in['template_id'];
		$output['do_next']=$next_function;
		$this->out = $output;
	}

	public function get_activeFields(){
		$output=array();

		$fields=$this->db->query("SELECT * FROM installation_fields WHERE sort='0' || sort>='10' ");
		while($fields->next()){
			switch($fields->f('sort')){
				case '10':
					$field_name=gm('Product');
					break;
				case '11':
					$field_name=gm('Service');
					break;
				default:
					$field_name=stripslashes($fields->f('name'));
					break;
			}
			$item=array(
				'name'				=> $field_name,
				'field_id'			=> $fields->f('field_id')
			);
			array_push($output, $item);
		}
		return $output;
	}

}
	$install = new InstallTempEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $install->output($install->$fname($in));
	}

	$install->getInstall();
	$install->output();

?>