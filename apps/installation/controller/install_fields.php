<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array('query' => array(), 'max_rows' => array());
$l_r = ROW_PER_PAGE;
$result['lr'] = $l_r;
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

	$fields=$db->query("SELECT * FROM installation_fields WHERE sort='0' ORDER BY field_id $filter_limit");
	while($fields->next()){
		$item=array(
			'name'				=> stripslashes($fields->f('name')),
			'field_id'			=> $fields->f('field_id'),
			'type'				=> $fields->f('type'),
		);
		array_push($result['query'], $item);
	}

    $max_rows=$db->query("SELECT * FROM installation_fields WHERE sort='0' ORDER BY field_id")->records_count();
    $result['max_rows'] = $max_rows;

    json_out($result);
?>