<?php

	if($in['xget']){
    	$fname = 'get_'.$in['xget'];
    	$fname($in,false);
	}

	function get_siteFields($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('data'=>array());
		$fields=$db->query("SELECT * FROM site_fields ORDER BY sort_order");
		while($fields->next()){
			$item=array(
				'name'				=> stripslashes($fields->f('name')),
				'field_id'			=> $fields->f('field_id'),
				'type'				=> $fields->f('type'),
			);
			array_push($data['data'], $item);
		}
		return json_out($data, $showin,$exit);
	}

	function get_Convention($in,$showin=true,$exit=true){
		$db = new sqldb();
		$result = array();

		$account_number = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INSTALLATION_START' ");
		$account_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INSTALLATION_DIGIT_NR' ");
		$data['account_number']=$account_number;
		$data['account_digits']=$account_digits;
		$data['example']=$account_number.str_pad(1,$account_digits,"0",STR_PAD_LEFT);
		
		$result['convention'] = $data;
		return json_out($result, $showin,$exit);
	}

	$result = array(
		'siteFields'		=> get_siteFields($in,true,false),
	);
json_out($result);
?>