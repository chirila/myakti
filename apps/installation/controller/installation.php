<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class Installation_Service extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getInstall(){
		if($this->in['installation_id'] == 'tmp'){ //add
			$this->getAddInstall();
		}else{ # edit
			$this->getEditInstall();
		}
	}

	public function getAddInstall(){
		$in = $this->in;

		$output=array('templates'=>array());

		//$next_function = 'installation-installation-ifields-addInstallation';
		$next_function = 'installation-installation-installation-updateCustomerData';

		$free_field='';

		if($in['buyer_id'] ){
			$buyer_info = $this->db->query("SELECT customers.payment_term,customers.payment_term_type, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name,
					customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.invoice_note2, customers.internal_language,
					customers.attention_of_invoice,customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc, customers.name AS company_name, customers.vat_regime_id,customers.identity_id
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$in['buyer_id']."' ");
			$buyer_info->next();
			$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');

			$text = gm('Company Name').':';
			$c_email = $buyer_info->f('c_email');
					
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND site='1' ");
			$address_id	= $buyer_details->f('address_id');
		
			$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
			$name = stripslashes($name);
			if(!$in['delivery_address_id']){
				$in['delivery_address_id']=$address_id;
			}
			//$next_function = 'installation-installation-ifields-addInstallation';
		}
		if($in['delivery_address_id'] && $in['delivery_address_id']!=$address_id){
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND address_id='".$in['delivery_address_id']."' ");
			$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
			$name = stripslashes($name);
		}
		$i=0;
		$template_match=0;
		$templates=$this->db->query("SELECT * FROM installation_templates ORDER BY name ASC");
		while($templates->next()){
			$left_fields=array();
			$right_fields=array();
			$fields1=$this->db->query("SELECT installation_template_fields.*,installation_fields.name,installation_fields.type,installation_fields.sort FROM installation_template_fields
				LEFT JOIN installation_fields ON installation_template_fields.field_id=installation_fields.field_id 
				WHERE installation_template_fields.template_id='".$templates->f('template_id')."' AND installation_template_fields.field_position='1' ORDER BY installation_template_fields.sort ASC");
			while($fields1->next()){
				$drop_opts=array();
				if($fields1->f('type')=='1' && $fields1->f('sort')=='0'){
					$drop_opts=build_instal_field_dropdown($fields1->f('field_id'));
				}
				$temp1=array(
					'field_id'  		=> $fields1->f('field_id'),
					'name'				=> stripslashes($fields1->f('name')),
					'type'				=> $fields1->f('type'),
					'ptype'				=> $fields1->f('sort'),
					'no_fields'				=> $drop_opts,
				);
				array_push($left_fields, $temp1);
			}
			$fields2=$this->db->query("SELECT installation_template_fields.*,installation_fields.name,installation_fields.type,installation_fields.sort FROM installation_template_fields
				LEFT JOIN installation_fields ON installation_template_fields.field_id=installation_fields.field_id 
				WHERE installation_template_fields.template_id='".$templates->f('template_id')."' AND installation_template_fields.field_position='2' ORDER BY installation_template_fields.sort ASC");
			while($fields2->next()){
				$drop_opts=array();
				if($fields2->f('type')=='1' && $fields2->f('sort')=='0'){
					$drop_opts=build_instal_field_dropdown($fields2->f('field_id'));
				}
				$temp2=array(
					'field_id'  		=> $fields2->f('field_id'),
					'name'			=> stripslashes($fields2->f('name')),
					'type'			=> $fields2->f('type'),
					'ptype'			=> $fields2->f('sort'),
					'no_fields'			=> $drop_opts,
				);
				array_push($right_fields, $temp2);
			}
			if($in['template_id'] && $in['template_id']== $templates->f('template_id')){
				$template_match=$i;
			}
			$temp_template=array(
				'name'			=> stripslashes($templates->f('name')),
				'template_id'	=> $templates->f('template_id'),
				'left_fields'	=> $left_fields,
				'right_fields'	=> $right_fields
			);
			array_push($output['templates'], $temp_template);
			$i++;
		}
		
		$output['installation_id']=$in['installation_id'];
		//$output['add_customer']		= !$in['buyer_id'] ? true : false;
		$output['add_customer']			= false;
		$output['buyer_id']             = $in['buyer_id'];
		$output['customer_id']			= $in['buyer_id'];
		$output['buyer_name']           = $name;
		$output['field']				= $in['buyer_id'] ? 'customer_id' : 'contact_id';
		$output['free_field']			= $free_field;
		$output['free_field_txt']		= nl2br($free_field);
		$output['delivery_address_id']	= $in['delivery_address_id'];
		$output['country_dd']			= $in['country_id']? build_country_list($in['country_id']):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['main_country_id']		= $in['country_id'] ? $in['country_id'] : ACCOUNT_BILLING_COUNTRY_ID;
		$output['cc']=$this->get_cc($in);
		$output['main_comp_info']= $this->getCompanyInfo();
		$output['addresses']=$this->get_addresses($in);
		$output['template_id']=$in['template_id'];
		$output['name']=$in['name'];
		//$output['serial_number'] = $in['serial_number'] ? $in['serial_number'] : generate_installation_number();
		$output['template']=array();
		if($in['template_id']){
			$output['template']=$output['templates'][$template_match];
		}
		$output['do_request']=$next_function;
		if($in['buyer_id']){
			$category=$this->db->query("SELECT customers.cat_id FROM customers WHERE customer_id = '".$in['buyer_id']."'");
			$category->move_next();
			if (!$category->f('cat_id'))
			{
				$in['cat_id']	= 1;
			}
			else
			{
				$in['cat_id']	= $category->f('cat_id');
			}
			$output['cat_id']=$in['cat_id'];
			$output['articles_list']	= $this->get_articles_list($in);
			$in['only_service']		= true;
			$output['services_list']	= $this->get_articles_list($in);
		}

		$this->out = $output;
	}

	public function getEditInstall(){
		$in = $this->in;
		$output=array('templates'=>array());
		$next_function = 'installation-installation-installation-updateCustomerData';

		$templates=$this->db->query("SELECT * FROM installation_templates");
		while($templates->next()){
			$temp_template=array(
				'name'			=> stripslashes($templates->f('name')),
				'template_id'	=> $templates->f('template_id'),
			);
			array_push($output['templates'], $temp_template);
		}
		$installation_data=$this->db->query("SELECT * FROM installations WHERE id='".$in['installation_id']."' ");
		$output['template_id']=$installation_data->f('template_id');
		$output['name']=stripslashes($installation_data->f('name'));
		$in['buyer_id']=$installation_data->f('customer_id');
		$output['customer_id']=$in['buyer_id'];
		$output['buyer_id']=$in['buyer_id'];
		$output['buyer_name']=get_customer_name($in['buyer_id']);
		$output['cc']=$this->get_cc($in);
		$output['addresses']=$this->get_addresses($in);
		$output['delivery_address_id']=$installation_data->f('address_id');
		//$output['no_fields']=array();
		$output['template']=array('left_fields'=>array(),'right_fields'=>array());
		if($in['buyer_id']){
			$category=$this->db->query("SELECT customers.cat_id FROM customers WHERE customer_id = '".$in['buyer_id']."'");
			$category->move_next();
			if (!$category->f('cat_id'))
			{
				$in['cat_id']	= 1;
			}
			else
			{
				$in['cat_id']	= $category->f('cat_id');
			}
			$output['cat_id']=$in['cat_id'];
		}
		$left_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$in['installation_id']."' AND position='1' ORDER BY id ASC");
		while($left_fields->next()){
			/*if($left_fields->f('type')==1 && !$left_fields->f('p_type')){
				array_push($output['no_fields'], array('id'=>stripslashes($left_fields->f('name')),'name'=>stripslashes($left_fields->f('name'))));			
			}*/
			$drop_opts=array();
			if($left_fields->f('type')=='1' && $left_fields->f('p_type')=='0'){
				$drop_opts=build_instal_field_dropdown($left_fields->f('field_id'));
			}
			if($left_fields->f('p_type')=='10'){
				$in['article_id']=$left_fields->f('p_id');
				$output['articles_list']	= $this->get_articles_list($in);
			}else if($left_fields->f('p_type')=='11'){
				$in['article_id']=$left_fields->f('p_id');
				$in['only_service']		= true;
				$output['services_list']	= $this->get_articles_list($in);
			}
			$temp_left=array(
					'field_id'  		=> $left_fields->f('field_id'),
					'name'				=> stripslashes($left_fields->f('label_name')),
					'name_final'		=> $left_fields->f('type')==3 ? ((int)$left_fields->f('name'))*1000 : stripslashes($left_fields->f('name')),
					'type'				=> $left_fields->f('type'),
					'ptype'				=> $left_fields->f('p_type'),
					'no_fields'			=> $drop_opts,
				);
			array_push($output['template']['left_fields'], $temp_left);
		}
		$right_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$in['installation_id']."' AND position='2' ORDER BY id ASC");
		while($right_fields->next()){
			/*if($right_fields->f('type')==1 && !$right_fields->f('p_type')){
				array_push($output['no_fields'], array('id'=>stripslashes($right_fields->f('name')),'name'=>stripslashes($right_fields->f('name'))));
			}*/
			$drop_opts=array();
			if($right_fields->f('type')=='1' && $right_fields->f('p_type')=='0'){
				$drop_opts=build_instal_field_dropdown($right_fields->f('field_id'));
			}else if($right_fields->f('p_type')=='10'){
				$in['article_id']=$right_fields->f('p_id');
				$output['articles_list']	= $this->get_articles_list($in);
			}else if($right_fields->f('p_type')=='11'){
				$in['article_id']=$right_fields->f('p_id');
				$in['only_service']		= true;
				$output['services_list']	= $this->get_articles_list($in);
			}
			$temp_right=array(
					'field_id'  		=> $right_fields->f('field_id'),
					'name'				=> stripslashes($right_fields->f('label_name')),
					'name_final'		=> $right_fields->f('type')==3 ? ((int)$right_fields->f('name'))*1000 : stripslashes($right_fields->f('name')),
					'type'				=> $right_fields->f('type'),
					'ptype'				=> $right_fields->f('p_type'),
					'no_fields'			=> $drop_opts,
				);
			array_push($output['template']['right_fields'], $temp_right);
		}

		$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND address_id='".$installation_data->f('address_id')."' ");
		$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
			$name = stripslashes($name);
		
		$output['installation_id']=$in['installation_id'];
		$output['main_comp_info']= $this->getCompanyInfo();
		$output['free_field']= $installation_data->f('free_field') ? $installation_data->f('free_field') : $free_field;
		$output['free_field_txt']= trim($installation_data->f('free_field')) ? nl2br($installation_data->f('free_field')) : nl2br($free_field );
		$output['country_dd']= $in['country_id']? build_country_list($in['country_id']):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['main_country_id']= $in['country_id'] ? $in['country_id'] : ACCOUNT_BILLING_COUNTRY_ID;
		$output['field']= $installation_data->f('customer_id') ? 'customer_id' : 'contact_id';
		$output['serial_number'] = $installation_data->f('serial_number') ? $installation_data->f('serial_number') : generate_installation_number();
		$output['do_request']=$next_function;
		$this->out = $output;
	}

	public function get_cc($in)
	{
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		if($in['internal']=='true'){
			$filter=" is_admin>0 ";
		}
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}
		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		if($in['internal'] == 'true'){
			$cust = $this->db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name,our_reference,currency_id,internal_language
			FROM customers
			WHERE $filter			
			ORDER BY name
			LIMIT 50")->getAll();
		}else{
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 50")->getAll();
		}
		
		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				//"id"					=> $value['cust_id'].'-'.$value['contact_id'],
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				/*"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_addresses(&$in)
	{

		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		$filter .=" AND site='1' ";

		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		"symbol"				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;
	}

	private function getCompanyInfo(){
		$array = array(			
				'name'		=> ACCOUNT_COMPANY,
				'address'		=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
				'zip'			=> ACCOUNT_DELIVERY_ZIP,
				'city'		=> ACCOUNT_DELIVERY_CITY,
				'country'		=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
				'phone'		=> ACCOUNT_PHONE,
				'fax'			=> ACCOUNT_FAX,
				'email'		=> ACCOUNT_EMAIL,
				'url'			=> ACCOUNT_URL,
				'logo'		=> '../'.ACCOUNT_LOGO_QUOTE,			
		);		
		return $array;
	}

	public function get_articles_list(&$in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id ';
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id';

			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference';
		}

		$filter.=" 1=1 ";
		if($in['only_service']){
			$filter.=" AND pim_articles.is_service='1' ";
		}else{
			$filter.=" AND pim_articles.is_service='0' ";
		}

		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id']){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
				if($vat_regime==2){
					$vat=0;
				}
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	'name'						=> htmlspecialchars_decode($article->f('internal_name')),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'ALLOW_STOCK'		=> ALLOW_STOCK==1 ? true : false,
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)), 
				'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
				'supplier_reference'	   => $article->f('supplier_reference'),
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		//array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));

		return $articles;
	}

}
	$install = new Installation_Service($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $install->output($install->$fname($in));
	}

	$install->getInstall();
	$install->output();

?>