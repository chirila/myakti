<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

	if($in['reset_list']){
 		if(isset($_SESSION['tmp_add_to_pdf_inst'])){
 			unset($_SESSION['tmp_add_to_pdf_inst']);
 		}
 		if(isset($_SESSION['add_to_pdf_inst'])){
 			unset($_SESSION['add_to_pdf_inst']);
 		}
 	}

	global $database_config;
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$dbu = new sqldb($db_config);

	$l_r = ROW_PER_PAGE;
	$arguments='';

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$filter = 'WHERE 1=1 ';
	$order_by_array=array('name', 'serial_number', 'customer_name', 'address', 'city', 'country_id');
	$order_by = " ORDER BY name ASC ";

	if(!empty($in['search'])){
		$filter.=" AND (installations.serial_number LIKE '%".$in['search']."%' OR installations.name LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%') ";
		$arguments.="&search=".$in['search'];
	}
	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	if(!empty($in['order_by'])){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == 'true' || $in['desc'] == '1'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		}
	}

	//All the users should see the instalations

	// $perm_admin = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
	// $perm_manager = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
	// switch (true) {
	// 	case $_SESSION['group'] == 'admin':
	// 	case $perm_admin:
	// 	case $perm_manager:
	// 		break;
	// 	default:
	// 		$filter.=" AND installations.user_id ='".$_SESSION['u_id']."' ";
	// 		break;
	// }

	$nav = array();
	$max_rows_data=$db->query("SELECT count(*) AS total_records, installations.id 
		FROM installations 
		LEFT JOIN customers ON installations.customer_id=customers.customer_id
		".$filter." GROUP BY installations.id");
	$max_rows=$max_rows_data->records_count();
	if(!$_SESSION['tmp_add_to_pdf_inst'] || ($_SESSION['tmp_add_to_pdf_inst'] && empty($_SESSION['tmp_add_to_pdf_inst']))){
	 	while($max_rows_data->next()){
	 		$_SESSION['tmp_add_to_pdf_inst'][$max_rows_data->f('id')]=1;
	 		array_push($nav, (object)['installation_id'=> $max_rows_data->f('id') ]);
	 	}
	}else{
		while($max_rows_data->next()){
	 		array_push($nav, (object)['installation_id'=> $max_rows_data->f('id') ]);
	 	}
	}
	$all_pages_selected=false;
	$minimum_selected=false;
	if($_SESSION['add_to_pdf_inst']){
	 	if($max_rows>0 && count($_SESSION['add_to_pdf_inst']) == $max_rows){
	 		$all_pages_selected=true;
	 	}else if(count($_SESSION['add_to_pdf_inst'])){
	 		$minimum_selected=true;
	 	}
	}

	$installations=$db->query("SELECT installations.*, customers.name as customer_name, customer_addresses.address, customer_addresses.city,customer_addresses.country_id
		FROM installations 
		LEFT JOIN customers ON installations.customer_id=customers.customer_id
		LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id

		".$filter.$order_by.$filter_limit);
	//$max_rows=$installations->records_count();
	$result = array('query'=>array(),'max_rows'=>$max_rows,'all_pages_selected'=>$all_pages_selected,'minimum_selected'=>$minimum_selected, 'nav'=>$nav);
	//$installations->move_to($offset*$l_r);
	//$j=0;
	while($installations->next()){
		$item=array(
			'id'				=> $installations->f('id'),
			'installation_id'	=> $installations->f('id'),
			'name'				=> stripslashes($installations->f('name')),
			'customer'			=> stripslashes($installations->f('customer_name')),
			'address'			=> $installations->f('address'),
			'city'				=> $installations->f('city'),
			'country'			=> $installations->f('country_id') ? get_country_name($installations->f('country_id')) : '',
			'serial_number'		=> $installations->f('serial_number'),
			'delete_link'		=> array('do'=>'installation-installations-ifields-deleteInstallation','installation_id'=>$installations->f('id')),
			'check_add_to_product'	=> $_SESSION['add_to_pdf_inst'][$installations->f('id')] == 1 ? true : false
		);
		array_push($result['query'], $item);
		//$j++;
	}

	$result['lr']=$l_r;
	$result['export_args'] = ltrim($arguments,'&');
json_out($result);
?>