<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps;
global $p_access;
perm::controller('all', 'admin',in_array($apps['installation'], $p_access));
perm::controller('all', 'user',in_array($apps['installation'], $p_access));
if(isset($_SESSION['team_int']) && $_SESSION['team_int']){
    perm::controller(array('ninstallation'), 'user', true);
}
if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::controller('all', 'admin',false);
	perm::controller('all', 'user',false);
}
//perm::controller('all', 'all');