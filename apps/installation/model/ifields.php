<?php

class ifields {

	function __construct() {
		$this->db = new sqldb();
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($database_users);
	}

	function addField(&$in){
		$this->db->query("INSERT INTO installation_fields SET name='".gm('Field')."',type='2' ");
		msg::success(gm("Field added"),"success");
		return true;
	}

	function deleteField(&$in){
		$this->db->query("DELETE FROM installation_fields WHERE field_id='".$in['field_id']."' ");
		msg::success(gm("Field deleted"),"success");
		return true;
	}

	function editNameField(&$in){
		$this->db->query("UPDATE installation_fields SET name='".addslashes($in['name'])."' WHERE field_id='".$in['field_id']."' ");
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}

	function editDataField(&$in){
		$this->db->query("UPDATE installation_fields SET type='".$in['value']."' WHERE field_id='".$in['field_id']."' ");
		msg::success(gm("Changes saved"),"success");
		return true;
	}

	function addTemplate(&$in){
		if(!$this->addTemplate_validate($in)){
			json_out($in);
			return false;
		}
		$in['fields1']=json_decode(stripslashes($in['fields1']));
		$in['fields2']=json_decode(stripslashes($in['fields2']));
		if(!empty($_FILES)){
			$tempFile = $_FILES['files']['tmp_name'];
			$ext = pathinfo($_FILES['files']['name'],PATHINFO_EXTENSION);
			$targetPath = INSTALLPATH.'upload/'.DATABASE_NAME.'/installations';
			$targetFile = rtrim($targetPath,'/') . '/tmp_file_'.time().'.'.$ext;
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			move_uploaded_file($tempFile,$targetFile);
		}else{
			$targetFile='';
		}
		$in['template_id']=$this->db->insert("INSERT INTO installation_templates SET name='".addslashes($in['name'])."',`icon_url`='".$targetFile."' ");
		$i=0;
		foreach($in['fields1'] as $key=>$value){
			$this->db->query("INSERT INTO installation_template_fields SET
								template_id	='".$in['template_id']."',
								field_id='".$value->field_id."',
								field_position	='1',
								sort 		= '".$i."' ");
			$i++;
		}
		$j=0;
		foreach($in['fields2'] as $key=>$value){
			$this->db->query("INSERT INTO installation_template_fields SET
								template_id	='".$in['template_id']."',
								field_id='".$value->field_id."',
								field_position	='2',
								sort 		= '".$j."' ");
			$j++;
		}
		msg::success(gm("Template created successfully"),"success");
		return true;
	}

	function addTemplate_validate(&$in){
		 $v=new validation($in);
		 $v->f('name', 'Name', 'required');
		 return $v->run();
	}

	function updateTemplate(&$in){
		if(!$this->addTemplate_validate($in)){
			json_out($in);
			return false;
		}
		$in['fields1']=json_decode(stripslashes($in['fields1']));
		$in['fields2']=json_decode(stripslashes($in['fields2']));
		if(!empty($_FILES)){
			$tempFile = $_FILES['files']['tmp_name'];
			$ext = pathinfo($_FILES['files']['name'],PATHINFO_EXTENSION);
			$targetPath = INSTALLPATH.'upload/'.DATABASE_NAME.'/installations';
			$targetFile = rtrim($targetPath,'/') . '/tmp_file_'.time().'.'.$ext;
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			move_uploaded_file($tempFile,$targetFile);
		}else{
			$targetFile='';
		}
		$old_icon=$this->db->field("SELECT icon_url FROM installation_templates WHERE template_id='".$in['template_id']."'");
		if($old_icon){
			unlink($old_icon);
		}
		$this->db->query("UPDATE installation_templates SET name='".addslashes($in['name'])."',`icon_url`='".$targetFile."' WHERE template_id='".$in['template_id']."' ");
		$this->db->query("DELETE FROM installation_template_fields WHERE template_id='".$in['template_id']."' ");
		$i=0;
		foreach($in['fields1'] as $key=>$value){
			$this->db->query("INSERT INTO installation_template_fields SET
								template_id	='".$in['template_id']."',
								field_id='".$value->field_id."',
								field_position	='1',
								sort 		= '".$i."' ");
			$i++;
		}
		$j=0;
		foreach($in['fields2'] as $key=>$value){
			$this->db->query("INSERT INTO installation_template_fields SET
								template_id	='".$in['template_id']."',
								field_id='".$value->field_id."',
								field_position	='2',
								sort 		= '".$j."' ");
			$j++;
		}
		msg::success(gm("Template updated successfully"),"success");
		return true;
	}

	function deleteTemplate(&$in){
		$this->db->query("DELETE FROM installation_templates WHERE template_id='".$in['template_id']."' ");
		$this->db->query("DELETE FROM installation_template_fields WHERE template_id='".$in['template_id']."' ");
		msg::success(gm("Template deleted successfully"),"success");
		return true;
	}

	function addInstallation_validate(&$in){
		$v=new validation($in);
		$v->f('name', 'Name', 'required');
		$v->f('customer_id', 'Customer', 'required');
		$v->f('delivery_address_id', 'delivery_address_id', 'required');
		//$v->f('template_id', 'Template', 'required');
		if(ark::$method=='addInstallation'){
			//$v->field('serial_number', 'ID', 'unique[installations.id]', gm('Unique serial number required'));
		}else{
			//$v->field('serial_number', gm('serial_number'), 'required');
			$v->field('serial_number', 'ID', "required:unique[installations.serial_number.( id!='".$in['installation_id']."')]", gm('Unique serial number required'));
		}
		$is_ok=$v->run();
		if($is_ok){
			/*if(!$in['delivery_address_id']){
				msg::error(gm('Site address needed'),'error');
				$is_ok=false;
			}*/
			if(!$in['template_id']){
				msg::error(gm('Template needed'),'error');
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function addInstallation(&$in){
		if(!$this->addInstallation_validate($in)){
			json_out($in);
			return false;
		}
		$in['serial_number'] = generate_installation_number();
		$in['installation_id']=$this->db->insert("INSERT INTO installations SET
											name 	='".addslashes($in['name'])."',
											template_id='".$in['template_id']."', 
											date='".time()."',
											user_id='".$_SESSION['u_id']."',
											customer_id='".$in['customer_id']."',
											address_id='".$in['delivery_address_id']."',
											free_field='".$in['free_field']."',
											serial_number='".$in['serial_number']."' ");
		foreach ($in['template']['left_fields'] as $key => $value) {
			$this->db->query("INSERT INTO installations_data SET
								installation_id    ='".$in['installation_id']."',
								field_id 		   ='".$value['field_id']."',
								label_name 		   ='".addslashes($value['name'])."',
								name 			   ='".($value['type']=='3' ? strtotime($value['name_final']) : addslashes($value['name_final']))."',
								type 			   ='".$value['type']."',
								position 		   ='1',
								p_type  		   ='".$value['ptype']."',
								p_id 			   ='".$value['name_final']."' ");
		}
		foreach ($in['template']['right_fields'] as $key => $value) {
			$this->db->query("INSERT INTO installations_data SET
								installation_id    ='".$in['installation_id']."',
								field_id 		   ='".$value['field_id']."',
								label_name 		   ='".addslashes($value['name'])."',
								name 			   ='".($value['type']=='3' ? strtotime($value['name_final']) : addslashes($value['name_final']))."',
								type 			   ='".$value['type']."',
								position 		   ='2',
								p_type  		   ='".$value['ptype']."',
								p_id 			   ='".$value['name_final']."' ");
		}
		$tracking_data=array(
		            'target_id'         => $in['installation_id'],
		            'target_type'       => '12',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array()
		          );
		addTracking($tracking_data);
		insert_message_log('installation','{l}Installation was created by{endl} '.get_user_name($_SESSION['u_id']),"installation_id",$in['installation_id'],false,$_SESSION['u_id']);
		msg::success(gm("Installation added successfully"),"success");
		return true;
	}

	function updateInstallation(&$in){
		if(!$this->addInstallation_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE installations SET
											name 	='".addslashes($in['name'])."',
											template_id='".$in['template_id']."', 
											date='".time()."',
											user_id='".$_SESSION['u_id']."',
											customer_id='".$in['customer_id']."',
											address_id='".$in['delivery_address_id']."',
											free_field='".$in['free_field']."',
											serial_number='".$in['serial_number']."' 
											WHERE id='".$in['installation_id']."' ");
		$this->db->query("DELETE FROM installations_data WHERE installation_id='".$in['installation_id']."' ");
		foreach ($in['template']['left_fields'] as $key => $value) {
			$this->db->query("INSERT INTO installations_data SET
								installation_id    ='".$in['installation_id']."',
								field_id 		   ='".$value['field_id']."',
								label_name 		   ='".addslashes($value['name'])."',
								name 			   ='".($value['type']=='3' ? strtotime($value['name_final']) : addslashes($value['name_final']))."',
								type 			   ='".$value['type']."',
								position 		   ='1',
								p_type  		   ='".$value['ptype']."',
								p_id 			   ='".$value['name_final']."' ");
		}
		foreach ($in['template']['right_fields'] as $key => $value) {
			$this->db->query("INSERT INTO installations_data SET
								installation_id    ='".$in['installation_id']."',
								field_id 		   ='".$value['field_id']."',
								label_name 		   ='".addslashes($value['name'])."',
								name 			   ='".($value['type']=='3' ? strtotime($value['name_final']) : addslashes($value['name_final']))."',
								type 			   ='".$value['type']."',
								position 		   ='2',
								p_type  		   ='".$value['ptype']."',
								p_id 			   ='".$value['name_final']."' ");
		}
		$trace_id=$this->db->field("SELECT trace_id FROM installations WHERE id='".$in['installation_id']."' ");
		if($trace_id){
			$this->db->query("UPDATE tracking SET origin_buyer_id='".$in['customer_id']."',target_buyer_id='".$in['customer_id']."' WHERE trace_id='".$trace_id."' ");
		}	
		insert_message_log('installation','{l}Installation has been updated by{endl} '.get_user_name($_SESSION['u_id']),"installation_id",$in['installation_id'],false,$_SESSION['u_id']);	
		msg::success(gm("Installation updated successfully"),"success");
		return true;
	}

	function deleteInstallation(&$in){
		$tracking_trace=$this->db->field("SELECT trace_id FROM installations WHERE id='".$in['installation_id']."' ");
		$this->db->query("DELETE FROM installations WHERE id='".$in['installation_id']."' ");
		$this->db->query("DELETE FROM installations_data WHERE installation_id='".$in['installation_id']."' ");
		if($tracking_trace){
		      $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
		      $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
		}
		msg::success(gm("Installation deleted successfully"),"success");
		return true;
	}

	function addCustomField(&$in){
		$last_sort=$this->db->field("SELECT sort_order FROM site_fields ORDER BY sort_order DESC LIMIT 1");
		if(!$last_sort){
			$last_sort=0;
		}
		$this->db->query("INSERT INTO site_fields SET name='".gm('Field')."',type='2',sort_order='".($last_sort+1)."' ");
		msg::success(gm("Field added"),"success");
		return true;
	}

	function editNameCustomField(&$in){
		$this->db->query("UPDATE site_fields SET name='".addslashes($in['name'])."' WHERE field_id='".$in['field_id']."' ");
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}

	function editDataCustomField(&$in){
		$this->db->query("UPDATE site_fields SET type='".$in['value']."' WHERE field_id='".$in['field_id']."' ");
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}

	function deleteCustomField(&$in){
		$this->db->query("DELETE FROM site_fields WHERE field_id='".$in['field_id']."' ");
		msg::success(gm("Field deleted"),"success");
		return true;
	}

	function updateSortOrder(&$in){
		$i=1;
		foreach($in['fields'] as $key=>$value){
			$this->db->query("UPDATE site_fields SET sort_order='".$i."'WHERE field_id ='".$value['field_id']."'");
			$i++;
		}
		msg::success(gm("Changes done"),"success");
		json_out($in);
	}

}