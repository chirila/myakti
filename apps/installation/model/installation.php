<?php

class installation {

	function __construct() {
		$this->db = new sqldb();
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($database_users);
	}

	/**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddC(&$in){
	    if(!$this->tryAddCValid($in)){ 
	      json_out($in);
	      return false; 
	    }
	    $in['installation_id'] = $in['item_id'];
	    //$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
	    $name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	    //Set account default vat on customer creation
	    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	    if(empty($vat_regime_id)){
	      $vat_regime_id = 0;
	    }

	    $c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			/*foreach ($in['c_type'] as $key) {
				if($key){
					$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
					$c_type_name .= $type.',';
				}
			}
			$c_types = implode(',', $in['c_type']);
			$c_type_name = rtrim($c_type_name,',');*/
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']==''){
			$in['user_id']=$_SESSION['u_id'];
		}


		if($in['user_id']){
			/*foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);*/
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				//$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}
		$vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
	        $vat_default=str_replace(',', '.', $vat);
		$selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");

	    if($in['add_customer']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
	        if(SET_DEF_PRICE_CAT == 1){
				$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
			}
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".$in['name']."',
	                                            our_reference = '".$account_reference_number."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            vat_regime_id     = '".$vat_regime_id."',
	                                            city_name = '".$in['city']."',
	                                            c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
	                                            user_id = '".$acc_manager_ids."',
                                            	acc_manager_name = '".addslashes($in['acc_manager'])."',
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            creation_date = '".time()."',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."',
	                                            commercial_name 		= '".$in['commercial_name']."',
												legal_type				= '".$in['legal_type']."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												sector					= '".$in['sector']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$c_type_name."',
												vat_id 					= '".$selected_vat."',
												invoice_email_type		= '1',
												sales_rep				= '".$in['sales_rep_id']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												stripe_cust_id			= '".$in['stripe_cust_id']."',
												cat_id					= '".$in['cat_id']."'");
	      
	        $this->db->query("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1',
	                        site='1' ");
	        $in['customer_name'] = $in['name'];

	        if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
					if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
						$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
					}
				}
			}

	      // include_once('../apps/company/admin/model/customer.php');
	        $in['value']=$in['btw_nr'];
	      // $comp=new customer();       
	      // $this->check_vies_vat_number($in);
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");*/
	            $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                            name    = :name,
	                            value   = :value ",
	                        ['user_id' => $_SESSION['u_id'],
	                         'name'    => 'company-customers_show_info',
	                         'value'   => '1']
	                    );
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
	          $this->db->query("UPDATE installations SET 
	            customer_id='".$in['buyer_id']."' 
	            WHERE id='".$in['item_id']."' ");         
	        }
	        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
	        msg::success (gm('Success'),'success');
	        return true;
	    }
	    if($in['add_individual']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
	        if(SET_DEF_PRICE_CAT == 1){
				$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
			}
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".$in['lastname']."',
	                                            firstname = '".$in['firstname']."',
	                                            our_reference = '".$account_reference_number."',
	                                            user_id = '".$acc_manager_ids."',
                                            	acc_manager_name = '".addslashes($in['acc_manager'])."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            vat_regime_id     = '".$vat_regime_id."',
	                                            city_name = '".$in['city']."',
	                                            c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
	                                            type = 1,
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            creation_date = '".time()."',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."',
	                                            commercial_name 		= '".$in['commercial_name']."',
												legal_type				= '".$in['legal_type']."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												sector					= '".$in['sector']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$c_type_name."',
												vat_id 					= '".$selected_vat."',
												invoice_email_type		= '1',
												sales_rep				= '".$in['sales_rep_id']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												stripe_cust_id			= '".$in['stripe_cust_id']."',
												cat_id					= '".$in['cat_id']."'");
	      
	        $in['main_address_id'] = $this->db->insert("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1',
	                        site='1' ");
	        $in['customer_name'] = $in['name'];

	        if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
					if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
						$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
					}
				}
			}

	      // include_once('../apps/company/admin/model/customer.php');
	        $in['value']=$in['btw_nr'];
	      // $comp=new customer();       
	      // $this->check_vies_vat_number($in);
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");*/
	            $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                            name    = :name,
	                            value   = :value ",
	                        ['user_id' => $_SESSION['u_id'],
	                         'name'    => 'company-customers_show_info',
	                         'value'   => '1']
	                    );
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
	          $this->db->query("UPDATE installations SET 
	            customer_id='".$in['buyer_id']."' 
	            WHERE id='".$in['item_id']."' ");         
	        }
	        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
	        msg::success (gm('Success'),'success');
	        return true;
	    }
	    if($in['add_contact']){
	      $customer_name='';
	      if($in['buyer_id']){
	        $customer_name = $this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' ");
	      }
	        $in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
	                                              firstname='".$in['firstname']."',
	                                              lastname='".$in['lastname']."',
	                                              `create`  = '".time()."',
	                                              customer_id = '".$in['buyer_id']."',
	                                              company_name= '".$customer_name."',
	                                              email='".$in['email']."',
	                                              cell='".$in['cell']."' ");
	        $this->db->query("INSERT INTO customer_contactsIds SET customer_id='".$in['buyer_id']."', contact_id='".$in['contact_id']."' ");
	        $in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
	        if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
                $vars=array();
                if($in['buyer_id']){
                    $vars['customer_id']=$in['buyer_id'];
                    $vars['customer_name']=$customer_name;
                }          
                $vars['contact_id']=$in['contact_id'];
                $vars['firstname']=$in['firstname'];
                $vars['lastname']=$in['lastname'];
                $vars['table']='customer_contacts';
                $vars['email']=$in['email'];
                $vars['op']='add';
                synctoZendesk($vars);
              }
	        if($in['country_id']){
	          $this->db->query("INSERT INTO customer_contact_address SET
	                          address='".$in['address']."',
	                          zip='".$in['zip']."',
	                          city='".$in['city']."',
	                          country_id='".$in['country_id']."',
	                          contact_id='".$in['contact_id']."',
	                          is_primary='1',
	                          delivery='1' ");
	        }
	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                    AND name    = 'company-contacts_show_info'  ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                    AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);	                                    
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                                  name    = 'company-contacts_show_info',
	                                  value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                                  name    = :name,
	                                  value   = :value ",
	                                ['user_id' => $_SESSION['u_id'],
	                                 'name'    => 'company-contacts_show_info',
	                                 'value'   => '1']
	                           	);	                                  
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                      AND name    = 'company-contacts_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                      AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);	                                      
	        }
	        $count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
	        if($count == 1){
	          doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $this->db->query("UPDATE installations SET contact_id='".$in['contact_id']."' WHERE id='".$in['item_id']."' ");  
	        }
	        insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
	        msg::success (gm('Success'),'success');
	    }
	    return true;
	  }

	  /**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddCValid(&$in)
	  {
	    if($in['add_customer']){
	        $v = new validation($in);
	      $v->field('name', 'name', 'required:unique[customers.name]');
	      $v->field('country_id', 'country_id', 'required');
	      return $v->run();  
	    }
	    if($in['add_contact']){
	      $v = new validation($in);
	      $v->field('firstname', 'firstname', 'required');
	      $v->field('lastname', 'lastname', 'required');
	      // $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
	      $v->field('country_id', 'country_id', 'required');
	      return $v->run();  
	    }
	    return true;
	  }

	function check_vies_vat_number(&$in){
	    $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

	    if(!$in['value'] || $in['value']==''){      
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    $value=trim($in['value']," ");
	    $value=str_replace(" ","",$value);
	    $value=str_replace(".","",$value);
	    $value=strtoupper($value);

	    $vat_numeric=is_numeric(substr($value,0,2));

	    /*if($vat_numeric || substr($value,0,2)=="BE"){
	      $trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
	      $trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
	      $trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

	      if(!$trends_access_token || $trends_expiration<time()){
	        $ch = curl_init();
	        $headers=array(
	          "Content-Type: x-www-form-urlencoded",
	          "Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
	          );
	        
	        $trends_data="grant_type=password&username=akti_api&password=akti_api";

	          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	          curl_setopt($ch, CURLOPT_POST, true);
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
	          curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

	          $put = json_decode(curl_exec($ch));
	        $info = curl_getinfo($ch);

	        if($info['http_code']==400){
	          if(ark::$method == 'check_vies_vat_number'){
	            msg::error ( $put->error,'error');
	            json_out($in);
	          }
	          return false;
	        }else{
	          if(!$trends_access_token){
	            $this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
	            $this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
	            $this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
	          }else{
	            $this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
	            $this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
	            $this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
	          }

	          $ch = curl_init();
	          $headers=array(
	            "Authorization: Bearer ".$put->access_token
	            );
	          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	            if($vat_numeric){
	                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
	            }else{
	                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
	            }
	            $put = json_decode(curl_exec($ch));
	          $info = curl_getinfo($ch);

	            if($info['http_code']==400 || $info['http_code']==429){
	              if(ark::$method == 'check_vies_vat_number'){
	              msg::error ( $put->error,'error');
	              json_out($in);
	            }
	            return false;
	          }else if($info['http_code']==404){
	            if($vat_numeric){
	              if(ark::$method == 'check_vies_vat_number'){
	                msg::error ( gm("Not a valid vat number"),'error');
	                json_out($in);
	              }
	              return false;
	            }
	          }else{
	            if($in['customer_id']){
	              $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	              if($country_id != 26){
	                $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
	                $in['remove_v']=1;
	              }else if($country_id == 26){
	                $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
	                $in['add_v']=1;
	              }
	            }
	            $in['comp_name']=$put->officialName;
	            $in['comp_address']=$put->street.' '.$put->houseNumber;
	            $in['comp_zip']=$put->postalCode;
	            $in['comp_city']=$put->city;
	            $in['comp_country']='26';
	            $in['trends_ok']=true;
	            $in['trends_lang']=$_SESSION['l'];
	            $in['full_details']=$put;
	            if(ark::$method == 'check_vies_vat_number'){
	              msg::success(gm('Success'),'success');
	              json_out($in);
	            }
	            return false;
	          }
	        }
	      }else{
	        $ch = curl_init();
	        $headers=array(
	          "Authorization: Bearer ".$trends_access_token
	          );
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	          if($vat_numeric){
	              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
	          }else{
	              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
	          }
	          $put = json_decode(curl_exec($ch));
	        $info = curl_getinfo($ch);

	          if($info['http_code']==400 || $info['http_code']==429){
	            if(ark::$method == 'check_vies_vat_number'){
	            msg::error ( $put->error,'error');
	            json_out($in);
	          }
	          return false;
	        }else if($info['http_code']==404){
	          if($vat_numeric){
	            if(ark::$method == 'check_vies_vat_number'){
	              msg::error (gm("Not a valid vat number"),'error');
	              json_out($in);
	            }
	            return false;
	          }
	        }else{
	          if($in['customer_id']){
	            $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	            if($country_id != 26){
	              $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
	              $in['remove_v']=1;
	            }else if($country_id == 26){
	              $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
	              $in['add_v']=1;
	            }
	          }
	          $in['comp_name']=$put->officialName;
	          $in['comp_address']=$put->street.' '.$put->houseNumber;
	          $in['comp_zip']=$put->postalCode;
	          $in['comp_city']=$put->city;
	          $in['comp_country']='26';
	          $in['trends_ok']=true;
	          $in['trends_lang']=$_SESSION['l'];
	          $in['full_details']=$put;
	          if(ark::$method == 'check_vies_vat_number'){
	            msg::success(gm('Success'),'success');
	            json_out($in);
	          }
	          return false;
	        }
	      }
	    }*/


	    if(!in_array(substr($value,0,2), $eu_countries)){
	      $value='BE'.$value;
	    }
	    if(in_array(substr($value,0,2), $eu_countries)){
	      $search   = array(" ", ".");
	      $vat = str_replace($search, "", $value);
	      $_GET['a'] = substr($vat,0,2);
	      $_GET['b'] = substr($vat,2);
	      $_GET['c'] = '1';
	      $dd = include('../valid_vat.php');
	    }else{
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    if(isset($response) && $response == 'invalid'){
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }else if(isset($response) && $response == 'error'){
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }else if(isset($response) && $response == 'valid'){
	      $full_address=explode("\n",$result->address);
	      switch($result->countryCode){
	        case "RO":
	          $in['comp_address']=$full_address[1];
	          $in['comp_city']=$full_address[0];
	          $in['comp_zip']=" ";
	          break;
	        case "NL":
	          $zip=explode(" ",$full_address[2],2);
	          $in['comp_address']=$full_address[1];
	          $in['comp_zip']=$zip[0];
	          $in['comp_city']=$zip[1];
	          break;
	        default:
	          $zip=explode(" ",$full_address[1],2);
	          $in['comp_address']=$full_address[0];
	          $in['comp_zip']=$zip[0];
	          $in['comp_city']=$zip[1];
	          break;
	      }

	      $in['comp_name']=$result->name;

	      $in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
	      $in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
	      $in['full_details']=$result;
	      $in['vies_ok']=1;
	      if($in['customer_id']){
	        $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	        if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
	      }
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::success ( gm('VAT Number is valid'),'success');
	        json_out($in);
	      }
	      return true;
	    }else{
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    if(ark::$method == 'check_vies_vat_number'){
	      json_out($in);
	    }
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
	    if(!$this->CanEdit($in)){
	      json_out($in);
	      return false;
	    }
	    if(!$this->tryAddAddressValidate($in)){
	      json_out($in);
	      return false;
	    }
	    $country_n = get_country_name($in['country_id']);
	    if($in['field']=='customer_id'){
	      Sync::start(ark::$model.'-'.ark::$method);

	      	$address_id = $this->db->insert("INSERT INTO customer_addresses SET
	                                        customer_id     = '".$in['customer_id']."',
	                                        country_id      = '".$in['country_id']."',
	                                        state_id      = '".$in['state_id']."',
	                                        city        = '".$in['city']."',
	                                        zip         = '".$in['zip']."',
	                                        address       = '".$in['address']."',
	                                        billing       = '".$in['billing']."',
	                                        is_primary      = '".$in['primary']."',
	                                        delivery      = '".$in['delivery']."',
	                                        site             ='".$in['site']."'");	
	      
	      if($in['billing']){
	        $this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
	      }

	      Sync::end($address_id);

	      if($in['primary']){
	        if($in['address'])
	        {
	          if(!$in['zip'] && $in['city'])
	          {
	            $address = $in['address'].', '.$in['city'].', '.$country_n;
	          }elseif(!$in['city'] && $in['zip'])
	          {
	            $address = $in['address'].', '.$in['zip'].', '.$country_n;
	          }elseif(!$in['zip'] && !$in['city'])
	          {
	            $address = $in['address'].', '.$country_n;
	          }else
	          {
	            $address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
	          }
	          $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
	          $output= json_decode($geocode);
	          $lat = $output->results[0]->geometry->location->lat;
	          $long = $output->results[0]->geometry->location->lng;
	        }
	        $this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
	        $this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
	        $this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
	      }
	    }else{      
	      $address_id = $this->db->insert("INSERT INTO customer_contact_address SET
	                                        contact_id      = '".$in['contact_id']."',
	                                        country_id      = '".$in['country_id']."',                                        city        = '".$in['city']."',
	                                        zip         = '".$in['zip']."',
	                                        address       = '".$in['address']."',
	                                        is_primary      = '".$in['primary']."',
	                                        delivery      = '".$in['delivery']."',
	                                        site='".$in['site']."'");
	    }
	    msg::success ( gm("Changes have been saved."),'success');
	    insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
	    $in['address_id'] = $address_id;
	    $in['country'] = $country_n;
	    $in['installation_id'] = $in['item_id'];
	    if($in['installation_id'] && is_numeric($in['installation_id'])){
	      $delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	      $this->db->query("UPDATE installations SET 
	          free_field='".$delivery_address."' WHERE id='".$in['installation_id']."'  "); 
	    }elseif ($in['installation_id'] == 'tmp') {
	        $in['customer_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	    	  $in['delivery_address_id']=$address_id;
	    }
	    $in['buyer_id'] = $in['customer_id'];
	    // $in['pagl'] = $this->pag;
	    
	    // json_out($in);
	    return true;

	}

	function CanEdit(&$in){
	    if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
	      return true;
	    }
	    $c_id = $in['customer_id'];
	    if(!$in['customer_id'] && $in['contact_id']) {
	      $c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	    }
	    if($c_id){
	      if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
	        $u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
	        if($u){
	          $u = explode(',', $u);
	          if(in_array($_SESSION['u_id'], $u)){
	            return true;
	          }
	          else{
	            msg::$warning = gm("You don't have enought privileges");
	            return false;
	          }
	        }else{
	          msg::$warning = gm("You don't have enought privileges");
	          return false;
	        }
	      }
	    }
	    return true;
	}

	/**
	   * undocumented function
	   *
	   * @return void
	   * @author PM
	   **/
	function tryAddAddressValidate(&$in)
	  {
	    $v = new validation($in);
	    if($in['customer_id']){
	      $v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
	    }else if($in['contact_id']){
	      $v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
	    }else{
	      msg::error(gm('Invalid ID'),'error');
	      return false;
	    }
	    $v->field('country_id', 'Country', 'required:exist[country.country_id]');

	    return $v->run();
	}

	function updateCustomerData(&$in)
	{
	    if($in['field'] == 'contact_id'){
	      $in['buyer_id'] ='';
	    }
	    $sql = "UPDATE installations SET ";
	    if($in['buyer_id']){
	      $buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, 
	                  customers.internal_language, customers.line_discount, customers.currency_id, customers.apply_line_disc,
	                  customer_addresses.address_id, customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount
	                  FROM customers
	                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
	                  AND customer_addresses.site=1
	                  WHERE customers.customer_id='".$in['buyer_id']."' ");
	      $buyer_info->next();

	      /*if($in['installation_id'] == 'tmp' && !$in['delivery_address_id']){
	        $in['delivery_address_id'] = $buyer_info->f('address_id');
	      }*/
	        
	      $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
	      $in['contact_name'] ='';
	      
	      $sql .= " customer_id = '".$in['buyer_id']."', ";      
	      $sql .= " address_id = '".$in['delivery_address_id']."', ";
	      if($in['delivery_address_id'] != $buyer_info->f('address_id')){
	      	$new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
	       	$in['address_info'] = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
	      }else{
	      	$in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
	      }
	      
	      $sql .= " free_field = '".addslashes($in['address_info'])."' ";      	 	      
	    }
	    $sql .=" WHERE id ='".$in['item_id']."' ";
	    if(!$in['isAdd']){
	      $this->db->query($sql);
	      if($in['item_id'] && is_numeric($in['item_id'])){
	        $trace_id=$this->db->field("SELECT trace_id FROM installations WHERE id='".$in['item_id']."' ");
	        if($trace_id && $in['buyer_id']){
	          $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
	        }
	      }
	    }
	    $in['installation_id'] = $in['item_id'];
	    msg::success(gm('Sync successfull.'),'success');
	    return true;
	}

	function Convention(&$in){
			if($in['account_number']){
				$this->db->query("UPDATE settings SET value='".$in['account_number']."' where constant_name='ACCOUNT_INSTALLATION_START'");
			}
			if($in['account_digits']){
				if(!defined('ACCOUNT_INSTALLATION_DIGIT_NR')){
					$this->db->query("INSERT INTO settings SET value='".$in['account_digits']."', constant_name='ACCOUNT_INSTALLATION_DIGIT_NR', module='installation' ");
					// msg::$success = gm("Setting added.");
				}else{
					$this->db->query("UPDATE settings SET value='".$in['account_digits']."' WHERE constant_name='ACCOUNT_INSTALLATION_DIGIT_NR'");
					// msg::$success = gm("Settings updated").'.';
				}
			}
			
			msg::success ( gm('Data saved'),'success');
			return true;
	}

	function saveAddToPdf(&$in){
	  	if($in['all']){
	  		if($in['value'] == 1){
	  			foreach ($in['item'] as $key => $value) {
				    $_SESSION['add_to_pdf_inst'][$value]= $in['value'];
		  		}
	  		}else{
	  			foreach ($in['item'] as $key => $value) {
				    unset($_SESSION['add_to_pdf_inst'][$value]);
		  		}
			  }
	  	}else{
		    if($in['value'] == 1){
		    	$_SESSION['add_to_pdf_inst'][$in['item']]= $in['value'];
		    }else{
		    	unset($_SESSION['add_to_pdf_inst'][$in['item']]);
		    }
		  }
		$all_pages_selected=false;
	    $minimum_selected=false;
	    if($_SESSION['add_to_pdf_inst'] && count($_SESSION['add_to_pdf_inst'])){
	      if($_SESSION['tmp_add_to_pdf_inst'] && count($_SESSION['tmp_add_to_pdf_inst']) == count($_SESSION['add_to_pdf_inst'])){
	        $all_pages_selected=true;
	      }else{
	        $minimum_selected=true;
	      }
	    }
	    $in['all_pages_selected']=$all_pages_selected;
	    $in['minimum_selected']=$minimum_selected;
	    json_out($in);
	}

	function saveAddToPdfAll(&$in){
	    $all_pages_selected=false;
	    $minimum_selected=false;
	    if($in['value']){
	      unset($_SESSION['add_to_pdf_inst']);
	      if($_SESSION['tmp_add_to_pdf_inst'] && count($_SESSION['tmp_add_to_pdf_inst'])){
	        $_SESSION['add_to_pdf_inst']=$_SESSION['tmp_add_to_pdf_inst'];
	        $all_pages_selected=true;
	      }
	    }else{
	      unset($_SESSION['add_to_pdf_inst']);
	    }
	    $in['all_pages_selected']=$all_pages_selected;
	    $in['minimum_selected']=$minimum_selected;
	    json_out($in);
  	}

}
?>