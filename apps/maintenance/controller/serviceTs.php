<?php

$now 							= time();
$today_of_week 		= date("N", $now);
$month        		= date('n');
$year         		= date('Y');
$week_start 			= mktime(0,0,0,$month,(date('j')-$today_of_week+1),$year);
$week_end 				= $week_start + 604799;
$k=0;
$days = array();
for($i = $week_start; $i <= $week_end; $i += 86400)
{
	array_push($days, $i);
	$day_id = 'tmp_day'.$j;
	$view->assign(array(
		// 'DAY_NAME'    	=> gm(date('D',$i)),
		// 'DATE_MONTH'  	=> date('d',$i).' '.gm(date('M',$i)),
		// 'TIMESTAMP'   	=> $i,
		// 'TD_ID'       	=> $day_id,
		'inc'						=> $k,
		'NO_MARGIN'			=> $j == 7 ? 'no-margin' : '',
		// 'TODAY'					=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? 'today' : '',
		// 'HIDE_ARROW'		=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? '' : 'hide',
	),'day_row');
    $view->loop('day_row');
    $j++;
    $k++;
}


$a = range(1, 24);
foreach ($a as $key ) {
	for ($i=0; $i < 7; $i++) {
		$view->assign(array(
			'hour'    	=> number_as_hour($key),
		),'day_cells');
		$view->loop('day_cells','day_body_rows');
	}
	$view->loop('day_body_rows');
}

return $view->fetch();