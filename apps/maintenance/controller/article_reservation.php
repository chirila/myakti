<?php

	$result=array('recipients'=>array(),'lines'=>array(),'order_articles_id' =>array());
	$allow_packing=$db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING'");
	$allow_sale_unit=$db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT'");
	/*if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){*/
	if(ALLOW_STOCK){
		$result['view_location']		= true;
	}else{
		$result['view_location']		= false;	
	}
	$result['xview']=$in['xview'] ? $in['xview'] : '';

	if($in['service_id'] && !$in['reservation_id']){
		$result['service_id'] =$in['service_id'];
		$result['is_add'] = true;
		$result['page_title'] = gm('Reserving stock');
		$result['delivery_note'] = $in['delivery_note'];

		$service_data=$db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		$delivery_address=$db->query("SELECT * FROM customer_addresses WHERE customer_id='".$service_data->f('customer_id')."' AND delivery='1' ");
		if($delivery_address->next()){
			$result['delivery_address']=strip_tags(nl2br($delivery_address->f('address')."\n".$delivery_address->f('zip').' '.$delivery_address->f('city')."\n".get_country_name($delivery_address->f('country_id'))));
		}else{
			$result['delivery_address']='';
		}
		
		$result['location_dd']				= build_dispach_location_dd($in['select_main_location'],$article_ids);
		$result['select_main_location']		= '0-1';
		$i = 0;
		$j = 0;
		$k = 0;
		$customers_dep = $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC")->getAll();
		$stocks=$db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_customer_id,pim_stock_disp.to_naming,pim_stock_disp.to_address_id,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country 
		           FROM  dispatch_stock 
		           INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id and pim_stock_disp.to_address_id=dispatch_stock.address_id
		           WHERE dispatch_stock.customer_id!=0
		           GROUp BY dispatch_stock.customer_id,dispatch_stock.address_id")->getAll();
	
		$service_article = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_delivered='0' AND article_id!='0' AND has_variants='0' ORDER BY a_id ASC ");
		while($service_article->next()){
			$reserved = 0;
			$delivered = 0;
			$left = 0;
			$delivered = $db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$service_article->f('article_id')."' ");
			$reserved = $db->field("SELECT SUM(quantity) FROM service_reservation WHERE service_id='".$in['service_id']."' AND a_id='".$service_article->f('article_id')."' ");
			$left = $service_article->f('quantity') - $delivered;
			$left_to_reserve = $service_article->f('quantity') - $reserved;

			$use_serial_no = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$service_article->f('article_id')."' ");
			if($use_serial_no==1){
				$j++;
			}
			$use_batch_no = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$service_article->f('article_id')."' ");
				if($use_batch_no==1){
				$k++;
			}
			$hide_stock = $db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$service_article->f('article_id')."' ");
			$packing = $db->field("SELECT packing FROM pim_articles WHERE article_id='".$service_article->f('article_id')."' ");
			$sale_unit = $db->field("SELECT sale_unit FROM pim_articles WHERE article_id='".$service_article->f('article_id')."' ");

			$array_disp = array();
			$nr = 0;
			$new_qty=0;
			foreach($customers_dep as $key => $value){
				$qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$service_article->f('article_id')."' and address_id='".$value['address_id']."' and main_address=1");
				array_push($array_disp, array(
					'quantity'			      =>  display_number($qty),
					'qty'                  	      => $qty,
					//'quantity_delivered'          => display_number($in['quantity_delivered']),
					'quantity_reserved'          => display_number($in['quantity_reserved']),
					'article_id'			=> $service_article->f('article_id'),
					'customer_name'			=> $value['naming'],
					'customer_id'   			=> $value['customer_id'],
					'address_id'    			=> $value['address_id'],
					'type'          			=> 2,
					'order_buyer_country'     	=> $value['country'],
					'order_buyer_city'         	=> $value['city'],
					'order_buyer_zip'          	=> $value['zip'],
					'order_buyer_address'      	=> $value['address'],
					'cust_addr_id'			=> $value['customer_id'].'-'.$value['address_id'],
					'is_error'				=> false,
				 ));
				if($nr==0 && ALLOW_STOCK==0){
			      	$new_qty=$left;
			      }
				$nr++;
			}
			// foreach ($stocks as $key => $value) {
			//   	$qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$service_article->f('article_id')."' and address_id='".$value['to_address_id']."' and customer_id='".$value['customer_id']."'");
			//   	array_push($array_disp, array(
			// 		'quantity'				=>  display_number($qty),
			// 		'qty'                  		 => $qty,
			// 		//'quantity_delivered'        => display_number($in['quantity_delivered']),
			// 		'quantity_reserved'          => display_number($in['quantity_reserved']),
			// 		'article_id'			=> $service_article->f('article_id'),
			// 		'customer_name'			=> $value['to_naming'],
			// 		'customer_id'   			=> $value['to_customer_id'],
			// 		'address_id'    			=> $value['to_address_id'],
			// 		'type'          			=> 2,
			// 		'order_buyer_country'     	=> $value['to_country'],
			// 		'order_buyer_city'         	=> $value['to_city'],
			// 		'order_buyer_zip'          	=> $value['to_zip'],
			// 		'order_buyer_address'      	=> $value['to_address'],
			// 		'cust_addr_id'			=> $value['customer_id'].'-'.$value['address_id'],
			// 		'is_error'				=> false,
			//   	));
			// }

			$first_location_qty =return_value($array_disp[0]['quantity']);

			$order_line=array(
				'article'				=> $service_article->f('name'),
				'article_id'			=> $service_article->f('article_id'),
				//'quantity_value'			=> $left > $new_qty ? $new_qty : $left,
				//'quantity'				=> $hide_stock ? display_number($left) : ($left > $new_qty ? display_number($new_qty) : display_number($left)),
/*				'quantity_value'							=> $left,
      			'quantity'											=> display_number($left),*/
      			'quantity_value'		=> $left_to_reserve,
      			'quantity'				=> display_number($left_to_reserve),
				'hide_stock'			=> $hide_stock ,
				'quantity_value2'			=> $left_to_reserve,
				'quantity2'				=> display_number($left_to_reserve),
				'a_id' 				=> $service_article->f('a_id'),
				'show_stock'           		=> $hide_stock == 0 ? true : false,
				'use_serial_no_art'		=> $use_serial_no == 1 ? true : false,
				'count_selected_s_n'		=> $db->field("SELECT COUNT(id) FROM serial_numbers WHERE article_id='".$service_article->f('article_id')."' AND status_id = '1' "),		
				'use_batch_no_art'		=> $use_batch_no == 1 ? true : false,
				'count_selected_b_n'		=> 0,	
				'selected_s_n'			=> '',	
				'allow_article_packing'       => $allow_packing,
				'allow_article_sale_unit'     => $allow_sale_unit,
				'packing'				=> remove_zero_decimals($packing),
				'sale_unit'				=> $sale_unit,
				'red_mark'				=> false,
				//'view_location'			=> ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS ? true : false,
				'view_location'			=>ALLOW_STOCK ? true : false,
				'disp_addres_info' 		=> $array_disp,
				//'is_error'				=> $hide_stock ? false : ($left > $new_qty ? true : false),
				'is_error'										=> (!$hide_stock && ALLOW_STOCK) ? ($left_to_reserve > $first_location_qty ? true : false) : false,
			);
			if($left_to_reserve>0){
				array_push($result['lines'],$order_line);
				array_push($result['order_articles_id'] , $service_article->f('a_id'));
				$i++;
			}
			
		}
		$result['allow_article_packing']       = $allow_packing;
  		$result['allow_article_sale_unit']     = $allow_sale_unit;
		$result['use_serial_no'] 		= $j > 0 && ALLOW_STOCK ? true : false;
		$result['use_batch_no'] 		= $k > 0 && ALLOW_STOCK ? true : false;
		$result['serial_number']		= $service_data->f('serial_number');
		$result['allow_stock']			= defined('ALLOW_STOCK') && ALLOW_STOCK=='1' ? true : false;
	}else{
		$result['service_id'] = $in['service_id'];
		$result['delivery_id'] = $in['delivery_id'];
		$result['page_title'] = gm('Edit Delivery');

		$service_data=$db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");

		$del = $db->query("SELECT service_deliveries.*,service_delivery.delivery_note
		  	FROM  service_deliveries
		  	INNER JOIN service_delivery ON service_delivery.delivery_id = service_deliveries.delivery_id
		  	WHERE service_deliveries.service_id='".$in['service_id']."' AND service_deliveries.delivery_id='".$in['delivery_id']."'");

		$del->next();
		$result['delivery_note']	= $del->f('delivery_note');
		$result['is_add']       	=  false;
		$result['order_info']   	= array();

		$result['delivery_date'] 		= date(ACCOUNT_DATE_FORMAT,$del->gf('date'));
		$result['delivery_date_js'] 		= $del->f('date')*1000;
		$result['hour']          		= $del->f('hour')?$del->f('hour'):'';
		$result['minute']        		= $del->f('minute')?$del->f('minute'):'';
		$result['pm']            		= $del->f('pm')?$del->f('pm'):'';
		$result['contact_name']  		= $del->f('contact_name')?$del->f('contact_name'):'-';
		$result['contact_id']  			= $del->f('contact_id')?$del->f('contact_id'):'';
		$result['delivery_address_txt']  	= nl2br($del->f('delivery_address'));
		$result['lines'] 				= array();
		$i = 0;
		$line = $db->query("SELECT service_delivery.*,servicing_support_articles.name FROM service_delivery
					INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
					WHERE service_delivery.service_id='".$in['service_id']."' AND service_delivery.delivery_id='".$in['delivery_id']."' AND servicing_support_articles.article_id!='0' ORDER BY id ASC");
		while($line->next()){
			$delivered = 0;
			$left = 0;
			$delivered = $db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$line->f('a_id')."' ");
			$left = $line->f('quantity');

			$packing = $db->field("SELECT packing FROM pim_articles WHERE article_id='".$line->f('a_id')."' ");
			$sale_unit = $db->field("SELECT sale_unit FROM pim_articles WHERE article_id='".$line->f('a_id')."' ");

			$linie = array(
				'article'			=> $line->f('name'),
				'quantity'			=> display_number($left),
				'a_id' 			=> $line->f('a_id'),
			      'packing'       		=> remove_zero_decimals($packing),
			      'sale_unit'       	=> $sale_unit,
			);
			$use_serial_nr = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
			if( $use_serial_nr == 1 ){
			      $article_s_n_data = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$line->f('article_id')."' AND delivery_id = '".$in['delivery_id']."' ");
			      $k = 0;
			      $selected_s_n ='';
			      while($article_s_n_data->next()){
			          if($k==0){
			            $selected_s_n .= $article_s_n_data->f('id');
			          }else{
			            $selected_s_n .= ','.$article_s_n_data->f('id');
			          }
			        $k++;
			      }
			      if($k>0){
			        $use_serial_no = true;
			        $linie['use_serial_no_art']     = true;
			      }
			      $count_selected_s_n = $k;
			      $linie['selected_s_n']          = $selected_s_n;
			      $linie['count_selected_s_n']    = $count_selected_s_n;
			}
			$use_batch_nr = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('a_id')."' ");
		    	if( $use_batch_nr == 1 ){
		      	$article_b_n_data = $db->query("SELECT * FROM batches WHERE article_id = '".$line->f('a_id')."' AND delivery_id = '".$in['delivery_id']."' ");
		      	$m = 0;
		      	while($article_b_n_data->next()){
		        	$m++;
		      	}
		      	if($m > 0){
		        	$use_batch_no = true;
		        	$linie['use_batch_no_art']     = true;
		      	}
		    	}
		    	if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
		      	$linie['view_location']=true;
		      	$j = 0;

		      	$locations=$db->field("SELECT stock_movements.location_info  FROM stock_movements
		        		WHERE stock_movements.article_id='".$line->f('a_id')."' AND stock_movements.delivery_id='".$in['delivery_id']."'");
		      	$locations=unserialize($locations);
		      	$linie['locations']=array();
		      	if(!empty($locations)){
			      	foreach ($locations as $customer => $value) {
						if(return_value($value)!='0.00'){
			          			$customeres=explode('-', $customer);
					          	if($customeres[0]==0){
					            	$customer_info=$db->query("SELECT dispatch_stock_address.* FROM dispatch_stock_address WHERE address_id='".$customeres['1']."'");
					          	}else{
					            	$customer_info=$db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_naming,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
					         		FROM  dispatch_stock
					         		INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id
					         		WHERE dispatch_stock.customer_id='".$customeres['0']."' and dispatch_stock.address_id='".$customeres['1']."' ");
					          	}
							array_push($linie['locations'],array(
									'quantity_loc'=> remove_zero_decimals($value),
									'depo'   	=> $customer_info->f('naming')?$customer_info->f('naming'):$customer_info->f('to_naming'),
									'address'   =>$customer_info->f('address')?$customer_info->f('address'):$customer_info->f('to_address'),
									'city'     =>$customer_info->f('city')?$customer_info->f('city'):$customer_info->f('to_city'),
									'zip'   	=>$customer_info->f('zip')?$customer_info->f('zip'):$customer_info->f('to_zip'),
									'country'   =>get_country_name($customer_info->f('country_id'))?get_country_name($customer_info->f('country_id')):get_country_name($customer_info->f('to_country_id')),
							));
			        		}
			      	}
			    }
		      	$j++;
			}else{
			  	$linie['view_location']=false;
			  	$linie['locations']=array();
			}

			array_push($result['lines'],$linie);
			array_push($result['order_articles_id'] , $linie['order_articles_id']);
			$result['order_info']['delivery_note']=$line->f('delivery_note');
			$i++;
		}
		if($i>0){
			$data = true;
		}

  		$result['allow_article_packing']       = $allow_packing;
  		$result['allow_article_sale_unit']     = $allow_sale_unit;
  		$result['use_serial_no']         	   = $use_serial_no;
  		$result['use_batch_no']         	   = $use_batch_no;
  		$result['serial_number']		= $service_data->f('serial_number');

  		if($del->f('delivery_address')){
   			$result['delivery_address']=strip_tags($del->f('delivery_address'));
  		}else{
  			$delivery_address=$db->query("SELECT * FROM customer_addresses WHERE customer_id='".$service_data->f('customer_id')."' AND delivery='1' ");
			if($delivery_address->next()){
				$result['delivery_address']=strip_tags(nl2br($delivery_address->f('address')."\n".$delivery_address->f('zip').' '.$delivery_address->f('city')."\n".get_country_name($delivery_address->f('country_id'))));
			}else{
				$result['delivery_address']='';
			}
  		}
	}

	$customer_contacts = $db->query("SELECT customer_contacts.* FROM servicing_support
		             INNER JOIN customer_contacts ON customer_contacts.customer_id=servicing_support.customer_id
		             WHERE servicing_support.service_id='".$in['service_id']."' AND customer_contacts.active ='1'");

	while ($customer_contacts->move_next()){
		$recipient = array(
			'recipient_name'  			=> $customer_contacts->f('firstname').' '.$customer_contacts->f('lastname'),
			'recipient_email' 			=> $customer_contacts->f('email'),
			'recipient_id'    			=> $customer_contacts->f('contact_id'),
		);
		array_push($result['recipients'], $recipient);
	}   

return json_out($result);
?>