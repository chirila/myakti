<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$view = new at(__DIR__.'/../view/submit_email.html');
$db=new sqldb();
$service = $db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."'");
if(!$service->move_next()){
	$view->assign(array('data'=>false));
	return $view->fetch();
}

$date = date(ACCOUNT_DATE_FORMAT,$service->f('planeddate'));
if($service->f('startdate')){
	$date .= ' - '.number_as_hour($service->f('startdate'));
}

switch ($service->f('status')) {
	case '1':
		$status = 'Planned';
		$classbg = 'quote_sent';
		break;
	case '2':
		$status = 'Closed';
		$classbg = '';
		break;
	default:
		$status = 'Draft';
		$classbg = '';
		break;
}
$user_nr=0;
$users = $db->query("SELECT * FROM servicing_support_users WHERE service_id='".$in['service_id']."' ORDER BY name ASC ");
while($users->next()){
	$hour_data=$db->field("SELECT SUM(end_time-start_time-break) FROM servicing_support_sheet WHERE service_id='".$in['service_id']."' AND user_id='".$users->f('user_id')."' ");
	$exp_data=$db->field("SELECT SUM(CASE WHEN expense.unit_price > 0 THEN expense.unit_price*project_expenses.amount ELSE project_expenses.amount END) FROM project_expenses
				LEFT JOIN expense ON project_expenses.expense_id=expense.expense_id 
				WHERE project_expenses.service_id='".$users->f('service_id')."' AND project_expenses.user_id='".$users->f('user_id')."' ");

	$sheet = $db->query("SELECT servicing_support.*,servicing_support_sheet.*, servicing_support_sheet.date AS date2  FROM servicing_support_sheet
			LEFT JOIN servicing_support ON servicing_support_sheet.service_id=servicing_support.service_id
			WHERE servicing_support.service_id='".$in['service_id']."' AND servicing_support_sheet.user_id='".$users->f('user_id')."' ");
	$i=0;
	while ($sheet->next()) {
		$view->assign(array(
			'DATE'						=> date(ACCOUNT_DATE_FORMAT,$sheet->f('date2')),
			'hours'						=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break')),
			'break'						=> number_as_hour($sheet->f('break')),
			'start_time'				=> number_as_hour($sheet->f('start_time')),
			'end_time'					=> number_as_hour($sheet->f('end_time')),
			'VIEW_L'					=> $sheet->f('status') != 2 ? "" : 'hide',
			'time_report_txt'			=> nl2br(stripslashes($sheet->f('notes'))),
			'time_report'				=> stripslashes($sheet->f('notes')),
			'internal_report_txt'		=> $sheet->f('internal_report') ? 'Yes' : 'No',
			'extra_hours_txt'			=> $sheet->f('extra_hours') ? gm('Yes') : gm('No'),
			'fixed'					=> $service->f('service_type')==1? true : false,
		),'hours');
		$view->loop('hours','users');		
		$i++;
	}
	$expense = $db->query("SELECT project_expenses.*, servicing_support.customer_name AS c_name, servicing_support.serial_number AS p_name, servicing_support_users.name as u_name,
			expense.name AS e_name,expense.unit_price, expense.unit, servicing_support.customer_id as customer_id
				FROM project_expenses
				INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
				INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
				INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id AND project_expenses.service_id=servicing_support_users.service_id
		AND project_expenses.user_id = servicing_support_users.user_id
				WHERE project_expenses.user_id='".$users->f('user_id')."' AND servicing_support.service_id='".$in['service_id']."' ORDER BY id");
	$q=0;
	while ($expense->next()) {
		$amount = place_currency(display_number($expense->f('amount')));
		if($expense->f('unit_price')){
			$amount = place_currency(display_number(($expense->f('amount') * $expense->f('unit_price'))))." (".$expense->f('amount')." ".$expense->f('unit').")";
		}
		$view->assign(array(
			'DATE'   					=> date(ACCOUNT_DATE_FORMAT,$expense->f('date')),
			'PROJECT_CUSTOMER_NAME'		=> $expense->f('c_name'),
			'PROJECT_LIST_NAME'			=> $expense->f('p_name'),
			'category'					=> $expense->f('e_name'),
			'amount'					=> $amount,
			'amount_txt'				=> $expense->f('amount'),
			'note'						=> split_lines($expense->f('note')) ? split_lines($expense->f('note')):'',
			'VIEW_IMG'					=> $expense->f('picture') == "" ? false : true,
			'IMG_HREF'					=> UPLOAD_PATH.DATABASE_NAME."/receipt/".$expense->f('picture'),
			'VIEW_L'					=> $expense->f('billed') == 1 ? 'hide' : '',
		),'expenses');
		$view->loop('expenses','users');		
		$q++;
	}
	$view->assign(array(
		'name' 		=> $users->f('name'),
		'id'		=> $users->f('u_id'),
		'pr_m'		=> $users->f('pr_m') == 1 ? 'checked="checked"' : '' ,
		'hours'			=> ($hour_data && $hour_data>0) ? number_as_hour($hour_data) : '00:00',
		'expences'			=> $exp_data ? place_currency(display_number($exp_data)) : place_currency(display_number(0)),
		'is_hours_data'	=> $i>0 ? true : false,
		'is_expense_data'	=> $q>0 ? true : false,
		'fixed'				=> $service->f('service_type')==1? true : false,
	),'users');
	$view->loop('users');
	$user_nr++;
}

	$task_nr =0;
	$task = $db->query("SELECT * FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY task_name ASC ");
	while($task->next()){
		$view->assign(array(
			'name' 			=> $task->f('task_name'),
			'checked'		=> $task->f('closed') == 1 ? 'checked="checked"' : '',
			'comment'		=> nl2br($task->f('comment')),
			'fixed'			=> $service->f('service_type')==1? true : false,
		),'tasks');
		$view->loop('tasks');
		$task_nr++;
	}

	$services_nr =0;
	$srv = $db->query("SELECT servicing_support_tasks.*,pim_articles.item_code FROM servicing_support_tasks
			INNER JOIN pim_articles ON servicing_support_tasks.article_id=pim_articles.article_id 
			WHERE servicing_support_tasks.service_id='".$in['service_id']."' AND servicing_support_tasks.article_id!='0' ORDER BY servicing_support_tasks.task_name ASC ");
	while($srv->next()){
		$view->assign(array(
			'name' 			=> $srv->f('task_name'),
			'checked'		=> $srv->f('closed') == 1 ? 'checked="checked"' : '',
			'comment'		=> nl2br($srv->f('comment')),
			'task_budget'	=> display_number($srv->f('task_budget')),
			'code'		  	    => $srv->f('item_code'),
			'quantity'			=> display_number($srv->f('quantity')),
		),'services');
		$view->loop('services');
		$services_nr++;
	}

	$suppy_nr=0;
	$supply = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY name ASC ");
	while($supply->next()){
		$view->assign(array(
			'name' 			=> $supply->f('name'),
			'id'			=> $supply->f('a_id'),
			'quantity'		=> display_number($supply->f('quantity')),
			'to_deliver'	=> display_number($supply->f('to_deliver')),
			'delivered'		=> display_number($supply->f('delivered')),
			'price'			=> place_currency(display_number($supply->f('price')*$supply->f('margin')/100+$supply->f('price')),$currency),
			'sell_price'	=> place_currency(display_number($supply->f('price')*$supply->f('margin')/100+$supply->f('price')),$currency),
			'billable'		=> $supply->f('billable') ? 'checked="checked"' : '',
		),'supply');
		$view->loop('supply');
		$suppy_nr++;
	}

	$article_nr=0;
	$articles = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY name ASC ");
	while($articles->next()){
		$art_data=$db->query("SELECT * FROM pim_articles WHERE article_id='".$articles->f('article_id')."' ");
		$q_delivered=$db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$articles->f('article_id')."' ");
		$view->assign(array(
			'name' 			=> $articles->f('name'),
			'id'				=> $articles->f('a_id'),
			'quantity'			=> display_number($articles->f('quantity')),
			'to_be_delivered'		=> display_number($articles->f('quantity')-$q_delivered),
			'price'			=> place_currency(display_number($articles->f('price')),$currency),
			'sell_price'		=> place_currency(display_number($articles->f('price')),$currency),
			'billable'			=> $articles->f('billable') ? 'checked="checked"' : '',
			'code'			=> $art_data->f('item_code'),
		),'articles');
		$view->loop('articles');
		$article_nr++;
	}

	$allow_packing=$db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING'");
	$allow_sale_unit=$db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT'");

	$delivery_nr=0;
	$deliveries = $db->query("SELECT * FROM service_deliveries WHERE service_id='".$in['service_id']."' ORDER BY delivery_id ASC");
	while($deliveries->next()){
		$line = $db->query("SELECT service_delivery.*,servicing_support_articles.name FROM service_delivery
					INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
					WHERE service_delivery.service_id='".$in['service_id']."' AND service_delivery.delivery_id='".$deliveries->f('delivery_id')."' AND servicing_support_articles.article_id!='0' ORDER BY id ASC");
		$delivery_art=0;
		while($line->next()){
			$packing = $db->field("SELECT packing FROM pim_articles WHERE article_id='".$line->f('a_id')."' ");
			$sale_unit = $db->field("SELECT sale_unit FROM pim_articles WHERE article_id='".$line->f('a_id')."' ");
			$view->assign(array(
				'line_q'			=> display_number($line->f('quantity')),
				'article'			=> $line->f('name'),		
			    'packing'       	=> remove_zero_decimals($packing),
			    'sale_unit'       	=> $sale_unit,
			    'allow_article_packing'		=> $allow_packing==1 ? true : false,
			    'allow_article_sale_unit'	=> $allow_sale_unit==1 ? true : false,
			),'delivery_articles');
			$view->loop('delivery_articles','deliveries');
			$delivery_art++;
		}
		$view->assign(array(
			'date'				=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('date')),
			'address'			=> nl2br($deliveries->f('delivery_address')),
			'contact'			=> $db->field("SELECT CONCAT_WS(' ',lastname,firstname) FROM customer_contacts WHERE contact_id='".$deliveries->f('contact_id')."' "),
			'is_article'		=> $delivery_art ? true : false,
			'allow_article_packing'		=> $allow_packing==1 ? true : false,
			'allow_article_sale_unit'	=> $allow_sale_unit==1 ? true : false,
		),'deliveries');
		$view->loop('deliveries');
		$delivery_nr++;
	}
	$view->assign(array(
		'is_user'				=> $user_nr ? true : false,
		'is_task'				=> $task_nr ? true : false,
		'is_service'			=> $services_nr ? true : false,
		'is_supply'				=> $suppy_nr ? true : false,
		'is_article'			=> $article_nr ? true : false, 
		'is_delivery'			=> $delivery_nr ? true : false,
		'data'					=> true,
		'STATUS'				=> $status,
		'intervention_report'	=> nl2br($service->f('report')),
		'fixed'					=> $service->f('service_type')==1? true : false,
	));
return $view->fetch();
