<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $config;
// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);

if(!$in['service_id'])
{
	return ark::run('maintenance-service');
}
$tblinvoice = $db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."'");
if(!$tblinvoice->move_next()){
	return ark::run('maintenance-service');
}
$in['message'] = stripslashes($in['message']);
global $database_config,$config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$e_lang = $tblinvoice->f('email_language');
if(!$e_lang || $e_lang > 4){
	$e_lang=1;
}
$text_array = array('1' => array('simple' => array('1' => 'INTERVENTION WEB LINK', '2'=> 'You can download your intervention HERE','3'=>"HERE",'4'=>'Web Link'),
                       'pay' => array('1' => 'INTERVENTION WEB LINK', '2'=> 'You can download your intervention HERE and pay it online','3'=>"HERE",'4'=>'Web Link')
                       ),
              '2' => array('simple' => array('1' => 'LIEN WEB INTERVENTION', '2'=> 'Vous pouvez télécharger votre intervention ICI','3'=>'ICI','4'=>'Lien web'),
                       'pay' => array('1' => 'LIEN WEB INTERVENTION', '2'=> 'Vous pouvez télécharger votre intervention ICI et la payer en ligne','3'=>'ICI','4'=>'Lien web')
                       ),
              '3' => array('simple' => array('1' => 'WEB LINK INTERVENTIE', '2'=> 'U kan uw interventie HIER downloaden','3'=>'HIER','4'=>'Weblink'),
                       'pay' => array('1' => 'WEB LINK INTERVENTIE', '2'=> 'U kan uw interventie HIER downloaden en online betalen','3'=>'HIER','4'=>'Weblink')
                       ),
              '4' => array('simple' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
                       'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER','4'=>'WEB-LINK')
                       )
    );

$db_users = new sqldb($database_users);
if($in['use_html']){
	if(defined('USE_MAINTENANCE_WEB_LINK') && USE_MAINTENANCE_WEB_LINK == 1){
		//$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['service_id']."' AND `type`='m' ");
    $exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type ",['d'=>DATABASE_NAME,'item_id'=>$in['service_id'],'type'=>'m']);

        $extra = "<p style=\"background: #f7f9fa; width: 500px; text-align:center; padding-bottom: 15px; border: 2px solid #deeaf0; color:#868d91;\"><br />";
        $extra .="<img src=\"https://app.akti.com/pim/img/email_arrow_left.png\" width=\"18\" height=\"8\" />&nbsp;&nbsp;";
        $extra .="<b style=\"color: #5199b7;\"><a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b>";
        $extra .="&nbsp;&nbsp;<img src=\"https://app.akti.com/pim/img/email_arrow_right.png\" width=\"18\" height=\"8\" /><br />";
        $extra .=$text_array[$e_lang]['simple']['2']."<br /></p>";

        $in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $in['message']);
    }else{
        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
    }
}else{
	if(defined('USE_MAINTENANCE_WEB_LINK') && USE_MAINTENANCE_WEB_LINK == 1){
		//$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['service_id']."' AND `type`='m' ");
    $exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type ",['d'=>DATABASE_NAME,'item_id'=>$in['service_id'],'type'=>'m']);

	   	$extra = "\n\n-----------------------------------";
	   	$extra .="\n".$text_array[$e_lang]['simple']['1'];
	   	$extra .="\n-----------------------------------";
	   	$extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);

	   	$in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $in['message']);
	}else{
        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
    }
}

$result = array(
  'e_message' => $in['message'],
  'attach_calendar' => $db->field("SELECT value FROM settings WHERE constant_name='MAINTENANCE_ATTACH_CALENDAR' ")? true:false,

  );
json_out($result);
/*
$view->assign(array(
	'e_message'            => nl2br($in['message']),
));

global $main_menu_item;
global $sub_menu_item;
$main_menu_item='invoices';
$sub_menu_item='invoices_list';
return $view->fetch();*/
?>