<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	$l_r = ROW_PER_PAGE;

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	$filter = 'WHERE 1=1 ';
	$order_by_array=array('name', 'nr');
	$order_by = " ORDER BY name ASC ";

	if(!empty($in['search'])){
		$filter.=" AND name LIKE '%".$in['search']."%' ";
	}

	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	if(!empty($in['order_by'])){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == 'true' || $in['desc']=='1'){
				$order = " DESC ";
			}
			if($in['order_by']=='nr' ){
				$filter_limit =' ';
	    		$order_by ='';
		    }else{
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
				$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
			}
		}
	}

	$teams=$db->query("SELECT * FROM servicing_team ".$filter.$order_by.$filter_limit);
	$max_rows=$db->query("SELECT * FROM servicing_team ".$filter)->records_count();
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	//$teams->move_to($offset*$l_r);
	$j=0;
	while($teams->move_next() ){
		$users_nr=$db->field("SELECT COUNT(user_id) FROM servicing_team_users WHERE team_id='".$teams->f('team_id')."' ORDER BY sort ASC"); 
		$item=array(
			'team_id'		=> $teams->f('team_id'),
			'name'		=> stripslashes($teams->f('name')),
			'nr'			=> $users_nr,
			'delete_link'	=> array('do'=>'maintenance-teams-team-delete','team_id'=>$teams->f('team_id'))
		);
		array_push($result['query'], $item);
		$j++;
	}

	if($in['order_by']=='nr' ){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'], SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by'], SORT_DESC);
	    }

	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	}
    $result['lr'] = $l_r;
	json_out($result);
?>