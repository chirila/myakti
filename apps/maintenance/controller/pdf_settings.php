<?php
/* * **********************************************************************
 * @Author: MedeeaWeb Works                                              *
 * ********************************************************************** */
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
$result = array( 'logo'=>array());
$tblinvoice = $db->query("SELECT email_language,pdf_logo, identity_id FROM servicing_support WHERE service_id='".$in['service_id']."'");

$set_logo = $tblinvoice->f('pdf_logo');
if(!$set_logo){
	$set_logo = ACCOUNT_MAINTENANCE_LOGO;
}
if(ACCOUNT_MAINTENANCE_LOGO == '')   {
    /*$view->assign('account_logo','../img/no-logo.png','logo');
    $view->assign('default','CHECKED','logo');
    $view->loop('logo');*/
    array_push($result['logo'], array(
        'account_logo'=>'../img/no-logo.png',
        'default'=>'../img/no-logo.png'
        ));
}
else {
    $logos = glob(__DIR__.'/../../../../upload/'.DATABASE_NAME.'/{m_logo_img_}*',GLOB_BRACE);
    foreach ($logos as $v) {
        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
        $size = getimagesize('../'.$logo);
        $ratio = 250 / 77;
        if($size[0]/$size[1] > $ratio ){
            // $view->assign('attr', 'width="250"','logo');
            $attr = 'width="250"';
        }else{
            // $view->assign('attr', 'height="77"','logo');
            $attr = 'height="77"';
        }
        /*$view->assign(array(
            'account_logo'  => $logo,
            'default'       => $logo == $set_logo ? 'CHECKED' : ''
        ),'logo');
        $view->loop('logo');*/
        array_push($result['logo'], array(
        'account_logo'=>$logo,
        'default'=>$set_logo,
        'attr' => $attr
        ));
    }
}

if(!$tblinvoice->f('identity_id')) {
  $selected='0';
}else{
 $selected=$tblinvoice->f('identity_id');
}
/*$view->assign(array(
    'language_dd'   => build_language_dd_new($tblinvoice->f('email_language')),

    'multiple_identity' => build_identity_dd($selected),
    'identity_id'       => $tblinvoice->f('identity_id'),
));*/
$result['language_dd'] = build_language_dd_new($tblinvoice->f('email_language'));
$result['language_id'] = $tblinvoice->f('email_language') ? $tblinvoice->f('email_language') : '1';
$result['multiple_identity'] = build_identity_dd($selected);
$result['multiple_identity_id'] = $selected;
$result['identity_id'] = $tblinvoice->f('identity_id');
$result['default_logo'] = $set_logo;
$result['service_id'] = $in['service_id'];
// $view->assign('language_dd', build_language_dd_new($tblinvoice) );
json_out($result);
// return $view->fetch();