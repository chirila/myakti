<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
session_write_close();
global $cfg;
$db5 = new sqldb();
$srv_data = $db5->query("SELECT update_pdf FROM servicing_support WHERE service_id = '".$in['service_id']."' ");
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");
/*
if($quote_data->f('preview') == 1 && !$in['save_as']){
	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);
	if(ark::$model == 'quote' && (ark::$method == 'send' || ark::$method == 'sendNewEmail')){
		$in['attach_file_name'] = 'quote_'.$in['id'].'_'.$in['version_id'].'.pdf';
		$file = $aws->getItem('quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf',$in['attach_file_name']);
		if($file === true){
			return;
		}else{
			$in['attach_file_name'] = null;
		}
	}else{
		$link =  $aws->getLink($cfg['awsBucket'].DATABASE_NAME.'/quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf');
		$content = file_get_contents($link);
		if($content){
			header('Content-Type: application/pdf');
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $content;
			exit();
		}
	}
}*/
$in['attach_file_name'] = 'intervention_'.$in['service_id'].'.pdf';
if(!$in['save_as'] && $srv_data->f('update_pdf') == 0){
	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);
	if(ark::$model == 'maintenance' && (ark::$method == 'send' || ark::$method == 'sendNewEmail')){
		$in['attach_file_name'] = 'intervention_'.$in['service_id'].'.pdf';
		$file = $aws->getItem('intervention/intervention_'.$in['service_id'].'.pdf',$in['attach_file_name']);
		if($file === true){
			return;
		}else{
			$in['attach_file_name'] = null;
		}
	}else{
		$link =  $aws->getLink($cfg['awsBucket'].DATABASE_NAME.'/intervention/intervention_'.$in['service_id'].'.pdf');
		$content = file_get_contents($link);
		$inv_serial_number=$db->field("SELECT serial_number FROM servicing_support WHERE service_id='{$in['service_id']}' ");
		if($inv_serial_number){
             $name=$inv_serial_number.'.pdf';
		}else{
			$name='intervention_'.$in['service_id'].'.pdf';
		}
		if($content){
			if($in['base64']){
				//$name= 'invoice.pdf';
				$str = 'Content-Type: application/pdf;'."\r\n";
				$str .= ' name="'.$name.'"'."\r\n";
				$str .= 'Content-Transfer-Encoding: base64'."\r\n";
				$str .= 'Content-Disposition: attachment;'."\r\n";
				$str .= ' filename="'.$name.'"'."\r\n\r\n";
				$str = chunk_split(base64_encode($content), 76, "\r\n");
				return $str;
			}
			doQueryLog();
			header('Content-Type: application/pdf');
			header("Content-Disposition:inline;filename=".$name);
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $content;
			exit();
		}

	}
}

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetFont('helvetica', '', 10, '', false);
if($custom_lng){
	#we can use both font types but dejavusans looks better
	$pdf->SetFont('dejavusans', '', 9, '', false);
	// $pdf->SetFont('freeserif', '', 10, '', false);
}
$pdf_type =array(4,5);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Intervention');
$pdf->SetSubject('Intervention');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);


if(!USE_QUOTE_PAGE_NUMBERING){
	$pdf->setPrintFooter(false);
	$pdf->SetAutoPageBreak(TRUE, 10);
}

// echo PDF_MARGIN_BOTTOM; exit();

// find out where to set auto page break

// while($quote_data->next()){
/*	$show_total_p = $quote_data->f('show_grand');
	$show_vat_p = $quote_data->f('show_vat');
	$discount_p = $quote_data->f('discount');
	$apply_discount_p = $quote_data->f('apply_discount');
	$general_vat_p = $quote_data->f('vat');
// }
$discount_procent = $discount_p ? $discount_p : 0;
if($apply_discount_p < 2){
	$discount_procent = 0;
}*/

/*if($show_total_p){
	$a_custom_page_break=30;
	if(!$discount_procent){
		$a_custom_page_break-=6;
	}
	if(!$show_vat_p){
		$a_custom_page_break-=6;
	}else{
		$nr_of_vats_p = $db5->field("SELECT COUNT( DISTINCT tblquote_line.vat )
		FROM tblquote_group
		LEFT JOIN tblquote_line ON tblquote_group.group_id = tblquote_line.group_id
		WHERE tblquote_group.quote_id = '".$in['id']."'
		AND tblquote_group.version_id = '".$in['version_id']."'
		AND tblquote_line.vat !=0");
		$a_custom_page_break = $a_custom_page_break + ($nr_of_vats_p*6)-6+2;
	}
}else{*/
	$a_custom_page_break=10;
// }
// echo $a_custom_page_break; exit();
if(in_array($in['type'], $pdf_type)){
	$pdf->SetMargins(10, 10, 10);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, $a_custom_page_break);
	$hide_all = 2;
}

// $print_logo=ACCOUNT_LOGO_QUOTE;
// 	        $image_file = '../../'.$print_logo;
$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();
$pdf->customTmargin = 15;

$htmll = include('print_body.php');
// var_dump(include('print_body.php'));
// print_r($html); exit();

//console::benchmark('writeHTML');
$pdf->writeHTML($htmll, true, false, true, false, '');
//console::end('writeHTML');
// if($in['height']){
	// echo 'height = '.$height; echo "<br/>";
	// print_r($pdf->GetY());
// }
/*
$height_generated = $pdf->GetY();
if($height_generated-8 > $height && $in['type'] !=3){
	$pdf->AddPage();
}

$pdf->SetY($height);
if(in_array($in['type'], $pdf_type)){
	$hide_all = 1;
	// $pdf->SetAutoPageBreak(TRUE, 0);
	// if($in['type'] == 5){
		$pdf->SetY($height-11);
		$pdf->SetAutoPageBreak(TRUE, 10);

	$htmls = include('print_body.php');
// print_r($htmls);exit();
	$pdf->writeHTML($htmls, true, false, true, false, '');
}*/
// quote custom type
// c0831b0d_6be4_d196_2915707ca24a (database for the client DAS MEDIA)

$pdf->lastPage();
$in['last_page'] = $pdf->getPage();

if(isset($in['print'])){
	echo "<pre>";
	echo $htmll;
	exit();
}


if($in['save_as'] == 'F'){
	$in['invoice_pdf_name'] = 'intervention_'.time().'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['invoice_pdf_name'], 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['invoice_pdf_name'];
	$pdfFile = 'intervention/intervention_'.$in['service_id'].".pdf";
	$a->uploadFile($pdfPath,$pdfFile);
	if(ark::$method == 'gpasta'){
		$outputType = "S";
		if($in['attachAsString']==1){
			$outputType = "E";
		}
		$str = $pdf->Output(__DIR__.'/../../../'.$in['invoice_pdf_name'],$outputType);
	}
	$db->query("UPDATE servicing_support SET update_pdf=0 WHERE service_id='".$in['service_id']."'");
	unlink(INSTALLPATH.$in['invoice_pdf_name']);
}else if($srv_data->f('update_pdf') == '1'){
	$in['int_name'] = 'intervention_'.time().'.pdf';
	//$pdf->Output(__DIR__.'/../../../'.$in['int_name'], 'F');
	$pdf->Output(__DIR__.'/../../../intervention_'.$in['service_id'].".pdf", 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['int_name'];
	$pdfFile = 'intervention/intervention_'.$in['service_id'].".pdf";
	$a->uploadFile($pdfPath,$pdfFile);
	unlink(INSTALLPATH.$in['int_name']);
	$db->query("UPDATE servicing_support SET update_pdf=0 WHERE service_id='".$in['service_id']."'");
	$pdf->Output($serial_number.'.pdf','I');
}else if($in['do']=='maintenance-print'){
	doQueryLog();
	$pdf->Output($serial_number.'.pdf','I');
}else{
	$pdf->Output(__DIR__.'/../../../'.'intervention.pdf', 'F');
}