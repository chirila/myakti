<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}



	function get_PDFlayout($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'layout'=>array());
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		for ($i=1; $i <= 1; $i++) {
			array_push($data['layout'], array(
				'view_invoice_pdf' 		=>'index.php?do=maintenance-print&type='.$i.'&lid='.$language,
				'img_href'						=> '../pim/img/type-'.$i.'_i.jpg',
				'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
				'selected'						=> ACCOUNT_MAINTENANCE_PDF_FORMAT == $i && USE_CUSTOME_MAINTENANCE_PDF == 0 ? 'active' : '',
			));
		}

		return json_out($data, $showin,$exit);
	}

	function get_customLabel($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'customLabel'=>array());

		$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
		while($pim_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 		=> gm($pim_lang->f('language')),
			'do'												=> 'maintenance-settings',
			'xget'											=> 'labels',
			'label_language_id'	     		=> $pim_lang->f('lang_id'),
			'label_custom_language_id'	=> '',
			));
		}
		$pim_custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
		while($pim_custom_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 				=> $pim_custom_lang->f('language'),
			'do'														=> 'maintenance-settings',
			'xget'													=> 'labels',
			'label_custom_language_id'	    => $pim_custom_lang->f('lang_id'),
			'label_language_id'	     				=> '',
			));
		}

		return json_out($data, $showin,$exit);
	}


	function get_namingConv($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'namingConv'=>array());
		$const = array();
		$db->query("SELECT value, constant_name FROM settings WHERE constant_name ='ACCOUNT_SERVICE_START' OR constant_name ='ACCOUNT_SERVICE_DIGIT_NR' ");
		while ($db->move_next()) {
			$const[$db->f('constant_name')] = $db->f('value');
		}

		$data['namingConv']['EXAMPLE_SRV'] 									= $const['ACCOUNT_SERVICE_START'].str_pad(1,$const['ACCOUNT_SERVICE_DIGIT_NR'],"0",STR_PAD_LEFT);
		$data['namingConv']['do']														= 'maintenance-settings-maintenance-intervention_naming';
		$data['namingConv']['xget']													= 'namingConv';
		$data['namingConv']['ACCOUNT_SERVICE_START'] 				= $const['ACCOUNT_SERVICE_START'];
		$data['namingConv']['ACCOUNT_SERVICE_DIGIT_NR']			= $const['ACCOUNT_SERVICE_DIGIT_NR'];

		return json_out($data, $showin,$exit);
	}

	function get_labels($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$const = array();


		$table = 'label_language_int';

		if($in['label_language_id'])
		{
			$filter = "WHERE label_language_id='".$in['label_language_id']."'";
			$id = $in['label_language_id'];
		}elseif($in['label_custom_language_id'])
		{
			$filter = "WHERE lang_code='".$in['label_custom_language_id']."'";
			$id = $in['label_custom_language_id'];
		}

		$db->query("SELECT * FROM $table $filter");

		$data=array(
			'intervention'				=> $db->f('intervention'),
			'serial_number' 			=> $db->f('serial_number'),
			'date' 								=> $db->f('date'),
			'phone'								=> $db->f('phone'),
			'fax' 								=> $db->f('fax'),
			'email' 							=> $db->f('email'),
			'url' 								=> $db->f('url'),
			'report' 							=> $db->f('report'),
			'staff' 							=> $db->f('staff'),
			'start_time'					=> $db->f('start_time'),
			'end_time' 						=> $db->f('end_time'),
			'break' 							=> $db->f('break'),
			'total' 							=> $db->f('total'),
			'iname' 							=> $db->f('iname'),
			'expenses' 						=> $db->f('expenses'),
			'task' 								=> $db->f('task'),
			'done' 								=> $db->f('done'),
			'purchase' 						=> $db->f('purchase'),
			'quantity'	  				=> $db->f('quantity'),
			'price' 							=> $db->f('price'),
			'delivered'					  => $db->f('delivered'),
			'hrate'						 		=> $db->f('hrate'),
			'tamount'						 	=> $db->f('tamount'),
			'allpricenovat'			 	=> $db->f('allpricenovat'),
			'to_be_delivered'			 	=> $db->f('to_be_delivered'),
			'label_language_id'		=> $id
		);

		return json_out($data, $showin,$exit);
	}

	function get_emailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$is_normal = true;
		if($code=='nl'){
                $code='du';
            }
		// $in['lang_code'] = $code;
		$name = 'servicemess';
$text = '-----------------------------------
Dear [!CUSTOMER!],
-----------------------------------
We have finished our intervention. In attachment
you will find more information on our intervention [!SERIAL_NUMBER!], executed on
[!INTERVENTION_DATE!].

You can also check your intervention report online, using this [!WEB_LINK_2!].
Of course, any feedback from your part is very welcome!

Thank you for your confidence,

[!ACCOUNT_COMPANY!]
-----------------------------------';
		$subject = 'Intervention #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
		$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        		=> $subject,
				'text'           		=> $text,
				'language_dd' 			=> build_language_dd_new($in['languages']),
				'active_tabb'			=> $in['tabb'],
				'CHECKED'				=> '',
				'use_html'				=> true,
				'translate_cls'			=> 'form-language-'.$code,
				'languages'				=> $in['languages'],
				'do'					=> 'quote-settings-quote-default_message',
				'xget'					=> 'emailMessage',
				'name'					=> $name,
				'lang_code'				=> $code,
				'calendar_available'	=> $db->field("SELECT value FROM settings WHERE constant_name='MAINTENANCE_ATTACH_CALENDAR' ")? true:false,
			);
		}else{
			$data['message']= array(
				'subject'        	=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           	=> $sys_message_invmess->f('use_html') == 1 ? $sys_message_invmess->f('html_content') : $sys_message_invmess->f('text'),
				'language_dd' 		=> build_language_dd_new($in['languages']),
				'active_tabb'		=> $in['tabb'],
				'CHECKED'			=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				//'use_html'		=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'use_html'			=> true,
				'translate_cls' 	=> 'form-language-'.$code,
				'languages'			=> $in['languages'],
				'do'				=> 'maintenance-settings-maintenance-default_message',
				'xget'				=> 'emailMessage',
				'name'				=> $name,
				'lang_code'			=> $code,
				'calendar_available'	=> $db->field("SELECT value FROM settings WHERE constant_name='MAINTENANCE_ATTACH_CALENDAR' ")? true:false,
			);
		}

		return json_out($data, $showin,$exit);
	}

	function get_settings($in,$showin=true,$exit=true){
		$db = new sqldb();
		$rate = $db->field("SELECT value FROM settings WHERE constant_name='MAINTENANCE_RATE'");
		$data = array('settings'=>array(
			'maintenance_is_billable'		=> $db->field("SELECT value FROM settings WHERE constant_name='MAINTENANCE_IS_BILLABLE'") == '1' ? true : false,
			'maintenance_rate'				=> is_numeric($rate) ? display_number($rate) : display_number(0),
			'use_stock_reservation'			=> $db->field("SELECT value FROM settings WHERE constant_name='USE_STOCK_RESERVATION'") == '1' ? true : false,
			'do'							=> 'maintenance-settings-maintenance-settings',
			'xget'							=> 'settings',
			));

		return json_out($data, $showin,$exit);
	}

	function get_expenses($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('expenses'=>array());
		$groups = $db->query("SELECT * FROM expense");

		while ($groups->next()) {
			$u_price = $groups->f('unit_price');
			array_push($data['expenses'],array(
				'expense_id'	=> $groups->f('expense_id'),
				"name"				=> $groups->f('name'),
				"price"				=> empty($u_price) ? '' : $u_price.' / '.$groups->f('unit'),
				'u_price'			=> display_number($groups->f('unit_price')),
				'unit'				=> $groups->f('unit'),
				'hide_delete'	=> $groups->f('is_default') == 1 ? false : true,
			));
		}

		return json_out($data, $showin,$exit);
	}

	function get_weblink($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('weblink'=>array(
			'use_maintenance_web_link' 		=> $db->field("SELECT value FROM settings WHERE constant_name='USE_MAINTENANCE_WEB_LINK' ") == '1' ? true : false,
			'allow_maintenance_comments' 	=> $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_MAINTENANCE_COMMENTS' ") == '1' ? true : false,
			'web_maintenance_include_pdf' => $db->field("SELECT value FROM settings WHERE constant_name='WEB_MAINTENANCE_INCLUDE_PDF' ") == '1' ? true : false,
			'stars_comments' 							=> $db->field("SELECT value FROM settings WHERE constant_name='STARS_COMMENTS' "),
			'allow_stars_rating'		 			=> $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STARS_RATING' ") == '1' ? true : false,
			'do'													=> 'maintenance-settings-maintenance-changeWL',
			'xget'												=> 'weblink',
			));

		return json_out($data, $showin,$exit);
	}

	function get_defaultEmail($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'email_default'=>array());

		$email = $db->query("SELECT * FROM default_data WHERE type='intervention_email' ");
		$mail_type1 = $db->field("SELECT value FROM default_data WHERE  type = 'intervention_email_type' ");
		$default_email_bcc = $db->query("SELECT * FROM default_data WHERE type = 'bcc_intervention_email' ");

		$data['email_default'] = array(
			'default_name'	=> $email->gf('default_name'),
			'email_value'	=> $email->gf('value'),
			'bcc_email'		=> $default_email_bcc->gf('value'),
			'mail_type_1'	=> $mail_type1,
			'do'			=> 'maintenance-settings-maintenance-update_default_email',
			'xget'			=> 'defaultEmail',
		);


		return json_out($data, $showin,$exit);
	}

	$result = array(
		'PDFlayout'			=> get_PDFlayout($in,true,false),
		'namingConv'		=> get_namingConv($in,true,false),
	);
json_out($result);
?>