<?php

 if(!defined('BASEPATH')) exit('No direct script access allowed');


global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);

$dbu = new sqldb($db_config);

$l_r = ROW_PER_PAGE;
$l_r = 30;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$order_by =" ORDER BY servicing_support.rec_next_date ASC ";
$filter = " servicing_support.is_recurring='1' ";

if($in['customer_id']){
	$filter .=" AND customer_id='".$in['customer_id']."' ";
	$arguments .='&customer_id='.$in['customer_id'];
}
if(!$in['archived']){
	$filter.= " AND servicing_support.active=1 ";
}else{
	$filter.= " AND servicing_support.active=0 ";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}

//FILTER LIST

if($in['search']){
	$filter .= " AND (servicing_support.customer_name LIKE '%".$in['search']."%' OR servicing_support.subject LIKE '%".$in['search']."%' OR servicing_support.serial_number LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}

if($in['order_by']){
	$order = " ASC ";
	if($in['desc'] == '1' || $in['desc']=='true'){ //'true'
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
}
//$perm_admin = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
$perm_admin = $dbu->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
//$perm_manager = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
$perm_manager = $dbu->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
$perm_reg_user=in_array('13', perm::$allow_apps);
switch (true) {
	case $_SESSION['group'] == 'admin':
	case $perm_admin:
	case $perm_manager:
	case $perm_reg_user:
		$join = '';
		break;
	default:
		$join=" INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id ";
		$filter.=" AND servicing_support_users.user_id ='".$_SESSION['u_id']."' ";
		break;
}


$arguments = $arguments.$arguments_s;

$max_rows =  $db->field("SELECT count(DISTINCT servicing_support.service_id) FROM servicing_support
						".$join."
						WHERE ".$filter );

$intervention = $db->query("SELECT servicing_support.service_id,servicing_support.rec_next_date,servicing_support.rec_frequency,servicing_support.customer_name,servicing_support.contact_name,servicing_support.subject,servicing_support.serial_number,servicing_support.rating,servicing_support.accept,servicing_support.status FROM servicing_support
			".$join."
			WHERE ".$filter." ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
$ints = array();
foreach ($intervention as $key => $value) {
	switch($value['rec_frequency']){
		    case 1:  $frequency= gm('Weekly'); break;
		    case 2:  $frequency= gm('Monthly'); break;
		    case 3:  $frequency= gm('Quarterly'); break;
		    case 4:  $frequency= gm('Yearly'); break;
		    case 5:  $frequency= gm('Every').' '.$value['days'].' '.gm('Day'); break;
		    case 6:  $frequency= gm('Bi-Annual'); break;
		    case 7:  $frequency= gm('Once every 2 years'); break;
	}
	$line = array(
		'accept' => $value['accept'],
		'contact_name'=> $value['contact_name'],
		'customer_name'=> $value['customer_name'],
		'next_date'=> $value['rec_next_date'] ? date(ACCOUNT_DATE_FORMAT,$value['rec_next_date']) : '',
		'rating'=> $value['rating'],
		'serial_number'=> $value['serial_number'],
		'service_id'=> $value['service_id'],
		'status'=> $value['status'],
		'subject'=> $value['subject'],
		'archive_link'=> array('do'=>'maintenance-rec_services-maintenance-archiveService','service_id'=>$value['service_id']),
		'activate_link'=> array('do'=>'maintenance-rec_services-maintenance-activateService','service_id'=>$value['service_id']),
		'delete_link'=> array('do'=>'maintenance-rec_services-maintenance-deleteService','service_id'=>$value['service_id']),
		'confirm'			=> gm('Confirm'),
		'ok'				=> gm('Ok'),
		'cancel'			=> gm('Cancel'),
		'frequency'			=> $frequency,
		);	
	array_push($ints, $line);
}

$result = array('query'=>$ints,'max_rows'=>$max_rows);
$result['lr'] = $l_r;
json_out($result);
?>