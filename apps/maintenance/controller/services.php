<?php

 if(!defined('BASEPATH')) exit('No direct script access allowed');
if(DATABASE_NAME == 'c9fc01f1_5334_cdcb_0e4b168974f7'){
	include(__DIR__.'/services_new.php');
	exit;
}
if($in['reset_list']){
 	if(isset($_SESSION['tmp_add_to_pdf_int'])){
 		unset($_SESSION['tmp_add_to_pdf_int']);
 	}
 	if(isset($_SESSION['add_to_pdf_int'])){
 		unset($_SESSION['add_to_pdf_int']);
 	}
}

global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);

$dbu = new sqldb($db_config);

if(!empty($in['start_date_js'])){
    $in['start_date'] =strtotime($in['start_date_js']);
    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
    $in['stop_date'] =strtotime($in['stop_date_js']);
    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}

$l_r = ROW_PER_PAGE;
$l_r = 30;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$order_by =" ORDER BY servicing_support.serial_number DESC ";
$filter = " 1=1 AND servicing_support.is_recurring='0' ";

//All statuses
if($in['view'] == 5){
	//$filter = " 1=1 ";
	$filter = " 1=1 AND servicing_support.is_recurring='0'";
	$arguments.="&view=5";
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
}

if($in['customer_id']){
	$filter .=" AND customer_id='".$in['customer_id']."' ";
	$arguments .='&customer_id='.$in['customer_id'];
}
if(!$in['archived']){
	$filter.= " AND servicing_support.active=1 ";
}else{
	$filter.= " AND servicing_support.active=0 ";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}

//FILTER LIST

if($in['search']){
	$filter .= " AND (servicing_support.customer_name LIKE '%".$in['search']."%' OR servicing_support.subject LIKE '%".$in['search']."%' OR servicing_support.serial_number LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}

if(!isset($in['view'])){
	$in['view'] = 6;
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
	$filter.=" and (servicing_support.status = '1' OR servicing_support.status = '0' ) ";

}
if($in['view'] == 1){
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
	$filter.=" and servicing_support.status = '1' ";
	// $view_list->assign('selected_all','class="selected"');
	$arguments.="&view=1";
}
if($in['view'] == 0){
	$filter.=" and servicing_support.status = '0' ";
	// $arguments.="&view=0";
	// $view_list->assign('selected_draft','class="selected"');
}
if($in['view'] == 2){
	$filter.=" and servicing_support.status = '1' ";
	$arguments.="&view=2";
	// $view_list->assign('selected_proforma','class="selected"');
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
}
if($in['view'] == 3){
	$filter.=" and servicing_support.status = '2' AND servicing_support.accept=0 ";
	$arguments.="&view=3";
	// $view_list->assign('selected_credit','class="selected"');
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
}
if($in['view'] == 4){
	$filter.=" and servicing_support.status = '2' AND servicing_support.accept=1 ";
	$arguments.="&view=4";
	// $view_list->assign('selected_acc','class="selected"');
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
}
if($in['view'] == 6){
	$filter.=" and (servicing_support.status = '1' OR servicing_support.status = '0' ) ";
	$arguments.="&view=6";
	// $view_list->assign('selected_acc','class="selected"');
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
}

$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
if($in['order_by']){
	$order = " ASC ";
	if($in['desc'] == '1' || $in['desc']=='true'){
		$order = " DESC ";
	}
	if($in['order_by']=='manager' ){
		$filter_limit =' ';
    	$order_by ='';
    }else{
       $order_by =" ORDER BY ".$in['order_by']." ".$order;
       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
       $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
    }
}


//$perm_admin = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
$perm_admin = $dbu->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
//$perm_manager = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
$perm_manager = $dbu->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
$perm_reg_user=in_array('13', perm::$allow_apps);
$ONLY_IF_INT_USER=defined('ONLY_IF_INT_USER') && ONLY_IF_INT_USER == 1 ? true : false;
switch (true) {
	case $_SESSION['group'] == 'admin':
	case $perm_admin:
	case $perm_manager:
	case ($perm_reg_user && !$ONLY_IF_INT_USER):
		$join = '';
		break;
	default:
		$join=" INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id ";
		$filter.=" AND servicing_support_users.user_id ='".$_SESSION['u_id']."' ";
		break;
}

if(!empty($in['start_date']) && !empty($in['stop_date'])){
	    $filter.=" and servicing_support.planeddate BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	    $arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	}
	else if(!empty($in['start_date'])){
	    $filter.=" and cast(servicing_support.planeddate as signed) > ".$in['start_date']." ";
	    $arguments.="&start_date=".$in['start_date'];
	}
	else if(!empty($in['stop_date'])){
	    $filter.=" and cast(servicing_support.planeddate as signed) < ".$in['stop_date']." ";
	    $arguments.="&stop_date=".$in['stop_date'];
	}

if($in['manager_id']){
	$join=" INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id AND servicing_support_users.pr_m=1";
	$filter.=" AND servicing_support_users.user_id ='".$in['manager_id']."' ";
}
if($in['color_ball']){
	$filter.=" AND servicing_support.color_ball='".str_replace("#",'', $in['color_ball'])."' ";
}

$nav=array();
$arguments = $arguments.$arguments_s;

$max_rows_data=$db->query("SELECT count(*) AS total_records, servicing_support.service_id FROM servicing_support
						".$join."
						WHERE ".$filter." GROUP BY servicing_support.service_id ".$order_by);
$max_rows=$max_rows_data->records_count();

if(!$_SESSION['tmp_add_to_pdf_int'] || ($_SESSION['tmp_add_to_pdf_int'] && empty($_SESSION['tmp_add_to_pdf_int']))){
 	while($max_rows_data->next()){
 		$_SESSION['tmp_add_to_pdf_int'][$max_rows_data->f('service_id')]=1;
 		array_push($nav, (object)['service_id'=> $max_rows_data->f('service_id') ]);
 	}
}else{
	while($max_rows_data->next()){
 		array_push($nav, (object)['service_id'=> $max_rows_data->f('service_id') ]);
 	}
}
$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_to_pdf_int']){
 	if($max_rows>0 && count($_SESSION['add_to_pdf_int']) == $max_rows){
 		$all_pages_selected=true;
 	}else if(count($_SESSION['add_to_pdf_int'])){
 		$minimum_selected=true;
 	}
}

$color_ball_dd=build_color_ball_dd();

$intervention = $db->query("SELECT servicing_support.active,servicing_support.is_recurring,servicing_support.service_id,servicing_support.planeddate,servicing_support.customer_name,
	servicing_support.contact_name,servicing_support.subject,servicing_support.serial_number,servicing_support.rating,servicing_support.accept,servicing_support.status, servicing_support.trace_id, servicing_support.billed,customers.comp_phone AS account_phone,customer_contactsIds.phone AS contact_phone,customer_addresses.city,servicing_support.service_type,servicing_support.color_ball
	FROM servicing_support
			".$join."
			LEFT JOIN customers ON servicing_support.customer_id=customers.customer_id
			LEFT JOIN customer_contactsIds ON servicing_support.customer_id=customer_contactsIds.customer_id AND servicing_support.contact_id=customer_contactsIds.contact_id
			LEFT JOIN customer_addresses ON customer_addresses.address_id=(CASE WHEN servicing_support.same_address>'0' THEN servicing_support.same_address ELSE servicing_support.main_address_id END)
			WHERE ".$filter." ".$order_by.$filter_limit )->getAll();
$ints = array();
foreach ($intervention as $key => $value) {
	if($value['status']=='1'){
		$status = gm('Planned');
	}elseif($value['status']=='2'){
		if($value['is_recurring']=='0'&&$value['active']=='1'&&$value['accept']=='1'){
			$status = gm('Accepted');
		}else{
			$status = gm('Closed');
		}
	}else{
		$status = gm('Draft');
	}

	$is_from_recurring=0;
	$target_type = $db->field("SELECT target_type FROM  tracking WHERE  trace_id ='".$value['trace_id']."'");
	if($target_type=='5'){
		$origin = $db->query("SELECT * FROM  tracking_line WHERE  trace_id ='".$value['trace_id']."'");
		if($origin->next()){
			if($origin->f('origin_type')=='5'){
				$is_from_recurring = $db->field("SELECT is_recurring FROM servicing_support WHERE service_id ='".$origin->f('origin_id')."'");
			}
		}
	}

	$managers = $db->query("SELECT user_id FROM servicing_support_users WHERE service_id ='".$value['service_id']."' AND pr_m = '1' ")->getAll();
	$managers_list ='';
	foreach($managers as $k =>$v){
		$managers[$k]['name']=get_user_name($v['user_id']);
		$managers_list .= $managers[$k]['name'].', '; 
	}

	$managers_list = rtrim($managers_list,', ');

	$has_downpayment=$db->field("SELECT invoice_id FROM tbldownpayments WHERE service_id='".$value['service_id']."'");

    $deal_id="";
    $deal_stage="-";
    if($value['trace_id']){
        $deal_data=$db->query("SELECT tblopportunity.opportunity_id,tblopportunity_stage.name AS stage_name FROM tracking_line
			INNER JOIN tblopportunity ON tracking_line.origin_id=tblopportunity.opportunity_id
			INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
			WHERE tracking_line.trace_id='".$value['trace_id']."' AND tracking_line.origin_type='11' ")->getAll();
        if(!empty($deal_data)){
            $deal_id=$deal_data[0]['opportunity_id'];
            $deal_stage=$deal_data[0]['stage_name'];
        }
    }

	$line = array(
		'accept' => $value['accept'],
		'contact_name'=> $value['contact_name'],
		'customer_name'=> $value['customer_name'],
		'planeddate'=> $value['planeddate'] ? date(ACCOUNT_DATE_FORMAT,$value['planeddate']) : '',
		'rating'=> $value['rating'],
		'serial_number'=> $value['serial_number'],
		'id'=> $value['service_id'],
		'service_id'=> $value['service_id'],
		'status'=> $value['status'],
		'manager'=> $managers_list,
		'type_status'=> $status,
		'subject'=> $value['subject'],
		'archive_link'=> array('do'=>'maintenance-services-maintenance-archiveService','service_id'=>$value['service_id']),
		'activate_link'=> array('do'=>'maintenance-services-maintenance-activateService','service_id'=>$value['service_id']),
		'delete_link'=> array('do'=>'maintenance-services-maintenance-deleteService','service_id'=>$value['service_id']),
		'confirm'			=> gm('Confirm'),
		'ok'				=> gm('Ok'),
		'cancel'			=> gm('Cancel'),
		'is_from_recurring'	=> $is_from_recurring,
		'has_invoice'			=> $value['billed'] ? true : false,
		'invoiced'				=>'inv_ordGreenIcons' ,
		'invoiced_info'			=> gm('Invoiced'),
		'has_downpayment'    	=> $has_downpayment? true:false,
		'downpayment_title'     => gm('A downpayment has been invoiced from this intervention'),
		'account_phone'			=> $value['account_phone'],
		'contact_phone'			=> $value['contact_phone'],
		'city'					=> $value['city'],
		'check_add_to_product'	=> $in['exported'] ? false : ($_SESSION['add_to_pdf_int'][$value['service_id']] == 1 ? true : false),
		'invoicing_type'		=> $value['service_type'] == 2 ? gm('Not Billable') : ($value['service_type'] == 1 ? gm('Fixed price') : gm('Time & Expenses')),
		'color_code'			=> '#'.$value['color_ball'],
        'deal_stage' => $deal_stage,
		);

	array_push($ints, $line);
	

}


$result = array('query'=>$ints,'max_rows'=>$max_rows,'all_pages_selected'=> $in['exported'] ? false : $all_pages_selected,'minimum_selected'=> $in['exported'] ? false : $minimum_selected, 'nav'=>$nav);
$result['managers']=get_managers_dd($in);
$result['lr'] = $l_r;

if($in['order_by']=='manager' ){

    if($order ==' ASC '){
       $exo = array_sort($result['query'], $in['order_by'], SORT_ASC);    
    }else{
       $exo = array_sort($result['query'], $in['order_by'], SORT_DESC);
    }

    $exo = array_slice( $exo, $offset*$l_r, $l_r);
    $result['query']=array();
       foreach ($exo as $key => $value) {
           array_push($result['query'], $value);
       }
}

$result['color_ball_dd']=$color_ball_dd;

$result['columns']=array();
$cols_default=default_columns_dd(array('list'=>'interventions'));
$cols_order_dd=default_columns_order_dd(array('list'=>'interventions'));
$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='interventions' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
if(!count($cols_selected)){
	$i=1;
    $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'interventions'));
    foreach($cols_default as $key=>$value){
		if($key=='invoicing_type'){
        	continue;
        }
		$tmp_item=array(
			'id'				=> $i,
			'column_name'		=> $key,
			'name'				=> $value,
			'order_by'			=> $cols_order_dd[$key]
		);
        if ($default_selected_columns_dd[$key]) {
            array_push($result['columns'],$tmp_item);
        }
		$i++;
	}
}else{
	foreach($cols_selected as $key=>$value){
		$tmp_item=array(
			'id'				=> $value['id'],
			'column_name'		=> $value['column_name'],
			'name'				=> $cols_default[$value['column_name']],
			'order_by'			=> $cols_order_dd[$value['column_name']]
		);
		array_push($result['columns'],$tmp_item);
	}
}

json_out($result);

/*
while($db->move_next()){
	switch ($db->f('status')) {
		case '1':
			$status = gm('In progress');
			$img = 'green';
			break;
		case '2':
			$status = gm('Done');
			break;
		default:
			$status = gm('Draft');
			$img = 'gray';
			if($db->f('planeddate') != 0){
				$img = 'orange';
			}
			break;
	}

	$view_list->assign(array(
		'NAME'						=> $db->f('customer_name') ? $db->f('customer_name') : $db->f('contact_name'),
		'EDIT_LINK'					=> 'index.php?do=maintenance-services&service_id='.$db->f('service_id'),
		'id'						=> $db->f('service_id'),
		'u_name'					=> $db->f("user_name"),
		'planedd'					=> $db->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$db->f('planeddate')) : '',
		'subject'					=> $db->f("subject"),
		'number'					=> $db->f('serial_number'),
		'DELETE_LINK'				=> 'index.php?do=maintenance-service_list-maintenance-deleteService&service_id='.$db->f('service_id').$arguments,
		'ARCHIVE_LINK'				=> 'index.php?do=maintenance-service_list-maintenance-archiveService&service_id='.$db->f('service_id').$arguments,
		'ACTIVATE_LINK'				=> 'index.php?do=maintenance-service_list-maintenance-activateService&service_id='.$db->f('service_id').$arguments,
		'status'					=> $status,
		'accepted'					=> $db->f('accept') == 1 ? true : false,
		'status_customer'			=> $img,
		'see_icon'					=> $img,
	),'projects');


	for($j=1;$j<6;$j++){
		$view_list->assign(array(
			'bluestar' => $db->f('rating') >= $j ? 'bluestar' : '',
		),'stars');
		$view_list->loop('stars','projects');
	}

	if($db->f('active')==1){
		$view_list->assign(array(
			'VIEW_DELETE_LINK'     => 'hide',
			'VIEW_ARCHIVE_LINK'    => '',
		),'projects');
	}else{
		$view_list->assign(array(
      		'VIEW_ARCHIVE_LINK'   => 'hide',
      		'VIEW_DELETE_LINK'    => '',
	    ),'projects');
	}
	$view_list->loop('projects');
	$i++;
}/*
$start = $offset;
$end = ceil($max_rows/$l_r);
$link = '';
if($end<=5){
	//if there are less then 5 pages then we go about building a normal pagination
	for ($i = 0; $i < $end; $i++){
		$page = $i+1;
		$class = $page == $start+1 ? 'class="current"' : '';
		$link .= <<<HTML
		<li><a href="index.php?do={$in['do']}&offset={$i}{$arguments}" {$class}>{$page}</a></li>
HTML;
	}
}else{
	if($start == 0 || $start <3){
		for ($i = 0; $i < 5; $i++){
			$page = $i+1;
			$class = $page == $start+1 ? 'class="current"' : '';
			$link .= <<<HTML
			<li><a href="index.php?do={$in['do']}&offset={$i}{$arguments}" {$class}>{$page}</a></li>
HTML;
		}
	}elseif ($start+2 >= $end-1){
		//we are close to the end
		for ($i = $end-5; $i < $end; $i++){
			$page = $i+1;
			$class = $page == $start+1 ? 'class="current"' : '';
			$link .= <<<HTML
			<li><a href="index.php?do={$in['do']}&offset={$i}{$arguments}" {$class}>{$page}</a></li>
HTML;
		}
	}else{
		for ($i = $start-2; $i < $start; $i++){
			$page = $i+1;
			$link .= <<<HTML
			<li><a href="index.php?do={$in['do']}&offset={$i}{$arguments}" {$class}>{$page}</a></li>
HTML;
		}
		$page = $start+1;
		$class = $page == $start+1 ? 'class="current"' : '';
		$link .= <<<HTML
		<li><a href="index.php?do={$in['do']}&offset={$start}{$arguments}" {$class}>{$page}</a></li>
HTML;
		for ($i = $start+1; $i < $start+3; $i++){
			$page = $i+1;
			$link .= <<<HTML
			<li><a href="index.php?do={$in['do']}&offset={$i}{$arguments}" >{$page}</a></li>
HTML;
		}
	}
}

if($max_rows == 0){
	$view_list->assign('HIDE_TABLE','hide');
	$view_list->assign("MSG_NOTICE", '<div class="info info_big">'.gm('No data to display').'</div>');
}

if($max_rows <= $l_r){
	$view_list->assign('HIDE_NAV','hide');

}else{
	$view_list->assign('HIDE_NAV','');
}

$view_list->assign(array(
	'PAGG' => $link,
));

if($offset > 0)
{
     $view_list->assign('BACKLINK',"index.php?do=".$in['do']."&offset=".($offset-1).$arguments);
     $view_list->assign('FIRST_LINK',"index.php?do=".$in['do'].$arguments);
}
else
{
     $view_list->assign('BACKLINK','#');
     $view_list->assign('FIRST_LINK','#');
}
if($offset < $end-1)
{
     $view_list->assign('NEXTLINK',"index.php?do=".$in['do']."&offset=".($offset+1).$arguments);
}
else
{
     $view_list->assign('NEXTLINK','#');
}
$view_list->assign('LAST_LINK',"index.php?do=".$in['do']."&offset=".($end-1).$arguments);
if($in['view'])
{
	$add_view='&view='.$in['view'];
}else{
	$add_view='';
}

if(!$in['archived']){
	$view_list->assign(array(
		'VIEW_ACTIVE'     	=> 'hide',
		'ARCHIVED_LIST'	  	=> get_link('index.php?do=maintenance-service_list&archived=1').$add_view,
		'ARCHIVE'		  	=> '',
	));
}else{
	$view_list->assign(array(
		'VIEW_ARCHIVED'   	=> 'hide',
		'ACTIVE_LIST'	  	=> get_link('index.php?do=maintenance-service_list').$add_view,
		'ARCHIVE'		  	=> '&archived=1',
	));
}
$args_v = preg_replace('/&view=([0-9]+)/','',$arguments);
$view_list->assign(array(
	'VIEW_GO'					=> $_SESSION['access_level'] == 1 ? '' : 'hide',
	'OPEN'						=> $in['view'] ? '' : 'selected',
	'CLOSED'					=> $in['view'] ? 'selected' : '',
	'ARGS'						=> $args_v,
	'ARCHIVED'					=> $in['archived'],
	'is_pagination' 			=> $max_rows > $l_r ? true : false,
	'is_data'					=> $i ? true : false,
	'total_number'				=> $max_rows,
));

$_SESSION['filters'] = $arguments.$arguments_o;

*/

// return $view->fetch();