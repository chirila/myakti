<?php
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();
include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');

$q = strtolower($in["term"]);
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);
$filter .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";

if($in['current_id']){
	$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
}

if($in['customer_id']){
	$filter .= " AND customer_contacts.customer_id='".$in['customer_id']."'";
}
//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
$items = array();
$db= new sqldb();
$defRates = defined('MAINTENANCE_RATE') ? MAINTENANCE_RATE : 0;
$rates = array();
$allrates = $db->query("SELECT * FROM default_data WHERE `type`='maintenance_rate' ");
while($allrates->next()){
	$rates[$allrates->f('default_name')] = $allrates->f('value');
}

$db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, customers.name, customers.currency_id,customers.internal_language,customer_contacts.email
			FROM customer_contacts
			LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
			WHERE customer_contacts.active=1 $filter ORDER BY lastname ");

while($db->move_next()){
	$r = $defRates;
	$name = $db->f('firstname').' '.$db->f('lastname').' > '.$db->f('name');
	if($in['only_contact_name']){
		$name = $db->f('firstname').' '.$db->f('lastname');
	}
  	$items[$name]=$db->f('contact_id');
  	if ($db->f('customer_id')){
  		$price_category[$name] = $db->f('cat_id');
  	}
  	else{
  		$price_category[$name] = "1";
  	}
  	if(array_key_exists($db->f('customer_id'), $rates)){
  		$r = $rates[$db->f('customer_id')];
  	}
  	$customer_id[$name] = $db->f('customer_id');
  	$customer_name[$name] = $db->f('name');
  	$c_name[$name] = $db->f('firstname').' '.$db->f('lastname');
  	$currency[$name] = $db->f('currency_id');
  	$lang[$name] = $db->f('internal_language');
  	$email[$name] = $db->f('email');
  	$rate[$name] = $r;
}
$result = array();
foreach ($items as $key=>$value) {
	$price_category_id=$price_category[$key];
	if($q){
		if (strpos(strtolower($key), $q) !== false) {
			array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key),'email'=>$email[$key], "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)),"price_category_id"=>$price_category_id, 'customer_id' => $customer_id[$key], 'c_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$customer_name[$key]), 'currency_id' => $currency[$key], "lang_id" => $lang[$key], 'contact_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$c_name[$key]), 'rate' => $rate[$key] ));
		}
	}else{
		array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key),'email'=>$email[$key], "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)),"price_category_id"=>$price_category_id, 'customer_id' => $customer_id[$key], 'c_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$customer_name[$key]), 'currency_id' => $currency[$key], "lang_id" => $lang[$key], 'contact_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$c_name[$key]), 'rate' => $rate[$key] ));
	}
	if (count($result) > 100)
	break;
}
json_out($result);exit();

?>