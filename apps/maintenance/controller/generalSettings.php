<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_settings($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$s = $db->query("SELECT * FROM settings");
		while ($s->next()) {
			$data[$s->f('constant_name')] =  $s->f("type") == 1 ? $s->f('value') : $s->f('long_value');
		}
		return json_out($data, $showin,$exit);
	}

	function get_dispAddress($in,$showin=true,$exit = true){
		$db = new sqldb();
		$filter = "1=1 ";

		$data = $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address WHERE $filter")->getAll();
		foreach ($data as $key => $value) {
			$data[$key]['country'] = get_country_name($value['country_id']);
		}
		return json_out($data, $showin,$exit);

	}

	$result = array(
		'settings'				=> get_settings($in,true,false),
		// 'dispAddresss'		=> get_dispAddress($in,true,false),
	);
json_out($result);
