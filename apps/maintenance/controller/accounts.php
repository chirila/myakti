<?php

error_reporting(0);
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();

$is_admin = strtolower($in["is_admin"]);


include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');

ini_set('display_errors', 1);
  ini_set('error_reporting', 1);
  // error_reporting(E_ALL);
  error_reporting(E_ALL ^ E_NOTICE);


$q = strtolower($in["term"]);
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);
$filter =" AND is_admin='0' ";

if($q){
	$filter .=" AND name LIKE '%".addslashes($q)."%'";
}

if($is_admin){
	$filter =" AND is_admin='".$is_admin."' ";
}

if($in['current_id']){
	$filter .= " AND customer_id !='".$in['current_id']."'";
}
//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
	$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
$db= new sqldb();

$rates = array();
$allrates = $db->query("SELECT * FROM default_data WHERE `type`='maintenance_rate' ")->getAll();
foreach ($allrates as $key => $value) {
	$rates[$value['default_name']] = $value['value'];
}
$allrates = null;
$cust = $db->query("SELECT name,customer_id,active,our_reference,currency_id,internal_language FROM customers WHERE active=1 $filter ORDER BY name ")->getAll();

$defRates = defined('MAINTENANCE_RATE') ? MAINTENANCE_RATE : 0;
# foreach e 2x mai rapid decat while-ul
$result = array();
/*$accounts = array();
$ref = array();
$currency = array();
$lang = array();
$rate = array();*/
foreach ($cust as $key => $value) {
	$r = $defRates;
	$cname = trim($value['name']);
	if(array_key_exists($value['customer_id'], $rates)){
		$r = $rates[$value['customer_id']];
	}
	/*$accounts[$cname]=$value['customer_id'];
	$ref[$cname]=$value['our_reference'];
	$currency[$cname]=$value['currency_id'];
	$lang[$cname] = $value['internal_language'];
	$rate[$cname] = $r;*/
	$result[]=array(
		"id"=>$value['customer_id'],
		"label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
		"value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
		"ref" => $value['our_reference'],
		"currency_id" => $value['currency_id'],
		"lang_id" => $value['internal_language'],
		"rate" => $r
		// )
	);
	if (count($result) > 100)
		break;
}
json_out($result);
/*while($cust->next()){
	$r = $defRates;
	/*if(array_key_exists($cust->f('customer_id'), $rates)){
  		$r = $rates[$cust->f('customer_id')];
  	}*\/
  	$accounts[trim($cust->f('name'))]=$cust->f('customer_id');
  	$ref[trim($cust->f('name'))]=$cust->f('our_reference');
  	$currency[trim($cust->f('name'))]=$cust->f('currency_id');
  	$lang[trim($cust->f('name'))] = $cust->f('internal_language');
  	$rate[trim($cust->f('name'))] = $r;
}*/

/*
if($accounts){
	foreach ($accounts as $key=>$value) {
		if($q){
			if (strpos(strtolower($key), $q) !== false) {
				array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key), "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)), "ref" => $ref[$key], "currency_id" => $currency[$key], "lang_id" => $lang[$key], "rate" => $rate[$key] ));
			}
		}else{
			array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key), "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)), "ref" => $ref[$key], "currency_id" => $currency[$key], "lang_id" => $lang[$key], "rate" => $rate[$key] ));
		}
		/*if (count($result) > 100)
		break;* /
	}
	// echo array_to_json($result,true);
	json_out($result);
}*/

if($is_admin){
		exit();
}
?>