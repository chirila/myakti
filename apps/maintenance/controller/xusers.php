<?php
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();

include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');

global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);

$db = new sqldb($db_config);
$q = strtolower($in["term"]);

$filter = " AND users.credentials like '%13%' ";

if($q){
	$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
}

$db->query("SELECT first_name,last_name,users.user_id,h_rate FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");
while($db->move_next()){
  $users[utf8_encode($db->f('first_name').' '.$db->f('last_name'))]=$db->f('user_id');
  $u[utf8_encode($db->f('first_name').' '.$db->f('last_name'))] = $db->f('h_rate');
}

$result = array();
if($users){
	foreach ($users as $key=>$value) {
		if($q){
			if (strpos(strtolower($key), $q) !== false) {
				array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "h_rate" => $u[$key]));
			}
		}else{
			array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "h_rate" => $u[$key]));
		}
		if (count($result) > 2000)
		break;
	}
	// echo array_to_json($result);
	json_out($result);
}else {
	array_push($result, array("id"=>'0', "label"=>'No matches', "value" => 'No matches', "h_rate" => 'No matches'));
	// echo array_to_json($result);
	json_out($result);
}

error_reporting(0);
?>