<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_intNotSeen($in,$showin=true,$exit=true){
		global $database_config;
		$db = new sqldb();
		$data = array( 'intNotSeen'=>array(),'select'=>array(),'user_id'=>0,'planning'=>array(),'planningAllDay'=>array());
		if(!$in['user_id'] )
		{
			$in['user_id'] = $_SESSION['u_id'];
		}
		$services = $db->query("SELECT * FROM customer_meetings
			INNER JOIN servicing_support ON customer_meetings.service_id = servicing_support.service_id
			WHERE notify = '1' AND customer_meetings.user_id ='".$_SESSION['u_id']."' AND servicing_support.active='1' ");
		while ($services->next()) {

			array_push($data['intNotSeen'], array(
				'date'						=> $services->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$services->f('planeddate')) : '',
				'contact_name'		=> $services->f('customer_name') ? $services->f('customer_name') : $services->f('contact_name'),
				'subject'					=> $services->f('serial_number'),
				'service_id'			=> $services->f('service_id'),
				'cid'							=> $services->f('customer_meeting_id'),
			));
		}
		$select = build_user_dd_service($in['user_id'], $_SESSION['access_level']);
		$team_members=$db->query("SELECT user_id FROM servicing_team_users")->getAll();
		$new_select=array();
		foreach($select as $key=>$value){
			foreach($team_members as $key1=>$value1){
				if($value['id']==$value1['user_id']){
					array_push($new_select,$value);
				}
			}
		}
		$data['select'] = $new_select;
		$data['user_id'] = $in['user_id'];

		##################### planning #########################
		/*if($in['start_date']){
			$now				= strtotime($in['start_date']);
			$today_of_week		= date("N", strtotime($in['start_date']));
			$month        		= date('n',strtotime($in['start_date']));
			$year         		= date('Y',strtotime($in['start_date']));
			$week_start 		= mktime(0,0,0,$month,(date('j',strtotime($in['start_date']))-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;
		}else	if(!($in['start'])){
			$now 				= time();
			$today_of_week 		= date("N", $now);
			$month        		= date('n');
			$year         		= date('Y');
			$week_start 		= mktime(0,0,0,$month,(date('j')-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;
		} else {
			$now 				= strtotime($in['start'].' week');
			// var_dump($now);
			$today_of_week 		= date("N", $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			$week_start 			= mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
			$week_end 				= $week_start + 604799;
		}*/

		if($in['start_date']){
			$data['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		$week_end 			= $week_start + 604799;

		$in['week_start_serv'] = $week_start;
		$in['week_end_serv'] = $week_end;

		$days = array();
		$un_array=array();
		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			array_push($days, $i);
			array_push($un_array, array(
				'day_name'    	=> gm(date('D',$i)),
				'date_month'  	=> date('d',$i).' '.gm(date('M',$i)),
			));
		}

		$height = 31;
		$left = 135;
		//$colors = array('78c2c6','f1a93d','528dc0','c5d666','bba6ce','ec6040');
		$colors = array('ea9c62','41ad65','8e9599');

		$j=0;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);

		//$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
		//$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
			// case $is_service_manager:
				$filter = '';
				break;
			default:
				$filter = " AND servicing_support.service_id IN (SELECT service_id FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' GROUP BY service_id )";
				break;
		}

		$sheet = $db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$in['week_start_serv']."' AND '".$in['week_end_serv']."'
												 AND servicing_support_users.user_id='".$in['user_id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ");

		$is_top = false;
		// $padding_fix = 0;
		$some_array = array();
		$same_day =array();
		$same_day_parsed = array();
		while ($sheet->next()) {
			if((floor($sheet->f('startdate'))-1)*$height < 0){
				$is_top = true;
				// $padding_fix = 20;
			}
			$new_date = mktime(0,0,0,date('m',$sheet->f('planeddate')), date('d',$sheet->f('planeddate')), date('Y',$sheet->f('planeddate')));
			// if($sheet->f('startdate') != 0){
				if(!array_key_exists($new_date, $some_array)){
					$some_array[$new_date] = array();
				}
				array_push($some_array[$new_date],array(intval($sheet->f('startdate')),intval($sheet->f('duration'))) );
				/*if(array_key_exists($new_date, $same_day)){
					$same_day[$new_date] ++;
				}else{
					$same_day[$new_date]=1;
				}*/
			// }
			// $same_day_parsed[$new_date] = 0;
		}

		// var_dump($some_array);

		$last_try = array();
		$first_hour = 24;
		foreach ($some_array as $key => $value) {
			$last_try[$key] = array();
			if(count($value) > 1){
			 	foreach ($value as $v) {
			 		if($v[0] != 0 && $v[0] < $first_hour){
			 			$first_hour = $v[0];
			 		}
			 		$increment = 0;
			 		$same_hour = 0;
			 		foreach ($value as $v1) {
			 			$e = abs($v[0]-$v1[0]);
			 			if($e <= 1){
			 				$increment++;
			 			}
			 			if($e == 0){
			 				$same_hour ++;
			 			}
			 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
			 				$same_hour ++;
			 			}
			 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
			 				$same_hour ++;
			 			}
			 		}
			 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
			 	}
			}
		}
		if($first_hour == 24){
			$first_hour = 9;
		}
		// var_dump($last_try);
		// console::log($some_array);

		$sheet->reset();
		while ($sheet->next()) {
			$new_date = mktime(0,0,0,date('m',$sheet->f('planeddate')), date('d',$sheet->f('planeddate')), date('Y',$sheet->f('planeddate')));
			$k = array_search($new_date, $days);
			$width_compensation = 0;

			$width = 0;
			if(@array_key_exists(intval($sheet->f('startdate')), $last_try[$new_date])){
				$width = $last_try[$new_date][intval($sheet->f('startdate'))]['same_hour'];
			}
			if($sheet->f('startdate')){
				array_push($data['planning'], array(
					'c_name'						=> $sheet->f('customer_name') ? $sheet->f('customer_name') : $sheet->f('contact_name'),
					'DATE'							=> date(ACCOUNT_DATE_FORMAT,$sheet->f('planeddate')),
					'serial_number'			=> $sheet->f('serial_number'),
					'hours'							=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')),
					'id'								=> $sheet->f('id'),
					'service_id'				=> $sheet->f('service_id'),
					'tmsmp'							=> $sheet->f('planeddate'),
					'start_time'				=> $sheet->f('startdate')? number_as_hour($sheet->f('startdate')) : '',
					'end_time'					=> $sheet->f('duration')>0 ? number_as_hour($sheet->f('startdate')+$sheet->f('duration')) : '',
					'phour'							=> 'phour_'.floor($sheet->f('startdate')),
					'pduration'					=> floor($sheet->f('duration')) > 0 ? 'pduration_'.ceil($sheet->f('duration')) : '',
					'pday'							=> 'pday_'.date('N',$new_date),
					//'colors'						=> 'div_'.$colors[$j],
					'colors'					=> !$sheet->f('status') ? 'div_'.$colors[0] : ($sheet->f('status')==1 ? 'div_'.$colors[1] : 'div_'.$colors[2]),
					'top_class'					=> (floor($sheet->f('startdate'))-1)*$height < 0 ? "top_class" : '',
					'pwidth'						=> $width ? 'pwidth_'.$width : '',
					'subject'						=> $sheet->f('subject'),
					'status'					=> $sheet->f('status'),
					'color_ball'				=> '#'.$sheet->f('color_ball'),
				));
			}else{
				array_push($data['planningAllDay'], array(
					'c_name'						=> $sheet->f('customer_name') ? $sheet->f('customer_name') : $sheet->f('contact_name'),
					'DATE'							=> date(ACCOUNT_DATE_FORMAT,$sheet->f('planeddate')),
					'serial_number'			=> $sheet->f('serial_number'),
					'hours'							=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')),
					'id'								=> $sheet->f('id'),
					'service_id'				=> $sheet->f('service_id'),
					'tmsmp'							=> $sheet->f('planeddate'),
					'start_time'				=> '',
					'end_time'					=> '',
					'phour'							=> 'phour_'.floor($sheet->f('startdate')),
					'pduration'					=> floor($sheet->f('duration')) > 0 ? 'pduration_'.floor($sheet->f('duration')) : '',
					'pday'							=> 'pday_'.date('N',$new_date),
					//'colors'						=> 'div_'.$colors[$j],
					'colors'					=> !$sheet->f('status') ? 'div_'.$colors[0] : ($sheet->f('status')==1 ? 'div_'.$colors[1] : 'div_'.$colors[2]),
					'top_class'					=> (floor($sheet->f('startdate'))-1)*$height < 0 ? "top_class" : '',
					'pwidth'						=> $width ? 'pwidth_'.$width : '',
					'subject'						=> $sheet->f('subject'),
					'status'					=> $sheet->f('status'),
					'color_ball'				=> '#'.$sheet->f('color_ball'),
				));
			}

			$j++;
			if($j>5){
				$j=0;
			}
		}

		$data['is_top']	= $is_top;
		$data['is_top_class'] = $is_top ? 'is_top_class' : '';
		$data['week_txt']= date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end);
		$data['query'] = $week_start;
		$data['week_days'] = $un_array;
		$data['first_hour'] = $first_hour-2;
		##################### planning #########################


		return json_out($data, $showin,$exit);
	}

	function get_intervention($in,$showin=true,$exit=true){
		$db = new sqldb();
		// $data = array( );
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$dbu = new sqldb($db_config);
		$l_r = ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$order_by =" ORDER BY servicing_support.planeddate ASC ";
		$filter = ' 1=1 ';

		if($in['customer_id']){
			$filter .=" AND customer_id='".$in['customer_id']."' ";
			$arguments .='&customer_id='.$in['customer_id'];
		}
		if(!$in['archived']){
			$filter.= " AND servicing_support.active=1 ";
		}else{
			$filter.= " AND servicing_support.active=0 ";
			$arguments.="&archived=".$in['archived'];
			$arguments_a = "&archived=".$in['archived'];
		}
		//FILTER LIST
		if($in['search']){
			$filter .= " AND (servicing_support.customer_name LIKE '%".$in['search']."%' OR servicing_support.subject LIKE '%".$in['search']."%' OR servicing_support.serial_number LIKE '%".$in['search']."%' )";
			$arguments_s.="&search=".$in['search'];
		}

		if(!isset($in['view'])){
			$in['view'] = 1;
		}
		if($in['view'] == 1){
			$order_by =" ORDER BY servicing_support.serial_number DESC ";
			$filter.=" and ((servicing_support.status = '0' AND servicing_support.planeddate<>'0' ) or servicing_support.status = '1') ";
			$arguments.="&view=1";
		}
		if($in['view'] == 2){
			$filter.=" and servicing_support.status = '1' ";
		}
		if($in['view'] == 3){
			$filter.=" and servicing_support.status = '2' ";
		}
		if($in['view'] == 4){
			$filter.=" and servicing_support.status = '2' AND servicing_support.accept=1 ";
		}

		if($in['order_by']){
			$order = " ASC ";
			if($in['desc']){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		}
		//$perm_admin = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_admin = $dbu->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
		//$perm_manager = $dbu->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_manager = $dbu->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
		$perm_reg_user=in_array('13', perm::$allow_apps);
		$ONLY_IF_INT_USER=defined('ONLY_IF_INT_USER') && ONLY_IF_INT_USER == 1 ? true : false;
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case ($perm_reg_user && !$ONLY_IF_INT_USER):
				$join = '';
				break;
			default:
				$join=" INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id ";
				$filter.=" AND servicing_support_users.user_id ='".$_SESSION['u_id']."' ";
				break;
		}

		$arguments = $arguments.$arguments_s;

		$max_rows =  $db->field("SELECT count(DISTINCT servicing_support.service_id) FROM servicing_support
								".$join."
								WHERE ".$filter );

		$db->query("SELECT * FROM servicing_support
					".$join."
					WHERE ".$filter." ".$order_by." LIMIT ".$offset*$l_r.",".$l_r );
		$intervention = array();
		while($db->move_next() && $i<$l_r){

			array_push($intervention, array(
				'NAME'							=> $db->f('customer_name') ? $db->f('customer_name') : $db->f('contact_name'),
				'EDIT_LINK'					=> 'index.php?do=maintenance-services&service_id='.$db->f('service_id').'&from_misc=1',
				'service_id'				=> $db->f('service_id'),
				'u_name'						=> $db->f("user_name"),
				'planedd'						=> $db->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$db->f('planeddate')) : '',
				'subject'						=> $db->f("subject"),
				'number'						=> $db->f('serial_number'),
				'DELETE_LINK'				=> 'index.php?do=maintenance-service_list-maintenance-deleteService&service_id='.$db->f('service_id').$arguments,
				'ARCHIVE_LINK'			=> 'index.php?do=maintenance-service_list-maintenance-archiveService&service_id='.$db->f('service_id').$arguments,
				'ACTIVATE_LINK'			=> 'index.php?do=maintenance-service_list-maintenance-activateService&service_id='.$db->f('service_id').$arguments,
				// 'status'						=> $status,
				'status'						=> $db->f('status'),
				'accepted'					=> $db->f('accept') == 1 ? true : false,
				'status_customer'		=> $img,
				'see_icon'					=> $img,
				'rating'						=> $db->f('rating'),
			));

		}
		$data = array('query'=>$intervention,'max_rows'=>$max_rows);
		$data['lr'] = $l_r;
		return json_out($data, $showin,$exit);
	}

	function get_Month($in,$showin=true,$exit=true){
		global $database_config;
		$db = new sqldb();

		if(!$in['user_id'] )
		{
			$in['user_id'] = $_SESSION['u_id'];
		}

		$data=array('week'=>array());
		$select = build_user_dd_service($in['user_id'], $_SESSION['access_level']);
		$team_members=$db->query("SELECT user_id FROM servicing_team_users")->getAll();
		$new_select=array();
		foreach($select as $key=>$value){
			foreach($team_members as $key1=>$value1){
				if($value['id']==$value1['user_id']){
					array_push($new_select,$value);
				}
			}
		}
		$data['select'] = $new_select;
		$data['user_id'] = $in['user_id'];


		if($in['start_date']){
			$data['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$month = date('n',$now);
		$year = date('Y',$now);
		$data['month_txt']= date('F',$now);
		$month_t = mktime(0,0,0,$month,1,$year);

		$month_days = date('t',$month_t);
		$month_first_day = date('N',$month_t);

		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);

		//$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
		//$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
		/*$is_service_manager = $db->field("SELECT pr_m FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' AND service_id='".$in['service_id']."' ");*/
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
			// case $is_service_manager:
				$filter = '';
				break;
			default:
				$filter = " AND servicing_support.service_id IN (SELECT service_id FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' GROUP BY service_id )";
				break;
		}

		$i = 1;
		$day = 1;
		$loopweek = true;
		$q=0;
		$w=0;		
		while ($loopweek) {
			$k = 1;
			$day_final=array();
			while ($k <= 7) {
				$text = $day;
				$bg = 'planning_empty';
				$serv_data=array();
				if($i == 1 && $k < $month_first_day){
					$text = date('j',mktime(0,0,0,$month,$day-($month_first_day-$k),$year));
					$s_st=mktime(0,0,0,$month,$day-($month_first_day-$k),$year);
					$s_end=mktime(23,59,59,$month,$day-($month_first_day-$k),$year);				
				}
				if($day > $month_days){
					$text = date('j',mktime(0,0,0,$month,$day,$year));
					$s_st=mktime(0,0,0,$month,$day,$year);
					$s_end=mktime(23,59,59,$month,$day,$year);
					$day++;
				}
				if($text == $day){
					$s_st=mktime(0,0,0,$month,$day,$year);
					$s_end=mktime(23,59,59,$month,$day,$year);					
					$day++;
					$bg = '';
				}

				$data_nr=0;
				$sheet = $db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$s_st."' AND '".$s_end."'
												 AND servicing_support_users.user_id='".$in['user_id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ");
				while($sheet->next()){
					$temp_data=array(
						'c_name'		=> $sheet->f('customer_name') ? $sheet->f('customer_name') : $sheet->f('contact_name'),
						'DATE'		=> date(ACCOUNT_DATE_FORMAT,$sheet->f('planeddate')),
						'serial_number'	=> $sheet->f('serial_number'),
						'hours'		=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')),
						'id'			=> $sheet->f('id'),
						'service_id'	=> $sheet->f('service_id'),
						'tmsmp'		=> $sheet->f('planeddate'),
						'start_time'	=> $sheet->f('startdate')? number_as_hour($sheet->f('startdate')) : '',
						'end_time'		=> $sheet->f('duration') > 0 ? number_as_hour($sheet->f('startdate')+ $sheet->f('duration')) : '',
						'pduration'		=> $sheet->f('duration') > 0 ? true : false,
						'subject'		=> $sheet->f('subject'),
						'status'		=> $sheet->f('status'),
						'top'			=> $sheet->f('startdate')-1<0 ? true : false,
						'color_ball' 	=> '#'.$sheet->f('color_ball')
					);
					array_push($serv_data, $temp_data);
					$data_nr++;
				}

				$t_day=array(
					'text'		=> $text,				
					'planning_empty'	=> $bg,
					'data'		=> $serv_data,
					'data_nr'		=> $data_nr,
				);
				array_push($day_final, $t_day);
				$k++;
				if($day > $month_days){
					$loopweek = false;
				}
			}
			$temp_week=array('day'=>$day_final);
			array_push($data['week'], $temp_week);
			$i++;
		}

		return json_out($data, $showin,$exit);
	}

	function get_Day($in,$showin=true,$exit=true){
		global $database_config;
		$db = new sqldb();
		$data = array('user_data'=>array(),'days_nr'=>array());

		$select = build_user_dd_service($in['user_id'], $_SESSION['access_level']);
		$team_members=$db->query("SELECT user_id FROM servicing_team_users")->getAll();
		$new_select=array();
		foreach($select as $key=>$value){
			foreach($team_members as $key1=>$value1){
				if($value['id']==$value1['user_id']){
					array_push($new_select,$value);
				}
			}
		}
		$data['select'] = $new_select;
		$data['user_id'] = $in['user_id'];

		if($in['selected_user_id']){
			foreach($select as $key=>$value){
				if($value['id']==$in['selected_user_id']){
					$content_users=array($value);
					break;
				}			
			}
		}else{
			$content_users=$select;
		}

		if($in['start_date']){
			$data['start_date']=is_numeric($in['start_date']) ? $in['start_date']*1000 : strtotime($in['start_date'])*1000;
			$now			= is_numeric($in['start_date']) ? $in['start_date'] : strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$day=date('j',$now);
		$month= date('n', $now);
		$year= date('Y', $now);
		$start_day=mktime(0,0,0,$month,$day,$year);
		$end_day=$start_day+86399;

		$data['day_txt']=date(ACCOUNT_DATE_FORMAT,$end_day);

		for($i=1;$i<25;$i++){
			array_push($data['days_nr'],array('day'=>number_as_hour($i)));
		}

		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);

		$colors = array('ea9c62','41ad65','8e9599');
		$is_top = false;
		foreach($content_users as $row => $val){
			$day_data=array();			
			$sheet = $db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$start_day."' AND '".$end_day."'
												 AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ORDER BY p_date, servicing_support.startdate ASC ");
			$some_array = array();
			while($sheet->next()){				
				if($sheet->f('startdate')-1 < 0){
					$is_top = true;
				}
				$new_date = mktime(0,0,0,date('m',$sheet->f('planeddate')), date('d',$sheet->f('planeddate')), date('Y',$sheet->f('planeddate')));
				if(!array_key_exists($new_date, $some_array)){
					$some_array[$new_date] = array();
				}
				array_push($some_array[$new_date],array(intval($sheet->f('startdate')),intval($sheet->f('duration'))) );
			}
			
			$last_try = array();
			$first_hour = 24;
			foreach ($some_array as $key => $value) {
				$last_try[$key] = array();
				if(count($value) > 1){
				 	foreach ($value as $v) {
				 		if($v[0] != 0 && $v[0] < $first_hour){
				 			$first_hour = $v[0];
				 		}
				 		$increment = 0;
				 		$same_hour = 0;
				 		foreach ($value as $v1) {
				 			$e = abs($v[0]-$v1[0]);
				 			if($e <= 1){
				 				$increment++;
				 			}
				 			if($e == 0){
				 				$same_hour ++;
				 			}
				 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
				 				$same_hour ++;
				 			}
				 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
				 				$same_hour ++;
				 			}
				 		}
				 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
				 	}
				}
			}

			$sheet->reset();
			$day_data=array();
			$all_day_data=array();
			$service_nr=0;
			while($sheet->next()){
				$new_date = mktime(0,0,0,date('m',$sheet->f('planeddate')), date('d',$sheet->f('planeddate')), date('Y',$sheet->f('planeddate')));
				$k = array_search($new_date, $days);
				$width_compensation = 0;

				$width = 0;
				if(@array_key_exists(intval($sheet->f('startdate')), $last_try[$new_date])){
					$width = $last_try[$new_date][intval($sheet->f('startdate'))]['same_hour'];
				}
				if($sheet->f('startdate')){
					$services_data_temp=array(
						'c_name'				=> $sheet->f('customer_name') ? $sheet->f('customer_name') : $sheet->f('contact_name'),
						'DATE'				=> date(ACCOUNT_DATE_FORMAT,$sheet->f('planeddate')),
						'serial_number'			=> $sheet->f('serial_number'),
						'hours'				=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')),
						'id'					=> $sheet->f('id'),
						'service_id'			=> $sheet->f('service_id'),
						'tmsmp'				=> $sheet->f('planeddate'),
						'start_time'			=> $sheet->f('startdate') ? number_as_hour($sheet->f('startdate')) : '',
						'end_time'				=> $sheet->f('duration')>0 ? number_as_hour($sheet->f('startdate')+$sheet->f('duration')) : '',
						'phour'				=> 'phour_'.floor($sheet->f('startdate')),
						'pduration'				=> floor($sheet->f('duration')) > 0 ? 'pduration_'.ceil($sheet->f('duration')) : '',
						'colors'				=> !$sheet->f('status') ? 'div_'.$colors[0] : ($sheet->f('status')==1 ? 'div_'.$colors[1] : 'div_'.$colors[2]),
						'top_class'				=> (floor($sheet->f('startdate'))-1)*$height < 0 ? "top_class" : '',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'subject'				=> $sheet->f('subject'),
						'status'				=> $sheet->f('status'),
						'color_ball'			=> '#'.$sheet->f('color_ball'),
					);
					array_push($day_data,$services_data_temp);
					$service_nr++;
				}else{
					$services_data_temp=array(
						'c_name'				=> $sheet->f('customer_name') ? $sheet->f('customer_name') : $sheet->f('contact_name'),
						'DATE'				=> date(ACCOUNT_DATE_FORMAT,$sheet->f('planeddate')),
						'serial_number'			=> $sheet->f('serial_number'),
						'hours'				=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')),
						'id'					=> $sheet->f('id'),
						'service_id'			=> $sheet->f('service_id'),
						'tmsmp'				=> $sheet->f('planeddate'),
						'start_time'			=> '',
						'end_time'				=> '',
						'phour'				=> 'phour_'.floor($sheet->f('startdate')),
						'pduration'				=> floor($sheet->f('duration')) > 0 ? 'pduration_'.floor($sheet->f('duration')) : '',
						'colors'				=> !$sheet->f('status') ? 'div_'.$colors[0] : ($sheet->f('status')==1 ? 'div_'.$colors[1] : 'div_'.$colors[2]),
						'top_class'				=> (floor($sheet->f('startdate'))-1)*$height < 0 ? "top_class" : '',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'subject'				=> $sheet->f('subject'),
						'status'				=> $sheet->f('status'),
						'color_ball'			=> '#'.$sheet->f('color_ball'),
					);
					array_push($all_day_data,$services_data_temp);
					$service_nr++;
				}
			}

			$temp_users=array(
				'data'	=> $day_data,
				'user_id'	=> $val['id'],
				'name'	=> $val['value'],
				'all_day'	=> $all_day_data,				
			);
			array_push($data['user_data'],$temp_users);
		}
		$data['is_top']=$is_top;

		return json_out($data, $showin,$exit);
	}

	$result = array( );
json_out($result);
?>