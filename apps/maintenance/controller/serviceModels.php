<?php

 if(!defined('BASEPATH')) exit('No direct script access allowed');

	$db = new sqldb();
	$result = array('list'=>array(),'langs'=>array());

	$l_r =ROW_PER_PAGE;
	$result['lr']=$l_r;
	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	if($in['order_by']){
		$order = " ASC ";
		if($in['desc'] == '1' || $in['desc']=='true'){
			$order = " DESC ";
		}

       $order_by =" ORDER BY ".$in['order_by']." ".$order;
       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];

	}

	$filter = " active='2' ";

	$filter_c=' ';

	if(!empty($in['search'])){
		$filter .= " AND serial_number LIKE '%".$in['search']."%' ";
	}
	if(!empty($in['lang'])){
		$filter .= " AND email_language='".$in['lang']."' ";
		$result['lang']=$in['lang'];
	}

	$max_rows =  $db->field("SELECT count(service_id) FROM servicing_support WHERE ".$filter );
	$result['max_rows'] = $max_rows;
	$res=$db->query("SELECT servicing_support.* 
	            FROM servicing_support
	            WHERE ".$filter.$order_by." LIMIT ".$offset*$l_r.",".$l_r );
	$j=0;
	while($res->move_next()){
		$lang_f=$db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$res->f('email_language')."' ");
		if(!$lang_f){
			$lang_f='EN';
		}else if($lang_f=='du'){
			$lang_f='NL';
		}
		$list=array(
			'name'						=> $res->f('serial_number'),
			'service_id'				=> $res->f('service_id'),
			'delete_link' 		 		=> array('do'=>'maintenance-serviceModels-maintenance-deleteService', 'service_id'=>$res->f('service_id')),
			'lang_f'			=> $lang_f ? '('.strtoupper($lang_f).')' : '',
		);
		$result['list'][]=$list;
	}
	$languages=$db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order ");
	while($languages->next()){
		array_push($result['langs'],array('id'=>$languages->f('lang_id'),'name'=>gm($languages->f('language')), 'value' => gm($languages->f('language'))));
	}
	if($in['index']){
		$result['index'] = $in['index'];
	}
	json_out($result);
?>