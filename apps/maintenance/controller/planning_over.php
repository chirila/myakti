<?php

	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
	//$last_view=$db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='planning_view' ");
	$last_view=$db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'planning_view']);
	if(!$last_view && !$in['xget']){
		$in['xget']='Month';
		$in['activeView']='3';
	}else if($last_view && !$in['xget']){
		switch($last_view){
			case '1':
				$in['xget']='Day';			
				break;
			case '2':
				$in['xget']='Week';
				break;
			case '3':
				$in['xget']='Month';
				break;
		}
		$in['activeView']=$last_view;
	}

	if($in['xget']){
    	$fname = 'get_'.$in['xget'];
    	$fname($in,false);
	}
	

	function get_Day($in,$showin=true,$exit=true){
		global $database_config;
		$db = new sqldb();
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		$data = array('user_data'=>array(),'days_nr'=>array());

		$select = build_user_dd_service($in['user_id'], $_SESSION['access_level']);		
		$data['user_id'] = $in['user_id'];

		if($in['selected_user_id'] && !$in['team_id']){
			$in['team_id']=$db->field("SELECT team_id FROM servicing_team_users WHERE user_id='".$in['selected_user_id']."' ORDER BY sort ASC");
		}

		$teams=$db->query("SELECT * FROM servicing_team ORDER BY name")->getAll();
		$data['select'] = $teams;
		if(count($teams)>0 && !$in['team_id']){
			$in['team_id']=$teams[0]['team_id'];
		}
		if($in['team_id']){
			$select=array();
			$data['team_id']=$in['team_id'];
			$team_members=$db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."' ORDER BY sort ASC");
			while($team_members->next()){
				$team_temp=array(
					'id'			=> $team_members->f('user_id'),
					'value'		=> htmlspecialchars_decode(stripslashes($db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$team_members->f('user_id')."' "))),
				);
				array_push($select,$team_temp);
			}
		}
		if($in['selected_user_id']){
			foreach($select as $key=>$value){
				if($value['id']==$in['selected_user_id']){
					$content_users=array($value);
					break;
				}			
			}
		}else{
			$content_users=$select;
		}
		if($in['start_date']){
			$data['start_date']=is_numeric($in['start_date']) ? $in['start_date']*1000 : strtotime($in['start_date'])*1000;
			$now			= is_numeric($in['start_date']) ? $in['start_date'] : strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$day=date('j',$now);
		$month= date('n', $now);
		$year= date('Y', $now);
		$start_day=mktime(0,0,0,$month,$day,$year);
		$end_day=$start_day+86399;

		$data['day_txt']=date(ACCOUNT_DATE_FORMAT,$end_day);

		for($i=1;$i<25;$i++){
			array_push($data['days_nr'],array('day'=>number_as_hour($i)));
		}

		//$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
		//$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
			// case $is_service_manager:
				$filter = '';
				break;
			default:
				$filter = " AND servicing_support.service_id IN (SELECT service_id FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' GROUP BY service_id )";
				break;
		}

		$colors = array('ea9c62','41ad65','8e9599');
		$is_top = false;
		foreach($content_users as $row => $val){
			$day_data=array();			
			$sheet = $db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$start_day."' AND '".$end_day."'
												 AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ");
			$some_array = array();
			while($sheet->next()){				
				if($sheet->f('startdate')-1 < 0){
					$is_top = true;
				}
				$new_date = mktime(0,0,0,date('m',$sheet->f('planeddate')), date('d',$sheet->f('planeddate')), date('Y',$sheet->f('planeddate')));
				if(!array_key_exists($new_date, $some_array)){
					$some_array[$new_date] = array();
				}
				array_push($some_array[$new_date],array(intval($sheet->f('startdate')),intval($sheet->f('duration'))) );
			}
			
			$last_try = array();
			$first_hour = 24;
			foreach ($some_array as $key => $value) {
				$last_try[$key] = array();
				if(count($value) > 1){
				 	foreach ($value as $v) {
				 		if($v[0] != 0 && $v[0] < $first_hour){
				 			$first_hour = $v[0];
				 		}
				 		$increment = 0;
				 		$same_hour = 0;
				 		foreach ($value as $v1) {
				 			$e = abs($v[0]-$v1[0]);
				 			if($e <= 1){
				 				$increment++;
				 			}
				 			if($e == 0){
				 				$same_hour ++;
				 			}
				 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
				 				$same_hour ++;
				 			}
				 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
				 				$same_hour ++;
				 			}
				 		}
				 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
				 	}
				}
			}

			$sheet->reset();
			$day_data=array();
			$all_day_data=array();
			$service_nr=0;
			while($sheet->next()){
				$new_date = mktime(0,0,0,date('m',$sheet->f('planeddate')), date('d',$sheet->f('planeddate')), date('Y',$sheet->f('planeddate')));
				$k = array_search($new_date, $days);
				$width_compensation = 0;

				$width = 0;
				if(@array_key_exists(intval($sheet->f('startdate')), $last_try[$new_date])){
					$width = $last_try[$new_date][intval($sheet->f('startdate'))]['same_hour'];
				}
				if($sheet->f('startdate')){
					$services_data_temp=array(
						'c_name'				=> $sheet->f('customer_name') ? $sheet->f('customer_name') : $sheet->f('contact_name'),
						'DATE'				=> date(ACCOUNT_DATE_FORMAT,$sheet->f('planeddate')),
						'serial_number'			=> $sheet->f('serial_number'),
						'hours'				=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')),
						'id'					=> $sheet->f('id'),
						'service_id'			=> $sheet->f('service_id'),
						'tmsmp'				=> $sheet->f('planeddate'),
						'start_time'			=> $sheet->f('startdate') ? number_as_hour($sheet->f('startdate')) : '',
						'end_time'				=> $sheet->f('duration')>0 ? number_as_hour($sheet->f('startdate')+$sheet->f('duration')) : '',
						'phour'				=> 'phour_'.floor($sheet->f('startdate')),
						'pduration'				=> floor($sheet->f('duration')) > 0 ? 'pduration_'.ceil($sheet->f('duration')) : '',
						'colors'				=> !$sheet->f('status') ? 'div_'.$colors[0] : ($sheet->f('status')==1 ? 'div_'.$colors[1] : 'div_'.$colors[2]),
						'top_class'				=> (floor($sheet->f('startdate'))-1)*$height < 0 ? "top_class" : '',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'subject'				=> $sheet->f('subject'),
						'status'				=> $sheet->f('status'),
						'color_ball'			=> '#'.$sheet->f('color_ball'),
					);
					array_push($day_data,$services_data_temp);
					$service_nr++;
				}else{
					$services_data_temp=array(
						'c_name'				=> $sheet->f('customer_name') ? $sheet->f('customer_name') : $sheet->f('contact_name'),
						'DATE'				=> date(ACCOUNT_DATE_FORMAT,$sheet->f('planeddate')),
						'serial_number'			=> $sheet->f('serial_number'),
						'hours'				=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')),
						'id'					=> $sheet->f('id'),
						'service_id'			=> $sheet->f('service_id'),
						'tmsmp'				=> $sheet->f('planeddate'),
						'start_time'			=> '',
						'end_time'				=> '',
						'phour'				=> 'phour_'.floor($sheet->f('startdate')),
						'pduration'				=> floor($sheet->f('duration')) > 0 ? 'pduration_'.floor($sheet->f('duration')) : '',
						'colors'				=> !$sheet->f('status') ? 'div_'.$colors[0] : ($sheet->f('status')==1 ? 'div_'.$colors[1] : 'div_'.$colors[2]),
						'top_class'				=> (floor($sheet->f('startdate'))-1)*$height < 0 ? "top_class" : '',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'subject'				=> $sheet->f('subject'),
						'status'				=> $sheet->f('status'),
						'color_ball'			=> '#'.$sheet->f('color_ball'),
					);
					array_push($all_day_data,$services_data_temp);
					$service_nr++;
				}
			}

			$temp_users=array(
				'data'	=> $day_data,
				'user_id'	=> $val['id'],
				'name'	=> $val['value'],
				'all_day'	=> $all_day_data,				
			);
			array_push($data['user_data'],$temp_users);
		}
		$data['is_top']=$is_top;
		if($in['track']){
			saveView($in['track']);
		}
		if($in['activeView']){
			$data['activeView']=$in['activeView'];
		}

		return json_out($data, $showin,$exit);
	}

	function get_Week($in,$showin=true,$exit=true){
		global $database_config;
		$db = new sqldb();
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		$data = array('user_data'=>array(),'days_nr'=>array());
		
		$select = build_user_dd_service($in['user_id'], $_SESSION['access_level']);
		$data['user_id'] = $in['user_id'];

		$teams=$db->query("SELECT * FROM servicing_team")->getAll();
		$data['select'] = $teams;
		if(count($teams)>0 && !$in['team_id']){
			$in['team_id']=$teams[0]['team_id'];
		}
		if($in['team_id']){
			$select=array();
			$data['team_id']=$in['team_id'];
			$team_members=$db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."' ORDER BY sort ASC");
			while($team_members->next()){
				$team_temp=array(
					'id'			=> $team_members->f('user_id'),
					'value'		=> htmlspecialchars_decode(stripslashes($db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$team_members->f('user_id')."' "))),
				);
				array_push($select,$team_temp);
			}
		}
		if($in['start_date']){
			$data['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		$week_end 			= $week_start + 604799;

		if(date('I',$week_end)=='1'){
			$week_end-=3600;
		}

		$in['week_start_serv'] = $week_start;
		$in['week_end_serv'] = $week_end;

		$data['week_txt']= date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end);

		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			array_push($data['days_nr'],array('day_name' => gm(date('D',$i)),'date_month' => date('d',$i).' '.gm(date('M',$i)),'end_day'=> date("N",$i)>5 ? true : false));
		}

		$j=0;

		//$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
		//$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
			// case $is_service_manager:
				$filter = '';
				break;
			default:
				$filter = " AND servicing_support.service_id IN (SELECT service_id FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' GROUP BY service_id )";
				break;
		}

		foreach($select as $row => $val){
			$day_data=array();
			$sheet = $db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$in['week_start_serv']."' AND '".$in['week_end_serv']."'
												 AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ")->getAll();

			$start_day=$week_start;
			while($start_day <= $week_end){
				$start_d=$start_day;
				$end_d=$start_d+86399;
				$blocked_day=$db->field("SELECT id FROM servicing_block WHERE `date`='".$start_d."' AND user_id='".$val['id']."' ");

				$service_nr=0;
				$services_data=array();
				foreach($sheet as $key=>$value){
					if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
						$services_data_temp=array(
							'c_name'		=> $value['customer_name'] ? $value['customer_name'] : $value['contact_name'],
							'status'		=> $value['status'],
							'top'			=> $value['startdate']-1<0 ? true : false,
							'start_time'	=> $value['startdate'] ? number_as_hour($value['startdate']) : '',
							'pduration'		=> $value['duration'] > 0 ? true : false,
							'end_time'		=> $value['duration'] > 0 ? number_as_hour($value['startdate']+ $value['duration']) : '',
							'hours'		=> number_as_hour($value['end_time']-$value['start_time']),
							'id'			=> $value['id'],
							'service_id'	=> $value['service_id'],
							'tmsmp'		=> $value['planeddate']
						);
						array_push($services_data,$services_data_temp);
						$service_nr++;
					}
				}

				$temp_day_data=array(
					'day_txt'		=> date('d',$end_d),
					'service_nr'	=> $service_nr,
					'services_data'	=> $services_data,
					'tmsp'		=> $start_d,
					'block_day'		=> $blocked_day ? true : false
				);
				array_push($day_data, $temp_day_data);
				$start_day=$start_day+86400;
			}

			$temp_users=array(
				'data'	=> $day_data,
				'user_id'	=> $val['id'],
				'name'	=> $val['value']
			);
			array_push($data['user_data'],$temp_users);
		}
		if($in['track']){
			saveView($in['track']);
		}
		if($in['activeView']){
			$data['activeView']=$in['activeView'];
		}
		return json_out($data, $showin,$exit);
	}

	function get_Month($in,$showin=true,$exit=true){
		global $database_config;
		$db = new sqldb();
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);

		$data=array('user_data'=>array(),'days_nr'=>array());
		$select = build_user_dd_service($in['user_id'], $_SESSION['access_level']);

		$data['user_id'] = $in['user_id'];

		$teams=$db->query("SELECT * FROM servicing_team")->getAll();
		$data['select'] = $teams;
		if(count($teams)>0 && !$in['team_id']){
			$in['team_id']=$teams[0]['team_id'];
		}
		if($in['team_id']){
			$select=array();
			$data['team_id']=$in['team_id'];
			$team_members=$db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."' ORDER BY sort ASC");
			while($team_members->next()){
				$team_temp=array(
					'id'			=> $team_members->f('user_id'),
					'value'		=> htmlspecialchars_decode(stripslashes($db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$team_members->f('user_id')."' "))),
				);
				array_push($select,$team_temp);
			}
		}
		if($in['start_date']){
			$data['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$month = date('n',$now);
		$year = date('Y',$now);
		$data['month_txt']= date('F',$now);
		$month_t = mktime(0,0,0,$month,1,$year);

		$month_days = date('t',$month_t);
		$month_first_day = date('N',$month_t);
		$month_last_day=mktime(23,59,59,$month,$month_days,$year);

		for($i=1;$i<=$month_days;$i++){
			$day_tmsp=mktime(23,59,59,$month,$i,$year);
			array_push($data['days_nr'],array('day'=>$i,'end_day'=> date("N",$day_tmsp)>5 ? true : false));
		}

		//$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_13']);
		//$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
			// case $is_service_manager:
				$filter = '';
				break;
			default:
				$filter = " AND servicing_support.service_id IN (SELECT service_id FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' GROUP BY service_id )";
				break;
		}

		foreach($select as $row => $val){
			$day_data=array();
			$sheet = $db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$month_t."' AND '".$month_last_day."'
												 AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ")->getAll();

			$day=1;
			while($day <= $month_days){
				$start_d=mktime(0,0,0,$month,$day,$year);
				$end_d=mktime(23,59,59,$month,$day,$year);
				$blocked_day=$db->field("SELECT id FROM servicing_block WHERE `date`='".$start_d."' AND user_id='".$val['id']."' ");

				$service_nr=0;
				$services_data=array();
				foreach($sheet as $key=>$value){
					if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
						$services_data_temp=array('tmsmp'=>$value['planeddate']);
						array_push($services_data,$services_data_temp);
						$service_nr++;
					}
				}

				$temp_day_data=array(
					'day_txt'		=> $day,
					'service_nr'	=> $service_nr,
					'services_data'	=> $services_data,
					'tmsp'		=> $start_d,
					'block_day'		=> $blocked_day ? true : false
				);
				array_push($day_data, $temp_day_data);
				$day++;
			}

			$temp_users=array(
				'data'	=> $day_data,
				'user_id'	=> $val['id'],
				'name'	=> $val['value']
			);
			array_push($data['user_data'],$temp_users);
		}
		if($in['track']){
			saveView($in['track']);
		}
		if($in['activeView']){
			$data['activeView']=$in['activeView'];
		}

		return json_out($data, $showin,$exit);
	}

	function saveView($view){
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		//$last_view=$db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='planning_view' ");
		$last_view=$db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'planning_view']);
		if(!$last_view){
			//$db_users->query("INSERT INTO user_meta SET user_id='".$_SESSION['u_id']."',name='planning_view',value='".$view."' ");
			$db_users->insert("INSERT INTO user_meta SET user_id= :user_id,name= :name,value= :value ",['user_id'=>$_SESSION['u_id'],'name'=>'planning_view','value'=>$view]);
		}else{
			//$db_users->query("UPDATE user_meta SET value='".$view."' WHERE user_id='".$_SESSION['u_id']."' AND name='planning_view' ");
			$db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>$view,'user_id'=>$_SESSION['u_id'],'name'=>'planning_view']);
		}
	}

	$result = array();
json_out($result);
?>