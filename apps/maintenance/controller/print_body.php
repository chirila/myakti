<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
//session_start();

$db = new sqldb();
$db2 = new sqldb();
$db3 = new sqldb();
//$path = __DIR__.'/../view/';
$path = ark::$viewpath;
global $config,$database_config;

$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);
/*if($in['type']){
	$html='service_print_'.$in['type'].'.html';
}elseif ($in['custom_type']) {
	$path = INSTALLPATH.'upload/'.DATABASE_NAME.'/custom_pdf/';

	$html='custom_quote_print-'.$in['custom_type'].'.html';
}else{*/
	$html='service_print_1.html';
// }

// echo $path.$html; exit();
$view1 = new at($path.$html);

// echo "<pre>";
// 	print_r(realpath($path.$html));
// 	exit();
$source = '[Source]';
$type = '[Type]';

$labels_query = $db->query("SELECT * FROM label_language_int WHERE label_language_id='".$in['lid']."'")->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
/*	if( ($j % 2) == 0){
		$j++;
		continue;
	}*/
	$labels[$key] = $value;
	$j++;
}
$labels_query_custom = $db->query("SELECT * FROM label_language_int WHERE lang_code='".$in['lid']."'")->getAll();
if($labels_query_custom){
	foreach ($labels_query_custom['0'] as $key => $value) {
/*		if( ($j % 2) == 0 ) {
			$j++;
			continue;
		}*/
		$labels[$key] = $value;
		$j++;
	}
}

//custom width default and we need it!!
$all_custom_widths = array(4,14,13,15,11,13,14);
$can_be_hidden_custom_widths = array(
						'custom_line_1' => 14,
						'custom_line_2' => 13,
						'custom_line_3' => 15,
						'custom_line_4' => 11,
						'custom_line_5' => 13,
						'custom_line_6' => 14
					);

//custom width
//for salesassist_2
if($in['custom_type'] && DATABASE_NAME == 'salesassist_2'){
	//in this array we put all the fixed widths of each table column
	$all_custom_widths = array(4,14,13,15,11,13,14);
	//in this array we put the name of the columns and the width of the columns that can be hidden
	//if less that 4 we fill with 0;
	//don't change the array structure or size
	$can_be_hidden_custom_widths = array(
		'custom_line_1' => 14, //article code
		'custom_line_2' => 13, //quantity
		'custom_line_3' => 15, //unit price
		'custom_line_4' => 11, //discount
		'custom_line_5' => 13, //packing
		'custom_line_6' => 14 //amount
	);
}
//custom width
//for BOA CONSTRUCTOR
/*if($in['custom_type'] && DATABASE_NAME == 'c98581b0_2564_c9dc_62996aa9def1'){
	//in this array we put all the fixed widths of each table column
	$all_custom_widths = array(4,14,13,15,11,13,14);
	//in this array we put the name of the columns and the width of the columns that can be hidden
	//if less that 4 we fill with 0;
	//don't change the array structure or size
	$can_be_hidden_custom_widths = array(
		'custom_line_1' => 14, //article code
		'custom_line_2' => 13, //quantity
		'custom_line_3' => 15, //unit price
		'custom_line_4' => 11, //discount
		'custom_line_5' => 13, //packing
		'custom_line_6' => 14 //amount
	);
}*/

if(!$in['service_id']){ //sample
	//assign dummy data
	$quote_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));

	$pdf->setQuotePageLabel($labels['page']);

	$view1->assign(array(
//	'ACCOUNT_LOGO_Q'      => ACCOUNT_LOGO_QUOTE?$upload_path.ACCOUNT_LOGO_QUOTE:$upload_path.'img/no-logo.png',
	'ACCOUNT_LOGO_Q'      => 'images/no-logo.png',
	/*'BILLING_ADDRESS_TXT' => getq_label_txt('billing_address',$in['lid']),
	'QUOTE_NOTE_TXT'      => getq_label_txt('quote_note',$in['lid']),
	'QUOTE_TXT'           => getq_label_txt('quote',$in['lid']),
	'REFERENCE_TXT'       => getq_label_txt('reference',$in['lid']),
	'DATE_TXT'            => getq_label_txt('date',$in['lid']),
	'CUSTOMER_TXT'        => getq_label_txt('customer',$in['lid']),
	'ITEM_TXT'            => getq_label_txt('item',$in['lid']),
  'ARTICLE_CODE_TXT'    => getq_label_txt('article_code',$in['lid']),
	'UNITMEASURE_TXT'     => getq_label_txt('unitmeasure',$in['lid']),
	'QUANTITY_TXT'        => getq_label_txt('quantity',$in['lid']),
	'UNIT_PRICE_TXT'      => getq_label_txt('unit_price',$in['lid']),
	'AMOUNT_TXT'          => getq_label_txt('amount',$in['lid']),
	'SUBTOTAL_TXT'        => getq_label_txt('subtotal',$in['lid']),
	'DISCOUNT_TXT'        => getq_label_txt('discount',$in['lid']),
	'VAT_TXT'             => getq_label_txt('vat',$in['lid']),
	'PAYMENTS_TXT'        => getq_label_txt('payments',$in['lid']),
	'AMOUNT_DUE_TXT'      => getq_label_txt('amount_due',$in['lid']),
	'NOTES_TXT'           => getq_label_txt('notes',$in['lid']),
	'SALE_UNIT_TXT'       => getq_label_txt('sale_unit',$in['lid']),
	'PACKAGE_TXT'         => getq_label_txt('package',$in['lid']),
	'YOUR_REFERENCE_TXT'  => getq_label_txt('your_ref',$in['lid']),
	'AUTHOR_TXT'	    => getq_label_txt('author',$in['lid']),
	'GRAND_TOTAL_TXT'     => getq_label_txt('grand_total',$in['lid']),
	'PHONE_TXT'			  => get_label_txt('phone',$in['lid']),
	'FAX_TXT'			  => get_label_txt('fax',$in['lid']),
	'URL_TXT'			  => get_label_txt('url',$in['lid']),
	'EMAIL_TXT'			  => get_label_txt('email',$in['lid']),
	'SOURCE_TXT'		  => getq_label_txt('source',$in['lid']),
	'TYPE_TXT'			  => getq_label_txt('type',$in['lid']),
	'GENERAL_CONDITIONS_TXT' => getq_label_txt('general_conditions',$in['lid']),

	*/
	'intervention'						=> $labels['intervention'],
		'serial_number'						=> $labels['serial_number'],
		'DATE_TXT'								=> $labels['date'],
		'PHONE_TXT'								=> $labels['phone'],
		'FAX_TXT'									=> $labels['fax'],
		'EMAIL_TXT'								=> $labels['email'],
		'URL_TXT'									=> $labels['url'],
		'Interventionreport'			=> $labels['report'],
		'Staff'										=> $labels['staff'],
		'Starttime'								=> $labels['start_time'],
		'Endtime'									=> $labels['end_time'],
		'Break'										=> $labels['break'],
		'Total'										=> $labels['total'],
		'Interventionexpenses'		=> $labels['expenses'],
		'Name'										=> $labels['iname'],
		'Tasks'										=> $labels['task'],
		'Purchases'								=> $labels['purchase'],
		'Articles'								=> $labels['articles'],
		'Quantity'								=> $labels['quantity'],
		'Price'										=> $labels['price'],
		'Delivered'								=> $labels['delivered'],
		'TobeDelivered'								=> $labels['to_be_delivered'],
		'Hourlyrate'							=> $labels['hrate'],
		'TotalAmount'							=> $labels['tamount'],
		'AllpricesarewithoutVAT'	=> $labels['allpricenovat'],

	'SELLER_NAME'         => '[Account Name]',
	'SELLER_D_COUNTRY'    => '[Country]',
	'SELLER_D_STATE'      => '[State]',
	'SELLER_D_CITY'       => '[City]',
	'SELLER_D_ZIP'        => '[Zip]',
	'SELLER_D_ADDRESS'    => '[Address]',
	'SELLER_B_COUNTRY'    => '[Billing Country]',
	'SELLER_B_STATE'      => '[Billing State]',
	'SELLER_B_CITY'       => '[Billing City]',
	'SELLER_B_ZIP'        => '[Billing Zip]',
	'SELLER_B_ADDRESS'    => '[Billing Address]',
	'SERIAL_NUMBER'       => '####',
	'BUYER_REFERENCE'     => '####',
	'QUOTE_DATE'          => $quote_date ,
	'QUOTE_VAT'           => '##',
	'BUYER_NAME'       	  => '[Customer Name]',
	'QUOTE_CONTACT_NAME'  => '[Contact Name]',
	'BUYER_COUNTRY'    	  => '[Customer Country]',
	'BUYER_STATE'      	  => '[Customer State]',
	'BUYER_CITY'       	  => '[Customer City]',
	'BUYER_ZIP'           => '[Customer ZIP]',
	'BUYER_ADDRESS'    	  => '[Customer Address]',
  'NOTES'            	  => '[NOTES]',
  'SOURCE'			  => $labels['source'].': [Source]',
	'TYPE'			 	  => $labels['type'].': [Type]',
	'AUTHOR'			  => '[Author]',
	'OWN_REFERENCE'		  => '[Your Reference]' ,
	'HIDE_DEFAULT_TOTAL'  => 'hide',
	'SOURCE'			  => $source ? $labels['source'].": ".$source : '',
	'TYPE'			 	  => $type ? $labels['type'].': '.$type : '',
	'SOURCE4'			  => $source? $source : '',
	'TYPE4'			 	  => $type ? $type : '',
	'HIDE_DELIVERY'		  => 'hide',
	'DELIVERY_ADDRESS'	  => '[Delivery Address]',

	'SELLER_B_FAX'        => '[Fax]',
	'SELLER_B_EMAIL'      => '[Email]',
	'SELLER_B_PHONE'      => '[Phone]',
	'SELLER_B_URL'        => '[URL]',


	'is_validity_date'    		=> true,
	'VALID_UNTILL_TXT'    		=> $labels['valid_until'],
	'validity_date'		  	=> date(ACCOUNT_DATE_FORMAT,  strtotime('+1 week')),
	'SHOW_TOTAL'		  		=> true,
	'show_table'		  		=>true,
	'subtotal_txt'			  	=> $labels['subtotal'],
	'total_novat'				=> place_currency(display_number(2020),get_commission_type_list($currency_type)),
	));
	$item_width=60;
	if($in['type']==4 || $in['type']==5){
		$item_width = 54;
	}

	$grand_total = 2020;
	$currency_type = 1;
	$version_code="A";
	$discount_procent = 10;
	$serial_number = '2011-014';
	$image ='<br/><img src="'.$config['real_path'].'phpthumb/phpThumb.php?src=../img/no_image.gif&w=185&h=185'.'"  />';


  	$height = 262;
	if($discount_procent){
		$height -= 5;
		$discount_value = ($grand_total * $discount_procent) / 100;
		$grand_total -= $discount_value;
		$view1->assign(array('TOTAL_DISCOUNT_PROCENT'=>display_number($discount_procent)));
		$view1->assign(array('DISCOUNT_VALUE'=>$discount_value ? place_currency(display_number($discount_value),get_commission_type_list($currency_type)) : place_currency(display_number(0)),get_commission_type_list($currency_type)));
		$view1->assign(array('VIEW_DISCOUNT'	=>''));
	}else{
		$view1->assign(array('VIEW_DISCOUNT'	=>'hide'));
	}

	if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
		$total_default = $grand_total*return_value($currency_rate);
	}

	$view1->assign(array('GRAND_TOTAL' 		=> place_currency(display_number($grand_total),get_commission_type_list($currency_type))));
	$view1->assign(array('GRAND_TOTAL_DEF' 	=> place_currency(display_number($total_default),get_commission_type_list(ACCOUNT_CURRENCY_TYPE))));


}else{

	//$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
	$perm_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
	$is_service_manager = $db->field("SELECT pr_m FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' AND service_id='".$in['service_id']."' ");
	switch (true) {
		case $_SESSION['group'] == 'admin':
		case $perm_admin:
		case $is_service_manager:
			$can_edit = true;
			break;
		default:
			$can_edit = false;
			break;
	}

	$service = $db->query("SELECT servicing_support.*,multiple_identity.* FROM servicing_support
							LEFT JOIN multiple_identity ON multiple_identity.identity_id=servicing_support.identity_id
							WHERE service_id='".$in['service_id']."'");
	if(!$in['lid']){
		$in['lid']=$service->f('email_language');
	}
	$quote_date = $service->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,  $service->f('planeddate')) :'' ;
	$pdf->setQuotePageLabel($labels['page']);

	$img = 'images/no-logo.png';
	$attr = '';
	if($service->f('company_logo')){
		$print_logo = $service->f('company_logo');
	}else if($service->f('pdf_logo')){
		$print_logo = $service->f('pdf_logo');
	}else {
		$print_logo=ACCOUNT_MAINTENANCE_LOGO;
	}
	if($print_logo){
		$img = $print_logo;
		$size = @getimagesize($img);
		if($size === false){
			$img=ACCOUNT_LOGO ? ACCOUNT_LOGO :'images/no-logo.png';
			$size = getimagesize($img);
		}
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
	            //meyer db
		}
	}


	if(!file_exists($img)){
		$img = 'images/no-logo.png';
	}
	// var_dump($img);exit();
	$free_field = $service->f('customer_address').'
'.$service->f('customer_zip').' '.$service->f('customer_city').'
'.get_country_name($service->f('customer_country_id'));

	$serial_number=$db->f('serial_number');
	$view1->assign(array(
		'ACCOUNT_LOGO_Q'      		=> $img,
		'ATTR'				  		=> $attr,
		/*'BILLING_ADDRESS_TXT' 		=> getq_label_txt('billing_address',$in['lid']),
		'QUOTE_NOTE_TXT'      		=> getq_label_txt('quote_note',$in['lid']),
		'QUOTE_TXT'           		=> getq_label_txt('quote',$in['lid']),
		'DATE_TXT'            		=> getq_label_txt('date',$in['lid']),
		'REFERENCE_TXT'       		=> getq_label_txt('reference',$in['lid']),
		'YOUR_REFERENCE_TXT'  		=> getq_label_txt('your_ref',$in['lid']),
		'CUSTOMER_TXT'        		=> getq_label_txt('customer',$in['lid']),
		'DISCOUNT_TXT'        		=> getq_label_txt('discount',$in['lid']),
		'GRAND_TOTAL_TXT'     		=> getq_label_txt('grand_total',$in['lid']),
		'NOTES_TXT'           		=> $notes ? getq_label_txt('notes',$in['lid']) : '' ,
		'SALE_UNIT_TXT'       		=> getq_label_txt('sale_unit',$in['lid']),
		'PACKAGE_TXT'         		=> getq_label_txt('package',$in['lid']),
		'AUTHOR_TXT'							=> getq_label_txt('author',$in['lid']),
		'SOURCE_TXT'							=> getq_label_txt('source',$in['lid']),
		'TYPE_TXT'								=> getq_label_txt('type',$in['lid']),
		'PHONE_TXT'								=> getq_label_txt('phone',$in['lid']),
		'FAX_TXT'			  					=> getq_label_txt('fax',$in['lid']),
		'URL_TXT'			  					=> getq_label_txt('url',$in['lid']),
		'EMAIL_TXT'								=> getq_label_txt('email',$in['lid']),
		'subtotal_txt'			  		=> getq_label_txt('subtotal',$in['lid']),
		'subtotal_2_txt'					=> getq_label_txt('subtotal_2',$in['lid']),
		'GENERAL_CONDITIONS_TXT'	=> getq_label_txt('general_conditions',$in['lid']),
		'VAT_NR_TXT'		  				=> getq_label_txt('vat_number', $in['lid']),
		'BIC_TXT'			  					=> getq_label_txt('bic_code', $in['lid']),
		'IBAN_TXT'			  				=> getq_label_txt('iban', $in['lid']),
		'BANK_NAME_TXT'						=> getq_label_txt('bank_name', $in['lid']),
		'BANK_DETAILS_TXT'				=> getq_label_txt('bank_details', $in['lid']),*/
		'intervention'						=> $labels['intervention'],
		'serial_number'						=> $labels['serial_number'],
		'DATE_TXT'								=> $labels['date'],
		'PHONE_TXT'								=> $labels['phone'],
		'FAX_TXT'									=> $labels['fax'],
		'EMAIL_TXT'								=> $labels['email'],
		'URL_TXT'									=> $labels['url'],
		'Interventionreport'			=> $labels['report'],
		'Staff'										=> $labels['staff'],
		'Starttime'								=> $labels['start_time'],
		'Endtime'									=> $labels['end_time'],
		'Break'										=> $labels['break'],
		'Total'										=> $labels['total'],
		'Interventionexpenses'		=> $labels['expenses'],
		'Name'										=> $labels['iname'],
		'Tasks'										=> $labels['task'],
		'Purchases'								=> $labels['purchase'],
		'Articles'								=> $labels['articles'],
		'Quantity'								=> $labels['quantity'],
		'Price'										=> $labels['price'],
		'Delivered'								=> $labels['delivered'],
		'TobeDelivered'								=> $labels['to_be_delivered'],
		'Hourlyrate'							=> $labels['hrate'],
		'TotalAmount'							=> $labels['tamount'],
		'AllpricesarewithoutVAT'	=> $labels['allpricenovat'],

		'SELLER_NAME'        			=> $service->f('identity_id')? $service->f('company_name') : ACCOUNT_COMPANY,
		'CODE_CLIENT'        			=> $db->f('buyer_reference'),
    'is_validity_code_client' => $db->f('buyer_reference') ? true : false,
		'SELLER_D_COUNTRY'   	 	=> $service->f('identity_id')? get_country_name($service->f('country_id')) : get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
		// 'SELLER_D_STATE'      		=> get_state_name($db->f('seller_d_state_id')),
		'SELLER_D_CITY'       		=> $service->f('identity_id')? $service->f('city_name') :ACCOUNT_DELIVERY_CITY,
		'SELLER_D_ZIP'       		=> $service->f('identity_id')? $service->f('company_zip') :ACCOUNT_DELIVERY_ZIP,
		'SELLER_D_ADDRESS'    		=> $service->f('identity_id')? $service->f('company_address') :ACCOUNT_DELIVERY_ADDRESS,
		'SELLER_B_COUNTRY'    		=> get_country_name($db->f('seller_b_country_id')),
		// 'SELLER_B_STATE'      		=> get_state_name($db->f('seller_b_state_id')),
		'SELLER_B_CITY'       		=> $db->f('seller_b_city'),
		'SELLER_B_ZIP'       	 		=> $db->f('seller_b_zip'),
		'SELLER_B_ADDRESS'    		=> $db->f('seller_b_address'),
		'SERIAL_NUMBER'       		=> $db->f('serial_number'),
		'QUOTE_DATE'          		=> $quote_date,
		'QUOTE_VAT'           		=> $db->f('vat'),
		'BUYER_NAME'          		=> $db->f('customer_name'),
		'QUOTE_CONTACT_NAME'			=> $db->f('contact_name'),
		'free_field'							=> $db->f('free_field') ? nl2br($db->f('free_field')) : nl2br($free_field),
		'contact_email'						=> $db->f('customer_email'),
		'contact_phone'						=> $db->f('customer_phone'),
		'BUYER_REFERENCE'     		=> $db->f('buyer_reference') ? $db->f('buyer_reference') : '' ,
		'OWN_REFERENCE'		  			=> $db->f('own_reference') ? $db->f('own_reference') : '' ,
		'HIDE_YOUR_REF'		  			=> $db->f('own_reference') ? '' : 'hide' ,
		'HIDE_BUYER_REF'      		=> $db->f('buyer_reference') ? '' : 'hide',
		'BUYER_COUNTRY'       		=> get_country_name($db->f('buyer_country_id')),
		// 'BUYER_STATE'         		=> get_state_name($db->f('buyer_state_id')),
		'BUYER_CITY'          		=> $db->f('buyer_city'),
		'BUYER_ZIP'           		=> $db->f('buyer_zip'),
		'BUYER_BTW'			  				=> $buyer_btw,
		'HIDE_BUYER_BTW'					=> $buyer_btw ? '' : hide,
		'DELIVERY_ADDRESS'	  		=> $db->f('delivery_address')? nl2br($db->f('delivery_address')):'',
		'HIDE_DELIVERY'		  			=> $db->f('delivery_address')? '':'hide',
		'BUYER_ADDRESS'       		=> $db->f('buyer_address'),
		'NOTES'               		=> $notes ? nl2br($notes) : '' ,
		'VIEW_NOTES'          		=> $notes ? '' : 'hide' ,
		//for BOA CONSTRUCTOR
		'IS_VIEW_NOTES'          	=> $notes ? true : false ,
		'DEF_CURRENCY'			 			=> build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
		'HIDE_DEFAULT_TOTAL'  		=> 'hide',//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide',
		'SOURCE'									=> $source ? $labes['source'].": ".$source : '',
		'SOURCE4'									=> $source? $source : '',
		'HIDE_SOURCE'		 					=> $source ? '' : 'hide',
		'TYPE'			 							=> $type ? $labes['type'].': '.$type : '',
		'TYPE4'			 							=> $type ? $type : '',
		'HIDE_TYPE'			  				=> $type ? '': 'hide',
		'AUTHOR'			  					=> trim($author) ? $author : '' ,
		'HIDE_AUTHOR'		  				=> trim($author) ? '' : 'hide' ,
		'dbsession'			  				=> DATABASE_NAME,

		'SELLER_B_FAX'        		=> $service->f('identity_id')? $service->f('company_fax') : ACCOUNT_FAX,
		'SELLER_B_EMAIL'      		=> $service->f('identity_id')? $service->f('company_email') : ACCOUNT_EMAIL,
		'SELLER_B_PHONE'      		=> $service->f('identity_id')? $service->f('company_phone') : ACCOUNT_PHONE,
		'SELLER_B_URL'        		=> $service->f('identity_id')? $service->f('company_url') : ACCOUNT_URL,
		'SELLER_BANK'				  		=> ACCOUNT_BANK_NAME,
		'SELLER_BIC'		  				=> ACCOUNT_BIC_CODE,
	  'SELLER_IBAN'		  				=> ACCOUNT_IBAN,
	  'SELLER_BTW'							=> ACCOUNT_VAT_NUMBER,

		'HIDE_F'			  					=> defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? 'hide' : '',
		'HIDE_E'			  					=> defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? 'hide' : '',
		'HIDE_P'			 						=> defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? 'hide' : '',
		'HIDE_U'			  					=> defined('ACCOUNT_URL') && !ACCOUNT_URL ? 'hide' : '',

		'SHOW_F'			  					=> defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? false : true,
		'SHOW_E'			  					=> defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? false : true,
		'SHOW_P'			 						=> defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? false : true,
		'SHOW_U'			  					=> defined('ACCOUNT_URL') && !ACCOUNT_URL ? false : true,

		'sh_vat'			  					=> $show_vat ? '' : 'hide',
		'SHOW_TOTAL'		  				=> $show_total,
		'show_table'		  				=>($show_total||$show_vat||$discount_procent)? '1':'',
		'account_vat_number'  		=> ACCOUNT_VAT_NUMBER,
		'VALID_UNTILL_TXT'    		=> $labels['valid_until'],
		'VAT_TXT'			  					=> $labels['vat'],
		'hrate'										=> place_currency(display_number($db->f('rate'))),
		'shropdf'									=> $db->f('shropdf') && $can_edit ? $db->f('shropdf') : '',
	));
$srate = $db->f('rate');
$total_amount = 0;

	$users_exist=false;
	$u = array();
	$users = $db->query("SELECT * FROM servicing_support_users WHERE service_id='".$in['service_id']."' ORDER BY name ASC ");
	while($users->next()){
		$users_exist=true;
		$u[$users->f('user_id')] = $users->f('name');
		$view1->assign(array(
			'name' 		=> $users->f('name'),
			'id'		=> $users->f('u_id'),
			'pr_m'		=> $users->f('pr_m') == 1 ? 'CHECKED' : '' ,
			'sign'		=> count($u) <= 1 ? '' : ' / ',
		),'users');
		$view1->loop('users');
	}
	$total_h = 0;
	$sheet = $db->query("SELECT servicing_support.*,servicing_support_sheet.*, servicing_support_sheet.date AS date2,servicing_support_sheet.user_id AS u_id
						FROM servicing_support_sheet
						INNER JOIN servicing_support ON servicing_support_sheet.service_id=servicing_support.service_id
						WHERE servicing_support_sheet.service_id='".$in['service_id']."' ");
	while ($sheet->next()) {
		$view1->assign(array(
			'DATE'						=> date(ACCOUNT_DATE_FORMAT,$sheet->f('date2')),
			'hours'						=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break')),
			'break'						=> number_as_hour($sheet->f('break')),
			'id'						=> $sheet->f('id'),
			'service_id'				=> $sheet->f('service_id'),
			'tmsmp'						=> $sheet->f('date2'),
			'start_time'				=> number_as_hour($sheet->f('start_time')),
			'end_time'					=> number_as_hour($sheet->f('end_time')),
			'user'						=> get_user_name($sheet->f('u_id'))
		),'intervention');
		$view1->loop('intervention');
		$total_h += $sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break');
		$total_amount += ($sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break'))*$srate;
	}
	$total_exp = 0;
	$is_exp = false;
	$expense = $db->query("SELECT project_expenses.*, expense.name AS e_name,
								expense.unit_price, expense.unit
						   FROM project_expenses
						   INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
						   INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
						   WHERE project_expenses.service_id='".$in['service_id']."' ");
	while ($expense->next()) {
		$is_exp = true;
		$amount = place_currency(display_number($expense->f('amount')));
		$a = $expense->f('amount');
		if($expense->f('unit_price')){
			$amount = place_currency(display_number(($expense->f('amount') * $expense->f('unit_price'))))." (".$expense->f('amount')." ".$expense->f('unit').")";
			$a = $expense->f('amount')* $expense->f('unit_price');
		}
		//console::log($amount);
		$view1->assign(array(
			'DATE'						=> date(ACCOUNT_DATE_FORMAT,$expense->f('date')),
			'name'						=> $expense->f('e_name'),
			'hours'						=> $amount,
		),'intervention_exp');
		$view1->loop('intervention_exp');
		$total_exp += $a;
	}
	$total_amount += $total_exp;
	$tasks =0;
	$task = $db->query("SELECT * FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' ORDER BY sort_order ASC ");
	while($task->next()){
		$view1->assign(array(
			'name' 			=> htmlentities($task->f('task_name') ),
			'id'			=> $task->f('task_id'),
			'checked'		=> $task->f('closed') == 1 ? "CHECKED" : '',
			'comment'		=> nl2br($task->f('comment')),
			'closed'		=> $task->f('closed') == 1 ? true : false,
		),'task');
		$view1->loop('task');
		$tasks++;
	}

	$expens=0;
	$task = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY sort_order ASC ");
	while($task->next()){
		$view1->assign(array(
			'name' 		 	  => $task->f('name'),
			'id'			    => $task->f('a_id'),
			'quantity'		=> display_number($task->f('quantity')),
			'to_deliver'	=> display_number($task->f('to_deliver')),
			'delivered'		=> display_number($task->f('delivered')),
			'price'			  => place_currency(display_number($task->f('price')*$task->f('margin')/100+$task->f('price')),$currency),
		),'expenses');
		$view1->loop('expenses');
		$expens++;
		$total_amount += ($task->f('price')*$task->f('margin')/100+$task->f('price'))*$task->f('delivered');
	}

	$article=0;
	$task = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id!='0' AND has_variants='0' ORDER BY sort_order ASC ");
	while($task->next()){
		$q_delivered=$db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$task->f('article_id')."' ");
		$view1->assign(array(
			'name' 				=> $task->f('name'),
			'id'					=> $task->f('a_id'),
			'quantity'		=> display_number($task->f('quantity')),
			'to_deliver'	=> display_number($task->f('to_deliver')),
			'delivered'		=> display_number($task->f('delivered')),
			'tobedelivered'	=> display_number($task->f('quantity')-$q_delivered),
			//'price'			=> place_currency(display_number($task->f('price')*$task->f('margin')/100+$task->f('price')),$currency),
			'price'			=> place_currency(display_number($task->f('price')),$currency),
		),'articles');
		$view1->loop('articles');
		$article++;
	}


	$view1->assign(array(
		'is_exp'				=> $is_exp,
		'total_h'				=> number_as_hour($total_h),
		'total_exp'				=> display_number($total_exp),
		'invoice_seller_name'	=>$service->f('customer_name'),
		'factur_nr'				=>$service->f('serial_number'),
		'factur_date'			=>$date,
		'invoice_seller_b_address'=>$service->f('project_name'),
		'invoice_seller_b_zip'	=>$service->f('task_name'),
		'invoice_seller_b_country'=>$service->f('contract_name'),
		'invoice_vat_no'		=> $service->f('subject'),
		'total_amount'			=> place_currency(display_number($total_amount)),
		'intervention_report'	=> $service->f('report'),
		'expens'				=>$expens,
		'article'				=>$article,
		'users_exist'			=>$users_exist,
		'tasks_exist'			=>$tasks ? true : false
	));
}

if($hide_all == 1){
	$view1->assign(array(
		'HIDE_ALL'		=>	'hide',
		'is_hide_all'	=>	false,
		'is_hide_t'		=> 	true,));
}else if($hide_all == 2){
	$view1->assign(array(
		'HIDE_T'		=>	'hide',
		'is_hide_all'	=> 	true,
		'is_hide_t'		=> 	false));
}
/*
switch ($in['lid']) {
	case '1':
		$language = 'nl';
		break;
	case '2':
		$language = 'en';
		break;
	case '3':
		$language = 'fr';
		break;
	case '4':
		$language = 'de';
		break;
	default:
		$language = 'en';
		break;
}
*/
// include(__DIR__.'/../../../../../lang/lang_'.strtolower($language).'.php');

return $view1->fetch();