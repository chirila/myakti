<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ServiceEdit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getService(){
		if($this->in['service_id'] == 'tmp'){ //add
			$this->getAddService();
		}elseif ($this->in['template']) { # create quote template
			$this->getTemplateService();
		}else{ 
			if($this->in['xview']){
				$this->getViewService();
			}else{
				# edit
				$this->getEditService();
			}		
		}
	}

	public function getAddService(){
		$in=$this->in;

		$output=array();

		$next_function = 'maintenance-service-maintenance-updateCustomerData';

		if(!$in['user_add_id']){
			$output['user_id'] = $_SESSION['u_id'];
			$output['user_add_id'] = get_user_name($_SESSION['u_id']);
		}

		$user_lang = $_SESSION['l'];
		if ($user_lang=='nl')
		{
			$user_lang='du';
		}
		switch ($user_lang) {
			case 'en':
				$user_lang_id='1';
				break;
			case 'du':
				$user_lang_id='3';
				break;
			case 'fr':
				$user_lang_id='2';
				break;
		}	
		if($in['deal_id']){
			$output['deal_id']=$in['deal_id'];
			$deal_data=$this->db->query("SELECT * FROM tblopportunity WHERE opportunity_id='".$in['deal_id']."' ");
			if($deal_data->next()){
				if(!$in['buyer_id'] && $deal_data->f('buyer_id')){
					$in['buyer_id']=$deal_data->f('buyer_id');
					$output['add_customer']=false;
					$in['sameAddress']=!$deal_data->f('same_address') ? '1' : ''; 
				}
				if(!$in['contact_id'] && $deal_data->f('contact_id')){
					$in['contact_id']=$deal_data->f('contact_id');
				}
				$in['subject']=$deal_data->f('subject');
				$in['main_address_id']=$deal_data->f('main_address_id');
				$in['delivery_address_id']=$deal_data->f('same_address');
				$in['identity_id']=$deal_data->f('identity_id');
				$in['source_id']=$deal_data->f('source_id');
				$in['type_id']=$deal_data->f('type_id');
				$in['segment_id']=$deal_data->f('segment_id');
			}		
		}	

		$details = array('table'	 => $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field' 	 => $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'	 => $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],					 
					 'filter'	 => $in['buyer_id'] ? ' AND is_primary =1' : '',
					);
		$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
		
		if($in['buyer_id'] ){
			$buyer_info = $this->db->query("SELECT customers.payment_term,customers.payment_term_type, customers.btw_nr, customers.c_email, customers.acc_manager_name,customers.user_id, customer_legal_type.name as l_name,
					customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.invoice_note2, customers.internal_language,
					customers.attention_of_invoice,customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc, customers.name AS company_name, customers.vat_regime_id,customers.identity_id
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$in['buyer_id']."' ");
			$buyer_info->next();
			$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');
			$acc_manager_id = explode(',',$buyer_info->f('user_id'));
	      	$acc_manager_name = explode(',',$buyer_info->f('acc_manager_name'));
		  	$output['acc_manager_id'] = $acc_manager_id[0];
		  	$output['acc_manager_name']	= $acc_manager_name[0];

			$text = gm('Company Name').':';
			$c_email = $buyer_info->f('c_email');
			$c_fax = $buyer_details->f('comp_fax');
			$c_phone = $buyer_details->f('comp_phone');
					
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			$main_address_id			= $buyer_details->f('address_id');
			$sameAddress = false;
			if($buyer_details->f('is_primary')==1){
				$sameAddress = true;
			}
			//if(!$in['is_recurring']){
				//$next_function='maintenance-service-maintenance-addService';
			//}		
			$in['email_language']=$buyer_info->f('internal_language');
			$in['vat_regime_id']=$buyer_info->f('vat_regime_id');

			if(!$in['vat_regime_id']){
				$vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

			    if(empty($vat_regime_id)){
			      $vat_regime_id = 1;
			    }
				$in['vat_regime_id'] = $vat_regime_id;
			}
			
			$output['deals']= $this->get_deals($in);
			$output['ADV_CRM']= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;

			$customer_phone=$this->db->field("SELECT customers.comp_phone FROM customers WHERE customer_id='".$in['buyer_id']."' ");
			if($customer_phone){
				$output['customer_phone']='<i class="fa fa-phone" aria-hidden="true"></i> <span>'.$customer_phone.'</span>';
			}
			if($in['contact_id']){
				$contact_data=$this->db->query("SELECT customer_contactsIds.phone,customer_contacts.cell FROM customer_contactsIds
				 LEFT JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
				 WHERE customer_contactsIds.customer_id='".$in['buyer_id']."' AND customer_contactsIds.contact_id='".$in['contact_id']."' ");
				if($contact_data->f('phone') || $contact_data->f('cell')){
					$contact_phone='';
					if($contact_data->f('cell')){
						$contact_phone='<i class="fa fa-mobile" aria-hidden="true"></i> <span>'.$contact_data->f('cell').'</span>';
					}else if($contact_data->f('phone')){
						$contact_phone='<i class="fa fa-phone" aria-hidden="true"></i> <span>'.$contact_data->f('phone').'</span>';
					}
					$output['contact_phone']=$contact_phone;				
				}
		    }

		}else{
			$buyer_info = $this->db->query("SELECT phone, cell, email, CONCAT_WS(' ',firstname, lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
			$buyer_info->next();
			$c_email = $buyer_info->f('email');
			$c_fax = '';
			$c_phone = $buyer_info->f('phone');
			$text =gm('Name').':';
			$name = $buyer_info->f('name');
			if($in['contact_id']){
				//$next_function='maintenance-service-maintenance-addService';
			}
		}


		$free_field = $buyer_details->f('address').'
		'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
		'.get_country_name($buyer_details->f('country_id'));
		$name = stripslashes($name);

		if($in['main_address_id'] && $in['main_address_id']!=$main_address_id){
			$main_address_id=$in['main_address_id'];
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
		}
		if($in['delivery_address_id']){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
			$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
		}

		//$output['serial_number'] = $in['serial_number'] ? $in['serial_number'] : generate_service_number();
		$output['billable']=($in['billable'] || $in['billable']=='') ? $in['billable'] : (defined('MAINTENANCE_IS_BILLABLE') && MAINTENANCE_IS_BILLABLE == 1 ? true : false);
		$output['rate'] = $in['rate'] ? $in['rate'] : (defined('MAINTENANCE_RATE') && MAINTENANCE_RATE ? display_number(MAINTENANCE_RATE) : display_number(0));
		$output['price'] = $in['price'] ? $in['price'] : display_number(0);
		$output['country_dd']			= $in['country_id']? build_country_list($in['country_id']):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['main_country_id']		= $in['country_id'] ? $in['country_id'] : ACCOUNT_BILLING_COUNTRY_ID;
		$output['language_dd2'] 		= isset($in['email_language']) ? build_language_dd_new($in['email_language']) : build_language_dd_new(0);
		$output['multiple_identity'] 		= build_identity_dd($selected);
		$output['customer']			= 'customer';
		$output['contact']			= 'contact';
		$output['main_address_id']		= $main_address_id;
		$output['sameAddress']			= $in['sameAddress'] ? ($in['sameAddress'] == '1' ? true:false) : false;
		$output['buyer_id']             	= $in['buyer_id'];
		$output['customer_id']			= $in['buyer_id'];
		$output['contact_id']           	= $in['contact_id'];
		$output['buyer_name']           	= $name;
		$output['field']				= $in['buyer_id'] ? 'customer_id' : 'contact_id';
		$output['free_field']			= $in['delivery_address'] ? $in['delivery_address'] : $free_field;
		$output['free_field_txt']		= $in['delivery_address'] ? nl2br($in['delivery_address']) : nl2br($free_field);
		$output['delivery_address_id']	= $in['delivery_address_id'];
		$output['author_id']			= $in['author_id'] ? $in['author_id'] : $_SESSION['u_id'];
		$output['authors']			= $this->get_author($in);
		$output['accmanager']			= $this->get_accmanager($in);
		$output['gender_dd']			= gender_dd();
		$output['title_dd']				= build_contact_title_type_dd(0);
		$output['title_id']				= '0';
		$output['gender_id']			= '0';

		if($in['for_user']){
			$output['for_user']=$in['for_user'];
		}

		if($in['contact_id']){
			$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
		}

		$quote_task=false;
		$quote_art=false;
		$quote_service=false;
		if($in['quote_id']){
			$quote_inf=$this->db->query("SELECT * FROM tblquote WHERE id='".$in['quote_id']."' ");
			$output['quote_id']			= $in['quote_id'];
			$output['quote_hide']			= $in['quote_id'] ? false : true;
			$output['customer']			= '';
			$output['contact']			= '';
			$output['customer_name']		= urldecode($in['customer']);
			$output['customer_readonly']		= true;
			$output['currency_type']		= $in['currency_type'];
			$output['source_id']			= $in['source_id'];
			$output['type_id']			= $in['type_id'];
			$output['segment_id']			= $in['segment_id'];
			$in['subject']				= $quote_inf->f('subject');
			$in['vat_regime_id']=$quote_inf->f('vat_regime_id');
			$output['installation_id']		= $quote_inf->f('installation_id');
			$output['main_address_id']		= $quote_inf->f('main_address_id');
			$output['sameAddress']			= '1';
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$quote_inf->f('main_address_id')."' ");
			$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
			$output['free_field']			= $free_field;
			$output['free_field_txt']		= nl2br($free_field);
			$in['email_language']			= $in['languages'];
			if($in['acc_manager_id']){
				$output['acc_manager_id']=$in['acc_manager_id'];
			}

			if($in['quote_group_data'] && !empty($in['quote_group_data'])){
				$output['quote_group_data']=$in['quote_group_data'];
				$quote_task=true;
			}else{
				$groups = $this->db->query("SELECT DISTINCT tblquote_line.group_id,tblquote_group.title FROM tblquote_line
						INNER JOIN tblquote_version
						ON tblquote_line.version_id = tblquote_version.version_id
						INNER JOIN tblquote_group ON tblquote_line.group_id = tblquote_group.group_id
						LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
						WHERE tblquote_line.quote_id='".$in['quote_id']."'
						AND tblquote_line.content_type='1'
						AND tblquote_version.active='1'
						AND tblquote_line.article_id='0'
						AND tblquote_line.is_tax='0'
						ORDER BY group_id ASC ");
				
				$output['quote_group_data']=array();
				while($groups->next()){				
					$data_line = $this->db->query("SELECT tblquote_line.* FROM tblquote_line
								INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
								LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
								WHERE tblquote_line.quote_id='".$in['quote_id']."'
								AND tblquote_line.content_type='1'
								AND tblquote_line.line_type<'3'
								AND tblquote_version.active='1'
								AND tblquote_line.article_id='0'
								AND tblquote_line.is_tax='0'
								AND tblquote_line.group_id = '".$groups->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;
					$i=0;
					$temp_data_line=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$d_line=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
							'amount_nr'			=> $amount,
						);
						array_push($temp_data_line, $d_line);
						$total+=$amount;
						$i++;
					}

					$quote_group_data=array(
						'group_title'		=> $groups->f('title') ? $groups->f('title') : gm('Group').' '.$i,
						'nr_crt'			=> $i,
						'group_id'			=> $groups->f('group_id'),
						'checked'		=> false,
						'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
						'data_line'			=> $temp_data_line,
						'total_free'		=> $total
					);
					if($i>0){
						$quote_task=true;
						array_push($output['quote_group_data'], $quote_group_data);					
					}	
				}		
			}
			if($in['quote_group_services'] && !empty($in['quote_group_services'])){
				$output['quote_group_services']=$in['quote_group_services'];
				$quote_service=true;
			}else{
				$q_services = $this->db->query("SELECT DISTINCT tblquote_line.group_id,tblquote_group.title FROM tblquote_line
						INNER JOIN tblquote_version
						ON tblquote_line.version_id = tblquote_version.version_id
						INNER JOIN tblquote_group ON tblquote_line.group_id = tblquote_group.group_id
						LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id 
						WHERE tblquote_line.quote_id='".$in['quote_id']."'
						AND tblquote_line.content_type='1'
						AND tblquote_version.active='1'
						AND (tblquote_line.article_id>'0' AND pim_articles.is_service='1')
						ORDER BY group_id ASC ");
				$j=1;
				$output['quote_group_services']=array();
				while($q_services->next()){
					$quote_service=true;
					$data_line = $this->db->query("SELECT tblquote_line.* FROM tblquote_line
								INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
								LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id  
								WHERE tblquote_line.quote_id='".$in['quote_id']."'
								AND tblquote_line.content_type='1'
								AND tblquote_line.line_type<'3'
								AND tblquote_version.active='1'
								AND (tblquote_line.article_id>'0' AND pim_articles.is_service='1')
								AND tblquote_line.group_id = '".$q_services->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;
					$temp_services=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$art=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'unit_price_nr'		=> $data_line->f('price'),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
							'article_id'		=> $data_line->f('article_id'),
							'code'			=> stripslashes($data_line->f('item_code')),
						);
						array_push($temp_services, $art);
						$total+=$amount;
					}
					$quote_group_services=array(
							'group_title'		=> $q_services->f('title') ? $q_services->f('title') : gm('Group').' '.$j,
							'nr_crt'			=> $j,
							'group_a_id'		=> $q_services->f('group_id'),
							'checked'		=> false,
							'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
							'services'			=> $temp_services,
							'total_free'		=> $total
					);
					//if($total>0){
						array_push($output['quote_group_services'], $quote_group_services);
						$j++;
					//}				
				}		
			}
			if($in['quote_group_articles'] && !empty($in['quote_group_articles'])){
				$output['quote_group_articles']=$in['quote_group_articles'];
				$quote_art=true;
			}else{
				$articles = $this->db->query("SELECT DISTINCT tblquote_line.group_id,tblquote_group.title FROM tblquote_line
						INNER JOIN tblquote_version
						ON tblquote_line.version_id = tblquote_version.version_id
						INNER JOIN tblquote_group ON tblquote_line.group_id = tblquote_group.group_id
						LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
						WHERE tblquote_line.quote_id='".$in['quote_id']."'
						AND tblquote_line.content_type='1'
						AND tblquote_version.active='1'
						AND (tblquote_line.article_id>'0' AND pim_articles.is_service='0' AND tblquote_line.is_tax='0')
						ORDER BY group_id ASC ");
				$j=1;
				$output['quote_group_articles']=array();
				while($articles->next()){					
					$data_line = $this->db->query("SELECT tblquote_line.* FROM tblquote_line
								INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
								LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
								WHERE tblquote_line.quote_id='".$in['quote_id']."'
								AND tblquote_line.content_type='1'
								AND tblquote_line.line_type<'3'
								AND tblquote_version.active='1'
								AND (tblquote_line.article_id>'0' AND pim_articles.is_service='0' AND tblquote_line.is_tax='0')
								AND tblquote_line.group_id = '".$articles->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;
					$temp_articles=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$art=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
							'article_id'		=> $data_line->f('article_id'),
							'price'				=> $data_line->f('price'),
							'sell_price'		=> $data_line->f('price'),
						);
						array_push($temp_articles, $art);
						$total+=$amount;
					}
					$quote_group_articles=array(
							'group_title'		=> $articles->f('title') ? $articles->f('title') : gm('Group').' '.$j,
							'nr_crt'			=> $j,
							'group_a_id'		=> $articles->f('group_id'),
							'checked'		=> false,
							'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
							'articles'			=> $temp_articles,
							'total_free'		=> $total
					);
					if($total>0){
						$quote_art=true;
						array_push($output['quote_group_articles'], $quote_group_articles);
						$j++;
					}				
				}		
			}		
		}else if($in['contract_id']){
			$contract_inf=$this->db->query("SELECT * FROM contracts WHERE contract_id='".$in['contract_id']."'");
			$contract_end_date=$this->db->field("SELECT end_date FROM contracts_version WHERE contract_id='".$in['contract_id']."' AND active='1' ");
			$output['contract_id']			= $in['contract_id'];
			$output['quote_hide']			= $in['contract_id'] ? false : true;
			$output['contract_end_date']		= $contract_end_date ? $contract_end_date*1000 : '';
			$output['contract_has_end']		= $contract_end_date ? true : false; 
			$output['customer']			= '';
			$output['contact']			= '';
			$output['customer_name']		= urldecode($in['customer']);
			$output['customer_readonly']		= true;
			$output['currency_type']		= $in['currency_type'];
			$output['source_id']			= $in['source_id'];
			$output['type_id']			= $in['type_id'];
			$output['segment_id']			= $in['segment_id'];
			$in['subject']				= $contract_inf->f('subject');
			$in['vat_regime_id']=$contract_inf->f('vat_regime_id');
			$output['main_address_id']		= $contract_inf->f('main_address_id');
			$output['sameAddress']			= '1';
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$contract_inf->f('main_address_id')."' ");
			$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
			$output['free_field']			= $free_field;
			$output['free_field_txt']		= nl2br($free_field);
			$in['email_language']			= $in['languages'];
			if($in['acc_manager_id']){
				$output['acc_manager_id']=$in['acc_manager_id'];
			}

			if($in['quote_group_data'] && !empty($in['quote_group_data'])){
				$output['quote_group_data']=$in['quote_group_data'];
				$quote_task=true;
			}else{
				$groups = $this->db->query("SELECT DISTINCT contract_line.group_id,contracts_group.title FROM contract_line
						INNER JOIN contracts_version
						ON contract_line.version_id = contracts_version.version_id
						INNER JOIN contracts_group ON contract_line.group_id = contracts_group.group_id
						LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
						WHERE contract_line.contract_id='".$in['contract_id']."'
						AND contract_line.content_type='1'
						AND contracts_version.active='1'
						AND contract_line.article_id='0'
						AND contract_line.is_tax='0'
						ORDER BY group_id ASC ");
				
				$output['quote_group_data']=array();
				while($groups->next()){					
					$data_line = $this->db->query("SELECT contract_line.* FROM contract_line
								INNER JOIN contracts_version ON contract_line.version_id = contracts_version.version_id
								LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
								WHERE contract_line.contract_id='".$in['contract_id']."'
								AND contract_line.content_type='1'
								AND contract_line.line_type<'3'
								AND contracts_version.active='1'
								AND contract_line.article_id='0'
								AND contract_line.is_tax='0'
								AND contract_line.group_id = '".$groups->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;
					$i=0;
					$temp_data_line=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$d_line=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
							'amount_nr'			=> $amount
						);
						array_push($temp_data_line, $d_line);
						$total+=$amount;
						$i++;
					}

					$quote_group_data=array(
						'group_title'		=> $groups->f('title') ? $groups->f('title') : gm('Group').' '.$i,
						'nr_crt'			=> $i,
						'group_id'			=> $groups->f('group_id'),
						'checked'		=> false,
						'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
						'data_line'			=> $temp_data_line,
						'total_free'		=> $total,
						'checked'			=> true,
					);
					if($i>0){
						$quote_task=true;
						array_push($output['quote_group_data'], $quote_group_data);
						
					}				
				}			
			}
			if($in['quote_group_services'] && !empty($in['quote_group_services'])){
				$output['quote_group_services']=$in['quote_group_services'];
				$quote_service=true;
			}else{
				$q_services = $this->db->query("SELECT DISTINCT contract_line.group_id,contracts_group.title FROM contract_line
						INNER JOIN contracts_version
						ON contract_line.version_id = contracts_version.version_id
						INNER JOIN contracts_group ON contract_line.group_id = contracts_group.group_id
						LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
						WHERE contract_line.contract_id='".$in['contract_id']."'
						AND contract_line.content_type='1'
						AND contracts_version.active='1'
						AND (contract_line.article_id>'0' AND pim_articles.is_service='1')
						ORDER BY group_id ASC ");
				$j=1;
				$output['quote_group_services']=array();
				while($q_services->next()){					
					$data_line = $this->db->query("SELECT contract_line.* FROM contract_line
								INNER JOIN contracts_version ON contract_line.version_id = contracts_version.version_id
								LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
								WHERE contract_line.contract_id='".$in['contract_id']."'
								AND contract_line.content_type='1'
								AND contract_line.line_type<'3'
								AND contracts_version.active='1'
								AND (contract_line.article_id>'0' AND pim_articles.is_service='1')
								AND contract_line.group_id = '".$q_services->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;
					$temp_services=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$art=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
							'article_id'		=> $data_line->f('article_id'),
							'code'			=> stripslashes($data_line->f('item_code')),
							'amount_nr'			=> $amount,
						);
						array_push($temp_services, $art);
						$total+=$amount;
					}
					$quote_group_services=array(
							'group_title'		=> $q_services->f('title') ? $q_services->f('title') : gm('Group').' '.$j,
							'nr_crt'			=> $j,
							'group_a_id'		=> $q_services->f('group_id'),
							'checked'		=> false,
							'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
							'services'			=> $temp_services,
							'total_free'		=> $total,
							'checked'			=> true,
					);
					if($total>0){
						$quote_service=true;
						array_push($output['quote_group_services'], $quote_group_services);
						$j++;
					}				
				}		
			}
			if($in['quote_group_articles'] && !empty($in['quote_group_articles'])){
				$output['quote_group_articles']=$in['quote_group_articles'];
				$quote_art=true;
			}else{
				$articles = $this->db->query("SELECT DISTINCT contract_line.group_id,contracts_group.title FROM contract_line
						INNER JOIN contracts_version
						ON contract_line.version_id = contracts_version.version_id
						INNER JOIN contracts_group ON contract_line.group_id = contracts_group.group_id
						LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
						WHERE contract_line.contract_id='".$in['contract_id']."'
						AND contract_line.content_type='1'
						AND contracts_version.active='1'
						AND (contract_line.article_id>'0' AND pim_articles.is_service='0')
						ORDER BY group_id ASC ");
				$j=1;
				$output['quote_group_articles']=array();
				while($articles->next()){					
					$data_line = $this->db->query("SELECT contract_line.* FROM contract_line
								INNER JOIN contracts_version ON contract_line.version_id = contracts_version.version_id
								LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
								WHERE contract_line.contract_id='".$in['contract_id']."'
								AND contract_line.content_type='1'
								AND contract_line.line_type<'3'
								AND contracts_version.active='1'
								AND (contract_line.article_id>'0' AND pim_articles.is_service='0')
								AND contract_line.group_id = '".$articles->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;
					$temp_articles=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$art=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
							'article_id'		=> $data_line->f('article_id'),
							'price'				=> $data_line->f('price'),
							'sell_price'		=> $data_line->f('price'),
						);
						array_push($temp_articles, $art);
						$total+=$amount;
					}
					$quote_group_articles=array(
							'group_title'		=> $articles->f('title') ? $articles->f('title') : gm('Group').' '.$j,
							'nr_crt'			=> $j,
							'group_a_id'		=> $articles->f('group_id'),
							'checked'		=> false,
							'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
							'articles'			=> $temp_articles,
							'total_free'		=> $total,
							'checked'			=> true,
					);
					if($total>0){
						$quote_art=true;
						array_push($output['quote_group_articles'], $quote_group_articles);
						$j++;
					}				
				}		
			}		
		}

		if(!$in['email_language']){
			$in['email_language'] = $user_lang_id;
		}

		$to_edit=false;
		$perm_admin1 = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_manager1 = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin1:
			case $perm_manager1:
				$to_edit=true;
				break;
		}

		$output['is_recurring']		= $in['is_recurring'] ? true : false;
		$output['email_language']	= $in['email_language'];
		$output['quote_task']		= $quote_task ? true : false;
		$output['quote_art']		= $quote_art ? true : false;
		$output['quote_service']	= $quote_service ? true : false;
		$output['quote_task_e']		= $quote_task;
		$output['quote_art_e']		= $quote_art;
		$output['do_next']		= $next_function;
		$output['cc']			= $this->get_cc($in);
		$output['contacts']		= $this->get_contacts($in);
		$output['addresses']		= $this->get_addresses($in);
		$in['site_add']			= true;
		$output['site_addresses']	= $this->get_addresses($in);
		$output['main_comp_info']	= getCompanyInfo($in);
		$output['user_auto']		= $this->get_autoUsers($in);
		$output['service_id']		= $in['service_id'];
		$output['add_customer']		= false;
		$output['subject']		= $in['subject'];
		$output['ACCOUNT_CURRENCY'] 	= get_currency(ACCOUNT_CURRENCY_TYPE);
		$output['service_type']		= $in['service_type'];
		$output['edit']			= true;
		$output['rec_start_date']=$in['rec_start_date'] ? strtotime($in['rec_start_date'])*1000 : '';
		$output['rec_end_date']=$in['rec_end_date'] ? strtotime($in['rec_end_date'])*1000 : '';
		$output['rec_frequency']=$in['rec_frequency'] ? $in['rec_frequency'] : '1';
		$output['frequency_list'] = build_frequency_list_new($in['rec_frequency']);
		$output['rec_days']=$in['rec_days'] ? $in['rec_days'] : '0';
		$output['rec_number']=$in['rec_number'] ? $in['rec_number'] : '';
		$output['ALLOW_STOCK']	= ALLOW_STOCK==1 ? true : false;
		$output['vat_regim_dd']			= build_vat_regime_dd();
		$output['vat_regime_id']=$in['vat_regime_id'];
		$output['can_change']=$to_edit;
		$output['identity_id']=$in['identity_id'];
		$output['multiple_identity_dd'] = build_identity_dd($in['identity_id']);
		$output['segment_dd']= get_categorisation_segment();
        	$output['source_dd']= get_categorisation_source();
        	$output['type_dd']= get_categorisation_type();
        	$output['segment_id']=$in['segment_id'];
        	$output['source_id']=$in['source_id'];
        	$output['type_id']=$in['type_id'];
        	$output['service_title']=$in['is_recurring'] ? gm('New recurring intervention') : gm('Add Intervention');
        $languages_dd=array();
		$languages=$this->db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order ");
		while($languages->next()){
			array_push($languages_dd,array('id'=>$languages->f('lang_id'),'name'=>gm($languages->f('language'))));
		}
		$output['langs_dd']				= $languages_dd;
		if(!$in['is_recurring']){
			$output['color_ball_dd']=build_color_ball_dd();
		}		
		$output['color_ball']=$in['color_ball'] ? $in['color_ball'] : '#322B78';
		$output['your_ref'] = $in['your_ref'] ? $in['your_ref'] : '';
		$output['date_pick_format'] = pick_date_format();
		$output['articles_list']	= $this->get_articles_list($in);
		$in['only_service']		= true;
		$output['services_list']	= $this->get_articles_list($in);
		$output['installations']	= $this->get_installations($in);

//		$output['users'] = $in['users'] ? $in['users'] : array('users'=>array(),'is_data'=>0);
		$output['users'] = $this->get_serviceUser($in);
		$output['expense'] = $in['expense'] ? $in['expense'] : array('expense'=>array(),'total'=>0,'is_data'=>0);  
//		$output['tasks'] = $in['tasks'] ? $in['tasks'] : array('tasks'=>array(),'is_data'=>0);
		$output['tasks']			= $this->get_serviceSubTasks($in);
//		$output['services'] = $in['services'] ? $in['services'] : array('services'=>array(),'is_data'=>0);
		$output['services']		= $this->get_serviceSubServices($in);
//		$output['supplies']=$in['supplies'] ? $in['supplies'] : array('supplies'=>array(),'is_data'=>0);
		$output['supplies'] = $this->get_serviceSupplies($in);
//		$output['articles']=$in['articles'] ? $in['articles'] : array('articles'=>array(),'is_data'=>0);
		$output['articles']		= $this->get_serviceArticles($in);
		$articles_denied=$this->db_users->field("SELECT plan FROM users WHERE user_id='".$_SESSION['u_id']."' ");

		$output['articles_allowed']= ($articles_denied==2 || $articles_denied==5) ? false : true;
		$output['can_edit']	= true;
		$output['allow_model'] = true;
		$has_admin_rights 					= getHasAdminRights(array('module'=>'maintenance'));
		$deny_view_article_price=false;
		if(!$has_admin_rights && defined('NO_ACCESS_PRICE_ARTICLE_INT_REG_USER') && NO_ACCESS_PRICE_ARTICLE_INT_REG_USER =='1'){
			$deny_view_article_price=true;
		}
		$output['deny_view_article_price']=$deny_view_article_price;
		$output['inst_edit']=in_array('17', perm::$allow_apps) ? true : false;
		$this->out = $output;
	}

	public function getTemplateService(){
		//
	}

	public function getEditService(){
		$in=$this->in;
		global $config;
		$output=array();
		if($in['template_id'] && is_numeric($in['template_id'])){
			$in['service_id']=$in['template_id'];
		}else{
			ark::run('maintenance--maintenance-external_id');
		}
		if ($in['duplicate_service_id']) {
			$in['service_id'] = $in['duplicate_service_id'];
		}
		
		$service =	$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");

		if(!$in['email_language']){
			$in['email_language'] = $this->db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1' ");
		}
		if($in['email_language'] >=1000) {
			$lang_code = $this->db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$in['email_language'] ."' GROUP BY code ");
		} else {
			$lang_code = $this->db->field("SELECT language FROM pim_lang WHERE lang_id='".$in['email_language'] ."' GROUP BY code ");
		}

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_man=$this->db_users->field("SELECT value FROM user_meta WHERE name='intervention_admin' AND user_id='".$_SESSION['u_id']."' ");
		$is_service_manager = $this->db->field("SELECT pr_m FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' AND service_id='".$in['service_id']."' ");
		$int_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $is_service_manager && $_SESSION['team_int']:
			case $int_user:
				$can_edit = true;
				break;
			default:
				$can_edit = false;
				break;
		}
		$articles_denied=$this->db_users->field("SELECT plan FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$article_locations=$this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
		$article_stock_warning=$this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_STOCK_WARNING'");
		$use_stock_reservation=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_STOCK_RESERVATION'");
		$in['cat_id']= $service->f('customer_id') !=0 ? $this->db->field('SELECT cat_id FROM customers WHERE customer_id ='.$service->f('customer_id')) : 0;

		$in['buyer_id']=$service->f('customer_id');

		$in['contact_id']=$service->f('contact_id');
		$site_address_id= $service->f('same_address');
		if($service->f('customer_id')){
			if($site_address_id){
				$address= $this->db->query("SELECT customer_addresses.*,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id									 
									 WHERE customer_addresses.address_id='".$site_address_id."' ");
			}else{
				$address= $this->db->query("SELECT customer_addresses.*,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id									
									 WHERE customer_addresses.address_id='".$service->f('main_address_id')."' ");
			}

			while($address->next()){
				$site_address = $address->f('address');
				$site_zip = $address->f('zip');
				$site_city = $address->f('city');
				$site_country = $address->f('country');
			}

			
		}

		$invoice_link=array();
		$invoice_link['service_id']=$in['service_id'];
		$invoice_link['base_type']=6;
		$invoice_link['languages']=$in['email_language'];
		if($service->f('customer_id')){
			$invoice_link['buyer_id']=$service->f('customer_id');
			$invoice_link['customer_name']=$service->f('customer_name');
			$invoice_link['contact_id']=$service->f('contact_id');
		}else{
			$invoice_link['buyer_id']=0;
			$invoice_link['contact_id']=$service->f('contact_id');
			$invoice_link['contact_name']=$service->f('contact_id') ? $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$service->f('contact_id')."' ") : '';
		}

		$toTasks = $this->db->field("SELECT count(task_id) FROM servicing_support_tasks WHERE service_id = '".$in['service_id']."' AND closed='0' ");
		$toSupply=$this->db->field("SELECT count(a_id) FROM servicing_support_articles WHERE service_id = '".$in['service_id']."' AND quantity>delivered AND article_id='0' ");
		$generalArt = $this->db->field("SELECT SUM(quantity) FROM servicing_support_articles WHERE service_id = '".$in['service_id']."' AND article_id!='0' ");
		$artDel=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' ");
		$toArt=$generalArt-$artDel;
		$show_inv_create=true;
		if(!$service->f('billable')){
			$intv_supply = $this->db->field("SELECT COUNT(a_id) FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND delivered<>0 AND billable='1' AND article_id='0' ");
			$intv_article = $this->db->field("SELECT COUNT(service_delivery.a_id) FROM service_delivery
				INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
				WHERE service_delivery.service_id='".$in['service_id']."' AND service_delivery.invoiced='0' AND servicing_support_articles.billable='1' AND servicing_support_articles.article_id!='0' GROUP BY service_delivery.a_id");
			$intv_expense = $this->db->field("SELECT COUNT(project_expenses.id) FROM project_expenses
									INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
									INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
									WHERE servicing_support.service_id='".$in['service_id']."' AND project_expenses.billable='1' ORDER BY id");
			if(!$intv_supply && !$intv_article && !$intv_expense){
				$show_inv_create=false;
			}
		}
		if($service->f('billed')){
			$show_inv_create=false;
		}

		$show_down=true;
		if(!$service->f('billable')){
			$intv_supply = $this->db->field("SELECT COUNT(a_id) FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND billable='1' ");
			if(!$intv_supply){
				$show_down=false;
			}
		}
		$to_edit=false;
		$perm_admin1 = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_manager1 = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin1:
			case $perm_manager1:
				$to_edit=true;
				break;
		}
		
		$existIdentity = $this->db->field("SELECT COUNT(identity_id) FROM multiple_identity ");

		$output['is_recurring']=$service->f('is_recurring') ? true : false;
		$output['rec_start_date']=$service->f('rec_start_date')*1000;
		$output['rec_end_date']=$service->f('rec_end_date') ? $service->f('rec_end_date')*1000 : '';
		$output['rec_frequency']=$service->f('rec_frequency');
		$output['rec_number']=$service->f('rec_number');
		$output['frequency_list'] = build_frequency_list_new($service->f('rec_frequency'));
		$output['rec_days']=$service->f('rec_days');
		$output['end_contract']=$service->f('end_contract') ? true : false;
		$output['advance_day']=$service->f('advance_day');
		if($service->f('is_recurring')){
			$contract_end_date=$this->db->field("SELECT end_date FROM contracts_version WHERE contract_id='".$service->f('contract_id')."' AND active='1' ");
			$output['contract_has_end']		= $contract_end_date ? true : false; 
			$int_planned=$this->db->field("SELECT COUNT(id) FROM servicing_recurring WHERE service_id='".$in['service_id']."' ");
		}
		if($int_planned){
			$ref_first_day=$this->db->field("SELECT `date` FROM servicing_recurring WHERE service_id='".$in['service_id']."' AND done='0' ORDER BY `date` ASC LIMIT 1");
			if($ref_first_day){
				if($ref_first_day<time()){
					$output['rec_first_day']=time();
				}else{
					$output['rec_first_day']=$ref_first_day;
				}	
			}
		}
		if($service->f('billed')){
			$billed_invoice_id=$this->db->query("SELECT tracking.target_id FROM tracking_line
				INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
				WHERE tracking_line.origin_id='".$in['service_id']."' AND tracking_line.origin_type='5' AND tracking.target_type='1' ");
			while($billed_invoice_id->next()){
				$inv_downpayment = $this->db->field("SELECT id FROM  `tbldownpayments` WHERE  `invoice_id` ='".$billed_invoice_id->f('target_id')."' ");
				if($inv_downpayment){
					continue;
				}
				$output['billed_invoice_id'] = $billed_invoice_id->f('target_id');
				$output['invoice_sn']=$this->db->field("SELECT serial_number FROM tblinvoice WHERE id='".$output['billed_invoice_id']."' ");
			}
		}	
		$output['has_planning']=$int_planned ? true : false;

		$last_step=false;
		$recurring_replan=false;
		if($service->f('is_recurring')){
			if(!$service->f('last_step')){
				$last_step=true;
			}/*else{
				$planned_in_advance=$this->db->field("SELECT id FROM servicing_recurring WHERE service_id='".$in['service_id']."' AND `done`='1' ORDER BY `date` ASC LIMIT 1");
				if(!$planned_in_advance && !$service->f('time_run')){
					$last_step=true;
					$recurring_replan=true;
				}
			}	*/	
			//removed in AKTI4318 
		}

		$output['last_step']=$last_step;
		$output['recurring_replan']=$recurring_replan;
		$output['is_team']=$_SESSION['team_int'];
		$output['email_language_txt'] = gm($lang_code);
		$output['item_id']=$in['service_id'];
		$output['user_id'] = $service->f('user_id');
		$output['user_name'] = $service->f('user_name');
		$output['customer_id'] = $service->f('customer_id');
		$output['buyer_id']=$service->f('customer_id');
		$output['contact_id'] = $service->f('contact_id');
		$output['contract_id'] = $service->f('contract_id');
		$output['service_ctype'] = $service->f('service_ctype');
		$output['project_id'] = $service->f('project_id');
		//$output['task_id'] = $service->gf('task_id');
		$output['type'] = $service->f('type');
		$output['date_ts'] = $service->f('date');
		$output['planeddate_ts'] = $in['duplicate_service_id'] ? '' : $service->f('planeddate');
		$output['planeddate_ts_js'] = $service->f('planeddate')*1000;
		$output['planeddate_js'] = $in['duplicate_service_id'] ? '' : ($in['planeddate_js'] ? strtotime($in['planeddate_js'])*1000 : ($service->f('planeddate') ? ($service->f('planeddate')-$_SESSION['user_timezone_offset'])*1000 : ''));
		$output['free_field'] = $service->f('free_field');
		$output['customer_phone'] = $service->f('customer_phone');
		$output['customer_email'] = $service->f('customer_email');
			// $output['startdate_ts'] = $service->gf('startdate');
		$output['enddate_ts'] = $service->f('enddate');
		$output['customer_name'] = $service->f('customer_name');
		$output['contact_name'] = $service->f('contact_id') ? $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$service->f('contact_id')."' ") : '';
		$output['project_name'] = $service->f('project_name');
		//$output['task_name'] = $service->gf('task_name');
		$output['contract_name'] = $service->f('contract_name');
		$output['subject'] = $service->f('subject');
		$output['duration'] = $in['duplicate_service_id'] ? '' : ($in['duration'] ? number_as_hour($in['duration']) : ($service->f('duration') ? number_as_hour($service->f('duration')) : ''));
		$output['report'] = $service->f('report');
//		$output['planeddate'] = $service->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$service->f('planeddate')) : '';
		$output['date'] = $service->f('date') ? date(ACCOUNT_DATE_FORMAT,$service->f('date')) : '';
		$output['startdate'] = $in['duplicate_service_id'] ? '' : ($in['startdate'] ? number_as_hour($in['startdate']) : ($service->f('startdate') ? number_as_hour($service->f('startdate')) : ''));
		$output['sdate'] = $service->f('startdate');
		$output['enddate'] = $service->f('enddate') ? date(ACCOUNT_DATE_FORMAT,$service->f('enddate')) : '';
		$output['cat_id'] = $service->f('customer_id') ? $this->db->field('SELECT cat_id FROM customers WHERE customer_id ='.$service->gf('customer_id')) : 0;
		$output['email_language'] = $service->f('email_language');
		$output['email_lg']=$lang_code;
		$output['finishDate'] = $service->f('finishDate') ? date(ACCOUNT_DATE_FORMAT,$service->gf('finishDate')) : '';
		$output['finishDate_ts'] = $service->f('finishDate');
		$output['finishDate_ts_js'] = $service->f('finishDate')*1000;
		$output['billable'] = $service->f('billable') == 1 ? true : false;
		$output['billable_txt'] = $service->f('billable') == 1 ? gm('Yes') : gm('No');
		$output['status'] = $service->f('status');
		$output['rate'] = display_number($service->f('rate'));
		$output['price']= display_number($service->f('price'));
		$output['service_type']= $service->f('service_type');
		$output['shropdf'] = $service->f('shropdf');
		$output['serial_number'] = $in['duplicate_service_id'] ? generate_service_number() : $service->f('serial_number');
		$output['language_dd2'] = build_language_dd_new($service->f('email_language'));
		$output['date_pick_format'] = pick_date_format();
		$output['loged_user'] = $this->db->field("SELECT user_id FROM servicing_support_users WHERE user_id='{$_SESSION['u_id']}'");
		$output['pdf_link']= 'index.php?do=maintenance-print&service_id='.$in['service_id'].'&lid='.$service->f('email_language');
		$output['invoice_link']= $invoice_link;
		$output['rating'] = $service->f('rating');
		$output['edit'] = $can_edit ? ($service->f('status') < 2 ? true : false) : false;
		$output['can_edit']	= $can_edit;
		$output['inst_edit']=in_array('17', perm::$allow_apps) ? true : false;
		$output['not_accepted'] = $service->f('accept') == 1 || $service->f('billed') == 1 ? false : true;
		$output['field']= $service->f('customer_id') ? 'customer_id' : 'contact_id';
		$output['main_address_id']= $service->f('main_address_id');
		$output['sameAddress']= !$service->f('same_address') ? true : false;
		$output['delivery_address_id']= $service->f('same_address');
		$output['articles_allowed']= ($articles_denied==2 || $articles_denied==5) ? false : true;	
		$output['cat_id']	= $in['cat_id'];	
		$output['article_loc']	= $article_locations==1? true : false;
		$output['article_stock_warn'] = $article_stock_warning==1 ? true : false;
		$output['use_stock_reservation'] = $use_stock_reservation==1 ? true : false;
		$output['can_duplicate']= ($toTasks > 0 || $toSupply>0 || $toArt > 0) ? true : false;
		$output['service_id']=$in['service_id'];
		$output['multiple_identity_txt'] = $this->db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$service->gf('identity_id')."'");
		$output['multiple_identity_dd'] = build_identity_dd($service->f('identity_id'));
		$output['existIdentity']= $existIdentity > 0 ? true : false;
		$output['identity_id']=$service->f('identity_id');
		$in['identity_id']=$service->f('identity_id');
		$output['author_id']=$service->f('author_id');
		$output['acc_manager_id']= $service->f('acc_manager_id');
		$output['authors']= $this->get_author($in);
		$output['accmanager']= $this->get_accmanager($in);
		$output['show_inv_create']=$show_inv_create;
		$output['show_down']=$show_down;
		$output['gender_dd'] = gender_dd();
		$output['title_dd'] = build_contact_title_type_dd(0);
		//$output['drop_info']= array('drop_folder' => 'interventions', 'customer_id' => $service->f('customer_id'), 'item_id' => $in['service_id'], 'isConcact' => '0', 'serial_number' => $service->f('serial_number'));
		$output['s3_info']=array('s3_folder'=>'intervention','id'=>$in['service_id']);
		$output['vat_regim_dd']			= build_vat_regime_dd();
		$output['vat_regime_id']		= $service->f('vat_regime_id') ? $service->f('vat_regime_id') : '';
		$output['can_change']=$to_edit;
		$output['segment_dd']= get_categorisation_segment();
		$output['source_dd']= get_categorisation_source();
    	$output['type_dd']= get_categorisation_type();
    	$output['segment_id']=$service->f('segment_id');
    	$output['source_id']=$service->f('source_id');
    	$output['type_id']=$service->f('type_id');
    	$output['cost_centre']=$service->f('cost_centre');
    	$output['deals']= $this->get_deals($in);
    	$output['ADV_CRM'] = defined('ADV_CRM') && ADV_CRM == 1 ? true : false;
    	if($service->f('trace_id')){
			$linked_doc=$this->db->field("SELECT tracking_line.origin_id FROM tracking_line
				INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
				WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$service->f('trace_id')."' ");
			if($linked_doc){
				$output['deal_id']=$linked_doc;	
			}
		}

		if($service->f('status')=='1'){
		    $type_title = gm('Planned');
		}elseif($service->f('status')=='2'){
		    if($service->f('is_recurring')=='0'&& $service->f('active')=='1'&& $service->f('accept')=='1'){
		        $type_title = gm('Accepted');
		    }else{
		        $type_title = gm('Closed');
		    }
		}else{
		    $type_title = gm('Draft');
		}
		$nr_status= $service->f('status');
		if(!$service->f('active')){
			$type_title=gm('Archived');
			$nr_status=0;
		}
		$output['type_title']=$type_title;
		$output['nr_status']=$nr_status;

		if ($in['duplicate_service_id']) {
			$output['nr_status'] = 0;
			$output['type_title'] = gm('Draft');
		}


		if(defined('USE_MAINTENANCE_WEB_LINK') && USE_MAINTENANCE_WEB_LINK == 1){
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['service_id']."' AND `type`='m'  ");
			if(defined('WEB_MAINTENANCE_INCLUDE_PDF') && WEB_MAINTENANCE_INCLUDE_PDF == 0){
				//$in['include_pdf'] = false;
			}
		}

		$message_data=get_sys_message('servicemess',$service->f('email_language'));
		$subject=$message_data['subject'];

		$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
		$subject=str_replace('[!SERIAL_NUMBER!]',$service->f('serial_number'), $subject);
		$subject=str_replace('[!INTERVENTION_DATE!]',$output['planeddate'], $subject);
		$subject=str_replace('[!INTERVENTION_HOUR!]',$output['startdate'], $subject);
		$subject=str_replace('[!CUSTOMER!]',$service->f('customer_name'), $subject);

		$body=$message_data['text'];
		$body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
		$body=str_replace('[!CUSTOMER!]',$service->f('customer_name'), $body);
		$body=str_replace('[!SERIAL_NUMBER!]',$service->f('serial_number'), $body);
		$body=str_replace('[!INTERVENTION_DATE!]',$output['planeddate'], $body);
		$body=str_replace('[!INTERVENTION_HOUR!]',$output['startdate'], $body);

		$body=str_replace('[!SITE_ADDRESS!]',$site_address, $body);
		$body=str_replace('[!SITE_ZIP!]',$site_zip, $body);
		$body=str_replace('[!SITE_CITY!]', $site_city, $body);
		$body=str_replace('[!SITE_COUNTRY!]', $site_country, $body);

		if($message_data['use_html']){
		    $body=str_replace('[!SIGNATURE!]',get_signature(), $body);
		 }else{
			  $body=str_replace('[!SIGNATURE!]','', $body);
		}

		/*$output['e_subject'] = $subject;
		$output['e_message'] = $body;*/
		$output['e_subject']              = $in['e_subject'] ? $in['e_subject'] : $subject;
		$output['e_message']            = $in['e_message'] ? stripslashes($in['e_message']) : $body;

		$account_manager_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
	    $account_user_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$service->f('acc_manager_id')."'");
		$output['use_html']	= $message_data['use_html'];
		$output['recipients']		= $this->get_recipients($in);
		
		if($in['contact_id']){
			if($service->f('customer_id')){
				$contact_email = $this->db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND customer_id = '".$service->f('customer_id')."'");
			}else{
				$contact_email = $this->db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."'");
			}
			if($contact_email){
				$cc_email = $contact_email;
			}else{
				$customer_email = $this->db->field("SELECT c_email FROM customers WHERE customer_id='".$service->f('customer_id')."'");
				if($customer_email){
					$cc_email = $customer_email;
				}
			}
		}else{
			$customer_email = $this->db->field("SELECT c_email FROM customers WHERE customer_id='".$service->f('customer_id')."'");
				if($customer_email){
					$cc_email = $customer_email;
				}
		}

		$customer_phone=$this->db->field("SELECT customers.comp_phone FROM customers WHERE customer_id='".$service->f('customer_id')."' ");
		if($customer_phone){
			$output['customer_phone']='<i class="fa fa-phone" aria-hidden="true"></i> <span>'.$customer_phone.'</span>';
		}
		
		if($service->f('contact_id')){
			$contact_data=$this->db->query("SELECT customer_contactsIds.phone,customer_contacts.cell FROM customer_contactsIds
			 LEFT JOIN customer_contacts ON customer_contactsIds.contact_id=customer_contacts.contact_id
			 WHERE customer_contactsIds.customer_id='".$in['buyer_id']."' AND customer_contactsIds.contact_id='".$service->f('contact_id')."' ");
			if($contact_data->f('phone') || $contact_data->f('cell')){
				$contact_phone='';
				if($contact_data->f('cell')){
					$contact_phone='<i class="fa fa-mobile" aria-hidden="true"></i> <span>'.$contact_data->f('cell').'</span>';
				}else if($contact_data->f('phone')){
					$contact_phone='<i class="fa fa-phone" aria-hidden="true"></i> <span>'.$contact_data->f('phone').'</span>';
				}
				$output['contact_phone']=$contact_phone;				
			}
	    }

	if(!isset($in['include_pdf']))
		{
			$in['include_pdf'] = true;
		}

		$output['email'] = array(
			'e_subject'			=> $subject,
			'dropbox_files'		=> '',
			'language_email' 	=> $service->f('email_language'),
			'e_message' 		=> $in['e_message'] ? $in['e_message'] : ($body),
			'lid'				=> $service->f('email_language'),
			'include_pdf'		=> $in['include_pdf'] ? true : false,
			'id'				=> $in['service_id'],
			'service_id'		=> $in['service_id'],
			'version_id'		=> $this->version_id,
			'files'				=> array(),
			'use_html'			=> $message_data['use_html'],
			'serial_number'		=> $service->f('serial_number'),
			/*'recipients'		=> array_map(function($field){
										return $field['email'];
									},$output['recipients']),*/
			'recipients'		=> $cc_email,
			'save_disabled'		=> $cc_email? false:true,
			'alerts'			=> array(),
			'user_loged_email'	=> $account_manager_email,
			'user_acc_email'	=> $account_user_email,
// mail la userul logat.
		);


	

		$free_field = $service->gf('customer_address').'
		'.$service->gf('customer_zip').' '.$service->gf('customer_city').'
		'.get_country_name($service->gf('customer_country_id'));

		$downpayment_data=$this->db->query("SELECT tbldownpayments.invoice_id,tblinvoice.serial_number FROM tbldownpayments 
			INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
			WHERE tbldownpayments.service_id='".$in['service_id']."' AND tblinvoice.f_archived='0' ");

		$output['service_title']	= $service->f('is_recurring') ? gm('Recurring intervention') : gm('Intervention');
		$output['installations']	= $this->get_installations($in);
		$output['installation_id']  = $service->f('installation_id');
		$output['old_installation_id']  = $service->f('installation_id');
		$output['downpayment_made']=$downpayment_data->f('invoice_id') ? true : false;
		$output['downpayment_invoice']=$downpayment_data->f('invoice_id');
		$output['downpayment_sn']=$downpayment_data->f('serial_number');
		$output['country_dd']		= $service->f('customer_country_id')? build_country_list($service->f('customer_country_id')):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['main_country_id']	= $service->f('customer_country_id') ? $service->f('customer_country_id') : ACCOUNT_BILLING_COUNTRY_ID;
		$output['buyer_name']         = $service->f('customer_name');
		$output['shropdf'] 		= $service->f('shropdf') == 1 ? true :false ;
		$output['free_field']		= $service->f('free_field') ? $service->f('free_field') : $free_field;
		$output['free_field_txt']	= trim($service->f('free_field')) ? nl2br($service->f('free_field')) : nl2br($free_field );
		$output['web_url']		= $config['web_link_url'].'?q=';
		$output['external_url']		= $service->f('status') <= 1 ? false : $exist_url;
		$output['customer_address']	= $customer_address;
		$output['users']			= $this->get_serviceUser($in);
		$output['hours'] 			= $this->get_serviceHours($in);
		$output['expense']		= $this->get_serviceExpense($in);
		$output['tasks']			= $this->get_serviceSubTasks($in);
		$output['supplies']		= $this->get_serviceSupplies($in);
		$output['user_auto']		= $this->get_autoUsers($in);
		$output['autoSrvUsers']		= $this->get_autoSrvUsers($in);
		$output['expenseCat']		= $this->get_expenseCat($in);
		
		$output['main_comp_info']	= getCompanyInfo($in);
		$output['addresses']		= $this->get_addresses($in);
		$in['site_add']			= true;
		$output['site_addresses']	= $this->get_addresses($in);
		$output['contacts']		= $this->get_contacts($in);
		$output['cc']			= $this->get_cc($in);
		$output['articles']		= $this->get_serviceArticles($in);
		$output['articles_list']	= $this->get_articles_list($in);
		$output['ACCOUNT_CURRENCY'] 	= get_currency(ACCOUNT_CURRENCY_TYPE);
		$output['services']		= $this->get_serviceSubServices($in);
		$in['only_service']		= true;
		$output['services_list']	= $this->get_articles_list($in);

		$output['ALLOW_STOCK']	= ALLOW_STOCK==1 ? true : false;
		$output['include_pdf']	= $in['include_pdf'];

		if(!$service->f('status')){	
			$in['planeddate_js'] = $in['planeddate_js'] ? strtotime($in['planeddate_js'])*1000 : ($service->f('planeddate') ? ($service->f('planeddate')-$_SESSION['user_timezone_offset'])*1000 : '');
			$output['planning']=$this->get_planUsers($in);
		}
		$drop_connected=$this->db->field("SELECT active FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
		$output['drop_active']=$drop_connected ? true : false;
		$output['template_name']=$service->f('serial_number');
		$languages_dd=array();
		$languages=$this->db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order ");
		while($languages->next()){
			array_push($languages_dd,array('id'=>$languages->f('lang_id'),'name'=>gm($languages->f('language'))));
		}
		$output['langs_dd']				= $languages_dd;
		if($service->f('active')==2){
			$output['template_id']		= $service->f('service_id');
		}
		//$output['allow_model']=!$service->f('model_id') ? true : false;
		$output['allow_model']=true;

		$mail_setting_option = $this->db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
		$output['sendgrid_selected'] = false;
		if($mail_setting_option==3){
			$output['sendgrid_selected'] =true;
		}
		if(!$service->f('is_recurring')){
			$output['color_ball_dd']=build_color_ball_dd();
		}		
		$output['color_ball']='#'.$service->f('color_ball');
		$output['your_ref'] = stripslashes($service->f('your_ref'));
		$has_admin_rights 					= getHasAdminRights(array('module'=>'maintenance'));
		$deny_view_article_price=false;
		if(!$has_admin_rights && defined('NO_ACCESS_PRICE_ARTICLE_INT_REG_USER') && NO_ACCESS_PRICE_ARTICLE_INT_REG_USER =='1'){
			$deny_view_article_price=true;
		}
		$output['deny_view_article_price']=$deny_view_article_price;
		$output['duplicate_service_id'] = $in['duplicate_service_id'] ?: '';

		$this->out = $output;
	}


	public function get_serviceUser(&$in){
			$log_user_on_int = false;
			$usersSql = $this->db->query("SELECT servicing_support_users.* FROM servicing_support_users WHERE servicing_support_users.service_id='" . $in['service_id'] . "' ORDER BY sort_order ASC ");
			$users = array('users' => array());
			$i = 0;
			$total_hours = 0;
			$no_access = 0;
			while ($usersSql->next()) {
				$user_creds = $this->db_users->field("SELECT credentials FROM users WHERE user_id='" . $usersSql->f('user_id') . "' ");
				$user_cred = explode(';', $user_creds);
				if (!in_array('13', $user_cred)) {
					$no_access++;
				}
				if ($usersSql->f('user_id') == $_SESSION['u_id'] && $usersSql->f('pr_m') == 1) {
					$log_user_on_int = true;
				}
				$in['user_id'] = $usersSql->f('user_id');
				$hour_data = $this->db->field("SELECT SUM(end_time-start_time-break) FROM servicing_support_sheet WHERE service_id='" . $usersSql->f('service_id') . "' AND user_id='" . $usersSql->f('user_id') . "' ");
				$exp_data = $this->db->field("SELECT SUM(CASE WHEN expense.unit_price > 0 THEN expense.unit_price*project_expenses.amount ELSE project_expenses.amount END) FROM project_expenses
				LEFT JOIN expense ON project_expenses.expense_id=expense.expense_id 
				WHERE project_expenses.service_id='" . $usersSql->f('service_id') . "' AND project_expenses.user_id='" . $usersSql->f('user_id') . "' ");
				$user = array(
					'name' => get_user_name($usersSql->f('user_id')),
					'id' => $usersSql->f('u_id'),
					'user_id' => $usersSql->f('user_id'),
					'pr_m' => $usersSql->f('pr_m') == 1 ? true : false,
					'hours' => ($hour_data && $hour_data > 0) ? number_as_hour($hour_data) : '00:00',
					'expences' => $exp_data ? place_currency(display_number($exp_data)) : place_currency(display_number(0)),
					'hours_data' => $this->get_serviceHours($in),
					'expences_data' => $this->get_serviceExpense($in),
				);
				array_push($users['users'], $user);
				$total_hours += $hour_data;
				$i++;
			}
			if ($no_access == 1) {
				$users['allow_submit'] = true;
			} else if ($no_access > 1) {
				if ($log_user_on_int) {
					$users['allow_submit'] = true;
				} else {
					$users['allow_submit'] = false;
				}
			} else {
				$users['allow_submit'] = false;
			}
		$users['is_data']=$i;
		$users['total_hours']=number_as_hour($total_hours);

		if ($in['service_id'] == 'tmp') {
			$log_user_on_int = false;
			$users = array('users' => array());
			$i = 0;
			$total_hours = 0;
			$no_access = 0;
			foreach ($in['list'] as $idx => $data) {
				if ($data['checked']) {
					$usersSql = $this->db->query("SELECT servicing_support_users.* FROM servicing_support_users WHERE servicing_support_users.service_id='" . $data['service_id'] . "' ORDER BY sort_order ASC ");
					while ($usersSql->next()) {
						$user_creds = $this->db_users->field("SELECT credentials FROM users WHERE user_id='" . $usersSql->f('user_id') . "' ");
						$user_cred = explode(';', $user_creds);
						if (!in_array('13', $user_cred)) {
							$no_access++;
						}
						if ($usersSql->f('user_id') == $_SESSION['u_id'] && $usersSql->f('pr_m') == 1) {
							$log_user_on_int = true;
						}
						$in['user_id'] = $usersSql->f('user_id');
						$hour_data = $this->db->field("SELECT SUM(end_time-start_time-break) FROM servicing_support_sheet WHERE service_id='" . $usersSql->f('service_id') . "' AND user_id='" . $usersSql->f('user_id') . "' ");
						$exp_data = $this->db->field("SELECT SUM(CASE WHEN expense.unit_price > 0 THEN expense.unit_price*project_expenses.amount ELSE project_expenses.amount END) FROM project_expenses
						LEFT JOIN expense ON project_expenses.expense_id=expense.expense_id 
						WHERE project_expenses.service_id='" . $usersSql->f('service_id') . "' AND project_expenses.user_id='" . $usersSql->f('user_id') . "' ");
						$user = array(
							'name' => get_user_name($usersSql->f('user_id')),
							'id' => $usersSql->f('u_id'),
							'user_id' => $usersSql->f('user_id'),
							'pr_m' => $usersSql->f('pr_m') == 1 ? true : false,
							'hours' => ($hour_data && $hour_data > 0) ? number_as_hour($hour_data) : '00:00',
							'expences' => $exp_data ? place_currency(display_number($exp_data)) : place_currency(display_number(0)),
							'hours_data' => $this->get_serviceHours($in),
							'expences_data' => $this->get_serviceExpense($in),
						);
						array_push($users['users'], $user);
						$total_hours += $hour_data;
						$i++;
					}
				}
			}
			if ($no_access == 1) {
				$users['allow_submit'] = true;
			} else if ($no_access > 1) {
				if ($log_user_on_int) {
					$users['allow_submit'] = true;
				} else {
					$users['allow_submit'] = false;
				}
			} else {
				$users['allow_submit'] = false;
			}
			$users['is_data']=$i;
			$users['total_hours']=number_as_hour($total_hours);
		}
		return $users;
	}
	public function get_serviceHours(&$in){
		if($in['user_id']){
			$filter=" AND servicing_support_sheet.user_id='".$in['user_id']."' ";
		}
		$total = 0;
		$sheet = $this->db->query("SELECT servicing_support.*,servicing_support_sheet.*, servicing_support_sheet.date AS date2  FROM servicing_support_sheet
												 LEFT JOIN servicing_support ON servicing_support_sheet.service_id=servicing_support.service_id
												 WHERE servicing_support.service_id='".$in['service_id']."' ".$filter." ");
		$hours = array('hours'=>array(),'total'=>0);
		$i=0;
		while ($sheet->next()) {
			$total +=($sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break'));
			$hour = array(
				'DATE'						=> date(ACCOUNT_DATE_FORMAT,$sheet->f('date2')),
				'c_name'					=> get_user_name($sheet->f('user_id')),
				'user_id'					=> $sheet->f('user_id'),
				'serial_number'				=> $sheet->f('serial_number'),
				'hours'						=> number_as_hour($sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break')),
				'break'						=> number_as_hour($sheet->f('break')),
				'id'						=> $sheet->f('id'),
				'service_id'				=> $sheet->f('service_id'),
				'tmsmp'						=> $sheet->f('date2'),
				'tmsmp_js'						=> ($sheet->f('date2')+10800)*1000,
				'start_time'				=> number_as_hour($sheet->f('start_time')),
				'end_time'					=> number_as_hour($sheet->f('end_time')),
				'VIEW_L'					=> $sheet->f('status') != 2 ? "" : 'hide',
				'delete_link'				=> 'index.php?do=maintenance-services_ts_loop-maintenance-deleteServiceSheet&serv_sheet_id='.$sheet->f('id').'&user_id='.$in['user_id'].'&servicing_sheet_id='.$sheet->f('service_id').'&service_date='.$sheet->f('date2').'&close_srv='.$in['close_srv'],
				'time_report_txt'			=> nl2br(stripslashes($sheet->f('notes'))),
				'time_report'				=> stripslashes($sheet->f('notes')),
				'internal_report_txt'		=> $sheet->f('internal_report') ? gm('Yes') : gm('No'),
				'internal_report'			=> $sheet->f('internal_report') ? true : false,
				'extra_hours'				=> $sheet->f('extra_hours') ? true : false,
				'extra_hours_txt'			=> $sheet->f('extra_hours') ? gm('Yes') : gm('No'),
			);
			array_push($hours['hours'], $hour);
			$i++;
		}

		$hours['total'] = number_as_hour($total);
		$hours['is_data']=$i;
		return $hours;
	}
	public function get_serviceExpense(&$in){
		global $config;
		$total = 0;
		$exps = array('expense'=>array(),'total'=>0);
		$filter = '1=1';
		if($in['user_id']){
			$filter.=" AND project_expenses.user_id='".$in['user_id']."' ";
		}
		$expense = $this->db->query("SELECT project_expenses.*, servicing_support.customer_name AS c_name, servicing_support.serial_number AS p_name, servicing_support_users.name as u_name,
									expense.name AS e_name,
									expense.unit_price, expense.unit, servicing_support.customer_id as customer_id
							   FROM project_expenses
							   INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
							   INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
							   INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id AND project_expenses.service_id=servicing_support_users.service_id
		AND project_expenses.user_id = servicing_support_users.user_id
							   WHERE ".$filter." AND servicing_support.service_id='".$in['service_id']."' ORDER BY id");
		$q=0;
		$i=0;
		while ($expense->next()) {
			$amount = place_currency(display_number($expense->f('amount')));
			$pret = $expense->f('amount');
			if($expense->f('unit_price')){
				$amount = place_currency(display_number(($expense->f('amount') * $expense->f('unit_price'))))." (".$expense->f('amount')." ".$expense->f('unit').")";
				$pret = $expense->f('amount') * $expense->f('unit_price');
			}
			if($expense->f('picture')){
				$size = @getimagesize($config['upload_url'].DATABASE_NAME."/receipt/".$expense->f('picture'));
			}
			$exp = array(
				'DATE'   					=> date(ACCOUNT_DATE_FORMAT,$expense->f('date')),
				'PROJECT_CUSTOMER_NAME'		=> $expense->f('c_name'),
				'PROJECT_LIST_NAME'			=> $expense->f('p_name'),
				'category'			=> $expense->f('e_name'),
				'u_name'			=> get_user_name($expense->f('user_id')),
				'CUSTOMER_ID1'				=> $expense->f('customer_id'),
				'amount'					=> $amount,
				'amount_txt'				=> $expense->f('amount'),
				'note'						=> split_lines($expense->f('note')) ? split_lines($expense->f('note')):'',
				'E_ID'						=> $expense->f('id'),
				'PROJECT_IDE1'				=> $expense->f('service_id'),
				'DATEE'						=> $expense->f('date'),
				'EXP_ID'					=> $expense->f('expense_id'),
				'VIEW_IMG'					=> $expense->f('picture') == "" ? false : true,
				'IMG_HREF'					=> UPLOAD_PATH.DATABASE_NAME."/receipt/".$expense->f('picture'),
				'VIEW_L'					=> $expense->f('billed') == 1 ? 'hide' : ($in['hide_s'] == 1 ? 'hide' : ''),
				'HIDE_EDIT'					=> $in['is_ajax'] == 1 ? '1':'',
				'bill'						=> $expense->f('billable'),
				'adhoc'						=> $expense->f('active') == 2 ? '1' : '0',
				'img_w'						=> (string)$size[0],
				'service_date'				=> ($expense->f('date')+10800)*1000,
				'billabel'					=> $expense->f('billable') ? true : false,
				'service_id'				=> $expense->f('service_id'),
			);
			array_push($exps['expense'], $exp);
			$total += $pret;
			$i++;
		}
		$exps['total'] = place_currency(display_number($total));
		$exps['is_data']=$i;
		return $exps;
	}

	public function get_serviceSubTasks(&$in){

		$tasks =array('tasks'=>array());
		$i=0;
		if ($in['service_id'] == 'tmp') {
			foreach($in['list'] as $idx=>$data) {
				if ($data['checked']) {
					$service_id = $this->db->field("SELECT service_id FROM servicing_support WHERE service_id = '". $data['service_id'] ."'");

					$task = $this->db->query("SELECT * FROM servicing_support_tasks WHERE service_id = '". $service_id ."' AND article_id = '0' ORDER BY sort_order ASC");
					while($task->next()){
						$t=array(
							'name' 				=> stripslashes($task->f('task_name')),
							'id'					=> $task->f('task_id'),
							'checked'			=> $task->f('closed') == 1 ? true : false,
							'no-plus'			=> $task->f('comment') == '' ? "" : 'no-plus',
							'comment'			=> $task->f('comment'),
							'readonly'		=> $in['status'] == 2 ? ' readonly="readonly" ' : '',
							'task_budget'	=> display_number($task->f('task_budget')),
							'task_quantity' => display_number($task->f('quantity'))
						);
						array_push($tasks['tasks'], $t);
						$i++;
					}
				}
			}
		} else {
			$task = $this->db->query("SELECT * FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY sort_order ASC ");
			while($task->next()){
				$t=array(
					'name' 				=> stripslashes($task->f('task_name')),
					'id'					=> $task->f('task_id'),
					'checked'			=> $task->f('closed') == 1 ? true : false,
					'no-plus'			=> $task->f('comment') == '' ? "" : 'no-plus',
					'comment'			=> $task->f('comment'),
					'readonly'		=> $in['status'] == 2 ? ' readonly="readonly" ' : '',
					'task_budget'	=> display_number($task->f('task_budget')),
					'task_quantity' => display_number($task->f('quantity'))
				);
				array_push($tasks['tasks'], $t);
				$i++;
			}
		}
		$tasks['is_data']=$i;
		return $tasks;
	}

	public function get_serviceSubServices(&$in){
		$tasks =array('services'=>array());
		$i=0;
		if ($in['service_id'] == 'tmp') {
			foreach($in['list'] as $idx=>$data) {
				if ($data['checked']) {
					$service_id = $this->db->field("SELECT service_id FROM servicing_support WHERE service_id = '". $data['service_id'] ."'");

					$task = $this->db->query("SELECT servicing_support_tasks.*,pim_articles.item_code FROM servicing_support_tasks
					INNER JOIN pim_articles ON servicing_support_tasks.article_id=pim_articles.article_id
					WHERE servicing_support_tasks.service_id='" . $service_id . "' AND servicing_support_tasks.article_id!='0' ORDER BY servicing_support_tasks.sort_order ASC ");
					while ($task->next()) {
						$t = array(
							'name' => $task->f('task_name'),
							'id' => $task->f('task_id'),
							'checked' => $task->f('closed') == 1 ? true : false,
							'no-plus' => $task->f('comment') == '' ? "" : 'no-plus',
							'comment' => $task->f('comment'),
							'ALLOW_STOCK' => ALLOW_STOCK == 1 ? true : false,
							'readonly' => $in['status'] == 2 ? ' readonly="readonly" ' : '',
							'task_budget' => display_number($task->f('task_budget')),
							'code' => $task->f('item_code'),
							'quantity' => display_number($task->f('quantity')),
							'article_id' => $task->f('article_id')
						);
						array_push($tasks['services'], $t);
						$i++;
					}
				}
			}
		} else {
			$task = $this->db->query("SELECT servicing_support_tasks.*,pim_articles.item_code FROM servicing_support_tasks
			INNER JOIN pim_articles ON servicing_support_tasks.article_id=pim_articles.article_id 
			WHERE servicing_support_tasks.service_id='" . $in['service_id'] . "' AND servicing_support_tasks.article_id!='0' ORDER BY servicing_support_tasks.sort_order ASC ");
			while ($task->next()) {
				$t = array(
					'name' => $task->f('task_name'),
					'id' => $task->f('task_id'),
					'checked' => $task->f('closed') == 1 ? true : false,
					'no-plus' => $task->f('comment') == '' ? "" : 'no-plus',
					'comment' => $task->f('comment'),
					'ALLOW_STOCK' => ALLOW_STOCK == 1 ? true : false,
					'readonly' => $in['status'] == 2 ? ' readonly="readonly" ' : '',
					'task_budget' => display_number($task->f('task_budget')),
					'code' => $task->f('item_code'),
					'quantity' => display_number($task->f('quantity')),
					'article_id' => $task->f('article_id')
				);
				array_push($tasks['services'], $t);
				$i++;
			}
		}
		$tasks['is_data'] = $i;
		return $tasks;
	}

	public function get_serviceSupplies(&$in){
		$supplies=array('supplies'=>array());
		$i=0;
		if ($in['service_id'] == 'tmp') {
			foreach($in['list'] as $idx=>$data) {
				if ($data['checked']) {
					$service_id = $this->db->field("SELECT service_id FROM servicing_support WHERE service_id = '". $data['service_id'] ."'");

					$sups = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='" . $service_id . "' AND article_id='0' ORDER BY sort_order ASC ");
					while ($sups->next()) {
						$sup = array(
							'article_id' => $sups->f('article_id'),
							'name' => $sups->f('name'),
							'id' => $sups->f('a_id'),
							'quantity' => display_number($sups->f('quantity')),
							'to_deliver' => display_number($sups->f('to_deliver')),
							'delivered' => display_number($sups->f('delivered')),
							'price' => display_number($sups->f('price')),
							'margin' => display_number($sups->f('margin')),
							'sell_price' => display_number($sups->f('price') * $sups->f('margin') / 100 + $sups->f('price')),
							'readonly' => $in['status'] == 2 ? ' readonly="readonly" ' : '',
							'billed' => $sups->f('billed') == 1 ? true : false,
							'billable' => $sups->f('billable') ? true : false
						);
						array_push($supplies['supplies'], $sup);
						$i++;
					}
				}
			}
		} else {
			$sups = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='" . $in['service_id'] . "' AND article_id='0' ORDER BY sort_order ASC ");
			while ($sups->next()) {
				$sup = array(
					'article_id' => $sups->f('article_id'),
					'name' => $sups->f('name'),
					'id' => $sups->f('a_id'),
					'quantity' => display_number($sups->f('quantity')),
					'to_deliver' => display_number($sups->f('to_deliver')),
					'delivered' => display_number($sups->f('delivered')),
					'price' => display_number($sups->f('price')),
					'margin' => display_number($sups->f('margin')),
					'sell_price' => display_number($sups->f('price') * $sups->f('margin') / 100 + $sups->f('price')),
					'readonly' => $in['status'] == 2 ? ' readonly="readonly" ' : '',
					'billed' => $sups->f('billed') == 1 ? true : false,
					'billable' => $sups->f('billable') ? true : false
				);
				array_push($supplies['supplies'], $sup);
				$i++;
			}
		}
		$supplies['is_data']=$i;
		return $supplies;
	}

	public function get_serviceArticles(&$in){
		$articles=array('articles'=>array());
		$article=0;
		if ($in['service_id'] == 'tmp') {
			foreach($in['list'] as $idx=>$data) {
				if ($data['checked']) {
					$service_id = $this->db->field("SELECT service_id FROM servicing_support WHERE service_id = '". $data['service_id'] ."'");

					$task = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='" . $service_id . "' AND article_id!='0' ORDER BY sort_order ASC ");
					while ($task->next()) {
						$art_data = $this->db->query("SELECT * FROM pim_articles WHERE article_id='" . $task->f('article_id') . "' ");
						if ($art_data->f('stock') <= 0) {
							$stock_status = 'text-danger';
						} else if ($art_data->f('stock') > 0 && $art_data->f('stock') <= $art_data->f('article_threshold_value')) {
							$stock_status = 'text-warning';
						} else {
							$stock_status = 'text-success';
						}
						$billed = $this->db->field("SELECT SUM(invoiced) FROM service_delivery WHERE service_id='" . $service_id . "' AND a_id='" . $task->f('article_id') . "' ");
						$q_delivered = $this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='" . $service_id . "' AND a_id='" . $task->f('article_id') . "' ");
						$q_reserved = $this->db->field("SELECT SUM(quantity) FROM service_reservation WHERE service_id='" . $service_id . "' AND a_id='" . $task->f('article_id') . "' ");
						$is_reserved = $this->db->field("SELECT reserved FROM servicing_support_articles WHERE service_id='" . $service_id . "' AND article_id='" . $task->f('article_id') . "' ");

						$q_delivered_total = $this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE a_id='" . $task->f('article_id') . "' AND from_reserved='1'");
						$reserved_stock = $this->db->field("SELECT SUM( service_reservation.quantity ) AS reserved_stock FROM service_reservation WHERE service_reservation.a_id =  '" . $task->f('article_id') . "' ");
						$reserved_stock_delivered = 0;
						if ($is_reserved) {
							$reserved_stock_delivered = $reserved_stock - $q_delivered_total;
						}

						$row_id = $task->f('a_id');
						if ($task->f('has_variants')) {
							$last_variant_parent = $row_id;
						}

						$quantity_component = 0;
						if ($task->f('component_for')) {
							$parent_article_id = $this->db->field("SELECT article_id FROM servicing_support_articles where a_id ='" . $task->f('component_for') . "' ");
							$quantity_component = get_quantity_component($parent_article_id, $task->f('article_id'));
						}

						$art_line = array(
							'article_id' => $task->f('article_id'),
							'name' => $task->f('name'),
							'id' => $task->f('a_id'),
							'quantity' => display_number($task->f('quantity')),
							'ALLOW_STOCK' => ALLOW_STOCK == 1 ? true : false,
							'to_deliver' => display_number($task->f('to_deliver')),
							'delivered' => $task->f('article_delivered') ? true : false,
							'to_be_reserved' => remove_zero_decimals($task->f('quantity') - $q_reserved),
							'to_be_delivered' => remove_zero_decimals($task->f('quantity') - $q_delivered),
							'reserved_stock' => remove_zero_decimals($reserved_stock_delivered),
							'price' => display_number($task->f('price')),
							'margin' => display_number($task->f('margin_per')),
							'purchase_price' => display_number($task->f('purchase_price')),
							//'sell_price'		=> display_number($task->f('price')*$task->f('margin')/100+$task->f('price')),
							'sell_price' => display_number($task->f('price')),
							'hstock' => $art_data->f('hide_stock') ? '1' : '0',
							'code' => $art_data->f('item_code'),
							'stock' => remove_zero_decimals($art_data->f('stock')),
							'billed' => $billed ? true : false,
							'billable' => $task->f('billable') ? true : false,
							'stock_status' => $stock_status,
							'has_variants' => $task->f('has_variants'),
							'has_variants_done' => $task->f('has_variants_done'),
							'is_variant_for' => $task->f('is_variant_for'),
							'is_variant_for_line' => $task->f('is_variant_for') ? $last_variant_parent : '',
							'variant_type' => $task->f('variant_type'),
							'is_combined' => $task->f('is_combined'),
							'component_for' => $task->f('component_for') ? $task->f('component_for') : '',
							'quantity_component' => display_number($quantity_component)
						);
						array_push($articles['articles'], $art_line);
						$article++;
					}
				}
			}
		} else {
			$task = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='" . $in['service_id'] . "' AND article_id!='0' ORDER BY sort_order ASC ");
			while ($task->next()) {
				$art_data = $this->db->query("SELECT * FROM pim_articles WHERE article_id='" . $task->f('article_id') . "' ");
				if ($art_data->f('stock') <= 0) {
					$stock_status = 'text-danger';
				} else if ($art_data->f('stock') > 0 && $art_data->f('stock') <= $art_data->f('article_threshold_value')) {
					$stock_status = 'text-warning';
				} else {
					$stock_status = 'text-success';
				}
				$billed = $this->db->field("SELECT SUM(invoiced) FROM service_delivery WHERE service_id='" . $in['service_id'] . "' AND a_id='" . $task->f('article_id') . "' ");
				$q_delivered = $this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='" . $in['service_id'] . "' AND a_id='" . $task->f('article_id') . "' ");
				$q_reserved = $this->db->field("SELECT SUM(quantity) FROM service_reservation WHERE service_id='" . $in['service_id'] . "' AND a_id='" . $task->f('article_id') . "' ");
				$is_reserved = $this->db->field("SELECT reserved FROM servicing_support_articles WHERE service_id='" . $in['service_id'] . "' AND article_id='" . $task->f('article_id') . "' ");

				$q_delivered_total = $this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE a_id='" . $task->f('article_id') . "' AND from_reserved='1'");
				$reserved_stock = $this->db->field("SELECT SUM( service_reservation.quantity ) AS reserved_stock FROM service_reservation WHERE service_reservation.a_id =  '" . $task->f('article_id') . "' ");
				$reserved_stock_delivered = 0;
				if ($is_reserved) {
					$reserved_stock_delivered = $reserved_stock - $q_delivered_total;
				}

				$row_id = $task->f('a_id');
				if ($task->f('has_variants')) {
					$last_variant_parent = $row_id;
				}

				$quantity_component = 0;
				if ($task->f('component_for')) {
					$parent_article_id = $this->db->field("SELECT article_id FROM servicing_support_articles where a_id ='" . $task->f('component_for') . "' ");
					$quantity_component = get_quantity_component($parent_article_id, $task->f('article_id'));
				}

				$art_line = array(
					'article_id' => $task->f('article_id'),
					'name' => $task->f('name'),
					'id' => $task->f('a_id'),
					'quantity' => display_number($task->f('quantity')),
					'ALLOW_STOCK' => ALLOW_STOCK == 1 ? true : false,
					'to_deliver' => display_number($task->f('to_deliver')),
					'delivered' => $task->f('article_delivered') ? true : false,
					'to_be_reserved' => remove_zero_decimals($task->f('quantity') - $q_reserved),
					'to_be_delivered' => remove_zero_decimals($task->f('quantity') - $q_delivered),
					'reserved_stock' => remove_zero_decimals($reserved_stock_delivered),
					'price' => display_number($task->f('price')),
					'margin' => display_number($task->f('margin_per')),
					'purchase_price' => display_number($task->f('purchase_price')),
					//'sell_price'		=> display_number($task->f('price')*$task->f('margin')/100+$task->f('price')),
					'sell_price' => display_number($task->f('price')),
					'hstock' => $art_data->f('hide_stock') ? '1' : '0',
					'code' => $art_data->f('item_code'),
					'stock' => remove_zero_decimals($art_data->f('stock')),
					'billed' => $billed ? true : false,
					'billable' => $task->f('billable') ? true : false,
					'stock_status' => $stock_status,
					'has_variants' => $task->f('has_variants'),
					'has_variants_done' => $task->f('has_variants_done'),
					'is_variant_for' => $task->f('is_variant_for'),
					'is_variant_for_line' => $task->f('is_variant_for') ? $last_variant_parent : '',
					'variant_type' => $task->f('variant_type'),
					'is_combined' => $task->f('is_combined'),
					'component_for' => $task->f('component_for') ? $task->f('component_for') : '',
					'quantity_component' => display_number($quantity_component)
				);
				array_push($articles['articles'], $art_line);
				$article++;
			}
		}
		$articles['is_data']=$article;
		$articles['allow_stock']		= defined('ALLOW_STOCK') && ALLOW_STOCK=='1' ? true : false;
		return $articles;
	}

	public function get_serviceUserStat(&$in){

		$data=array('list'=>array());

		$query = $this->db->query("SELECT servicing_support_users . name , SUM( servicing_support_sheet.end_time - servicing_support_sheet.start_time - servicing_support_sheet.break ) as hours
		FROM servicing_support_users
		LEFT JOIN servicing_support_sheet ON servicing_support_sheet.user_id = servicing_support_users.user_id
		AND servicing_support_sheet.service_id = servicing_support_users.service_id
		WHERE servicing_support_users.service_id =  '".$in['service_id']."'
		GROUP BY servicing_support_users.user_id ORDER BY servicing_support_users.name ASC ");

		while($query->next()){
			array_push($data['list'], array(
		  	'name'								=> $query->f('name'),
		  	'hours'								=> number_as_hour($query->f('hours')),
			));
		}
		return $data;
	}

	public function get_report(&$in){

		$data=array(
			'table'=>$this->get_serviceHours($in),
			'hours'=>0,
			'chart'=>array(
				'hours'=>array(),
				'name'=>array(),
				'color'=>array()
			),
			'expenses'=>$this->get_serviceExpense($in)
		);
		$hours = 0;
		$color = array('#90c3e1','#51c38b','#ffd58b','#ed82ab','#bce9f7','#51e9ed','#f9cfe6','#ffeee3','#b4e7ea','#beb2a9');
		/*
		$service_user = $this->db->query("SELECT sum(servicing_support_sheet.end_time-servicing_support_sheet.start_time-servicing_support_sheet.break) as h, servicing_support_users.name AS uname,servicing_support_users.user_id
														FROM servicing_support_sheet
														INNER JOIN servicing_support_users ON servicing_support_sheet.service_id=servicing_support_users.service_id
														AND servicing_support_sheet.user_id=servicing_support_users.user_id
														WHERE servicing_support_sheet.service_id='".$in['service_id']."'
														GROUP BY servicing_support_sheet.user_id ");
		while ($service_user->next()) {
			array_push($data['table'], array(
				'name' 	=> $service_user->f('uname'),
				'hours'	=> number_as_hour($service_user->f('h')),
				'u_id'	=> $service_user->f('user_id'),
				'color'	=> $color[$i]
			));
			$hours +=$service_user->f('h');
			$i++;
		}
		$data['hours'] = number_as_hour($hours);*/

		$this->db->query("SELECT sum(servicing_support_sheet.end_time-servicing_support_sheet.start_time-servicing_support_sheet.break) as h, servicing_support_users.name AS uname,servicing_support_users.user_id
								FROM servicing_support_sheet
								INNER JOIN servicing_support_users ON servicing_support_sheet.service_id=servicing_support_users.service_id
								AND servicing_support_sheet.user_id=servicing_support_users.user_id
								WHERE servicing_support_sheet.service_id='".$in['service_id']."'
								GROUP BY servicing_support_sheet.user_id ");
		$i=0;
		while($this->db->move_next()){
				/*$sss = new stdClass();
				$sss->name = $db->f('uname');
				$sss->y = (float)number_format( $db->f('h'),2,'.', '' );
				$sss->visible = true;
				$sss->color = $color[$i];*/
				foreach($data['table']['hours'] as $key=>$value){
					if($value['user_id']==$this->db->f('user_id')){
						$data['table']['hours'][$key]['color']=$color[$i];
					}
				}
				array_push($data['chart']['hours'], (float)number_format( $this->db->f('h'),2,'.', '' ));
				array_push($data['chart']['name'], get_user_name($this->db->f('user_id')));
				array_push($data['chart']['color'], $color[$i]);
				$i++;
		}

		return $data;
	}

	public function get_viewIntervention(&$in){

		$service =	$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		$free_field = $service->f('customer_address').'
'.$service->f('customer_zip').' '.$service->f('customer_city').'
'.get_country_name($service->f('customer_country_id'));

		$customer_pnumber=$this->db->field("SELECT comp_phone FROM customers WHERE customer_id='".$service->f('customer_id')."' ");
		$contact_pnumber=$this->db->field("SELECT cell FROM customer_contacts WHERE contact_id='".$service->f('contact_id')."' ");
		$data=array();
		$data['installation_id']=$service->f('installation_id');
		$data['customer_id']=$service->f('customer_id');
		$data['service_type']=$service->f('service_type');
		/*$data['installation']=array();
		if($service->f('installation_id')){
			$installation_data=$this->db->query("SELECT * FROM installations WHERE id='".$service->f('installation_id')."' ");
			$data['installation']['name']=stripslashes($installation_data->f('name'));
			$data['installation']['template']=array('left_fields'=>array(),'right_fields'=>array());
			$left_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$service->f('installation_id')."' AND position='1' ORDER BY id ASC");
			while($left_fields->next()){
				$temp_left=array(
						'name'				=> stripslashes($left_fields->f('label_name')),
						'name_final'		=> $left_fields->f('type')==3 ? ((int)$left_fields->f('name') ? date(ACCOUNT_DATE_FORMAT,(int)$left_fields->f('name')): '') : stripslashes($left_fields->f('name')),
					);
				array_push($data['installation']['template']['left_fields'], $temp_left);
			}
			$right_fields=$this->db->query("SELECT * FROM installations_data WHERE installation_id='".$service->f('installation_id')."' AND position='2' ORDER BY id ASC");
			while($right_fields->next()){
				$temp_right=array(
						'name'				=> stripslashes($right_fields->f('label_name')),
						'name_final'		=> $right_fields->f('type')==3 ? ((int)$right_fields->f('name') ? date(ACCOUNT_DATE_FORMAT,(int)$right_fields->f('name')) : '') : stripslashes($right_fields->f('name')),
					);
				array_push($data['installation']['template']['right_fields'], $temp_right);
			}
		}*/
		$data['customer_name']              = $service->f('customer_name');
		$data['contact_name']               = $service->f('contact_id') ? $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$service->f('contact_id')."' ") : '';
		$data['free_field']					= $service->f('free_field') ?  nl2br($service->f('free_field')) : nl2br($free_field);
		$data['customer_email']				= $service->f('customer_email');
		$data['customer_phone']				= $customer_pnumber;
		$data['contact_phone']				= $contact_pnumber;
		$data['report'] 					= $service->f('report');
		$data['loged_user'] 				= $_SESSION['u_id'];
		$data['autoSrvUsers']				= $this->get_autoSrvUsers($in);
		$data['expenseCat']					= $this->get_expenseCat($in);
		return $data;
	}

	public function get_autoUsers(&$in){

		$q = strtolower($in["term"]);

		$filter = " AND 1=1 ";

		if($q){
			$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
		}
		$team_members=$this->db->query("SELECT user_id FROM servicing_team_users ")->getAll();
		$this->db_users->query("SELECT first_name,last_name,users.user_id,h_rate FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) AND users.group_id!='2' ORDER BY first_name ");
		while($this->db_users->move_next()){
			foreach($team_members as $key=>$value){
				if(in_array($this->db_users->f('user_id'), $value)){
					$users[htmlspecialchars_decode(stripslashes($this->db_users->f('first_name').' '.$this->db_users->f('last_name')))]=$this->db_users->f('user_id');
		 			$u[htmlspecialchars_decode(stripslashes($this->db_users->f('first_name').' '.$this->db_users->f('last_name')))] = $this->db_users->f('h_rate');
				}
			}	  
		}

		$result = array();
		if($users){
			foreach ($users as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "h_rate" => $u[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "h_rate" => $u[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "value" => 'No matches', "h_rate" => 'No matches'));
		}
   		 return $result;
	}
	public function get_autoSrvUsers(&$in){

		$q = strtolower($in["term"]);

		$filter =" service_id='".$in['service_id']."' ";
		if($q){
			$filter .=" AND name LIKE '%".$q."%'";
		}
		$is_manager=$this->db->field("SELECT pr_m FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' AND service_id='".$in['service_id']."' ");
		if(!$is_manager && $_SESSION['team_int']){
			$filter .=" AND user_id='".$_SESSION['u_id']."' ";
		}

		$this->db->query("SELECT name,user_id FROM servicing_support_users WHERE $filter ORDER BY name ");
		while($this->db->move_next()){
		  $users[get_user_name($this->db->f('user_id'))]=$this->db->f('user_id');
		}

		$result = array();
		if($users){
			foreach ($users as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key) ));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key) ));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "value" => 'No matches', "h_rate" => 'No matches'));
		}
    		return $result;
	}
	public function get_expenseCat(&$in){

		$q = strtolower($in["term"]);

		$filter =" 1=1 ";
		if($q){
			$filter .=" AND name LIKE '%".$q."%'";
		}
		$prj_bill =array();
		if($in['project_id']){
			$this->db->query("SELECT * FROM project_expences
				WHERE project_id = '".$in['project_id']."' ");
			while ($this->db->next()) {
				array_push($prj_bill, $this->db->f('expense_id'));
			}
		}
		$this->db->query("SELECT * FROM expense WHERE $filter");

		while($this->db->move_next()){
		  $expense[$this->db->f('name')]= $this->db->f('expense_id');
		  $unit_price[$this->db->f('name')] = $this->db->f('unit_price');
		  $unit[$this->db->f('name')] = $this->db->f('unit');
		  $billable = '';
		  if(in_array($this->db->f('expense_id'), $prj_bill)){
		  	$billable = 1;
		  }
		  $billable_expense[$this->db->f('name')] = $billable;
		}

		$result = array();
		if($expense){
			foreach ($expense as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "unit_price" => $unit_price[$key], "unitt" => $unit[$key], "billable_expense" => $billable_expense[$key] ));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "unit_price" => $unit_price[$key], "unitt" => $unit[$key], "billable_expense" => $billable_expense[$key] ));
				}
				if (count($result) > 2000)
				break;
			}
			// echo array_to_json($result);
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "value" => 'No matches'));
			// echo array_to_json($result);
		}
    		return $result;
	}
	public function get_recipients(&$in){
		if($q){
			$filter =" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		/*if($in['buyer_id']){
			$filter .= " AND customer_contactsIds.customer_id='".$in['buyer_id']."'";
		}*/
		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		if($in['contact_id']){
			$filter .= " AND customer_contacts.contact_id='".$in['contact_id']."'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();
		$defRates = defined('MAINTENANCE_RATE') ? MAINTENANCE_RATE : 0;
		$rates = array();
		$allrates = $this->db->query("SELECT * FROM default_data WHERE `type`='maintenance_rate' ");
		while($allrates->next()){
			$rates[$allrates->f('default_name')] = $allrates->f('value');
		}

		$this->db->query("SELECT customer_contactsIds.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE customer_contacts.active=1 $filter ORDER BY customer_contacts.lastname ");

		while($this->db->move_next()){
			$r = $defRates;
			$name = $this->db->f('firstname').' '.$this->db->f('lastname').' > '.$this->db->f('name');
			if($in['only_contact_name']){
				$name = $this->db->f('firstname').' '.$this->db->f('lastname');
			}
		  	$items[$name]=$this->db->f('contact_id');
		  	if ($this->db->f('customer_id')){
		  		$price_category[$name] = $this->db->f('cat_id');
		  	}
		  	else{
		  		$price_category[$name] = "1";
		  	}
		  	if(array_key_exists($this->db->f('customer_id'), $rates)){
		  		$r = $rates[$this->db->f('customer_id')];
		  	}
		  	$customer_id[$name] = $this->db->f('customer_id');
		  	$customer_name[$name] = $this->db->f('name');
		  	$c_name[$name] = $this->db->f('firstname').' '.$this->db->f('lastname');
		  	$currency[$name] = $this->db->f('currency_id');
		  	$lang[$name] = $this->db->f('internal_language');
		  	$email[$name] = $this->db->f('email');
		  	$rate[$name] = $r;
		}

		if($in['buyer_id'] && !$in['contact_id']){
			$customer = $this->db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE 1=1 $filter ");
		}else{
			$customer = $this->db->query("SELECT customers.name as name,customers.c_email as email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ");
		}
		$customerEmail = $customer->f('email');
		if(!empty($customerEmail)){
			$items2 = array(
				    'id' => $customer->f('email'), 
				    'label' => $customer->f('name'),
				    'value' => $customer->f('name').' - '.$customer->f('email'),
			);
		}

		$result = array();
		foreach ($items as $key=>$value) {
			$price_category_id=$price_category[$key];
			if($q){
				if (strpos(strtolower($key), $q) !== false) {
					//array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key),'email'=>$email[$key], "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)),"price_category_id"=>$price_category_id, 'customer_id' => $customer_id[$key], 'c_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$customer_name[$key]), 'currency_id' => $currency[$key], "lang_id" => $lang[$key], 'contact_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$c_name[$key]), 'rate' => $rate[$key] ));
					if(!empty($email[$key])){
						array_push($result, array("id"=>$email[$key], "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key),'email'=>$email[$key], "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key))." - ".$email[$key],"price_category_id"=>$price_category_id, 'customer_id' => $customer_id[$key], 'c_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$customer_name[$key]), 'currency_id' => $currency[$key], "lang_id" => $lang[$key], 'contact_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$c_name[$key]), 'rate' => $rate[$key] ));
					}

				}
			}else{
				//array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key),'email'=>$email[$key], "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)),"price_category_id"=>$price_category_id, 'customer_id' => $customer_id[$key], 'c_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$customer_name[$key]), 'currency_id' => $currency[$key], "lang_id" => $lang[$key], 'contact_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$c_name[$key]), 'rate' => $rate[$key] ));
					if(!empty($email[$key])){
						array_push($result, array("id"=>$email[$key], "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key),'email'=>$email[$key], "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key))." - ".$email[$key],"price_category_id"=>$price_category_id, 'customer_id' => $customer_id[$key], 'c_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$customer_name[$key]), 'currency_id' => $currency[$key], "lang_id" => $lang[$key], 'contact_name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$c_name[$key]), 'rate' => $rate[$key] ));
					}
			}
			if (count($result) > 100)
			break;
		}

			$added = false;
			foreach ($result as $key => $value) {
				if($value['id'] == $items2['id']){
					$added = true;
				}
			}
			if(!$added){
				array_push($result, $items2);
			}

    		return $result;
	}
	public function getCompanyInfo(){
		$array = array(			
			'name'		=> ACCOUNT_COMPANY,
			'address'		=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
			'zip'			=> ACCOUNT_DELIVERY_ZIP,
			'city'		=> ACCOUNT_DELIVERY_CITY,
			'country'		=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
			'phone'		=> ACCOUNT_PHONE,
			'fax'			=> ACCOUNT_FAX,
			'email'		=> ACCOUNT_EMAIL,
			'url'			=> ACCOUNT_URL,
			'logo'		=> '../'.ACCOUNT_LOGO_QUOTE,			
		);		
		return $array;
	}
	public function get_addresses(&$in)
	{

		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		if($in['site_add']){
			$filter.=" AND site='1' ";
		}
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		"symbol"				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){

			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;
	}
	public function get_cc(&$in)
	{

		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		if($in['internal']=='true'){
			$filter=" is_admin>0 ";
		}
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}
		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."' ";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		if($in['internal'] == 'true'){
			$cust = $this->db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name,our_reference,currency_id,internal_language
			FROM customers
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 50")->getAll();
		}else{
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id		
			ORDER BY name
			LIMIT 50")->getAll();
		}
		
		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				/*"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}
	public function get_contacts(&$in) {

		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY customer_contacts.lastname limit 9")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
				'bottom' 				=> '',
			);

		}

		$added = false;
		if(count($result)==9){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_articles_list(&$in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id ';
			if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    	$table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
		  	}
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference,
						pim_articles.has_variants,
						pim_articles.use_combined';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id';
			if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    	$table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
		  	}
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference,
						pim_articles.has_variants,
						pim_articles.use_combined';
		}

		$filter.=" 1=1 ";
		if($in['only_service']){
			$filter.=" AND pim_articles.is_service='1' ";
		}else{
			$filter.=" AND pim_articles.is_service='0' ";
		}

		if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    $filter.=' AND pim_article_variants.article_id IS NULL ';
		}

		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id']){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
				if($vat_regime==2){
					$vat=0;
				}
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	'name'						=> htmlspecialchars_decode($article->f('internal_name')),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'ALLOW_STOCK'		=> ALLOW_STOCK==1 ? true : false,
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)), 
				'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
				'supplier_reference'	   => $article->f('supplier_reference'),
				'has_variants'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? true : false,
				'has_variants_done'			=> 0,
				'is_variant_for'			=> 0,
				'is_combined'				=> $article->f('use_combined') ? true : false,
				'component_for'				=> '',
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		if(in_array(12,explode(';', $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']])))){
			array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));
		}

		return $articles;
	}

	public function get_tasks_list(&$in){
		$tasks=array();
		$data=$this->db->query("SELECT * FROM pim_articles WHERE is_service='1' ");
		while($data->next()){
			array_push($tasks, array('id'=>$data->f('article_id'),'name'=>$data->f('internal_name')));
		}
		return $tasks;
	}

	public function get_planUsers(&$in){
		$data = array('user_data'=>array(),'days_nr'=>array());

		if($in['planning_date']){
			$data['planning_date']=strtotime($in['planning_date'])*1000;
			$now			= strtotime($in['planning_date']);
		}else{
			if($in['planeddate_js']){
				$data['planning_date']=$in['planeddate_js'];
				$now			= $in['planeddate_js']/1000;
			}else{
				$data['planning_date']=time()*1000;
				$now			= time();
			}
		}

		if($in['free']){
			$select = array();
			$temp_sort=array();
			$user_team=$this->db->query("SELECT user_id FROM servicing_team_users ORDER BY sort ASC");
			while($user_team->next()){
				$temp_sort[$user_team->f('user_id')]=$this->db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$user_team->f('user_id')."' ");			
			}
			asort($temp_sort,SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
			foreach($temp_sort as $key=>$value){
				$team_temp=array(
					'id'		=> $key,
					'value'	=> $value,
					'added'	=> $this->db->field("SELECT u_id FROM servicing_support_users WHERE user_id='".$key."' AND service_id='".$in['service_id']."' ") ? '1' : ''
				);
				array_push($select,$team_temp);
			}
			$data['free']=$in['free'];
			$blocked_users=$this->db->query("SELECT user_id FROM servicing_block 
				WHERE date BETWEEN '".mktime(0,0,0,date('n',$now),date('j',$now),date('Y',$now))."' AND '".mktime(23,59,59,date('n',$now),date('j',$now),date('Y',$now))."' ")->getAll();
			foreach($blocked_users as $key=>$value){
				foreach($select as $key1=>$value1){
					if($value['user_id'] == $value1['id']){
						array_splice($select, $key1, 1);
						break;
					}
				}
			}
		}else{
			$select=array();
			$usersSql = $this->db->query("SELECT servicing_support_users.* FROM servicing_support_users WHERE servicing_support_users.service_id='".$in['service_id']."' ORDER BY name ASC ");
			while($usersSql->next()){
				$user_temp=array(
					'id'	=> $usersSql->f('user_id'),
					'value'	=> $usersSql->f('name'),
					'added'	=> '1'
					);
				array_push($select, $user_temp);
			}
		}

		$filter_users='';
		foreach($select as $key=>$value){
			$filter_users.=$value['id'].',';
		}
		$filter_users=rtrim($filter_users,",");
		if(empty($filter_users)){
			$teams=$this->db->query("SELECT * FROM servicing_team")->getAll();
		}else{
			$teams=$this->db->query("SELECT servicing_team.* FROM servicing_team_users
				INNER JOIN servicing_team ON servicing_team_users.team_id=servicing_team.team_id
				WHERE servicing_team_users.user_id IN(".$filter_users.") GROUP BY servicing_team.team_id")->getAll();
		}
		
		$data['select'] = $teams;

		if($in['team_id']){
			$select_temp=array();
			$data['team_id']=$in['team_id'];
			$team_members=$this->db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."' ORDER BY sort ASC");
			while($team_members->next()){
				foreach($select as $key=>$value){
					if($team_members->f('user_id') == $value['id']){
						$team_temp=array(
							'id'			=> $team_members->f('user_id'),
							'value'		=> $this->db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$team_members->f('user_id')."' "),
						);
						array_push($select_temp,$team_temp);
						break;
					}
				}			
			}
			$select=$select_temp;
		}

		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		$week_end 			= $week_start + 604799;

		$in['week_start_serv'] = $week_start;
		$in['week_end_serv'] = $week_end;

		$data['week_txt']= date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end);

		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			array_push($data['days_nr'],array('day_name' => gm(date('D',$i)),'date_month' => date('d',$i).' '.gm(date('M',$i)),'end_day'=> date("N",$i)>5 ? true : false));
		}

		$j=0;

		foreach($select as $row => $val){
			$day_data=array();
			$sheet = $this->db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$in['week_start_serv']."' AND '".$in['week_end_serv']."'
												 AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ORDER BY p_date, servicing_support.startdate ASC ")->getAll();

			$start_day=$week_start;
			while($start_day <= $week_end){
				$start_d=$start_day;
				$end_d=$start_d+86399;
				$blocked_day=$this->db->field("SELECT id FROM servicing_block WHERE `date`='".$start_d."' AND user_id='".$val['id']."' ");

				$service_nr=0;
				$services_data=array();
				foreach($sheet as $key=>$value){
					if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
						$services_data_temp=array(
							'c_name'		=> $value['customer_name'] ? $value['customer_name'] : $value['contact_name'],
							'status'		=> $value['status'],
							'top'			=> $value['startdate']-1<0 ? true : false,
							'start_time'	=> number_as_hour($value['startdate']),
							'pduration'		=> $value['duration'] > 0 ? true : false,
							'end_time'		=> $value['duration'] > 0 ? number_as_hour($value['startdate']+ $value['duration']) : number_as_hour($value['end_time']),
							'hours'		=> number_as_hour($value['end_time']-$value['start_time']),
							'id'			=> $value['id'],
							'service_id'	=> $value['service_id'],
							'tmsmp'		=> $value['planeddate']
						);
						array_push($services_data,$services_data_temp);
						$service_nr++;
					}
				}

				$temp_day_data=array(
					'day_txt'		=> date('d',$end_d),
					'service_nr'	=> $service_nr,
					'services_data'	=> $services_data,
					'tmsp'		=> $start_d,
					'block_day'		=> $blocked_day ? true : false
				);
				array_push($day_data, $temp_day_data);
				$start_day=$start_day+86400;
			}

			$temp_users=array(
				'data'	=> $day_data,
				'user_id'	=> $val['id'],
				'name'	=> $val['value'],
				'added'	=> array_key_exists('added', $val) ? $val['added'] : ''
			);
			array_push($data['user_data'],$temp_users);
		}

		return $data;

	}

	public function get_accmanager($in)
	{
		$q = strtolower($in["term"]);		

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}

		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
											FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
											WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>stripslashes($db_users->f('first_name')).' '.stripslashes($db_users->f('last_name')) ));
		}
		return $items;
	}

	public function get_author($in)
	{
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}
		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>$db_users->f('first_name').' '.$db_users->f('last_name') ) );
		}
		return $items;
	}

	public function get_installations(&$in){
		$filter = '';
		if(is_numeric($in['service_id'])){
			$service_data=$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
			if(!$service_data->f('quote_id')){
				if($service_data->f('same_address')){
					$address=$service_data->f('same_address');
				}else{
					$address=$service_data->f('main_address_id');
				}
				//$filter .=" AND address_id='".$address."' ";
			}else{
				$address="";
				$q_installation=$this->db->field("SELECT installation_id FROM tblquote WHERE id='".$service_data->f('quote_id')."' ");
				if($q_installation){
					$addressq=$this->db->field("SELECT installations.address_id FROM installations WHERE id='".$q_installation."' ");
					if($addressq){
						$address.=$addressq.",";
					}
				}
				if($service_data->f('same_address')){
					$address.=$service_data->f('same_address').',';
				}else{
					$address.=$service_data->f('main_address_id').',';
				}
				$filter .=" AND address_id IN(".rtrim($address,',').") ";
			}
		}

		$q = strtolower($in["term"]);	

		if($q){
		  $filter .=" AND name LIKE '".$q."%'";
		}
		$installation=$this->db->query("SELECT * FROM installations 
			WHERE customer_id='".$in['buyer_id']."' $filter ORDER BY name ASC");
		$items = array();
		while($installation->next()){
			array_push( $items, array('installation_id'=>$installation->f('id'),'name'=>stripslashes($installation->f('name')),'serial_number'=>$installation->f('serial_number')));
		}
		if($q){
            array_push($items,array('installation_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
        }else{
            array_push($items,array('installation_id'=>'99999999999','name'=>''));
        }
		return $items;
	}

	public function get_planRecurring(&$in){
		$data = array('user_data'=>array(),'days_nr'=>array(),'planning_dates'=>array());

		$teams=$this->db->query("SELECT * FROM servicing_team")->getAll();
		$data['select'] = $teams;
		if(count($teams)>0 && !$in['team_id']){
			$in['team_id']=$teams[0]['team_id'];
		}
		if($in['team_id']){
			$select=array();
			$data['team_id']=$in['team_id'];
			$team_members=$this->db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."' ORDER BY sort ASC");
			while($team_members->next()){
				$team_temp=array(
					'id'			=> $team_members->f('user_id'),
					'value'		=> htmlspecialchars_decode(stripslashes($this->db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$team_members->f('user_id')."' "))),
				);
				array_push($select,$team_temp);
			}
		}

		if($in['planning_dates'] && !empty($in['planning_dates'])){
			$data['planning_dates']=$in['planning_dates'];
			$planning_dates=$in['planning_dates'];
		}else{
			$planning_dates=array();
			$startdate=mktime(0,0,0,date('n', strtotime($in['rec_start_date'])),date('j',strtotime($in['rec_start_date'])),date('Y',strtotime($in['rec_start_date'])));
			if($in['rec_end_date'] && !$in['rec_number']){
				$enddate=mktime(23,59,59,date('n', strtotime($in['rec_end_date'])),date('j',strtotime($in['rec_end_date'])),date('Y',strtotime($in['rec_end_date'])));
				do{
					switch($in['rec_frequency']){
						case '1':
							array_push($planning_dates,$startdate);
							$startdate+=604800;
							break;
						case '2':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+1 month",$startdate);
							break;
						case '3':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+3 months",$startdate);
							break;
						case '4':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+1 year",$startdate);
							break;
						case '5':
							array_push($planning_dates,$startdate);
							$startdate=$startdate+($in['rec_days'] * 24 * 60 * 60);
							break;
						case '6':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+6 months",$startdate);
							break;
						case '7':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+24 months",$startdate);
							break;
					}
				}while($startdate<$enddate);
			}else{
				$s=0;
				do{
					switch($in['rec_frequency']){
						case '1':
							array_push($planning_dates,$startdate);
							$startdate+=604800;
							break;
						case '2':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+1 month",$startdate);
							break;
						case '3':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+3 months",$startdate);
							break;
						case '4':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+1 year",$startdate);
							break;
						case '5':
							array_push($planning_dates,$startdate);
							$startdate=$startdate+($in['rec_days'] * 24 * 60 * 60);
							break;
						case '6':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+6 months",$startdate);
							break;
						case '7':
							array_push($planning_dates,$startdate);
							$startdate=strtotime("+24 months",$startdate);
							break;
					}
					$s++;
				}while($s<$in['rec_number']);
			}

			$data['planning_dates']=$planning_dates;
		}	

		if($in['planning_users'] && !empty($in['planning_users'])){
			$data['planning_users']=$in['planning_users'];
		}else{
			$data['planning_users']=array();
		}
	
		if(!$in['start_date']){
			$data['start_date']=strtotime($in['rec_start_date'])*1000;
			$now			= strtotime($in['rec_start_date']);
		}else{
			$data['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
			// case $is_service_manager:
				$filter = '';
				break;
			default:
				$filter = " AND servicing_support.service_id IN (SELECT service_id FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' GROUP BY service_id )";
				break;
		}

		if($in['activeType']=='1'){
			$today_of_week 		= date("N", $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;

			$data['date_txt']= date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end);

			for($i = $week_start; $i <= $week_end; $i += 86400)
			{
				array_push($data['days_nr'],array('day_name' => gm(date('D',$i)),'date_month' => date('d',$i).' '.gm(date('M',$i)),'end_day'=> date("N",$i)>5 ? true : false));
			}

			$j=0;

			foreach($select as $row => $val){
				$day_data=array();
				$sheet = $this->db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
							INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
							WHERE servicing_support.planeddate BETWEEN '".$week_start."' AND '".$week_end."'
							AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ")->getAll();

				$start_day=$week_start;
				while($start_day <= $week_end){
					$start_d=$start_day;
					$end_d=mktime(23,59,59,date('n',$start_d),date('j',$start_d),date('Y',$start_d));
					$blocked_day=$this->db->field("SELECT id FROM servicing_block WHERE `date`='".$start_d."' AND user_id='".$val['id']."' ");

					$service_nr=0;
					$services_data=array();
					foreach($sheet as $key=>$value){
						if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
							$services_data_temp=array(
								'tmsmp'		=> $value['planeddate']
							);
							array_push($services_data,$services_data_temp);
							$service_nr++;
						}
					}
					$is_proposed=false;
					foreach($planning_dates as $key=>$value){
						if($value >= $start_d && $value <= $end_d){
							$is_proposed=true;
							break;
						}
					}			

					$temp_day_data=array(
						'day_txt'		=> date('d',$end_d),
						'service_nr'	=> $service_nr,
						'services_data'	=> $services_data,
						'tmsp'		=> $start_d,
						'block_day'		=> $blocked_day ? true : false,
						'is_proposed'	=> $is_proposed
					);
					array_push($day_data, $temp_day_data);
					$start_day=$start_day+86400;
				}

				$temp_users=array(
					'data'		=> $day_data,
					'user_id'	=> $val['id'],
					'name'		=> $val['value'],
					'added'		=> in_array($val['id'], $in['planning_users']) ? true : false,
				);
				array_push($data['user_data'],$temp_users);
			}
		}else{
			$month = date('n',$now);
			$year = date('Y',$now);
			$data['date_txt']= date('F',$now);
			$month_t = mktime(0,0,0,$month,1,$year);

			$month_days = date('t',$month_t);
			$month_first_day = date('N',$month_t);
			$month_last_day=mktime(23,59,59,$month,$month_days,$year);

			for($i=1;$i<=$month_days;$i++){
				$day_tmsp=mktime(23,59,59,$month,$i,$year);
				array_push($data['days_nr'],array('day'=>$i,'end_day'=> date("N",$day_tmsp)>5 ? true : false));
			}

			foreach($select as $row => $val){
				$day_data=array();
				$sheet = $this->db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
								INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
								WHERE servicing_support.planeddate BETWEEN '".$month_t."' AND '".$month_last_day."'
								AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ")->getAll();

				$day=1;
				while($day <= $month_days){
					$start_d=mktime(0,0,0,$month,$day,$year);

					$end_d=mktime(23,59,59,$month,$day,$year);
					$blocked_day=$this->db->field("SELECT id FROM servicing_block WHERE `date`='".$start_d."' AND user_id='".$val['id']."' ");

					$service_nr=0;
					$services_data=array();
					foreach($sheet as $key=>$value){
						if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
							$services_data_temp=array(
								'tmsmp'=>$value['planeddate']
							);
							array_push($services_data,$services_data_temp);
							$service_nr++;
						}
					}
					$is_proposed=false;
					foreach($planning_dates as $key=>$value){
						if($value >= $start_d && $value <= $end_d){
							$is_proposed=true;
							break;
						}
					}

					$temp_day_data=array(
						'day_txt'		=> $day,
						'service_nr'	=> $service_nr,
						'services_data'	=> $services_data,
						'tmsp'		=> $start_d,
						'block_day'		=> $blocked_day ? true : false,
						'is_proposed'	=> $is_proposed
					);
					array_push($day_data, $temp_day_data);
					$day++;
				}

				$temp_users=array(
					'data'		=> $day_data,
					'user_id'	=> $val['id'],
					'name'		=> $val['value'],
					'added'		=> in_array($val['id'], $in['planning_users']) ? true : false,
				);
				array_push($data['user_data'],$temp_users);
			}
		}
		return $data;
	}

	public function get_planRecurringSaved(&$in){
		$data = array('user_data'=>array(),'days_nr'=>array(),'planning_dates'=>array(),'service_id'=>$in['service_id'],'rec_first_day'=>$in['rec_first_day']);
		$service=$this->db->query("SELECT end_contract,rec_end_date,contract_id FROM servicing_support WHERE service_id='".$in['service_id']."'");
		$data['end_contract']=$service->f('end_contract') ? true : false;
		$data['rec_end_date']=$service->f('rec_end_date') ? $service->f('rec_end_date')*1000 : '';
		if($service->f('contract_id')){
			$contract_end_date=$this->db->field("SELECT end_date FROM contracts_version WHERE contract_id='".$service->f('contract_id')."' AND active='1' ");
			$data['contract_has_end']= $contract_end_date ? true : false; 
		}	

		$teams=$this->db->query("SELECT * FROM servicing_team")->getAll();
		$data['select'] = $teams;
		if(count($teams)>0 && !$in['team_id']){
			$in['team_id']=$teams[0]['team_id'];
		}
		if($in['team_id']){
			$select=array();
			$data['team_id']=$in['team_id'];
			$team_members=$this->db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."' ORDER BY sort ASC");
			while($team_members->next()){
				$team_temp=array(
					'id'			=> $team_members->f('user_id'),
					'value'		=> htmlspecialchars_decode(stripslashes($this->db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$team_members->f('user_id')."' "))),
				);
				array_push($select,$team_temp);
			}
		}

		if($in['planning_dates'] && !empty($in['planning_dates'])){
			$data['planning_dates']=$in['planning_dates'];
			$planning_dates=$in['planning_dates'];
		}else{
			$planning_dates=array();
			$savedRec=$this->db->query("SELECT * FROM servicing_recurring WHERE service_id='".$in['service_id']."' AND `date`>='".$in['rec_first_day']."' ");
			while($savedRec->next()){
				array_push($planning_dates,$savedRec->f('date'));
			}
			$data['planning_dates']=$planning_dates;
		}
		if($in['planning_users'] && !empty($in['planning_users'])){
			$data['planning_users']=$in['planning_users'];
		}else{
			$data['planning_users']=array();
			$users=$this->db->query("SELECT user_id FROM servicing_support_users WHERE service_id='".$in['service_id']."'");
			while($users->next()){
				array_push($data['planning_users'],$users->f('user_id'));
			}
		}

		if(is_numeric($in['start_date'])){
			$data['start_date']=$in['start_date']*1000;
			$now			= $in['start_date'];
		}else{
			$data['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}	

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
				$filter = '';
				break;
			default:
				$filter = " AND servicing_support.service_id IN (SELECT service_id FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' GROUP BY service_id )";
				break;
		}

		if($in['activeType']=='1'){
			$today_of_week 		= date("N", $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;

			$data['date_txt']= date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end);

			for($i = $week_start; $i <= $week_end; $i += 86400)
			{
				array_push($data['days_nr'],array('day_name' => gm(date('D',$i)),'date_month' => date('d',$i).' '.gm(date('M',$i)),'end_day'=> date("N",$i)>5 ? true : false));
			}

			$j=0;

			foreach($select as $row => $val){
				$day_data=array();
				$sheet = $this->db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
							INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
							WHERE servicing_support.planeddate BETWEEN '".$week_start."' AND '".$week_end."'
							AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ")->getAll();

				$start_day=$week_start;
				while($start_day <= $week_end){
					$start_d=$start_day;
					$end_d=mktime(23,59,59,date('n',$start_d),date('j',$start_d),date('Y',$start_d));
					$blocked_day=$this->db->field("SELECT id FROM servicing_block WHERE `date`='".$start_d."' AND user_id='".$val['id']."' ");

					$service_nr=0;
					$services_data=array();
					foreach($sheet as $key=>$value){
						if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
							$services_data_temp=array(
								'tmsmp'		=> $value['planeddate']
							);
							array_push($services_data,$services_data_temp);
							$service_nr++;
						}
					}
					$is_proposed=false;
					foreach($planning_dates as $key=>$value){
						if($value >= $start_d && $value <= $end_d){
							$is_proposed=true;
							break;
						}
					}			

					$temp_day_data=array(
						'day_txt'		=> date('d',$end_d),
						'service_nr'	=> $service_nr,
						'services_data'	=> $services_data,
						'tmsp'		=> $start_d,
						'block_day'		=> $blocked_day ? true : false,
						'is_proposed'	=> $is_proposed
					);
					array_push($day_data, $temp_day_data);
					$start_day=$start_day+86400;
				}

				$temp_users=array(
					'data'		=> $day_data,
					'user_id'	=> $val['id'],
					'name'		=> $val['value'],
					'added'		=> in_array($val['id'], $data['planning_users']) ? true : false,
				);
				array_push($data['user_data'],$temp_users);
			}
		}else{
			$month = date('n',$now);
			$year = date('Y',$now);
			$data['date_txt']= date('F',$now);
			$month_t = mktime(0,0,0,$month,1,$year);

			$month_days = date('t',$month_t);
			$month_first_day = date('N',$month_t);
			$month_last_day=mktime(23,59,59,$month,$month_days,$year);

			for($i=1;$i<=$month_days;$i++){
				$day_tmsp=mktime(23,59,59,$month,$i,$year);
				array_push($data['days_nr'],array('day'=>$i,'end_day'=> date("N",$day_tmsp)>5 ? true : false));
			}

			foreach($select as $row => $val){
				$day_data=array();
				$sheet = $this->db->query("SELECT servicing_support.*, cast( FROM_UNIXTIME( planeddate ) AS DATE ) AS p_date FROM servicing_support
								INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
								WHERE servicing_support.planeddate BETWEEN '".$month_t."' AND '".$month_last_day."'
								AND servicing_support_users.user_id='".$val['id']."' AND servicing_support.active='1' ".$filter." ORDER BY p_date, servicing_support.startdate ASC ")->getAll();

				$day=1;
				while($day <= $month_days){
					$start_d=mktime(0,0,0,$month,$day,$year);

					$end_d=mktime(23,59,59,$month,$day,$year);
					$blocked_day=$this->db->field("SELECT id FROM servicing_block WHERE `date`='".$start_d."' AND user_id='".$val['id']."' ");

					$service_nr=0;
					$services_data=array();
					foreach($sheet as $key=>$value){
						if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
							$services_data_temp=array(
								'tmsmp'=>$value['planeddate']
							);
							array_push($services_data,$services_data_temp);
							$service_nr++;
						}
					}
					$is_proposed=false;
					foreach($planning_dates as $key=>$value){
						if($value >= $start_d && $value <= $end_d){
							$is_proposed=true;
							break;
						}
					}

					$temp_day_data=array(
						'day_txt'		=> $day,
						'service_nr'	=> $service_nr,
						'services_data'	=> $services_data,
						'tmsp'		=> $start_d,
						'block_day'		=> $blocked_day ? true : false,
						'is_proposed'	=> $is_proposed
					);
					array_push($day_data, $temp_day_data);
					$day++;
				}

				$temp_users=array(
					'data'		=> $day_data,
					'user_id'	=> $val['id'],
					'name'		=> $val['value'],
					'added'		=> in_array($val['id'], $data['planning_users']) ? true : false,
				);
				array_push($data['user_data'],$temp_users);
			}
		}
		return $data;
	}

	public function get_Downpayment(&$in){
		$result=array();
		$service=$this->db->query("SELECT billable FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		$show_down=true;
		if(!$service->f('billable')){
			$intv_supply = $this->db->field("SELECT COUNT(a_id) FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND billable='1' ");
			if(!$intv_supply){
				$show_down=false;
			}
		}
		$result['show_down']=$show_down;
		return $result;
	}

	public function get_deals(&$in){
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND subject LIKE '".$q."%'";
		}
		$deals=$this->db->query("SELECT * FROM tblopportunity 
			WHERE buyer_id='".$in['buyer_id']."' $filter ORDER BY subject ASC");
		$items = array();
		while($deals->next()){
			array_push( $items, array('deal_id'=>$deals->f('opportunity_id'),'name'=>stripslashes($deals->f('subject'))));
		}

		if(defined('ADV_CRM') && ADV_CRM == 1){
			if($q){
		        array_push($items,array('deal_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		    }else{
		        array_push($items,array('deal_id'=>'99999999999','name'=>''));
		    }
		}
		return $items;
	}

	public function get_serviceModels(&$in){
	    $result = array('list'=>array(),'langs'=>array(), 'service_id'=>$in['service_id']);

	    $l_r =ROW_PER_PAGE;
	    $result['lr']=$l_r;
	    if((!$in['offset']) || (!is_numeric($in['offset'])))
	    {
	        $offset=0;
	        $in['offset']=1;
	    }
	    else
	    {
	        $offset=$in['offset']-1;
	    }

	    $filter = " active='2' ";

	    $filter_c=' ';

	    if(!empty($in['search'])){
	        $filter .= " AND serial_number LIKE '%".$in['search']."%' ";
	    }
	    if(!empty($in['lang'])){
	        $filter .= " AND email_language='".$in['lang']."' ";
	        $result['lang']=$in['lang'];
	    }
	    $max_rows =  $this->db->field("SELECT count(service_id) FROM servicing_support WHERE ".$filter );
	    $result['max_rows'] = $max_rows;
	    $res=$this->db->query("SELECT serial_number,service_id,email_language FROM servicing_support WHERE ".$filter." ORDER BY serial_number LIMIT ".$offset*$l_r.",".$l_r );
	    $j=0;
	    while($res->move_next()){
	        $lang_f=$this->db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$res->f('email_language')."' ");
	        if(!$lang_f){
	            $lang_f='EN';
	        }else if($lang_f=='du'){
	            $lang_f='NL';
	        }
	        $list=array(
	            'name'                      => $res->f('serial_number'),
	            'service_id'                => $res->f('service_id'),
	            'lang_f'            		=> $lang_f ? '('.strtoupper($lang_f).')' : '',
	        );
	        $result['list'][]=$list;
	    }
	    $languages=$this->db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order ");
	    while($languages->next()){
	        array_push($result['langs'],array('id'=>$languages->f('lang_id'),'name'=>gm($languages->f('language'))));
	    }
	    if($in['index']){
	        $result['index'] = $in['index'];
	    }
		return $result;
	}

	function get_components(&$in){
	  $lines = array( 'components'=>array());

	  $components=$this->db->query("SELECT pim_articles_combined.*, pim_articles.internal_name, pim_articles.item_code,pim_articles.hide_stock,pim_articles.stock FROM pim_articles_combined 
	                          LEFT JOIN pim_articles on pim_articles.article_id = pim_articles_combined.article_id
	                          WHERE parent_article_id='".$in['article_id']."'  ORDER BY pim_articles_combined.`sort_order` ASC ");
	  if(!$in['quantity']){
		$in['quantity']=1;
	  }
	  $is_components=false;
	  $total_components=0;
	  $i=0;

	  while ($components->next()){
	  	$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$components->f('article_id')."' AND pim_article_prices.base_price='1'");
	  	
	     $linie = array(
	      'tr_id'                   => 'tmp'.$i.strtotime('now'),
	      'is_vat'                  => false,
	      'component_id'            => $components->f('pim_articles_combined_id'),
	      'component_article_id'    => $components->f('article_id'),
	      'component_name'          => $components->f('internal_name'),
	      'component_code'          => $components->f('item_code'),
	      'parent_article_id'       => $in['article_id'],
	      'quantity_component'     => display_number($components->f('quantity')),
	      'quantity_nr'            => $components->f('quantity')*$in['quantity'],
	      'quantity'                => display_number($components->f('quantity')*$in['quantity']),
	      'purchase_price' 			=> $purchase_price,
	      'percent'                 => 0,
	      'percent_x'               => display_number(0),
	      'vat_value'               => 0,
	      'vat_value_x'             => display_number(0),
	      'disc_val'                => display_number(0),
	      'price'                   => display_number_var_dec(0),
	      'price_vat'               => display_number_var_dec(0),
	      'line_total'              => display_number(0),
	      'td_width'            	=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
	      'input_width'        		=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
	      'hide_disc'          		=> $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
	      'visible'          		=> $components->f('visible')? true :false,
	      'hstock'			=> $components->f('hide_stock') ? '1' : '0', 
	      'stock'						=> $components->f('stock'),
	    );

	    array_push($lines['components'], $linie);
	    $is_components=true;
	    $i++;
	  }
	  $lines['is_components'] = $is_components;
	  return $lines;
	}

	public function getViewService(){
		$in=$this->in;
		global $config;
		$output=array();
		$service_exists = $this->db->field("SELECT service_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");

		if(!$service_exists){
			msg::error('Service does not exist','error');
			$in['item_exists']= false;
		    json_out($in);
		}

		$service =	$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' AND is_recurring='0' ");
		if(!$service->next()){
			return ark::run('maintenance-services');
		}



		$output['item_exists']=true;
		$output['service_id']=$in['service_id'];
		$output['serial_number'] = $service->f('serial_number');
		if($service->f('status')=='1'){
		    $type_title = gm('Planned');
		}elseif($service->f('status')=='2'){
		    if($service->f('is_recurring')=='0'&& $service->f('active')=='1'&& $service->f('accept')=='1'){
		        $type_title = gm('Accepted');
		    }else{
		        $type_title = gm('Closed');
		    }
		}else{
		    $type_title = gm('Draft');
		}
		$nr_status= $service->f('status');
		if(!$service->f('active')){
			$type_title=gm('Archived');
			$nr_status=0;
		}
		$output['type_title']=$type_title;
		$output['nr_status']=$nr_status;
		$output['status']=$service->f('status');
		$output['planeddate'] = $service->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$service->f('planeddate')) : '';
		$output['subject'] = stripslashes($service->f('subject'));

		if(defined('USE_MAINTENANCE_WEB_LINK') && USE_MAINTENANCE_WEB_LINK == 1){
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['service_id']."' AND `type`='m'  ");
		}

		$output['web_url']		= $config['web_link_url'].'?q=';
		$output['external_url']		= $service->f('status') <= 1 ? false : $exist_url;
		$output['buyer_id']=$service->f('customer_id');
		$in['buyer_id']=$service->f('customer_id');
		$in['contact_id']=$service->f('contact_id');
		$output['buyer_name']= stripslashes($service->f('customer_name'));
		$output['buyer_phone'] = strip_tags($service->f('customer_phone'));
		if(!$output['buyer_phone']){
			$customer_phone=$this->db->field("SELECT customers.comp_phone FROM customers WHERE customer_id='".$service->f('customer_id')."' ");
			if($customer_phone){
				$output['buyer_phone']=$customer_phone;
			}
		}
		$output['buyer_fax']=strip_tags($service->f('customer_fax'));
		$output['buyer_email']=strip_tags($service->f('customer_email'));
		if($service->f('contact_id')){
			$contact = $this->db->query(" SELECT CONCAT_WS(' ',firstname, lastname) as contact_name, cell, email FROM customer_contacts
									WHERE contact_id='".$service->f('contact_id')."' ");
			$output['contact_name'] 		= $contact->f('contact_name');

			if($service->f('customer_id')){
				$contact = $this->db->query(" SELECT phone as cell, email FROM customer_contactsIds
										WHERE contact_id='".$service->f('contact_id')."' AND customer_id= '".$service->f('customer_id')."'");
			}			
			$output['contact_phone'] 		= $contact->f('cell');
			$output['contact_email'] 		= $contact->f('email');
		}

		$free_field = $service->f('customer_address').'
		'.$service->f('customer_zip').' '.$service->f('customer_city').'
		'.get_country_name($service->f('customer_country_id'));

		$output['free_field']	= trim($service->f('free_field')) ? nl2br($service->f('free_field')) : nl2br($free_field );
		$output['is_active']=$service->f('active') == 1 ? true : false;
		$output['is_archived']=$service->f('active') == 0 ? true : false;

		$output['deliveries']=[];
		$deliveries = $this->db->query("SELECT * FROM service_deliveries WHERE service_id='".$in['service_id']."' ORDER BY delivery_id ASC");
		while($deliveries->next()){
			$query=array(
				'delivery_id'		=> $deliveries->f('delivery_id'),
				'date'			=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('date')),
			);
			array_push($output['deliveries'], $query);
		}	

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$is_service_manager = $this->db->field("SELECT pr_m FROM servicing_support_users WHERE user_id='".$_SESSION['u_id']."' AND service_id='".$in['service_id']."' ");
		$int_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $is_service_manager && $_SESSION['team_int']:
			case $int_user:
				$can_edit = true;
				break;
			default:
				$can_edit = false;
				break;
		}
		$output['can_edit_general']=$can_edit;
		$output['can_edit']=false;
		$output['edit'] = false;
		$output['is_team']=$_SESSION['team_int'];
		$output['segment_id']=$service->f('segment_id');
    	$output['source_id']=$service->f('source_id');
    	$output['type_id']=$service->f('type_id');
    	$output['articles']		= $this->get_serviceArticles($in);

    	$invoice_nr_row = array();
    	$linked_downpayment_invoice=false;
    	$inv_nr = $this->db->query("SELECT tblinvoice.id,tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.due_date, tblinvoice.paid,tblinvoice.f_archived,tbldownpayments.id as downpayent_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id = tracking.trace_id
						INNER JOIN tblinvoice ON tracking.target_id = tblinvoice.id
						LEFT JOIN tbldownpayments ON tblinvoice.id = tbldownpayments.invoice_id AND tbldownpayments.service_id='".$in['service_id']."'
						WHERE tracking_line.origin_id = '".$in['service_id']."' AND tracking_line.origin_type ='5' AND (tracking.target_type ='1' OR tracking.target_type ='7') ")->getAll();

    	foreach($inv_nr as $key=>$value){
			$type_title='';
		    switch ($value['type']){
			case '0':
			    $type_title = gm('Regular invoice');
			    break;
			case '1':
			    $type_title = gm('Proforma invoice');
			    break;
			case '2':
			    $type_title = gm('Credit Invoice');
			    break;
		    }	    

		    if($value['sent'] == '0' && $value['type']!='2'){
				$type_title = gm('Draft');
		    }else if($value['status']=='1'){
				$type_title = $value['paid']=='2' ? gm('Partially Paid') : gm('Paid');
		    }else if($value['sent']!='0' && $value['not_paid']=='0'){
				$type_title = $value['status']=='0' && $value['due_date']< time() ? gm('Late') : gm('Final');
		    } else if($value['sent']!='0' && $value['not_paid']=='1'){
				$type_title = gm('No Payment');
		    }

		    $downp_txt = "";
		    if($value['downpayent_id']){
		    	$downp_txt = gm('Downpayment');
		    	$linked_downpayment_invoice=true;
		    }

	    	array_push($invoice_nr_row, array( 'serial' => ($value['f_archived']=='1' ? '': $value['serial_number']), 'id'=> $value['id'], 'status'=> ($value['f_archived']=='1' ? gm('Archived') : $type_title),'downp_txt'=>$downp_txt));
	    
		}
		$output['downpayment_made']=$linked_downpayment_invoice ? true : false;
		$output['invoices'] = $invoice_nr_row;

		$p_order_nr_row = array();

		$pord_nr = $this->db->query("SELECT tracking.*, tracking_line.*, pim_p_orders.serial_number, pim_p_orders.sent, pim_p_orders.invoiced, pim_p_orders.rdy_invoice,pim_p_orders.active FROM tracking_line
								INNER JOIN tracking ON tracking_line.trace_id = tracking.trace_id
								INNER JOIN pim_p_orders ON tracking.target_id = pim_p_orders.p_order_id
								WHERE tracking_line.origin_id = '".$in['service_id']."' AND tracking_line.origin_type ='5' AND tracking.target_type ='6' ");

		while($pord_nr->move_next()){
			$status ='';
				if($pord_nr->f('sent')==0 ){
					$status = gm('Draft');
				}
				if($pord_nr->f('invoiced')){
					if($pord_nr->f('rdy_invoice') == 1){
						$status = gm('Received');
					}else{
						$status = gm('Final');
					}
				}else{
					if($pord_nr->f('rdy_invoice') == 2){
						$status = gm('Final');
					}else{
						if($pord_nr->f('rdy_invoice') == 1){
							$status = gm('Received');
						}else{
							if($pord_nr->f('sent') == 1){
								$status = gm('Final');
							}else{
								$status = gm('Draft');
							}
						}
					}
				}

				$is_p_delivery_planned=false;
				if(P_ORDER_DELIVERY_STEPS==2){
					if($pord_nr->f('rdy_invoice') == 2){
						$dels_p=$this->db->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id = '".$pord_nr->f('target_id')."' ");
						while($dels_p->next()){
							if(!$dels_p->f('delivery_done')){
								$is_p_delivery_planned=true;
							}
						}
					}
				}
				if($is_p_delivery_planned){
					$status=gm('Delivery planned');
				}

		    	array_push($p_order_nr_row, array( 'serial' => (!$pord_nr->f('active') ? '' : $pord_nr->f('serial_number')), 'id'=> $pord_nr->f('target_id'), 'status'=> (!$pord_nr->f('active') ? gm('Archived') : $status))); 
		}

		$output['p_orders'] = $p_order_nr_row;

		$deal_nr_row=array();
		if($service->f('trace_id')){
			$linked_deals=$this->db->query("SELECT tracking_line.line_id,tblopportunity.opportunity_id,tblopportunity.subject FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						INNER JOIN tblopportunity ON tracking_line.origin_id=tblopportunity.opportunity_id
						WHERE tracking_line.origin_type='11' AND tracking.target_type='5' AND tracking_line.trace_id='".$service->f('trace_id')."' ");
			while($linked_deals->next()){
				array_push($deal_nr_row, array('id'=>$linked_deals->f('opportunity_id'),'name'=>stripslashes($linked_deals->f('subject'))));
			}
		}	
		$output['deals'] = $deal_nr_row;

		if($service->f('installation_id')){
			$output['installation_name']=$this->db->field("SELECT serial_number FROM installations WHERE id='".$service->f('installation_id')."' ");
		}

		$show_down=true;
		if(!$service->f('billable')){
			$intv_supply = $this->db->field("SELECT COUNT(a_id) FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND billable='1' ");
			if(!$intv_supply){
				$show_down=false;
			}
		}
		$output['show_down']=$show_down;
		$use_stock_reservation=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_STOCK_RESERVATION'");
		$output['use_stock_reservation'] = $use_stock_reservation==1 ? true : false;
		$output['planeddate_js'] = $in['planeddate_js'] ? strtotime($in['planeddate_js'])*1000 : ($service->f('planeddate') ? ($service->f('planeddate')-$_SESSION['user_timezone_offset'])*1000 : '');

		$output['users']			= $this->get_serviceUser($in);
		$output['hours'] 			= $this->get_serviceHours($in);
		$output['expense']		= $this->get_serviceExpense($in);
		$output['tasks']			= $this->get_serviceSubTasks($in);
		$output['supplies']		= $this->get_serviceSupplies($in);
		$output['user_auto']		= $this->get_autoUsers($in);
		$output['autoSrvUsers']		= $this->get_autoSrvUsers($in);
		$output['expenseCat']		= $this->get_expenseCat($in);

		$mail_setting_option = $this->db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
		$output['sendgrid_selected'] = false;
		if($mail_setting_option==3){
			$output['sendgrid_selected'] =true;
		}
		$output['articles_list']	= $this->get_articles_list($in);
		$output['services']		= $this->get_serviceSubServices($in);
		$in['only_service']		= true;
		$output['services_list']	= $this->get_articles_list($in);
		$data['loged_user'] 				= $_SESSION['u_id'];
		$output['date_pick_format'] = pick_date_format();
		$output['email_language'] = $service->f('email_language');
		$output['include_pdf']	= $in['include_pdf'];

		$output['startdate'] = $in['startdate'] ? number_as_hour($in['startdate']) : ($service->f('startdate') ? number_as_hour($service->f('startdate')) : '');
		$output['enddate'] = $service->f('enddate') ? date(ACCOUNT_DATE_FORMAT,$service->f('enddate')) : '';
		$output['duration'] = $in['duration'] ? number_as_hour($in['duration']) : ($service->f('duration') ? number_as_hour($service->f('duration')) : '');
		$output['s3_info']=array('s3_folder'=>'intervention','id'=>$in['service_id']);

		$output['startdate_txt']=$service->f('startdate') ? number_as_hour($service->f('startdate')) : '';
		$output['enddate_txt']='';
		if($service->f('startdate') && $service->f('duration')){
			if($service->f('startdate')+$service->f('duration')>24){
				$output['enddate_txt']=date(ACCOUNT_DATE_FORMAT,$service->f('planeddate')+86400).' '.number_as_hour($service->f('startdate')+$service->f('duration')-24);				
			}else{
				$output['enddate_txt']=number_as_hour($service->f('startdate')+$service->f('duration'));
			}
		}

		$output['planeddate_ts'] = $service->f('planeddate');
		$output['planeddate_ts_js'] = $service->f('planeddate')*1000;
		$output['service_type']= $service->f('service_type');
		$output['cat_id'] = $service->f('customer_id') ? $this->db->field('SELECT cat_id FROM customers WHERE customer_id ='.$service->gf('customer_id')) : 0;

		$message_data=get_sys_message('servicemess',$service->f('email_language'));
		$subject=$message_data['subject'];

		$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
		$subject=str_replace('[!SERIAL_NUMBER!]',$service->f('serial_number'), $subject);
		$subject=str_replace('[!INTERVENTION_DATE!]',$output['planeddate'], $subject);
		$subject=str_replace('[!INTERVENTION_HOUR!]',$output['startdate'], $subject);
		$subject=str_replace('[!CUSTOMER!]',$service->f('customer_name'), $subject);

		$body=$message_data['text'];
		$body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
		$body=str_replace('[!CUSTOMER!]',$service->f('customer_name'), $body);
		$body=str_replace('[!SERIAL_NUMBER!]',$service->f('serial_number'), $body);
		$body=str_replace('[!INTERVENTION_DATE!]',$output['planeddate'], $body);
		$body=str_replace('[!INTERVENTION_HOUR!]',$output['startdate'], $body);

		$body=str_replace('[!SITE_ADDRESS!]',$site_address, $body);
		$body=str_replace('[!SITE_ZIP!]',$site_zip, $body);
		$body=str_replace('[!SITE_CITY!]', $site_city, $body);
		$body=str_replace('[!SITE_COUNTRY!]', $site_country, $body);

		if($message_data['use_html']){
		    $body=str_replace('[!SIGNATURE!]',get_signature(), $body);
		 }else{
			  $body=str_replace('[!SIGNATURE!]','', $body);
		}

		$output['e_subject']              = $in['e_subject'] ? $in['e_subject'] : $subject;
		$output['e_message']            = $in['e_message'] ? stripslashes($in['e_message']) : $body;

		$account_manager_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$account_user_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$service->f('acc_manager_id')."'");
		$output['use_html']	= $message_data['use_html'];
		$output['recipients']		= $this->get_recipients($in);

		if($service->f('contact_id')){
			if($service->f('customer_id')){
				$contact_email = $this->db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$service->f('contact_id')."' AND customer_id = '".$service->f('customer_id')."'");
			}else{
				$contact_email = $this->db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$service->f('contact_id')."'");
			}
			if($contact_email){
				$cc_email = $contact_email;
			}else{
				$customer_email = $this->db->field("SELECT c_email FROM customers WHERE customer_id='".$service->f('customer_id')."'");
				if($customer_email){
					$cc_email = $customer_email;
				}
			}
		}else{
			$customer_email = $this->db->field("SELECT c_email FROM customers WHERE customer_id='".$service->f('customer_id')."'");
				if($customer_email){
					$cc_email = $customer_email;
				}
		}

		if(!isset($in['include_pdf']))
		{
			$in['include_pdf'] = true;
		}

		$output['email'] = array(
			'e_subject'			=> $subject,
			'dropbox_files'		=> '',
			'language_email' 	=> $service->f('email_language'),
			'e_message' 		=> $in['e_message'] ? $in['e_message'] : ($body),
			'lid'				=> $service->f('email_language'),
			'include_pdf'		=> $in['include_pdf'] ? true : false,
			'id'				=> $in['service_id'],
			'service_id'		=> $in['service_id'],
			'files'				=> array(),
			'use_html'			=> $message_data['use_html'],
			'serial_number'		=> $service->f('serial_number'),
			'recipients'		=> $cc_email,
			'save_disabled'		=> $cc_email? false:true,
			'alerts'			=> array(),
			'user_loged_email'	=> $account_manager_email,
			'user_acc_email'	=> $account_user_email,
		);

		

		$invoice_link=array();
		$invoice_link['service_id']=$in['service_id'];
		$invoice_link['base_type']=6;
		$invoice_link['languages']=$service->f('email_language');
		if($service->f('customer_id')){
			$invoice_link['buyer_id']=$service->f('customer_id');
			$invoice_link['customer_name']=$service->f('customer_name');
			$invoice_link['contact_id']=$service->f('contact_id');
		}else{
			$invoice_link['buyer_id']=0;
			$invoice_link['contact_id']=$service->f('contact_id');
			$invoice_link['contact_name']=$service->f('contact_id') ? $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$service->f('contact_id')."' ") : '';
		}
		$output['invoice_link']=$invoice_link;
		$output['rate'] = display_number($service->f('rate'));
		$output['ACCOUNT_CURRENCY'] 	= get_currency(ACCOUNT_CURRENCY_TYPE);
		$output['vat_regim_dd']		= build_vat_regime_dd();
		$output['pdf_link']= 'index.php?do=maintenance-print&service_id='.$in['service_id'].'&lid='.$service->f('email_language');
		$output['not_accepted'] = $service->f('accept') == 1 || $service->f('billed') == 1 ? false : true;

		$show_inv_create=true;
		if(!$service->f('billable')){
			$intv_supply = $this->db->field("SELECT COUNT(a_id) FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND delivered<>0 AND billable='1' AND article_id='0' ");
			$intv_article = $this->db->field("SELECT COUNT(service_delivery.a_id) FROM service_delivery
				INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
				WHERE service_delivery.service_id='".$in['service_id']."' AND service_delivery.invoiced='0' AND servicing_support_articles.billable='1' AND servicing_support_articles.article_id!='0' GROUP BY service_delivery.a_id");
			$intv_expense = $this->db->field("SELECT COUNT(project_expenses.id) FROM project_expenses
									INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
									INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
									WHERE servicing_support.service_id='".$in['service_id']."' AND project_expenses.billable='1' ORDER BY id");
			if(!$intv_supply && !$intv_article && !$intv_expense){
				$show_inv_create=false;
			}
		}
		if($service->f('billed')){
			$show_inv_create=false;
		}
		$output['show_inv_create']=$show_inv_create;
		$toTasks = $this->db->field("SELECT count(task_id) FROM servicing_support_tasks WHERE service_id = '".$in['service_id']."' AND closed='0' ");
		$toSupply=$this->db->field("SELECT count(a_id) FROM servicing_support_articles WHERE service_id = '".$in['service_id']."' AND quantity>delivered AND article_id='0' ");
		$generalArt = $this->db->field("SELECT SUM(quantity) FROM servicing_support_articles WHERE service_id = '".$in['service_id']."' AND article_id!='0' ");
		$artDel=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' ");
		$toArt=$generalArt-$artDel;	
		$output['can_duplicate']= ($toTasks > 0 || $toSupply>0 || $toArt > 0) ? true : false;
		$output['installation_id']  = $service->f('installation_id');
		$output['ALLOW_STOCK']	= ALLOW_STOCK==1 ? true : false;
		$articles_denied=$this->db_users->field("SELECT plan FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$output['articles_allowed']= ($articles_denied==2 || $articles_denied==5) ? false : true;
		//$output['allow_model']=!$service->f('model_id') ? true : false;
		$output['allow_model']=true;
		if(!$service->f('status')){	
			$in['planeddate_js'] = $in['planeddate_js'] ? strtotime($in['planeddate_js'])*1000 : ($service->f('planeddate') ? ($service->f('planeddate')-$_SESSION['user_timezone_offset'])*1000 : '');
			$output['planning']=$this->get_planUsers($in);
		}
		$output['finishDate_ts_js'] = $service->f('finishDate')*1000;
		$output['report'] = $service->f('report');
		$output['invoicing_type']=$service->f('service_type') == 1 ? gm('Fixed price') : ($service->f('service_type') == 2 ? gm('Not Billable') : gm('Time & Expenses'));
		$output['color_ball']='#'.$service->f('color_ball');
		$output['your_ref']=stripslashes($service->f('your_ref'));
		$has_admin_rights 					= getHasAdminRights(array('module'=>'maintenance'));
		$deny_view_article_price=false;
		if(!$has_admin_rights && defined('NO_ACCESS_PRICE_ARTICLE_INT_REG_USER') && NO_ACCESS_PRICE_ARTICLE_INT_REG_USER =='1'){
			$deny_view_article_price=true;
		}
		$output['deny_view_article_price']=$deny_view_article_price;

		$this->out = $output;
	}


}
	$service = new ServiceEdit($in,$db);

	/*if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $service->output($service->$fname($in));
	}*/
	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    if( method_exists($service, $fname) ){
	    	$service->output( $service->$fname($in) );	
	    }else{
	    	msg::error('Method does not exist','error');
	    	$service->output($in);
	    }    
	}
	$service->getService();
	$service->output();

?>
	