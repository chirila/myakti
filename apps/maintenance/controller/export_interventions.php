<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

	ini_set('memory_limit', '1000M');
	$in['services_ids'] ='';
	if($_SESSION['add_to_pdf_int']){
	  foreach ($_SESSION['add_to_pdf_int'] as $key => $value) {
	    $in['services_ids'] .= $key.',';
	  }
	}
    if(isset($_SESSION['tmp_add_to_pdf_int'])){
        unset($_SESSION['tmp_add_to_pdf_int']);
    }
    if(isset($_SESSION['add_to_pdf_int'])){
        unset($_SESSION['add_to_pdf_int']);
    }
	$in['services_ids'] = rtrim($in['services_ids'],',');
	if(!$in['services_ids']){
	  $in['services_ids']= '0';
	}

	$headers = array("SERIAL NUMBER",
		"PLANNED DATE",
		"CUSTOMER NAME",
		"INTERVENTION MANAGER",
		"SUBJECT",
		"STATUS",
		"ACCOUNT PHONE NUMBER",
		"ACCOUNT EMAIL",
		"CONTACT PHONE NUMBER",
		"CONTACT EMAIL",
		"SITE LOCATION"
	);
	$db = new sqldb();

	$filename ="export_interventions.csv";
	$contacts=array();
	$managers_ar=array();
	$final_data=array();

	$intervention_data=$db->query("SELECT servicing_support.service_id,servicing_support.planeddate,servicing_support.customer_name,
	servicing_support.subject,servicing_support.serial_number,servicing_support.status,servicing_support.accept,servicing_support.customer_id,servicing_support.contact_id,customers.comp_phone,customers.c_email,customer_addresses.city
	FROM servicing_support
			LEFT JOIN customers ON servicing_support.customer_id=customers.customer_id
			LEFT JOIN customer_addresses ON customer_addresses.address_id=(CASE WHEN servicing_support.same_address>'0' THEN servicing_support.same_address ELSE servicing_support.main_address_id END)
			WHERE servicing_support.service_id IN (".$in['services_ids'].") GROUP BY servicing_support.service_id ORDER BY servicing_support.serial_number DESC ");
	
	while($intervention_data->next()){
		$contact_email="";
		$contact_phone="";
		if($intervention_data->f('customer_id') && $intervention_data->f('contact_id')){
			if(array_key_exists($intervention_data->f('customer_id').'-'.$intervention_data->f('contact_id'), $contacts) === false){
				$contacts[$intervention_data->f('customer_id').'-'.$intervention_data->f('contact_id')]=array('contact_email'=>'','contact_phone'=>'');
				$contacts_data=$db->query("SELECT * FROM customer_contactsIds WHERE customer_id='".$intervention_data->f('customer_id')."' AND contact_id='".$intervention_data->f('contact_id')."' ");
				$contacts_data->next();
				if($contacts_data->f('email')){
					$contact_email=$contacts_data->f('email');
					$contacts[$intervention_data->f('customer_id').'-'.$intervention_data->f('contact_id')]['contact_email']=$contacts_data->f('email');
				}
				if($contacts_data->f('phone')){
					$contact_phone=$contacts_data->f('phone');
					$contacts[$intervention_data->f('customer_id').'-'.$intervention_data->f('contact_id')]['contact_phone']=$contacts_data->f('phone');
				}
			}else{
				$contact_email=$contacts[$intervention_data->f('customer_id').'-'.$intervention_data->f('contact_id')]['contact_email'];
				$contact_phone=$contacts[$intervention_data->f('customer_id').'-'.$intervention_data->f('contact_id')]['contact_phone'];
			}		
		}
		$company_email="";
		$company_phone="";
		if($intervention_data->f('customer_id')){
			$company_email=$intervention_data->f('c_email');
			$company_phone=$intervention_data->f('comp_phone');
		}	
		$managers = $db->query("SELECT user_id FROM servicing_support_users WHERE service_id ='".$intervention_data->f('service_id')."' AND pr_m = '1' ")->getAll();
		$managers_list ='';
		foreach($managers as $k =>$v){
			if(in_array($v['user_id'], $managers_ar)){
				$managers_list .= $managers_ar[$v['user_id']].', '; 
			}else{
				$tmp_user_name=get_user_name($v['user_id']);
				$managers_list.= $tmp_user_name.', '; 
				$managers_ar[$v['user_id']]=$tmp_user_name;
			}
		}
		$managers_list = rtrim($managers_list,', ');
		if($intervention_data->f('status')=='1'){
			$status = gm('Planned');
		}elseif($intervention_data->f('status')=='2'){
			if($intervention_data->f('active')=='1'&&$intervention_data->f('accept')=='1'){
				$status = gm('Accepted');
			}else{
				$status = gm('Closed');
			}
		}else{
			$status = gm('Draft');
		}		
			
		$tmp_line=array(
			$intervention_data->f('serial_number'),
			($intervention_data->f('planeddate') ? date(ACCOUNT_DATE_FORMAT,$intervention_data->f('planeddate')) : ''),
			stripslashes(preg_replace("/[\n\r]/"," ",$intervention_data->f('customer_name'))),
			$managers_list,
			stripslashes(preg_replace("/[\n\r]/"," ",$intervention_data->f('subject'))),
			$status,
			"\t".$company_phone,
			"\t".$company_email,
			"\t".$contact_phone,
			"\t".$contact_email,
			stripslashes(preg_replace("/[\n\r]/","",$intervention_data->f('city')))
		);
		array_push($final_data,$tmp_line);
	}
/*
	$from_location=INSTALLPATH.'upload/'.DATABASE_NAME.'/tmp_export_interventions.csv';
    $fp = fopen($from_location, 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);

    ark::loadLibraries(array('PHPExcel'));
	$objReader = PHPExcel_IOFactory::createReader('CSV');

	$objPHPExcel = $objReader->load($from_location);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	unlink($from_location);

	doQueryLog();

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="export_interventions.xls"');
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');   
	exit();*/

	header('Content-Type: application/excel');
	header('Content-Disposition: attachment;filename="'.$filename.'"');

	$fp = fopen("php://output", 'w');
	fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
	fputcsv($fp, $headers);
	foreach ( $final_data as $line ) {
		fputcsv($fp, $line);
	}
	fclose($fp);
	doQueryLog();
	exit();


	// set column width to auto
	foreach(range('A','W') as $columnID) {
		$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}

	$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
	$rows_format='A1:A'.$xlsRow;
	$objPHPExcel->getActiveSheet()->getStyle($rows_format)
		->getNumberFormat()
		->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	$objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('upload/'.$filename);





	define('ALLOWED_REFERRER', '');
	define('BASE_DIR','upload/');
	define('LOG_DOWNLOADS',false);
	define('LOG_FILE','downloads.log');
	$allowed_ext = array (

	// archives
	'zip' => 'application/zip',

	// documents
	'pdf' => 'application/pdf',
	'doc' => 'application/msword',
	'xls' => 'application/vnd.ms-excel',
	'ppt' => 'application/vnd.ms-powerpoint',
	'csv' => 'text/csv',

	// executables
	//'exe' => 'application/octet-stream',

	// images
	'gif' => 'image/gif',
	'png' => 'image/png',
	'jpg' => 'image/jpeg',
	'jpeg' => 'image/jpeg',

	// audio
	'mp3' => 'audio/mpeg',
	'wav' => 'audio/x-wav',

	// video
	'mpeg' => 'video/mpeg',
	'mpg' => 'video/mpeg',
	'mpe' => 'video/mpeg',
	'mov' => 'video/quicktime',
	'avi' => 'video/x-msvideo'
	);
	####################################################################
	###  DO NOT CHANGE BELOW
	####################################################################

	// If hotlinking not allowed then make hackers think there are some server problems
	if (ALLOWED_REFERRER !== ''
	&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
	) {
	die("Internal server error. Please contact system administrator.");
	}
	$fname = basename($filename);
	function find_file ($dirname, $fname, &$file_path) {
	$dir = opendir($dirname);
	while ($file = readdir($dir)) {
		if (empty($file_path) && $file != '.' && $file != '..') {
		if (is_dir($dirname.'/'.$file)) {
			find_file($dirname.'/'.$file, $fname, $file_path);
		}
		else {
			if (file_exists($dirname.'/'.$fname)) {
			$file_path = $dirname.'/'.$fname;
			return;
			}
		}
		}
	}

	} // find_file
	// get full file path (including subfolders)
	$file_path = '';
	find_file('upload', $fname, $file_path);
	if (!is_file($file_path)) {
	die("File does not exist. Make sure you specified correct file name.");
	}
	// file size in bytes
	$fsize = filesize($file_path);
	// file extension
	$fext = strtolower(substr(strrchr($fname,"."),1));
	// check if allowed extension
	if (!array_key_exists($fext, $allowed_ext)) {
	die("Not allowed file type.");
	}
	// get mime type
	if ($allowed_ext[$fext] == '') {
	$mtype = '';
	// mime type is not set, get from server settings
	if (function_exists('mime_content_type')) {
		$mtype = mime_content_type($file_path);
	}
	else if (function_exists('finfo_file')) {
		$finfo = finfo_open(FILEINFO_MIME); // return mime type
		$mtype = finfo_file($finfo, $file_path);
		finfo_close($finfo);
	}
	if ($mtype == '') {
		$mtype = "application/force-download";
	}
	}
	else {
	// get mime type defined by admin
	$mtype = $allowed_ext[$fext];
	}
	doQueryLog();
	// set headers
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Type: $mtype");
	header("Content-Disposition: attachment; filename=\"$fname\"");
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . $fsize);
	// download
	//@readfile($file_path);
	$file = @fopen($file_path,"rb");
	if ($file) {
	while(!feof($file)) {
		print(fread($file, 1024*8));
		flush();
		if (connection_status()!=0) {
		@fclose($file);

		die();
		}
	}
	@fclose($file);
	}
?>