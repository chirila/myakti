<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $cfg,$database_config, $config;

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$user_lang = $_SESSION['l'];
if ($user_lang=='nl')
{
	$user_lang='du';
}
switch ($user_lang) {
	case 'en':
		$user_lang_id='1';
		break;
	case 'du':
		$user_lang_id='3';
		break;
	case 'fr':
		$user_lang_id='2';
		break;
}
if(!$in['languages']){
	$in['languages'] = $user_lang_id;
}
$in['language_dd2'] = build_language_dd_new($in['languages']);
$in['multiple_identity'] = build_identity_dd($selected);
$in['add_country_id'] = @constant("MAIN_COUNTRY");
$in['add_ccountry_id'] = @constant("MAIN_COUNTRY");
if(!$in['add_country_id']){
	$in['add_country_id'] = '26';
}
if(!$in['add_ccountry_id']){
	$in['add_ccountry_id'] = '26';
}
$result = array(
	'user_auto'=>get_autoUsers($in,true,false),
	'country_dd'=>build_country_list(),
);

json_out($result);

function get_autoUsers($in,$showin=true,$exit=true){

    global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$db = new sqldb($db_config);
		$q = strtolower($in["term"]);

		$filter = " AND users.credentials like '%13%' ";

		if($q){
			$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
		}

		$db->query("SELECT first_name,last_name,users.user_id,h_rate FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");
		while($db->move_next()){
		  $users[utf8_encode($db->f('first_name').' '.$db->f('last_name'))]=$db->f('user_id');
		  $u[utf8_encode($db->f('first_name').' '.$db->f('last_name'))] = $db->f('h_rate');
		}

		$result = array();
		if($users){
			foreach ($users as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "h_rate" => $u[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key), "h_rate" => $u[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "value" => 'No matches', "h_rate" => 'No matches'));
		}
    return json_out($result, $showin,$exit);
}

?>