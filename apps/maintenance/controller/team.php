<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class TeamEdit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getTeam(){
		if($this->in['team_id'] == 'tmp'){ //add
			$this->getAddTeam();
		}elseif ($this->in['template']) { # create quote template
			$this->getTemplateTeam();
		}else{ # edit
			$this->getEditTeam();
		}
	}

	public function getAddTeam(){
		$in=$this->in;

		$output=array();

		$next_function = 'maintenance-team-team-add';

		$output['left_users']=$this->get_ActiveUsers($in);
		$output['team_id']=$in['team_id'];
		$output['users']=array();
		$output['do_next']=$next_function;
		$this->out = $output;
	}

	public function getEditTeam(){
		$in=$this->in;

		$output=array();

		$next_function = 'maintenance-team-team-update';

		$team_name=$this->db->field("SELECT name FROM servicing_team WHERE team_id='".$in['team_id']."' ");

		$output['users']=array();
		$team_members=$this->db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."' ORDER BY sort ASC");
		while($team_members->next()){
			$team_temp=array(
				'name'		=> htmlspecialchars_decode(stripslashes($this->db_users->field("SELECT CONCAT_WS(' ',last_name,first_name) FROM users WHERE user_id='".$team_members->f('user_id')."' "))),
				'user_id'		=> $team_members->f('user_id')
			);
			array_push($output['users'],$team_temp);
		}

		$output['name']=$team_name;
		$output['left_users']=$this->get_ActiveUsers($in);
		$output['team_id']=$in['team_id'];
		$output['do_next']=$next_function;
		$this->out = $output;
	}

	public function get_ActiveUsers(&$in){
		$data=array();
		
		$filter=" 1=1 ";
		if($in['team_id']!='tmp'){
			$filter.=" AND team_id='".$in['team_id']."' ";
		}
		$team_users=$this->db->query("SELECT user_id FROM servicing_team_users WHERE ".$filter." ")->getAll();
		$users=$this->db_users->query("SELECT users.* FROM users LEFT JOIN user_meta ON users.user_id=user_meta.user_id AND user_meta.name='active' WHERE database_name='".DATABASE_NAME."' AND users.active<>1000 AND user_meta.value IS NULL AND users.group_id!='2' GROUP BY users.user_id ORDER BY last_name");
		while($users->next()){
			$is_teamed=false;
			if($in['team_id']!='tmp'){
				foreach($team_users as $key=>$value){
					if(in_array($users->f('user_id'), $value)){
						$is_teamed=true;
						break;
					}
				}
			}	
			if(!$is_teamed){
				$temp_users=array(
					'name'		=> htmlspecialchars_decode(stripslashes($users->f('last_name').' '.$users->f('first_name'))),
					'user_id'		=> $users->f('user_id')
				);
				array_push($data,$temp_users);
			}		
		}
		return $data;
	}

}

	$team = new TeamEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $team->output($team->$fname($in));
	}

	$team->getTeam();
	$team->output();