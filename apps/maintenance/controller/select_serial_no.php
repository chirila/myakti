<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$result=array('serial_row'=>array(),'sel_serial_row'=>array());
$is_data = false;

if($in['a_id'] && $in['count_selected_s_n'] > 0){
	$article_id = $db->field("SELECT article_id FROM servicing_support_articles WHERE a_id = '".$in['a_id']."' ");
	if($in['selected_s_n']){
		$in['selected_s_n']=rtrim($in['selected_s_n'],',');		
		$is_data = true;
		$selected_s_n = count(explode(',', $in['selected_s_n']));
		$serial_numbers = $db->query("SELECT id,serial_number FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id NOT IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");			
		$sel_serial_no = $db->query("SELECT id,serial_number FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");	
	}else{		
		$selected_s_n = 0;
		$serial_numbers = $db->query("SELECT id,serial_number FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' ORDER BY serial_number ASC ");	
		$sel_serial_no = $db->query("SELECT id,serial_number FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id < '0' ");
	}
	$i=0;
	while($serial_numbers->next()){
		$serial_r=array(
			'serial_number'		=> $serial_numbers->f('serial_number'),
			'serial_nr_id'		=> $serial_numbers->f('id'),			
		);
		array_push($result['serial_row'], $serial_r);
		$i++;
	}

	$j=0;
	while($sel_serial_no->next()){
		$sel_serial_r=array(
			'serial_number'		=> $sel_serial_no->f('serial_number'),
			'serial_nr_id'		=> $sel_serial_no->f('id'),			
		);		
		array_push($result['sel_serial_row'], $sel_serial_r);
		$j++;
	}		
	$result['overflow_y']				= $i > 9 ?  'style="overflow-y:scroll;"' : '';	
	$result['sel_overflow_y']			= $j > 9 ?  'style="overflow-y:scroll;"' : '';	
	$result['sel_s_n']				= $selected_s_n ? $selected_s_n : 0;
	$result['total_s_n']				= $in['count_selected_s_n'];
	$result['not_delivery_id']			= true;
}
if($i>0){
	$is_data = true;
}

	$result['is_data']				= $is_data;


return json_out($result);
?>