<?php
include ('apps/misc/model/service.php');
/**
 * undocumented class
 *
 * @package default
 * @author PM
 **/
class maintenance
{
	var $service;
	var $field_n = 'service_id';
	var $pag = 'service';
	private $db;
	private $db_users;

	function __construct() {
		$this -> db = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$this -> db_users = new sqldb($db_config);
	}

	# servicing and support
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addService(&$in)
	{
		$f_active="1";
		if($in['template_id']){
			$v=new validation($in);
			$v->field('template_name','Template Name','required');
			if(!$v->run()){
				json_out($in);
			}
			$f_active="2";
			$in['serial_number']=$in['template_name'];
		}else{
		    if(!$this->addService_validate($in)){
                json_out($in);
                return false;
            }
			if($in['is_recurring']){
		 		$in['serial_number'] = '';
		 	}else{
		 		$in['serial_number'] = generate_service_number();
		 	}
		}

		if(!$in['email_language']){
			$in['email_language'] = $this->db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1' ");
		}
        if(isset($in['planeddate_js']) && !empty($in['planeddate_js']) ){
            $in['planeddate_ts'] = strtotime($in['planeddate_js']);
            $in['planeddate_ts'] = mktime(0,0,0,date('n',$in['planeddate_ts']),date('j',$in['planeddate_ts']),date('Y',$in['planeddate_ts']));
        }
		if($in['quote_id']){
			$this->db->query("UPDATE tblquote SET transformation='1' WHERE id='".$in['quote_id']."'");
		}

		if($in['customer_id']){
			$buyer_details = $this->db->query("SELECT customers.*,customer_addresses.*
			                    	FROM customers
			                    	LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.is_primary=1
	                            	WHERE customers.customer_id='".$in['customer_id']."'");
	    $in['customer_email']=addslashes($buyer_details->f('c_email'));
	    $in['customer_phone']=addslashes($buyer_details->f('comp_phone'));
	    $in['customer_address']=addslashes($buyer_details->f('address'));
	    $in['customer_zip']=addslashes($buyer_details->f('zip_name'));
	    $in['customer_city']=addslashes($buyer_details->f('city_name'));
	    $in['customer_country_id']=$buyer_details->f('country_id');
	    $in['customer_state']=$buyer_details->f('state');
	    $same_address=0;
	    if($in['sameAddress']!=1){
	    	$same_address=$in['delivery_address_id'];
	    }

	    $in['address_info'] = $buyer_details->f('address')."\n".$buyer_details->f('zip').'  '.$buyer_details->f('city')."\n".get_country_name($buyer_details->f('country_id'));
	      if($buyer_details->f('address_id') != $in['main_address_id']){
	        $buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
	        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
	      }
	      if($in['sameAddress']==1){
	        $in['free_field']=addslashes($in['address_info']);
	      }else{
	        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
	        $new_address->next();
	        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
	        $in['free_field']=addslashes($new_address_txt);      
	      }

	    }elseif($in['contact_id']){
	      	$buyer_details = $this->db->query("SELECT customer_contacts.*,customer_contact_address.*
			                    	FROM customer_contacts
			                    	LEFT JOIN customer_contact_address ON customer_contact_address.contact_id=customer_contacts.contact_id AND customer_contact_address.is_primary=1
	                            	WHERE customer_contacts.contact_id='".$in['contact_id']."'");
		    $in['customer_email']=addslashes($buyer_details->f('email'));
		    $in['customer_phone']=addslashes($buyer_details->f('phone'));
		    $in['customer_address']=addslashes($buyer_details->f('address'));
		    $in['customer_zip']=addslashes($buyer_details->f('zip'));
		    $in['customer_city']=addslashes($buyer_details->f('city'));
		    $in['customer_country_id']=$buyer_details->f('country_id');
		    $in['customer_state']=$buyer_details->f('state');
	    }

	    if(!$in['template_id']){
	    	//Set email language as account / contact language
		    $emailMessageData = array('buyer_id' 		=> $in['customer_id'],
			    						  'contact_id' 		=> $in['contact_id'],
			    						  'param'		 	=> 'add');
			$in['email_language'] = get_email_language($emailMessageData);
		    //End Set email language as account / contact language

			$default_email_language = $this->db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
		 	if($in['email_language']==0){
		 		$in['email_language'] = $default_email_language;
		 	}
	    }	    
	 	 	
	 	if(DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'){
	 		$cost_center="3991000";
	 		if($in['sameAddress']!=1){
	 			$address_zip=$this->db->field("SELECT zip FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
		    		if($address_zip){
		    			$cost_code=$this->db->field("SELECT `code` FROM cost_centre WHERE `zip`='".$address_zip."' ");
		    			if($cost_code){
		    				$cost_center="3".$cost_code."1000";
		    			}
		    		}
		      }else{
		      		$address_zip=$this->db->field("SELECT zip FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
		      		if($address_zip){
		      			$cost_code=$this->db->field("SELECT `code` FROM cost_centre WHERE `zip`='".$address_zip."' ");
		    			if($cost_code){
		    				$cost_center="3".$cost_code."1000";
		    			}
		      		}
		      }
		      $cost_center.="/";
		      if($in['segment_id']){
		      	$segment_id=$this->db->field("SELECT `code` FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
		      	if($segment_id){
		      		$cost_center.=$segment_id;
		      	}else{
		      		$cost_center.="0";
		      	}
		      }else{
		      	$cost_center.="0";
		      }
		      if($in['source_id']){
		      	$source_id=$this->db->field("SELECT `code` FROM tblquote_source WHERE id='".$in['source_id']."' ");
		      	if($source_id){
		      		$cost_center.=$source_id;
		      	}else{
		      		$cost_center.="0";
		      	}
		      }else{
		      	$cost_center.="0";
		      }
		      if($in['type_id']){
		      	$type_id=$this->db->field("SELECT `code` FROM tblquote_type WHERE id='".$in['type_id']."' ");
		      	if($type_id){
		      		$cost_center.=$type_id;
		      	}else{
		      		$cost_center.="0";
		      	}
		      }else{
		      	$cost_center.="0";
		      }
	 	}else{
	 		$cost_center="";
	 	}
	 	if(!$in['quote_id'] && !$in['deal_id'] && !$in['contract_id']){
			$in['identity_id']= $this->db_users->field("SELECT default_identity_id FROM user_info WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	    }else{
	      $in['identity_id']=0;
	    }

		$in['service_id'] = $this->db->insert("INSERT INTO servicing_support SET
												service_type					='".$in['service_type']."',
												customer_id						='".$in['customer_id']."',
												contact_id 						='".$in['contact_id']."',
												project_id 						='".$in['project_id']."',
												quote_id 						='".$in['quote_id']."',
												task_id 						='".$in['task_id']."',
												contract_id 						='".$in['contract_id']."',
												subject 								='".$in['subject']."',
												planeddate 						='".$in['planeddate_ts']."',
												date 									='".$in['date_ts']."',
												startdate 							='".$in['startdate']."',
												enddate 								='".$in['enddate_ts']."',
												duration 							='".$in['duration']."',
												report 								='".$in['report']."',
												type 									='".$in['type']."',
												customer_name 				 	='".$in['buyer_name']."',
												customer_email       	='".$in['customer_email']."',
												customer_phone       	='".$in['customer_phone']."',
                   									customer_address       ='".$in['customer_address']."',
                    									customer_zip     	  	='".$in['customer_zip']."',
                   									customer_city    	   	='".$in['customer_city']."',
                   									customer_country_id    ='".$in['customer_country_id']."',
                    									customer_state       	='".$in['customer_state']."',
												contact_name						='".$in['contact_name']."',
												project_name						='".$in['project_name']."',
												task_name							='".$in['task_name']."',
												contract_name					='".$in['contract_name']."',
												user_name							='".$in['user_name']."',
												serial_number  				='".$in['serial_number']."',
												user_id 								='".$_SESSION['u_id']."',
												email_language					='".$in['email_language']."',
												billable               ='".$in['billable']."',
												rate 									='".return_value($in['rate'])."',
												price    							='".return_value($in['price'])."',
												active 								='".$f_active."' ,
												free_field 						='".$in['free_field']."',
												update_pdf							=1,
												identity_id 						='".$in['identity_id']."',
												same_address					='".$same_address."',
												main_address_id				='".$in['main_address_id']."',
												author_id 					='".$in['author_id']."', 
												acc_manager_id           	=	'".$in['acc_manager_id']."',
                                      						acc_manager_name          =	'".addslashes($in['acc_manager_name'])."',
                                      						service_ctype 			= '".$in['service_ctype']."',
                                      						is_recurring 			='".$in['is_recurring']."',
                                      						installation_id 			='".$in['installation_id']."',
                                      						vat_regime_id 			='".$in['vat_regime_id']."',
                                      						source_id 				='".$in['source_id']."',
                                      						type_id 				='".$in['type_id']."',
                                      						cost_centre 			='".$cost_center."',
                                      						segment_id 				='".$in['segment_id']."',
                                      						color_ball='".(str_replace('#', '', $in['color_ball']))."',
                                      						your_ref='".addslashes($in['your_ref'])."' ");

		/*if(!$in['name']){
			$in['user_id'] = $_SESSION['u_id'];
			$in['name'] = get_user_name($in['user_id']);
		}
		$in['value'] = 1;*/
		//$this->addServiceUser($in);
		// $in['user_id'] = $_SESSION['u_id'];
		//$this->serviceSetPrM($in);
		/*if($in['type']!=2){
			$this->serviceAddTask($in);
		}*/

        if (!$in['duplicate_service_id']) {
            $users_nr=$this->db->field("SELECT COUNT(u_id) FROM servicing_support_users WHERE service_id='".$in['service_id']."' ");
            if(!$users_nr && !$in['template_id']){
                $team_nr=$this->db->field("SELECT COUNT(team_id) FROM servicing_team ");
                if($team_nr==1){
                    $team_id=$this->db->field("SELECT team_id FROM servicing_team ");
                    $u_nr=$this->db->field("SELECT COUNT(servicing_team_users.user_id) FROM servicing_team_users WHERE servicing_team_users.team_id= '".$team_id."'");
                    if($u_nr==1){
                            $in['user_id'] = $this->db->field("SELECT user_id FROM servicing_team_users ");
                            $in['name'] = get_user_name($in['user_id']);
                            $this->addServiceUser($in);
                    }
                }
            }
        }

		if($in['is_recurring']){
			/*$now=mktime(0,0,0,date('n'),date('j'),date('Y'));
			if(strtotime($in['rec_start_date']) >= $now && strtotime($in['rec_start_date']) < ($now+86400)){
				switch($in['rec_frequency']){
					case 1: 
						$rec_next_date=strtotime($in['rec_start_date'])+604800;
						break; //week
					case 2:
						$rec_next_date=strtotime("+1 month",strtotime($in['rec_start_date']));
						break; //month
					case 3:
						$rec_next_date=strtotime("+3 months",strtotime($in['rec_start_date']));
						break; //3 month
					case 4:  
						$rec_next_date=strtotime("+1 year",strtotime($in['rec_start_date']));
						break; //year
					case 5:  
						$rec_next_date=strtotime($in['rec_start_date'])+($in['days'] * 24 * 60 * 60);
						break; //days;
				}
			}else if(strtotime($in['rec_start_date']) > ($now+86399)){
				$rec_next_date=strtotime($in['rec_start_date']);
			}*/	
			if($in['rec_number'] && $in['end_contract'] && $in['contract_has_end']){
				$rec_end_date=$in['contract_end_date']/1000;
			}else{
				$rec_end_date=strtotime($in['rec_end_date']);
			}
			$this->db->query("UPDATE servicing_support SET
				rec_start_date='".strtotime($in['rec_start_date'])."',
				rec_frequency='".$in['rec_frequency']."',
				rec_days='".$in['rec_days']."',
				rec_next_date='".$rec_next_date."',
				rec_end_date='".$rec_end_date."',
				rec_number='".$in['rec_number']."',
				end_contract='".$in['end_contract']."'
				WHERE service_id='".$in['service_id']."' ");
			/*if($in['planning_users'] && !empty($in['planning_users'])){
				foreach($in['planning_users'] as $key=>$value){
					$in['user_id']=$value;
					$in['name'] = get_user_name($in['user_id']);
					$this->addServiceUser($in);		
				}
			}
			if($in['planning_dates'] && !empty($in['planning_dates'])){
				sort($in['planning_dates']);
				foreach($in['planning_dates'] as $key=>$value){
					$new_val=$value;
					if(date('I',$new_val)=='1'){
			            $new_val+=3600;
			        }
					$this->db->query("INSERT INTO servicing_recurring SET
						service_id='".$in['service_id']."',
						date='".$new_val."' ");
				}
			}*/
		}
		if($in['for_user']){
			$in['user_id']=$in['for_user'];
			$in['name'] = get_user_name($in['user_id']);
			$this->addServiceUser($in);
		}

		if($in['users'] && is_array($in['users'])){
			foreach($in['users']['users'] as $key=>$value){
				if(!is_numeric($value['id']) || $in['duplicate_service_id']){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_users WHERE service_id='".$in['service_id']."'  ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$this->db->query("INSERT INTO servicing_support_users SET user_id='".$value['user_id']."', service_id='".$in['service_id']."', name='".addslashes($value['name'])."', pr_m='".$value['pr_m']."',sort_order='".($last_sort_order+1)."' ");
				}		
			}
		}
		if($in['tasks'] && is_array($in['tasks'])){
			foreach($in['tasks']['tasks'] as $key=>$value){
                if(!is_numeric($value['id']) || $in['duplicate_service_id']){
					$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($value['name'])."', task_budget='".return_value($value['task_budget'])."', quantity='".return_value($value['task_quantity'])."',sort_order='".$key."' ");
				}else{
					$this->db->query("UPDATE servicing_support_tasks SET sort_order='".$key."' WHERE task_id='".$value['id']."' AND service_id='".$in['service_id']."' ");
				}			
			}
		}
		if($in['services'] && is_array($in['services'])){
			foreach($in['services']['services'] as $key=>$value){
                if(!is_numeric($value['id']) || $in['duplicate_service_id']){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($value['name'])."', task_budget='".return_value($value['task_budget'])."',article_id='".$value['article_id']."',quantity='".return_value($value['quantity'])."',sort_order='".($last_sort_order+1)."' ");
				}		
			}
		}
		if($in['supplies'] && is_array($in['supplies'])){
			foreach($in['supplies']['supplies'] as $key=>$value){
                if(!is_numeric($value['id'])  || $in['duplicate_service_id']){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$this->db->query("INSERT INTO servicing_support_articles SET service_id='".$in['service_id']."', name='".addslashes($value['name'])."',billable='".$value['billable']."',quantity='".return_value($value['quantity'])."',sort_order='".($last_sort_order+1)."' ");
				}			
			}
		}
		if($in['articles'] && is_array($in['articles'])){
			foreach($in['articles']['articles'] as $key=>$value){
				$use_combined = $this->db->field("SELECT use_combined FROM pim_articles WHERE article_id='".$value['article_id']."'");
                if($value['is_combined'] || (!$use_combined && !$value['component_for'])){
                    $last_combined = '';
                }

				if( $value['has_variants'] ){
                  $last_variant_parent = '';
                }
				if(!is_numeric($value['id'])  || $in['duplicate_service_id']){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$margin_nr=return_value($value['sell_price'])-$value['purchase_price'];
					$margin_percent=(return_value($value['sell_price'])-$value['purchase_price'])/return_value($value['sell_price'])*100;
					$line_id = $this->db->insert("INSERT INTO servicing_support_articles SET service_id='".$in['service_id']."',
									name='".addslashes($value['name'])."',
									article_id='".$value['article_id']."',
									quantity='".return_value($value['quantity'])."',
									price='".return_value($value['sell_price'])."',
									purchase_price='".$value['purchase_price']."',
									margin='".$margin_nr."',
									margin_per='".$margin_percent."',
									billable='".$value['billable']."',
									sort_order='".($last_sort_order+1)."',
									has_variants          = '".$value['has_variants']."',
                                    has_variants_done     ='".$value['has_variants_done']."',
                                    is_variant_for        ='".$value['is_variant_for']."',
                                    is_variant_for_line   ='".$last_variant_parent."',
                                    variant_type            ='".$value['variant_type']."',
                                    is_combined          = '".$value['is_combined']."',
                                    component_for         ='".$last_combined."' ");
				}else{
					$line_id=$value['id'];
				}

				if($value['is_combined']){
                    $last_combined = $line_id;
                }
				if($value['has_variants'] && $value['has_variants_done']){
                    $last_variant_parent = $line_id;
                }
                if($value['is_variant_for']){
                    $last_variant_parent = '';
                }		
			}
		}

		if($in['quote_id']){
			//$this->add_quote_data($in);
			$tracking_data=array(
		            'target_id'         => $in['service_id'],
		            'target_type'       => '5',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['quote_id'],'origin_type'=>'2')
		                                    )
		          );
		      addTracking($tracking_data);
		}else if($in['contract_id']){
			//$this->add_contract_data($in);
			$tracking_data=array(
		            'target_id'         => $in['service_id'],
		            'target_type'       => '5',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['contract_id'],'origin_type'=>'9')
		                                    )
		          );
		      addTracking($tracking_data);
		}else{
			$tracking_data=array(
		            'target_id'         => $in['service_id'],
		            'target_type'       => '5',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array()
		          );
		      addTracking($tracking_data);
		}
        if($in['deal_id']){
			$trace_id=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' ");	
				}else{
					$this->db->query("INSERT INTO tracking_line SET
							origin_id='".$in['deal_id']."',
							origin_type='11',
							trace_id='".$trace_id."' ");
				}
			}else{
				$tracking_data=array(
			            'target_id'         => $in['service_id'],
			            'target_type'       => '5',
			            'target_buyer_id'   => $in['customer_id'],
			            'lines'             => array(
			                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
			                                    )
			          );
			      addTracking($tracking_data);
			}
		}

		$count = $this->db->field("SELECT COUNT(service_id) FROM servicing_support ");
	    if($count == 1){
	      doManageLog('Created the first intervention.','',array( array("property"=>'first_module_usage',"value"=>'Intervention') ));
	    }

		  # for pdf
	    /*$params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $in['email_language'];
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf

		msg::success ( gm('Changes saved'),'success');
		insert_message_log($this->pag,'{l}Intervention added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id'],false);
		if($in['customer_id'] && !$in['is_recurring']){
			$buyer_credit_limit_info=$this->db->query("SELECT credit_limit,currency_id FROM customers WHERE customer_id='".$in['customer_id']."' ");
			if($buyer_credit_limit_info->f('credit_limit')){
				$invoices_customer_due=customer_credit_limit($in['customer_id']);
				if($invoices_customer_due && $invoices_customer_due>$buyer_credit_limit_info->f('credit_limit')){
					$buyer_currency_id=$buyer_credit_limit_info->f('currency_id') ? $buyer_credit_limit_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
					$in['customer_credit_limit']=place_currency(display_number($buyer_credit_limit_info->f('credit_limit')),get_commission_type_list($buyer_currency_id));
				}
			}	
		}
		json_out($in);
		return true;
	}

	/**
	 * Updates a maintenance
	 *
	 * @return void
	 * @author PM
	 **/
	function editService(&$in)
	{
		if($in['template_id']){
			$v=new validation($in);
			$v->field('template_name','Template name','required');
			if(!$v->run()){
				json_out($in);
			}
			$in['serial_number']=$in['template_name'];
		}else{
            if(!$this->editService_validate($in)){
                json_out($in);
            }
		}	

		if(isset($in['planeddate_js']) && !empty($in['planeddate_js']) ){
			$in['planeddate_ts'] = strtotime($in['planeddate_js']);
		}

		if(isset($in['planeddate_ts']) && !empty($in['planeddate_ts'])){
			$in['planeddate_ts'] = mktime(0,0,0,date('n',$in['planeddate_ts']),date('j',$in['planeddate_ts']),date('Y',$in['planeddate_ts']));
			$in['planeddate'] = date(ACCOUNT_DATE_FORMAT,$in['planeddate_ts']);
		}

		if(!$in['template_id']){
			//Set email language as account / contact language
			$emailMessageData = array('buyer_id' 		=> $in['customer_id'],
		    						  'contact_id' 		=> $in['contact_id'],
		    						  'item_id'			=> $in['service_id'],
		    						  'email_language' 	=> $in['email_language'],
		    						  'table'			=> 'servicing_support',
		    						  'table_label'		=> 'service_id',
		    						  'table_buyer_label' => 'customer_id',
		    						  'table_contact_label' => 'contact_id',
		    						  'param'		 	=> 'edit');
			$in['email_language'] = get_email_language($emailMessageData);
		}	
		if(DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'){
	 		$cost_center="3991000";
	 		if($in['sameAddress']!=1){
	 			$address_zip=$this->db->field("SELECT zip FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
		    		if($address_zip){
		    			$cost_code=$this->db->field("SELECT `code` FROM cost_centre WHERE `zip`='".$address_zip."' ");
		    			if($cost_code){
		    				$cost_center="3".$cost_code."1000";
		    			}
		    		}
		      }else{
		      		$address_zip=$this->db->field("SELECT zip FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
		      		if($address_zip){
		      			$cost_code=$this->db->field("SELECT `code` FROM cost_centre WHERE `zip`='".$address_zip."' ");
		    			if($cost_code){
		    				$cost_center="3".$cost_code."1000";
		    			}
		      		}
		      }
		      $cost_center.="/";
		      if($in['segment_id']){
		      	$segment_id=$this->db->field("SELECT `code` FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
		      	if($segment_id){
		      		$cost_center.=$segment_id;
		      	}else{
		      		$cost_center.="0";
		      	}
		      }else{
		      	$cost_center.="0";
		      }
		      if($in['source_id']){
		      	$source_id=$this->db->field("SELECT `code` FROM tblquote_source WHERE id='".$in['source_id']."' ");
		      	if($source_id){
		      		$cost_center.=$source_id;
		      	}else{
		      		$cost_center.="0";
		      	}
		      }else{
		      	$cost_center.="0";
		      }
		      if($in['type_id']){
		      	$type_id=$this->db->field("SELECT `code` FROM tblquote_type WHERE id='".$in['type_id']."' ");
		      	if($type_id){
		      		$cost_center.=$type_id;
		      	}else{
		      		$cost_center.="0";
		      	}
		      }else{
		      	$cost_center.="0";
		      }
	 	}else{
	 		$cost_center="";
	 	}
	    //End Set email language as account / contact language

	    if($in['customer_id']){
	    	$customer_data=$this->db->query("SELECT customers.*,customer_addresses.*,customer_legal_type.name as l_name FROM customers 
	    		LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.is_primary=1
		        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
		        WHERE customers.customer_id='".$in['customer_id']."' ");
		    $in['customer_name']=addslashes(stripslashes($customer_data->f('name').' '.$customer_data->f('l_name')));
		    $in['customer_email']=$customer_data->f('c_email');
		    $in['customer_phone']=$customer_data->f('comp_phone');
		    $in['customer_fax']=$customer_data->f('comp_fax');
		    $in['customer_zip']=addslashes($customer_data->f('zip_name'));
	    	$in['customer_city']=addslashes($customer_data->f('city_name'));
	    	$in['customer_country_id']=$customer_data->f('country_id');
	    	$in['customer_state']=$customer_data->f('state');

		    $same_address=0;
		    if($in['sameAddress']!=1){
		    	$same_address=$in['delivery_address_id'];
		    }

		    $in['address_info'] = $customer_data->f('address')."\n".$customer_data->f('zip').'  '.$customer_data->f('city')."\n".get_country_name($customer_data->f('country_id'));
		    if($customer_data->f('address_id') != $in['main_address_id']){
		        $buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
		        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
		    }
		    if($in['sameAddress']==1){
		        $in['free_field']=addslashes($in['address_info']);
		    }else{
		        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
		        $new_address->next();
		        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
		        $in['free_field']=addslashes($new_address_txt);      
		    }
	    }

		$in['sdate'] = $in['startdate'];
		$this->db->query("UPDATE servicing_support SET
								customer_id		='".$in['customer_id']."',
								contact_id 		='".$in['contact_id']."',
								project_id 		='".$in['project_id']."',
								task_id 			='".$in['task_id']."',
								subject 			='".$in['subject']."',
								planeddate 		='".$in['planeddate_ts']."',
								date 				='".$in['date_ts']."',
								startdate 			='".$in['startdate']."',
								enddate 			='".$in['enddate_ts']."',
								duration 			='".$in['duration']."',
								report 			='".$in['report']."',
								type 				='".$in['type']."',
								customer_name  	='".$in['customer_name']."',
								customer_email     ='".$in['customer_email']."',
								customer_phone     ='".$in['customer_phone']."',
								customer_fax     	='".$in['customer_fax']."',
								customer_zip     	  	='".$in['customer_zip']."',
								customer_city    	   	='".$in['customer_city']."',
								customer_country_id    ='".$in['customer_country_id']."',
								customer_state       	='".$in['customer_state']."',
								contact_name		='".$in['contact_name']."',
								project_name		='".$in['project_name']."',
								task_name			='".$in['task_name']."',
								contract_name		='".$in['contract_name']."',
								user_name			='".$in['user_name']."',
								billable           ='".$in['billable']."',
								email_language		='".$in['email_language']."',
								rate 				='".return_value($in['rate'])."',
								price 				='".return_value($in['price'])."',
								free_field 		='".$in['free_field']."',
								finishDate         ='".$in['finishDate']."',
								serial_number  	='".$in['serial_number']."',
								identity_id  	='".$in['identity_id']."',
								author_id 		='".$in['author_id']."', 
								same_address					='".$same_address."',
								main_address_id				='".$in['main_address_id']."',
								acc_manager_id   ='".$in['acc_manager_id']."',
                                      		acc_manager_name  ='".addslashes($in['acc_manager_name'])."',
                                      		vat_regime_id 	='".$in['vat_regime_id']."',
                                      		rec_frequency    ='".$in['rec_frequency']."',
                                      		rec_days         ='".$in['rec_days']."',
                                      		source_id 				='".$in['source_id']."',
                                      		type_id 				='".$in['type_id']."',
                                      		segment_id 				='".$in['segment_id']."',
                                      		cost_centre 			='".$cost_center."',
                                      		color_ball='".(str_replace('#', '', $in['color_ball']))."',
                                      		your_ref='".addslashes($in['your_ref'])."'
								WHERE service_id='".$in['service_id']."' ");
		if($in['startdate'] && $in['planeddate_ts'] && !$in['is_recurring']){
			$this->updateCalendar($in);
		}

		if($in['users'] && is_array($in['users'])){
			foreach($in['users']['users'] as $key=>$value){
				if(!is_numeric($value['id'])){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_users WHERE service_id='".$in['service_id']."'  ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$this->db->query("INSERT INTO servicing_support_users SET user_id='".$value['user_id']."', service_id='".$in['service_id']."', name='".addslashes($value['name'])."', pr_m='".$value['pr_m']."',sort_order='".($last_sort_order+1)."' ");
				}		
			}
		}
		if($in['tasks'] && is_array($in['tasks'])){
			foreach($in['tasks']['tasks'] as $key=>$value){
				if(!is_numeric($value['id'])){
					$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($value['name'])."', task_budget='".return_value($value['task_budget'])."',sort_order='".$key."' ");
				}else{
					$this->db->query("UPDATE servicing_support_tasks SET sort_order='".$key."' WHERE task_id='".$value['id']."' AND service_id='".$in['service_id']."' ");
				}			
			}
		}
		if($in['services'] && is_array($in['services'])){
			foreach($in['services']['services'] as $key=>$value){
				if(!is_numeric($value['id'])){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($value['name'])."', task_budget='".return_value($value['task_budget'])."',article_id='".$value['article_id']."',quantity='".return_value($value['quantity'])."',sort_order='".($last_sort_order+1)."' ");
				}		
			}
		}
		if($in['supplies'] && is_array($in['supplies'])){
			foreach($in['supplies']['supplies'] as $key=>$value){
				if(!is_numeric($value['id'])){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$this->db->query("INSERT INTO servicing_support_articles SET service_id='".$in['service_id']."', name='".addslashes($value['name'])."',billable='".$value['billable']."',quantity='".return_value($value['quantity'])."',sort_order='".($last_sort_order+1)."' ");
				}			
			}
		}
		if($in['articles'] && is_array($in['articles'])){
			foreach($in['articles']['articles'] as $key=>$value){
				$use_combined = $this->db->field("SELECT use_combined FROM pim_articles WHERE article_id='".$value['article_id']."'");
				if($value['is_combined'] || (!$use_combined && !$value['component_for'])){
                    $last_combined = '';
                }
				if( $value['has_variants'] ){
                  $last_variant_parent = '';
                }
				if(!is_numeric($value['id'])){
					$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY sort_order DESC LIMIT 1");
					if(!$last_sort_order){
						$last_sort_order=0;
					}
					$margin_nr=return_value($value['sell_price'])-$value['purchase_price'];
					$margin_percent=(return_value($value['sell_price'])-$value['purchase_price'])/return_value($value['sell_price'])*100;
					$line_id = $this->db->insert("INSERT INTO servicing_support_articles SET service_id='".$in['service_id']."',
									name='".addslashes($value['name'])."',
									article_id='".$value['article_id']."',
									quantity='".return_value($value['quantity'])."',
									price='".return_value($value['sell_price'])."',
									purchase_price='".$value['purchase_price']."',
									margin='".$margin_nr."',
									margin_per='".$margin_percent."',
									billable='".$value['billable']."',
									sort_order='".($last_sort_order+1)."',
									has_variants          = '".$value['has_variants']."',
                                    has_variants_done     ='".$value['has_variants_done']."',
                                    is_variant_for        ='".$value['is_variant_for']."',
                                    is_variant_for_line   ='".$last_variant_parent."',
                                    variant_type            ='".$value['variant_type']."',
                                    is_combined          = '".$value['is_combined']."',
                                    component_for         ='".$last_combined."' ");
				}else{
					$line_id=$value['id'];
				}

				if($value['is_combined']){
                    $last_combined = $line_id;
                }
				if($value['has_variants'] && $value['has_variants_done']){
                    $last_variant_parent = $line_id;
                }
                if($value['is_variant_for']){
                    $last_variant_parent = '';
                }		
			}
		}

		$last_step=$this->db->field("SELECT last_step FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		if($in['is_recurring'] && (!$last_step || $in['recurring_replan'])){
			$this->db->query("UPDATE servicing_support SET
				advance_day='".$in['advance_day']."',
				last_step='1'
				WHERE service_id='".$in['service_id']."' ");
			if($in['planning_users'] && !empty($in['planning_users'])){
				foreach($in['planning_users'] as $key=>$value){
					$in['user_id']=$value;
					$in['name'] = get_user_name($in['user_id']);
					$this->addServiceUser($in);		
				}
			}
			if($in['planning_dates'] && !empty($in['planning_dates'])){
				sort($in['planning_dates']);
				$this->db->query("DELETE FROM servicing_recurring WHERE service_id='".$in['service_id']."' ");
				foreach($in['planning_dates'] as $key=>$value){
					$new_val=$value;
					if(date('I',$new_val)=='1'){
			            $new_val+=3600;
			        }
					$this->db->query("INSERT INTO servicing_recurring SET
						service_id='".$in['service_id']."',
						date='".$new_val."' ");
				}
    			$today_end = mktime( 23, 59, 59, date('m'), date('d'), date('Y'));
    			$first_int=strtotime('-'.(int)$in['advance_day'].' days',$in['planning_dates'][0]);
    			if($first_int<=$today_end){
    				$in['choice_2']=true;
    				$this->createBulkInterventions($in);
    				$this->db->query("UPDATE servicing_support SET time_run='1' WHERE service_id='".$in['service_id']."' ");
    			}else{
    				$rec_next_date=$this->db->field("SELECT `date` FROM servicing_recurring WHERE service_id='".$in['service_id']."' AND `done`='0' ORDER BY `date` ASC LIMIT 1 ");
			        $this->db->query("UPDATE servicing_support SET rec_next_date='".strtotime('-'.(int)$in['advance_day'].' days',$rec_next_date)."' WHERE service_id='".$in['service_id']."' ");
    			}
			}else{
				$rec_start_date=$this->db->field("SELECT rec_start_date FROM servicing_support WHERE service_id='".$in['service_id']."'");
    			$today_end = mktime( 23, 59, 59, date('m'), date('d'), date('Y'));
				$first_int=strtotime('-'.(int)$in['advance_day'].' days',$rec_start_date);
				$this->db->query("DELETE FROM servicing_recurring WHERE service_id='".$in['service_id']."' ");
				if($first_int<=$today_end){
					$this->db->query("INSERT INTO servicing_recurring SET
						service_id='".$in['service_id']."',
						date='".$rec_start_date."' ");
					$in['choice_2']=true;
    				$this->createBulkInterventions($in);
    				$this->db->query("DELETE FROM servicing_recurring WHERE service_id='".$in['service_id']."' ");
    				$this->db->query("UPDATE servicing_support SET time_run='1' WHERE service_id='".$in['service_id']."' ");
    				$rec_services=$this->db->query("SELECT rec_frequency,rec_days,rec_start_date,rec_number,rec_end_date FROM servicing_support WHERE service_id='".$in['service_id']."' ");				 
    				switch($rec_services->f('rec_frequency')){
	                    case 1: 
	                        $rec_next_date=$rec_services->f('rec_start_date')+604800;
	                        break; //week
	                    case 2:
	                        $rec_next_date=strtotime("+1 month",$rec_services->f('rec_start_date'));
	                        break; //month
	                    case 3:
	                        $rec_next_date=strtotime("+3 months",$rec_services->f('rec_start_date'));
	                        break; //3 month
	                    case 4:  
	                        $rec_next_date=strtotime("+1 year",$rec_services->f('rec_start_date'));
	                        break; //year
	                    case 5:  
	                        $rec_next_date=$rec_services->f('rec_start_date')+($rec_services->f('rec_days') * 24 * 60 * 60);
	                        break; //days;
	                    case 6:
	                        $rec_next_date=strtotime("+6 months",$rec_services->f('rec_start_date'));
	                        break; //6 month
	                    case 7:
	                        $rec_next_date=strtotime("+24 months",$rec_services->f('rec_start_date'));
	                        break;//once every two years
	                }
	                if($rec_services->f('rec_number') && $rec_services->f('rec_number')>1){
	                    $rec_next_date=strtotime('-'.(int)$in['advance_day'].' days',$rec_next_date);
	                    if(date('I',$rec_next_date)=='1'){
	                        $rec_next_date+=3600;
	                    }
	                }else if(!$rec_services->f('rec_number') && $rec_services->f('rec_end_date') && $rec_next_date<=$rec_services->f('rec_end_date')){
	                    $rec_next_date=strtotime('-'.(int)$in['advance_day'].' days',$rec_next_date);
	                    if(date('I',$rec_next_date)=='1'){
	                        $rec_next_date+=3600;
	                    }
	                }else if(!$rec_services->f('rec_number') && !$rec_services->f('rec_end_date')){
	                    $rec_next_date=strtotime('-'.(int)$in['advance_day'].' days',$rec_next_date);
	                    if(date('I',$rec_next_date)=='1'){
	                        $rec_next_date+=3600;
	                    }
	                }
	                if($rec_next_date){
	                	$this->db->query("UPDATE servicing_support SET rec_next_date='".$rec_next_date."' WHERE service_id='".$in['service_id']."' ");
	                }
	                /*if(($rec_services->f('rec_number') && $rec_services->f('rec_number')>1) || (!$rec_services->f('rec_number') && $rec_next_date<=$rec_services->f('rec_end_date'))){
	                	$rec_next_date=strtotime('-'.(int)$in['advance_day'].' days',$rec_next_date);
		                if(date('I',$rec_next_date)=='1'){
				            $rec_next_date+=3600;
				        }
				        $this->db->query("UPDATE servicing_support SET rec_next_date='".$rec_next_date."' WHERE service_id='".$in['service_id']."' ");
    				}*/        
				}else{
					$rec_next_date=strtotime('-'.(int)$in['advance_day'].' days',$rec_start_date);
					if(date('I',$rec_next_date)=='1'){
			            $rec_next_date+=3600;
			        }
			        $this->db->query("UPDATE servicing_support SET rec_next_date='".$rec_next_date."' WHERE service_id='".$in['service_id']."' ");
				}
			}
			/*if($in['choice_1'] || $in['choice_2']){
				$this->createBulkInterventions($in);
			}*/
		}

		$trace_id=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		if($in['deal_id']){
			//$trace_id=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' ");	
				}else{
					$this->db->query("INSERT INTO tracking_line SET
							origin_id='".$in['deal_id']."',
							origin_type='11',
							trace_id='".$trace_id."' ");
				}
			}else{
				$tracking_data=array(
			            'target_id'         => $in['service_id'],
			            'target_type'       => '5',
			            'target_buyer_id'   => $in['customer_id'],
			            'lines'             => array(
			                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
			                                    )
			          );
			      addTracking($tracking_data);
			}
		}else{
			//$trace_id=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("DELETE FROM tracking_line WHERE line_id='".$linked_doc."' ");	
				}
			}
		}
		if($trace_id){
			$this->db->query("UPDATE tracking SET origin_buyer_id='".$in['customer_id']."',target_buyer_id='".$in['customer_id']."' WHERE trace_id='".$trace_id."' ");
		}

		# for pdf
		$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
    /*$params = array();
    $params['service_id'] = $in['service_id'];
    $params['lid'] = $in['email_language'];
    $params['save_as'] = 'F';
    $this->generate_pdf($params);*/
    # for pdf
		// msg::$success = gm('Changes saved');
    	insert_message_log($this->pag, "{l}Intervention details was updated by{endl} ".get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id']);
		msg::success( gm('Changes saved'), 'success');
		return true;
	}

	function saveNote(&$in){
		$this->db->query("UPDATE servicing_support SET
								report 			='".$in['report']."'
								WHERE service_id='".$in['service_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function editService_validate(&$in)
	{
		if(!$in['service_id']){
			msg::error( gm("Invalid ID"),'error');
			return false;
		}
		return $this->addService_validate($in);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addService_validate(&$in)
	{
		/*if(!$this->tryAddC($in)){
			return false;
		}*/
		if(!$in['customer_id'] && !$in['contact_id']){
			// msg::$error_big = gm('Please select a customer or a contact');
			msg::error( gm("Please select a customer or a contact"),'error');
			return false;
		}
		$v = new validation($in);
		$is_ok=true;

		//$v->f('subject', 'subject', 'required');

		$v->field('customer_id', 'ID', 'exist[customers.customer_id]', gm('Invalid ID'));
		$v->field('contact_id', 'ID', 'exist[customer_contacts.contact_id]', gm('Invalid ID'));
		/*if(!$in['service_id']){
			//$in['serial_number'] = generate_service_number();
			$v->field('serial_number', 'ID', 'unique[servicing_support.service_id]', gm('Unique serial number required'));
		}else{
			$this->db->query("SELECT serial_number FROM servicing_support WHERE service_id<>'".$in['service_id']."' AND serial_number='".$in['serial_number']."' ");
			if($this->db->next()){
				$in['serial_number'] = $this->db->field("SELECT serial_number FROM servicing_support WHERE service_id='".$in['service_id']."' ");
			}
		}*/
		if(ark::$method=='editService' && !$in['is_recurring']){
			$v->field('serial_number', 'ID', "required:unique[servicing_support.serial_number.( service_id!='".$in['service_id']."')]", gm('Unique serial number required'));
		}
		if($in['is_recurring']){
			$v->field('rec_start_date', 'Start Date', 'required');
			$v->field('rec_frequency', 'Frequency', 'required');
		}
		$is_ok = $v->run();

		return $is_ok;
	}

	/**
	 * Add user to an intervention
	 *
	 * @return void
	 * @author PM
	 **/
	function addServiceUser(&$in)
	{
		if(!$this->addServiceUser_validate($in)){
			return false;
		}
		$pr_m=0;
        $nr_users=$this->db->field("SELECT COUNT(u_id) FROM servicing_support_users WHERE service_id='".$in['service_id']."' ");
        if(!$nr_users){
            $pr_m=1;
        }
        $last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_users WHERE service_id='".$in['service_id']."'  ORDER BY sort_order DESC LIMIT 1");
		if(!$last_sort_order){
			$last_sort_order=0;
		}
		$in['user_id'] = $this->db->insert("INSERT INTO servicing_support_users SET user_id='".$in['user_id']."', service_id='".$in['service_id']."', name='".addslashes($in['name'])."', `pr_m`='".$pr_m."',sort_order='".($last_sort_order+1)."' ");
		$service = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		if($service->next()){
			$in['startdate'] = $in['startdate'] ? $in['startdate']: $service->f('startdate');
			$in['planeddate_ts'] = $service->f('planeddate');
			$in['serial_number'] = $service->f('serial_number');
			if($in['startdate'] && $in['planeddate_ts'] && !$service->f('is_recurring')){
				$this->updateCalendar($in);
			}
		}
		// msg::$success = gm('User added');
		msg::success( gm('User added'), 'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addServiceUser_validate(&$in)
	{
		$v = new validation($in);
		if(ark::$method != 'addService'){
			$v->f('user_id', 'user_id', 'required:unique[servicing_support_users.user_id.( user_id=\''.$in["user_id"].'\' AND service_id=\''.$in['service_id'].'\' )]');
		}
		$v->f('service_id', 'service_id', 'required');

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCalendar(&$in)
	{
		if(isset($in['planeddate_ts']) && !empty($in['planeddate_ts'])){
			$in['planeddate_ts'] = mktime(0,0,0,date('n',$in['planeddate_ts']),date('j',$in['planeddate_ts']),date('Y',$in['planeddate_ts']));
			$in['planeddate'] = date(ACCOUNT_DATE_FORMAT,$in['planeddate_ts']);
		}
		$in['comment_due_date'] = $in['planeddate_ts']+round($in['startdate']*3600);
		$mtime = $in['planeddate_ts'];

		if($in['startdate']){
			$mtime += $in['startdate']*3600;
		}
		$end_t=$mtime;
		if($in['duration']){
			$end_t+=$in['duration']*3600;
		}
		if($in['plannedtime']){
			$mtime += $in['plannedtime']*3600;
		}
		// $in['comment'] = gm('Maintenance') . ': ' . $in['serial_number'] . ' ' . gm('planned at') . ' ' . date(ACCOUNT_DATE_FORMAT.' H:i',$in['comment_due_date']);
		$srv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		$cust = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$srv->f('customer_id')."' ");
		$in['comment'] = 'Intervention - ' . $in['serial_number'];
		$in['comment'] .="\n".$srv->f('customer_name')."\n".$srv->f('report')."\n".$cust->f('address')."\n".$cust->f('city').' '.$cust->f('zip')."\n".get_country_name($cust->f('country_id'));
		$users = $this->db->query("SELECT * FROM servicing_support_users WHERE service_id='".$in['service_id']."' ");
		//$mtime -= $_SESSION['user_timezone_offset'];
		//$end_t -= $_SESSION['user_timezone_offset'];
		while ($users->next()) {
			$in['to_user_id'] = $users->f('user_id');
			//$my_name = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
			$my_name = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
			$my_name->next();

			$is_meet = $this->db->query("SELECT * FROM customer_meetings WHERE user_id='".$in['to_user_id']."' AND service_id='".$in['service_id']."' ");

			if($is_meet->next()){
				$in['meeting_id'] = $is_meet->f('customer_meeting_id');
				$this->db->insert("UPDATE customer_meetings SET start_date='".$mtime."',end_date='".$end_t."',message='".addslashes($in['comment'])."' WHERE user_id='".$in['to_user_id']."' AND service_id='".$in['service_id']."' ");
			}else{

				$in['meeting_id'] = $this->db->insert("INSERT INTO customer_meetings SET
															   subject='".addslashes($srv->f('subject'))."',
															   location='',
															   start_date='".$mtime."',
															   end_date='".$end_t."',
															   duration='0',
															   all_day='',
															   message='".addslashes($in['comment'])."',
															   from_c='".addslashes($my_name->f('first_name'))." ".addslashes($my_name->f('last_name'))."',
															   type='3',
															   customer_id='".$srv->f('customer_id')."',
															   customer_name='".addslashes($srv->f('customer_name'))."',
															   user_id='".$in['to_user_id']."',
															   service_id='".$in['service_id']."',
															   notify = '1'
															");
			}
			if($in['contact_id']){
				$c = $this->db->query("SELECT * FROm customer_contact_activity_contacts WHERE activity_id='".$in['meeting_id']."' AND action_type IN (1,2) ");
				if(!$c->next()){
					$this->db->insert("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['meeting_id']."', contact_id='".$in['contact_id']."', action_type=1 ");
				}
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceSetPrM(&$in)
	{
		if(!$this->serviceSetPrM_validate($in)){
			json_out($in);
			return false;
		}
		/*$manager_nr=$this->db->field("SELECT COUNT(u_id) FROM servicing_support_users WHERE service_id='".$in['service_id']."' AND pr_m='1' ");
		if($manager_nr==1 && (!$in['value'] || $in['value']=='')){
			msg::error(gm("At least one intervention manager needed"),"error");
			$in['one_manager']=true;
			json_out($in);
		}*/
		$this->db->query("UPDATE servicing_support_users SET pr_m='".$in['value']."' WHERE u_id='".$in['user_id']."' AND service_id='".$in['service_id']."' ");
		// msg::$success = gm('Changes saved');
		msg::success( gm('Changes saved'), 'success');
		if(ark::$method=='serviceSetPrM'){
			json_out($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceSetPrM_validate(&$in)
	{
		$v = new validation($in);
		$v->f('user_id', 'user_id', 'required');
		$v->f('service_id', 'service_id', 'required');
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteServiceUser(&$in)
	{
		if(!$this->serviceSetPrM_validate($in)){
			json_out($in);
		}
		$querys['There is data associated with this user. You cannot delete this user.'] = "SELECT id
FROM  `servicing_support_sheet`
INNER JOIN servicing_support_users ON ( servicing_support_sheet.user_id = servicing_support_users.user_id
AND servicing_support_sheet.service_id = servicing_support_users.service_id )
WHERE servicing_support_users.user_id='".$in['u_id']."'
AND servicing_support_sheet.service_id ='".$in['service_id']."' ";
		if(!$this->verify($querys)){
			return true;
		}
		/*$isPrM = $this->db->field("SELECT pr_m FROM servicing_support_users WHERE u_id='".$in['user_id']."' AND service_id='".$in['service_id']."' ");
		if($isPrM == 1){
			msg::error( gm('You cannot delete a intervention manager'), 'error');
			json_out($in);
		}*/
		$this->db->query("DELETE FROM servicing_support_users WHERE user_id='".$in['u_id']."' AND service_id='".$in['service_id']."' ");
		$this->db->query("DELETE FROM logging WHERE to_user_id='".$in['u_id']."' AND field_name='service_id' AND field_value='".$in['service_id']."' ");
		$this->db->query("DELETE FROM customer_meetings WHERE user_id='".$in['u_id']."' AND service_id='".$in['service_id']."' ");
		// msg::$success = gm('Changes saved');
		msg::success( gm('Changes saved'), 'success');
		return true;
	}

	/**
	 * Verify function
	 *
	 * @param $querys -> must be an array where the keys are the messages to be shown and the values are the querys to be executed
	 * @return void
	 * @author PM
	 **/
	function verify($querys=array())
	{
		foreach ($querys as $key => $value) {
			$this->db->query($value);
			if($this->db->move_next()){
				// msg::$notice = $key;
				msg::notice( gm($key), 'notice');
				return false;
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddTask(&$in)
	{
		if(!$this->serviceAddTask_validate($in)){
			return false;
		}
		$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY sort_order DESC LIMIT 1");
		if(!$last_sort_order){
			$last_sort_order=0;
		}
		$in['task_id'] = $this->db->insert("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($in['task_name'])."', task_budget='".$in['task_budget']."',sort_order='".($last_sort_order+1)."' ");
		$in['task_name'] = stripslashes($in['task_name']);
		// $this->changeServiceStatus($in);
		// msg::$success = gm('Changes saved');
		msg::success( gm('Changes saved'), 'success');
		if(ark::$method=='serviceAddTask'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddTask_validate(&$in)
	{
		$v = new validation($in);
		$v->f('task_name', 'task_name', 'required');
		$v->f('service_id', 'service_id', 'required');
		return $v->run();
	}

	function serviceAddService(&$in)
	{
		$unique_service=$this->db->field("SELECT task_id FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' AND article_id='".$in['task_service_id']."' ");
		if($unique_service){
			msg::error(gm('Service already added'),"error");
			json_out($in);
		}
		$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY sort_order DESC LIMIT 1");
		if(!$last_sort_order){
			$last_sort_order=0;
		}
		$this->db->insert("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".$in['task_name']."', task_budget='".$in['task_budget']."',article_id='".$in['task_service_id']."',quantity='".($in['quantity'] ? $in['quantity'] : '1')."',sort_order='".($last_sort_order+1)."' ");
		msg::success( gm('Changes saved'), 'success');
		if(ark::$method=='serviceAddService'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteService_validate(&$in)
	{
		if(!$in['service_id']){
			msg::error( gm("Invalid ID"),'error');
			return false;
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function closeTask(&$in)
	{
		if(!$this->deleteService_validate($in)){
			json_out($in);
			return false;
		}
		if(!$in['id']){
			msg::error( gm("Invalid ID"),'error');
			json_out($in);
			return false;
		}
		// $status = $this->db->field("SELECT status FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		$this->db->query("UPDATE servicing_support_tasks SET closed='".$in['value']."' WHERE task_id='".$in['id']."' AND service_id='".$in['service_id']."' ");
		// $this->changeServiceStatus($in);
		msg::success( gm('Changes saved'), 'success');
		json_out($in);
		return;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteServiceTask(&$in)
	{
		if(!$this->deleteServiceTask_validate($in)){
			return false;
		}
		if($this->db->field("SELECT comment FROM servicing_support_tasks WHERE task_id='".$in['task_id']."' AND service_id='".$in['service_id']."' ") ){
			msg::notice(gm('Please delete the comment and then the task'),'notice');
			return false;
		}
		$this->db->query("DELETE FROM servicing_support_tasks WHERE task_id='".$in['task_id']."' AND service_id='".$in['service_id']."' ");
		// $this->changeServiceStatus($in);
		msg::success( gm('Changes saved'), 'success');
		if(ark::$method=='deleteServiceTask'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteServiceTask_validate(&$in)
	{
		$v = new validation($in);
		$v->f('task_id', 'task_id', 'required');
		$v->f('service_id', 'service_id', 'required');
		return $v->run();
	}

	/**
	 * undocumented function
	 * this function has changed, it does not save expenses it saves purchases
	 * @return void
	 * @author PM
	 **/
	function serviceAddSupply(&$in)
	{
		if(!$this->serviceAddSupply_validate($in)){
			return false;
		}
		$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='0' ORDER BY sort_order DESC LIMIT 1");
		if(!$last_sort_order){
			$last_sort_order=0;
		}	
		$in['e_id'] = $this->db->insert("INSERT INTO servicing_support_articles SET service_id='".$in['service_id']."', name='".$in['name']."',billable='1',sort_order='".($last_sort_order+1)."' ");
		$in['name'] = stripslashes($in['name']);
		msg::success( gm('Changes saved'), 'success');
		if(ark::$method=='serviceAddSupply'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddSupply_validate(&$in)
	{
		$v = new validation($in);
		$v->f('name', 'name', 'required');
		// $v->f('expense_id', 'expense_id', 'required');
		$v->f('service_id', 'service_id', 'required');
		return $v->run();
	}

	function deleteServiceSupply(&$in){
		if(!$this->deleteServiceArticle_validate($in)){
			return false;
		}
		$billed=$this->db->field("SELECT billed FROM servicing_support_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			msg::error(gm('Cannot delete billed item'),"error");
			json_out($in);
		}
		$this->db->query("DELETE FROM servicing_support_articles WHERE a_id='".$in['a_id']."' AND service_id='".$in['service_id']."' ");
		msg::success ( gm('Changes saved'),'success');
		if(ark::$method=='deleteServiceSupply'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteServiceArticle(&$in)
	{
		if(!$this->deleteServiceArticle_validate($in)){
			return false;
		}
		$billed=$this->db->field("SELECT SUM(invoiced) FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$in['article_id']."' ");
		if($billed){
			msg::error(gm('Cannot delete billed item'),"error");
			json_out($in);
		}
		$delivery_ids=array();
		$article_delivered=$this->db->query("SELECT * FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$in['article_id']."' ");
		while($article_delivered->next()){
			$in['quantity']=$article_delivered->f('quantity');
			$in['delivery_id']=$article_delivered->f('delivery_id');
			array_push($delivery_ids,$article_delivered->f('delivery_id'));
			$this->undo_delivery($in);
			$this->db->query("DELETE FROM service_delivery WHERE id='".$article_delivered->f('id')."' ");
			if(WAC){
				generate_purchase_price($in['article_id']);
		 	}
		}
		foreach($delivery_ids as $key=>$value){
			$delivery_data=$this->db->field("SELECT COUNT(id) FROM service_delivery WHERE delivery_id='".$value."' ");
			if(!$delivery_data){
				$this->db->query("DELETE FROM service_deliveries WHERE delivery_id='".$value."' ");
			}
		}
		$this->db->query("DELETE FROM servicing_support_articles WHERE a_id='".$in['a_id']."' AND service_id='".$in['service_id']."' ");
		$this->db->query("DELETE FROM servicing_support_articles WHERE is_variant_for_line='".$in['a_id']."' AND service_id='".$in['service_id']."' ");
		$this->db->query("DELETE FROM servicing_support_articles WHERE component_for='".$in['a_id']."' AND service_id='".$in['service_id']."' ");
		msg::success ( gm('Changes saved'),'success');
		if(ark::$method=='deleteServiceArticle'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteServiceArticle_validate(&$in)
	{
		$v = new validation($in);
		$v->f('a_id', 'a_id', 'required');
		$v->f('service_id', 'service_id', 'required');
		return $v->run();
	}

	function shropdf(&$in){
		if(!$this->deleteService_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE servicing_support SET shropdf='".$in['value']."' WHERE service_id='".$in['service_id']."' ");
		// msg::$success = gm('Changes saved');
		msg::success( gm('Changes saved'), 'success');
		if(ark::$method=='shropdf'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function changeServiceStatus(&$in)
	{
		if(!$in['service_id']){
			msg::error( gm("Invalid ID"),'error');
			json_out($in);
			return false;
		}
		if($in['status'] == 1){
			$users_nr=$this->db->field("SELECT COUNT(u_id) FROM servicing_support_users WHERE service_id='".$in['service_id']."' ");
			if(!$users_nr){
				msg::error( gm("At least one user needed"),'error');
				json_out($in);
				return false;

			}
		}
		if(!$in['planeddate_js'] && $in['status'] == 1 && !$in['planeddate_ts']){
			msg::error( gm("Please select a planed date"),'error');
			json_out($in);
			return false;
		}

		if(isset($in['planeddate_js']) && !empty($in['planeddate_js']) ){
			$in['planeddate_ts'] = strtotime($in['planeddate_js']);
		}

		if(isset($in['planeddate_ts']) && !empty($in['planeddate_ts'])){
			$in['planeddate_ts'] = mktime(0,0,0,date('n',$in['planeddate_ts']),date('j',$in['planeddate_ts']),date('Y',$in['planeddate_ts']));
			$in['planeddate'] = date(ACCOUNT_DATE_FORMAT,$in['planeddate_ts']);
		}

		$sql = '';
		if($in['status'] == 1 && $in['ss'] == 1){
			if(!$in['finishDate_ts']){
				$now = time();
			}else{
				$now = $in['finishDate_ts'];
			}
			$in['finishDate'] = date(ACCOUNT_DATE_FORMAT,$now);
			$in['finishDate_ts'] = $now;
			$sql = ", finishDate='".$now."' ";
		}
		if($in['startdate']){
			$sql .= ", startdate='".$in['startdate']."' ";
		}
		if($in['duration']){
			$sql .= ", duration='".$in['duration']."' ";
		}
		$this->db->query("UPDATE servicing_support SET status='".$in['status']."' ".$sql."  WHERE service_id='".$in['service_id']."' ");
		if($in['planeddate_ts']){
			$this->db->query("UPDATE servicing_support SET planeddate='".$in['planeddate_ts']."' WHERE service_id='".$in['service_id']."' ");
		}
		if( $in['planeddate_ts']){
			$this->updateCalendar($in);
		}
		if($in['remove_dates']){
			$this->db->query("UPDATE servicing_support SET planeddate='0',startdate='0',duration='0' WHERE service_id='".$in['service_id']."' ");
			$this->db->query("DELETE FROM customer_meetings WHERE service_id='".$in['service_id']."' ");
		}
		if(ark::$method=='changeServiceStatus'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
	  if($in['status']==1){
	  	insert_message_log($this->pag,'{l}Intervention status changed to planned by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id'],false);
	  }elseif($in['status']==2){
	  	insert_message_log($this->pag,'{l}Intervention status changed to closed by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id'],false);
	  }else{
	  	insert_message_log($this->pag,'{l}Intervention status changed to draft by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id'],false);
	  }
	  
		msg::success( gm('Changes saved'), 'success');

		// }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function add_sheet(&$in)
	{
		if(isset($in['service_date']) && !empty($in['service_date']) ){
			$in['service_date'] = strtotime($in['service_date']);
		}
		$this->db->query("SELECT u_id FROM servicing_support_users WHERE user_id='".$in['sheet_user_id']."' AND service_id='".$in['servicing_sheet_id']."' ");
		if(!$this->db->next()){
			msg::notice ( gm('User was not assign to maintenance'),'notice');
			if(!$in['xget']){
		  	json_out($in);
		  }
			return false;
		}
		$st = $this->db->field("SELECT status FROM servicing_support WHERE service_id='".$in['servicing_sheet_id']."'");
		if($st != 1){
			msg::notice ( gm('Maintenance is not in progress'),'notice');
			if(!$in['xget']){
		  	json_out($in);
		  }
			return false;
		}

		$in['service_date'] = mktime(0,0,0,date('n',$in['service_date']),date('j',$in['service_date']),date('Y',$in['service_date']));
		$this->service = new service();
		$this->service->addServiceSheet($in);
		$result_for_hours = $this->db->query("SELECT sum(servicing_support_sheet.end_time-servicing_support_sheet.start_time-servicing_support_sheet.break) as h, servicing_support_users.u_id
						FROM servicing_support_users
						LEFT JOIN servicing_support_sheet ON ( servicing_support_sheet.service_id=servicing_support_users.service_id
														AND servicing_support_sheet.user_id=servicing_support_users.user_id )
						WHERE servicing_support_users.service_id='".$in['servicing_sheet_id']."' AND servicing_support_users.user_id='".$in['sheet_user_id']."' ");
		$in['houres']=number_as_hour($result_for_hours->f('h'));
		$in['usr_id']=$result_for_hours->f('u_id');
		$in['service_id'] = $in['servicing_sheet_id'];
		if(ark::$method=='add_sheet'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['servicing_sheet_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['servicing_sheet_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
	  msg::success( gm('Changes saved'), 'success');
	  if(!$in['xget']){
	  	json_out($in);
	  }
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteServiceSheet(&$in)
	{
		if(!$this->deleteServiceSheet_validate($in)){
			return false;
		}
		$user_id = $this->db->field("SELECT u_id FROM servicing_support_users
			WHERE service_id='".$in['servicing_sheet_id']."' AND user_id=(SELECT user_id FROM servicing_support_sheet WHERE id='".$in['serv_sheet_id']."') ");#get the info first
		$this->db->query("DELETE FROM servicing_support_sheet WHERE id='".$in['serv_sheet_id']."' "); # delete the entry

		$result_for_hours = $this->db->query("SELECT sum(servicing_support_sheet.end_time-servicing_support_sheet.start_time-servicing_support_sheet.break) as h, servicing_support_users.u_id
						FROM servicing_support_users
						LEFT JOIN servicing_support_sheet ON ( servicing_support_sheet.service_id=servicing_support_users.service_id
														AND servicing_support_sheet.user_id=servicing_support_users.user_id )
						WHERE servicing_support_users.service_id='".$in['servicing_sheet_id']."' AND servicing_support_users.u_id='".$user_id."' ");
		$in['houres']=number_as_hour($result_for_hours->f('h'));
		$in['usr_id']=$result_for_hours->f('u_id');
		$in['service_id'] = $in['servicing_sheet_id'];
		msg::success ( gm('Changes saved'),'success');
		if(ark::$method=='deleteServiceSheet'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['servicing_sheet_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['servicing_sheet_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
	  msg::success( gm('Changes saved'), 'success');
		return false;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteServiceSheet_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('serv_sheet_id', gm('ID'), 'required:exist[servicing_support_sheet.id]', gm("Invalid ID"));
		return $v->run();
	}

	function generate_pdf(&$in)
  {
    include_once(__DIR__.'/../controller/print.php');
    //return $str;
  }

  /**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function editServiceSheet(&$in)
	{
		if(!$this->editServiceSheet_validate($in)){
			json_out($in);
			return false;
		}
		if(isset($in['date_js']) && !empty($in['date_js']) ){
			$in['date'] = strtotime($in['date_js']);
		}

		$this->db->query("UPDATE servicing_support_sheet SET
											start_time = '".$in['start_time']."',
											end_time = '".$in['end_time']."',
											date = '".$in['date']."',
											break = '".$in['break']."',
											notes='".addslashes($in['time_report'])."',
											internal_report='".$in['internal_report']."',
											extra_hours='".$in['extra_hours']."'
											WHERE id='".$in['id']."'
											");
		$this->db->query("UPDATE servicing_support SET status='1' WHERE service_id='".$in['service_id']."' ");
		$user_id = $this->db->field("SELECT u_id FROM servicing_support_users
			WHERE service_id='".$in['service_id']."' AND user_id=(SELECT user_id FROM servicing_support_sheet WHERE id='".$in['id']."') ");

		$result_for_hours = $this->db->query("SELECT sum(servicing_support_sheet.end_time-servicing_support_sheet.start_time-servicing_support_sheet.break) as h, servicing_support_users.u_id
						FROM servicing_support_users
						LEFT JOIN servicing_support_sheet ON ( servicing_support_sheet.service_id=servicing_support_users.service_id
														AND servicing_support_sheet.user_id=servicing_support_users.user_id )
						WHERE servicing_support_users.service_id='".$in['service_id']."' AND servicing_support_users.u_id='".$user_id."' ");
		$in['houres']=number_as_hour($result_for_hours->f('h'));
		$in['usr_id']=$result_for_hours->f('u_id');

		// msg::$success = gm('Entry updated');
		if(ark::$method=='editServiceSheet'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
	  msg::success( gm('Changes saved'), 'success');
	  // json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function editServiceSheet_validate(&$in)
	{

		$is_ok = true;
		$v = new validation($in);
		$v->field('id', gm('ID'), 'required:exist[servicing_support_sheet.id]', gm("Invalid ID"));
		$is_ok = $v->run();
		if($is_ok === true){
			return $this->addServiceSheet_validate($in);
		}
		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addServiceSheet_validate(&$in)
	{

		$is_ok = true;
		$v = new validation($in);
		$v->field('service_id', gm('ID'), 'required:exist[servicing_support.service_id]', gm("Invalid ID"));
		// $v->field('servicing_task_id', gm('ID'), "required:exist[servicing_support_tasks.task_id.( service_id='".$in['servicing_sheet_id']."' )]", gm("Invalid ID"));
		$v->field('service_date','service_date','required');
		$v->field('start_time','start_time','required');
		$v->field('end_time','end_time','required');
		$is_ok = $v->run();
		if($is_ok === true){
			if($in['start_time'] > $in['end_time']){
				// msg::$error = gm('Start time should be bigger then end time');
				msg::error( gm('Start time should be lower then end time'), 'error');
				$is_ok = false;
			}
		}else if($in['break'] && $in['break'] >= ($in['end_time']-$in['start_time']) ){
			// msg::$error = gm('Break is bigger then the hours');
			msg::error( gm('Break is bigger then the hours'), 'error');
			$is_ok = false;
		}
		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function add_exp(&$in)
	{
		if(isset($in['service_date']) && !empty($in['service_date']) ){
			$in['service_date'] = strtotime($in['service_date']);
		}
		$in['date'] = mktime(0,0,0,date('n',$in['service_date']),date('j',$in['service_date']),date('Y',$in['service_date']));
		$in['exp_is_srv']=1;
		if(!$in['user_id']){
			msg::$error = gm('Please select a user');
			if(!$in['xget']){
		  	json_out($in);
		  }
			return false;
		}
		$this->db->query("SELECT u_id FROM servicing_support_users WHERE user_id='".$in['user_id']."' AND service_id='".$in['service_id']."' ");
		if(!$this->db->next()){
			msg::$notice = gm('User was not assign to maintenance');
			if(!$in['xget']){
		  	json_out($in);
		  }
			return false;
		}
		$in['amount'] = return_value($in['amount']);
		$this->service = new service();
		$this->service->add_expense($in);
		if(ark::$method=='add_exp'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
	  if(!$in['xget']){
	  	json_out($in);
	  }
		return true;
	}

	function update_expense(&$in){

		if(!$this->validate_add_expense($in)){
			return false;
		}
		/*if(isset($in['service_date']) && !empty($in['service_date']) ){
			$in['service_date'] = strtotime($in['service_date']);
		}*/
		if($in['billabel']=='true'){
			$in['billabel']='1';
		}
		$this->db->insert("UPDATE project_expenses SET amount='".$in['amount']."',
													 		 note='".$in['note']."',
													 		 date='".$in['service_date']."',
													 		 billable = '".$in['billabel']."'
													 	 WHERE id='".$in['id']."' ");
		$in['ndate'] = date(ACCOUNT_DATE_FORMAT,$in['service_date']);
		$in['namount'] = $in['amount'];
		$this->db->query("SELECT * FROM expense WHERE expense_id='".$in['expense_id']."' ");
		$this->db->move_next();
		if($this->db->f('unit_price')){
			$in['amount'] = place_currency(display_number($in['amount'] * $this->db->f('unit_price')))." (".$in['amount']." ". $this->db->f('unit').")";
		}else{
			$in['amount'] = place_currency(display_number($in['amount']));
		}

		if(!empty($_FILES['Filedata']['name'])){

			$this->upload_file($in);
		}

		msg::success ( gm('Expense Updated'),'success');
		if(ark::$method=='update_expense'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}
	function validate_add_expense(&$in){
		$is_ok = true;

		$v = new validation($in);

		$v->field('service_id', gm('ID'), 'required', gm("Please select a Service"));
		$v->field('expense_id', gm('ID'), 'required', gm("Please select a category"));
		$v->field('amount',gm('Quantity'),'required:numeric');

		$is_ok = $v->run();
		if ($is_ok==false)
		{
			$in['no_hide'] = 1;
		}
		else
		{
			$in['no_hide'] = 0;
		}
		return $is_ok;
	}
	function upload_file(&$in)
	{

	    global $_FILES, $is_live, $script_path,$config;
        $allowed['.png']=1;
        $allowed['.tiff']=1;
        $allowed['.jpg']=1;
        $allowed['.jpeg']=1;
        $f_ext=substr($_FILES['Filedata']['name'],strrpos($_FILES['Filedata']['name'],"."));
        if(!$allowed[strtolower($f_ext)])
        {
        	// $in['error'].="Only jpg, jpeg, png and tiff files are allowed.";
        	msg::error (gm("Only jpg, jpeg, png and tiff files are allowed."),'error' );
        	json_out($in);
        	return false;
        }

     	$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."'");
    	if($this->db->move_next())
    	{
        	@unlink(UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
					$this->db->query("UPDATE project_expenses SET picture=NULL WHERE id='".$in['id']."'");
    	}


		$f_ext = strtolower($f_ext);
        $f_title="receipt_".$in['id'].$f_ext;
        if(!file_exists(UPLOAD_PATH.DATABASE_NAME."/receipt/")){
        	mkdir(UPLOAD_PATH.DATABASE_NAME."/receipt/", 0775, true);
        }
        $f_out=UPLOAD_PATH.DATABASE_NAME."/receipt/".$f_title;

        if(!$_FILES['Filedata']['tmp_name'])
        {
        	// $in['error'].="Please choose a file!"."<br>";
        	msg::error (gm("Please choose a file!"),'error' );
        	json_out($in);
            return false;
        }

        $filename = $_FILES['Filedata']['tmp_name'];
		if (FALSE === move_uploaded_file($_FILES['Filedata']['tmp_name'],$f_out)) {
			// $in['error'].="Unable to upload the file.";
			msg::error (gm("Unable to upload the file."),'error' );
			json_out($in);
            	return false;
		}

		@chmod($f_out, 0664);
    	$this->db->query("UPDATE project_expenses SET picture='".$f_title."' WHERE id='".$in['id']."' ");
        return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceArticleUpdate(&$in)
	{
		if(!$this->serviceArticleUpdate_validate($in)){
			json_out($in);
			return false;
		}
		$field = array('quantity','to_deliver','delivered','margin','price');
		if(in_array($in['field'], $field)){
			if($in['field']=='quantity'){
				$delivered=$this->db->field("SELECT article_delivered FROM servicing_support_articles WHERE a_id='".$in['article_id']."' AND article_id!='0' ");
				if($delivered){
					$in['already_delivered']=1;
					$in['quantity']=display_number($this->db->field("SELECT quantity FROM servicing_support_articles WHERE a_id='".$in['article_id']."' "));
					msg::error(gm("Cannot change quantity of delivered article"),"error");
					json_out($in);
				}else{
					$article_id=$this->db->field("SELECT article_id FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
					if($article_id){
						$stock=$this->db->field("SELECT stock FROM pim_articles WHERE article_id='".$article_id."' ");
						if($stock-return_value($in['value'])<0){
							$in['minus_stock']=1;
							$in['stock']=display_number($stock);
						}
						$this->db->query("UPDATE servicing_support_articles SET ".$in['field']."='".return_value($in['value'])."' WHERE a_id='".$in['article_id']."' ");
					}else{						
						$this->db->query("UPDATE servicing_support_articles SET ".$in['field']."='".return_value($in['value'])."' WHERE a_id='".$in['article_id']."' ");
					}
				}
			}else{
				if($in['field']=='delivered'){
					$old_q=$this->db->field("SELECT quantity FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
					if(return_value($in['value'])>$old_q){
						msg::error(gm("Delivered quantity is bigger than actual quantity"),"error");
						json_out($in);
					}
				}
				$this->db->query("UPDATE servicing_support_articles SET ".$in['field']."='".return_value($in['value'])."' WHERE a_id='".$in['article_id']."' ");
			}
		}
		/*if($in['field'] == 'delivered' || $in['field'] == 'price' || $in['field'] == 'margin'){
			$this->addProjectPurchas($in);
		}*/
		msg::success( gm('Changes saved'), 'success');
	  json_out($in);
		// msg::$success = gm('Changes saved');
		return false;
	}

	function update_article_qty(&$in)
	{
		$delivered=$this->db->field("SELECT article_delivered FROM servicing_support_articles WHERE a_id='".$in['a_id']."' ");
		if($delivered){
			$in['qty']=display_number($this->db->field("SELECT quantity FROM servicing_support_articles WHERE a_id='".$in['a_id']."' "));
			msg::error(gm('Quantity cannot be changed due to article delivery'),"error");
			json_out($in);
		}else{
			$in['qty_done']=1;
			$article_id=$this->db->field("SELECT article_id FROM servicing_support_articles WHERE a_id='".$in['a_id']."' ");
			$stock=$this->db->field("SELECT stock FROM pim_articles WHERE article_id='".$article_id."' ");
			if($stock-return_value($in['qty'])<0 && defined('ALLOW_STOCK') && ALLOW_STOCK=='1'){
				$in['minus_stock']=1;
				$in['stock']=display_number($stock);
			}
			$this->db->query("UPDATE servicing_support_articles SET quantity='".return_value($in['qty'])."' WHERE a_id='".$in['a_id']."' ");
			$this->db->query("UPDATE servicing_support_articles SET quantity='".return_value($in['qty'])."' WHERE component_for='".$in['a_id']."' ");
			msg::success(gm('Changes saved'),"success");
		}
		return true;
	}
	function update_article_name(&$in)
	{
		$delivered=$this->db->field("SELECT article_delivered FROM servicing_support_articles WHERE a_id='".$in['a_id']."' ");
		if($delivered){
			$in['name']=$this->db->field("SELECT name FROM servicing_support_articles WHERE a_id='".$in['a_id']."' ");
			msg::error(gm('Name cannot be changed due to article delivery'),"error");
			json_out($in);
		}else{
			$this->db->query("UPDATE servicing_support_articles SET name='".$in['name']."' WHERE a_id='".$in['a_id']."' ");
			msg::success(gm('Changes saved'),"success");
		}
		return true;
	}
	function update_task_name(&$in)
	{
		$this->db->query("UPDATE servicing_support_tasks SET task_name='".$in['name']."' WHERE task_id='".$in['task_id']."' ");
		msg::success(gm('Changes saved'),"success");
		return true;
	}

	function serviceArtSellUpdate(&$in)
	{
		if(!$this->serviceArticleUpdate_validate($in)){
			json_out($in);
			return false;
		}
		$billed=$this->db->field("SELECT billed FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
		if($billed){
			$in['price']=display_number($this->db->field("SELECT price FROM servicing_support_articles WHERE a_id='".$in['article_id']."' "));
			msg::error(gm('Cannot change price of billed article'),"error");
			return false;
		}
		$purchase_price = $this->db->field("SELECT purchase_price FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
		$margin_nr=return_value($in['value'])-$purchase_price;
		$margin_percent=(return_value($in['value'])-$purchase_price)/return_value($in['value'])*100;
		$this->db->query("UPDATE servicing_support_articles SET margin='".$margin_nr."',margin_per='".$margin_percent."',price='".return_value($in['value'])."' WHERE a_id='".$in['article_id']."' ");
		// $in['margin']=display_number($margin_percent);
		msg::success( gm('Changes saved'), 'success');
	  json_out($in);
		// msg::$success = gm('Changes saved');
		return false;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceArticleUpdate_validate(&$in)
	{
		$v = new validation($in);
		$v->f('article_id', 'article_id', 'required');
		return $v->run();
	}

	function serviceArtPriceUpdate(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
		if($billed){
			$in['purchase_price']=display_number($this->db->field("SELECT purchase_price FROM servicing_support_articles WHERE a_id='".$in['article_id']."' "));
			msg::error(gm('Cannot change purchase price of billed article'),"error");
			json_out($in);
		}
		$margin_per = $this->db->field("SELECT margin_per FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
		if($margin_per==100){
			$sell_price1=(-return_value($in['value'])*100)/(1-100);
			$sell_price2=(-return_value($in['value'])*100)/(99-100);
			$sell_price=$sell_price1+$sell_price2;
		}else{
			$sell_price=(-return_value($in['value'])*100)/($margin_per-100);
		}	
		$this->db->query("UPDATE servicing_support_articles SET price='".$sell_price."',purchase_price='".return_value($in['value'])."' WHERE a_id='".$in['article_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function serviceArtMargUpdate(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
		if($billed){
			$in['margin']=display_number($this->db->field("SELECT margin_per FROM servicing_support_articles WHERE a_id='".$in['article_id']."' "));
			msg::error(gm('Cannot change margin of billed article'),"error");
			json_out($in);
		}
		$article_purchase_price = $this->db->field("SELECT purchase_price FROM servicing_support_articles WHERE a_id='".$in['article_id']."' ");
		if(return_value($in['value'])==100){
			$sell_price1=(-$article_purchase_price*100)/(1-100);
			$sell_price2=(-$article_purchase_price*100)/(99-100);
			$sell_price=$sell_price1+$sell_price2;
		}else{
			$sell_price=(-$article_purchase_price*100)/(return_value($in['value'])-100);
		}	
		$margin=$sell_price-$article_purchase_price;
		$this->db->query("UPDATE servicing_support_articles SET margin='".$margin."',price='".$sell_price."',margin_per='".return_value($in['value'])."' WHERE a_id='".$in['article_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateArt(&$in)
	{

		if(!$in['service_id']){
			return false;
		}
		$allowd = array('name','info');
		if(!in_array($in['type'],$allowd)){
			return false;
		}
		$this->db->query("UPDATE servicing_support_articles SET ".$in['type']." ='".$in['value']."' WHERE service_id='".$in['service_id']."' AND a_id='".$in['purchase_id']."' ");
		msg::$success = gm('Changes saved');
		if(ark::$method=='updateArt'){
			# for pdf
			$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
			/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    $params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
	  msg::success( gm('Changes saved'), 'success');
	  json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addTaskComment(&$in)
	{
		if(!$this->addTaskComment_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE servicing_support_tasks SET comment='".$in['t_comment']."' WHERE task_id='".$in['task_id']."' AND service_id='".$in['service_id']."' ");
		msg::success( gm('Changes saved'), 'success');
	  json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addTaskComment_validate(&$in)
	{
		$v = new validation($in);
		$v->field('service_id', gm('Invoice Nr'), 'required:exist[servicing_support.service_id]');
		$v->field('task_id', gm('Invoice Nr'), 'required:exist[servicing_support_tasks.task_id]');
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendNewEmail(&$in)
	{
		if(!$this->sendNewEmailValidate($in)){
			msg::error (gm('Invalid email address ').$in['email'],'error');
			json_out($in);
			return false;
		}
		global $config;
		$in['weblink_url'] =$this->external_id($in);
    
    	$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
    	$event_start = date( "Y-m-d", $this->db->f('planeddate') ) . ' ' . number_as_hour2($this->db->f('startdate'));
    	$event_end=date( "Y-m-d", $this->db->f('planeddate') ).' ';
		if($this->db->f('startdate') && $this->db->f('duration')){
			if($this->db->f('startdate')+$this->db->f('duration')>24){
				$event_end=date("Y-m-d",$this->db->f('planeddate')+86400).' '.number_as_hour2($this->db->f('startdate')+$this->db->f('duration')-24);				
			}else{
				$event_end.=number_as_hour2($this->db->f('startdate')+$this->db->f('duration'));
			}
		}

	  	
	  	$location = $this->db->f('free_field');
	  	$uid = $this->db->f('serial_number');


		$e_lang = $this->db->f('email_language');
		$in['lid'] = $this->db->f('email_language');
		if($this->db->f('pdf_layout'))
		{
			$in['type']=$this->db->f('pdf_layout');
			$in['logo']=$this->db->f('pdf_logo');
		}else {
			$in['type'] = ACCOUNT_MAINTENANCE_PDF_FORMAT;
		}

		$this->generate_pdf($in);

  	$mail = new PHPMailer();
    $mail->WordWrap = 50;

    $def_email = $this->default_email();
    $body= stripslashes(htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8'));

    $mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']),0);
    $mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));

    $subject= stripslashes(utf8_decode($in['e_subject']));

    $mail->Subject = $subject;

    $in['serial_number'] = $this->db->f('serial_number');
    if($this->db->f('serial_number')){
      $name=$this->db->f('serial_number').'.pdf';
    }elseif($this->db->f('service_id')){
      $name='intervention_'.$this->db->f('service_id').'.pdf';    
    }

    if($in['include_pdf']){
    	$tmp_file_name = 'intervention.pdf';
      if($in['attach_file_name']){
        $tmp_file_name = $in['attach_file_name'];
      }
      $mail->AddAttachment($tmp_file_name, $name); // attach files/invoice-user-1234.pdf, and rename it to invoice.pdf
    }

    if($in['attach_calendar']){
    	include_once(__DIR__."/../../misc/model/iCalendar.php");

      $calendar = new iCalendar($in, DATABASE_NAME, utf8_encode($subject), $event_start, $event_end, utf8_encode($subject), $location , $uid);

      $calendar_data = $calendar->get_iCalendar($in);
      $mail->addStringAttachment($calendar_data['calendar'], 'calendar.ics');

    }

    if($in['dropbox_files']){          
          foreach ($in['dropbox_files'] as $key => $value) {
            if($value['checked'] == 1){           
                 $main_content = file_get_contents($value['url']);
        
                if($main_content){
                 	$mail->addStringAttachment($main_content, $value['name']);
                }
            }
          }
    }

    if(!$e_lang || $e_lang > 4){
    	$e_lang=1;
    }
    $text_array = array('1' => array('simple' => array('1' => 'INTERVENTION WEB LINK', '2'=> 'You can download your intervention HERE','3'=>"HERE",'4'=>'Web Link'),
    								   'pay' => array('1' => 'INTERVENTION WEB LINK', '2'=> 'You can download your intervention HERE and pay it online','3'=>"HERE",'4'=>'Web Link')
    								   ),
    					'2' => array('simple' => array('1' => 'LIEN WEB INTERVENTION', '2'=> 'Vous pouvez télécharger votre intervention ICI','3'=>'ICI','4'=>'Lien web'),
    								   'pay' => array('1' => 'LIEN WEB INTERVENTION', '2'=> 'Vous pouvez télécharger votre intervention ICI et la payer en ligne','3'=>'ICI','4'=>'Lien web')
    								   ),
    					'3' => array('simple' => array('1' => 'WEB LINK INTERVENTIE', '2'=> 'U kan uw interventie HIER downloaden','3'=>'HIER','4'=>'Weblink'),
    								   'pay' => array('1' => 'WEB LINK INTERVENTIE', '2'=> 'U kan uw interventie HIER downloaden en online betalen','3'=>'HIER','4'=>'Weblink')
    								   ),
    					'4' => array('simple' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
    								   'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER','4'=>'WEB-LINK')
    								   )
    );
		$mail->WordWrap = 50;

	$body = stripslashes(utf8_decode($in['e_message']));	

	if($in['sendgrid_selected']){
      $body = stripslashes($in['e_message']);
    } 

    $in['use_html'] =1;
    if($in['use_html']){

    	$mail->IsHTML(true);
    	$head = '<style>p {	margin: 0px; padding: 0px; }</style>';
    	//$body = stripslashes(utf8_decode($in['e_message']));

    	if(defined('USE_MAINTENANCE_WEB_LINK') && USE_MAINTENANCE_WEB_LINK == 1){
				//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['service_id']."' AND `type`='m' ");
    		$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type ",['d'=>DATABASE_NAME,'item_id'=>$in['service_id'],'type'=>'m']);
      	$extra = "<p style=\"background: #f7f9fa; width: 500px; text-align:center; padding-bottom: 15px; border: 2px solid #deeaf0; color:#868d91;\"><br />";
        $extra .="<img src=\"https://app.akti.com/pim/img/email_arrow_left.png\" width=\"18\" height=\"8\" />&nbsp;&nbsp;";
        $extra .="<b style=\"color: #5199b7;\"><a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b>";
        $extra .="&nbsp;&nbsp;<img src=\"https://app.akti.com/pim/img/email_arrow_right.png\" width=\"18\" height=\"8\" /><br />";
          $extra .=$text_array[$e_lang]['simple']['2']."<br /></p>";
      	// $body .=$extra;
      	$body=str_replace('[!WEB_LINK!]', $extra, $body);
      	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
      	$body=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$exist_url, $body);
      }else{
      	$body=str_replace('[!WEB_LINK!]', '', $body);
      	$body=str_replace('[!WEB_LINK_2!]', '', $body);
      	$body=str_replace('[!WEB_LINK_URL!]', '', $body);
      }
    	$mail->Body    = $head.$body;
    }else{
      if(defined('USE_MAINTENANCE_WEB_LINK') && USE_MAINTENANCE_WEB_LINK == 1){
				//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['service_id']."' AND `type`='m' ");
      			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type ",['d'=>DATABASE_NAME,'item_id'=>$in['service_id'],'type'=>'m']);

      	$extra = "\n\n-----------------------------------";
      	$extra .="\n".$text_array[$e_lang]['simple']['1'];
      	$extra .="\n-----------------------------------";
    		$extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
      	// $body .=$extra;
      	$body=str_replace('[!WEB_LINK!]', $extra, $body);
      	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
      	$body=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$exist_url, $body);
      }else{
      	$body=str_replace('[!WEB_LINK!]', '', $body);
      	$body=str_replace('[!WEB_LINK_2!]', '', $body);
      	$body=str_replace('[!WEB_LINK_URL!]', '', $body);
      }
    	$mail->MsgHTML(nl2br(($body)));
    }

	$mail->AddAddress(trim($in['email']));

    $msg_log = '{l}Intervention was sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl}: '.$in['email'];

   // $in['sendgrid_selected'] = 0;//pt test

    if($in['sendgrid_selected']){

    	  $in['new_email'] = $in['email'];
	      //SendGrid Send
	      include_once(__DIR__."/../../misc/model/sendgrid.php");
	      $sendgrid = new sendgrid($in, $this->pag, $this->field_n,$in['service_id'], $body,$def_email, DATABASE_NAME);

	      $sendgrid_data = $sendgrid->get_sendgrid($in);

	      if($sendgrid_data['error']){
	        msg::error($sendgrid_data['error'],'error');
	      }elseif($sendgrid_data['success']){
	        msg::success(gm("Email sent by Sendgrid"),'success');
	      }

	     // $msg_log = $msg_log .= " {l}via SendGrid{endl}.";
    } else {
      $mail->Send();
      if($mail->IsError()){
			msg::error ($mail->ErrorInfo,'error');
			json_out($in);
			return false;
		}
		/*if($mail->ErrorInfo){
			msg::notice ($mail->ErrorInfo,'notice');
		}*/
		msg::success( gm("Email sent"),'success');
		if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
			update_log_message($in['logging_id'],' '.$in['email']);
		}else{
	 		$in['logging_id'] = insert_message_log($this->pag,$msg_log,$this->field_n,$in['service_id']);
		}
    } 

   	
   	if($in['attach_file_name']){
      unlink($in['attach_file_name']);
    }		

	if($in['mark_as_finished'] == 2){
		$in['status'] = 2;
		$this->changeServiceStatus($in);
	}

		json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendNewEmailValidate(&$in)
	{
		$v = new validation($in);
		$in['email'] = trim($in['email']);
		$v->field('email', 'Email', 'email');
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function external_id(&$in)
	{
		if(defined('USE_MAINTENANCE_WEB_LINK') && USE_MAINTENANCE_WEB_LINK == 1){
			//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['service_id']."' AND `type`='m'  ");
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type  ",['d'=>DATABASE_NAME,'item_id'=>$in['service_id'],'type'=>'m']);
			if(!$exist_url){
				$url = generate_chars();
				while (!isUnique($url)) {
					$url = generate_chars();
				}
				//$this->db_users->query("INSERT INTO urls SET `url_code`='".$url."', `database`='".DATABASE_NAME."', `item_id`='".$in['service_id']."', `type`='m'  ");
				$this->db_users->insert("INSERT INTO urls SET `url_code`= :url_code, `database`= :d, `item_id`= :item_id, `type`= :type  ",['url_code'=>$url,'d'=>DATABASE_NAME,'item_id'=>$in['service_id'],'type'=>'m']);
				$exist_url = $url;
			}
			return $exist_url;
		}
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
			if(MAIL_SETTINGS_PREFERRED_OPTION==2){
				if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
					$array['from']['email'] = MAIL_SETTINGS_EMAIL;
				}
			}else{
				if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
					$array['from']['email'] = ACCOUNT_EMAIL;
				}
			}
		}
		$this->db->query("SELECT * FROM default_data WHERE type='intervention_email' ");
		if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
			$array['from']['name'] = $this->db->f('default_name');
			if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)) && $this->db->f('value')!=''){
				$array['from']['email'] = $this->db->f('value');
			}
		}

		$this->db->query("SELECT * FROM default_data WHERE type='bcc_intervention_email' ");
          if($this->db->move_next() && $this->db->f('value')){
            $array['bcc']['email'] = $this->db->f('value'); // pot fi valori multiple, delimitate de ;
          }
		return $array;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function pdf_settings(&$in)
	{
		if(!$in['service_id']){
			msg::error ( gm("Invalid ID"),'error');
			json_out($in);
			return false;
		}

		$this->db->query("UPDATE servicing_support SET email_language='".$in['email_language']."', pdf_logo='".$in['logo']."', identity_id='".$in['identity_id']."'  WHERE service_id='".$in['service_id']."' ");
		msg::success ( gm('Changes saved'),'success');

		# for pdf
		$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
		/*$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
    $params = array();
    $params['service_id'] = $in['service_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);*/
    # for pdf
    json_out($in);
		return true;
	}

	function uploadify(&$in)
	{
		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = '../upload/'.DATABASE_NAME;
			$in['name'] = 'm_logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

				move_uploaded_file($tempFile,$targetFile);
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);
				$db_upload = new sqldb($database_2);
				$logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_MAINTENANCE_LOGO' ");
				if($logo == ''){
					if(defined('ACCOUNT_MAINTENANCE_LOGO')){
						$db_upload->query("UPDATE settings SET
		                           value = 'upload/".DATABASE_NAME."/".$in['name']."'
		                           WHERE constant_name='ACCOUNT_MAINTENANCE_LOGO' ");
					}else{
						$db_upload->query("INSERT INTO settings SET
		                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
		                           constant_name='ACCOUNT_MAINTENANCE_LOGO' ");
					}
				}
				$response['success'] = 'success';
        echo json_encode($response);
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
        echo json_encode($response);
				// echo gm('Invalid file type.');
			}
		}
	}

	/**
   * undocumented function
   *
   * @return Boolean
   * @author PM
   **/
  function delete_logo(&$in)
  {
    if($in['name'] == ACCOUNT_MAINTENANCE_LOGO){
      msg::error ( gm('You cannot delete the default logo'),'error');
      json_out($in);
      return true;
    }
    if($in['name'] == '../img/no-logo.png'){
      msg::error ( gm('You cannot delete the default logo'),'error');
      json_out($in);
      return true;
    }
    $exists = $this->db->field("SELECT COUNT(service_id) FROM servicing_support WHERE pdf_logo='".$in['name']."' ");
    if($exists>=1){
      msg::error ( gm('This logo is set for one ore more invoices.'),'error');
      json_out($in);
      return false;
    }
    @unlink($in['name']);
    msg::success ( gm('File deleted'),'success');
    return true;
  }

  /**
   * undocumented function
   *
   * @return boolean
   * @author PM
   **/
  function set_default_logo(&$in)
  {
    $this->db->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_MAINTENANCE_LOGO' ");
    msg::success (gm("Default logo set."),'success');
		json_out($in);
    return true;
  }

  /**
   * undocumented function
   *
   * @return boolean
   * @author PM
   **/
  function intervention_naming(&$in){
		$this->db->query("UPDATE settings SET value='".(int)$in['ACCOUNT_SERVICE_DIGIT_NR']."' WHERE constant_name='ACCOUNT_SERVICE_DIGIT_NR' ");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_SERVICE_START']."' WHERE constant_name='ACCOUNT_SERVICE_START' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
   * undocumented function
   *
   * @return Boolean
   * @author PM
   **/
  function label_update(&$in){

    $fields = '';
    $table = 'label_language_int';

    $exist = $this->db->query("SELECT label_language_id FROM $table WHERE label_language_id='".$in['label_language_id']."' ");

    if($exist->next()){
    $this->db->query("UPDATE $table SET
    	intervention ='".$in['intervention']."',
serial_number ='".$in['serial_number']."',
`date` ='".$in['date']."',
phone ='".$in['phone']."',
fax ='".$in['fax']."',
email ='".$in['email']."',
url ='".$in['url']."',
report ='".$in['report']."',
staff ='".$in['staff']."',
start_time ='".$in['start_time']."',
end_time ='".$in['end_time']."',
break ='".$in['break']."',
total ='".$in['total']."',
iname ='".$in['iname']."',
expenses ='".$in['expenses']."',
task ='".$in['task']."',
done ='".$in['done']."',
purchase ='".$in['purchase']."',
quantity ='".$in['quantity']."',
price ='".$in['price']."',
delivered ='".$in['delivered']."',
hrate ='".$in['hrate']."',
tamount ='".$in['tamount']."',
to_be_delivered ='".$in['to_be_delivered']."',
allpricenovat ='".$in['allpricenovat']."'
                      WHERE label_language_id='".$in['label_language_id']."'");
  }else{
  	$this->db->query("INSERT INTO $table SET
    	intervention ='".$in['intervention']."',
serial_number ='".$in['serial_number']."',
`date` ='".$in['date']."',
phone ='".$in['phone']."',
fax ='".$in['fax']."',
email ='".$in['email']."',
url ='".$in['url']."',
report ='".$in['report']."',
staff ='".$in['staff']."',
start_time ='".$in['start_time']."',
end_time ='".$in['end_time']."',
break ='".$in['break']."',
total ='".$in['total']."',
iname ='".$in['iname']."',
expenses ='".$in['expenses']."',
task ='".$in['task']."',
done ='".$in['done']."',
purchase ='".$in['purchase']."',
quantity ='".$in['quantity']."',
price ='".$in['price']."',
delivered ='".$in['delivered']."',
hrate ='".$in['hrate']."',
tamount ='".$in['tamount']."',
to_be_delivered ='".$in['to_be_delivered']."',
allpricenovat ='".$in['allpricenovat']."',
                       label_language_id='".$in['label_language_id']."'");
  }
    msg::success ( gm('Account has been successfully updated'),'success');
    // $in['failure']=false;
    json_out($in);
    return true;
  }

 	/**
   * undocumented function
   *
   * @return Boolean
   * @author PM
   **/
	function default_message(&$in)
	{
		$set = "text	= '".$in['text']."' ";
		if($in['use_html']){
			$set = "html_content	= '".$in['text']."' ";
		}
		$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					".$set."
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		//$this->db->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");
		$this->db->query("UPDATE sys_message SET use_html='1' WHERE name='".$in['name']."' ");

		$exist_calendar_available = $this->db->field("SELECT constant_name FROM settings WHERE constant_name='MAINTENANCE_ATTACH_CALENDAR' ");
		if($exist_calendar_available){
			$this->db->query("UPDATE settings SET value='".(int)$in['calendar_available']."' WHERE constant_name='MAINTENANCE_ATTACH_CALENDAR' ");
		}else{
			$this->db->query("INSERT INTO settings SET value='".(int)$in['calendar_available']."',constant_name='MAINTENANCE_ATTACH_CALENDAR' ");
		}
	
		msg::success( gm('Message has been successfully updated'),'success');
		// json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function settings(&$in){

		$this->db->query("UPDATE settings SET value='".(int)$in['maintenance_is_billable']."' WHERE constant_name='MAINTENANCE_IS_BILLABLE' ");
		$this->db->query("UPDATE settings SET value='".return_value($in['maintenance_rate'])."' WHERE constant_name='MAINTENANCE_RATE' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['use_stock_reservation']."' WHERE constant_name='USE_STOCK_RESERVATION' ");

		msg::success ( gm('Changes saved'),'success');

		return true;
	}

	/**
	 * Update the general expense at the settings level
	 *
	 * @return void
	 * @author PM
	 **/
	function update_expense2(&$in){

		if(!$this->validate_expense($in)){
			json_out($in);
			return false;
		}

		$this->db->query("UPDATE expense SET name='".$in['name']."', unit_price='".return_value($in['u_price'])."', unit='".$in['unit']."' WHERE expense_id='".$in['expense_id']."' ");

		msg::success ( gm("Expense category updated"),'success');
		json_out($in);
		return true;
	}

	/**
	 * Update the general expense at the settings level
	 *
	 * @return void
	 * @author PM
	 **/
	function validate_expense(&$in){
		$v = new validation($in);
		if($in['do']=='maintenance--maintenance-add_expense'){
			$v->field('name', gm('Name'), 'required:unique[expense.name]');
		}else{
			$v->field('name', gm("Name"), "required:unique[expense.name.( expense_id!='".$in['expense_id']."')]");
		}
		return $v->run();
	}

	/**
	 * Update the general expense at the settings level
	 *
	 * @return void
	 * @author PM
	 **/
	function add_expense(&$in){

		if(!$this->validate_expense($in)){
			json_out($in);
			return false;
		}

		$this->db->query("INSERT INTO expense SET name='".$in['name']."', unit_price='".return_value($in['u_price'])."', unit='".$in['unit']."' ");

		msg::success ( gm("Expense category added"),'success');
		json_out($in);
		return true;
	}

	/**
	 * Update the general expense at the settings level
	 *
	 * @return void
	 * @author PM
	 **/
	function delete_expense(&$in){

		$this->db->query("SELECT expense_id FROM expense WHERE expense_id='".$in['expense_id']."' ");
		if(!$this->db->move_next()){
			msg::error ( gm("Invalid ID"),'error');
			json_out($in);
			return false;
		}
		$exist_on_project=$this->db->field("SELECT project_id FROM project_expenses WHERE expense_id='".$in['expense_id']."' ");
		if($exist_on_project){
			msg::error ( gm("One or more expenses exist in this category"),'error');
			json_out($in);
			return false;
		}
		$v = new validation($in);
		$v->field('expense_id', gm('ID'), 'required:exist[expense.expense_id]');
		$is_ok=$v->run();
		if ($is_ok)
		{
			$this->db->query("DELETE FROM expense WHERE expense_id='".$in['expense_id']."' ");
			msg::success ( gm("Expense category deleted"),'success');
		}
		json_out($in);
		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function changeWL(&$in)
	{

		$consts = array('USE_MAINTENANCE_WEB_LINK','WEB_MAINTENANCE_INCLUDE_PDF','ALLOW_MAINTENANCE_COMMENTS','STARS_COMMENTS','ALLOW_STARS_RATING');
		foreach ($consts as  $value) {
			// if($in[strtolower($value)]){
				$this->db->query("SELECT * FROM settings WHERE constant_name='".$value."' ");
				if($this->db->next()){
					$up = $this->db->update("UPDATE settings SET value='".$in[strtolower($value)]."' WHERE constant_name='".$value."' ");
				}else{
					$this->db->query("INSERT INTO settings SET value='".$in[strtolower($value)]."',constant_name='".$value."' ");
				}
			// }
		}

		msg::success ( gm("Changes saved"),'success');
		// json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteService(&$in)
	{
		if(!$this->deleteService_validate($in)){
			return false;
		}
		$tracking_trace=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		$tables = array('servicing_support_users','servicing_support_articles','servicing_support_expenses','servicing_support_tasks');
		foreach ($tables as $key => $value) {
			$this->db->query("DELETE FROM $value WHERE service_id='".$in['service_id']."' ");
		}
		$this->db->query("DELETE FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		$this->db->query("DELETE FROM customer_meetings WHERE service_id='".$in['service_id']."' ");
		if($tracking_trace){
		      $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
		      $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
		}
		msg::success(gm('Changes saved'),"success");
		#here we should delete from amazon too

		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function archiveService(&$in)
	{
		if(!$this->deleteService_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE servicing_support SET active='0' WHERE service_id='".$in['service_id']."' ");
		$tracking_trace=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	      if($tracking_trace){
	      	$this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
	      }
		msg::success ( gm('Changes saved'),'success');
		insert_message_log($this->pag,'{l}Intervention has been archieved by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id'],false);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function activateService(&$in)
	{
		if(!$this->deleteService_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE servicing_support SET active='1' WHERE service_id='".$in['service_id']."' ");
		$tracking_trace=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
            if($tracking_trace){
          		$this->db->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
            }
		msg::success ( gm('Changes saved'),'success');
		insert_message_log($this->pag,'{l}Intervention has been successfully activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id'],false);
		return true;
	}

	function markAsSeenList(&$in)
	  {

	    if($in['all']){
	      if($in['value'] == 1){
	        foreach ($in['item'] as $key => $value) {
	          $_SESSION['mark_as_seen'][$value]= $in['value'];
	        }
	      }else{
	        foreach ($in['item'] as $key => $value) {
	          unset($_SESSION['mark_as_seen'][$value]);
	        }
	      }
	    }else{
	      if($in['value'] == 1){
	        $_SESSION['mark_as_seen'][$in['item']]= $in['value'];
	      }else{
	        unset($_SESSION['mark_as_seen'][$in['item']]);
	      }
	    }
	      json_out($_SESSION['mark_as_seen']);
	    return true;
	  }

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function MarkAsSeen(&$in)
	{	
		foreach($in['items'] as $key=>$value){
			$in['id']=$value;
			if(!$this->MarkAsSeenValidate($in)){
				json_out($in);
				return false;
			}
			$this->db->query("UPDATE customer_meetings SET notify='0' WHERE customer_meeting_id='".$in['id']."' ");
		}

		msg::success ( gm('Changes saved'),'success');
		$in['message_service'] = false;
		$nrs = $this->db->field("SELECT count(customer_meeting_id) FROM customer_meetings WHERE notify='1' AND user_id='".$_SESSION['u_id']."' ");
		if($nrs > 0 ){
			$in['message_service'] = true;
			$in['service_number'] = $nrs;
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function MarkAsSeenValidate(&$in)
	{
		$v = new validation($in);
		$v->field('id', gm('ID'), 'required:exist[customer_meetings.customer_meeting_id]', gm("Invalid ID"));
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
	    if(!$this->CanEdit($in)){
	      json_out($in);
	      return false;
	    }
	    if(!$this->tryAddAddressValidate($in)){
	      json_out($in);
	      return false;
	    }

	    $country_n = get_country_name($in['country_id']);
	    if($in['field']=='customer_id'){
	      Sync::start(ark::$model.'-'.ark::$method);

	      $address_id = $this->db->insert("INSERT INTO customer_addresses SET
	                                        customer_id     = '".$in['customer_id']."',
	                                        country_id      = '".$in['country_id']."',
	                                        state_id      = '".$in['state_id']."',
	                                        city        = '".$in['city']."',
	                                        zip         = '".$in['zip']."',
	                                        address       = '".$in['address']."',
	                                        billing       = '".$in['billing']."',
	                                        is_primary      = '".$in['primary']."',
	                                        delivery      = '".$in['delivery']."',
	                                        site 		='".$in['site']."'");
	      if($in['billing']){
	        $this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
	      }

	      Sync::end($address_id);

	      if($in['primary']){
	        if($in['address'])
	        {
	          if(!$in['zip'] && $in['city'])
	          {
	            $address = $in['address'].', '.$in['city'].', '.$country_n;
	          }elseif(!$in['city'] && $in['zip'])
	          {
	            $address = $in['address'].', '.$in['zip'].', '.$country_n;
	          }elseif(!$in['zip'] && !$in['city'])
	          {
	            $address = $in['address'].', '.$country_n;
	          }else
	          {
	            $address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
	          }
	          $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
	          $output= json_decode($geocode);
	          $lat = $output->results[0]->geometry->location->lat;
	          $long = $output->results[0]->geometry->location->lng;
	        }
	        $this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
	        $this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
	        $this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
	      }
	    }else{      
	      $address_id = $this->db->insert("INSERT INTO customer_contact_address SET
	                                        contact_id      = '".$in['contact_id']."',
	                                        country_id      = '".$in['country_id']."',                                        city        = '".$in['city']."',
	                                        zip         = '".$in['zip']."',
	                                        address       = '".$in['address']."',
	                                        is_primary      = '".$in['primary']."',
	                                        delivery      = '".$in['delivery']."',
	                                        site 		='".$in['site']."'");
	    }
	    msg::success ( gm("Changes have been saved."),'success');
	    insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
	    $in['address_id'] = $address_id;
	    $in['country'] = $country_n;
	    $in['service_id'] = $in['item_id'];
	    if($in['service_id'] && is_numeric($in['service_id'])){
	      $delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	      $this->db->query("UPDATE servicing_support SET 
	          free_field='".$delivery_address."', 
	          same_address='0' WHERE service_id='".$in['service_id']."'  "); 
	    }elseif ($in['service_id'] == 'tmp') {

	        $in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	        //$in['sameAddress'] = 1;
	        $in['delivery_address_id'] = $address_id;

	    }
	    $in['buyer_id'] = $in['customer_id'];
	    // $in['pagl'] = $this->pag;
	    
	    // json_out($in);
	    return true;

	}

	function CanEdit(&$in){
	    if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
	      return true;
	    }
	    $c_id = $in['customer_id'];
	    if(!$in['customer_id'] && $in['contact_id']) {
	      $c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	    }
	    if($c_id){
	      if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
	        $u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
	        if($u){
	          $u = explode(',', $u);
	          if(in_array($_SESSION['u_id'], $u)){
	            return true;
	          }
	          else{
	            msg::$warning = gm("You don't have enought privileges");
	            return false;
	          }
	        }else{
	          msg::$warning = gm("You don't have enought privileges");
	          return false;
	        }
	      }
	    }
	    return true;
	}

	/**
	   * undocumented function
	   *
	   * @return void
	   * @author PM
	   **/
	function tryAddAddressValidate(&$in)
	  {
	    $v = new validation($in);

	    if($in['customer_id']){
	      $v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
	      if(!$in['primary'] && !$in['delivery'] && !$in['billing'] && !$in['site']){
				$v->field('primary', 'primary', 'required',gm("Main Address is mandatory"));
				$v->field('delivery', 'delivery', 'required',gm("Delivery Address is mandatory"));
				$v->field('billing', 'billing', 'required',gm("Billing Address is mandatory"));
				$v->field('site', 'site', 'required',gm("Site Address is mandatory"));

				$message = 'You must select an address type.';
				$is_ok = $v->run();
				if(!$is_ok){
					msg::error(gm($message),'error');
					return false;		
				}
			} else {
				$v->field('address', 'Address', 'required:text',gm("Street is mandatory"));
				$v->field('zip', 'Zip', 'required:text',gm("Zip Code is mandatory"));
				$v->field('city', 'City', 'required:text',gm("City is mandatory"));
				$v->field('country_id', 'Country', 'required:exist[country.country_id]');
			}
	    }else if($in['contact_id']){
	      $v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
	    }else{
	      msg::error(gm('Invalid ID'),'error');
	      return false;
	    }
	    $v->field('country_id', 'Country', 'required:exist[country.country_id]',gm("Country is mandatory"));

	    return $v->run();
	}

	function updateCustomerData(&$in)
	{	
	    if($in['field'] == 'contact_id'){
	      $in['buyer_id'] ='';
	    }

	    $sql = "UPDATE servicing_support SET ";

	    if($in['buyer_id']){
	      $buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, 
	                  customers.internal_language, customers.line_discount, customers.currency_id, customers.apply_line_disc, customers.acc_manager_name,customers.user_id,
	                  customer_addresses.address_id, customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount
	                  FROM customers
	                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
	                  AND customer_addresses.is_primary=1
	                  WHERE customers.customer_id='".$in['buyer_id']."' ");
	      $buyer_info->next();

      	  //Change account manager only if the customer was changed
          $acc_manager_id = explode(',',$buyer_info->f('user_id'));
	      $acc_manager_name = explode(',',$buyer_info->f('acc_manager_name'));

	      $currentBuyerId = $this->db->query("SELECT customer_id FROM servicing_support WHERE service_id ='".$in['service_id']."'")->getAll();

	      if($currentBuyerId){
	      	$currentBuyerId = $currentBuyerId[0]['customer_id'];
	      		if($currentBuyerId != $in['buyer_id']){
	      			$sql .= " acc_manager_id = '".$acc_manager_id[0]."', ";
	      			$sql .= " acc_manager_name = '".addslashes($acc_manager_name[0])."', ";
	      		}
      	  }
      	  //End Change account manager

	      if($in['service_id'] == 'tmp'){
	        $in['delivery_address_id'] = $buyer_info->f('address_id');
	      }
	        
	      $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
	      $in['contact_name'] ='';
	      
	      //Set email language as account / contact language
		  $emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
	    						  'contact_id' 		=> $in['contact_id'],
	    						  'item_id'			=> $in['service_id'],
	    						  'email_language' 	=> $in['email_language'],
	    						  'table'			=> 'servicing_support',
	    						  'table_label'		=> 'service_id',
	    						  'table_buyer_label' => 'customer_id',
	    						  'table_contact_label' => 'contact_id',
	    						  'param'		 	=> 'update_customer_data');
		  $in['email_language'] = get_email_language($emailMessageData);
	      //End Set email language as account / contact language

	      $sql .= " customer_id = '".$in['buyer_id']."', ";
	      $sql .= " email_language = '".$in['email_language']."', ";
	      $sql .= " customer_name = '".addslashes($buyer_info->f('name'))."', ";	      
	      $sql .= " main_address_id = '".($in['main_address_id'] ? $in['main_address_id'] : $buyer_info->f('address_id'))."', ";
	      $sql .= " installation_id = '".$in['installation_id']."', ";
	      if($in['contact_id']){
	        $contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	        // $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
	        $sql .= " contact_id = '".$in['contact_id']."', ";
	        $in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
	        $sql .= " contact_name = '".addslashes($in['contact_name'])."', ";
	      }else{	        
	        $sql .= " contact_id = '0', ";
	      }
	      $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
	      if($in['main_address_id'] && $buyer_info->f('address_id') != $in['main_address_id']){
	        $buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
	        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
	      }
	      if($in['sameAddress']==1){
	      	if(DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'){
	      		$cost_center="3991000";
	      		$address_zip=$this->db->field("SELECT zip FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
		      	if($address_zip){
		      		$cost_code=$this->db->field("SELECT `code` FROM cost_centre WHERE `zip`='".$address_zip."' ");
		    		if($cost_code){
		    				$cost_center="3".$cost_code."1000";
		    		}
		      	}
		      	$cost_center.="/";
			      if($in['segment_id']){
			      	$segment_id=$this->db->field("SELECT `code` FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
			      	if($segment_id){
			      		$cost_center.=$segment_id;
			      	}else{
			      		$cost_center.="0";
			      	}
			      }else{
			      	$cost_center.="0";
			      }
			      if($in['source_id']){
			      	$source_id=$this->db->field("SELECT `code` FROM tblquote_source WHERE id='".$in['source_id']."' ");
			      	if($source_id){
			      		$cost_center.=$source_id;
			      	}else{
			      		$cost_center.="0";
			      	}
			      }else{
			      	$cost_center.="0";
			      }
			      if($in['type_id']){
			      	$type_id=$this->db->field("SELECT `code` FROM tblquote_type WHERE id='".$in['type_id']."' ");
			      	if($type_id){
			      		$cost_center.=$type_id;
			      	}else{
			      		$cost_center.="0";
			      	}
			      }else{
			      	$cost_center.="0";
			      }
		      	$sql .= " cost_centre = '".$cost_center."', ";
	      	}
	        $sql .= " same_address = '0', ";
	        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
	      }else{
	      	if(DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'){
	      		$cost_center="3991000";
	      		$address_zip=$this->db->field("SELECT zip FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
		    	if($address_zip){
		    		$cost_code=$this->db->field("SELECT `code` FROM cost_centre WHERE `zip`='".$address_zip."' ");
		    		if($cost_code){
		    			$cost_center="3".$cost_code."1000";
		    		}
		    	}
		    	$cost_center.="/";
			      if($in['segment_id']){
			      	$segment_id=$this->db->field("SELECT `code` FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
			      	if($segment_id){
			      		$cost_center.=$segment_id;
			      	}else{
			      		$cost_center.="0";
			      	}
			      }else{
			      	$cost_center.="0";
			      }
			      if($in['source_id']){
			      	$source_id=$this->db->field("SELECT `code` FROM tblquote_source WHERE id='".$in['source_id']."' ");
			      	if($source_id){
			      		$cost_center.=$source_id;
			      	}else{
			      		$cost_center.="0";
			      	}
			      }else{
			      	$cost_center.="0";
			      }
			      if($in['type_id']){
			      	$type_id=$this->db->field("SELECT `code` FROM tblquote_type WHERE id='".$in['type_id']."' ");
			      	if($type_id){
			      		$cost_center.=$type_id;
			      	}else{
			      		$cost_center.="0";
			      	}
			      }else{
			      	$cost_center.="0";
			      }
	      		$sql .= " cost_centre = '".$cost_center."', ";
	      	}
	        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
	        $new_address->next();
	        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
	        $sql .= " same_address = '".$in['delivery_address_id']."', ";
	        $sql .= " free_field = '".addslashes($new_address_txt)."' ";      
	      }	     
	      
	    }else{
	      if(!$in['contact_id']){
	        msg::error ( gm('Please select a company or a contact'),'error');
	        json_out($in);
	      }
	      $contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	      $contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
	      $sql .= " customer_id = '0', ";
	      $sql .= " contact_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
	      $sql .= " contact_id = '".$in['contact_id']."', ";

	      $in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
	      
	      if($in['sameAddress']==1){
	        $sql .= " same_address = '0', ";
	        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
	      }else{
	        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
	        $new_address->next();
	        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
	        $sql .= " same_address = '".$in['delivery_address_id']."', ";
	        $sql .= " free_field = '".addslashes($new_address_txt)."' ";      
	      }

	    }
	    $sql .=" WHERE service_id ='".$in['item_id']."' ";
	    if(!$in['isAdd']){
	      $this->db->query($sql);   
	      if($in['item_id'] && is_numeric($in['item_id'])){
	        $trace_id=$this->db->field("SELECT trace_id FROM servicing_support WHERE service_id='".$in['item_id']."' ");
	        if($trace_id && $in['buyer_id']){
	          $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
	        }
	      }  
	    }
	    $in['service_id'] = $in['item_id'];
	    msg::success(gm('Sync successfull.'),'success');
	    return true;
	}


	/**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddC(&$in){
	    if(!$this->tryAddCValid($in)){ 
	      json_out($in);
	      return false; 
	    }
	    $in['service_id'] = $in['item_id'];
	    //$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
	    $name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	    //Set account default vat on customer creation
	    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	    if(empty($vat_regime_id)){
	      $vat_regime_id = 0;
	    }

	    $c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			/*foreach ($in['c_type'] as $key) {
				if($key){
					$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
					$c_type_name .= $type.',';
				}
			}
			$c_types = implode(',', $in['c_type']);
			$c_type_name = rtrim($c_type_name,',');*/
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']==''){
			$in['user_id']=$_SESSION['u_id'];
		}


		if($in['user_id']){
			/*foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);*/
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				//$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}
		$vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
	        $vat_default=str_replace(',', '.', $vat);
		$selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");


	    if($in['add_customer']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
	        if(SET_DEF_PRICE_CAT == 1){
				$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
			}
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".$in['name']."',
	                                            our_reference = '".$account_reference_number."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            vat_regime_id     = '".$vat_regime_id."',
	                                            city_name = '".$in['city']."',
	                                            c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
	                                            user_id = '".$acc_manager_ids."',
                                            	acc_manager_name = '".addslashes($in['acc_manager'])."',
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            creation_date = '".time()."',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."',
	                                            commercial_name 		= '".$in['commercial_name']."',
												legal_type				= '".$in['legal_type']."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												sector					= '".$in['sector']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$c_type_name."',
												vat_id 					= '".$selected_vat."',
												invoice_email_type		= '1',
												sales_rep				= '".$in['sales_rep_id']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												stripe_cust_id			= '".$in['stripe_cust_id']."',
												cat_id					= '".$in['cat_id']."'");
	      
	        $this->db->query("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1',
	                        site='1' ");
	        $in['customer_name'] = $in['name'];

	        if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
					if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
						$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
					}
				}
			}

	      // include_once('../apps/company/admin/model/customer.php');
	        $in['value']=$in['btw_nr'];
	      // $comp=new customer();       
	      // $this->check_vies_vat_number($in);
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                            name    = :name,
	                            value   = :value ",
	                        ['user_id' => $_SESSION['u_id'],
	                         'name'    => 'company-customers_show_info',
	                         'value'   => '1']
	                    );	                            
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                	
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
	          $this->db->query("UPDATE servicing_support SET 
	            customer_id='".$in['buyer_id']."', 
	            customer_name='".$in['name']."',
	            contact_id='',
	            free_field = '".$address."'
	            WHERE service_id='".$in['item_id']."' ");         
	        }
	        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
	        msg::success (gm('Success'),'success');
	        return true;
	    }
	    if($in['add_individual']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
	        if(SET_DEF_PRICE_CAT == 1){
				$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
			}
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".addslashes($in['lastname'])."',
	                                            firstname = '".addslashes($in['firstname'])."',
	                                            our_reference = '".$account_reference_number."',
	                                            user_id = '".$acc_manager_ids."',
                                            	acc_manager_name = '".addslashes($in['acc_manager'])."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            vat_regime_id     = '".$vat_regime_id."',
	                                            city_name = '".$in['city']."',
	                                            c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
	                                            type = 1,
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            creation_date = '".time()."',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."',
	                                            commercial_name 		= '".$in['commercial_name']."',
												legal_type				= '".$in['legal_type']."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												sector					= '".$in['sector']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$c_type_name."',
												vat_id 					= '".$selected_vat."',
												invoice_email_type		= '1',
												sales_rep				= '".$in['sales_rep_id']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												stripe_cust_id			= '".$in['stripe_cust_id']."',
												cat_id					= '".$in['cat_id']."'");
	      
	        $in['main_address_id'] = $this->db->insert("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1',
	                        site='1' ");
	        $in['customer_name'] = $in['name'];

	        if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
					if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
						$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
					}
				}
			}

	      // include_once('../apps/company/admin/model/customer.php');
	        $in['value']=$in['btw_nr'];
	      // $comp=new customer();       
	      // $this->check_vies_vat_number($in);
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                            name    = :name,
	                            value   = :value ",
	                        ['user_id' => $_SESSION['u_id'],
	                         'name'    => 'company-customers_show_info',
	                         'value'   => '1']
	                    );	                            
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
	          $this->db->query("UPDATE servicing_support SET 
	            customer_id='".$in['buyer_id']."', 
	            customer_name='".$in['name']."',
	            contact_id='',
	            free_field = '".$address."'
	            WHERE service_id='".$in['item_id']."' ");         
	        }
	        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
	        msg::success (gm('Success'),'success');
	        return true;
	    }
	    if($in['add_contact']){
	      $customer_name='';

	      if($in['buyer_id']){
	        $customer_name = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
	      }
	      if($in['birthdate']){
				$in['birthdate'] = strtotime($in['birthdate']);
			}
	        $in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
						customer_id	=	'".$in['buyer_id']."',
						firstname	=	'".$in['firstname']."',
						lastname	=	'".$in['lastname']."',
						email		=	'".$in['email']."',
						birthdate	=	'".$in['birthdate']."',
						cell		=	'".$in['cell']."',
						sex			=	'".$in['sex']."',
						title		=	'".$in['title_contact_id']."',
						language	=	'".$in['language']."',
						company_name=	'".$customer_name."',
						`create`	=	'".time()."'");
	        $this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
			if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				}
			}

		  	if($in['buyer_id']){
				$contact_accounts_sql="";
				$accepted_keys=["position","department","title","e_title","email","phone","fax","s_email","customer_address_id"];
		 		foreach($in['accountRelatedObj'] as $key=>$value){
		 			if(in_array($value['model'],$accepted_keys)){
		 				$contact_accounts_sql.="`".$value['model']."`='".addslashes($value['model_value'])."',";
		 			}
		 		}
		 		if($in['email']){
		 			$contact_accounts_sql.="`email`='".$in['email']."',";
		 		}
		 		if(!empty($contact_accounts_sql)){
		 			$contact_accounts_sql=rtrim($contact_accounts_sql,",");
		 			$this->db->query("INSERT INTO customer_contactsIds SET 
			  								customer_id='".$in['buyer_id']."', 
			  								contact_id='".$in['contact_id']."',
			  								".$contact_accounts_sql." ");
		 		}	
		 	}
	        $in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
	        if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
                $vars=array();
                if($in['buyer_id']){
                    $vars['customer_id']=$in['buyer_id'];
                    $vars['customer_name']=$customer_name;
                }          
                $vars['contact_id']=$in['contact_id'];
                $vars['firstname']=$in['firstname'];
                $vars['lastname']=$in['lastname'];
                $vars['table']='customer_contacts';
                $vars['email']=$in['email'];
                $vars['op']='add';
                synctoZendesk($vars);
              }
	        if($in['country_id']){
	          $this->db->query("INSERT INTO customer_contact_address SET
	                          address='".$in['address']."',
	                          zip='".$in['zip']."',
	                          city='".$in['city']."',
	                          country_id='".$in['country_id']."',
	                          contact_id='".$in['contact_id']."',
	                          is_primary='1',
	                          delivery='1' ");
	        }
	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                    AND name    = 'company-contacts_show_info'  ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                    AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);	                                    
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                                  name    = 'company-contacts_show_info',
	                                  value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                                  name    = :name,
	                                  value   = :value ",
	                                ['user_id' => $_SESSION['u_id'],
	                                 'name'    => 'company-contacts_show_info',
	                                 'value'   => '1']
	                            );	                                  
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                      AND name    = 'company-contacts_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                      AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);	                                      
	        }
	        $count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
	        if($count == 1){
	          doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $this->db->query("UPDATE servicing_support SET contact_id='".$in['contact_id']."' WHERE service_id='".$in['item_id']."' ");  
	        }
	        insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
	        msg::success (gm('Success'),'success');
	    }
	    return true;
	  }

	  /**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddCValid(&$in)
	  {
	    if($in['add_customer']){
	        $v = new validation($in);
	      $v->field('name', 'name', 'required:unique[customers.name]');
	      $v->field('country_id', 'country_id', 'required');
	      return $v->run();  
	    }
	    if($in['add_contact']){
	      $v = new validation($in);
	      $v->field('firstname', 'firstname', 'required');
	      $v->field('lastname', 'lastname', 'required');
	      // $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
	      // $v->field('country_id', 'country_id', 'required');
	      return $v->run();  
	    }
	    return true;
	  }

	function check_vies_vat_number(&$in){
	    $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

	    if(!$in['value'] || $in['value']==''){      
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    $value=trim($in['value']," ");
	    $value=str_replace(" ","",$value);
	    $value=str_replace(".","",$value);
	    $value=strtoupper($value);

	    $vat_numeric=is_numeric(substr($value,0,2));

	    /*if($vat_numeric || substr($value,0,2)=="BE"){
	      $trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
	      $trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
	      $trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

	      if(!$trends_access_token || $trends_expiration<time()){
	        $ch = curl_init();
	        $headers=array(
	          "Content-Type: x-www-form-urlencoded",
	          "Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
	          );
	        
	        $trends_data="grant_type=password&username=akti_api&password=akti_api";

	          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	          curl_setopt($ch, CURLOPT_POST, true);
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
	          curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

	          $put = json_decode(curl_exec($ch));
	        $info = curl_getinfo($ch);

	        if($info['http_code']==400){
	          if(ark::$method == 'check_vies_vat_number'){
	            msg::error ( $put->error,'error');
	            json_out($in);
	          }
	          return false;
	        }else{
	          if(!$trends_access_token){
	            $this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
	            $this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
	            $this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
	          }else{
	            $this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
	            $this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
	            $this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
	          }

	          $ch = curl_init();
	          $headers=array(
	            "Authorization: Bearer ".$put->access_token
	            );
	          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	            if($vat_numeric){
	                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
	            }else{
	                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
	            }
	            $put = json_decode(curl_exec($ch));
	          $info = curl_getinfo($ch);

	            if($info['http_code']==400 || $info['http_code']==429){
	              if(ark::$method == 'check_vies_vat_number'){
	              msg::error ( $put->error,'error');
	              json_out($in);
	            }
	            return false;
	          }else if($info['http_code']==404){
	            if($vat_numeric){
	              if(ark::$method == 'check_vies_vat_number'){
	                msg::error ( gm("Not a valid vat number"),'error');
	                json_out($in);
	              }
	              return false;
	            }
	          }else{
	            if($in['customer_id']){
	              $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	              if($country_id != 26){
	                $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
	                $in['remove_v']=1;
	              }else if($country_id == 26){
	                $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
	                $in['add_v']=1;
	              }
	            }
	            $in['comp_name']=$put->officialName;
	            $in['comp_address']=$put->street.' '.$put->houseNumber;
	            $in['comp_zip']=$put->postalCode;
	            $in['comp_city']=$put->city;
	            $in['comp_country']='26';
	            $in['trends_ok']=true;
	            $in['trends_lang']=$_SESSION['l'];
	            $in['full_details']=$put;
	            if(ark::$method == 'check_vies_vat_number'){
	              msg::success(gm('Success'),'success');
	              json_out($in);
	            }
	            return false;
	          }
	        }
	      }else{
	        $ch = curl_init();
	        $headers=array(
	          "Authorization: Bearer ".$trends_access_token
	          );
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	          if($vat_numeric){
	              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
	          }else{
	              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
	          }
	          $put = json_decode(curl_exec($ch));
	        $info = curl_getinfo($ch);

	          if($info['http_code']==400 || $info['http_code']==429){
	            if(ark::$method == 'check_vies_vat_number'){
	            msg::error ( $put->error,'error');
	            json_out($in);
	          }
	          return false;
	        }else if($info['http_code']==404){
	          if($vat_numeric){
	            if(ark::$method == 'check_vies_vat_number'){
	              msg::error (gm("Not a valid vat number"),'error');
	              json_out($in);
	            }
	            return false;
	          }
	        }else{
	          if($in['customer_id']){
	            $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	            if($country_id != 26){
	              $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
	              $in['remove_v']=1;
	            }else if($country_id == 26){
	              $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
	              $in['add_v']=1;
	            }
	          }
	          $in['comp_name']=$put->officialName;
	          $in['comp_address']=$put->street.' '.$put->houseNumber;
	          $in['comp_zip']=$put->postalCode;
	          $in['comp_city']=$put->city;
	          $in['comp_country']='26';
	          $in['trends_ok']=true;
	          $in['trends_lang']=$_SESSION['l'];
	          $in['full_details']=$put;
	          if(ark::$method == 'check_vies_vat_number'){
	            msg::success(gm('Success'),'success');
	            json_out($in);
	          }
	          return false;
	        }
	      }
	    }*/


	    if(!in_array(substr($value,0,2), $eu_countries)){
	      $value='BE'.$value;
	    }
	    if(in_array(substr($value,0,2), $eu_countries)){
	      $search   = array(" ", ".");
	      $vat = str_replace($search, "", $value);
	      $_GET['a'] = substr($vat,0,2);
	      $_GET['b'] = substr($vat,2);
	      $_GET['c'] = '1';
	      $dd = include('../valid_vat.php');
	    }else{
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    if(isset($response) && $response == 'invalid'){
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }else if(isset($response) && $response == 'error'){
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }else if(isset($response) && $response == 'valid'){
	      $full_address=explode("\n",$result->address);
	      switch($result->countryCode){
	        case "RO":
	          $in['comp_address']=$full_address[1];
	          $in['comp_city']=$full_address[0];
	          $in['comp_zip']=" ";
	          break;
	        case "NL":
	          $zip=explode(" ",$full_address[2],2);
	          $in['comp_address']=$full_address[1];
	          $in['comp_zip']=$zip[0];
	          $in['comp_city']=$zip[1];
	          break;
	        default:
	          $zip=explode(" ",$full_address[1],2);
	          $in['comp_address']=$full_address[0];
	          $in['comp_zip']=$zip[0];
	          $in['comp_city']=$zip[1];
	          break;
	      }

	      $in['comp_name']=$result->name;

	      $in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
	      $in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
	      $in['full_details']=$result;
	      $in['vies_ok']=1;
	      if($in['customer_id']){
	        $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	        if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
	      }
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::success ( gm('VAT Number is valid'),'success');
	        json_out($in);
	      }
	      return true;
	    }else{
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    if(ark::$method == 'check_vies_vat_number'){
	      json_out($in);
	    }
	  }

	function update_article_delivered(&$in)
	{

		/*if(!$this->article_delivery_validation($in)){
			msg::error(gm('Total quantity lower than quantity selected'),"error");
			json_out($in);
		}*/
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$in['delivery_id'] = $this->db->insert("INSERT INTO service_deliveries
			                      SET service_id='".$in['service_id']."',
			                         `date` = '".$in['date_del_line_h']."',
                                     `hour` = '".$in['hour']."',
                                     `minute` = '".$in['minute']."',
                                     `pm` = '".$in['pm']."',
                                      contact_name 	= '".$in['contact_name']."',
		                              contact_id	= '".$in['contact_id']."',
		                              delivery_address	= '".$in['delivery_address']."',
		                              delivery_done 	= '1'");
		$articles_ids=array();
		if($in['lines_reserved']){
			foreach($in['lines_reserved'] as $key=>$value){
				array_push($articles_ids,$value['article_id']);
				$already_delivered=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$value['article_id']."' ");
				if(!$already_delivered){
					$already_delivered=0;
				}
				//$totalQ=$this->db->field("SELECT quantity FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='".$value['article_id']."' ");
				$totalQ=$this->db->field("SELECT SUM(quantity) FROM service_reservation WHERE service_id='".$in['service_id']."' AND a_id='".$value['article_id']."'");
				if(return_value($value['quantity'])+$already_delivered>=$totalQ){
					$this->db->query("UPDATE servicing_support_articles SET article_delivered='1', quantity='".(return_value($value['quantity'])+$already_delivered)."' WHERE service_id='".$in['service_id']."' AND article_id='".$value['article_id']."' ");
				}
				$in['line']=$value;
				$this->make_delivery($in);
			}
		}
		foreach($in['lines'] as $key=>$value){
			array_push($articles_ids,$value['article_id']);
			$already_delivered=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['service_id']."' AND a_id='".$value['article_id']."' ");
			if(!$already_delivered){
				$already_delivered=0;
			}
			$totalQ=$this->db->field("SELECT quantity FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='".$value['article_id']."' ");
			if(return_value($value['quantity'])+$already_delivered>=$totalQ){
				$this->db->query("UPDATE servicing_support_articles SET article_delivered='1', quantity='".(return_value($value['quantity'])+$already_delivered)."' WHERE service_id='".$in['service_id']."' AND article_id='".$value['article_id']."' ");
			}
			$in['line']=$value;
			$this->make_delivery($in);
		}
		if(WAC){
		  foreach ($articles_ids as $key => $value) {
		    generate_purchase_price($value);
		  }
		}
		/*if($in['multiple']){
			$this->db->query("UPDATE servicing_support_articles SET article_delivered='".$in['delivered']."', quantity='".return_value($in['quantity_delivered'])."', delivery_note='".$in['delivery_note']."', delivery_date='".time()."' WHERE a_id='".$in['a_id']."' ");
		}else{
			$this->db->query("UPDATE servicing_support_articles SET article_delivered='".$in['delivered']."', delivery_note='".$in['delivery_note']."', delivery_date='".time()."' WHERE a_id='".$in['a_id']."' ");
		}	
		$article_data=$this->db->query("SELECT servicing_support_articles.article_id,servicing_support_articles.service_id,servicing_support_articles.quantity,servicing_support.serial_number FROM servicing_support_articles 
			INNER JOIN servicing_support ON servicing_support_articles.service_id=servicing_support.service_id
			WHERE servicing_support_articles.a_id='".$in['a_id']."'");
		$in['article_id']=$article_data->f('article_id');
		$in['service_id']=$article_data->f('service_id');
		$in['serial_number']=$article_data->f('serial_number');
		$in['quantity']=$article_data->f('quantity');
		$this->make_delivery($in);
		if(WAC){
              generate_purchase_price($in['article_id']);
            }*/
		msg::success(gm('Delivery made'),"success");
		return true;
	}

	function article_delivery_validation(&$in)
	{
		$is_ok=true;
		//$quantity=$this->db->field("SELECT quantity FROM servicing_support_articles WHERE a_id='".$in['a_id']."'");
		foreach($in['lines'] as $key => $value){
			if($value['is_error']=='1'){
				$is_ok=false;
			}
		}
		foreach($in['lines_reserved'] as $key => $value){
			if($value['is_error']=='1'){
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function undo_article_delivered(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM servicing_support_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			msg::error(gm('Cannot undeliver billed article'),"error");
			json_out($in);
		}
		$delivered=$this->db->field("SELECT article_delivered FROM servicing_support_articles WHERE a_id='".$in['a_id']."' ");
		if($delivered){
			$this->db->query("UPDATE servicing_support_articles SET article_delivered='".$in['delivered']."', delivery_note='' WHERE a_id='".$in['a_id']."' ");
			$article_data=$this->db->query("SELECT article_id,service_id,quantity FROM servicing_support_articles WHERE a_id='".$in['a_id']."'");
			$in['article_id']=$article_data->f('article_id');
			$in['service_id']=$article_data->f('service_id');
			$in['quantity']=$article_data->f('quantity');
			$this->undo_delivery($in);
			if(WAC){
	              generate_purchase_price($in['article_id']);
	            }
			msg::success(gm('Changes saved'),"success");
			return true;
		}
	}

	function make_delivery(&$in)
	{
		$const =array();
		$const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
		$const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

		$now = time();
		//foreach ($in['quantity'] as $key => $value) {
			//if(return_value($value) > 0){
			if($in['line']['quantity'] > 0) {
				$hide_stock = $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$in['line']['article_id']."'");
				if($const['ALLOW_STOCK'] && $hide_stock==0 ){

					$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$in['line']['article_id']."' ");
					$article_data->next();
					$stock = $article_data->f("stock");

					$stock_packing = $article_data->f("packing");
					if(!$const['ALLOW_ARTICLE_PACKING']){
						$stock_packing = 1;
					}
					$stock_sale_unit = $article_data->f("sale_unit");
					if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
						$stock_sale_unit = 1;
					}

					$new_stock = $stock - (return_value($in['line']['quantity'])*$stock_packing/$stock_sale_unit);

					$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['line']['article_id']."'");

					/*start for  stock movements*/

					if($in['line']['disp_addres_info']){
						$locations=array();
						foreach($in['line']['disp_addres_info'] as $key => $value){
							$locations[$value['customer_id'].'-'.$value['address_id']]=return_value($value['quantity_delivered']);
						}
					}
					$this->db->insert("INSERT INTO stock_movements SET
							date 						=	'".$now."',
							created_by 					=	'".$_SESSION['u_id']."',
							service_id 					= 	'".$in['service_id']."',
							article_id 					=	'".$in['line']['article_id']."',
							article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
							item_code  					= 	'".addslashes($article_data->f("item_code"))."',
							movement_type				=	'17',
							quantity 					= 	'".(return_value($in['line']['quantity'])*$stock_packing/$stock_sale_unit)."',
							stock 						=	'".$stock."',
							new_stock 					= 	'".$new_stock."',
							backorder_articles 			= 	'0',
						      location_info                =   '".serialize($locations)."',
						      delivery_id                 =    '".$in['delivery_id']."',
						      delivery_date 			='".$in['date_del_line_h']."'
				        ");
					/*end for  stock movements*/
				}

			}
		//}
		if($in['line']['disp_addres_info']){
                 	foreach ($in['line']['disp_addres_info'] as $key => $value) {
                 		$dispatch_quantity=return_value($value['quantity_delivered']);
                 		$from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$in['line']['article_id']."' AND  address_id='".$value['address_id']."' and customer_id='".$value['customer_id']."'");                  
	                  $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -($dispatch_quantity*($stock_packing/$stock_sale_unit )))."' WHERE article_id='".$in['line']['article_id']."'   AND  address_id='".$value['address_id']."' and customer_id='".$value['customer_id']."'");
                 	}
            }
            if($in['line']['selected_s_n']!=''){
            	$in['line']['selected_s_n']=rtrim($in['line']['selected_s_n'],',');
            	$selected_serials=explode(',', $in['line']['selected_s_n']);
            	foreach($selected_serials as $key_serial=>$value_serial){
            		$this->db->query("UPDATE serial_numbers
										SET 		status_id 	= '2',
												status_details_2 	= '".$in['serial_number']."',
												date_out 		= '".time()."',
												service_id 	      ='".$in['service_id']."',
												delivery_id 	= '".$in['delivery_id']."'
										WHERE id = '".$value_serial."' ");
            	}
            }
            if(return_value($in['line']['quantity'])){
            	$this->db->query("INSERT INTO service_delivery SET service_id='".$in['service_id']."' , a_id='".$in['line']['article_id']."', quantity='".return_value($in['line']['quantity'])."', delivery_id='".$in['delivery_id']."', delivery_note='".$in['delivery_note']."', from_reserved='".$in['line']['is_reserved']."' ");
            }
            
		insert_message_log($this->pag,'{l}Article delivered by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id']);
		return true;
	}
	function undo_delivery(&$in){

		$const=array();
		$const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
		$const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

		$hide_stock = $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$in['article_id']."'");
		if($const['ALLOW_STOCK'] && $hide_stock==0 ){

			$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$in['article_id']."' ");
			$article_data->next();
			$stock = $article_data->f("stock");

			$stock_packing = $article_data->f("packing");
			if(!$const['ALLOW_ARTICLE_PACKING']){
				$stock_packing = 1;
			}
			$stock_sale_unit = $article_data->f("sale_unit");
			if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
				$stock_sale_unit = 1;
			}

			$new_stock = $stock + ($in['quantity']*$stock_packing/$stock_sale_unit);

			$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id']."'");

			/*start for  stock movements*/

			$stock_move=$this->db->query("SELECT location_info,movement_id FROM stock_movements WHERE service_id='".$in['service_id']."' AND article_id='".$in['article_id']."' AND delivery_id='".$in['delivery_id']."' ");
			if($stock_move->f('location_info') !=''){
	        	      $location=unserialize($stock_move->f('location_info'));
	        	      foreach ($location as $key => $value) {
	        	           	if(return_value($value)>0){
	        	           	      $info=explode("-", $key);
                                    $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$in['article_id']."' AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
                                    $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock+return_value($value)*$stock_packing/$stock_sale_unit)."' WHERE article_id='".$in['article_id']."'   AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
	        	           	}
	        	      }
			}
			$this->db->query("DELETE FROM stock_movements WHERE movement_id='".$stock_move->f('movement_id')."'");
			/*end for  stock movements*/

			$this->db->query("UPDATE serial_numbers
										SET 		status_id 	= '1',
												status_details_2 	= '',
												date_out 		= '0'
										WHERE service_id='".$in['service_id']."' AND article_id='".$in['article_id']."' AND delivery_id='".$in['delivery_id']."'");
		}
	}

	function edit_delivery(&$in){
		$now = time();
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$this->db->query("UPDATE service_deliveries
		                  SET 	`date` = '".$in['date_del_line_h']."',
                                    `hour` = '".$in['hour']."',
                                    `minute` = '".$in['minute']."',
                                    `pm` = '".$in['pm']."',
                                    contact_name 	= '".$in['contact_name']."',
		                        contact_id	= '".$in['contact_id']."',
		                        delivery_address	= '".$in['delivery_address']."'
                              WHERE service_id='".$in['service_id']."' and delivery_id='".$in['delivery_id']."' ");
		$this->db->query("UPDATE 	service_delivery
		                 	SET   	`delivery_note` = '".$in['delivery_note']."'
                              WHERE     	service_id='".$in['service_id']."' and delivery_id='".$in['delivery_id']."' ");
		msg::success(gm('Changes saved'),"success");
		if(!$in['xview']){
			json_out($in);
		}	
	}

	function delete_delivery(&$in){
		$err=0;
		$articles_ids=array();
		$articles = $this->db->query("SELECT * FROM service_delivery WHERE service_id ='".$in['service_id']."' AND delivery_id='".$in['delivery_id']."' ");
		while($articles->next()){
			if($articles->f('invoiced')){
				msg::error(gm('Some billed articles could not be reverted'),"error");
				$err++;
				continue;
			}		
			array_push($articles_ids,$articles->f('a_id'));
			$this->db->query("UPDATE servicing_support_articles SET article_delivered='0' WHERE article_id='".$articles->f('a_id')."' AND service_id='".$in['service_id']."' ");
			$in['article_id']=$articles->f('a_id');
			$in['quantity']=$articles->f('quantity');
			$this->undo_delivery($in);
			$this->db->query("DELETE FROM service_delivery WHERE id='".$articles->f('id')."' ");
		}	
		if(WAC){
		     	foreach ($articles_ids as $key => $value) {
		        generate_purchase_price($value);
		      }
 		}
 		if($err == 0){
 			$this->db->query("DELETE FROM service_deliveries WHERE service_id ='".$in['service_id']."' AND delivery_id='".$in['delivery_id']."'");
 		}
 		msg::success(gm("Changes saved"),"success");
 		return true;	
	}

	function serviceAddArticle(&$in)
	{
		if(!$this->serviceAddArticle_validate($in)){
			json_out($in);
		}
		$unique_art=$this->db->field("SELECT a_id FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='".$in['article_id']."' ");
		if($unique_art){
			msg::error(gm('Article already added'),"error");
			json_out($in);
		}
		$last_sort_order=$this->db->field("SELECT sort_order FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id!='0' ORDER BY sort_order DESC LIMIT 1");
		if(!$last_sort_order){
			$last_sort_order=0;
		}
		$margin_nr=$in['sell_price']-$in['purchase_price'];
		$margin_percent=($in['sell_price']-$in['purchase_price'])/$in['sell_price']*100;
		$in['a_id'] = $this->db->insert("INSERT INTO servicing_support_articles SET service_id='".$in['service_id']."',
									name='".$in['name']."',
									article_id='".$in['article_id']."',
									quantity='".($in['quantity'] ? $in['quantity'] : '1')."',
									price='".$in['sell_price']."',
									purchase_price='".$in['purchase_price']."',
									margin='".$margin_nr."',
									margin_per='".$margin_percent."',
									billable='1',
									sort_order='".($last_sort_order+1)."' ");
		$in['name'] = stripslashes($in['name']);
		msg::success(gm('Changes saved'),"success");
		if(ark::$method=='serviceAddArticle'){
			# for pdf
			$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
	    /*$params = array();
	    $params['service_id'] = $in['service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	  }
		return true;
	}

	function serviceAddArticle_validate(&$in)
	{
		$v = new validation($in);
		$v->f('name', 'name', 'required');
		$v->f('service_id', 'service_id', 'required');
		$v->f('article_id', 'article_id', 'required');
		return $v->run();
	}

	function duplicate(&$in)
	{
		if(!$this->duplicate_validate($in)){
			json_out($in);
		}
		$in['serial_number'] = generate_service_number();
		$in['service_id'] = $this->db->insert("INSERT INTO servicing_support SET
										service_type		='".$this->service->f('service_type')."',
										customer_id			='".$this->service->f('customer_id')."',
										contact_id 			='".$this->service->f('contact_id')."',
										project_id 			='".$this->service->f('project_id')."',						
										task_id 			='".$this->service->f('task_id')."',					
										subject 			='".addslashes($this->service->f('subject'))."',
										planeddate 			='',
										date 				='',
										startdate 			='',
										enddate 			='',
										duration 			='',
										report 			='".addslashes($this->service->f('report'))."',
										type 				='".$this->service->f('type')."',
										customer_name  		='".addslashes($this->service->f('customer_name'))."',
										contact_name		='".addslashes($this->service->f('contact_name'))."',
										project_name		='".addslashes($this->service->f('project_name'))."',
										task_name			='".addslashes($this->service->f('task_name'))."',
										contract_name		='".addslashes($this->service->f('contract_name'))."',
										user_name			='".addslashes($this->service->f('user_name'))."',
										serial_number  		='".addslashes($in['serial_number']) ."',
										user_id 			='".$_SESSION['u_id']."',
										billable                ='".$this->service->f('billable')."',
										rate 				='".$this->service->f('rate')."',
										price 			='".$this->service->f('price')."',
										active 			='1',
										email_language		='".$this->service->f('email_language')."',
										free_field 			='".$this->service->f('free_field')."', 
										identity_id 		='".$this->service->f('identity_id')."',
										same_address		='".$this->service->f('same_address')."',
										main_address_id			='".$this->service->f('main_address_id')."',
										service_ctype 			= '".$this->service->f('service_ctype')."',
										vat_regime_id 			='".$this->service->f('vat_regime_id')."',
										source_id 				='".$this->service->f('source_id')."',
                                      				type_id 				='".$this->service->f('type_id')."',
                                      				cost_centre 			='".$this->service->f('cost_centre')."',
                                      				segment_id 				='".$this->service->f('segment_id')."'");

		$u =$this->db->query("SELECT * FROM servicing_support_users WHERE service_id ='".$in['d_service_id']."' ");
		while ($u->next()) {
			$in['user_id'] = $u->f('user_id');
			$in['name'] = get_user_name($in['user_id']);
			$in['value'] = 1;
			$this->addServiceUser($in);
			if($u->f('pr_m')){
				$in['user_id'] = $u->f('user_id');
				$this->serviceSetPrM($in);
			}
		}
		if($in['toTasks']){
			$t = $this->db->query("SELECT * FROM servicing_support_tasks WHERE service_id = '".$in['d_service_id']."' AND closed='0' ");
			while ($t->next()) {
				$in['task_budget']=$t->f('task_budget');
				$in['task_name'] = $t->f('task_name');
				if($t->f('article_id')){
					$in['task_service_id']=$t->f('article_id');
					$in['quantity']=$t->f('quantity');
					$this->serviceAddService($in);
				}else{
					$this->serviceAddTask($in);
				}
			}
		}
		$in['task_name'] = $this->service->f('task_name');
		if($in['toSupply']){
			$t = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id = '".$in['d_service_id']."' AND article_id='0' AND quantity>delivered ");
			while ($t->next()) {
				$this->db->insert("INSERT INTO servicing_support_articles SET
					service_id='".$in['service_id']."',
					name='".addslashes($t->f('name'))."',
					article_id='".$t->f('article_id')."',
					quantity='".($t->f('quantity')-$t->f('delivered'))."',
					price='".$t->f('price')."',
					billable='".$t->f('billable')."',
					margin='".$t->f('margin')."' ");
			}
		}
		if($in['toArt']){
			$art = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id = '".$in['d_service_id']."' AND article_id!='0' ");
			while ($art->next()) {
				/*$already_inv=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['d_service_id']."' AND a_id='".$art->f('article_id')."' AND invoiced='1'");
				if(!$already_inv){
					$already_inv=0;
				}*/
				$already_del=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['d_service_id']."' AND a_id='".$art->f('article_id')."' ");
				if(!$already_del){
					$already_del=0;
				}
				if($art->f('quantity')==$already_del){
					continue;
				}
				$this->db->insert("INSERT INTO servicing_support_articles SET
					service_id='".$in['service_id']."',
					name='".addslashes($art->f('name'))."',
					article_id='".$art->f('article_id')."',
					quantity='".($art->f('quantity')-(int)$already_del)."',
					price='".$art->f('price')."',
					margin='".$art->f('margin')."',
					margin_per='".$art->f('margin_per')."',
					billable='".$art->f('billable')."',
					purchase_price='".$art->f('purchase_price')."',
					has_variants          = '".$art->f('has_variants')."',
                    has_variants_done     ='".$art->f('has_variants_done')."',
                    is_variant_for        ='".$art->f('is_variant_for')."',
                    is_variant_for_line   ='".$art->f('is_variant_for_line')."',
                    variant_type            ='".$art->f('variant_type')."',
                    is_combined ='".$art->f('is_combined')."',
                    component_for='".$art->f('component_for')."'");
			}
		}
		$tracking_data=array(
		      'target_id'         => $in['service_id'],
		      'target_type'       => '5',
		      'target_buyer_id'   => $this->service->f('customer_id'),
		      'lines'             => array(
		                              array('origin_id'=>$in['d_service_id'],'origin_type'=>'5')
		                         )
		);
		addTracking($tracking_data);
		if(ark::$method=='updateServiceTaskName'){
			# for pdf
			$inv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['d_service_id']."' ");
	    /*$params = array();
	    $params['service_id'] = $in['d_service_id'];
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);*/
	    # for pdf
	 	 }

		json_out($in);
	}

	function duplicate_validate(&$in)
	{
		if(!$in['d_service_id']){
			msg::error(gm('Invalid ID'),"error");
			json_out($in);
		}
		$this->service = $this->db->query("SELECT * FROM servicing_support WHERE service_id = '".$in['d_service_id']."' ");
		if(!$this->service->next()){
			msg::error(gm('Invalid ID'),"error");
			json_out($in);
		}
		$in['toTasks'] = $this->db->field("SELECT count(task_id) FROM servicing_support_tasks WHERE service_id = '".$in['d_service_id']."' AND closed='0' ");
		$in['toArt']=0;
		$articles=$this->db->query("SELECT article_id,quantity FROM servicing_support_articles WHERE service_id = '".$in['d_service_id']."' AND article_id!='0'");
		while($articles->next()){
			/*$already_invoiced=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['d_service_id']."' AND a_id='".$articles->f('article_id')."' AND invoiced='1' ");
			if(!$already_invoiced){
				$already_invoiced=0;
			}
			$in['toArt']+=($articles->f('quantity')-(int)$already_invoiced);*/
			$already_delivered=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE service_id='".$in['d_service_id']."' AND a_id='".$articles->f('article_id')."' ");
			if(!$already_delivered){
				$already_delivered=0;
			}
			$in['toArt']+=($articles->f('quantity')-(int)$already_delivered);
		}
		$in['toSupply'] = $this->db->field("SELECT count(a_id) FROM servicing_support_articles WHERE service_id = '".$in['d_service_id']."' AND quantity>delivered AND article_id='0' ");
		if($in['toTasks']> 0 || $in['toArt'] > 0 || $in['toSupply'] > 0){
			return true;
		}

		msg::error(gm('Invalid ID'),"error");
		json_out($in);
	}

	function add_quote_data(&$in){
		foreach ($in['quote_group_data'] as $key => $value) {
			if($value['checked']){
				$group_ids.= $value['group_id'].',';
			}				
		}
		$group_ids = rtrim($group_ids,',');
		$t_amount = 0;
		if($in['quote_group_data'] && !empty($in['quote_group_data']) && !empty($group_ids)){
			$lines = $this->db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
											   INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
											   LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
											   WHERE tblquote_line.quote_id='".$in['quote_id']."'
											   AND tblquote_line.content_type='1'
											   AND tblquote_version.active='1'
											   AND tblquote_line.line_type<'3'
											   AND tblquote_line.article_id='0'
											   AND tblquote_line.is_tax='0'
											   AND tblquote_line.group_id IN(".$group_ids.")
											   ORDER BY tblquote_line.group_id ASC, id ASC  ");

					while ($lines->next()) {
						$text = str_replace(array("\r\n", "\r"), "\n", $lines->f('name'));
						$first_line = explode("\n", $text);
						//$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($first_line[0])."',task_budget='".$lines->f('amount')."' ");
						$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($first_line[0])."',task_budget='".($in['service_type'] == '1' ? ($lines->f('quantity')*$lines->f('price')) : $lines->f('price'))."' ");
						$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
						$i++;
						$t_amount += $lines->f('amount');
					}
		}

		foreach ($in['quote_group_services'] as $key => $value) {
			if($value['checked']){
				$group_srvs.= $value['group_a_id'].',';
			}				
		}
		$group_srvs = rtrim($group_srvs,',');
		if($in['quote_group_services'] && !empty($in['quote_group_services']) && !empty($group_srvs)){
			$lines = $this->db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
											   INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
											   LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
											   WHERE tblquote_line.quote_id='".$in['quote_id']."'
											   AND tblquote_line.content_type='1'
											   AND tblquote_version.active='1'
											   AND tblquote_line.line_type<'3'
											   AND (tblquote_line.article_id>'0' AND pim_articles.is_service='1')
											   AND tblquote_line.is_tax='0'
											   AND tblquote_line.group_id IN(".$group_srvs.")
											   ORDER BY tblquote_line.group_id ASC, id ASC  ");

					while ($lines->next()) {
						$text = str_replace(array("\r\n", "\r"), "\n", $lines->f('name'));
						$first_line = explode("\n", $text);
						//$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($first_line[0])."',task_budget='".$lines->f('amount')."',quantity='".$lines->f('quantity')."',article_id='".$lines->f('article_id')."' ");
						$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($first_line[0])."',task_budget='".$lines->f('price')."',quantity='".$lines->f('quantity')."',article_id='".$lines->f('article_id')."' ");
						$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
						$i++;
					}
		}

		foreach ($in['quote_group_articles'] as $key => $value) {
			if($value['checked']){
				$group_arts.= $value['group_a_id'].',';
			}		 		
		}
		$group_arts = rtrim($group_arts,',');
		if($in['quote_group_articles'] && !empty($in['quote_group_articles']) && !empty($group_arts)){
					$articles = $this->db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
									INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
									LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
									WHERE tblquote_line.quote_id='".$in['quote_id']."'
									AND tblquote_line.content_type='1'
									AND tblquote_line.line_type<'3'
									AND tblquote_version.active='1'
									AND (tblquote_line.article_id>'0' AND pim_articles.is_service='0' AND tblquote_line.is_tax='0')
									AND tblquote_line.group_id IN(".$group_arts.")
									ORDER BY tblquote_line.group_id ASC, id ASC ");
					while($articles->next()){
					 	$purchase_price=$this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$articles->f('article_id')."' AND base_price='1' ");
					 	$margin_nr=$articles->f('price')-$purchase_price;
					 	$margin_percent=($articles->f('price')-$purchase_price)/$articles->f('price')*100;
					 	$this->db->query("INSERT INTO servicing_support_articles SET 
					 				service_id='".$in['service_id']."',
									name='".addslashes($articles->f('name'))."',
									article_id='".$articles->f('article_id')."',
									quantity='".$articles->f('quantity')."',
									price='".$articles->f('price')."',
									purchase_price='".$purchase_price."',
									margin='".$margin_nr."',
									margin_per='".$margin_percent."',
									billable='1' ");
					 }
		}
		return true;
	}

	function add_contract_data(&$in){
		foreach ($in['quote_group_data'] as $key => $value) {
			if($value['checked']){
				$group_ids.= $value['group_id'].',';
			}				
		}
		$group_ids = rtrim($group_ids,',');
		$t_amount = 0;
		if($in['quote_group_data'] && !empty($in['quote_group_data']) && !empty($group_ids)){
			$lines = $this->db->query("SELECT contract_line.*, contracts_version.active FROM contract_line
											   INNER JOIN contracts_version ON contract_line.version_id = contracts_version.version_id
											   LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
											   WHERE contract_line.contract_id='".$in['contract_id']."'
											   AND contract_line.content_type='1'
											   AND contracts_version.active='1'
											   AND contract_line.line_type<'3'
											   AND contract_line.article_id='0'
											   AND contract_line.is_tax='0'
											   AND contract_line.group_id IN(".$group_ids.")
											   ORDER BY contract_line.group_id ASC, id ASC  ");

					while ($lines->next()) {
						$text = str_replace(array("\r\n", "\r"), "\n", $lines->f('name'));
						$first_line = explode("\n", $text);
						$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($first_line[0])."',task_budget='".$lines->f('amount')."' ");
						$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
						$i++;
						$t_amount += $lines->f('amount');
					}
		}

		foreach ($in['quote_group_services'] as $key => $value) {
			if($value['checked']){
				$group_srvs.= $value['group_a_id'].',';
			}				
		}
		$group_srvs = rtrim($group_srvs,',');
		if($in['quote_group_services'] && !empty($in['quote_group_services']) && !empty($group_srvs)){
			$lines = $this->db->query("SELECT contract_line.*, contracts_version.active FROM contract_line
											   INNER JOIN contracts_version ON contract_line.version_id = contracts_version.version_id
											   LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
											   WHERE contract_line.contract_id='".$in['contract_id']."'
											   AND contract_line.content_type='1'
											   AND contracts_version.active='1'
											   AND contract_line.line_type<'3'
											   AND (contract_line.article_id>'0' AND pim_articles.is_service='1')
											   AND contract_line.is_tax='0'
											   AND contract_line.group_id IN(".$group_srvs.")
											   ORDER BY contract_line.group_id ASC, id ASC  ");

					while ($lines->next()) {
						$text = str_replace(array("\r\n", "\r"), "\n", $lines->f('name'));
						$first_line = explode("\n", $text);
						$this->db->query("INSERT INTO servicing_support_tasks SET service_id='".$in['service_id']."', task_name='".addslashes($first_line[0])."',task_budget='".$lines->f('amount')."',quantity='".$lines->f('quantity')."',article_id='".$lines->f('article_id')."' ");
						$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['service_id']."'");
						$i++;
					}
		}

		foreach ($in['quote_group_articles'] as $key => $value) {
			if($value['checked']){
				$group_arts.= $value['group_a_id'].',';
			}		 		
		}
		$group_arts = rtrim($group_arts,',');
		if($in['quote_group_articles'] && !empty($in['quote_group_articles']) && !empty($group_arts)){
					$articles = $this->db->query("SELECT contract_line.*, contracts_version.active FROM contract_line
									INNER JOIN contracts_version ON contract_line.version_id = contracts_version.version_id
									LEFT JOIN pim_articles ON contract_line.article_id=pim_articles.article_id
									WHERE contract_line.contract_id='".$in['contract_id']."'
									AND contract_line.content_type='1'
									AND contract_line.line_type<'3'
									AND contracts_version.active='1'
									AND (contract_line.article_id>'0' AND pim_articles.is_service='0')
									AND contract_line.group_id IN(".$group_arts.")
									ORDER BY contract_line.group_id ASC, id ASC ");
					while($articles->next()){
					 	$purchase_price=$this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$articles->f('article_id')."' AND base_price='1' ");
					 	$margin_nr=$articles->f('price')-$purchase_price;
					 	$margin_percent=($articles->f('price')-$purchase_price)/$articles->f('price')*100;
					 	$this->db->query("INSERT INTO servicing_support_articles SET 
					 				service_id='".$in['service_id']."',
									name='".addslashes($articles->f('name'))."',
									article_id='".$articles->f('article_id')."',
									quantity='".$articles->f('quantity')."',
									price='".$articles->f('price')."',
									purchase_price='".$purchase_price."',
									margin='".$margin_nr."',
									margin_per='".$margin_percent."',
									billable='1' ");
					 }
		}
		return true;
	}

	function updateServiceType(&$in){
		$this->db->query("UPDATE servicing_support SET service_type='".$in['service_type']."',billable='1' WHERE service_id='".$in['service_id']."' ");
		msg::success(gm('Changes saved'),"success");
		return true;
	}
	
	function updateArticleBillable(&$in){
		$this->db->query("UPDATE servicing_support_articles SET billable='".$in['billable']."' WHERE a_id='".$in['a_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function setServiceBillable(&$in){
		$this->db->query("UPDATE servicing_support SET billable='0' WHERE service_id='".$in['service_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}
	function setSubjectRate(&$in){
		$this->db->query("UPDATE servicing_support SET subject='".$in['subject']."',rate='".return_value($in['rate'])."' WHERE service_id='".$in['service_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}
	function downpaymentInvoice_validate(&$in){
		$is_ok=true;
		if($in['downpayment'] && !$in['downpayment_type']){
			msg::error( gm("Please select downpayment value"),'error');
			$is_ok=false;
		}
		if($in['downpayment'] && $in['downpayment_type']){
			if(($in['service_type']=='0' || $in['service_type']=='2') && !return_value($in['downpayment_fixed'])){
				msg::error( gm("Please select downpayment value"),'error');
				$is_ok=false;
			}else if($in['service_type']=='1' && !return_value($in['downpayment_fixed']) && !return_value($in['downpayment_percent'])){
				msg::error( gm("Please select downpayment value"),'error');
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function downpaymentInvoice(&$in){
		if(!$this->downpaymentInvoice_validate($in)){
			json_out($in);
			return false;
		}
		$service_data=$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ");
		//if($in['downpayment_type'] == '1'){
			$price=return_value($in['downpayment_fixed']);
		/*}else{
			$service_price=$service_data->f('price');
			$price=$service_price*return_value($in['downpayment_percent'])/100;
		}*/
		
		$buyer_details = $this->db->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers
                        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                        WHERE customer_id='".$service_data->f('customer_id')."' ");
      	$buyer_address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$service_data->f('customer_id')."' AND billing ='1' ");
      	$address_info = $buyer_address->f('address')."\n".$buyer_address->f('zip').'  '.$buyer_address->f('city')."\n".get_country_name($buyer_address->f('country_id'));
        $free_field=addslashes($service_data->f('free_field'));

	    $email_language = $service_data->f('email_language');
	    $langs = $service_data->f('email_language');
	    if(!$email_language){
	    	$email_language = $buyer_details->f('internal_language');
      	$langs = $buyer_details->f('internal_language');      
	    }
	    if(!$email_language){
	    	$user_lang = $_SESSION['l'];
		if ($user_lang=='nl')
		{
			$user_lang='du';
		}
		switch ($user_lang) {
			case 'en':
				$user_lang_id='1';
				break;
			case 'du':
				$user_lang_id='3';
				break;
			case 'fr':
				$user_lang_id='2';
				break;
		}
	      $email_language = $user_lang_id;
	      $langs = $user_lang_id;
	    }

	    $quote_ts=time();
    	$due_days = $buyer_details->f('payment_term');
	    $invoice_due_date = $buyer_details->f('payment_term') ? $quote_ts + ($buyer_details->f('payment_term') * (60*60*24)) : time();
	    $payment_term_type = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
	    $payment_type_choose = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
	    if($payment_term_type == 2){
	      /*$curMonth = date('n',$quote_ts);
	      $curYear  = date('Y',$quote_ts);
	        $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
	      $invoice_due_date = $buyer_details->f('payment_term') ? $firstDayNextMonth + ($buyer_details->f('payment_term') * (60*60*24)-1) : time();*/
	      if($buyer_details->f('payment_term')){
	        $tmstmp_month=$quote_ts + ( $buyer_details->f('payment_term') * ( 60*60*24 )-1);
	        $lastday = date('t',$tmstmp_month);
	        $invoice_due_date = mktime(23, 59, 59,date('m',$tmstmp_month), $lastday,date('Y',$tmstmp_month));
	      }else{
	        $invoice_due_date=time();
	      }
	    }

	    $currency_type = $service_data->f('currency_type') ? $service_data->f('currency_type') : ($buyer_details->f('currency_type') ? $buyer_details->f('currency_type') : ACCOUNT_CURRENCY_TYPE);

	    $invoice_note_lang_id = $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1' AND active='1'");
	    // multilanguage notes
	    if($invoice_note_lang_id==1){
	      $invoice_note = 'invoice_note';
	    }else{
	      $invoice_note = 'invoice_note_'.$invoice_note_lang_id;
	    }

	    $notes_1 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note' "));
	    $notes_2 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_2' "));
	    $notes_3 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_3' "));
	    $notes_4 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_4' "));

	    $custom_translate_loop=array();
	    $custom_langs_inv = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ");
	    while($custom_langs_inv->next()) {
	      $cust_translate_loop=array(
	        'lang_id'           =>    $custom_langs_inv->f('lang_id'),
	        'NOTES'             =>    addslashes($this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type='invoice_note_".$custom_langs_inv->f('lang_id')."' ")),
	      );
	      array_push($custom_translate_loop, $cust_translate_loop);
	    }

	    $NOTES=$notes_1;
	    $notes2 =  addslashes($buyer_details->f('invoice_note2'));
	    $contact_id = $service_data->f('contact_id');
	    $buyer_id = $service_data->f('customer_id');
	    $buyer_name = $service_data->f('customer_id') ? addslashes($buyer_details->f('name')).' '.addslashes($buyer_details->f('l_name')) : addslashes($buyer_details->f('firstname')).' '.addslashes($buyer_details->f('lastname'));
	    $customer_address = addslashes($buyer_address->f('address'));
	    $buyer_email = $buyer_id ? addslashes($buyer_details->f('c_email')) : addslashes($buyer_details->f('email'));
	    $buyer_phone = $buyer_id ? $buyer_details->f('comp_phone') : $buyer_details->f('phone');
	    $buyer_fax = $buyer_id ? $buyer_details->f('comp_fax') : '';
	    $buyer_zip = $buyer_address->f('zip');
	    $buyer_city = addslashes($buyer_address->f('city'));
	    $buyer_country_id = $buyer_address->f('country_id');
	    $buyer_state_id = $buyer_address->f('state_id');

	    $currency_rate = $currency_type != ACCOUNT_CURRENCY_TYPE ? $this->get_rate($currency_type) : 1;

	    $acc_manager=$this->db->field("SELECT acc_manager_name FROM customers WHERE customer_id = '".$buyer_id."'");
	    $acc_manager = explode(',',$acc_manager);
	    $acc_manager_id=$this->db->field("SELECT user_id FROM customers WHERE customer_id = '".$buyer_id."'");
	    $acc_manager_id = explode(',',$acc_manager_id);

	    $ac_manager_id=$acc_manager_id[0];
	    $ac_manager_name=$acc_manager[0];
	    $pdf_layout = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$buyer_details->f('identity_id')."' and module='inv'");
	    if($contact_id) {
	      $contact_name=addslashes($this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE customer_contacts.contact_id='".$contact_id."'"));
	    }

	    $invoice_total = $price*$currency_rate;
	    $invoice_total_vat=$invoice_total+($invoice_total*get_customer_vat($buyer_id)/100);

	    if($_SESSION['main_u_id']=='0'){
			//$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");				
			$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);	
		}else{
			$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
		}
		if($is_accountant){
			$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
			if($accountant_settings=='1'){
				$acc_force=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_FORCE_DRAFT' ");
				if($acc_force){
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					    $this->db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					    $this->db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}else{
				$force_draft=$this->db_users->field("SELECT force_draft FROM accountants WHERE account_id='".$is_accountant."' ");
				if($force_draft){
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					      $this->db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					      $this->db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}	
	     }
	     $DRAFT_INVOICE_NO_NUMBER=$this->db->field("SELECT value FROM settings WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
	     if($DRAFT_INVOICE_NO_NUMBER){
	     		$s_number='';
	     }else{
	     		$s_number=addslashes(generate_invoice_number(DATABASE_NAME,false));
	     }
	     
	    $invoice_id=$this->db->insert("INSERT INTO tblinvoice SET
                            serial_number           =   '".$s_number."',
                            invoice_date            =   '".$quote_ts."',
                            due_date          		= '".$invoice_due_date."',
                            due_days          		=   '".$due_days."',
                            payment_term_type     	=   '".$payment_type_choose."',
                            req_payment             =   '100',
                            currency_type           =   '".$currency_type."',
                            notes2                  =   '".utf8_encode($notes2)."',
                            type                    =   '0',
                            contact_id          	=   '".$contact_id."',
                            contact_name         	=   '".$contact_name."',
                            status                  =   '0',
                            f_archived              =   '0',
                            paid                  	=   '0',
                            buyer_id                =   '".$buyer_id."',
                            buyer_name            	=   '".$buyer_name."',
                            buyer_address           =   '".$customer_address."',
                            buyer_email             =   '".$buyer_email."',
                            buyer_phone             =   '".$buyer_phone."',
                            buyer_fax             	=   '".$buyer_fax."',
                            buyer_zip             	=   '".$buyer_zip."',
                            buyer_city            	=   '".$buyer_city."',
                            buyer_country_id        =   '".$buyer_country_id."',
                            buyer_state_id          =   '".$buyer_state_id."',
                            seller_id             	=   '".$in['seller_id']."',
                            seller_name             =   '".addslashes(ACCOUNT_COMPANY)."',
                            seller_d_address        =   '".addslashes(ACCOUNT_DELIVERY_ADDRESS)."',
                            seller_d_zip            =   '".ACCOUNT_DELIVERY_ZIP."',
                            seller_d_city           =   '".addslashes(ACCOUNT_DELIVERY_CITY)."',
                            seller_d_country_id     =   '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                            seller_d_state_id       =   '".ACCOUNT_DELIVERY_STATE_ID."',
                            seller_b_address        =   '".addslashes(ACCOUNT_BILLING_ADDRESS)."',
                            seller_b_zip            =   '".ACCOUNT_BILLING_ZIP."',
                            seller_b_city           =   '".addslashes(ACCOUNT_BILLING_CITY)."',
                            seller_b_country_id     =   '".ACCOUNT_BILLING_COUNTRY_ID."',
                            seller_b_state_id       =   '".ACCOUNT_BILLING_STATE_ID."',
                            seller_bwt_nr           =   '".$buyer_details->f('btw_nr')."',
                            created                 =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                            created_by              =   '".$_SESSION['u_id']."',
                            last_upd                =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                            last_upd_by             =   '".$_SESSION['u_id']."',
                            vat             		=   '".get_customer_vat($buyer_id)."',
                            our_ref           		=   '".$service_data->f('serial_number')."',
                            remove_vat          	=   '".$buyer_details->f('no_vat')."',
                            discount 				= '0',
                            currency_rate       	= '".$currency_rate."',
                            quote_id          		= '0',
                            contract_id         	= '0',
                            email_language        	= '".$email_language."',
                            order_id          		= '0',
                            free_field          	= '".$free_field."',
                            same_invoice        	= '0',
                            apply_discount        	= '0',
                            service_id          	= '".$in['service_id']."',
                            attach_timesheet        =   '0',
                            attach_intervention     =     '0',
                            vat_regime_id           ='".$buyer_details->f('vat_regime_id')."',
                            acc_manager_id          ='".$ac_manager_id."',
                            acc_manager_name        ='".addslashes($ac_manager_name)."',
                            identity_id 			='".$service_data->f('identity_id')."',
                            source_id 			='".$service_data->f('source_id')."',
                            type_id 			='".$service_data->f('type_id')."',
                            segment_id 			='".$service_data->f('segment_id')."',
                            pdf_layout				='".$pdf_layout."',
                            same_address			='1',
                            amount                  ='".$invoice_total."', 
                            amount_vat              ='".$invoice_total_vat."',
                            margin                  ='".$invoice_total."', 
                            margin_percent          ='0' ");

		if($DRAFT_INVOICE_NO_NUMBER!='1'){
			$ogm = generate_ogm(DATABASE_NAME,$invoice_id);
	    	$this->db->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$invoice_id."' ");
		}

	    $lang_note_id = $email_language;
        if($lang_note_id == $email_language){
            $active_lang_note = 1;
        }else{
            $active_lang_note = 0;
        }

        $this->db->insert("INSERT INTO note_fields SET
                            item_id         = '".$invoice_id."',
                            lang_id         =   '".$email_language."',
                            active          =   '".$active_lang_note."',
                            item_type         =   'invoice',
                            item_name         =   'notes',
                            item_value        =   '".$NOTES."' ");
        $custom_lang_note_id = $email_language;

        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
        while($custom_langs->next()) {
          foreach ($custom_translate_loop as $key => $value) {
            if($value['lang_id'] == $custom_langs->f('lang_id')){
              $nota = $value['NOTES'];
              break;
            }
          }
          if($custom_lang_note_id == $custom_langs->f('lang_id')) {
            $custom_active_lang_note = 1;
          } else {
            $custom_active_lang_note = 0;
          }
          $this->db->insert("INSERT INTO note_fields SET   item_id     = '".$invoice_id."',
                                      lang_id     = '".$custom_langs->f('lang_id')."',
                                      active      = '".$custom_active_lang_note."',
                                      item_type     = 'invoice',
                                      item_name     = 'notes',
                                      item_value    = '".$nota."' ");
        }

		$this->db->query("INSERT INTO tblinvoice_line SET
                                    name                =   '".(gm('Downpayment').' '.addslashes($service_data->f('subject')))."', 
                                    invoice_id          =   '".$invoice_id."',
                                    quantity            =   '1',
                                    price               =   '".$invoice_total."',
                                    amount              =   '".$invoice_total."',
                                    f_archived          =   '0',
                                    created             =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                                    created_by          =   '".$_SESSION['u_id']."',
                                    last_upd            =   '".$created_date."',
                                    last_upd_by         =   '".$_SESSION['u_id']."',
                                    vat                 = '".return_value(get_customer_vat($buyer_id))."',
                                    discount            = '0',
                                    visible             ='1' ");
                
  		insert_message_log($this->pag,'{l}Invoice was created by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);

  		$down_exist=$this->db->field("SELECT id FROM tbldownpayments WHERE service_id='".$in['service_id']."' ");
  		if($down_exist){
  			$this->db->query("UPDATE tbldownpayments SET
  									invoice_id  = '".$invoice_id."',
  									date        = '".$quote_ts."',
  									user_id 	= '".$_SESSION['u_id']."',
  									value_fixed = '".return_value($in['downpayment_fixed'])."',
  									value_percent='".return_value($in['downpayment_percent'])."'
  									WHERE service_id='".$in['service_id']."' ");
  		}else{
  			$this->db->query("INSERT INTO tbldownpayments SET
  									invoice_id  = '".$invoice_id."',
  									date        = '".$quote_ts."',
  									user_id 	= '".$_SESSION['u_id']."',
  									value_fixed = '".return_value($in['downpayment_fixed'])."',
  									value_percent='".return_value($in['downpayment_percent'])."',
  									service_id   = '".$in['service_id']."' ");
  		}	

  		$tracking_data=array(
	            'target_id'         => $invoice_id,
	            'target_type'       => '1',
	            'target_buyer_id'   => $buyer_id,
	            'lines'             => array(
	                                          array('origin_id'=>$in['service_id'],'origin_type'=>'5')
	                                    )
	          );
	      addTracking($tracking_data);	


  		# for pdf
	    $inv = $this->db->query("SELECT * FROM tblinvoice WHERE id = '".$invoice_id."' ");
	    $params = array();
	    $params['use_custom'] = 0;
	    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
	      $params['logo'] = $inv->f('pdf_logo');
	      $params['type']=$inv->f('pdf_layout');
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
	      $params['custom_type']=$inv->f('pdf_layout');
	      unset($params['type']);
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	      $params['use_custom'] = 1;
	    }else{
	      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
	      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
	    }
	    #if we are using a customer pdf template
	    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
	      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
	      unset($params['type']);
	    }
	    $params['id'] = $invoice_id;
	    $params['lid'] = $email_language;
	    $params['save_as'] = 'F';
	    $this->generate_pdf($params);
	    # for pdf      

	    msg::success(gm('Downpayment invoice created'),"success");
	    return true;	
	}

	function get_rate($from)
  	{
	    $currency = currency::get_currency($from,'code');
	    if(empty($currency)){
	      $currency = "USD";
	    }
	    $separator = ACCOUNT_NUMBER_FORMAT;
	    $into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');

	    return currency::getCurrency($currency, $into, 1,$separator);
  	}

  	function update_task_budget(&$in){
  		$this->db->query("UPDATE servicing_support_tasks SET task_budget='".return_value($in['task_budget'])."' WHERE task_id='".$in['task_id']."' ");
  		msg::success(gm('Changes saved'),"success");
  		return true;
  	}

    function update_task_quantity(&$in){
        $this->db->query("UPDATE servicing_support_tasks SET quantity='".return_value($in['task_quantity'])."' WHERE task_id='".$in['task_id']."' ");
        msg::success(gm('Changes saved'),"success");
        return true;
    }

  	function setInstallation(&$in){
  		$this->db->query("UPDATE servicing_support SET installation_id='".$in['installation_id']."' WHERE service_id='".$in['service_id']."' ");
  		msg::success(gm('Changes saved'),"success");
  		json_out($in);
  	}

  	function update_service_qty(&$in)
	{
		$this->db->query("UPDATE servicing_support_tasks SET quantity='".return_value($in['qty'])."' WHERE task_id='".$in['task_id']."' ");
		msg::success(gm('Changes saved'),"success");
		return true;
	}

	function createBulkInterventions(&$in){
		$filter="";
		if($in['choice_2']){
			$filter.=" LIMIT 1 ";
		}
		$data_main=$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['service_id']."' ")->getAll();
		$rec_services=$this->db->query("SELECT * FROM servicing_recurring WHERE service_id='".$in['service_id']."' ORDER BY `date` ASC ".$filter." ");
    	$escaped_values=array('is_recurring','service_id','rec_start_date','rec_frequency','rec_days','rec_next_date','trace_id','author_id','rec_end_date','rec_number','last_step','end_contract','advance_day');
    	while($rec_services->move_next()){
    		$data_main_sql="INSERT INTO servicing_support SET";
    		foreach($data_main as $key=>$value){
    			foreach($value as $key1=>$value1){
    				if(in_array($key1, $escaped_values)){
    					continue;
    				}else if($key1 == 'user_id'){
    					$data_main_sql.=" ".$key1."='System',";
    				}else if($key1 == 'serial_number'){
    					$data_main_sql.=" ".$key1."='".generate_service_number(DATABASE_NAME)."',";
    				}else if($key1 == 'planeddate'){
    					$data_main_sql.=" ".$key1."='".$rec_services->f('date')."',";
    				}else{
    					$data_main_sql.=" ".$key1."='".addslashes($value1)."',";
    				}
    			}
    		}
    		$data_main_sql=rtrim($data_main_sql,",");
    		$service_id=$this->db->insert($data_main_sql);	
    			
    		$data_users=$this->db->query("SELECT * FROM servicing_support_users WHERE service_id='".$in['service_id']."' ")->getAll();
    		foreach($data_users as $key=>$value){
    			$data_users_sql="INSERT INTO servicing_support_users SET";
    			foreach($value as $key1=>$value1){
    				if($key1=='u_id'){
    					continue;
    				}else if($key1 == 'service_id'){
    					$data_users_sql.=" ".$key1."='".$service_id."',";
    				}else{
    					$data_users_sql.=" ".$key1."='".addslashes($value1)."',";
    				}		
    			}
    			$data_users_sql=rtrim($data_users_sql,",");
	    		if(!empty($data_users)){
	    			$this->db->query($data_users_sql);
	    		}
    		}		
 		
    		$data_tasks=$this->db->query("SELECT * FROM servicing_support_tasks WHERE service_id='".$in['service_id']."' ")->getAll();
    		foreach($data_tasks as $key=>$value){
    			$data_tasks_sql="INSERT INTO servicing_support_tasks SET";
    			foreach($value as $key1=>$value1){
    				if($key1=='task_id'){
    					continue;
    				}else if($key1 == 'service_id'){
    					$data_tasks_sql.=" ".$key1."='".$service_id."',";
    				}else{
    					$data_tasks_sql.=" ".$key1."='".addslashes($value1)."',";
    				}			
    			}
    			$data_tasks_sql=rtrim($data_tasks_sql,",");
	    		if(!empty($data_tasks)){
	    			$this->db->query($data_tasks_sql);
	    		}
    		}  		
		
    		$data_articles=$this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."' ")->getAll();
    		foreach($data_articles as $key=>$value){
    			$data_articles_sql="INSERT INTO servicing_support_articles SET";
    			foreach($value as $key1=>$value1){
    				if($key1=='a_id'){
    					continue;
    				}else if($key1 == 'service_id'){
    					$data_articles_sql.=" ".$key1."='".$service_id."',";
    				}else{
    					$data_articles_sql.=" ".$key1."='".addslashes($value1)."',";
    				}  				
    			}
    			$data_articles_sql=rtrim($data_articles_sql,",");
	    		if(!empty($data_articles)){
	    			$this->db->query($data_articles_sql);
	    		}
    		}		

    		$tracking_data=array(
		      'target_id'         => $service_id,
		      'target_type'       => '5',
		      'target_buyer_id'   => $data_main[0]['customer_id'],
		       'lines'            => array(
		                               array('origin_id'=>$in['service_id'],'origin_type'=>'5')
		                           )
		       );
			addTracking($tracking_data);
			$this->db->query("UPDATE servicing_recurring SET `done`='1' WHERE id='".$rec_services->f('id')."' ");
			$adv_days=$this->db->field("SELECT advance_day FROM servicing_support WHERE service_id='".$in['service_id']."'");
			$rec_next_date=$this->db->field("SELECT `date` FROM servicing_recurring WHERE service_id='".$in['service_id']."' AND `done`='0' ORDER BY `date` ASC LIMIT 1 ");
			if($rec_next_date){
				$this->db->query("UPDATE servicing_support SET rec_next_date='".strtotime('-'.$adv_days.' days',$rec_next_date)."' WHERE service_id='".$in['service_id']."' ");
			}
			insert_message_log($this->pag,'{l}Intervention added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$service_id,false);
	    }
	}

	function setFrequency(&$in){
		$this->db->query("UPDATE servicing_support SET rec_frequency='".$in['rec_frequency']."' WHERE service_id='".$in['service_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function setFrequencyDay(&$in){
		$this->db->query("UPDATE servicing_support SET rec_days='".$in['rec_days']."' WHERE service_id='".$in['service_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function setRecOpts(&$in){
		$end_date=$in['rec_end_date'] ? strtotime($in['rec_end_date']) : '0';
		$this->db->query("UPDATE servicing_support SET 
							rec_number='".$in['rec_number']."',
							rec_end_date='".$end_date."'
			WHERE service_id='".$in['service_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function setAdvanceDay(&$in){
		$this->db->query("UPDATE servicing_support SET advance_day='".$in['advance_day']."' WHERE service_id='".$in['service_id']."' ");
		insert_message_log($this->pag, "{l}Intervention creation days before the planned date updated by{endl} ".get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id']);
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function changeRecurringPlan(&$in){
		$old_dates=$this->db->query("SELECT * FROM servicing_recurring WHERE service_id='".$in['service_id']."' AND done='0'")->getAll();
		$this->db->query("INSERT INTO logging SET pag='service_recurring_dates', message='".serialize($old_dates)."', field_name='service_id', field_value='".$in['service_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
		if($in['planning_users'] && !empty($in['planning_users'])){
			foreach($in['planning_users'] as $key=>$value){
				$in['user_id']=$value;
				$in['name'] = get_user_name($in['user_id']);
				$this->addServiceUser($in);		
			}
		}
		if($in['planning_dates'] && !empty($in['planning_dates'])){
			$this->db->query("DELETE FROM servicing_recurring WHERE service_id='".$in['service_id']."' AND done='0' ");
			sort($in['planning_dates']);
			foreach($in['planning_dates'] as $key=>$value){
				$new_val=$value;
				if(date('I',$new_val)=='1'){
			        $new_val+=3600;
			    }
				$this->db->query("INSERT INTO servicing_recurring SET
						service_id='".$in['service_id']."',
						date='".$new_val."' ");
			}
			$adv_days=$this->db->field("SELECT advance_day FROM servicing_support WHERE service_id='".$in['service_id']."'");
			$first_run=$in['planning_dates'][0];
			if(date('I',$first_run)=='1'){
			    $first_run+=3600;
			}
			$this->db->query("UPDATE servicing_support SET rec_next_date='".strtotime('-'.$adv_days.' days',$first_run)."' WHERE service_id='".$in['service_id']."' ");
		}
		msg::success(gm('Changes saved'),"success");
		return true;
	}


	function submitIntervention(&$in){
		$author_id=$this->db->field("SELECT author_id FROM servicing_support WHERE service_id='".$in['service_id']."'");
		if(!$author_id){
			msg::error(gm('No author'),'error');
			json_out($in);
		}
		$author_email=$this->db_users->field("SELECT email FROM users WHERE user_id='".$author_id."'");
		if(!$author_email){
			msg::error(gm('No author email'),'error');
			json_out($in);
		}
		$mail = new PHPMailer();
		$mail->CharSet="UTF-8";
        $mail->WordWrap = 50;
        $fromMail='noreply@akti.com';
		$mail->SetFrom($fromMail, 'Akti');
		$forgot_email = include(__DIR__.'/../controller/submit_email.php');
		$body=$forgot_email;
		$mail->Subject = 'Service Report for intervention '.$in['serial_number'];
		$mail->MsgHTML($body);
		$mail->AddAddress($author_email);
		$mail->Send();
		if($mail->IsError() || $mail->ErrorInfo){
	      msg::error($mail->ErrorInfo,'error');
	      json_out($in);
	    }
    	msg::success(gm("Email sent"),'success');
        $this->db->query("UPDATE servicing_support SET submitted='1' WHERE service_id='".$in['service_id']."' ");
		json_out($in);
	}

		function update_article_reserved(&$in)
	{
		//var_dump($in['lines'][0]['disp_addres_info']);exit();
		if(!$this->article_reservation_validation($in)){
			msg::error(gm('Total quantity lower than quantity selected'),"error");
			json_out($in);
		}
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$in['reservation_id'] = $this->db->insert("INSERT INTO service_reservations
			                      SET service_id='".$in['service_id']."',
			                         `date` = '".$in['date_del_line_h']."'");
		$articles_ids=array();
		foreach($in['lines'] as $key=>$value){
			array_push($articles_ids,$value['article_id']);
			$already_reserved=$this->db->field("SELECT SUM(quantity) FROM service_reservation WHERE service_id='".$in['service_id']."' AND a_id='".$value['article_id']."' ");
			if(!$already_reserved){
				$already_reserved=0;
			}
			$totalQ=$this->db->field("SELECT quantity FROM servicing_support_articles WHERE service_id='".$in['service_id']."' AND article_id='".$value['article_id']."' ");
			if(return_value($value['quantity'])+$already_reserved>=$totalQ){
				//$this->db->query("UPDATE servicing_support_articles SET article_reserved='1', quantity='".(return_value($value['quantity'])+$already_reserved)."' WHERE service_id='".$in['service_id']."' AND article_id='".$value['article_id']."' ");
			}
			$in['line']=$value;
			$this->make_stock_reservation($in);
		}
/*		if(WAC){
		  foreach ($articles_ids as $key => $value) {
		    generate_purchase_price($value);
		  }
		}*/

		msg::success(gm('Stock reservation made'),"success");
		return true;
	}

	function article_reservation_validation(&$in)
	{
		$is_ok=true;
		//$quantity=$this->db->field("SELECT quantity FROM servicing_support_articles WHERE a_id='".$in['a_id']."'");
		foreach($in['lines'] as $key => $value){
			if($value['is_error']=='1'){
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function make_stock_reservation(&$in)
	{
		$const =array();
		$const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
		$const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

		$now = time();
		//foreach ($in['quantity'] as $key => $value) {
			//if(return_value($value) > 0){
			if($in['line']['quantity'] > 0) {
				$hide_stock = $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$in['line']['article_id']."'");
				if($const['ALLOW_STOCK'] && $hide_stock==0 ){

					$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$in['line']['article_id']."' ");
					$article_data->next();
					$stock = $article_data->f("stock");

					$stock_packing = $article_data->f("packing");
					if(!$const['ALLOW_ARTICLE_PACKING']){
						$stock_packing = 1;
					}
					$stock_sale_unit = $article_data->f("sale_unit");
					if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
						$stock_sale_unit = 1;
					}

					$new_stock = $stock - (return_value($in['line']['quantity'])*$stock_packing/$stock_sale_unit);

					//$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['line']['article_id']."'");

					/*start for  stock movements*/

					if($in['line']['disp_addres_info']){
						$locations=array();
						foreach($in['line']['disp_addres_info'] as $key => $value){
							$locations[$value['customer_id'].'-'.$value['address_id']]=return_value($value['quantity_reserved']);
						}
					}
					/*$this->db->insert("INSERT INTO stock_movements SET
							date 						=	'".$now."',
							service_id 					= 	'".$in['service_id']."',
							article_id 					=	'".$in['line']['article_id']."',
							article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
							item_code  					= 	'".addslashes($article_data->f("item_code"))."',
							movement_type				=	'17',
							quantity 					= 	'".(return_value($in['line']['quantity'])*$stock_packing/$stock_sale_unit)."',
							stock 						=	'".$stock."',
							new_stock 					= 	'".$new_stock."',
							backorder_articles 			= 	'0',
						      location_info                =   '".serialize($locations)."',
						      delivery_id                 =    '".$in['delivery_id']."'
				        ");*/
					/*end for  stock movements*/
				}

			}
		//}
		if($in['line']['disp_addres_info']){
                 	foreach ($in['line']['disp_addres_info'] as $key => $value) {
                 	
                 			$quantity_reserved=return_value($value['quantity_reserved']);
                 		if($quantity_reserved){
                 			$from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$in['line']['article_id']."' AND  address_id='".$value['address_id']."' and customer_id='".$value['customer_id']."'");                  
	                  	 	$this->db->query("INSERT INTO service_reservation SET service_id='".$in['service_id']."' , reservation_id='".$in['reservation_id']."' , a_id='".$in['line']['article_id']."', quantity='".$quantity_reserved."', location_id='".$value['address_id']."'");
	                  	 	$this->db->query("UPDATE servicing_support_articles SET reserved ='1' WHERE service_id='".$in['service_id']."' AND article_id='".$in['line']['article_id']."' ");
                 		}
                 		
	                  //$this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -($dispatch_quantity*($stock_packing/$stock_sale_unit )))."' WHERE article_id='".$in['line']['article_id']."'   AND  address_id='".$value['address_id']."' and customer_id='".$value['customer_id']."'");
                 	}
            }
/*            if($in['line']['selected_s_n']!=''){
            	$in['line']['selected_s_n']=rtrim($in['line']['selected_s_n'],',');
            	$selected_serials=explode(',', $in['line']['selected_s_n']);
            	foreach($selected_serials as $key_serial=>$value_serial){
            		$this->db->query("UPDATE serial_numbers
										SET 		status_id 	= '2',
												status_details_2 	= '".$in['serial_number']."',
												date_out 		= '".time()."',
												service_id 	      ='".$in['service_id']."',
												delivery_id 	= '".$in['delivery_id']."'
										WHERE id = '".$value_serial."' ");
            	}
            }*/
           
		//insert_message_log($this->pag,'{l}Article delivered by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['service_id']);
		return true;
	}

    function serviceUpdateModel(&$in){
        $v=new validation($in);
        $v->field("service_id","Intervention Id","required");
        if(!$v->run()){
            json_out($in);
        }
        $is_ok=false;
        //$checked_index=0;
        foreach($in['list'] as $key=>$value){
            if($value['checked']){
                $is_ok=true;
                //$checked_index=$key;
                break;
            }
        }
        if(!$is_ok){
            msg::error(gm('At least one model required'),'error');
            json_out($in);
        }
        if ($in['service_id'] != 'tmp') {
            foreach ($in['list'] as $idx => $data) {
                if ($data['checked']) {
                    $service_id = $data['service_id'];

                    $data_users = $this->db->query("SELECT * FROM servicing_support_users WHERE service_id='" . $service_id . "' ")->getAll();
                    foreach ($data_users as $key => $value) {
                        $user_exist = $this->db->field("SELECT user_id FROM servicing_support_users WHERE user_id='" . $value['user_id'] . "' AND service_id='" . $in['service_id'] . "'");
                        if ($user_exist) {
                            continue;
                        }
                        $data_users_sql = "INSERT INTO servicing_support_users SET";
                        foreach ($value as $key1 => $value1) {
                            if ($key1 == 'u_id') {
                                continue;
                            } else if ($key1 == 'service_id') {
                                $data_users_sql .= " " . $key1 . "='" . $in['service_id'] . "',";
                            } else {
                                $data_users_sql .= " " . $key1 . "='" . addslashes($value1) . "',";
                            }
                        }
                        $data_users_sql = rtrim($data_users_sql, ",");
                        if (!empty($data_users)) {
                            $this->db->query($data_users_sql);
                        }
                    }
                    $data_tasks = $this->db->query("SELECT * FROM servicing_support_tasks WHERE service_id='" . $service_id . "' ")->getAll();
                    foreach ($data_tasks as $key => $value) {
                        $data_tasks_sql = "INSERT INTO servicing_support_tasks SET";
                        foreach ($value as $key1 => $value1) {
                            if ($key1 == 'task_id') {
                                continue;
                            } else if ($key1 == 'service_id') {
                                $data_tasks_sql .= " " . $key1 . "='" . $in['service_id'] . "',";
                            } else {
                                $data_tasks_sql .= " " . $key1 . "='" . addslashes($value1) . "',";
                            }
                        }
                        $data_tasks_sql = rtrim($data_tasks_sql, ",");
                        if (!empty($data_tasks)) {
                            $this->db->query($data_tasks_sql);
                        }
                    }
                    $data_articles = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='" . $service_id . "' ")->getAll();
                    foreach ($data_articles as $key => $value) {
                        $data_articles_sql = "INSERT INTO servicing_support_articles SET";
                        foreach ($value as $key1 => $value1) {
                            if ($key1 == 'a_id') {
                                continue;
                            } else if ($key1 == 'service_id') {
                                $data_articles_sql .= " " . $key1 . "='" . $in['service_id'] . "',";
                            } else {
                                $data_articles_sql .= " " . $key1 . "='" . addslashes($value1) . "',";
                            }
                        }
                        $data_articles_sql = rtrim($data_articles_sql, ",");
                        if (!empty($data_articles)) {
                            $this->db->query($data_articles_sql);
                        }
                    }
                }
            }
            $this->db->query("UPDATE servicing_support SET model_id='" . $service_id . "' WHERE service_id='" . $in['service_id'] . "'");
            msg::success(gm('Changes saved'), 'success');
        } elseif ($in['service_id'] == 'tmp') {
            $in['template'] = false;
            if($in['list']) {
                foreach($in['list'] as $list) {
                    if($list['checked']) {
                        $in['template'] = true;
                    }
                }
            }
            msg::success ( gm("Changes saved"),'success');
        }
        return true;
    }

	function saveAddToPdf(&$in){
	  	if($in['all']){
	  		if($in['value'] == 1){
	  			foreach ($in['item'] as $key => $value) {
				    $_SESSION['add_to_pdf_int'][$value]= $in['value'];
		  		}
	  		}else{
	  			foreach ($in['item'] as $key => $value) {
				    unset($_SESSION['add_to_pdf_int'][$value]);
		  		}
			  }
	  	}else{
		    if($in['value'] == 1){
		    	$_SESSION['add_to_pdf_int'][$in['item']]= $in['value'];
		    }else{
		    	unset($_SESSION['add_to_pdf_int'][$in['item']]);
		    }
		  }
		$all_pages_selected=false;
	    $minimum_selected=false;
	    if($_SESSION['add_to_pdf_int'] && count($_SESSION['add_to_pdf_int'])){
	      if($_SESSION['tmp_add_to_pdf_int'] && count($_SESSION['tmp_add_to_pdf_int']) == count($_SESSION['add_to_pdf_int'])){
	        $all_pages_selected=true;
	      }else{
	        $minimum_selected=true;
	      }
	    }
	    $in['all_pages_selected']=$all_pages_selected;
	    $in['minimum_selected']=$minimum_selected;
	    json_out($in);
	    //return true;
	}

	function saveAddToPdfAll(&$in){
	    $all_pages_selected=false;
	    $minimum_selected=false;
	    if($in['value']){
	      unset($_SESSION['add_to_pdf_int']);
	      if($_SESSION['tmp_add_to_pdf_int'] && count($_SESSION['tmp_add_to_pdf_int'])){
	        $_SESSION['add_to_pdf_int']=$_SESSION['tmp_add_to_pdf_int'];
	        $all_pages_selected=true;
	      }
	    }else{
	      unset($_SESSION['add_to_pdf_int']);
	    }
	    $in['all_pages_selected']=$all_pages_selected;
	    $in['minimum_selected']=$minimum_selected;
	    json_out($in);
  	}

	function changeOrderUsers(&$in){
		$v=new validation($in);
		$v->field("service_id","Id","required:exist[servicing_support.service_id]");
		$v->field("order_users","Data","required");
		if(!$v->run()){
			json_out($in);
		}
		$this->db->query("UPDATE servicing_support_users SET sort_order='0' WHERE service_id='".$in['service_id']."' ");
		foreach($in['order_users'] as $key=>$id){
			$this->db->query("UPDATE servicing_support_users SET sort_order='".$key."' WHERE service_id='".$in['service_id']."' AND u_id='".$id."' ");
		}
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	}

	function changeOrderTasks(&$in){
		$v=new validation($in);
		$v->field("service_id","Id","required:exist[servicing_support.service_id]");
		$v->field("order_tasks","Data","required");
		if(!$v->run()){
			json_out($in);
		}
		$this->db->query("UPDATE servicing_support_tasks SET sort_order='0' WHERE article_id='0' AND service_id='".$in['service_id']."' ");
		foreach($in['order_tasks'] as $key=>$task_id){
			$this->db->query("UPDATE servicing_support_tasks SET sort_order='".$key."' WHERE article_id='0' AND service_id='".$in['service_id']."' AND task_id='".$task_id."' ");
		}
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	}

	function changeOrderServices(&$in){
		$v=new validation($in);
		$v->field("service_id","Id","required:exist[servicing_support.service_id]");
		$v->field("order_services","Data","required");
		if(!$v->run()){
			json_out($in);
		}
		$this->db->query("UPDATE servicing_support_tasks SET sort_order='0' WHERE article_id!='0' AND service_id='".$in['service_id']."' ");
		foreach($in['order_services'] as $key=>$id){
			$this->db->query("UPDATE servicing_support_tasks SET sort_order='".$key."' WHERE article_id!='0' AND service_id='".$in['service_id']."' AND task_id='".$id."' ");
		}
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	}

	function changeOrderArticles(&$in){
		$v=new validation($in);
		$v->field("service_id","Id","required:exist[servicing_support.service_id]");
		$v->field("order_articles","Data","required");
		if(!$v->run()){
			json_out($in);
		}
		$this->db->query("UPDATE servicing_support_articles SET sort_order='0' WHERE article_id!='0' AND service_id='".$in['service_id']."' ");
		foreach($in['order_articles'] as $key=>$id){
			$this->db->query("UPDATE servicing_support_articles SET sort_order='".$key."' WHERE article_id!='0' AND service_id='".$in['service_id']."' AND a_id='".$id."' ");
		}
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	}

	function changeOrderSupplies(&$in){
		$v=new validation($in);
		$v->field("service_id","Id","required:exist[servicing_support.service_id]");
		$v->field("order_supplies","Data","required");
		if(!$v->run()){
			json_out($in);
		}
		$this->db->query("UPDATE servicing_support_articles SET sort_order='0' WHERE article_id='0' AND service_id='".$in['service_id']."' ");
		foreach($in['order_supplies'] as $key=>$id){
			$this->db->query("UPDATE servicing_support_articles SET sort_order='".$key."' WHERE article_id='0' AND service_id='".$in['service_id']."' AND a_id='".$id."' ");
		}
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	}

	function update_default_email(&$in)
	{
		if($in['bcc_email']){
	      $bcc_email=$in['bcc_email'];
	      $in['bcc_email'] = str_replace(';', ',', $in['bcc_email']);   
	    }
		if(!$this->validate_default_email($in)){
			return false;
		}

		$mail_type = $in['mail_type_1'];

		$this->db->query("SELECT * FROM default_data WHERE type='intervention_email' ");
	    if($this->db->next()){
	      $this->db->query("UPDATE default_data SET default_name='".$in['default_name']."', value='".$in['email_value']."' WHERE type='intervention_email' ");
	    }else{
	      $this->db->query("INSERT INTO default_data SET  default_name='".$in['default_name']."', value='".$in['email_value']."', type='intervention_email' ");
	    }

		$this->db->query("SELECT * FROM default_data WHERE type='intervention_email_type' ");
	    if($this->db->next()){
	      $this->db->query("UPDATE default_data SET value='".$mail_type."' WHERE type='intervention_email_type' ");
	    }else{
	      $this->db->query("INSERT INTO default_data SET value='".$mail_type."', type='intervention_email_type' ");
	    }



    	$this->db->query("SELECT * FROM default_data WHERE type='bcc_intervention_email' ");
	    if($this->db->next()){
	      $this->db->query("UPDATE default_data SET value='".$in['bcc_email']."' WHERE type='bcc_intervention_email' ");
	    }else{
	      $this->db->query("INSERT INTO default_data SET value='".$in['bcc_email']."', type='bcc_intervention_email' ");
	    }
    	msg::success ( gm('Default email updated'),'success');
    	// $in['failure']=false;
    
    	return true;
	}

	function validate_default_email(&$in)
	{
		$v = new validation($in);
		$v->field('mail_type_1', 'Email', 'required');
		if($in['mail_type_1'] == 2){
			$v->field('default_name', 'Email', 'required');
			$v->field('email_value', 'Email', 'email:required');
		}
		if($in['bcc_email']){   
	      $v->field('bcc_email', 'Type of link', 'emails');
	    }
		return $v->run();
	}

}
?>