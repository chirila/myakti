<?php

/**
* 
*/
class team
{
	private $db;

	function __construct()
	{
		$this->db = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($db_config);
	}

	function add(&$in){
		if(!$this->add_validate($in)){
			json_out($in);
			return false;
		}
		$in['team_id']=$this->db->insert("INSERT INTO servicing_team SET name='".addslashes($in['name'])."' ");
		$i=0;
		foreach($in['users'] as $key=>$value){
			$this->db->query("INSERT INTO servicing_team_users SET
								team_id	='".$in['team_id']."',
								user_id	='".$value['user_id']."',
								sort 		= '".$i."' ");
			$i++;
			$access=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$value['user_id']."' ");
			$access_data=explode(";",$access);
			if(in_array('13', $access_data) === false){
				$already=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$value['user_id']."' AND name='team_intervention' ");
				if(is_null($already)){
					$this->db_users->query("INSERT INTO user_meta SET name='team_intervention', user_id='".$value['user_id']."',value='1' ");		
				}else{
					$this->db_users->query("UPDATE user_meta SET value='1' WHERE name='team_intervention' AND user_id='".$value['user_id']."' ");
				}	 
			}
		}
		msg::success(gm("Team created successfully"),"success");
		return true;
	}

	function add_validate(&$in){
		$v = new validation($in);
		$v->f('name', 'Name', 'required');
		$is_ok = $v->run();
		if($is_ok === true){
			if(count($in['users'])==0){
				msg::error(gm('At least one team member required'),"error");
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function update(&$in){
		if(!$this->add_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE servicing_team SET name='".addslashes($in['name'])."' WHERE team_id='".$in['team_id']."' ");
		$i=0;
		$team_users=$this->db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."'");
		while($team_users->next()){
			$access=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$team_users->f('user_id')."' ");
			$access_data=explode(";",$access);
			if(in_array('13', $access_data) === false){
				$already=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$team_users->f('user_id')."' AND name='team_intervention' ");
				if(!is_null($already)){
					$this->db_users->query("UPDATE user_meta SET value='0' WHERE name='team_intervention' AND user_id='".$team_users->f('user_id')."' ");
				}	 
			}
		}
		$this->db->query("DELETE FROM servicing_team_users WHERE team_id='".$in['team_id']."' ");
		foreach($in['users'] as $key=>$value){
			$this->db->query("INSERT INTO servicing_team_users SET
								team_id	='".$in['team_id']."',
								user_id	='".$value['user_id']."',
								sort 		= '".$i."' ");
			$i++;
			$access=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$value['user_id']."' ");
			$access_data=explode(";",$access);
			if(in_array('13', $access_data) === false){
				$already=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$value['user_id']."' AND name='team_intervention' ");
				if(is_null($already)){
					$this->db_users->query("INSERT INTO user_meta SET name='team_intervention', user_id='".$value['user_id']."',value='1' ");		
				}else{
					$this->db_users->query("UPDATE user_meta SET value='1' WHERE name='team_intervention' AND user_id='".$value['user_id']."' ");
				}	 
			}
		}
		msg::success(gm("Team updated successfully"),"success");
		return true;
	}

	function delete(&$in){
		$team_users=$this->db->query("SELECT user_id FROM servicing_team_users WHERE team_id='".$in['team_id']."'");
		while($team_users->next()){
			$access=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$team_users->f('user_id')."' ");
			$access_data=explode(";",$access);
			if(in_array('13', $access_data) === false){
				$already=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$team_users->f('user_id')."' AND name='team_intervention' ");
				if(!is_null($already)){
					$this->db_users->query("UPDATE user_meta SET value='0' WHERE name='team_intervention' AND user_id='".$team_users->f('user_id')."' ");
				}	 
			}
		}
		$this->db->query("DELETE FROM servicing_team WHERE team_id	='".$in['team_id']."' ");
		$this->db->query("DELETE FROM servicing_team_users WHERE team_id	='".$in['team_id']."' ");
		msg::success(gm("Team deleted successfully"),"success");
		return true;
	}

	function blockDay(&$in){
		if($in['value']=='1'){
			$this->db->query("INSERT INTO servicing_block SET `date`='".$in['date']."',user_id='".$in['user_id']."' ");
		}else{
			$this->db->query("DELETE FROM servicing_block WHERE `date`='".$in['date']."' AND user_id='".$in['user_id']."' ");
		}
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}
}
?>