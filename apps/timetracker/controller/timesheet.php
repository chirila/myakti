<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class Timesheet extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Week(){
		$in = $this->in;

		global $p_access;
		global $config;

		$result=array();

		//$tips = $page_tip->getTip('misc-timesheet');
		$filter = '1=1';
		$filter2 = ' AND 1=1 ';

		if(!$in['user_id'] )
		{
			$in['user_id'] = $_SESSION['u_id'];
		}

		if($in['start']){
			$result['start']=$in['start'];
			$now			= $in['start'];
		}else{
			$now 			= time();
		}

		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		if(date('I',$now)=='1'){
			$week_start+=3600;
		}
		$week_end 			= $week_start + 604799;

		$week_nr = date('W',$week_start);
		$year_nr = date("Y",$week_start);

		if($_SESSION['group'] != 'admin'){
			$is_manager = $this->db->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");
			if(!empty($is_manager)){
				$filter2 =" AND task_time.project_id IN ( SELECT project_id FROM project_user WHERE manager='1' OR user_id='".$_SESSION['u_id']."' ) ";
			}
		}

		$result['week_txt'] = date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end-3600);
		$result['PREV_WEEK_LINK']='index.php?do=misc-timesheet&user_id='.$in['user_id'].'&start='.($week_start-604800);
		$result['NEXT_WEEK_LINK']='index.php?do=misc-timesheet&user_id='.$in['user_id'].'&start='.($week_end+1);
		$result['ACTIVE_WEEK']='activated';

		$e = $this->db->query("SELECT task_time.contract_id,task_time.user_id,task_time.date as start_date,task_time.project_id,task_time.task_id,task_time.billed,task_time.submited AS submited,
		            tasks.task_name,projects.name as project_name,projects.active, projects.company_name AS c_name, projects.status_rate, task_time.customer_id AS c_id
		            FROM task_time
		            INNER JOIN tasks ON tasks.task_id=task_time.task_id
		            INNER JOIN projects ON projects.project_id=task_time.project_id
		            WHERE task_time.user_id='".$in['user_id']."' AND date BETWEEN '".$week_start."' AND '".$week_end."' ".$filter2."
		            GROUP BY task_time.task_id
		            ORDER BY c_name, project_name, task_name
				   ")->getAll();
		$showed_projects=array();
		$showed_projects2=array();
		$rows = count($e);
		$r=1;
		$result['task_row']=array();
		$all_approved = 1;
		$count_submitted=0;
		$i=0;
		foreach ($e as $key => $value) {
			//select rows
			$t_d_row=$this->db->query("SELECT task_time.*, projects.stage
		                  FROM task_time
		                  INNER JOIN projects ON projects.project_id=task_time.project_id
		                  WHERE user_id='".$in['user_id']."'
		                  AND date BETWEEN '".$week_start."' AND '".$week_end."'
		                  AND task_time.project_id='".$value['project_id']."'
		                  AND task_time.task_id='".$value['task_id']."' ORDER BY date ASC ");
			$d=1;
			$task_day_row=array();
			$billed_row=false;
			$approved_row=false;
			while($t_d_row->move_next()){
				$entries_sheet=$this->db->field("SELECT COUNT(id) FROM servicing_support_sheet WHERE task_time_id='".$t_d_row->f('task_time_id')."' AND user_id='".$t_d_row->f('user_id')."' AND notes!='' ");
				if($d > 7){
					break;
				}
				if( $t_d_row->f('hours'))
				{
					$is_pdf= true;
					
				}
				$count_submitted+=$t_d_row->f('submited');
				$i++;
				$day_id = 'tmp_day'.$d;
				$note = trim($t_d_row->f('notes'));		
				$temp_task_day_r=array(
					'TD_ID_TASK_DAY'=> $day_id,
				  	'TASK_DAY_ID' 	=> $t_d_row->f('task_time_id'),
					'HOURSB'      	=> $t_d_row->f('hours') ? ($t_d_row->f('project_status_rate') == 0 ? number_as_hour($t_d_row->f('hours')) : $t_d_row->f('hours')) : '',
					'HOURS'       	=> $t_d_row->f('hours'),
					'IS_BILLED'   	=> ($t_d_row->f('billed')==1 || $value['active']==0) ? true:false,
					'IS_SUBMITED' 	=> $t_d_row->f('submited') == 1 ? true:false,
					'SUBMITED'	  	=> $t_d_row->f('submited') == 1 ? 'submited':'',
					'BG_COLOR'		=> !empty($note) ? 'bg-warning' : ( date('D') == date('D',$t_d_row->f('date')) && (date('W') == date('W',$t_d_row->f('date'))) && (date('Y')==date('Y',$t_d_row->f('date'))) ? 'grayish2' : ''),
					'APP'			=> $t_d_row->f('approved') == 1 ? 'approvedish': ($t_d_row->f('submited') == 1 ? 'approvedish':''),
					'IS_APPROVED' 	=> $t_d_row->f('approved') == 1 ? true:false,
					'NO_PLUS'		=> !empty($note) ? 'no-plus' : '',
					'hourly_rate'	=> $t_d_row->f('project_status_rate') == 0 ? true : false,
					'change_hours'	=> $t_d_row->f('project_status_rate') == 0 ? 'hours' : 'days',
					'hide_spinner'	=> ($t_d_row->f('billed')==1 || $value['active']=='0' || $t_d_row->f('submited') == 1 || $t_d_row->f('approved') == 1 || $t_d_row->f('stage')==2 || $t_d_row->f('stage')==0) ? false : true, 
					'POSITION_DAY'	=> ($t_d_row->f('billed')==1 || $value['active']=='0' || $t_d_row->f('submited') == 1 || $t_d_row->f('approved') == 1 ) ? '' : 'margin-left:11px;float:left',
					'IS_CLOSED'		=> ($t_d_row->f('stage')=='2') ? true:false,
					'BILLED_IMG'	=> $t_d_row->f('billed')==1 ? true : false,
					'UNAPPROVED'	=> $t_d_row->f('unapproved')==1 ? true : false,
					'has_note'	=> $entries_sheet || $note ? true : false
			  	);
				if($t_d_row->f('billed')==1){
					$billed_row=true;
				}
				if($t_d_row->f('approved')==1){
					$approved_row=true;
				}
				if(!$approved_row){
					$all_approved =0;
				}
			  	array_push($task_day_row, $temp_task_day_r);
				$d++;
			}
			$in['hide_s'] = $value['submited'];
			$row_id = 'tmp'.$r;

			$t_row=array(
				'TR_ID_TASK'           		=> $row_id,
				'classN'				=> $value['contract_id'] ? 'contractId' : 'cId',
				'cId'					=> $value['c_id'],
				'project_id'			=> $value['project_id'],
			 	'PROJECT_LIST_NAME' 		=> $value['project_name'],
			 	'project_type'			=> $value['status_rate'],
			 	'HIDE_PROJECT_NAME'		=> in_array($value['c_id'], $showed_projects) ? false : true,
			 	'HIDE_PROJECT_NAME2'		=> in_array($value['project_id'], $showed_projects2) ? false : true,
			 	'PROJECT_CUSTOMER_NAME' 	=> $value['c_name'],
				'TASK_LIST_NAME'    		=> $value['contract_id'] ? ''  :$value['task_name'],
				'TOTAL'             		=> '&nbsp;',
				'HIDE_BILLED'			=> ($value['billed'] || $value['active']==0)? false : true,
				'HIDE_UNBILLED'			=> ($value['billed'] || $value['active']==0)? true : false,
				'HIDE_SUBMITED'			=> $value['submited'] ==1 ? true : false,
				'HIDE_SUBMITED_L'			=> $value['submited'] ==1 ? false : true,
				'items_border'			=> $r == $rows ? 'show_border' : ($value['c_id']!= $e[$key+1]['c_id'] ? 'show_border' : ''),
				'maintenance_icon_show'		=> $value['service'] == 1 ? 'maintenance_icon_show' : "",
				'billed'				=> $billed_row,
				'approved'				=> $approved_row,
				'task_day_row'			=> $task_day_row,
			);
			array_push($result['task_row'], $t_row);
			array_push($showed_projects,$value['c_id']);
			array_push($showed_projects2,$value['project_id']);
			$r++;
			if($value['submited'] == 1){
				$submited=1;
			}
		}

		if($all_approved){
			$status = 3;
			$status_text = gm('Approved');
		}else{
			if(!$count_submitted){
				$status = 0;
				$status_text = gm('Unsubmitted');
			}elseif($count_submitted<$i){
				$status = 4;
				$status_text = gm('Partially submitted');
			}elseif($count_submitted==$i){
				$status = 1;
				$status_text = gm('Submitted');
			}else{
				$status = 2;
				$status_text = gm('Rejected');
			}
		}

	/*	if($submited==0 && $all_approved=0){
			$status = 0;
			$status_text = gm('Unsubmitted');
		}elseif($submited){

		}*/

		$result['HIDE_TASKS']=true;
		if($r == 1){
			$result['HIDE_TASKS']=false;
			$in['info'] .= gm('There are no hours added.');
			if(date('W',$week_start) == date('W')){
				$prev_week_end = $week_start - 1;
				$prev_week_start = $prev_week_end - 604799;

		//select all tasks
				$prev_query = $this->db->query("SELECT task_time.user_id,task_time.project_id,task_time.task_id,task_time.billable,
				            task_time.customer_id AS c_id, task_time.project_status_rate, projects.stage
				            FROM task_time
				            INNER JOIN projects ON task_time.project_id = projects.project_id
				            WHERE task_time.user_id='".$in['user_id']."'
				            AND date BETWEEN '".$prev_week_start."' AND '".$prev_week_end."' AND projects.closed='0'
				            GROUP BY task_time.task_id
				            ORDER BY task_time.task_time_id
						   ");

				$i=0;
				while ($prev_query->next()) {
					$check_h = $this->db->field("SELECT SUM(hours) FROM task_time
											WHERE user_id='".$prev_query->f('user_id')."' AND
												  project_id='".$prev_query->f('project_id')."' AND
												  task_id='".$prev_query->f('task_id')."'
												  AND date BETWEEN '".$prev_week_start."' AND '".$prev_week_end."' ");
					if($check_h > 0 ){
						$days = $week_start;
						 $billable=$this->db->field("SELECT billable FROM tasks WHERE task_id='".$prev_query->f('task_id')."'");
						while ($days < $week_end ){
							if($prev_query->f('stage')==1){
								$query = $this->db->query("INSERT INTO task_time
													SET user_id='".$prev_query->f('user_id')."',
														project_id='".$prev_query->f('project_id')."',
														task_id='".$prev_query->f('task_id')."',
														customer_id='".$prev_query->f('c_id')."',
														date='".$days."',
														hours='0',
														billable='".$billable."',
														project_status_rate='".$prev_query->f('project_status_rate')."' ");
							}
							$days += 86400;
						}
						$i++;
					}
				}
				$prev_query = null;
			}
		}

		//now walk the days and display the info
		$j =1;
		//echo $week_start.' -- '.$week_end;
		$result['day_row']=array();
		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			$day_id = 'tmp_day'.$j;
			$d_row=array(
				'day_name'    	=> gm(date('D',$i)),
				'date_month'  	=> date('d',$i).' '.gm(date('M',$i)),
				'TIMESTAMP'   	=> $i,
				'TD_ID'       	=> $day_id,
				'NO_MARGIN'		=> $j == 7 ? 'no-margin' : '',
				'TODAY'		=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? 'today' : '',
				'HIDE_ARROW'	=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? '' : 'hide',
				'end_day'		=> date("N",$i)>5 ? true : false
			);
			array_push($result['day_row'], $d_row);
		    $j++;
		}

		$i=0;
		$result['history_row']=array();
		$activity = $this->db->query("SELECT * FROM timesheet_log WHERE user_id='".$in['user_id']."' AND start_date='".$week_start."' limit 5 ");
		while($activity->next()){
			$h_row=array(
				'HISTORY_DATE'		=>$activity->f('date'),
				'HISTORY_DESCRIPTION'	=>$activity->f('message'),
			);
			array_push($result['history_row'], $h_row);
			$i++;
		}
		if($i==0){
			$result['HIDE_LOG']	= false;
		}else {
			$result['HIDE_LOG']=$_SESSION['access_level'] == 1 ? true : false;
		}

		if(!$in['tab']){
			$in['tab'] = 1;
		}
		/*if($in['success']){
			$view->assign('MSG_SUCCESS','<div class="success">'.$in['success'].'</div>');
		}*/
		$add_row_box = false;

		if(date('W') == date("W",$week_start)){
			$day = '';
		}else{
			$day = '&start='.$week_start;
		}

		$user_dd = build_user_dd_timetracker($in['user_id'], $_SESSION['access_level']);
		//$user_name = get_user_name($in['user_id']);

		$partial_time=array();
		for($o=0;$o<=6;$o++){
			if(count($partial_time)==0){
				$partial_time[]=$week_start;
			}else if($o<6){
				$partial_time[$o]=strtotime("+1day",$partial_time[$o-1]);
			}else{
				$partial_time[]=$week_end;
			}
		}

		$partial_time_full=array();
		foreach($partial_time as $key => $val){
			$partial_time_full[$val]=date(ACCOUNT_DATE_FORMAT,$val);
		}

		switch ($status){
			case '0': //unsubmitted
				$result['btn_type1']	    	= 'primary';
				$result['btn_type2']	    	= 'secondary';
				$result['disabled1']	    	= false;
				$result['disabled2']	    	= false;
				$result['action1']	    		= 'approve';
				$result['action2']	    		= 'reject';
				$result['approve_text']	    	= gm('Approve timesheet');
			  	$result['reject_text']	    	= gm('Reject timesheet');
				break;
			case '1': //submitted
				$result['btn_type1']	    	= 'primary';
				$result['btn_type2']	    	= 'secondary';
				$result['disabled1']	    	= false;
				$result['disabled2']	    	= false;
				$result['action1']	    		= 'approve';
				$result['action2']	    		= 'reject';
				$result['approve_text']	    	= gm('Approve timesheet');
			  	$result['reject_text']	    	= gm('Reject timesheet');
				break;
			case '2': //rejected
				$result['btn_type1']	    	= 'plain';
				$result['btn_type2']	    	= 'secondary';
				$result['disabled1']	    	= false;
				$result['disabled2']	    	= true;
				$result['action1']	    		= 'resubmit';
				$result['action2']	    		= 'reject';
				$result['approve_text']	    	= gm('Re-submit');
			  	$result['reject_text']	    	= gm('Rejected timesheet');
				break;
			case '3': //approved
				$result['btn_type1']	    	= 'primary';
				$result['btn_type2']	    	= 'plain';
				$result['disabled1']	    	= true;
				$result['disabled2']	    	= false;
				$result['action1']	    		= 'approve';
				$result['action2']	    		= 'reopen';
				$result['approve_text']	    	=  gm('Approved timesheet');
			  	$result['reject_text']	    	=gm('Re-open');
				break;
			case '4': //partially submitted
				$result['btn_type1']	    	= 'primary';
				$result['btn_type2']	    	= 'secondary';
				$result['disabled1']	    	= false;
				$result['disabled2']	    	= false;
				$result['action1']	    		= 'approve';
				$result['action2']	    		= 'reject';
				$result['approve_text']	    	= gm('Approve timesheet');
			  	$result['reject_text']	    	= gm('Reject timesheet');
				break;
		}


			$result['PAGE_TITLE']	    	= gm('Timesheet for').' <span id="username">'.$user_dd[0].$user_dd[1][0].($user_dd[1][1] > 1 ? '<b id="change_user" class="'.($in['is_ajax'] == 1 ? 'hide': '').'" >&nbsp;</b>' : '').'</span>';
			$result['user_dd']		= $user_dd;
			$result['user_id']           	= $in['user_id'];
			$result['user_name']           	= get_user_name($in['user_id']);
			$result['PAG']              	= $in['is_ajax'] == 1 ? 'timesheet' : $in['pag'];
			$result['ADD_ROW_BOX']       	= $add_row_box;
			$result['NEXT_FUNCTION']     	= 'timetracker-timesheet-time-add_hour';
			$result['START']            	= $week_start;
			$result['START_DAY']		= $day;
			$result['week_s']        	= $week_start;
			$result['week_e']          	= $week_end;
			$result['HIDE_AJAX']         	= $in['is_ajax'] == 1 ? false: true;
			$result['HIDE_L']	        	= $_SESSION['access_level'] == 1 ? true : false;
			$result['DISPLAY_SAVE']      	= $submited == 1 ? false : true;
			$result['IS_AJAX']           	= $in['is_ajax'] == 1 ? '1': '2';
		  	$result['STYLE']             	= ACCOUNT_NUMBER_FORMAT;
		  	$result['tab']			= $in['tab'];
		  	$result['page_tip']		= $tips;
		  	$result['pick_date_format']	= pick_date_format();
		  	$result['TODAY_nr']		= $today_of_week;
		  	$result['allowadhoc']		= USE_ADHOC_TMS == '1'? true:false;
		  	$result['partial_submit']	= build_simple_dropdown($partial_time_full);
		  	$result['projects_list']	= $this->get_projects_list($in);
		  	$result['tasks_list']		= $this->get_tasks_list($in);
		  	$result['customers_list']	= $this->get_cc_list($in);
		  	$result['ARCHIVE_LINK']			= array('do'=>'timetracker--time-remove_submit','user_id'=>$in['user_id'],'week_start'=>$week_start);
			$result['APPROVE_LINK']		= array('do'=>'timetracker-timesheets-time-approve','user_id'=>$in['user_id'],'week_start'=>$week_start,'list'=>'1');
			$result['btn_type1']	    	= 'primary';
			$result['btn_type2']	    	= $all_approved?'plain':'secondary';
			$result['disabled1']	    	= $all_approved? true:false;
			$result['disabled2']	    	= false;
			$result['action1']	    	= 'approve';
			$result['action2']	    	= $all_approved?'reopen':'reject';
			$result['week_start']        	= $week_start;
			$result['week_end']          	= $week_end;
			$result['status']        		= $status;
			$result['status_text']          = $status_text;
			$result['approve_text']	    	=  $all_approved?gm('Approved timesheet'):gm('Approve timesheet');
		  	$result['reject_text']	    	= $all_approved? gm('Re-open'):gm('Reject timesheet');


		 //if we have a dummy task and an expense, we hide the buttons
		// because is already submited
		$count_dummy_task = $this->db->field("SELECT COUNT(task_time_id) FROM task_time
							WHERE task_time.user_id='".$in['user_id']."'
							AND date BETWEEN '".$week_start."' AND '".$week_end."'
							AND notes = 'no_task_just_expense' ");
		// echo $count_dummy_task;

		if($count_dummy_task > 0){
			$in['hide_s'] = 1 ;//hide shit from timesheet_expense
		}
		$result['hide_s']=$in['hide_s'];
		$result['hide_if_task_no_exp_submited']	= $count_dummy_task > 0 ? false : true;

		$user_info = $this->db_users->query("SELECT credentials, group_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		if($user_info->f('group_id')==1){
			$result['can_unlock']=true;
		}else{
			$credentials = explode(';',$user_info->f('credentials'));
			$is_timesheet_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timesheet_admin' ");
			if($is_timesheet_admin==1 && in_array('8',$credentials)){
				$result['can_unlock']=true;
			}else{
				$result['can_unlock']=false;
			}
		}
		$result['has_p']=in_array('3', perm::$allow_apps) ? true : false;	

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_19' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timetracker_admin' ");
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
				$has_rights = true;
				break;
			default:
				$has_rights = false;
				break;
		}
		$result['can_do']=$has_rights;	

		$this->out = $result;
	}

	public function get_Month(&$in){
		$in = $this->in;

		$result=array();

		if($in['start']){
			$result['start']=$in['start'];
			$now		= $in['start'];
		}else{
			$now 				= time();
		}

		$month = date('n',$now);
		$year = date('Y',$now);
		$month_t = mktime(0,0,0,$month,1,$year);
		if(date('I',$now)=='1'){
			$month_t+=3600;
		}

		$time_start = mktime(0,0,0,$month,1,$year);
		if(date('I',$now)=='1'){
			$time_start+=3600;
		}
		$time_end = mktime(0,0,0,$month+1,1,$year);
		if(date('I',$now)=='1'){
			$time_end+=3600;
		}

		$user_dd = build_user_dd_timetracker($in['user_id'], $_SESSION['access_level']);

		$result['month_txt']		= gm(date('F',$month_t));
		$result['user_dd']		= $user_dd;
		$result['user_id']		= $in['user_id'];
		$result['year']			= $year;
		$result['next']			= mktime(0,0,0,$month+1,1,$year);
		$result['prev']			= mktime(0,0,0,$month-1,1,$year);
		$result['PAGE_TITLE']		= '<span id="username">'.$user_dd[0].$user_dd[1][0].($user_dd[1][1] > 1 ? '<b id="change_user" class="'.($in['is_ajax'] == 1 ? 'hide': '').'" >&nbsp;</b>' : '').'</span>';
		$result['expense_pdf_link']	= 'index.php?do=timetracker-expenses_report_print&time_start='.$time_start.'&time_end='.$time_end.'&u_id='.$in['user_id'];
		$result['month_start']		= $time_start;
		$result['month_end']		= $time_end;
		$result['hide_s']			= $in['hide_s'];

		$month_days = date('t',$month_t);
		$month_first_day = date('N',$month_t);

		$result['month']=array();
		$i = 1;
		$day = 1;
		$loopweek = true;
		$q=0;
		$w=0;
		while ($loopweek) {
			$k = 1;
			$day_final=array('week'=>array());
			while ($k <= 7) {
				$expenses = 0;
				$text = $day;
				$hours = '';
				$days = ';';
				$bg = 'planning_empty';
				if($i == 1 && $k < $month_first_day){

					$date1=mktime(0,0,0,$month,$day-($month_first_day-$k),$year);
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$text = date('j',$date1);
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours).' <span>'.gm('hours').'</span>';
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days.' <span>'.gm('days').'</span>';
					}
				}
				if($day > $month_days){
					$date1=mktime(0,0,0,$month,$day,date('y'));
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$text = date('j',$date1);
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours). ' <span>'.gm('hours').'</span>';
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days. ' <span>'.gm('days').'</span>';
					}
					$day++;
				}
				if($text == $day){
					$date1=mktime(0,0,0,$month,$day,$year);
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours).' <span>'.gm('hours').'</span>';
						$w++;
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days.' <span>'.gm('days').'</span>';
						$w++;
					}
					$day++;
					$bg = '';
				}
				$temp_data=array(
					'text'		=> $text,
					'hours'		=> $hours,
					'days'		=> $days,
					'expens'		=> $expenses ? place_currency(display_number($expenses)) : '',
					'bg'			=> $bg,
					'last'		=> $k == 7 ? 'last' : '',
				);
				array_push($day_final['week'], $temp_data);
				$k++;
				if($day > $month_days){
					$loopweek = false;
				}
			}
			array_push($result['month'], $day_final);
			$i++;
		}

		if(!$in['tab']){
			$in['tab'] = 1;
		}

		$result['tab']		= $in['tab'];

		$this->out = $result;
	}

	public function get_projects_list(&$in){

		$q = strtolower($in["term"]);

		$filter ='';
		if($in['projects_ids'] && !empty($in['projects_ids'])){
			$filter .=" AND projects.project_id IN (".$in['projects_ids'].") ";
		}else if($in['user_id']){
			$filter .=" AND project_user.user_id='".$in['user_id']."'";
			if($_SESSION['group'] != 'admin' && $in['user_id'] != $_SESSION['u_id']){
				$is_manager = $this->db->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");
				if(!empty($is_manager)){
					$filter .=" AND projects.project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
				}
			}
		}
		if($q){
			$filter .=" AND (projects.name LIKE '%".$q."%' OR projects.company_name LIKE '%".$q."%') ";
		}

		if($in['exp'] == 1){
			$filter .= " AND add_expense='1' ";
		}

		$this->db->query("SELECT projects.project_id,projects.name AS p_name,projects.active, projects.customer_id, projects.company_name AS c_name
		            FROM projects
		            INNER JOIN project_user ON project_user.project_id=projects.project_id
		            WHERE projects.active=1 AND projects.closed='0' AND project_user.active='1' AND projects.stage='1' $filter LIMIT 5
		          ");
		while($this->db->move_next()){
		  $projects[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('project_id');
		  $customer[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('customer_id');
		}

		$result = array();
		if($projects){
			foreach ($projects as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "c_id" => $customer[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "c_id" => $customer[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "name" => gm('No matches')));
		}
		return $result;
	}

	public function get_tasks_list(&$in){

		$q = strtolower($in["term"]);

		$filter ='';

		if($q){
			$filter .=" AND tasks.task_name LIKE '%".$q."%'";
		}
		if($in['tasks_ids'] && !empty($in['tasks_ids'])){
			$filter.=" AND task_id IN (".$in['tasks_ids'].") ";
		}

		if($in['project_id']){
			$bil_type = $this->db->field("SELECT billable_type FROM projects WHERE project_id='".$in['project_id']."' ");
			if($bil_type == 7){
				$filter .= " AND default_task_id IN (SELECT func_id FROM user_function WHERE user_id='".$in['user_id']."' ) ";
			}

			$this->db->query("SELECT tasks.task_id,tasks.project_id,tasks.task_name,tasks.billable FROM tasks WHERE tasks.project_id='".$in["project_id"]."' AND closed='0' $filter");
			while($this->db->move_next()){
				$t_name = str_replace(array("\r\n", "\r"), " ", $this->db->f('task_name'));
				$tasks[$t_name]= $this->db->f('task_id');
				$billable[$t_name] = $this->db->f('billable');
			}
		}else if($in['c_id']){
			$task = $this->db->query("SELECT * FROM default_data WHERE default_main_id='0' AND type='task_no' AND active='1' ");
			while($task->next()){
				$default_fields = $this->db->field("SELECT value FROM default_data WHERE default_main_id='".$task->f('default_id')."' AND default_name='task_billable' ");
				$tasks[$task->f('default_name')]= $task->f('default_id');
				$billable[$task->f('default_name')] = $default_fields ? true : false;
			}
		}

		$result = array();
		if($tasks){
			foreach ($tasks as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "billable" => $billable[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "billable" => $billable[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}
		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_19' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timetracker_admin' ");
		if(($perm_admin || $perm_manager || $_SESSION['group'] == 'admin') && $in['c_id']){
			array_push($result,array('id'=>'99999999999','name'=>''));
		}
		return $result;
	} 

	public function get_cc_list(&$in){
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 5")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> $symbol,
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']
			);
		}
		if(in_array('1', perm::$allow_apps)){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999','value'=>''));
			}
		}
		
		return $result;
	}

}

	$timesheet = new Timesheet($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $timesheet->output($timesheet->$fname($in));
	}

	$timesheet->get_Week();
	$timesheet->output();

?>