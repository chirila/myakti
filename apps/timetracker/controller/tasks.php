<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

class TimetrackTask extends Controller{

	public function getList(){
		$in=$this->in;
		$output=array('query'=>array());

		$l_r = ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$filter = " default_main_id='0' AND type='task_no' ";
		$order_by = " ORDER BY default_name ";
		if ($in['archived'] == 1) {
            $filter.= " AND active=1 ";
        } elseif ($in['archived'] == -1) {
            $filter.= " AND active=0 ";
        } elseif ($in['archived'] == '') {
		    $filter.= '';
        }

		if(!empty($in['search'])){
			$filter .= " AND default_name LIKE '%".$in['search']."%' ";
		}

		$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
		
		if(!empty($in['order_by'])){
		   
				$order = " ASC ";
				if($in['desc'] == '1' || $in['desc']=='true'){
				    $order = " DESC ";
				}
				if($in['order_by']=='task_rate' || $in['order_by']=='daily_rate'){
					$order_by='';
					$filter_limit =' ';
				}else{
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
					$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
					$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
				}
	  
		}

		$max_rows =  $this->db->field("SELECT count(default_id) FROM default_data WHERE ".$filter );
		$output['max_rows']=$max_rows;
		$default_task = $this->db->query("SELECT *,default_name AS name FROM default_data WHERE ".$filter." ".$order_by.$filter_limit);
		$j=0;
		while($default_task->next() ){
			$item=array(
				'name'					=> $default_task->f('default_name'),
				'task_rate'					=> '',
				'daily_rate'				=> '',
				'task_rate_ord'				=> '',
				'daily_rate_ord'			=> '',
				'task_billable'				=> false,
				'id'						=> $default_task->f('default_id'),
				'is_active'     			=> $default_task->f('active')? true : false,
				'delete_link'				=> array('do'=>'timetracker-tasks-project-action','task_id'=>$default_task->f('default_id'),'action_type'=>'deleted'),
				'archive_link'				=> array('do'=>'timetracker-tasks-project-action','task_id'=>$default_task->f('default_id'),'action_type'=>'archived'),
				'activate_link'				=> array('do'=>'timetracker-tasks-project-action','task_id'=>$default_task->f('default_id'),'action_type'=>'activated'),
			);

		    $task_line = $this->db->query("SELECT * FROM default_data WHERE default_main_id='".$default_task->f('default_id')."' ");
		    while ($task_line->next()) {
		    	switch ($task_line->f('default_name')) {
		    		case 'task_billable':
		    			$item['task_billable']=$task_line->f('value') == 'yes' ? true : false;
		    			break;
		    		case 'task_rate':
		    			$item['task_rate']=place_currency(display_number($task_line->f('value')));
		    			$item['task_rate_ord']=$task_line->f('value');
		    			break;
		    		case 'daily_rate':
		    			$item['daily_rate']=place_currency(display_number($task_line->f('value')));
		    			$item['daily_rate_ord']=$task_line->f('value');
		    			break;
		    		default:
		    			$item[$task_line->f('default_name')]=is_numeric($task_line->f('value')) ? display_number_var_dec($task_line->f('value')) : $task_line->f('value');
		    			break;
		    	}
		    }
		    array_push($output['query'],$item);
			$j++;
		}

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_19' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timetracker_admin' ");
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
				$has_rights = true;
				break;
			default:
				$has_rights = false;
				break;
		}
		$output['can_do']=$has_rights;

		if($in['order_by']=='task_rate' || $in['order_by']=='daily_rate'){

	    	if($order ==' ASC '){
		       $exo = array_sort($output['query'], $in['order_by'].'_ord', SORT_ASC);    
		    }else{
		       $exo = array_sort($output['query'], $in['order_by'].'_ord', SORT_DESC);
		    }
		    $exo = array_slice( $exo, $offset*$l_r, $l_r);
		    $output['query']=array();
		       foreach ($exo as $key => $value) {
		           array_push($output['query'], $value);
		       }
		    
		}
        $output['lr'] = $l_r;
        $this->out = $output;
	}

	public function get_task(&$in){
		$result=array();

		if(!$in['task_id'] || $in['task_id']=='tmp'){
			$do_next = 'timetracker--project-default_task_add';
			$result['task_rate']=display_number(0);
			$result['daily_rate']=display_number(0);
			$result['title']=gm('Add task');
		}else{
			$do_next = 'timetracker--project-default_task_update';
			$result['task_name'] = $this->db->field("SELECT default_name FROM default_data WHERE default_id='".$in['task_id']."' ");
			$result['task_rate'] = display_number_var_dec($this->db->field("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' "));
			$result['daily_rate'] = display_number_var_dec($this->db->field("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='daily_rate' "));
			$billable = $this->db->field("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
			$result['task_billable']=$billable == 'yes' ? true : false;
			$result['task_id']=$in['task_id'];
			$result['title']=gm('Edit task');
		}
		$result['do_next']=$do_next;
		return $result;
	}


}

	$t_task = new TimetrackTask($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $t_task->output($t_task->$fname($in));
	}

	$t_task->getList();
	$t_task->output();
	
?>