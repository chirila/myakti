<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

	if(!is_numeric($in['start'])){
		$now 				= time();
		$today_of_week 		= date("N", $now);
		$month        		= date('n');
		$year         		= date('Y');
		$week_start 		= mktime(0,0,0,$month,(date('j')-$today_of_week+1),$year);
		$week_end 			= $week_start + 604799;
	} else {
		$week_start 		= $in['start'];
		$week_end 			= $in['start'] + 604799;
	}
	$week_d=build_days_of_the_week($week_start,$week_start);
	$day=1;
	foreach($week_d as $key=>$value){
		$week_d[$key]['disabled']=$in['partial_sub']=='0' ? false : ($day>$in['partial_sub'] ? true : false);
		$day++;
	}

	$result=array(
		'SELECT_DAY'		=> $week_d,
		'user_id'			=> $in['user_id'],
		'week_start'		=> (string)$week_start,
		//'week_end'			=> (string)($week_end-86399)
		'week_end'			=> $in['partial_sub']=='0' ? (string)($week_end-86399) : (string)($week_start+($in['partial_sub']-1)*86400),
	);


return json_out($result);
?>