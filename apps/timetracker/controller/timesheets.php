<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class Timesheets extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Data(){
		$in=$this->in;
		$result=array();

		$separator = ACCOUNT_NUMBER_FORMAT;

		$filter = ' 1=1 ';
		$filter2 = ' ';
		$filter_u = ' 1=1 ';
		$ORD = ' ASC ';
		$hide_filter = '';
		$hide_admin = true;
		$hide_admin1 = true;

		$is_project_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' ");
		if(!$is_project_admin && $_SESSION['group']=='user'){
			$users_array = array();
			$users_list = '';
			$filter3 = " AND project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
			array_push($users_array, $_SESSION['u_id']);
			$users_ids = $this->db->query("SELECT user_id FROM project_user WHERE 1=1 $filter3 GROUP BY user_id ");
			while ($users_ids->next()) {
				if(!in_array($users_ids->f('user_id'), $users_array)){
					array_push($users_array, $users_ids->f('user_id'));
				}
			}
			foreach ($users_array as $key) {
				$users_list .= $key.',';
			}
			if(!$in['users_id'] || empty($in['users_id'])){
				$in['users_id'] = $_SESSION['u_id'];
			}
			$filter_u .= " AND project_user.user_id IN (".rtrim($users_list,',').") ";
			$hide_admin = false;
			$hide_admin1 = false;
		}

		if(!isset($in['view'])){
			$in['view']='0';
		}

		if($in['view'] == '' || $in['view']=='0' ){
			$current_start=mktime(0,0,0,date('n'),(date('j')-date('N')+1),date('Y'));
			if(date('I',$current_start)=='1'){
				$current_start+=3600;
			}
			$last_5weeks=$current_start-86400*28;
			$filter2 .="  AND date>='".$last_5weeks."' ";
			$hide_filter = 'none';		
		}else if($in['view'] == '1'){
			//$current_start=mktime(0,0,0,date('n'),(1-date('N')+1),date('Y'));	
			$current_start=mktime(0,0,0,date('n'),1,date('Y'));		
			if(date('I',$current_start)=='1'){
				$current_start+=3600;
			}	
			if(date('w',$current_start)>0){
				$current_start-=(date('w',$current_start)-1)*86400;
			}else{
				$current_start-=6*86400;
			}		
			$filter2 .="  AND date>='".$current_start."' ";					
		}else if($in['view'] == '2'){
			/*$current_start=mktime(0,0,0,date('n'),(1-date('N')+1),date('Y'));
			$current_start=strtotime('-1 month',$current_start);
			$current_start=mktime(0,0,0,date('n',$current_start),(1-date('N',$current_start)+1),date('Y',$current_start));
			if(date('I',$current_start)=='1'){
				$current_start+=3600;
			}*/
			$current_start=mktime(0,0,0,date('n'),1,date('Y'));
			$current_start=strtotime("-1 month",$current_start);
			if(date('I',$current_start)=='1'){
				$current_start+=3600;
			}
			if(date('w',$current_start)>0){
				$current_start-=(date('w',$current_start)-1)*86400;
			}else{
				$current_start-=6*86400;
			}			
			$end_month_init=strtotime("-1 month",time());
			$end_month=mktime(23,59,59,date('n',$end_month_init),date('t',$end_month_init),date('Y',$end_month_init));
			if(date('I',$end_month)=='1'){
				$end_month+=3600;
			}
			if(date('w',$end_month)>0){
				$end_month+=(7-date('w',$end_month))*86400;
			}
			//$end_month=mktime(0,0,0,date('n'),(1-date('N')+1),date('Y'));	
			/*if(date('I',$end_month)=='1'){
				$end_month+=3600;
			}
			$end_month-=86401;
			if(date('n',$end_month)<date('t',$end_month_init)){
				$end_month+=86400*7;
			}*/
			$filter2 .="  AND date BETWEEN '".$current_start."' AND '".$end_month."' ";	
		}else if($in['view'] == '3'){
			$in['from_w']=strtotime($in['from_w']);
			$in['to_w']=strtotime($in['to_w']);
			//$ORD = ' DESC ';
			//$filter .=" AND approved='1' AND submited='1' ";
			//$filter_u = ' 1=2 ';
			if($in['from_w'] && $in['to_w']){
				if(date('D',$in['from_w']) != 'Mon'){
					$in['from_w'] = mktime(0,0,0,date('n',$in['from_w']),(date('j',$in['from_w'])-date('N',$in['from_w'])+1),date('Y',$in['from_w']));
				}
				$in['to_w'] = mktime(0,0,0,date('n',$in['to_w']),(date('j',$in['to_w'])+7-date('N',$in['to_w'])),date('Y',$in['to_w']));
				$filter2 = " AND date BETWEEN '".$in['from_w']."' AND '".$in['to_w']."' ";
				$filter_u = ' 1=1 ';
			}else if( $in['from_w'] && !$in['to_w']){
				if(date('D',$in['from_w']) != 'Mon'){
					$in['from_w'] = mktime(0,0,0,date('n',$in['from_w']),(date('j',$in['from_w'])-date('N',$in['from_w'])+1),date('Y',$in['from_w']));
				}
				$filter2 = " AND date >= '".$in['from_w']."' ";
				$filter_u = ' 1=1 ';
			}elseif($in['to_w'] && !$in['from_w']){
				$in['to_w'] = mktime(0,0,0,date('n',$in['to_w']),(date('j',$in['to_w'])+7-date('N',$in['to_w'])),date('Y',$in['to_w']));
				$filter2 = " AND date < '".$in['to_w']."' ";
				$filter_u = ' 1=1 ';
			}
		}

		if(isset($in['users_id']) && !empty($in['users_id'])){
			$user_t = $in['users_id'];
			$filter2 .= " AND user_id='".$user_t."' ";
			$filter_u = " user_id='".$user_t."' ";
		} else if(empty($in['users_id'])){
			$user_t = '0';
		}

		if(!isset($in['status'])){
			$in['status']='5';
		}

		if($in['status']=='1'){
			$filter .= " AND submited='0' ";
		}else if($in['status']=='2'){
			$filter .= " AND submited='1' AND approved='0' ";
		}else if($in['status']=='3'){
			$filter .= " AND submited='2' AND approved='0' ";
		}else if($in['status']=='4'){
			$filter .= " AND submited='1' AND approved='1' ";
		}else if($in['status']=='5'){
			$filter .= " AND approved!='1' ";
		}

		/* comments */
		$comments = array();
		$comment = $this->db->query("SELECT * FROM t_comments ");
		while ($comment->next()) {
			$comments[$comment->f('user_id')][$comment->f('start_date')] = $comment->f('comment');
		}
		$comment = null;
		/* comments */

		/* expenses */
		$exp = array();
		$expense_amount = 0;
		$expense = $this->db->query("SELECT * FROM project_expenses INNER JOIN expense ON project_expenses.expense_id=expense.expense_id WHERE 1=1 $filter2 ");
		while ($expense->next()) {
			$first_m = mktime(0,0,0,date('n',$expense->f('date')),(date('j',$expense->f('date'))-date('N',$expense->f('date'))+1),date('Y',$expense->f('date')));
			if(!array_key_exists($expense->f('user_id'), $exp)){
				$exp[$expense->f('user_id')] = array();
			}
			if(!array_key_exists($first_m,$exp[$expense->f('user_id')])){
				$exp[$expense->f('user_id')][$first_m] = 0;
			}
			if($expense->f('unit_price')){
				$exp[$expense->f('user_id')][$first_m] += $expense->f('amount')*$expense->f('unit_price');
			}else{
				$exp[$expense->f('user_id')][$first_m] += $expense->f('amount');
			}
		}
		$expense = null;
		/* expenses */

		$user_name = array();
		$inactive = array();
		$inactive_user = $this->db_users->query("SELECT user_id FROM users WHERE active='0' AND database_name='".DATABASE_NAME."' ");
		while ($inactive_user->next()) {
			array_push($inactive, $inactive_user->f('user_id'));
		}
		$inactive_user = null;
		$weeks = array();		#array with the weeks
		$i = 0;
		/* optimization */
		#we limit the number of results else the script take to much memory
		$limit = 5000;	# suggested number 5000
		$loffset = 0;
		$exec_loop = false;
		do {
			$k = 0;
			$exec_loop = false;
			$data = $this->db->query("SELECT user_id, hours, approved, `date`, submited, project_status_rate,task_time_id FROM  `task_time` WHERE $filter $filter2 ORDER BY `date`,`task_time_id` $ORD limit ".($loffset*$limit).", ".$limit." ");

			while ($data->next()) {
				$first_m = mktime(0,0,0,date('n',$data->f('date')),(date('j',$data->f('date'))-date('N',$data->f('date'))+1),date('Y',$data->f('date')));
				if(!array_key_exists($data->f('user_id'), $weeks)){
					$weeks[$data->f('user_id')] = array();
				}
				if(!array_key_exists($first_m,$weeks[$data->f('user_id')])){
					# h - hours, a - approved hours, t - days, c - comment, e - expenses, s - submited hours, su - submited in week
					$weeks[$data->f('user_id')][$first_m] = array();
					$weeks[$data->f('user_id')][$first_m]['h'] = 0;
					$weeks[$data->f('user_id')][$first_m]['a'] = 0;
					$weeks[$data->f('user_id')][$first_m]['days'] = 0;
					//$weeks[$data->f('user_id')][$first_m]['app_days'] = 0;
					$weeks[$data->f('user_id')][$first_m]['t'] = 0;
					$weeks[$data->f('user_id')][$first_m]['c'] = $comments[$data->f('user_id')][$first_m];
					$weeks[$data->f('user_id')][$first_m]['e'] = $exp[$data->f('user_id')][$first_m];
					$weeks[$data->f('user_id')][$first_m]['s'] = $data->f('submited');
					$weeks[$data->f('user_id')][$first_m]['su'] = 0;

				}


				if($data->f('submited')){
					$weeks[$data->f('user_id')][$first_m]['su']++;
				}

				if($data->f('project_status_rate') ==0){
					/*if($data->f('approved') != 1 && $in['view'] != '1'){
						$weeks[$data->f('user_id')][$first_m]['h'] += $data->f('hours');
					}
					if($data->f('approved') == 1 && $data->f('submited') == 1 && $in['view'] == '1' ){
						$weeks[$data->f('user_id')][$first_m]['h'] += $data->f('hours');
					}*/
					$weeks[$data->f('user_id')][$first_m]['h'] += $data->f('hours');
				}else{
					/*if($data->f('approved') != 1 && $in['view'] != '1'){
						$weeks[$data->f('user_id')][$first_m]['days'] += $data->f('hours');
					}

					if($data->f('approved') == 1 && $data->f('submited') == 1 && $in['view'] == '1' ){
						$weeks[$data->f('user_id')][$first_m]['days'] += $data->f('hours');
					}*/	
					$weeks[$data->f('user_id')][$first_m]['days'] += $data->f('hours');
				}
				if($data->f('approved')){
					$weeks[$data->f('user_id')][$first_m]['a'] ++;
					$weeks[$data->f('user_id')][$first_m]['s'] = 3;
				}
				if($data->f('submited') == 2){
					$weeks[$data->f('user_id')][$first_m]['s'] = 2;
				}

				if ($weeks[$data->f('user_id')][$first_m]['s'] == 1 && $data->f('submited') == 0) {
					$weeks[$data->f('user_id')][$first_m]['s'] = 0;
				}
				$weeks[$data->f('user_id')][$first_m]['t'] ++;

				$k++;
			}
			$data = null;
			if($k==$limit){
				$exec_loop = true;
			}else{
				break;
			}
			$loffset++;
		} while($exec_loop == true);
			
		/* here we sort the users */
		$w = array();
		$u_name = $this->db->query("SELECT user_name, user_id FROM project_user WHERE $filter_u GROUP BY user_id ORDER BY user_name");
		while ($u_name->next()) {
			$user_name[$u_name->f('user_id')] = $u_name->f('user_name');
			$w[$u_name->f('user_id')] = $weeks[$u_name->f('user_id')];
		}
		$weeks = null;
		$u_name = null;
		//console::log($w);
		$result['project_row']=array();
		$users_list=array();
		$navigator_list=array();

		/* here we sort the users */
		foreach ($w as $key => $value) {
			if(in_array($key, $inactive)){
				continue;
			}
			if(!$value){
				continue;
			}
			$j =0;
			$time_row=array();
			foreach ($value as $k => $v) {
				$week_start = $k;
				$week_end = $week_start+604799;
				if(date('H',$week_start) != '00'){
					if(date('H',$week_start) == '23' ){
						$week_start 		= $week_start + 3600;
						$week_end 			= $week_start + 604799;
						if(date('H',$week_end) == '22' ){
							$week_end= $week_end+ 3600;
						}
					}else {
						$week_start 		= $week_start - 3600;
						$week_end 			= $week_start + 604799;
						if(date('H',$week_end) == '00' ){
							$week_end= $week_end - 3600;
						}
					}
				}else{
					$week_start 		= $week_start;
					$week_end 			= $week_start + 604799;
					if(date('H',$week_end) =='00'){
						$week_end 			= $week_start + 604799 -3600;
					}
				}

				$app = $v['t'];
				$app2 = $v['a'];
				/*if($in['view'] != '1' && $app == $app2 && $v['s'] == 1){
					continue;
				}*/
				if($v['t']%7!=0 && $in['status']=='2'){
					continue;
				}
				/*switch ($v['s']){
					case '0':
						$submited= gm('Unsubmited');
						$color = 'gray';
						break;
					case '1':
						$submited= gm('Submitted');
						$color = 'orange';
						break;
					case '2':
						$submited= gm('Rejected');
						$color = 'red';
						break;
					case '3':
						$submited= $in['status']=='2' ? gm('Submitted') : gm('Approved');
						$color = $in['status']=='2' ? 'orange' : 'green';
						break;
				}*/
				if(!$in['status']){
					if($v['s']==2){
						$submited= gm('Rejected');
						$color = 'red';
					}else if($v['su']%7==0){
						if($v['s']==3 && $v['a']%7==0){
							$submited= gm('Approved');
							$color = 'green';
						}else if($v['s']==3 && $v['su']%7==0){
							$submited= gm('Submitted');
							$color = 'orange';
						}else if($v['s']==1 && $v['su']%7==0){
							$submited= gm('Submitted');
							$color = 'orange';
						}else{
							$submited= gm('Unsubmited');
							$color = 'gray';
						}
					}else{
						$submited= gm('Unsubmited');
						$color = 'gray';
					}
				}else{
					switch ($v['s']){
						case '0':
							$submited= gm('Unsubmited');
							$color = 'gray';
							break;
						case '1':
							$submited= gm('Submitted');
							$color = 'orange';
							break;
						case '2':
							$submited= gm('Rejected');
							$color = 'red';
							break;
						case '3':
							$submited=  gm('Approved');
							$color =  'green';
							/*$submited= $in['status']=='2' ? gm('Submitted') : gm('Approved');
							$color = $in['status']=='2' ? 'orange' : 'green';*/
							break;
					}
				}
				/*if(date('W') == date('W',$k) && date('Y') == date('Y',$k)){
					if($v['s']==0){
						$submited = gm('In progress');
						$color = 'gray';
					}
				}*/
				$expense_amount = $v['e'];
				$comment = $v['c'];

				$partial_data=$in['status'] ? '' : ($app2 > 0 && $v['a']%7 != 0 ? ' <i>'.gm('Partial approved').'</i>' : ($v['su'] == 0 || $v['su']%7 == 0 ? '' : '<i>'.gm('Partially submitted').'</i>'));

				if($partial_data){
					$submited=$partial_data;
					$color = 'orange';
				}

				# don't do queries here!!!
				$temp_trow=array(
					//'STATUS'			=> $app2 > 0 && $v['a']%7 != 0 ? $submited.' <i>'.gm('Partial approved').'</i>' : $submited,
					//'PARTIAL_SUBMIT'	=> $v['su'] == 0 || $v['su']%7 == 0 ? '' : '<i>'.gm('Partially submitted').'</i>',
					'STATUS'			=> $submited,
					//'PARTIAL_SUBMIT'	=> $partial_data,
					'PARTIAL_SUBMIT'	=> '',
					'PERIOD'			=> date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end),
					'USER_ID'			=> $key,
					'START_W'			=> $week_start,
					'HOURS'				=> number_as_hour($v['h'],2,substr($separator, -1),substr($separator, 0)),
					'DAYS'				=> $v['days'],
					'ARCHIVE_LINK'		=> array('do'=>'timetracker--time-remove_submit','user_id'=>$key,'week_start'=>$week_start),
					'APPROVE_LINK'		=> array('do'=>'timetracker-timesheets-time-approve','user_id'=>$key,'week_start'=>$week_start,'list'=>'1'),
					'APPROVED'			=> $app2 == $app && $v['s'] == 1 ? 'green' : $color,
					'HIDE_LINKS'		=> 'hide',
					'COMMENTS'			=> $comment ? $comment : '',
					//'VIEW_LINK'			=> ($app2 > 0 || $v['s'] == 0 || $submited == gm('Rejected')) ? false : true,
					'VIEW_LINK'			=> ($app2 > 0 || $v['s'] == 0 || $submited == gm('Rejected')) ? false : true,
					'VIEW_P_APP'		=> $v['s'] == 0 || $submited == gm('Rejected') ? true : false,
					'VIEW_LINK2'		=> $v['s'] == 1 && $v['t']%7==0 ? true : false,
					'VIEW_REMIND_LINK'	=> $v['s'] == '0' || $v['s'] == '2' ? true : false,
					'REMIND_LINK'		=> array('do'=>'timetracker-timesheets-time-notice','user_id'=>$key,'week_start'=>$week_start),
					'WEEK'				=> $week_start,
					'DELETE_LINK'		=> $in['view'] == '1' ? 'undo' : 'delete',
					'REJECT_TEXT'		=> $app2 > 0 ? gm('Re-Open') : gm('Reject'),
					'EXPENSES'			=> $expense_amount ? place_currency(display_number($expense_amount)) : '&nbsp;',
					'HIDE_ADMIN'		=> $hide_admin,
					'HIDE_SUBMIT'		=> $in['view'] ? true : false,
					'reject_link'		=> array('do'=>'timetracker-timesheets-time-remove_accept','user_id'=>$key,'week_start'=>$week_start),
					'view'				=> $in['view'] == '1' ? true : false,
					'is_admin'			=> $_SESSION['group'] == 'admin' ? true : false,
					'is_approve'		=> $app2 > 0 && $v['s'] != '2' ? true : false,
					'can_delete'		=> ($v['t']==7 && $v['su']==0 && $v['h']==0 && $v['days']==0) ? true : false,
					'partial_sub'		=> $v['su'],
					'list_order'		=> $i,
				);
				array_push($time_row,$temp_trow);
				array_push($navigator_list,$key.'-'.$week_start);
				$i++;
				$j++;
			}
			if(!empty($time_row)){
				array_push($users_list,array('id'=>$key,'name'=>$user_name[$key]));
				$temp_prow=array(
					'PROJECT'		=> $user_name[$key],
					'u_id'		=> $key,
					'time_row'		=> $time_row,
					);
				array_push($result['project_row'],$temp_prow);
			}		
		}
		//console::log($navigator_list);
		$instance = aktiTimesheet::getInstance();
		$nav_li=aktiTimesheet::setList($navigator_list);
		//console::log($nav_li);
		//$prev_item=aktiTimesheet::get('prev','11-1534712400');
		//console::log($prev_item);

		$user_dd = build_user_dd_timetracker($in['user_id'], $_SESSION['access_level']);

		$result['view']			= $in['view'];
		$result['users_list']		= $user_dd;
		$result['FILTER_BOX']		= $hide_filter;
		$result['users_id']		= $user_t;
		$result['HIDE_ADMIN1']		= $hide_admin1;
		$result['pick_date_format']	= pick_date_format();

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_19' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timetracker_admin' ");
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
				$has_rights = true;
				break;
			default:
				$has_rights = false;
				break;
		}
		$result['can_do']=$has_rights;

		$this->out = $result;
	}

}
	$timesheet = new Timesheets($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $timesheet->output($timesheet->$fname($in));
	}

	$timesheet->get_Data();
	$timesheet->output();

?>