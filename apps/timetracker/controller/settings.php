<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_expenses($in,$showin=true,$exit=true){
		$db = new sqldb();
		$groups = $db->query("SELECT * FROM expense");
		$i=0;
		$data = array( 'expenses'=>array());
		while ($groups->next()) {
			$u_price = $groups->f('unit_price');
			array_push($data['expenses'], array(
				"NAME"			=> $groups->f('name'),
				"Q_ID"			=> $groups->f('expense_id'),
				"PRICE"			=> empty($u_price) ? '&nbsp;' : $u_price.' / '.$groups->f('unit'),
				"PRICE_NORMAL"	=> $u_price,
				"PER"			=> $groups->f('unit'),
				'HIDE_DELETE'	=> $groups->f('is_default') == 1 ? true : false,
				'xget'			=> 'expenses',
			));
			$i++;
		}


		return json_out($data, $showin,$exit);
	}
	function get_name_convention($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db->query("SELECT value, constant_name FROM settings WHERE constant_name ='ACCOUNT_PROJECT_START' OR constant_name='ACCOUNT_PROJECT_DIGIT_NR' OR constant_name='ACCOUNT_PROJECT_DEL' OR constant_name='ACCOUNT_PROJECT_REF' ");
		$data = array( 'name_convention'=>array());
		while ($db->move_next()) {
			$const[$db->f('constant_name')] = $db->f('value');
		}
		$data['name_convention']=array(
			'ACT'						=> 'project-settings-project-project_naming',
			'ACCOUNT_PROJECT_START' 	=> $const['ACCOUNT_PROJECT_START'],
			'ACCOUNT_PROJECT_DIGIT_NR'	=> $const['ACCOUNT_PROJECT_DIGIT_NR'],
			'ACCOUNT_PROJECT_REF'		=> $const['ACCOUNT_PROJECT_DEL'],
			'CHECKED'					=> $const['ACCOUNT_PROJECT_REF'] ? "CHECKED" : '',
		);

		return json_out($data, $showin,$exit);
	}
	function get_emailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array(),'message_labels'=>array());
		$default_lang = $db->field("SELECT code FROM pim_lang WHERE sort_order = '1' ");
		if(!$in['lang_code']){
				$in['lang_code'] = $default_lang;
		}
		$timesheet = $db->query("SELECT * FROM sys_message WHERE name='timesheetmess' AND lang_code='".$in['lang_code']."' ");
		if(!$timesheet->move_next()){
			$timesheet->query("SELECT * FROM sys_message WHERE name='timesheetmess' AND lang_code='en' ");
			$timesheet->move_next();
		}
		$reject = $db->query("SELECT * FROM sys_message WHERE name='timesheet_rejectmess' AND lang_code='".$in['lang_code']."' ");
		if(!$reject->move_next()){
			$reject->query("SELECT * FROM sys_message WHERE name='timesheet_rejectmess' AND lang_code='en' ");
			$reject->move_next();
		}

		$reminder = $db->query("SELECT * FROM sys_message WHERE name='timesheet_remindermess'  AND lang_code='".$in['lang_code']."' ");
		if(!$reminder->move_next()){
			$reminder->query("SELECT * FROM sys_message WHERE name='timesheet_remindermess' AND lang_code='en' ");
			$reminder->move_next();
		}

		$aproved = $db->query("SELECT * FROM sys_message WHERE name='timesheet_approvemess'  AND lang_code='".$in['lang_code']."' ");
		if(!$aproved->move_next()){
			$aproved->query("SELECT * FROM sys_message WHERE name='timesheet_approvemess' AND lang_code='en' ");
			$aproved->move_next();
		}
		
		$data['message']= array(
				'SUBJECT_TIMESHEET'      	=> $timesheet->f('subject'),
				'TEXT_TIMESHEET'         	=> $timesheet->f('html_content'),	
				'SUBJECT_APPROVED'      	=> $aproved->f('subject'),
				'TEXT_APPROVED'         	=> $aproved->f('text'),	
				'SUBJECT_REMINDER'      	=> $reminder->f('subject'),
				'TEXT_REMINDER'         	=> $reminder->f('text'),
				'SUBJECT_REJECT'        	=> $reject->f('subject'),
				'TEXT_REJECT'           	=> $reject->f('text'),
				'LANG_DD'					=> build_pdf_language_code_dd($in['lang_code'],true),
				'LANG_ID'					=> $in['lang_code'],
				'USE_HTML'					=> $db->f('use_html') == 1 ? true : false,
				'NAME'						=> 'timesheetmess',

		);
		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['USER_NAME']=gm("Name of the user");
		$array_values['START_DATE']=gm("Start Date");
		$array_values['END_DATE']=gm("End Date");
		$array_values['DATE']=gm("Date");
		$array_values['SUMMARY']=gm("Hours summary");
		$array_values['COMMENT']=gm("Comment");
		$array_values['FIRST_NAME']=gm("First Name");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);		



		return json_out($data, $showin,$exit);
	}
	function get_timesheet($in,$showin=true,$exit=true){
		$db = new sqldb();
		//$use_Adhoc = $db->field("SELECT value FROM settings WHERE constant_name='USE_ADHOC_TMS'");
		$aut_app=$db->field("SELECT value FROM settings WHERE constant_name='TIME_AUTO_APPROVE' ");
		$data = array('timesheet'=>array());
		$data['timesheet']=array(
			'auto_app'		=> $aut_app=='1'? true : false
		);

		return json_out($data, $showin,$exit);
	}
	function get_customLabel($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'customLabel'=>array());

		$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
		while($pim_lang->move_next()){
		
		    array_push($data['customLabel'], array(
		    	'do'							=> 'timetracker-settings',
				'xget'							=> 'labels',
				'name'	                 		=> gm($pim_lang->f('language')),
				'label_language_id'	     		=> $pim_lang->f('lang_id'),
				'label_custom_language_id'	    => '',
			));		
		}

		$pim_custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
		while($pim_custom_lang->move_next()){
	
		    array_push($data['customLabel'], array(
		    	'do'							=> 'timetracker-settings',
				'xget'							=> 'labels',
				'name'	                 		=> $pim_custom_lang->f('language'),
				'label_custom_language_id'	    => $pim_custom_lang->f('lang_id'),
				'label_language_id'	     		=> '',
			));
		}

		return json_out($data, $showin,$exit);
	}
	function get_labels($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$const = array();


		$table = 'label_language_time';

		if($in['label_language_id'])
		{
			$filter = "WHERE label_language_id='".$in['label_language_id']."'";
			$id = $in['label_language_id'];
		}elseif($in['label_custom_language_id'])
		{
			$filter = "WHERE lang_code='".$in['label_custom_language_id']."'";
			$id = $in['label_custom_language_id'];
		}

		$db->query("SELECT * FROM $table $filter");

		$data['labels']=array(
				'label_language_id'  =>	$db->f('label_language_id'),
				'timeframe'			 =>	$db->f('timeframe'),
				'hours'		         => $db->f('hours'),
				'total'		 		 =>	$db->f('total'),
				'client'	         => $db->f('client'),
				'project'		 	 =>	$db->f('project'),
				'task'	             => $db->f('task'),
				'person'	         => $db->f('person'),
				'customer'	         => $db->f('customer'),
				'consultant'		 => $db->f('consultant'),
				'comment'			 => $db->f('comment'),
				'page'		 		 => $db->f('page'),
				'do'				 => 'timetracker-settings-project-label_update',
				'xget'				 => 'labels',
				'label_language_id'	 => $id
		);
		return json_out($data, $showin,$exit);
	}

	function get_PDFlayout($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'layout_header'=>array(),'custom_layout'=>array(),'layout_body'=>array(),'layout_footer'=>array());
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		
		$client 	= $db->field("SELECT active FROM default_data WHERE default_name='timesheet_print' AND value='client'");
		$project 	= $db->field("SELECT active FROM default_data WHERE default_name='timesheet_print' AND value='project'");
		$task 		= $db->field("SELECT active FROM default_data WHERE default_name='timesheet_print' AND value='task'");
		$comment 	= $db->field("SELECT active FROM default_data WHERE default_name='timesheet_print' AND value='comment'");

		$data['client']		= $client 	== '1' ? true : false;
		$data['project']	= $project 	== '1' ? true : false;
		$data['task']		= $task 	== '1' ? true : false;
		$data['comment']	= $comment 	== '1' ? true : false;

		
		/*$data['lang_id']=$_SESSION['lang_id'];*/
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=timetracker-timesheet_print&header='.$data['header'].'&layout='.$z.'&lid='.$language.'&header_id='.$def_header.'&save_as=preview',
					'img_href'						=> 'images/type_header-'.$z.'.jpg',
					'type'							=> $z,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=timetracker-timesheet_print&layout='.$y.'&lid='.$language.'&body='.$data['body'].'&save_as=preview',
					'img_href'						=> 'images/type_body-'.$y.'.jpg',
					'type'							=> $y,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',
				));
		}
		for ($i=1; $i <= 2; $i++) {
				array_push($data['layout_footer'], array(
					'view_invoice_pdf' 				=>'index.php?do=timetracker-timesheet_print&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
					'img_href'						=> 'images/type_footer-'.$i.'.jpg',
					'type'							=> $i,
					'footer'						=> 'footer',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
				));
		}
		$data['lang_id']=$language;
		$data['selected_header'] = $def_header;
		$data['selected_body'] = $def_body;
		$data['selected_footer'] = $def_footer;




		//

		return json_out($data, $showin,$exit);
	}
	function get_business($in,$showin=true,$exit=true){
			$db = new sqldb();
			$i = 0;
			$j = 0;
			$data = array('bigloop'=>array());
			$array = array('bunit');

			foreach ($array as $key) {
				$data['bigloop'][$j]=array(
					'name'			=> gm(ucfirst(str_replace('_', ' ', $key))),
					'table' 		=> $key,
					'count'			=> $i,
					'small'			=> array(),
				);
				$i=1;
				$db->query("SELECT * FROM project_bunit ORDER BY sort_order ");
				while ($db->next()) {
					$data['bigloop'][$j]['small'][]=array(
						'field_id'	=> $db->f('id'),
						'label'		=> gm(ucfirst($key)).' '.$i,
						'value'		=> $db->f('name'),
					);
					$i++;
				}
				$j++;
			}
		return json_out($data, $showin,$exit);
	}
	function get_internal($in,$showin=true,$exit=true){
		$db = new sqldb();
		$i = 0;
		// $j = 0;
		$data = array('internal'=>array());
		$array = array('Internal Companies');

		// foreach ($array as $key) {
		$data['internal']=array(
			'name'			=> gm(ucfirst(str_replace('_', ' ', $array[0]))),
			'table' 		=> $array[0],
			'count'			=> $i,
			'small'			=> array(),
			'accounts'		=> get_accounts($in) // o functie pt company
		);
		$i=1;
		$db->query("SELECT customer_id,name FROM customers WHERE is_admin > 0 ORDER BY is_admin ");
		while ($db->next()) {
			$data['internal']['small'][]=array(
				'field_id'	=> $db->f('customer_id'),
				'label'		=> gm(ucfirst($array[0])).' '.$i,
				'value'		=> $db->f('name'),
			);
			$i++;
		}
				// $j++;
			// }
		return json_out($data, $showin,$exit);
	}
	
	$result = array(
		'PDFlayout'			=> get_PDFlayout($in,true,false),
		'expenses' 			=> get_expenses($in,true,false),
		'name_convention' 	=> get_name_convention($in,true,false),
	);
	

	json_out($result);

function get_accounts ($in){
	$q = strtolower(trim($in["term"]));
	$filter =" AND is_admin='0' ";
	if($q){
		$filter = " name LIKE '%".$q."%' ";
	}
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$result = array();
	$db= new sqldb();
	$func = $db->query("SELECT customers.name,customers.customer_id FROM customers
				WHERE customers.active=1 $filter ORDER BY customers.name ")->getAll();
	foreach ($func as $key => $value) {
		$result[] = array('id'=>$value['customer_id'], 'value'=>$value['name']);
	}
	return $result;
}
?>
