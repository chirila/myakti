<?php
/**
 * undocumented class
 *
 * @package default
 * @author Mp
 **/
class project
{

	var $pag = 'project';
	var $field_n = 'project_id';
	private $db;

	function __construct() {

		$this -> db = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$this->db_users = new sqldb($db_config);
	}

	/****************************************************************
	* function delete(&$in)                                          *
	****************************************************************/
	function delete(&$in)
	{
		global $config;
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}

		$querys['There is data associated with this project. You cannot delete this project.'] = "SELECT project_id FROM task_time WHERE project_id='".$in['project_id']."' ";
		if(!$this->verify($querys)){
			json_out($in);
			return true;
		}
		$tracking_trace=$this->db->field("SELECT trace_id FROM projects WHERE project_id='".$in['project_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$sheet_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
		if($app->f('active') && $sheet_id!=''){
    		$ch = curl_init();
    		$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
	    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$sheet_id);

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
	    }
		$this->db->query("DELETE FROM tasks WHERE project_id='".$in['project_id']."'");
		$this->db->query("DELETE FROM project_user WHERE project_id='".$in['project_id']."'");
		$this->db->query("DELETE FROM projects WHERE project_id='".$in['project_id']."'");
		$this->db->query("DELETE FROM project_expenses WHERE project_id='".$in['project_id']."'");
		$this->db->query("DELETE FROM project_expences WHERE project_id='".$in['project_id']."'");
		if($tracking_trace){
		      $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
		      $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
		}
		msg::success(gm("Project deleted."),"succes");
		return true;
	}

	/****************************************************************
	* function delete_validate(&$in)                                          *
	****************************************************************/
	function delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('project_id', gm('ID'), 'required:exist[projects.project_id]', gm("Invalid ID"));
		return $v->run();
	}

	/**
	 * Verify function
	 *
	 * @param $querys -> must be an array where the keys are the messages to be shown and the values are the querys to be executed
	 * @return void
	 * @author
	 **/
	function verify($querys=array())
	{
		foreach ($querys as $key => $value) {
			$this->db->query($value);
			if($this->db->move_next()){
				msg::notice($key,"notice");
				return false;
			}
		}
		return true;
	}

	/****************************************************************
	* function archive(&$in)                                          *
	****************************************************************/
	function archive(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE projects SET active=0 WHERE project_id='".$in['project_id']."'");
		$tracking_trace=$this->db->field("SELECT trace_id FROM projects WHERE project_id='".$in['project_id']."' ");
	      if($tracking_trace){
	      	$this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
	      }
		msg::success(gm("Project has been archieved"),"succes");
		insert_message_log($this->pag,'{l}Project has been archieved by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['project_id'],false);
		return true;
	}

	/****************************************************************
	* function activate(&$in)                                          *
	****************************************************************/
	function activate(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}
		$project = $this->db->query("SELECT name, customer_id FROM projects WHERE project_id='".$in['project_id']."' ");
		$project->next();
		$querys['Dublicate project name for the same company. Please change this project name'] = "SELECT name FROM projects WHERE project_id!='".$in['project_id']."' AND name='".$project->f('name')."' AND customer_id='".$project->f('customer_id')."' AND active='0'";
		if(!$this->verify($querys)){
			json_out($in);
			return true;
		}
		$this->db->query("UPDATE projects SET active=1 WHERE project_id='".$in['project_id']."'");
		$tracking_trace=$this->db->field("SELECT trace_id FROM projects WHERE project_id='".$in['project_id']."' ");
        	if($tracking_trace){
          		$this->db->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
        	}
		msg::success(gm("Project has been successfully activated"),"success");
		insert_message_log($this->pag,'{l}Project has been successfully activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['project_id'],false);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
	    if(!$this->CanEdit($in)){
	      json_out($in);
	      return false;
	    }
	    if(!$this->tryAddAddressValidate($in)){
	      json_out($in);
	      return false;
	    }
	    $country_n = get_country_name($in['country_id']);
	    if($in['field']=='customer_id'){
	      Sync::start(ark::$model.'-'.ark::$method);

	      $address_id = $this->db->insert("INSERT INTO customer_addresses SET
	                                        customer_id     = '".$in['customer_id']."',
	                                        country_id      = '".$in['country_id']."',
	                                        state_id      = '".$in['state_id']."',
	                                        city        = '".$in['city']."',
	                                        zip         = '".$in['zip']."',
	                                        address       = '".$in['address']."',
	                                        billing       = '".$in['billing']."',
	                                        is_primary      = '".$in['primary']."',
	                                        delivery      = '".$in['delivery']."'");
	      if($in['billing']){
	        $this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
	      }

	      Sync::end($address_id);

	      if($in['primary']){
	        if($in['address'])
	        {
	          if(!$in['zip'] && $in['city'])
	          {
	            $address = $in['address'].', '.$in['city'].', '.$country_n;
	          }elseif(!$in['city'] && $in['zip'])
	          {
	            $address = $in['address'].', '.$in['zip'].', '.$country_n;
	          }elseif(!$in['zip'] && !$in['city'])
	          {
	            $address = $in['address'].', '.$country_n;
	          }else
	          {
	            $address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
	          }
	          $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
	          $output= json_decode($geocode);
	          $lat = $output->results[0]->geometry->location->lat;
	          $long = $output->results[0]->geometry->location->lng;
	        }
	        $this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
	        $this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
	        $this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
	      }
	    }else{      
	      $address_id = $this->db->insert("INSERT INTO customer_contact_address SET
	                                        contact_id      = '".$in['contact_id']."',
	                                        country_id      = '".$in['country_id']."',                                        city        = '".$in['city']."',
	                                        zip         = '".$in['zip']."',
	                                        address       = '".$in['address']."',
	                                        is_primary      = '".$in['primary']."',
	                                        delivery      = '".$in['delivery']."'");
	    }
	    msg::success ( gm("Changes have been saved."),'success');
	    insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
	    $in['address_id'] = $address_id;
	    $in['country'] = $country_n;
	    $in['project_id'] = $in['item_id'];
	    if($in['project_id'] && is_numeric($in['project_id'])){
	      $delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	      $this->db->query("UPDATE projects SET 
	          customer_address='".$delivery_address."', 
	          same_address='0' WHERE project_id='".$in['project_id']."'  "); 
	    }elseif ($in['project_id'] == 'tmp') {
	        $in['customer_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	        $in['sameAddress'] = 1;
	    }
	    $in['buyer_id'] = $in['customer_id'];
	    // $in['pagl'] = $this->pag;
	    
	    // json_out($in);
	    return true;

	}

	function CanEdit(&$in){
	    if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
	      return true;
	    }
	    $c_id = $in['customer_id'];
	    if(!$in['customer_id'] && $in['contact_id']) {
	      $c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	    }
	    if($c_id){
	      if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
	        $u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
	        if($u){
	          $u = explode(',', $u);
	          if(in_array($_SESSION['u_id'], $u)){
	            return true;
	          }
	          else{
	            msg::$warning = gm("You don't have enought privileges");
	            return false;
	          }
	        }else{
	          msg::$warning = gm("You don't have enought privileges");
	          return false;
	        }
	      }
	    }
	    return true;
	}

	/**
	   * undocumented function
	   *
	   * @return void
	   * @author PM
	   **/
	function tryAddAddressValidate(&$in)
	  {
	    $v = new validation($in);
	    if($in['customer_id']){
	      $v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
	    }else if($in['contact_id']){
	      $v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
	    }else{
	      msg::error(gm('Invalid ID'),'error');
	      return false;
	    }
	    $v->field('country_id', 'Country', 'required:exist[country.country_id]');

	    return $v->run();
	}

	function updateCustomerData(&$in)
	{
	    if($in['field'] == 'contact_id'){
	      $in['buyer_id'] ='';
	    }

	    $sql = "UPDATE projects SET ";
	    if($in['buyer_id']){
	      $buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, 
	                  customers.internal_language, customers.line_discount, customers.currency_id, customers.apply_line_disc,
	                  customer_addresses.address_id, customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount
	                  FROM customers
	                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
	                  AND customer_addresses.is_primary=1
	                  WHERE customers.customer_id='".$in['buyer_id']."' ");
	      $buyer_info->next();

	      if($in['project_id'] == 'tmp'){
	        $in['delivery_address_id'] = $buyer_info->f('address_id');
	      }
	        
	      $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
	      $in['contact_name'] ='';
	      
	      $sql .= " customer_id = '".$in['buyer_id']."', ";
	      $sql .= " company_name = '".addslashes($buyer_info->f('name'))."', ";	      
	      $sql .= " main_address_id = '".$in['main_address_id']."', ";
	      $sql .= " is_contact = '0', ";
	      if($in['contact_id']){
	        $contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	        // $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
	        $sql .= " contact_id = '".$in['contact_id']."', ";
	        $in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
	      }else{
	        // $sql .= " contact_name = '', ";
	        $sql .= " contact_id = '0', ";
	      }
	      $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
	      if($buyer_info->f('address_id') != $in['main_address_id']){
	        $buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
	        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
	      }
	      if($in['sameAddress']==1){
	        $sql .= " same_address = '0', ";
	        $sql .= " customer_address = '".addslashes($in['address_info'])."' ";
	      }else{
	        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
	        $new_address->next();
	        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
	        $sql .= " same_address = '".$in['delivery_address_id']."', ";
	        $sql .= " customer_address = '".addslashes($new_address_txt)."' ";      
	      }	     
	      
	    }else{
	      if(!$in['contact_id']){
	        msg::error ( gm('Please select a company or a contact'),'error');
	        return false;
	      }
	      $contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	      $contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
	      $sql .= " customer_id = '0', ";
	      $sql .= " company_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
	      $sql .= " contact_id = '".$in['contact_id']."', ";
	      $sql .= " is_contact = '1', ";

	      $in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
	      
	      if($in['sameAddress']==1){
	        $sql .= " same_address = '0', ";
	        $sql .= " customer_address = '".addslashes($in['address_info'])."' ";
	      }else{
	        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
	        $new_address->next();
	        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
	        $sql .= " same_address = '".$in['delivery_address_id']."', ";
	        $sql .= " customer_address = '".addslashes($new_address_txt)."' ";      
	      }

	    }
	    $sql .=", invoice_method='".$in['invoice_method']."', our_reference='".$in['our_reference']."' WHERE project_id ='".$in['item_id']."' ";
	    if(!$in['isAdd']){
	      $this->db->query($sql);
	      if($in['buyer_id']){
	      	$c_id=$in['buyer_id'];
	      }else{
	      	$c_id=0;
	      }
	      $this->db->query("UPDATE task_time SET customer_id='".$c_id."' WHERE project_id='".$in['item_id']."'");     
	    }
	    $in['project_id'] = $in['item_id'];
	    msg::success(gm('Sync successfull.'),'success');
	    return true;
	}


	/**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddC(&$in){
	    if(!$this->tryAddCValid($in)){ 
	      json_out($in);
	      return false; 
	    }
	    $in['project_id'] = $in['item_id'];
	    //$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
	    $name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

	    //Set account default vat on customer creation
	    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	    if(empty($vat_regime_id)){
	      $vat_regime_id = 0;
	    }

	    if($in['add_customer']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".$in['name']."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            vat_regime_id     = '".$vat_regime_id."',
	                                            city_name = '".$in['city']."',
	                                            c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
	                                            user_id = '".$_SESSION['u_id']."',
	                                            acc_manager_name = '".$name_user."',
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            creation_date = '".time()."',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."'");
	      
	        $this->db->query("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1' ");
	        $in['customer_name'] = $in['name'];

	      // include_once('../apps/company/admin/model/customer.php');
	        $in['value']=$in['btw_nr'];
	      // $comp=new customer();       
	      // $this->check_vies_vat_number($in);
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                            name    = :name,
	                            value   = :value ",
	                        ['user_id' => $_SESSION['u_id'],
	                         'name'    => 'company-customers_show_info',
	                         'value'   => '1']
	                    );	                            
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
	          $this->db->query("UPDATE projects SET 
	            customer_id='".$in['buyer_id']."', 
	            company_name='".$in['name']."',
	            contact_id='',
	            customer_address = '".$address."'
	            WHERE project_id='".$in['item_id']."' ");         
	        }
	        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
	        msg::success (gm('Success'),'success');
	        return true;
	    }
	    if($in['add_contact']){
	      $customer_name='';
	      if($in['buyer_id']){
	        $customer_name = $this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' ");
	      }
	        $in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
	                                              firstname='".$in['firstname']."',
	                                              lastname='".$in['lastname']."',
	                                              sex='".$in['gender_id']."',
	                                              title ='".$in['title_id']."',
	                                              language ='".$in['language_id']."',
	                                              `create`  = '".time()."',
	                                              customer_id = '".$in['buyer_id']."',
	                                              company_name= '".$customer_name."',
	                                              email='".$in['email']."',
	                                              cell='".$in['cell']."' ");
	        $this->db->query("INSERT INTO customer_contactsIds SET
	        							customer_id='".$in['buyer_id']."',
	        							contact_id='".$in['contact_id']."',
	        							email='".$in['email']."'
	        							");
	        $in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
	        if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
                $vars=array();
                if($in['buyer_id']){
                    $vars['customer_id']=$in['buyer_id'];
                    $vars['customer_name']=$customer_name;
                }          
                $vars['contact_id']=$in['contact_id'];
                $vars['firstname']=$in['firstname'];
                $vars['lastname']=$in['lastname'];
                $vars['table']='customer_contacts';
                $vars['email']=$in['email'];
                $vars['op']='add';
                synctoZendesk($vars);
            }
	        if($in['country_id']){
	          $this->db->query("INSERT INTO customer_contact_address SET
	                          address='".$in['address']."',
	                          zip='".$in['zip']."',
	                          city='".$in['city']."',
	                          country_id='".$in['country_id']."',
	                          contact_id='".$in['contact_id']."',
	                          is_primary='1',
	                          delivery='1' ");
	        }
	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                    AND name    = 'company-contacts_show_info'  ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                    AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);	                                    
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                                  name    = 'company-contacts_show_info',
	                                  value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                                  name    = :name,
	                                  value   = :value ",
	                                ['user_id' => $_SESSION['u_id'],
	                                 'name'    => 'company-contacts_show_info',
	                                 'value'   => '1']
	                            );	                                  	
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                      AND name    = 'company-contacts_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                      AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);	                                      
	        }
	        $count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
	        if($count == 1){
	          doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
	        }
	        if($in['item_id'] && is_numeric($in['item_id'])){
	          $this->db->query("UPDATE projects SET contact_id='".$in['contact_id']."' WHERE project_id='".$in['item_id']."' ");  
	        }
	        insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
	        msg::success (gm('Success'),'success');
	    }
	    return true;
	  }

	  /**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddCValid(&$in)
	  {
	    if($in['add_customer']){
	        $v = new validation($in);
	      $v->field('name', 'name', 'required:unique[customers.name]');
	      $v->field('country_id', 'country_id', 'required');
	      return $v->run();  
	    }
	    if($in['add_contact']){
	      $v = new validation($in);
	      $v->field('firstname', 'firstname', 'required');
	      $v->field('lastname', 'lastname', 'required');
/*	      $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
	      $v->field('country_id', 'country_id', 'required');*/
	      return $v->run();  
	    }
	    return true;
	  }

	function check_vies_vat_number(&$in){
	    $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

	    if(!$in['value'] || $in['value']==''){      
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    $value=trim($in['value']," ");
	    $value=str_replace(" ","",$value);
	    $value=str_replace(".","",$value);
	    $value=strtoupper($value);

	    $vat_numeric=is_numeric(substr($value,0,2));

	    /*if($vat_numeric || substr($value,0,2)=="BE"){
	      $trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
	      $trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
	      $trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

	      if(!$trends_access_token || $trends_expiration<time()){
	        $ch = curl_init();
	        $headers=array(
	          "Content-Type: x-www-form-urlencoded",
	          "Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
	          );
	        
	        $trends_data="grant_type=password&username=akti_api&password=akti_api";

	          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	          curl_setopt($ch, CURLOPT_POST, true);
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
	          curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

	          $put = json_decode(curl_exec($ch));
	        $info = curl_getinfo($ch);

	        if($info['http_code']==400){
	          if(ark::$method == 'check_vies_vat_number'){
	            msg::error ( $put->error,'error');
	            json_out($in);
	          }
	          return false;
	        }else{
	          if(!$trends_access_token){
	            $this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
	            $this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
	            $this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
	          }else{
	            $this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
	            $this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
	            $this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
	          }

	          $ch = curl_init();
	          $headers=array(
	            "Authorization: Bearer ".$put->access_token
	            );
	          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	            if($vat_numeric){
	                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
	            }else{
	                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
	            }
	            $put = json_decode(curl_exec($ch));
	          $info = curl_getinfo($ch);

	            if($info['http_code']==400 || $info['http_code']==429){
	              if(ark::$method == 'check_vies_vat_number'){
	              msg::error ( $put->error,'error');
	              json_out($in);
	            }
	            return false;
	          }else if($info['http_code']==404){
	            if($vat_numeric){
	              if(ark::$method == 'check_vies_vat_number'){
	                msg::error ( gm("Not a valid vat number"),'error');
	                json_out($in);
	              }
	              return false;
	            }
	          }else{
	            if($in['customer_id']){
	              $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	              if($country_id != 26){
	                $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
	                $in['remove_v']=1;
	              }else if($country_id == 26){
	                $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
	                $in['add_v']=1;
	              }
	            }
	            $in['comp_name']=$put->officialName;
	            $in['comp_address']=$put->street.' '.$put->houseNumber;
	            $in['comp_zip']=$put->postalCode;
	            $in['comp_city']=$put->city;
	            $in['comp_country']='26';
	            $in['trends_ok']=true;
	            $in['trends_lang']=$_SESSION['l'];
	            $in['full_details']=$put;
	            if(ark::$method == 'check_vies_vat_number'){
	              msg::success(gm('Success'),'success');
	              json_out($in);
	            }
	            return false;
	          }
	        }
	      }else{
	        $ch = curl_init();
	        $headers=array(
	          "Authorization: Bearer ".$trends_access_token
	          );
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	          if($vat_numeric){
	              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
	          }else{
	              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
	          }
	          $put = json_decode(curl_exec($ch));
	        $info = curl_getinfo($ch);

	          if($info['http_code']==400 || $info['http_code']==429){
	            if(ark::$method == 'check_vies_vat_number'){
	            msg::error ( $put->error,'error');
	            json_out($in);
	          }
	          return false;
	        }else if($info['http_code']==404){
	          if($vat_numeric){
	            if(ark::$method == 'check_vies_vat_number'){
	              msg::error (gm("Not a valid vat number"),'error');
	              json_out($in);
	            }
	            return false;
	          }
	        }else{
	          if($in['customer_id']){
	            $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	            if($country_id != 26){
	              $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
	              $in['remove_v']=1;
	            }else if($country_id == 26){
	              $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
	              $in['add_v']=1;
	            }
	          }
	          $in['comp_name']=$put->officialName;
	          $in['comp_address']=$put->street.' '.$put->houseNumber;
	          $in['comp_zip']=$put->postalCode;
	          $in['comp_city']=$put->city;
	          $in['comp_country']='26';
	          $in['trends_ok']=true;
	          $in['trends_lang']=$_SESSION['l'];
	          $in['full_details']=$put;
	          if(ark::$method == 'check_vies_vat_number'){
	            msg::success(gm('Success'),'success');
	            json_out($in);
	          }
	          return false;
	        }
	      }
	    }*/


	    if(!in_array(substr($value,0,2), $eu_countries)){
	      $value='BE'.$value;
	    }
	    if(in_array(substr($value,0,2), $eu_countries)){
	      $search   = array(" ", ".");
	      $vat = str_replace($search, "", $value);
	      $_GET['a'] = substr($vat,0,2);
	      $_GET['b'] = substr($vat,2);
	      $_GET['c'] = '1';
	      $dd = include('../valid_vat.php');
	    }else{
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    if(isset($response) && $response == 'invalid'){
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }else if(isset($response) && $response == 'error'){
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }else if(isset($response) && $response == 'valid'){
	      $full_address=explode("\n",$result->address);
	      switch($result->countryCode){
	        case "RO":
	          $in['comp_address']=$full_address[1];
	          $in['comp_city']=$full_address[0];
	          $in['comp_zip']=" ";
	          break;
	        case "NL":
	          $zip=explode(" ",$full_address[2],2);
	          $in['comp_address']=$full_address[1];
	          $in['comp_zip']=$zip[0];
	          $in['comp_city']=$zip[1];
	          break;
	        default:
	          $zip=explode(" ",$full_address[1],2);
	          $in['comp_address']=$full_address[0];
	          $in['comp_zip']=$zip[0];
	          $in['comp_city']=$zip[1];
	          break;
	      }

	      $in['comp_name']=$result->name;

	      $in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
	      $in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
	      $in['full_details']=$result;
	      $in['vies_ok']=1;
	      if($in['customer_id']){
	        $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
	        if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
	      }
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::success ( gm('VAT Number is valid'),'success');
	        json_out($in);
	      }
	      return true;
	    }else{
	      if(ark::$method == 'check_vies_vat_number'){
	        msg::error ( gm('Error'),'error');
	        json_out($in);
	      }
	      return false;
	    }
	    if(ark::$method == 'check_vies_vat_number'){
	      json_out($in);
	    }
	  }

	  function update_stage(&$in){
	  	global $config;
	  	if(!$this->validate_stage($in)){
			json_out($in);
		}
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$smart_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
		$in['start_done']=='1';	
		if($in['next_status']=='2'){
			$open_tasks=$this->db->field("SELECT COUNT(task_id) FROM tasks WHERE project_id='".$in['project_id']."' AND closed='0' ");
			$undelivered_articles=$this->db->field("SELECT COUNT(a_id) FROM project_articles WHERE project_id='".$in['project_id']."' AND delivered='0' ");
			$undelivered_purchases=$this->db->field("SELECT COUNT(project_purchase_id) FROM project_purchase WHERE project_id='".$in['project_id']."' AND delivered='0' ");
			if(!$open_tasks && !$undelivered_articles && !$undelivered_purchases){
				$this->db->query("UPDATE projects SET start_date='".strtotime($in['planned_date'])."', end_date='".strtotime($in['planned_end'])."', stage='".$in['next_status']."', start_done='".$in['start_done']."', closed='1' WHERE project_id='".$in['project_id']."' ");
				msg::success(gm('Project status updated'),"success");
			}else{
				msg::error(gm('There are open tasks or articles/purchases undelivered'),"error");
			}	
			if($app->f('active') && $smart_id!=''){
				$tasks=$this->db->query("SELECT row_id FROM tasks WHERE project_id='".$in['project_id']."'")->getAll();
				$final_array=array();
				foreach($tasks as $key => $value){
					$row_array=array();
					$row_array['id']=$value['row_id'];
					$row_array['locked']=true;
					array_push($final_array, $row_array);
				}
				$final_array=json_encode($final_array);

				$ch = curl_init();
				$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $final_array);
		        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows');

		        $put = curl_exec($ch);
		    	$info = curl_getinfo($ch);
			}
		}else{
			$this->db->query("UPDATE projects SET start_date='".strtotime($in['planned_date'])."', end_date='".strtotime($in['planned_end'])."', stage='".$in['next_status']."', start_done='".$in['start_done']."', closed='0' WHERE project_id='".$in['project_id']."' ");
			if($app->f('active') && $smart_id!=''){
				$tasks=$this->db->query("SELECT row_id FROM tasks WHERE project_id='".$in['project_id']."'")->getAll();
				$final_array=array();
				foreach($tasks as $key => $value){
					$row_array=array();
					$row_array['id']=$value['row_id'];
					$row_array['locked']=false;
					array_push($final_array, $row_array);
				}
				$final_array=json_encode($final_array);

				$ch = curl_init();
				$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $final_array);
		        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows');

		        $put = curl_exec($ch);
		    	$info = curl_getinfo($ch);

		    	if($in['next_status']=='0' || $in['next_status']=='1'){
		    		$ch = curl_init();
		    		$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$in['project_id']."' AND col_name='closed'");
					$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
					$final_data=array('locked'=>$in['next_status']=='0' ? true : false);
					$final_data=json_encode($final_data);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
					curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        		curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/columns/'.$col_id);

	        		$put = curl_exec($ch);
	    			$info = curl_getinfo($ch);
		    	}
			}
			msg::success(gm('Project status updated'),"success");
		}	
		return true;
	}

	function validate_stage(&$in){
		$is_ok=true;
		if($in['next_status']=='1'){
			$dates=$this->db->query("SELECT start_date,end_date FROM projects WHERE project_id='".$in['project_id']."' ");
			if(!strtotime($in['planned_date']) && !$dates->f('start_date')){
				msg::error(gm("This field is required"),"planned_date");
				$is_ok=false;
			}else if($in['billable_type']=='6' && !strtotime($in['planned_end']) && !$dates->f('end_date')){
				msg::error(gm("This field is required"),"planned_end");
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function update_task_name(&$in)
	{
		global $config;
		$this->db->query("UPDATE tasks SET task_name='".$in['task_name']."' WHERE task_id='".$in['task_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");	
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='task_name'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=$in['task_name'];
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}

		msg::success(gm('Changes have been saved.'),"success");
		json_out($in);
	}

	function update_project_tasks_billable(&$in){
		global $config;
		$this->db->query("UPDATE tasks  SET billable= '".$in['task']."'  WHERE project_id='".$in['project_id']."'");
		$this->db->query("UPDATE task_time  SET billable= '".$in['task']."'  WHERE project_id='".$in['project_id']."'");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$smart_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
		if($app->f('active') && $smart_id!=''){
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$in['project_id']."' AND col_name='billable'");
			$tasks=$this->db->query("SELECT row_id FROM tasks WHERE project_id='".$in['project_id']."'")->getAll();
			$final_array=array();
			foreach($tasks as $key => $value){
				$row_array=array('cells'=>array());
				$row_array['id']=$value['row_id'];
				$line_array['columnId']=$col_id;
				$line_array['value']=$in['task']==1 ? true : false;
				array_push($row_array['cells'], $line_array);
				array_push($final_array, $row_array);
			}
			$final_array=json_encode($final_array);
			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_array);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}

		msg::success(gm("Tasks updated"),"success");
		json_out($in);
	}

	function update_project_task_billable(&$in){
		global $config;
		$this->db->query("UPDATE tasks  SET billable='".$in['task']."'  WHERE task_id='".$in['task_id']."'");
		$this->db->query("UPDATE task_time  SET billable='".$in['task']."'  WHERE task_id='".$in['task_id']."'");
		
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){		
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='billable'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=$in['task']==1 ? true : false;
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}

		msg::success(gm("Task updated"),"success");
		json_out($in);
	}

	function update_task_t_h_rate(&$in)
	{
		global $config;
		$this->db->query("UPDATE tasks SET t_h_rate='".return_value($in['h_rate'])."' WHERE task_id='".$in['task_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='t_h_rate'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=return_value($in['h_rate']);
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

		}

		msg::success(gm('Changes have been saved.'),"success");
		return true;
	}

	function update_daily_t_h_rate(&$in)
	{
		global $config;
		$this->db->query("UPDATE tasks SET t_daily_rate='".return_value($in['h_rate'])."' WHERE task_id='".$in['task_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='t_daily_rate'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=return_value($in['h_rate']);
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

		}
		msg::success(gm('Changes have been saved.'),"success");
		return true;
	}

	function update_task_hours(&$in)
	{
		global $config;
		$this->db->query("UPDATE tasks SET t_hours='".return_value($in['t_hours'])."' WHERE task_id='".$in['task_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){	
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='t_hours'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=return_value($in['t_hours']);
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

		}
		msg::success(gm('Changes have been saved.'),"success");
		return true;
	}

	function update_task_budget(&$in)
	{
		global $config;
		$this->db->query("UPDATE tasks SET task_budget='".return_value($in['t_budget'])."' WHERE task_id='".$in['task_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){	
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='task_budget'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=return_value($in['t_budget']);
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

		}

		msg::success(gm('Changes have been saved.'),"success");
		return true;
	}

	function update_project_task_closed(&$in){
		global $config;
		$this->db->query("UPDATE tasks SET closed='".$in['t_closed']."' WHERE task_id='".$in['task_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='closed'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=$in['t_closed']==1 ? true : false;
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}
		msg::success(gm("Task updated"),"success");
		json_out($in);
	}

	function update_project_tasks_closed(&$in){
		global $config;
		$this->db->query("UPDATE tasks SET closed='".$in['t_closed']."' WHERE project_id='".$in['project_id']."' AND closed<>'2' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$smart_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
		if($app->f('active') && $smart_id!=''){
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$in['project_id']."' AND col_name='closed'");
			$tasks=$this->db->query("SELECT row_id FROM tasks WHERE project_id='".$in['project_id']."'")->getAll();
			$final_array=array();
			foreach($tasks as $key => $value){
				$row_array=array('cells'=>array());
				$row_array['id']=$value['row_id'];
				$line_array['columnId']=$col_id;
				$line_array['value']=$in['t_closed']==1 ? true : false;
				array_push($row_array['cells'], $line_array);
				array_push($final_array, $row_array);
			}
			$final_array=json_encode($final_array);
			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_array);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}
		msg::success(gm("Task updated"),"success");
		json_out($in);
	}

	function delete_task(&$in)
	{
		global $config;
		if(!$this->delete_task_validate($in)){
			json_out($in);
		}
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$smart_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
		if($app->f('active') && $smart_id!=''){
			$task=$this->db->field("SELECT row_id FROM tasks WHERE task_id='".$in['task_id']."'");
			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows?ids='.$task.'&ignoreRowsNotFound=true');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

		}
		$this->db->query("DELETE FROM tasks WHERE task_id='".$in['task_id']."' ");
		$this->db->query("DELETE FROM task_time WHERE task_id='".$in['task_id']."' ");

		msg::success(gm("Task deleted."),"success");
    	return true;
	}

	function delete_task_validate(&$in)
	{

		$v = new validation($in);
		$v->field('task_id', gm('ID'), 'required:exist[tasks.task_id]', gm("Invalid ID"));
		return $v->run();
	}

	function update_project_task_start(&$in){
		global $config;
		$this->db->query("UPDATE tasks SET start_date='".strtotime($in['start'])."' WHERE task_id='".$in['task_id']."' ");
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$p_data=$this->db->query("SELECT projects.smart_id,projects.project_id,tasks.row_id FROM tasks
				INNER JOIN projects ON tasks.project_id=projects.project_id 
				WHERE tasks.task_id='".$in['task_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){
			$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$p_data->f('project_id')."' AND col_name='start_date'");
			$final_data=array('id'=>$p_data->f('row_id'),'cells'=>array());
			$temp_data['columnId']=$col_id;
			$temp_data['value']=$in['start'];
			array_push($final_data['cells'], $temp_data);

			$final_data=json_encode($final_data);

			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}
		msg::success(gm("Task updated"),"success");
    	json_out($in);
	}

	function update_project_task_end(&$in){
		$this->db->query("UPDATE tasks SET end_date='".strtotime($in['end'])."' WHERE task_id='".$in['task_id']."' ");
		msg::success(gm("Task updated"),"success");
    	json_out($in);
	}

	/****************************************************************
	* function add_task(&$in)                                          *
	****************************************************************/
	function add_task(&$in)
	{
		global $config;
		if(!$this->add_task_validate($in)){
			json_out($in);
		}
		$def_id = $this->db->field("SELECT article_id FROM pim_articles WHERE internal_name='".$in['task_name']."' ");
		$default = $this->db->query("SELECT * FROM pim_articles where article_id='".$def_id."' ");
		$billable = '0';
		$t_h_rate = '0';
		$t_daily_rate='0';
		$t_hours = '0';

		while ($default->next()) {
			$billable=$default->f('billable');
			$t_h_rate=$default->f('h_price');
			$t_daily_rate = $default->f('d_price');
		}
		
		$t_id=$this->db->insert("INSERT INTO tasks SET project_id  = '".$in['project_id']."', task_name = '".$in['task_name']."', billable='".$billable."', t_h_rate='".$t_h_rate."', t_daily_rate='".$t_daily_rate."', t_hours='".$t_hours."' ");
       	
       	$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
       	$p_data=$this->db->query("SELECT smart_id,status_rate,invoice_method FROM projects WHERE project_id='".$in['project_id']."'");
		if($app->f('active') && $p_data->f('smart_id')!=''){
			$ptype=$p_data->f('status_rate')==0 ? 't_h_rate' : 't_daily_rate';
			$p_cols=$this->db->query("SELECT * FROM smart_cols WHERE project_id='".$in['project_id']."' AND `index`<8 ORDER BY `index` ASC")->getAll();
			$smart_line=array(
				'task_name'		=> $in['task_name'],
				'billable'		=> $p_data->f('invoice_method')==0 ? false : ($billable ? true : false),
				 $ptype   		=> $p_data->f('status_rate')==0 ? $t_h_rate : $t_daily_rate,
				't_hours'		=> $t_hours,
				'task_budget'	=> 0,
				'closed'		=> false
			);
			$final_data=array('cells'=>array(),'toBottom'=>true);
			foreach($p_cols as $key => $value){
				$temp_data['columnId']=$value['col_id'];
				$temp_data['value']=$smart_line[$value['col_name']];
				array_push($final_data['cells'], $temp_data);
			}
			$final_data=json_encode($final_data);
			$ch = curl_init();
			$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$p_data->f('smart_id').'/rows');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

	    	if($info['http_code']>=200 && $info['http_code']<300){
	    		$f_col=json_decode(preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', $put));
	    		$this->db->query("UPDATE tasks SET row_id='".$f_col->result->id."' WHERE task_id='".$t_id."' ");
	    	}
		}
       	msg::success(gm("Task added"),"success");
        	return true;
	}

	/****************************************************************
	* function add_task_validate(&$in)                                   *
	****************************************************************/
	function add_task_validate(&$in)
	{

     	$v = new validation($in);
		$v->field('project_id', gm('ID'), 'required:exist[projects.project_id]', gm("Invalid ID"));
		$v->field('task_name', gm('ID'), 'required:unique[tasks.task_name.( project_id='.$in['project_id'].' )]');
		return $v->run();
	}

	function add_user(&$in)
	{
		if(!$this->add_user_validate($in)){
			json_out($in);
		}
		//$user_data=$this->db_users->query("SELECT h_rate,daily_rate,h_cost,daily_cost FROM users WHERE user_id='".$in['user_id']."' ");
		$user_data=$this->db_users->query("SELECT h_rate,daily_rate,h_cost,daily_cost FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
		$status_rate=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$in['project_id']."' ");
		$p_user=$this->db->query("SELECT project_user_id FROM project_user WHERE project_id='".$in['project_id']."' AND user_id='".$in['user_id']."' ");
		if($p_user->move_next()){
			if($status_rate == 1){
				$this->db->query("UPDATE project_user SET active='1', p_daily_rate='".$user_data->f('daily_rate')."', p_daily_cost ='".$user_data->f('daily_cost')."' WHERE project_user_id='".$p_user->f('project_user_id')."' ");
			}else{
				$this->db->query("UPDATE project_user SET active='1', p_h_rate='".$user_data->f('h_rate')."', p_hourly_cost ='".$user_data->f('h_cost')."' WHERE project_user_id='".$p_user->f('project_user_id')."' ");
			}
		}
		else{
			if($status_rate == 1){
				$this->db->query("INSERT INTO project_user SET
											project_id  = '".$in['project_id']."',
											user_id	  = '".$in['user_id']."',
											user_name	  = '".$in['user_name']."',
											p_daily_rate = '".$user_data->f('daily_rate')."',
											p_daily_cost ='".$user_data->f('daily_cost')."' ");
			}else{
				$this->db->query("INSERT INTO project_user SET
											project_id  = '".$in['project_id']."',
											user_id	  = '".$in['user_id']."',
											user_name	  = '".$in['user_name']."',
											p_h_rate = '".$user_data->f('h_rate')."',
											p_hourly_cost ='".$user_data->f('h_cost')."' ");
			}
		}

         msg::success(gm("User added"),"success");
         return true;
	}

	/****************************************************************
	* function add_user_validate(&$in)                                   *
	****************************************************************/
	function add_user_validate(&$in)
	{
      $v = new validation($in);
	  $v->field('project_id', gm('ID'), 'required:exist[projects.project_id]', gm('Invalid ID'));
	  $v->field('user_id', gm('ID'), 'required', gm('Invalid ID'));
	  $v->field('user_name', gm('ID'), "required:unique[project_user.user_name.( project_id='".$in['project_id']."' AND user_id='".$in['user_id']."' AND active='1')]");
	  return $v->run();
	}

	function update_user_manager(&$in)
	{

		$this->db->query("UPDATE project_user SET manager='".$in['manager']."' WHERE project_user_id='".$in['project_user_id']."' ");
		msg::success(gm('Changes have been saved.'),"success");
		json_out($in);
	}

	function update_user_phours(&$in)
	{

		$this->db->query("UPDATE project_user SET p_hours='".return_value($in['p_hours'])."' WHERE project_user_id='".$in['project_user_id']."' ");
		msg::success(gm('Changes have been saved.'),"success");
		return true;
	}

	function update_user_phrate(&$in)
	{

		$this->db->query("UPDATE project_user SET p_h_rate='".return_value($in['h_rate'])."' WHERE project_user_id='".$in['project_user_id']."' ");
		msg::success(gm('Changes have been saved.'),"success");
		return true;
	}
	function update_user_pdailyrate(&$in)
	{

		$this->db->query("UPDATE project_user SET p_daily_rate='".return_value($in['h_rate'])."' WHERE project_user_id='".$in['project_user_id']."' ");
		msg::success(gm('Changes have been saved.'),"success");
		return true;
	}

	function delete_user(&$in)
	{
		if(!$this->delete_user_validate($in)){
			json_out($in);
		}
		$this->db->query("UPDATE project_user SET active='0' WHERE project_user_id='".$in['project_user_id']."' ");
        	msg::success(gm('Changes have been saved.'),"success");
        	return true;
	}

	function delete_user_validate(&$in)
	{
		$v = new validation($in);
		$v->field('project_user_id', gm('ID'), 'required:exist[project_user.project_user_id]', gm("Invalid ID"));
		return $v->run();
	}

	function update_user_cost(&$in)
	{

		if($in['status_rate']==0){
			$this->db->query("UPDATE project_user SET p_hourly_cost='".return_value($in['user_cost'])."' WHERE project_user_id='".$in['project_user_id']."' ");
		}else{
			$this->db->query("UPDATE project_user SET p_daily_cost='".return_value($in['user_cost'])."' WHERE project_user_id='".$in['project_user_id']."' ");
		}
		msg::success(gm('User cost updated'),"success");
		json_out($in);
	}

	function update_article_name(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			$in['name']=$this->db->field("SELECT name FROM project_articles WHERE a_id='".$in['a_id']."' ");
			msg::error(gm('Cannot change name of billed article'),"error");
			json_out($in);
		}
		$this->db->query("UPDATE project_articles SET name='".$in['name']."' WHERE a_id='".$in['a_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function update_article_qty(&$in)
	{
		$delivered=$this->db->field("SELECT delivered FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($delivered){
			$in['qty']=display_number($this->db->field("SELECT quantity FROM project_articles WHERE a_id='".$in['a_id']."' "));
			msg::error(gm('Quantity cannot be changed due to article delivery'),"error");
			json_out($in);
		}else{
			$article_id=$this->db->field("SELECT article_id FROM project_articles WHERE a_id='".$in['a_id']."' ");
			$stock=$this->db->field("SELECT stock FROM pim_articles WHERE article_id='".$article_id."' ");
			if($stock-return_value($in['qty'])<0){
				$in['minus_stock']=1;
				$in['stock']=display_number($stock);
			}
			$this->db->query("UPDATE project_articles SET quantity='".return_value($in['qty'])."' WHERE a_id='".$in['a_id']."' ");
			msg::success(gm('Changes saved'),"success");
		}
		return true;
	}

	function update_article_price(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			$in['price']=display_number($this->db->field("SELECT price FROM project_articles WHERE a_id='".$in['a_id']."' "));
			msg::error(gm('Cannot change price of billed article'),"error");
			json_out($in);
		}
		$purchase_price = $this->db->field("SELECT purchase_price FROM project_articles WHERE a_id='".$in['a_id']."' ");
		$margin_nr=return_value($in['price'])-$purchase_price;
		$margin_percent=(return_value($in['price'])-$purchase_price)/return_value($in['price'])*100;
		$this->db->query("UPDATE project_articles SET margin='".$margin_nr."',margin_per='".$margin_percent."',price='".return_value($in['price'])."' WHERE a_id='".$in['a_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function delete_article(&$in)
	{
		$billed=$this->db->field("SELECT SUM(invoiced) FROM service_delivery WHERE project_id='".$in['project_id']."' AND a_id='".$in['article_id']."' ");
		if($billed){
			msg::error(gm('Cannot delete billed article'),"error");
			json_out($in);
		}
		$delivery_ids=array();
		$article_delivered=$this->db->query("SELECT * FROM service_delivery WHERE project_id='".$in['project_id']."' AND a_id='".$in['article_id']."' ");
		while($article_delivered->next()){
			$in['quantity']=$article_delivered->f('quantity');
			$in['delivery_id']=$article_delivered->f('delivery_id');
			array_push($delivery_ids,$article_delivered->f('delivery_id'));
			$this->undo_delivery($in);
			$this->db->query("DELETE FROM service_delivery WHERE id='".$article_delivered->f('id')."' ");
			if(WAC){
				generate_purchase_price($in['article_id']);
		 	}
		}
		foreach($delivery_ids as $key=>$value){
			$delivery_data=$this->db->field("SELECT COUNT(id) FROM service_delivery WHERE delivery_id='".$value."' ");
			if(!$delivery_data){
				$this->db->query("DELETE FROM service_deliveries WHERE delivery_id='".$value."' ");
			}
		}
		$this->db->query("DELETE FROM project_articles WHERE a_id='".$in['a_id']."' ");
		msg::success(gm('Article deleted'),"success");
		return true;
	}

	function make_delivery(&$in)
	{
		$const =array();
		$const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
		$const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

		$now = time();
		//foreach ($in['quantity'] as $key => $value) {
			//if(return_value($value) > 0){
			if(return_value($in['line']['quantity']) > 0) {
				$hide_stock = $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$in['line']['article_id']."'");
				if($const['ALLOW_STOCK'] && $hide_stock==0 ){

					$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$in['line']['article_id']."' ");
					$article_data->next();
					$stock = $article_data->f("stock");

					$stock_packing = $article_data->f("packing");
					if(!$const['ALLOW_ARTICLE_PACKING']){
						$stock_packing = 1;
					}
					$stock_sale_unit = $article_data->f("sale_unit");
					if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
						$stock_sale_unit = 1;
					}

					$new_stock = $stock - (return_value($in['line']['quantity'])*$stock_packing/$stock_sale_unit);

					$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['line']['article_id']."'");

					/*start for  stock movements*/

					if($in['line']['disp_addres_info']){
						$locations=array();
						foreach($in['line']['disp_addres_info'] as $key => $value){
							$locations[$value['customer_id'].'-'.$value['address_id']]=return_value($value['quantity_delivered']);
						}
					}
					$this->db->insert("INSERT INTO stock_movements SET
							date 						=	'".$now."',
							created_by 					=	'".$_SESSION['u_id']."',
							project_id 					= 	'".$in['project_id']."',
							article_id 					=	'".$in['line']['article_id']."',
							article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
							item_code  					= 	'".addslashes($article_data->f("item_code"))."',
							movement_type				=	'16',
							quantity 					= 	'".(return_value($in['line']['quantity'])*$stock_packing/$stock_sale_unit)."',
							stock 						=	'".$stock."',
							new_stock 					= 	'".$new_stock."',
							backorder_articles 			= 	'0',
						      location_info                =   '".serialize($locations)."',
						      delivery_id                 =    '".$in['delivery_id']."',
						      delivery_date 			='".$in['date_del_line_h']."'
				        ");
					/*end for  stock movements*/
				}

			}
		//}
		if($in['line']['disp_addres_info']){
                 	foreach ($in['line']['disp_addres_info'] as $key => $value) {
                 		$dispatch_quantity=return_value($value['quantity_delivered']);
                 		$from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$in['line']['article_id']."' AND  address_id='".$value['address_id']."' and customer_id='".$value['customer_id']."'");                  
	                  $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -($dispatch_quantity*($stock_packing/$stock_sale_unit )))."' WHERE article_id='".$in['line']['article_id']."'   AND  address_id='".$value['address_id']."' and customer_id='".$value['customer_id']."'");
                 	}
            }
            if($in['line']['selected_s_n']!=''){
            	$in['line']['selected_s_n']=rtrim($in['line']['selected_s_n'],',');
            	$selected_serials=explode(',', $in['line']['selected_s_n']);
            	foreach($selected_serials as $key_serial=>$value_serial){
            		$this->db->query("UPDATE serial_numbers
										SET 		status_id 	= '2',
												status_details_2 	= '".$in['serial_number']."',
												date_out 		= '".time()."',
												project_id 	      ='".$in['project_id']."',
												delivery_id 	= '".$in['delivery_id']."'
										WHERE id = '".$value_serial."' ");
            	}
            }
            $this->db->query("INSERT INTO service_delivery SET project_id='".$in['project_id']."' , a_id='".$in['line']['article_id']."', quantity='".return_value($in['line']['quantity'])."', delivery_id='".$in['delivery_id']."', delivery_note='".$in['delivery_note']."' ");

		insert_message_log($this->pag,'{l}Article delivered by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['project_id']);
		return true;
	}

	function undo_delivery(&$in){

		$const=array();
		$const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
		$const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

		$hide_stock = $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$in['article_id']."'");
		if($const['ALLOW_STOCK'] && $hide_stock==0 ){

			$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$in['article_id']."' ");
			$article_data->next();
			$stock = $article_data->f("stock");

			$stock_packing = $article_data->f("packing");
			if(!$const['ALLOW_ARTICLE_PACKING']){
				$stock_packing = 1;
			}
			$stock_sale_unit = $article_data->f("sale_unit");
			if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
				$stock_sale_unit = 1;
			}

			$new_stock = $stock + ($in['quantity']*$stock_packing/$stock_sale_unit);

			$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id']."'");

			/*start for  stock movements*/

			$stock_move=$this->db->query("SELECT location_info,movement_id FROM stock_movements WHERE project_id='".$in['project_id']."' AND article_id='".$in['article_id']."' AND delivery_id='".$in['delivery_id']."' ");
			if($stock_move->f('location_info') !=''){
	        	      $location=unserialize($stock_move->f('location_info'));
	        	      foreach ($location as $key => $value) {
	        	           	if(return_value($value)>0){
	        	           	      $info=explode("-", $key);
                                    $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$in['article_id']."' AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
                                    $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock+return_value($value)*$stock_packing/$stock_sale_unit)."' WHERE article_id='".$in['article_id']."'   AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
	        	           	}
	        	      }
			}
			$this->db->query("DELETE FROM stock_movements WHERE movement_id='".$stock_move->f('movement_id')."'");
			/*end for  stock movements*/

			$this->db->query("UPDATE serial_numbers
										SET 		status_id 	= '1',
												status_details_2 	= '',
												date_out 		= '0'
										WHERE project_id='".$in['project_id']."' AND article_id='".$in['article_id']."' AND delivery_id='".$in['delivery_id']."' ");
		}
	}

	function edit_delivery(&$in){
		$now = time();
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$this->db->query("UPDATE service_deliveries
		                  SET 	`date` = '".$in['date_del_line_h']."',
                                    `hour` = '".$in['hour']."',
                                    `minute` = '".$in['minute']."',
                                    `pm` = '".$in['pm']."',
                                    contact_name 	= '".$in['contact_name']."',
		                        contact_id	= '".$in['contact_id']."',
		                        delivery_address	= '".$in['delivery_address']."'
                              WHERE project_id='".$in['project_id']."' and delivery_id='".$in['delivery_id']."' ");
		$this->db->query("UPDATE 	service_delivery
		                 	SET   	`delivery_note` = '".$in['delivery_note']."'
                              WHERE     	project_id='".$in['project_id']."' and delivery_id='".$in['delivery_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function delete_delivery(&$in){
		$err=0;
		$articles_ids=array();
		$articles = $this->db->query("SELECT * FROM service_delivery WHERE project_id ='".$in['project_id']."' AND delivery_id='".$in['delivery_id']."' ");
		while($articles->next()){
			if($articles->f('invoiced')){
				msg::error(gm('Some billed articles could not be reverted'),"error");
				$err++;
				continue;
			}		
			array_push($articles_ids,$articles->f('a_id'));
			$this->db->query("UPDATE project_articles SET delivered='0' WHERE project_id='".$in['project_id']."' AND article_id='".$articles->f('a_id')."' ");
			$in['article_id']=$articles->f('a_id');
			$in['quantity']=$articles->f('quantity');
			$this->undo_delivery($in);
			$this->db->query("DELETE FROM service_delivery WHERE id='".$articles->f('id')."' ");
		}	
		if(WAC){
		     	foreach ($articles_ids as $key => $value) {
		        generate_purchase_price($value);
		      }
 		}
 		if($err == 0){
 			$this->db->query("DELETE FROM service_deliveries WHERE project_id ='".$in['project_id']."' AND delivery_id='".$in['delivery_id']."'");
 		}
 		msg::success(gm("Changes saved"),"success");
 		return true;	
	}

	function projectAddArticle(&$in)
	{
		if(!$this->projectAddArticle_validate($in)){
			json_out($in);
		}
		$unique_art=$this->db->field("SELECT a_id FROM project_articles WHERE project_id='".$in['project_id']."' AND article_id='".$in['article_id']."' ");
		if($unique_art){
			msg::error(gm('Article already added'),"error");
			json_out($in);
		}
		$margin_nr=$in['sell_price']-$in['purchase_price'];
		$margin_percent=($in['sell_price']-$in['purchase_price'])/$in['sell_price']*100;
		$this->db->query("INSERT INTO project_articles SET project_id='".$in['project_id']."',
									name='".$in['name']."',
									article_id='".$in['article_id']."',
									quantity='1',
									price='".$in['sell_price']."',
									purchase_price='".$in['purchase_price']."',
									margin='".$margin_nr."',
									margin_per='".$margin_percent."' ");
		msg::success(gm('Changes saved'),"success");
		return true;
	}

	function projectAddArticle_validate(&$in)
	{
		$v = new validation($in);
		$v->f('name', 'name', 'required');
		$v->f('project_id', 'project_id', 'required');
		$v->f('article_id', 'article_id', 'required');
		return $v->run();
	}

	function update_article_purchase_price(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			$in['purchase_price']=display_number($this->db->field("SELECT purchase_price FROM project_articles WHERE a_id='".$in['a_id']."' "));
			msg::error(gm('Cannot change purchase price of billed article'),"error");
			json_out($in);
		}
		$margin_per = $this->db->field("SELECT margin_per FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($margin_per==100){
			$sell_price1=(-return_value($in['purchase_price'])*100)/(1-100);
			$sell_price2=(-return_value($in['purchase_price'])*100)/(99-100);
			$sell_price=$sell_price1+$sell_price2;
		}else{
			$sell_price=(-return_value($in['purchase_price'])*100)/($margin_per-100);
		}
		$in['sell_price']=display_number($sell_price);
		$this->db->query("UPDATE project_articles SET price='".$sell_price."',purchase_price='".return_value($in['purchase_price'])."' WHERE a_id='".$in['a_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function update_article_margin(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			$in['margin']=display_number($this->db->field("SELECT margin_per FROM project_articles WHERE a_id='".$in['a_id']."' "));
			msg::error(gm('Cannot change margin of billed article'),"error");
			json_out($in);
		}
		$article_purchase_price = $this->db->field("SELECT purchase_price FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if(return_value($in['margin'])==100){
			$sell_price1=(-$article_purchase_price*100)/(1-100);
			$sell_price2=(-$article_purchase_price*100)/(99-100);
			$sell_price=$sell_price1+$sell_price2;
		}else{
			$sell_price=(-$article_purchase_price*100)/(return_value($in['margin'])-100);
		}
		$margin=$sell_price-$article_purchase_price;
		$in['sell_price']=display_number($sell_price);
		$this->db->query("UPDATE project_articles SET margin='".$margin."',price='".$sell_price."',margin_per='".return_value($in['margin'])."' WHERE a_id='".$in['a_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function update_article_sell_price(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			$in['price']=display_number($this->db->field("SELECT price FROM project_articles WHERE a_id='".$in['a_id']."' "));
			msg::error(gm('Cannot change price of billed article'),"error");
			json_out($in);
		}
		$purchase_price = $this->db->field("SELECT purchase_price FROM project_articles WHERE a_id='".$in['a_id']."' ");
		$margin_nr=return_value($in['price'])-$purchase_price;
		$margin_percent=(return_value($in['price'])-$purchase_price)/return_value($in['price'])*100;
		$in['margin']=display_number($margin_percent);
		$this->db->query("UPDATE project_articles SET margin='".$margin_nr."',margin_per='".$margin_percent."',price='".return_value($in['price'])."' WHERE a_id='".$in['a_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function update_article_delivered(&$in)
	{
		if(!$this->article_delivery_validation($in)){
			msg::error(gm('Total quantity lower than quantity selected or stocks not enough'),"error");
			json_out($in);
		}
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$in['delivery_id'] = $this->db->insert("INSERT INTO service_deliveries
			                      SET project_id='".$in['project_id']."',
			                         `date` = '".$in['date_del_line_h']."',
                                     `hour` = '".$in['hour']."',
                                     `minute` = '".$in['minute']."',
                                     `pm` = '".$in['pm']."',
                                      contact_name 	= '".$in['contact_name']."',
		                              contact_id	= '".$in['contact_id']."',
		                              delivery_address	= '".$in['delivery_address']."',
		                              delivery_done 	= '1'");
		$articles_ids=array();
		foreach($in['lines'] as $key=>$value){
			array_push($articles_ids,$value['article_id']);
			$already_delivered=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE project_id='".$in['project_id']."' AND a_id='".$value['article_id']."' ");
			if(!$already_delivered){
				$already_delivered=0;
			}
			$totalQ=$this->db->field("SELECT quantity FROM project_articles WHERE project_id='".$in['project_id']."' AND article_id='".$value['article_id']."' ");
			if(return_value($value['quantity'])+$already_delivered>=$totalQ){
				$this->db->query("UPDATE project_articles SET delivered='1', quantity='".(return_value($value['quantity'])+$already_delivered)."' WHERE project_id='".$in['project_id']."' AND article_id='".$value['article_id']."' ");
			}
			$in['line']=$value;
			$this->make_delivery($in);
		}
		if(WAC){
		  foreach ($articles_ids as $key => $value) {
		    generate_purchase_price($value);
		  }
		}

		/*if($in['multiple']){
			$this->db->query("UPDATE project_articles SET delivered='".$in['delivered']."', quantity='".return_value($in['quantity_delivered'])."', delivery_note='".$in['delivery_note']."', delivery_date='".time()."' WHERE a_id='".$in['a_id']."' ");
		}else{
			$this->db->query("UPDATE project_articles SET delivered='".$in['delivered']."', delivery_note='".$in['delivery_note']."', delivery_date='".time()."' WHERE a_id='".$in['a_id']."' ");	
		}		
		$article_data=$this->db->query("SELECT project_articles.article_id,project_articles.project_id,project_articles.quantity,projects.serial_number FROM project_articles
				INNER JOIN projects ON project_articles.project_id=projects.project_id
				WHERE project_articles.a_id='".$in['a_id']."'");
		$in['article_id']=$article_data->f('article_id');
		$in['project_id']=$article_data->f('project_id');
		$in['serial_number']=$article_data->f('serial_number');
		$in['quantity']=$article_data->f('quantity');
		$this->make_delivery($in);
		if(WAC){
              generate_purchase_price($in['article_id']);
            }*/
		msg::success(gm('Delivery made'),"success");
		return true;
	}

	function article_delivery_validation(&$in)
	{
		$is_ok=true;
		//$quantity=$this->db->field("SELECT quantity FROM project_articles WHERE a_id='".$in['a_id']."'");
		foreach($in['lines'] as $key => $value){
			if($value['is_error']=='1'){
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function undo_article_delivered(&$in)
	{
		$billed=$this->db->field("SELECT billed FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($billed){
			msg::error(gm('Cannot undeliver billed article'),"error");
			json_out($in);
		}
		$delivered=$this->db->field("SELECT delivered FROM project_articles WHERE a_id='".$in['a_id']."' ");
		if($delivered){
			$this->db->query("UPDATE project_articles SET delivered='".$in['delivered']."', delivery_note='' WHERE a_id='".$in['a_id']."' ");
			$article_data=$this->db->query("SELECT article_id,project_id,quantity FROM project_articles WHERE a_id='".$in['a_id']."'");
			$in['article_id']=$article_data->f('article_id');
			$in['project_id']=$article_data->f('project_id');
			$in['quantity']=$article_data->f('quantity');
			$this->undo_delivery($in);
			if(WAC){
              		generate_purchase_price($in['article_id']);
            	}
			msg::success(gm('Changes saved'),"success");
			return true;
		}
	}

	function add_purchase(&$in){
		$this->db->query("INSERT INTO project_purchase SET supplier_id='".$in['supplier']."',
											billable='".$in['billable']."',
											purchase_date='".$in['purchase_date']."',
											description ='".$in['description']."',
											quantity = '".return_value($in['quantity'])."',
											unit_price = '".return_value($in['unit_price'])."',
											margin = '".return_value($in['margin'])."',
											project_id='".$in['project_id']."',
											amount='".return_value($in['quantity']) * return_value($in['unit_price'])."' ");

		msg::success(gm("Purchase added."),"success");
		return true;
	}

	/**
	 * update a purchase
	 *
	 * @param array $in
	 * @return true
	 */
	function update_purchase(&$in){
		$this->db->insert("UPDATE project_purchase SET supplier_id='".$in['supplier']."',
											billable='".$in['billable']."',
											purchase_date='".$in['purchase_date']."',
											description ='".$in['description']."',
											quantity = '".return_value($in['quantity'])."',
											unit_price = '".return_value($in['unit_price'])."',
											margin = '".return_value($in['margin'])."',
											project_id='".$in['project_id']."',
											amount='".return_value($in['quantity']) * return_value($in['unit_price'])."'
								WHERE project_purchase_id='".$in['project_purchase_id']."'  ");
		msg::success(gm("Purchase updated."),"success");
		return true;
	}

	function update_purchase_field(&$in){

		$this->db->query("UPDATE project_purchase SET unit_price='".$in['price']."', margin='".$in['margin']."' WHERE project_purchase_id='".$in['project_purchase_id']."' ");
		msg::success(gm('Changes have been saved.'),"success");
		json_out($in);
	}

	function delete_purchase(&$in){

		$this->db->query("DELETE FROM project_purchase WHERE project_purchase_id='".$in['project_purchase_id']."' ");
		msg::success(gm("Changes have been saved."),"success");
		return true;
	}

	function edit_purchase_note(&$in){

		$this->db->query("UPDATE project_purchase SET note='".$in['note']."' WHERE project_purchase_id='".$in['project_purchase_id']."' ");
		msg::success(gm("Changes have been saved."),"success");
		json_out($in);
	}

	/****************************************************************
	* function update(&$in)                                          *
	****************************************************************/
	function update(&$in)
	{
		global $config;
		if(!$this->update_validate($in)){
			json_out($in);
		}
		$billable_before=$this->db->field("SELECT billable_type FROM projects WHERE project_id='".$in['project_id']."' ");
		if(!$in['billable_type']){
			$billable_after=$billable_before;
		}else{
			$billable_after=$in['billable_type'];
		}
		$this->db->query("UPDATE projects  SET customer_id= '".$in['buyer_id']."',
												name	           		= '".$in['name']."',
												code	           		= '".$in['code']."',
												invoice_method    		= '".$in['invoice_method']."',
												billable_type	   		= '".$billable_after."',
												pr_h_rate	       		= '".return_value($in['pr_h_rate'])."',
												t_pr_hour	       		= '".$in['t_pr_hour']."',
												t_pr_fees	       		= '".$in['t_pr_fees']."',
												show_budget_to_all 		= '".$in['show_budget_to_all']."',
												send_email_alerts  		= '".$in['send_email_alerts']."',
												alert_percent	   		= '".$in['alert_percent']."',
												budget_type			='".$in['budget_type']."',
												start_date		   		= '".strtotime($in['start_date'])."',
												end_date		   		= '".strtotime($in['end_date'])."',
												serial_number	   		= '".$in['serial_number']."',
												our_reference			= '".$in['our_reference']."',
												currency_type			= '".$in['currency_type']."',
												retainer_budget			= '".return_value($in['retainer_budget'])."',
												quote_id 				= '".$in['quote_id']."',
												source_id				= '".$in['source_id']."',
												status_rate				='".$in['project_rate_status']."',
												bunit                               ='".$in['bunit_id']."',
												notes 				= '".$in['notes']."'
					     							WHERE project_id='".$in['project_id']."'");

		$this->db->query("UPDATE tasks SET billable= '0' WHERE project_id='".$in['project_id']."'");
		$this->db->query("UPDATE tasks SET closed='0' WHERE project_id='".$in['project_id']."' AND closed<>'2' ");
		$this->db->query("UPDATE task_time  SET billable= '0'  WHERE project_id='".$in['project_id']."'");

		foreach($in['tasks']['tasks'] as $key => $value){
			$this->db->query("UPDATE tasks SET billable='".$value['BILLABLE_CHECKED']."', 
									task_budget='".return_value($value['task_budget'])."', 
									closed='".$value['task_budget_c']."' 
									WHERE task_id='".$value['REL']."'");		
			$this->db->query("UPDATE task_time SET billable='".$value['BILLABLE_CHECKED']."' WHERE task_id='".$value['REL']."'");
		}

		$this->db->query("UPDATE task_time SET customer_id='".$in['buyer_id']."' WHERE project_id='".$in['project_id']."' ");
		$this->db->query("UPDATE project_user  SET manager= '0' WHERE project_id='".$in['project_id']."'");
		foreach($in['users']['users'] as $key => $value){
			$this->db->query("UPDATE project_user  SET manager= '".$value['MANAGER_CHECKED']."'  WHERE project_user_id='".$value['REL']."'");
		}

		//billable
		if($in['billable_type']=='1' || $in['billable_type']=='7'){
			foreach($in['tasks']['tasks'] as $key => $value){
				$this->db->query("UPDATE tasks SET t_h_rate= '".return_value($value['t_h_rate'])."', 
										t_daily_rate='".$value['daily_h_rate']."' 
										WHERE task_id='".$value['REL']."'");
			}
		}
		if($in['billable_type']=='2' || $in['billable_type']=='5'){
			foreach($in['users']['users'] as $key => $value){
				$this->db->query("UPDATE project_user SET p_h_rate='".return_value($value['p_h_rate'])."',
											p_daily_rate='".return_value($value['p_daily_rate'])."'
											WHERE project_user_id='".$value['REL']."'");
			}
		}
		if($in['billable_type'] == '7'){
			$this->db->query("UPDATE tasks SET must_delete='1' WHERE default_task_id<>'0' AND project_id='".$in['project_id']."' ");
			$func = $this->db->query("SELECT DISTINCT func_id FROM  `user_function` WHERE user_id IN (SELECT user_id FROM project_user WHERE project_id ='".$in['project_id']."' AND active='1') ");
			while ($func->next()) {
				$task_exist = $this->db->field("SELECT task_id FROM tasks WHERE default_task_id='".$func->f('func_id')."' AND project_id='".$in['project_id']."' ");
				if(!$task_exist){
					$tname = $this->db->field("SELECT default_name FROM default_data WHERE default_id='".$func->f('func_id')."' ");
					$unit_p = $this->db->field("SELECT value FROM default_data WHERE default_main_id='".$func->f('func_id')."' AND default_name='".$in['buyer_id']."' AND type='unit_price' ");
					//$daily_p = $this->db->field("SELECT value FROM default_data WHERE default_main_id='".$func->f('func_id')."' AND default_name='".$in['customer_id']."' AND type='unit_price' ");
					if(!$unit_p){
						$unit_p = $this->db->field("SELECT value FROM default_data WHERE default_main_id='".$func->f('func_id')."' AND default_name='unit_price' ");
					}
					$this->db->query("INSERT INTO tasks SET project_id='".$in['project_id']."', task_name='".$tname."', t_h_rate='".$unit_p."', default_task_id='".$func->f('func_id')."' ");
				}else{
					$this->db->query("UPDATE tasks SET must_delete='0' WHERE task_id='".$task_exist."' ");
				}
			}
			$this->db->query("DELETE FROM tasks WHERE must_delete='1' AND project_id='".$in['project_id']."' ");
		}
		//budget
		if($in['budget_type']=='4'){
			foreach($in['tasks']['tasks'] as $key => $value){
				$this->db->query("UPDATE tasks  SET t_hours= '".return_value($value['t_hours'])."'  WHERE task_id='".$value['REL']."'");
			}
		}else{
			    $this->db->query("UPDATE tasks  SET t_hours= '0'  WHERE project_id='".$in['project_id']."'");
			    $app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
			    $smart_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
			    if($app->f('active') && $smart_id!=''){
					$tasks=$this->db->query("SELECT row_id FROM tasks WHERE project_id='".$in['project_id']."'")->getAll();
					$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$in['project_id']."' AND col_name='t_hours'");
					$final_array=array();
					foreach($tasks as $key => $value){
						$row_array=array('cells'=>array());
						$row_array['id']=$value['row_id'];
						$line_array['columnId']=$col_id;
						$line_array['value']=0;
						array_push($row_array['cells'], $line_array);
						array_push($final_array, $row_array);
					}
					$final_array=json_encode($final_array);

					$ch = curl_init();
					$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
					curl_setopt($ch, CURLOPT_POSTFIELDS, $final_array);
			        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows');

			        $put = curl_exec($ch);
			    	$info = curl_getinfo($ch);
			    }
		}
		if($in['budget_type']=='5'){
			foreach($in['users']['users'] as $key => $value){
				$this->db->query("UPDATE project_user  SET p_hours= '".return_value($value['p_hours'])."'  WHERE project_user_id='".$value['REL']."'");
			}
		}else{
			    $this->db->query("UPDATE project_user  SET p_hours= '0'  WHERE project_id='".$in['project_id']."'");
		}

		$this->db->query("UPDATE projects SET add_expense='0' WHERE project_id='".$in['project_id']."' ");
		$this->db->query("DELETE FROM project_expences WHERE project_id='".$in['project_id']."' ");

		if($in['add_expenses']){
			$this->db->query("UPDATE projects SET add_expense='1' WHERE project_id='".$in['project_id']."' ");
			if($in['invoice_method']){
				foreach ($in['expenses'] as $key => $value){
					if($value['EXP_CHECKED']){
						$this->db->query("INSERT INTO project_expences SET project_id='".$in['project_id']."', expense_id='".$value['EXPENSE_ID']."' ");
					}				
				}
			}
		}else{
			$this->db->query("UPDATE projects SET add_expense='0' WHERE project_id='".$in['project_id']."' ");
		}

		set_first_letter('projects',$in['name'],$this->field_n,$in['project_id']);
		msg::success(gm("Project details was updated"),"success");
		insert_message_log($this->pag, "{l}Project details was updated by{endl} ".get_user_name($_SESSION['u_id']),$this->field_n,$in['project_id']);
		return true;
	}

	/****************************************************************
	* function update_validate(&$in)                                          *
	****************************************************************/
	function update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('project_id', gm('ID'), 'required:exist[projects.project_id]', gm('Invalid ID'));

		$is_ok = $v->run();

		if($in['new']=='1' && !$in['quote_id'] && !$in['chose_type'] && $in['billable_type'] != '7'){
			msg::error(gm("Chose an option"),"error");
			$is_ok = false;
		}

		if($is_ok === true){
			//$v->field('name', gm('Name'), 'required');
			$v->field('serial_number', gm('serial_number'), 'required');
			if($in['billable_type'] == 6 && !$in['end_date']){
				msg::error(gm('End date required.'),"error");
				$is_ok = false;
			}
			if(!$v->run()){
				$is_ok = false;
			}else{
				if(!$in['buyer_id']){
					msg::error(gm('Invalid customer name. Please select one from the list.'),"error");
					$is_ok = false;
				}
			}
		}

		return $is_ok;
	}

	/****************************************************************
	* function add(&$in)                                          *
	****************************************************************/
	function add(&$in)
	{
		global $config;
		global $database_config;
		if(!$this->add_validate($in)){
			json_out($in);
		}
		$in['start_date']=strtotime($in['start_date']);
		$next_date = mktime(0,0,0,date('n',$in['start_date'])+1,date('j',$in['start_date']),date('Y',$in['start_date']));
		if(date('j',$in['start_date']) != date('j',$next_date) ){
			$next_date = mktime(0,0,0,date('n',$in['start_date'])+2,0,date('Y',$in['start_date']));
		}
		// $ref = $this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$in['customer_id']."' ");
		if(!$in['currency_type']){
			$in['currency_type'] = ACCOUNT_CURRENCY_TYPE;
		}
		if($in['customer_id']){
			$in['is_contact']==0;
			$company_name=trim($in['buyer_name']," ");
			$customer_id=$in['customer_id'];
		}else{
			$in['is_contact']==1;
			$company_name=trim($in['contact_name']," ");
			$customer_id=$in['contact_id'];
			$in['customer_id']=$customer_id;
		}
		if($in['internal_value']==1){
			$inv_method=0;
			$billable_type=$in['billable_type'];
			$project_rate=0;
		}else{
			$inv_method=1;
			$billable_type=$in['billable_type'];
			if($in['billable_type']==5 || $in['billable_type']==6){
				$project_rate=0;
			}else{
				$project_rate=$in['project_rate'];
			}
		}
		$budget_type=$in['budget_type'];
		$in['status_rate']=$project_rate;
		$same_address=0;
		if($in['sameAddress']!=1){
		    $same_address=$in['delivery_address_id'];
		}
		$in['project_id'] = $this->db->insert("INSERT INTO projects SET
														                  customer_id    				= '".$customer_id."',
														                  name	         				= '".$in['name']."',
														                  is_contact					= '".$in['is_contact']."',
														                  code	         				= '".$in['code']."',
														                  invoice_method 				= '".$inv_method."',
														                  billable_type	 			= '".$billable_type."',
														                  pr_h_rate	     				= '".return_value($in['pr_h_rate'])."',
														                  budget_type	 				= '".$budget_type."',
														                  t_pr_hour	     				= '".$in['t_pr_hour']."',
														                  t_pr_fees	     				= '".$in['t_pr_fees']."',
														                  show_budget_to_all			= '".$in['show_budget_to_all']."',
														                  send_email_alerts	    		= '".$in['send_email_alerts']."',
														                  alert_percent	        		= '".$in['alert_percent']."',
														                  notes	                		= '".$in['notes']."',
														                  end_date						= '".strtotime($in['end_date'])."',
														                  our_reference					= '".$in['our_reference']."',
														                  company_name					= '".$company_name."',
														                  serial_number					= '".$in['serial_number']."',
														                  currency_type					= '".$in['currency_type']."',
														                  quote_id 						= '".$in['quote_id']."',
														                  next_date						= '".$next_date."',
														                  retainer_budget				= '".return_value($in['retainer_budget'])."',
														                  step 							= '0',
														                  add_expense 					= '0',
														                  source_id 						= '".$in['source_id']."',
														                  status_rate					='".$project_rate."',
														                  bunit                               ='".$in['bunit_id']."',
														                  same_address					='".$same_address."',
														                  main_address_id				='".$in['main_address_id']."',
														                  contact_id 					='".$in['contact_id']."',
														                  customer_address 				='".$in['free_field']."',
														                  project_type 				='".$in['project_type']."' ");

		if($in['billable_type']==6){
			$this->db->query("UPDATE projects SET start_date='".$in['start_date']."' WHERE project_id='".$in['project_id']."'");
		}
		if($in['manager_id']){
			//$user_name = utf8_encode($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$in['manager_id']."' "));
			$user_name = utf8_encode($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id ",['user_id'=>$in['manager_id']]));
			//$manager_costs=$this->db_users->query("SELECT h_rate,daily_rate,h_cost,daily_cost FROM users WHERE user_id='".$in['manager_id']."' ");
			$manager_costs=$this->db_users->query("SELECT h_rate,daily_rate,h_cost,daily_cost FROM users WHERE user_id= :user_id ",['user_id'=>$in['manager_id']]);
			if($project_rate==0){
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."',
												user_id='".$in['manager_id']."',
												user_name='".addslashes($user_name)."',
												p_h_rate = '".$manager_costs->f('h_rate')."',
												p_hourly_cost ='".$manager_costs->f('h_cost')."',
												manager='1' ");
			}else{
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."',
												user_id='".$in['manager_id']."',
												user_name='".addslashes($user_name)."',
												p_daily_rate = '".$manager_costs->f('daily_rate')."',
												p_daily_cost ='".$manager_costs->f('daily_cost')."',
												manager='1' ");
			}
		}else{
			//$user_name = utf8_encode($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."' "));
			$user_name = utf8_encode($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]));
			//$manager_costs=$this->db_users->query("SELECT h_rate,daily_rate,h_cost,daily_cost FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$manager_costs=$this->db_users->query("SELECT h_rate,daily_rate,h_cost,daily_cost FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
			if($project_rate==0){
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."',
												user_id='".$_SESSION['u_id']."',
												user_name='".addslashes($user_name)."',
												p_h_rate = '".$manager_costs->f('h_rate')."',
												p_hourly_cost ='".$manager_costs->f('h_cost')."',
												manager='1' ");
			}else{
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."',
												user_id='".$_SESSION['u_id']."',
												user_name='".addslashes($user_name)."',
												p_daily_rate = '".$manager_costs->f('h_rate')."',
												p_daily_cost ='".$manager_costs->f('h_cost')."',
												manager='1' ");
			}
		}
		$i = 0;

    	$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
    	if($app->f('active')){
    		$ch = curl_init();
    		$headers=array('Content-Type: application/json','Authorization: Bearer '.$app->f('api'));
	    	$smart_data=array(
	    			'name'		=> $in['name'],
	    			'fromId'	=> '5066554783098756'
	    		);
	    	$smart_data=json_encode($smart_data);
	    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $smart_data);
	        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets');

	        $put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

	    	if($info['http_code']>=200 && $info['http_code']<300){
	    		$r_data=json_decode(preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', $put));
	    		$this->db->query("UPDATE projects SET smart_id='".$r_data->result->id."' WHERE project_id='".$in['project_id']."' ");
	    		$this->db_users->query("INSERT INTO smart_data SET sheet_id='".$r_data->result->id."', `database`='".$database_config['mysql']['database']."' ");
	    		$ch = curl_init();

	    		$ptype=$project_rate==0 ? 'Hourly Rate' : 'Daily Rate';

			    $col_data=array(
			    			array(
			    					'title'		=> 'Billable',
			    					'type'		=> 'CHECKBOX',
			    					'index'		=> 1,
			    					'locked'	=> false
			    				),
			    			array(
			    					'title'		=> $ptype,
			    					'type'		=> 'TEXT_NUMBER',
			    					'index'		=> 1,
			    					'locked'	=> false
			    				),
			    			array(
			    					'title'		=> 'Budget Hours',
			    					'type'		=> 'TEXT_NUMBER',
			    					'index'		=> 1,
			    					'locked'	=> false
			    				),
			    			array(
			    					'title'		=> 'Budget Task',
			    					'type'		=> 'TEXT_NUMBER',
			    					'index'		=> 1,
			    					'locked'	=> false
			    				),
			    			array(
			    					'title'		=> 'Closed',
			    					'type'		=> 'CHECKBOX',
			    					'index'		=> 1,
			    					'locked'	=> true
			    				)
			    );
			    $col_data=json_encode($col_data);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $col_data);
	        	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        	curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$r_data->result->id.'/columns');

	        	$put = curl_exec($ch);
	    		$info = curl_getinfo($ch);

	    		if($info['http_code']>=200 && $info['http_code']<300){
	    			$ch = curl_init();
	    			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        		curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$r_data->result->id.'/columns');

	        		$put = curl_exec($ch);
	    			$info = curl_getinfo($ch);

	    			if($info['http_code']>=200 && $info['http_code']<300){
	    				$f_col=json_decode(preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', $put));
	    				foreach($f_col->data as $key => $value){
	    					if((int)$value->index==6 || (int)$value->index > 8){
	    						continue;
	    					}else{
	    						switch((int)$value->index){
	    							case 0:
	    								$col_name='task_name';
	    								break;
	    							case 1:
	    								$col_name='billable';
	    								break;
	    							case 2:
	    								$col_name=$project_rate==0 ? 't_h_rate' : 't_daily_rate';
	    								break;
	    							case 3:
	    								$col_name='t_hours';
	    								break;
	    							case 4:
	    								$col_name='task_budget';
	    								break;
	    							case 5:
	    								$col_name='closed';
	    								break;
	    							case 7:
	    								$col_name='start_date';
	    								break;
	    							case 8:
	    								$col_name='end_date';
	    								break;
	    						}
	    						$this->db->query("INSERT INTO smart_cols SET project_id='".$in['project_id']."',
	    																		col_name='".$col_name."',
	    																		col_id='".$value->id."',
	    																		`index`='".(int)$value->index."' ");
	    					}
	    				}
	    				$ch = curl_init();
	    				$web_data=array(
	    					'name'				=> 'Webhook_'.$r_data->result->id,
	    					'callbackUrl'		=> $config['site_url'].'smart_hook.php',
	    					'scope'				=> 'sheet',
	    					'scopeObjectId'		=> $r_data->result->id,
	    					'version'			=> 1,
	    					'events'			=> array("*.*")
	    				);
	    				$web_data=json_encode($web_data);		
		    			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		        		curl_setopt($ch, CURLOPT_POST, true);
			    		curl_setopt($ch, CURLOPT_POSTFIELDS, $web_data);
		        		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        		curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/webhooks');

		        		$put = curl_exec($ch);
		    			$info = curl_getinfo($ch);
		    			if($info['http_code']>=200 && $info['http_code']<300){
		    				$hook_data=json_decode(preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', $put));
		    				$this->db->query("UPDATE projects SET smart_hook_id='".$hook_data->result->id."' WHERE project_id='".$in['project_id']."' ");
		    			
		    				$ch = curl_init();
		    				$hook_enable=array(
		    					'enabled'				=> true
		    				);
		    				$hook_enable=json_encode($hook_enable);		
			    			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			        		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
			        		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				    		curl_setopt($ch, CURLOPT_POSTFIELDS, $hook_enable);
			        		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			        		curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/webhooks/'.$hook_data->result->id);

			        		$put = curl_exec($ch);
			    			$info = curl_getinfo($ch);
		    			}
	    			}
	    		}
	    	}
    	}

		if($in['quote_id']){
			$this->add_quote_data($in);
			$tracking_data=array(
		            'target_id'         => $in['project_id'],
		            'target_type'       => '3',
		            'target_buyer_id'   => $customer_id,
		            'lines'             => array(
		                                          array('origin_id'=>$in['quote_id'],'origin_type'=>'2')
		                                    )
		          );
		      addTracking($tracking_data);
		}
		if($in['deal_id']){
			$tracking_data=array(
		            'target_id'         => $in['project_id'],
		            'target_type'       => '3',
		            'target_buyer_id'   => $customer_id,
		            'lines'             => array(
		                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
		                                    )
		          );
		      addTracking($tracking_data);
		}
		if(!$in['quote_id'] && !$in['deal_id']){
			$tracking_data=array(
		            'target_id'         => $in['project_id'],
		            'target_type'       => '3',
		            'target_buyer_id'   => $customer_id,
		            'lines'             => array()
		          );
		      addTracking($tracking_data);
		}

		if($in['billable_type'] == 7){
			$this->db->query("UPDATE projects SET step='2' WHERE project_id='{$in['project_id']}' ");
		}

		set_first_letter('projects',$in['name'],$this->field_n,$in['project_id']);

        msg::success(gm("Project added"),"success");
        insert_message_log($this->pag,'{l}Project added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['project_id'],false);
		/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
																		AND name		= 'project-projects_show_info'	");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
																		AND name		= :name	",['user_id'=>$_SESSION['u_id'],'name'=>'project-projects_show_info']);																		
		if(!$show_info->move_next()) {
			/*$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name = 'project-projects_show_info',
																value = '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET 	user_id = :user_id,
																name = :name,
																value = :value ",
															['user_id' => $_SESSION['u_id'],
															 'name' => 'project-projects_show_info',
															 'value' => '1']
														);																
		} else {
			/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
															   AND name = 'project-projects_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
															   AND name = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'project-projects_show_info']);															   
		}

		$count = $this->db->field("SELECT COUNT(project_id) FROM projects ");
	    if($count == 1){
	      doManageLog('Created the first project.','',array( array("property"=>'first_module_usage',"value"=>'Project') ));
	    }
	    return true;
	}

	/****************************************************************
	* function add_validate(&$in)                                   *
	****************************************************************/
	function add_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('name', gm('Name'), 'required');
		if(!$in['project_id']){
			$v->field('serial_number', 'ID', 'unique[projects.project_id]', gm('Unique serial number required'));
		}
		
		if(!$in['billable_type']){
			msg::error(gm('Project type required'),"error");
			return false;
		}
		if($in['billable_type'] == 6 && !$in['end_date']){
			msg::error(gm('End date required.'),"error");
			return false;
		}
		if($in['quote_id']){
			$t_nr=0;
			$t_nr_line=0;
			$art_nr=0;
			$art_nr_line=0;
			foreach ($in['quote_group_data'] as $key => $value) {
				$t_nr_line++;
				if($value['checked']){
					$t_nr++;
				}
			}
			foreach ($in['quote_group_articles'] as $key => $value) {
				$art_nr_line++;
				if($value['checked']){
					$art_nr++;
				}
			}
			if($t_nr==0 && $art_nr==0){
				msg::error(gm('Select at least one task list or article list'),"error");
				return false;
			}
		}
		//$in['serial_number']=generate_project_number(DATABASE_NAME);
		if(!$v->run()){
			$is_ok = false;
		}else{
			if(!$in['customer_id'] && !$in['contact_id']){
				msg::error(gm('Invalid customer or contact name. Please select one from the list.'),"error");
				$is_ok = false;
			}
		}

		return $is_ok;
	}

	function add_quote_data(&$in){
		global $config;
				$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
				$sheet_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
				$ptype=$in['status_rate']==0 ? 't_h_rate' : 't_daily_rate';
				$p_cols=$this->db->query("SELECT * FROM smart_cols WHERE project_id='".$in['project_id']."' AND `index`<8 ORDER BY `index` ASC")->getAll();
				foreach ($in['quote_group_data'] as $key => $value) {
					if($value['checked']){
						$group_ids.= $value['group_id'].',';
					}				
				}
				$group_ids = rtrim($group_ids,',');
				$t_amount = 0;
				if($in['quote_group_data'] && !empty($in['quote_group_data']) && !empty($group_ids)){
					$lines = $this->db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
											   INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
											   LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
											   WHERE tblquote_line.quote_id='".$in['quote_id']."'
											   AND tblquote_line.content_type='1'
											   AND tblquote_version.active='1'
											   AND tblquote_line.line_type<'3'
											   AND (tblquote_line.article_id='0' OR (tblquote_line.article_id>'0' AND pim_articles.is_service='1'))
											   AND tblquote_line.is_tax='0'
											   AND tblquote_line.group_id IN(".$group_ids.")
											   ORDER BY tblquote_line.group_id ASC, id ASC  ");

					while ($lines->next()) {
						$text = str_replace(array("\r\n", "\r"), "\n", $lines->f('name'));
						$first_line = explode("\n", $text);
						$billable = '0';
						$t_h_rate = $lines->f('price');
						$t_hours = $lines->f('quantity');
						if($in['status_rate']==0){
							$t_id=$this->db->insert("INSERT INTO tasks SET project_id='".$in['project_id']."', task_name='".addslashes($first_line[0])."', billable='1', t_h_rate='".$t_h_rate."', t_hours='".$t_hours."', task_budget='".$lines->f('amount')."' ");
						}else{
							$t_id=$this->db->insert("INSERT INTO tasks SET project_id='".$in['project_id']."', task_name='".addslashes($first_line[0])."', billable='1', t_daily_rate='".$t_h_rate."', t_hours='".$t_hours."', task_budget='".$lines->f('amount')."' ");
						}
						$i++;
						$t_amount += $lines->f('amount');
						if($app->f('active') && $sheet_id!=''){
							$smart_line=array(
								'task_name'		=> $first_line[0],
								'billable'		=> $in['internal_value']==1 ? false : true,
								 $ptype   		=> $t_h_rate,
								 't_hours'		=> $t_hours,
								 'task_budget'	=> $lines->f('amount'),
								 'closed'		=> false
							);
							$final_data=array('cells'=>array(),'toBottom'=>true);
							foreach($p_cols as $key => $value){
								$temp_data['columnId']=$value['col_id'];
								$temp_data['value']=$smart_line[$value['col_name']];
								array_push($final_data['cells'], $temp_data);
							}
							$final_data=json_encode($final_data);

							$ch = curl_init();
							$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    		curl_setopt($ch, CURLOPT_POST, true);
				   	 		curl_setopt($ch, CURLOPT_POSTFIELDS, $final_data);
		        			//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        			curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$sheet_id.'/rows');

		        			$put = curl_exec($ch);
		    				$info = curl_getinfo($ch);

		    				if($info['http_code']>=200 && $info['http_code']<300){
		    					$f_col=json_decode(preg_replace('/\"([a-zA-Z0-9_]+)\":\s?(\d{14,})/', '"${1}":"${2}"', $put));
		    					$this->db->query("UPDATE tasks SET row_id='".$f_col->result->id."' WHERE task_id='".$t_id."' ");
		    				}
						}
					}
				}

				foreach ($in['quote_group_articles'] as $key => $value) {
					if($value['checked']){
						$group_arts.= $value['group_a_id'].',';
					}		 		
				}
				$group_arts = rtrim($group_arts,',');
				if($in['quote_group_articles'] && !empty($in['quote_group_articles']) && !empty($group_arts)){
					$articles = $this->db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
									INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
									LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
									WHERE tblquote_line.quote_id='".$in['quote_id']."'
									AND tblquote_line.content_type='1'
									AND tblquote_line.line_type<'3'
									AND tblquote_version.active='1'
									AND (tblquote_line.article_id>'0' AND pim_articles.is_service='0')
									AND tblquote_line.group_id IN(".$group_arts.")
									ORDER BY tblquote_line.group_id ASC, id ASC ");
					while($articles->next()){
					 	$purchase_price=$this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$articles->f('article_id')."' AND base_price='1' ");
					 	$margin_nr=$articles->f('price')-$purchase_price;
					 	$margin_percent=($articles->f('price')-$purchase_price)/$articles->f('price')*100;
					 	$this->db->query("INSERT INTO project_articles SET project_id='".$in['project_id']."',
									 	name='".addslashes($articles->f('name'))."',
									 	article_id='".$articles->f('article_id')."',
									 	quantity='".$articles->f('quantity')."',
									 	price='".$articles->f('price')."',
									 	purchase_price='".$purchase_price."',
									 	margin='".$margin_nr."',
									 	margin_per='".$margin_percent."' ");
					 }
				}

			#setting the budget type to 4;
			$this->db->query("UPDATE projects SET budget_type='".$in['budget_type']."' WHERE project_id='".$in['project_id']."' ");
			//$this->db->query("UPDATE tblquote SET pending = '0' WHERE id = '".$in['quote_id']."' ");

			return true;
		//}
	}

	function approveAllHours(&$in)
	{
		global $config;
		if(!$this->delete_validate($in)){
			json_out($in);
		}
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		if($in['end_date']){
			$in['end_date']=strtotime($in['end_date']);
			$in['end_date']=mktime(23,59,59,date('n',$in['end_date']),date('j',$in['end_date']),date('Y',$in['end_date']));
			$this->db->query("UPDATE task_time SET approved='1' WHERE project_id='".$in['project_id']."' AND date<='".$in['end_date']."' ");
		}else{
			$this->db->query("UPDATE task_time SET approved='1' WHERE project_id='".$in['project_id']."' ");
		}
		if($in['close_all']){
			$this->db->query("UPDATE tasks SET closed='1' WHERE project_id='".$in['project_id']."' ");
			$this->db->query("UPDATE projects SET closed='1' WHERE project_id='".$in['project_id']."' ");
			$smart_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
			if($app->f('active')&& $smart_id!=''){
				$tasks=$this->db->query("SELECT row_id FROM tasks WHERE project_id='".$in['project_id']."'")->getAll();
				$final_array=array();
				foreach($tasks as $key => $value){
					$row_array=array();
					$row_array['id']=$value['row_id'];
					$row_array['locked']=true;
					array_push($final_array, $row_array);
				}
				$final_array=json_encode($final_array);

				$ch = curl_init();
				$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $final_array);
		        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows');

		        $put = curl_exec($ch);
		    	$info = curl_getinfo($ch);
			}
		}
		msg::success(gm("Project details was updated"),"success");
		return true;
	}

	function approveAllTask(&$in)
	{
		global $config;
		if(!$this->delete_validate($in)){
			json_out($in);
		}
		$app = $this->db->query("SELECT api,active FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		$tasks_all="";
		foreach ($in['tasks']['tasks'] as $key => $value) {
			if($value['tasks_approve']){
				$this->db->query("UPDATE task_time SET submited='1', approved='1' WHERE project_id='".$in['project_id']."' AND task_id='".$value['REL']."' ");
				if($in['close_tasks']){
					$tasks_all.=$value['REL'].',';
					$this->db->query("UPDATE tasks SET closed='1' WHERE  project_id='".$in['project_id']."' AND task_id='".$value['REL']."' ");
				}
			}	
		}
		if(!empty($tasks_all)){
			$tasks_all=rtrim($tasks_all,",");
			$smart_id=$this->db->field("SELECT smart_id FROM projects WHERE project_id='".$in['project_id']."'");
			if($app->f('active') && $smart_id!=''){
				$col_id=$this->db->field("SELECT col_id FROM smart_cols WHERE project_id='".$in['project_id']."' AND col_name='closed'");
				$tasks=$this->db->query("SELECT row_id FROM tasks WHERE task_id IN(".$tasks_all.") ")->getAll();
				$final_array=array();
				foreach($tasks as $key => $value){
					$row_array=array('cells'=>array());
					$row_array['id']=$value['row_id'];
					$line_array['columnId']=$col_id;
					$line_array['value']=true;
					array_push($row_array['cells'], $line_array);
					array_push($final_array, $row_array);
				}
				$final_array=json_encode($final_array);

				$ch = curl_init();
				$headers=array('Content-Type: application/json','Smartsheet-Change-Agent: Subscriber','Authorization: Bearer '.$app->f('api'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $final_array);
		        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets/'.$smart_id.'/rows');

		        $put = curl_exec($ch);
		    	$info = curl_getinfo($ch);

			}
		}
		msg::success(gm("Project details was updated"),"success");
		return true;
	}

	function unApproveHours(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
		}
		if(!$in['week']){
			msg::error(gm("Select week"),"error");
			json_out($in);
		}
		list($start_week,$end_week) = explode('-', $in['week']);
		if(!$start_week || !$end_week){
			msg::error(gm("Select week"),"error");
			json_out($in);
		}
		$this->db->query("UPDATE task_time SET submited='0',approved='0'
											WHERE project_id='".$in['project_id']."' AND billed='0' AND date BETWEEN '".$start_week."' AND '".$end_week."' ");
		msg::success(gm("Project details was updated"),"success");
		return true;

	}
	function update_particle_delivered(&$in){
		$this->db->query("UPDATE project_purchase SET delivered='".$in['delivered']."' WHERE project_purchase_id='".$in['project_purchase_id']."'  ");
		msg::success(gm("Purchase updated."),"success");
		return true;
	}

	function add_expense(&$in){

		if(!$this->validate_expense($in)){
			return false;
		}
		$this->db->query("INSERT INTO expense SET name='".$in['NAME']."', unit_price='".return_value($in['PRICE_NORMAL'])."', unit='".$in['PER']."' ");

		msg::success ( gm("Expense category added"),'success');
		return true;
	}
	function update_expense(&$in){

		if(!$this->validate_expense($in)){
			return false;
		}

		$this->db->query("UPDATE expense SET name='".$in['NAME']."', unit_price='".return_value($in['PRICE_NORMAL'])."', unit='".$in['PER']."' WHERE expense_id='".$in['Q_ID']."' ");

		msg::success ( gm("Expense category updated"),'success');
		return true;
	}
	function validate_expense(&$in){

		$v = new validation($in);
		if($in['do']=='project-settings-project-add_expense'){
			$v->field('NAME', gm('Name'), 'required:unique[expense.name]');
		}else{
			$v->field('NAME', gm("Name"), "required:unique[expense.name.( expense_id!='".$in['Q_ID']."')]");
		}
		return $v->run();
	}
	function delete_expense(&$in){

		$this->db->query("SELECT expense_id FROM expense WHERE expense_id='".$in['Q_ID']."' ");
		if(!$this->db->move_next()){
			msg::error ( gm("Invalid ID"),'error');
			return false;
		}
		$exist_on_project=$this->db->field("SELECT project_id FROM project_expenses WHERE expense_id='".$in['Q_ID']."' ");
		if($exist_on_project){
			msg::error (gm("One or more expenses exist in this category"),'error');
			return false;
		}
		$v = new validation($in);
		$v->field('Q_ID', gm('ID'), 'required:exist[expense.expense_id]');
		$is_ok=$v->run();
		if ($is_ok)
		{
			$this->db->query("DELETE FROM expense WHERE expense_id='".$in['Q_ID']."' ");
			msg::success ( gm("Expense category deleted"),'success');
		}

		return $is_ok;
	}
	function project_naming(&$in){
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_PROJECT_DIGIT_NR']."' WHERE constant_name='ACCOUNT_PROJECT_DIGIT_NR' ");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_PROJECT_START']."' WHERE constant_name='ACCOUNT_PROJECT_START' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function use_html(&$in)
	{
		$this->db->query("UPDATE sys_message SET use_html='".$in['use_html']."' WHERE lang_code = '".$in['lang_code']."' ");
/*		$is_line_code = $this->db->field("SELECT sys_message_id FROM sys_message WHERE name = '".$in['name']."' AND lang_code = '".$in['lang_code']."' ");
		if($is_line_code){
			$this->db->query("UPDATE sys_message SET use_html='".$in['use_html']."' WHERE name='".$in['name']."' AND lang_code = '".$in['lang_code']."' ");
		}else{
			$this->db->query("INSERT INTO sys_message SET use_html='".$in['use_html']."', name='".$in['name']."' , lang_code = '".$in['lang_code']."' ");
		}*/
		return true;
	}
	function default_message(&$in){
		if($in['TEXT_TIMESHEET']){
			if($in['USER_HTML']){
				$this->db->query("UPDATE sys_message SET html_content='".$in['TEXT_TIMESHEET']."', subject='".$in['SUBJECT_TIMESHEET']."' WHERE name='timesheetmess' AND lang_code='".$in['LANG_ID']."' ");
			}else{
				$this->db->query("UPDATE sys_message SET text='".$in['TEXT_TIMESHEET']."', subject='".$in['SUBJECT_TIMESHEET']."' WHERE name='timesheetmess' AND lang_code='".$in['LANG_ID']."' ");
			}
		}
		if($in['TEXT_REJECT']){
			$this->db->query("UPDATE sys_message SET text='".$in['TEXT_REJECT']."', subject='".$in['SUBJECT_REJECT']."' WHERE name='timesheet_rejectmess' AND lang_code='".$in['LANG_ID']."' ");
		}
		if($in['TEXT_REMINDER']){
			$this->db->query("UPDATE sys_message SET text='".$in['TEXT_REMINDER']."', subject='".$in['SUBJECT_REMINDER']."' WHERE name='timesheet_remindermess' AND lang_code='".$in['LANG_ID']."' ");
		}
		if($in['TEXT_APPROVED']){
			$this->db->query("UPDATE sys_message SET text='".$in['TEXT_APPROVED']."', subject='".$in['SUBJECT_APPROVED']."' WHERE name='timesheet_approvemess' AND lang_code='".$in['LANG_ID']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function setcnst(&$in)
	{
		//$this->db->query("UPDATE settings SET value='".$in['checked_useadhoc']."' WHERE constant_name='USE_ADHOC_TMS' ");
		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='TIME_AUTO_APPROVE' ");
		if(is_null($const)){
			$this->db->query("INSERT INTO settings SET constant_name='TIME_AUTO_APPROVE',value='".$in['auto_app']."' ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['auto_app']."' WHERE constant_name='TIME_AUTO_APPROVE' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function label_update(&$in)
	{
		$this->db->query("UPDATE label_language_time SET
								timeframe='".$in['timeframe']."',
								hours='".$in['hours']."',
                                total='".$in['total']."',
                                `project`='".$in['project']."',
                                task='".$in['task']."',
                                customer='".$in['customer']."',
                                consultant='".$in['consultant']."',
                                comment='".$in['comment']."',
                                page='".$in['page']."'
		  					WHERE label_language_id='".$in['label_language_id']."'");
		msg::success ( gm("Changes have been saved."),'success');
        return true;
	}
	function uploadify(&$in)
	  {
	    $response=array();
	    if (!empty($_FILES)) {
	      $tempFile = $_FILES['Filedata']['tmp_name'];
	      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
	      $targetPath = 'upload/'.DATABASE_NAME;
	      $in['name'] = 'p_logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
	      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
	      // Validate the file type
	      $fileTypes = array('jpg','jpeg','gif'); // File extensions
	      $fileParts = pathinfo($_FILES['Filedata']['name']);
	      @mkdir(str_replace('//','/',$targetPath), 0775, true);
	      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
	        move_uploaded_file($tempFile,$targetFile);
	        global $database_config;

	        $database_2 = array(
	          'hostname' => $database_config['mysql']['hostname'],
	          'username' => $database_config['mysql']['username'],
	          'password' => $database_config['mysql']['password'],
	          'database' => DATABASE_NAME,
	        );
	        $db_upload = new sqldb($database_2);
	        $logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_TIMESHEET' ");
	        if($logo == ''){
	          if(defined('ACCOUNT_LOGO')){
	            $db_upload->query("UPDATE settings SET
	                               value = 'upload/".DATABASE_NAME."/".$in['name']."'
	                               WHERE constant_name='ACCOUNT_LOGO_TIMESHEET' ");
	          }else{
	            $db_upload->query("INSERT INTO settings SET
	                               value = 'upload/".DATABASE_NAME."/".$in['name']."',
	                               constant_name='ACCOUNT_LOGO_TIMESHEET' ");
	          }
	        }
	        $response['success'] = 'success';
	        echo json_encode($response);
	        // ob_clean();
	      } else {
	        $response['error'] = gm('Invalid file type.');
	        echo json_encode($response);
	        // echo gm('Invalid file type.');
	      }
	    }
	  }
	  function delete_logo(&$in)
	  {
	      if($in['name'] == ACCOUNT_LOGO_TIMESHEET){
	        msg::error ( gm('You cannot delete the default logo'),'error');
	        json_out($in);
	        return true;
	      }
	      if($in['name'] == '../img/no-logo.png'){
	        msg::error ( gm('You cannot delete the default logo'),'error');
	        json_out($in);
	        return true;
	      }
	      $exists = $this->db->field("SELECT COUNT(service_id) FROM servicing_support WHERE pdf_logo='".$in['name']."' ");
	      if($exists>=1){
	        msg::error ( gm('This logo is set for one ore more invoices.'),'error');
	        json_out($in);
	        return false;
	      }
	      @unlink($in['name']);
	      msg::success ( gm('File deleted'),'success');
	      return true;
	  }
	  function set_default_logo(&$in)
	  {

	      $this->db->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_LOGO_TIMESHEET' ");
	      msg::success (gm("Default logo set."),'success');
	      json_out($in);
	      return true;
	  }
	function settings(&$in){

		$this->db->query("UPDATE default_data SET active='".(int)$in['client']."' WHERE default_name='timesheet_print' AND value='client'");
		$this->db->query("UPDATE default_data SET active='".(int)$in['project']."' WHERE default_name='timesheet_print' AND value='project'");
		$this->db->query("UPDATE default_data SET active='".(int)$in['task']."' WHERE default_name='timesheet_print' AND value='task'");
		$this->db->query("UPDATE default_data SET active='".(int)$in['comment']."' WHERE default_name='timesheet_print' AND value='comment'");

		msg::success ( gm('Changes saved'),'success');
		/*json_out($in);*/
		return true;
	}
	function default_pdf_format_header(&$in)
  	{

	  $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
      $this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_PROJECT_HEADER_PDF_FORMAT'");
      $this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PROJECT_PDF_MODULE'");
      global $config;
      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function default_pdf_format_body(&$in)
  {

	  $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
      $this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_PROJECT_BODY_PDF_FORMAT'");
    
      $this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PROJECT_PDF_MODULE'");
      global $config;
    
      msg::success ( gm('Changes saved'),'success');
    
    return true;
  }
  function default_pdf_format_footer(&$in)
  {

      $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
      $this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_PROJECT_FOOTER_PDF_FORMAT'");
      $this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PROJECT_PDF_MODULE'");
      global $config;

      msg::success ( gm('Changes saved'),'success');
   
    return true;
  }
  function reset_data(&$in){
    if($in['header'] && $in['header']!='undefined'){
      $variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
    }elseif($in['footer'] && $in['footer']!='undefined'){
      $variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
    }
    $result=array("var_data" => $variable_data);
    json_out($result);
    return true;
  }

  function pdfSaveData(&$in){

    if($in['header']=='header'){
      $this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='project' AND type='".$in['header']."' AND initial='1' ");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");
      if($exist){
        $this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->db->query("INSERT INTO tblquote_pdf_data SET master='project',
                          type='".$in['header']."',
                          content='".$in['variable_data']."',
                          initial='1', 
                          layout='".$in['layout']."',
                          `default`='1' ");
      }
    }else{
      $this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='project' AND type='".$in['footer']."' AND initial='1' ");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

      if($exist){
        $this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->db->query("INSERT INTO tblquote_pdf_data SET master='project',
                          type='".$in['footer']."',
                          content='".$in['variable_data']."',
                          initial='1', 
                          layout='".$in['layout']."',
                          `default`='1' ");
      }
    }
    msg::success ( gm('Data saved'),'success');
    return true;
  }
  	function add_field(&$in)
	{
		$in['field_id'] = $this->db->insert("INSERT INTO project_".$in['table']." SET name='".$in['name']."' ");
		msg::success(gm('Field added.'),'success');
		return true;
	}

	function UpdateField(&$in){
	 	$this->db->insert("UPDATE project_".$in['table']." SET name='".$in['value']."' WHERE id='".$in['id']."' ");
		msg::success(gm('Field updated.'),'success');
		return true;
	}
	function DeleteField(&$in){

		$this->db->query("DELETE FROM project_".$in['table']." WHERE id='".$in['field_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function add_internal(&$in)
	{
		$this->db->query("UPDATE customers SET is_admin='2' WHERE customer_id='".$in['customer_id']."' ");
		msg::success(gm('Internal company added'),'success');
		return true;
	}
	function delete_internal(&$in)
	{
		$this->db->query("UPDATE customers SET is_admin='0' WHERE customer_id='".$in['customer_id']."' ");
		msg::success(gm('Internal company deleted'),'success');
		return true;
	}
	function updateBillableType(&$in){
		$this->db->query("UPDATE projects SET billable_type='".$in['billable_type']."' WHERE project_id='".$in['project_id']."' ");
		msg::success(gm('Project type updated'),'success');
		return true;
	}

	function action($in)
	{
		if(!$this->validate_id($in)){
			json_out($in);
		}
		switch ($in['action_type']) {
			case 'archived':
				$this->db->query("UPDATE default_data SET active='0' WHERE default_id='".$in['task_id']."' ");
				$task_group_id=$this->db->field("SELECT task_group_id FROM task_group WHERE task_id='".$in['task_id']."' ");
				$total_tasks=$this->db->field("SELECT total_tasks FROM task_groups WHERE task_group_id 	='".$task_group_id."' ");
				if($task_group_id!=0){
					   $this->db->query("UPDATE task_groups SET total_tasks='".($total_tasks-1)."' WHERE task_group_id='".$task_group_id."' ");
				}
				$this->db->query("DELETE FROM  task_group WHERE task_id='".$in['task_id']."' ");
				msg::success(gm('Task archived'),'success');
				break;
			case 'activated':
				$this->db->query("UPDATE default_data SET active='1' WHERE default_id='".$in['task_id']."' ");
				msg::success(gm('Task activated'),'success');
				break;
			case 'deleted':
				$this->db->query("DELETE FROM default_data WHERE default_id='".$in['task_id']."' ");
				$this->db->query("DELETE FROM default_data WHERE default_main_id='".$in['task_id']."' ");
				msg::success(gm('Task deleted'),'success');
				break;
			default:
				break;
		}
		return true;
	}

	function validate_id(&$in)
	{
		$v = new validation($in);
		$v->f('task_id','Price','exist[default_data.default_id]');
		return $v->run();
	}

	function default_task_add(&$in)
	{
		if(!$this->validate_task_add($in)){
			json_out($in);
		}
		$in['task_id'] = $this->db->insert("INSERT INTO default_data SET default_name='".$in['task_name']."', type='task_no' ");
		if($in['task_rate']){
			$this->db->query("INSERT INTO default_data SET default_name='task_rate', value='".return_value($in['task_rate'])."', default_main_id='".$in['task_id']."' ");
		}
		if($in['daily_rate']){
			$this->db->query("INSERT INTO default_data SET default_name='daily_rate', value='".return_value($in['daily_rate'])."', default_main_id='".$in['task_id']."' ");
		}
		if($in['task_billable']){
			$this->db->query("INSERT INTO default_data SET default_name='task_billable', value='yes', default_main_id='".$in['task_id']."' ");
		}
		msg::success(gm('Task added'),'success');
		json_out($in);
	}

	/**
	 * validate the add task process
	 *
	 * @return bool
	 * @author Mp
	 **/
	function validate_task_add(&$in)
	{
		$v = new validation($in);
		$v->f('task_name', 'Price', 'required');
		$v->f('task_rate', 'Hourly', 'required');
		$v->f('daily_rate', 'Daily', 'required');
		return $v->run();
	}

	/**
	 * add a default task
	 *
	 * @return bool
	 * @author Mp
	 **/
	function default_task_update(&$in)
	{
		if(!$this->validate_task_update($in)){
			json_out($in);
		}
		$this->db->query("UPDATE default_data SET default_name='".$in['task_name']."', type='task_no' WHERE default_id='".$in['task_id']."' ");
		$this->db->query("UPDATE task_group SET task_name='".$in['task_name']."' WHERE task_id='".$in['task_id']."' ");
		$this->db->query("DELETE FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
		$this->db->query("DELETE FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='daily_rate' ");
		if($in['task_rate']){
			$this->db->query("INSERT INTO default_data SET default_name='task_rate', value='".return_value($in['task_rate'])."', default_main_id='".$in['task_id']."' ");
		}
		if($in['daily_rate']){
			$this->db->query("INSERT INTO default_data SET default_name='daily_rate', value='".return_value($in['daily_rate'])."', default_main_id='".$in['task_id']."' ");
		}

		$exist = $this->db->query("SELECT * FROM  `default_data`
									LEFT JOIN tasks ON default_data.default_name = tasks.task_name
									LEFT JOIN task_time ON tasks.task_id = task_time.task_id
									WHERE default_name = '".$in['task_name']."' GROUP BY task_name");
		if(!$exist->f('task_time_id')){
			$this->db->query("DELETE FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
			if($in['task_billable']){
				$this->db->query("INSERT INTO default_data SET default_name='task_billable', value='yes', default_main_id='".$in['task_id']."' ");
			}
			msg::success(gm('Task updated'),'success');
		}else{
			$bill = $this->db->field("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
			if($bill == 'yes' && !$in['task_billable']){
				msg::notice(gm("There are entries associated with this setting"),'notice');
				json_out($in);
			}elseif($bill == 'no' && $in['task_billable']){
				msg::notice(gm("There are entries associated with this setting"),'notice');
				json_out($in);
			}else{
				msg::success(gm('Task updated'),'success');
			}
		}
		json_out($in);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Mp
	 **/
	function validate_task_update(&$in)
	{
		$v = new validation($in);
		$v->f('task_id','Price','exist[default_data.default_id]');
		if($v->run()){
			return $this->validate_task_add($in);
		}
		return false;
	}


} // END class

?>