<?php
/**
 * undocumented class
 *
 * @package default
 * @author Mp
 **/
class timetracker
{

	var $pag = 'project';
	var $field_n = 'project_id';
	private $db;

	function __construct() {

		$this -> db = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$this->db_users = new sqldb($db_config);
	}

	function default_pdf_format_header(&$in)
  	{
  		$t1=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_CUSTOME_PROJECT_PDF' ");
  		if(is_null($t1)){
  			$this->db->query("INSERT INTO settings SET constant_name='USE_CUSTOME_PROJECT_PDF',value='0' ");
  		}else{
  			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
  		}
	  	$t2=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_PROJECT_HEADER_PDF_FORMAT' ");
	  	if(is_null($t2)){
	  		$this->db->query("INSERT INTO settings set `constant_name`='ACCOUNT_PROJECT_HEADER_PDF_FORMAT', value='".$in['type']."' ");
	  	}else{
	  		$this->db->query("UPDATE settings set value='".$in['type']."' where `constant_name`='ACCOUNT_PROJECT_HEADER_PDF_FORMAT'");
	  	}
      	$t3=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_PROJECT_PDF_MODULE' ");
      	if(is_null($t3)){
      		$this->db->query("INSERT INTO settings SET constant_name = 'ACCOUNT_PROJECT_PDF_MODULE',value='1' ");
      	}else{
      		$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PROJECT_PDF_MODULE'");
      	}
      
      	msg::success ( gm('Changes saved'),'success');
    		return true;
  	}
  	function default_pdf_format_body(&$in)
  	{
  		$t1=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_CUSTOME_PROJECT_PDF' ");
  		if(is_null($t1)){
  			$this->db->query("INSERT INTO settings SET constant_name='USE_CUSTOME_PROJECT_PDF',value='0' ");
  		}else{
  			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
  		}
  		$t2=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_PROJECT_BODY_PDF_FORMAT' ");
	  	if(is_null($t2)){
	  		$this->db->query("INSERT INTO settings set `constant_name`='ACCOUNT_PROJECT_BODY_PDF_FORMAT', value='".$in['type']."' ");
	  	}else{
	  		$this->db->query("UPDATE settings set value='".$in['type']."' where `constant_name`='ACCOUNT_PROJECT_BODY_PDF_FORMAT'");
	  	}
    		$t3=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_PROJECT_PDF_MODULE' ");
      	if(is_null($t3)){
      		$this->db->query("INSERT INTO settings SET constant_name = 'ACCOUNT_PROJECT_PDF_MODULE',value='1' ");
      	}else{
      		$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PROJECT_PDF_MODULE'");
      	}
    
      	msg::success ( gm('Changes saved'),'success');
    
    		return true;
  	}
  	function default_pdf_format_footer(&$in)
  	{
      	$t1=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_CUSTOME_PROJECT_PDF' ");
  		if(is_null($t1)){
  			$this->db->query("INSERT INTO settings SET constant_name='USE_CUSTOME_PROJECT_PDF',value='0' ");
  		}else{
  			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PROJECT_PDF' ");
  		}
  		$t2=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_PROJECT_FOOTER_PDF_FORMAT' ");
	  	if(is_null($t2)){
	  		$this->db->query("INSERT INTO settings set `constant_name`='ACCOUNT_PROJECT_FOOTER_PDF_FORMAT', value='".$in['type']."' ");
	  	}else{
	  		$this->db->query("UPDATE settings set value='".$in['type']."' where `constant_name`='ACCOUNT_PROJECT_FOOTER_PDF_FORMAT'");
	  	}
      	$t3=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_PROJECT_PDF_MODULE' ");
      	if(is_null($t3)){
      		$this->db->query("INSERT INTO settings SET constant_name = 'ACCOUNT_PROJECT_PDF_MODULE',value='1' ");
      	}else{
      		$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PROJECT_PDF_MODULE'");
      	}

      	msg::success ( gm('Changes saved'),'success');
   
    		return true;
  	}

  	function reset_data(&$in){
	    if($in['header'] && $in['header']!='undefined'){
	      $variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
	    }elseif($in['footer'] && $in['footer']!='undefined'){
	      $variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
	    }
	    $result=array("var_data" => $variable_data);
	    json_out($result);
	    return true;
	  }

	  function pdfSaveData(&$in){

	    if($in['header']=='header'){
	      $this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='project' AND type='".$in['header']."' AND initial='1' ");
	      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");
	      if($exist){
	        $this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->db->query("INSERT INTO tblquote_pdf_data SET master='project',
	                          type='".$in['header']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1' ");
	      }
	    }else{
	      $this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='project' AND type='".$in['footer']."' AND initial='1' ");
	      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

	      if($exist){
	        $this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->db->query("INSERT INTO tblquote_pdf_data SET master='project',
	                          type='".$in['footer']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1' ");
	      }
	    }
	    msg::success ( gm('Data saved'),'success');
	    return true;
	  }

}