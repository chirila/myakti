<?php
/************************************************************************
* @Author: MedeeaWeb Works                                                   *
************************************************************************/
class time
{
	function time()
	{
		$this->db = new sqldb();
		global $database_config;
		//$this->dbu_users = new sqldb(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		$this->database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->dbu =  new sqldb($this->database_2);
	}

	/****************************************************************
	* function approve(&$in)		                            *
	****************************************************************/
	function approve(&$in){
		//$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		$user_name->next();
		$name = htmlspecialchars_decode(stripslashes($user_name->f('first_name')." ".$user_name->f('last_name')));
		//$filter = " ";
		$filter = ",submited='1' ";
		$filter2 = '';
		if(!$in['week_end']){
			$in['week_end'] = $in['week_start']+600000;
			//$filter = ",submited='1' ";
		}
		if($_SESSION['group'] != 'admin'){
			$filter2 =" AND project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
		}
		$this->db->query("UPDATE task_time SET approved='1' $filter WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$in['week_start']."' AND '".$in['week_end']."' ".$filter2." ");
		$this->db->query("INSERT INTO timesheet_log SET user_id='".$in['user_id']."', message='".(gm('Timesheet approved by').' '.addslashes($name))."', date='".date(ACCOUNT_DATE_FORMAT)."', start_date='".$in['week_start']."'  ");
		msg::success(gm('Timesheet approved.'),"success");

		#check to see if the entire week has been approved
		$now 				= $in['week_start'];
		$today_of_week 		= date("N", $now);
		$month        		= date('n',$now);
		$year         		= date('Y',$now);
		$week_start 		= mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
		$week_end 			= $week_start + 600000;
		$not_fully_app = 1;
		//$in['es'] = $week_start;
		//$in['ee'] = $week_end;
		$this->db->query("SELECT approved FROM task_time WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$week_start."' AND '".$week_end."' ".$filter2." ");
		while ($this->db->move_next()) {
			if($this->db->f('approved') == 0){
				$not_fully_app = 0;
			}
		}
		if($not_fully_app == 1){
			$this->db->query("UPDATE task_time SET submited='1' WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$week_start."' AND '".$week_end."' ".$filter2." ");
			//$this->send_approved_mail($in);
		}else{
			$separator = ACCOUNT_NUMBER_FORMAT;
			$in['hours_left'] = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$week_start."' AND '".$week_end."' ".$filter2." AND approved='0' ");
			$in['hours_left'] = number_as_hour($in['hours_left'],2,substr($separator, -1),substr($separator, 0));
		}
		if($in['list']){
			return true;
		}else{
			json_out($in);
		}		
	}

	function send_approved_mail(&$in){
//	
        	global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$db = new sqldb($db_config);
        	//$email = $db->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$in['user_id']."'");
			$email = $db->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
        	$email->next();

        	$in['week_end'] = $in['week_start']+604799;

        	$mail = new PHPMailer();
		$mail->WordWrap = 50;

        	$fromMail='noreply@akti.com';
        	$mail->SetFrom($fromMail, utf8_decode(ACCOUNT_COMPANY),0);

        	//$code = $db->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id='".$in['user_id']."' ");
        	$code = $db->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
        	if($code == 'nl'){$code = 'du';}
        	$message_data=get_sys_message('timesheet_approvemess',$code);
        	$subject='<style>body{background-color: #f2f2f2;}</style><div style="margin: 35px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 30px;">'.utf8_decode($message_data['text']).'</div>';
        	$subject=stripslashes($message_data['text']);
				$subject=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject );
				$subject=str_replace('[!FIRST_NAME!]',utf8_decode($email->f('first_name')), $subject);
				$subject=str_replace('[!LAST_NAME!]',utf8_decode($email->f('last_name')), $subject);
				$subject=str_replace('[!START_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_start']), $subject);
				$subject=str_replace('[!END_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_end']), $subject);
				$subject=str_replace('[!DATE!]',date(ACCOUNT_DATE_FORMAT), $subject);
				$subject=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_TIMESHEET."\" alt=\"\">",$subject);
				if($code != 'en'){
					$body = $subject;
					$body .='

					-----------------------------------------------------------------------------------------------------------------------------------------

					';
				}

	        $body .='Dear '.utf8_decode($email->f('first_name')).',

	        Your timesheet from "'.date(ACCOUNT_DATE_FORMAT,$in['week_start']).' to '.date(ACCOUNT_DATE_FORMAT,$in['week_end']).'" has been approved.

	        '.utf8_decode(ACCOUNT_COMPANY).'
	        '.date(ACCOUNT_DATE_FORMAT);

        	$subject_text = $message_data['subject'];
        	$subject_text = str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject_text );

        	$subject= $code =='en' ? '['.utf8_decode(ACCOUNT_COMPANY).'] Timesheet approved.' : $subject_text;
        	$mail->Subject = $subject;
        	$mail->MsgHTML(nl2br($body));
        	$mail->AddAddress($email->f('email'),utf8_decode($email->f('first_name')));

		$mail->Send();

		return true;
	}

	function remove_submit(&$in){
		//$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		$user_name->next();
		$name = htmlspecialchars_decode(stripslashes($user_name->f('first_name')." ".$user_name->f('last_name')));

		$in['week_end'] = $in['week_start']+600000;
		$this->db->query("UPDATE task_time SET submited='2',approved='0' WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$in['week_start']."' AND '".$in['week_end']."'  ");
		$this->db->query("INSERT INTO timesheet_log SET user_id='".$in['user_id']."', message='".(gm('Timesheet rejected by').' '.addslashes($name))."', date='".date(ACCOUNT_DATE_FORMAT)."', start_date='".$in['week_start']."'  ");
		if($in['t_comment']){
			$this->db->query("SELECT * FROM t_comments WHERE user_id='".$in['user_id']."' AND start_date='".$in['week_start']."'");
			if($this->db->move_next()){
				$this->db->query("UPDATE t_comments SET comment='".$in['t_comment']."' WHERE user_id='".$in['user_id']."' AND start_date='".$in['week_start']."' ");
			}else{
				$this->db->query("INSERT INTO t_comments SET user_id='".$in['user_id']."', comment='".$in['t_comment']."', start_date='".$in['week_start']."' ");
			}
		}
		//here we delete the dummy tasks
		$this->db->query("DELETE FROM task_time WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$in['week_start']."' AND '".$in['week_end']."'  AND notes = 'no_task_just_expense' ");
		msg::success(gm('Timesheet rejected.'),"success");

		$this->send_user_reject($in);
		json_out($in);
	}

	/****************************************************************
	* function remove_submit(&$in)		                            *
	****************************************************************/

	function send_user_reject(&$in){
       	global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$db = new sqldb($db_config);
        	//$email = $db->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$in['user_id']."'");
			$email = $db->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
        	$email->next();
        	$mail = new PHPMailer();
		$mail->WordWrap = 50;

        	$fromMail='timesheet@akti.com';
        	$mail->SetFrom($fromMail, $fromMail);

        	//$code = $db->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id='".$in['user_id']."' ");
        	$code = $db->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
        	if($code == 'nl'){$code = 'du';}
        	$message_data=get_sys_message('timesheet_rejectmess',$code);

        	$subject=$message_data['text'];
				$subject=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject );
				$subject=str_replace('[!FIRST_NAME!]',utf8_decode($email->f('first_name')), $subject);
				$subject=str_replace('[!START_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_start']), $subject);
				$subject=str_replace('[!END_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_end']), $subject);
				$subject=str_replace('[!COMMENT!]',$in['t_comment'], $subject);
				$subject=str_replace('[!DATE!]',date(ACCOUNT_DATE_FORMAT), $subject);

				if($code != 'en'){
					$body = $subject;
					$body .='

					-----------------------------------------------------------------------------------------------------------------------------------------

					';
				}

	        $body .='Dear '.utf8_decode($email->f('first_name')).',

	        Your timesheet from "'.date(ACCOUNT_DATE_FORMAT,$in['week_start']).' to '.date(ACCOUNT_DATE_FORMAT,$in['week_end']).'" has been rejected.
	        Please recheck your timesheet.
	        Comment: '.$in['t_comment'].'.

	        '.utf8_decode(ACCOUNT_COMPANY).'
	        '.date(ACCOUNT_DATE_FORMAT);

        	$subject_text = $message_data['subject'];
        	$subject_text=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject_text );

        	$subject= $code == 'en' ? '['.utf8_decode(ACCOUNT_COMPANY).'] Timesheet rejected.' : $subject_text;
        	$mail->Subject = $subject;
        	$mail->MsgHTML(nl2br($body));
        	$mail->AddAddress($email->f('email'));

		$mail->Send();

		return true;
	}

	function notice(&$in){

		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$db = new sqldb($db_config);
        	//$email = $db->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$in['user_id']."'");
			$email = $db->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
        	$email->next();

        	$in['week_end'] = $in['week_start']+604799;

        	$mail = new PHPMailer();
		$mail->WordWrap = 50;

        	$fromMail='timesheet@akti.com';
        	$mail->SetFrom($fromMail, $fromMail);

        	//$code = $db->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id='".$in['user_id']."' ");
        	$code = $db->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
        	if($code == 'nl'){$code = 'du';}
        	$message_data=get_sys_message('timesheet_remindermess',$code);

        	$subject=$message_data['text'];
				$subject=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject );
				$subject=str_replace('[!FIRST_NAME!]',utf8_decode($email->f('first_name')), $subject);
				$subject=str_replace('[!START_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_start']), $subject);
				$subject=str_replace('[!END_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_end']), $subject);
				$subject=str_replace('[!DATE!]',date(ACCOUNT_DATE_FORMAT), $subject);

				if($code != 'en'){
					$body = $subject;
					$body .='

					-----------------------------------------------------------------------------------------------------------------------------------------

					';
				}

	        $body .='Dear '.utf8_decode($email->f('first_name')).',

	        Your timesheet from "'.date(ACCOUNT_DATE_FORMAT,$in['week_start']).' to '.date(ACCOUNT_DATE_FORMAT,$in['week_end']).'" needs to be submitted.
	        Please check your timesheet.

	        '.utf8_decode(ACCOUNT_COMPANY).'
	        '.date(ACCOUNT_DATE_FORMAT);

        	$subject_text = $message_data['subject'];
        	$subject_text=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject_text );

        	$subject= $code =='en' ? '['.utf8_decode(ACCOUNT_COMPANY).'] Timesheet unsubmitted.' : $subject_text;
        	$mail->Subject = $subject;
        	$mail->MsgHTML(nl2br($body));
        	$mail->AddAddress($email->f('email'),utf8_decode($email->f('first_name')));

		$mail->Send();

		msg::success(gm('Mail sent'),"success");
		return true;
	}

	function remove_accept(&$in)
	{
		//$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		$user_name->next();
		$name = htmlspecialchars_decode(stripslashes($user_name->f('first_name')." ".$user_name->f('last_name')));

		$in['week_end'] = $in['week_start']+600000;
		$this->db->query("UPDATE task_time SET unapproved='1' WHERE user_id='".$in['user_id']."' AND approved='1' AND billed='1' AND `date` BETWEEN '".$in['week_start']."' AND '".$in['week_end']."'  ");
		$this->db->query("UPDATE task_time SET approved='0', billed='0' WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$in['week_start']."' AND '".$in['week_end']."'  ");
		$this->db->query("UPDATE project_expenses SET billed='0' WHERE user_id='".$in['user_id']."' AND `date` BETWEEN '".$in['week_start']."' AND '".$in['week_end']."' AND project_id>'0' AND service_id='0' ");
		$this->db->query("INSERT INTO timesheet_log SET user_id='".$in['user_id']."', message='".(gm('Timesheet reverted by').' '.addslashes($name))."', date='".date(ACCOUNT_DATE_FORMAT)."', start_date='".$in['week_start']."'  ");
		msg::success(gm('Timesheet reverted.'),"success");
		return true;
	}

	/****************************************************************
	* function add_hour_simple(&$in)                                          *
	****************************************************************/
	function add_hour_simple(&$in)
	{
		$notice_message=false;
		$closed = $this->db->field("SELECT closed FROM projects WHERE project_id=(SELECT project_id FROM task_time WHERE task_time_id='".$in['TASK_DAY_ID']."') ");
		if(!$closed){
			$this->db->query("UPDATE task_time SET hours='".$in['HOURS']."' WHERE task_time_id='".$in['TASK_DAY_ID']."'");
			if(!$in['HOURS']){
				$this->db->query("DELETE FROM servicing_support_sheet WHERE task_time_id='".$in['TASK_DAY_ID']."' ");
			}
		}else{
			$notice_message=true;
			msg::notice(gm("Some hours where not added because the project is closed"),"notice");
		}
		$this->check_project_budget();
		json_out($in);
	}

	/****************************************************************
	* function add_hour(&$in)                                          *
	****************************************************************/
	function add_hour(&$in)
	{

		$notice_message=false;
		if(isset($in['task_row']) && !empty($in['task_row'])){
			foreach ($in['task_row'] as $key => $value){
				foreach($value['task_day_row'] as $key1=>$value1){
					$closed = $this->db->field("SELECT closed FROM projects WHERE project_id=(SELECT project_id FROM task_time WHERE task_time_id='".$value1['TASK_DAY_ID']."') ");
					if(!$closed){
						$this->db->query("UPDATE task_time SET hours='".$value1['HOURS']."' WHERE task_time_id='".$value1['TASK_DAY_ID']."'");
					}else{
						$notice_message=true;
						msg::notice(gm("Some hours where not added because the project is closed"),"notice");
					}
				}		
			}
			if(!$notice_message){
				msg::success(gm("Time has been successfully added."),"success");
			}
		}
		else{
			msg::error(gm("Please add a Project first"),"error");
		}
		$this->check_project_budget();
		return true;
	}

	/****************************************************************
	* function check_project_budget(&$in)                           *
	****************************************************************/
	function check_project_budget(){

		$project = $this->db->query("SELECT projects.name AS p_name, projects.pr_h_rate AS pr_h_rate, projects.billable_type AS billable_type, projects.project_id AS p_id, projects.budget_type, projects.alert_percent AS procent, projects.company_name AS c_name, projects.customer_id AS c_id FROM projects WHERE projects.active='1' AND budget_type>'0' AND alert_percent!='0' AND alert_sent!='1' ");
		while($project->next()){
			$currency_show = 0;
			switch ($project->f('budget_type')){
			case '2':
				$budget = $this->db->field("SELECT t_pr_hour FROM projects WHERE project_id='".$project->f('p_id')."' ");
				$budget_t = $budget;
				break;
			case '3':
				$budget = $this->db->field("SELECT t_pr_fees FROM projects WHERE project_id='".$project->f('p_id')."' ");
				$budget_t = $budget;
				if( $project->f('billable_type') == '1'){
					$rate_value = $this->db->query("SELECT t_h_rate FROM tasks WHERE project_id='".$project->f('p_id')."' AND billable='1' AND task_id IN (SELECT DISTINCT task_id FROM task_time WHERE customer_id='".$project->f('c_id')."' AND project_id='".$project->f('p_id')."' AND hours !=0 )");
					while ($rate_value->next()) {
						$rate += $rate_value->f('t_h_rate');
					}
				}
				if($project->f('billable_type') == '2'){
						$rate_value = $this->db->field("SELECT p_h_rate FROM project_user WHERE project_id='".$project->f('p_id')."' ");
						$rate = $rate_value;
				}
				if( $project->f('billable_type') == '3'){
						$rate = $project->f('pr_h_rate');
				}
				if( $project->f('billable_type') == '4') {
						$rate = 0;
				}
				$currency_show = 1;
				break;

			case '4':
				$budget = $this->db->query("SELECT t_hours FROM tasks WHERE project_id='".$project->f('p_id')."' ");
				while ($budget->next()) {
					$budget_t += $budget->f('t_hours');
				}
				break;
			case '5':
				$budget = $this->db->query("SELECT p_hours FROM project_user WHERE project_id='".$project->f('p_id')."' ");
				while ($budget->next()) {
					$budget_t += $budget->f('p_hours');
				}
				break;
			}
			$spent = $this->db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$project->f('p_id')."' ");
			$procent = $project->f('procent');

			if( $spent > ($budget_t*($procent/100)) ){

				$over = $spent-($budget_t*($procent/100));
				$procent_value =$budget_t*($procent/100);
				$this->send_budget_mail($project->f('p_name'),$project->f('p_id'),$project->f('c_name'), $budget_t, $spent, $procent,$over, $procent_value,$currency_show);
			}

		}
		return true;
	}

	function send_budget_mail(&$project,&$p_id,&$customer,&$budget,&$spent,&$procent,&$over, &$procent_value,$currency = 0,$contract_id = 0){

        	require_once ('class.phpmailer.php');

        $mail = new PHPMailer();
        $mail->WordWrap = 50;

        $fromMail='notification@akti.com';
        $mail->SetFrom($fromMail, $fromMail);

        $body='The project '.$project.' has reached the '.$procent.'% threshold: '.display_number($spent).' '.($currency==1 ? get_currency(ACCOUNT_CURRENCY_TYPE) : 'hours').' out of '.display_number($budget).' '.($currency==1 ? get_currency(ACCOUNT_CURRENCY_TYPE) : 'hours').'.';

        $subject= 'Project Budget Alert:'.$customer.' / '.$project;
        $mail->Subject = $subject;
        $mail->MsgHTML(nl2br($body));

        $this->db->query("SELECT user_id FROM project_user WHERE project_id='".$p_id."' AND manager='1' ");
        $k = 0;
        while ($this->db->move_next()) {
        	$email = $this->dbu->field("SELECT email FROM users WHERE user_id='".$this->db->f('user_id')."' ");
        	$mail->AddAddress($email);
        	$k++;
        }
        if($k > 0){
        	$mail->Send();

			$sent_date= time();
			$this->db->query("UPDATE projects SET alert_sent='1',date_sent='".$sent_date."' WHERE project_id='".$p_id."'");
		}
		return true;
	}

	/****************************************************************
	* function delete_time(&$in)                                    *
	****************************************************************/
	function delete_time(&$in){

		if(!$this->delete_time_validate($in)){
			json_out($in);
			return false;
		}
		foreach ($in['task_day_row'] as $key => $value){
			$this->db->query("SELECT date FROM task_time WHERE task_time_id='".$value['TASK_DAY_ID']."'");
			$this->db->move_next();
			$this->db->query("DELETE FROM timesheet_log WHERE start_date='".$this->db->f('date')."' ");
			$this->db->query("DELETE FROM task_time WHERE task_time_id='".$value['TASK_DAY_ID']."' ");
			$this->db->query("DELETE FROM servicing_support_sheet WHERE task_time_id='".$value['TASK_DAY_ID']."' ");
		}
		msg::success(gm("Project entries deleted"),"success");
		return true;
	}

	/****************************************************************
	* function delete_time_validate(&$in)                           *
	****************************************************************/
	function delete_time_validate(&$in){
		$can_delete = true;
		foreach ($in['task_day_row'] as $key => $value) {
			$item = $this->db->query("SELECT * FROM task_time WHERE task_time_id='".$value['TASK_DAY_ID']."' ");
			if($item->f('approved') || $item->f('submited') == 1 || $item->f('billed') ){
				$can_delete = false;
				break;
			}
		}
		if(!$can_delete){
			msg::error(gm("Entries locked"),"error");
		}		
		return $can_delete;
	}

	function delete_image(&$in){

		$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."' ");
		if($this->db->move_next()){
			@unlink(UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
			$this->db->query("UPDATE project_expenses set picture='' WHERE id='".$in['id']."' ");
		}
		msg::success(gm('Expense attachement deleted'),"success");
		return true;
	}

	/****************************************************************
	* function update_single_time(&$in)                             *
	****************************************************************/
	function update_single_time(&$in){
		if(!$this->update_validate($in)){
			json_out($in);
		}
		$this->db->query("UPDATE task_time SET notes='".trim($in['comment'])."' WHERE task_time_id='".$in['task_time_id']."' ");
		msg::success(gm('Comment has been added.'),"success");
		json_out($in);
	}

	/****************************************************************
	* function update_validate(&$in)                                *
	****************************************************************/
	function update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('task_time_id', gm('ID'), 'required:exist[task_time.task_time_id]', gm("Invalid ID"));

		return $v->run();
	}

	function addSubHoursPlus(&$in){
		if(!$in['project_id'] && $in['c_id']){
			$in['task_name']=stripslashes($this->db->field("SELECT default_name FROM default_data WHERE default_main_id='0' AND type='task_no' AND active='1' AND default_id='".$in['task_id']."' "));
			$this->db->query("SELECT project_id FROM projects WHERE customer_id='".$in['c_id']."' AND active='2' AND contract_id='0' ");
			if($this->db->move_next()){
				$in['project_id'] = $this->db->f('project_id');
				$this->db->query("SELECT task_id FROM tasks WHERE project_id='".$in['project_id']."' AND task_name='".addslashes($in['task_name'])."' ");
				if($this->db->move_next()){
					$in['task_id'] = $this->db->f('task_id');
				}else{
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
					$this->db->move_next();
					$value = $this->db->f('value');
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
					$this->db->move_next();
					$billable = 0;
					if($this->db->f('value') == 'yes'){
						$billable = 1;
					}
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
					if($this->db->move_next()){
						$value = $this->db->f('value');
					}
					$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."' ");
				}
			}else{
				$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
				$this->db->move_next();
				$value = $this->db->f('value');
				$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
				$this->db->move_next();
				$billable = 0;
				if($this->db->f('value') == 'yes'){
					$billable = 1;
				}
				$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
				if($this->db->move_next()){
					$value = $this->db->f('value');
				}
				$in['project_id'] = $this->db->insert("INSERT INTO projects SET customer_id='".$in['c_id']."', name='ad hoc', active='2', billable_type='1', invoice_method='1', company_name=(SELECT name FROM customers WHERE customer_id='".$in['c_id']."'), stage='1', step='0' ");
				$u_name = get_user_name($in['user_id']);
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."', user_id='".$in['user_id']."', user_name='".addslashes($u_name)."' ");
				$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."' ");
			}
		}
		$task_time_subdata=$this->db->query("SELECT * FROM task_time WHERE date='".$in['date']."' AND project_id='".$in['project_id']."' AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."' ");
		while($task_time_subdata->next()){
			$this->db->query("INSERT INTO servicing_support_sheet SET 
					start_time		= '0',
					end_time		= '0',
					break			= '0',
					project_id		= '".$task_time_subdata->f('project_id')."',
					task_id		= '".$task_time_subdata->f('task_id')."',
					task_time_id	= '".$task_time_subdata->f('task_time_id')."',
					date			= '".$in['date']."',
					user_id		= '".$in['user_id']."' ");
		}
		msg::success(gm("Line added"),"success");
		return true;
	}

	function updateDayTotalHours(&$in){
		$this->db->query("UPDATE servicing_support_sheet SET total_hours='".$in['value']."',start_time='0',end_time='0',break='0' WHERE id='".$in['id']."' ");
		$hours_count=0;
		$hours_data=$this->db->query("SELECT * FROM servicing_support_sheet WHERE task_time_id='".$in['task_time_id']."' ");
		while($hours_data->next()){
			if($hours_data->f('total_hours')==0){
				$hours_count+=($hours_data->f('end_time')-$hours_data->f('start_time')-$hours_data->f('break'));
			}else{
				$hours_count+=$hours_data->f('total_hours');
			}
		}
		$this->db->query("UPDATE task_time SET hours='".$hours_count."' WHERE task_time_id='".$in['task_time_id']."' ");
		msg::success(gm('Data saved'),"success");
		json_out($in);
	}

	function updateDayHours(&$in){
		$this->db->query("UPDATE servicing_support_sheet SET total_hours='0',start_time='".$in['start_time']."',end_time='".$in['end_time']."',break='".$in['break']."' WHERE id='".$in['id']."' ");
		$hours_count=0;
		$hours_data=$this->db->query("SELECT * FROM servicing_support_sheet WHERE task_time_id='".$in['task_time_id']."' ");
		while($hours_data->next()){
			if($hours_data->f('total_hours')==0){
				$hours_count+=($hours_data->f('end_time')-$hours_data->f('start_time')-$hours_data->f('break'));
			}else{
				$hours_count+=$hours_data->f('total_hours');
			}
		}
		$this->db->query("UPDATE task_time SET hours='".$hours_count."' WHERE task_time_id='".$in['task_time_id']."' ");
		msg::success(gm('Data saved'),"success");
		json_out($in);
	}

	function updateDayComment(&$in){
		$this->db->query("UPDATE servicing_support_sheet SET notes='".trim($in['notes'])."' WHERE id='".$in['id']."'");
		msg::success(gm("Data saved"),"success");
		json_out($in);
	}

	function deleteSubHours(&$in){
		$taskt_id=$this->db->field("SELECT task_time_id FROM servicing_support_sheet WHERE id='".$in['id']."'");
		$this->db->query("DELETE FROM servicing_support_sheet WHERE id='".$in['id']."'");
		$hours_count=0;
		$hours_data=$this->db->query("SELECT * FROM servicing_support_sheet WHERE task_time_id='".$taskt_id."' ");
		while($hours_data->next()){
			if($hours_data->f('total_hours')==0){
				$hours_count+=($hours_data->f('end_time')-$hours_data->f('start_time')-$hours_data->f('break'));
			}else{
				$hours_count+=$hours_data->f('total_hours');
			}
		}
		$this->db->query("UPDATE task_time SET hours='".$hours_count."' WHERE task_time_id='".$taskt_id."' ");
		msg::success(gm("Data saved"),"success");
		json_out($in);
	}

	/****************************************************************
	* function submit_hour(&$in)		                            *
	****************************************************************/
	function submit_hour(&$in){

		if(!$this->add_hour($in)){
			json_out($in);
			return false;
		}
		//$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		$user_name->next();
		$name = htmlspecialchars_decode(stripslashes($user_name->f('first_name')." ".$user_name->f('last_name')));
		$time_auto_app=$this->db->field("SELECT value FROM settings WHERE constant_name='TIME_AUTO_APPROVE' ");
		$auto_app="";
		if($time_auto_app=='1'){
			$auto_app=",approved='1' ";
		}
		$this->db->query("UPDATE task_time SET submited='1'".$auto_app." WHERE `date` BETWEEN '".$in['week_s']."' AND '".$in['week_e']."' AND user_id='".$in['user_id']."'  ");
		$this->db->query("INSERT INTO timesheet_log SET user_id='".$in['user_id']."', message='".(gm('Timesheet submitted by').' '.addslashes($name))."', date='".date(ACCOUNT_DATE_FORMAT)."', start_date='".$in['week_s']."'  ");
		msg::success(gm('Timesheet submitted.'),"success");

		//$this->send_manage_mail($in);
		return true;
	}

	function send_manage_mail(&$in){
//		require_once ('class.phpmailer.php');
//        include_once ("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
        $dbu = new sqldb();
	    foreach($in['task_row'] as $key=>$value){
	        $dbu->query("SELECT user_id FROM project_user WHERE manager='1' AND project_id='".$value['project_id']."' ");
	        while ($dbu->move_next()) {
	        	$user_list .= $dbu->f('user_id').',';
	        }
	    }
        $user_list = rtrim($user_list,',');
        if($user_list){
	       global $database_config;
			$db_config = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
			);

			$db = new sqldb($db_config);
	        $email = $db->query("SELECT email FROM users WHERE user_id IN ($user_list)");
	        $emails = array();
	        while ($email->next()) {
	        	if(!in_array($email->f('email'),$emails)){
	        		array_push($emails,$email->f('email'));
	        	}
	        }

	        $mail = new PHPMailer();
		$mail->WordWrap = 50;
	        /*$mail->IsSMTP(); // telling the class to use SMTP
	        $mail->SMTPDebug = 1; // enables SMTP debug information (for testing)
	        // 1 = errors and messages
	        // 2 = messages only
	        $mail->SMTPAuth = true; // enable SMTP authentication
	        $mail->Host = SMTP_HOST; // sets the SMTP server
	        $mail->Port = SMTP_PORT; // set the SMTP port for the GMAIL server
	        $mail->Username = SMTP_USERNAME; // SMTP account username
	        $mail->Password = SMTP_PASSWORD; // SMTP account password*/

	        $fromMail='timesheet@akti.com';
	        $mail->SetFrom($fromMail, $fromMail);

	        //$code = $this->dbu->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id='".$in['user_id']."' ");
	        $code = $this->dbu->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
	        if($code == 'nl'){$code = 'du';}
	        //$user = $dbu->field("SELECT user_name FROM project_user WHERE user_id='".$in['user_id']."' ");
	        $user=get_user_name($in['user_id']);
	        $message_data=get_sys_message('timesheetmess',$code);

	        $subject=$message_data['text'];
			$subject=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject );
			$subject=str_replace('[!USER_NAME!]',utf8_decode($user), $subject);
			$subject=str_replace('[!START_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_s']), $subject);
			$subject=str_replace('[!END_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_e']), $subject);
			$subject=str_replace('[!DATE!]',date(ACCOUNT_DATE_FORMAT), $subject);

			if($code != 'en'){
				$body = $subject;
				$body .='

				-----------------------------------------------------------------------------------------------------------------------------------------

				';
			}

	        $body .= $user.'\'s timesheet from "'.date(ACCOUNT_DATE_FORMAT,$in['week_s']).' to '.date(ACCOUNT_DATE_FORMAT,$in['week_e']).'" has been submitted.

	        '.utf8_decode(ACCOUNT_COMPANY).'
	        '.date(ACCOUNT_DATE_FORMAT);

	        $subject_text = $message_data['subject'];
        	$subject_text=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject_text );

	        $subject= $code == 'en' ? '['.utf8_decode(ACCOUNT_COMPANY).'] Timesheet submitted.' : $subject_text;
	        $mail->Subject = $subject;
	        $mail->MsgHTML(nl2br($body));

			foreach ($emails as $key => $value ){
	        	$mail->AddAddress($value);
			}
			$mail->Send();
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function add_tasks(&$in)
	{
		$i=0;
		$y=0;
		$is_adhoc = false;
		if(!$in['project_id']){
			$is_adhoc = true;
		}
		if($in['tasks_id'] && is_array($in['tasks_id']) ) {
			foreach ($in['tasks_id'] as $in['task_id']) {
				if($is_adhoc){
					$in['project_id'] = '';
					$in['task_name']=stripslashes($this->db->field("SELECT default_name FROM default_data WHERE default_main_id='0' AND type='task_no' AND active='1' AND default_id='".$in['task_id']."' "));
				}
				if($this->add_task($in)){
					$i++;
				}else{
					$y++;
				}
			}
		}
		$in['status_rate']=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$in['project_id']."' ");
		$task_single = gm('Task');
		$task_plural = gm('tasks');
		msg::success(gm('Added').' '.$i.' '.($i > 1 ? $task_plural : $task_single).'. '.$y.' '.gm('Duplicate').' '.($y > 1 ? $task_plural : $task_single),"success");
		return true;
	}

	/****************************************************************
	* function add task(&$in)                                          *
	****************************************************************/
	function add_task(&$in)
	{
		if(!$this->add_validate($in)){
			return false;
		}
		$this->db->query("UPDATE projects SET step='0' WHERE project_id='".$in['project_id']."' ");
		$in['status_rate']=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$in['project_id']."' ");
		$in['customer_id']=$this->db->field("SELECT customer_id FROM projects WHERE project_id='".$in['project_id']."' ");
		$in['billable'] = $this->db->field("SELECT billable FROM tasks WHERE task_id='".$in['task_id']."' ");
		foreach ($in['day_row'] as $key => $value ){
			$this->db->query("INSERT INTO task_time SET 				  user_id         = '".$in['user_id']."',
														                  task_id	     		= '".$in['task_id']."',
														                  project_id	 		= '".$in['project_id']."',
														                  customer_id	 		= '".$in['customer_id']."',
														                  contract_id	 		= '".$in['contract_id']."',
														                  date            = '".$value['TIMESTAMP']."',
														                  hours	          = '0',
														                  billable			  = '".$in['billable']."',
														                  project_status_rate      ='".$in['status_rate']."' ");
		}

		msg::success(gm('Project entries added.'),"success");
		return true;
	}

	/****************************************************************
	* function add_validate(&$in)                                   *
	****************************************************************/
	function add_validate(&$in)
	{
		$is_ok = true;
		if($in['project_id'] && $in['task_id'] && $in['user_id']){
			foreach($in['task_row'] as $key=>$value){
				foreach($value['task_day_row'] as $key1=>$value1){
					$this->db->query("SELECT task_time.* FROM task_time WHERE project_id='".$in['project_id']."'
		                     AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."' AND task_time_id='".$value1['TASK_DAY_ID']."'
		                     ");
					while($this->db->next()){
						if($this->db->f('billed') == '1'){
							$in['duplicate'] = 1;
							msg::error(gm('Task was invoiced.'),"error");
							$is_ok = false;
						}else{
							$in['duplicate'] = 1;
							msg::error(gm('Task was already added.'),"error");
							$is_ok = false;
						}
					}
				}
			}
		}else if(!$in['project_id'] && $in['c_id'] && $in['task_id']){
			$this->db->query("SELECT project_id FROM projects WHERE customer_id='".$in['c_id']."' AND active='2' AND contract_id='0' ");
			if($this->db->move_next()){
				$in['project_id'] = $this->db->f('project_id');
				$this->db->query("SELECT task_id FROM tasks WHERE project_id='".$in['project_id']."' AND task_name='".addslashes($in['task_name'])."' ");
				if($this->db->move_next()){
					$in['task_id'] = $this->db->f('task_id');
					foreach($in['task_row'] as $key=>$value){
						foreach($value['task_day_row'] as $key1=>$value1){
							$this->db->query("SELECT task_time.* FROM task_time WHERE project_id='".$in['project_id']."'
				                     AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."' AND task_time_id='".$value1['TASK_DAY_ID']."'
				                     ");
							while($this->db->next()){
								if($this->db->f('billed') == '1'){
									$in['duplicate'] = 1;
									msg::error(gm('Task was invoiced.'),"error");
									$is_ok = false;
								}else{
									$in['duplicate'] = 1;
									msg::error(gm('Task was already added.'),"error");
									$is_ok = false;
								}
							}
						}
					}
				}else{
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
					$this->db->move_next();
					$value = $this->db->f('value');
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
					$this->db->move_next();
					$billable = 0;
					if($this->db->f('value') == 'yes'){
						$billable = 1;
					}
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
					if($this->db->move_next()){
						$value = $this->db->f('value');
					}
					$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."' ");
					$is_ok = true;
				}
			}else{
				$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
				$this->db->move_next();
				$value = $this->db->f('value');
				$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
				$this->db->move_next();
				$billable = 0;
				if($this->db->f('value') == 'yes'){
					$billable = 1;
				}
				$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
				if($this->db->move_next()){
					$value = $this->db->f('value');
				}
				$in['project_id'] = $this->db->insert("INSERT INTO projects SET customer_id='".$in['c_id']."', name='ad hoc', active='2', billable_type='1', invoice_method='1', company_name=(SELECT name FROM customers WHERE customer_id='".$in['c_id']."'), stage='1', step='0' ");
				$u_name = get_user_name($in['user_id']);
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."', user_id='".$in['user_id']."', user_name='".addslashes($u_name)."' ");
				$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."' ");
				$is_ok = true;
			}
		}else{
			msg::error(gm("Please select a project"),"error");
			$is_ok = false;
		}
		return $is_ok;
	}

	function delete_empty_row(&$in){
		$week_s=$in['week_start'];
		$week_e=$in['week_start']+600000;
		$data=$this->db->query("SELECT task_time_id,date FROM task_time WHERE date BETWEEN '".$week_s."' AND '".$week_e."' AND user_id='".$in['user_id']."' ");
		while($data->next()){
			$this->db->query("DELETE FROM servicing_support_sheet WHERE task_time_id='".$data->f('task_time_id')."' ");
			$this->db->query("DELETE FROM timesheet_log WHERE start_date='".$data->f('date')."' ");
			$this->db->query("DELETE FROM task_time WHERE task_time_id='".$data->f('task_time_id')."' ");
		}
		msg::success(gm("Project entries deleted"),"success");
		return true;
	}

}//end class
