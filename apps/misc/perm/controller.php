<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
perm::controller('all', 'all');

global $apps;
global $p_access;

if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::controller('all', 'admin',false);
	perm::controller('all', 'user',false);
	perm::controller(array('board','top_header','footer','isLoged','home','modal','main_menu','header','console','template','lang','notice','generalSettings'), 'admin',true);
	perm::controller(array('board','top_header','footer','isLoged','home','modal','main_menu','header','console','template','lang','notice','generalSettings'), 'user',true);
}

// perm::controller('all', 'all',in_array($apps['misc'], $p_access));
//perm::controller('all', 'all');