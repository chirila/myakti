<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
//global $config;
// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
$db2 = new sqldb();
$db3 = new sqldb();
$log = $db->query("SELECT * FROM logging WHERE log_id='".$in['log_id']."' ");
if($log->next()){
	switch ($log->f('pag')) {
		case 'order':
			$link=true;
			$item_url='order.view';
			$item_param='order_id';
			$item_field=$log->f('field_value');
			$type= gm('Order');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT customer_name, contact_name, customer_id, contact_id FROM pim_orders WHERE order_id='".$log->f('field_value')."'");
			if($db2->f('customer_id')){
				$company_link = $db2->f('customer_id');
				$company_name = $db2->f('customer_name');	
			}
			if($db2->f('contact_id')){
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('contact_name')));
			}
			break;
		case 'p_order':
			$link=true;
			$item_url='purchase_order.view';
			$item_param='p_order_id';
			$item_field=$log->f('field_value');
			$type= gm('Purchase Order');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT customer_name, contact_name,customer_id, contact_id FROM pim_p_orders WHERE p_order_id='".$log->f('field_value')."'");
			if($db2->f('customer_id')){
				$company_link = $db2->f('customer_id');
				$company_name = $db2->f('customer_name');	
			}
			if($db2->f('contact_id')){
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('contact_name')));
			}
			break;
		case 'invoice':
			$link=true;
			$item_url='invoice.view';
			$item_param='invoice_id';
			$item_field=$log->f('field_value');
			$type = gm('Invoice');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT buyer_name, buyer_id, contact_id FROM tblinvoice WHERE id='".$log->f('field_value')."'");
			if($db2->f('buyer_id'))
			{
				$company_name = $db2->f('buyer_name');
				$company_link = $db2->f('buyer_id');
				if($db2->f('contact_id')){
					$db3->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$db2->f('contact_id')."'");
					array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db3->f('firstname').' '.$db3->f('lastname')));
				}
			}else
			{
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('buyer_name')));
			}
			break;
		case 'purchase_invoice':
			$link=true;
			$item_url='purchase_invoice.view';
			$item_param='invoice_id';
			$item_field=$log->f('field_value');
			$type = gm('Purchase Invoice');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT supplier_id FROM tblinvoice_incomming WHERE invoice_id='".$log->f('field_value')."'");
			if($db2->f('supplier_id'))
			{
				$company_name = get_customer_name($db2->f('supplier_id'));
				$company_link = $db2->f('supplier_id');
				if($db2->f('contact_id')){
					$db3->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$db2->f('contact_id')."'");
					array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db3->f('firstname').' '.$db3->f('lastname')));
				}
			}else
			{
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('buyer_name')));
			}
			break;
		case 'quote':
			$link=true;
			$item_url='quote.view';
			$item_param='quote_id';
			$item_field=$log->f('field_value');
			$type = gm('Quote');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT buyer_name, buyer_id, contact_id FROM tblquote WHERE id='".$log->f('field_value')."'");
			if($db2->f('buyer_id'))
			{
				$company_name = $db2->f('buyer_name');
				$company_link = $db2->f('buyer_id');
				if($db2->f('contact_id')){
					$db3->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$db2->f('contact_id')."'");
					array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db3->f('firstname').' '.$db3->f('lastname')));
				}
			}else
			{
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('buyer_name')));
			}
			break;
		case 'contract':
			$link=true;
			$item_url='contract.view';
			$item_param='contract_id';
			$item_field=$log->f('field_value');
			$type = gm('Contract');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT customer_name, customer_id, contact_id FROM contracts WHERE contract_id='".$log->f('field_value')."'");
			if($db2->f('customer_id'))
			{
				$company_name = $db2->f('customer_name');
				$company_link = $db2->f('customer_id');
				if($db2->f('contact_id')){
					$db3->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$db2->f('contact_id')."'");
					array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db3->f('firstname').' '.$db3->f('lastname')));
				}
			}else
			{
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('customer_name')));
			}
			break;
		case 'project':
			$link=true;
			$item_url='project.edit';
			$item_param='project_id';
			$item_field=$log->f('field_value');
			$type = gm('Project');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT company_name,is_contact, customer_id FROM projects WHERE project_id='".$log->f('field_value')."'");
			if($db2->f('is_contact')=='0')
			{
				$company_name = $db2->f('company_name');
				$company_link = $db2->f('customer_id');
			}else
			{
				array_push($contact_name, array('id'=>$db2->f('customer_id'),'name'=>$db2->f('company_name')));
			}
			break;
		case 'customer':
			$link=true;
			$item_url='customerView';
			$item_param='customer_id';
			$item_field=$log->f('field_value');
			$type = gm('Customer');
			$company_name = '';
			$contact_name =array();
			$company_link =	0;
			if($db->f('reminder_date') != 0){
				$link = '<a href="#" ng-click="vm.showItemInfo(\'customerView\',\'customer_id\','.$log->f('field_value').')">'.gm('customer').'</a>';
			}else{

				$activity_type=$db2->field("SELECT customer_contact_activity.contact_activity_type FROM logging_tracked INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity.customer_contact_activity_id WHERE logging_tracked.log_id='".$log->f('log_id')."' GROUP BY logging_tracked.activity_id ");
				switch($activity_type){
					case '1':
						$link="Email";
						break;
					case '2':
						$link="Note";
						break;
					case '4':
						$link="Meeting";
						break;
					case '5':
						$link="Call";
						break;
					default:
						$link="Task";
				}
			}
			$db2->query("SELECT name,customer_id FROM customers WHERE customer_id='".$log->f('field_value')."'");
			$company_name = $db2->f('name');
			$company_link = $db2->f('customer_id');
			$db2->query("SELECT firstname, lastname, customer_id,contact_id 
						FROM customer_contacts
						WHERE contact_id in (
							SELECT contact_id
							FROM customer_contact_activity_contacts
							WHERE activity_id=(
								SELECT activity_id
								FROM logging_tracked
								WHERE log_id='".$in['log_id']."' limit 1
							)
						)");
			while($db2->next()){
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('firstname').' '.$db2->f('lastname')));
			}
			break;
		case 'xcustomer_contact':
			$link=true;
			$item_url='contactView';
			$item_param='contact_id';
			$item_field=$log->f('field_value');
			$type = gm('Contact');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT firstname, lastname, customer_id, contact_id FROM customer_contacts WHERE contact_id='".$log->f('field_value')."'");
			if($db2->f('contact_id')){
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('firstname').' '.$db2->f('lastname')));
			}
			if($db2->f('customer_id')){
				$company_name = $db3->field("SELECT name FROM customers WHERE customer_id='".$db2->f('customer_id')."'");
				$company_link =$db2->f('customer_id');	
			}
			break;
		case 'opportunity':
			$link = '<a href="index.php?do=quote-opportunity&opportunity_id='.$log->f('field_value').'">opportunity</a>';
			$type = gm('Quote');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT buyer_name, buyer_id, contact_id FROM tblopportunity WHERE opportunity_id='".$log->f('field_value')."'");
			if($db2->f('buyer_id'))
			{
				$company_name = $db2->f('buyer_name');
				$company_link =$db2->f('buyer_id');
				if($db2->f('contact_id')){
					$db3->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$db2->f('contact_id')."'");
					array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db3->f('firstname').' '.$db3->f('lastname')));
				}
			}else
			{
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('buyer_name')));
			}
			break;
		case 'service':
			$link=true;
			$item_url='service.view';
			$item_param='service_id';
			$item_field=$log->f('field_value');
			$type = gm('Maintenance');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			$db2->query("SELECT customer_name, contact_name, contact_id, customer_id FROM servicing_support WHERE service_id='".$db->f('field_value')."'");
			if($db2->f('customer_id')){
				$company_link = $db2->f('customer_id');
				$company_name = $db2->f('customer_name');	
			}
			if($db2->f('contact_id')){
				array_push($contact_name, array('id'=>$db2->f('contact_id'),'name'=>$db2->f('contact_name')));
			}
			break;
		default:
			$link=false;
			$type = $log->f('pag');
			$company_name = '';
			$contact_name =array();
			$company_link =0;
			break;
	}

	$result=array(
		'from'	 			=> get_user_name($log->f('user_id')),
		'subject'	 		=> $log->f('type') != 2 ? gm('New task') : $log->f('log_comment'),
		'message'	 		=> nl2br($log->f('message')),
		'date'				=> date(ACCOUNT_DATE_FORMAT.' H:i',$log->f('due_date')),
		'to'				=> get_user_name($log->f('to_user_id')),
		'item'				=> $link,
		'item_url'			=> $item_url,
		'item_param'		=> $item_param,
		'item_field'		=> $item_field,
		'data'				=> true,
		'contact_name'		=> $contact_name,
		'company_name'		=> $company_name,
		'customer_id'		=> $company_link,
		'type'				=> $type,
		'status_on'			=> $log->f('finished') == '0' ? true : false,
		'status_dd'			=> array(array('id'=>'0','name'=>gm('Select')),array('id'=>'1','name'=>gm('Scheduled')),array('id'=>'2','name'=>gm('In progress')),array('id'=>'3','name'=>gm('On hold')),array('id'=>'4','name'=>gm('Completed'))),
		'status'			=> !$log->f('finished') ? $log->f('status_other') : '4',
		'log_id'			=> $in['log_id'],
		'task'				=> $log->f('not_task')==0 ? true : false,
		'not_task'			=> $log->f('not_task')==0 ? false : true,

	);

	if($log->f('done') == '0'){
		$db->query("UPDATE logging SET done='1' WHERE log_id='".$in['log_id']."' ");
	}
}

return json_out($result);