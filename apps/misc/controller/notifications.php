<?php

	if(!$_SESSION['u_id']){
		json_out(array());
	}
	global $database_config;

	$db_config = array(
	    'hostname' => $database_config['mysql']['hostname'],
	    'username' => $database_config['mysql']['username'],
	    'password' => $database_config['mysql']['password'],
	    'database' => $database_config['user_db'],
	); 
	$db_users= new sqldb($db_config);

	$result=array('list'=>array(),'max_rows'=>0);

	require_once(__DIR__."/../model/NotificationHandler.php");
    $notificationHandler = new NotificationHandler();

	$result['max_rows'] = $db_users->field("SELECT COUNT(id) FROM `notifications` WHERE user_id='".$_SESSION['u_id']."' AND db_name='".DATABASE_NAME."' AND msg_read='0' ");

	$notifications = $db_users->query("SELECT * FROM `notifications` WHERE user_id='".$_SESSION['u_id']."' AND db_name='".DATABASE_NAME."' ORDER BY `created_at` DESC ")->getAll();

	foreach($notifications as $notification){

		$additional_data=$notificationHandler->execute(array('module_name'=>$notification['module_name'],'module_id'=>$notification['module_id'],'action'=>$notification['action_type']));
		
		$tmp_item=array(
			'id'			=> $notification['id'],
			'header_text'	=> $additional_data['header_text'],
			'time'			=> date(ACCOUNT_DATE_FORMAT.' H:i',$notification['created_at']),
			'selected'		=> $notification['msg_read'] ? '' : $notification['id'],
			'read'			=> $notification['msg_read'] ? true : false,
			'open_modal'	=> $additional_data['open_modal'],
			'modal_params'	=> $additional_data['modal_params'],
			'link'			=> $additional_data['link'],
		);
		array_push($result['list'], $tmp_item);	
	}

	json_out($result);

?>