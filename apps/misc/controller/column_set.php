<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class ColSet extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Data(){
		$in=$this->in;
		$result=array('available'=>array(),'selected'=>array(),'list_name'=>$in['list']);
		$cols_default=default_columns_dd($in);
		$cols=$this->db->query("SELECT * FROM column_settings WHERE list_name='".$in['list']."' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
		if(!count($cols)){
			$i=1;
            $default_selected_columns_dd = default_selected_columns_dd($in);
			foreach($cols_default as $key=>$value){
				if($in['list']=='articles' && (ALLOW_STOCK=='0' || ADV_PRODUCT == '0') && ($key=='stock' || $key=='ant_stock') ){
					continue;
				}
				if($in['list']=='articles' && ALLOW_ARTICLE_PACKING!='1' && ($key=='stock_pack' || $key=='ant_stock_pack') ){
                    continue;
                }
				$tmp_item=array(
					'id'				=> $i,
					'column_name'		=> $key,
					'name'				=> $value
				);
                if ($default_selected_columns_dd[$key]) {
                    array_push($result['selected'],$tmp_item);
                } else {
                    array_push($result['available'],$tmp_item);
                }
				$i++;
			}
		}else{
			$i=1;
			foreach($cols_default as $key=>$value){				
				$col_found=false;
				foreach($cols as $key1=>$value1){
					if($key == $value1['column_name']){
						$col_found=true;				
						break;
					}	
				}
				if(!$col_found){
					if($in['list']=='articles' && (ALLOW_STOCK=='0' || ADV_PRODUCT == '0') && ($key=='stock' || $key=='ant_stock') ){
						continue;
					}
					if($in['list']=='articles' && ALLOW_ARTICLE_PACKING!='1' && ($key=='stock_pack' || $key=='ant_stock_pack') ){
	                    continue;
	                }
					$tmp_item=array(
						'id'				=> $i,
						'column_name'		=> $key,
						'name'				=> $value
					);
					array_push($result['available'],$tmp_item);
					$i++;
				}
			}
			foreach($cols as $key=>$value){
				if($in['list']=='articles' && (ALLOW_STOCK=='0' || ADV_PRODUCT == '0') && ($value['column_name']=='stock' || $value['column_name']=='ant_stock') ){
					continue;
				}
				if($in['list']=='articles' && ALLOW_ARTICLE_PACKING!='1' && ($value['column_name']=='stock_pack' || $value['column_name']=='ant_stock_pack') ){
                    continue;
                }
				$tmp_item=array(
					'id'				=> $value['id'],
					'column_name'		=> $value['column_name'],
					'name'				=> $cols_default[$value['column_name']]
				);
				array_push($result['selected'],$tmp_item);
			}		
		}
		$this->out = $result;
	}

}
	$colset = new ColSet($in,$db);

	$colset->get_Data();
	$colset->output();

?>