<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	$nylas_data=$db->query("SELECT * FROM nylas_events WHERE id='".$in['log_id']."' ");

	$nylas_p='';
	$nylas_participants=$db->query("SELECT * FROM nylas_participants WHERE event_id='".$nylas_data->f('event_id')."' ");
	while($nylas_participants->next()){
		$nylas_p.=$nylas_participants->f('name').',';
	}
	$nylas_p=rtrim($nylas_p,",");
	$result=array(
			'contacts'		=> $nylas_p,
			'start_time'	=> date(ACCOUNT_DATE_FORMAT.' H:i',$nylas_data->f('start_time')),
			'end_time'		=> date(ACCOUNT_DATE_FORMAT.' H:i',$nylas_data->f('end_time')),
			'subject'		=> $nylas_data->f('title'),
			'message'		=> $nylas_data->f('description'),
			'location'		=> $nylas_data->f('location'),
		);

	return json_out($result);
?>