<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

ark::loadLibraries(array('google-api-php-client/src/Google_Client','google-api-php-client/src/contrib/Google_CalendarService'));
global $config;
$db2 = new sqldb();

global $database_config;

$database_users = array(
'hostname' => $database_config['mysql']['hostname'],
'username' => $database_config['mysql']['username'],
'password' => $database_config['mysql']['password'],
'database' => $database_config['user_db'],
);
$db = new sqldb($database_users);

$calendar = $db2->query("SELECT * FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");
//$cal_active = $db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='google_cal_active' ");
$cal_active = $db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'google_cal_active']);
// if(!$cal_active){
	$client = new Google_Client();
	$client->setApplicationName("Google Calendar PHP Starter Application");
	$client->setClientId('276047236640-ivltoj4ovpdeei5dhak1r7h3gnmlru0t.apps.googleusercontent.com');
	$client->setClientSecret('-K13Jw5RwQlxF7Tp0X1jXy3q');
	$client->setRedirectUri('https://app.akti.com/pim/admin/index.php?do=misc-maccount');
	$client->setDeveloperKey('AIzaSyAhqFkkJLyO--XNMTTXS6LPy11czGJd4Cg');
	$cal = new Google_CalendarService($client);
	// $client->setAccessToken(GOOGLE_CALENDAR);
// }
if($in['code']){
	//$id = $db->field("SELECT umeta_id FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='google_cal_active'");
	$id = $db->field("SELECT umeta_id FROM user_meta WHERE user_id= :user_id AND name= :name",['user_id'=>$_SESSION['u_id'],'name'=>'google_cal_active']);
	if(!$id){
		//$db->query("INSERT INTO user_meta SET user_id='".$_SESSION['u_id']."', name='google_cal_active', value='1' ");
		$db->insert("INSERT INTO user_meta SET user_id= :user_id, name= :name, value= :value ",['user_id'=>$_SESSION['u_id'],'name'=>'google_cal_active','value'=>'1']);
	}else{
		$db->query("UPDATE user_meta SET value='1' WHERE umeta_id='".$id."' ");
	}
	$client->authenticate($in['code']);
	$api = $client->getAccessToken();
	$db2->query("UPDATE apps SET active='1', api='".$api."' WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");

	$calendar_rel = $db2->field("SELECT app_id FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");
	$google_sync=$db2->field("SELECT app_id FROM apps WHERE main_app_id='".$calendar_rel."' AND type='outgoing' ");
	if(!$google_sync){
		$db2->query("INSERT INTO apps SET api='1', main_app_id='".$calendar_rel."', type='outgoing' ");
	}else{
		$db2->query("UPDATE apps SET api='1' WHERE app_id='".$google_sync."' ");
	}
}

//$user_type=$db->field("SELECT active FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$subscription = $db->field("SELECT base_plan_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
$user_type=$db->field("SELECT active FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
$is_new_subscription = $db2->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");
$show_notification = $db2->field("SELECT value FROM settings WHERE constant_name='ADV_CRM' ");
$ERP=$db2->field("SELECT value FROM settings WHERE constant_name = 'SUBSCRIPTION_TYPE' ");
$result =array('modules'=>array());
$result =array('sign'=>array());

console::log(perm::$allow_apps);

if(is_null($is_new_subscription)){
	
	$menu = array();
	/*$menu['free'] = array('1'=>'CRM','12'=>'Catalogue','3'=>'Projects','4'=>'Invoices','2'=>'Graphs','17'=>'Installations','16'=>'Stock');
	$menu['services'] = array('1'=>'CRM','12'=>'Catalogue','5'=>'Quotes','3'=>'Projects','13'=>'Intervention','4'=>'Invoices','2'=>'Graphs','17'=>'Installations','16'=>'Stock');
	$menu['goods'] = array('1'=>'CRM','5'=>'Quotes','12'=>'Catalogue','6'=>'Orders','14'=>'Purchase Orders','13'=>'Intervention','4'=>'Invoices','9'=>'Webshop','2'=>'Graphs','17'=>'Installations','16'=>'Stock');
	$menu['both'] = array('1'=>'CRM','5'=>'Quotes','12'=>'Catalogue','6'=>'Orders','14'=>'Purchase Orders','3'=>'Projects','13'=>'Intervention','11'=>'Contracts','4'=>'Invoices','9'=>'Webshop','2'=>'Graphs','17'=>'Installations','16'=>'Stock');*/

	$menu['free'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
	$menu['services'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));
	$menu['goods'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));
	$menu['both'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'11'=>gm('Contracts'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));

	foreach ($menu[$_SESSION['acc_type']] as $key => $value) {
		//$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_".$key."' AND user_id='".$_SESSION['u_id']."' ");
		$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$key,'user_id'=>$_SESSION['u_id']]);
		if($val===NULL){
			$val = $db2->field("SELECT value FROM settings WHERE constant_name='MODULE_".$key."' ");
		}
		if($key!='4' && $key!='10' && $user_type=='1000'){
			continue;
		}
		if($value == '5' && $subscription=='9'){
			continue;
		}

		$line=array(
			'module'		=> $value,
			'key'			=> $key,
			'checked'		=> $val == 1 && in_array($key, perm::$allow_apps) ? true : false,
			'disable'		=> in_array($key, perm::$allow_apps) ? false : true,
		);
		if($key=='12'){
			$line['disable'] = in_array(12, perm::$allow_apps) || in_array(3, perm::$allow_apps) ? false : true;
		}
		$result['modules'][] = $line;
		if($key=='1' && $subscription!='7' && $subscription!='9'){
			$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_DEALS','user_id'=>$_SESSION['u_id']]);
			if(is_null($val)){
				$val=1;
			}
			$line=array(
				'module'		=> gm('Deals'),
				'key'			=> "DEALS",
				'checked'		=> $val == 1 && in_array($key, perm::$allow_apps) ? true : false,
				'disable'		=> in_array($key, perm::$allow_apps) ? false : true,
			);
			$result['modules'][] = $line;
		}
		if($key=='4'){
			if($subscription!='7' && $ERP=='1' && $subscription!='9'){
				$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_PURCHASE_INVOICE','user_id'=>$_SESSION['u_id']]);
				if(is_null($val)){
					$val=1;
				}
				$line=array(
					'module'		=> gm('Bills'),
					'key'			=> "PURCHASE_INVOICE",
					'checked'		=> $val == 1 && in_array($key, perm::$allow_apps) ? true : false,
					'disable'		=> in_array($key, perm::$allow_apps) ? false : true,
				);
				$result['modules'][] = $line;
			}
			$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_INVOICE_EXPORTS','user_id'=>$_SESSION['u_id']]);
			if(is_null($val)){
				$val=1;
			}
			$line=array(
				'module'		=> gm('Export'),
				'key'			=> "INVOICE_EXPORTS",
				'checked'		=> $val == 1 && in_array($key, perm::$allow_apps) ? true : false,
				'disable'		=> in_array($key, perm::$allow_apps) ? false : true,
			);
			$result['modules'][] = $line;
		}
		if($key == '16'){
			$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_DISPATCHING','user_id'=>$_SESSION['u_id']]);
			if(is_null($val)){
				$val=1;
			}
			$line=array(
				'module'		=> gm('Dispatching'),
				'key'			=> "DISPATCHING",
				'checked'		=> $val == 1 && in_array($key, perm::$allow_apps) ? true : false,
				'disable'		=> in_array($key, perm::$allow_apps) ? false : true,
			);
			$result['modules'][] = $line;
		}
	}
}else{
	// ,'9'=>'Webshop'
	$menu = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'11'=>gm('Contracts'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));
	foreach (perm::$allow_apps as $key => $value) {
		if($menu[$value]){
			//$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_".$value."' AND user_id='".$_SESSION['u_id']."' ");
			$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$value,'user_id'=>$_SESSION['u_id']]);
			if($val===NULL){
				$val = $db2->field("SELECT value FROM settings WHERE constant_name='MODULE_".$value."' ");
			}
			if($value!='4' && $value!='10' && $user_type=='1000'){
				continue;
			}
			if($value == '5' && $subscription=='9'){
				continue;
			}

			$line=array(
				'module'		=> $menu[$value],
				'key'			=> $value,
				'checked'		=> $val == 1 ? true : false,
				'disable'		=> array_key_exists($value, $menu) ? false : true,
			);
			if($value=='12'){
				$line['disable'] = in_array(12, perm::$allow_apps) || in_array(3, perm::$allow_apps) ? false : true;

			}
			$result['modules'][] = $line;
			if($value=='1' && $subscription!='7' && $subscription!='9' && $show_notification=='1'){
				$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_DEALS','user_id'=>$_SESSION['u_id']]);
				if(is_null($val)){
					$val=1;
				}
				$line=array(
					'module'		=> gm('Deals'),
					'key'			=> "DEALS",
					'checked'		=> $val == 1 ? true : false,
					'disable'		=> array_key_exists($value, $menu) ? false : true,
				);
				$result['modules'][] = $line;
			}
			if($value=='4'){
				if($subscription!='7' && $ERP=='1' && $subscription!='9'){
					$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_PURCHASE_INVOICE','user_id'=>$_SESSION['u_id']]);
					if(is_null($val)){
						$val=1;
					}
					$line=array(
						'module'		=> gm('Bills'),
						'key'			=> "PURCHASE_INVOICE",
						'checked'		=> $val == 1 ? true : false,
						'disable'		=> array_key_exists($value, $menu) ? false : true,
					);
					$result['modules'][] = $line;
				}
				$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_INVOICE_EXPORTS','user_id'=>$_SESSION['u_id']]);
				if(is_null($val)){
					$val=1;
				}
				$line=array(
					'module'		=> gm('Export'),
					'key'			=> "INVOICE_EXPORTS",
					'checked'		=> $val == 1 ? true : false,
					'disable'		=> array_key_exists($value, $menu) ? false : true,
				);
				$result['modules'][] = $line;
			}
			if($value=='16'){
				$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_DISPATCHING','user_id'=>$_SESSION['u_id']]);
				if(is_null($val)){
					$val=1;
				}
				$line=array(
					'module'		=> gm('Dispatching'),
					'key'			=> "DISPATCHING",
					'checked'		=> $val == 1 ? true : false,
					'disable'		=> array_key_exists($value, $menu) ? false : true,
				);
				$result['modules'][] = $line;
			}
		}
	}
}

//$user = $db->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."'  ")->getAll();
$user = $db->query("SELECT * FROM users WHERE user_id= :user_id  ",['user_id'=>$_SESSION['u_id']])->getAll();

foreach ($user[0] as $key => $value) {
	$result[$key] = htmlspecialchars_decode(stripslashes($value));
}
$result['two_factor']=$user[0]['two_factor'] ? true : false;

$result['password'] = '';

//$checked =$db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='send_email_type' ");
$checked =$db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'send_email_type']);
$checked = explode(',',$checked);
$outgoing=$db2->field("SELECT api FROM apps WHERE main_app_id='".$calendar->f('app_id')."' AND type='outgoing' ");
//$nylas_user=$db->query("SELECT * FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
$nylas_user=$db->query("SELECT * FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
$nylas_active=$db2->field("SELECT active FROM apps WHERE name='Nylas' and type='main' AND main_app_id='0' ");
//$zen_active=$db2->field("SELECT active FROM apps WHERE name='Zendesk' and type='main' AND main_app_id='0' ");
$easyinvoice=$db2->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");

$result['notification_instant'] 		= $checked[0] == 1 ? true : false;
$result['notification_daily'] 			= $checked[1] == 2 ? true : false;
$result['show_notification'] 			= $show_notification? true : false;
$result['LANGUAGE_DD']					= build_language_dd_user($result['lang_id']);
$result['do_next']						= 'misc-myaccount-user-update_account';
$result['outgoing']						= $outgoing == 1 ? true : false;
$result['nylas_active']					= $nylas_active ? true : false;
$result['nylas_user_active']			= $nylas_user->f('active') ? true : false;
$result['nylas_title']					= $nylas_user->f('active') ? gm("Disconnect") : gm('Connect');
$result['nylas_email']					= $nylas_user->f('email');
$result['nylas_app_id']     			= $config['nylas_app_id'];
$result['nylas_app_secret'] 			= $config['nylas_app_secret'];
$result['nylas_authorize_url']			= $config['nylas_authorize_url'];
$result['easyinvoice']					= $easyinvoice==1 ? true : false;
//$result['zen_active']				= $zen_active ? true : false;

// $signature = $db->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."'  ");

$result['sign']	= array(
/*	'logo'					=> $result['signature']?"".INSTALLPATH.$result['signature']."".'?'.time():'',
	'title'					=> $result['title'],
	'mobile'				=> $result['mobile'],
	'phone'					=> $result['phone'],
	'fax'					=> $result['fax'],*/
	'text_signature'		=> $result['text_signature'],
	'do_next'				=> 'misc-myaccount-user-update_account_signature',
	'user_id'				=> $_SESSION['u_id'],
	'file'					=> '',
);

json_out($result);
?>