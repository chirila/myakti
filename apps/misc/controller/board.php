<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$db2 = new sqldb();

global $database_config;

$database_users = array(
'hostname' => $database_config['mysql']['hostname'],
'username' => $database_config['mysql']['username'],
'password' => $database_config['mysql']['password'],
'database' => $database_config['user_db'],
);
$db = new sqldb($database_users);

//$name =$db->field("SELECT first_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
$name =$db->field("SELECT first_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$firstname =$db->field("SELECT first_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$lastname =$db->field("SELECT last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$username =$db->field("SELECT username FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$logos =$db2->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO'");
$do = 'misc--user-start_aplication';
$image ='images/akti-picture.png';

/*$u_plan=$db->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$new_plan_reduced=$db2->field("SELECT value FROM settings WHERE constant_name='REDUCED_START_PRICE' ");
if($u_plan && (is_null($new_plan_reduced) || $new_plan_reduced=='1')){
  $version = '1';
}else{
  $version = '2';
}*/
$type_e=$db2->field("SELECT value FROM settings WHERE constant_name='SUBSCRIPTION_TYPE'");
if($type_e=='1'){
    $version = '2';
}else{
    $version = '1';
}
global $apps;
for ($i = 1; $i<=15; $i++){
    if(in_array($i,perm::$allow_apps)){
    	if($i==1){
        	$is_1 = true;
       	}
    	if($i==4){
    		$is_4 = true;
    	}
    	if($i==5){
    		$is_5 = true;
    	}
    	if($i==12){
    		$is_12 = true;
    	}
    }
}

$easyinvoice=$db2->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
$user_email =$db->field("SELECT email FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$domain = explode('@', $user_email);


$result['logos']								= $in['default_upload_logo'] ? $image : ($logos ? $logos.'?'.time() : $image);
$result['crm_active']							= $is_1;
$result['inv_active']							= $is_4; 
$result['quo_active']							= $is_5;
$result['cat_active']							= $is_12;
$result['country'] 								= build_country_list();
$result['annual']								= build_month_list();
$result['activity']								= build_activity_list();
$result['company_size']							= build_company_list();
$result['name']									= stripslashes($name);
$result['firstname']                            = stripslashes($firstname);
$result['lastname']                             = stripslashes($lastname);
$result['username']                             = $username;
$result['placeholder1']                         = $domain[1]? 'colleague@'.$domain[1] : 'colleague1@company.com';
$result['placeholder2']                         = $domain[1]? 'colleague2@'.$domain[1] : 'colleague2@company.com';
$result['placeholder3']                         = $domain[1]? 'colleague3@'.$domain[1]  : 'colleague3@company.com';
$result['version']								= $version;
$result['word']                                 = gm('Being paid');
$result['do_next']								= $do;
$result['ACCOUNT_COMPANY']                      = $db2->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_COMPANY'");
$result['easyinvoice']                          = $easyinvoice=='1' ? true : false;
$result['is_facq_subscription']                 = get_subscription_plan($_SESSION['u_id'])=='8' ? true : false;
$result['is_easy_invoice']                      = get_subscription_plan($_SESSION['u_id'])=='7' ? true : false;
/*
$calendar = $db2->query("SELECT * FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");
$cal_active = $db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='google_cal_active' ");*/	

switch($_SESSION['l']){
    case 'en':
        $result['service_terms_url']="#";
        $result['privay_policy_url']="#";
        break;
    case 'fr':
        $result['service_terms_url']="https://support.akti.com/hc/fr/articles/360000151329--Convention-d-utilisation-pour-Akti-";
        $result['privay_policy_url']="https://support.akti.com/hc/fr/articles/360001239289-Privacy-Policy";
        break;
    case 'nl':
    case 'du':
        $result['service_terms_url']="https://support.akti.com/hc/nl/sections/360000205049-Voorwaarden-en-Privacy";
        $result['privay_policy_url']="https://support.akti.com/hc/nl/articles/360001239289-Privacy-Policy";
        break;
} 


json_out($result);

?>