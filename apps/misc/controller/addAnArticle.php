<?php

$article = array();

$article['article_brand'] 			= build_article_brand_dd($in['article_brand_id']);
$article['ledger_account'] 			= build_article_ledger_account_dd($in['ledger_account_id']);
$article['article_category'] 		= build_article_category_dd($in['article_category_id']);
$article['article_category_id'] 	= $in['article_category_id'];
$article['ledger_account_id'] 		= $in['ledger_account_id'];
$article['is_admin']				= $_SESSION['access_level'] == 1 ? true : false;
$article['do_next']					= 'invoice-ninvoice-article-add';
$article['do_next_service']			= 'invoice-ninvoice-service-add';
$article['xget']					= 'articles_list';
$article['customer_id']				= $in['customer_id'];
$article['lang_id']					= $in['lang_id'];
$article['lang_code']				= $db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['lang_id']."'");
$article['cat_id']					= $in['cat_id'];
$article['remove_vat']				= $in['remove_vat'];
$article['vat_regime_id']				= $in['vat_regime_id'];
$article['service_checked']			= $in['service_checked'];
$article['vat_dd']					= build_vat_dd();
$article['vat_id']	                = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");
$article['price_type']				= '1';
$article['sale_unit']				= '1';
$article['packing']					= '1';
$article['price']					= display_number('0');
$article['supplier']				= get_supplier($in,true,false);



json_out($article);

function get_supplier(&$in,$showin=true,$exit=true){
	$in['supplier'] = 1;
	$in['from_addArticle_page'] = true;
	$in['noAdd'] = 'false';
	// return false;
	return ark::run('quote-accounts');
}

?>