<?php

/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$db = new sqldb();

if($in['is_batch']){
	if(!$in['path'] || !$in['for'] || !$in['file']){
			exit();
		}
		$d = new drop($in['for'],null,$in['item_id'],true,'',null,null,null, true);
}else if($in['document_files']){
	if(!$in['path'] || !$in['for'] || !$in['file']){
		exit();
	}
	$d = new drop($in['for'],null,$in['item_id'],false,'',null,null,null, false,false,true);
}else{
		if(!$in['customer_id'] || !$in['path'] || !$in['for'] || !$in['file']){
			exit();
		}
		$d = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);
}

$outFile = false;

$path = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/dropbox_files/';
if(!file_exists($path)){
	mkdir($path,0775,true);
}

//$f = fopen($path.$in['file'], 'w+b');

// Download the file
	$d->getFile($in['path'], $path.$in['file']);
//fclose($f);
doQueryLog();
header("Content-Disposition: attachment; filename=\"".$in['file']."\"");
header("Content-type: application/".$in['mime_type']);

// Dump the output
echo file_get_contents($path.$in['file']);
unlink($path.$in['file']);
exit();
?>

