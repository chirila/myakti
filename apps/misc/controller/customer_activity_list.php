<?php if (!defined('BASEPATH'))	exit('No direct script access allowed');
$o=array();
$now = time();
$pag = $in['pag'];

$count = $db->field("SELECT count(log_id) FROM logging WHERE pag='".$in['pag']."' AND field_name='".$in['field_name']."' AND field_value='".$in['field_value']."' AND type='0' ORDER BY date DESC ");
if($in['more'] == 'false'){
	$limit=' LIMIT 5';
}else
{
	$limit='';
}
$counts = ($count-3).' '.gm('more actions');
	if(($count-3) == 1 ){
		$counts = ($count-3).' '.gm(' more action');
	}

	$o['more']		= $in['more'];
	$o['count'] 		= $counts;

$o['messages']=array();
$comments = $db->query("SELECT * FROM logging
						WHERE pag='".$in['pag']."' AND field_name='".$in['field_name']."' AND field_value='".$in['field_value']."' AND type='0'
						ORDER BY date DESC ".$limit);
$i=0;
while ($comments->next()) {
	$i++;

	if($i == $max){
		break;
	}
	$replace_first = '\[\!L\!\]';
	$replace = '\[\!\/L\!\]';
	if(!strstr($comments->f('message'),'[!/L!]')){
		$replace = '\{endl\}';
		$replace_first = '\{l\}';
	}

	//Add translation to opened the weblink
	$openedText = 'opened the weblink';
	if(strpos($c,$openedText) != false){
		$openedFullText = gm('opened the weblink');
		$c = str_replace($openedText, $openedFullText, $c);
	}
	//End Add translation to opened the weblink

	preg_match_all('/'.$replace_first.'(.*)'.$replace.'/U', $comments->f('message'),$b);
	$c = $comments->f('message');
	if($b[1]){
		foreach ($b[1] as $key => $value) {
			$c = preg_replace('/'.$replace_first.'(.*)'.$replace.'/U', gm($value), $c,1);
		}
	}
	$text = $c;
	$text=str_replace("[SITE_URL]", $config['site_url'], $text);
	if(strpos($text, "href") !== false){
		preg_match("/(?<=>).*?(?=<)/", $text, $textMatch);
		
		if($textMatch && is_array($textMatch) && count($textMatch) == 1){
			preg_match("/(?<=<).*?(?=>)/", $text, $linkMatch);
			if($linkMatch && is_array($linkMatch) && count($linkMatch) == 1){
				if($pag == 'recurring_invoice' && $field_name=='recurring_invoice_id'){
					$explodedWord=explode(" ", $textMatch[0]);
					$text='<'.$linkMatch[0].' class="text-link">'.$explodedWord[0].'</a>'.str_replace($explodedWord[0], "", $textMatch[0]);
				}else if($pag == 'invoice' && $field_name=='invoice_id'){
					$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
				}else if($pag == 'quote' && $field_name=='quote_id'){
					$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
				}else if($pag == 'order' && $field_name=='order_id'){
					$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
				}else if($pag == 'p_order' && $field_name=='p_order_id'){
					$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
				}				
			}
		}
	}

	$messages=array(
		'dates' 			=> date(ACCOUNT_DATE_FORMAT,$comments->f('date')+$_SESSION['user_timezone_offset']),
		'message'			=> $text,

	);
	array_push($o['messages'], $messages);
	/*$view->loop('comments');*/
}

json_out($o);
?>