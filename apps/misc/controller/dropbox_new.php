<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
session_write_close();

if(!function_exists('order_array')){
	function order_array($a, $b){
		return strcmp($a['name'], $b['name']);
	}
}

global $config;
$drop = false;
$result=array('dropbox_files'=>array(),'dropbox_images'=>array());
$dropbox_active=$db->field("SELECT active FROM `apps` WHERE `name`='Dropbox' AND `type`='main' ");
if($dropbox_active == '1'){
	$drop = true;
	if($in['drop_folder'] && $in['customer_id'] && $in['serial_number']){
		$already_checked=$db->field("SELECT id FROM dropbox_files_check WHERE dropbox_folder='".$in['drop_folder']."' AND parent_id='".$in['item_id']."' AND dropbox_check='1' ");
		if($already_checked){
			$i=0;
			$temp_data=array();
			$saveData=$db->query("SELECT * FROM dropbox_files WHERE dropbox_folder='".$in['drop_folder']."' AND parent_id='".$in['item_id']."'");
			while($saveData->next()){
				$unserialized_data=unserialize(stripslashes($saveData->f('serialized_content')));
				if (stripslashes($saveData->f('link')) ){
					$link = stripslashes($saveData->f('link'));
				}else{
					$d = new drop($in['drop_folder'], $in['customer_id'], $in['item_id'],true,'',$in['isConcact'],$in['serial_number']);
					$link = $d->getLink(urldecode($unserialized_data->path_display));
					if($link){
						$db->query("UPDATE dropbox_files SET link='".$link."' WHERE id='".$saveData->f('id')."'");
					}					
				}
				$fls=array(
					'link'				=> $link ,
					'file' 				=> $in['serial_number'].'/'.$unserialized_data->name,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$unserialized_data->path_display,'customer_id'=>$in['customer_id'],'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'isConcact'=>$in['isConcact'],'serial_number'=>$in['serial_number'],'file_id'=> $saveData->f('id'), 'certificate_id'=> $saveData->f('certificate_id')),
					'down_file_link'	=> $saveData->f('is_url') ? $link : 'index.php?do=misc-download_dropbox_file&path='.urlencode($unserialized_data->path_display).'&customer_id='.$in['customer_id'].'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$unserialized_data->mime_type.'&file='.urlencode(str_replace('/','_', $unserialized_data->path_display)).'&isConcact='.$in['isConcact'].'&serial_number='.$in['serial_number'],
					'file_id'			=> $saveData->f('id'),
					'path'				=> $saveData->f('is_url') ? $link : $unserialized_data->path_display,
					'name'				=> $saveData->f('is_url') ? $link : $unserialized_data->name,
					'is_url'			=> $saveData->f('is_url') ? true : false
				);
				array_push($temp_data, $fls);
				$i++;
			}
			foreach($temp_data as $key=>$value){
				if(strrpos($value['name'], ".jpg") === false && strrpos($value['name'], ".jpeg") === false && strrpos($value['name'], ".png") === false && strrpos($value['name'], ".gif") === false && strrpos($value['name'], ".tiff") === false && strrpos($value['name'], ".tif") === false && strrpos($value['name'], ".bmp") === false){
					array_push($result['dropbox_files'], $value);				
				}else{
					$value['url']=str_replace("?dl=0","?raw=1",$value['link']);
					array_push($result['dropbox_images'], $value);
				}
			}
			$result['dropbox_is_file']		= true;
			goto endFile;
		}else{
			$db->query("INSERT INTO dropbox_files_check SET dropbox_folder='".addslashes($in['drop_folder'])."',parent_id='".$in['item_id']."',dropbox_check='1' ");
		}
		$d = new drop($in['drop_folder'], $in['customer_id'], $in['item_id'],true,$in['folder'],$in['isConcact'],$in['serial_number']);
		$files = $d->getContent();
		$i=0;
		if(!empty($files->entries)){
			$temp_data=array();
			foreach ($files->entries as $key => $value) {
				$l = $d->getLink(urldecode($value->path_display));
				$fls=array(
					'link'			=> $l,
					'file' 			=> $in['serial_number'].'/'.$value->name,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$value->path_display,'customer_id'=>$in['customer_id'],'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'isConcact'=>$in['isConcact'],'serial_number'=>$in['serial_number'], 'certificate_id'=> $in['certificate_id']),
					'down_file_link'		=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($value->path_display).'&customer_id='.$in['customer_id'].'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$value->mime_type.'&file='.urlencode(str_replace('/','_', $value->path_display)).'&isConcact='.$in['isConcact'].'&serial_number='.$in['serial_number'],
					'file_id'			=> 'drop_'.$i,
					'path'			=> $value->path_display,
					'name'			=> $value->name,
				);
				$db->query("INSERT INTO dropbox_files SET 
					dropbox_folder='".addslashes($in['drop_folder'])."',
					parent_id='".$in['item_id']."',
					name='".addslashes($value->name)."',
					type='".(strrpos($value->name, ".jpg") === false && strrpos($value->name, ".jpeg") === false && strrpos($value->name, ".png") === false && strrpos($value->name, ".gif") === false && strrpos($value->name, ".tiff") === false && strrpos($value->name, ".tif") === false && strrpos($value->name, ".bmp") === false ? 1 : 2)."',
					serialized_content='".addslashes(serialize($value))."',
					link='".addslashes($l)."',
					certificate_id='".$in['certificate_id']."' ");
				array_push($temp_data, $fls);
				$i++;
			}
			foreach($temp_data as $key=>$value){
				if(strrpos($value['name'], ".jpg") === false && strrpos($value['name'], ".jpeg") === false && strrpos($value['name'], ".png") === false && strrpos($value['name'], ".gif") === false && strrpos($value['name'], ".tiff") === false && strrpos($value['name'], ".tif") === false && strrpos($value['name'], ".bmp") === false){
					array_push($result['dropbox_files'], $value);				
				}else{
					$value['url']=str_replace("?dl=0","?raw=1",$value['link']);
					array_push($result['dropbox_images'], $value);
				}
			}
			$result['dropbox_is_file']		= true;
		}
	} elseif($in['drop_folder'] && $in['is_batch']){
		$already_checked=$db->field("SELECT id FROM dropbox_files_check WHERE dropbox_folder='".$in['drop_folder']."' AND parent_id='".$in['item_id']."' AND dropbox_check='1' ");
		if($already_checked){
			$i=0;
			$temp_data=array();
			$saveData=$db->query("SELECT * FROM dropbox_files WHERE dropbox_folder='".$in['drop_folder']."' AND parent_id='".$in['item_id']."'");
			while($saveData->next()){
				$unserialized_data=unserialize(stripslashes($saveData->f('serialized_content')));
				$fls=array(
					'link'				=> stripslashes($saveData->f('link')),
					'file' 				=> $unserialized_data->path_display,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$unserialized_data->path_display,'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'is_batch=1','file_id'=> $saveData->f('id'), 'certificate_id'=> $saveData->f('certificate_id')),
					'down_file_link'	=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($unserialized_data->path_display).'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$unserialized_data->mime_type.'&file='.urlencode(str_replace('/','_', $unserialized_data->path_display)).'&is_batch=1',
					'file_id'			=> $saveData->f('id'),
					'path'				=> $unserialized_data->path_display,
					'name'				=> $unserialized_data->name,
				);
				array_push($temp_data, $fls);
				$i++;
			}
			foreach($temp_data as $key=>$value){
				if(strrpos($value['name'], ".jpg") === false && strrpos($value['name'], ".jpeg") === false && strrpos($value['name'], ".png") === false && strrpos($value['name'], ".gif") === false && strrpos($value['name'], ".tiff") === false && strrpos($value['name'], ".tif") === false && strrpos($value['name'], ".bmp") === false){
					array_push($result['dropbox_files'], $value);				
				}else{
					$value['url']=str_replace("?dl=0","?raw=1",$value['link']);
					array_push($result['dropbox_images'], $value);
				}
			}
			$result['dropbox_is_file']		= true;
			goto endFile;
		}else{
			$db->query("INSERT INTO dropbox_files_check SET dropbox_folder='".addslashes($in['drop_folder'])."',parent_id='".$in['item_id']."',dropbox_check='1' ");
		}
		$d = new drop($in['drop_folder'], null, $in['item_id'],true,$in['folder'],null,null,null,true);
		$files = $d->getContent();
		$i=0;
		if(!empty($files->entries)){
			$temp_data=array();
			foreach ($files->entries as $key => $value) {
				$l = $d->getLink(urldecode($value->path_display));
				$fls=array(
					'link'			=> $l,
					'file' 			=> $value->path_display,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$value->path_display,'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'is_batch=1', 'certificate_id'=> $in['certificate_id']),
					'down_file_link'		=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($value->path_display).'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$value->mime_type.'&file='.urlencode(str_replace('/','_', $value->path_display)).'&is_batch=1',
					'file_id'			=> 'drop_'.$i,
					'path'			=> $value->path_display,
					'name'			=> $value->name,
				);
				$db->query("INSERT INTO dropbox_files SET 
					dropbox_folder='".addslashes($in['drop_folder'])."',
					parent_id='".$in['item_id']."',
					name='".addslashes($value->name)."',
					type='".(strrpos($value->name, ".jpg") === false && strrpos($value->name, ".jpeg") === false && strrpos($value->name, ".png") === false && strrpos($value->name, ".gif") === false && strrpos($value->name, ".tiff") === false && strrpos($value->name, ".tif") === false && strrpos($value->name, ".bmp") === false ? 1 : 2)."',
					serialized_content='".addslashes(serialize($value))."',
					link='".addslashes($l)."' ");
				array_push($temp_data, $fls);
				$i++;
			}
			foreach($temp_data as $key=>$value){
				if(strrpos($value['name'], ".jpg") === false && strrpos($value['name'], ".jpeg") === false && strrpos($value['name'], ".png") === false && strrpos($value['name'], ".gif") === false && strrpos($value['name'], ".tiff") === false && strrpos($value['name'], ".tif") === false && strrpos($value['name'], ".bmp") === false){
					array_push($result['dropbox_files'], $value);				
				}else{
					$value['url']=str_replace("?dl=0","?raw=1",$value['link']);
					array_push($result['dropbox_images'], $value);
				}
			}
			$result['dropbox_is_file']		= true;
		}
	}else if($in['drop_folder'] && $in['document_files']){
		$d = new drop($in['drop_folder'], null, $in['item_id'],false,$in['folder'],null,null,null,false,false,true);
		$files = $d->getContent();
		$i=0;
		if(!empty($files->entries)){
			$temp_data=array();
			foreach ($files->entries as $key => $value) {
				$l = $d->getLink(urldecode($value->path_display));
				$fls=array(
					'link'			=> $l,
					'file' 			=> $value->path_display,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$value->path_display,'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'document_files=1', 'certificate_id'=> $in['certificate_id']),
					'down_file_link'		=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($value->path_display).'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$value->mime_type.'&file='.urlencode(str_replace('/','_', $value->path_display)).'&document_files=1',
					'file_id'			=> 'drop_'.$i,
					'path'			=> $value->path_display,
					'name'			=> $value->name,
				);
				array_push($temp_data, $fls);
				$i++;
			}
			foreach($temp_data as $key=>$value){
				if(strrpos($value['name'], ".jpg") === false && strrpos($value['name'], ".jpeg") === false && strrpos($value['name'], ".png") === false && strrpos($value['name'], ".gif") === false && strrpos($value['name'], ".tiff") === false && strrpos($value['name'], ".tif") === false && strrpos($value['name'], ".bmp") === false){
					array_push($result['dropbox_files'], $value);				
				}else{
					$value['url']=str_replace("?dl=0","?raw=1",$value['link']);
					array_push($result['dropbox_images'], $value);
				}
			}
			$result['dropbox_is_file']		= true;
		}
	}
}

	endFile:

	if(!empty($result['dropbox_files'])){
		usort($result['dropbox_files'], 'order_array'); 
	}
	if(!empty($result['dropbox_images'])){
		usort($result['dropbox_images'], 'order_array'); 
	}

	$result['is_drop']		= $drop;
	$result['SHOW_TABLE']		= $i > 0 ? true : false;
	$result['folder']			= DATABASE_NAME;
	$result['e']			= msg::$error;
	$result['drop_folder']		= $in['drop_folder'];
	$result['customer_id']		= $in['customer_id'];
	$result['item_id']		= $in['item_id'];
	$result['is_contact']		= $in['isConcact'];
	$result['serial_number']	= $in['serial_number'];
	$result['is_batch']		= $in['is_batch'];
	$result['document_files']=$in['document_files'];
	$result['nr']			= $i;

return json_out($result);