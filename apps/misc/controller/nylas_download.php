<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

	global $config,$database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
	//$token_id=$db_users->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
	$token_id=$db_users->field("SELECT token FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/files/'.$in['id'].'/download');

	$put = curl_exec($ch);
	$info = curl_getinfo($ch);

	if($info['http_code']>300 || $info['http_code']==0){
		echo "<script>window.close();</script>";
	}else{
		doQueryLog();
		header("Content-type: ".$in['content_type']."; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$in['filename']);
		print_r($put);
		exit();
	}
	
?>