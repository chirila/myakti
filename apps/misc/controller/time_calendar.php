<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
/*color: e78554 Orange Intervention
color: 68c4af Green    Activities
color: e0bd5c Yellow   Schedules
color: 27a4dd Blue     Email Events*/
class TimeCalendar extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Month(){
		$in = $this->in;
		$result=array();

		if($in['start_date']){
			$result['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}
		$month = date('n',$now);
		$year = date('Y',$now);
		$month_t = mktime(0,0,0,$month,1,$year);

		$result['month_txt']	= date('F',$month_t);
		$result['user_id']		= $in['user_id'];
		$result['logged_user']	= $_SESSION['u_id'];
		$result['year']			= $year;
		$result['next']			= mktime(0,0,0,$month+1,1,$year);
		$result['prev']			= mktime(0,0,0,$month-1,1,$year);
		$result['start_t']		= $month_t;

		if(!$in['user_id'])
		{
			$in['user_id'] = $_SESSION['u_id'];
		}else
		{
			$is_shared = $this->db->query("SELECT share_calendar_id FROM customer_shared_calendars WHERE user_id='".$in['user_id']."' AND to_user='".$_SESSION['u_id']."' AND rights!='0'");
			if(!$is_shared->next())
			{
				$in['user_id'] = $_SESSION['u_id'];
			}
		}
		$month_days = date('t',$month_t);
		$month_first_day = date('N',$month_t);
		$user_ids = '';
		$shared_withme = $this->db->query("SELECT * FROM customer_shared_calendars WHERE to_user='".$_SESSION['u_id']."' AND rights!='0'");
		while($shared_withme->next())
		{
			$user_ids .= $shared_withme->f('user_id').',';
		}
		$user_ids = rtrim($user_ids,',');
		if(empty($user_ids)){
			$user_dd = build_user_dd_calendar($in['user_id'],$in['user_id']);
		}else{
			$user_dd = build_user_dd_calendar($in['user_id'],$user_ids);
		}
		$result['user_dd']=$user_dd;
		$result['user_id']=$in['user_id'];
		$result['month']=array();
		if($in['user_id']!=$_SESSION['u_id']){
			$share_filter = '&user_id='.$in['user_id'];
			$right = $this->db->field("SELECT rights FROM customer_shared_calendars WHERE user_id='".$in['user_id']."' AND to_user='".$_SESSION['u_id']."'");

			$result['hide_meet']	= $right != '2'? false :true;
			$result['hide_app']		= $right != '2'? false : true;
			$result['hide_share']	= false;	
			$i = 1;
			$day = 1;
			$loopweek = true;
			while ($loopweek) {
				$k = 1;
				$day_final=array('week'=>array());
				while ($k <= 7) {
					$text = $day;
					$bg = 'planning_empty';
					$time_data=array();
					if($i == 1 && $k < $month_first_day){
						$text = date('j',mktime(0,0,0,$month,$day-($month_first_day-$k),$year));
						$start = mktime(0,0,0,$month,$day-($month_first_day-$k),$year);
						$s_end=mktime(23,59,59,$month,$day-($month_first_day-$k),$year);

					}
					if($day > $month_days){
						$text = date('j',mktime(0,0,0,$month,$day,$year));
						$start = mktime(0,0,0,$month,$day,$year);
						$s_end=mktime(23,59,59,$month,$day,$year);					
						$day++;
					}
					if($text == $day){
						$bg = '';
						$start = mktime(0,0,0,$month,$day,$year);
						$s_end=mktime(23,59,59,$month,$day,$year);					
						$day++;

					}
					$data_nr=0;
					$hours = $this->db->query("SELECT * FROM customer_meetings WHERE user_id='".$in['user_id']."' AND start_date BETWEEN '".$start."' AND '".$s_end."'  ORDER BY start_date ");
					while($hours->next()){
						if($hours->f('type')==1)
						{
							$color='e0bd5c';
						}else if($hours->f('type')==3){
							$color='e78554';
						}

						if($hours->f('type')=='1')
						{
							$type=gm('Meeting');
						}else{
							$type=gm('Appointment');
						}
						$all_d=false;
						if($hours->f('end_date')-$hours->f('start_date')==86400){
							$all_d=true;
						}
						$temp_data=array(
							'start_time'	=> date("H:i",($hours->f('start_date'))),
							'end_time'		=> $hours->f('end_date')-$hours->f('start_date')>0 ? date("H:i",($hours->f('end_date'))) : 0,
							'message'		=> $hours->f('subject'),
							'id'			=> $hours->f('customer_meeting_id'),
							'start'			=> $start,
							'type'			=> $type,
							'is_nylas'		=> false,
							'is_log'		=> false,
							'is_service'	=> false,
							'colors'		=> 'div_'.$color,
							'tmsmp'			=> $hours->f('start_date'),
							'all_day'		=> $all_d
						);
						array_push($time_data, $temp_data);
						$data_nr++;
					}

					$nylas_events=$this->db->query("SELECT * FROM nylas_events WHERE user_id='".$in['user_id']."' AND start_time BETWEEN '".$start."' AND '".$s_end."' ORDER BY start_time");						
					while($nylas_events->next()){
						$all_d=false;
						if($nylas_events->f('end_time')-$nylas_events->f('start_time')==86400){
							$all_d=true;
						}
						$temp_data=array(
							'start_time'	=> date("H:i",$nylas_events->f('start_time')),
							'end_time'		=> $nylas_events->f('end_time')-$nylas_events->f('start_time')>0 ? date("H:i",$nylas_events->f('end_time')) : 0,
							'message'		=> $nylas_events->f('title'),
							'id'			=> $nylas_events->f('id'),
							'class'			=> '',
							'class' 		=> 'text-muted',
							'colors'		=> 'div_27a4dd',
							'start'			=> $start,
							'type'			=> gm('Email Event'),
							'is_nylas'		=> true,
							'is_log'		=> false,
							'is_task'		=> false,
							'is_service'	=> false,
							'tmsmp'			=> $nylas_events->f('start_time'),
							'all_day'		=> $all_d,
							'event_id'		=> $nylas_events->f('event_id'),
						);
						array_push($time_data, $temp_data);
						$data_nr++;
					}

					foreach($time_data as $key=>$value){
						if($key>0 && $value['tmsmp']<$time_data[$key-1]['tmsmp']){
							$new_time_data=array_splice($time_data, $key, 1);
    						array_splice($time_data, $key-1, 0, $new_time_data);
						}
					}
					
					$t_day=array(
						'text'				=> $text,				
						'bg'				=> $bg,
						'data'				=> $time_data,
						'data_nr'			=> $data_nr,
					);
					array_push($day_final['week'], $t_day);
					$k++;
					if($day > $month_days){
						$loopweek = false;
					}
				}
				array_push($result['month'], $day_final);
				$i++;
			}
		}
		else{
			$i = 1;
			$day = 1;
			$loopweek = true;
			while ($loopweek) {
				$k = 1;
				$day_final=array('week'=>array());
				while ($k <= 7) {
					$text = $day;
					$bg = 'planning_empty';
					$time_data=array();
					if($i == 1 && $k < $month_first_day){
						$text = date('j',mktime(0,0,0,$month,$day-($month_first_day-$k),$year));
						$start = mktime(0,0,0,$month,$day-($month_first_day-$k),$year);
						$s_end=mktime(23,59,59,$month,$day-($month_first_day-$k),$year);			
					}
					if($day > $month_days){
						$text = date('j',mktime(0,0,0,$month,$day,$year));
						$start = mktime(0,0,0,$month,$day,$year);
						$s_end=mktime(23,59,59,$month,$day,$year);
						$day++;
					}
					if($text == $day){
						$bg = '';
						$start = mktime(0,0,0,$month,$day,$year);
						$s_end=mktime(23,59,59,$month,$day,$year);
						$day++;

					}
					$data_nr=0;
					$hours = $this->db->query("SELECT logging.* FROM logging
						INNER JOIN logging_tracked ON logging.user_id=logging_tracked.user_id AND logging.log_id=logging_tracked.log_id
						INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity.customer_contact_activity_id
						WHERE customer_contact_activity.contact_activity_type!='4' AND logging.to_user_id='".$in['user_id']."' AND (logging.due_date BETWEEN '".$start."' AND '".$s_end."' OR logging.finish_date BETWEEN '".$start."' AND '".$s_end."') AND logging.done < 2  AND logging.field_name!='service_id' ");
					while($hours->next()){
						$class = $hours->f('field_name') == 'service_id' ? 'show_key' : 'show_log';
						$type = $hours->f('field_name') == 'service_id' ? gm('Intervention') : '';
						/*if($hours->f('due_date')<time() && $hours->f('due_date') && $hours->f('finished')=='0')
						{
							$color='text-warning';
						}else if($hours->f('finished')=='1')
						{
							$color = 'text-success';
						}else
						{
							$color = 'text-muted';
						}*/			
						$temp_data=array(
							'start_time'	=> date("H:i",$hours->f('due_date')),
							'end_time'		=> 0,
							'message'		=> $hours->f('log_comment'),
							'id'			=> $hours->f('log_id'),
							'class'			=> $class,
							'colors'		=> 'div_68c4af',
							'start'			=> $start,
							'type'			=> $type,
							'is_nylas'		=> false,
							'is_log'		=> true,
							'is_service'	=> false,
							'is_task'		=> $hours->f('message') ? true : false,
							'tmsmp'			=> $hours->f('due_date'),
							'all_day'		=> false
						);
						array_push($time_data, $temp_data);
						$data_nr++;
					}
						
					$meetings = $this->db->query("SELECT customer_meetings.*,servicing_support.planeddate,servicing_support.serial_number FROM customer_meetings 
						LEFT JOIN servicing_support ON customer_meetings.service_id=servicing_support.service_id
						WHERE customer_meetings.user_id='".$in['user_id']."' AND customer_meetings.start_date BETWEEN '".$start."' AND '".$s_end."' ORDER BY customer_meetings.start_date");
					while($meetings->next()) {
						$is_service=false;
						if($meetings->f('type')=='1'){
							$type_meet=gm('Meeting');
						}else if($meetings->f('type')==2){
							$type_meet = gm('Appointment');
						}else if($meetings->f('type')==3){
							$type_meet = gm('Intervention');
							$is_service=true;
						}else {
							$type_meet = gm('Task');
						}
						if($meetings->f('type')==1)
						{
							$color='e0bd5c';
						}else if($meetings->f('type')==3){
							$color='e78554';
						}
						$all_d=false;
						if($meetings->f('end_date')-$meetings->f('start_date')==86400){
							$all_d=true;
						}
						$temp_data=array(
							'start_time'	=> date("H:i",$meetings->f('start_date')),
							'end_time'		=> $meetings->f('end_date')-$meetings->f('start_date')>0 ? date("H:i",$meetings->f('end_date')) : 0,
							'DATE'		=> date(ACCOUNT_DATE_FORMAT,$meetings->f('planeddate')),
							'serial_number'	=> $meetings->f('serial_number'),
							'subject'		=> $meetings->f('subject'),
							'service_id'	=> $meetings->f('service_id'),
							'message'		=> $meetings->f('subject'),
							'id'			=> $meetings->f('customer_meeting_id'),
							'class'			=> '',
							'class' 		=> 'text-muted',
							'colors'		=> 'div_'.$color,
							'start'			=> $start,
							'type'			=> $type_meet,
							'is_nylas'		=> false,
							'is_log'		=> false,
							'is_service'	=> $is_service,
							'is_task'		=> $meetings->f('subject') ? true : false,
							'tmsmp'			=> $meetings->f('start_date'),
							'all_day'		=> $all_d 
						);
						array_push($time_data, $temp_data);
						$data_nr++;
					}

					$nylas_events=$this->db->query("SELECT * FROM nylas_events WHERE user_id='".$_SESSION['u_id']."' AND start_time BETWEEN '".$start."' AND '".$s_end."' ORDER BY start_time");						
					while($nylas_events->next()){
						$all_d=false;
						if($nylas_events->f('end_time')-$nylas_events->f('start_time')==86400){
							$all_d=true;
						}
						$temp_data=array(
							'start_time'	=> date("H:i",$nylas_events->f('start_time')),
							'end_time'		=> $nylas_events->f('end_time')-$nylas_events->f('start_time')>0 ? date("H:i",$nylas_events->f('end_time')) : 0,
							'message'		=> $nylas_events->f('title'),
							'id'			=> $nylas_events->f('id'),
							'class'			=> '',
							'class' 		=> 'text-muted',
							'colors'		=> 'div_27a4dd',
							'start'			=> $start,
							'type'			=> gm('Email Event'),
							'is_nylas'		=> true,
							'is_log'		=> false,
							'is_task'		=> false,
							'is_service'	=> false,
							'tmsmp'			=> $nylas_events->f('start_time'),
							'all_day'		=> $all_d,
							'event_id'		=> $nylas_events->f('event_id'),
						);
						array_push($time_data, $temp_data);
						$data_nr++;
					}

					foreach($time_data as $key=>$value){
						if($key>0 && $value['tmsmp']<$time_data[$key-1]['tmsmp']){
							$new_time_data=array_splice($time_data, $key, 1);
    						array_splice($time_data, $key-1, 0, $new_time_data);
						}
					}
					$t_day=array(
						'text'				=> $text,				
						'bg'				=> $bg,
						'data'				=> $time_data,
						'data_nr'			=> $data_nr,
					);
					array_push($day_final['week'], $t_day);
					$k++;
					if($day > $month_days){
						$loopweek = false;
					}
				}
				array_push($result['month'], $day_final);
				$i++;
			}
		}
		if($in['start'] && $in['tab'])
		{
			$filter_export = '&start='.$in['start'];
		}elseif($in['start'] && !$in['tab'])
		{
			$filter_export = '&begin='.$in['start'];
		}

		$user_nylas=$this->db_users->field("SELECT active FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$nylas_active=$this->db->field("SELECT active FROM apps WHERE name='Nylas' AND main_app_id='0' and type='main' ");
		$result['pick_date_format'] 	= pick_date_format();
		$result['export_filter']	   	= $filter_export;
		$result['session_user_id']  	= $_SESSION['u_id'];
		$result['tab']					= 1;
		$result['nylas_active']			= ($nylas_active && $user_nylas) ? true : false;
		$result['task_nr']=$this->get_task_nr();

		$this->out = $result;
	}

	public function get_Day(){
		$in = $this->in;
		$data = array('user_data'=>array(),'days_nr'=>array());

		if(!$in['user_id']){
			$in['user_id']=$_SESSION['u_id'];
		}
		$user_ids = '';
		$shared_withme = $this->db->query("SELECT * FROM customer_shared_calendars WHERE to_user='".$_SESSION['u_id']."' AND rights!='0'");
		while($shared_withme->next())
		{
			$user_ids .= $shared_withme->f('user_id').',';
		}
		$user_ids = rtrim($user_ids,',');
		if(empty($user_ids)){
			$user_dd = build_user_dd_calendar($in['user_id'],$in['user_id']);
		}else{
			$user_dd = build_user_dd_calendar($in['user_id'],$user_ids);
		}

		$data['user_id'] = $in['user_id'];
		$data['user_dd']=$user_dd;

		if($in['selected_user_id']){
			foreach($user_dd as $key=>$value){
				if($value['id']==$in['selected_user_id']){
					$content_users=array($value);
					break;
				}			
			}
		}else{
			$content_users=$user_dd;
		}

		if($in['start_date']){
			$data['start_date']=is_numeric($in['start_date']) ? $in['start_date']*1000 : strtotime($in['start_date'])*1000;
			$now			= is_numeric($in['start_date']) ? $in['start_date'] : strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$day=date('j',$now);
		$month= date('n', $now);
		$year= date('Y', $now);
		$start_day=mktime(0,0,0,$month,$day,$year);
		$end_day=$start_day+86399;

		$data['day_txt']=date(ACCOUNT_DATE_FORMAT,$end_day);
		$data['start_t']=$start_day;

		for($i=1;$i<25;$i++){
			array_push($data['days_nr'],array('day'=>number_as_hour($i)));
		}
		$colors = array('ea9c62','41ad65','8e9599');
		$is_top = false;
		foreach($content_users as $row => $val){
			$day_data=array();
			$all_day_data=array();
			if($val['id']!=$_SESSION['u_id']){
				$hours = $this->db->query("SELECT * FROM customer_meetings WHERE user_id='".$val['id']."' AND start_date BETWEEN '".$start_day."' AND '".$end_day."'  ORDER BY start_date ");
				$some_array = array();
				while($hours->next()){				
					$new_date = mktime(0,0,0,date('m',$hours->f('start_date')), date('d',$hours->f('start_date')), date('Y',$hours->f('start_date')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$hours->f('start_date')),(date('G',$hours->f('end_date')) - date('G',$hours->f('start_date')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$hours->reset();
				$time_nr=0;
				while($hours->next()){
					$new_date = mktime(0,0,0,date('m',$hours->f('start_date')), date('d',$hours->f('start_date')), date('Y',$hours->f('start_date')));
					$width = 0;
					if(@array_key_exists(date('G',$hours->f('start_date')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$hours->f('start_date'))]['same_hour'];
					}
					if($hours->f('type')=='1'){
						$type=gm('Meeting');
					}else{
						$type=gm('Appointment');
					}
					if($hours->f('type')==1)
					{
						$color='e0bd5c';
					}else if($hours->f('type')==3){
						$color='e78554';
					}
					$all_d=false;
					if($hours->f('end_date')-$hours->f('start_date')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $hours->f('subject'),
						'hours'					=> date("H:i",($hours->f('start_date'))),
						'id'					=> $hours->f('customer_meeting_id'),
						'tmsmp'					=> $hours->f('start_date'),
						'start_time'			=> date("H:i",($hours->f('start_date'))),
						'end_time'				=> $hours->f('end_date')-$hours->f('start_date')>0 ? date("H:i",($hours->f('end_date'))) : 0,
						'type'					=> $type,
						'colors'				=> 'div_'.$color,
						'is_nylas'				=> false,
						'is_log'				=> false,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$hours->f('start_date'))),
						'pduration'				=> floor(date('G',$hours->f('end_date'))-date('G',$hours->f('start_date'))) > 0 ? 'pduration_'.floor(date('G',$hours->f('end_date'))-date('G',$hours->f('start_date'))) : '',
						//'colors'				=> 'div_'.$colors[2],
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d
					);
					if($all_d){
						array_push($all_day_data,$data_temp);
					}else{
						array_push($day_data,$data_temp);
					}			
					$time_nr++;
				}

				$nylas_events=$this->db->query("SELECT * FROM nylas_events WHERE user_id='".$val['id']."' AND start_time BETWEEN '".$start_day."' AND '".$end_day."' ORDER BY start_time");						
				$some_array = array();
				while($nylas_events->next()){				
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$nylas_events->f('start_time')),(date('G',$nylas_events->f('end_time')) - date('G',$nylas_events->f('start_time')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$nylas_events->reset();
				while($nylas_events->next()){
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					$width = 0;
					if(@array_key_exists(date('G',$nylas_events->f('start_time')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$nylas_events->f('start_time'))]['same_hour'];
					}
					$all_d=false;
					if($nylas_events->f('end_time')-$nylas_events->f('start_time')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $nylas_events->f('title'),
						'hours'					=> date("H:i",($nylas_events->f('start_time'))),
						'id'					=> $nylas_events->f('id'),
						'tmsmp'					=> $nylas_events->f('start_time'),
						'start_time'			=> date("H:i",($nylas_events->f('start_time'))),
						'end_time'				=> $nylas_events->f('end_time')-$nylas_events->f('start_time')>0 ? date("H:i",($nylas_events->f('end_time'))) : 0,
						'type'					=> gm('Email Event'),
						'is_nylas'				=> true,
						'is_log'				=> false,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$nylas_events->f('start_time'))),
						'pduration'				=> floor(date('G',$nylas_events->f('end_time'))-date('G',$nylas_events->f('start_time'))) > 0 ? 'pduration_'.floor(date('G',$nylas_events->f('end_time'))-date('G',$nylas_events->f('start_time'))) : '',
						//'colors'				=> 'div_'.$colors[2],
						'colors'				=> 'div_27a4dd',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d,
						'event_id'				=> $nylas_events->f('event_id'),
					);
					if($all_d){
						array_push($all_day_data,$data_temp);
					}else{
						array_push($day_data,$data_temp);	
					}
					$time_nr++;
				}

				foreach($day_data as $key=>$value){
					if($key>0 && $value['tmsmp']<$day_data[$key-1]['tmsmp']){
						$new_time_data=array_splice($day_data, $key, 1);
    					array_splice($day_data, $key-1, 0, $new_time_data);
					}
				}

				$temp_users=array(
					'data'		=> $day_data,
					'all_day'	=> $all_day_data,
					'user_id'	=> $val['id'],
					'name'		=> $val['name']				
				);
				array_push($data['user_data'],$temp_users);
			}else{
				$hours = $this->db->query("SELECT logging.* FROM logging 
						INNER JOIN logging_tracked ON logging.user_id=logging_tracked.user_id AND logging.log_id=logging_tracked.log_id
						INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity.customer_contact_activity_id
						WHERE customer_contact_activity.contact_activity_type!='4' AND logging.to_user_id='".$val['id']."' AND (logging.due_date BETWEEN '".$start_day."' AND '".$end_day."' OR logging.finish_date BETWEEN '".$start_day."' AND '".$end_day."') AND logging.done < 2  AND logging.field_name!='service_id' ");
				$time_nr=0;
				while($hours->next()){				
					$type = $hours->f('field_name') == 'service_id' ? gm('Intervention') : '';
					/*if($hours->f('due_date')<time() && $hours->f('due_date') && $hours->f('finished')=='0')
					{
						$color=$colors[0];
					}else if($hours->f('finished')=='1')
					{
						$color = $colors[1];
					}else
					{
						$color = $colors[2];
					}*/

					$data_temp=array(
						'subject'				=> $hours->f('log_comment'),
						'hours'					=> date("H:i",($hours->f('due_date'))),
						'id'					=> $hours->f('log_id'),
						'tmsmp'					=> $hours->f('due_date'),
						'start_time'			=> date("H:i",($hours->f('due_date'))),
						'end_time'				=> 0,
						'type'					=> $type,
						'is_nylas'				=> false,
						'is_log'				=> true,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$hours->f('due_date'))),
						'pduration'				=> '',
						//'colors'				=> 'div_'.$color,
						'colors'				=> 'div_68c4af',
						'pwidth'				=> '',
						'all_day'				=> false
					);
					array_push($day_data,$data_temp);
					$time_nr++;
				}

				$meetings = $this->db->query("SELECT customer_meetings.*,servicing_support.planeddate,servicing_support.serial_number FROM customer_meetings 
					LEFT JOIN servicing_support ON customer_meetings.service_id=servicing_support.service_id
					WHERE customer_meetings.user_id='".$val['id']."' AND customer_meetings.start_date BETWEEN '".$start_day."' AND '".$end_day."' ORDER BY customer_meetings.start_date");
				$some_array = array();
				while($meetings->next()){				
					$new_date = mktime(0,0,0,date('m',$meetings->f('start_date')), date('d',$meetings->f('start_date')), date('Y',$meetings->f('start_date')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$meetings->f('start_date')),(date('G',$meetings->f('end_date')) - date('G',$meetings->f('start_date')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$meetings->reset();
				while($meetings->next()){
					$new_date = mktime(0,0,0,date('m',$meetings->f('start_date')), date('d',$meetings->f('start_date')), date('Y',$meetings->f('start_date')));
					$width = 0;
					if(@array_key_exists(date('G',$meetings->f('start_date')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$meetings->f('start_date'))]['same_hour'];
					}
					$is_service=false;
					if($meetings->f('type')=='1'){
						$type_meet=gm('Meeting');
					}else if($meetings->f('type')==2){
						$type_meet = gm('Appointment');
					}else if($meetings->f('type')==3){
						$type_meet = gm('Intervention');
						$is_service=true;
					}else {
						$type_meet = gm('Task');
					}
					if($meetings->f('type')==1)
					{
						$color='e0bd5c';
					}else if($meetings->f('type')==3){
						$color='e78554';
					}
					$all_d=false;
					if($meetings->f('end_date')-$meetings->f('start_date')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $meetings->f('subject'),
						'hours'					=> date("H:i",($meetings->f('start_date'))),
						'id'					=> $meetings->f('customer_meeting_id'),
						'tmsmp'					=> $meetings->f('start_date'),
						'start_time'			=> date("H:i",($meetings->f('start_date'))),
						'end_time'				=> $meetings->f('end_date')-$meetings->f('start_date')>0 ? date("H:i",($meetings->f('end_date'))) : 0,
						'DATE'				=> date(ACCOUNT_DATE_FORMAT,$meetings->f('planeddate')),
						'serial_number'			=> $meetings->f('serial_number'),
						'service_id'			=> $meetings->f('service_id'),
						'type'					=> $type_meet,
						'is_nylas'				=> false,
						'is_log'				=> false,
						'is_service'			=> $is_service,
						'phour'					=> 'phour_'.floor(date('G',$meetings->f('start_date'))),
						'pduration'				=> floor(date('G',$meetings->f('end_date'))-date('G',$meetings->f('start_date'))) > 0 ? 'pduration_'.floor(date('G',$meetings->f('end_date'))-date('G',$meetings->f('start_date'))) : '',
						//'colors'				=> 'div_'.$colors[2],
						'colors'				=> 'div_'.$color,
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d,
					);
					if($all_d){
						array_push($all_day_data,$data_temp);
					}else{
						array_push($day_data,$data_temp);	
					}
					$time_nr++;
				}

				$nylas_events=$this->db->query("SELECT * FROM nylas_events WHERE user_id='".$_SESSION['u_id']."' AND start_time BETWEEN '".$start_day."' AND '".$end_day."' ORDER BY start_time");						
				$some_array = array();
				while($nylas_events->next()){				
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$nylas_events->f('start_time')),(date('G',$nylas_events->f('end_time')) - date('G',$nylas_events->f('start_time')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$nylas_events->reset();
				while($nylas_events->next()){
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					$width = 0;
					if(@array_key_exists(date('G',$nylas_events->f('start_time')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$nylas_events->f('start_time'))]['same_hour'];
					}
					$all_d=false;
					if($nylas_events->f('end_time')-$nylas_events->f('start_time')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $nylas_events->f('title'),
						'hours'					=> date("H:i",($nylas_events->f('start_time'))),
						'id'					=> $nylas_events->f('id'),
						'tmsmp'					=> $nylas_events->f('start_time'),
						'start_time'			=> date("H:i",($nylas_events->f('start_time'))),
						'end_time'				=> $nylas_events->f('end_time')-$nylas_events->f('start_time')>0 ? date("H:i",($nylas_events->f('end_time'))) : 0,
						'type'					=> gm('Email Event'),
						'is_nylas'				=> true,
						'is_log'				=> false,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$nylas_events->f('start_time'))),
						'pduration'				=> floor(date('G',$nylas_events->f('end_time'))-date('G',$nylas_events->f('start_time'))) > 0 ? 'pduration_'.floor(date('G',$nylas_events->f('end_time'))-date('G',$nylas_events->f('start_time'))) : '',
						//'colors'				=> 'div_'.$colors[2],
						'colors'				=> 'div_27a4dd',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d,
						'event_id'				=> $nylas_events->f('event_id'),
					);
					if($all_d){
						array_push($all_day_data,$data_temp);
					}else{
						array_push($day_data,$data_temp);	
					}
					$time_nr++;
				}

				foreach($day_data as $key=>$value){
					if($key>0 && $value['tmsmp']<$day_data[$key-1]['tmsmp']){
						$new_time_data=array_splice($day_data, $key, 1);
    					array_splice($day_data, $key-1, 0, $new_time_data);
					}
				}
				$temp_users=array(
					'data'		=> $day_data,
					'all_day'	=> $all_day_data,
					'user_id'	=> $val['id'],
					'name'		=> $val['name']				
				);
				array_push($data['user_data'],$temp_users);
			}
		}

		$user_nylas=$this->db_users->field("SELECT active FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$nylas_active=$this->db->field("SELECT active FROM apps WHERE name='Nylas' AND main_app_id='0' and type='main' ");
		$data['is_top']=$is_top;
		$data['pick_date_format'] 	= pick_date_format();
		$data['tab']				= 1;
		$data['nylas_active']		= ($nylas_active && $user_nylas) ? true : false;
		$this->out = $data;
	}

	function get_Week(){
		$in = $this->in;
		$data = array('day_data'=>array(),'all_day_data'=>array(),'days_nr'=>array(),'day_body_rows'=>array());

		if(!$in['user_id']){
			$in['user_id']=$_SESSION['u_id'];
		}
		$user_ids = '';
		$shared_withme = $this->db->query("SELECT * FROM customer_shared_calendars WHERE to_user='".$_SESSION['u_id']."' AND rights!='0'");
		while($shared_withme->next())
		{
			$user_ids .= $shared_withme->f('user_id').',';
		}
		$user_ids = rtrim($user_ids,',');
		if(empty($user_ids)){
			$user_dd = build_user_dd_calendar($in['user_id'],$in['user_id']);
		}else{
			$user_dd = build_user_dd_calendar($in['user_id'],$user_ids);
		}

		$data['user_id'] = $in['user_id'];
		$data['user_dd']=$user_dd;
		$data['logged_user']	= $_SESSION['u_id'];

		if($in['start_date']){
			$data['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		$week_end 			= $week_start + 604799;

		$data['week_txt']=date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end);
		$data['start_t']=$week_start;

		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			array_push($data['days_nr'],array('day_name' => gm(date('D',$i)),'date_month' => date('d',$i).' '.gm(date('M',$i)),'end_day'=> date("N",$i)>5 ? true : false));
		}

		$colors = array('ea9c62','41ad65','8e9599');
		$is_top = false;
			$day_data=array();
			$all_day_data=array();
			if($in['user_id']!=$_SESSION['u_id']){
				$hours = $this->db->query("SELECT * FROM customer_meetings WHERE user_id='".$in['user_id']."' AND start_date BETWEEN '".$week_start."' AND '".$week_end."'  ORDER BY start_date ");
				$some_array = array();
				while($hours->next()){				
					$new_date = mktime(0,0,0,date('m',$hours->f('start_date')), date('d',$hours->f('start_date')), date('Y',$hours->f('start_date')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$hours->f('start_date')),(date('G',$hours->f('end_date')) - date('G',$hours->f('start_date')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$hours->reset();
				$time_nr=0;
				while($hours->next()){
					$new_date = mktime(0,0,0,date('m',$hours->f('start_date')), date('d',$hours->f('start_date')), date('Y',$hours->f('start_date')));
					$width = 0;
					if(@array_key_exists(date('G',$hours->f('start_date')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$hours->f('start_date'))]['same_hour'];
					}
					if($hours->f('type')=='1'){
						$type=gm('Meeting');
					}else{
						$type=gm('Appointment');
					}
					if($hours->f('type')==1)
					{
						$color='e0bd5c';
					}else if($hours->f('type')==3){
						$color='e78554';
					}
					$all_d=false;
					if($hours->f('end_date')-$hours->f('start_date')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $hours->f('subject'),
						'hours'					=> date("H:i",($hours->f('start_date'))),
						'id'					=> $hours->f('customer_meeting_id'),
						'tmsmp'					=> $hours->f('start_date'),
						'start_time'			=> date("H:i",($hours->f('start_date'))),
						'end_time'				=> $hours->f('end_date')-$hours->f('start_date')>0 ? date("H:i",($hours->f('end_date'))) : 0,
						'type'					=> $type,
						'is_nylas'				=> false,
						'is_log'				=> false,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$hours->f('start_date'))),
						'pduration'				=> (correct_val(date('H:i',$hours->f('end_date')))-correct_val(date('H:i',$hours->f('start_date'))) > 0) ? 'pduration_'.ceil(ceil(correct_val(date('H:i',$hours->f('end_date'))))-floor(correct_val(date('H:i',$hours->f('start_date'))))  ) : 'pduration_1',
						'pday'					=> 'pday_'.date('N',$new_date),
						//'colors'				=> 'div_'.$colors[2],
						'colors'				=> 'div_'.$color,
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d
					);
					if($all_d){
						array_push($data['all_day_data'],$data_temp);
					}else{
						array_push($data['day_data'],$data_temp);
					}			
					$time_nr++;
				}

				$nylas_events=$this->db->query("SELECT * FROM nylas_events WHERE user_id='".$in['user_id']."' AND start_time BETWEEN '".$week_start."' AND '".$week_end."' ORDER BY start_time");						
				$some_array = array();
				while($nylas_events->next()){				
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$nylas_events->f('start_time')),(date('G',$nylas_events->f('end_time')) - date('G',$nylas_events->f('start_time')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$nylas_events->reset();
				while($nylas_events->next()){
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					$width = 0;
					if(@array_key_exists(date('G',$nylas_events->f('start_time')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$nylas_events->f('start_time'))]['same_hour'];
					}
					$all_d=false;
					if($nylas_events->f('end_time')-$nylas_events->f('start_time')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $nylas_events->f('title'),
						'hours'					=> date("H:i",($nylas_events->f('start_time'))),
						'id'					=> $nylas_events->f('id'),
						'tmsmp'					=> $nylas_events->f('start_time'),
						'start_time'			=> date("H:i",($nylas_events->f('start_time'))),
						'end_time'				=> $nylas_events->f('end_time')-$nylas_events->f('start_time')>0 ? date("H:i",($nylas_events->f('end_time'))) : 0,
						'type'					=> gm('Email Event'),
						'is_nylas'				=> true,
						'is_log'				=> false,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$nylas_events->f('start_time'))),
						'pduration'				=> (correct_val(date('H:i',$nylas_events->f('end_time')))-correct_val(date('H:i',$nylas_events->f('start_time'))) > 0) ? 'pduration_'.ceil(ceil(correct_val(date('H:i',$nylas_events->f('end_time'))))-floor(correct_val(date('H:i',$nylas_events->f('start_time')))) ) : '',
						'pday'					=> 'pday_'.date('N',$new_date),
						//'colors'				=> 'div_'.$colors[2],
						'colors'				=> 'div_27a4dd',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d,
						'event_id'				=> $nylas_events->f('event_id'),
					);
					if($all_d){
						array_push($data['all_day_data'],$data_temp);
					}else{
						array_push($data['day_data'],$data_temp);	
					}
					$time_nr++;
				}

			}else{
				$time_nr=0;
				$hours = $this->db->query("SELECT logging.* FROM logging 
						INNER JOIN logging_tracked ON logging.user_id=logging_tracked.user_id AND logging.log_id=logging_tracked.log_id
						INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity.customer_contact_activity_id
						WHERE customer_contact_activity.contact_activity_type!='4' AND logging.to_user_id='".$in['user_id']."' AND (logging.due_date BETWEEN '".$week_start."' AND '".$week_end."' OR logging.finish_date BETWEEN '".$week_start."' AND '".$week_end."') AND logging.done < 2  AND logging.field_name!='service_id' ");
				$some_array = array();
				while($hours->next()){				
					$new_date = mktime(0,0,0,date('m',$hours->f('due_date')), date('d',$hours->f('due_date')), date('Y',$hours->f('due_date')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$hours->f('due_date')),date('G',$hours->f('due_date')) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$hours->reset();
				while($hours->next()){
					$new_date = mktime(0,0,0,date('m',$hours->f('due_date')), date('d',$hours->f('due_date')), date('Y',$hours->f('due_date')));
					$width = 0;
					if(@array_key_exists(date('G',$hours->f('due_date')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$hours->f('due_date'))]['same_hour'];
					}				
					$type = $hours->f('field_name') == 'service_id' ? gm('Intervention') : '';
					/*if($hours->f('due_date')<time() && $hours->f('due_date') && $hours->f('finished')=='0')
					{
						$color=$colors[0];
					}else if($hours->f('finished')=='1')
					{
						$color = $colors[1];
					}else
					{
						$color = $colors[2];
					}*/
					$data_temp=array(
						'subject'				=> $hours->f('log_comment'),
						'hours'					=> date("H:i",($hours->f('due_date'))),
						'id'					=> $hours->f('log_id'),
						'tmsmp'					=> $hours->f('due_date'),
						'start_time'			=> date("H:i",($hours->f('due_date'))),
						'end_time'				=> 0,
						'type'					=> $type,
						'is_nylas'				=> false,
						'is_log'				=> true,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$hours->f('due_date'))),
						'pduration'				=> 'pduration_1',
						'pday'					=> 'pday_'.date('N',$new_date),
						//'colors'				=> 'div_'.$color,
						'colors'				=> 'div_68c4af',
						'pwidth'				=> '',
						'all_day'				=> false
					);
					array_push($data['day_data'],$data_temp);
					$time_nr++;
				}

				$meetings = $this->db->query("SELECT customer_meetings.*,servicing_support.planeddate,servicing_support.serial_number FROM customer_meetings
						LEFT JOIN servicing_support ON customer_meetings.service_id=servicing_support.service_id
				 		WHERE customer_meetings.user_id='".$in['user_id']."' AND customer_meetings.start_date BETWEEN '".$week_start."' AND '".$week_end."' ORDER BY customer_meetings.start_date");
				$some_array = array();
				while($meetings->next()){				
					$new_date = mktime(0,0,0,date('m',$meetings->f('start_date')), date('d',$meetings->f('start_date')), date('Y',$meetings->f('start_date')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$meetings->f('start_date')),(date('G',$meetings->f('end_date')) - date('G',$meetings->f('start_date')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$meetings->reset();
				while($meetings->next()){
					$new_date = mktime(0,0,0,date('m',$meetings->f('start_date')), date('d',$meetings->f('start_date')), date('Y',$meetings->f('start_date')));
					$width = 0;
					if(@array_key_exists(date('G',$meetings->f('start_date')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$meetings->f('start_date'))]['same_hour'];
					}
					$is_service=false;
					if($meetings->f('type')=='1'){
						$type_meet=gm('Meeting');
					}else if($meetings->f('type')==2){
						$type_meet = gm('Appointment');
					}else if($meetings->f('type')==3){
						$type_meet = gm('Intervention');
						$is_service=true;
					}else {
						$type_meet = gm('Task');
					}
					if($meetings->f('type')==1)
					{
						$color='e0bd5c';
					}else if($meetings->f('type')==3){
						$color='e78554';
					}
					$all_d=false;
					if($meetings->f('end_date')-$meetings->f('start_date')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $meetings->f('subject'),
						'hours'					=> date("H:i",($meetings->f('start_date'))),
						'id'					=> $meetings->f('customer_meeting_id'),
						'tmsmp'					=> $meetings->f('start_date'),
						'start_time'			=> date("H:i",($meetings->f('start_date'))),
						'end_time'				=> $meetings->f('end_date')-$meetings->f('start_date')>0 ? date("H:i",($meetings->f('end_date'))) : 0,
						'DATE'				=> date(ACCOUNT_DATE_FORMAT,$meetings->f('planeddate')),
						'serial_number'			=> $meetings->f('serial_number'),
						'service_id'			=> $meetings->f('service_id'),
						'type'					=> $type_meet,
						'is_nylas'				=> false,
						'is_log'				=> false,
						'is_service'			=> $is_service,
						'phour'					=> 'phour_'.floor(date('G',$meetings->f('start_date'))),
						'pduration'				=> (correct_val(date('H:i',$meetings->f('end_date')))-correct_val(date('H:i',$meetings->f('start_date'))) > 0) ? 'pduration_'.ceil(ceil(correct_val(date('H:i',$meetings->f('end_date'))))-floor(correct_val(date('H:i',$meetings->f('start_date')))) ) : 'pduration_1',
						'pday'					=> 'pday_'.date('N',$new_date),
						//'colors'				=> 'div_'.$colors[2],
						'colors'				=> 'div_'.$color,
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d
					);
					if($all_d){
						array_push($data['all_day_data'],$data_temp);
					}else{
						array_push($data['day_data'],$data_temp);	
					}
					$time_nr++;
				}

				$nylas_events=$this->db->query("SELECT * FROM nylas_events WHERE user_id='".$_SESSION['u_id']."' AND start_time BETWEEN '".$week_start."' AND '".$week_end."' ORDER BY start_time");						
				$some_array = array();
				while($nylas_events->next()){				
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					if(!array_key_exists($new_date, $some_array)){
						$some_array[$new_date] = array();
					}
					array_push($some_array[$new_date],array(date('G',$nylas_events->f('start_time')),(date('G',$nylas_events->f('end_time')) - date('G',$nylas_events->f('start_time')) ) ));				
				}
				
				$last_try = array();
				$first_hour = 24;
				foreach ($some_array as $key => $value) {
					$last_try[$key] = array();
					if(count($value) > 1){
					 	foreach ($value as $v) {
					 		if($v[0] != 0 && $v[0] < $first_hour){
					 			$first_hour = $v[0];
					 		}
					 		$increment = 0;
					 		$same_hour = 0;
					 		foreach ($value as $v1) {
					 			$e = abs($v[0]-$v1[0]);
					 			if($e <= 1){
					 				$increment++;
					 			}
					 			if($e == 0){
					 				$same_hour ++;
					 			}
					 			if($v1[0] < $v[0] && ($v1[0]+$v1[1]) > $v[0]){
					 				$same_hour ++;
					 			}
					 			if($v1[0] > $v[0] && ($v[0]+$v[1]) > $v1[0]){
					 				$same_hour ++;
					 			}
					 		}
					 		$last_try[$key][$v[0]] = array('inc'=>$increment,'parsed'=>0,'same_hour'=>$same_hour,'compensation'=>0);
					 	}
					}
				}
				$nylas_events->reset();
				while($nylas_events->next()){
					$new_date = mktime(0,0,0,date('m',$nylas_events->f('start_time')), date('d',$nylas_events->f('start_time')), date('Y',$nylas_events->f('start_time')));
					$width = 0;
					if(@array_key_exists(date('G',$nylas_events->f('start_time')), $last_try[$new_date])){
						$width = $last_try[$new_date][date('G',$nylas_events->f('start_time'))]['same_hour'];
					}
					$all_d=false;
					if($nylas_events->f('end_time')-$nylas_events->f('start_time')==86400){
						$all_d=true;
						$is_top = true;
					}
					$data_temp=array(
						'subject'				=> $nylas_events->f('title'),
						'hours'					=> date("H:i",($nylas_events->f('start_time'))),
						'id'					=> $nylas_events->f('id'),
						'tmsmp'					=> $nylas_events->f('start_time'),
						'start_time'			=> date("H:i",($nylas_events->f('start_time'))),
						'end_time'				=> $nylas_events->f('end_time')-$nylas_events->f('start_time')>0 ? date("H:i",($nylas_events->f('end_time'))) : 0,
						'type'					=> gm('Email Event'),
						'is_nylas'				=> true,
						'is_log'				=> false,
						'is_service'			=> false,
						'phour'					=> 'phour_'.floor(date('G',$nylas_events->f('start_time'))),
						'pduration'				=> (correct_val(date('H:i',$nylas_events->f('end_time')))-correct_val(date('H:i',$nylas_events->f('start_time'))) > 0) ? 'pduration_'.ceil(ceil(correct_val(date('H:i',$nylas_events->f('end_time'))))-floor(correct_val(date('H:i',$nylas_events->f('start_time')))) ) : '',
						'pday'					=> 'pday_'.date('N',$new_date),
						//'colors'				=> 'div_'.$colors[2],
						'colors'				=> 'div_27a4dd',
						'pwidth'				=> $width ? 'pwidth_'.$width : '',
						'all_day'				=> $all_d,
						'event_id'				=> $nylas_events->f('event_id'),
					);
					if($all_d){
						array_push($data['all_day_data'],$data_temp);
					}else{
						array_push($data['day_data'],$data_temp);	
					}
					$time_nr++;
				}

				foreach($day_data as $key=>$value){
					if($key>0 && $value['tmsmp']<$day_data[$key-1]['tmsmp']){
						$new_time_data=array_splice($day_data, $key, 1);
    					array_splice($day_data, $key-1, 0, $new_time_data);
					}
				}

				foreach($all_day_data as $key=>$value){
					if($key>0 && $value['tmsmp']<$all_day_data[$key-1]['tmsmp']){
						$new_time_data=array_splice($all_day_data, $key, 1);
    					array_splice($all_day_data, $key-1, 0, $new_time_data);
					}
				}
			}


		$data['is_top']=$is_top;
		for($i=1;$i<25;$i++){
			$t_array=array('nr'=>number_as_hour($i),'data'=>array());
			for($j=0;$j<7;$j++){
				array_push($t_array['data'], array('hour'=>number_as_hour($j)));
			}
			array_push($data['day_body_rows'], $t_array);
		}

		$user_nylas=$this->db_users->field("SELECT active FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$nylas_active=$this->db->field("SELECT active FROM apps WHERE name='Nylas' AND main_app_id='0' and type='main' ");
		$data['pick_date_format'] 	= pick_date_format();
		$data['tab']				= 1;
		$data['nylas_active']		= ($nylas_active && $user_nylas) ? true : false;
		$data['task_nr']			= $this->get_task_nr();
		$this->out = $data;
	}

	function get_list(){
		$in = $this->in;
		$result=array('list'=>array());
		$l_r = ROW_PER_PAGE;
		$result['lr']=$l_r;
		if(!$in['user_id'])
		{
			$in['user_id'] = $_SESSION['u_id'];
			$rights = true;
		}else
		{
			$is_shared = $this->db->query("SELECT share_calendar_id FROM customer_shared_calendars WHERE user_id='".$in['user_id']."' AND to_user='".$_SESSION['u_id']."' AND rights!='0'");
			if(!$is_shared->next())
			{
				$in['user_id'] = $_SESSION['u_id'];
				$rights = true;
			}else
			{
				if($is_shared->f('rights')=='2')
				{
					$rights = true;
				}
			}
		}
		$user_ids = '';
		$shared_withme = $this->db->query("SELECT * FROM customer_shared_calendars WHERE to_user='".$_SESSION['u_id']."' AND rights!='0'");
		while($shared_withme->next())
		{
			$user_ids .= $shared_withme->f('user_id').',';
		}
		$user_ids = rtrim($user_ids,',');
		if(empty($user_ids)){
			$user_dd = build_user_dd_calendar($in['user_id'],$in['user_id']);
		}else{
			$user_dd = build_user_dd_calendar($in['user_id'],$user_ids);
		}
		$result['user_dd']=$user_dd;
		$result['user_id']=$in['user_id'];

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$filter = "customer_meetings.user_id='".$in['user_id']."' ";

		$order_by =" ORDER BY customer_meetings.start_date DESC ";

		if($in['meeting_id']){
			$filter .= "AND customer_meetings.customer_meeting_id='{$in['meeting_id']}' ";
		}else{
			if(!empty($in['start'])){
				$filter .= "AND customer_meetings.start_date BETWEEN '".$in['start']."' AND '".mktime(23,59,59,date('n',$in['start']),date('j',$in['start']),date('Y',$in['start']))."' ";
			}
			if(!empty($in['finish'])){
				$filter .= "AND customer_meetings.end_date BETWEEN '".$in['finish']."' AND '".mktime(23,59,59,date('n',$in['finish']),date('j',$in['finish']),date('Y',$in['finish']))."' ";
			}
		}

		if(!empty($in['order_by'])){
			$order = " ASC ";
			if($in['desc'] == '1'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
		}

		if(!empty($in['start'])) {
			$filter .= "AND customer_meetings.start_date BETWEEN '".$in['start']."' AND '".mktime(23,59,59,date('n',$in['start']),date('j',$in['start']),date('Y',$in['start']))."' ";
		}

		$max_rows =  $this->db->field("SELECT count(customer_meeting_id) FROM customer_meetings WHERE ".$filter );
		$result['max_rows']=$max_rows;
		$time = time();
		$meetings_list=$this->db->query("SELECT customer_meetings.*,servicing_support.planeddate,servicing_support.serial_number FROM customer_meetings 
			LEFT JOIN servicing_support ON customer_meetings.service_id=servicing_support.service_id
			WHERE ".$filter." ".$order_by." LIMIT ".$offset*$l_r.",".$l_r );
		$j=0;
		while($meetings_list->move_next()){
			$is_service=false;
			if($meetings_list->f('type')==1)
			{
				$type = gm('Meeting');
			}else if($meetings_list->f('type')==2)
			{
				$type = gm('Appointment');
			}else if($meetings_list->f('type')==3){
				$type = gm('Intervention');
				$is_service=true;
			}else if($meetings_list->f('type')==4){
				$type = gm('Task');
			}
			$item=array(
				'name'						=> htmlspecialchars_decode($meetings_list->f('from_c')),
				'date'						=> date(ACCOUNT_DATE_FORMAT,$meetings_list->f('start_date')),
				'id'						=> $meetings_list->f('customer_meeting_id'),
				'subject'					=> $meetings_list->f('subject'),
		 		'delete_link'				=> array('do'=>'misc-time_calendar-time-delete_meeting','xget'=>'list','log_id'=>$meetings_list->f('customer_meeting_id'),'user_id'=>$in['user_id']),
				'type'						=> $type,
				'company_name'				=> $this->db->field("SELECT name FROM customers WHERE customer_id='".$meetings_list->f('customer_id')."'"),
				'contact_name'				=> $contact_name,
				'rights'					=> $rights,
				'is_service'				=> $is_service,
				'DATE'					=> date(ACCOUNT_DATE_FORMAT,$meetings_list->f('planeddate')),
				'serial_number'				=> $meetings_list->f('serial_number'),
				'service_id'				=> $meetings_list->f('service_id'),
				'start_time'				=> date("H:i",$meetings_list->f('start_date')),
				'end_time'					=> $meetings_list->f('end_date')-$meetings_list->f('start_date')>0 ? date("H:i",$meetings_list->f('end_date')) : 0,
			);
			array_push($result['list'], $item);
			$j++;
		}

		$result['tab']					= 2;
		$result['task_nr']=$this->get_task_nr();
		$this->out = $result;
	}

	public function get_tasks(){
		$in = $this->in;
		$result=array();
		/*if(!$in['user_id']){
			$in['user_id']=$_SESSION['u_id'];
		}
		$user_ids = '';
		$shared_withme = $this->db->query("SELECT * FROM customer_shared_calendars WHERE to_user='".$_SESSION['u_id']."' AND rights!='0'");
		while($shared_withme->next())
		{
			$user_ids .= $shared_withme->f('user_id').',';
		}
		$user_ids = rtrim($user_ids,',');
		if(empty($user_ids)){
			$user_dd = build_user_dd_calendar($in['user_id'],$in['user_id']);
		}else{
			$user_dd = build_user_dd_calendar($in['user_id'],$user_ids);
		}*/
		$user_dd=array();
		$user_drop=build_user_dd();
		foreach($user_drop as $key=>$value){
			array_push($user_dd, array('id'=>$value['id'],'name'=>$value['value']));
		}
		// for unassigned tasks - since AKTI-4797
		$user_dd = array_merge($user_dd, array(array('id'=>'0', 'name'=>'Unassigned')));
        if(!$in['user_done']){
			$in['user_id']=$_SESSION['u_id'];
		}
		$result['user_id'] = $in['user_id'];
		$result['user_done']=true;
		
		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_1' ");
		if($perm_admin || $_SESSION['group'] == 'admin'){
			$result['user_dd']=$user_dd;
			$result['crm_admin']=true;
		}else{
			$result['crm_admin']=false;
			$result['user_dd']=array();
		}

		$result['list']=array();

		$l_r =ROW_PER_PAGE;
		$result['lr'] = $l_r;
		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
		$order_by_array = array('date','reminder_date');

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		if($result['crm_admin']){
			if($in['user_id']){
				$filter = " logging.to_user_id='".$in['user_id']."' AND logging.reminder_date>0 ";
			}else{
			    //commented since AKTI-4797
//				$user_filter="";
//				foreach($user_dd as $key=>$value){
//					$user_filter.="'".$value['id']."',";
//				}
//				$user_filter=rtrim($user_filter,",");
				$filter = " logging.to_user_id ='".$in['user_id']."' AND logging.type IN ('1','2','4','5','6','7') AND logging.reminder_date>0 ";
			}
		}else{
			$filter = " logging.to_user_id='".$_SESSION['u_id']."' AND logging.reminder_date>0 ";
		}
		$join=" LEFT JOIN logging_tracked ON logging.log_id=logging_tracked.log_id AND logging.user_id = logging_tracked.user_id";

		//$order_by =" ORDER BY logging.due_date DESC ";
		$order_by =" ORDER BY logging.reminder_date DESC ";
        if(!$in['view']==0){
            $search_customer_name="OR customers.name LIKE '%".$in['search']."%'";
        }else{
            $search_customer_name="";
        }

		if(!empty($in['search'])){
			$string='';
			$user_id=$this->db_users->query("SELECT user_id FROM users WHERE last_name LIKE '%".$in['search']."%' OR first_name LIKE '%".$in['search']."%' ");
			while($user_id->next()){
				$string.=$user_id->f('user_id').',';
			}
			$filter .= " AND (logging.user_id IN ('".trim($string,',')."') OR logging.log_comment LIKE '%".$in['search']."%' ".$search_customer_name." ) ";
		}
		if($in['activity'] > 0){
			//$join.=" LEFT JOIN logging_tracked ON logging.log_id=logging_tracked.log_id AND logging.user_id = logging_tracked.user_id  INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id ";
			$join.=" INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id
			        LEFT JOIN customers ON customer_contact_activity.customer_id=customers.customer_id";
			switch($in['activity']){
				case 8:
					$filter.=" AND customer_contact_activity.contact_activity_type='2' ";
					break;
				case 9:
					$filter.=" AND customer_contact_activity.contact_activity_type='5' ";
					break;
				case 10:
					$filter.=" AND customer_contact_activity.contact_activity_type='1' ";
					break;
				case 11:
					$filter.=" AND customer_contact_activity.contact_activity_type='4' ";
					break;
				default:
					$filter.=" AND customer_contact_activity.contact_activity_type='6' ";
			}
			$selected_task_list=1;
		}

		if($in['archived'] == 1){
			$filter.=" and logging.done = '2' ";
		}else{
			$filter .= " AND logging.done < '2' ";
		}

		if($in['view'] == 1){
			$filter.=" and logging.done = '2' ";
		}
		if($in['view'] == 2){
/*			if(!$selected_task_list){
				$join.=" INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id ";
			}	*/
			$filter.=" AND logging.finished='0' AND (logging.status_other='0' OR logging.status_other='1') ";
		}
		if($in['view'] == 3){
			if(!$selected_task_list){
				$join.=" INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id ";
			}
			$filter.=" AND logging.finished='0' AND logging.status_other='2' AND customer_contact_activity.contact_activity_type='6' ";
		}
		if($in['view'] == 4){
			if(!$selected_task_list){
				$join.=" INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id ";
			}
			$filter.=" AND logging.finished='0' AND logging.status_other='3' AND customer_contact_activity.contact_activity_type='6' ";
		}
		if($in['view'] == 5){
			$filter.=" AND logging.finished='0' AND logging.due_date > '0' AND logging.due_date < '".time()."' ";
		}
		if($in['view'] == 6){
			$filter.=" AND logging.finished='1' ";
		}
		if($in['view'] == 7){
			if(!$selected_task_list){
				$join.=" LEFT JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id
				         LEFT JOIN customers ON customer_contact_activity.customer_id=customers.customer_id";
			}
			$filter.=" AND logging.finished='0' AND (logging.status_other='0' OR logging.status_other='1' OR (logging.status_other='2' AND customer_contact_activity.contact_activity_type='6') OR (logging.status_other='3' AND customer_contact_activity.contact_activity_type='6') OR (logging.due_date > '0' AND logging.due_date < '".time()."')) ";
		}

		if(!empty($in['start_d']) && !empty($in['stop_d'])){
			$strt=strtotime($in['start_d']);
			$end=strtotime($in['stop_d']);
			$start_date= mktime(0, 0, 0, date('m',$strt), date('d',$strt), date('Y',$strt));
			$stop_date= mktime(23, 59, 59, date('m',$end), date('d',$end), date('Y',$end));
			$filter .= "AND logging.date BETWEEN '".$start_date."' AND '".$stop_date."' ";
			$filter_meet .= "AND logging.date BETWEEN '".$start_date."' AND '".$stop_date."' ";
		}
		if(!empty($in['start_d'])){
			$strt=strtotime($in['start_d']);
			$start_date= mktime(0, 0, 0, date('m',$strt), date('d',$strt), date('Y',$strt));
			$filter .= "AND logging.date BETWEEN '".$start_date."' AND '".time()."' ";
			$filter_meet .= "AND logging.date BETWEEN '".$start_date."' AND '".time()."' ";
		}
		if(!empty($in['stop_d'])){
			$end=strtotime($in['stop_d']);
			$stop_date= mktime(23, 59, 59, date('m',$end), date('d',$end), date('Y',$end));
			$filter .= "AND logging.date BETWEEN '946684800' AND '".$stop_date."' ";
		}
		if(!empty($in['late']))
		{
			$filter .= " AND logging.due_date!='0' AND due_date<'".mktime(23,59,59,date('n'),date('j'),date('Y'))."' AND logging.finished='0'";
		}
		//FILTER LIST

		$filter_c=' ';

		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1'){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}

		$max_rows =  $this->db->field("SELECT count(logging.log_id) FROM logging ".$join." WHERE ".$filter);
		$result['max_rows']=$max_rows;	
		$logs=$this->db->query("SELECT logging.* FROM logging ".$join." WHERE ".$filter." ".$order_by);
		$logs->move_to($offset*$l_r);
		$time = mktime(0,0,0);
		$j=0;
		$is_activity=false;
		$customer_contact_activity_id=0;
		$contact_name_array = array();
		$contacts_array = array();
		while($logs->move_next() && $j<$l_r){
			switch ($logs->f('pag')) {
				case 'order':
					$type= gm('Order');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-order.png" title="'.gm('Order').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=order-order&order_id='.$logs->f('field_value').'">'.$type.'</a>';
					$pim_orders=$this->db->query("SELECT customer_name, contact_name,contact_id FROM pim_orders WHERE order_id='".$logs->f('field_value')."'");
					$company_name = $pim_orders->f('customer_name');
					$contact_name = $pim_orders->f('contact_name');
					$contact_id=$pim_orders->f('contact_id');
					break;
				case 'p_order':
					$type= gm('Purchase Order');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-purchase_order.png" title="'.gm('Purchase Order').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=order-npo_order&p_order_id='.$logs->f('field_value').'">'.$type.'</a>';
					$pim_p_orders=$this->db->query("SELECT customer_name, contact_name,contact_id FROM pim_p_orders WHERE p_order_id='".$logs->f('field_value')."'");
					$company_name = $pim_p_orders->f('customer_name');
					$contact_name = $pim_p_orders->f('contact_name');
					$contact_id=$pim_p_orders->f('contact_id');
					break;
				case 'invoice':
					$type = gm('Invoice');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-invoice.png" title="'.gm('Invoice').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=invoice-invoice&invoice_id='.$logs->f('field_value').'">'.$type.'</a>';
					$tblinvoice=$this->db->query("SELECT buyer_name, buyer_id, contact_id FROM tblinvoice WHERE id='".$logs->f('field_value')."'");
					if($tblinvoice->f('buyer_id'))
					{
						$company_name = $tblinvoice->f('buyer_name');
						if($tblinvoice->f('contact_id'))
						{
							$cust_contacts=$this->db->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'");
							$contact_name = $cust_contacts->f('firstname').' '.$cust_contacts->f('lastname');
							$contact_id=$tblinvoice->f('contact_id');
						}
					}else
					{
						$contact_name = $tblinvoice->f('buyer_name');
						$contact_id=$tblinvoice->f('buyer_id');
					}
					break;
				case 'purchase_invoice':
					$type = gm('Purchase Invoice');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-invoice.png" title="'.gm('Purchase Invoice').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=invoice-purchase_invoice&invoice_id='.$logs->f('field_value').'">'.$type.'</a>';
					$tblinvoice=$this->db->query("SELECT supplier_id FROM tblinvoice_incomming WHERE invoice_id='".$logs->f('field_value')."'");
					if($tblinvoice->f('supplier_id'))
					{
						$company_name = get_customer_name($tblinvoice->f('supplier_id'));
						if($tblinvoice->f('contact_id'))
						{
							$cust_contacts=$this->db->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'");
							$contact_name = $cust_contacts->f('firstname').' '.$cust_contacts->f('lastname');
							$contact_id=$tblinvoice->f('contact_id');
						}
					}else
					{
						$contact_name = '';
						$contact_id='';
					}
					break;
				case 'quote':
					$type = gm('Quote');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-quote.png" title="'.gm('Quote').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=quote-nquote&quote_id='.$logs->f('field_value').'">'.$type.'</a>';
					$tblquote=$this->db->query("SELECT buyer_name, buyer_id, contact_id FROM tblquote WHERE id='".$logs->f('field_value')."'");
					if($tblquote->f('buyer_id'))
					{
						$company_name = $tblquote->f('buyer_name');
						if($tblquote->f('contact_id'))
						{
							$cust_contacts=$this->db->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$tblquote->f('contact_id')."'");
							$contact_name = $cust_contacts->f('firstname').' '.$cust_contacts->f('lastname');
							$contact_id=$tblquote->f('contact_id');
						}
					}else
					{
						$contact_name = $tblquote->f('buyer_name');
						$contact_id=$tblquote->f('buyer_id');
					}
					break;
				case 'contract':
					$type = gm('Contract');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-quote.png" title="'.gm('Contract').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=contract-contract&contract_id='.$logs->f('field_value').'">'.$type.'</a>';
					$tblquote=$this->db->query("SELECT customer_name, customer_id, contact_id FROM contracts WHERE contract_id='".$logs->f('field_value')."'");
					if($tblquote->f('customer_id'))
					{
						$company_name = $tblquote->f('customer_name');
						if($tblquote->f('contact_id'))
						{
							$cust_contacts=$this->db->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$tblquote->f('contact_id')."'");
							$contact_name = $cust_contacts->f('firstname').' '.$cust_contacts->f('lastname');
							$contact_id=$tblquote->f('contact_id');
						}
					}else
					{
						$contact_name = $tblquote->f('customer_name');
						$contact_id=$tblquote->f('customer_id');
					}
					break;
				case 'project':
					$type = gm('Project');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-project.png" title="'.gm('Project').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=project-project&project_id='.$logs->f('field_value').'">'.$type.'</a>';
					$projects=$this->db->query("SELECT company_name,is_contact FROM projects WHERE project_id='".$logs->f('field_value')."'");
					if($projects->f('is_contact')=='0')
					{
						$company_name = $projects->f('company_name');
					}else
					{
						$contact_name = $projects->f('company_name');
						$contact_id=$projects->f('customer_id');
					}
					break;
				case 'customer':
					$type = gm('Customer');
					$company_name = '';
					$contact_name = '';
					$contact_id='';
					$type_img='';
					$is_activity=true;
					//$type_link='<a style="display:block;color:inherit" href="index.php?do=company-customer&customer_id='.$db->f('field_value').'">'.$type.'</a>';
					$activity_type_data=$this->db->query("SELECT customer_contact_activity.contact_activity_type,customer_contact_activity.customer_contact_activity_id FROM logging_tracked INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity.customer_contact_activity_id WHERE logging_tracked.log_id='".$logs->f('log_id')."' GROUP BY logging_tracked.activity_id ");
					$activity_type=$activity_type_data->f('contact_activity_type');
					$customer_contact_activity_id=$activity_type_data->f('customer_contact_activity_id');
					switch($activity_type){
						case '1':
							$type_link="Email";
							$type_img='<img src="images/email_icon.png" title="'.gm('Email').'">';
							break;
						case '2':
							$type_link="Note";
							$type_img='<img src="images/note_icon.png" title="'.gm('Note').'">';
							break;
						case '4':
							$type_link="Meeting";
							$type_img='<img src="images/meeting_icons.png" title="'.gm('Meeting').'">';
							break;
						case '5':
							$type_link="Call";
							$type_img='<img src="images/call_icon.png" title="'.gm('Call').'">';
							break;
						default:
							$type_link="Task";
							$type_img='<img src="images/task_icons.png" title="'.gm('Task').'">';
					}
					$custom=$this->db->query("SELECT name FROM customers WHERE customer_id='".$logs->f('field_value')."'");
					$company_name = $custom->f('name');
					$custom_contacts=$this->db->query("SELECT firstname, lastname, customer_id,contact_id
								FROM customer_contacts
								WHERE contact_id in (
									SELECT contact_id
									FROM customer_contact_activity_contacts
									WHERE activity_id=(
										SELECT activity_id
										FROM logging_tracked
										WHERE log_id='".$logs->f('log_id')."' limit 1
									)
								)");

					while($custom_contacts->next()){
						$contact_name .= $custom_contacts->f('firstname').' '.$custom_contacts->f('lastname').', ';
						$contact_id=$custom_contacts->f('contact_id');

						if(!array_key_exists($custom_contacts->f('contact_id'), $contact_name_array)){
							$contact_name_array[$custom_contacts->f('contact_id')] = get_contact_first_and_name($custom_contacts->f('contact_id'));
						}
						$contacts_array[]=array('contact_name'=>$contact_name_array[$custom_contacts->f('contact_id')],'contact_id'=>$custom_contacts->f('contact_id'));

					}
					if($contact_name){
						$contact_name = rtrim($contact_name,', ');
					}
					break;
				case 'xcustomer_contact':
					$type = gm('Contact');
					$company_name = '';
					$contact_name ='';
					$type_img='<img src="images/activity_types-contact.png" title="'.gm('Contact').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=company-xcustomer_contact&contact_id='.$logs->f('field_value').'">'.$type.'</a>';
					$custom_contacts=$this->db->query("SELECT firstname, lastname, customer_id FROM customer_contacts WHERE contact_id='".$logs->f('field_value')."'");
					$contact_name = $custom_contacts->f('firstname').' '.$custom_contacts->f('lastname');
					if($custom_contacts->f('customer_id'))
					{
						$company_name = $this->db->field("SELECT name FROM customers WHERE customer_id='".$custom_contacts->f('customer_id')."'");
					}
					break;
				case 'opportunity':
					$type = gm('Opportunity');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-opportunity.png" title="'.gm('Opportunity').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=quote-opportunity&opportunity_id='.$logs->f('field_value').'">'.$type.'</a>';
					$tblopportunity=$this->db->query("SELECT buyer_name, buyer_id, contact_id FROM tblopportunity WHERE opportunity_id='".$logs->f('field_value')."'");
					if($tblopportunity->f('buyer_id'))
					{
						$company_name = $tblopportunity->f('buyer_name');
						if($tblopportunity->f('contact_id'))
						{
							$custom_contacts=$this->db->query("SELECT firstname, lastname FROM customer_contacts WHERE contact_id='".$tblopportunity->f('contact_id')."'");
							$contact_name = $custom_contacts->f('firstname').' '.$custom_contacts->f('lastname');
							$contact_id =$tblopportunity->f('contact_id');
						}
					}else
					{
						$contact_name = $tblopportunity->f('buyer_name');
						$contact_id =$tblopportunity->f('buyer_id');
					}
					break;
				case 'service':
					$type = gm('Intervention');
					$company_name = '';
					$contact_name ='';
					$contact_id='';
					$type_img='<img src="images/activity_types-intervention.png" title="'.gm('Intervention').'">';
					$type_link='<a style="display:block;color:inherit" href="index.php?do=maintenance-services&service_id='.$logs->f('field_value').'">'.$type.'</a>';
					$servicing_support=$this->db->query("SELECT customer_name, contact_name, contact_id,customer_id FROM servicing_support WHERE service_id='".$logs->f('field_value')."'");
					$company_name = get_customer_name($servicing_support->f('customer_id'));
					$contact_name = $servicing_support->f('contact_name');
					$contact_id=$servicing_support->f('contact_id');
					break;
				default:
					$company_name = '';
					$contact_name = '';
					$contact_id='';
					$type_img='<img src="images/activity_types-default.png" title="'.gm('Other').'">';
					$custom=$this->db->query("SELECT name FROM customers WHERE customer_id='".$logs->f('field_value')."'");
					$company_name = $custom->f('name');
					$custom_contacts=$this->db->query("SELECT firstname, lastname, customer_id,contact_id
								FROM customer_contacts
								WHERE contact_id in (
									SELECT contact_id
									FROM customer_contact_activity_contacts
									WHERE activity_id=(
										SELECT activity_id
										FROM logging_tracked
										WHERE log_id='".$logs->f('log_id')."' limit 1
									)
								)");

					while($custom_contacts->next()){
						$contact_name .= $custom_contacts->f('firstname').' '.$custom_contacts->f('lastname').', ';
						$contact_id=$custom_contacts->f('contact_id');
					}
					if($contact_name){
						$contact_name = rtrim($contact_name,', ');
					}
					break;
			}

			$status_img='';
			$title_img='';
			if($logs->f('finished') == '0' && $logs->f('reminder_date')>0 && $logs->f('reminder_date') < time()){
				$status_img='item-status-red';
				$title_img=gm('Late');
			}elseif($logs->f('finished') == '0' && $logs->f('status_other') == '0'){
				$status_img='item-status-gray';
				$title_img=gm('New');
			}elseif($logs->f('finished') == '0' && $logs->f('status_other') == '1'){
				$status_img='item-status-gray';
				$title_img=gm('Scheduled');
			}else	if($logs->f('finished') == '0' && $logs->f('status_other') == '2'){
				$status_img='item-status-yellow';
				$title_img=gm('In progress');
			}else if($logs->f('finished') == '0' && $logs->f('status_other') == '3'){
				$status_img='item-status-orange';
				$title_img=gm('On hold');
			}else	if($logs->f('finished') == '1'){
				$status_img='item-status-green';
				$title_img=gm('Completed');
			}

			$item=array(
				'name'					=> get_user_name($logs->f('user_id')),
				'date'					=> date(ACCOUNT_DATE_FORMAT,$logs->f('date')),
				'due_date'					=> $logs->f('reminder_date') ? date(ACCOUNT_DATE_FORMAT,$logs->f('reminder_date')) : '&nbsp;' ,
				'id'						=> $logs->f('log_id'),
				'read'					=> $logs->f('done'),
				//'subject'					=> $logs->f('done') == '0' ? '<b>'.$logs->f('log_comment').'</b>' : $logs->f('log_comment'),
				//'subject'					=> $logs->f('done') == '0' ? (strlen(strip_tags($logs->f('message'),"<p>"))>30 ? '<b>'.substr(strip_tags($logs->f('message'),"<p>"),0,30).'...</b>' : '<b>'.substr(strip_tags($logs->f('message'),"<p>"),0,30).'</b>') : (strlen(strip_tags($logs->f('message'),"<p>"))>30 ? substr(strip_tags($logs->f('message'),"<p>"),0,30).'...' : strip_tags($logs->f('message'),"<p>")),
				//'subject'                    => strlen(strip_tags($logs->f('message'),"<p>"))>30 ? '<p>'.substr(strip_tags($logs->f('message')),0,30).'...</p>' : strip_tags($logs->f('message'),"<p>"),
                		'subject'                    		=> $logs->f('log_comment') ? '<p>'.$logs->f('log_comment').'</p>' : '','done'						=> $logs->f('finished') == 1 ? true : false,
				'color'					=> $logs->f('finished') == 1 ? 'tr_green' : ($logs->f('due_date') ? ($logs->f('due_date') < $time ? 'tr_red' : '') :''),
		 		'delete_link'				=> array('do'=>'misc-time_calendar-time-delete_log','log_id'=>$logs->f('log_id')),
				'archive_link'				=> array('do'=>'misc-time_calendar-time-archive_log','log_id'=>$logs->f('log_id')),
				'activate_link'				=> array('do'=>'misc-time_calendar-time-activate_log','log_id'=>$logs->f('log_id')),
				'status_link'				=> 'index.php?do=pim-message_center_list-message-status&log_id='.$logs->f('log_id').'&read='.$logs->f('done').$arguments.$arguments_o,
				'mail_status'				=> $logs->f('done') == '0' ? 'unread' : 'read',
				'is_active'					=> $in['archived'] != 1 ? true : false,
				'type'					=> $logs->f('done') == '0' ? '<b>'.$type_link.'</b>' : $type_link,
				//'company_name'				=> $logs->f('done') == '0' ? '<a style="display:block;color:inherit" href="index.php?do=company-customer&customer_id='.$logs->f('field_value').'"><b>'.$company_name.'</b></a>' : '<a style="display:block;color:inherit" href="index.php?do=company-customer&customer_id='.$logs->f('field_value').'">'.$company_name.'</a>',
				//'contact_name'				=> $logs->f('done') == '0' ? '<a style="display:block;color:inherit" href="index.php?do=company-xcustomer_contact&contact_id='.$contact_id.'"><b>'.$contact_name.'</b></a>' : '<a style="display:block;color:inherit" href="index.php?do=company-xcustomer_contact&contact_id='.$contact_id.'">'.$contact_name.'</a>',
				'company_name'				=> $company_name,
				'contact_name'				=> $contact_name,
				'is_archive'				=> $in['archived'] == 1 ? true : false,
				'status'					=> $status_img,
				'title'					=> $title_img,
				'cursor'					=> $logs->f('pag') == 'customer' ? 'cursor' : '',
				'type_img'					=> $type_img,
				'is_nylas' 					=> false,
				'is_log'					=> true,

				'is_activity'				=> $is_activity,
				'message'					=> strip_tags($logs->f('log_comment')),
				'by'						=> get_user_name($logs->f('user_id')),
				'day_tmpstmp'				=> $logs->f('due_date'),
				'time2'						=> date('G:i',$logs->f('due_date')),
				'type_of_call'				=> $is_activity && $type_link=='Call' ? ($logs->f('type_of_call') == 1 ? gm('incoming call') : gm('outgoing call')) : '',
				'type_of_call_id'			=> $is_activity && $type_link=='Call' ? ($logs->f('type_of_call') == 1 ? 1 : 0): '',
				'contacts'					=> $contacts_array,
				'comments'					=> $logs->f('message'),
				'reminder_assign'			=> $logs->f('reminder_date') > 0 ?true : false,
				'to_user'					=> get_user_name($logs->f('to_user_id')),
				'reminder_tmpstmp'			=> $logs->f('reminder_date'),
				'reminder_time'				=> date('G:i',$logs->f('reminder_date')),
				'status_change'				=> $logs->f('finished') == 1 ? 1 : 0,
				'log_code'				=> $logs->f('log_id'),
				'log_id'				=> $customer_contact_activity_id,
			);

		    	array_push($result['list'],$item);
			$j++;
		}

		$result['tab']					= 3;
		$result['task_nr']=$this->get_task_nr();
		$result['showGdpr']=false;
		if($_SESSION['time_login'] && $_SESSION['showGdpr'] && !$result['gdpr_first'] && time()-$_SESSION['time_login']<15){
			$result['showGdpr']=true;
			$result['gdpr_first']=true;
		}
		$result['account_date_format'] = pick_date_format();
		$this->out = $result;
	}

	public function get_task_nr(){
		$nr = $this->db->field("SELECT count(logging.log_id) FROM logging 
			LEFT JOIN logging_tracked ON logging.log_id=logging_tracked.log_id AND logging.user_id = logging_tracked.user_id 
			LEFT JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id
			WHERE logging.done='0' AND logging.to_user_id='".$_SESSION['u_id']."' AND (customer_contact_activity.reminder_date > 0 OR logging.reminder_date>0) ");
		return $nr;
	}

	public function get_calendars(&$in){
		global $config;
		$data=array('calendar_dd'=>array(),'start_date'=>$in['start_date'],'start_time'=>$in['start_time'],'type_s'=>$in['type_s'],'user_id'=>$in['user_id']);
		$ch = curl_init();
		$token_id=$this->db_users->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/calendars ');

        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    $data_temp=json_decode($put);
	    foreach($data_temp as $key=>$value){
	    	if($value->read_only==false){
	    		array_push($data['calendar_dd'], $value);
	    	}
	    }
	    
	    $data['calendar_id']=$this->db->field("SELECT value FROM settings WHERE constant_name='NYLAS_CALENDAR_ID' ");
	    if(!$data['calendar_id']){
	    	$data['calendar_id']='0';
	    }
	    return $data;
	}

	public function get_task(){
		$in = $this->in;
		$result=array('deals'=>array(),'user_auto'=>array());

		$activity_data=$this->db->query("SELECT * FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['activity_id']."' ");
		$activity_data->next();
		if($activity_data->f('opportunity_id')){
			$result['deal_id']=$activity_data->f('opportunity_id');
		}
		if($activity_data->f('customer_id')){
			$deals=$this->db->query("SELECT * FROM tblopportunity 
			WHERE buyer_id='".$activity_data->f('customer_id')."' AND f_archived='0' ORDER BY subject ASC");
			$items = array();
			while($deals->next()){
				array_push( $result['deals'], array('id'=>$deals->f('opportunity_id'),'value'=>stripslashes($deals->f('subject'))));
			}
		}
		$meeting_end_date='';
		$location_meet='';
		if($value['contact_activity_type'] == '4'){
			$meeting_data = $this->db->query("SELECT * FROM customer_meetings WHERE activity_id='".$activity_data->f('customer_contact_activity_id')."' ");
			$meeting_end_date=$meeting_data->f('end_date');
			$location_meet=stripslashes($meeting_data->f('location'));
		}

		$contact_ids=array();
		$contacts=$this->db->query("SELECT * FROM customer_contact_activity_contacts WHERE action_type = '0' AND activity_id='".$activity_data->f('customer_contact_activity_id')."' ");
		while($contacts->next()){
			$contact_ids[]=$contacts->f('contact_id');
		}

		$result['reminder_id']			= $activity_data->f('reminder_id');
		$result['task_type']				= $activity_data->f('contact_activity_type');
		$result['comment']				= $activity_data->f('contact_activity_note');
		$result['log_comment']			= $activity_data->f('log_comment');
		$result['customer_id']			= $activity_data->f('customer_id');
		$result['contact_ids']			= $contact_ids;
		$result['end_d_meet']			= $meeting_end_date;
		$result['location_meet']		= $location_meet;
		$result['reminder_dd']=build_reminder_dd();

		$users_auto = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
		foreach ($users_auto as $key => $value) {
			$result['user_auto'][] = array('id'=>$value['user_id'], 'value'=>htmlspecialchars(strip_tags($value['first_name'] . ' ' . $value['last_name'])) );
		}
		$log_data=$this->db->query("SELECT * FROM logging WHERE log_id='".$in['log_id']."' ");
		$log_data->next();
		if($log_data->f('to_user_id')){
			$result['to_user_id']=$log_data->f('to_user_id');
			$result['user_to_id']= $log_data->f('to_user_id');
		}
		$result['user_id']=$_SESSION['u_id'];
		return $result;
	} 

}
	$time_calendar = new TimeCalendar($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $time_calendar->output($time_calendar->$fname($in));
	}

	$time_calendar->get_Month();
	$time_calendar->output();

?>