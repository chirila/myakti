<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

$users_auto = $db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
$user_auto = array();
foreach ($users_auto as $key => $value) {
	$user_auto[] = array('id'=>$value['user_id'], 'value'=>htmlspecialchars_decode(stripslashes($value['first_name'] . ' ' . $value['last_name'])) );
}

$result =array();

$result['user_auto'] = $user_auto;

$result['customer_dd'] = $db->query("SELECT customer_contact_activity.customer_id AS id, customers.name AS value FROM customer_contact_activity
						INNER JOIN customers ON customer_contact_activity.customer_id=customers.customer_id
						INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
						LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
						WHERE  email_to='".$_SESSION['u_id']."' GROUP BY customer_contact_activity.customer_id ORDER BY customers.name ")->getAll();

// $result['user_dd']					= build_user_dd();
$result['user_id']					= $_SESSION['u_id'];
//array_unshift($result['customer_dd'],array('id'=>'0','value'=>gm('All Customers')));
$result['customer2_id']				= '0';
$result['accounts']					= get_customers($in);

$result['ADV_CRM']					= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;

if($result['ADV_CRM']===false){
	json_out(array('ADV_CRM'=>false));
}

$result['account_date_format'] = pick_date_format();
$result['reminder_dd']=build_reminder_dd();

json_out($result);

function get_customers($in){
	$db = new sqldb();
	$q = strtolower($in["term"]);
	$filter = '';
	if($q){
		$filter .=" AND name LIKE '%".addslashes($q)."%'";
	}
	if($in['customer_id']){
		$filter .=" AND customer_id='".$in['customer_id']."' ";	
	}
	if($in['contact_id'])
	{
		$customers = $db->query("SELECT customers.name AS value, customers.customer_id AS id
		FROM customers
		INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id 
		WHERE customer_contactsIds.contact_id='".$in['contact_id']."' ORDER BY name limit 100  ")->getAll();
	}else
	{
		$customers = $db->query("SELECT name AS value, customer_id AS id FROM customers WHERE active=1 AND is_admin='0' {$filter} ORDER BY name limit 100")->getAll();
	}
	
	return $customers;
}

function get_contacts($in){
	$db = new sqldb();
	$q = strtolower($in["term"]);
	$filter = '';
	if($q){
		$filter .=" AND (customer_contacts.firstname LIKE '%".addslashes($q)."%' OR customer_contacts.lastname LIKE '%".addslashes($q)."%') ";
	}
	if($in['customer_id']){
		$filter .=" AND customer_contactsIds.customer_id='".$in['customer_id']."' ";	
	}
	if($in['contact_id'] && is_array($in['contact_id'])){
		$filter .=" AND customer_contactsIds.contact_id IN (".rtrim(implode(",", $in['contact_id']),",").") ";
	}
	$customers = $db->query("SELECT CONCAT_WS(' ',customer_contacts.firstname,customer_contacts.lastname) AS value, customer_contacts.contact_id AS id FROM customer_contacts 
	                        INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
	                        WHERE customer_contacts.active=1 {$filter} limit 100")->getAll();
	return $customers;
}

?>