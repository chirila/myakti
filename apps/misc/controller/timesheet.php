<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class Timesheet extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Week(){
		$in = $this->in;

		global $p_access;
		global $config;

		$result=array();

		//$tips = $page_tip->getTip('misc-timesheet');
		$filter = '1=1';
		$filter2 = ' AND 1=1 ';

		if(!$in['user_id'] )
		{
			$in['user_id'] = $_SESSION['u_id'];
		}

		$isUserActive = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='active' ");

		/*if(!is_numeric($in['start'])){
			$now 				= time();
			$today_of_week 		= date("N", $now);
			$month        		= date('n');
			$year         		= date('Y');
			$week_start 		= mktime(0,0,0,$month,(date('j')-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;

		}else if($in['start_date']){
			$now 				= pick_date_timestamp($in['start_date']);
			$today_of_week 		= date("N", $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
			$week_end 			= $week_start + 604799;

		} else {
			if(date('H',$in['start']) != '00'){
				if(date('H',$in['start']) == '23' ){
					$week_start 		= $in['start'] + 3600;
					$week_end 			= $in['start'] + 604799;
					if(date('H',$week_end) == '22' ){
						$week_end= $week_end+ 3600;
					}
				}else {
					$week_start 		= $in['start'] - 3600;
					$week_end 			= $in['start'] + 604799;
					if(date('H',$week_end) == '00' ){
						$week_end= $week_end - 3600;
					}
				}
			}else{
				$week_start 		= $in['start'];
				$week_end 			= $in['start'] + 604799;
				if(date('H',$week_end) =='00'){
					$week_end 			= $in['start'] + 604799 -3600;
				}
			}
		}*/

		if($in['start_date']){
			$result['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}
		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		/*if(date('I',$now)=='1'){
			$week_start+=3600;
		}
		$week_end 			= $week_start + 604799;*/
		if(date('H',$week_start) != '00'){
			if(date('H',$week_start) == '23' ){
				$week_start 		= $week_start + 3600;
				$week_end 			= $week_start + 604799;
				if(date('H',$week_end) == '22' ){
					$week_end= $week_end+ 3600;
				}
			}else {
				$week_start 		= $week_start - 3600;
				$week_end 			= $week_start + 604799;
				if(date('H',$week_end) == '00' ){
					$week_end= $week_end - 3600;
				}
			}
		}else{
			$week_end 			= $week_start + 604799;
			if(date('H',$week_end) =='00'){
				$week_end 			= $week_start + 604799 -3600;
			}
		}

		$is_pdf = true;
		$week_nr = date('W',$week_start);
		$year_nr = date("Y",$week_start);

		if($_SESSION['group'] != 'admin' && $_SESSION['u_id'] != $in['user_id'] ){
			$is_manager = $this->db->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");
			if(!empty($is_manager)){
				$filter2 =" AND task_time.project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
			}
		}

		$result['week_txt'] = date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end-3600);
		$result['PREV_WEEK_LINK']='index.php?do=misc-timesheet&user_id='.$in['user_id'].'&start='.($week_start-604800);
		$result['NEXT_WEEK_LINK']='index.php?do=misc-timesheet&user_id='.$in['user_id'].'&start='.($week_end+1);
		$result['ACTIVE_WEEK']='activated';

		$e = $this->db->query("SELECT task_time.contract_id,task_time.user_id,task_time.date as start_date,task_time.project_id,task_time.task_id,task_time.billed,task_time.submited AS submited,
		            tasks.task_name,projects.name as project_name,projects.active, projects.company_name AS c_name, projects.status_rate, task_time.customer_id AS c_id
		            FROM task_time
		            INNER JOIN tasks ON tasks.task_id=task_time.task_id
		            INNER JOIN projects ON projects.project_id=task_time.project_id
		            WHERE task_time.user_id='".$in['user_id']."' AND date BETWEEN '".$week_start."' AND '".$week_end."' ".$filter2."
		            GROUP BY task_time.task_id
		            ORDER BY c_name, project_name, task_name
				   ")->getAll();
		$showed_projects=array();
		$showed_projects2=array();
		$rows = count($e);
		$r=1;
		$submited = 1;
		$submited_total = 1;
		$result['task_row']=array();
		foreach ($e as $key => $value) {
			//select rows
			$t_d_row=$this->db->query("SELECT task_time.*, projects.stage
		                  FROM task_time
		                  INNER JOIN projects ON projects.project_id=task_time.project_id
		                  WHERE user_id='".$in['user_id']."'
		                  AND date BETWEEN '".$week_start."' AND '".$week_end."'
		                  AND task_time.project_id='".$value['project_id']."'
		                  AND task_time.task_id='".$value['task_id']."' ORDER BY date ASC ");
			$d=1;			
			$task_day_row=array();
			while($t_d_row->move_next()){
				$entries_sheet=$this->db->field("SELECT COUNT(id) FROM servicing_support_sheet WHERE task_time_id='".$t_d_row->f('task_time_id')."' AND user_id='".$t_d_row->f('user_id')."' AND notes!='' ");
				$notes = $this->db->field("SELECT notes FROM servicing_support_sheet WHERE task_time_id='".$t_d_row->f('task_time_id')."' AND user_id='".$t_d_row->f('user_id')."' ");
				if($t_d_row->f('submited')==0 || $t_d_row->f('submited')==2){
					$submited_total = 0;
				}
				if($d > 7){
					break;
				}
				if( $t_d_row->f('hours') && ( $_SESSION['access_level'] == 1 || ( !empty($is_manager) && $is_manager == $t_d_row->f('project_id') ) ) )
				{
					/*$is_pdf= true;
					if($_SESSION['access_level'] != 1){
						$timesheet_only = 1;
					}*/
				}
				$day_id = 'tmp_day'.$d;
				$note = trim($t_d_row->f('notes'));		
				$temp_task_day_r=array(
					'TD_ID_TASK_DAY'=> $day_id,
				  	'TASK_DAY_ID' 	=> $t_d_row->f('task_time_id'),
					//'HOURSB'      	=> $t_d_row->f('hours') ? ($t_d_row->f('project_status_rate') == 0 ? number_as_hour($t_d_row->f('hours')) : $t_d_row->f('hours')) : '',
					'HOURSB'      	=> $t_d_row->f('hours') ? ($t_d_row->f('project_status_rate') == 0 ? number_as_hour($t_d_row->f('hours')) : $t_d_row->f('hours')) : $notes ? number_as_hour($t_d_row->f('hours')) : '',
					'HOURS'       	=> $t_d_row->f('hours'),
					'IS_BILLED'   	=> ($t_d_row->f('billed')==1 || $value['active']==0) ? true:false,
					'IS_SUBMITED' 	=> $t_d_row->f('submited') == 1 || $t_d_row->f('service') == 1 ? true:false,
					'SUBMITED'	  	=> $t_d_row->f('submited') == 1 ? 'submited':'',
					'BG_COLOR'		=> !empty($note) ? 'bg-warning' : ( date('D') == date('D',$t_d_row->f('date')) && (date('W') == date('W',$t_d_row->f('date'))) && (date('Y')==date('Y',$t_d_row->f('date'))) ? 'grayish2' : ''),
					'APP'			=> $t_d_row->f('approved') == 1 ? 'approvedish': ($t_d_row->f('submited') == 1 ? 'approvedish':''),
					'IS_APPROVED' 	=> $t_d_row->f('approved') == 1 ? true:false,
					'NO_PLUS'		=> !empty($note) ? 'no-plus' : '',
					'hourly_rate'	=> $t_d_row->f('project_status_rate') == 0 ? true : false,
					'change_hours'	=> $t_d_row->f('project_status_rate') == 0 ? 'hours' : 'days',
					'hide_spinner'	=> ($t_d_row->f('billed')==1 || $value['active']=='0' || $t_d_row->f('submited') == 1 || $t_d_row->f('service') == 1 || $t_d_row->f('approved') == 1 || $isUserActive == '1' || $t_d_row->f('stage')==2 || $t_d_row->f('stage')==0) ? false : true, 
					'POSITION_DAY'	=> ($t_d_row->f('billed')==1 || $value['active']=='0' || $t_d_row->f('submited') == 1 || $t_d_row->f('service') == 1 || $t_d_row->f('approved') == 1 || $isUserActive == '1') ? '' : 'margin-left:11px;float:left',
					'IS_CLOSED'		=> ($t_d_row->f('stage')=='2' || $t_d_row->f('stage')=='0') ? true:false,
					'BILLED_IMG'	=> $t_d_row->f('billed')==1 ? true : false,
					'UNAPPROVED'	=> $t_d_row->f('unapproved')==1 ? true : false,
					'has_note'	=> $entries_sheet || $note ? true : false
			  	);
			  	array_push($task_day_row, $temp_task_day_r);
				$d++;
			}
			$in['hide_s'] = $value['submited'];
			$row_id = 'tmp'.$r;

			$t_row=array(
				'TR_ID_TASK'           		=> $row_id,
				'classN'				=> $value['contract_id'] ? 'contractId' : 'cId',
				'cId'					=> $value['c_id'],
				'project_id'			=> $value['project_id'],
			 	'PROJECT_LIST_NAME' 		=> $value['project_name'],
			 	'project_type'			=> $value['status_rate'],
			 	/*'HIDE_PROJECT_NAME'		=> in_array($value['c_id'], $showed_projects) ? false : true,*/
			 	'HIDE_PROJECT_NAME'		=> array_key_exists($value['c_id'], $showed_projects) ? false : true,
			 	'HIDE_PROJECT_NAME2'		=> in_array($value['project_id'], $showed_projects2) ? false : true,
			 	//'PROJECT_CUSTOMER_NAME' 	=> $value['c_name'],
			 	'PROJECT_CUSTOMER_NAME' 	=> array_key_exists($value['c_id'], $showed_projects) ? $showed_projects[$value['c_id']] : $value['c_name'],
				'TASK_LIST_NAME'    		=> $value['contract_id'] ? ''  :$value['task_name'],
				'TOTAL'             		=> '&nbsp;',
				'HIDE_BILLED'			=> ($value['billed'] || $value['active']==0)? false : true,
				'HIDE_UNBILLED'			=> ($value['billed'] || $value['active']==0)? true : false,
				'HIDE_SUBMITED'			=> $value['submited'] ==1 ? true : false,
				'HIDE_SUBMITED_L'			=> $value['submited'] ==1 ? false : true,
				'items_border'			=> $r == $rows ? 'show_border' : ($value['c_id']!= $e[$key+1]['c_id'] ? 'show_border' : ''),
				'maintenance_icon_show'		=> $value['service'] == 1 ? 'maintenance_icon_show' : "",
				'hide_maintenance_delete' 	=> $value['service'] == 1 ? false : true,
				'task_day_row'			=> $task_day_row
			);
			array_push($result['task_row'], $t_row);
			//array_push($showed_projects,$value['c_id']);	
			if(array_key_exists($value['c_id'], $showed_projects) === false){
				$showed_projects[$value['c_id']]=$value['c_name'];
			}		
			array_push($showed_projects2,$value['project_id']);
			$r++;
			if($value['submited'] == 1 && $submited_total == 1 ){
				$submited++;
			}
		}

		$result['HIDE_TASKS']=true;
		if($r == 1){
			$result['HIDE_TASKS']=false;
			$in['info'] .= gm('There are no hours added.');
			if(date('W',$week_start) == date('W')){
				$prev_week_end = $week_start - 1;
				$prev_week_start = $prev_week_end - 604799;

		//select all tasks
				$prev_query = $this->db->query("SELECT task_time.user_id,task_time.project_id,task_time.task_id,task_time.billable,
				            task_time.customer_id AS c_id, task_time.project_status_rate, projects.stage
				            FROM task_time
				            INNER JOIN projects ON task_time.project_id = projects.project_id
				            WHERE task_time.user_id='".$in['user_id']."'
				            AND date BETWEEN '".$prev_week_start."' AND '".$prev_week_end."' AND projects.closed='0'
				            GROUP BY task_time.task_id
				            ORDER BY task_time.task_time_id
						   ");

				$i=0;
				while ($prev_query->next()) {
					$check_h = $this->db->field("SELECT SUM(hours) FROM task_time
											WHERE user_id='".$prev_query->f('user_id')."' AND
												  project_id='".$prev_query->f('project_id')."' AND
												  task_id='".$prev_query->f('task_id')."'
												  AND date BETWEEN '".$prev_week_start."' AND '".$prev_week_end."' ");
					if($check_h > 0 ){
						$days = $week_start;
						 $billable=$this->db->field("SELECT billable FROM tasks WHERE task_id='".$prev_query->f('task_id')."'");
						while ($days < $week_end ){
							if($prev_query->f('stage')==1){
								$query = $this->db->query("INSERT INTO task_time
													SET user_id='".$prev_query->f('user_id')."',
														project_id='".$prev_query->f('project_id')."',
														task_id='".$prev_query->f('task_id')."',
														customer_id='".$prev_query->f('c_id')."',
														date='".$days."',
														hours='0',
														billable='".$billable."',
														project_status_rate='".$prev_query->f('project_status_rate')."' ");
							}
							$days += 86400;

						}
						$i++;
					}
				}
				$prev_query = null;
			}
		}

		//now walk the days and display the info
		$j =1;
		//echo $week_start.' -- '.$week_end;
		$result['day_row']=array();
		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			$day_id = 'tmp_day'.$j;
			$d_row=array(
				'day_name'    	=> gm(date('D',$i)),
				'date_month'  	=> date('d',$i).' '.gm(date('M',$i)),
				'TIMESTAMP'   	=> $i,
				'TD_ID'       	=> $day_id,
				'NO_MARGIN'		=> $j == 7 ? 'no-margin' : '',
				'TODAY'		=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? 'today' : '',
				'HIDE_ARROW'	=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? '' : 'hide',
				'end_day'		=> date("N",$i)>5 ? true : false
			);
			array_push($result['day_row'], $d_row);
		    $j++;
		}

		//if we have a dummy task and an expense, we hide the buttons
		// because is already submited
		$count_dummy_task = $this->db->field("SELECT COUNT(task_time_id) FROM task_time
							WHERE task_time.user_id='".$in['user_id']."'
							AND date BETWEEN '".$week_start."' AND '".$week_end."'
							AND notes = 'no_task_just_expense' ");
		// echo $count_dummy_task;

		$i=0;
		$result['history_row']=array();

		$beginOfDay = strtotime("midnight", $week_start);
		$endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;

		//$activity = $this->db->query("SELECT * FROM timesheet_log WHERE user_id='".$in['user_id']."' AND start_date='".$week_start."' limit 5 ");
		$activity = $this->db->query("SELECT * FROM timesheet_log WHERE user_id='".$in['user_id']."' AND start_date BETWEEN '".$beginOfDay."' AND '".$endOfDay."' ");
		while($activity->next()){

			$message = $activity->f('message');
			if(strpos($message,gm('rejected'))){
				$comment = $this->db->query("SELECT comment FROM t_comments WHERE user_id='".$in['user_id']."' AND start_date BETWEEN '".$beginOfDay."' AND '".$endOfDay."' ");
				$comment_r = $comment->f('comment');
			}else{
				$comment_r = '';
			}
			$h_row=array(
				'HISTORY_DATE'		=>$activity->f('date'),
				'HISTORY_DESCRIPTION'	=>$activity->f('message'),
				'HISTORY_MESSAGE'		=>$comment_r,
			);
			array_push($result['history_row'], $h_row);
			$i++;
		}
		if($i==0){
			$result['HIDE_LOG']	= false;
		}else {
			$result['HIDE_LOG']=$_SESSION['access_level'] == 1 ? true : false;
			if($count_dummy_task>0){
				$result['HIDE_LOG']=$_SESSION['access_level'] == 1 ? true : false;
			}
		}

		if(!$in['tab']){
			$in['tab'] = 1;
		}
		/*if($in['success']){
			$view->assign('MSG_SUCCESS','<div class="success">'.$in['success'].'</div>');
		}*/
		$add_row_box = false;

		if(date('W') == date("W",$week_start)){
			$day = '';
		}else{
			$day = '&start='.$week_start;
		}

		$user_dd = $this->build_user_dd_timetracker($in['user_id'], $_SESSION['access_level']);
		//$user_name = get_user_name($in['user_id']);

		$l=0;
		//print languages
		$result['languages_pdf']=array();
		$pim_lang = $this->db->query("SELECT * FROM pim_lang WHERE active=1 ORDER BY sort_order ");
		while($pim_lang->move_next()){
			// if($pim_lang->f('default_lang')){
		 		$default_lang = $pim_lang->f('lang_id');
		 	// }
		  	$temp_lang=array(
				'language'	        	=> gm($pim_lang->f('language')),
				'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$pim_lang->f('lang_id').'&timesheet_only='.$timesheet_only,
			);
			$l++;
			array_push($result['languages_pdf'], $temp_lang);
		}
		$pim_cust=$this->db->query("SELECT * from pim_custom_lang where active=1 ORDER BY sort_order ");
		 while($pim_cust->move_next()){
		 	$lbl_lang=$this->db->query("SELECT * FROM label_language_time WHERE lang_code='".$pim_cust->f('lang_id')."'");
		 	if($lbl_lang->move_next())
		 	{
			 	$temp_lg=array(
					'language'	        	=> $lbl_lang->f('name'),
					'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$lbl_lang->f('label_language_id').'&timesheet_only='.$timesheet_only,
				);
				$l++;
			array_push($result['languages_pdf'], $temp_lg);
			}
		}
		if($l==1){
			
			  $result['generate_pdf_id']  	= '';
			  $result['web_invoice_id']   	= '';
			  $result['hide_language']	  	= false;
			  $result['pdf_class']			= 'generate_pdf';
			  $result['pdf_link']	         	= "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$default_lang.'&timesheet_only='.$timesheet_only;
			
		}else{

			  $result['generate_pdf_id']  		= 'generate_pdf';
			  $result['web_invoice_id']   		= 'web_invoice';
			  $result['hide_language']	  		= '';
			  $result['pdf_link']	      		= '#';

		}
		$result['pdf_link']	         	= "index.php?do=misc-timesheet_print&time_start=".$week_start."&time_end=".$week_end."&user_id=".$in['user_id'].'&lid='.$default_lang.'&timesheet_only='.$timesheet_only;
		$t = time();
		$day_nr = 0;
		if($week_start < $t && $t < $week_end){
			$day_nr = date('N');
		}

		$in['close_srv'] = $submited == $r && $submited != 1 ? false : true;
		$i=13;
		$val =$this->db_users->field("SELECT value FROM user_meta WHERE name='MODULE_".$i."' AND user_id='".$_SESSION['u_id']."' ");
		if($val===NULL){
			$val = constant('MODULE_'.$i);
		}

		if(!in_array('13',$p_access)){ $val = 0; }
		$result['hide_'.$i]= $val == 0 ? false : true;

		unset($_SESSION['articles_id']);

		$partial_time=array();
		for($o=0;$o<=6;$o++){
			if(count($partial_time)==0){
				$partial_time[]=$week_start;
			}else if($o<6){
				$partial_time[$o]=strtotime("+1day",$partial_time[$o-1]);
			}else{
				$partial_time[]=$week_end;
			}
		}

		$partial_time_full=array();
		foreach($partial_time as $key => $val){
			$partial_time_full[$val]=date(ACCOUNT_DATE_FORMAT,$val);
		}


			$result['PAGE_TITLE']	    	= gm('Timesheet for').' <span id="username">'.$user_dd[0].$user_dd[1][0].($user_dd[1][1] > 1 ? '<b id="change_user" class="'.($in['is_ajax'] == 1 ? 'hide': '').'" >&nbsp;</b>' : '').'</span>';
			$result['user_dd']		= $user_dd;
			$result['user_id']           	= $in['user_id'];
			$result['is_pdf']			= $is_pdf;
			$result['PAG']              	= $in['is_ajax'] == 1 ? 'timesheet' : $in['pag'];
			$result['ADD_ROW_BOX']       	= $add_row_box;
			$result['NEXT_FUNCTION']     	= 'misc-timesheet-time-add_hour';
			$result['START']            	= $week_start;
			$result['START_DAY']		= $day;
			$result['week_s']        	= $week_start;
			$result['week_e']          	= $week_end;
			$result['HIDE_AJAX']         	= $in['is_ajax'] == 1 ? false: true;
			$result['HIDE_L']	        	= $_SESSION['access_level'] == 1 ? true : false;
			$result['DISPLAY_SAVE']      	= $submited == $r && $submited != 1 ? false : true;
			$result['IS_AJAX']           	= $in['is_ajax'] == 1 ? '1': '2';
		  	$result['STYLE']             	= ACCOUNT_NUMBER_FORMAT;
		  	$result['tab']			= $in['tab'];
		  	$result['page_tip']		= $tips;
		  	$result['pick_date_format']	= pick_date_format();
		  	$result['TODAY_nr']		= $today_of_week;
		  	//$result['allowadhoc']		= USE_ADHOC_TMS == '1'? true:false;
		  	$result['partial_submit']	= build_simple_dropdown($partial_time_full);
		  	$result['projects_list']	= $this->get_projects_list($in);
		  	$result['customers_list']	= $this->get_cc_list($in);
		  	$result['tasks_list']		= $this->get_tasks_list($in);

		if($submited == $r && $submited != 1 ){
			$in['hide_s'] = 1;
		}else{
			$in['hide_s'] = 0;
		}

		if($count_dummy_task > 0){
			$in['hide_s'] = 1 ;//hide shit from timesheet_expense
			$in['close_srv'] = false; //hide shit from Maintenance
		}

			$result['hide_if_task_no_exp_submited']	= $count_dummy_task > 0 ? false : true;

		if(in_array('11',$p_access)){
			$result['is_contract']	= true;
		}else{
			$result['is_contract'] = false;
		}
		$result['has_p']=in_array('3', perm::$allow_apps) ? true : false;

		$this->out = $result;
	}

	public function get_Month(&$in){
		$in = $this->in;

		$result=array();

		if($in['start_date']){
			$result['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$month = date('n',$now);
		$year = date('Y',$now);
		$month_t = mktime(0,0,0,$month,1,$year);
		if(date('I',$now)=='1'){
			$month_t+=3600;
		}

		$time_start = mktime(0,0,0,$month,1,$year);
		if(date('I',$now)=='1'){
			$time_start+=3600;
		}
		$time_end = mktime(0,0,0,$month+1,1,$year);
		if(date('I',$now)=='1'){
			$time_end+=3600;
		}

		$user_dd = $this->build_user_dd_timetracker($in['user_id'], $_SESSION['access_level']);

		$result['month_txt']		= gm(date('F',$month_t));
		$result['user_dd']		= $user_dd;
		$result['user_id']		= $in['user_id'];
		$result['year']			= $year;
		$result['next']			= mktime(0,0,0,$month+1,1,$year);
		$result['prev']			= mktime(0,0,0,$month-1,1,$year);
		$result['PAGE_TITLE']		= '<span id="username">'.$user_dd[0].$user_dd[1][0].($user_dd[1][1] > 1 ? '<b id="change_user" class="'.($in['is_ajax'] == 1 ? 'hide': '').'" >&nbsp;</b>' : '').'</span>';
		$result['expense_pdf_link']	= 'index.php?do=project-expenses_report_print&time_start='.$time_start.'&time_end='.$time_end.'&u_id='.$in['user_id'];
		$result['month_start']		= $time_start;
		$result['month_end']		= $time_end;

		$month_days = date('t',$month_t);
		$month_first_day = date('N',$month_t);

		$result['month']=array();
		$i = 1;
		$day = 1;
		$loopweek = true;
		$q=0;
		$w=0;
		while ($loopweek) {
			$k = 1;
			$day_final=array('week'=>array());
			while ($k <= 7) {
				$expenses = 0;
				$text = $day;
				$hours = '';
				$days = ';';
				$bg = 'planning_empty';
				if($i == 1 && $k < $month_first_day){

					$date1=mktime(0,0,0,$month,$day-($month_first_day-$k),$year);
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$text = date('j',$date1);
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours).' <span>'.gm('hours').'</span>';
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days.' <span>'.gm('days').'</span>';
					}
				}
				if($day > $month_days){
					$date1=mktime(0,0,0,$month,$day,date('y'));
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$text = date('j',$date1);
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours). ' <span>'.gm('hours').'</span>';
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days. ' <span>'.gm('days').'</span>';
					}
					$day++;
				}
				if($text == $day){
					$date1=mktime(0,0,0,$month,$day,$year);
					if(date('I',$now)=='1'){
						$date1+=3600;
					}
					$hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='0' ");
					$days = $this->db->field("SELECT SUM(hours) FROM task_time WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_status_rate='1' ");
					$expens = $this->db->query("SELECT SUM(amount) AS amount,unit_price FROM project_expenses
																INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
																WHERE user_id='".$in['user_id']."' AND date='".$date1."' AND project_id!=0 GROUP BY project_expenses.expense_id ");
					while ($expens->next()) {
						if($expens->f('unit_price')){
							$expenses += $expens->f('amount') * $expens->f('unit_price');
						}else{
							$expenses += $expens->f('amount');
						}
						$q++;
					}
					if(empty($hours)){
						$hours = '-';
					}else{
						$hours = number_as_hour($hours).' <span>'.gm('hours').'</span>';
						$w++;
					}
					if(empty($days)){
						$days = '-';
					}else{
						$days = $days.' <span>'.gm('days').'</span>';
						$w++;
					}
					$day++;
					$bg = '';
				}
				$temp_data=array(
					'text'		=> $text,
					'hours'		=> $hours,
					'days'		=> $days,
					'expens'		=> $expenses ? place_currency(display_number($expenses)) : '',
					'bg'			=> $bg,
					'last'		=> $k == 7 ? 'last' : '',
				);
				array_push($day_final['week'], $temp_data);
				$k++;
				if($day > $month_days){
					$loopweek = false;
				}
			}
			array_push($result['month'], $day_final);
			$i++;
		}

		if(!$in['tab']){
			$in['tab'] = 1;
		}

		$project_admin = $this->db_users->field("SELECT value FROM user_meta WHERE name='project_admin' AND user_id='".$_SESSION['u_id']."' ");
		$group = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."'");

		if($group == 1 || $project_admin == 1){
			$is_project_admin = 1;
		}
		$is_manager = false;
		if($_SESSION['access_level']==1 || $is_project_admin == 1){
			$is_manager = true;
		}

		$result['is_manager']		= $is_manager;
		$result['are_expenses']		= $q>0? true : false;
		$result['is_time_data']		= $w>0? true : false;
		$result['tab']		= $in['tab'];

		$result['languages_pdf']=array();
		//print languages
		$l = 0;
		$pim_lang = $this->db->query("SELECT * FROM pim_lang WHERE active=1 ORDER BY sort_order ");
		while($pim_lang->move_next()){
			// if($pim_lang->f('default_lang')){
		 		$default_lang = $pim_lang->f('lang_id');
		 	// }
		  	$temp_lang=array(
				'language'	        	=> gm($pim_lang->f('language')),
				'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$time_start."&time_end=".$time_end."&user_id=".$in['user_id'].'&lid='.$pim_lang->f('lang_id').'&timesheet_only='.$timesheet_only,
			);
			$l++;
			array_push($result['languages_pdf'], $temp_lang);
		}
		$custom_lang=$this->db->query("SELECT * from pim_custom_lang where active=1 ORDER BY sort_order ");

		while($custom_lang->move_next()){
		 	$this->db->query("SELECT * FROM label_language_time WHERE lang_code='".$custom_lang->f('lang_id')."'");
		 	if($this->db->move_next())
		 	{
			 	$temp_c_lang=array(
					'language'	        	=> $this->db->f('name'),
					'pdf_link_loop'         => "index.php?do=misc-timesheet_print&time_start=".$time_start."&time_end=".$time_end."&user_id=".$in['user_id'].'&lid='.$this->db->f('label_language_id').'&timesheet_only='.$timesheet_only,
				);
				$l++;
				array_push($result['languages_pdf'], $temp_c_lang);
			}
		}

		$this->out = $result;
	}

	public function get_Day(){
		$in=$this->in;
		$result=array();

		if($in['start_date']){
			$result['start_date']=strtotime($in['start_date'])*1000;
			$now			= strtotime($in['start_date']);
		}else{
			$now 				= time();
		}

		$today_of_week= date("N", $now);
		$day=date('j',$now);
		$month= date('n', $now);
		$year= date('Y', $now);
		$start_day=mktime(0,0,0,$month,$day,$year);
		if(date('I',$now)=='1'){
			$start_day+=3600;
		}
		$end_day=$start_day+86399;
		$week_start= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		if(date('I',$now)=='1'){
			$week_start+=3600;
		}
		$week_end= $week_start + 604799;

		$result['day_txt']=date(ACCOUNT_DATE_FORMAT,$end_day-7200);

		$user_dd = $this->build_user_dd_timetracker($in['user_id'], $_SESSION['access_level']);

		$result['user_dd']		= $user_dd;
		$result['user_id']		= $in['user_id'];
		$result['date']			= $start_day;

		$result['day_row']=array();
		for($i = $week_start; $i <= $week_end; $i += 86400)
		{
			$day_id = 'tmp_day'.$j;
			$d_row=array(
				'day_name'    	=> gm(date('D',$i)),
				'date_month'  	=> date('d',$i).' '.gm(date('M',$i)),
				'TIMESTAMP'   	=> $i,
				'TD_ID'       	=> $day_id,
				'NO_MARGIN'		=> $j == 7 ? 'no-margin' : '',
				'TODAY'		=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? 'today' : '',
				'HIDE_ARROW'	=> date('D') == date('D',$i) && ($week_nr == date('W')) && ($year_nr == date('Y')) ? '' : 'hide',
				'end_day'		=> date("N",$i)>5 ? true : false
			);
			array_push($result['day_row'], $d_row);
		    $j++;
		}

		$order_by = " ORDER BY id ASC  ";
		$order_by_array = array('start_time');
		if($in['order_by']){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == true){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}
		$in['only_h']=true;
		$in['tasks_ids']='';
		//$in['projects_ids']='';
		$task_time_data=$this->db->query("SELECT task_id,project_id FROM task_time WHERE date BETWEEN '".$start_day."' AND '".$end_day."' AND project_status_rate='0' AND user_id='".$in['user_id']."' GROUP BY task_id");
		while($task_time_data->next()){
			$in['tasks_ids'].=$task_time_data->f('task_id').',';
			/*if(strpos($in['projects_ids'],$task_time_data->f('project_id')) === false){
				$in['projects_ids'].=$task_time_data->f('project_id').',';
			}*/	
		}
		$in['tasks_ids']=rtrim($in['tasks_ids'],",");
		//$in['projects_ids']=rtrim($in['projects_ids'],",");
		$result['DISPLAY_SAVE']=true;
		$result['rows']=array();
		if(!empty($in['tasks_ids'])){
			$entries=array();
			$entries=explode(",",$in['tasks_ids']);
			foreach($entries as $key=>$value){
				$entries_sheet=$this->db->field("SELECT COUNT(id) FROM servicing_support_sheet WHERE task_id='".$value."' AND date BETWEEN '".$start_day."' AND '".$end_day."' AND user_id='".$in['user_id']."'");
				$task_time_temp=$this->db->query("SELECT * FROM task_time WHERE task_id='".$value."' AND date BETWEEN '".$start_day."' AND '".$end_day."' AND user_id='".$in['user_id']."' ");	
				if($task_time_temp->f('hours')>0 && !$entries_sheet){
					$this->db->query("INSERT INTO servicing_support_sheet SET 
									task_id='".$value."',
									date='".$task_time_temp->f('date')."',
									user_id='".$in['user_id']."',
									project_id='".$task_time_temp->f('project_id')."',
									task_time_id='".$task_time_temp->f('task_time_id')."',
									total_hours='".$task_time_temp->f('hours')."' ");			
				}
			}
			$task_time_subdata=$this->db->query("SELECT servicing_support_sheet.*,task_time.billed,task_time.submited,task_time.approved,task_time.unapproved,projects.active,projects.company_name AS c_name,projects.name as project_name,projects.stage FROM servicing_support_sheet 
												INNER JOIN task_time ON servicing_support_sheet.task_time_id=task_time.task_time_id
												INNER JOIN projects ON task_time.project_id=projects.project_id
												WHERE servicing_support_sheet.project_id!='0' AND servicing_support_sheet.task_id IN (".$in['tasks_ids'].") AND servicing_support_sheet.date BETWEEN '".$start_day."' AND '".$end_day."' AND servicing_support_sheet.user_id='".$in['user_id']."' ".$order_by." ");	
			while($task_time_subdata->next()){
				if($task_time_subdata->f('submited')==1){
					$result['DISPLAY_SAVE']=false;
				}
				$temp_data=array(
					'id'			=> $task_time_subdata->f('id'),
					'start_time'	=> number_as_hour($task_time_subdata->f('start_time')),
					'end_time'		=> number_as_hour($task_time_subdata->f('end_time')),
					'break'			=> number_as_hour($task_time_subdata->f('break')),
					'project_id'	=> $task_time_subdata->f('project_id'),
					'task_id'		=> $task_time_subdata->f('task_id'),
					'task_time_id'	=> $task_time_subdata->f('task_time_id'),
					'HIDE_UNBILLED'	=> ($task_time_subdata->f('billed') || $task_time_subdata->f('active')==0)? true : false,
					'HIDE_SUBMITED'	=> $task_time_subdata->f('submited')==1 ? true : false,
					'PROJECT_CUSTOMER_NAME' 	=> $task_time_subdata->f('c_name'),
					'PROJECT_LIST_NAME' 		=> $task_time_subdata->f('project_name'),
					'TASK_LIST_NAME'    		=> $this->db->field("SELECT task_name FROM tasks WHERE task_id='".$task_time_subdata->f('task_id')."' "),
					'IS_BILLED'   	=> ($task_time_subdata->f('billed')==1 || $task_time_subdata->f('active')==0) ? true:false,
					'IS_APPROVED' 	=> $task_time_subdata->f('approved') == 1 ? true:false,
					'IS_SUBMITED' 	=> $task_time_subdata->f('submited') == 1 ? true:false,
					'IS_CLOSED'		=> ($task_time_subdata->f('stage')=='2' || $task_time_subdata->f('stage')=='0') ? true:false,
					'IS_APPROVED' 	=> $task_time_subdata->f('approved') == 1 ? true:false,
					'UNAPPROVED'	=> $task_time_subdata->f('unapproved')==1 ? true : false,
					'disabled'		=> $task_time_subdata->f('submited') ? true : false,
					'BILLED_IMG'	=> $task_time_subdata->f('billed')==1 ? true : false,
					'HIDE_BILLED'			=> ($task_time_subdata->f('billed') || $task_time_subdata->f('active')==0)? false : true,
					'HIDE_SUBMITED_L'		=> $task_time_subdata->f('submited') ==1 ? false : true,
					'line_h_wo'				=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? $task_time_subdata->f('total_hours') : $task_time_subdata->f('end_time')-$task_time_subdata->f('start_time')-$task_time_subdata->f('break'),
					'line_h'				=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? number_as_hour($task_time_subdata->f('total_hours')) : number_as_hour($task_time_subdata->f('end_time')-$task_time_subdata->f('start_time')-$task_time_subdata->f('break')),
					'is_total_h'			=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? true : false,
					'comment'			=> $task_time_subdata->f('notes'),
				);
				array_push($result['rows'], $temp_data);
			}
			
		}
		if(!$in['tab']){
			$in['tab'] = 1;
		}
		$result['tab']		= $in['tab'];
		$result['projects_list']=$this->get_projects_list($in);
		$result['customers_list']	= $this->get_cc_list($in);
		$result['tasks_list']=$this->get_tasks_list($in);
		$result['has_p']=in_array('3', perm::$allow_apps) ? true : false;
		//$result['tasks_ids']=$in['tasks_ids'];
		$this->out = $result;
	}

	public function get_projects_list(&$in){

		$q = strtolower($in["term"]);

		$filter ='';
		if($in['only_h']){
			$filter .=" AND projects.status_rate='0' ";
		}
		/*if($in['projects_ids'] && !empty($in['projects_ids'])){
			$filter .=" AND projects.project_id IN (".$in['projects_ids'].") ";
    }else */if($in['user_id']){
			$filter .=" AND project_user.user_id='".$in['user_id']."'";
			if($_SESSION['group'] != 'admin' && $in['user_id'] != $_SESSION['u_id']){
				$is_manager = $this->db->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");
				if(!empty($is_manager)){
					$filter .=" AND projects.project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
				}
			}
		}
		if($q){
			$filter .=" AND (projects.name LIKE '%".$q."%' OR projects.company_name LIKE '%".$q."%') ";
		}

		if($in['exp'] == 1){
			$filter .= " AND add_expense='1' ";
		}

		$this->db->query("SELECT projects.project_id,projects.name AS p_name,projects.active, projects.customer_id, projects.company_name AS c_name
		            FROM projects
		            INNER JOIN project_user ON project_user.project_id=projects.project_id
		            WHERE projects.active=1 AND projects.closed='0' AND project_user.active='1' AND projects.stage='1' $filter LIMIT 10
		          ");
		while($this->db->move_next()){
		  $projects[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('project_id');
		  $customer[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('customer_id');
		}

		$result = array();
		if($projects){
			foreach ($projects as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "c_id" => $customer[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "c_id" => $customer[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			//array_push($result, array("id"=>'0', "label"=>'No matches', "name" => gm('No matches')));
		}
		return $result;
	}

	public function get_tasks_list(&$in){

		$q = strtolower($in["term"]);

		$filter ='';

		if($q){
			$filter .=" AND tasks.task_name LIKE '%".$q."%'";
		}
		if($in['tasks_ids'] && !empty($in['tasks_ids'])){
			$filter.=" AND task_id IN (".$in['tasks_ids'].") ";
		}

		if($in['project_id']){
			$bil_type = $this->db->field("SELECT billable_type FROM projects WHERE project_id='".$in['project_id']."' ");
			if($bil_type == 7){
				$filter .= " AND default_task_id IN (SELECT func_id FROM user_function WHERE user_id='".$in['user_id']."' ) ";
			}

			$this->db->query("SELECT tasks.task_id,tasks.project_id,tasks.task_name,tasks.billable FROM tasks WHERE tasks.project_id='".$in["project_id"]."' AND closed='0' $filter");
			while($this->db->move_next()){
				$t_name = str_replace(array("\r\n", "\r"), " ", $this->db->f('task_name'));
				$tasks[$t_name]= $this->db->f('task_id');
				$billable[$t_name] = $this->db->f('billable');
			}
		}else if($in['c_id']){
			$task = $this->db->query("SELECT * FROM default_data WHERE default_main_id='0' AND type='task_no' AND active='1' ");
			while($task->next()){
				$default_fields = $this->db->field("SELECT value FROM default_data WHERE default_main_id='".$task->f('default_id')."' AND default_name='task_billable' ");
				$tasks[$task->f('default_name')]= $task->f('default_id');
				$billable[$task->f('default_name')] = $default_fields ? true : false;
			}
		}

		$result = array();
		if($tasks){
			foreach ($tasks as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "billable" => $billable[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "billable" => $billable[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}
		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_19' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timetracker_admin' ");
		if(($perm_admin || $perm_manager || $_SESSION['group'] == 'admin') && $in['c_id']){
			array_push($result,array('id'=>'99999999999','name'=>''));
		}
		return $result;
	} 

	public function get_cc_list(&$in){
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 5")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> $symbol,
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']
			);
		}
		if(in_array('1', perm::$allow_apps)){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999','value'=>''));
			}
		}	
		return $result;
	}

	function build_user_dd_timetracker($selected, $access){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    //$perm_admin = $db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_19' ");
    $perm_admin = $db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_19']);
    //$perm_manager = $db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='timetracker_admin' ");
    $perm_manager = $db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'timetracker_admin']);
    //$db->query("SELECT user_id, first_name, last_name,group_id FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ");
    $db->query("SELECT user_id, first_name, last_name,group_id FROM users WHERE database_name= :d AND user_id= :user_id  ",['d'=>DATABASE_NAME,'user_id'=>$_SESSION['u_id']]);
    $db->move_next();
    $array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
    $filter = ' 1=1 ';
    $group_id = $db->f('group_id');
    if($group_id != 2 || $perm_admin || $perm_manager){
        //$db->query("SELECT user_id, first_name, last_name FROM users WHERE database_name='".DATABASE_NAME."' AND user_id!='".$_SESSION['u_id']."' AND active='1' ");
        $db->query("SELECT user_id, first_name, last_name FROM users WHERE database_name= :d AND user_id!= :user_id AND active= :active ",['d'=>DATABASE_NAME,'user_id'=>$_SESSION['u_id'],'active'=>'1']);
        while ($db->move_next()) {
        	// Admin
        	if($group_id == 1) {
        		$array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
        	// Has only user rights and must see only itself	
        	} else if($group_id != 2 && !$perm_admin && !$perm_manager){
        		continue;
        	//Has manager / admin rights	
        	} else if($group_id != 2 || $perm_admin || $perm_manager){
        		$array[$db->f('user_id')] = $db->f('first_name')." ".$db->f('last_name');
        	}
            
        }
    }

    asort($array);
    $out=array();
    foreach($array as $key => $value){
        array_push($out, array('id'=>$key,'name'=>htmlspecialchars_decode(stripslashes($value))));
    }
    return $out;
}

	public function get_articles_list(&$in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id ';
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id';

			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference';
		}

		$filter.=" 1=1 ";
		if($in['only_service']){
			$filter.=" AND pim_articles.is_service='1' ";
		}else{
			$filter.=" AND pim_articles.is_service='0' ";
		}

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id']){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}

		$articles= array( 'lines' => array());

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
				if($vat_regime==2){
					$vat=0;
				}
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	'name'						=> htmlspecialchars_decode($article->f('internal_name')),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'ALLOW_STOCK'		=> ALLOW_STOCK==1 ? true : false,
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				'price'						=> $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)), 
				'base_price'				=> place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
				'supplier_reference'	   => $article->f('supplier_reference'),
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		return $articles;
	}

}

	$timesheet = new Timesheet($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $timesheet->output($timesheet->$fname($in));
	}

	$timesheet->get_Week();
	$timesheet->output();

?>