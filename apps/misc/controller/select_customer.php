<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array('logo'=>array());
$time_start = $in['time_start'];
$time_end = $in['time_end'];
$user_id = $in['user_id'];
$result["customer_dd"]=build_timesheet_customer_dd(0,$time_start,$time_end,$user_id);
$result['customer_id']="0";
$default_logo = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_TIMESHEET' ");

if($default_logo == '')   {
	$temp_logo=array(
		'account_logo' => 'images/no-logo.png',
	);
	array_push($result['logo'], $temp_logo);
}
else {
	$patt = '{t_logo_img_}';
	$search = strpos(ACCOUNT_LOGO_TIMESHEET,'t_logo_img_');
	if($search === false){
		$patt = '{'.str_replace('upload/'.DATABASE_NAME.'/','',ACCOUNT_LOGO_TIMESHEET).',t_logo_img_}';
	}
	$logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/'.$patt.'*',GLOB_BRACE);
	foreach ($logos as $v) {
		$temp_logo=array();
		$logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
		$size = getimagesize($logo);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$temp_logo['attr']='width="250"';
		}else{
			$temp_logo['attr']='height="77"';
		}
		$temp_logo['account_logo']	= $logo;
		$temp_logo['default' ]		= $logo == $default_logo ? true : false;
		array_push($result['logo'], $temp_logo);
	}
}
$selected = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_TIMESHEET_PDF_FORMAT' ");
$result['pdf_layout']=$selected;
$result['pdf_type_dd']= build_pdf_type_dd($selected,false,false,true);

return json_out($result);