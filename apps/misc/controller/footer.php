<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}


	function get_foot($in,$showin=true,$exit=true){
		$db = new sqldb();
		if($_SESSION['l']=='en'){
			$privacy='https://akti.com/privacy-policy/';
		}elseif($_SESSION['l']=='fr'){
			$privacy='https://akti.com/fr/politique-de-confidentialite/';
		}elseif($_SESSION['l']=='nl'){
			$privacy='https://akti.com/nl/privacybeleid/';
		}elseif($_SESSION['l']=='de'){
			$privacy='https://akti.com/privacy-policy/';
		}
		
		$data=array('footer'=>array());
		$data['footer']['privacy']=$privacy;
		if($_SESSION['u_id']){
			global $database_config;
			$database_users = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
			);
			$db_u = new sqldb($database_users);
			$db = new sqldb();

			$user_info = $db_u->query("SELECT users.payed,users.active,user_info.end_date,user_info.is_trial,users.default_admin FROM users 
								JOIN user_info ON users.user_id = user_info.user_id	
								WHERE users.user_id= :user_id ",['user_id'=>$_SESSION['u_id']])
						->getAll();						
			$user_info = $user_info[0];					

			$user_type = 3;
			if($user_info['is_trial'] == 1){
				$user_type = 4;
				if ($user_info['payed'] == 2) {
					$user_type = 5;
				}
			}
			if($user_info['active'] == 2){
				$user_type = 5;
			}
			if($user_info['active'] == 0){
				$user_type = 6;
			}

			$days_left = 0;
			$is_trial =  $user_type == 3 ? true : false;
			if($is_trial){
				$days_left = ceil(($user_info['end_date'] - time())/(24*60*60));
			}
			if($days_left < 0){
				$days_left = 0;
			}

			$wizzard_complete = $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");
			$data['footer']['is_trial']=$is_trial;
			$data['footer']['wizzard_complete']=$wizzard_complete? true : false;
			$data['footer']['days_left']=$days_left;
			$data['footer']['HIDE_PAYMENT']=$user_info['default_admin'] == 1 ? true : false;

			$is_new_plan = $db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION'");
			if(is_null($is_new_plan)){
				$data['footer']['plan_go']='subscription_new';
			}else{
				$data['footer']['plan_go']='subscription_new';
			}

		}
		return json_out($data, $showin,$exit);

	}



  
	$result = array(
		'foot'		=> get_foot($in,true,false),
	);


json_out($result);

