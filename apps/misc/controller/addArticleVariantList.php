
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$l_r =10;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$def_lang = DEFAULT_LANG_ID;
if($in['lang_id']){
	$def_lang= $in['lang_id'];
}
if($in['lang_id']>=1000) {
	$def_lang = DEFAULT_LANG_ID;
}

switch ($def_lang) {
	case '1':
		$text = gm('Name');
		break;
	case '2':
		$text = gm('Name fr');
		break;
	case '3':
		$text = gm('Name du');
		break;
	default:
		$text = gm('Name');
		break;
}

if(!$in['cat_id'] && $in['customer_id']){
    $in['cat_id'] = $db->field('SELECT cat_id FROM customers WHERE customer_id ='.$in['customer_id']);
  }

$cat_id = $in['cat_id'];

$filter=" 1=1 AND pim_articles.is_service='0' AND pim_articles.has_variants='0' AND pim_article_variants.article_id IS NULL ";

	$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id  AND pim_article_prices.base_price=1
  LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id			  
						   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
					 AND pim_articles_lang.lang_id=\''.$def_lang.'\'	   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id 
					  LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id';
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
				pim_article_prices.price, pim_articles.internal_name,
				pim_article_brands.name AS article_brand,
				pim_articles_lang.description AS description,
				pim_article_categories.name AS categorie,
				pim_articles_lang.name2 AS item_name2,
				pim_articles_lang.name AS item_name,
				pim_articles_lang.lang_id,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference,
				pim_article_categories.name AS article_category,
				pim_articles.weight';


if ($in['search'])
{

	$search_words = explode(" ", $in['search']);
    $query_string2 ='';
    $all_tcl=1;
    $tcl=0;
    $len = count($search_words);

    for ($s = 0; $s < $len; $s++) {
        if ($search_words[$s]) {

            if(strlen($search_words[$s])>3){
              $all_tcl=0;
            }else{
              $tcl=1;
            }
              $query_string4 .= "+".$search_words[$s]."* "; 
              $query_string5 .= "+".$search_words[$s]." "; 
              if($s==0){
                $query_string6 .= "+".$search_words[$s]." "; 
              }else{
                $query_string6 .= "+".$search_words[$s]."* ";
              }
        }
    }
    $query_string7 =SUBSTR($query_string4, 0, STRLEN($query_string4) - 2);

    if($all_tcl || $tcl ){
        $size = $len - 1;

        if($size){
            $perm = range(0, $size);
            $j = 0;

            do { 
                 foreach ($perm as $i) { $perms[$j][] = $search_words[$i]; }
            } while ($perm = pc_next_permutation($perm, $size) and ++$j);

            foreach ($perms as $p) {
                $query_string1 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.item_code ";
                $query_string2 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.internal_name ";
                $query_string3 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.supplier_reference ";
                $query_string33 .= "LIKE '%" . join(' ', $p) . "%' OR pim_article_categories.name ";
            }

            $query_string1 = SUBSTR($query_string1, 0, STRLEN($query_string1) - 27);
            $query_string2 = SUBSTR($query_string2, 0, STRLEN($query_string2) - 31);
            $query_string3 = SUBSTR($query_string3, 0, STRLEN($query_string3) - 36);
            $query_string33 = SUBSTR($query_string33, 0, STRLEN($query_string33) - 32);

        }else{
                $query_string1 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string2 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string3 .= "LIKE '%" . $search_words[0]. "%' ";
                $query_string33 .= "LIKE '%" . $search_words[0]. "%' ";

        }

        $query_string8 =SUBSTR($query_string8, 0, STRLEN($query_string8) - 31);
        $query_string9 =SUBSTR($query_string9, 0, STRLEN($query_string9) - 27);
        $query_string10 =SUBSTR($query_string10, 0, STRLEN($query_string10) - 36);
        $query_string11 =SUBSTR($query_string11, 0, STRLEN($query_string11) - 32);
        
    }
       
    if ($len>1){
      if($all_tcl || $tcl){ 
          $filter .= " AND ( pim_articles.item_code ".$query_string1."  OR pim_articles.internal_name ".$query_string2." OR pim_articles.supplier_reference ".$query_string3." OR pim_article_categories.name ".$query_string33." ) ";     
      }else{
        $filter .= " AND (match(pim_articles.internal_name) against ('".$query_string4."' in boolean mode)
                    OR match(pim_articles.internal_name) against ('".$query_string5."' in boolean mode)
                    OR match(pim_articles.internal_name) against ('".$query_string6."' in boolean mode)   
                    OR match(pim_articles.internal_name) against ('".$query_string7."' in boolean mode)    
                    OR match(pim_articles.item_code) against ('".$query_string4."' in boolean mode)
                    OR match(pim_articles.supplier_reference) against ('".$query_string4."' in boolean mode) 
                    OR match(pim_article_categories.name) against ('".$query_string4."' in boolean mode)
                    OR match(pim_article_categories.name) against ('".$query_string5."' in boolean mode)
                    OR match(pim_article_categories.name) against ('".$query_string6."' in boolean mode)   
                    OR match(pim_article_categories.name) against ('".$query_string7."' in boolean mode)  
                           )";
      }
       
    }else{
        $filter .= " AND (pim_articles.internal_name LIKE '%".rtrim(ltrim($in['search']))."%'
                      OR pim_articles.item_code LIKE '%".rtrim(ltrim($in['search']))."%'
                      OR pim_articles.supplier_reference like '%" . rtrim(ltrim($in['search'])) . "%'
                      OR pim_article_categories.name like '%" . rtrim(ltrim($in['search'])) . "%' )"; 
    }
}


if($in['article_category_id']){
    $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
}

$articles= array( 'lines' => array());
$articles['max_rows']= (int)$db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");


$article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' GROUP BY pim_articles.article_id ORDER BY pim_articles.item_code  LIMIT ".$offset*$l_r.",".$l_r);


//Get  Quotes / Orders / Purchase Orders / Invoice Modules article fields settings
$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='ORDER_FIELD_LABEL'");

if($in['is_purchase_order'] || ($in['app'] == 'po_order')){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='P_ORDER_FIELD_LABEL'");
}

if($in['app'] == 'quote'){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");	
}
if($in['app'] == 'invoice'){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='INVOICE_FIELD_LABEL'");
}

if($in['app'] == 'contract'){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='CONTRACT_FIELD_LABEL'");
}

$time = time();

$j=0;
while($article->next()){
	$vat = $db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");

	if($in['customer_id']){
		if($in['vat_regime_id']){
			if($in['vat_regime_id']<10000){
				if($in['vat_regime_id']==2){
					$vat=0;
				}
			}else{
				$vat_regime=$db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
					WHERE vat_new.id='".$in['vat_regime_id']."'");
				if(!$vat_regime){
					$vat_regime=0;
				}
				if($vat>$vat_regime){
					$vat=$vat_regime;
				}
			}			
		}			
	}

	$values = $article->next_array();

	//Purchase orders Weight Setting
	if($in['app'] == 'po_order'){
		$values['weight'] = display_number($values['weight']);	
	}
	
	$tags = array_map(function($field){
		return '/\[\!'.strtoupper($field).'\!\]/';
	},array_keys($values));
	

	$label = preg_replace($tags, $values, $fieldFormat);
	
	if($article->f('price_type')==1){

	    $price_value_custom_fam=$db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

        $pim_article_price_category_custom=$db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
            $price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

        }else{
       	   	$price_value=$price_value_custom_fam;

         	 //we have to apply to the base price the category spec
    	 	$cat_price_type=$db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
    	    $cat_type=$db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
    	    $price_value_type=$db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

    	    if($cat_price_type==2){
                $article_base_price=get_article_calc_price($article->f('article_id'),3);
            }else{
                $article_base_price=get_article_calc_price($article->f('article_id'),1);
            }

       		switch ($cat_type) {
				case 1:                  //discount
					if($price_value_type==1){  // %
						$price = $article_base_price - $price_value * $article_base_price / 100;
					}else{ //fix
						$price = $article_base_price - $price_value;
					}
					break;
				case 2:                 //profit margin
					if($price_value_type==1){  // %
						$price = $article_base_price + $price_value * $article_base_price / 100;
					}else{ //fix
						$price =$article_base_price + $price_value;
					}
					break;
			}
        }

	    if(!$price || $article->f('block_discount')==1 ){
        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
        }
    }else{
    	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
        if(!$price || $article->f('block_discount')==1 ){
        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
        }
    }

    $pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
  	$base_price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

    $start= mktime(0, 0, 0);
    $end= mktime(23, 59, 59);
    $promo_price=$db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
    if($promo_price->move_next()){
    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

        }else{
            $price=$promo_price->f('price');
            $base_price = $price;
        }
    }
 	if($in['customer_id']){
  		$customer_custom_article_price=$db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['customer_id']."'");
    	if($customer_custom_article_price->move_next()){

            $price = $customer_custom_article_price->f('price');

            $base_price = $price;
       	}
   	}

	if($in['app']=='po_order'){
		// $view_list->assign("is_purchase_order",true);
		$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");
		//if($purchase_price > 0){
		$base_price = $purchase_price;
		$price = $purchase_price;
		//}
	}
    // $view_list->assign("is_purchase_order",false);

    $ant_stock=0;
  
   
	//items on purchase
	    $items_order=$db->field("SELECT SUM(pim_p_order_articles.quantity) 
		                      FROM  pim_p_order_articles 
		                      INNER JOIN pim_p_orders ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
		                      WHERE pim_p_order_articles.article_id='".$article->f('article_id')."'
		                      AND pim_p_orders.rdy_invoice!=0");

	    $items_received=$db->field("SELECT SUM(pim_p_orders_delivery.quantity) 
		                         FROM   pim_p_orders_delivery 
		                         INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
		                         WHERE pim_p_order_articles.article_id='".$article->f('article_id')."'");
	//items on orders	
	     $items_on_order=$db->field("SELECT SUM(pim_order_articles.quantity) 
		                      FROM  pim_order_articles 
		                      INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
		                      WHERE pim_order_articles.article_id='".$article->f('article_id')."'
		                      AND pim_orders.sent=1");

	    $items_on_received=$db->field("SELECT SUM(pim_orders_delivery.quantity) 
		                         FROM   pim_orders_delivery 
		                         INNER JOIN pim_order_articles ON pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
		                         WHERE pim_order_articles.article_id='".$article->f('article_id')."'");

	//items on project
	     $items_on_project=$db->field("SELECT SUM(project_articles.quantity) 
		                      FROM  project_articles 
		                      INNER JOIN  projects ON  projects.project_id=project_articles.project_id
		                      WHERE project_articles.article_id='".$article->f('article_id')."'
		                      AND projects.stage!=0 AND project_articles.delivered=0");

	//items on intervetions
	     $items_on_intervetion=$db->field("SELECT SUM( servicing_support_articles.quantity) 
		                      FROM   servicing_support_articles 
		                      INNER JOIN  servicing_support ON  servicing_support.service_id=servicing_support_articles.service_id
		                      WHERE servicing_support_articles.article_id='".$article->f('article_id')."'
		                      AND servicing_support.status!=0 AND servicing_support_articles.article_delivered=0");

	$ant_stock=$article->f('stock')-($items_on_order-$items_on_received+$items_on_project+$items_on_intervetion ) + ($items_order-$items_received);

	$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");
	$vat_customer_id = $db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['customer_id']."'");
	$vat_customer = $db->field("SELECT value FROM vats WHERE vat_id='".$vat_customer_id."'");
	$customer_disc = $db->query("SELECT line_discount, apply_line_disc FROM customers WHERE customer_id='".$in['customer_id']."'");

	$linie = array(
	  	'article_id'				=> $article->f('article_id'),
      'code'                => $article->f('item_code'),
      'name'            => htmlspecialchars_decode($article->f('internal_name')),
      'article_category'      => $article->f('article_category'),
      'name2'           => $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
      'base_price'        => place_currency(display_number_var_dec($base_price)),
      'stock2'          => $article->f('hide_stock')==1? '' : remove_zero_decimals($article->f('stock')),
      'ant_stock'         => $article->f('hide_stock')==1? '' : '('.remove_zero_decimals($ant_stock).')',
	  	'supplier_reference'    => $article->f('supplier_reference'),
	);
	array_push($articles['lines'], $linie);
}

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$articles['families']=array();
while($db->move_next()){
    $families = array(
    'name'  => $db->f('name'),
    'id'=> $db->f('id')
    ); 
    array_push($articles['families'], $families);
}

$articles['parent_article_id']=$in['parent_article_id'];
$articles['lang_id'] 				= $in['lang_id'];
$articles['lr']			        = $l_r;
$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;
$articles['remove_vat'] 		= $in['remove_vat'];
$articles['vat_regime_id']	= $in['vat_regime_id'];
$articles['offset']         = $offset;
$articles['article_category_id']         = $in['article_category_id'];

json_out($articles);