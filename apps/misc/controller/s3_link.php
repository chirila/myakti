<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
session_write_close();

if(!function_exists('order_array')){
	function order_array($a, $b){
		return strcmp($a['name'], $b['name']);
	}
}

global $config;
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$result=array('dropbox_files'=>array(),'dropbox_images'=>array(),'signature'=>array());
	$db = new sqldb();
    $expire = time() + (60*60*24*365);
    if($in['s3_folder']){
		$info=$db->query("SELECT * FROM amazon_files WHERE master='".$in['s3_folder']."' AND parent_id='".$in['id']."' ");
        while($info->next()){
            if (defined('AWS_FACTORY_KEY') && strpos($info->f('link'), AWS_FACTORY_KEY) === false) {
                $link = $a->getLink($config['awsBucket'].DATABASE_NAME.'/'.$info->f('item'));
                $db->query("UPDATE amazon_files SET `link`='".$link."', `expire`='".$expire."' WHERE id='".$info->f('id')."' ");
            } else {
                if($info->f('expire') > (time()+60*60*24*3)){
                    $link=!$info->f('is_url') ? $info->f('link') : '';
                }else{
                    if(!$info->f('is_url')){
                        $link = $a->getLink($config['awsBucket'].DATABASE_NAME.'/'.$info->f('item'));
                        $db->query("UPDATE amazon_files SET `link`='".$link."',`expire`='".$expire."' WHERE id='".$info->f('id')."' ");
                    }else{
                        $link=$info->f('link');
                    }
                }
            }
			$item=array(
				'id'				=> $info->f('id'),		
				'url'				=> $link,
				'name'				=> htmlspecialchars_decode(stripslashes($info->f('name'))),
				'delete_file_link'	=> array('do'=>'misc--misc-deleteS3File','id'=>$info->f('id')),
				'down_file_link'	=> $info->f('is_url') ? $link : 'index.php?do=misc-download_s3_file&id='.$info->f('id'),
				'item'				=> $info->f('item'),
				'file' 				=> htmlspecialchars_decode(stripslashes($info->f('name'))),
				'is_url'			=> $info->f('is_url') ? true : false
			);
			if($info->f('type')=='1'){
				//file
				array_push($result['dropbox_files'],$item);
			}else{
				//picture		
				if($info->f('is_signature')){
					array_push($result['signature'],$item);
				}else{
					array_push($result['dropbox_images'],$item);
				}
			}
		}
		$result['dropbox_is_file']		= true;
	}

	if(!empty($result['dropbox_files'])){
		usort($result['dropbox_files'], 'order_array'); 
	}
	if(!empty($result['dropbox_images'])){
		usort($result['dropbox_images'], 'order_array'); 
	}
	
	json_out($result);
?>