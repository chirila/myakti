<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	$db=new sqldb();
	$result=array(
		'add_customer'		=> true,
		'country_dd'		=> build_country_list(0),
		'country_id'		=> ACCOUNT_BILLING_COUNTRY_ID ? ACCOUNT_BILLING_COUNTRY_ID : ACCOUNT_DELIVERY_COUNTRY_ID,
		'gender_dd'			=> gender_dd(),
		'title_dd'			=> build_contact_title_type_dd(0),
		'title_id'			=> '0', 
		'gender_id'			=> '0',
		'language_dd'		=> build_language_dd(),
		'language_id'		=> '0',
		'do_next'			=> 'misc-cc_new-misc-tryAddC',
		'app'				=> 'misc',
		'item_id'			=> 'tmp',
		);
	if($in['buyer_id']){
		$result['buyer_id']=$in['buyer_id'];
		if($in['add_customer']){
			$result['buyer_name']=$in['name'];
		}else if($in['add_individual']){
			$result['buyer_name']=$in['lastname'].''.$in['firstname'];
		}
	}


	json_out($result);
?>