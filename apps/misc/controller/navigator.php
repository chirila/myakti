<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class Navigator extends Controller{

	var $item_id = '';
	var $table = '';

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Data(){
		//session_write_close();
		switch($this->in['original_page']){
			case 'customerView':
			case 'customer':
				$this->item_id ='customer_id';
				$this->item_name ='customer_id';
				$this->table ='customers';
				$this->get_itemsData();
				break;
			case 'contactView':
			case 'contact':
				$this->item_id ='contact_id';
				$this->item_name ='contact_id';
				$this->table ='customer_contacts';
				$this->get_itemsData();
				break;
			case 'article.add':
				$this->item_id ='article_id';
				$this->item_name ='article_id';
				$this->table ='pim_articles';
				$this->get_itemsData();
				//$this->get_articleData();
				break;
			case 'service.add':
				$this->item_id ='article_id';
				$this->item_name ='article_id';
				$this->table ='pim_articles';
				$this->get_itemsData();
				//$this->get_serviceData();
				break;
			case 'quote.view':
				$this->item_id ='id';
				$this->item_name ='quote_id';
				$this->table ='tblquote';
				$this->get_itemsData();
				//$this->get_quoteData();
				break;
			case 'order.view':
				$this->item_id ='order_id';
				$this->item_name ='order_id';
				$this->table ='pim_orders';
				$this->get_itemsData();
				//$this->get_orderData();
				break;
			case 'purchase_order.view':
				$this->item_id ='p_order_id';
				$this->item_name ='p_order_id';
				$this->table ='pim_p_orders';
				$this->get_itemsData();
				//$this->get_porderData();
				break;
			case 'service.edit':
			case 'service.view':
				$this->item_id ='service_id';
				$this->item_name ='service_id';
				$this->table ='servicing_support';
				$this->get_itemsData();
				//$this->get_interventionData();
				break;
			case 'project.edit':
				$this->item_id ='project_id';
				$this->item_name ='project_id';
				$this->table ='projects';
				$this->get_itemsData();
				//$this->get_projectData();
				break;
			case 'installation.view':
				$this->item_id ='id';
				$this->item_name ='installation_id';
				$this->table ='installations';
				$this->get_itemsData();
				//$this->get_installationData();
				break;
			case 'contract.view':
				$this->item_id ='contract_id';
				$this->item_name ='contract_id';
				$this->table ='contracts';
				$this->get_itemsData();
				//$this->get_contractData();
				break;
			case 'invoice.view':
				$this->item_id ='id';
				$this->item_name ='invoice_id';
				$this->table ='tblinvoice';
				$this->get_itemsData();
				//$this->get_invoiceData();
				break;
			case 'recinvoice.edit':
			case 'recinvoice.view':
				$this->item_id ='recurring_invoice_id';
				$this->item_name ='recurring_invoice_id';
				$this->table ='recurring_invoice';
				$this->get_itemsData();
				//$this->get_recinvoiceData();
				break;
			case 'purchase_invoice.view':
				$this->item_id ='invoice_id';
				$this->item_name ='invoice_id';
				$this->table ='tblinvoice_incomming';
				$this->get_itemsData();
				//$this->get_purchaseinvoiceData();
				break;
			case 'dealView':
				$this->item_id ='opportunity_id';
				$this->item_name ='opportunity_id';
				$this->table ='tblopportunity';
				$this->get_itemsData();
				//$this->get_dealData();
				break;
			case 'timesheetView':
				$this->get_timesheetData();
				break;
			case 'user.edit':
				$this->item_id ='user_id';
				$this->item_name ='user_id';
				$this->table ='users';
				$this->get_itemsData();
				break;
		}
	}

	private function get_itemsData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		$l_r =ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		if($this->table == 'users'){
			 $id_exist=$this->db_users->field("SELECT ".$this->item_id." FROM ".$this->table." WHERE ".$this->item_id."=:user_id AND users.database_name= :d ",['user_id'=>$in['original_id'],'d'=>DATABASE_NAME]);
		}else{			 
			$id_exist=$this->db->field("SELECT ".$this->item_id." FROM ".$this->table." WHERE ".$this->item_id."='".$in['original_id']."' ");
		}
	
			if(!empty($in['nav_list']) && isset($in['nav_list']) && !is_null($in['nav_list']) ){
				if($this->item_id =='opportunity_id'){
					$result['stage_name']=stripslashes($this->db->field("SELECT tblopportunity_stage.name FROM tblopportunity
				INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
				WHERE opportunity_id='".$in['original_id']."' "));
				}
			
				$result['total_items']=count($in['nav_list']);	

				if(is_null($in['cindex'])){
					$result['start_item'] = array_search($in['original_id'], $in['nav_list'])+1;
					if($result['start_item']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $in['nav_list'][$result['start_item']-2];
					}
					$next_item = $in['nav_list'][$result['start_item']];
				}else{
					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($result['start_item']<2 ){
						$result['start_item'] = 1;
						$prev_item =1;
						$next_item = $in['nav_list'][1];
					}else{
						
						$prev_item = $in['nav_list'][$result['start_item']-2];
						$next_item = $in['nav_list'][$result['start_item']];
					}
					
					
				}


			}	

		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']=$this->item_id;
		$this->out = $result;
	}

	private function get_customersData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		$l_r =ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		
		/*$order_by_array = array('name','city_name','country_name','c_type_name','acc_manager_name','serial_number_customer','comp_phone','c_email','our_reference','language');

		if($in['active']==1){
			$filter.= " AND active=1";
		}

		$filter = " is_admin='0' AND front_register=0 ";
		if(empty($in['archived'])){
			$filter.= " AND active=1";
		}else{
			$in['archived'] = 1;
			$filter.= " AND active=0";
		}
		$admin_licence=aktiUser::get('user_type');

		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
			$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}

		$filter_c=' ';
		$order_by = " ORDER BY customers.name ";
		$ad_search_f = '';
		$escape_chars = array('(','?','*','+',')');
		if(!empty($in['search'])){
			$filter_x = '';
			$search_arr = str_split(stripslashes($in['search']));
			foreach ($search_arr as $value) {
				if(in_array($value, $escape_chars)){
					$value = '\\'.$value;
				}
				$filter_x .= $value."[^a-zA-Z0-9]?";
			}
			$filter .= " AND ( name regexp '".addslashes($filter_x)."' ) ";
		}
		if(!empty($in['acc_manager_name'])){
			$filter .= " AND ( acc_manager_name LIKE '%".$in['acc_manager_name']."%' ) ";
		}
		if(!empty($in['zip_from']) && empty($in['zip_to'])){
			$zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
			$filter .= " AND ( zip LIKE '%".$zip_from."%' ) ";
		}
		if(!empty($in['zip_to'])){
			$db->query("DROP FUNCTION IF EXISTS STRIP_NON_DIGIT;
		CREATE FUNCTION STRIP_NON_DIGIT(input VARCHAR(255))
		   RETURNS VARCHAR(255)
		BEGIN
		   DECLARE output   VARCHAR(255) DEFAULT '';
		   DECLARE iterator INT          DEFAULT 1;
		   WHILE iterator < (LENGTH(input) + 1) DO
		      IF SUBSTRING(input, iterator, 1) IN ( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ) THEN
		         SET output = CONCAT(output, SUBSTRING(input, iterator, 1));
		      END IF;
		      SET iterator = iterator + 1;
		   END WHILE;
		   RETURN output;
		END;");
			$zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
			$zip_to = preg_replace('/[^0-9]/', '', $in['zip_to']);
			if(empty($zip_from)){
				$zip_from = '0';
			}
			$filter .= " AND ( STRIP_NON_DIGIT(zip) BETWEEN '".$zip_from."' AND '".$zip_to."' ) ";
		}
		if(!empty($in['country_id'])){
			$filter .= " AND country_name = '".$in['country_id']."' ";
		}
		if(!empty($in['city_name'])){
			$filter .= " AND ( city_name LIKE '%".$in['city_name']."%' ) ";
		}
		if(!empty($in['our_reference'])){
			$filter .= " AND ( our_reference LIKE '%".$in['our_reference']."%' ) ";
		}

		if(!empty($in['btw_nr'])){
			$escape_chars2 = array('.',' ','/',',');
			$filter_y = '';
			$search_arr = str_split(stripslashes($in['btw_nr']));

			foreach ($search_arr as $value) {
				if(!in_array($value, $escape_chars2)){
					$filter_y .= $value."\.?";
				}

			}
			$filter .= " AND ( btw_nr regexp '".addslashes($filter_y)."' OR btw_nr='".$in['btw_nr']."') ";
		}
		if(!empty($in['commercial_name'])){
			$filter_x = '';
			$search_arr = str_split(stripslashes($in['commercial_name']));
			foreach ($search_arr as $value) {
				if($value == '('){
					$value = '\\(';
				}
				$filter_x .= $value.".?";
			}
			$filter .= " AND ( commercial_name regexp '".addslashes($filter_x)."' ) ";
		}

		if(!empty($in['lead_source'])){
			$filter .= " AND lead_source='".$in['lead_source']."' ";
		}

		if(!empty($in['sector'])){
			$filter .= " AND sector='".$in['sector']."' ";
		}

		if (!empty($in['c_type_name'])){
			$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
		}
		if (isset($in['type'])){
			switch ($in['type']) {
				case '1':
					$filter .= " AND customers.type='".$in['type']."'  ";
					break;
				case '2':
				    $filter .= " AND customers.type='0'  ";
				    break;
			}

		}
		if (isset($in['is_supplier'])){
			switch ($in['is_supplier']) {
				case '1':
					$filter .= " AND customers.is_supplier='".$in['is_supplier']."'  ";
					break;
				case '2':
					$filter .= " AND customers.is_customer='1'  ";
					break;
				case '3':
					$filter .= " AND customers.is_supplier='1' AND customers.is_customer='1' ";
					break;
				case '4':
					$filter .= " AND customers.is_supplier='' AND customers.is_customer='' ";
					break;
				default:
					# code...
					break;
			}

		}
		if(!empty($in['first']) && $in['first'] == '[0-9]'){
			$filter.=" AND SUBSTRING(LOWER(customers.name),1,1) BETWEEN '0' AND '9'";
		} elseif (!empty($in['first'])){
			$filter .= " AND customers.name LIKE '".$in['first']."%'  ";
		}

		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1' || $in['desc']=='true'){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}

		if(!empty($in['address'])){
			$filter .= " AND ( customer_addresses.address LIKE '%".$in['address']."%' ) ";
		}*/
		$id_exist=$this->db->field("SELECT customer_id FROM customers WHERE customer_id='".$in['original_id']."' "); 
		/*if($id_exist){*/
			/*$result['total_items'] =  $this->db->field("SELECT count(DISTINCT customers.customer_id) FROM customers
					LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
					WHERE ".$filter );*/

			if(!empty($in['nav_list']) && isset($in['nav_list']) && !is_null($in['nav_list']) ){
				/*$items =  $this->db->query("SELECT DISTINCT customers.customer_id FROM customers
					LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
					WHERE ".$filter.$order_by )->getAll();
				$in['nav_list']=array();
				foreach ($items as $key => $value) {
			    	array_push($in['nav_list'],$value['customer_id']);
				}
				$result['total_items']=count($items);*/
			
				$result['total_items']=count($in['nav_list']);	

				if(is_null($in['cindex'])){
					$result['start_item'] = array_search($in['original_id'], $in['nav_list'])+1;
					if($result['start_item']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $in['nav_list'][$result['start_item']-2];
					}
					$next_item = $in['nav_list'][$result['start_item']];
				}else{
					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($result['start_item']<2 ){
						$result['start_item'] = 1;
						$prev_item =1;
						$next_item = $in['nav_list'][1];
					}else{
						
						$prev_item = $in['nav_list'][$result['start_item']-2];
						$next_item = $in['nav_list'][$result['start_item']];
					}
					
					
				}

			/*if(is_null($in['cindex'])){
				$order_by = " ORDER BY customers.customer_id ";
				$filter_item=" AND customers.customer_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(DISTINCT customers.customer_id) FROM customers
						LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
						WHERE ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT customers.customer_id FROM customers
						LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
						WHERE ".$filter." GROUP BY customers.customer_id ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT customers.customer_id FROM customers
						LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
						WHERE ".$filter." GROUP BY customers.customer_id ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT customers.customer_id FROM customers
						LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
						WHERE ".$filter." GROUP BY customers.customer_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}		
				$next_item = $this->db->field("SELECT customers.customer_id FROM customers
						LEFT JOIN customer_addresses on customers.customer_id = customer_addresses.customer_id
						WHERE ".$filter." GROUP BY customers.customer_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}*/	
			}	
		/*}*/
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='customer_id';
		$this->out = $result;
	}

	private function get_contactsData(){
		$in=$this->in;

		$result=array('total_items'=>0,'start_item'=>1);

		$l_r =28;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$admin_licence=aktiUser::get('user_type');
		$order_by_array = array('lastname','firstname','cus_name','function');

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$result['view'] = 0;
		if(CUSTOMER_VIEW_LIST==1){
			$result['view'] = 1;
		}

		$filter = " customer_contacts.front_register = 0 ";
		if(empty($in['archived'])){
			$filter.= " AND customer_contacts.active=1";
		}else{
			$in['archived'] = 1;
			$filter.= " AND customer_contacts.active=0";
		}

		//FILTER LIST
		$order_by = " ORDER BY lastname ";
		$escape_chars = array('(','?','*','+',')');
		if(!empty($in['search'])){
			$filter_x = '';
			// $search_arr = str_split($in['search']);
			$search_arr = str_split(stripslashes($in['search']));
			foreach ($search_arr as $value) {
				if(in_array($value, $escape_chars)){
					$value = '\\'.$value;
				}
				$filter_x .= $value.".?";
			}

			$search_words = explode(' ', $in['search']);
			foreach ($search_words as $word_pos => $searched_word) {
				if(trim($searched_word)){
					
					if($word_pos==0){
						$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
									OR lastname LIKE '%".$searched_word."%'
									OR company_name LIKE '%".$searched_word."%'
									OR customer_contactsIds.phone LIKE '%".$searched_word."%'
									OR cell LIKE '%".$searched_word."%'
									OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
					}else{
						$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
									OR lastname LIKE '%".$searched_word."%'
									OR company_name LIKE '%".$searched_word."%'
									OR customer_contactsIds.phone LIKE '%".$searched_word."%'
									OR cell LIKE '%".$searched_word."%'
									OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
					}

				}
			}
		}

		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc']=='1' || $in['desc'] == 'true') {
					$order = " DESC ";
				}
				if($in['order_by']=='cus_name'){
					$in['order_by']='customers.name';
				}elseif($in['order_by']=='function'){
					$in['order_by']='customer_contactsIds.position';
				}else if($in['order_by']=='lastname' || $in['order_by']=='firstname'){
					$in['order_by']='customer_contacts.'.$in['order_by'];
				}else{

				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;			
			}
		}

		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			$id_exist=$this->db->field("SELECT contact_id FROM customer_contacts WHERE contact_id='".$in['original_id']."' ");
			if($id_exist){
				$result['total_items'] =  $this->db->field("SELECT COUNT( contact_id ) AS max_contact_rows 
									FROM customer_contacts
									LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter );
				if(is_null($in['cindex'])){
					$order_by = " ORDER BY customer_contacts.contact_id ";
					$filter_item=" AND customer_contacts.contact_id<'".$in['original_id']."' ";
					$result['start_item'] =  ($this->db->field("SELECT COUNT( contact_id ) AS max_contact_rows 
									FROM customer_contacts
									LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter.$filter_item.$order_by ))+1;
					if($result['start_item']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT customer_contacts.contact_id
						FROM customer_contacts
						LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
						LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
						LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
						LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
						WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".($result['start_item']-2).",1");
					}			
					$next_item = $this->db->field("SELECT customer_contacts.contact_id
						FROM customer_contacts
						LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
						LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
						LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
						LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
						WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$result['start_item'].",1");
				}else{
					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT customer_contacts.contact_id
							FROM customer_contacts
							LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
							LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
							LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
							LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
							WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
					}		
					$next_item = $this->db->field("SELECT customer_contacts.contact_id
						FROM customer_contacts
						LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
						LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
						LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
						LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
						WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}
			}
		}else{
			$id_exist=$this->db->field("SELECT contact_id FROM customer_contacts WHERE contact_id='".$in['original_id']."' ");
			if($id_exist){
				$result['total_items'] =  $this->db->field("SELECT count(customer_contacts.contact_id) FROM customer_contacts 
						LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND `primary`='1' AND customer_contactsIds.customer_id!='0'
						LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
						WHERE ".$filter );
				if(is_null($in['cindex'])){
					$order_by = " ORDER BY customer_contacts.contact_id ";
					$filter_item=" AND customer_contacts.contact_id<'".$in['original_id']."' ";
					$result['start_item'] =  ($this->db->field("SELECT count(customer_contacts.contact_id) FROM customer_contacts 
						LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND `primary`='1' AND customer_contactsIds.customer_id!='0'
						LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
						WHERE ".$filter.$filter_item.$order_by ))+1;
					if($result['start_item']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT customer_contacts.contact_id
					  		FROM customer_contacts
							LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
							LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
							LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
							WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".($result['start_item']-2).",1");
					}		
					$next_item = $this->db->field("SELECT customer_contacts.contact_id
				  		FROM customer_contacts
						LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
						LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
						LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
						LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
						WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$result['start_item'].",1");
				}else{
					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT customer_contacts.contact_id
					  		FROM customer_contacts
							LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
							LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
							LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
							WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");					
					}
					$next_item = $this->db->field("SELECT customer_contacts.contact_id
					  		FROM customer_contacts
							LEFT JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND  customer_contactsIds.customer_id!='0'
							LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							LEFT JOIN customer_contact_title ON customer_contact_title.id=customer_contactsIds.title
							LEFT JOIN customer_contact_job_title ON customer_contact_job_title.id=customer_contactsIds.position
							WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}
			}
		}
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='contact_id';

		$this->out = $result;
	}

	private function get_articleData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		$l_r =ROW_PER_PAGE;
		$order_by = " ORDER BY  pim_articles.item_code  ";

		$order_by_array = array('item_code','internal_name','article_category','price','total_price','customer_name','stock','stock_ant');

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		//FILTER LIST
		$selected =array();
		$filter = '1=1';
		$filter_attach = '';
		$filter.= " AND pim_articles.is_service='0' ";

		if($in['archived']==0){
		    $filter.= " AND pim_articles.active='1' ";
		}else{
		    $filter.= " AND pim_articles.active='0' ";
		}

		if(!empty($in['search'])){
		    $search_words = explode(" ", $in['search']);
		    $query_string ='';
		    $query_string2 ='';
		    $all_tcl=1;
		    $tcl=0;
		    $len = count($search_words);

		    for ($s = 0; $s < $len; $s++) {
		        if ($search_words[$s]) {
		            $query_string8 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.internal_name ";
		            $query_string9 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.item_code ";
		            $query_string10 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.origin_number ";

		            if(strlen($search_words[$s])>3){
		              $all_tcl=0;
		            }else{
		              $tcl=1;
		            }

		              $query_string4 .= "+".$search_words[$s]."* "; 
		              $query_string5 .= "+".$search_words[$s]." "; 
		              if($s==0){
		                $query_string6 .= "+".$search_words[$s]." "; 
		              }else{
		                $query_string6 .= "+".$search_words[$s]."* ";
		              }
		            
		        }
		    }
		    $query_string7 =SUBSTR($query_string4, 0, STRLEN($query_string4) - 2);

		    if($all_tcl || $tcl ){
		        $size = $len - 1;

		        if($size){
		            $perm = range(0, $size);
		            $j = 0;

		            do { 
		                 foreach ($perm as $i) { $perms[$j][] = $search_words[$i]; }
		            } while ($perm = pc_next_permutation($perm, $size) and ++$j);

		            foreach ($perms as $p) {
		                $query_string1 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.item_code ";
		                $query_string2 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.internal_name ";
		                $query_string3 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.origin_number ";
		            }

		            $query_string1 = SUBSTR($query_string1, 0, STRLEN($query_string1) - 27);
		            $query_string2 = SUBSTR($query_string2, 0, STRLEN($query_string2) - 31);
		            $query_string3 = SUBSTR($query_string3, 0, STRLEN($query_string3) - 31);

		        }else{
		                $query_string1 .= "LIKE '%" . $search_words[0] . "%' ";
		                $query_string2 .= "LIKE '%" . $search_words[0] . "%' ";
		                $query_string3 .= "LIKE '%" . $search_words[0]. "%' ";
		        }

		        $query_string8 =SUBSTR($query_string8, 0, STRLEN($query_string8) - 31);
		        $query_string9 =SUBSTR($query_string9, 0, STRLEN($query_string9) - 27);
		        $query_string10 =SUBSTR($query_string10, 0, STRLEN($query_string10) - 31);
		        
		    }

		    $query_string = SUBSTR($query_string, 0, STRLEN($query_string) - 31);
		        
		    if ($len>1){
		      if($all_tcl || $tcl){ 
		        if($tcl){
		          $filter .= " AND ( pim_articles.item_code ".$query_string9."  OR pim_articles.internal_name ".$query_string8." OR pim_articles.origin_number ".$query_string10." ) ";
		        }else{
		          $filter .= " AND ( pim_articles.item_code ".$query_string1."  OR pim_articles.internal_name ".$query_string2." OR pim_articles.origin_number ".$query_string3." ) ";
		        }
		        
		      }else{
		        $filter .= " AND (match(pim_articles.internal_name) against ('".$query_string4."' in boolean mode)
		                    OR match(pim_articles.internal_name) against ('".$query_string5."' in boolean mode)
		                    OR match(pim_articles.internal_name) against ('".$query_string6."' in boolean mode)   
		                    OR match(pim_articles.internal_name) against ('".$query_string7."' in boolean mode)    
		                    OR match(pim_articles.item_code) against ('".$query_string4."' in boolean mode)
		                    OR match(pim_articles.origin_number) against ('".$query_string4."' in boolean mode)            
		                           )";
		      }
		       
		    }else{
		        $filter .= " AND (pim_articles.internal_name LIKE '%".rtrim(ltrim($in['search']))."%'
		                      OR pim_articles.item_code LIKE '%".rtrim(ltrim($in['search']))."%'
		                      OR pim_articles.origin_number like '%" . rtrim(ltrim($in['search'])) . "%' )"; 
		    }
		}
		if(!empty($in['name'])){
		    $filter .= " AND ( customers.name LIKE '%".$in['name']."%') ";
		}
		if(!empty($in['supplier_reference'])){
		    $filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
		}
		if($in['supplier_id'] && !empty($in['supplier_id'])){
		    $filter .=" AND pim_articles.supplier_id='".$in['supplier_id']."'";
		}

		if($in['article_category_id']){
		    $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
		}
		if($in['first'] && $in['first'] == '[0-9]'){
		    $filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
		} elseif ($in['first']){
		    $filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
		}

		if($in['order_by']){
		    if(in_array($in['order_by'], $order_by_array)){
		    $order = " ASC ";
		    if($in['desc'] == '1' || $in['desc']=='true'){
		        $order = " DESC ";
		    }
		    if($in['order_by']=='customer_name'){
		        $in['order_by']='supplier_name';
		    }
		    if($in['order_by']=='stock_ant' || $in['order_by']=='total_price' ){

		    }else{
		       $order_by =" ORDER BY ".$in['order_by']." ".$order;
		    }
		    }
		}

		$id_exist=$this->db->field("SELECT article_id FROM pim_articles WHERE article_id='".$in['original_id']."' AND is_service='0' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(pim_articles.article_id)
		            FROM pim_articles
		            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
		        	WHERE $filter  ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY pim_articles.article_id ";
				$filter_item=" AND pim_articles.article_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(pim_articles.article_id)
					FROM pim_articles
					WHERE ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT pim_articles.article_id
			            FROM pim_articles
			            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
			            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
			            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id	            
			        	WHERE $filter ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT pim_articles.article_id
			            FROM pim_articles
			            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
			            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
			            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id	            
			        	WHERE $filter ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($in['order_by']=='stock_ant' || $in['order_by']=='total_price'){
					$filter_po = ' ';
					$filter_po2 = ' ';
					if(defined('P_ORDER_DELIVERY_STEPS') && P_ORDER_DELIVERY_STEPS == '2'){
					    $filter_po = ' INNER JOIN pim_p_order_deliveries ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id ';
					    $filter_po2 = " AND pim_p_order_deliveries.delivery_done ='1'  ";
					}
					$tmp_array=array();
				    $articles = $this->db->query("SELECT pim_articles.article_id, pim_articles.stock,pim_articles.price,vats.value AS vat_value
		            	FROM pim_articles
		            	LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
		            	LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
		            	LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id	            
		        		WHERE $filter ");
				    while($articles->next()){
				      $qty_orders_p=0;
				      $qty_ord_p = $this->db->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order, COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity ) as total_deliver , pim_p_orders_delivery.delivery_id, pim_p_order_articles.p_order_id
				          FROM pim_p_order_articles
				          LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
				          LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
				          $filter_po
				          WHERE pim_p_order_articles.article_id =  '".$articles->f('article_id')."' AND pim_p_orders.rdy_invoice!=0 ".$filter_po2."  GROUP BY pim_p_orders.p_order_id");

					    $total_deliver=0;
					    while($qty_ord_p->move_next()) {
					      if($qty_ord_p->f('nr_del')){
					            if($qty_ord_p->f('total_order')/$qty_ord_p->f('nr_del') < $qty_ord_p->f('total_deliver')){
					                $total_deliver = $qty_ord_p->f('total_order')/$qty_ord_p->f('nr_del');
					            }else{
					                $total_deliver = $qty_ord_p->f('total_deliver');
					            }

					         $qty_orders_p +=($qty_ord_p->f('total_order')/$qty_ord_p->f('nr_del') - $total_deliver); 
					        }else{
					          $total_deliver = $qty_ord_p->f('total_deliver');
					          $qty_orders_p +=($qty_ord_p->f('total_order') - $total_deliver); 
					        }
					        
					    }
					    $qty_diff_p=0;
					    $diff_p = $this->db->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order ,COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity ) as total_deliver , pim_p_orders_delivery.delivery_id, pim_p_order_articles.p_order_id
					        FROM pim_p_order_articles
					        LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
					        LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
					        WHERE pim_p_order_articles.article_id =  '".$articles->f('article_id')."' AND pim_p_orders.sent =1 AND pim_p_orders.rdy_invoice =  '1' GROUP BY pim_p_orders.p_order_id");	   
					    $total_deliver=0;
					    while($diff_p->move_next()) {
					       if($diff_p->f('nr_del')){
					            if($diff_p->f('total_order')/$diff_p->f('nr_del') < $diff_p->f('total_deliver')){
					                $total_deliver = $diff_p->f('total_order')/$diff_p->f('nr_del');
					            }else{
					                $total_deliver = $diff_p->f('total_deliver');
					            }
					            $qty_diff_p +=($diff_p->f('total_order')/$diff_p->f('nr_del') - $total_deliver); 
					       }else{
					            $total_deliver = $diff_p->f('total_deliver');
					            $qty_diff_p +=($diff_p->f('total_order')/$diff_p->f('nr_del') - $total_deliver); 
					       }		  
					    }

						$qty_orders=0;
						$qty_ord = $this->db->query("SELECT pim_order_articles.order_articles_id, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del , pim_order_articles.article_id, SUM( pim_orders_delivery.quantity ) as total_deliver , pim_orders_delivery.delivery_id, pim_order_articles.order_id
						        FROM pim_order_articles
						        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
						        LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
						        WHERE pim_order_articles.article_id =  '".$articles->f('article_id')."' AND pim_orders.sent =1 GROUP BY pim_orders.order_id");
						$total_deliver=0;
						while($qty_ord->move_next()) {
					        $nr_del = $qty_ord->f('nr_del');
					        if($nr_del==0) {
					          $nr_del = 1;
					        }
					        if($qty_ord->f('total_order')/$nr_del < $qty_ord->f('total_deliver')){
					            $total_deliver = $qty_ord->f('total_order')/$nr_del;
					        }else{
					            $total_deliver = $qty_ord->f('total_deliver');
					        }
					        $qty_orders +=($qty_ord->f('total_order')/$nr_del - $total_deliver);  
						}

						$qty_diff=0;
						$diff = $this->db->query("SELECT pim_order_articles.order_articles_id, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del, pim_order_articles.article_id, SUM( pim_orders_delivery.quantity ) as total_deliver , pim_orders_delivery.delivery_id, pim_orders_delivery.order_id
						        FROM pim_order_articles
						        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
						        LEFT JOIN pim_orders ON pim_orders_delivery.order_id = pim_orders.order_id
						        WHERE pim_order_articles.article_id =  '".$articles->f('article_id')."' AND pim_orders.sent =1 AND pim_orders.rdy_invoice =  '1' GROUP BY pim_orders.order_id");
						$total_deliver=0;
						while($diff->move_next()) {
					        if($diff->f('total_order')/$diff->f('nr_del') < $diff->f('total_deliver')){
					            $total_deliver = $diff->f('total_order')/$diff->f('nr_del');
					        }else{
					            $total_deliver = $diff->f('total_deliver');
					        }
						     $qty_diff +=($diff->f('total_order')/$diff->f('nr_del') - $total_deliver); 	     
						}
						$items_on_project=$this->db->field("SELECT SUM(project_articles.quantity) 
						     FROM  project_articles 
						     INNER JOIN  projects ON  projects.project_id=project_articles.project_id
						      WHERE project_articles.article_id='".$articles->f('article_id')."' AND projects.stage!=0 AND project_articles.delivered=0");
						$items_on_intervetion=$this->db->field("SELECT SUM( servicing_support_articles.quantity) 
						      FROM   servicing_support_articles 
						      INNER JOIN  servicing_support ON  servicing_support.service_id=servicing_support_articles.service_id
						       WHERE servicing_support_articles.article_id='".$articles->f('article_id')."' AND servicing_support_articles.article_delivered=0");
						$ant_stock=$articles->f('stock')-($qty_orders- $qty_diff +$items_on_project+$items_on_intervetion ) + ($qty_orders_p - $qty_diff_p);
						$tmp_item =array(
					        'article_id'     	=> $articles->f('article_id'),			     
					        'stock_ant_ord'     => $ant_stock,				    
					        'total_price_ord'   => $articles->f('price')+ $articles->f('price')*$articles->f('vat_value')/100,			     
					        );
					    array_push($tmp_array, $tmp_item);
				    }
				    if($order ==' ASC '){
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_ASC);    
				    }else{
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_DESC);
				    }
				    $new_exo=array();
				    foreach($exo as $key=>$value){
				    	array_push($new_exo,$value);
				    }
				    unset($exo);
				    if($offset*$l_r+$in['cindex']-2<0){
				    	$prev_item=1;
				    }else{
				    	$prev_item=$new_exo[$offset*$l_r+$in['cindex']-2]['article_id'];
				    }
				    $next_item=$new_exo[$offset*$l_r+$in['cindex']]['article_id'];
				}else{
				    if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT pim_articles.article_id
			            FROM pim_articles
			            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
			            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
			            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id	            
			        	WHERE $filter ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
					}		
					$next_item = $this->db->field("SELECT pim_articles.article_id
			            FROM pim_articles
			            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
			            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
			            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id	            
			        	WHERE $filter ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}			
			}
		}

		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='article_id';

		$this->out = $result;
	}

	private function get_serviceData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		$l_r =ROW_PER_PAGE;
		$order_by = " ORDER BY  pim_articles.item_code  ";

		$order_by_array = array('item_code','internal_name','article_category','price','customer_name','block_discount');

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		//FILTER LIST
		$selected =array();
		$filter = '1=1';
		$filter_attach = '';
		$filter.= " AND pim_articles.is_service='1' ";

		if(!$in['archived']){
			$filter.= " AND pim_articles.active='1' ";
		}else{
			$filter.= " AND pim_articles.active='0' ";
		}


		if(!empty($in['search'])){
			$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_articles.origin_number LIKE '%".$in['search']."%') ";
		}
		if(!empty($in['name'])){
			$filter .= " AND ( pim_articles.supplier_name LIKE '%".$in['name']."%') ";
		}
		if(!empty($in['supplier_reference'])){
			$filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
		}

		if($in['article_category_id']){
			$filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
		}
		if($in['first'] && $in['first'] == '[0-9]'){
			$filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
		} elseif ($in['first']){
			$filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
		}

		if($in['order_by']){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc']=='1' || $in['desc']=='true'){
					$order = " DESC ";
				}
				if($in['order_by']=='customer_name'){
					$in['order_by']='name';
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}
		$in['lang_id'] = $this->db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1' GROUP BY lang_id ");
		if(!$in['lang_id']){
			$in['lang_id']=1;
		}

		$id_exist=$this->db->field("SELECT article_id FROM pim_articles WHERE article_id='".$in['original_id']."' AND is_service='1' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(pim_articles.article_id)
					FROM pim_articles
					LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
					WHERE $filter ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY pim_articles.article_id ";
				$filter_item=" AND pim_articles.article_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(pim_articles.article_id)
					FROM pim_articles
					WHERE ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT pim_articles.article_id
		            FROM pim_articles
		            LEFT JOIN customers ON pim_articles.supplier_id = customers.customer_id
		            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
		            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
		            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
					WHERE $filter
				   ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT pim_articles.article_id
		            FROM pim_articles
		            LEFT JOIN customers ON pim_articles.supplier_id = customers.customer_id
		            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
		            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
		            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
					WHERE $filter
				   ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT pim_articles.article_id
		            FROM pim_articles
		            LEFT JOIN customers ON pim_articles.supplier_id = customers.customer_id
		            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
		            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
		            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
					WHERE $filter
				   ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}		
				$next_item = $this->db->field("SELECT pim_articles.article_id
		            FROM pim_articles
		            LEFT JOIN customers ON pim_articles.supplier_id = customers.customer_id
		            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
		            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
		            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
					WHERE $filter
				   ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}
		}

		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='article_id';

		$this->out = $result;
	}

	private function get_quoteData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		if(!empty($in['start_date_js'])){
			$in['start_date'] =strtotime($in['start_date_js']);
			$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
		}
		if(!empty($in['stop_date_js'])){
			$in['stop_date'] =strtotime($in['stop_date_js']);
			$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
		}
		$order_by_array = array('serial_number','quote_date','subject','buyer_name','amount');
		$today = mktime(0,0,0,date('n'),date('j'),date('Y'));

		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

		$l_r = ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$filter_q = 'WHERE 1=1 ';
		$order_by = " ORDER BY tblquote.quote_date DESC ";
		if(defined('SHORT_QUOTES_BY_SERIAL') && SHORT_QUOTES_BY_SERIAL == 1){
			$order_by = " ORDER BY tblquote.serial_number DESC ";
		}
		$filter_your_ref = '';
		if(!$in['archived']){
			$filter_q.= " AND f_archived = '0' ";
		}else{
			$filter_q.= " AND f_archived = '1' ";
		}
		if($in['search']){
			$filter_q.=" AND (tblquote.serial_number like '%".$in['search']."%' OR tblquote.buyer_name like '%".$in['search']."%'  OR tblquote.subject like '%".$in['search']."%' )";
		}
		if($in['your_reference_search']){
			$filter_your_ref =" AND tblquote.buyer_reference LIKE '%".$in['your_reference_search']."%' ";
		}

		$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
		if($in['order_by']){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1' || $in['desc']=='true'){
					$order = " DESC ";
				}
				if($in['order_by']=='amount' ){
					$filter_limit =' ';
		    		$order_by ='';
			    }else{
			       $order_by =" ORDER BY ".$in['order_by']." ".$order;
			       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			       $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
			    }
			
			}
		}

		if($in['start_date'] && $in['stop_date']){
			$filter_q.=" and tblquote.quote_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
			$filter_link = 'none';
		}
		else if($in['start_date']){
			$filter_q.=" and cast(tblquote.quote_date as signed) > ".$in['start_date']." ";
		}
		else if($in['stop_date']){
			$filter_q.=" and cast(tblquote.quote_date as signed) < ".$in['stop_date']." ";
			$filter_link = 'none';
		}
		if(!isset($in['view'])){
			$in['view'] = 0;
		}
		if($in['view'] == 0){
			# do nothing
		}
		if($in['view'] == 1){
			$filter_q.=" AND tblquote.status_customer='0' AND tblquote_version.sent='0' ";
		}
		if($in['view'] == 2){
			$filter_q.=" AND tblquote.status_customer='0' AND tblquote_version.sent = '1' ";
		}
		if($in['view'] == 3){
			$filter_q.=" AND tblquote.status_customer > '1' ";
		}
		if($in['view'] == 4){
			$filter_q.=" AND tblquote.status_customer = '1' ";
		}
		if($in['view'] == 5){
			$filter_q.=" AND tblquote.created_by = '".$_SESSION['u_id']."' ";
		}
		if($in['view'] == 6 ) {
			$filter_q .= " AND tblquote.pending = '1' AND tblquote.status_customer > '1' AND tblquote.transformation = '0' ";
		}

		$filter_c = " AND 1=1 ";
		$cust_id = '';
		if($in['customer_id']){
			$cust_id=$in['customer_id'];
			$filter_c = " AND buyer_id='".$in['customer_id']."' ";
			$page_title_q='<h2>'.gm('List quotes').'<a href="index.php?do=quote-quotes&add=true" title="'.gm('Add').'" class="add">'.gm('Add').'</a>';
		}
		if($in['opportunity_id']){
			$quote_ids = "";
			$opportunity_quotes = $this->db->query("SELECT quote_id FROM  tblopportunity_quotes WHERE opportunity_id = '".$in['opportunity_id']."' ORDER BY id DESC");
			$j=0;
			while($opportunity_quotes->next()){
				$quote_ids .= "'".$opportunity_quotes->f('quote_id')."',";
				$j++;
			}
			$quote_ids = rtrim($quote_ids,',');
			$filter_q.=" AND tblquote.id IN (".$quote_ids.") ";
		}
		$admin_licence=aktiUser::get('user_type');

		if($admin_licence != '3' && ONLY_IF_ACC_MANAG4 == '1'){
			$filter_q.= " AND CONCAT( ',', acc_manager_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$id_exist=$this->db->field("SELECT id FROM tblquote WHERE id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items'] =  $this->db->field("SELECT count(tblquote.id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id  ".$filter_q." ".$filter_c.$filter_your_ref );
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY tblquote.id ";
				$filter_item=" AND tblquote.id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(tblquote.id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id  ".$filter_q.$filter_c.$filter_your_ref.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT tblquote.id
		            FROM tblquote
		            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
		            ".$filter_q." AND active=1 ".$filter_c.$filter_your_ref.$order_by ." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT tblquote.id
		            FROM tblquote
		            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
		            ".$filter_q." AND active=1 ".$filter_c.$filter_your_ref.$order_by ." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($in['order_by']=='amount'){
					$tmp_array=array();
					$quote = $this->db->query("SELECT tblquote.id, tblquote.currency_rate,tblquote_version.*  
					            FROM tblquote
					            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
					            ".$filter_q." AND active=1 ".$filter_c.$filter_your_ref );

					while($quote->move_next()){
						$amount = $this->db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$quote->f('id')."' AND version_id='".$quote->f('version_id')."' AND show_block_total='0' ");

						if($quote->f('discount') && $quote->f('apply_discount') > 1){
							$amount = $amount - ($amount*$quote->f('discount')/100);
						}

						if($quote->f('currency_rate')){
							$amount = $amount*return_value($quote->f('currency_rate'));
						}
						$tmp_item =array(
					        'quote_id'      => $quote->f('id'),	
					        'amount_ord'	=> $amount,
					        );
					    array_push($tmp_array, $tmp_item);   
					}
					if($order ==' ASC '){
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_ASC);    
				    }else{
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_DESC);
				    }
				    $new_exo=array();
				    foreach($exo as $key=>$value){
				    	array_push($new_exo,$value);
				    }
				    unset($exo);
				    if($offset*$l_r+$in['cindex']-2<0){
				    	$prev_item=1;
				    }else{
				    	$prev_item=$new_exo[$offset*$l_r+$in['cindex']-2]['quote_id'];
				    }
				    $next_item=$new_exo[$offset*$l_r+$in['cindex']]['quote_id'];
				}else{

					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT tblquote.id
			            FROM tblquote
			            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
			            ".$filter_q." AND active=1 ".$filter_c.$filter_your_ref.$order_by ." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
					}		
					$next_item = $this->db->field("SELECT tblquote.id
			            FROM tblquote
			            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
			            ".$filter_q." AND active=1 ".$filter_c.$filter_your_ref.$order_by ." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}
			}
		}
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='quote_id';

		$this->out = $result;
	}

	private function get_orderData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		$l_r =ROW_PER_PAGE;

		if(!empty($in['start_date_js'])){
			$in['start_date'] =strtotime($in['start_date_js']);
			$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
		}
		if(!empty($in['stop_date_js'])){
			$in['stop_date'] =strtotime($in['stop_date_js']);
			$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
		}
		if(!empty($in['d_start_date_js'])){
			$in['d_start_date'] =strtotime($in['d_start_date_js']);
			$in['d_start_date'] = mktime(0,0,0,date('n',$in['d_start_date']),date('j',$in['d_start_date']),date('y',$in['d_start_date']));
		}
		if(!empty($in['d_stop_date_js'])){
			$in['d_stop_date'] =strtotime($in['d_stop_date_js']);
			$in['d_stop_date'] = mktime(23,59,59,date('n',$in['d_stop_date']),date('j',$in['d_stop_date']),date('y',$in['d_stop_date']));
		}

		if(($in['ofs']) || (is_numeric($in['ofs'])))
		{
			$in['offset']=$in['ofs'];
		}
		$order_by_array = array('serial_number','t_date','del_date','our_ref','customer_name','amount');
		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$filter = 'WHERE 1=1 ';
		$filter_link = 'block';
		$filter_article='';
		$filter_your_ref = '';
		$order_by = " ORDER BY pim_orders.date DESC, pim_orders.serial_number DESC ";

		if(!$in['archived']){
			$filter.= " AND pim_orders.active=1";
		}else{
			$filter.= " AND pim_orders.active=0";
		}
		if($in['customer_id']){
			$filter.= " AND pim_orders.customer_id='".$in['customer_id']."'";
		}
		if($in['search']){
			$filter.=" and (pim_orders.serial_number like '%".$in['search']."%' OR pim_orders.customer_name like '%".$in['search']."%' OR pim_orders.our_ref like '%".$in['search']."%' )";
		}

		if($in['article_name_search']){
			$filter_article.=" INNER JOIN pim_order_articles ON pim_order_articles.order_id=pim_orders.order_id
		                       INNER JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id AND (pim_articles.internal_name like '%".$in['article_name_search']."%' OR pim_order_articles.article_code like '%".$in['article_name_search']."%')";
		}
		if($in['your_reference_search']){
			$filter_your_ref =" AND pim_orders.your_ref LIKE '%".$in['your_reference_search']."%' ";
		}
		if($in['order_by']){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1' || $in['desc']=='true'){
					$order = " DESC ";
				}
				if($in['order_by']=='t_date'){
					$in['order_by']='pim_orders.date';
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}

		if(!isset($in['view'])){
			$in['view'] = 4;
		}
		$show_only_my = true;
		if(defined('SHOW_MY_ORDERS_ONLY') && SHOW_MY_ORDERS_ONLY == 1){
			if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
				$show_only_my = false;
				if($in['view'] == 0){
					$in['view'] = 4;
				}
				$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
			}
		}
			switch ($in['view']) {
				case '1':
					$filter.=" and pim_orders.rdy_invoice = '0' and pim_orders.sent='0' ";
					break;
				case '2':
					$filter.=" and pim_orders.rdy_invoice != '1' and pim_orders.sent='1'";
					break;
				case '3':
					$filter.=" and pim_orders.rdy_invoice = '1' ";
					break;
				case '4':
					if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])) {
						$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
					}
					break;
				case '5':
					$filter.=" and pim_orders.rdy_invoice = '2' ";
				break;
				default:
					break;
			}

		if($in['start_date'] && $in['stop_date']){
			$filter.=" and pim_orders.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
			$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
			$filter_link = 'none';
		}
		else if($in['start_date']){
			$filter.=" and cast(pim_orders.date as signed) > ".$in['start_date']." ";
		}
		else if($in['stop_date']){
			$filter.=" and cast(pim_orders.date as signed) < ".$in['stop_date']." ";
			$filter_link = 'none';
		}

		if($in['d_start_date'] && $in['d_stop_date']){
			$filter.=" and pim_orders.del_date BETWEEN '".$in['d_start_date']."' and '".$in['d_stop_date']."' ";
			$filter_link = 'none';
		}
		else if($in['d_start_date']){
			$filter.=" and cast(pim_orders.del_date as signed) > ".$in['d_start_date']." ";
		}
		else if($in['d_stop_date']){
			$filter.=" and cast(pim_orders.start_date as signed) < ".$in['d_stop_date']." ";
			$filter_link = 'none';
		}

		if($in['new_orders']==1) {
			$days_30 = time() - 30*3600*24;
			$now = time();
			$filter .= " AND pim_orders.rdy_invoice!=1 AND date BETWEEN ".$days_30." AND ".$now." ";
		}
		$id_exist=$this->db->field("SELECT order_id FROM pim_orders WHERE order_id='".$in['original_id']."' ");
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(*)
					FROM pim_orders
					".$filter_article."
					 ".$filter."
					 ".$filter_your_ref." ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY pim_orders.order_id ";
				$filter_item=" AND pim_orders.order_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(*)
					FROM pim_orders
					".$filter_article."
					 ".$filter."
					 ".$filter_your_ref.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT pim_orders.order_id
					FROM pim_orders
					
					".$filter_article."
					 ".$filter."
					 ".$filter_your_ref." 
					 ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT pim_orders.order_id
					FROM pim_orders
					
					".$filter_article."
					 ".$filter."
					 ".$filter_your_ref." 
					".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT pim_orders.order_id
					FROM pim_orders
					
					".$filter_article."
					 ".$filter."
					 ".$filter_your_ref." 
					".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}		
				$next_item = $this->db->field("SELECT pim_orders.order_id
					FROM pim_orders
					
					".$filter_article."
					 ".$filter."
					 ".$filter_your_ref." 
					 ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}
		}
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='order_id';

		$this->out = $result;
	} 

	private function get_porderData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		$l_r =ROW_PER_PAGE;

		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$filter_po = 'WHERE 1=1 ';
		$filter_c='';
		if($in['created_from']){
			$filter_c = ', servicing_support.serial_number AS service_sn';
			$filter_po = ' INNER JOIN servicing_support ON servicing_support.service_id = pim_p_orders.service_id WHERE 1=1 ';
			$filter_po.=" and (servicing_support.serial_number like '%".$in['created_from']."%' )";
		}

		$filter_link = 'block';
		$order_by = " ORDER BY pim_p_orders.date DESC, pim_p_orders.serial_number DESC ";

		if(!$in['archived']){
			$filter_po.= " AND pim_p_orders.active=1";
		}else{
			$filter_po.= " AND pim_p_orders.active=0";
		}

		if($in['search']){
			$filter_po.=" and (pim_p_orders.serial_number like '%".$in['search']."%' OR pim_p_orders.our_ref like '%".$in['search']."%' OR pim_p_orders.customer_name like '%".$in['search']."%')";
		}

		if($in['order_by']){
			$order = " ASC ";
			if($in['desc']=='1' || $in['desc']=='true'){
				$order = " DESC ";
			}
			if($in['order_by']=='t_date'){
				$in['order_by']='pim_p_orders.date';
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			
		}

		if(!empty($in['start_date_js'])){
			$in['start_date'] =strtotime($in['start_date_js']);
			$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
		}
		if(!empty($in['stop_date_js'])){
			$in['stop_date'] =strtotime($in['stop_date_js']);
			$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
		}
		if($in['start_date'] && $in['stop_date']){
			$filter_po.=" and pim_p_orders.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
			$filter_link = 'none';
		}
		else if($in['start_date']){
			$filter_po.=" and cast(pim_p_orders.date as signed) > ".$in['start_date']." ";
		}
		else if($in['stop_date']){
			$filter_po.=" and cast(pim_p_orders.date as signed) < ".$in['stop_date']." ";
			$filter_link = 'none';
		}

		switch ($in['view']) {
				case '1':
					$filter_po.=" and pim_p_orders.rdy_invoice = '0' and pim_p_orders.sent='0' ";
					break;
			  	case '2':
			  		if($in['archived']){
			  			$filter_po.=" and pim_p_orders.sent='0' and pim_p_orders.rdy_invoice != '0' and pim_p_orders.rdy_invoice != '1'";
			  		} else {
			  			$filter_po.=" and pim_p_orders.sent='1' and pim_p_orders.rdy_invoice != '1' ";
			  		}
					break;		
				case '3':
					$filter_po.=" and pim_p_orders.rdy_invoice = '1' ";
					break;
				case '4':
					$filter_po.=" and pim_p_orders.rdy_invoice = '2' and pim_p_orders.sent='1' and pim_p_order_deliveries.delivery_id !=0";
					break;
				default:
					break;
			}
		$id_exist=$this->db->field("SELECT p_order_id FROM pim_p_orders WHERE p_order_id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(pim_p_orders.p_order_id)
					FROM pim_p_orders
					LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
					".$filter_article."
					 ".$filter_po."  ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY pim_p_orders.p_order_id ";
				$filter_item=" AND pim_p_orders.p_order_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(pim_p_orders.p_order_id)
					FROM pim_p_orders
					LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
					".$filter_article."
					 ".$filter_po.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT pim_p_orders.p_order_id
					FROM pim_p_orders 
					LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
					".$filter_po." ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT pim_p_orders.p_order_id
					FROM pim_p_orders 
					LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
					".$filter_po." ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT pim_p_orders.p_order_id
					FROM pim_p_orders 
					LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
					".$filter_po." ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}		
				$next_item = $this->db->field("SELECT pim_p_orders.p_order_id
					FROM pim_p_orders 
					LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
					".$filter_po." ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}
		}
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='p_order_id';

		$this->out = $result;
	}

	private function get_interventionData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);
		$l_r = 30;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$order_by =" ORDER BY servicing_support.serial_number ASC ";
		$filter = " 1=1 AND servicing_support.is_recurring='0' ";

		if($in['customer_id']){
			$filter .=" AND customer_id='".$in['customer_id']."' ";
		}
		if(!$in['archived']){
			$filter.= " AND servicing_support.active=1 ";
		}else{
			$filter.= " AND servicing_support.active=0 ";
		}

		if($in['search']){
			$filter .= " AND (servicing_support.customer_name LIKE '%".$in['search']."%' OR servicing_support.subject LIKE '%".$in['search']."%' OR servicing_support.serial_number LIKE '%".$in['search']."%' )";
		}

		if(!isset($in['view'])){
			$in['view'] = 5;
			$order_by =" ORDER BY servicing_support.serial_number DESC ";
		}
		if($in['view'] == 1){
			$order_by =" ORDER BY servicing_support.serial_number DESC ";
			$filter.=" and servicing_support.status = '1' ";
		}
		if($in['view'] == 0){
			$filter.=" and servicing_support.status = '0' ";
		}
		if($in['view'] == 2){
			$filter.=" and servicing_support.status = '1' ";
			$order_by =" ORDER BY servicing_support.serial_number DESC ";
		}
		if($in['view'] == 3){
			$filter.=" and servicing_support.status = '2' AND servicing_support.accept=0 ";
			$order_by =" ORDER BY servicing_support.serial_number DESC ";
		}
		if($in['view'] == 4){
			$filter.=" and servicing_support.status = '2' AND servicing_support.accept=1 ";
			$order_by =" ORDER BY servicing_support.serial_number DESC ";
		}
		if($in['view'] == 5){
			//$filter = " 1=1 ";
			$filter = " 1=1 AND servicing_support.is_recurring='0' AND servicing_support.active=1 ";
			$order_by =" ORDER BY servicing_support.serial_number DESC ";
		}

		$join = "";

		$doMatrixApiCall=false;
		$matrix_hash_id="";
		if(DATABASE_NAME == 'c9fc01f1_5334_cdcb_0e4b168974f7'){					
			$matrix_origin_string="";
			if(($in['street'] && !empty($in['street'])) || ($in['zip'] && !empty($in['zip'])) || ($in['city'] && !empty($in['city'])) || ($in['country_id'] && !empty($in['country_id']))){
				if($in['street']){
					$matrix_origin_string.=trim($in['street']).",";
				}
				if($in['zip'] && !$in['city']){
					$matrix_origin_string.=trim($in['zip']).",";
				}else if($in['zip'] && $in['city']){
					$matrix_origin_string.=trim($in['zip']).' '.trim($in['city']).",";
				}else if(!$in['zip'] && $in['city']){
					$matrix_origin_string.=trim($in['city']).",";
				}
				if($in['country_id']){
					$matrix_origin_string.=get_country_name(trim($in['country_id'])).",";
				}
				$matrix_origin_string=rtrim($matrix_origin_string,",");
				if(!empty($matrix_origin_string)){
					$doMatrixApiCall=true;
					$matrix_hash_id=base64_encode($matrix_origin_string);
				}
			}
			if($doMatrixApiCall){
				$in['start_date']=mktime(0,0,0,date('n'),date('j'),date('y'));
				$in['stop_date']=strtotime("+30 days");
				$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
				$filter.=" and servicing_support.planeddate BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
				$join.=" LEFT JOIN servicing_support_distance ON servicing_support.service_id=servicing_support_distance.service_id AND servicing_support_distance.hash_id='".$matrix_hash_id."' ";
			}
		}

		$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
		if($in['order_by']){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
				$order = " DESC ";
			}
			if($in['order_by']=='manager'){
				$filter_limit =' ';
		    	$order_by ='';
		    }else if($in['order_by']=='distance_ord'){
		    	$in['order_by'] ='servicing_support_distance.distance';
		    	$order_by =" ORDER BY ".$in['order_by']." ".$order;
		    	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
		    }else{
		       $order_by =" ORDER BY ".$in['order_by']." ".$order;
		       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		       $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
		    }
		}

		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_13' ");
		$perm_manager = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='intervention_admin' ");
		$perm_reg_user=in_array('13', perm::$allow_apps);
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
			case $perm_manager:
			case $perm_reg_user:
				//$join = '';
				break;
			default:
				$join.=" INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id ";
				$filter.=" AND servicing_support_users.user_id ='".$_SESSION['u_id']."' ";
				break;
		}
		$id_exist=$this->db->field("SELECT service_id FROM servicing_support WHERE service_id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items'] =  $this->db->field("SELECT count(DISTINCT servicing_support.service_id) FROM servicing_support
								".$join."
								WHERE ".$filter );
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY servicing_support.service_id ";
				$filter_item=" AND servicing_support.service_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(DISTINCT servicing_support.service_id) FROM servicing_support
								".$join."
								WHERE ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT servicing_support.service_id
						FROM servicing_support
					".$join."
					WHERE ".$filter.$order_by." LIMIT ".($result['start_item']-2).",1");
				}				
				$next_item = $this->db->field("SELECT servicing_support.service_id
					FROM servicing_support
					".$join."
					WHERE ".$filter.$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($in['order_by']=='manager'){
					$tmp_array=array();
					$intervention = $this->db->query("SELECT servicing_support.service_id FROM servicing_support
								".$join."
								WHERE ".$filter);

					while($intervention->move_next()){
							$managers = $this->db->query("SELECT user_id FROM servicing_support_users WHERE service_id ='".$intervention->f('service_id')."' AND pr_m = '1' ")->getAll();
							$managers_list ='';
							foreach($managers as $k =>$v){
								$managers[$k]['name']=get_user_name($v['user_id']);
								$managers_list .= $managers[$k]['name'].', '; 
							}

							$managers_list = rtrim($managers_list,', ');
							$tmp_item =array(
						        'service_id'      => $intervention->f('service_id'),	
						        'manager_ord'	=> $managers_list,
						        );
						    array_push($tmp_array, $tmp_item);   
					}
					if($order ==' ASC '){
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_ASC);    
				    }else{
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_DESC);
				    }
				    $new_exo=array();
				    foreach($exo as $key=>$value){
				    	array_push($new_exo,$value);
				    }
				    unset($exo);
				    if($offset*$l_r+$in['cindex']-2<0){
				    	$prev_item=1;
				    }else{
				    	$prev_item=$new_exo[$offset*$l_r+$in['cindex']-2]['service_id'];
				    }
				    $next_item=$new_exo[$offset*$l_r+$in['cindex']]['service_id'];
				}else{

					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT servicing_support.service_id
						FROM servicing_support
						".$join."
						WHERE ".$filter.$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
					}		
					$next_item = $this->db->field("SELECT servicing_support.service_id
						FROM servicing_support
						".$join."
						WHERE ".$filter.$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}
			}
		}
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='service_id';

		$this->out = $result;
	}

	private function get_projectData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);
		$l_r = ROW_PER_PAGE;

		$order_by_array=array('serial_number','start_date','end_date','name','company_name');

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$order_by =" ORDER BY serial_number DESC ";
		$filter = ' 1=1 ';
		if($in['customer_id']){
			$filter .=" AND customer_id='".$in['customer_id']."' ";
		}
		//$order_by = " ORDER BY projects.start_date ASC ";
		if(!$in['archived'] || $in['archived']=='0'){
			$filter.= " AND projects.active=1 ";
		}else{
			$filter.= " AND projects.active=0 ";
		}
		$views .= " ";
		if($in['view']=='2'){
			$views = " AND (projects.closed='1' OR projects.stage='2') ";
			$order_by =" ORDER BY serial_number DESC ";
		}else{
			$views .= " AND projects.closed='0' ";
			$filter.=" AND projects.stage<2 ";
		}
		$filter = $filter.$views;
		//FILTER LIST

		if(!empty($in['search'])){
			$filter .= " AND (projects.name LIKE '%".$in['search']."%' OR company_name LIKE '%".$in['search']."%' OR projects.serial_number LIKE '%".$in['search']."%' )";
		}

		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1' || $in['desc']=='true'){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}

		if($_SESSION['access_level'] !='1'){
			$filter .=" AND project_user.user_id='".$_SESSION['u_id']."' AND project_user.active='1' ";
		}

		$id_exist=$this->db->field("SELECT project_id FROM projects WHERE project_id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(DISTINCT projects.project_id) FROM projects
								LEFT JOIN project_user ON projects.project_id=project_user.project_id
								WHERE ".$filter);
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY projects.project_id ";
				$filter_item=" AND projects.project_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(DISTINCT projects.project_id) FROM projects
								LEFT JOIN project_user ON projects.project_id=project_user.project_id
								WHERE ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT projects.project_id FROM projects			
					LEFT JOIN project_user ON projects.project_id=project_user.project_id
					WHERE ".$filter." GROUP BY project_id ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT projects.project_id FROM projects			
					LEFT JOIN project_user ON projects.project_id=project_user.project_id
					WHERE ".$filter." GROUP BY project_id ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT projects.project_id FROM projects			
					LEFT JOIN project_user ON projects.project_id=project_user.project_id
					WHERE ".$filter." GROUP BY project_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}		
				$next_item = $this->db->field("SELECT projects.project_id FROM projects			
					LEFT JOIN project_user ON projects.project_id=project_user.project_id
					WHERE ".$filter." GROUP BY project_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}
		}

		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='project_id';

		$this->out = $result;
	}

	private function get_installationData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);

		$l_r = ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$filter = 'WHERE 1=1 ';
		$order_by_array=array('name', 'serial_number', 'customer_name', 'address', 'city', 'country_id');
		$order_by = " ORDER BY installations.name ASC ";

		if(!empty($in['search'])){
			$filter.=" AND (installations.serial_number LIKE '%".$in['search']."%' OR installations.name LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%') ";
		}
		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == 'true' || $in['desc'] == '1'){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}
		$id_exist=$this->db->field("SELECT id FROM installations WHERE id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(installations.id)
					FROM installations
					LEFT JOIN customers ON installations.customer_id=customers.customer_id
					LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id
					".$filter." ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY installations.id ";
				$filter_item=" AND installations.id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(installations.id)
					FROM installations
					LEFT JOIN customers ON installations.customer_id=customers.customer_id
					LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id
					".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT installations.id FROM installations
					LEFT JOIN customers ON installations.customer_id=customers.customer_id
					LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id
					".$filter." ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT installations.id FROM installations
					LEFT JOIN customers ON installations.customer_id=customers.customer_id
					LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id
					".$filter." ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT installations.id FROM installations
					LEFT JOIN customers ON installations.customer_id=customers.customer_id
					LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id
					".$filter." ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}		
				$next_item = $this->db->field("SELECT installations.id FROM installations
					LEFT JOIN customers ON installations.customer_id=customers.customer_id
					LEFT JOIN customer_addresses ON installations.address_id = customer_addresses.address_id
					".$filter." ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}
		}

		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='installation_id';

		$this->out = $result;
	}

	private function get_contractData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);
		$l_r = ROW_PER_PAGE;

		$order_by_array=array('iii','customer_name','serial_number','start_date','end_date','subject','amount');

		$beginOfDay = strtotime("midnight", strtotime('now'));
		$endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$order_by =" ORDER BY serial_number ";
		$filter = ' 1=1 ';
		if($in['customer_id']){
			$filter .=" AND contracts.customer_id='".$in['customer_id']."' ";
		}
		if(!$in['archived'] || $in['archived']=='0'){
			$filter.= " AND contracts.active=1 ";
		}else{
			$filter.= " AND contracts.active=0 ";
		}
		$views .= " AND (contracts.closed='0' OR contracts.closed='1') ";

		if($in['view']==1){
			$views = " AND contracts.sent='0' ";
			$order_by =" ORDER BY serial_number DESC ";
		}
		if($in['view']==2){
			$views = " AND contracts.sent='1' && contracts.status_customer='0' ";
			$order_by =" ORDER BY serial_number DESC ";
		}
		if($in['view']==3){
			$views = " AND contracts.sent='1' && contracts.status_customer='1' ";
			$order_by =" ORDER BY serial_number DESC ";
		}
		if($in['view']==4){
			$views = " AND contracts.sent='1' && contracts.status_customer='2' ";
			$order_by =" ORDER BY serial_number DESC ";
		}
		if($in['view']==5){
			$views = " AND contracts.closed='1' ";
			$order_by =" ORDER BY serial_number DESC ";
		}

		$filter = $filter.$views;

		if(!empty($in['search'])){
			$filter .= " AND (contracts.customer_name LIKE '%".$in['search']."%' OR contracts.serial_number LIKE '%".$in['search']."%' OR contracts.subject like '%".$in['search']."%' )";
		}

		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1' || $in['desc']=='true'){
					$order = " DESC ";
				}
				if($in['order_by']=='iii'){
					$in['order_by']="serial_number";
				}
				if($in['order_by']=='amount' ){
					$filter_limit =' ';
		    		$order_by ='';
			    }else{
			       $order_by =" ORDER BY ".$in['order_by']." ".$order;
			       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			        $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
			    }
			
			}
		}
		$id_exist=$this->db->field("SELECT contract_id FROM contracts WHERE contract_id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(DISTINCT contracts.contract_id) FROM contracts
							INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
							WHERE ".$filter." AND contracts_version.active='1' ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY contracts.contract_id ";
				$filter_item=" AND contracts.contract_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(DISTINCT contracts.contract_id) FROM contracts
							INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
							WHERE ".$filter." AND contracts_version.active='1' ".$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT contracts.contract_id FROM contracts
						INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id	
						WHERE ".$filter." AND contracts_version.active='1' GROUP BY contracts.contract_id ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT contracts.contract_id FROM contracts
					INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id	
					WHERE ".$filter." AND contracts_version.active='1' GROUP BY contracts.contract_id ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($in['order_by']=='amount'){
					$tmp_array=array();
					$contracts = $this->db->query("SELECT contracts.contract_id, contracts.currency_rate,contracts_version.version_id,contracts_version.discount,contracts_version.apply_discount FROM contracts
							INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id	
							WHERE ".$filter." AND contracts_version.active='1' GROUP BY contracts.contract_id ");

					while($contracts->move_next()){
						$amount = $this->db->field("SELECT SUM(amount) FROM contract_line WHERE contract_id='".$contracts->f('contract_id')."' AND version_id='".$contracts->f('version_id')."' AND show_block_total='0' AND use_recurring = 0 ");

						if($contracts->f('discount') && $contracts->f('apply_discount') > 1){
							$amount = $amount - ($amount*$contracts->f('discount')/100);
						}

						if($contracts->f('currency_rate')){
							$amount = $amount*return_value($contracts->f('currency_rate'));
						}
						$tmp_item =array(
					        'contract_id'      => $contracts->f('contract_id'),	
					        'amount_ord'	   => $amount,
					        );
					    array_push($tmp_array, $tmp_item);   
					}
					if($order ==' ASC '){
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_ASC);    
				    }else{
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_DESC);
				    }
				    $new_exo=array();
				    foreach($exo as $key=>$value){
				    	array_push($new_exo,$value);
				    }
				    unset($exo);
				    if($offset*$l_r+$in['cindex']-2<0){
				    	$prev_item=1;
				    }else{
				    	$prev_item=$new_exo[$offset*$l_r+$in['cindex']-2]['contract_id'];
				    }
				    $next_item=$new_exo[$offset*$l_r+$in['cindex']]['contract_id'];
				}else{

					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT contracts.contract_id FROM contracts
							INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id	
							WHERE ".$filter." AND contracts_version.active='1' GROUP BY contracts.contract_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
					}		
					$next_item = $this->db->field("SELECT contracts.contract_id FROM contracts
						INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id	
						WHERE ".$filter." AND contracts_version.active='1' GROUP BY contracts.contract_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}
			}
		}
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='contract_id';

		$this->out = $result;
	}

	private function get_invoiceData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);
		$l_r = ROW_PER_PAGE;

		global $config;
		if(!empty($in['start_date_js'])){
		    $in['start_date'] =strtotime($in['start_date_js']);
		    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
		}
		if(!empty($in['stop_date_js'])){
		    $in['stop_date'] =strtotime($in['stop_date_js']);
		    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
		}

		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
		$order_by_array = array('serial_number','t_date','buyer_name','our_ref','amount_vat','amount', 'due_date', 'balance','sepa_number');

		if($in['customer_id']){
		    $in['buyer_id'] = $in['customer_id'];
		}

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$filter = "WHERE 1=1 ";
		$filter_link = 'block';
		$filter_article='';
		$filter_your_ref = '';
		//$order_by = " ORDER BY t_date DESC, iii DESC ";
		$order_by = " ORDER BY DATE_ADD(cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ), INTERVAL 3 HOUR) DESC, serial_number DESC ";
		if($in['buyer_id']){
		    $filter .=" AND tblinvoice.buyer_id='".$in['buyer_id']."' ";
		}
		if($in['view'] != 2){
		    if(empty($in['archived'])){
			$filter.= " AND tblinvoice.f_archived='0' ";
		    }else{
			$filter.= " AND tblinvoice.f_archived='1' ";
		    }
		}

		if(!empty($in['c_invoice_id'])){
		    $filter.=" and (tblinvoice.c_invoice_id = '".$in['c_invoice_id']."')";

		}
		if(!empty($in['search'])){
		    $final_s="";
			if(is_numeric(str_replace(',','.',$in['search']))){
				$final_s=" OR FLOOR(tblinvoice.amount)='".$in['search']."' OR FLOOR(tblinvoice.amount_vat)='".$in['search']."' ";
			}
		    $filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' ".$final_s." )";
		}
		if($in['article_name_search']){
			$filter_article.=" INNER JOIN tblinvoice_line ON tblinvoice_line.invoice_id=tblinvoice.id 
						AND (tblinvoice_line.name like '%".$in['article_name_search']."%' OR tblinvoice_line.item_code like '%".$in['article_name_search']."%')  ";
		                       
		}
		if($in['your_reference_search']){
			$filter_your_ref =" AND tblinvoice.your_ref LIKE '%".$in['your_reference_search']."%' ";
		}
		if($in['ogm']){
			$filter.=" AND REPLACE(tblinvoice.ogm,'/','')='".$in['ogm']."' "; 
		}
		if(!empty($in['order_by'])){
		    if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1' || $in['desc']=='true'){
				    $order = " DESC ";
				}
				if($in['order_by']=='balance'){
					$order_by='';
					$filter_limit =' ';
				}else{
					if($in['order_by']=='t_date'){
						$order_by =" ORDER BY cast( FROM_UNIXTIME( `invoice_date` ) AS DATE) ".$order.", serial_number ".$order;
					}else{
						$order_by =" ORDER BY ".$in['order_by']." ".$order;
					}
					$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
				}

		    }
		}

		if(!isset($in['view'])){
		    $in['view'] = 0;
		}
		if($in['view'] == 1){
		    $filter.=" and tblinvoice.not_paid = '0' and tblinvoice.status = '0' AND sent='0' ";
		}
		if($in['view'] == 2){
		    $filter.=" and tblinvoice.not_paid = '0' and tblinvoice.type = '1' ";
		    if(empty($in['archived'])){
			$filter.= " AND (f_archived='0' OR f_archived='3')";
		    }else{
			$filter.= " AND (f_archived='1' OR f_archived='3') ";
		    }
		}
		if($in['view'] == 3){
		    $filter.=" and tblinvoice.not_paid = '0' and (tblinvoice.type = '2' || tblinvoice.type = '4')";
		}
		if($in['view'] == 4){
		    $filter.=" and tblinvoice.not_paid = '0' AND tblinvoice.status = '0' and tblinvoice.sent = '1' and tblinvoice.due_date < ".time();
		}
		if($in['view'] == 7){
		    $filter.=" and tblinvoice.not_paid = '1' ";
		}
		if($in['view'] == 5){
		    $filter.=" and tblinvoice.not_paid = '0' and tblinvoice.status = '0' and tblinvoice.sent = '1' ";
		}
		if($in['view'] == 8){
		    $filter.=" and tblinvoice.not_paid = '0' and tblinvoice.paid = '1' and tblinvoice.status = '1' and tblinvoice.sent = '1' ";
		}
		if($in['view'] == 6){
		    $filter.=" and tblinvoice.not_paid = '0' and tblinvoice.paid = '0' and tblinvoice.sent != '0' ";
		}
		// }
		if(!empty($in['start_date']) && !empty($in['stop_date'])){
		    $filter.=" and tblinvoice.invoice_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
		    $filter_link = 'none';
		}
		else if(!empty($in['start_date'])){
		    $filter.=" and cast(tblinvoice.invoice_date as signed) > ".$in['start_date']." ";
		}
		else if(!empty($in['stop_date'])){
		    $filter.=" and cast(tblinvoice.invoice_date as signed) < ".$in['stop_date']." ";
		    $filter_link = 'none';
		}

		$id_exist=$this->db->field("SELECT id FROM tblinvoice WHERE id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(*)
					FROM tblinvoice
					LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id
					".$filter_article."
			     ".$filter."
			    ".$filter_your_ref." ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY tblinvoice.id ";
				$filter_item=" AND tblinvoice.id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(*)
				    FROM tblinvoice
				    LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id 
				     ".$filter_article."
				     ".$filter."
				    ".$filter_your_ref.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT tblinvoice.id
			    FROM tblinvoice
			    LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id 
			     ".$filter_article."
			     ".$filter."
			    ".$filter_your_ref.$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT tblinvoice.id
			    FROM tblinvoice
			    LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id 
			     ".$filter_article."
			     ".$filter."
			    ".$filter_your_ref.$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($in['order_by']=='balance'){
					$tmp_array=array();
					$tblinvoice = $this->db->query("SELECT tblinvoice.id, tblinvoice.mandate_id, tblinvoice.status, tblinvoice.sent, tblinvoice.type, tblinvoice.amount_vat, mandate.sepa_number
		    			FROM tblinvoice
		    			LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id 
		     			".$filter_article."
		     			".$filter."
		    			".$filter_your_ref);
					while($tblinvoice->move_next()){
						if($tblinvoice->f('status')=='1' || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2'){
							continue; //Draft and Paid invoices are not displayed
						}
						//calculate balance
						$already_payedd = $this->db->query("SELECT SUM(amount) AS total_payedd FROM tblinvoice_payments WHERE invoice_id='".$tblinvoice->f('id')."'");
						$balance = $tblinvoice->f('amount_vat') - $already_payedd->f('total_payedd');
						$tmp_item =array(
					        'invoice_id'      => $tblinvoice->f('id'),			     
					        'balance_ord'     => ($tblinvoice->f('status')=='1' || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2')? '' : $balance,     
					        );
					    array_push($tmp_array, $tmp_item);   
					}
					if($order ==' ASC '){
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_ASC);    
				    }else{
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_DESC);
				    }
				    $new_exo=array();
				    foreach($exo as $key=>$value){
				    	array_push($new_exo,$value);
				    }
				    unset($exo);
				    if($offset*$l_r+$in['cindex']-2<0){
				    	$prev_item=1;
				    }else{
				    	$prev_item=$new_exo[$offset*$l_r+$in['cindex']-2]['invoice_id'];
				    }
				    $next_item=$new_exo[$offset*$l_r+$in['cindex']]['invoice_id'];
				}else{
					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT tblinvoice.id
				    FROM tblinvoice
				    LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id 
				     ".$filter_article."
				     ".$filter."
				    ".$filter_your_ref.$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
					}		
					$next_item = $this->db->field("SELECT tblinvoice.id
				    FROM tblinvoice
				    LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id 
				     ".$filter_article."
				     ".$filter."
				    ".$filter_your_ref.$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}
			}
		}
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='invoice_id';

		$this->out = $result;
	}

	private function get_recinvoiceData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);
		$l_r = ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$filter = 'WHERE 1=1 ';
		$order_by = " ORDER BY recurring_invoice.next_date ASC ";
		if(!$in['archived']){
			$filter.= " AND recurring_invoice.f_archived='0' ";
		}else{
			$filter.= " AND recurring_invoice.f_archived='1' ";
		}

		if($in['search']){
			$filter.=" and (recurring_invoice.buyer_name like '%".$in['search']."%' OR recurring_invoice.title like '%".$in['search']."%') ";
		}
		if($in['order_by']){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		}

		$id_exist=$this->db->field("SELECT recurring_invoice_id FROM recurring_invoice WHERE recurring_invoice_id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(DISTINCT recurring_invoice.recurring_invoice_id)
					FROM recurring_invoice
		            LEFT JOIN mandate ON recurring_invoice.mandate_id = mandate.mandate_id
		            ".$filter."  ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY recurring_invoice.recurring_invoice_id ";
				$filter_item=" AND recurring_invoice.recurring_invoice_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(recurring_invoice.recurring_invoice_id)
					FROM recurring_invoice
		            LEFT JOIN mandate ON recurring_invoice.mandate_id = mandate.mandate_id
		            ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT recurring_invoice.recurring_invoice_id
		            FROM recurring_invoice
		            LEFT JOIN mandate ON recurring_invoice.mandate_id = mandate.mandate_id
		            ".$filter."
		            GROUP BY recurring_invoice.recurring_invoice_id ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT recurring_invoice.recurring_invoice_id
		            FROM recurring_invoice
		            LEFT JOIN mandate ON recurring_invoice.mandate_id = mandate.mandate_id
		            ".$filter."
		            GROUP BY recurring_invoice.recurring_invoice_id ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT recurring_invoice.recurring_invoice_id
		            FROM recurring_invoice
		            LEFT JOIN mandate ON recurring_invoice.mandate_id = mandate.mandate_id
		            ".$filter."
		            GROUP BY recurring_invoice.recurring_invoice_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}		
				$next_item = $this->db->field("SELECT recurring_invoice.recurring_invoice_id
		            FROM recurring_invoice
		            LEFT JOIN mandate ON recurring_invoice.mandate_id = mandate.mandate_id
		            ".$filter."
		            GROUP BY recurring_invoice.recurring_invoice_id ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}
		}

		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='recurring_invoice_id';

		$this->out = $result;
	}

	private function get_purchaseinvoiceData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);
		$l_r = ROW_PER_PAGE;$l_r = ROW_PER_PAGE;

		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

		$arguments_o='';
		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$arguments='';
		$filter = "WHERE 1=1 ";
		$filter_link = 'block';
		$order_by = " ORDER BY tblinvoice_incomming.booking_number DESC ";
		$order_by_array = array('booking_number','invoice_number','t_date','due_date', 'buyer_name', 'total', 'total_with_vat', 'balance');

		if(!empty($in['search'])){
			$filter.=" AND (tblinvoice_incomming.invoice_number LIKE '%".$in['search']."%' OR tblinvoice_incomming.booking_number LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%' )";
		}
		$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc']==1 || $in['desc']=='true'){
					$order = " DESC ";
				}
				if($in['order_by']=='balance' || $in['order_by']=='buyer_name' ){
					$order_by='';
					$filter_limit =' ';
				}else{
					if($in['order_by']=='t_date'){
						$order_by =" ORDER BY cast( FROM_UNIXTIME( `invoice_date` ) AS DATE) ".$order;
					}else{
						$order_by =" ORDER BY ".$in['order_by']." ".$order;
					}			
					$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
					$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
				}
			
			}	
		}

		if(!isset($in['view'])){
			$in['view'] = 0;
		}

		if($in['view'] == 1){
			$filter.=" and tblinvoice_incomming.paid = '0' and tblinvoice_incomming.type != '2' ";
			
		}
		if($in['view'] == 2){
			$filter.=" and tblinvoice_incomming.paid = '0' and tblinvoice_incomming.status='1' and tblinvoice_incomming.due_date<'".time()."' ";		
		}
		if($in['view'] == 3){
			$filter.=" and tblinvoice_incomming.paid = '1'";		
		}
		if($in['view'] == 4){
			$filter.=" and tblinvoice_incomming.type = '2'";		
		}
		if($in['view'] == 5){
			$filter.=" and tblinvoice_incomming.status = '0' AND paid='0' ";
		}
		if($in['view'] == 6){
			$filter.=" and tblinvoice_incomming.status = '1' AND paid='0' ";	
		}
		if(empty($in['archived'])){
			$filter.= " AND tblinvoice_incomming.f_archived='0' ";
		}else{
			$filter.= " AND tblinvoice_incomming.f_archived='1' ";
		}

		if(!empty($in['start_date_js'])){
			$in['start_date'] =strtotime($in['start_date_js']);
			$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
		}
		if(!empty($in['stop_date_js'])){
			$in['stop_date'] =strtotime($in['stop_date_js']);
			$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
		}
		if(!empty($in['start_date']) && !empty($in['stop_date'])){
		    $filter.=" and tblinvoice_incomming.invoice_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
			$filter_link = 'none';
		}
		else if(!empty($in['start_date'])){
			$filter.=" and cast(tblinvoice_incomming.invoice_date as signed) > ".$in['start_date']." ";
		}
		else if(!empty($in['stop_date'])){
			$filter.=" and cast(tblinvoice_incomming.invoice_date as signed) < ".$in['stop_date']." ";
			$filter_link = 'none';
		}
		$id_exist=$this->db->field("SELECT invoice_id FROM tblinvoice_incomming WHERE invoice_id='".$in['original_id']."' "); 
		if($id_exist){
			$result['total_items']=$this->db->field("SELECT count(tblinvoice_incomming.invoice_id)
					FROM tblinvoice_incomming
					LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter." ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY tblinvoice_incomming.invoice_id ";
				$filter_item=" AND tblinvoice_incomming.invoice_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(tblinvoice_incomming.invoice_id)
					FROM tblinvoice_incomming
					LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT tblinvoice_incomming.invoice_id
					FROM tblinvoice_incomming
					LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter.$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT tblinvoice_incomming.invoice_id
					FROM tblinvoice_incomming
					LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter.$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($in['order_by']=='balance' || $in['order_by']=='buyer_name'){
					$tmp_array=array();
					$tblinvoice = $this->db->query("SELECT tblinvoice_incomming.*,customers.name as seller_name, booking_number as iii, 
						cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date
								FROM tblinvoice_incomming
								LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter);

					while($tblinvoice->move_next()){
				
							$customer=$this->db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('supplier_id')."'");
							if(!$tblinvoice->f('supplier_id')){
								$customer=$this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id ='".$tblinvoice->f('contact_id')."'");
							}
						//calculate balance
							$total = $tblinvoice->f('total_with_vat'); 
							$already_payed = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$tblinvoice->f('invoice_id')."'  ");
							$total_payed = $already_payed->f('total_payed');
							$amount_due = round($total - $total_payed,2);
						$tmp_item =array(
					        'invoice_id'      => $tblinvoice->f('id'),	
					        'buyer_name_ord'  => $customer,		     
					        'balance_ord'     => ($tblinvoice->f('status')=='1' || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2')? '' : $amount_due,     
					        );
					    array_push($tmp_array, $tmp_item);   
					}
					if($order ==' ASC '){
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_ASC);    
				    }else{
				       $exo = array_sort($tmp_array, $in['order_by'].'_ord', SORT_DESC);
				    }
				    $new_exo=array();
				    foreach($exo as $key=>$value){
				    	array_push($new_exo,$value);
				    }
				    unset($exo);
				    if($offset*$l_r+$in['cindex']-2<0){
				    	$prev_item=1;
				    }else{
				    	$prev_item=$new_exo[$offset*$l_r+$in['cindex']-2]['invoice_id'];
				    }
				    $next_item=$new_exo[$offset*$l_r+$in['cindex']]['invoice_id'];
				}else{

					$result['start_item']=$offset*$l_r+$in['cindex'];
					if($offset*$l_r+$in['cindex']-2<0){
						$prev_item=1;
					}else{
						$prev_item = $this->db->field("SELECT tblinvoice_incomming.invoice_id
						FROM tblinvoice_incomming
						LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter.$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
					}		
					$next_item = $this->db->field("SELECT tblinvoice_incomming.invoice_id
						FROM tblinvoice_incomming
						LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter.$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
				}
			}
		}

		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='invoice_id';

		$this->out = $result;
	}

	private function get_dealData(){
		$in=$this->in;

		/*$order_by_array = array('serial_number','opportunity_date','buyer_name','expected_revenue' ,'subject');
		$filter=" ";
		$crm_admin=aktiUser::get('is_admin_company');*/

		$l_r=ROW_PER_PAGE;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		/*$order_by = defined('DEAL_VIEW_LIST') && DEAL_VIEW_LIST == 1 ? " ORDER BY tblopportunity_stage.board_id ASC, tblopportunity_stage.id ASC, tblopportunity.sort_order ASC " : " ORDER BY tblopportunity_stage.board_id ASC, tblopportunity_stage.id ASC, tblopportunity.opportunity_date DESC ";

		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc']=='1' || $in['desc'] == 'true') {
					$order = " DESC ";
				}
				if($in['order_by']=='cus_name'){
					$in['order_by']='customers.name';
				}elseif($in['order_by']=='function'){
					$in['order_by']='customer_contactsIds.position';
				}else{

				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
				$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];				
			}
		}


		if(!empty($in['start_date_js'])){
		    $in['start_date'] =strtotime($in['start_date_js']);
		    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
		}
		if(!empty($in['stop_date_js'])){
		    $in['stop_date'] =strtotime($in['stop_date_js']);
		    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
		}	

		if(!$in['archived']){
			$filter.= " AND tblopportunity.f_archived=0 ";
		}else{
			$filter.= " AND tblopportunity.f_archived=1 ";
			$arguments.="&archived=".$in['archived'];
			$arguments_a = "&archived=".$in['archived'];
		}

		if($in['user_id']){
				$filter.= " AND tblopportunity.created_by='".$in['user_id']."' ";
			}

		if($in['search']){
			$filter .= " AND (customers.name LIKE '%".$in['search']."%' OR tblopportunity.subject LIKE '%".$in['search']."%' OR tblopportunity.serial_number LIKE '%".$in['search']."%') "; 
		}
		if(!empty($in['start_date']) && !empty($in['stop_date'])){
			    $filter.=" AND tblopportunity.opportunity_date BETWEEN '".$in['start_date']."' AND '".$in['stop_date']."' ";
			    $arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
			}
			else if(!empty($in['start_date'])){
			    $filter.=" AND cast(tblopportunity.opportunity_date as signed) > ".$in['start_date']." ";
			    $arguments.="&start_date=".$in['start_date'];
			}
			else if(!empty($in['stop_date'])){
			    $filter.=" AND cast(tblopportunity.opportunity_date as signed) < ".$in['stop_date']." ";
			    $arguments.="&stop_date=".$in['stop_date'];
			}

		if($in['manager_id']){
			//$filter = " AND tblopportunity.assigned_to='".$in['manager_id']."' ";
			$filter .= " AND tblopportunity.created_by='".$in['manager_id']."' ";
		}

		$filter_b = ' WHERE 1=1 ';
		if($in['board_id']){
		 $filter_b .= " AND tblopportunity_stage.board_id='".$in['board_id']."' ";
		}
		if($in['stage_id']){
			$filter_b .= " AND tblopportunity_stage.id='".$in['stage_id']."' ";
		}*/

		$id_exist=$this->db->field("SELECT opportunity_id FROM tblopportunity WHERE opportunity_id='".$in['original_id']."' "); 
		if($id_exist){
			$result['stage_name']=stripslashes($this->db->field("SELECT tblopportunity_stage.name FROM tblopportunity
				INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
				WHERE opportunity_id='".$in['original_id']."' "));
			/*$result['total_items']=$this->db->field("SELECT count(tblopportunity.opportunity_id)
					FROM tblopportunity
					INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
					".$filter_b."  ".$filter." ");
			if(is_null($in['cindex'])){
				$order_by = " ORDER BY tblopportunity.opportunity_id ";
				$filter_item=" AND tblopportunity.opportunity_id<'".$in['original_id']."' ";
				$result['start_item'] =  ($this->db->field("SELECT count(tblopportunity.opportunity_id)
					FROM tblopportunity
					INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
		            ".$filter_b."  ".$filter.$filter_item.$order_by))+1;
				if($result['start_item']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT tblopportunity.opportunity_id
		            FROM tblopportunity
					INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
		            ".$filter_b."  ".$filter." ".$order_by." LIMIT ".($result['start_item']-2).",1");
				}			
				$next_item = $this->db->field("SELECT tblopportunity.opportunity_id
		            FROM tblopportunity
					INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
		            ".$filter_b."  ".$filter." ".$order_by." LIMIT ".$result['start_item'].",1");
			}else{
				$result['start_item']=$offset*$l_r+$in['cindex'];
				if($offset*$l_r+$in['cindex']-2<0){
					$prev_item=1;
				}else{
					$prev_item = $this->db->field("SELECT tblopportunity.opportunity_id
		            FROM tblopportunity
					INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
		            ".$filter_b."  ".$filter." ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']-2).",1");
				}
				$next_item = $this->db->field("SELECT tblopportunity.opportunity_id
		            FROM tblopportunity
					INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
					INNER JOIN customers ON tblopportunity.buyer_id=customers.customer_id
		            ".$filter_b."  ".$filter." ".$order_by." LIMIT ".($offset*$l_r+$in['cindex']).",1");
			}*/

			$result['total_items']=aktiDeals::get('total_items');
			$result['start_item']=aktiDeals::get('start_item',$in['original_id']);
			$prev_item=aktiDeals::get('prev',$in['original_id']);
			$next_item=aktiDeals::get('next',$in['original_id']);
		}
		
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='opportunity_id';

		$this->out = $result;
	}


	private function get_timesheetData(){
		$in=$this->in;
		$result=array('total_items'=>0,'start_item'=>1);
	
		//list($u_id,$start_w)=explode('-',$in['original_id']);	
		
		//$instance = aktiTimesheet::getInstance();
		//$prev_item=$instance->get('prev',$in['original_id']);
		$prev_item=aktiTimesheet::get('prev',$in['original_id']);
		$next_item=aktiTimesheet::get('next',$in['original_id']);
		$total_items=aktiTimesheet::get('total_items',$in['original_id']);
		$start_item=aktiTimesheet::get('start_item',$in['original_id']);

		$result['start_item']=$start_item;
		$result['total_items']=$total_items;
		$result['prev_item']=$prev_item;
		$result['next_item']=$next_item;
		$result['item_name']='timesheet';

		$this->out = $result;
	}



}
$navigator = new Navigator($in,$db);

	$navigator->get_Data();
	$navigator->output();

?>