<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
// $view = new at();
session_write_close();
global $config;
/*$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);*/
$o=array();
$pag = isset($in['pagl']) ? $in['pagl'] : '';
$page = explode('-', $pag);
if($page[1]){
    $pag = $page[1];
}
$field_name = '';
$field_value = '';

foreach ($in as $key => $value) {
	if($key == 'do'){
		continue;
	}
	elseif(strstr($key,'_id') == true) {
		$field_name = $key;
		if($field_name == 'version_id'){
			continue;
		}
		$field_value = $value;
		break;
	}
	else {
		continue;
	}
}

if(array_key_exists('is_log_ajax',$in)){
	$pag = $in['page'];
}

if(!$field_value || !$field_name || !$pag){
	return;
}
$i = 0;

$now = time();
$is_loging =false;

$count = $db->field("SELECT count(log_id) FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='0' ORDER BY date DESC ");


	$is_loging =true;
	$count_comment = $db->field("SELECT count(log_id) FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='1' ORDER BY date DESC ");

	$logs = $db->query("SELECT message, date FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND type='0' ORDER BY date ASC LIMIT 0,1 ");
	
	$logs->next();
	

	$patt = '/\[!L!\](.*)\[!\/L!\]\s/';
	if(!strstr($logs->f('message'),'[!L!]')){
		$patt = '/\{l\}(.*)\{endl\}\s/';
	}
	$names = preg_replace($patt, '', $logs->f('message'));

	if($names == '{l}Invoice created by recurring tool{endl}'){
		$names = str_replace('{l}', '[!L!]', $logs->f('message'));
		$names = str_replace('{endl}', '[!/L!]', $names);
	}

	if($names == '[!L!]Invoice created by recurring tool[!/L!]'){
		$names = gm('Recurring tool');
	}
	if($names == '[!L!]Invoice created by contract invoice tool[!/L!]'){
		$names = gm('Recurring tool');
	}
	$name_uncomplete=strpos($names,'{l}');
	if($name_uncomplete !== false){
	    $names=substr($names,0,$name_uncomplete);
	}

	$counts = ($count-5).' '.gm('more actions');
	if(($count-5) == 1 ){
		$counts = ($count-5).' '.gm(' more action');
	}
	$in['field_value'] = $field_value;
	$in['field_name'] = $field_name;
	$in['pag'] = $pag;
	if($count_comment){
		$has_comment = true;
	}

		$o['ago']				= $logs->f('date') ? date(ACCOUNT_DATE_FORMAT,$logs->f('date')) : '';
		$o['user_name']			= $names;
		$o['field_name']			= $field_name;
		$o['field_value']			= $field_value;
		$o['pag']				= $pag;
		$o['page']				= $pag;
		$o['user_id']			= $_SESSION['u_id'];
		//$o['to_user_id']			= '0';
		$o['to_user_id']			= '';
		$o['user_dd']			= $has_comment ? build_user_dd(): '';
		$o['count']				= $counts;
		$o['more']				= $count > 5 ? true : false;
		$o['created']			= $names ? gm('created this item') : gm('Item created');
		/*$o['comments_list'] 		= $has_comment ? ark::run('pim-customer_activity_comment_list') : '';*/
		$o['comments_list'] 		= $has_comment ? true : false;

		if($pag == 'service' && $field_name == 'service_id'){
			$is_regular_intervention=$db->field("SELECT service_id FROM servicing_support WHERE active!='2' AND is_recurring='0' AND service_id='".$field_value."' ");
			if($is_regular_intervention){
				$o['regular_intervention']=true;
			}
		}

	$o['messages']=array();
	$comment = $db->query("SELECT `message`, `date`, `type`  FROM logging WHERE pag='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' AND (type='0' OR type ='1') ORDER BY date DESC, log_id DESC  LIMIT 4 ");
	while ($comment->next()) {
		$replace = '\[\!\/L\!\]';
		$replace_first = '\[\!L\!\]';


		if(!strstr($comment->f('message'),'[!/L!]')){
			$replace = '\{endl\}';
			$replace_first = '\{l\}';
		}
		
		/* a second approach*/
		preg_match_all('/'.$replace_first.'(.*)'.$replace.'/U', $comment->f('message'),$b);

		$c = $comment->f('message');

		//Add translation to opened the weblink
		$openedText = 'opened the weblink';
		if(strpos($c,$openedText) != false){
			$openedFullText = gm('opened the weblink');
			$c = str_replace($openedText, $openedFullText, $c);
		}
		//End Add translation to opened the weblink	

		if($b[1]){
			foreach ($b[1] as $key => $value) {
				//console::log($value);
				$c = preg_replace('/'.$replace_first.'(.*)'.$replace.'/U', gm($value), $c,1);
			}
		}
		$text = $c;
		$text=str_replace("[SITE_URL]", $config['site_url'], $text);
		if(strpos($text, "href") !== false && $comment->f('type') != 1){
			preg_match("/(?<=>).*?(?=<)/", $text, $textMatch);
			if($textMatch && is_array($textMatch) && count($textMatch) == 1){
				preg_match("/(?<=<).*?(?=>)/", $text, $linkMatch);
				if($linkMatch && is_array($linkMatch) && count($linkMatch) == 1){
					if($pag == 'recurring_invoice' && $field_name=='recurring_invoice_id'){
						$explodedWord=explode(" ", $textMatch[0]);
						$text='<'.$linkMatch[0].' class="text-link">'.$explodedWord[0].'</a>'.str_replace($explodedWord[0], "", $textMatch[0]);
					}else if($pag == 'invoice' && $field_name=='invoice_id'){
						$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
					}else if($pag == 'quote' && $field_name=='quote_id'){
						$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
					}else if($pag == 'order' && $field_name=='order_id'){
						$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
					}else if($pag == 'p_order' && $field_name=='p_order_id'){
						$text=substr($text, 0, strpos($text, "<a")).'<'.$linkMatch[0].' class="text-link">'.$textMatch[0].'</a>';
					}				
				}
			}
		}

		$messages=array(
			'message'				=> $text,
			'dates'				=> date(ACCOUNT_DATE_FORMAT,$comment->f('date')+$_SESSION['user_timezone_offset']),
			//'is_link'			=> strpos($text, "href") !== false ? true : false
		);
		array_push($o['messages'], $messages);
		/*$view->loop('messages');*/
		$i++;
	}

global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($db_config);
//$user=$db_users->query("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
$user=$db_users->query("SELECT group_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$user->move_next();
if($user->f('group_id')=='1'){
	
	$o['comment_style']		= false;
	$o['history_style'] 		= true;
	$o['textarea_style']		= 'width:455px;';
	$o['comment_style_id']		= '1';

}else{

	$o['comment_style']		= true;
	$o['history_style'] 		= true;
	$o['textarea_style']		= '';
	$o['comment_style_id']		= '0';

}


	$o['style_comment']		= pick_date_format(ACCOUNT_DATE_FORMAT);
	$o['is_loging'] 			= $is_loging;
	$o['hide_comment_form'] 	= $has_comment ? true : false;
$o['format']=pick_date_format();

$o['notes']=stripslashes($db->field("SELECT notes FROM internal_notes WHERE `module`='".$pag."' AND field_name='".$field_name."' AND field_value='".$field_value."' "));

json_out($o);
?>