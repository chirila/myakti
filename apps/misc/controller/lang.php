<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $lang;

foreach ($lang as $key => $val){
	$lang[$key] = utf8_encode($val);
}

json_out($lang);
?>