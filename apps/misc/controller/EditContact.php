<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();
$contact = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ")->getAll();

if(!$contact[0]['title']){
	$contact[0]['title'] =undefined;
}

if(!$contact[0]['sex']){
	$contact[0]['sex'] ='';
}

if(!$contact[0]['department']){
	$contact[0]['department'] =undefined;
}

if($contact[0]['position']){
	$contact[0]['position'] = explode(',', $contact[0]['position']);	
}

if($contact[0]['birthdate']){
	$contact[0]['birthdate'] = $contact[0]['birthdate']*1000;
}

if($contact[0]['language'] == '0'){
	$contact[0]['language'] = undefined;
}

$contact[0]['is_unsub'] = $contact[0]['unsubscribe'] ? true : false;
$contact[0]['unsub'] = $contact[0]['unsubscribe'] ? date(ACCOUNT_DATE_FORMAT,$contact[0]['unsubscribe']) : "";

$contact[0]['s_email'] = $contact[0]['s_email'] == '1' ? true : false;

$contact[0]['position_n'] = array();

$label = $db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY field_id ");
$all = $label->getAll();
$contact[0]['extra']=array();
$result['extra'] = array();
foreach ($all as $key => $value) {
	$f_value = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$value['field_id']."' ");	
	if($value['is_dd']==1){
		$result['extra'][$value['field_id']]=build_extra_contact_field_dd('',$value['field_id']);
		/*if(!$f_value){
			$f_value = '0';
		}*/
	}
	$contact[0]['extra'][$value['field_id']]=$f_value;
}
$result['is_admin']			= $_SESSION['access_level'] == 1 ? true : false;
$result['customer_contact_title']	= build_contact_title_type_dd('');
$result['customer_contact_language']	= build_language_dd();
$result['customer_contact_dep']		= build_contact_dep_type_dd();
$result['customer_contact_job_title']= build_contact_job_title_type_dd();

/*if($contact[0]['title'] && !array_key_exists($contact[0]['title'], $result['title'])){
	$contact[0]['title']='0';
}*/

$result['do_next']			= 'misc-EditCustomer-misc-contact_update';

if($in['c_id']){
$contact_new = $db->query("SELECT * FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND customer_id='".$in['c_id']."' ")->getAll();	
} else {
	$contact_new = $db->query("SELECT * FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND `primary`='1'")->getAll();
}



$contact_new = $contact_new[0];
$contact[0]['position'] = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$contact_new['position']."'");
$contact[0]['department_new'] = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact_new['department']."'");
$contact[0]['phone'] = $contact_new['phone'];
$contact[0]['fax'] = $contact_new['fax'];

$contact[0]['gender_dd'] = gender_dd();
$contact[0]['is_primary'] = $contact_new['primary'] ? $contact_new['primary'] : $contact[0]['is_primary'];
$contact[0]['e_title'] = $contact_new['e_title'] ? $contact_new['e_title'] : $contact[0]['e_title'];
$contact[0]['department'] = $contact_new['department'] ? $contact_new['department'] : $contact[0]['department'];
$contact[0]['position'] = $contact_new['position'] ? $contact_new['position'] : $contact[0]['position'];
$contact[0]['s_email'] = $contact_new['s_email'] ? $contact_new['s_email'] : 0;

$contact[0]['exact_title']=$contact_new['e_title'];
$contact[0]['function']=$contact_new['e_title'];
$contact[0]['zen_id']=$contact['zen_contact_id'];
$contact[0]['title_txt'] = $db->field("SELECT name FROM customer_contact_title  WHERE id='".$contact['title']."' ");

if(!is_numeric($contact[0]['title'])){
	$contact[0]['title']='1';
}

$contact[0]['email'] = $contact_new['primary'] ? $contact_new['email'] : $contact[0]['email'];

if($contact[0]['s_email'] == '1'){
	$contact[0]['s_email'] = true;
	$contact[0]['s_email_txt'] = gm('Yes');
}else{
	$contact[0]['s_email'] = false;
	$contact[0]['s_email_txt'] = gm('No');
}

//Build Sorted Field Section
$queryF = $db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact'  AND value=1 ORDER BY sort_order ASC");
$queryExtraF = $db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY sort_order ASC ");

$result['contact']	= $contact[0];

while ($queryF->next()) {
$fieldInfo = contactStaticFieldsInfo($queryF);
	if(!empty($fieldInfo['label'])){
		$result['contact']['linesObj'][] = $fieldInfo; 
	}	
}

while ($queryExtraF->next()) {
	$f_value = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$queryExtraF->f('field_id')."' ");
	if($in['contact_id']=='tmp' || !$in['contact_id']){
		$creation = $queryExtraF->f('creation_value') == 1 ? true : false;
	}else{
		$creation= true;
	}
	if($value['is_dd']==1){
		$extra_field_dd_name = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$queryExtraF->f('field_id')."' ");
	}

	$result['contact']['linesObj'][] = array(
		'field_id'			=> $queryExtraF->f('field_id'),
		'label'				=> $queryExtraF->f('label'),
		'value'				=> $f_value ? $f_value : '',
		'normal_input'		=> $queryExtraF->f('is_dd') == 1 ? false:true,
		'extra_field_name' 	=> $queryExtraF->f('is_dd') == 1 ? $extra_field_dd_name : ( $f_value ? $f_value : '' ),
		'extra_field_dd'  	=> $queryExtraF->f('is_dd') ==1 ? build_extra_field_dd($f_value,$queryExtraF->f('field_id')):'',
		'is_custom'			=> true,
		'sort_order'		=> $queryExtraF->f('sort_order'),
		);
}

usort($result['contact']['linesObj'], "myCustomSort");
//End Build Sorted Field Section

json_out($result);

function contactStaticFieldsInfo($query){
	$fieldLabelName = '';
	$ifCondition = '';
	$isDD = false;
	$selectizeConfig = '';
	$selectizeOptions = '';
	$model = '';
	$isCustom = false;
	$sortOrder = $query->f('sort_order');
	$hasTable = false;
	$table = '';
	$isDate = false;
	$hasName = false;

	switch($query->f('field')){
		case 'title':
			$fieldLabelName = gm('Salutation');
			$ifCondition = 'title_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_contact_title';
			$model = 'title';
			$hasTable = true;
			$table = 'customer_contact_title';
			break;
		case 'gender':
			$fieldLabelName = gm('Gender');;
			$ifCondition = 'gender_if';
			$isDD = true;
			$model = 'sex'; 
			break;
		case 'language':
			$fieldLabelName = gm('Language');
			$ifCondition = 'language_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_contact_language';
			$model = 'language';
			$hasTable = true;
			$table = 'customer_contact_language';
			break;
		case 'mobile':
			$fieldLabelName = gm('Cell');
			$ifCondition = 'mobile_if';
			$hasName = true;
			$model = 'cell';
			break;
		case 'birthdate':
			$fieldLabelName = gm('Birthdate');
			$ifCondition = 'birthdate_if';
			$isDate = true;
			$model = 'birthdate';
			break;
		case 'email':
			$fieldLabelName = gm('Email');
			$model = 'email';
			$hasName = true;
			break;
		case 'exact_title':
			$fieldLabelName = gm('Exact Title');
			$model = 'e_title';
			$hasName = true; 
			break;
		case 'phone':
			$fieldLabelName = gm('Phone');
			$model = 'phone';
			$hasName = true;
			break;
		case 'fax':
			$fieldLabelName = gm('Fax');
			$model = 'fax';
			$hasName = true;
			break;
		case 'function':
			$fieldLabelName = gm('Function');
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_contact_job_title';
			$hasTable = true;
			$table = 'customer_contact_job_title';
			$model = 'position';
			break;
		case 'department':
			$fieldLabelName = gm('Department');
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_contact_dep';
			$hasTable = true;
			$table = 'customer_contact_dep';
			$model = 'department';
			break;	
	}

	return array('label' 			=> $fieldLabelName,
			'if_condition' 		=> $ifCondition,
			'is_dd'		   		=> $isDD,
			'selectize_config' 	=> $selectizeConfig,
			'selectize_options' => $selectizeOptions,
			'model'				=> $model,
			'is_custom'			=> $isCustom,
			'sort_order'		=> $sortOrder,
			'has_table'			=> $hasTable,
			'table'				=> $table,
			'is_date'			=> $isDate,
			'has_name'			=>$hasName,
			);
}

function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

?>