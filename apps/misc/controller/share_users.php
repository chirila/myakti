<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');

global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db = new sqldb($db_config);
$db2 = new sqldb();
$l_r = ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$filter = '1=1';
if(!empty($in['search'])){
	$filter .= " AND (users.first_name LIKE '%".$in['search']."%' OR users.last_name LIKE '%".$in['search']."%')  ";
}

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = strtolower($row[$col]);
    }
    array_multisort($sort_col, $dir, $arr,SORT_STRING);
}
$db->query("SELECT users.* FROM users WHERE ".$filter." AND database_name='".DATABASE_NAME."' ORDER BY last_name ");
$max_rows=$db->records_count();
$result = array('list'=>array(),'max_rows'=>$max_rows);
$result['lr']=$l_r;
//$db->move_to($offset*$l_r);
$start = $offset*$l_r;
//$start = 0;
$k=0;
while ($db->move_next()) {
	$all_rows[$k] = array(
		'name11' 				=> utf8_encode($db->f('last_name')).' - '.utf8_encode($db->f('first_name')),
		'name22' 				=> utf8_encode($db->f('first_name'))." ".utf8_encode($db->f('last_name')),
		'user_id' 				=> $db->f('user_id'),
		'active'				=> $db->f('active'),
	);
	$k++;
}

$i=0;
$k=$start;
while($k<$max_rows && $i<$l_r){
	$sharing = $db2->query("SELECT * FROM customer_shared_calendars WHERE to_user='".$all_rows[$k]['user_id']."' AND user_id='".$_SESSION['u_id']."'");
	$sharing->next();
	$item=array(
		'NAME'						=> $all_rows[$k]['name11'],
		'NAME2'						=> $all_rows[$k]['name22'],
		'user_id' 			       	=> $all_rows[$k]['user_id'],
		'read'						=> ($sharing->f('rights')=='1' || $all_rows[$k]['user_id'] == $_SESSION['u_id'])? true : false,
		'read_write'				=> ($sharing->f('rights')=='2' || $all_rows[$k]['user_id'] == $_SESSION['u_id'])? true : false,
		'disabled'					=> $all_rows[$k]['user_id'] == $_SESSION['u_id'] ? true : false,
	);
	array_push($result['list'], $item);
	$k++;
	$i++;
}

return json_out($result);