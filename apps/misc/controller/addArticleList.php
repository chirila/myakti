
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}
$accepted_variants_apps=array('quote','po_order','order','invoice','maintenance');
/*
$db2 = new sqldb();
$db3 = new sqldb();*/

$l_r =10;

$order_by_array = array('item_code','internal_name','article_category','supplier_reference','stock');

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$def_lang = DEFAULT_LANG_ID;
if($in['lang_id']){
	$def_lang= $in['lang_id'];
}
if($in['lang_id']>=1000) {
	$def_lang = DEFAULT_LANG_ID;
}

switch ($def_lang) {
	case '1':
		$text = gm('Name');
		break;
	case '2':
		$text = gm('Name fr');
		break;
	case '3':
		$text = gm('Name du');
		break;
	default:
		$text = gm('Name');
		break;
}

if(!$in['cat_id'] && $in['customer_id'] && is_numeric($in['customer_id'])){
    $in['cat_id'] = $db->field('SELECT cat_id FROM customers WHERE customer_id ='.$in['customer_id']);
  }

$cat_id = $in['cat_id'];


$filter=" 1=1 ";
if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && in_array($in['app'], $accepted_variants_apps)){
    $filter.=' AND pim_article_variants.article_id IS NULL ';
}
if(!$in['from_address_id']) {
	$table = 'pim_articles LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id';
  if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && in_array($in['app'], $accepted_variants_apps)){
    $table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
  }
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,pim_articles.internal_name,
				pim_article_categories.name AS categorie,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference,
				pim_article_categories.name AS article_category,
				pim_articles.weight,
        pim_articles.has_variants,
        pim_articles.use_combined ';

}else{
	$table = 'pim_articles INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id 
					   LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id';
  if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && in_array($in['app'], $accepted_variants_apps)){
    $table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
  }
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,
				pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,
				pim_article_categories.name AS categorie,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference,
				pim_article_categories.name AS article_category,
				pim_articles.weight,
        pim_articles.has_variants,
        pim_articles.use_combined ';
}

//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

if ($in['search'])
{
	//$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_articles.supplier_reference LIKE '%".$in['search']."%' OR pim_article_categories.name LIKE '%".$in['search']."%')";
	// $arguments.="&search=".$in['search'];

	$search_words = explode(" ", $in['search']);
    $query_string2 ='';
    $all_tcl=1;
    $tcl=0;
    $len = count($search_words);

    for ($s = 0; $s < $len; $s++) {
        if ($search_words[$s]) {
/*            $query_string8 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.internal_name ";
            $query_string9 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.item_code ";
            $query_string10 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.supplier_reference ";
            $query_string11 .= "LIKE '%" . $search_words[$s] . "%' OR pim_article_categories.name ";*/

            if(strlen($search_words[$s])>3){
              $all_tcl=0;
            }else{
              $tcl=1;
            }
/*            if($len>1 && $s>0){
                  $query_string4 .= "+".$search_words[$s]." "; 
            }else{*/
              $query_string4 .= "+".$search_words[$s]."* "; 
              $query_string5 .= "+".$search_words[$s]." "; 
              if($s==0){
                $query_string6 .= "+".$search_words[$s]." "; 
              }else{
                $query_string6 .= "+".$search_words[$s]."* ";
              }
            
           /* }*/
        }
    }
    $query_string7 =SUBSTR($query_string4, 0, STRLEN($query_string4) - 2);

    if($all_tcl || $tcl ){
        $size = $len - 1;

        if($size){
            $perm = range(0, $size);
            $j = 0;

            do { 
                 foreach ($perm as $i) { $perms[$j][] = $search_words[$i]; }
            } while ($perm = pc_next_permutation($perm, $size) and ++$j);

            foreach ($perms as $p) {
                $query_string1 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.item_code ";
                $query_string2 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.internal_name ";
                $query_string3 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.supplier_reference ";
                $query_string33 .= "LIKE '%" . join(' ', $p) . "%' OR pim_article_categories.name ";
            }

            $query_string1 = SUBSTR($query_string1, 0, STRLEN($query_string1) - 27);
            $query_string2 = SUBSTR($query_string2, 0, STRLEN($query_string2) - 31);
            $query_string3 = SUBSTR($query_string3, 0, STRLEN($query_string3) - 36);
            $query_string33 = SUBSTR($query_string33, 0, STRLEN($query_string33) - 32);

        }else{
                $query_string1 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string2 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string3 .= "LIKE '%" . $search_words[0]. "%' ";
                $query_string33 .= "LIKE '%" . $search_words[0]. "%' ";

        }

        $query_string8 =SUBSTR($query_string8, 0, STRLEN($query_string8) - 31);
        $query_string9 =SUBSTR($query_string9, 0, STRLEN($query_string9) - 27);
        $query_string10 =SUBSTR($query_string10, 0, STRLEN($query_string10) - 36);
        $query_string11 =SUBSTR($query_string11, 0, STRLEN($query_string11) - 32);
        
    }
       
    //var_dump($filter);exit();
    if ($len>1){
      if($all_tcl || $tcl){ 
       /* if($tcl){
          $filter .= " AND ( pim_articles.item_code ".$query_string9."  OR pim_articles.internal_name ".$query_string8." OR pim_articles.supplier_reference ".$query_string10." OR pim_article_categories.name ".$query_string11."  ) ";
        }else{*/
          $filter .= " AND ( pim_articles.item_code ".$query_string1."  OR pim_articles.internal_name ".$query_string2." OR pim_articles.supplier_reference ".$query_string3." OR pim_article_categories.name ".$query_string33." ) ";
       /* }*/
        
      }else{
        $filter .= " AND (match(pim_articles.internal_name) against ('".$query_string4."' in boolean mode)
                    OR match(pim_articles.internal_name) against ('".$query_string5."' in boolean mode)
                    OR match(pim_articles.internal_name) against ('".$query_string6."' in boolean mode)   
                    OR match(pim_articles.internal_name) against ('".$query_string7."' in boolean mode)    
                    OR match(pim_articles.item_code) against ('".$query_string4."' in boolean mode)
                    OR match(pim_articles.supplier_reference) against ('".$query_string4."' in boolean mode) 
                    OR match(pim_article_categories.name) against ('".$query_string4."' in boolean mode)
                    OR match(pim_article_categories.name) against ('".$query_string5."' in boolean mode)
                    OR match(pim_article_categories.name) against ('".$query_string6."' in boolean mode)   
                    OR match(pim_article_categories.name) against ('".$query_string7."' in boolean mode)  
                           )";
      }
       
    }else{
        $filter .= " AND (pim_articles.internal_name LIKE '%".rtrim(ltrim($in['search']))."%'
                      OR pim_articles.item_code LIKE '%".rtrim(ltrim($in['search']))."%'
                      OR pim_articles.supplier_reference like '%" . rtrim(ltrim($in['search'])) . "%'
                      OR pim_article_categories.name like '%" . rtrim(ltrim($in['search'])) . "%' )"; 
    }


}
if ($in['hide_article_ids'])
{
	$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
	// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
}
// if ($in['lang_id'])
// {

// 	$arguments.="&lang_id=".$in['lang_id'];
// }
// if ($in['is_purchase_order'])
// {

	// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
// }
if ($in['show_stock'])
{
	$filter.=" AND pim_articles.hide_stock=0";
	// $arguments.="&show_stock=".$in['show_stock'];
}
if ($in['from_customer_id2'])
{
	$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
	// $arguments.="&from_customer_id=".$in['from_customer_id'];
}
if ($in['from_address_id2'])
{
	$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
	// $arguments.="&from_address_id=".$in['from_address_id'];
}

if($in['article_category_id']){
    $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
   // $arguments.="&article_category_id=".$in['article_category_id'];
}

if($in['only_articles']=='1'){
	$filter.=" AND pim_articles.is_service='0' ";
}

$order_by=" ORDER BY pim_articles.item_code ";
if($in['order_by']){
    if(in_array($in['order_by'], $order_by_array)){
      $order = " ASC ";
      if($in['desc'] == '1' || $in['desc']=='true'){
          $order = " DESC ";
      }
      $order_by =" ORDER BY ".$in['order_by']." ".$order;
    }
}

$articles= array( 'lines' => array());
$articles['max_rows']= (int)$db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");


$article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' GROUP BY pim_articles.article_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r);


//Get  Quotes / Orders / Purchase Orders / Invoice Modules article fields settings
$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='ORDER_FIELD_LABEL'");

if($in['is_purchase_order'] || ($in['app'] == 'po_order')){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='P_ORDER_FIELD_LABEL'");
}

if($in['app'] == 'quote'){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");	
}
if($in['app'] == 'invoice'){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='INVOICE_FIELD_LABEL'");
}

if($in['app'] == 'contract'){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='CONTRACT_FIELD_LABEL'");
}

$time = time();

$j=0;
while($article->next()){
	$vat = $db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");

	if($in['customer_id']){
		if($in['vat_regime_id']){
			if($in['vat_regime_id']<10000){
				if($in['vat_regime_id']==2){
					$vat=0;
				}
			}else{
				$vat_regime=$db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
					WHERE vat_new.id='".$in['vat_regime_id']."'");
				if(!$vat_regime){
					$vat_regime=0;
				}
				if($vat>$vat_regime){
					$vat=$vat_regime;
				}
			}			
		}			
	}

  $lang_data=$db->query("SELECT name,name2,description FROM pim_articles_lang WHERE item_id='".$article->f('article_id')."' AND pim_articles_lang.lang_id='".$def_lang."' ");

	$values = $article->next_array();

	//Purchase orders Weight Setting
	if($in['app'] == 'po_order'){
		$values['weight'] = display_number($values['weight']);	
	}
  $values['item_name']=$lang_data->f('name');
  $values['item_name2']=$lang_data->f('name2');
  $values['description']=$lang_data->f('description');
	
	$tags = array_map(function($field){
		return '/\[\!'.strtoupper($field).'\!\]/';
	},array_keys($values));

	$label = preg_replace($tags, $values, $fieldFormat);
	
	if($article->f('price_type')==1){

	    $price_value_custom_fam=$db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

        $pim_article_price_category_custom=$db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
            $price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

        }else{
       	   	$price_value=$price_value_custom_fam;

         	 //we have to apply to the base price the category spec
    	 	$cat_price_type=$db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
    	    $cat_type=$db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
    	    $price_value_type=$db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

    	    if($cat_price_type==2){
                $article_base_price=get_article_calc_price($article->f('article_id'),3);
            }else{
                $article_base_price=get_article_calc_price($article->f('article_id'),1);
            }

       		switch ($cat_type) {
				case 1:                  //discount
					if($price_value_type==1){  // %
						$price = $article_base_price - $price_value * $article_base_price / 100;
					}else{ //fix
						$price = $article_base_price - $price_value;
					}
					break;
				case 2:                 //profit margin
					if($price_value_type==1){  // %
						$price = $article_base_price + $price_value * $article_base_price / 100;
					}else{ //fix
						$price =$article_base_price + $price_value;
					}
					break;
			}
        }

	    if(!$price || $article->f('block_discount')==1 ){
        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
        }
    }else{
    	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
        if(!$price || $article->f('block_discount')==1 ){
        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
        }
    }

    $pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
  	$base_price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

    if($article->f('is_service') == 1){
      $price=$base_price;
    }

    $start= mktime(0, 0, 0);
    $end= mktime(23, 59, 59);
    $promo_price=$db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
    if($promo_price->move_next()){
    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

        }else{
            $price=$promo_price->f('price');
            $base_price = $price;
        }
    }
 	if($in['customer_id']){
  		$customer_custom_article_price=$db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['customer_id']."'");
    	if($customer_custom_article_price->move_next()){

            $price = $customer_custom_article_price->f('price');

            $base_price = $price;
       	}
   	}


  $purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");
  $code = $article->f('item_code');
  
	if($in['app']=='po_order'){
    if($in['customer_id']){
      $supplier_article = $db->query("SELECT purchasing_price, supplier_ref FROM pim_article_references WHERE article_id='".$article->f('article_id')."' AND supplier_id='".$in['customer_id']."' ");
      if($supplier_article->move_next()){
        if($supplier_article->f('purchasing_price')>0){
            $purchase_price = $supplier_article->f('purchasing_price');
          }
        $code = $supplier_article->f('supplier_ref');
      }
    }
		$base_price = $purchase_price;
		$price = $purchase_price;
	}

    $ant_stock=0;
  
   if(ALLOW_STOCK == 1){
	//items on purchase
	    $items_order=$db->field("SELECT SUM(pim_p_order_articles.quantity) 
		                      FROM  pim_p_order_articles 
		                      INNER JOIN pim_p_orders ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
		                      WHERE pim_p_order_articles.article_id='".$article->f('article_id')."'
		                      AND pim_p_orders.rdy_invoice!=0");

	    $items_received=$db->field("SELECT SUM(pim_p_orders_delivery.quantity) 
		                         FROM   pim_p_orders_delivery 
		                         INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
		                         WHERE pim_p_order_articles.article_id='".$article->f('article_id')."'");
	//items on orders	
	     $items_on_order=$db->field("SELECT SUM(pim_order_articles.quantity) 
		                      FROM  pim_order_articles 
		                      INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
		                      WHERE pim_order_articles.article_id='".$article->f('article_id')."'
		                      AND pim_orders.sent=1");

	    $items_on_received=$db->field("SELECT SUM(pim_orders_delivery.quantity) 
		                         FROM   pim_orders_delivery 
		                         INNER JOIN pim_order_articles ON pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
		                         WHERE pim_order_articles.article_id='".$article->f('article_id')."'");

	//items on project
	     $items_on_project=$db->field("SELECT SUM(project_articles.quantity) 
		                      FROM  project_articles 
		                      INNER JOIN  projects ON  projects.project_id=project_articles.project_id
		                      WHERE project_articles.article_id='".$article->f('article_id')."'
		                      AND projects.stage!=0 AND project_articles.delivered=0");

	//items on intervetions
	     $items_on_intervetion=$db->field("SELECT SUM( servicing_support_articles.quantity) 
		                      FROM   servicing_support_articles 
		                      INNER JOIN  servicing_support ON  servicing_support.service_id=servicing_support_articles.service_id
		                      WHERE servicing_support_articles.article_id='".$article->f('article_id')."'
		                      AND servicing_support.status!=0 AND servicing_support_articles.article_delivered=0");

	$ant_stock=$article->f('stock')-($items_on_order-$items_on_received+$items_on_project+$items_on_intervetion ) + ($items_order-$items_received);

  }

	/*$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");*/
	$vat_customer_id = $db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['customer_id']."'");
	$vat_customer = $db->field("SELECT value FROM vats WHERE vat_id='".$vat_customer_id."'");
	$customer_disc = $db->query("SELECT line_discount, apply_line_disc FROM customers WHERE customer_id='".$in['customer_id']."'");
  $nr_taxes = $db->field("SELECT COUNT(article_tax_id) FROM pim_articles_taxes WHERE article_id='".$article->f('article_id')."' ");

	$linie = array(
	  	'article_id'				=> $article->f('article_id'),
	  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
	  	'name'						=> htmlspecialchars_decode($article->f('internal_name')),
	  	'name2'           => $lang_data->f('name') ? htmlspecialchars(html_entity_decode($lang_data->f('name'))) : htmlspecialchars(html_entity_decode($lang_data->f('name'))),
	  	'article_category'			=> $article->f('article_category'),
	  	'weight'					=> display_number($article->f('weight')),
	    'stock'						=> $article->f('stock'),
	    //'stock2'					=> remove_zero_decimals($article->f('stock')),
	    'stock2'					=> $article->f('hide_stock')==1 || $article->f('is_service') == 1 ? '' : remove_zero_decimals_dn(display_number($article->f('stock')*1)),
	    //'ant_stock'					=> remove_zero_decimals($ant_stock),
	    'ant_stock'					=> $article->f('hide_stock')==1 || $article->f('is_service') == 1 ? '' : '('.remove_zero_decimals_dn(display_number($ant_stock)).')',
	    'quantity'		    		=> 1,
	    'pending_articles'  		=> intval($pending_articles),
	    'threshold_value'   		=> $article->f('article_threshold_value'),
	  	'sale_unit'					=> $article->f('sale_unit'),
	  	'percent'           		=> $vat_percent,
		'percent_x'         		=> display_number($vat_percent),
	    'packing'					=> $article->f('packing')>0 ? remove_zero_decimals($article->f('packing')) : 1,
	  	'code'		  	    		=> $code,
	  	'supplier_reference'		=> in_array($in['app'], $accepted_variants_apps) && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? '' : $article->f('supplier_reference'),
		'price'						=> in_array($in['app'], $accepted_variants_apps) && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price,
		'price_vat'					=> $in['remove_vat'] == 1 ? (in_array($in['app'], $accepted_variants_apps) && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0: $price) : (in_array($in['app'], $accepted_variants_apps) && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price + (($price*$vat)/100)),
		'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
		'purchase_price'			=> in_array($in['app'], $accepted_variants_apps) && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $purchase_price,
		'vat'			    		  => $in['remove_vat'] == 1 ? '0' : $vat,
		//'vat'			    		=> $vat_customer == '0' ? '0' : $vat,
		'quoteformat'    		=> html_entity_decode(gfn($label)),
		'base_price'				=> in_array($in['app'], $accepted_variants_apps) && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? place_currency(display_number_var_dec(0)) : place_currency(display_number_var_dec($base_price)),
		'show_stock'				=> $article->f('hide_stock') ? false:true,
		'hide_stock'				=> $article->f('hide_stock'),
		'is_service'				=> $article->f('is_service'),
		'allow_stock'       =>ALLOW_STOCK == 1 ? true : false,
		'line_discount'			=> $customer_disc->f('apply_line_disc')==1? $customer_disc->f('line_discount') : 0,
    'has_variants'      => in_array($in['app'], $accepted_variants_apps) && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? true : false,
    'has_variants_done' => 0,
    'is_variant_for'    => 0,
    'variant_type'      => 0,
    'is_combined'       => $article->f('use_combined'),
    'visible'           => 1,
    'show_stock_warning'=> defined('SHOW_STOCK_WARNING') && SHOW_STOCK_WARNING=='1' ? true : false,
    'has_taxes'         => $nr_taxes? true : false,
    'stock_pack'      => $article->f('hide_stock')==1 || $article->f('is_service') == 1? '' : ($article->f('stock')>0 && $article->f('packing')>0 ? remove_zero_decimals_dn(display_number($article->f('stock')/$article->f('packing'))) : '0'),

	);
	array_push($articles['lines'], $linie);
  	
}

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$articles['families']=array();
while($db->move_next()){
    $families = array(
    'name'  => $db->f('name'),
    'id'=> $db->f('id')
    );
      
    array_push($articles['families'], $families);
}

$articles['customer_id'] 		= $in['customer_id'];
$articles['lang_id'] 				= $in['lang_id'];
$articles['cat_id'] 				= $in['cat_id'];
$articles['txt_name']			  = $text;
$articles['lr']			        = $l_r;
$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;
$articles['only_articles'] 		= $in['only_articles'];
$articles['remove_vat'] 		= $in['remove_vat'];
$articles['vat_regime_id']	= $in['vat_regime_id'];
$articles['app']	          = $in['app'];
$articles['offset']         = $offset;
$articles['article_category_id']         = $in['article_category_id'];
$articles['hide_article_ids']            = $in['hide_article_ids'];
$articles['allow_stock_packing']        = ALLOW_ARTICLE_PACKING == '1' ? true : false;


if($in['from_order_page']===true){
	return json_out($articles,true,false);
}
json_out($articles);

function get_taxes($in,$showin=true,$exit=true){
	$taxes = array( 'lines'=>array());
	$db = new sqldb();
/*	$db->query("SELECT pim_articles.vat_id,pim_articles.article_id,vats.value
	                       FROM pim_articles
	                       INNER JOIN vats ON vats.vat_id=pim_articles.vat_id
	                       WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.active='1' ");
	$vat_percent=$db->f('value');*/

	$gov_tax=array();
	if(!$in['quantity']){
		$in['quantity']==1;
	}

	$get_article_taxes=$db->query("SELECT tax_id FROM pim_articles_taxes WHERE article_id='".$in['article_id']."'");
	while ($get_article_taxes->next()){
		$gov_tax[$get_article_taxes->f('tax_id')]+=$in['quantity'];
	}

	$is_gov_taxes=false;
	$total_gov_taxes=0;
	$i=0;

	foreach($gov_tax as $tax_id => $quantity){
		$gov_tax = $db->query("SELECT pim_article_tax.* ,pim_article_tax_type.name as type_name, vats.value
		                       FROM pim_article_tax
		                       LEFT JOIN pim_article_tax_type ON pim_article_tax_type.id=pim_article_tax.type_id
		                       LEFT JOIN vats ON vats.vat_id=pim_article_tax.vat_id
		                       WHERE pim_article_tax.tax_id='".$tax_id."'");
		$vat_percent=$gov_tax->f('value');
		/*if($vat_regime==2){
		  	$vat_percent=0;
		}*/
		if($in['vat_regime_id']){
			$vat_regime=$db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
					WHERE vat_new.id='".$in['vat_regime_id']."'");
			if(!$vat_regime){
				$vat_regime=0;
			}
			if($vat_percent>$vat_regime){
				$vat_percent=$vat_regime;
			}
		}

		$vat_value= $gov_tax->f('amount')*($vat_percent/100);

		$linie = array(
			'tr_id'             			=> 'tmp'.$i.strtotime('now'),
			'is_vat'						=> $in['remove_vat'] == 1 ? false : true,
			'tax_id'            			=> $tax_id,
			'tax_for_article_id'			=> $in['article_id'],
			'quantity_old'      			=> $quantity,
			'quantity'          			=> display_number($quantity),
      'quantity_component'      => 0,
			'percent'           			=> $vat_percent,
			'percent_x'         			=> display_number($vat_percent),
			'vat_value'         			=> $vat_value,
			'vat_value_x'       			=> display_number($vat_value),
			'tax_id'            			=> $tax_id,
			//'tax_name'          			=> $gov_tax->f('type_name'),
			'tax_name'          			=> $gov_tax->f('code'),
			'tax_quantity'      			=> display_number($quantity),
			'disc_val'          			=> display_number(0),
			'price'             			=> $in['exchange']==1 ? display_number_var_dec($gov_tax->f('amount')/return_value($in['ex_rate'])) : display_number_var_dec($gov_tax->f('amount')),
			'price_vat'         			=> $in['exchange']==1 ? display_number_var_dec($gov_tax->f('amount')/return_value($in['ex_rate'])+($gov_tax->f('amount')/return_value($in['ex_rate'])) * ($vat_percent/100) ) : display_number_var_dec($gov_tax->f('amount')+$gov_tax->f('amount') * ($vat_percent/100)),
			'line_total'        			=> $in['exchange']==1 ? display_number(($gov_tax->f('amount')/return_value($in['ex_rate'])) * $quantity ) : display_number($gov_tax->f('amount') * $quantity),
			//'tax_code'						=> $gov_tax->f('code'),
			'tax_code'						=> $gov_tax->f('type_name'),
			'td_width'						=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
			'input_width'					=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
			'hide_disc'						=> $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
			'allow_article_packing' 		=> $in['allow_article_packing'],
			'allow_article_sale_unit'		=>$in['allow_article_sale_unit'],
			'apply_to'						=> $gov_tax->f('apply_to'),
		);
		// $view->assign('allow_article_packing',$in['allow_article_packing'],'tax_line');
		// $view->assign('allow_article_sale_unit',$in['allow_article_sale_unit'],'tax_line');
		$total_gov_taxes+= $gov_tax->f('amount') * $quantity;

		array_push($taxes['lines'], $linie);
		$is_gov_taxes=true;
		// $view->loop('tax_line');
		$i++;
	}
	$taxes['is_gov_taxes'] = $is_gov_taxes;
	$taxes['gov_taxes_value']= display_number($total_gov_taxes);
	// $view->assign(array(
	// 	'td_width'			=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
	// ));
	return json_out($taxes, $showin, $exit);
}
function get_variant_types($in,$showin=true,$exit=true){
  $data = array( 'list'=>array(),'title'=>gm('Bulk change type of variants for').' '.$in['title']);
  $db = new sqldb();
  $db->query("SELECT name, id FROM pim_article_variant_types ORDER BY sort_order ASC");
  while($db->move_next()){
      array_push($data['list'], array('id'=>$db->f('id'),'name'=>htmlspecialchars_decode($db->f('name'))));
  }

  return json_out($data, $showin, $exit);
}

function get_components($in,$showin=true,$exit=true){
  $lines = array( 'components'=>array());
  $db = new sqldb();

  $components=$db->query("SELECT pim_articles_combined.*, pim_articles.internal_name, pim_articles.item_code FROM pim_articles_combined 
                          LEFT JOIN pim_articles on pim_articles.article_id = pim_articles_combined.article_id
                          WHERE parent_article_id='".$in['article_id']."'  ORDER BY pim_articles_combined.`sort_order` ASC ");
  $is_components=false;
  $total_components=0;
  $i=0;

  while ($components->next()){
    $purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$components->f('article_id')."' AND pim_article_prices.base_price='1'");
    
     $linie = array(
      'tr_id'                   => 'tmp'.$i.strtotime('now'),
      'is_vat'                  => false,
      'component_id'            => $components->f('pim_articles_combined_id'),
      'component_article_id'    => $components->f('article_id'),
      'component_name'          => $components->f('internal_name'),
      'component_code'          => $components->f('item_code'),
      'parent_article_id'       => $in['article_id'],
      'quantity_old'            => $components->f('quantity'),
      'quantity'                => display_number($components->f('quantity')),
      'quantity_component'      => display_number($components->f('quantity')),
      'purchase_price'          => $purchase_price,
      'percent'                 => 0,
      'percent_x'               => display_number(0),
      'vat_value'               => 0,
      'vat_value_x'             => display_number(0),
      'disc_val'                => display_number(0),
      'price'                   => display_number_var_dec(0),
      'price_vat'               => display_number_var_dec(0),
      'line_total'              => display_number(0),
      'td_width'                => ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
      'input_width'             => ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
      'hide_disc'               => $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
      'visible'                 => $components->f('visible')? true: false,
    );

    array_push($lines['components'], $linie);
    $is_components=true;
    $i++;
  }
  $lines['is_components'] = $is_components;
  return json_out($lines, $showin, $exit);
}