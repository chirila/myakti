<?php

    if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
    }
    /*
	used to get the number of unseen interventions
    */
    function get_topInfo($in,$showin=true,$exit=true){
	$db = new sqldb();
	$nr = $db->field("SELECT count(logging.log_id) FROM logging
	    LEFT JOIN logging_tracked ON logging.log_id=logging_tracked.log_id AND logging.user_id = logging_tracked.user_id
	    LEFT JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity_id
	    WHERE logging.done='0' AND logging.to_user_id='".$_SESSION['u_id']."' AND (customer_contact_activity.reminder_date > 0 OR logging.reminder_date>0) ");
	$info = $db->field("SELECT count(customer_meeting_id) as meet FROM customer_meetings
	    INNER JOIN servicing_support ON customer_meetings.service_id = servicing_support.service_id
	    WHERE notify='1' AND customer_meetings.user_id='".$_SESSION['u_id']."' ");
	$data =array('info'=>$info,'nr'=>$nr);
	return json_out($data, $showin,$exit);
    }
    /*
	checks if you can switch to another accounts
    */
    function get_switchInfo($in,$showin=true,$exit=true){
	global $database_config;
	$db = new sqldb();

	$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);
	$company_user=array();
	$users_colors=array();
	$user_letters=array();
	$db_u = new sqldb($database_users);
	//$exist = $db_u->query("SELECT * FROM db_link_user WHERE user_id1='".$_SESSION['u_id']."' OR  user_id2='".$_SESSION['u_id']."' ")->getAll();
	$exist = $db_u->query("SELECT * FROM db_link_user WHERE user_id1= :user_id1 OR  user_id2= :user_id2 ",['user_id1'=>$_SESSION['u_id'],'user_id2'=>$_SESSION['u_id']])->getAll();
	$is_accountant = false;
	if(!empty($exist[0])){
	    $i =0;
	    $users =array();
	    foreach ($exist as $key => $value) {
		if($value['user_id1'] != $_SESSION['u_id']){
		    $users[$value['id']] = get_company_byUser($db_u,$value['user_id1']);
		    $tmp_user_name=get_user_name($value['user_id1']);
		    $company_user[$value['id']] = $tmp_user_name;
		    $users_colors[$value['id']] = get_user_color($value['user_id1']);
		    preg_match_all('/(?<=\s|^)[a-z]/i', $tmp_user_name, $matches);	    
		    $user_letters[$value['id']]=strtoupper(implode('', $matches[0]));
		}
		if($value['user_id2'] != $_SESSION['u_id']){
		    $users[$value['id']] = get_company_byUser($db_u,$value['user_id2']);
		    $tmp_user_name=get_user_name($value['user_id2']);
		    $company_user[$value['id']] = $tmp_user_name;
		    $users_colors[$value['id']] = get_user_color($value['user_id2']);
		    preg_match_all('/(?<=\s|^)[a-z]/i', $tmp_user_name, $matches);	    
		    $user_letters[$value['id']]=strtoupper(implode('', $matches[0]));
		}
		$i++;
	    }
	    $users[0]=get_company_byUser($db_u,$_SESSION['u_id']);
	    $tmp_user_name=get_user_name($_SESSION['u_id']);
	    $company_user[0] = $tmp_user_name;
	    $users_colors[0] = get_user_color($_SESSION['u_id']);
	    preg_match_all('/(?<=\s|^)[a-z]/i', $tmp_user_name, $matches);	    
		$user_letters[0]=strtoupper(implode('', $matches[0]));
	}else{
	    //$acc = $db_u->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."'");
	    $acc = $db_u->query("SELECT * FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	    if($acc->f('active')=='1000'){
		// console::log('ee');
		$account_id = $db_u->field("SELECT accountant_id FROM users WHERE user_id='".$acc->f('main_user_id')."'");
		$exist = $db_u->query("SELECT user_id, database_name FROM users WHERE accountant_id='".$account_id."' ")->getAll();
		if(!empty($exist[0])){
		    foreach ($exist as $key => $value) {
			$user_id=$db_u->field("SELECT user_id FROM users WHERE database_name='".$value['database_name']."' AND active='1000' ");
			if($user_id != $_SESSION['u_id']){
			    $users[$user_id] = get_company_byUser($db_u,$value['user_id']);
			    $tmp_user_name=get_user_name($value['user_id']);
			    $company_user[$user_id] = $tmp_user_name;
			    $users_colors[$user_id] = get_user_color($value['user_id']);
			    preg_match_all('/(?<=\s|^)[a-z]/i', $tmp_user_name, $matches);	    
		    	$user_letters[$user_id]=strtoupper(implode('', $matches[0]));
			}
			$i++;
		    }
		    if($i>0){
			$is_accountant = true;
		    }
		}
		$users[0]=get_company_byUser($db_u,$_SESSION['main_u_id']);
		$tmp_user_name=get_user_name($_SESSION['main_u_id']);
		$company_user[0] = $tmp_user_name;
		$users_colors[0] = get_user_color($_SESSION['main_u_id']);
		preg_match_all('/(?<=\s|^)[a-z]/i', $tmp_user_name, $matches);	    
		$user_letters[0]=strtoupper(implode('', $matches[0]));
	    }
	}

	
	function build_sorter($key) {
	    return function ($a, $b) use ($key) {
	        return strnatcmp($a[$key], $b[$key]);
	    };
	}
	//$users = build_simple_dropdown($users);
	$tmp_users=array();
	foreach($users as $k=>$v){
		array_push($tmp_users, array('id'=>$k,'value'=>$v, 'u_name'=>$company_user[$k], 'u_color'=>$users_colors[$k], 'u_letters'=>$user_letters[$k] ));
	}
	$users=$tmp_users;
	usort($users, build_sorter('id'));

	//Check if user account is trial
	/*$user_info = $db_u->query("SELECT users.payed,users.active,user_info.end_date,user_info.is_trial FROM users 
								JOIN user_info ON users.user_id = user_info.user_id	
								WHERE users.user_id='".$_SESSION['u_id']."' ")
						->getAll();*/
	$user_info = $db_u->query("SELECT users.payed,users.active,user_info.end_date,user_info.is_trial FROM users 
								JOIN user_info ON users.user_id = user_info.user_id	
								WHERE users.user_id= :user_id ",['user_id'=>$_SESSION['u_id']])
						->getAll();						
	$user_info = $user_info[0];					

	$user_type = 3;
	if($user_info['is_trial'] == 1){
		$user_type = 4;
		if ($user_info['payed'] == 2) {
			$user_type = 5;
		}
	}
	if($user_info['active'] == 2){
		$user_type = 5;
	}
	if($user_info['active'] == 0){
		$user_type = 6;
	}

	$days_left = 0;
	$is_trial =  $user_type == 3 ? true : false;
	if($is_trial){
		$days_left = ceil(($user_info['end_date'] - time())/(24*60*60));
	}
	if($days_left < 0){
		$days_left = 0;
	}
	// End Check if user account is trial

	$wizzard_complete = $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");

	$data = array(
	    'data'	 		=> $i > 0 ? true : false,
	    'user_dd'		=> $users,
	    'is_accountant'	=> $is_accountant,
	    'is_trial'		=> $is_trial,
	    'wizzard_complete'		=> $wizzard_complete? true : false,
	    'days_left'		=> $days_left,
	);
	return json_out($data, $showin,$exit);
    }

    $result = array(
	'lang_page'		=> $_SESSION['l'],
	'topInfo'		=> get_topInfo($in,true,false),
	'switchInfo'	=> get_switchInfo($in,true,false),
	'accountName'	=> ACCOUNT_COMPANY,
	'ADV_CRM'		=> defined('ADV_CRM') && ADV_CRM == 1 ? true : false,
    );
json_out($result);

	function get_status($in,$showin=true,$exit=true){
		if($_SESSION['u_id']){
			global $config;
			$data=array(
				'hash'=>$config['allow_socket'] ? base64_encode(DATABASE_NAME.':'.$_SESSION['u_id']) : '',
				'ws_url'=>$config['allow_socket'] ? $config['socket_url'] : '',
				'allow_socket' => $config['allow_socket'] ? true : false
			);
			return json_out($data, $showin,$exit);
		}else{
			header('X-PHP-Response-Code: 401', true, 401);
    		exit();
		}		
	}


?>