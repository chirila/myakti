<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	$result=array();
	if($in['start_date']){
		$result['start_date']=strtotime($in['start_date'])*1000;
		$now			= strtotime($in['start_date']);
	}else{
		$result['start_date']=time()*1000;
		$now 				= time();
	}
	if(date('I',$now)=='1'){
		$now+=3600;
	}
	$today_of_week 		= date("N", $now);
	$today  			= date('d', $now);
	$month        		= date('n', $now);
	$year         		= date('Y', $now);
	switch ($in['period_id']) {
		case '0':
			//weekly
			$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
			$time_end = $time_start + 604799;
			$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
			break;
		case '1':
			//monthly
			$time_start = mktime(0,0,0,$month,1,$year);
			$time_end   = mktime(23,59,59,$month+1,0,$year);
			$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
			break;
		case '2':
			//custom
			if($in['from'] && !$in['to']){
				$today  			= date('d',strtotime($in['from']));
				$month        		= date('n', strtotime($in['from']));
				$year         		= date('Y', strtotime($in['from']));
				$time_start = mktime(0,0,0,$month,$today,$year);
				$time_end = mktime(0,0,0,date('n'),date('d'),date('Y'));
			}else if($in['to'] && !$in['from']){
				$today  			= date('d',strtotime($in['to']));
				$month        		= date('n', strtotime($in['to']));
				$year         		= date('Y', strtotime($in['to']));
				$time_start=946684800;
				$time_end = mktime(0,0,0,$month,$today,$year);
			}else{
				$today  			= date('d',strtotime($in['from']));
				$month        		= date('n', strtotime($in['from']));
				$year         		= date('Y', strtotime($in['from']));
				$time_start = mktime(0,0,0,$month,$today,$year);
				$today  			= date('d',strtotime($in['to']));
				$month        		= date('n', strtotime($in['to']));
				$year         		= date('Y', strtotime($in['to']));
				$time_end = mktime(0,0,0,$month,$today,$year);
			}
			$result['period_txt']='';
			break;
	}

global $database_config;

$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$dbu = new sqldb($database_users);

$filters = array('finished','opened','due');
$i = 0;
$is_data=false;
$result['row']=array();
foreach ($filters as $value) {
	$tmp_item=array();
	$filter = '';
	switch ($value) {
		case 'finished':
			$filter = " AND logging.finished='1' ";
			$text = gm('Finished');
			break;
		case 'opened':
			$filter = " AND (logging.due_date IS NULL OR logging.due_date  > '".time()."') ";
			$text = gm('Opened');
			break;
		case 'due':
			$filter = " AND logging.due_date < '".time()."' AND logging.finished<>'1' ";
			$text = gm('Due');
			break;		
	}
	$tmp_item['type']=$text;
	$comments = $db->query("SELECT customer_contact_activity.email_to, 
	count( Case When contact_activity_type=1 Then customer_contact_activity_id End  ) AS email,
	count( Case When contact_activity_type=2 Then customer_contact_activity_id End  ) AS events,
	count( Case When contact_activity_type=3 Then customer_contact_activity_id End  ) AS fax,
	count( Case When contact_activity_type=4 Then customer_contact_activity_id End  ) AS meeting,
	count( Case When contact_activity_type=5 Then customer_contact_activity_id End  ) AS phone,
	count( Case When contact_activity_type=6 Then customer_contact_activity_id End  ) AS other
	FROM customer_contact_activity 
	LEFT JOIN logging_tracked ON customer_contact_activity.email_to = logging_tracked.user_id
	LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
	WHERE customer_contact_activity.email_to<>'0' AND customer_contact_activity.date BETWEEN ".$time_start." AND ".$time_end." 
		AND customer_contact_activity.customer_contact_activity_id = logging_tracked.activity_id {$filter}
	GROUP BY customer_contact_activity.email_to 
	ORDER BY customer_contact_activity.date DESC")->getAll();
	if(count($comments) == 0){
		continue;
	}
	$tmp_item['detaile_row']=array();
	foreach ($comments as $key => $value) {
		$comment_item=array(
			'user'		=> get_user_name($value['email_to']),
			'email'		=> $value['email'],
			'events'	=> $value['events'],
			'fax'		=> $value['fax'],
			'meeting'	=> $value['meeting'],
			'phone'		=> $value['phone'],
			'other'		=> $value['other'],
			'total'		=> ($value['email']+$value['events']+$value['fax']+$value['meeting']+$value['phone']+$value['other']),
		);
		array_push($tmp_item['detaile_row'],$comment_item);
		$i++;
	}
	array_push($result['row'],$tmp_item);
}
if($i>0){
	$is_data = true;
}
$result['is_data']=$is_data;
json_out($result);

?>