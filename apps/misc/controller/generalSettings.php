<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

	function get_settings($in){
		$db = new sqldb();
		$data = array();
		if(!$_SESSION['u_id']){
			return $data;
		}
		$skip = array('SMTP_HOST','SMTP_PASSWORD','SMTP_PORT','SMTP_USERNAME','TEHNICAL_SUPPORT');
		$s = $db->query("SELECT * FROM settings");
		while ($s->next()) {
			if(in_array($s->f('constant_name'), $skip)){
				continue;
			}
			$data[$s->f('constant_name')] =  $s->f("type") == 1 ? $s->f('value') : $s->f('long_value');
		}
		$data[strtoupper('pick_date_format')]=pick_date_format();
		return $data;
	}

	function get_dispAddress($in){
		$db = new sqldb();
		if(!$_SESSION['u_id']){
			return array();
		}
		$filter = "1=1 ";

		$data = $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address WHERE $filter")->getAll();
		foreach ($data as $key => $value) {
			$data[$key]['country'] = get_country_name($value['country_id']);
		}
		return $data;

	}

	function get_main_menu($in){
		global $database_config;

		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db = new sqldb($database_users);
		$db2 = new sqldb();
		$data = array();
		if(!$_SESSION['u_id']){
			return array();
		}
		$get_anyway =$in['get_anyway'];
		//$subscription = $db->field("SELECT base_plan_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$subscription = get_subscription_plan($_SESSION['u_id']);
		$is_new_subscription = $db2->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");
		$ERP=$db2->field("SELECT value FROM settings WHERE constant_name = 'SUBSCRIPTION_TYPE' ");
		if(is_null($is_new_subscription)){
			$menu = array();
			$menu['free'] = array('1'=>'Accounts','12'=>'Catalogue','3'=>'Projects','4'=>'Invoices','2'=>'Graphs','15'=>'CashRegister','17'=>'Installations','16'=>'Stock','18'=>gm('Reports'),'19'=>gm('Timetracker'));
			$menu['services'] = array('1'=>'Accounts','12'=>'Catalogue','5'=>'Quotes','3'=>'Projects','13'=>'Intervention','4'=>'Invoices','2'=>'Graphs','15'=>'CashRegister','17'=>'Installations','16'=>'Stock','18'=>gm('Reports'),'19'=>gm('Timetracker'));
			$menu['goods'] = array('1'=>'Accounts','5'=>'Quotes','12'=>'Catalogue','6'=>'Orders','14'=>'Purchase Orders','13'=>'Intervention','4'=>'Invoices','9'=>'Webshop','2'=>'Graphs','15'=>'CashRegister','17'=>'Installations','16'=>'Stock','18'=>gm('Reports'));
			$menu['both'] = array('1'=>'Accounts','5'=>'Quotes','12'=>'Catalogue','6'=>'Orders','14'=>'Purchase Orders','3'=>'Projects','13'=>'Intervention','11'=>'Contracts','4'=>'Invoices','9'=>'Webshop','2'=>'Graphs','15'=>'CashRegister','17'=>'Installations','16'=>'Stock','18'=>gm('Reports'),'19'=>gm('Timetracker'));
			
			foreach ($menu[$_SESSION['acc_type']] as $key => $value) {
				$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_".$key."' AND user_id='".$_SESSION['u_id']."' ");
				if($val===NULL){
					$val = $db2->field("SELECT value FROM settings WHERE constant_name='MODULE_".$key."' ");
				}
				$data[$key]= $val == 1 ? true : false;
				if($key=='1' && $subscription!='7'){
					$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_DEALS' AND user_id='".$_SESSION['u_id']."' ");
					if(is_null($val)){
						$val=1;
					}
					$data["DEALS"]= $val == 1 ? true : false;
				}
				if($key=='4'){
					if($subscription!='7' && $ERP=='1'){
						$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_PURCHASE_INVOICE' AND user_id='".$_SESSION['u_id']."' ");
						if(is_null($val)){
							$val=1;
						}
						$data["PURCHASE_INVOICE"]= $val == 1 ? true : false;
					}
					$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_INVOICE_EXPORTS' AND user_id='".$_SESSION['u_id']."' ");
					if(is_null($val)){
						$val=1;
					}
					$data["INVOICE_EXPORTS"]= $val == 1 ? true : false;
				}
				if($key=='16'){
					$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_DISPATCHING' AND user_id='".$_SESSION['u_id']."' ");
					if(is_null($val)){
						$val=1;
					}
					$data["DISPATCHING"]= $val == 1 ? true : false;
				}
			}
		}else{
			
			// ,'9'=>'Webshop'
			$menu = array('1'=>'CRM','5'=>'Quotes','12'=>'Catalogue','6'=>'Orders','14'=>'Purchase Orders','3'=>'Projects','13'=>'Intervention','11'=>'Contracts','4'=>'Invoices','2'=>'Graphs','15'=>'CashRegister','16'=>'Stock','17'=>'Installations','18'=>gm('Reports'),'19'=>gm('Timetracker'));

			if($get_anyway){
				$access_array_init=explode(';',aktiUser::get('credentials', $get_anyway));
			 
			    $access_array=array();
			    foreach($access_array_init as $key=>$value){
			        $access_array[$key]=(int)$value;
			    }

			    if(defined('TACTILL_ACTIVE') && TACTILL_ACTIVE==1){
			        if(!in_array('15', $access_array)){
			            array_push($access_array,15);
			        }
			    }else{
			        $key = array_search('15', $access_array);
			        if($key!==false){
			            array_splice($access_array, $key,1);
			        }
			    }

			    array_push($access_array,'100');
			    perm::$allow_apps = sortCred($access_array);
			}
		
		  
			foreach (perm::$allow_apps as $key => $value) {
				if($menu[$value]){
					$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_".$value."' AND user_id='".$_SESSION['u_id']."' ");
					if($val===NULL){
						$val = $db2->field("SELECT value FROM settings WHERE constant_name='MODULE_".$value."' ");
					}
					$data[$value] = $val == 1 ? true : false;
					if($value=='1' && $subscription!='7' && ADV_CRM =='1'){
						$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_DEALS' AND user_id='".$_SESSION['u_id']."' ");
						if(is_null($val)){
							$val=1;
						}
						$data["DEALS"]= $val == 1 ? true : false;
					}	
					if($value == '4'){
						if($subscription!='7' && $ERP=='1'){
							$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_PURCHASE_INVOICE' AND user_id='".$_SESSION['u_id']."' ");
							if(is_null($val)){
								$val=1;
							}
							$data["PURCHASE_INVOICE"]= $val == 1 ? true : false;
						}
						$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_INVOICE_EXPORTS' AND user_id='".$_SESSION['u_id']."' ");
						if(is_null($val)){
							$val=1;
						}
						$data["INVOICE_EXPORTS"]= $val == 1 ? true : false;
					}
					if($value == '16'){
						$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_DISPATCHING' AND user_id='".$_SESSION['u_id']."' ");
						if(is_null($val)){
							$val=1;
						}
						$data["DISPATCHING"]= $val == 1 ? true : false;
					}				
				}
			}
		}

		return $data;

	}

	function get_is_admin_cred($in){
		$db = new sqldb();
		$data = array();
		if(!$_SESSION['u_id']){
			return $data;
		}
		if($_SESSION['group'] == 'admin'){
			$data = array('1','3','4','5','6','11','12', '13','14','17','19', '2','7','8','9','16', '18');
		}elseif($_SESSION['group'] == 'user'  ){
			foreach ($_SESSION['admin_sett'] as $key => $value) {
				switch ($value) {
					case 'article':
						array_push($data, '12');
						break;
					case 'company':
					    array_push($data, '1');
					    break;
					case 'order':
						array_push($data, '6');
						break;
					case 'po_order':
						array_push($data, '14');
						break;
					case 'quote':
						array_push($data, '5');
						break;
					case 'project':
						array_push($data, '3');
						break;
					case 'timetracker':
						array_push($data, '19');
						break;
					case 'service':
						array_push($data, '13');
						break;
					case 'installation':
						array_push($data, '17');
						break;
					case 'contract':
						array_push($data, '11');
						break;
					case 'invoice':
						array_push($data, '4');
						break;
				}
			}
				
		}

		return $data;
	}

	function get_show_settings(){
		$data = array();
		if(!$_SESSION['u_id']){
			return $data;
		}
		global $database_config;
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		);

		$db_users = new sqldb($db_config);
		$db=new sqldb();
		$array = array(
		    'company',
		    'article',
		    'order',
		    'po_order',
		    'quote',
		    'project',
		    'service',
		    'installation',
		    'contract',
		    'invoice',
		    'timetracker',
		    'stock'
		);
		foreach ($array as $value) {
		    $is_admin = false;
		    if(in_array($value, $_SESSION['admin_sett'])) {
		        $is_admin = true;
		    }
		    if($_SESSION['group'] == 'admin'){
		        $is_admin = true;
		    }
		    $data['is_admin_'.$value]=$is_admin;
		}
		$easyinvoice= $db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
		$is_accountant=$db_users->field("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND active=1000 ") ? true : false;
		$is_zen=$db->field("SELECT active FROM apps WHERE name='Zendesk' and type='main' AND main_app_id='0' ") ? true : false;
		$access_api_webhooks=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCESS_API_WEBHOOKS' ");
		$data['easyinvoice']	= $easyinvoice=='1' ? true : false;
	    $data['is_accountant'] = $is_accountant ? true : false;
	    $data['is_zen']        = $is_zen ? true : false;
	    $data['access_api_webhooks']   = $access_api_webhooks=='1' ? true : false;

		if($easyinvoice=='1'){
			$data['show_deliveries']	 	=false;
			$data['show_projects']			= false;
			$data['show_interventions']		= false;
			$data['noteasy']		= false;
		}else{
			$data['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
			$data['show_projects']			= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
			$data['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
			$data['noteasy']		= true;
		}
		if(defined('NEW_SUBSCRIPTION')){
		    $data['adv_crm']= ADV_CRM == 1 ? true : false;
		    $data['adv_quotes']= ADV_QUOTE == 1 ? true :  false;    
		}else{
		    $data['adv_crm']= true;
		    $data['adv_quotes']= true;    
		}
		$adv_sepa=false;
		if(defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1){
		    if(defined('ADV_SEPA') && ADV_SEPA==1 && defined('SHOW_INVOICE_SEPA') && SHOW_INVOICE_SEPA==1){
		        $adv_sepa=true;
		    }
		}else if(defined('SHOW_INVOICE_SEPA') && SHOW_INVOICE_SEPA==1){
		    $adv_sepa=true;
		}
		$data['show_sepa']=$adv_sepa;
		$subscription = $db_users->field("SELECT base_plan_id FROM users WHERE `user_id`='".$_SESSION['u_id']."' ");
		$data['not_easyinv_subscription']=$subscription=='7'? false : true;

		$user_special = $db->field("SELECT value FROM settings WHERE constant_name = 'ONLY_IF_ACC_MANAG' ");
		$user_special1 = $db->field("SELECT value FROM settings WHERE constant_name = 'ONLY_IF_ACC_MANAG2' ");
		if($user_special==1 && $user_special1 ==1 && $_SESSION['group']=='user'){
		    $special_user=false;
		}else{
		    $special_user=true;
		}
		$data['special_user']=$special_user;
		$is = false;
		for ($i = 1; $i<=19; $i++){
		    if(in_array($i,perm::$allow_apps)){
		        $is = true;
		    }
		    if($easyinvoice==1){
		        if($i!='4'){
		            $data['is_'.$i]= false;
		        }else{
		            $data['is_'.$i]= $is;
		        }
		    }else{
		        $data['is_'.$i]= $is;
		    }    
		    $is = false;
		}
		$credentials = $db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$credentials = explode(';', $credentials);
		$showcatalog = true;
		$showcataloginv = true;

		if(!in_array(12,$credentials) || !in_array(6,$credentials)){
		        $showcatalog = false;
		    }
		    if(!in_array(12,$credentials)){
		        $showcataloginv = false;
		    }
		    if($easyinvoice==1){
		    $showcatalog=false;
		    $showcataloginv = false;
		}
		$data['showcatalog']= $showcatalog;
		$data['showcataloginv']= $showcataloginv;
		$data['unique_id']=DATABASE_NAME;
		$data['view_all_interventions']=true;
		$intervention_perm_manager = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'intervention_admin']);
		if(!$data['is_admin_service'] && !$intervention_perm_manager && in_array('13', perm::$allow_apps) && defined('ONLY_IF_INT_USER') && ONLY_IF_INT_USER == 1){
			$data['view_all_interventions']=false;
		}
		$data['not_easy_invoice_diy']=aktiUser::get('is_easy_invoice_diy') ? false : true;
		$data['not_easy_invoice_billtobox']=aktiUser::get('is_easy_invoice_billtobox') ? false : true;
		$data['pricing_plan_id']=(int)aktiUser::get('subscription_pricing_plan_id');

		return $data;
	}

	global $config;
	# be carefull, this options reset properties in the helper services in angular
	$result = array(
		'settings'				=> get_settings($in),
		'base_url'				=> $config['site_url'],
		'main_menu'				=> get_main_menu($in),
		'is_admin_for'			=> get_is_admin_cred($in),
		'show_settings'			=> get_show_settings()
		// 'dispAddresss'		=> get_dispAddress($in,true,false),
	);
json_out($result);
