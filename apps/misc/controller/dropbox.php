<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
global $config;

$drop = false;
$result=array('dropbox_files'=>array(),'dropbox_images'=>array());
if(defined('DROPBOX') && DROPBOX != ''){
	$drop = true;
	if($in['drop_folder'] && ($in['drop_folder']=='installations' || $in['drop_folder']=='interventions')){
		$d = new drop($in['drop_folder'], $in['customer_id'], $in['item_id'],true,$in['folder'],$in['isConcact'],$in['serial_number']);
		$files = $d->getContent();
		$i=0;
		if(!empty($files->entries)){
			$temp_data=array();
			$temp_images=array();
			foreach ($files->entries as $key => $value) {
				$l = $d->getLink(urldecode($value->path_display));
				$fls=array(
					'link'			=> $l,
					'file' 			=> $in['serial_number'].'/'.$value->name,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$value->path_display,'customer_id'=>$in['customer_id'],'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'isConcact'=>$in['isConcact'],'serial_number'=>$in['serial_number']),
					'down_file_link'		=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($value->path_display).'&customer_id='.$in['customer_id'].'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$value->mime_type.'&file='.urlencode(str_replace('/','_', $value->path_display)).'&isConcact='.$in['isConcact'].'&serial_number='.$in['serial_number'],
					'file_id'			=> 'drop_'.$i,
					'path'			=> $value->path_display,
					'name'			=> $value->name,
				);
				array_push($temp_data, $fls);
				$i++;
			}
			foreach($temp_data as $key=>$value){
				if(strrpos($value['name'], ".jpg") === false && strrpos($value['name'], ".jpeg") === false && strrpos($value['name'], ".png") === false && strrpos($value['name'], ".gif") === false && strrpos($value['name'], ".tiff") === false && strrpos($value['name'], ".tif") === false && strrpos($value['name'], ".bmp") === false){
					array_push($result['dropbox_files'], $value);				
				}else{
					$value['url']=str_replace("?dl=0","?raw=1",$value['link']);
					array_push($result['dropbox_images'], $value);
				}
			}
			$result['dropbox_is_file']		= true;
		}
	}else if($in['drop_folder'] && $in['customer_id']){
		$d = new drop($in['drop_folder'], $in['customer_id'], $in['item_id'],true,$in['folder'],$in['isConcact'],$in['serial_number']);
		$files = $d->getContent();
		$i=0;
		if(!empty($files->entries)){
			foreach ($files->entries as $key => $value) {
				$l = $d->getLink(urldecode($value->path_display));
				$fls=array(
					'link'			=> $l,
					'file' 			=> $in['serial_number'].'/'.$value->name,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$value->path_display,'customer_id'=>$in['customer_id'],'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'isConcact'=>$in['isConcact'],'serial_number'=>$in['serial_number']),
					'down_file_link'		=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($value->path_display).'&customer_id='.$in['customer_id'].'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$value->mime_type.'&file='.urlencode(str_replace('/','_', $value->path_display)).'&isConcact='.$in['isConcact'].'&serial_number='.$in['serial_number'],
					'file_id'			=> 'drop_'.$i,
					'path'			=> $value->path_display,
				);
				array_push($result['dropbox_files'], $fls);
				//$view->loop('files');
				$i++;
			}
			$result['dropbox_is_file']		= true;
		}
	}elseif($in['drop_folder'] && $in['is_batch']){
		$d = new drop($in['drop_folder'], null, $in['item_id'],true,$in['folder'],null,null,null,true);
		$files = $d->getContent();
		$i=0;
		if(!empty($files->entries)){
			foreach ($files->entries as $key => $value) {
				$l = $d->getLink(urldecode($value->path_display));
				$fls=array(
					'link'			=> $l,
					'file' 			=> $value->path_display,
					'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$value->path_display,'item_id'=>$in['item_id'],'drop_folder'=>$in['drop_folder'],'is_batch=1'),
					'down_file_link'		=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($value->path_display).'&item_id='.$in['item_id'].'&for='.urlencode($in['drop_folder']).'&type='.$value->mime_type.'&file='.urlencode(str_replace('/','_', $value->path_display)).'&is_batch=1',
					'file_id'			=> 'drop_'.$i,
					'path'			=> $value->path_display,
				);
				array_push($result['dropbox_files'], $fls);
				//$view->loop('files');
				$i++;
			}
			$result['dropbox_is_file']		= true;
		}
	}
}

	$result['dropbox']		= $drop;
	$result['SHOW_TABLE']		= $i > 0 ? true : false;
	$result['folder']			= DATABASE_NAME;
	$result['e']			= msg::$error;
	$result['drop_folder']		= $in['drop_folder'];
	$result['customer_id']		= $in['customer_id'];
	$result['item_id']		= $in['item_id'];
	$result['is_contact']		= $in['isConcact'];
	$result['serial_number']	= $in['serial_number'];
	$result['is_batch']		= $in['is_batch'];
	$result['nr']			= $i;

return json_out($result);