<?php if (!defined('BASEPATH'))	exit('No direct script access allowed');
session_write_close();
//global $config;
$o=array();
$now = time();
$pag = $in['pag'];
if($in['old_frame']){
	$pag = $in['pag_old'];
}

if($in['moreCtms'] == 'true' || !$in['moreCtms']){
	$limit=' LIMIT 3';
}else
{
	$limit='';
}
// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
$max = $db->field("SELECT count(log_id) FROM logging
						WHERE pag='".$pag."' AND field_name='".$in['field_name']."' AND field_value='".$in['field_value']."' AND type='1'
						ORDER BY date DESC ");

$comments = $db->query("SELECT * FROM logging
						WHERE pag='".$pag."' AND field_name='".$in['field_name']."' AND field_value='".$in['field_value']."' AND type='1'
						ORDER BY date DESC ".$limit);
$i=0;
$o['comments']=array();
while ($comments->next()) {

	$comment=array(
		'name' 			=> $comments->f('user_id') ? get_user_name($comments->f('user_id')) : $comments->f('customer_name'),
		'message'		=> nl2br($comments->f('message')),
		'time'			=> time_ago($comments->f('date'),$now),
		'id'			=> $comments->f('log_id'),
		'hide_comment'	=> $i > 2 ? true : false,
		'field_value' 	=> $in['field_value'],
		'field_name' 	=> $in['field_name'],
		'pag' 			=> $pag,
		'assign_to'		=> $comments->f('to_user_id') ? true : false,
		'to'			=> $comments->f('to_user_id') ? get_user_name($comments->f('to_user_id')) : '',
		'strike'		=> $comments->f('done') ? 'strike' : '',
		'checked'		=> $comments->f('done') ? 'checked' : '',
		'due_date'		=> $comments->f('due_date')? date(ACCOUNT_DATE_FORMAT,$comments->f('due_date')):'',
	);
	array_push($o['comments'], $comment);
	/*$view->loop('comments');*/
	$i++;
}

if($i>0){
	$o['add_comment']		= true;
}else{
	$o['add_comment']		= false;
}
	
$o['format']=pick_date_format();

if($max>3){
	$o['comment'] 		= true;
	$o['count']			= $max-3;
}

json_out($o);
?>
