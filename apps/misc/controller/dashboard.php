<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
	$output=array();

	if($_SESSION['u_id']){

		if($in['xget']){
		    $fname = 'get_'.$in['xget'];
		    $fname($in,false);
		}

		$YodaDashboardGraph = aktiUser::get('YodaDashboardGraph');
		$YodaDashboardGraph = explode(';', $YodaDashboardGraph);

		$graphs = array('1'=>gm('Turnover'),'2'=>gm('Cumulative'));

		$graphs_option = array('1'=>'turnover','2'=>'turnoverCumulate');
		$output['graphs'] = array();
		foreach ($graphs as $key => $value) {
			$show=false;
			if(!in_array($key, $YodaDashboardGraph)){
				$show=true;
			}
			$output['graphs'][] = array('name'=>$value,'show'=>$show,'option'=>$graphs_option[$key],'key'=>$key);
		}

		$output['showGdpr']=false;
		if($_SESSION['time_login'] && $_SESSION['showGdpr'] && !$output['gdpr_first'] && time()-$_SESSION['time_login']<15){
			$output['showGdpr']=true;
			$output['gdpr_first']=true;
		}

		$turnover = get_turnover();
		$output['turnover']			= $turnover['fix'];
		$output['turnoverCumulate']	= $turnover['cumulate'];

	}

	json_out($output);


	function get_turnover(){

		$db= new sqldb();
		$negative = 1;
		if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
			$negative = -1;
		}
		$inv = array();
		$ina = array(date('Y')=>array(),'cumulated '.date('Y')=>array(),date('Y')-1=>array(),'cumulated '.date('Y')-1=>array());
		$i=1;
		//current_year
		$months=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
		$months_credit=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
		$months_cumulated = array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
		$current_year = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr,  MONTH( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) ) AS `month` , YEAR( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) ) AS `year`,
		SUM(tblinvoice.amount) as sum_amount
		FROM `tblinvoice`
		WHERE YEAR( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) )=YEAR(CURDATE()) AND sent='1' AND f_archived='0' AND type='0' AND c_invoice_id='0'
		GROUP BY `year` , `month` ASC");
		while ($current_year->move_next()){
			if($current_year->f('month') <= date('m')){
				$months[$current_year->f('month')]= number_format($current_year->f('sum_amount'),2,'.','');
			}
		}
		#credit note
		$current_year_credit = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr,  MONTH( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) ) AS `month` , YEAR( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) ) AS `year`,
		SUM(tblinvoice.amount) as sum_amount
		FROM `tblinvoice`
		WHERE YEAR( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) )=YEAR(CURDATE()) AND sent='1' AND f_archived='0' AND type='2' 
		GROUP BY `year` , `month` ASC");
		while ($current_year_credit->move_next()){
			$months_credit[$current_year_credit->f('month')]= number_format($current_year_credit->f('sum_amount'),2,'.','');
		}
		for ($i = 1; $i <= 12; $i++) {
			if($i <= date('m')){
				if($months_credit[$i]>0){
					$months[$i]-= $months_credit[$i];
				}else{
					$months[$i]+= $months_credit[$i];
				}
				$months_cumulate+=$months[$i];
			}else{
				$months_cumulate = ($i>=2 ? $months_cumulated[$i-1] : 0);
			}
			$months_cumulated[$i] = $months_cumulate;

		}
		//last year
		$months_last_year=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
		$months_last_year_credit=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
		$months_cumulated_last_year=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);

		$j=0;
		$last_year = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
		SUM(tblinvoice.amount) as sum_amount
		FROM `tblinvoice`
		WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()- INTERVAL 1 YEAR)  AND sent='1' AND f_archived='0' AND type='0' AND c_invoice_id='0'
		GROUP BY `year` , `month` ASC");
		while ($last_year->move_next()){
			$months_last_year[$last_year->f('month')]=number_format($last_year->f('sum_amount'),2,'.','');
			$j++;
		}

		if($j){
			$last_year_credit = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
			SUM(tblinvoice.amount) as sum_amount
			FROM `tblinvoice`
			WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()- INTERVAL 1 YEAR)  AND sent='1' AND f_archived='0' AND type='2' AND c_invoice_id!='0'
			GROUP BY `year` , `month` ASC");
			while ($last_year_credit->move_next()){
				$months_last_year_credit[$last_year_credit->f('month')]=number_format($last_year_credit->f('sum_amount'),2,'.','');
			}
		}

		for ($i = 1; $i <= 12; $i++) {
			if($months_last_year_credit[$i]>0){
				$months_last_year[$i]-=$months_last_year_credit[$i];
			}else{
				$months_last_year[$i]+=$months_last_year_credit[$i];
			}
			$months_cumulate_last_year+=$months_last_year[$i];
			$months_cumulated_last_year[$i] = $months_cumulate_last_year;

		}

		$data = array(
		    'labels'=>array(),
		    'data'=>array(),
		    'series'=>array(gm('').' '.date('Y'),gm('').' '.date('Y', strtotime('-1 year'))),
		    'colors'=>array('#211c4e','#e6e6e6'),
		    'opt'=>array(
		        'responsive'=> true,
	            'legend'=>array(
	                'display'=> true,
	                'position'=> 'bottom',
	                'labels'=> array(
	                    'boxWidth'=> 10
	                    )
	                ),
	            'hover'=> array(
	                'mode'=> 'label'
	            	)
	        	),
	        'datasetOverride' => array( 'lineTension'=> 0.3 , 'fill'=> true, 'pointBorderWidth'=> 0, 'pointRadius'=> 4, 'pointHoverRadius'=> 5 ),
		    'chart_type'=>'line');
		$dataCumulate = array(
		    'labels'=>array(),
		    'data'=>array(),
		    'series'=>array(gm('').' '.date('Y'),gm('').' '.date('Y', strtotime('-1 year'))),
		    'colors'=>array('#211c4e','#e6e6e6'),
			
		    'opt'=>array(
		        'responsive'=> true,
	            'legend'=>array(
	                'display'=> true,
	                'position'=> 'bottom',
	                'labels'=> array(
	                    'boxWidth'=> 0
	                    )
	                ),
	            'hover'=> array(
	                'mode'=> 'label'
	            )
	        ),
	        'datasetOverride' => array( 'lineTension'=> 0.3 , 'fill'=> true, 'pointBorderWidth'=> 0, 'pointRadius'=> 4, 'pointHoverRadius'=> 5 ),
		    'chart_type'=>'line');

		#year
		$first = array();
		foreach ($months as $key => $value) {
			$data['labels'][] = str_pad($key,2,"0",STR_PAD_LEFT);
			$first[] = $value;
		}
		array_push($data['data'], $first);

		$firstC = array();
		foreach ($months_cumulated as $key => $value) {
			$dataCumulate['labels'][] = str_pad($key,2,"0",STR_PAD_LEFT);
			$firstC[] = $value;
		}
		array_push($dataCumulate['data'], $firstC);

		$second = array();
		foreach ($months_last_year as $key => $value) {
			$second[] = $value;
		}
		array_push($data['data'], $second);

		$secondC = array();
		foreach ($months_cumulated_last_year as $key => $value) {
			$secondC[] = $value;
		}
		array_push($dataCumulate['data'], $secondC);

		return array('fix'=>$data,'cumulate'=>$dataCumulate);
	}

	function get_KPI(){
		session_write_close();
		ini_set('memory_limit','1G');
		$db= new sqldb();
		$result=array();

		$invoicesarr = array();

		$now = time();
		$lateinvarr = array();
		$invs = $db->query("SELECT amount,id,amount_vat,not_paid, due_date FROM tblinvoice WHERE paid!='1' AND sent='1' AND f_archived='0' AND type!='2' AND type!='1' AND c_invoice_id='0' ");
		while ($invs->next()) {
		    if($invs->f('not_paid')==1){
			$not_paid_amount = get_not_paid_invoice($invs->f('id'));
			$invoicesarr[$invs->f('id')] = array($invs->f('amount')-$not_paid_amount[0],$invs->f('amount_vat')-$not_paid_amount[1]);
		    }else{
		        $invoicesarr[$invs->f('id')] = array($invs->f('amount'),$invs->f('amount_vat'));
		    }
		    if($invs->f('due_date') < $now){
				if($invs->f('not_paid')==1){
				    $not_paid_amount = get_not_paid_invoice($invs->f('id'));
			        $lateinvarr[$invs->f('id')] = array($invs->f('amount')-$not_paid_amount[0],$invs->f('amount_vat')-$not_paid_amount[1]);
				}else{
				    $lateinvarr[$invs->f('id')] = array($invs->f('amount'),$invs->f('amount_vat'));
				}
		    }
		}

		$paysarr = array();
		$paylatesarr = array();
		$invs = $db->query("SELECT tblinvoice_payments.amount,tblinvoice_payments.invoice_id FROM tblinvoice_payments
		    INNER JOIN tblinvoice ON tblinvoice_payments.invoice_id=tblinvoice.id
		    WHERE tblinvoice.paid!='1' AND tblinvoice.sent='1' AND tblinvoice.f_archived='0' AND tblinvoice.type!='2' AND tblinvoice.type!='1' AND tblinvoice.c_invoice_id='0'  ");
		while ($invs->next()) {
		    $paysarr[$invs->f('invoice_id')] += $invs->f('amount');
		}

		$totalNoVat = 0;
		$totalWithVat = 0;
		foreach ($invoicesarr as $key => $value) {
		    if($value[0]-$paysarr[$key] >0){
				$totalNoVat+=$value[0]-$paysarr[$key];
		    }
		    if($value[1]-$paysarr[$key] >0){
				$totalWithVat+=$value[1]-$paysarr[$key];
		    }
		}
		#lates
		$totalLateNoVat = 0;
		$totalLateWithVat = 0;
		foreach ($lateinvarr as $key => $value) {
		    if($value[0]-$paysarr[$key] >0){
				$totalLateNoVat+=$value[0]-$paysarr[$key];
		    }
		    if($value[1]-$paysarr[$key] >0){
				$totalLateWithVat+=$value[1]-$paysarr[$key];
		    }
		}

		$f_d_month = mktime(0,0,0,date('n'),1,date('Y'));
		$l_d_month = mktime(23,59,59,date('n')+1,0,date('Y'));
		$invoice_amount_month = $db->query("SELECT amount AS a, id AS c, amount_vat AS av, type, c_invoice_id FROM tblinvoice WHERE invoice_date BETWEEN '".$f_d_month."' AND '".$l_d_month."' AND sent='1' AND f_archived='0' ")->getAll();
		$invoice_amount_month_a = array('a'=>0,'av'=>0);
		foreach ($invoice_amount_month as $key => $value) {
		    if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
				$invoice_amount_month_a['a']+=$value['a'];
				$invoice_amount_month_a['av']+=$value['av'];
		    }
		    if($value['type'] == '2'){ # credit invoices	
				if($value['a']>0){
					$invoice_amount_month_a['a']-=$value['a'];
				}else{
					$invoice_amount_month_a['a']+=$value['a'];
				}

				if($value['av']>0){
					$invoice_amount_month_a['av']-=$value['av'];
				}else{
					$invoice_amount_month_a['av']+=$value['av'];
				}
		    }
		}

		$f_d_year = mktime(0,0,0,1,1,date('Y'));
		$l_d_year = mktime(23,59,59,12,31,date('Y'));
		$invoice_amount_year = $db->query("SELECT amount AS a, id AS c, amount_vat AS av, type, c_invoice_id FROM tblinvoice WHERE invoice_date BETWEEN '".$f_d_year."' AND '".$l_d_year."' AND sent='1' AND f_archived='0' ")->getAll();
		$invoice_amount_year_a = array('a'=>0,'av'=>0);
		foreach ($invoice_amount_year as $key => $value) {
		    if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
				$invoice_amount_year_a['a']+=$value['a'];
				$invoice_amount_year_a['av']+=$value['av'];
		    }
		    if($value['type'] == '2'){ # credit invoices	
				if($value['a']>0){
					$invoice_amount_year_a['a']-=$value['a'];
				}else{
					$invoice_amount_year_a['a']+=$value['a'];
				}

				if($value['av']>0){
					$invoice_amount_year_a['av']-=$value['av'];
				}else{
					$invoice_amount_year_a['av']+=$value['av'];
				}
		    }
		}

		$result['amount_to_be_inv_year']=place_currency(display_number($invoice_amount_year_a['a']));
		$result['amount_to_be_inv_year_with_btw']=place_currency(display_number($invoice_amount_year_a['av']));
	   
	   	$result['amount_to_be_inv_month']=place_currency(display_number($invoice_amount_month_a['a']));
		$result['amount_to_be_inv_month_with_btw']=place_currency(display_number($invoice_amount_month_a['av']));

		$result['amount_to_be_inv_late_with_btw']=place_currency(display_number($totalLateWithVat));
		$result['amount_to_be_inv_late']=place_currency(display_number($totalLateNoVat));

		$result['amount_to_be_inv_with_btw']=place_currency(display_number($totalWithVat));
		$result['amount_to_be_inv']=place_currency(display_number($totalNoVat));	
		
		json_out($result);
	}

?>