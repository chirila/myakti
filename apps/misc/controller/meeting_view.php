<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	$result=array();
	$rights = false;
	if(!$in['user_id'])
	{
		$in['user_id'] = $_SESSION['u_id'];
		$rights = true;
	}else if($in['user_id']!=$_SESSION['u_id'])
	{
		$is_shared = $db->query("SELECT share_calendar_id, rights FROM customer_shared_calendars WHERE user_id='".$in['user_id']."' AND to_user='".$_SESSION['u_id']."' AND rights!='0'");
		if(!$is_shared->next())
		{
			$in['user_id'] = $_SESSION['u_id'];
			$rights = true;
		}else
		{
			if($is_shared->f('rights')=='2')
			{
				$rights = true;
			}
		}
	}else
	{
		$in['user_id'] = $_SESSION['u_id'];
		$rights = true;
	}
	$meetings = $db->query("SELECT * FROM customer_meetings WHERE customer_meeting_id='".$in['log_id']."' ");
	if($meetings->next()){
		$contact_name = array();
		$contacts = $db->query("SELECT * FROM customer_contact_activity_contacts WHERE activity_id='".$meetings->f('customer_meeting_id')."' AND action_type IN (1,2)");
		while($contacts->next())
		{
			array_push($contact_name, array('id'=>$contacts->f('contact_id'),'name'=>get_contact_name($contacts->f('contact_id'))));
		}
		if($meetings->f('type')==3){
			$rights = false;
		}
		$time_value1 = $meetings->f('start_date');
		$time_value2 = $meetings->f('end_date');
		$result['from']	 		= get_user_name($meetings->f('user_id'));
		$result['subject']	 	= $meetings->f('subject');
		$result['meeting_id']	= $meetings->f('customer_meeting_id');
		$result['message']	 	= nl2br($meetings->f('message'));
		$result['date']			= date(ACCOUNT_DATE_FORMAT.' H:i',$time_value1);
		$result['end_date']		= date(ACCOUNT_DATE_FORMAT.' H:i',$time_value2);
		$result['type']			= $meetings->f('type')==1? 'meeting':( $meetings->f('type')==2?'appointment':( $meetings->f('type')==3?'intervention':'task'));
		$result['type_nr']		= $meetings->f('type');
		$result['data']			= true;
		$result['location']		= $meetings->f('location');
		$result['contact_name']	= $contact_name;
		$result['rights']		= $rights;
		$result['company_name']	= $db->field("SELECT name FROM customers WHERE customer_id='".$meetings->f('customer_id')."' ");
		$result['customer_id']	= $meetings->f('customer_id');
		$result['service_id']	= $meetings->f('service_id');
	}

return json_out($result);