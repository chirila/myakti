<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();
$customer = $db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ")->getAll();

if(!$customer[0]['lead_source']){
	$customer[0]['lead_source']=undefined;
}
if(!$customer[0]['sector']){
	$customer[0]['sector']=undefined;
}
if(!$customer[0]['language']){
	$customer[0]['language']=undefined;
}
if(!$customer[0]['lead_source']){
	$customer[0]['lead_source']=undefined;
}
if(!$customer[0]['activity']){
	$customer[0]['activity']=undefined;
}
if(!$customer[0]['various1']){
	$customer[0]['various1']=undefined;
}
if(!$customer[0]['various2']){
	$customer[0]['various2']=undefined;
}
if(!$customer[0]['legal_type']){
	$customer[0]['legal_type']=undefined;
}
if($customer[0]['is_supplier']==1){
	$customer[0]['is_supplier']=true;
}else{
	$customer[0]['is_supplier']=false;
}
if($customer[0]['acc_manager_name']){
	$customer[0]['acc_manager'] = explode(',', $customer[0]['acc_manager_name']);
	$customer[0]['user_id'] = explode(',', $customer[0]['user_id']);
}
if($customer[0]['c_type']){
	$customer[0]['c_type'] = explode(',', $customer[0]['c_type']);
}
// var_dump($customer[0]['vat_id']);
if(!$customer[0]['vat_id']){
	$vat = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."' ");
	$customer[0]['vat_id'] = $vat;
}

if(!$customer[0]['currency_id']){
	$customer[0]['currency_id'] = ACCOUNT_CURRENCY_TYPE;
}

if($customer[0]['creation_date']){
	$customer[0]['creation_date'] = date(ACCOUNT_DATE_FORMAT,$customer[0]['creation_date']);
}

if($customer[0]['apply_fix_disc']=='1'){
	$customer[0]['apply_fix_disc']=true;
}

if($customer[0]['apply_line_disc']=='1'){
	$customer[0]['apply_line_disc']=true;
}

$customer[0]['payment_term_start'] = $customer[0]['payment_term_type'];

$is_siret = false;
$country_id = $db->field("SELECT country_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' ");
if( $country_id=='79' || $country_id=='132' ){ //france and luxembourg
	$is_siret = true;
}

$label = $db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY field_id ");
$all = $label->getAll();
$customer[0]['extra']=array();
$result['extra'] = array();
foreach ($all as $key => $value) {
	$f_value = $db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$value['field_id']."' ");	
	if($value['is_dd']==1){
		$result['extra'][$value['field_id']]=build_extra_field_dd('',$value['field_id']);
		if(!$f_value){
			$f_value = undefined; //'0'
		}
	}
	$customer[0]['extra'][$value['field_id']]=$f_value;
}
$address = $db->query("SELECT * FROM  customer_addresses WHERE customer_id='".$in['customer_id']."' and is_primary=1 ")->getAll();
foreach ($address[0] as $key => $value) {
	$customer[0][$key] = $value;
}

$result['obj']						= $customer[0];
$result['obj']['add_customer']		= $in['add_customer'] || !isset($in['add_customer'])? true : false;
$result['obj']['add_contact']		= $in['add_contact'] ? true : false;
$result['customer_contact_language']		= build_language_dd();
$result['customer_lead_source']			= build_l_source_dd();
$result['customer_activity']				= build_c_activity_dd();
$result['various1']					= build_various_dd('','1');
$result['various2']					= build_various_dd('','2');
$result['legal_type']				= build_l_type_dd();
$result['customer_sector']			= build_s_type_dd();
$result['customer_type']			= getCustomerType();
$result['accountManager']			= getAccountManager();
$result['sales_rep']				= getSales();
$result['vat_dd']					= build_vat_dd();
$result['vat_regim_dd']				= build_vat_regime_dd();
$result['currency_dd']				= build_currency_list();
$result['cat_dd']					= build_cat_dd();
$result['lang_dd']					= build_language_dd_new();
$result['multiple_dd'] 				= build_identity_dd();
$result['is_siret']					= $is_siret;
$result['is_admin']					= $_SESSION['access_level'] == 1 ? true : false;
$result['obj']['do']				= 'misc-EditCustomer-misc-updateCustomer';
$result['is_add']					= false;
$result['obj']['country_dd']		= build_country_list(0);
$result['obj']['country_id']		= $result['obj']['country_id'] ? $result['obj']['country_id'] : ACCOUNT_BILLING_COUNTRY_ID;
if($in['financial_tab']){
	$result['obj']['financial_tab']=true;
}
if($in['changed_invoice_preference']){
	$result['obj']['changed_invoice_preference']=true;
}
/*$label = $db->query("SELECT * FROM customer_fields WHERE value = '1' AND is_dd = 1 ORDER BY field_id ");
$all = $label->getAll();

foreach ($all as $key => $value) {
	
}*/

//Build Sorted Field Section
$queryF = $db->query("SELECT * FROM customise_field WHERE controller='company-customer'  AND value=1 ORDER BY sort_order ASC");
$queryExtraF = $db->query("SELECT * FROM customer_fields WHERE value = '1'  ORDER BY sort_order ASC ");

while ($queryF->next()) {
$fieldInfo = customerStaticFieldsInfo($queryF);
	if(!empty($fieldInfo['label'])){
		$result['obj']['linesObj'][] = $fieldInfo; 
	}	
}

while ($queryExtraF->next()) {
	$f_value = $db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$queryExtraF->f('field_id')."' ");
	if($in['customer_id']=='tmp'){
		$creation=$queryExtraF->f('creation_value') == 1 ? true:false;
	}else{
		$creation= true;
	}

	if($queryExtraF->f('is_dd')==1){
		$extra_field_dd_name = $db->field("SELECT name
										FROM customer_extrafield_dd
										INNER JOIN customer_field ON ( customer_extrafield_dd.extra_field_id = customer_field.field_id
										AND customer_extrafield_dd.id = customer_field.value )
										WHERE extra_field_id =  '".$queryExtraF->f('field_id')."'
										AND customer_id='".$in['customer_id']."' ");
	}

	$result['obj']['linesObj'][] = array(
		'field_id'			=> $queryExtraF->f('field_id'),
		'label'				=> $queryExtraF->f('label'),
		'label_if'			=> $creation,
		'value'				=> $f_value ? $f_value : '',
		'normal_input'		=> $queryExtraF->f('is_dd')== 1 ? false:true,
		'extra_field_dd'	=> $queryExtraF->f('is_dd')== 1 ? build_extra_field_dd($f_value,$queryExtraF->f('field_id')):'',
		// 'is_admin'			=> $_SESSION['access_level') == 1 ? true : false,
		'extra_field_name' 	=> $queryExtraF->f('is_dd') == 1 ? $extra_field_dd_name : ( $f_value ? $f_value : '' ),
		'is_custom'			=> true,
		'sort_order'		=> $queryExtraF->f('sort_order'),
		);
}

usort($result['obj']['linesObj'], "myCustomSort");
//End Build Sorted Field Section

function customerStaticFieldsInfo($query){
	$fieldLabelName = '';
	$ifCondition = '';
	$isDD = false;
	$selectizeConfig = '';
	$selectizeOptions = '';
	$model = '';
	$isCustom = false;
	$sortOrder = $query->f('sort_order');
	$hasTable = false;
	$table = '';
	$isCheckbox = false;

	switch($query->f('field')){
		case 'legal_type':
			$fieldLabelName = gm('Legal Type');
			$ifCondition = 'legal_type_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'legal_type';
			$model = 'legal_type';
			$hasTable = true;
			$table = 'customer_legal_type';
			break;
		case 'commercial_name':
			$fieldLabelName = gm('Commercial Name');
			$ifCondition = 'commercial_name_if';
			$isDD = false;
			$model = 'commercial_name'; 
			break;
		case 'our_reference':
			$fieldLabelName = gm('Our reference');
			$ifCondition = 'our_reference_if';
			$model = 'our_reference';
			break;
		case 'sector':
			$fieldLabelName = gm('Activity Sector');
			$ifCondition = 'sector_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_sector';
			$hasTable = true;
			$table = 'customer_sector';
			$model = 'sector';
			break;
		case 'c_type':
			$fieldLabelName = gm('Type of relationship');
			$ifCondition = 'c_type_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_type';
			$hasTable = true;
			$table = 'customer_type';
			$model = 'c_type';
			break;
		case 'language':
			$fieldLabelName = gm('Language');
			$ifCondition = 'language_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'lang_dd';
			$hasTable = true;
			$table = 'customer_contact_language';
			$model = 'language';
			break;
		case 'acc_manager':
			$fieldLabelName = gm('Account Manager');
			$ifCondition = 'acc_manager_if';
			$isDD = true;
			$selectizeConfig = 'customerTypeCfg';
			$selectizeOptions = 'accountManager';
			$model = 'user_id';
			break;
		case 'sales_rep':
			$fieldLabelName = gm('Sales representative');
			$ifCondition = 'sales_rep_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'sales_rep';
			$model = 'sales_rep';
			$hasTable = true;
			$table = 'sales_rep';
			break;
		case 'lead_source':
			$fieldLabelName = gm('Lead source');
			$ifCondition = 'lead_source_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_lead_source';
			$model = 'lead_source';
			$hasTable = true;
			$table = 'customer_lead_source';
			break;
		case 'comp_size':
			$fieldLabelName = gm('Company size');
			$ifCondition = 'comp_size_if';
			$model = 'comp_size';
			break;
		case 'activity':
			$fieldLabelName = gm('Activity');
			$ifCondition = 'activity_if';
			$isDD = true;
			$selectizeConfig = 'salesCfg';
			$selectizeOptions = 'customer_activity';
			$model = 'activity';
			$hasTable = true;
			$table = 'customer_activity';
			break;
		case 'is_supplier':
			$fieldLabelName = gm('Supplier');
			$ifCondition = 'is_supplier_if';
			$model = 'is_supplier';
			$isCheckbox = true;
			break;
		case 'first_name':
			$fieldLabelName = gm('First Name');
			$ifCondition = 'first_name_if';
			$model = 'firstname';
			break;		
	}

	return array('label' 			=> $fieldLabelName,
			'if_condition' 		=> $ifCondition,
			'is_dd'		   		=> $isDD,
			'selectize_config' 	=> $selectizeConfig,
			'selectize_options' => $selectizeOptions,
			'model'				=> $model,
			'is_custom'			=> $isCustom,
			'sort_order'		=> $sortOrder,
			'has_table'			=> $hasTable,
			'table'				=> $table,
			'is_checkbox'		=> $isCheckbox,
			);
}

json_out($result);

function myCustomSort($a, $b)
{
	return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
}

?>