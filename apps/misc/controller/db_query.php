<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_base($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'database'=>array());
		if(!defined('BASEPATH')) exit('No direct script access allowed');

		if(($_SERVER['REMOTE_ADDR'] != '188.26.120.86' && $_SERVER['HTTP_X_FORWARDED_FOR'] != '188.26.120.86') && $_SERVER['HTTP_HOST'] != '10.0.0.1'){
			header("Location: index.php");
		}

		global $database_config;
		global $config;

		$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);

		$databases = array();
		$database_only= array();
		//if(strpos($config['site_url'],'my.akti')){
		   $datab = $db_users->query("SELECT DISTINCT database_name FROM users WHERE user_id >= 10000 OR (user_id < 10000 AND migrated='1') ");
		//$datab = $db_users->query("SELECT DISTINCT database_name FROM users WHERE user_id >10737");
/*		}else{
		   $datab = $db_users->query("SELECT DISTINCT database_name FROM users");
		}*/
		while ($datab->next()) {
			if($datab->f('database_name')){
				array_push($databases, $datab->f('database_name'));
			}
		}
		$in['base']='';
		$in['users']='';
		$data['database'] = array(
			'QUERY'		=>stripslashes($glob['query']),
			'databases'	=> $databases,
			'db_base'	=> $config['db_base'],
			'iteration'	=> '0',
			'bases'		=> $in['base'] == 1 ? true : false,
			'users'		=> $in['users'] == 1 ? true : false,
		);
			
		return json_out($data, $showin,$exit);

	}	
	$result = array(
		'base'				=> get_base($in,true,false),
	);
json_out($result);

?>