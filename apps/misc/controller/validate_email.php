<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$view = new at(ark::$viewpath.'validate_email.html');
global $database_config,$config;
// $view = new at();
$database_2 = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);

$user_db = new sqldb($database_2);
//$user = $user_db->field("SELECT username FROM users WHERE user_id='".$_SESSION['u_id']."'");
$user = $user_db->field("SELECT username FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
//$in['lang_id'] = $user_db->field("SELECT lang_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
$in['lang_id'] = $user_db->field("SELECT lang_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$site_url = $config['site_url'];
$crypt = base64_encode($user.' '.$in['email'].' '.$in['lang_id'].' '.$in['group_id']);
$crypt = strrev($crypt);

if($_SESSION['l']=='nl'){
	$mail_id = 'confirmation_nl';
	$subject_id = 'subject_nl';
}elseif($_SESSION['l']=='en'){
	$mail_id = 'confirmation_en';
	$subject_id = 'subject_en';
}elseif($_SESSION['l']=='fr'){
	$mail_id = 'confirmation_fr';
	$subject_id = 'subject_fr';
}elseif($_SESSION['l']=='ge'){
	$mail_id = 'confirmation_ge';
	$subject_id = 'subject_ge';
}



$mail_content = $user_db->field("SELECT ".$mail_id ." FROM confirmation_mail WHERE confirmation_id='1'");
$mail_subject = $user_db->field("SELECT ".$subject_id ." FROM confirmation_mail WHERE confirmation_id='1'");


$in['time'] =mktime();
$timestamp = strtotime("+48 hours",$in['time']);
/*$time =  $user_db->field("INSERT INTO user_email SET user_id='".$_SESSION['u_id']."', crypt='".$crypt."', time_email='".$timestamp."' ");
*/

/*$duo= $crypt.';'.$user.';'.$in['email'].';'.$in['lang_id'].';'.$in['group_id'];*/
//$duo=$crypt;

//$site = $site_url.'create_account/'.$duo;
$view->assign(array(
    'site_url'		=> $site_url,
    'content'		=> $mail_content
));

return $view->fetch();
