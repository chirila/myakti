<?php
if(!$_SESSION['u_id']){
	return false;
}
global $database_config,$config;
$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
$db_users =  new sqldb($database_users);
$db=new sqldb();

if($_SESSION['acc_type'] == 'free'){
	$view->assign('show_timesheet', false);
}else{
	$view->assign('show_timesheet', true);
}

$is_admin = false;
if($_SESSION['group'] == 'admin'){
	$is_admin = true;
}
$view->assign('is_admin',$is_admin);
if($_SESSION['l']=='fr'){
	$view->assign('tutorials_link','https://support.akti.com/hc/fr/categories/115000814665-Tutoriels');
	$view->assign('faq_link','https://support.akti.com/hc/fr/categories/115000837289-FAQ');
}else{
	$view->assign('tutorials_link','https://support.akti.com/hc/nl/categories/115000814665-Handleidingen');
	$view->assign('faq_link','https://support.akti.com/hc/nl/categories/115000837289-FAQ');
}

//$user = $db_users->query("SELECT user_id,first_name,last_name,email,default_admin,group_id,avatar FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$user = $db_users->query("SELECT users.user_id,users.first_name,users.last_name,users.email,users.default_admin,users.group_id,users.avatar, user_info.is_trial 
							FROM users
							LEFT JOIN user_info ON users.user_id = user_info.user_id
							WHERE users.user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
$user->next();

$view->assign('HIDE_PAYMENT',$user->f('default_admin') == 1 ? true : false);
if($user->f('avatar')){
	$view->assign('avatar',$user->f('avatar'));
}else{
	$view->assign('no_avatar',true);
}
$view->assign(array(
		'first_name'	=> htmlspecialchars_decode(stripslashes($user->f('first_name'))),
		'last_name'		=> htmlspecialchars_decode(stripslashes($user->f('last_name'))),
		'user_id'		=> $user->f('user_id'),
		'email'			=> $user->f('email'),
		'product_id'	=> $config['beamer_product_id'],
		'language'		=> $_SESSION['l']=='nl' ? 'DU' : strtoupper($_SESSION['l'])
	));

$is_new_plan = $db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION'");
if(is_null($is_new_plan)){
	//$view->assign('plan_go','subscription');
	$view->assign('plan_go','subscription_new');
}else{
	$view->assign('plan_go','subscription_new');
}

$is = false;
for ($j = 1; $j<=19; $j++){
	if(in_array($j,perm::$allow_apps)){
		$is = true;
	}
	$view->assign('is_'.$j, $is);
	$view->assign('is_only_'.$j, $is);
	$is = false;
	//$val =$db_users->field("SELECT value FROM user_meta WHERE name='MODULE_".$j."' AND user_id='".$_SESSION['u_id']."' ");
	$val =$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$j,'user_id'=>$_SESSION['u_id']]);
	if($val===NULL){
		if(defined('MODULE_'.$j)){
			$val = constant('MODULE_'.$j);
		}
	}
	$view->assign('hide_'.$j, $val == 0 ? 'hidden' : '');
}
if($_SESSION['team_int']){
	$view->assign('is_13',true);
}
$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
	$view->assign(array(
		'hide_add'		=> ($user->f('group_id') == 2 || $easyinvoice=='1')?  false : true,
	));
$view->assign('noteasy',$easyinvoice==1 ? false : true);
$WIZZARD_COMPLETE = $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE'");

if(ADV_QUOTE==0 && ADV_PRODUCT==0 && ADV_CRM==0){
	$hide_calendar=false;
}else{
	$hide_calendar=true;
}

	
if($WIZZARD_COMPLETE==0){
    $hide_them_all=false;
    $special_button=true;
}else{
    $hide_them_all=true;
    $special_button=false;
}

if($user->f('is_trial')){
	$show_invite=true;
}else{
	$show_invite=false;
}
$view->assign('hide_calendar',$hide_calendar);
$view->assign('hide_them_all',$hide_them_all);
$view->assign('special_button',$special_button);
$view->assign('show_invite',$show_invite);
$view->assign('not_easy_invoice_diy', aktiUser::get('is_easy_invoice_diy') ? false : true);
$view->assign('easy_invoice_diy', aktiUser::get('is_easy_invoice_diy') ? true : false);

return $view->fetch();