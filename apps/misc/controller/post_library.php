<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
  session_write_close();
  ini_set("default_socket_timeout", 5);
  $db_1=new sqldb();
  $response=array();
  $post_library=array();
  $post_active=$db_1->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
    if($post_active){
        try{
          global $config;
            $env_usr = $config['postgreen_user'];
            $env_pwd = $config['postgreen_pswd'];
            $postg_data=$db_1->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
            $panel_usr=$postg_data->f("api");

            $post_green_data = array('user_id' => $_SESSION['u_id'],
                      'database_name' => DATABASE_NAME,
                      'item_id'   => '',
                      'module'    => 'misc',
                      'date'      => time(),
                      'action'    => 'LIBRARY_OBJECT_LIST'.' - '.$config['postgreen_wsdl'],
                      'content'   => '',
                      'login_user'  => addslashes($panel_usr),
                      'error_message' => ''
              );

            $panel_pswd=$db_1->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
            $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
            $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
            $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
            $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
            $soap->__setSoapHeaders(array($objHeader_Session_Outside));
            $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));

            $post_green_data['content'] = addslashes('<library_object_list_IN>
              <action type="string">LIBRARY_OBJECT_LIST</action>
              <identifier type="indstringlist">
                <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
              </identifier>
            </library_object_list_IN>');  

            $params = new \SoapVar('
            <library_object_list_IN>
              <action type="string">LIBRARY_OBJECT_LIST</action>
              <identifier type="indstringlist">
                <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
              </identifier>
            </library_object_list_IN>',
             XSD_ANYXML);
            $res = $soap->library_object_list($params);
            if($res->action->data[0]->_ == '000'){
              if(is_array($res->object_list->row)){
                foreach($res->object_list->row as $key=>$value){
                  if($value->col[4]->data=='BKC'){
                    $item=array(
                      'id'    => $value->col[2]->data,
                      'name'    => $value->col[3]->data,
                      'type'    => $value->col[4]->data,
                    );
                    array_push($post_library,$item);
                  }     
                }
              }else{
                if($res->object_list->row){
                  if($res->object_list->row->col[4]->data=='BKC'){
                    $item=array(
                      'id'    => $res->object_list->row->col[2]->data,
                      'name'    => $res->object_list->row->col[3]->data,
                      'type'    => $res->object_list->row->col[4]->data,
                      );
                    array_push($post_library,$item);
                  }   
                } 
              }
            }
        }catch(Exception $e){
            console::log('postgreen error');
            $post_green_data['error_message'] = addslashes($e->getMessage());

          if(empty($post_green_data['content'])){
            $post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
          }

        error_post($post_green_data);
        
            if($in['ret_res']){
                return $post_library;
            }else{
                $response['post_library']=$post_library;
                json_out($response);
            }
        }
    }
    if($in['ret_res']){
      return $post_library;
    }else{
      $response['post_library']=$post_library;
      json_out($response);
    }
    
?>