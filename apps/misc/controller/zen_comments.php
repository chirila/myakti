<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

    $ch = curl_init();
    $db=new sqldb();
    $result = array('activities' => array('item'=>array())); 
    $is_data=false;

    $zen_data=$db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
    $zen_email=$db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
	$zen_pass=$db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");

    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/tickets/'.$in['ticket_id'].'/comments.json?sort_order=desc');

	$put = curl_exec($ch);
	$info = curl_getinfo($ch);

	if($info['http_code']>300 || $info['http_code']==0){
		//
	}else{
		$data_d=json_decode($put);
		$names_array=array();
		$ids_array=array();

		foreach ($data_d->comments as $key => $value) {	
			$is_data=true;
			if(!in_array($value->author_id, $ids_array)){

				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
				curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/users/'.$value->author_id.'.json');

				$put = curl_exec($ch);
				$info = curl_getinfo($ch);

				if($info['http_code']>300 || $info['http_code']==0){
					//
				}else{
					$user_data=json_decode($put);
					array_push($ids_array,$value->author_id);
					array_push($names_array,$user_data->user->name);
				}

			}
			$name=$names_array[array_search($value->author_id, $ids_array)];
			$item_line = array(
				'time'					=> date('H:i',strtotime($value->created_at)),
				'day'					=> date(ACCOUNT_DATE_FORMAT,strtotime($value->created_at)),
				'comments'				=> nl2br($value->html_body),
				'by'					=> $name,
			);
			array_push($result['activities']['item'], $item_line);
		}
	}
	$result['activities']['is_data']=$is_data;
	json_out($result);

?>