<?php
global $database_config;
$response = array('isLoggedIn'=>false);
if($_SESSION['u_id']){
	$response['isLoggedIn'] = true;
	$response['name'] = get_user_name($_SESSION['u_id']);
	$response['group'] = $_SESSION['group'];
	$response['adminSetting'] = array();
	if(isset($_SESSION['admin_sett'])){
		foreach ($_SESSION['admin_sett'] as $key => $value) {
			array_push($response['adminSetting'],$value);
		}
	}
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($db_config);
	//$db=new sqldb();

	// $is_new_subscription = $db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");
	// $WIZZARD = $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");

	// $response['must_pay']=false;
	// $user_info = $db_users->query("SELECT * FROM user_info WHERE user_id='".$_SESSION['u_id']."' ");
	// if(time()>$user_info->f('end_date')){
	// 	$response['must_pay']=true;
	// 	if(is_null($is_new_subscription)){
	// 		$response['pag']='subscription';
	// 	}else{
	// 		$response['pag']='subscription_new';
	// 	}
	// }
	$showGdpr=false;
	//$gdpr_status = $db_users->field("SELECT gdpr_status FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$gdpr_status = $db_users->field("SELECT gdpr_status FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	//$group_id=$db_users->field("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$group_id=$db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	//$credential=$db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$credential=$db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	$credentials=explode(';', $credential);
	if($group_id==2 || (in_array('1', $credentials) === false)){
		$response['default_pag']='calendar';
		if(!$gdpr_status || $gdpr_status==2){
			$showGdpr=true;
		}
	}else{
		$response['default_pag']='customers';
		if(!$gdpr_status || $gdpr_status==2){
			$showGdpr=true;
		}
	}
	$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
	if($easyinvoice=='1'){
		$response['default_pag']='invoices';
		if(!$gdpr_status || $gdpr_status==2){
			$showGdpr=true;
		}
	}
	$response['WIZZARD_COMPLETE']= $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");
	$response['showGdpr']=$showGdpr;
}
json_out($response);