<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

	$db = new sqldb();
	$result=array();
	$task_time_data=$db->query("SELECT * FROM task_time WHERE task_time_id='".$in['task_time_id']."'");
	$project_type=$db->field("SELECT status_rate FROM projects WHERE project_id='".$task_time_data->f('project_id')."'");
	if($project_type){
		$result['comment']=$task_time_data->f('notes');
		$result['comment2']=html_entity_decode(strip_tags($task_time_data->f('notes')));
		$do_next='misc--time-update_single_time';
		$title=gm('Add Note');
	}else{
		$order_by = " ORDER BY id ASC  ";
		$order_by_array = array('start_time');
		if($in['order_by']){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == true){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}
		$entries_sheet=$db->field("SELECT COUNT(id) FROM servicing_support_sheet WHERE task_time_id='".$in['task_time_id']."' AND user_id='".$task_time_data->f('user_id')."' ");
		if($task_time_data->f('hours')>0 && !$entries_sheet){
			$db->query("INSERT INTO servicing_support_sheet SET 
									task_id='".$task_time_data->f('task_id')."',
									date='".$task_time_data->f('date')."',
									user_id='".$task_time_data->f('user_id')."',
									project_id='".$task_time_data->f('project_id')."',
									task_time_id='".$task_time_data->f('task_time_id')."',
									total_hours='".$task_time_data->f('hours')."' ");			
		}
		$task_time_subdata=$db->query("SELECT * FROM servicing_support_sheet WHERE project_id='".$task_time_data->f('project_id')."' AND task_id='".$task_time_data->f('task_id')."' AND task_time_id='".$in['task_time_id']."' AND user_id='".$task_time_data->f('user_id')."' ".$order_by." ");
		$result['rows']=array();
		while($task_time_subdata->next()){
			$temp_data=array(
				'id'			=> $task_time_subdata->f('id'),
				'start_time'	=> number_as_hour($task_time_subdata->f('start_time')),
				'end_time'		=> number_as_hour($task_time_subdata->f('end_time')),
				'break'		=> number_as_hour($task_time_subdata->f('break')),
				'comment'		=> $task_time_subdata->f('notes'),
				'comment2'		=> html_entity_decode(strip_tags($task_time_subdata->f('notes'))),
				'line_h_wo'		=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? $task_time_subdata->f('total_hours') : $task_time_subdata->f('end_time')-$task_time_subdata->f('start_time')-$task_time_subdata->f('break'),
				'line_h'		=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? number_as_hour($task_time_subdata->f('total_hours')) : number_as_hour($task_time_subdata->f('end_time')-$task_time_subdata->f('start_time')-$task_time_subdata->f('break')),
				'is_total_h'	=> ($task_time_subdata->f('total_hours')>0 && $task_time_subdata->f('end_time')==0 && $task_time_subdata->f('start_time')==0) ? true : false,
			);
			array_push($result['rows'], $temp_data);
		}
		$do_next='misc-xcomment-time-addSubHours';
		$title=gm('Log Time');
	}

	$result['disabled']=$task_time_data->f('submited') == 1 ? true : false;
	$result['task_time_id']=$task_time_data->f('task_time_id');
	$result['task_id']=$task_time_data->f('task_id');
	$result['project_id']=$task_time_data->f('project_id');
	$result['type']=$project_type;
	$result['total_hours']=$task_time_data->f('hours');
	$result['do_next']=$do_next;
	$result['date']=$task_time_data->f('date');
	$result['title']=$title;
	$result['user_id']=$task_time_data->f('user_id');


return json_out($result);