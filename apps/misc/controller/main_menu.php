<?php
if(!$_SESSION['u_id']){
    return false;
}
global $database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
);

$db_users = new sqldb($db_config);

$db_special = new sqldb();

$array = array(
    'company',
    'article',
    'order',
    'po_order',
    'quote',
    'project',
    'service',
    'installation',
    'contract',
    'invoice',
    'timetracker',
    'stock'
);

foreach ($array as $value) {
    $is_admin = false;
    if(in_array($value, $_SESSION['admin_sett'])) {
        $is_admin = true;
    }
    if($_SESSION['group'] == 'admin'){
        $is_admin = true;
    }
    $view->assign('is_admin_'.$value,$is_admin);
}
$is_general_admin = false;
if($_SESSION['group'] == 'admin'){
    $is_general_admin = true;
}
$view->assign('is_admin',$is_general_admin);
$easyinvoice = $db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
$view->assign('noteasy',$easyinvoice=='1'? false : true);

$subscription = $db_users->field("SELECT base_plan_id FROM users WHERE `user_id`='".$_SESSION['u_id']."' ");
$view->assign('not_easyinv_subscription', $subscription=='7'? false : true);

$is_crm=false;
$is_od=false;
$is_fs=false;
$hide_crm=0;
$hide_od=0;
$hide_fs=0;
$is = false;
for ($i = 1; $i<=19; $i++){
    if(in_array($i,perm::$allow_apps)){
        $is = true;
    }
    if($easyinvoice==1){
        if($i!='4'){
            $view->assign('is_'.$i, false);
        }else{
            $view->assign('is_'.$i, $is);
        }
    }else{
        if(($i==1 || $i==5 || $i==11) && $is===true){
            $is_crm=true;
        }
        if(($i==6 || $i==14) && $is===true){
            $is_od=true;
        }
        if(($i==13 || $i==17) && $is===true){
            $is_fs=true;
        }
        $view->assign('is_'.$i, $is);
    }    
    $is = false;
    //$val =$db_users->field("SELECT value FROM user_meta WHERE name='MODULE_".$i."' AND user_id='".$_SESSION['u_id']."' ");
    $val =$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$i,'user_id'=>$_SESSION['u_id']]);
    if($val===NULL){
        if(defined('MODULE_'.$i)){
            $val = constant('MODULE_'.$i);
        }
    }
    if(($i==1 || $i==5 || $i==11) && $val == 0){
        $hide_crm++;
    }
    if(($i==6 || $i==14) && $val == 0){
        $hide_od++;
    }
    if(($i==13 || $i==17) && $val == 0){
        $hide_fs++;
    }
    $view->assign('hide_'.$i, $val == 0 ? 'hidden' : '');
}

$view->assign(array(
    'is_crm'       => $is_crm,
    'hide_crm'     => $hide_crm==3 ? 'hide' : '',
    'is_od'        => $is_od,
    'hide_od'      => $hide_od==2 ? 'hide' : '',
    'is_fs'        => $is_fs,
    'hide_fs'      => $hide_fs==2 ? 'hide' : ''
));

$codabox=$db->query("SELECT app_id,active FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
if($codabox->f('active')){
    $payments_check=$db->field("SELECT api FROM apps WHERE type='payments' AND main_app_id='".$codabox->f('app_id')."' ");
    $coda_active_pay=is_null($payments_check) || $payments_check==1? true : false;
    $view->assign('codabox_active',$coda_active_pay);
}

//$project_admin = $db_users->field("SELECT value FROM user_meta WHERE name='project_admin' AND user_id='".$_SESSION['u_id']."' ");
$project_admin = $db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'project_admin','user_id'=>$_SESSION['u_id']]);
//$group = $db_users->field("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
$group = $db_users->field("SELECT group_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
//$more_proj_admin=$db_users->field("SELECT value FROM user_meta WHERE name='admin_3' AND value='project' AND user_id='".$_SESSION['u_id']."' ");
$more_proj_admin=$db_users->field("SELECT value FROM user_meta WHERE name= :name AND value= :value AND user_id= :user_id ",['name'=>'admin_3','value'=>'project','user_id'=>$_SESSION['u_id']]);
if($more_proj_admin || $project_admin == 1 || $group == 1){
    $view->assign('is_more_proj_admin',true);
}

//$umeta_service_manager=$db_users->field("SELECT value FROM user_meta WHERE name='intervention_admin' AND user_id='".$_SESSION['u_id']."' ");
$umeta_service_manager=$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'intervention_admin','user_id'=>$_SESSION['u_id']]);
if($_SESSION['group'] == 'admin' || ($_SESSION['admin_sett'] && in_array('service', $_SESSION['admin_sett'])) || $umeta_service_manager){
    $view->assign('is_team', true);
}
$view->assign('service_manager',($umeta_service_manager || $_SESSION['group'] == 'admin' || ($_SESSION['admin_sett'] && $_SESSION['admin_sett']['service'])) ? true : false);

if($plan == 2 || $plan == 5){
    //$cat_val =$db_users->field("SELECT value FROM user_meta WHERE name='MODULE_3' AND user_id='".$_SESSION['u_id']."' ");
    $cat_val =$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_3','user_id'=>$_SESSION['u_id']]);
}else if($plan == 4){
    //$cat_val =$db_users->field("SELECT value FROM user_meta WHERE name='MODULE_12' AND user_id='".$_SESSION['u_id']."' ");
    $cat_val =$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_12','user_id'=>$_SESSION['u_id']]);
}else{
    //$cat_val1 =$db_users->field("SELECT value FROM user_meta WHERE name='MODULE_3' AND user_id='".$_SESSION['u_id']."' ");
    $cat_val1 =$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_3','user_id'=>$_SESSION['u_id']]);
    //$cat_val2 =$db_users->field("SELECT value FROM user_meta WHERE name='MODULE_12' AND user_id='".$_SESSION['u_id']."' ");
    $cat_val2 =$db_users->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_12','user_id'=>$_SESSION['u_id']]);
    if($cat_val1 == 1 || $cat_val2 == 1){
        $cat_val = 1;
    }else{
        $cat_val = NULL;
        if( !is_null($cat_val1) ){
            $cat_val = $cat_val1;
        }
        if( !is_null($cat_val2) ){
            $cat_val = $cat_val2;
        }
        if( is_null($cat_val1) && is_null($cat_val2) ){
            if(defined('MODULE_12') && MODULE_12==1){
                $cat_val = constant('MODULE_12');
            }elseif (defined('MODULE_3') && MODULE_3==1) {
                $cat_val = constant('MODULE_3');
            }
        }
    }
}

if($cat_val == 0 && !is_null($cat_val)){
    $hide_catalogue = 'hidden';
}

if(defined('NEW_SUBSCRIPTION')){
    $view->assign('adv_crm', ADV_CRM == 1 ? true : false );
    $view->assign('adv_quotes', ADV_QUOTE == 1 ? true :  false );    
}else{
    $view->assign('adv_crm', true  );
    $view->assign('adv_quotes', true  );    
}

$view->assign('hide_catalogue', $hide_catalogue );

//$credentials = $db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$credentials = $db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
$credentials = explode(';', $credentials);
// var_dump($credentials);
// exit();
$adv_sepa=false;
if(defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1){
    if(defined('ADV_SEPA') && ADV_SEPA==1 && defined('SHOW_INVOICE_SEPA') && SHOW_INVOICE_SEPA==1){
        $adv_sepa=true;
    }
}else if(defined('SHOW_INVOICE_SEPA') && SHOW_INVOICE_SEPA==1){
    $adv_sepa=true;
}
$view->assign('show_sepa', $adv_sepa);
$showcatalog = true;
$showcataloginv = true;

$link='articles';
if($_SESSION['acc_type']=='services')
{
    $view->assign(array(
        'is_services'   => true,
        'is_goods'      => false,
    ));
    $link='aservices';    
    
}else{
    if(!in_array(12,perm::$allow_apps) && in_array(3,perm::$allow_apps)){
        $link='aservices';
    }    
}


//if($_SESSION['group'] == 'user'){
    if(!in_array(12,$credentials) || !in_array(6,$credentials)){
        $showcatalog = false;
    }
    if(!in_array(12,$credentials)){
        $showcataloginv = false;
    }
//}
if($easyinvoice==1){
    $showcatalog=false;
    $showcataloginv = false;
}
$view->assign('showcatalog', $showcatalog);
$view->assign('showcataloginv', $showcataloginv);

$view->assign('link',$link);

if(WIZZARD_COMPLETE==0){
    $hide_them_all=false;
}else{
    $hide_them_all=true;
}
$view->assign('hide_them_all',$hide_them_all);

$user_special = $db_special->field("SELECT value FROM settings WHERE constant_name = 'ONLY_IF_ACC_MANAG' ");
$user_special1 = $db_special->field("SELECT value FROM settings WHERE constant_name = 'ONLY_IF_ACC_MANAG2' ");

if($user_special==1 && $user_special1 ==1 && $_SESSION['group']=='user'){
    $special_user=false;
}else{
    $special_user=true;
}

$view->assign('special_user',$special_user);

$marketing_apps = $db_special->query("SELECT app_id FROM apps WHERE main_app_id='0' AND type='main' AND app_type='mail' AND template_file!='nylas' AND active = '1' ");
 $show_marketing = false;
if($marketing_apps->next()) {
    $show_marketing = true;
}

$view->assign('show_marketing',$show_marketing);

#reports first page
$vars=$view->return_vars();
$reportpage='';

if($showcatalog && !$vars->hide_catalogue && !$vars->hide_12 && $vars->is_6){
    $reportpage='report_catalogue';
}else if($showcataloginv && $vars->is_4 && !$vars->hide_4){
    $reportpage='report_catalogue_invoices';
}else if($vars->is_5 && !$vars->hide_5){
    $reportpage='report_quotes';
}else if($vars->is_6 && !$vars->hide_6){
    $reportpage='report_orders';
}else if($vars->is_3 && !$vars->hide_3){
    $reportpage='report_projects';
}else if($vars->is_4 && !$vars->hide_4){
    $reportpage='report_invoices';
}else{
    $reportpage='login';
}
//var_dump($vars );exit();
$view->assign('report_state',$reportpage);

    $ERP=$db->field("SELECT value FROM settings WHERE constant_name = 'SUBSCRIPTION_TYPE' ");
    if($ERP=='1'){
        $view->assign('ERP',true);
    }

$is_accountant =$db_users->field("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND active=1000 ") ? true : false;
$is_zen=$db->field("SELECT active FROM apps WHERE name='Zendesk' and type='main' AND main_app_id='0' ") ? true : false;
$access_api_webhooks=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCESS_API_WEBHOOKS' ");

$pag = get_default_page();

$view->assign(array(
    'is_accountant' => $is_accountant ? true : false,
    'is_zen'        => $is_zen ? true : false,
    'access_api_webhooks'   => $access_api_webhooks=='1' ? true : false,
    'default_pag'           => $pag['default_pag'],
    'not_easy_invoice_diy' => aktiUser::get('is_easy_invoice_diy') ? false : true
));

return $view->fetch();