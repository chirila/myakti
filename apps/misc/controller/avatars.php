<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	$result=array('images'=>array());

	$files=scandir('images/avatars/');
	foreach($files as $key=>$value){
		if(strpos($value,'.png') !== false){
			array_push($result['images'], array('url'=>'images/avatars/'.$value));
		}
	}

	json_out($result);

?>