<?php
	global $database_config;
    $database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users =  new sqldb($database_2);
    $data = array(); 
    //$user_gdpr=$db_users->query("SELECT gdpr_status,gdpr_time FROM users WHERE user_id='".$_SESSION['u_id']."' "); 
    $user_gdpr=$db_users->query("SELECT gdpr_status,gdpr_time FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    $data['gdpr_action']=$user_gdpr->f('gdpr_status');  
    switch($_SESSION['l']){
    	case 'en':
    		$data['service_terms_url']="#";
    		$data['privay_policy_url']="#";
    		$data['cookie_url']="#";
    		break;
    	case 'fr':
    		$data['service_terms_url']="https://support.akti.com/hc/fr/articles/360000151329--Convention-d-utilisation-pour-Akti-";
    		$data['privay_policy_url']="https://support.akti.com/hc/fr/articles/360001239289-Privacy-Policy";
    		$data['cookie_url']="https://support.akti.com/hc/fr/articles/360001240085-Cookies-Policy";
    		break;
    	case 'nl':
    	case 'du':
    		$data['service_terms_url']="https://support.akti.com/hc/nl/sections/360000205049-Voorwaarden-en-Privacy";
    		$data['privay_policy_url']="https://support.akti.com/hc/nl/articles/360001239289-Privacy-Policy";
    		$data['cookie_url']="https://support.akti.com/hc/nl/articles/360001240085-Cookies-Policy";
    		break;
    }  
    json_out($data);

?>