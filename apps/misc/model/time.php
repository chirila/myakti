<?php
/************************************************************************
* @Author: MedeeaWeb Works                                                   *
************************************************************************/

class time
{
	function time()
	{
		$this->db = new sqldb();
		global $database_config;
		//$this->dbu_users = new sqldb(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		$this->database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->dbu =  new sqldb($this->database_2);
	}

	function addSubHoursPlus(&$in){
		if(!$in['project_id'] && $in['c_id']){
			$project_active='2';
			$project_name="ad hoc";
			if($in['task_type'] && $in['task_type']=='2'){
				$in['task_name']=stripslashes($this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$in['task_id']."' "));
				$in['article_id']=$in['task_id'];
				$project_active='3';
				$project_name="service";
			}else{
				$in['task_name']=stripslashes($this->db->field("SELECT default_name FROM default_data WHERE default_main_id='0' AND type='task_no' AND active='1' AND default_id='".$in['task_id']."' "));
			}		
			$this->db->query("SELECT project_id FROM projects WHERE customer_id='".$in['c_id']."' AND active='".$project_active."' AND contract_id='0' ");
			if($this->db->move_next()){
				$in['project_id'] = $this->db->f('project_id');
				if($in['task_type'] && $in['task_type']=='2'){
					$this->db->query("SELECT task_id FROM tasks WHERE project_id='".$in['project_id']."' AND article_id='".$in['article_id']." ");				
				}else{
					$this->db->query("SELECT task_id FROM tasks WHERE project_id='".$in['project_id']."' AND task_name='".addslashes($in['task_name'])."' ");
				}
				
				if($this->db->move_next()){
					$in['task_id'] = $this->db->f('task_id');
					if($in['task_type'] && $in['task_type']=='2'){
						$article_id=$in['article_id'];
						$billable = 1;
						$params = array(
		                  'article_id' => $in['article_id'], 
		                  'price' => 0, 
		                  'quantity' => 1,
		                  'customer_id' => $in['c_id'],
		                  'cat_id' => $in['cat_id'],
		                  'asString' => true
		                );
                		$value= $this->getArticlePrice($params);
                		$this->db->query("UPDATE tasks SET t_h_rate='".$value."' WHERE task_id='".$in['task_id']."' ");
					}
				}else{
					$article_id='0';
					if($in['task_type'] && $in['task_type']=='2'){
						$article_id=$in['article_id'];
						$billable = 1;
						$params = array(
		                  'article_id' => $in['article_id'], 
		                  'price' => 0, 
		                  'quantity' => 1,
		                  'customer_id' => $in['c_id'],
		                  'cat_id' => $in['cat_id'],
		                  'asString' => true
		                );
                		$value= $this->getArticlePrice($params);
					}else{
						$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
						$this->db->move_next();
						$value = $this->db->f('value');
						$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
						$this->db->move_next();
						$billable = 0;
						if($this->db->f('value') == 'yes'){
							$billable = 1;
						}
						$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
						if($this->db->move_next()){
							$value = $this->db->f('value');
						}
					}				
					$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."',article_id='".$article_id."' ");
				}
			}else{
				$article_id='0';
				if($in['task_type'] && $in['task_type']=='2'){
					$article_id=$in['article_id'];
					$billable = 1;
					$params = array(
	                  'article_id' => $in['article_id'], 
	                  'price' => 0, 
	                  'quantity' => 1,
	                  'customer_id' => $in['c_id'],
	                  'cat_id' => $in['cat_id'],
	                  'asString' => true
	                );
            		$value= $this->getArticlePrice($params);
				}else{
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
					$this->db->move_next();
					$value = $this->db->f('value');
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
					$this->db->move_next();
					$billable = 0;
					if($this->db->f('value') == 'yes'){
						$billable = 1;
					}
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
					if($this->db->move_next()){
						$value = $this->db->f('value');
					}
				}			
				$in['project_id'] = $this->db->insert("INSERT INTO projects SET customer_id='".$in['c_id']."', name='".$project_name."', active='".$project_active."', billable_type='1', invoice_method='1', company_name=(SELECT name FROM customers WHERE customer_id='".$in['c_id']."'), stage='1', step='0' ");
				$u_name = get_user_name($in['user_id']);
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."', user_id='".$in['user_id']."', user_name='".addslashes($u_name)."' ");
				$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."',article_id='".$article_id."' ");
			}
		}
		$task_day_exist=$this->db->field("SELECT task_time_id FROM task_time WHERE date='".$in['date']."' AND project_id='".$in['project_id']."' AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."'");
		if(!$task_day_exist){
			$this->db->query("UPDATE projects SET step='0' WHERE project_id='".$in['project_id']."' ");
			$in['status_rate']=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$in['project_id']."' ");
			$in['customer_id']=$this->db->field("SELECT customer_id FROM projects WHERE project_id='".$in['project_id']."' ");
			$in['billable'] = $this->db->field("SELECT billable FROM tasks WHERE task_id='".$in['task_id']."' ");
			foreach ($in['day_row'] as $key => $value ){
				$this->db->query("INSERT INTO task_time SET user_id= '".$in['user_id']."',
											task_id	     		= '".$in['task_id']."',
											project_id	 		= '".$in['project_id']."',
											customer_id	 		= '".$in['customer_id']."',
											contract_id	 		= '".$in['contract_id']."',
											date            		= '".$value['TIMESTAMP']."',
											hours	          		= '0',
											billable			 = '".$in['billable']."',
											project_status_rate      ='".$in['status_rate']."' ");
			}
		}
		$task_time_subdata=$this->db->query("SELECT * FROM task_time WHERE date='".$in['date']."' AND project_id='".$in['project_id']."' AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."' ");
		while($task_time_subdata->next()){
			$this->db->query("INSERT INTO servicing_support_sheet SET 
					start_time		= '0',
					end_time		= '0',
					break			= '0',
					project_id		= '".$task_time_subdata->f('project_id')."',
					task_id		= '".$task_time_subdata->f('task_id')."',
					task_time_id	= '".$task_time_subdata->f('task_time_id')."',
					date			= '".$in['date']."',
					user_id		= '".$in['user_id']."' ");
		}
		msg::success(gm("Line added"),"success");
		return true;
	}

	function updateDayTotalHours(&$in){
		$this->db->query("UPDATE servicing_support_sheet SET total_hours='".$in['value']."',start_time='0',end_time='0',break='0' WHERE id='".$in['id']."' ");
		$hours_count=0;
		$hours_data=$this->db->query("SELECT * FROM servicing_support_sheet WHERE task_time_id='".$in['task_time_id']."' ");
		while($hours_data->next()){
			if($hours_data->f('total_hours')==0){
				$hours_count+=($hours_data->f('end_time')-$hours_data->f('start_time')-$hours_data->f('break'));
			}else{
				$hours_count+=$hours_data->f('total_hours');
			}
		}
		$this->db->query("UPDATE task_time SET hours='".$hours_count."' WHERE task_time_id='".$in['task_time_id']."' ");
		msg::success(gm('Data saved'),"success");
		json_out($in);
	}

	function updateDayHours(&$in){
		$this->db->query("UPDATE servicing_support_sheet SET total_hours='0',start_time='".$in['start_time']."',end_time='".$in['end_time']."',break='".$in['break']."' WHERE id='".$in['id']."' ");
		$hours_count=0;
		$hours_data=$this->db->query("SELECT * FROM servicing_support_sheet WHERE task_time_id='".$in['task_time_id']."' ");
		while($hours_data->next()){
			if($hours_data->f('total_hours')==0){
				$hours_count+=($hours_data->f('end_time')-$hours_data->f('start_time')-$hours_data->f('break'));
			}else{
				$hours_count+=$hours_data->f('total_hours');
			}
		}
		$this->db->query("UPDATE task_time SET hours='".$hours_count."' WHERE task_time_id='".$in['task_time_id']."' ");
		msg::success(gm('Data saved'),"success");
		json_out($in);
	}

	function deleteSubHours(&$in){
		$taskt_id=$this->db->field("SELECT task_time_id FROM servicing_support_sheet WHERE id='".$in['id']."'");
		$this->db->query("DELETE FROM servicing_support_sheet WHERE id='".$in['id']."'");
		$hours_count=0;
		$hours_data=$this->db->query("SELECT * FROM servicing_support_sheet WHERE task_time_id='".$taskt_id."' ");
		while($hours_data->next()){
			if($hours_data->f('total_hours')==0){
				$hours_count+=($hours_data->f('end_time')-$hours_data->f('start_time')-$hours_data->f('break'));
			}else{
				$hours_count+=$hours_data->f('total_hours');
			}
		}
		$this->db->query("UPDATE task_time SET hours='".$hours_count."' WHERE task_time_id='".$taskt_id."' ");
		msg::success(gm("Data saved"),"success");
		json_out($in);
	}

	function updateDayComment(&$in){
		$this->db->query("UPDATE servicing_support_sheet SET notes='".trim($in['notes'])."' WHERE id='".$in['id']."'");
		msg::success(gm("Data saved"),"success");
		json_out($in);
	}

	/****************************************************************
	* function update_single_time(&$in)                             *
	****************************************************************/
	function update_single_time(&$in){
		if(!$this->update_validate($in)){
			json_out($in);
		}
		$this->db->query("UPDATE task_time SET notes='".trim($in['comment'])."' WHERE task_time_id='".$in['task_time_id']."' ");
		msg::success(gm('Comment has been added.'),"success");
		json_out($in);
	}

	/****************************************************************
	* function update_validate(&$in)                                *
	****************************************************************/
	function update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('task_time_id', gm('ID'), 'required:exist[task_time.task_time_id]', gm("Invalid ID"));

		return $v->run();
	}

	/****************************************************************
	* function check_project_budget(&$in)                           *
	****************************************************************/
	function check_project_budget(){

		$project = $this->db->query("SELECT projects.name AS p_name, projects.pr_h_rate AS pr_h_rate, projects.billable_type AS billable_type, projects.project_id AS p_id,projects.contract_id AS con_id, projects.budget_type, projects.alert_percent AS procent, projects.company_name AS c_name, projects.customer_id AS c_id, projects.status_rate FROM projects  WHERE (projects.active='1' OR contract_id!='') AND budget_type>'0' AND alert_percent!='0' AND alert_sent!='1' ");
		while($project->next()){
			$currency_show = 0;
			$budget_t = 0;
			$contract_id=$project->f('con_id');
			switch ($project->f('budget_type')){
			case '2':
				$budget = $this->db->field("SELECT t_pr_hour FROM projects WHERE project_id='".$project->f('p_id')."' ");
				$budget_t = $budget;
				break;
			case '3':
				$budget = $this->db->field("SELECT t_pr_fees FROM projects WHERE project_id='".$project->f('p_id')."' ");
				$budget_t = $budget;
				if( $project->f('billable_type') == '1'){
					if($project->f('status_rate') == '0'){
						$rate_value = $this->db->query("SELECT t_h_rate FROM tasks WHERE project_id='".$project->f('p_id')."' AND billable='1' AND task_id IN (SELECT DISTINCT task_id FROM task_time WHERE customer_id='".$project->f('c_id')."' AND project_id='".$project->f('p_id')."' AND hours !=0 )");
						while ($rate_value->next()) {
							$rate += $rate_value->f('t_h_rate');
						}
					}else{
						$rate_value = $this->db->query("SELECT t_daily_rate FROM tasks WHERE project_id='".$project->f('p_id')."' AND billable='1' AND task_id IN (SELECT DISTINCT task_id FROM task_time WHERE customer_id='".$project->f('c_id')."' AND project_id='".$project->f('p_id')."' AND hours !=0 )");
						while ($rate_value->next()) {
							$rate += $rate_value->f('t_daily_rate');
						}
					}					
				}
				if($project->f('billable_type') == '2'){
					if($project->f('status_rate') == '0'){
						$rate_value = $this->db->field("SELECT p_h_rate FROM project_user WHERE project_id='".$project->f('p_id')."' ");
						$rate = $rate_value;
					}else{
						$rate_value = $this->db->field("SELECT p_daily_rate FROM project_user WHERE project_id='".$project->f('p_id')."' ");
						$rate = $rate_value;
					}					
				}
				if( $project->f('billable_type') == '3'){
						$rate = $project->f('pr_h_rate');
				}
				if( $project->f('billable_type') == '4') {
						$rate = 0;
				}
				$currency_show = 1;
				break;

			case '4':
				$budget = $this->db->query("SELECT t_hours FROM tasks WHERE project_id='".$project->f('p_id')."' ");
				while ($budget->next()) {
					$budget_t += $budget->f('t_hours');
				}
				break;
			case '5':
				$budget = $this->db->query("SELECT p_hours FROM project_user WHERE project_id='".$project->f('p_id')."' ");
				while ($budget->next()) {
					$budget_t += $budget->f('p_hours');
				}
				break;
			}
			$spent = $this->db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$project->f('p_id')."' ");
			$procent = $project->f('procent');

			if( $spent > ($budget_t*($procent/100)) ){

				$over = $spent-($budget_t*($procent/100));
				$procent_value =$budget_t*($procent/100);
				$this->send_budget_mail($project->f('p_name'),$project->f('p_id'),$project->f('c_name'), $budget_t, $spent, $procent,$over, $procent_value,$currency_show,$contract_id);
			}

		}
		return true;
	}

	function send_budget_mail(&$project,&$p_id,&$customer,&$budget,&$spent,&$procent,&$over, &$procent_value,$currency = 0,$contract_id = 0){

        $mail = new PHPMailer();
        $mail->WordWrap = 50;

        $fromMail='notification@akti.com';
        $mail->SetFrom($fromMail, $fromMail);

        $status_rate=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$p_id."'");
        if($status_rate == 0){
        	$proj_type='hours';
        }else{
        	$proj_type='days';
        }
        if($contract_id){
           $subject= 'Contract Budget Alert:'.$customer.' / '.$project;
           $body='The contract '.$project.' has reached the '.$procent.'% threshold: '.display_number($spent).' '.($currency==1 ? get_currency(ACCOUNT_CURRENCY_TYPE) : $proj_type).' out of '.display_number($budget).' '.($currency==1 ? get_currency(ACCOUNT_CURRENCY_TYPE) : $proj_type).'.';
        }else{
           $subject= 'Project Budget Alert:'.$customer.' / '.$project;
           $body='The project '.$project.' has reached the '.$procent.'% threshold: '.display_number($spent).' '.($currency==1 ? get_currency(ACCOUNT_CURRENCY_TYPE) : $proj_type).' out of '.display_number($budget).' '.($currency==1 ? get_currency(ACCOUNT_CURRENCY_TYPE) : $proj_type).'.';
        }

        $mail->Subject = $subject;
        $mail->MsgHTML(nl2br($body));

        $this->db->query("SELECT user_id FROM project_user WHERE project_id='".$p_id."' AND manager='1' ");
        $k = 0;
        while ($this->db->move_next()) {
        	$email = $this->dbu->field("SELECT email FROM users WHERE user_id='".$this->db->f('user_id')."' ");
        	$mail->AddAddress($email);
        	$k++;
        }
        if($k > 0){
        	$mail->Send();

			$sent_date= time();
			$this->db->query("UPDATE projects SET alert_sent='1',date_sent='".$sent_date."' WHERE project_id='".$p_id."'");
		}
		return true;
	}

	/****************************************************************
	* function add_hour(&$in)                                          *
	****************************************************************/
	function add_hour(&$in)
	{

		$notice_message=false;
		if(isset($in['task_row']) && !empty($in['task_row'])){
			foreach ($in['task_row'] as $key => $value){
				foreach($value['task_day_row'] as $key1=>$value1){
					$closed = $this->db->field("SELECT closed FROM projects WHERE project_id=(SELECT project_id FROM task_time WHERE task_time_id='".$value1['TASK_DAY_ID']."') ");
					if(!$closed){
						$this->db->query("UPDATE task_time SET hours='".$value1['HOURS']."' WHERE task_time_id='".$value1['TASK_DAY_ID']."'");
					}else{
						$notice_message=true;
						msg::notice(gm("Some hours where not added because the project is closed"),"notice");
					}
				}		
			}
			if(!$notice_message){
				msg::success(gm("Time has been successfully added."),"success");
			}
		}
		else{
			msg::error(gm("Please add a Project first"),"error");
		}
		$this->check_project_budget();
		return true;
	}

	/****************************************************************
	* function add_hour_simple(&$in)                                          *
	****************************************************************/
	function add_hour_simple(&$in)
	{
		$notice_message=false;
		$closed = $this->db->field("SELECT closed FROM projects WHERE project_id=(SELECT project_id FROM task_time WHERE task_time_id='".$in['TASK_DAY_ID']."') ");
		if(!$closed){
			$this->db->query("UPDATE task_time SET hours='".$in['HOURS']."' WHERE task_time_id='".$in['TASK_DAY_ID']."'");
			if(!$in['HOURS']){
				$this->db->query("UPDATE servicing_support_sheet SET start_time='0',end_time='0',`break`='0', total_hours='0' WHERE task_time_id='".$in['TASK_DAY_ID']."' ");
			}
		}else{
			$notice_message=true;
			msg::notice(gm("Some hours where not added because the project is closed"),"notice");
		}
		$this->check_project_budget();
		json_out($in);
	}

	/****************************************************************
	* function delete_time(&$in)                                    *
	****************************************************************/
	function delete_time(&$in){

		if(!$this->delete_time_validate($in)){
			json_out($in);
			return false;
		}
		foreach ($in['task_day_row'] as $key => $value){
			$this->db->query("SELECT date FROM task_time WHERE task_time_id='".$value['TASK_DAY_ID']."'");
			$this->db->move_next();
			$this->db->query("DELETE FROM timesheet_log WHERE start_date='".$this->db->f('date')."' ");
			$this->db->query("DELETE FROM task_time WHERE task_time_id='".$value['TASK_DAY_ID']."' ");
			$this->db->query("DELETE FROM servicing_support_sheet WHERE task_time_id='".$value['TASK_DAY_ID']."' ");
		}
		msg::success(gm("Project entries deleted"),"success");
		return true;
	}

	/****************************************************************
	* function delete_time_validate(&$in)                           *
	****************************************************************/
	function delete_time_validate(&$in){
		$can_delete = true;
		foreach ($in['task_day_row'] as $key => $value) {
			$item = $this->db->query("SELECT * FROM task_time WHERE task_time_id='".$value['TASK_DAY_ID']."' ");
			if($item->f('approved') || $item->f('submited') == 1 || $item->f('billed') ){
				$can_delete = false;
				break;
			}
		}
		if(!$can_delete){
			msg::error(gm("Entries locked"),"error");
		}		
		return $can_delete;
	}

	/****************************************************************
	* function submit_hour(&$in)		                            *
	****************************************************************/
	function submit_hour(&$in){

		if(!$this->add_hour($in)){
			json_out($in);
			return false;
		}
		//$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_name = $this->dbu->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		$user_name->next();
		$name = htmlspecialchars_decode(stripslashes($user_name->f('first_name')." ".$user_name->f('last_name')));

		// if we don't have a task time but we have an expense,
		// we have to make a dummy task time in order to see it on
		// projects -> timesheets
		// and than we have to hide the dummy task time on task times
		// which is pretty stupid.
		// okay dum dum
		$is_task = $this->db->field("SELECT COUNT(task_time_id) FROM task_time WHERE `date` BETWEEN '".$in['week_s']."' AND '".$in['week_e']."' AND user_id='".$in['user_id']."'  ");
		if($is_task == 0){//make dummy task time

			$expense = $this->db->query("SELECT project_expenses.*, projects.company_name AS c_name, projects.name AS p_name, expense.name AS e_name,
										expense.unit_price, expense.unit, projects.customer_id as customer_id, projects.active
								   FROM project_expenses
								   INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
								   INNER JOIN projects ON project_expenses.project_id=projects.project_id
								   WHERE project_expenses.user_id='".$in['user_id']."' AND project_expenses.date BETWEEN '".$in['week_s']."' AND '".$in['week_e']."' ORDER BY id");
			while($expense->next()){
				$task_time_id = $this->db->insert("INSERT INTO task_time SET
				                  user_id         = '".$in['user_id']."',
				                  project_id	 		= '".$expense->f('project_id')."',
				                  customer_id	 		= '".$expense->f('customer_id')."',
				                  date            		= '".$expense->f('date')."',
				                  hours				= '0',
				                  notes		    		= 'no_task_just_expense' ");
			}
		}
		$time_auto_app=$this->db->field("SELECT value FROM settings WHERE constant_name='TIME_AUTO_APPROVE' ");
		$auto_app="";
		if($time_auto_app=='1'){
			$auto_app=",approved='1' ";
		}
		$this->db->query("UPDATE task_time SET submited='1'".$auto_app." WHERE `date` BETWEEN '".$in['week_s']."' AND '".$in['week_e']."' AND user_id='".$in['user_id']."'  ");
		$this->db->query("INSERT INTO timesheet_log SET user_id='".$in['user_id']."', message='".(gm('Timesheet submitted by').' '.addslashes($name))."', date='".date(ACCOUNT_DATE_FORMAT)."', start_date='".$in['week_s']."'  ");
		msg::success(gm('Timesheet submitted.'),"success");

		//$this->send_manage_mail($in);
		return true;
	}

	function send_manage_mail(&$in){
//		require_once ('class.phpmailer.php');
//        include_once ("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
        $dbu = new sqldb();
        foreach($in['task_row'] as $key=>$value){
        	$dbu->query("SELECT user_id FROM project_user WHERE manager='1' AND project_id='".$value['project_id']."' ");
	        while ($dbu->move_next()) {
	        	$user_list .= $dbu->f('user_id').',';
	        }
        }
        $user_list = rtrim($user_list,',');
        if($user_list){
	       global $database_config;
			$db_config = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
			);

			$db = new sqldb($db_config);
	        $email = $db->query("SELECT email FROM users WHERE user_id IN ($user_list)");
	        $emails = array();
	        while ($email->next()) {
	        	if(!in_array($email->f('email'),$emails)){
	        		array_push($emails,$email->f('email'));
	        	}
	        }

	        $mail = new PHPMailer();
	        $mail->WordWrap = 50;
	        $mail->Encoding="base64";
	        /*$mail->IsSMTP(); // telling the class to use SMTP
	        $mail->SMTPDebug = 1; // enables SMTP debug information (for testing)
	        // 1 = errors and messages
	        // 2 = messages only
	        $mail->SMTPAuth = true; // enable SMTP authentication
	        $mail->Host = SMTP_HOST; // sets the SMTP server
	        $mail->Port = SMTP_PORT; // set the SMTP port for the GMAIL server
	        $mail->Username = SMTP_USERNAME; // SMTP account username
	        $mail->Password = SMTP_PASSWORD; // SMTP account password*/

	        $fromMail='timesheet@akti.com';
	        $mail->SetFrom($fromMail, $fromMail);

	        //$code = $this->dbu->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id='".$in['user_id']."' ");
	        $code = $this->dbu->field("SELECT lang.code FROM lang INNER JOIN users ON lang.lang_id=users.lang_id WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
	        if($code == 'nl'){$code = 'du';}
	        //$user = $dbu->field("SELECT user_name FROM project_user WHERE user_id='".$in['user_id']."' ");
	        $user=get_user_name($in['user_id']);
	        $message_data=get_sys_message('timesheetmess',$code);
// print_r($message_data);exit();
	       	$table = '<table border="1" cellpadding="5" cellspacing="0"><thead><tr><th>&nbsp;</th>';
	        for($i = $in['week_s']; $i <= $in['week_e']; $i += 86400)
			{
				$table .='<th width="60px" >'.date('d',$i).' '.gm(date('M',$i)).'</th>';
			}
	        $table .= '<th width="60px" >'.gm('Total').'</th></tr></thead><tbody>';
	        $p = $this->db->query("SELECT projects.name AS p_name, projects.company_name AS c_name, tasks.task_name AS t_name, projects.project_id, tasks.task_id
			FROM task_time
			INNER JOIN projects ON task_time.project_id=projects.project_id
			INNER JOIN tasks ON task_time.task_id=tasks.task_id
			WHERE user_id='".$in['user_id']."' AND date BETWEEN  '".$in['week_s']."' AND  '".$in['week_e']."'  GROUP BY tasks.task_id");
	        while ($p->next()) {
	        	$table.='<tr><td>'.$p->f('c_name').' ><br>'.$p->f('p_name').'<br>'.$p->f('t_name').'</td>';
        		$t = $this->db->query("SELECT task_time.hours, task_time.project_status_rate
			FROM task_time
			WHERE user_id =  '".$in['user_id']."' AND task_time.project_id='".$p->f('project_id')."' AND task_time.task_id='".$p->f('task_id')."'
			AND date BETWEEN  '".$in['week_s']."' AND  '".$in['week_e']."' AND submited='1' ORDER BY task_time.date ASC");
        		$h = 0;
        		$days=0;
        		while ($t->next()) {
        			if($t->f('project_status_rate') == 0){
        				$table .='<td align="center" >'.number_as_hour($t->f('hours')).' '.gm('Hours').'</td>';
					$h +=$t->f('hours');
        			}else{
        				$table .='<td align="center" >'.$t->f('hours').' '.gm('Days').'</td>';
					$days +=$t->f('hours');
        			}				
        		}
        		$table .='<td align="center" >'.number_as_hour($h).' '.gm('Hours').'<br/>'.$days.' '.gm('Days').'</td></tr>';
       		}

       		//expenses
       		$expense = $this->db->query("SELECT project_expenses.*, projects.company_name AS c_name, projects.name AS p_name, expense.name AS e_name,
       									expense.unit_price, expense.unit, projects.customer_id as customer_id, projects.active
       							   FROM project_expenses
       							   INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
       							   INNER JOIN projects ON project_expenses.project_id=projects.project_id
       							   WHERE project_expenses.user_id='".$in['user_id']."' AND project_expenses.date BETWEEN '".$in['week_s']."' AND '".$in['week_e']."' ORDER BY id");
       		$q=0;
       		$expense_data = array();
       		while($expense->next()){

       			$amount = place_currency(display_number($expense->f('amount')));
       			if($expense->f('unit_price')){
       				$amount = place_currency(display_number(($expense->f('amount') * $expense->f('unit_price'))))." (".$expense->f('amount')." ".$expense->f('unit').")";
       			}

       			$expense_data[$q]['date'] = date(ACCOUNT_DATE_FORMAT,$expense->f('date'));
       			$expense_data[$q]['company'] = $expense->f('c_name');
       			$expense_data[$q]['project'] = $expense->f('p_name');
       			$expense_data[$q]['expense'] = $expense->f('e_name');
       			$expense_data[$q]['amount'] = $amount;
       			$expense_data[$q]['notes'] = split_lines($expense->f('note')) ? split_lines($expense->f('note')):'&nbsp;';
       			$q++;
       		}
       		if($q>0){
       			$table .='<tr><td colspan="9" align="center"><strong>'.gm("Expenses").'</strong></td></tr><tr><td><strong>'.gm("Date").'</strong></td><td colspan="3"><strong>'.gm("Company").'</strong></td><td colspan="2"><strong>'.gm("Amount").'</strong></td><td colspan="3"><strong>'.gm("Notes").'</strong></td></tr>';
       			for($s=0;$s<count($expense_data);$s++){
       				$table .='<tr><td>'.$expense_data[$s]['date'].'</td><td colspan="3">'.$expense_data[$s]['company'].'<br>'.$expense_data[$s]['project'].'<br>'.$expense_data[$s]['expense'].'</td><td colspan="2">'.$expense_data[$s]['amount'].'</td><td colspan="3">'.$expense_data[$s]['notes'].'</td></tr>';
       			}
       		}


   			$table .='</tbody></table>';
   			// echo $table;exit();
	        $subject=$message_data['text'];
			$subject=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject );
			$subject=str_replace('[!USER_NAME!]',utf8_decode($user), $subject);
			$subject=str_replace('[!START_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_s']), $subject);
			$subject=str_replace('[!END_DATE!]',date(ACCOUNT_DATE_FORMAT,$in['week_e']), $subject);
			$subject=str_replace('[!SUMMARY!]',$table, $subject);
			$subject=str_replace('[!DATE!]',date(ACCOUNT_DATE_FORMAT), $subject);

			$body = $subject;

	        $subject_text = $message_data['subject'];
        	$subject_text=str_replace('[!ACCOUNT_COMPANY!]',utf8_decode(ACCOUNT_COMPANY), $subject_text );

	        $subject= $code == 'en' ? '['.utf8_decode(ACCOUNT_COMPANY).'] Timesheet submitted.' : $subject_text;
	        $mail->Subject = $subject;
	        $mail->MsgHTML(nl2br($body));


	           	if($message_data['use_html']){
	           		$mail->IsHTML(true);
	           		// $head = '<style>p {	margin: 0px; padding: 0px; }</style>';
				$body = stripslashes($body);
	           		$mail->Body    = $head.$body;
	           	}

			foreach ($emails as $key => $value ){
	        	$mail->AddAddress($value);
			}
			$mail->Send();
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function add_tasks(&$in)
	{
		$i=0;
		$y=0;
		$is_adhoc = false;
		if(!$in['project_id']){
			$is_adhoc = true;
		}

		if($in['tasks_id'] && is_array($in['tasks_id']) ) {
			foreach ($in['tasks_id'] as $in['task_id']) {
				if($is_adhoc){
					$in['project_id'] = '';
					if($in['task_type'] && $in['task_type']=='2'){
						$in['task_name']=stripslashes($this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$in['task_id']."' "));
						$in['article_id']=$in['task_id'];
					}else{
						$in['task_name']=stripslashes($this->db->field("SELECT default_name FROM default_data WHERE default_main_id='0' AND type='task_no' AND active='1' AND default_id='".$in['task_id']."' "));
					}
					
				}
				if($this->add_task($in)){
					$i++;
				}else{
					$y++;
				}
			}
		}
		$in['status_rate']=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$in['project_id']."' ");
		$task_single = gm('Task');
		$task_plural = gm('tasks');
		msg::success(gm('Added').' '.$i.' '.($i > 1 ? $task_plural : $task_single).'. '.$y.' '.gm('Duplicate').' '.($y > 1 ? $task_plural : $task_single),"success");
		return true;
	}

	/****************************************************************
	* function add task(&$in)                                          *
	****************************************************************/
	function add_task(&$in)
	{
		if(!$this->add_validate($in)){
			return false;
		}
		$this->db->query("UPDATE projects SET step='0' WHERE project_id='".$in['project_id']."' ");
		$in['status_rate']=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$in['project_id']."' ");
		$in['customer_id']=$this->db->field("SELECT customer_id FROM projects WHERE project_id='".$in['project_id']."' ");
		$in['billable'] = $this->db->field("SELECT billable FROM tasks WHERE task_id='".$in['task_id']."' ");
		foreach ($in['day_row'] as $key => $value ){
			$this->db->query("INSERT INTO task_time SET 				  user_id         = '".$in['user_id']."',
														                  task_id	     		= '".$in['task_id']."',
														                  project_id	 		= '".$in['project_id']."',
														                  customer_id	 		= '".$in['customer_id']."',
														                  contract_id	 		= '".$in['contract_id']."',
														                  date            = '".$value['TIMESTAMP']."',
														                  hours	          = '0',
														                  billable			  = '".$in['billable']."',
														                  project_status_rate      ='".$in['status_rate']."' ");
		}

		msg::success(gm('Project entries added.'),"success");
		return true;
	}

	/****************************************************************
	* function add_validate(&$in)                                   *
	****************************************************************/
	function add_validate(&$in)
	{
		$is_ok = true;
		if($in['project_id'] && $in['task_id'] && $in['user_id']){
			foreach($in['task_row'] as $key=>$value){
				foreach($value['task_day_row'] as $key1=>$value1){
					$this->db->query("SELECT task_time.* FROM task_time WHERE project_id='".$in['project_id']."'
		                     AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."' AND task_time_id='".$value1['TASK_DAY_ID']."'
		                     ");
					while($this->db->next()){
						if($this->db->f('billed') == '1'){
							$in['duplicate'] = 1;
							msg::error(gm('Task was invoiced.'),"error");
							$is_ok = false;
						}else{
							$in['duplicate'] = 1;
							msg::error(gm('Task was already added.'),"error");
							$is_ok = false;
						}
					}
				}
			}
		}else if(!$in['project_id'] && $in['c_id'] && $in['task_id']){
			$project_active='2';
			$project_name="ad hoc";
			if($in['task_type'] && $in['task_type']=='2'){
				$project_active='3';
				$project_name="service";
			}
			$this->db->query("SELECT project_id FROM projects WHERE customer_id='".$in['c_id']."' AND active='".$project_active."' AND contract_id='0' ");
			if($this->db->move_next()){
				$in['project_id'] = $this->db->f('project_id');
				if($in['task_type'] && $in['task_type']=='2'){
					$this->db->query("SELECT task_id FROM tasks WHERE project_id='".$in['project_id']."' AND article_id='".$in['article_id']."' ");
				}else{
					$this->db->query("SELECT task_id FROM tasks WHERE project_id='".$in['project_id']."' AND task_name='".addslashes($in['task_name'])."' ");
				}				
				if($this->db->move_next()){
					$in['task_id'] = $this->db->f('task_id');
					if($in['task_type'] && $in['task_type']=='2'){
						$article_id=$in['article_id'];
						$billable = 1;
						$params = array(
		                  'article_id' => $in['article_id'], 
		                  'price' => 0, 
		                  'quantity' => 1,
		                  'customer_id' => $in['c_id'],
		                  'cat_id' => $in['cat_id'],
		                  'asString' => true
		                );
                		$value= $this->getArticlePrice($params);
                		$this->db->query("UPDATE tasks SET t_h_rate='".$value."' WHERE task_id='".$in['task_id']."' ");
					}
					foreach($in['task_row'] as $key=>$value){
						foreach($value['task_day_row'] as $key1=>$value1){
							$this->db->query("SELECT task_time.* FROM task_time WHERE project_id='".$in['project_id']."'
				                     AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."' AND task_time_id='".$value1['TASK_DAY_ID']."'
				                     ");
							while($this->db->next()){
								if($this->db->f('billed') == '1'){
									$in['duplicate'] = 1;
									msg::error(gm('Task was invoiced.'),"error");
									$is_ok = false;
								}else{
									$in['duplicate'] = 1;
									msg::error(gm('Task was already added.'),"error");
									$is_ok = false;
								}
							}
						}
					}
				}else{
					$article_id='0';
					if($in['task_type'] && $in['task_type']=='2'){
						$article_id=$in['article_id'];
						$billable = 1;
						$params = array(
		                  'article_id' => $in['article_id'], 
		                  'price' => 0, 
		                  'quantity' => 1,
		                  'customer_id' => $in['c_id'],
		                  'cat_id' => $in['cat_id'],
		                  'asString' => true
		                );
                		$value= $this->getArticlePrice($params);	
                	}else{
						$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
						$this->db->move_next();
						$value = $this->db->f('value');
						$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
						$this->db->move_next();
						$billable = 0;
						if($this->db->f('value') == 'yes'){
							$billable = 1;
						}
						$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
						if($this->db->move_next()){
							$value = $this->db->f('value');
						}
					}				
					$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."',article_id='".$article_id."' ");
					$is_ok = true;
				}
			}else{
				$article_id='0';
				if($in['task_type'] && $in['task_type']=='2'){
					$article_id=$in['article_id'];
					$billable = 1;
					$params = array(
	                  'article_id' => $in['article_id'], 
	                  'price' => 0, 
	                  'quantity' => 1,
	                  'customer_id' => $in['c_id'],
	                  'cat_id' => $in['cat_id'],
	                  'asString' => true
	                );
            		$value= $this->getArticlePrice($params);
				}else{
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_rate' ");
					$this->db->move_next();
					$value = $this->db->f('value');
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND default_name='task_billable' ");
					$this->db->move_next();
					$billable = 0;
					if($this->db->f('value') == 'yes'){
						$billable = 1;
					}
					$this->db->query("SELECT value FROM default_data WHERE default_main_id='".$in['task_id']."' AND type='task_no'  AND default_name='".$in['c_id']."' ");
					if($this->db->move_next()){
						$value = $this->db->f('value');
					}
				}			
				$in['project_id'] = $this->db->insert("INSERT INTO projects SET customer_id='".$in['c_id']."', name='".$project_name."', active='".$project_active."', billable_type='1', invoice_method='1', company_name=(SELECT name FROM customers WHERE customer_id='".$in['c_id']."'), stage='1', step='0' ");
				$u_name = get_user_name($in['user_id']);
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."', user_id='".$in['user_id']."', user_name='".addslashes($u_name)."' ");
				$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".addslashes($in['task_name'])."', billable='".$billable."', t_h_rate='".$value."', default_task_id='".$in['task_id']."',article_id='".$article_id."' ");
				$is_ok = true;
			}
		}else{
			msg::error(gm("Please select a project or a customer"),"error");
			$is_ok = false;
		}
		return $is_ok;
	}

	function add_expense(&$in){
		if(!$this->validate_add_expense($in)){
			json_out($in);
			return false;
		}
		if($in['billable']=='true'){
			$in['billable']='1';
		}
		$in['id'] = $this->db->insert("INSERT INTO project_expenses SET expense_id='".$in['expense_id']."',
													 project_id='".$in['project_id']."',
													 user_id='".$in['user_id']."',
													 amount='".$in['amount']."',
													 note='".$in['note']."',
													 date='".$in['date']."',
													 billable='".$in['billable']."',
													 is_service='".$in['exp_is_srv']."',
													 service_id = '".$in['service_id']."' ");
		if(!empty($_FILES['Filedata']['name'])){
			$this->upload_file($in);
		}
		msg::success(gm('Expense added'),"success");
		json_out($in);
	}

	function update_expense(&$in){
		if(!$this->validate_add_expense($in)){
			json_out($in);
			return false;
		}
		if($in['billable']=='true'){
			$in['billable']='1';
		}
		$this->db->query("UPDATE project_expenses SET expense_id='".$in['expense_id']."',
													 project_id='".$in['project_id']."',
													 user_id='".$in['user_id']."',
													 amount='".$in['amount']."',
													 note='".$in['note']."',
													 date='".$in['date']."',
													 billable = '".$in['billable']."'
													 WHERE id='".$in['id']."' ");
		if(!empty($_FILES['Filedata']['name'])){
			$this->upload_file($in);
		}
		msg::success(gm('Expense Updated'),"success");
		json_out($in);
	}

	function validate_add_expense(&$in){
		$is_ok = true;

		$v = new validation($in);
		if(!$in['project_id'] && !$in['customer_id']){
			msg::set('project_id',"Please select a project or a customer");
			return false;
		}
		elseif (!$in['project_id'] && $in['customer_id']){
			$this->db->query("SELECT project_id FROM projects WHERE customer_id='".$in['customer_id']."' AND active='2' ");
			if($this->db->move_next()){
				$in['project_id'] = $this->db->f('project_id');
				$is_ok = true;
			}else{
				$in['project_id'] = $this->db->insert("INSERT INTO projects SET customer_id='".$in['customer_id']."', name='ad hoc', active='2', billable_type='1', invoice_method='1', company_name=(SELECT name FROM customers WHERE customer_id='".$in['customer_id']."'), stage='1', step='0' ");
				//$u_name = $this->dbu->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."' ");
				$u_name = $this->dbu->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."', user_id='".$_SESSION['u_id']."', user_name='".$u_name."' ");
				$is_ok = true;
			}
		}elseif (!$in['project_id']){
			msg::set('project_id',"Please select a project or a customer");
			return false;

		}
		$v->field('expense_id', gm('ID'), 'required', gm("Please select a category"));
		$v->field('amount',gm('Quantity'),'required:numeric');
		$v->field('date',gm('Date'),'required');

		$is_ok = $v->run();
		return $is_ok;
	}

	function upload_file(&$in)
	{
	    global $_FILES, $is_live;
        $allowed['.png']=1;
        $allowed['.tiff']=1;
        $allowed['.jpg']=1;
        $allowed['.jpeg']=1;
        $f_ext=substr($_FILES['Filedata']['name'],strrpos($_FILES['Filedata']['name'],"."));
        if(!$allowed[strtolower($f_ext)])
        {
        	msg::error(gm("Only jpg, jpeg, png and tiff files are allowed."),"error");
        	json_out($in);
        	return false;
        }

     	$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."'");
    	if($this->db->move_next())
    	{
        	@unlink(UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
			$this->db->query("UPDATE project_expenses SET picture=NULL WHERE id='".$in['id']."'");
    	}


		$f_ext = strtolower($f_ext);
        $f_title="receipt_".$in['id'].$f_ext;
        if(!file_exists(UPLOAD_PATH.DATABASE_NAME."/receipt/")){
        	mkdir(UPLOAD_PATH.DATABASE_NAME."/receipt/", 0775, true);
        }
        $f_out=UPLOAD_PATH.DATABASE_NAME."/receipt/".$f_title;

        if(!$_FILES['Filedata']['tmp_name'])
        {
        	msg::error(gm("Please choose a file!"),"error");
        	json_out($in);
            return false;
        }

        $filename = $_FILES['Filedata']['tmp_name'];
		if (FALSE === move_uploaded_file($_FILES['Filedata']['tmp_name'],$f_out)) {
			msg::error(gm("Unable to upload the file."),"error");
			json_out($in);
            	return false;
		}

		@chmod($f_out, 0664);
    		$this->db->query("UPDATE project_expenses SET picture='".$f_title."' WHERE id='".$in['id']."' ");
        return true;
	}

	function delete_image(&$in){

		$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."' ");
		if($this->db->move_next()){
			@unlink(UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
			$this->db->query("UPDATE project_expenses set picture='' WHERE id='".$in['id']."' ");
		}
		msg::success(gm('Expense attachement deleted'),"success");
		return true;
	}

	function delete_expense(&$in){

		$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."' ");
		if($this->db->move_next()){
			@unlink(UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
		}
		$this->db->query("DELETE FROM project_expenses WHERE id='".$in['id']."' ");
		msg::success(gm('Expense deleted'),"success");
		return true;
	}

	function calendar_sync(&$in){
		global $config;
		//$now= strtotime($in['start_date']);
		$now=time();
		$month = date('n',$now);
		$year = date('Y',$now);

		$month_first_day=mktime(0,0,0,$month,1,$year);
		$month_third=strtotime('+3 months',$month_first_day);

		/*if($in['type_s']=='1'){
			$start_t=$in['start_time'];
			$end_t=$start_t+86399;
		}else if($in['type_s']=='2'){
			$start_t=$in['start_time'];
			$end_t=$start_t+604799;
		}else{
			$start_t=$in['start_time'];
			$end_t=mktime(23,59,59,$month,date('t',$start_t),$year);
		}*/

		$ch = curl_init();
		//$token_id=$this->dbu->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$token_id=$this->dbu->field("SELECT token FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/events?starts_after='.time().'&ends_before='.$month_third);

        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){ 						 
			$err=json_decode($put);
    		msg::error($err->message,"error");
    	}else{
    		$data=json_decode($put);
    		foreach($data as $key=>$value){
    			$hours_check=$this->db->field("SELECT log_id FROM logging WHERE event_id='".$value->id."' ");
    			if($hours_check){
    				continue;
    			}
    			$meetings_check=$this->db->field("SELECT customer_meeting_id FROM customer_meetings WHERE event_id='".$value->id."' ");
    			if($meetings_check){
    				continue;
    			}
    			switch ($value->when->object) {
    					case 'time':
    						$start_time=0;
    						$end_time=$value->when->time;
    						break;
    					case 'timespan':
    						$start_time=$value->when->start_time;
    						$end_time=$value->when->end_time;
    						break;
    					case 'date':
    						$start_time=0;
    						$end_time=strtotime($value->when->date);
    						break;
    					case 'datespan':		
    						$start_time=strtotime($value->when->start_date);
    						$end_time=strtotime($value->when->end_date);
    						break;
    			}
    			$event_exists=$this->db->field("SELECT event_id FROM nylas_events WHERE event_id='".$value->id."' ");
    			if($event_exists){
    				$this->db->query("UPDATE nylas_events SET
    								description='".addslashes($value->description)."',
    								location='".$value->location."',
    								title='".addslashes($value->title)."',
    								event_type='".$value->when->object."',
    								start_time='".$start_time."',
    								end_time='".$end_time."',
    								calendar_id='".$value->calendar_id."'
    								WHERE event_id='".$value->id."' ");
    			}else{
    				$this->db->query("INSERT INTO nylas_events SET
    								user_id='".$_SESSION['u_id']."',
    								event_id='".$value->id."',
    								description='".addslashes($value->description)."',
    								location='".$value->location."',
    								title='".addslashes($value->title)."',
    								event_type='".$value->when->object."',
    								start_time='".$start_time."',
    								end_time='".$end_time."',
    								calendar_id='".$value->calendar_id."' ");
    			}
    			$this->db->query("DELETE FROM nylas_participants WHERE event_id='".$value->id."' ");
    			if(!empty($value->participants)){
    				foreach($value->participants as $key1=>$value1){
    					$this->db->query("INSERT INTO nylas_participants SET 
    						event_id='".$value->id."',
    						name='".$value1->name."',
    						email='".$value1->email."',
    						comment='".$value1->comment."' ");
    				}		
    			}		
    		}
    		
    		$calendar_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='NYLAS_CALENDAR_ID' ");
    		if($calendar_exist){
    			$this->db->query("UPDATE settings SET value='".$in['calendar_id']."' WHERE constant_name='NYLAS_CALENDAR_ID' ");
    		}else{
    			$this->db->query("INSERT INTO settings SET constant_name='NYLAS_CALENDAR_ID', value='".$in['calendar_id']."' ");
    		}
    		//$this->sync_events($start_t,$end_t,$in['calendar_id']);
    		$this->sync_events($now,$month_third,$in['calendar_id']);
    	}
    	msg::success(gm("Sync done successfully"),"success");
		return true;
	}

	function update_status(&$in){
		if($in['status'] == '0' || $in['status'] == '1' || $in['status'] == '2' || $in['status'] == '3'){
			$this->db->query("UPDATE logging SET finished='0', finish_date='0', status_other='".$in['status']."' WHERE log_id='".$in['log_id']."'");
		}else if($in['status'] == '4'){
			$finish_time=time();
			$this->db->query("UPDATE logging SET finished='1', finish_date='".$finish_time."' WHERE log_id='".$in['log_id']."' ");
		}

		msg::success(gm("Status changed succesfully"),"success");
		json_out($in);
	}

	function markDoneLog(&$in){
		$finish_time=$in['done']=='1' ? time() : 0;
		$this->db->query("UPDATE logging SET finished='".$in['done']."', finish_date='".$finish_time."'  WHERE log_id='".$in['id']."' ");
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}

	function markReadLog(&$in)
	{
		$status = '1';
		if($in['read'] == 1){
			$status = '0';
		}
		$this->db->query("UPDATE logging SET done='".$status."' WHERE log_id='".$in['id']."' ");
		msg::success(gm("Changes saved"),"success");
		return true;
	}

	function delete_log(&$in)
	{
		$this->db->query("UPDATE logging SET done='3' WHERE log_id='".$in['log_id']."' ");
		msg::success(gm('Message archived'),"success");
		return true;
	}

	function archive_log(&$in)
	{
		$this->db->query("UPDATE logging SET done='2' WHERE log_id='".$in['log_id']."' ");
		msg::success(gm('Message archived'),"success");
		return true;
	}

	function activate_log(&$in)
	{
		$this->db->query("UPDATE logging SET done='1' WHERE log_id='".$in['log_id']."' ");
		msg::success(gm('Message activated'),"success");
		return true;	
	}

	function sync_events($start_t,$end_t,$calendar_id){
		global $config;
		//$token_id=$this->dbu->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$token_id=$this->dbu->field("SELECT token FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$hours = $this->db->query("SELECT logging.*,customer_contact_activity.customer_contact_activity_id FROM logging
				INNER JOIN logging_tracked ON logging.user_id=logging_tracked.user_id AND logging.log_id=logging_tracked.log_id
				INNER JOIN customer_contact_activity ON logging_tracked.activity_id=customer_contact_activity.customer_contact_activity_id
				WHERE customer_contact_activity.contact_activity_type!='4' AND logging.to_user_id='".$_SESSION['u_id']."' AND (logging.due_date BETWEEN '".$start_t."' AND '".$end_t."' OR logging.finish_date BETWEEN '".$start_t."' AND '".$end_t."') AND logging.done < 2  AND logging.field_name!='service_id'");
		while($hours->next()){
			$contacts_arr=array();
			$contacts=$this->db->query("SELECT customer_contacts.firstname,customer_contacts.lastname,customer_contacts.email FROM customer_contact_activity_contacts
								INNER JOIN customer_contacts ON customer_contact_activity_contacts.contact_id=customer_contacts.contact_id
								WHERE customer_contact_activity_contacts.activity_id='".$hours->f('customer_contact_activity_id')."' AND customer_contact_activity_contacts.action_type='0' ");
			while($contacts->next()){
				array_push($contacts_arr, array('email'=>$contacts->f('email'),'name'=>$contacts->f('firstname').' '.$contacts->f('lastname')));
			}
			$event_data=array(
				'title'				=> $hours->f('log_comment'),
				'location'			=> '',
				'calendar_id'		=> $calendar_id,
				'when'				=> array('time'=>$hours->f('due_date')),
				'participants'		=> $contacts_arr,
				'description'		=> $hours->f('message'),
			);
			$event_data=json_encode($event_data);
			$ch = curl_init();			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $event_data);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
	        if($hours->f('event_id') == ''){
	        	curl_setopt($ch, CURLOPT_POST, true);
	        	curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/events');       	
	        }else{
	        	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	        	curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/events/'.$hours->f('event_id')); 
	        }       
		    $put = curl_exec($ch);
			$info = curl_getinfo($ch);
			if($info['http_code']>300 || $info['http_code']==0){
			    	//nothing
			}else{
				$data=json_decode($put);
			    $this->db->query("UPDATE logging SET event_id='".$data->id."' WHERE log_id='".$hours->f('log_id')."' ");
			}		
		}
		$meetings = $this->db->query("SELECT * FROM customer_meetings WHERE user_id='".$_SESSION['u_id']."' AND start_date BETWEEN '".$start_t."' AND '".$end_t."' ORDER BY start_date");
		while($meetings->next()){
			$contacts_arr=array();
			$contacts=$this->db->query("SELECT customer_contacts.firstname,customer_contacts.lastname,customer_contacts.email FROM customer_contact_activity_contacts
								INNER JOIN customer_contacts ON customer_contact_activity_contacts.contact_id=customer_contacts.contact_id
								WHERE customer_contact_activity_contacts.activity_id='".$meetings->f('customer_meeting_id')."' AND customer_contact_activity_contacts.action_type='1' ");
			while($contacts->next()){
				array_push($contacts_arr, array('email'=>$contacts->f('email'),'name'=>$contacts->f('firstname').' '.$contacts->f('lastname')));
			}
			$event_data=array(
				'title'				=> $meetings->f('subject'),
				'location'			=> $meetings->f('location'),
				'calendar_id'		=> $calendar_id,
				'when'				=> array('start_time'=>$meetings->f('start_date'),'end_time'=>$meetings->f('end_date')),
				'participants'		=> $contacts_arr,
				'description'		=> $meetings->f('message'),
			);
			$event_data=json_encode($event_data);
			$ch = curl_init();			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $event_data);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
	        if($meetings->f('event_id') == ''){
	        	curl_setopt($ch, CURLOPT_POST, true);
	        	curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/events');       	
	        }else{
	        	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	        	curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/events/'.$meetings->f('event_id')); 
	        }     
		    $put = curl_exec($ch);
			$info = curl_getinfo($ch);
			if($info['http_code']>300 || $info['http_code']==0){
			    	//nothing
			}else{
			    $data=json_decode($put);
			    $this->db->query("UPDATE customer_meetings SET event_id='".$data->id."' WHERE customer_meeting_id='".$meetings->f('customer_meeting_id')."' ");
			}
		}
	}

	function deleteNylasEvent(&$in){
		global $config;
		//$token_id=$this->dbu->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$token_id=$this->dbu->field("SELECT token FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$ch = curl_init();			
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
	    curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/events/'.$in['event_id']); 
	    $put = curl_exec($ch);
		$info = curl_getinfo($ch);

		if($info['http_code']>300 || $info['http_code']==0){ 						 
			$err=json_decode($put);
    		msg::error($err->message,"error");
    	}else{
    		$this->db->query("DELETE FROM nylas_events WHERE event_id='".$in['event_id']."' ");
    		$this->db->query("DELETE FROM nylas_participants WHERE event_id='".$in['event_id']."' ");
    		msg::success(gm("Changes saved"),"success");
    	}
		return true;
	}

	function markSeenMeeting(&$in){
		$notify=$in['read']=='1' ? 0 : 1;
		$this->db->query("UPDATE customer_meetings SET notify='".$notify."' WHERE customer_meeting_id='".$in['id']."' ");
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}

	function markSeenLog(&$in){
		$finish_time=$in['read']=='1' ? time() : 0;
		$this->db->query("UPDATE logging SET finished='".$in['read']."', finish_date='".$finish_time."', status_other='0'  WHERE log_id='".$in['id']."' ");
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}

	function markSeenNylas(&$in){
		$this->db->query("UPDATE nylas_events SET `read`='".$in['read']."' WHERE id='".$in['id']."' ");
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}

	function saveTimeLogs(&$in){
		foreach ($in['rows'] as $key => $value){
			$this->db->query("UPDATE servicing_support_sheet SET total_hours='".$value['line_h_i']."',start_time='".$value['start_time_i']."',end_time='".$value['end_time_i']."',break='".$value['break_i']."', notes='".trim($value['comment'])."' WHERE id='".$value['id']."' ");
		}
		$hours_count=0;
		$hours_data=$this->db->query("SELECT * FROM servicing_support_sheet WHERE task_time_id='".$in['task_time_id']."' ");
		while($hours_data->next()){
			if($hours_data->f('total_hours')==0){
				$hours_count+=($hours_data->f('end_time')-$hours_data->f('start_time')-$hours_data->f('break'));
			}else{
				$hours_count+=$hours_data->f('total_hours');
			}
		}
		$this->db->query("UPDATE task_time SET hours='".$hours_count."' WHERE task_time_id='".$in['task_time_id']."' ");
		msg::success(gm('Changes saved'),"success");
		json_out($in);
	}

	function getArticlePrice($in){
    $cat_id = $in['cat_id'];
    $article = $this->db->query("SELECT article_category_id, block_discount,price_type FROM pim_articles WHERE article_id='".$in['article_id']."' ");
    if($article->f('price_type')==1){

        $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");
          $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$in['article_id']."' and category_id='".$cat_id."' ");

          if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
              $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$in['article_id']."'");
          }else{
              $price_value=$price_value_custom_fam;

             //we have to apply to the base price the category spec
          $cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
            $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
            $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

            if($cat_price_type==2){
                  $article_base_price=get_article_calc_price($in['article_id'],3);
              }else{
                  $article_base_price=get_article_calc_price($in['article_id'],1);
              }

            switch ($cat_type) {
          case 1:                  //discount
            if($price_value_type==1){  // %
              $price = $article_base_price - $price_value * $article_base_price / 100;
            }else{ //fix
              $price = $article_base_price - $price_value;
            }
            break;
          case 2:                 //profit margin
            if($price_value_type==1){  // %
              $price = $article_base_price + $price_value * $article_base_price / 100;
            }else{ //fix
              $price =$article_base_price + $price_value;
            }
            break;
        }
          }

        if(!$price || $article->f('block_discount')==1 ){
            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$in['article_id']."' AND base_price=1");
          }
      }else{
        $price = $in['price'];
        // return $this->get_article_quantity_price($in);
      }

      $start= mktime(0, 0, 0);
      $end= mktime(23, 59, 59);
      $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$in['article_id']."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
      if($promo_price->move_next()){
        if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

          }else{
              $price=$promo_price->f('price');
          }
      }
    if($in['customer_id']){
        $customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
        if($customer_custom_article_price->move_next()){
              $price = $customer_custom_article_price->f('price');
          }
      }
      return $price;
  }

}//end class
