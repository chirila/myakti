<?php

class notification {

	function __construct() {
		$this->db = new sqldb();
		global $database_config;
		$this->database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($this->database_users);
	}

	function updateRead(&$in)
	{
		if(!$this->validate_notification($in)){
			json_out($in);
		}
		$this->db_users->query("UPDATE `notifications` SET msg_read='".$in['msg_read']."',updated_at='".time()."' WHERE id='".$in['id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function updateReadAll(&$in)
	{
		$this->db_users->query("UPDATE `notifications` SET msg_read='".$in['value']."',updated_at='".time()."' WHERE user_id='".$_SESSION['u_id']."' ");
		$in['msg_nr']=0;
		if(!$in['value']){
			$in['msg_nr']=$this->db_users->field("SELECT COUNT(id) FROM `notifications` WHERE user_id='".$_SESSION['u_id']."' AND msg_read='0' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function delete(&$in)
	{
		if(!$this->validate_notification($in)){
			json_out($in);
		}
		$this->db_users->query("DELETE FROM `notifications` WHERE id='".$in['id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function deleteAll(&$in)
	{
		$this->db_users->query("DELETE FROM `notifications` WHERE user_id='".$_SESSION['u_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}	

	function validate_notification(&$in)
	{
		$v = new validation($in);
		$v->field('id', 'ID', "required:exist[notifications.id.(user_id='".$_SESSION['u_id']."')]", "Invalid Id", $this->database_users);
		return $v->run();
	}

}
?>