<?php
// include ('apps/misc/model/time.php');

class service {

	private $db;
	public $time;

	function __construct() {
		$this->db = new sqldb();
		// $this->time = new time();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addServiceSheet(&$in)
	{
		if(!$this->addServiceSheet_validate($in)){
			return false;
		}

		$this->db->query("INSERT INTO servicing_support_sheet SET
											service_id = '".$in['servicing_sheet_id']."',
											task_id = '".$in['servicing_task_id']."',
											date = '".$in['service_date']."',
											start_time = '".$in['start_time']."',
											end_time = '".$in['end_time']."',
											break = '".$in['break']."',
											user_id = '".$in['sheet_user_id']."',
											notes='".addslashes($in['time_report'])."',
											internal_report='".$in['internal_report']."',
											extra_hours='".$in['extra_hours']."'
											");

		msg::success( gm('Entry added'),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addServiceSheet_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('servicing_sheet_id', gm('ID'), 'required:exist[servicing_support.service_id]', gm("Invalid ID"));
		// $v->field('servicing_task_id', gm('ID'), "required:exist[servicing_support_tasks.task_id.( service_id='".$in['servicing_sheet_id']."' )]", gm("Invalid ID"));
		$v->field('service_date','service_date','required');
		$v->field('start_time','start_time','required');
		$v->field('end_time','end_time','required');
		$is_ok = $v->run();
		if($is_ok === true){
			if($in['start_time'] > $in['end_time']){
				msg::error ( gm('Start time should be bigger then end time'),'error');
				$is_ok = false;
			}
		}
		if($in['break'] && $in['break'] >= ($in['end_time']-$in['start_time']) ){
			msg::error ( gm('Break is bigger then the hours'),'error');
			$is_ok = false;
		}
		/*if($is_ok === true && !$in['serv_sheet_id']){
			$this->db->query("SELECT * FROM servicing_support_sheet WHERE service_id = '".$in['servicing_sheet_id']."' AND task_id = '".$in['servicing_task_id']."' AND user_id = '".$in['sheet_user_id']."' AND date='".$in['service_date']."' ");
			if($this->db->next()){
				msg::$error =  gm('Duplicate entry');
				$is_ok = false;
			}
		}*/
		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function editServiceSheet(&$in)
	{
		if(!$this->editServiceSheet_validate($in)){
			return false;
		}

		$this->db->query("UPDATE servicing_support_sheet SET
											start_time = '".$in['start_time']."',
											end_time = '".$in['end_time']."',
											break = '".$in['break']."'
											WHERE id='".$in['serv_sheet_id']."'
											");
		$this->db->query("UPDATE servicing_support SET status='1' WHERE service_id='".$in['servicing_sheet_id']."' ");

		msg::success ( gm('Entry updated'),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function editServiceSheet_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('serv_sheet_id', gm('ID'), 'required:exist[servicing_support_sheet.id]', gm("Invalid ID"));
		$is_ok = $v->run();
		if($is_ok === true){
			return $this->addServiceSheet_validate($in);
		}
		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function nextAction($in)
	{
		$in['task_time_id'] = $this->db->field("SELECT task_time_id FROM task_time WHERE project_id='".$in['project_id']."' AND task_id='".$in['task_id']."' AND user_id='".$in['user_id']."' AND date='".$in['date']."' ");
		return $in['task_time_id'];
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteServiceSheet(&$in)
	{
		if(!$this->deleteServiceSheet_validate($in)){
			return false;
		}
		// $service = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['servicing_sheet_id']."' "); # get the info about the service
		// $sheet = $this->db->query("SELECT * FROM servicing_support_sheet WHERE id='".$in['serv_sheet_id']."' "); # get the info about the service entry
		$this->db->query("DELETE FROM servicing_support_sheet WHERE id='".$in['serv_sheet_id']."' "); # delete the entry

		msg::success ( gm('Changes saved'),'success');
		return false;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteServiceSheet_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('serv_sheet_id', gm('ID'), 'required:exist[servicing_support_sheet.id]', gm("Invalid ID"));
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function calculateHours($in)
	{
		$hours = 0;
		$sheet = $this->db->query("SELECT * FROM servicing_support_sheet WHERE service_id='".$in['servicing_sheet_id']."' AND user_id='".$in['user_id']."' AND date='".$in['service_date']."' ");
		while ($sheet->next()) {
			$hours += $sheet->f('end_time')-$sheet->f('start_time')-$sheet->f('break');
		}
		return $hours;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function add_expense(&$in){
		if(!$this->validate_add_expense($in)){
			return false;
		}

		$in['id'] = $this->db->insert("INSERT INTO project_expenses SET expense_id='".$in['expense_id']."',
													 project_id='',
													 user_id='".$in['user_id']."',
													 amount='".$in['amount']."',
													 note='".$in['note']."',
													 date='".$in['date']."',
													 billable='".$in['billabel']."',
													 is_service='".$in['exp_is_srv']."',
													 service_id='".$in['service_id']."' ");
		$in['namount'] =$in['amount'];
		$in['ndate'] = date(ACCOUNT_DATE_FORMAT,$in['date']);
		$this->db->query("SELECT * FROM expense WHERE expense_id='".$in['expense_id']."' ");
		$this->db->move_next();
		if($this->db->f('unit_price')){
			$in['amount'] = place_currency(display_number($in['amount'] * $this->db->f('unit_price')))."   (".$in['amount']." ".$this->db->f('unit').")";
		}else{
			$in['amount'] = place_currency(display_number($in['amount']));
		}

		if(!empty($_FILES['file_upload']['name'])){
			$this->upload_file($in);
		}
		msg::success ( gm('Expense added'),'success');

		return true;
	}

	function validate_add_expense(&$in){
		$is_ok = true;

		$v = new validation($in);

		$v->field('service_id', gm('ID'), 'required', gm("Please select a Service"));
		$v->field('expense_id', gm('ID'), 'required', gm("Please select a category"));
		$v->field('amount',gm('Quantity'),'required:numeric');

		$is_ok = $v->run();
		if ($is_ok==false)
		{
			$in['no_hide'] = 1;
		}
		else
		{
			$in['no_hide'] = 0;
		}
		return $is_ok;
	}

	function upload_file(&$in)
	{
	    global $_FILES, $is_live, $script_path;
        $allowed['.png']=1;
        $allowed['.tiff']=1;
        $allowed['.jpg']=1;
        $allowed['.jpeg']=1;
        $f_ext=substr($_FILES['file_upload']['name'],strrpos($_FILES['file_upload']['name'],"."));
        if(!$allowed[strtolower($f_ext)])
        {
        	$in['error'].="Only jpg, jpeg, png and tiff files are allowed.";
        	return false;
        }

     	$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."'");
    	if($this->db->move_next())
    	{
        	@unlink('../../'.$script_path.UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
			$this->db->query("UPDATE project_expenses SET picture=NULL WHERE id='".$in['id']."'");
    	}


		$f_ext = strtolower($f_ext);
        $f_title="receipt_".$in['id'].$f_ext;
        if(!file_exists('../../'.$script_path.UPLOAD_PATH.DATABASE_NAME."/receipt/")){
        	mkdir('../../'.$script_path.UPLOAD_PATH.DATABASE_NAME."/receipt/", 0775, true);
        }
        $f_out='../../'.$script_path.UPLOAD_PATH.DATABASE_NAME."/receipt/".$f_title;

        if(!$_FILES['file_upload']['tmp_name'])
        {
        	$in['error'].="Please choose a file!"."<br>";
            return false;
        }

        $filename = $_FILES['file_upload']['tmp_name'];
		if (FALSE === move_uploaded_file($_FILES['file_upload']['tmp_name'],$f_out)) {
			$in['error'].="Unable to upload the file.";
            return false;
		}

		@chmod($f_out, 0664);
    	$this->db->query("UPDATE project_expenses SET picture='".$f_title."' WHERE id='".$in['id']."' ");
        return true;
	}

	function update_expense(&$in){
		if(!$this->validate_add_expense($in)){
			return false;
		}

		$this->db->insert("UPDATE project_expenses SET amount='".$in['amount']."',
													 		 note='".$in['note']."',
													 		 date='".$in['date']."',
													 		 billable = '".$in['billabel']."'
													 	 WHERE id='".$in['id']."' ");
		$in['ndate'] = date(ACCOUNT_DATE_FORMAT,$in['date']);
		$in['namount'] = $in['amount'];
		$this->db->query("SELECT * FROM expense WHERE expense_id='".$in['expense_id']."' ");
		$this->db->move_next();
		if($this->db->f('unit_price')){
			$in['amount'] = place_currency(display_number($in['amount'] * $this->db->f('unit_price')))." (".$in['amount']." ". $this->db->f('unit').")";
		}else{
			$in['amount'] = place_currency(display_number($in['amount']));
		}

		if(!empty($_FILES['file_upload']['name'])){
			$this->upload_file($in);
		}
		msg::success ( gm('Expense Updated'),'success');
		return true;
	}

	function delete_expense(&$in){

		$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."' ");
		if($this->db->move_next()){
			@unlink( UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
		}
		$this->db->query("DELETE FROM project_expenses WHERE id='".$in['id']."' ");
		msg::success ( gm('Expense deleted'),'success');
		return true;
	}

	function delete_image(&$in){

		$this->db->query("SELECT picture FROM project_expenses WHERE id='".$in['id']."' ");
		if($this->db->move_next()){
			@unlink( UPLOAD_PATH.DATABASE_NAME."/receipt/".$this->db->f('picture') );
			$this->db->query("UPDATE project_expenses set picture='' WHERE id='".$in['id']."' ");
		}
		msg::success ( gm('Expense attachement deleted'),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function MarkAsSeen(&$in)
	{
		if(!$this->MarkAsSeenValidate($in)){
			return false;
		}
		$this->db->query("UPDATE customer_meetings SET notify='0' WHERE customer_meeting_id='".$in['id']."' ");
		msg::success (gm('Changes saved'),'success');
		$in['message_service'] = false;
		$nrs = $this->db->field("SELECT count(customer_meeting_id) FROM customer_meetings WHERE notify='1' AND user_id='".$_SESSION['u_id']."' ");
		if($nrs > 0 ){
			$in['message_service'] = true;
			$in['service_number'] = $nrs;
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function MarkAsSeenValidate(&$in)
	{
		$v = new validation($in);
		$v->field('id', gm('ID'), 'required:exist[customer_meetings.customer_meeting_id]', gm("Invalid ID"));
		return $v->run();
	}



}

?>