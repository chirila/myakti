<?php

class NotificationHandler {

	function __construct() {
		$this->db = new sqldb();
	}

	function execute($params)
	{
		global $config;
		$data=array(
			'header_text'	=> '',
			'link'	=> '',
			'open_modal'	=> false,
			'modal_params'	=> array()
		);
		switch($params['module_name']){
			case 'invoice':
			case 'purchase_invoice':
				$exploded_action=explode(".", $params['action']);
				$export_app="";
				$thing="";
				switch($exploded_action[0]){
					case 'export-fail':						
						switch ($exploded_action[1]) {
							case 'yuki':
								$title = gm("Export to Yuki");
								$thing = 'yuki';
								break;
							case 'codabox':
								$title = gm("Export to Codabox");
								$thing = 'codabox';
								break;
							case 'clearfacts':
								$title = gm("Export to ClearFacts");
								$thing = 'clearfacts';
								break;	
							case 'billtobox':
								$title = gm("Export to BillToBox");
								$thing = 'billtobox';
								break;
							case 'exact_online':
								$title = gm("Export to Exact online");
								$thing = 'exact_online';
								break;
							case 'winbooks':
							case 'btb':
								$title = gm('Export to E-FFF');
								$thing = 'winbooks';
								break;
							case 'jefacture':
								$title = gm("Export to Jefacture");
								$thing = 'jefacture';
								break;
						}
						$data['header_text']=$title.' '.gm('failed');
						$data['open_modal']=true;
						$data['modal_params']['app']=$thing;
						break;
				}
				break;
			case 'quote':
				$exploded_action=explode(".", $params['action']);
				switch($exploded_action[0]){
					case 'version-won':	
						$serial_number=$this->db->field("SELECT serial_number FROM tblquote WHERE id='".$params['module_id']."' ");	
						$title = gm("Quote").' '.$serial_number;				
						$data['header_text']=$title.' '.gm('won');
						$data['link']=$config['site_url'].'/quote/view/'.$params['module_id'].'?version_id='.$exploded_action[1];
						break;
				}
				break;
		}
		return $data;
	}

}
?>