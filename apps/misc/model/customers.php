<?php

class customers {

	function __construct() {
		$this->db = new sqldb();
		$this->db2 = new sqldb();
		$this->db_import = new sqldb;
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($database_users);
		$this->field_n='customer_id';
		$this->pag = 'customer';
		$this->pagc = 'xcustomer_contact';
		$this->field_nc = 'contact_id';
	}

	function delete_activity(&$in)
	{
		// if(!$this->CanEdit($in)){
		// 	return false;
		// }
		$activity_type=$this->db->field("SELECT contact_activity_type FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['id']."'");
		$is_zen=$this->db->field("SELECT zen_ticket_id FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['id']."'");
		$this->db->query("DELETE FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['id']."' ");
		$this->db->query("DELETE FROM customer_contact_activity_contacts WHERE activity_id='".$in['id']."' ");
		$log_id = $this->db->field("SELECT log_id FROM logging_tracked WHERE activity_id='".$in['id']."' LIMIT 1 ");
		$this->db->query("DELETE FROM logging_tracked WHERE activity_id='".$in['id']."' ");
		if($log_id){
			$this->db->query("DELETE FROM logging WHERE log_id='".$log_id."' ");
		}
		
		if($activity_type == 4){
			$this->db->query("DELETE FROM customer_meetings WHERE activity_id='".$in['id']."' ");
		}
		if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && $is_zen){
			$zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
			$zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
			$zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
			$ch = curl_init();
			$headers=array('Content-Type: application/json');
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
	    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
	    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    	curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/tickets/'.$is_zen.'.json');
	    	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}
		msg::success ( gm('Activity deleted'),'success');
		json_out($in);
		return true;
	}

	function update_status(&$in){
		$reminder_date=strtotimeActivity($in['reminder_event']);

        if($in['reminder_event']){
         	$this->db->query("UPDATE logging SET reminder_date='".$reminder_date."' WHERE log_id='".$in['log_code']."'");

         	$activity_id=$this->db->field("SELECT logging_tracked.activity_id FROM logging INNER JOIN logging_tracked ON logging.log_id=logging_tracked.log_id 
         		WHERE logging.log_id='".$in['log_code']."'
                GROUP BY logging_tracked.activity_id
         		");

         	$this->db->query("UPDATE customer_contact_activity SET reminder_date='".$reminder_date."' WHERE customer_contact_activity_id='".$activity_id."'");
        }
	    if(!$in['not_status_change']){
			if($in['status_change'] == '0'){
				$this->db->query("UPDATE logging SET status_other='1',finished='0',finish_date='0' WHERE log_id='".$in['log_code']."'");
			}else {
				$this->db->query("UPDATE logging SET finished='1',finish_date='".time()."' WHERE log_id='".$in['log_code']."'");
			}
		}	
        
		msg::success ( gm("Status changed succesfully!"),'success');
		if(!$in['return_data']){
			return true;
		}else{
			json_out($in);
		}
	}

	function add_activity_validate(&$in){
        $v=new validation($in);
        $v->field('task_type', 'task_type', 'required', gm('Task type required'));
        $v->field('comment', 'comment', 'required', gm('Comment required'));
        $v->field('log_comment', 'log_comment', 'required', gm('Description required'));
        if($in['task_type'] == '6'){
   			$v->field('reminder_id', 'reminder_id', 'required');
   		}
   		$is_ok=$v->run();
   		if($is_ok){
   			if($in['task_type'] == '4'){
   				if(strtotimeActivity($in['date']) >= strtotimeActivity($in['end_d_meet'])) {
   					msg::error(gm("End time should be bigger then Start time"),'error');
   					$is_ok=false;
   				}
   			}
   		}
        return $is_ok;
    }

	function add_activity(&$in){
		if(!$this->add_activity_validate($in)){
	          json_out($in);
	          return false;
	      }
		if($in['hd_events']){
			$original_date=strtotimeActivity($in['date']);
			switch($in['reminder_id']){
                case '2':
                    $in['reminder_event']=$in['date'];
                    break;
                case '3':
                	$in['reminder_event']= date('c',$original_date-(5*60));
                    break;
                case '4':
                	$in['reminder_event']= date('c',$original_date-(10*60));
                    break;
                case '5':
                	$in['reminder_event']= date('c',$original_date-(15*60));
                    break;
                case '6':
                	$in['reminder_event']= date('c',$original_date-(30*60));
                    break;
                case '7':
                	$in['reminder_event']= date('c',$original_date-(60*60));
                    break;
                case '8':
                	$in['reminder_event']= date('c',$original_date-(24*60*60));
                    break;
            }			
		}
		switch($in['task_type']){
			case '1':
				$this->email_add($in);
				break;
			case '2':
				$this->event_add($in);
				break;
			case '4':
				$this->meeting_add($in);
				break;
			case '5':
				$this->call_add($in);
				break;
			case '6':
				$this->task_add($in);
				break;
			case '7':
				$this->zen_add($in);
				break;
		}
		foreach ($in['contact_ids'] as $contact_id) {
			$this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['some_id']."',
																		 contact_id = '".$contact_id."',
																		 action_type = '0'");
		}
		$users = $this->db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();
		foreach ($users as $key => $value) {
				$this->db->query("INSERT INTO logging_tracked SET
									activity_id='".$in['some_id']."',
									log_id='".$in['log_id']."',
									customer_id='".$in['customer_id']."',
									user_id='".$value['user_id']."'");
		}
		$this->db->query("UPDATE logging_tracked SET seen='0' WHERE user_id='".$_SESSION['u_id']."'AND activity_id='".$in['some_id']."' AND log_id='".$in['log_id']."'");
		msg::success ( gm("Activity added").".",'success');
		if($in['filter_type']!='customer'){
			unset($in['customer_id']);
		}
		if($in['from_customer'] && $in['opportunity_id']){
			unset($in['opportunity_id']);
		}
		return true;
	}

	function task_add(&$in){
		if($in['user_to_id'] ){
			$user_id=$in['user_to_id'];
		} else {
			$user_id = 0;
		}
		$date = strtotimeActivity($in['date']);
		$date_reminder = strtotimeActivity($in['reminder_event']);
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='0',
														reminder_date='".$date_reminder."' ");

		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$date_reminder."',
																	email_to='".$in['user_id']."',
																	opportunity_id='".$in['opportunity_id']."',
																	reminder_id='".$in['reminder_id']."' ");
		
	}

	function event_add(&$in){
		//$date = strtotimeActivity($in['date']);
		$date = time();
		if($in['hd_events']){
			$date_reminder = strtotimeActivity($in['reminder_event']);
			if($in['user_to_id']){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		}else{
			$date_reminder=0;
			$user_id = $in['user_id'];
		}
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														not_task='1',
														reminder_date='".$date_reminder."' ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
														contact_activity_note='".$in['comment']."',
														log_comment='".$in['log_comment']."',
														log_comment_date='".time()."',
														date='".time()."',
														customer_id='".$in['customer_id']."',
														reminder_date='".$date_reminder."',
														email_to='".$in['user_id']."',
														opportunity_id='".$in['opportunity_id']."',
														reminder_id='".$in['reminder_id']."' ");
	}

	function call_add(&$in){
		$date = strtotimeActivity($in['date']);
		if($in['hd_events']){
			$reminder_date = strtotimeActivity($in['reminder_event']);
			if($in['user_to_id'] ){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		} else {
			$reminder_date=0;
			$user_id = $in['user_id'];
		}
		
						 

		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='1',
														not_task='1',
														reminder_date='".$reminder_date."',
														type_of_call='".$in['type_of_call']."'
														 ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	type_of_call='".$in['type_of_call']."',
																	email_to='".$in['user_id']."',
																	opportunity_id='".$in['opportunity_id']."',
																	reminder_id='".$in['reminder_id']."' ");
	}

	function email_add(&$in){
		$date = strtotimeActivity($in['date']);
		if($in['hd_events']){
			$reminder_date = strtotimeActivity($in['reminder_event']);
			if($in['user_to_id']){ //&& $in['assigned_email']
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		} else {
			$reminder_date=0;
			$user_id = $in['user_id'];
		}
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='1',
														not_task='1',
														type_of_call='".$in['type_of_call']."',
														reminder_date='".$reminder_date."' ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	type_of_call='".$in['type_of_call']."',
																	email_to='".$in['user_id']."',
																	opportunity_id='".$in['opportunity_id']."',
																	reminder_id='".$in['reminder_id']."' ");
	}

	function meeting_add(&$in){

		if($in['hd_events']){
			$reminder_date=strtotimeActivity($in['reminder_event']);
			if($in['user_to_id']){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		} else {
			$reminder_date=0;
			$user_id = $in['user_id'];
		}
		$due_date = strtotimeActivity($in['date']);
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$due_date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														not_task='1',
														reminder_date='".$reminder_date."' ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	email_to='".$in['user_id']."',
																	opportunity_id='".$in['opportunity_id']."',
																	reminder_id='".$in['reminder_id']."' ");
		//$my_name = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$my_name = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		$my_name->next();
		if($in['all_day_meet'])
		{
			$duration = 0;
			$all_day = 1;
		} else {
			switch ($in['duration_meet']) {
				case '0-15':
					$duration = '900';
					break;
				case '0-30':
					$duration = '1800';
					break;
				case '1-0':
					$duration = '3600';
					break;
				case '2-0':
					$duration = '7200';
					break;
				case '3-0':
					$duration = '10800';
					break;
				default:
					$duration = '';
					break;
			}
			
			$all_day = 0;
		}
		if(!$in['user_id'])
		{
			$in['user_id'] = $_SESSION['u_id'];
		}
		$start_date= $due_date;
		$end_date = strtotime($in['end_d_meet']);
		
		$in['meeting_id'] = $this->db->insert("INSERT INTO customer_meetings SET
															   subject='".addslashes(utf8_encode($in['comment']))."',
															   location='".$in['location_meet']."',
															   start_date='".$start_date."',
															   end_date='".$end_date."',
															   duration='".$duration."',
															   all_day='".$all_day."',
															   message='".strip_tags($in['log_comment'])."',
															   from_c='".addslashes($my_name->f('first_name'))." ".addslashes($my_name->f('last_name'))."',
															   type='1',
															   customer_id='".$in['customer_id']."',
															   customer_name='".addslashes($in['customer_name'])."',
															   user_id='".$in['user_id']."',
															   activity_id='".$in['some_id']."'
															");
		if(!empty($in['contact_ids'])){
			foreach ($in['contact_ids'] as $contact_id) {
				$this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['meeting_id']."',
																				 contact_id = '".$contact_id."',
																				 action_type = '1'");
			}
		}
		$coordinates=$this->db->field("SELECT coord_id FROM addresses_coord WHERE customer_id='".$in['customer_id']."'");
		if(!$coordinates){
			$this->db->query("INSERT INTO addresses_coord SET customer_id='".$in['customer_id']."',
										location_lat='".$in['latitude_customer']."',
										location_lng='".$in['longitude_customer']."' ");
		}
		$calendar = $this->db->field("SELECT app_id FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' AND active='1' ");
		$google_sync=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$calendar."' AND type='outgoing' ");
		if($google_sync==1){
			$this->sendToGoogle($in);
		}
	}

	function zen_add(&$in){
		$date = strtotimeActivity($in['date']);
		if($in['hd_events']){
			$reminder_date = strtotimeActivity($in['reminder_event']);
			if($in['user_to_id'] ){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		} else {
			$reminder_date=0;
			$user_id = $in['user_id'];
		}

		$zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
		$zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
		$zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
		$ch = curl_init();
		$headers=array('Content-Type: application/json');
		$zen_body=array();
		$zen_body['ticket']=array(
			'subject'		=> $in['comment'],
			'comment'		=> strip_tags($in['log_comment'])
		);
		if($in['contact_ids'] && !empty($in['contact_ids'])){
			$contact_zen=$this->db->field("SELECT zen_contact_id FROM customer_contacts WHERE contact_id='".$in['contact_ids'][0]."' ");
			if($contact_zen!=''){
				$zen_body['ticket']['requester_id']=$contact_zen;
				$zen_body['ticket']['submitter_id']=$contact_zen;
			}
		}
		$zen_body=json_encode($zen_body);
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
		curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $zen_body);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/tickets.json');

	    $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
	    	msg::error(gm('An error occured'),'error');
	    	json_out($in);
	    }else{
	    	$data_d=json_decode($put);
	    	$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='0',
														not_task='1',
														zen_requester_id='".$data_d->ticket->requester_id."',
														zen_submitter_id='".$data_d->ticket->submitter_id."',
														zen_assignee_id='".$data_d->ticket->assignee_id."',
														zen_group_id='".$data_d->ticket->group_id."',
														zen_organization_id='".$data_d->ticket->organization_id."' ");
			$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	email_to='".$in['user_id']."',
																	zen_ticket_id='".$data_d->ticket->id."',
																	is_our_zen='1',
																	opportunity_id='".$in['opportunity_id']."' ");
	    }	
	}

	function sendToGoogle(&$in)
	{
		### google calendar ###
		ark::loadLibraries(array('gCalendar'));
		$calendar = new gCalendar($in);
		$calendar->sendToGoogle($in);
		### google calendar ###
	}

	function modify_activity_date(&$in){

		$date_activity=strtotimeActivity($in['date']);
		
		$this->db->query("UPDATE logging SET type_of_call='".$in['type_of_call_id']."' , due_date='".$date_activity."'  WHERE  log_id = '".$in['log_code']."' ");

		if($in['type_of_call_id']){
            $in['type_of_call_view']=gm('incoming call');
		}else{
            $in['type_of_call_view']=gm('outgoing call');
		} 
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function log_comment(&$in)
	{
		$date = time();
		$in['log_comment_date'] = date(ACCOUNT_DATE_FORMAT,$date).', '.date('H:i',$date);
		$this->db->query("UPDATE customer_contact_activity SET log_comment='".$in['log_comment']."', log_comment_date='".$date."' WHERE customer_contact_activity_id='".$in['log_id']."' ");
		$log_id=$this->db->field("SELECT log_id FROM logging_tracked WHERE activity_id='".$in['log_id']."' GROUP BY activity_id ");
		$this->db->query("UPDATE logging SET message='".$in['log_comment']."' WHERE log_id='".$log_id."' ");
		$is_zen=$this->db->field("SELECT zen_ticket_id FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['log_id']."'");
		if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && $is_zen){
			$zen_data=$this->db->query("SELECT * FROM apps WHERE name='Zendesk' AND main_app_id='0' AND type='main' ");
			$zen_email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='email' ");
			$zen_pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_data->f('app_id')."' AND type='password' ");
			$ch = curl_init();
			$headers=array('Content-Type: application/json');
			$zen_body=array();
			$zen_body['ticket']=array(
				'comment'		=> strip_tags($in['comments'])
			);
			$zen_body=json_encode($zen_body);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, $zen_email."/token:".$zen_pass);
	    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $zen_body);
	    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    	curl_setopt($ch, CURLOPT_URL, $zen_data->f('api').'/api/v2/tickets/'.$is_zen.'.json');
	    	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
		}
		$in['comments'] = stripslashes($in['comments']);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function UpdateActivity(&$in)
	{
		if($in['reminder_id']){
			$original_date=strtotimeActivity($in['date']);
			switch($in['reminder_id']){
                case '2':
                    $in['reminder_event']=$in['date'];
                    break;
                case '3':
                	$in['reminder_event']= date('c',$original_date-(5*60));
                    break;
                case '4':
                	$in['reminder_event']= date('c',$original_date-(10*60));
                    break;
                case '5':
                	$in['reminder_event']= date('c',$original_date-(15*60));
                    break;
                case '6':
                	$in['reminder_event']= date('c',$original_date-(30*60));
                    break;
                case '7':
                	$in['reminder_event']= date('c',$original_date-(60*60));
                    break;
                case '8':
                	$in['reminder_event']= date('c',$original_date-(24*60*60));
                    break;
            }			
		}
		if($in['task_type'] != '2'){
			$this->modify_activity_date($in);
		}	
		$this->db->query("UPDATE customer_contact_activity SET contact_activity_note='".addslashes($in['comment'])."',opportunity_id='".$in['deal_id']."', reminder_id='".$in['reminder_id']."',customer_id='".$in['customer_id']."' WHERE customer_contact_activity_id='".$in['log_id']."' ");
		$this->db->query("UPDATE logging SET field_value='".$in['customer_id']."',log_comment='".addslashes($in['comment'])."',to_user_id='".$in['user_to_id']."' WHERE log_id='".$in['log_code']."' ");
		$this->db->query("UPDATE logging_tracked SET customer_id='".$in['customer_id']."' WHERE activity_id='".$in['log_id']."' AND log_id='".$in['log_code']."'");
		$this->db->query("DELETE FROM customer_contact_activity_contacts WHERE activity_id='".$in['log_id']."' ");
		foreach ($in['contact_ids'] as $contact_id) {
			$this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['log_id']."',
				contact_id = '".$contact_id."',
				action_type = '0'");
		}
		$this->log_comment($in);
		$this->update_status($in);
		if($in['task_type'] == '4'){
			$this->db->query("UPDATE customer_meetings SET 
				subject='".addslashes(utf8_encode($in['comment']))."',
				location='".$in['location_meet']."',
				start_date='".strtotimeActivity($in['date'])."',
				end_date='".strtotime($in['end_d_meet'])."',
				message='".strip_tags($in['log_comment'])."',
				customer_id='".$in['customer_id']."'
				WHERE activity_id='".$in['log_id']."' ");
		}
		if($in['filter_type']!='customer'){
			unset($in['customer_id']);
		}
		// json_out($in);
	}

	function alloCall_validate(&$in){
		$v=new validation($in);
		$v->field('device_id','device_id','required');
		return $v->run();
	}

	function alloCall(&$in){
		if(!$this->alloCall_validate($in)){
			json_out($in);
		}
		$ch = curl_init();
		$a_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Allocloud' AND type='main' AND main_app_id='0' ");
		$in['username']=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='username' ");
		$in['api_key']=$a_data->f('api');
		$in['url']=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='url' ");
		$expire=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='expire' ");
		$token_reset=true;
		if(!$expire || $expire < time()){
			include(__DIR__.'/misc.php');
			$misc_obj=new misc();
			$token_reset=$misc_obj->renewAlloToken($in);		
		}
		if(!$token_reset){
			json_out($in);
		}
		$token=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='token' ");

		$phone_number=trim($in['number']," ");
		$phone_number=str_replace(" ","",$phone_number);
		$phone_number=str_replace(".","",$phone_number);
		$phone_number=str_replace(",","",$phone_number);
		$phone_number=str_replace("-","",$phone_number);
		$phone_number=str_replace("_","",$phone_number);
		$phone_number=str_replace("/","",$phone_number);
		$phone_number=str_replace("(","",$phone_number);
		$phone_number=str_replace(")","",$phone_number);

    	$headers=array('Content-Type: application/json','X-Auth-Token:'.$token);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $in['url'].'/devices/'.$in['device_id'].'/quickcall/'.$phone_number);
        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
	    	$err=json_decode($put);
            msg::error($err->error->details,"error");
	    }else{
	    	$resp=json_decode($put);
	    	$in['date']=date('c');
			$in['task_type']='5';			
			$in['user_id']=$_SESSION['u_id'];
			$in['comment']='Allocloud Call';
			$in['log_comment']='Allocloud Call';
			$in['type_of_call']='9';//allocloud call
			$this->add_activity($in);
			msg::success($resp->data,'success');
	    }	
		json_out($in);
	}

	function get_addresses(&$in){

		$andWhere = '';
		$andWhereSec = '';

		switch($in['app']){
			case 'quote':
				if( $in['buyer_id']){
					$in['customer_id'] = $in['buyer_id']; 
					$andWhere = " AND customer_addresses.is_primary ='".'1'."' ";

					if($in['secondaryAddressesCheck']){
						$andWhereSec = " AND (customer_addresses.delivery = '".'1'."' 
										OR customer_addresses.site = '".'1'."') ";
					}

					if($in['delivery_address_id'] && $in['secondaryAddressesCheck']){
						$limit = 4;
					} else {
						$limit = 5;
					}

					$in['limit'] = $limit;
					$in['andWhere'] = $andWhere;
					$in['andWhereSec'] = $andWhereSec;				
				}		
			break;
			case 'order':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;
				}
			break;
			case 'po_order':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;
				}
			break;
			case 'project':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;
				}
			break;
			case 'maintenance':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;

					if($in['site_add']){
						$andWhereSec =" AND site='1' ";
					}
					$in['andWhereSec'] = $andWhereSec;
				}
			break;
			case 'installation':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;
					$andWhere = " AND site='1' ";
					$in['andWhere'] = $andWhere;
				}
			break;
			case 'contract':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;
				}
			break;
			case 'invoice':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;
					$andWhere = " AND (customer_addresses.is_primary = '1' OR customer_addresses.billing = '1') ";
					$in['andWhere'] = $andWhere;
				}
			break;
			case 'purchase_ninvoice':
				if($in['buyer_id']){
					$in['customer_id'] = $in['buyer_id'];
					$in['limit'] = 5;
				}
			break;
		}

		$address = $this->get_address_list($in);

		unset($in['limit']);
		unset($in['andWhere']);
		unset($in['andWhereSec']);

		$addresses=array('addresses' => array(), 'secondaryAddresses' => array());
		if($address['addresses']){
			while($address['addresses']->next()){
			  	$a = array(
			  		'symbol'				=> '',
				  	'address_id'	    => $address['addresses']->f('address_id'),
				  	'id'			    => $address['addresses']->f('address_id'),
				  	'address'			=> ($address['addresses']->f('address')),
				  	'top'				=> ($address['addresses']->f('address')),
				  	'zip'			    => $address['addresses']->f('zip'),
				  	'city'			    => $address['addresses']->f('city'),
				  	'state'			    => $address['addresses']->f('state'),
				  	'country'			=> $address['addresses']->f('country'),
				  	'right'				=> $address['addresses']->f('country'),
				  	'bottom'			=> $address['addresses']->f('zip').' '.$address['addresses']->f('city'),
			  	);

				array_push($addresses['addresses'], $a);
			}
		}

		if($address['secondaryAddresses']){
			while($address['secondaryAddresses']->next()){
			  	$a = array(
			  		'symbol'				=> '',
				  	'address_id'	    => $address['secondaryAddresses']->f('address_id'),
				  	'id'			    => $address['secondaryAddresses']->f('address_id'),
				  	'address'			=> ($address['secondaryAddresses']->f('address')),
				  	'top'				=> ($address['secondaryAddresses']->f('address')),
				  	'zip'			    => $address['secondaryAddresses']->f('zip'),
				  	'city'			    => $address['secondaryAddresses']->f('city'),
				  	'state'			    => $address['secondaryAddresses']->f('state'),
				  	'country'			=> $address['secondaryAddresses']->f('country'),
				  	'right'				=> $address['secondaryAddresses']->f('country'),
				  	'bottom'			=> $address['secondaryAddresses']->f('zip').' '.$address['secondaryAddresses']->f('city'),
			  	);
				array_push($addresses['secondaryAddresses'], $a);
			}
		}
		
		array_push($addresses['addresses'],array('address_id'=>'99999999999','id'=>'99999999999'));
		array_push($addresses['secondaryAddresses'],array('address_id'=>'99999999999','id'=>'99999999999'));
		
		return json_out($addresses);
	}

	function get_address_list($in){

		$limit = $in['limit'];
		$andWhere = $in['andWhere'];
		$andWhereSec = $in['andWhereSec'];

		$address = array();
		$secondaryAddresses = array();

		$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['customer_id']."'
									 $andWhere
									 ORDER BY customer_addresses.address_id limit $limit");

		if(!empty($andWhereSec)){
			$secondaryAddresses= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."'
									 $andWhereSec
									 ORDER BY customer_addresses.address_id limit $limit");	
		} 

		return array('addresses' => $address, 'secondaryAddresses' => $secondaryAddresses);
	}


}
?>