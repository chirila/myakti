<?php

class misc {

	function __construct() {
		$this->db = new sqldb();
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($database_users);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function switchUser(&$in)
	{
		global $config;
		$href = $config['site_url']."customers/";

		if($in['is_accountant']==1 && $in['user_id']){
			$in['switch_user_id'] = $in['user_id'];
		}else{
			if(!$this->switchUserValidate($in)){
				json_out($in);
				return false;
			}
		}

		/*$db = $this->db_users->query("SELECT password, main_user_id, user_role, database_name, lang.code, group_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE user_id = '".$in['switch_user_id']."'");*/
		$db = $this->db_users->query("SELECT password, main_user_id, user_role, database_name, lang.code, group_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE user_id = :user_id",['user_id'=>$in['switch_user_id']]);    
		
		if(!$db->next()){
			json_out($in);
			return false;
		}

		session_unset();
   		session_destroy();
		session_start();
		
		
		$_SESSION['u_id'] = $in['switch_user_id'];
		$_SESSION['main_u_id'] = $db->f('main_user_id');
	  $_SESSION['access_level'] = $db->f('user_role');
	  $_SESSION['l'] = $db->f('code');
	  // DATABASE_NAME = $db->f('database_name');
		$user_level = $_SESSION['access_level'];
		$_SESSION['group'] = 'user';
		if($db->f('group_id') == '1'){
			$_SESSION['group'] = 'admin';
		}else{
			$_SESSION['admin_sett'] = array();
			$extra = $this->db_users->query("SELECT * FROM user_meta WHERE name REGEXP 'admin_[0-9]+' AND user_id='".$_SESSION['u_id']."' ");
			while ($extra->next()) {
				if($extra->f('value')){
					array_push($_SESSION['admin_sett'], $extra->f('value'));
				}
			}
		}
		$_SESSION['regional'] = '';
    //$q = $this->db_users->query("SELECT * FROM users_settings WHERE user_id = '".$_SESSION['u_id']."'");
	$q = $this->db_users->query("SELECT * FROM users_settings WHERE user_id = :user_id",['user_id'=>$_SESSION['u_id']]);
    if($q->next()){
    	$_SESSION['regional'] = $q->next_array();
    }
    msg::success('Success','success');
    json_out($in);

	}

	/**
	 * undocumented function
	 *
	 * switched user id with link id for security
	 *
	 * @return void
	 * @author PM
	 **/
	function switchUserValidate(&$in)
	{
		if(!$in['user_id']){
			return false;
		}
		//$link = $this->db_users->query("SELECT * FROM db_link_user WHERE id='".$in['user_id']."' ");
		$link = $this->db_users->query("SELECT * FROM db_link_user WHERE id= :id ",['id'=>$in['user_id']]);
		if(!$link->next()){
			return false;
		}else{
			$in['switch_user_id'] = '';
			$is_link = false;
			if($link->f('user_id1') == $_SESSION['u_id']){
				$in['switch_user_id'] = $link->f('user_id2');
				$is_link = true;
			}
			if($link->f('user_id2') == $_SESSION['u_id']){
				$in['switch_user_id'] = $link->f('user_id1');
				$is_link = true;
			}
			return $is_link;
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer(&$in)
	{

		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->updateCustomer_validate($in)){
			return false;
		}
        $query_sync=array();

		// $c_type = explode(',', $in['c_type']);
		
		// foreach ($in['c_type'] as $key) {
		// 	if($key){
		// 		$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
		// 		$c_type_name .= $type.',';
		// 	}
		// }
		// $c_types = implode(',', $in['c_type']);
		// $c_type_name = rtrim($c_type_name,',');

		$c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}

		if($in['user_id']){
			foreach ($in['user_id'] as $key => $value) {
				$manager_id = get_user_name($value); 
				// $this->db_users->field("SELECT first_name,last_name FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET name 				= '".$in['name']."',
											serial_number_customer  =   '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											active					= '".$in['active']."',
											legal_type				= '".$in['legal_type']."',
											user_id					= '".$acc_manager_ids."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['c_email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['comp_phone']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											c_type_name				= '".$c_type_name."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											acc_manager_name		= '".addslashes($in['acc_manager'])."',
											sales_rep				= '".$in['sales_rep']."',
											is_supplier				= '".$in['is_supplier']."',
											our_reference			= '".$in['our_reference']."',
											firstname 				= '".$in['firstname']."'
					  	WHERE customer_id='".$in['customer_id']."' ");
			if($in['address_id']){
				$this->db->query("UPDATE customer_addresses SET
													country_id			=	'".$in['country_id']."',
													state_id			=	'".$in['state_id']."',
													city				=	'".$in['city']."',
													zip					=	'".$in['zip']."',
													address				=	'".$in['address']."'
													WHERE address_id 	=	'".$in['address_id']."'												
													");
			}else{
				$this->db->query("INSERT INTO customer_addresses SET
													customer_id 		= '".$in['customer_id']."',
													country_id			=	'".$in['country_id']."',
													state_id			=	'".$in['state_id']."',
													city				=	'".$in['city']."',
													zip					=	'".$in['zip']."',
													address				=	'".$in['address']."',
													is_primary 	=	'1'												
													");
			}

		

		if($in['customer_id_linked']){
			$link_id = $this->db->insert("INSERT INTO customer_link SET customer_id='".$in['customer_id']."',
														customer_id_linked='".$in['customer_id_linked']."',
														link_type='".$in['link_type']."'");
			if($in['link_type'] == 1){
				$type = 2;
			}
			else{
				$type = 1;
			}
			$this->db->query("INSERT INTO customer_link SET customer_id='".$in['customer_id_linked']."',
														customer_id_linked='".$in['customer_id']."',
														link_type='".$type."',
														link_id='".$link_id."'");
		}
		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."',	
														buyer_address 	='".$in['address']."',  	
													   buyer_email 		='".$in['c_email']."',
													   buyer_phone		='".$in['comp_phone']."',
													   buyer_fax		='".$in['comp_fax']."',
													   buyer_zip		='".$in['zip']."',
													   buyer_city		='".$in['city']."',
													   buyer_country_id	='".$in['country_id']."',
													   buyer_state_id	='".$in['state_id']."'
												WHERE buyer_id='".$in['customer_id']."' ");

		
		Sync::end($in['customer_id']);

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		// $in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if($in['customer_link']){
			$v->field('link_type', 'Type of link', 'required');
		}
		return $this -> addCustomer_validate($in);

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addCustomer_validate(&$in)
	{
		$in['c_email'] = trim($in['c_email']);
		$v = new validation($in);
		$v->field('name', 'Name', 'required');

		if (ark::$method == 'updateCustomer') {
			$our_ref_val=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_COMPANY_NUMBER' ");
		  	if($in['our_reference'] && $our_ref_val){
		 		$v->field('our_reference','Company Nr.',"unique[customers.our_reference.(customer_id!='".$in['customer_id']."')]");
		  	}
	  	}


		if($in['c_email']){
			$v->field('c_email','Email',"email:unique[customers.c_email.(customer_id!='".$in['customer_id']."')]");			
		}
		
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function CanEdit(&$in){
		if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
			return true;
		}
		$c_id = $in['customer_id'];
		if(!$in['customer_id'] && $in['contact_id']) {
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		}
		if($c_id){
			if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
				$u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
				if($u){
					$u = explode(',', $u);
					if(in_array($_SESSION['u_id'], $u)){
						return true;
					}
					else{
						msg::$warning = gm("You don't have enought privileges");
						return false;
					}
				}else{
					msg::$warning = gm("You don't have enought privileges");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_field(&$in)
	{
		$this->db->query("DELETE FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['customer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendCustomerToZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$vars_post = array();
		$data = $this->db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ");
		if($data){
			$bar_code_start = null;
			$bar_code_end = null;
			$custom_fields = $this->db->query("SELECT * FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
			while($custom_fields->next()){
				if($custom_fields->f('field_id') == 1){
					$bar_code_start = $custom_fields->f('value');
				}
				if($custom_fields->f('field_id') == 2){
					$bar_code_end = $custom_fields->f('value');
				}
			}
			//asdkauas
			$primary = array();
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
			if($address) {
				$primary = array(
					"Unformatted"=> null,
			    "Label"=> null,
			    "Building"=> null,
			    "Street"=> $address->f('address'),
			    "PostalCode"=> $address->f('zip'),
			    "City"=> $address->f('city'),
			    "State"=> $address->f('state_id'),
			    "Country"=> get_country_name($address->f('country_id')),
			    "CountryCode"=> get_country_code($address->f('country_id'))
			  );
			}
			$test = array();
			$vars_post = array(
				'Id'											=> (string)$in['customer_id'],
				"Name"										=> $data->f('name'),
			  "VatNumber"								=> $data->f('btw_nr') ? $data->f('btw_nr') : null,
			  "TimeZone"								=> null,
			  "Note"										=> null,
			  "PictureURI"							=> null,
			  "Importance"							=> null,
			  "Blog"										=> null,
			  "WebSite"									=> $data->f('website') ? $data->f('website') : null,
			  "FixedPhoneNumber"				=> $data->f('comp_phone') ? $data->f('comp_phone') : null,
			  "MobilePhoneNumber"				=> $data->f('other_phone') ? $data->f('other_phone') : null,
			  "FaxNumber"								=> $data->f('comp_fax') ? $data->f('comp_fax') : null,
			  "EMailAddress"						=> $data->f('c_email') ? $data->f('c_email') : null,
			  "DeliveryPostalAddress"		=> null,
			  "InvoicePostalAddress"		=> null,
			  "PostalAddress"						=> $primary,
			  "Fields"									=> array(
					array(
					  "Name"								=> "Reference",
					  "Value"								=> $data->f('our_reference')
					),
					array(
					  "Name"								=> "BarcodeFrom",
					  "Value"								=> $bar_code_start
					),
					array(
					  "Name"								=> "BarcodeTo",
					  "Value"								=> $bar_code_end
					)
			  ),
			  "Links"										=> $test,
			  "Categories"							=> null
			);

			$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'put',$vars_post);
			console::log($put);
			$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->update_financial_valid($in)){
			return false;
		}
		$apply_fix_disc = 0;
		if($in['apply_fix_disc']){$apply_fix_disc = 1;}
		$apply_line_disc = 0;
		if($in['apply_line_disc']){$apply_line_disc = 1;}
		$check_vat_number = $this->db->field("SELECT check_vat_number FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($in['btw_nr']!=$check_vat_number){
			$check_vat_number = '';
		}

		$c_email = $this->db->field("SELECT invoice_email FROM customers WHERE customer_id='".$in['customer_id']."' ");
		$invoice_email_type = $this->db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$in['customer_id']."' ");

		if($in['invoice_email_type']==1){
		    $in['invoice_email']='';
		    $in['attention_of_invoice']='';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET
											btw_nr								=	'".$in['btw_nr']."',
											bank_name							=	'".$in['bank_name']."',
											payment_term					=	'".$in['payment_term']."',
											payment_term_type 		= '".$in['payment_term_start']."',
											bank_bic_code					=	'".$in['bank_bic_code']."',
											fixed_discount				=	'".return_value($in['fixed_discount'])."',
											bank_iban							=	'".$in['bank_iban']."',
                      						vat_id								=	'".$in['vat_id']."',
											cat_id								=	'".$in['cat_id']."',
											no_vat								=	'".$in['no_vat']."',
											currency_id						= '".$in['currency_id']."',
											invoice_email_type					=	'".$in['invoice_email_type']."',
											invoice_email					=	'".$in['invoice_email']."',
											attention_of_invoice		   =	'".$in['attention_of_invoice']."',
											invoice_note2					=	'".$in['invoice_note2']."',
											deliv_disp_note				=	'".$in['deliv_disp_note']."',
											external_id						=	'".$in['external_id']."',
											internal_language			=	'".$in['internal_language']."',
											apply_fix_disc				= '".$apply_fix_disc."',
											apply_line_disc				= '".$apply_line_disc."',
											line_discount 				= '".return_value($in['line_discount'])."',
											comp_reg_number 				= '".$in['comp_reg_number']."',
											vat_regime_id				= '".$in['vat_regime_id']."',
											siret						= '".$in['siret']."',
											check_vat_number 			= '".$check_vat_number."',
											identity_id                 = '".$in['identity_id']."'
											WHERE customer_id			= '".$in['customer_id']."' ");
		//quote_reference			=	'".$in['quote_reference']."',

		/*if($invoice_email_type != $in['invoice_email_type']){
			$in['changed_invoice_preference']=true;
			customerInvoicingRecurringInvoiceUpdate(array('customer_id'=>$in['customer_id']));
		}else if($invoice_email_type == $in['invoice_email_type'] && $in['invoice_email_type']=='2' && $in['invoice_email']!=$c_email){
			$in['changed_invoice_preference']=true;
			customerInvoicingRecurringInvoiceUpdate(array('customer_id'=>$in['customer_id']));
		}*/

		Sync::end($in['customer_id']);
		// msg::$success = gm("Changes have been saved.");
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial_valid(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		if($in['invoice_email']){
			$v->field('invoice_email', 'Type of link', 'multi_email');
		}

		return $v->run();
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error1');
				json_out($in);
			}
			return false;
		}
        $exist_company = $this->db->query("SELECT name FROM customers WHERE btw_nr='".$in['value']."' AND customer_id != '". $in['customer_id']  ."' ");

		$in['exist_company'] = array();
		$in['multiple_companies'] = true;
		$i = 0;
        while ($exist_company->next()) {
            $i++;
            array_push($in['exist_company'], $exist_company->f('name'));
        }
		if (empty($in['exist_company'])) {
		    $in['exist_company'] = false;
        }
        if($i == 1) {
            $in['multiple_companies'] = false;
        }

		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		/*if($vat_numeric || substr($value,0,2)=="BE"){
			$trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
			$trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
			$trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

			if(!$trends_access_token || $trends_expiration<time()){
				$ch = curl_init();
				$headers=array(
					"Content-Type: x-www-form-urlencoded",
					"Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
					);
				/*$trends_data=!$trends_access_token ? "grant_type=password&username=akti_api&password=akti_api" : "grant_type=refresh_token&&refresh_token=".$trends_refresh_token;* /
				$trends_data="grant_type=password&username=akti_api&password=akti_api";

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
		       	curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
			    curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
			 	console::log('aaaaa');
			 	console::log($info);* /

			 	if($info['http_code']==400){
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else{
			 		if(!$trends_access_token){
			 			$this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
					 	$this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
					 	$this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
			 		}else{
			 			$this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
			 			$this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
			 			$this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
			 		}

			 		$ch = curl_init();
					$headers=array(
						"Authorization: Bearer ".$put->access_token
						);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    if($vat_numeric){
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
				    }else{
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
				    }
				    $put = json_decode(curl_exec($ch));
				 	$info = curl_getinfo($ch);

				 	/*console::log($put);
				 	console::log('bbbbb');
				 	console::log($info);* /

				    if($info['http_code']==400 || $info['http_code']==429){
				    	if(ark::$method == 'check_vies_vat_number'){
					 		msg::error ( $put->error,'error');
					 		json_out($in);
					 	}
						return false;
				 	}else if($info['http_code']==404){
				 		if($vat_numeric){
				 			if(ark::$method == 'check_vies_vat_number'){
					 			msg::error ( gm("Not a valid vat number"),'error');
					 			json_out($in);
					 		}
							return false;
				 		}
				 	}else{
				 		if($in['customer_id']){
				 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 			if($country_id != 26){
				 				$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 				$in['remove_v']=1;
				 			}else if($country_id == 26){
					 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
					 			$in['add_v']=1;
					 		}
				 		}
				 		$in['comp_name']=$put->officialName;
				 		$in['comp_address']=$put->street.' '.$put->houseNumber;
						$in['comp_zip']=$put->postalCode;
						$in['comp_city']=$put->city;
				 		$in['comp_country']='26';
				 		$in['trends_ok']=true;
				 		$in['trends_lang']=$_SESSION['l'];
				 		$in['full_details']=$put;
				 		if(ark::$method == 'check_vies_vat_number'){
					 		msg::success(gm('Success'),'success');
					 		json_out($in);
					 	}
				 		return false;
				 	}
			 	}
			}else{
				$ch = curl_init();
				$headers=array(
					"Authorization: Bearer ".$trends_access_token
					);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    if($vat_numeric){
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
			    }else{
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
			    }
			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
				console::log('ccccc');
				console::log($info);* /

			    if($info['http_code']==400 || $info['http_code']==429){
			    	if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else if($info['http_code']==404){
			 		if($vat_numeric){
			 			if(ark::$method == 'check_vies_vat_number'){
				 			msg::error (gm("Not a valid vat number"),'error');
				 			json_out($in);
				 		}
						return false;
			 		}
			 	}else{
			 		if($in['customer_id']){
			 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 		if($country_id != 26){
				 			$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['remove_v']=1;
				 		}else if($country_id == 26){
				 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['add_v']=1;
				 		}
				 	}
			 		$in['comp_name']=$put->officialName;
			 		$in['comp_address']=$put->street.' '.$put->houseNumber;
					$in['comp_zip']=$put->postalCode;
					$in['comp_city']=$put->city;
				 	$in['comp_country']='26';
			 		$in['trends_ok']=true;
			 		$in['trends_lang']=$_SESSION['l'];
			 		$in['full_details']=$put;
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::success(gm('Success'),'success');
				 		json_out($in);
				 	}
			 		return false;
			 	}
			}
		}*/


		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');			
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error2');
				json_out($in);
			}
			return false;
		}

		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error3');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::notice ( gm('Error'),'error4');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			if($result->countryCode=='BE' || $result->countryCode=='NL' || $result->countryCode=='FR'){
				$juridical_form = array('Andere/Autre/Other ', 'BEROEPSVERENIGING ', 'BVBA ', 'BV BVBA ', 'CV ', 'CVBA ', 'CVOA ', 'GCV ', 'NV ', 'VOF ', 'VZW ', 'SA ', 'SC SPRL ', 'SCA ', 'SCRI ', 'SCRL ', 'SNC ', 'SPRL ','Comm.V ', 'EBVBA ', 'SPRLU ', 'SC ', 'SCS ', 'CVA ', 'ZZP ', 'BV ', 'ESV ', 'GIE ', 'ASBL ', 'VoG ', 'SEP ', 'Comm.VA ', 'Mts ');
			
				if(strpos($result->name,'BV ')!==false){
					if(strpos($result->name,'BV BVBA ')!==false){
						$in['comp_name']=str_replace('BV BVBA ','', $result->name);
					}else{
						$in['comp_name']=str_replace('BV ','', $result->name);
					}
				}else if(strpos($result->name,'BVBA ')!==false){
					if(strpos($result->name,'EBVBA ')!==false){
						$in['comp_name']=str_replace('EBVBA ','', $result->name);
					}else{
						$in['comp_name']=str_replace('BVBA ','', $result->name);
					}
				}else if(strpos($result->name,'SC ')!==false){
					if(strpos($result->name,'SC SPRL ')!==false){
						$in['comp_name']=str_replace('SC SPRL ','', $result->name);
					}else{
						$in['comp_name']=str_replace('SC ','', $result->name);
					}
				}else{
					$in['comp_name']=str_replace($juridical_form,'', $result->name);
				}

	            $in['juridical_form']=str_replace(' '.$in['comp_name'],'', $result->name);
	           
	            if($in['juridical_form'] && in_array($in['juridical_form'].' ', $juridical_form)){
	            	$this->db->query("SELECT id FROM customer_legal_type WHERE name='".$in['juridical_form']."'");
	            	if($this->db->move_next()){
	                     $in['customer_legal_type_id']=$this->db->f('id');
	            	}else{
	            			$in['customer_legal_type_id']=$this->db->insert("INSERT INTO  customer_legal_type SET  name='".$in['juridical_form']."'");
	            	}

	            }
			}
			
            $in['customer_legal_type']		= build_l_type_dd();

		
			$in['country_dd']=build_country_list(ACCOUNT_DELIVERY_COUNTRY_ID);
			$in['comp_country']		=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']		= $result;



			$in['vies_ok']=1;
			if($in['customer_id'] != 'tmp'){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error5');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 * @author PM
	 */
	function contact_update(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->contact_update_validate($in)){
			return false;
		}
		if(isset($in['birthdate']) && !empty($in['birthdate']) ){
			$in['birthdate'] = strtotime($in['birthdate']);
		}
		/*$position = implode(',', $in['position']);*/
		$pos = '';
		foreach ($in['position_n'] as $key => $value) {
			$pos .= $value['name'].',';
		}
		$pos = addslashes(rtrim($pos,','));
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET
                                            customer_id	=	'".$in['customer_id']."',
                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$in['position']."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title']."',
										    e_title		=	'".$in['e_title']."',
											fax			=	'".$in['fax']."',
											company_name=	'".$in['customer']."',
											title_name	=	'".$in['title_name']."',
											position_n  = 	'".$pos."',
										    last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."'");
		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}

		Sync::end($in['contact_id']);

		$contact_data=$this->db->query("SELECT firstname,lastname,zen_contact_id,email FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && $contact_data->f('zen_contact_id') && defined('ZEN_SYNC_OLD_C') && ZEN_SYNC_OLD_C==1){
		    $prime_data=$this->db->query("SELECT email,customer_id FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND `primary`='1' ");	
		    $prime_mail=$prime_data->f('email');
		    if(!$prime_mail){
		    	$prime_mail=$contact_data->f('email');
		    }
		    $vars=array(); 
		    if($prime_data->f('customer_id')){
		    	$vars['customer_id']=$prime_data->f('customer_id');
		    	$vars['customer_name']=get_customer_name($prime_data->f('customer_id'));
		    }		           
            $vars['contact_id']=$in['contact_id'];
            $vars['firstname']=$contact_data->f('firstname');
            $vars['lastname']=$contact_data->f('lastname');
            $vars['table']='customer_contacts';
            $vars['email']=$prime_mail;
            $vars['op']='update';
            synctoZendesk($vars);
		}

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}
		if(!$in['customer_id']){
			$this->removeFromAddress($in);
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],'contact_id',$in['contact_id']);

		$this->updateContactAccounts($in);

		//save extra fields values
		$this->update_custom_contact_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		// $in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function updateContactAccounts(&$in)
	{
		$this->db->query("UPDATE customer_contactsIds SET position='".$in['position']."',
			                 								department='".$in['department']."',
			                 								title='".$in['title']."',
			                 								e_title='".$in['e_title']."',
			                 								email='".$in['email']."',
			                 								phone='".$in['phone']."',
			                 								fax='".$in['fax']."',
			                 								s_email='".$in['s_email']."'
			                WHERE customer_id='".$in['customer_id']."' AND contact_id='".$in['contact_id']."'");
		return true;
	}

	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}

		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return true
	 * @param array $in
	 * @author PM
	 **/
	function contact_add_validate(&$in)
	{
		$in['email'] = trim($in['email']);
		$v = new validation($in);
		$v->field('firstname', 'First name', 'required');
		$v->field('lastname', 'Last name', 'required');
		if ($in['do']=='company-xcustomer_contact-customer-contact_add')
		{
			$v->field('email', 'Email', 'email:unique[customer_contacts.email]');
		}
		else
		{
			$v->field('email','Email',"email:unique[customer_contacts.email.(contact_id!='".$in['contact_id']."')]");
		}
		return $v->run();

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function removeFromAddress(&$in)
	{
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/

	function Add_Entry(&$in){
		if($in['extra_id']){
			$sort_order = $this->db->field("SELECT sort_order FROM ".$in['table']." WHERE extra_field_id='".$in['extra_id']."' ORDER BY sort_order DESC LIMIT 1");
		
			if($in['id']){
				$this->db->query("UPDATE ".$in['table']." SET name='".htmlspecialchars_decode($in['name'])."' WHERE id=".$in['id']." AND extra_field_id='".$in['extra_id']."' ");
			}else{
				$this->db->query("INSERT INTO ".$in['table']." SET name='".htmlspecialchars_decode($in['name'])."', sort_order='".($sort_order+1)."', extra_field_id='".$in['extra_id']."'");
			}
			$in['lines'] = build_some_dd($in['table'],$in['extra_id']);
		}else{
			$sort_order = $this->db->field("SELECT sort_order FROM ".$in['table']." ORDER BY sort_order DESC LIMIT 1");
		
			if($in['id']){
				$this->db->query("UPDATE ".$in['table']." SET name='".htmlspecialchars_decode($in['name'])."' WHERE id=".$in['id']." ");
			}else{
				$this->db->query("INSERT INTO ".$in['table']." SET name='".htmlspecialchars_decode($in['name'])."', sort_order='".($sort_order+1)."'");
			}
			$in['lines'] = build_some_dd($in['table']);	
		}


		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	function Add_EntryField(&$in){
		$sort_order = $this->db->field("SELECT sort_order FROM site_fields_drops WHERE field_id='".$in['field_id']."' ORDER BY sort_order DESC LIMIT 1");	
		if($in['id']){
			$this->db->query("UPDATE site_fields_drops SET name='".htmlspecialchars_decode($in['name'])."' WHERE id=".$in['id']." ");
		}else{
			$this->db->query("INSERT INTO site_fields_drops SET name='".htmlspecialchars_decode($in['name'])."', field_id='".$in['field_id']."', sort_order='".($sort_order+1)."'");
		}
		$in['lines'] = build_site_field_dropdown($in['field_id']);	
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
	}

	function select_editField(&$in){
		$i=0;
		foreach ($in['lines'] as $key => $value){
			if(!empty($value['id'])){
				$id = $this->db->field("SELECT id FROM site_fields_drops WHERE id='".$value['id']."' ");				
				if(!$id){
					$this->db->query("INSERT INTO site_fields_drops SET name='".htmlspecialchars_decode($value['name'])."', field_id='".$in['field_id']."', sort_order='".($key+1)."' ");
					$i++;
				}else{
					$this->db->query("UPDATE site_fields_drops SET sort_order='".($key+1)."' WHERE id='".$id."' ");			
				}
			}		
		}
		$in['lines'] = build_site_field_dropdown($in['field_id']);
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
	}

	function delete_selectField(&$in){
		$this->db->query("DELETE FROM site_fields_drops WHERE id='".$in['id']."' ");
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
	}

	function Add_InstallField(&$in){
		$sort_order = $this->db->field("SELECT sort_order FROM installation_fields_drops WHERE field_id='".$in['field_id']."' ORDER BY sort_order DESC LIMIT 1");	
		if($in['id']){
			$this->db->query("UPDATE installation_fields_drops SET name='".htmlspecialchars_decode($in['name'])."' WHERE id=".$in['id']." ");
		}else{
			$this->db->query("INSERT INTO installation_fields_drops SET name='".htmlspecialchars_decode($in['name'])."', field_id='".$in['field_id']."', sort_order='".($sort_order+1)."'");
		}
		$in['lines'] = build_instal_field_dropdown($in['field_id']);	
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
	}

	function select_InstallField(&$in){
		$i=0;
		foreach ($in['lines'] as $key => $value){
			if(!empty($value['id'])){
				$id = $this->db->field("SELECT id FROM installation_fields_drops WHERE id='".$value['id']."' ");				
				if(!$id){
					$this->db->query("INSERT INTO installation_fields_drops SET name='".htmlspecialchars_decode($value['name'])."', field_id='".$in['field_id']."', sort_order='".($key+1)."' ");						
				}else{
					$this->db->query("UPDATE installation_fields_drops SET sort_order='".($key+1)."' WHERE id='".$id."' ");			
				}
				$i++;
			}		
		}
		$in['lines'] = build_instal_field_dropdown($in['field_id']);
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
	}

	function delete_InstallField(&$in){
		$this->db->query("DELETE FROM installation_fields_drops WHERE id='".$in['id']."' ");
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
	}

	function select_edit(&$in){
		$query_sync=array();
		$i=0;

		foreach ($in['lines'] as $key => $value){
			if(!empty($value['id'])){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");				
				if(!$id){
					$filter ='';
					if ($in['table']=='customer_extrafield_dd' && is_numeric($in['extra_id']))
					{
						$filter = ", extra_field_id='".$in['extra_id']."'";
					}
					if ($in['table']=='contact_extrafield_dd' && is_numeric($in['extra_id']))
					{
						$filter = ", extra_field_id='".$in['extra_id']."'";
					}
					$inserted_id=$this->db->insert("INSERT INTO ".$in['table']." SET name='".htmlspecialchars_decode($value['name'])."', sort_order='".($key+1)."' ".$filter." ");
					if($in['table']=='tblquote_lost_reason'){	
						if($in['quote_id']){
							$this->db->query("UPDATE tblquote SET lost_id='".$inserted_id."' WHERE id='".$in['quote_id']."' ");
						}
					}
					$query_sync[$i]="INSERT INTO ".$in['table']." SET id='".$inserted_id."', name='".htmlspecialchars_decode($value['name'])."', sort_order='".($key+1)."' ".$filter." ";

					if($in['table']=='contract_lost_reason'){	
						if($in['contract_id']){
							$this->db->query("UPDATE contracts SET lost_id='".$inserted_id."' WHERE contract_id='".$in['contract_id']."' ");
						}
					}
					$i++;
				}
			}
		}

		Sync::start(ark::$model.'-'.ark::$method);

        foreach ($in['lines'] as $key => $value){
			/*if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value['name'])."', sort_order='".($key+1)."' WHERE id='".$id."' ");

				}
			}*/
			if(!empty($value)){
				if($in['table']=='customer_type'){
					$info = $this->db->query("SELECT id,name FROM ".$in['table']." WHERE id='".$value['id']."' ");
					$id = $info->f('id');
					$old_value = $info->f('name');
				}else{
					$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				}
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".htmlspecialchars_decode($value['name'])."', sort_order='".($key+1)."' WHERE id='".$id."' ");

					# begin update type of relationship on every customer if has this type.
					if($in['table']=='customer_type' && $old_value != $value){
						$all_c_t = array();
						$all_c_type = $this->db->query("SELECT id,name FROM customer_type ");
						while ($all_c_type->next()) {
							$all_c_t[$all_c_type->f('id')] = $all_c_type->f('name');
						}
						$cust_with_this_rel = $this->db->query("SELECT customer_id, c_type, c_type_name FROM customers");
						while($cust_with_this_rel->next()){
							$update_customer = false;
							$c_type = $cust_with_this_rel->f('c_type');
							$c_type = explode(',', $c_type);
							$type_names = '';
							foreach ($c_type as $key => $value2) {
								if(array_key_exists($value2, $all_c_t)){
									// $type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$value2."' ");
									$type_names .= $all_c_t[$value2].', ';
								}
								if($id == $value2){
									$update_customer = true;
								}
							}
							if($update_customer==true){

								$type_names = addslashes(rtrim(rtrim($type_names,' '),','));
								$this->db->query("UPDATE customers SET c_type_name = '".$type_names."' WHERE customer_id = '".$cust_with_this_rel->f('customer_id')."' ");
							}
						}
					}
					# end update

				}
			}
		}
		if($in['extra_id']){
			$in['lines'] = build_some_dd($in['table'],$in['extra_id']);
		}else{
			$in['lines'] = build_some_dd($in['table']);
		}
		$in['inserted_id'] = $inserted_id;
		Sync::end(0,$query_sync);
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function delete_select(&$in){
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM ".$in['table']." WHERE id='".$in['id']."' ");
		Sync::end();
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	function insert_comment(&$in)
	{
		$in['log_id'] = insert_message_log($in['pag'],addslashes($in['comment']),$in['field_name'],$in['field_value'],1,$in['user_id']);
		if($in['to_user_id']){
			#do somehting
			$this->db->query("UPDATE logging SET to_user_id='".$in['to_user_id']."', due_date='".strtotime($in['comment_due_date'])."', reminder_date='".strtotime($in['comment_due_date'])."', log_comment='New task' WHERE log_id='".$in['log_id']."' ");
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['to_user_id']."' AND name='send_email_type' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['to_user_id'],'name'=>'send_email_type']);
			if($this->db_users->next()){
				$send_email_type = $this->db_users->f('value');
				$send_email_type = explode(',',$send_email_type);
				if($send_email_type[0] == '1'){
					$this->send_notification_email($in);
				}
				$today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
				$today_end = mktime( 23, 59, 59, date('m'), date('d'), date('Y'));
				$due_date = $in['comment_due_date'];
				if($send_email_type[0] != '1' && $send_email_type[1] == '2' && $due_date > $today && $due_date < $today_end){
					$this->send_notification_email($in);
				}

			}
		}

		return true;
	}

	function delete_comment(&$in)
	{
		$this->db->query("DELETE FROM logging WHERE log_id='".$in['log_id']."' ");
		return true;
	}

	function send_notification_email(&$in)
	{
		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		// require_once ('../../classes/class.phpmailer.php');
        // include_once ("../../classes/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
		/*$db_name = $this->db_users->query("SELECT users.database_name, users.email, lang.code FROM users
						   INNER JOIN lang ON users.lang_id=lang.lang_id WHERE user_id='".$in['to_user_id']."' ");*/
		$db_name = $this->db_users->query("SELECT users.database_name, users.email, lang.code FROM users
						   INNER JOIN lang ON users.lang_id=lang.lang_id WHERE user_id= :user_id ",['user_id'=>$in['to_user_id']]);						   
		$db_name->next();
        $log = $this->db->query("SELECT * FROM logging WHERE log_id='".$in['log_id']."' ");
		$message_log = '';
		$log->next();
		global $config;
		switch ($log->f('pag')) {
			case 'order':
				$link = '<a href="'.$config['site_url'].'order/view/'.$log->f('field_value').'">order</a>';
				break;
			case 'opportunity':
				$link = '<a href="'.$config['site_url'].'/index.php?do=quote-opportunity&opportunity_id='.$log->f('field_value').'">opportunity</a>';# trebuie schimbat cand se face modulu de opportunity
				break;
			case 'stock_dispatching_info':
				$link = '<a href="'.$config['site_url'].'stock_dispatch/view/'.$log->f('field_value').'">order</a>';
				break;
			case 'invoice':
				$link = '<a href="'.$config['site_url'].'invoice/view/'.$log->f('field_value').'">invoice</a>';
				break;
			case 'purchase_invoice':
				$link = '<a href="'.$config['site_url'].'purchase_invoice/view/'.$log->f('field_value').'">purchase invoice</a>';
			break;
			case 'quote':
				$link = '<a href="'.$config['site_url'].'quote/view/'.$log->f('field_value').'">quote</a>';
				break;
			case 'project':
				$link = '<a href="'.$config['site_url'].'project/edit/'.$log->f('field_value').'">project</a>';
				break;
			case 'customer':
				$front_register  = $this->db->field("SELECT front_register FROM customers WHERE customer_id='".$log->f('field_value')."'");
				if($front_register==1){
					$link = '<a href="'.$config['site_url'].'/index.php?do=pim-customer&customer_id='.$log->f('field_value').'&front_register=1">customer</a>';
				}else{
					$link = '<a href="'.$config['site_url'].'/index.php?do=company-customer&customer_id='.$log->f('field_value').'">customer</a>';
				}
				# cand se va face modulu de webshop trebuie adaptate si folosite linkurile de sus
				$link = '<a href="'.$config['site_url'].'customerView/'.$log->f('field_value').'">customer</a>';
				break;
			case 'xcustomer_contact':
				$front_register  = $this->db->field("SELECT front_register FROM customer_contacts WHERE contact_id='".$log->f('field_value')."'");
				if($front_register==1){
					$link = '<a href="'.$config['site_url'].'/index.php?do=pim-xcustomer_contact&contact_id='.$log->f('field_value').'&front_register=1">contact</a>';
				}else{
					$link = '<a href="'.$config['site_url'].'/index.php?do=company-xcustomer_contact&contact_id='.$log->f('field_value').'">contact</a>';
				}
				# cand se va face modulu de webshop trebuie adaptate si folosite linkurile de sus
				$link = '<a href="'.$config['site_url'].'contactView/'.$log->f('field_value').'">contact</a>';
				break;
			case 'maintenance':
				$link = '<a href="'.$config['site_url'].'service/edit/'.$log->f('field_value').'">intervention</a>';
				break;
			default:
				$link = $log->f('pag');
				break;
		}

		$from_user = $this->db_users->query("SELECT * FROM users WHERE user_id='".$log->f('user_id')."' ");
		$from_user->next();
		$user_email = $this->db_users->query("SELECT * FROM users WHERE user_id='".$log->f('to_user_id')."' ");
		$user_email->next();

$code = $db_name->f('code');
if($code == 'nl'){$code = 'du';}
$lngCode = $code;
if($lngCode == 'du'){$lngCode = 'nl';}
$subject_txt = gm('Subject',true);
$new_txt = gm('New task',true);
$date_txt = gm('Date',true);
$to_txt = gm('To',true);
$item_txt = gm('Item',true);
		$message_log .='
----------------------------------------------------
<table width="100%"><tbody><tr><td width="10%" style="text-align:right">From :</td><td>&nbsp;&nbsp;'.utf8_decode($from_user->f('first_name')).' '.utf8_decode($from_user->f('last_name')).'</td></tr><tr><td style="text-align:right">'.utf8_encode($subject_txt['0'][$lngCode]).':</td><td>&nbsp;&nbsp;'.htmlspecialchars($new_txt['0'][$lngCode]).'</td></tr><tr><td style="text-align:right">'.utf8_encode($date_txt['0'][$lngCode]).' :</td><td>&nbsp;&nbsp;'.date(ACCOUNT_DATE_FORMAT,$log->f('date')).'</td></tr><tr><td style="text-align:right">'.utf8_encode($to_txt['0'][$lngCode]).' :</td><td>&nbsp;&nbsp;'.utf8_decode($user_email->f('first_name')).' '.utf8_decode($user_email->f('last_name')).'</td></tr><tr><td style="text-align:right">'.utf8_encode($item_txt['0'][$lngCode]).' :</td><td>&nbsp;&nbsp;'.$link.'</td></tr></tboby></table>

'.utf8_decode($log->f('message')).'

----------------------------------------------------
';
		$code = $db_name->f('code');
		if($code == 'nl'){$code = 'du';}
		$mail = new PHPMailer();
		$mail->WordWrap = 50;
		$message_data=get_sys_message('internal_message_late',$code,$db_name->f('database_name'));
		$body= (utf8_decode($message_data['text']));
		$body=str_replace('[!MESSAGE!]',$message_log, $body );
		$subject = utf8_decode($message_data['subject']);


		$def_email = $this->default_email();
        $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
        $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);

        //$subject="New internal message";

        $mail->Subject = $subject;

        $mail->MsgHTML(nl2br(($body)));

        $mail->AddAddress($user_email->f('email'));
        
		$sent_date= time();
    	$this->db->query("UPDATE logging SET sent='1' WHERE log_id='".$in['log_id']."' ");

   		$mail->Send();
		return true;
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		return $array;
	}

	function deleteDropboxFile(&$in)
	{
		$info = $this->db->query("SELECT * FROM dropbox_files WHERE id='".$in['file_id']."' ");
		if($in['certificate_id']){			
			if($info->f('is_url')){
				$this->db->query("DELETE FROM dropbox_files WHERE id='".$in['file_id']."' ");
                //log of deleted link
                if ($in['drop_folder'] == 'orders' || $in['drop_folder'] == 'invoices') {
                    $str = $in['drop_folder'];
                    $pag = substr($str, 0, (strlen($str)-1));
                    $field_name = $pag.'_id';
                } elseif ($in['drop_folder'] == 'porders') {
                    $pag = 'p_order';
                    $field_name = 'p_order_id';
                } elseif ($in['drop_folder'] == 'APP/Akti/quotes') {
                    $pag = 'quote';
                    $field_name = 'quote_id';
                }
                if (!empty($pag) && !empty($field_name)){
                    insert_message_log($pag, 'Link ' .$info->f('link'). ' was removed by ' .get_user_name($_SESSION['u_id']), $field_name, $in['item_id'], false, $_SESSION['u_id']);
                }
				msg::success(gm('Link removed'),'success');
			}else{
				$d = new drop($in['drop_folder'],null,$in['item_id'],false,'',null,null, null, true);
				$e = $d->deleteF(stripslashes($in['path']));
				if(is_array($e)){
					if($in['file_id'] && is_numeric($in['file_id'])){
						$this->db->query("DELETE FROM dropbox_files WHERE id='".$in['file_id']."' ");
                        //log of deleted attachments
                        $unserialized_data=unserialize($info->f('serialized_content'));
                        $pag = '';
                        $field_name = '';
                        if ($in['drop_folder'] == 'orders' || $in['drop_folder'] == 'invoices') {
                            $str = $in['drop_folder'];
                            $pag = substr($str, 0, (strlen($str)-1));
                            $field_name = $pag.'_id';
                        } elseif ($in['drop_folder'] == 'porders') {
                            $pag = 'p_order';
                            $field_name = 'p_order_id';
                        } elseif ($in['drop_folder'] == 'APP/Akti/quotes') {
                            $pag = 'quote';
                            $field_name = 'quote_id';
                        }
                        if (!empty($pag) && !empty($field_name)){
                            insert_message_log($pag, 'Attachment ' .$unserialized_data->name. ' was removed by ' .get_user_name($_SESSION['u_id']), $field_name, $in['item_id'], false, $_SESSION['u_id']);
                        }
					}
					if($in['item_id'] && is_numeric($in['item_id'])){
						$this->db->query("DELETE FROM pim_order_certificate WHERE order_id='".$in['item_id']."' AND certificate_id='".$in['certificate_id']."' "); 
	        
					    $article_id = $this->db->query("SELECT article_id, delivery_id FROM pim_certificates WHERE certificate_id='".$in['certificate_id']."' ");
					     while($article_id->next()){
					       $this->db->query("UPDATE pim_orders_delivery
					                          SET `certified`  = '0'
					                         WHERE order_articles_id='".$article_id->f('article_id')."' AND delivery_id='".$article_id->f('delivery_id')."'");

					     }

					    $this->db->query("DELETE FROM pim_certificates WHERE certificate_id='".$in['certificate_id']."' "); 
					}
					msg::success(gm('File deleted'),"success");
				}else{
					msg::error($e,"error");
				}
			}		
			json_out($in);

		}else if($in['is_batch']){
			$d = new drop($in['drop_folder'],null,$in['item_id'],false,'',null,null, null, true);
			$e = $d->deleteF(stripslashes($in['path']));
			if(is_array($e)){
				if($in['file_id'] && is_numeric($in['file_id'])){
					$this->db->query("DELETE FROM dropbox_files WHERE id='".$in['file_id']."' ");
				}
				msg::success(gm('File deleted'),"success");
			}else{
				msg::error($e,"error");
			}
			json_out($in);

		}else if($in['document_files']){
			$d = new drop($in['drop_folder'],null,$in['item_id'],false,'',null,null, null, false,false,true);
			$e = $d->deleteF(stripslashes($in['path']));
			if(is_array($e)){
				msg::success(gm('File deleted'),"success");
			}else{
				msg::error($e,"error");
			}
			json_out($in);
		}else{
			if(!$in['drop_folder'] && !$in['customer_id']){
				msg::error(gm('Invalid Id'),"error");
				json_out($in);
			}
			if($info->f('is_url')){
				$this->db->query("DELETE FROM dropbox_files WHERE id='".$in['file_id']."' ");
                //log of deleted link
                if ($in['drop_folder'] == 'orders' || $in['drop_folder'] == 'invoices') {
                    $str = $in['drop_folder'];
                    $pag = substr($str, 0, (strlen($str)-1));
                    $field_name = $pag.'_id';
                } elseif ($in['drop_folder'] == 'porders') {
                    $pag = 'p_order';
                    $field_name = 'p_order_id';
                } elseif ($in['drop_folder'] == 'APP/Akti/quotes') {
                    $pag = 'quote';
                    $field_name = 'quote_id';
                }
                if (!empty($pag) && !empty($field_name)){
                    insert_message_log($pag, 'Link ' .$info->f('link'). ' was removed by ' .get_user_name($_SESSION['u_id']), $field_name, $in['item_id'], false, $_SESSION['u_id']);
                }
				msg::success(gm('Link removed'),'success');
			}else{
				$d = new drop($in['drop_folder'],$in['customer_id'],$in['item_id'],false,'',$in['isConcact'],$in['serial_number']);
				$e = $d->deleteF(stripslashes($in['path']));
				if(is_array($e)){
					if($in['file_id'] && is_numeric($in['file_id'])){
						$this->db->query("DELETE FROM dropbox_files WHERE id='".$in['file_id']."' ");
                        //log of deleted attachments
                        $unserialized_data=unserialize($info->f('serialized_content'));
                        $pag = '';
                        $field_name = '';
                        if ($in['drop_folder'] == 'orders' || $in['drop_folder'] == 'invoices') {
                            $str = $in['drop_folder'];
                            $pag = substr($str, 0, (strlen($str)-1));
                            $field_name = $pag.'_id';
                        } elseif ($in['drop_folder'] == 'porders') {
                            $pag = 'p_order';
                            $field_name = 'p_order_id';
                        } elseif ($in['drop_folder'] == 'APP/Akti/quotes') {
                            $pag = 'quote';
                            $field_name = 'quote_id';
                        }
                        if (!empty($pag) && !empty($field_name)){
                            insert_message_log($pag, 'Attachment ' .$unserialized_data->name. ' was removed by ' .get_user_name($_SESSION['u_id']), $field_name, $in['item_id'], false, $_SESSION['u_id']);
                        }
					}
					msg::success(gm('File deleted'),"success");
				}else{
					msg::error($e,"error");
				}
			}		
			json_out($in);
		}
	}

	function dropboxUpload(&$in)
	{
		$response=array();
		if(!$in['drop_folder'] && !$in['customer_id']){
			$response['error'] = gm('Invalid Id');
			echo json_encode($response);
			exit();
		}
		/*if($_FILES['Filedata']['error'] == 0){
			if($in['is_batch'] != 'null'){
				$d = new drop($in['drop_folder'],null,$in['item_id'],true,$in['folder'],null,null, null, true);
				$e = $d->upload($_FILES['Filedata']['name'],$_FILES['Filedata']['tmp_name'],$_FILES['Filedata']['size']);
				if($e){
					$response['success'] = gm("File uploaded");
					json_out($response);
				}else{
					$response['error'] = gm("Not connected to Dropbox");
					json_out($response);
				}
			}else{
				$d = new drop($in['drop_folder'],$in['customer_id'],$in['item_id'],true,$in['folder'],$in['isConcact'],$in['serial_number']);
				$e = $d->upload($_FILES['Filedata']['name'],$_FILES['Filedata']['tmp_name'],$_FILES['Filedata']['size']);
				if($e){
					$response['success'] = gm("File uploaded");
					json_out($response);
				}else{
					$response['error'] = gm("Not connected to Dropbox");
					json_out($response);
				}		
			}
		}
		$response['error'] = gm('unknown error');
		json_out($response);*/
		if (!empty($_FILES)) {
			// Validate the file type
			$size=$_FILES['Filedata']['size'];		    
		    $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
		    $fileParts = pathinfo($_FILES['Filedata']['name']); 
		    if(strpos(mime_content_type($_FILES['Filedata']['tmp_name']), 'image') !== false){
		    	$in['type']='2';
		    }else{
		    	$in['type']='1';
		    }
		    if($in['type']=='2'){
		      	if(in_array(strtolower($fileParts['extension']),$fileTypes)){
		      		if(!defined('MAX_IMAGE_SIZE') &&  $size>200000){
		      			$response['error'] = gm('Size exceeds 200kb');
		      			$response['filename'] = $_FILES['Filedata']['name'];
       					echo json_encode($response);
		      		}else{
		      			$target_file=$_FILES['Filedata']['tmp_name'];
                        if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE){
                            $micro_ar=explode(' ',microtime());
                            $micro_t=round($micro_ar[0]*1000);
                            $target_file = 'upload/'.DATABASE_NAME.'/'.$micro_ar[1].'_'.$micro_t.'_'.$_FILES['Filedata']["name"];
                            $image = new SimpleImage();
                            $image->load($_FILES['Filedata']['tmp_name']);
                            $image->scale(MAX_IMAGE_SIZE,$size);
                            $image->save($target_file);
                        }
		      			if($in['is_batch']){
							$d = new drop($in['drop_folder'],null,$in['item_id'],true,$in['folder'],null,null, null, true);
							$e = $d->upload($_FILES['Filedata']['name'],$target_file,$size);
							if($e){
								$l = $d->getLink(urldecode($e->path_display));
								$this->db->query("INSERT INTO dropbox_files SET 
									dropbox_folder='".addslashes($in['drop_folder'])."',
									parent_id='".$in['item_id']."',
									name='".addslashes($_FILES['Filedata']['name'])."',
									type='2',
									serialized_content='".addslashes(serialize($e))."',
									link='".addslashes($l)."' ");
								$response['success'] = gm("File uploaded");
								$response['filename'] = $_FILES['Filedata']['name'];
								echo json_encode($response);
							}else{							
								$response['error'] = gm("Not connected to Dropbox");
								$response['filename'] = $_FILES['Filedata']['name'];
								echo json_encode($response);
							}
						}else if($in['document_files']){
							$d = new drop($in['drop_folder'],null,$in['item_id'],false,$in['folder'],null,null, null, false,false,true);
							$e = $d->upload($_FILES['Filedata']['name'],$target_file,$size);
							if($e){
								$response['success'] = gm("File uploaded");
								$response['filename'] = $_FILES['Filedata']['name'];
								echo json_encode($response);
							}else{							
								$response['error'] = gm("Not connected to Dropbox");
								$response['filename'] = $_FILES['Filedata']['name'];
								echo json_encode($response);
							}
						}else{
							$d = new drop($in['drop_folder'],$in['customer_id'],$in['item_id'],true,$in['folder'],$in['isConcact'],$in['serial_number']);
							$e = $d->upload($_FILES['Filedata']['name'],$target_file,$size);
							if($e){
								$l = $d->getLink(urldecode($e->path_display));
								$this->db->query("INSERT INTO dropbox_files SET 
									dropbox_folder='".addslashes($in['drop_folder'])."',
									parent_id='".$in['item_id']."',
									name='".addslashes($_FILES['Filedata']['name'])."',
									type='2',
									serialized_content='".addslashes(serialize($e))."',
									link='".addslashes($l)."' ");
                                //log for uploaded attachments
                                $pag = '';
                                $field_name = '';
                                if ($in['drop_folder'] == 'orders' || $in['drop_folder'] == 'invoices') {
                                    $str = $in['drop_folder'];
                                    $pag = substr($str, 0, (strlen($str)-1));
                                    $field_name = $pag.'_id';
                                } elseif ($in['drop_folder'] == 'porders') {
                                    $pag = 'p_order';
                                    $field_name = 'p_order_id';
                                } elseif ($in['drop_folder'] == 'APP/Akti/quotes') {
                                    $pag = 'quote';
                                    $field_name = 'quote_id';
                                }
                                if (!empty($pag) && !empty($field_name)){
                                    insert_message_log($pag, 'Attachment ' .$e->name. ' was uploaded by ' .get_user_name($_SESSION['u_id']), $field_name, $in['item_id'], false, $_SESSION['u_id']);
                                }
								$response['success'] = gm("File uploaded");
								$response['filename'] = $_FILES['Filedata']['name'];
								echo json_encode($response);
							}else{
								$response['error'] = gm("Not connected to Dropbox");
								$response['filename'] = $_FILES['Filedata']['name'];
								echo json_encode($response);
							}		
						}
						if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE){
                            unlink($target_file);
                        }
		      		} 		
		      	}else{
		      		$response['error'] = gm('Invalid file type.');
		      		$response['filename'] = $_FILES['Filedata']['name'];
       				echo json_encode($response);
		      	}
		    }else if($in['type']=='1'){
		    	if(in_array(strtolower($fileParts['extension']),$fileTypes)){
		    		$response['error'] = gm('Invalid file type.');
		    		$response['filename'] = $_FILES['Filedata']['name'];
       				echo json_encode($response);
		    	}else{
	    		 	if($in['is_batch']){
						$d = new drop($in['drop_folder'],null,$in['item_id'],true,$in['folder'],null,null, null, true);
						$e = $d->upload($_FILES['Filedata']['name'],$_FILES['Filedata']['tmp_name'],$size);
						if($e){
							$l = $d->getLink(urldecode($e->path_display));
							$this->db->query("INSERT INTO dropbox_files SET 
								dropbox_folder='".addslashes($in['drop_folder'])."',
								parent_id='".$in['item_id']."',
								name='".addslashes($_FILES['Filedata']['name'])."',
								type='1',
								serialized_content='".addslashes(serialize($e))."',
								link='".addslashes($l)."' ");
							$response['success'] = gm("File uploaded");
							$response['filename'] = $_FILES['Filedata']['name'];
							echo json_encode($response);
						}else{
							$response['error'] = gm("Not connected to Dropbox");
							$response['filename'] = $_FILES['Filedata']['name'];
							echo json_encode($response);
						}
					}else if($in['document_files']){
						$d = new drop($in['drop_folder'],null,$in['item_id'],false,$in['folder'],null,null, null, false, false,true);
						$e = $d->upload($_FILES['Filedata']['name'],$_FILES['Filedata']['tmp_name'],$size);
						if($e){
							$response['success'] = gm("File uploaded");
							$response['filename'] = $_FILES['Filedata']['name'];
							echo json_encode($response);
						}else{
							$response['error'] = gm("Not connected to Dropbox");
							$response['filename'] = $_FILES['Filedata']['name'];
							echo json_encode($response);
						}
					}else{
						$d = new drop($in['drop_folder'],$in['customer_id'],$in['item_id'],true,$in['folder'],$in['isConcact'],$in['serial_number']);
						$e = $d->upload($_FILES['Filedata']['name'],$_FILES['Filedata']['tmp_name'],$size);
						if($e){
							$l = $d->getLink(urldecode($e->path_display));
							$this->db->query("INSERT INTO dropbox_files SET 
								dropbox_folder='".addslashes($in['drop_folder'])."',
								parent_id='".$in['item_id']."',
								name='".addslashes($_FILES['Filedata']['name'])."',
								type='1',
								serialized_content='".addslashes(serialize($e))."',
								link='".addslashes($l)."' ");
                            //log for uploaded attachments
                            $pag = '';
                            $field_name = '';
                            if ($in['drop_folder'] == 'orders' || $in['drop_folder'] == 'invoices') {
                                $str = $in['drop_folder'];
                                $pag = substr($str, 0, (strlen($str)-1));
                                $field_name = $pag.'_id';
                            } elseif ($in['drop_folder'] == 'porders') {
                                $pag = 'p_order';
                                $field_name = 'p_order_id';
                            } elseif ($in['drop_folder'] == 'APP/Akti/quotes') {
                                $pag = 'quote';
                                $field_name = 'quote_id';
                            }
                            if (!empty($pag) && !empty($field_name)){
                                insert_message_log($pag, 'Attachment ' .$e->name. ' was uploaded by ' .get_user_name($_SESSION['u_id']), $field_name, $in['item_id'], false, $_SESSION['u_id']);
                            }
							$response['success'] = gm("File uploaded");
							$response['filename'] = $_FILES['Filedata']['name'];
							echo json_encode($response);
						}else{
							$response['error'] = gm("Not connected to Dropbox");
							$response['filename'] = $_FILES['Filedata']['name'];
							echo json_encode($response);
						}		
					}  
		    	}    	
		    }
		}
		exit();
	}

	function renewAlloToken(&$in){
		$ch = curl_init();
    	$headers=array('Content-Type: application/json');
    	$c_data=array('data'=>array(
    		'username'		=> $in['username'],
    		'api_key'		=> $in['api_key']
    		));
    	$c_data=json_encode($c_data);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $c_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $in['url'].'/auth/user');

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>300 || $info['http_code']==0){
    		return false;
    	}else{
    		$data_c=json_decode($put);
    		$acloud_id=$this->db->field("SELECT app_id FROM apps WHERE name='Allocloud' AND type='main' AND main_app_id='0' ");
    		$token=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$acloud_id."' AND type='token' ");
    		if(!$token){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$acloud_id."', api='".$data_c->auth_token."', type='token' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".$data_c->auth_token."' WHERE main_app_id='".$acloud_id."' AND type='token' ");
    		}
    		$expiration=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$acloud_id."' AND type='expire' ");
    		if(!$expiration){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$acloud_id."', api='".(time()+3600)."', type='expire' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".(time()+3600)."' WHERE main_app_id='".$acloud_id."' AND type='expire' ");
    		}
    		return true;
    	}
	}

	/**
  * undocumented function
  *
  * @return void
  * @author PM
  **/
  function tryAddCValid(&$in)
  {
    if($in['add_customer']){
        $v = new validation($in);
      $v->field('name', 'name', 'required:unique[customers.name]');
      $v->field('country_id', 'country_id', 'required');
      return $v->run();  
    }
    if($in['add_individual']){
      $v = new validation($in);
      //$v->field('firstname', 'firstname', 'required');
      $v->field('lastname', 'lastname', 'required');
      // $v->field('email', 'Email', 'required:email:unique[customers.c_email]');
      $v->field('country_id', 'country_id', 'required');
      return $v->run();  
    }
    if($in['add_contact']){
		$v = new validation($in);
		$v->field('firstname', 'firstname', 'required');
		$v->field('lastname', 'lastname', 'required');
		return $v->run();  
	}
    return true;
  }

  /**
  * undocumented function
  *
  * @return void
  * @author PM
  **/
  function tryAddC(&$in){
    if(!$this->tryAddCValid($in)){ 
      json_out($in);
      return false; 
    }
    //$name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'"));
    $name_user= addslashes($this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]));
    //Set account default vat on customer creation
    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

    if(empty($vat_regime_id)){
      $vat_regime_id = 0;
    }

    $c_types = '';
	$c_type_name = '';
	if($in['c_type']){
		/*foreach ($in['c_type'] as $key) {
			if($key){
				$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
				$c_type_name .= $type.',';
			}
		}
		$c_types = implode(',', $in['c_type']);
		$c_type_name = rtrim($c_type_name,',');*/
		if(is_array($in['c_type'])){
			foreach ($in['c_type'] as $key) {
				if($key){
					$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
					$c_type_name .= $type.',';
				}
			}
			$c_types = implode(',', $in['c_type']);
			$c_type_name = rtrim($c_type_name,',');
		}else{
			$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
			$c_types = $in['c_type'];
		}
	}


	if($in['user_id']==''){
		$in['user_id']=$_SESSION['u_id'];
	}


	if($in['user_id']){
		/*foreach ($in['user_id'] as $key => $value) {
			$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
				$acc_manager .= $manager_id.',';
		}			
		$in['acc_manager'] = rtrim($acc_manager,',');
		$acc_manager_ids = implode(',', $in['user_id']);*/
		if(is_array($in['user_id'])){
			foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);
		}else{
			//$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
			$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
			$acc_manager_ids = $in['user_id'];
		}
	}else{
		$acc_manager_ids = '';
		$in['acc_manager'] = '';
	}
	$vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
        $vat_default=str_replace(',', '.', $vat);
	$selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");


    if($in['add_customer']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
        if(SET_DEF_PRICE_CAT == 1){
			$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
		}
        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".$in['name']."',
                                            our_reference = '".$account_reference_number."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            type='0',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."',
                                            commercial_name 		= '".$in['commercial_name']."',
											legal_type				= '".$in['legal_type']."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											sector					= '".$in['sector']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											c_type_name				= '".$c_type_name."',
											vat_id 					= '".$selected_vat."',
											invoice_email_type		= '1',
											sales_rep				= '".$in['sales_rep_id']."',
											internal_language		= '".$in['internal_language']."',
											is_supplier				= '".$in['is_supplier']."',
											is_customer				= '".$in['is_customer']."',
											stripe_cust_id			= '".$in['stripe_cust_id']."',
											cat_id					= '".$in['cat_id']."'
                                            ");
      
        $this->db->query("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
					$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
				}
			}
		}

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                        ['user_id' => $_SESSION['u_id'],
                         'name'    => 'company-customers_show_info',
                         'value'   => '1']
                    );                            
        } else {
          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        if($in['app'] == 'general' || $in['after_save']){
        	json_out($in);
        }else{
        	return true;
        }
    }
    if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
        if(SET_DEF_PRICE_CAT == 1){
			$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
		}
        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".$in['lastname']."',
                                            our_reference = '".$account_reference_number."',
                                            firstname = '".$in['firstname']."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            type = 1,
                                            ".$supplier_filter."
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."',
                                            commercial_name 		= '".$in['commercial_name']."',
											legal_type				= '".$in['legal_type']."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											sector					= '".$in['sector']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											c_type_name				= '".$c_type_name."',
											vat_id 					= '".$selected_vat."',
											invoice_email_type		= '1',
											sales_rep				= '".$in['sales_rep_id']."',
											internal_language		= '".$in['internal_language']."',
											is_supplier				= '".$in['is_supplier']."',
											is_customer				= '".$in['is_customer']."',
											stripe_cust_id			= '".$in['stripe_cust_id']."',
											cat_id					= '".$in['cat_id']."'");
      
        $this->db->query("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
					$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
				}
			}
		}

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
		$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                        ['user_id' => $_SESSION['u_id'],
                         'name'    => 'company-customers_show_info',
                         'value'   => '1']
                    );                            
        } else {
          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        if($in['app'] == 'general' || $in['after_save']){
        	json_out($in);
        }else{
        	return true;
        }
    }
    if($in['add_contact']){
    	$customer_name='';
		if($in['buyer_id']){
			$customer_name = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
		}
		if($in['birthdate']){
			$in['birthdate'] = strtotime($in['birthdate']);
		}
	  	$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
					customer_id	=	'".$in['buyer_id']."',
					firstname	=	'".$in['firstname']."',
					lastname	=	'".$in['lastname']."',
					email		=	'".$in['email']."',
					birthdate	=	'".$in['birthdate']."',
					cell		=	'".$in['cell']."',
					sex			=	'".$in['sex']."',
					title		=	'".$in['title_contact_id']."',
					language	=	'".$in['language']."',
					company_name=	'".$customer_name."',
					`create`	=	'".time()."'");

	  	$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}

	  	if($in['buyer_id']){
			$contact_accounts_sql="";
			$accepted_keys=["position","department","title","e_title","email","phone","fax","s_email","customer_address_id"];
	 		foreach($in['accountRelatedObj'] as $key=>$value){
	 			if(in_array($value['model'],$accepted_keys)){
	 				$contact_accounts_sql.="`".$value['model']."`='".addslashes($value['model_value'])."',";
	 			}
	 		}
	 		if($in['email']){
	 			$contact_accounts_sql.="`email`='".$in['email']."',";
	 		}
	 		if(!empty($contact_accounts_sql)){
	 			$contact_accounts_sql=rtrim($contact_accounts_sql,",");
	 			$this->db->query("INSERT INTO customer_contactsIds SET 
		  								customer_id='".$in['buyer_id']."', 
		  								contact_id='".$in['contact_id']."',
		  								".$contact_accounts_sql." ");
	 		}	
	 	}
	  	$in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
	  	if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
                $vars=array();
                if($in['buyer_id']){
                    $vars['customer_id']=$in['buyer_id'];
                    $vars['customer_name']=$customer_name;
                }          
                $vars['contact_id']=$in['contact_id'];
                $vars['firstname']=$in['firstname'];
                $vars['lastname']=$in['lastname'];
                $vars['table']='customer_contacts';
                $vars['email']=$in['email'];
                $vars['op']='add';
                synctoZendesk($vars);
            }
	  	if($in['country_id']){
	    	$this->db->query("INSERT INTO customer_contact_address SET
	                      address='".$in['address']."',
	                      zip='".$in['zip']."',
	                      city='".$in['city']."',
	                      country_id='".$in['country_id']."',
	                      contact_id='".$in['contact_id']."',
	                      is_primary='1',
	                      delivery='1' ");
	  	}
	  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-contacts_show_info'  ");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                
	  	if(!$show_info->move_next()) {
	    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                              name    = 'company-contacts_show_info',
	                              value   = '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                              name    = :name,
	                              value   = :value ",
	                            ['user_id' => $_SESSION['u_id'],
	                             'name'    => 'company-contacts_show_info',
	                             'value'   => '1']
	                        );		                              
	  	} else {
	    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                  AND name    = 'company-contacts_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                  AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                  
	  	}
	  	$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
	  	if($count == 1){
	    	doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
	  	}
	  	if($in['item_id'] && is_numeric($in['item_id'])){
	  		$this->db->query("UPDATE tblquote SET contact_id='".$in['contact_id']."' WHERE id='".$in['item_id']."' ");	
	  	}
	  	insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
	  	msg::success (gm('Success'),'success');
	  	if($in['app'] == 'general' || ($in['app'] == 'customers' && $in['app_mod'] && $in['app_mod']=='view')){
        	json_out($in);
        }else{
        	return true;
        }
    }
    return true;
  }

  	function default_task_add(&$in)
	{
		if(!$this->validate_task_add($in)){
			json_out($in);
		}
		$in['task_id'] = $this->db->insert("INSERT INTO default_data SET default_name='".$in['task_name']."', type='task_no' ");
		if($in['task_rate']){
			$this->db->query("INSERT INTO default_data SET default_name='task_rate', value='".return_value($in['task_rate'])."', default_main_id='".$in['task_id']."' ");
		}
		if($in['daily_rate']){
			$this->db->query("INSERT INTO default_data SET default_name='daily_rate', value='".return_value($in['daily_rate'])."', default_main_id='".$in['task_id']."' ");
		}
		if($in['task_billable']){
			$this->db->query("INSERT INTO default_data SET default_name='task_billable', value='yes', default_main_id='".$in['task_id']."' ");
		}
		msg::success(gm('Task added'),'success');
		json_out($in);
	}

	/**
	 * validate the add task process
	 *
	 * @return bool
	 * @author Mp
	 **/
	function validate_task_add(&$in)
	{
		$v = new validation($in);
		$v->f('task_name', 'Price', 'required');
		$v->f('task_rate', 'Hourly', 'required');
		$v->f('daily_rate', 'Daily', 'required');
		return $v->run();
	}

	function uploadS3(&$in){
		$response=array();
		if (!empty($_FILES)) {
			global $config;
	      	ark::loadLibraries(array('aws'));
	      	$a = new awsWrap(DATABASE_NAME);
	      	$micro_ar=explode(' ',microtime());
			$micro_t=round($micro_ar[0]*1000);
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);
			$size=$_FILES['Filedata']['size'];
			$in['name'] = 'tmp_file_'.$micro_ar[1].'.'.$ext;
			$targetPath = 'upload/'.DATABASE_NAME;
		    $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
		    // Validate the file type
		    $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
		    $fileParts = pathinfo($_FILES['Filedata']['name']);
		    mkdir(str_replace('//','/',$targetPath), 0775, true);
		    if(strpos(mime_content_type($_FILES['Filedata']['tmp_name']), 'image') !== false){
                $in['type']='2';
            }else{
                $in['type']='1';
            }
            $pag = '';
            $field_name = '';
		    if($in['type']=='2'){
		      	if(in_array(strtolower($fileParts['extension']),$fileTypes)){
		      		if(!defined('MAX_IMAGE_SIZE') && $size>200000){
		      			$response['error'] = gm('Size exceeds 200kb');
		      			$response['filename'] = $_FILES['Filedata']['name'];
       					echo json_encode($response);
		      		}else{
		      			if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE){
		      				$image = new SimpleImage();
                            $image->load($_FILES['Filedata']['tmp_name']);
                            $image->scale(MAX_IMAGE_SIZE,$size);
                            $image->save($targetFile);
		      			}else{
		      				move_uploaded_file($tempFile,$targetFile);
		      			}      			
	      				$pdfFile = $in['s3_folder'].'/file/parent_'.$in['id'].'_'.$micro_ar[1].'_'.$micro_t.'.'.$ext;
				      	$a->uploadFile($targetFile,$pdfFile);
				      	unlink($targetFile);
				      	$link = $a->getLink($config['awsBucket'].DATABASE_NAME.'/'.$pdfFile);
                          if($in['signature']){
                              $isSignature="`is_signature`='".$in['signature']."',";
                          }
	      				$this->db->query("INSERT INTO amazon_files SET 
	      					`master`='".$in['s3_folder']."',
	      					`parent_id`='".$in['id']."',
	      					`name`='".addslashes($_FILES['Filedata']['name'])."',
	      					`type`='".$in['type']."',
	      					`item`='".$pdfFile."',
	      					`link`='".$link."',
	      					".$isSignature."
	      					`expire`='".($micro_ar[1]+(60*60*24*365))."' ");
	      				// log of uploaded attachment
                        if ($in['s3_folder'] == 'intervention'){
                            $pag = 'service';
                            $field_name = 'service_id';
                        }
                        if (!empty($pag) && !empty($field_name)){
                            insert_message_log($pag, 'Attachment ' .$_FILES['Filedata']['name']. ' was uploaded by ' .get_user_name($_SESSION['u_id']), $field_name, $in['id'], false, $_SESSION['u_id']);
                        }
				        $response['success'] = gm('File uploaded');
				        $response['filename'] = $_FILES['Filedata']['name'];
				       	echo json_encode($response);
		      		} 		
		      	}else{
		      		$response['error'] = gm('Invalid file type.');
		      		$response['filename'] = $_FILES['Filedata']['name'];
       				echo json_encode($response);
		      	}
		    }else if($in['type']=='1'){
		    	if(in_array(strtolower($fileParts['extension']),$fileTypes)){
		    		$response['error'] = gm('Invalid file type.');
		    		$response['filename'] = $_FILES['Filedata']['name'];
       				echo json_encode($response);
		    	}else{
		    		move_uploaded_file($tempFile,$targetFile);
			      	$pdfFile = $in['s3_folder'].'/file/parent_'.$in['id'].'_'.$micro_ar[1].'_'.$micro_t.'.'.$ext;
				    $a->uploadFile($targetFile,$pdfFile);
				    unlink($targetFile);
				    $link = $a->getLink($config['awsBucket'].DATABASE_NAME.'/'.$pdfFile);
	      			$this->db->query("INSERT INTO amazon_files SET 
	      					`master`='".$in['s3_folder']."',
	      					`parent_id`='".$in['id']."',
	      					`name`='".addslashes($_FILES['Filedata']['name'])."',
	      					`type`='".$in['type']."',
	      					`item`='".$pdfFile."',
	      					`link`='".$link."',
	      					`expire`='".($micro_ar[1]+(60*60*24*365))."' ");
	      			//log of uploaded attachment
                    if ($in['s3_folder'] == 'intervention'){
                        $pag = 'service';
                        $field_name = 'service_id';
                    }
                    if (!empty($pag) && !empty($field_name)){
                        insert_message_log($pag, 'Attachment ' .$_FILES['Filedata']['name']. ' was uploaded by ' .get_user_name($_SESSION['u_id']), $field_name, $in['id'], false, $_SESSION['u_id']);
                    }
				    $response['success'] = gm('File uploaded');
				    $response['filename'] = $_FILES['Filedata']['name'];
				    echo json_encode($response);
		    	}    	
		    }
		}
		exit();
	}

	function deleteS3File(&$in){
		global $config;
    	$info = $this->db->query("SELECT * FROM amazon_files WHERE id='".$in['id']."' ");
        $pag = '';
        $field_name = '';
    	if($info->f('is_url')){
    		$this->db->query("DELETE FROM amazon_files WHERE id='".$in['id']."'");
            //log of deleted link
            if ($info->f('master') == 'intervention') {
                $pag = 'service';
                $field_name = 'service_id';
            }
            if (!empty($pag) && !empty($field_name)){
                insert_message_log($pag, 'Link ' .$info->f('link'). ' was removed by ' .get_user_name($_SESSION['u_id']), $field_name, $info->f('parent_id'), false, $_SESSION['u_id']);
            }
	     	msg::success(gm('Link removed'),'success');
    	}else{
    		ark::loadLibraries(array('aws'));
	      	$aws = new awsWrap(DATABASE_NAME);
	      	$link =  $aws->doesObjectExist($config['awsBucket'].DATABASE_NAME.'/'.$info->f('item'));
	      	if($link){
	        	$aws->deleteItem($config['awsBucket'].DATABASE_NAME.'/'.$info->f('item'));
	        	$this->db->query("DELETE FROM amazon_files WHERE id='".$in['id']."'");
                //log of deleted attachment
                if ($info->f('master') == 'intervention') {
                    $pag = 'service';
                    $field_name = 'service_id';
                }
                if (!empty($pag) && !empty($field_name)){
                    insert_message_log($pag, 'Attachment ' .$info->f('name'). ' was removed by ' .get_user_name($_SESSION['u_id']), $field_name, $info->f('parent_id'), false, $_SESSION['u_id']);
                }
	     		msg::success(gm('File deleted'),'success');
	      	}else{
	      		msg::success(gm('Could not delete file'),'error');
	      	}
    	}    	
      	json_out($in);
	}

	function get_related_fields(&$in){
		//$result = array('related_fields' => '');
		$result = array('related_fields' => array());

		if($in['related_for'] == 'add_customer' || $in['related_for'] == 'add_individual'){
			/*$queryF = $this->db->query("SELECT * FROM customise_field WHERE controller='company-customer' AND value=1  AND creation_value=1 ORDER BY sort_order ASC");
			while ($queryF->next()) {
				$fieldInfo = $this->customerStaticFieldsInfo($queryF);
				if(!empty($fieldInfo['label'])){
					$result['related_fields'][] = $fieldInfo; 
				}	
			}*/
			$queryF = $this->db->query("SELECT * FROM customise_field WHERE controller='company-customer' AND value=1  AND creation_value=1 AND field!='legal_type' ORDER BY sort_order ASC");
			$queryExtraF = $this->db->query("SELECT * FROM customer_fields WHERE value = '1' AND creation_value='1'  ORDER BY sort_order ASC ");

				while ($queryF->next()) {
			$fieldInfo = $this->customerStaticFieldsInfo($queryF);
				if(!empty($fieldInfo['label'])){
					$result['related_fields'][] = $fieldInfo; 
				}	
			}
			while ($queryExtraF->next()) {
				$f_value = $this->db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$queryExtraF->f('field_id')."' ");
					$creation = $queryExtraF->f('creation_value') == 1 ? true : false;

				if($queryExtraF->f('is_dd')==1){
					$extra_field_dd_name = $this->db->field("SELECT name
													FROM customer_extrafield_dd
													INNER JOIN customer_field ON ( customer_extrafield_dd.extra_field_id = customer_field.field_id
													AND customer_extrafield_dd.id = customer_field.value )
													WHERE extra_field_id =  '".$queryExtraF->f('field_id')."'
													AND customer_id='".$in['customer_id']."' ");
				}

				$result['related_fields'][] = array(
					'field_id'			=> $queryExtraF->f('field_id'),
					'label'				=> $queryExtraF->f('label'),
					'label_if'			=> $creation,
					'value'				=> $f_value ? $f_value : '',
					'normal_input'		=> $queryExtraF->f('is_dd')== 1 ? false:true,
					'extra_field_dd'	=> $queryExtraF->f('is_dd')== 1 ? build_extra_field_dd($f_value,$queryExtraF->f('field_id')):'',
					'extra_field_name' 	=> $queryExtraF->f('is_dd') == 1 ? $extra_field_dd_name : ( $f_value ? $f_value : '' ),
					'is_custom'			=> true,
					'sort_order'		=> $queryExtraF->f('sort_order'),
					);
			}

		} else if($in['related_for'] == 'add_contact'){
			$result['accountRelatedObj']=array();
			$queryF = $this->db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact' AND value=1  AND creation_value=1 ORDER BY sort_order ASC")->getAll();

			$queryExtraF = $this->db->query("SELECT * FROM contact_fields WHERE value = '1' AND creation_value='1'  ORDER BY sort_order ASC ");

			//add customer address till it will be put dynamic
			array_unshift($queryF,array(
				'controller'	=> 'company-xcustomer_contact',
				'field'			=> 'customer_address_id',
				'value'			=> '1',
				'default_value'	=> '1',
				'creation_value'	=> '1',
				'sort_order'		=> null
			));
			//end

			foreach ($queryF as $query) {
				$fieldInfo = $this->contactStaticFieldsInfo($query);
				if(!empty($fieldInfo['label'])){
					$result['related_fields'][] = $fieldInfo; 
				}
				if($query['field'] == 'email'){
					continue; //we skip email to be shown on customer side data
				}
				$accountRelatedfieldInfo = $this->contactStaticAccountRelatedFieldsInfo($query);
				if(!empty($accountRelatedfieldInfo['label'])){
					$result['accountRelatedObj'][] = $accountRelatedfieldInfo;
				}
			}

			while($queryExtraF->next()){
				$f_value = $this->db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$queryExtraF->f('field_id')."' ");
				if($in['contact_id']=='tmp' || !$in['contact_id']){
					$creation = $queryExtraF->f('creation_value') == 1 ? true : false;
				}else{
					$creation= true;
				}
				if($value['is_dd']==1){
					$extra_field_dd_name = $this->db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$queryExtraF->f('field_id')."' ");
				}

				$result['related_fields'][] = array(
					'field_id'			=> $queryExtraF->f('field_id'),
					'label'				=> $queryExtraF->f('label'),
					'label_if'			=> $creation,
					'value'				=> $f_value ? $f_value : '',
					'normal_input'		=> $queryExtraF->f('is_dd')== 1 ? false:true,
					'extra_field_name' 	=> $queryExtraF->f('is_dd') == 1 ? $extra_field_dd_name : ( $f_value ? $f_value : '' ),
					'is_custom'			=> true,
					'sort_order'		=> $queryExtraF->f('sort_order'),
					);
			}

			$result['customer_contact_title']		= build_contact_title_type_dd();
			$result['customer_contact_dep']			= build_contact_dep_type_dd();
			$result['lang_dd']						= build_language_dd_new();
			$result['customer_contact_language']	= build_language_dd();
			$result['country_dd']					= build_country_list('');
			$result['customer_contact_job_title'] 	= build_contact_function();
			$result['extra']=array();

			$label = $this->db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY field_id ");
			$all = $label->getAll();
			$result['extra_data'] = array();
			foreach ($all as $key => $value) {
			  if($value['is_dd']==1){
			    $result['extra_data'][$value['field_id']]=build_extra_contact_field_dd('',$value['field_id']);
			  }
			}
			$in['xget']='customers';
			$in['return_data']=true;
			$result['customer_dd']=ark::run('customers-Contact');	
			unset($in['xget']);
			unset($in['return_data']);
		} 
		
		usort($result['related_fields'], "myCustomSort");
		if($in['return_data']){
			return $result['related_fields'];
		}else{
			json_out($result);
		}		
	}

	function contactStaticFieldsInfo($query){
		$fieldLabelName = '';
		$ifCondition = '';
		$isDD = false;
		$selectizeConfig = '';
		$selectizeOptions = '';
		$model = '';
		$isCustom = false;
		$sortOrder = $query['sort_order'];
		$hasTable = false;
		$table = '';
		$isDate = false;

		switch($query['field']){
			case 'title':
				$fieldLabelName = gm('Salutation');
				$ifCondition = 'title_if';
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_contact_title';
				$model = 'title_contact_id';
				$hasTable = true;
				$table = 'customer_contact_title';
				break;
			case 'gender':
				$fieldLabelName = gm('Gender');;
				$ifCondition = 'gender_if';
				$isDD = true;
				$selectizeConfig = 'cfg';
				$selectizeOptions = 'gender_dd';
				$model = 'sex'; 
				break;
			case 'language':
				$fieldLabelName = gm('Language');
				$ifCondition = 'language_if';
				$isDD = true;
				$selectizeConfig = 'cfg';
				$selectizeOptions = 'lang_dd';
				$model = 'language';
				break;
			case 'mobile':
				$fieldLabelName = gm('Cell');
				$ifCondition = 'mobile_if';
				$model = 'cell';
				break;
			case 'birthdate':
				$fieldLabelName = gm('Birthdate');
				$ifCondition = 'birthdate_if';
				$isDate = true;
				$model = 'birthdate';
				break;
		}

		return array('label' 			=> $fieldLabelName,
				'if_condition' 		=> $ifCondition,
				'is_dd'		   		=> $isDD,
				'selectize_config' 	=> $selectizeConfig,
				'selectize_options' => $selectizeOptions,
				'model'				=> $model,
				'is_custom'			=> $isCustom,
				'sort_order'		=> $sortOrder,
				'has_table'			=> $hasTable,
				'table'				=> $table,
				'is_date'			=> $isDate,
				);
	}

	function contactStaticAccountRelatedFieldsInfo($query){
		$fieldLabelName = '';
		$ifCondition = '';
		$isDD = false;
		$selectizeConfig = '';
		$selectizeOptions = '';
		$model = '';
		$isCustom = false;
		$sortOrder = $query['sort_order'];
		$hasTable = false;
		$table = '';
		$hasName = false;
		$is_checkbox=false;
		
		switch($query['field']){
			case 'email':
				$fieldLabelName = gm('Professional Email');
				$model = 'email';
				$hasName = true;
				break;
			case 'exact_title':
				$fieldLabelName = gm('Exact Title');
				$model = 'e_title';
				$hasName = true; 
				break;
			case 'phone':
				$fieldLabelName = gm('Phone');
				$model = 'phone';
				break;
			case 'fax':
				$fieldLabelName = gm('Fax');
				$model = 'fax';
				break;
			case 'function':
				$fieldLabelName = gm('Function');
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_contact_job_title';
				$hasTable = true;
				$table = 'customer_contact_job_title';
				$model = 'position';
				break;
			case 'department':
				$fieldLabelName = gm('Department');
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_contact_dep';
				$hasTable = true;
				$table = 'customer_contact_dep';
				$model = 'department';
				break;	
			case 's_email':
				$fieldLabelName = gm('Allow on e-mailing lists');
				$model = 's_email';
				$hasName = true; 
				$is_checkbox=true;
				break;
			case 'customer_address_id':
				$fieldLabelName = gm('Location');
				$isDD = true;
				$selectizeConfig = 'addressCfg';
				$model = 'customer_address_id';
				break;
		}

		return array('label' 			=> $fieldLabelName,
				'if_condition' 		=> $ifCondition,
				'is_dd'		   		=> $isDD,
				'selectize_config' 	=> $selectizeConfig,
				'selectize_options' => $selectizeOptions,
				'model'				=> $model,
				'is_custom'			=> $isCustom,
				'sort_order'		=> $sortOrder,
				'has_table'			=> $hasTable,
				'table'				=> $table,
				'has_name'			=> $hasName,
				'is_checkbox'		=> $is_checkbox
				);
	}

	function customerStaticFieldsInfo($query){
		/*$fieldLabelName = '';
		$ifCondition = '';
		$sortOrder = $query->f('sort_order');

		switch($query->f('field')){
			case 'first_name':
				$fieldLabelName = gm('First Name');
				$ifCondition = 'first_name_if';
				break;		
		}

		return array('label' 			=> $fieldLabelName,
				'if_condition' 		=> $ifCondition,
				'sort_order'		=> $sortOrder
				);*/
		$fieldLabelName = '';
		$ifCondition = '';
		$isDD = false;
		$selectizeConfig = '';
		$selectizeOptions = '';
		$model = '';
		$isCustom = false;
		$sortOrder = $query->f('sort_order');
		$hasTable = false;
		$table = '';
		$isCheckbox = false;

		switch($query->f('field')){
			case 'legal_type':
				$fieldLabelName = gm('Legal Type');
				$ifCondition = 'legal_type_if';
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_legal_type';
				$model = 'legal_type';
				$hasTable = true;
				$table = 'customer_legal_type';
				break;
			case 'commercial_name':
				$fieldLabelName = gm('Commercial Name');
				$ifCondition = 'commercial_name_if';
				$isDD = false;
				$model = 'commercial_name'; 
				break;
			case 'our_reference':
				//$fieldLabelName = gm('Our reference');
				$fieldLabelName = gm('Account Number');
				$ifCondition = 'our_reference_if';
				$model = 'our_reference';
				break;
			case 'sector':
				$fieldLabelName = gm('Activity Sector');
				$ifCondition = 'sector_if';
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_sector';
				$hasTable = true;
				$table = 'customer_sector';
				$model = 'sector';
				break;
			case 'c_type':
				$fieldLabelName = gm('Type of relationship');
				$ifCondition = 'c_type_if';
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_type';
				$hasTable = true;
				$table = 'customer_type';
				$model = 'c_type';
				break;
			case 'language':
				$fieldLabelName = gm('Language');
				$ifCondition = 'language_if';
				$isDD = true;
				$selectizeConfig = 'cfg';
				$selectizeOptions = 'lang_dd';
				$model = 'language';
				break;
			case 'acc_manager':
				$fieldLabelName = gm('Account Manager');
				$ifCondition = 'acc_manager_if';
				$isDD = true;
				$selectizeConfig = 'customerTypeCfg';
				$selectizeOptions = 'accountManager';
				$model = 'user_id';
				break;
			case 'sales_rep':
				$fieldLabelName = gm('Sales representative');
				$ifCondition = 'sales_rep_if';
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'sales_rep';
				$model = 'sales_rep_id';
				$hasTable = true;
				$table = 'sales_rep';
				break;
			case 'lead_source':
				$fieldLabelName = gm('Lead source');
				$ifCondition = 'lead_source_if';
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_lead_source';
				$model = 'lead_source';
				$hasTable = true;
				$table = 'customer_lead_source';
				break;
			case 'comp_size':
				$fieldLabelName = gm('Company size');
				$ifCondition = 'comp_size_if';
				$model = 'comp_size';
				break;
			case 'activity':
				$fieldLabelName = gm('Activity');
				$ifCondition = 'activity_if';
				$isDD = true;
				$selectizeConfig = 'salesCfg';
				$selectizeOptions = 'customer_activity';
				$model = 'activity';
				$hasTable = true;
				$table = 'customer_activity';
				break;
			case 'is_supplier':
				$fieldLabelName = gm('Supplier');
				$ifCondition = 'is_supplier_if';
				$model = 'is_supplier';
				$isCheckbox = true;
				break;
			case 'is_customer':
				$fieldLabelName = gm('Customer');
				$ifCondition = 'is_customer_if';
				$model = 'is_customer';
				$isCheckbox = true;
				break;
			case 'first_name':
				$fieldLabelName = gm('First Name');
				$ifCondition = 'first_name_if';
				$model = 'firstname';
				break;	
			case 'stripe_cust_id':
				$fieldLabelName = gm('Stripe ID');
				$ifCondition = 'stripe_cust_id_if';
				$isDD = false;
				$model = 'stripe_cust_id'; 
				break;	
		}

		return array('label' 			=> $fieldLabelName,
				'if_condition' 		=> $ifCondition,
				'is_dd'		   		=> $isDD,
				'selectize_config' 	=> $selectizeConfig,
				'selectize_options' => $selectizeOptions,
				'model'				=> $model,
				'is_custom'			=> $isCustom,
				'sort_order'		=> $sortOrder,
				'has_table'			=> $hasTable,
				'table'				=> $table,
				'is_checkbox'		=> $isCheckbox,
				);
	}

	function myCustomSort($a, $b)
	{
		return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
	}

	function copyToS3(&$in){
		global $config;
		ark::loadLibraries(array('aws'));
		$a = new awsWrap(DATABASE_NAME);
		$upload_error=array();
		$upload_success=array();
		foreach($in['images'] as $key=>$value){
			$micro_ar=explode(' ',microtime());
			$micro_t=round($micro_ar[0]*100000);
			$ext_start=strrpos($value['item'],'.');
			$ext=substr($value['item'],$ext_start+1);
			$targetFile=$in['target_folder'].'/file/parent_'.$in['target_id'].'_'.$micro_ar[1].'_'.$micro_t.'.'.$ext;
			$source=$config['awsBucket'].DATABASE_NAME.'/'.$value['item'];
			$target=$config['awsBucket'].DATABASE_NAME.'/'.$targetFile;
			if($a->copyItem($source,$target)){
				$link = $a->getLink($config['awsBucket'].DATABASE_NAME.'/'.$targetFile);
				if($link){
					$this->db->query("INSERT INTO amazon_files SET 
						`master`='".$in['target_folder']."',
						`parent_id`='".$in['target_id']."',
						`name`='".addslashes($value['name'])."',
						`type`='2',
						`item`='".$targetFile."',
						`link`='".$link."',
						`expire`='".($micro_ar[1]+(60*60*24*365))."' "); 
					$upload_success[]=$value['name'].' '.gm('successfully copied');
				}else{
					$upload_error[]=$value['name'].' '.gm('could not be copied');
				}
			}else{
				$upload_error[]=$value['name'].' '.gm('could not be copied');
			}		
		}
		if(!empty($upload_error)){
			$err_msg='';
			foreach($upload_error as $key=>$value){
				$err_msg.=$value."\n";
			}
			$err_msg=rtrim($err_msg,"\n");
			msg::error(nl2br($err_msg),'error');
		}
		if(!empty($upload_success)){
			$succ_msg='';
			foreach($upload_success as $key=>$value){
				$succ_msg.=$value."\n";
			}
			$succ_msg=rtrim($succ_msg,"\n");
			msg::success (nl2br($succ_msg),'success');
		}	
		json_out($in);
	}

	function update_policy(&$in){
    	//$this->db_users->query("UPDATE users SET gdpr_status='".$in['type']."',gdpr_time='".time()."' WHERE user_id='".$_SESSION['u_id']."' ");
    	$this->db_users->query("UPDATE users SET gdpr_status= :gdpr_status,gdpr_time= :gdpr_time WHERE user_id= :user_id ",['gdpr_status'=>$in['type'],'gdpr_time'=>time(),'user_id'=>$_SESSION['u_id']]);
    	msg::success(gm('Changes saved'),'success');
    	json_out($in);
    }

    function updateColumnSettings(&$in){
    	if(!$in['selected'] || !count($in['selected'])){
    		msg::error(gm('You need to select at least 1 column'),'error');
    		json_out($in);
    	}
    	$this->db->query("DELETE FROM column_settings WHERE list_name='".$in['list_name']."' AND user_id='".$_SESSION['u_id']."' ");
    	foreach($in['selected'] as $key=>$value){
    		$this->db->query("INSERT INTO column_settings SET list_name='".$in['list_name']."',
    										column_name='".$value['column_name']."',
    										sort_order='".$key."',
    										user_id='".$_SESSION['u_id']."' ");
    	}
    	msg::success(gm("Changes have been saved."),'success');
    	json_out($in);
    }

    function addArticleVariant(&$in){
    	$article_attached=$this->db->field("SELECT id FROM pim_article_variants WHERE parent_article_id='".$in['parent_article_id']."' AND article_id='".$in['article_id']."' ");
    	if(!$article_attached){
    		$this->db->query("INSERT INTO pim_article_variants SET parent_article_id='".$in['parent_article_id']."',article_id='".$in['article_id']."' ");
    		msg::success(gm("Changes have been saved."),'success');
    	}else{
    		msg::error(gm("Article aldready added"),'error');
    	}
    	json_out($in);
    }

    function getReferralLink(){
	global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);
    
    $referral_code = $db_users->field("SELECT user_info.referral_code FROM user_info WHERE user_info.user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    if(!$referral_code){
    	$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$referral_code = substr(str_shuffle($permitted_chars), 0, 5);
    	$db_users->query("UPDATE user_info SET referral_code='".$referral_code."' WHERE user_info.user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    }
    $in['referral_code'] = $referral_code;
    $in['referral_link'] = 'http://akti.com/referral/?c='.$referral_code;
    json_out($in);
    //return $referral_code;
    }

    function updateInternalNotes($in){
    	$v=new validation($in);
    	$v->field("module","Module","required");
    	$v->field("field_name","Field Name","required");
    	$v->field("field_value","Field Value","required");
    	if(!$v->run()){
    		json_out($in);
    	}
    	$data_exists=$this->db->field("SELECT id FROM internal_notes WHERE `module`='".$in['module']."' AND field_name='".$in['field_name']."' AND field_value='".$in['field_value']."' ");
    	if($data_exists){
    		$this->db->query("UPDATE internal_notes SET `notes`='".addslashes($in['notes'])."' WHERE id='".$data_exists."' ");
    	}else{
    		$this->db->query("INSERT INTO internal_notes SET `module`='".$in['module']."', field_name='".$in['field_name']."', field_value='".$in['field_value']."', `notes`='".addslashes($in['notes'])."' ");
    	}
    	json_out($in);
    }

    function get_customer_other_fields($in){
    	$res=array();
    	$in['return_data']=true;
    	$res['related_fields']=$this->get_related_fields($in);
    	$res['is_legal_type']=false;

    	$is_legal_type = $this->db->field("SELECT creation_value FROM customise_field WHERE controller='company-customer' AND value=1  AND field='legal_type' ORDER BY sort_order ASC");
    	
    	if($is_legal_type == '1'){
    		$res['is_legal_type']=true;
    	}

    	$res['country_dd']		= build_country_list(0);
    	$res['country_id']		= ACCOUNT_DELIVERY_COUNTRY_ID;
    	$res['customer_contact_language']= build_language_dd();
		$res['customer_lead_source']		= build_l_source_dd();
		$res['customer_activity']		= build_c_activity_dd();
		$res['various1']					= build_various_dd($in['various1'],'1');
		$res['various2']					= build_various_dd($in['various2'],'2');
		$res['customer_legal_type']		= build_l_type_dd();
		$res['customer_sector']			= build_s_type_dd();
		$res['customer_type']			= getCustomerType();
		$res['accountManager']			= getAccountManager();
		$res['sales_rep']				= getSales();
		$res['vat_dd']					= build_vat_dd();
		$res['vat_regim_dd']				= build_vat_regime_dd();
		$res['currency_dd']				= build_currency_list();
		$res['cat_dd']					= build_cat_dd();
		$res['lang_dd']					= build_language_dd_new();
		$res['multiple_dd'] 				= build_identity_dd();
		$res['extra']					= array();
		$def=$this->db->query("SELECT * FROM customise_field WHERE controller='company-customer' AND value=1 ");
		while ($def->next()) {
			$line = array();
			$res[$def->f('field').'_if'] = $def->f('creation_value') == 1 ? true : false;
		}

		$label = $this->db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY field_id ");
		$all = $label->getAll();
		$res['extra_data']=array();
		foreach ($all as $key => $value) {
			if($value['is_dd']==1){
				$res['extra_data'][$value['field_id']]= build_extra_field_dd('',$value['field_id']);
			}
		}

    	json_out($res);
    }

    function retrieveCustomerNotes($in){
    	$v=new validation();
    	$v->field("buyer_id","Buyer id","required:exist[customers.customer_id]");
    	if(!$v->run()){
    		json_out($in);
    	}
    	$in['customer_notes']=stripslashes($this->db->field("SELECT customer_notes FROM customers WHERE customer_id='".$in['buyer_id']."' "));
    	json_out($in);
    }

    function updateCustomerNotes($in){
    	$v=new validation();
    	$v->field("buyer_id","Buyer id","required:exist[customers.customer_id]");
    	if(!$v->run()){
    		json_out($in);
    	}
    	$this->db->query("UPDATE customers SET customer_notes='".addslashes($in['notes'])."' WHERE customer_id='".$in['buyer_id']."' ");
    	msg::success('Changes saved','success');
    	json_out($in);
    }

     function get_barcode_articles($in){

     	$barcodes = preg_split("/[\s,;]+/", $in['barcodes'], -1, PREG_SPLIT_NO_EMPTY);
     	if(sizeof($barcodes)<1){
     		return false;
     	}
     	
    	$def_lang = DEFAULT_LANG_ID;
		if($in['lang']){
			$in['lang_id']  = $in['lang'];
			$def_lang 		= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];

		if($in['customer_id']){
			$in['buyer_id']=$in['customer_id'];
		}

		$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								  
								   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id ';
			if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    	$table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
		  	}
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_article_categories.name AS family,
						pim_article_categories.name AS categorie,
						pim_articles.supplier_reference,
						pim_articles.price AS unit_price,
						pim_articles.is_service,
						pim_articles.block_discount,
						pim_articles.use_combined,
						pim_articles.has_variants';

		

		$filter.=" 1=1 ";
		if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    $filter.=' AND pim_article_variants.article_id IS NULL ';
		}

	    $articles= array( 'lines' => array(), 'not_found'=>'');
	    $exists = false;
     	foreach($barcodes as $key => $value){
     		$exists = array_search($value, array_column($articles['lines'], 'ean_code'));
     		if( $exists !== false) {
     			$articles['lines'][$exists]['quantity'] ++;
     			continue;
     		}

     		$filter_code =" AND  pim_articles.ean_code='".$value."' ";
     		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter $filter_code AND pim_articles.active='1'  ORDER BY pim_articles.item_code LIMIT 1");

     		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='ORDER_FIELD_LABEL'");

			$time = time();

			$j=0;
			if($article->next()){
				$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
				
				if($in['buyer_id']){
					if($in['vat_regime_id']){
						if($in['vat_regime_id']<10000){
							if($in['vat_regime_id']==2){
								$vat=0;
							}
						}else{
							$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
								LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
								WHERE vat_new.id='".$in['vat_regime_id']."'");
							if(!$vat_regime){
								$vat_regime=0;
							}
							if($vat>$vat_regime){
								$vat=$vat_regime;
							}
						}			
					}else{
						$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
						if($vat_regime<10000){
							if($vat_regime==2){
								$vat=0;
							}
						}else{
							$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
								LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
								WHERE vat_new.id='".$vat_regime."'");
							if(!$vat_regime){
								$vat=0;
							}
						}
					}			
				}

				$values = $article->next_array();
				$tags = array_map(function($field){
					return '/\[\!'.strtoupper($field).'\!\]/';
				},array_keys($values));

				$label = preg_replace($tags, $values, $fieldFormat);

				if($article->f('price_type')==1){

				    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

			        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

			       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
			            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

			        }else{
			       	   	$price_value=$price_value_custom_fam;

			         	 //we have to apply to the base price the category spec
			    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
			    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
			    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

			    	    if($cat_price_type==2){
			                $article_base_price=get_article_calc_price($article->f('article_id'),3);
			            }else{
			                $article_base_price=get_article_calc_price($article->f('article_id'),1);
			            }

			       		switch ($cat_type) {
							case 1:                  //discount
								if($price_value_type==1){  // %
									$price = $article_base_price - $price_value * $article_base_price / 100;
								}else{ //fix
									$price = $article_base_price - $price_value;
								}
								break;
							case 2:                 //profit margin
								if($price_value_type==1){  // %
									$price = $article_base_price + $price_value * $article_base_price / 100;
								}else{ //fix
									$price =$article_base_price + $price_value;
								}
								break;
						}
			        }

				    if(!$price || $article->f('block_discount')==1 ){
			        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
			        }
			    }else{
			    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
			        if(!$price || $article->f('block_discount')==1 ){
			        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
			        }
			    }

			    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
			  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

			  	if($article->f('is_service') == 1){
			  		$price=$article->f('unit_price');
			        $base_price = $price;
			  	}

			    $start= mktime(0, 0, 0);
			    $end= mktime(23, 59, 59);
			    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
			    if($promo_price->move_next()){
			    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

			        }else{
			            $price=$promo_price->f('price');
			            $base_price = $price;
			        }
			    }
			 	if($in['buyer_id']){
			 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
			  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
			    	if($customer_custom_article_price->move_next()){

			            $price = $customer_custom_article_price->f('price');

			            $base_price = $price;
			       	}
			       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
			   	}

				$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");
				if($in['buyer_id']){
					$customer_disc = $this->db->query("SELECT line_discount, apply_line_disc FROM customers WHERE customer_id='".$in['buyer_id']."' ");
				}else{
					$customer_disc = $this->db->query("SELECT line_discount, apply_line_disc FROM customers WHERE customer_id='".$in['customer_id']."' ");
				}

				$nr_taxes = $this->db->field("SELECT COUNT(article_tax_id) FROM pim_articles_taxes WHERE article_id='".$article->f('article_id')."' ");
				//console::log($in['buyer_id'], $in['customer_id']);

				$linie = array(
				  	'article_id'				=> $article->f('article_id'),
				  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
				  	//'name'						=> htmlspecialchars_decode($article->f('internal_name')),
				  	'name'						=> html_entity_decode(stripslashes($article->f('internal_name')), ENT_QUOTES, 'UTF-8'),
				  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
				    'stock'						=> $article->f('stock'),
				    'supplier_reference'		=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? '': $article->f('supplier_reference'),
				    'family'					=> $article->f('family') ? $article->f('family') : '',
				    'stock2'					=> remove_zero_decimals_dn(display_number($article->f('stock'))),
				    'quantity'		    		=> 1,
				    'pending_articles'  		=> intval($pending_articles),
				    'threshold_value'   		=> $article->f('article_threshold_value'),
				  	'sale_unit'					=> $article->f('sale_unit'),
				  	'percent'           		=> $vat_percent,
					'percent_x'         		=> display_number($vat_percent),
				    'packing'					=> $article->f('packing')>0 ? remove_zero_decimals($article->f('packing')) : 1,
				  	'code'		  	    		=> $article->f('item_code'),
				  	'ean_code'		  	    	=> $article->f('ean_code'),
					/*'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price),*/
					'price'						=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price,
					'price_vat'					=> $in['remove_vat'] == 1 ? (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price) : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price + (($price*$vat)/100)) ,
					'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
					'purchase_price'			=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $purchase_price,
					'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
					'quoteformat'    			=> html_entity_decode(gfn($label)),
					/*'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? place_currency(display_number_var_dec(0)) : place_currency(display_number_var_dec($base_price))),*/
					'base_price'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? place_currency(display_number_var_dec(0)) : place_currency(display_number_var_dec($base_price)),
					'show_stock'				=> $article->f('hide_stock') ? false:true,
					'hide_stock'				=> $article->f('hide_stock'),
					'is_service'				=> $article->f('is_service'),
					'line_discount'				=> $customer_disc->f('apply_line_disc')==1? $customer_disc->f('line_discount') : 0,
					'has_variants'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? true : false,
					 'has_variants_done'		=> 0,
					 'is_variant_for'			=> 0,
					 'is_variant_for_line'		=> 0,
					'is_combined' 				=> $article->f('use_combined'),
					'visible'					=> 1,
					'has_taxes'					=> $nr_taxes? true : false,
					'facq'						=> $article->f('facq'),
					'allow_stock'               => defined('ALLOW_STOCK') && ALLOW_STOCK == 1 ? true : false
				);
				array_push($articles['lines'], $linie);
			  	
			}else{
				//array_push($articles['not_found'], $value);
				$articles['not_found'] .= $value.'<br>';
			}

     	}

		

		$articles['buyer_id'] 				= $in['buyer_id'];
		$articles['lang_id'] 				= $def_lang;
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			 	= $text;
		$articles['allow_stock']			= defined('ALLOW_STOCK') && ALLOW_STOCK == 1 ? true : false;
		
		json_out($articles);
	}

	 function getExchangeRate(&$in){
	 	$rate = 1;
	 	if($in['currency_id_base'] && $in['currency_id_to_change']){
	 		$rate = $this->db->field("SELECT rate FROM currency_rates WHERE currency_id_base='".$in['currency_id_base']."' AND currency_id_to_change='".$in['currency_id_to_change']."'");
	 		if(!$rate){
	 			$rate = 1;
	 		}
	 	}
	    $in['purchase_price_new'] = $in['purchase_price']* $rate;

		json_out($in);
    }

    function upload_s3_url_link(&$in){
    	$v=new validation($in);
    	$v->field('url_link','Url','required:url');
    	if(!$v->run()){
    		json_out($in);
    	}
    	$this->db->query("INSERT INTO amazon_files SET 
			`master`='".$in['s3_folder']."',
			`parent_id`='".$in['id']."',
			`name`='".$in['url_link']."',
			`type`='".$in['type']."',
			`link`='".$in['url_link']."',
			`is_url`='1' ");
        //log of uploaded link
        $pag = '';
        $field_name = '';
        if ($in['s3_folder'] == 'intervention') {
            $pag = 'service';
            $field_name = 'service_id';
        }
        if (!empty($pag) && !empty($field_name)){
            insert_message_log($pag, 'Link ' .$in['url_link']. ' was added by ' .get_user_name($_SESSION['u_id']), $field_name, $in['id'], false, $_SESSION['u_id']);
        }
    	msg::success ( gm('Link added'),'success');
    	json_out($in);
    }

    function upload_dropbox_url_link(&$in){
    	$v=new validation($in);
    	$v->field('url_link','Url','required:url');
    	if(!$v->run()){
    		json_out($in);
    	}
    	$this->db->query("INSERT INTO dropbox_files SET 
			dropbox_folder='".addslashes($in['drop_folder'])."',
			parent_id='".$in['item_id']."',
			type='".(strrpos($in['url_link'], ".jpg") === false && strrpos($in['url_link'], ".jpeg") === false && strrpos($in['url_link'], ".png") === false && strrpos($in['url_link'], ".gif") === false && strrpos($in['url_link'], ".tiff") === false && strrpos($in['url_link'], ".tif") === false && strrpos($in['url_link'], ".bmp") === false ? 1 : 2)."',
			link='".$in['url_link']."',
			certificate_id='".$in['certificate_id']."',
			`is_url`='1' ");
        //log of uploaded link
        if ($in['drop_folder'] == 'orders' || $in['drop_folder'] == 'invoices') {
            $str = $in['drop_folder'];
            $pag = substr($str, 0, (strlen($str)-1));
            $field_name = $pag.'_id';
        } elseif ($in['drop_folder'] == 'porders') {
            $pag = 'p_order';
            $field_name = 'p_order_id';
        } elseif ($in['drop_folder'] == 'APP/Akti/quotes') {
            $pag = 'quote';
            $field_name = 'quote_id';
        }
        if (!empty($pag) && !empty($field_name)){
            insert_message_log($pag, 'Link ' .$in['url_link']. ' was added by ' .get_user_name($_SESSION['u_id']), $field_name, $in['item_id'], false, $_SESSION['u_id']);
        }
    	msg::success ( gm('Link added'),'success');
    	json_out($in);
    }

}