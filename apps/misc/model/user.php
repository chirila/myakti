<?php
// include ('apps/misc/model/time.php');

class user {

	private $db;
	public $time;

	function __construct() {
		global $database_config;
		$this->db = new sqldb();
		$this->db_q = new sqldb();
		$this->database_2 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($this->database_2);
	}

	function update_account(&$in)
	{
		global $config;
		$in['user_id']=$_SESSION['u_id'];
		if(!$this->update_validate($in))
		{
			return false;
		}
		$this->db->query("UPDATE project_user SET user_name='".$in['first_name']." ".$in['last_name']."' WHERE user_id='".$_SESSION['u_id']."'");
		//$email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$email = $this->db_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		
		if($email != $in['email']){
			$this->sendValidationEmail($in);
			$in['notification_email_sent'] = true;
			$this->sendNotificationEmail($in);
		}

		//Reload the page only if the language was changed
		$in['language_change'] = false;
		//$old_lang_id = $this->db_users->field("SELECT lang_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$old_lang_id = $this->db_users->field("SELECT lang_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		if($old_lang_id != $in['lang_id']){
			$in['language_change'] = true;			
		} 

		$this->update_account_signature($in);
		// email 			= '".$in['email']."',
		/*$this->db_users->query("UPDATE users SET
					first_name 	    = '".htmlspecialchars($in['first_name'])."',
					last_name 		= '".htmlspecialchars($in['last_name'])."',
					username  	    = '".htmlspecialchars($in['username'])."',
					
					country_id 	    = '".$in['country_id']."',
					lang_id 		= '".$in['lang_id']."'
					WHERE user_id='".$_SESSION['u_id']."'
					");*/
		$this->db_users->query("UPDATE users SET
					first_name 	    = :first_name,
					last_name 		= :last_name,
					username  	    = :username,
					country_id 	    = :country_id,
					lang_id 		= :lang_id
					WHERE user_id= :user_id",
				[	'first_name' 	    => htmlspecialchars($in['first_name']),
					'last_name' 		=> htmlspecialchars($in['last_name']),
					'username'  	    => htmlspecialchars($in['username']),
					'country_id' 	    => $in['country_id'],
					'lang_id' 		=> $in['lang_id'],
					'user_id'		=> $_SESSION['u_id']]
			);

		if(strpos($config['site_url'], 'my.akti')){
			//if(strpos($config['site_url'], 'akti')){
		//Values for Gaetan account
				global $database_config;
				$database_new = array(
						'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
						'username' => 'admin',
						'password' => 'hU2Qrk2JE9auQA',
						'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
						//'database' => '32ada082_5cf4_1518_3aa09bca7f29'
				);
					
				$db_user_new = new sqldb($database_new);

				$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");

				$customer_contacts = $db_user_new->query("UPDATE customer_contacts SET
												firstname='".htmlspecialchars($in['first_name'])."',
												lastname='".htmlspecialchars($in['last_name'])."'
												WHERE contact_id='".$select_contact."'
				");
				$customer_contactsIds = $db_user_new->query("UPDATE customer_contactsIds SET email='".htmlspecialchars($in['email'])."'	WHERE contact_id='".$select_contact."'
				");

		//
		}





		$this->db->query("UPDATE pim_lang SET default_lang='0' ");
		$lang_code=$this->db->field("SELECT code FROM lang WHERE lang_id='".$in['lang_id']."' ");
		$this->db->query("UPDATE pim_lang SET default_lang='1' WHERE code='".$lang_code."' ");
		if($lang_code=='nl'){		
			$this->db->query("UPDATE pim_lang SET default_lang='1' WHERE code='du' ");
		}elseif($lang_code=='du'){
			$this->db->query("UPDATE pim_lang SET default_lang='1' WHERE code='nl' ");
		}
		//$contact_id = $this->db_users->field("SELECT contact_id FROM customer_contacts WHERE table_user_id='".$in['user_id']."'");
		$contact_id = $this->db_users->field("SELECT contact_id FROM customer_contacts WHERE table_user_id= :table_user_id",['table_user_id'=>$in['user_id']]);
		if ($contact_id)
		{
			// email 			= '".$in['email']."'
			/*$this->db_users->query("UPDATE customer_contacts SET
					firstname 	    = '".$in['first_name']."',
					lastname 		= '".$in['last_name']."',
					username  	    = '".$in['username']."'
					
					WHERE contact_id='".$contact_id."'
					");*/
			$this->db_users->query("UPDATE customer_contacts SET
					firstname 	    = :firstname,
					lastname 		= :lastname,
					username  	    = :username
					WHERE contact_id= :contact_id",
				['firstname' 	    => $in['first_name'],
				 'lastname' 		=> $in['last_name'],
				 'username'  	    => $in['username'],
				 'contact_id'		=> $contact_id]
			);
		}
		if($in['password']){
			$session = session_id();
        	//$id = $this->db_users->field("SELECT activity_id FROM user_activity WHERE session_id='".$session."' AND user_id='".$_SESSION['u_id']."' ");
        	$id = $this->db_users->field("SELECT activity_id FROM user_activity WHERE session_id= :session_id AND user_id=:user_id ",['session_id'=>$session,'user_id'=>$_SESSION['u_id']]);
			//$this->db_users->query("UPDATE users SET password = '".md5($in['password'])."'  WHERE user_id='".$_SESSION['u_id']."' ");
			$this->db_users->query("UPDATE users SET password = :password  WHERE user_id= :user_id ",['password'=>md5($in['password']),'user_id'=>$_SESSION['u_id']]);
			//$this->db_users->query("UPDATE user_info SET reset_request='' WHERE user_id='".$_SESSION['u_id']."' ");
			$this->db_users->query("UPDATE user_info SET reset_request= :reset_request WHERE user_id= :user_id ",['reset_request'=>'','user_id'=>$_SESSION['u_id']]);
			$this->db_users->query("UPDATE user_activity SET expired='1' WHERE user_id='".$_SESSION['u_id']."'  AND activity_id<>'".$id."' ");
		}

		//$this->db_users->query("SELECT code FROM lang WHERE lang_id='".$in['lang_id']."'");
		$this->db_users->query("SELECT code FROM lang WHERE lang_id= :lang_id",['lang_id'=>$in['lang_id']]);
		$this->db_users->move_next();
		$lang = $this->db_users->f('code');
		if($in['notification_daily']==1){ $in['notification_daily'] = 2; }
		$notification=$in['notification_instant'].','.$in['notification_daily'];
		//$this->db_users->query("DELETE FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='send_email_type' ");
		$this->db_users->query("DELETE FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'send_email_type']);
		//$this->db_users->query("INSERT INTO user_meta SET user_id='".$_SESSION['u_id']."', name='send_email_type', value='".$notification."' ");
		$this->db_users->insert("INSERT INTO user_meta SET user_id= :user_id, name= :name, value= :value ",['user_id'=>$_SESSION['u_id'],'name'=>'send_email_type','value'=>$notification]);

		$_SESSION['l'] = $lang;
		setcookie("l",$lang,time()+(60*60*24*356),'/');
		$_SESSION['success'] = 'ok';
		msg::success (gm("Account has been successfully updated"),'success');
		// header('Location: index.php?do=misc-'.$in['pag'].'&success=ok');
		return true;
	}
	function update_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('user_id', gm('ID'), "required:exist[users.user_id.( database_name='".DATABASE_NAME."')]", gm("Invalid ID"),$this->database_2);
		$is_ok = $v->run();

		if($is_ok && !$this->add_validate($in)){
			$is_ok = false;
		}
		return $is_ok;
	}

	/****************************************************************
	* function add_validate(&$in)                                   *
	****************************************************************/
	function add_validate(&$in)
	{
		$v = new validation($in);
		$is_ok = true;
		$v->field('first_name',gm('First Name'),'required');
		$v->field('last_name',gm('Last Name'),'required');
		$v->field('email',gm('Email'),'required:email');
		$v->field('lang_id',gm('Language'),'required',gm("Please select a language"));
		$v->field('username', gm("Username"), "required:min_length[6]:unique[users.username.( user_id!='".$in['user_id']."')]",false,$this->database_2);
		$v->field('password',gm("Password"),'strange_chars:min_length[6]');
		$v->field('password1',gm("Confirm Password"),'strange_chars:min_length[6]:match[password]');
		$is_ok = $v->run();

		return $is_ok;
	}

	function calendar_sync(&$in)
	{
		$calendar = $this->db->field("SELECT app_id FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");
		$google_sync=$this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$calendar."' AND type='".$in['name']."' ");
		if(!$google_sync){
			$this->db->query("INSERT INTO apps SET api='".$in['value']."', main_app_id='".$calendar."', type='".$in['name']."' ");
		}else{
			$this->db->query("UPDATE apps SET api='".$in['value']."' WHERE app_id='".$google_sync."' ");
		}
		msg::success(gm("Changes saved"),'success');
		json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function setModuleAccess(&$in)
	{
		// $this->db->query("UPDATE `settings` SET value='".$in['val']."' WHERE `constant_name`='MODULE_".$in['module']."' ");
		//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='MODULE_".$in['module']."' ");
		$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'MODULE_'.$in['module']]);
		if($this->db_users->move_next()){
			//$this->db_users->query("UPDATE user_meta SET value='".$in['val']."' WHERE user_id='".$_SESSION['u_id']."' AND name='MODULE_".$in['module']."' ");
			$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>$in['val'],'user_id'=>$_SESSION['u_id'],'name'=>'MODULE_'.$in['module']]);
		}else{
			//$this->db_users->query("INSERT INTO user_meta SET value='".$in['val']."', user_id='".$_SESSION['u_id']."', name='MODULE_".$in['module']."' ");
			$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>$in['val'],'user_id'=>$_SESSION['u_id'],'name'=>'MODULE_'.$in['module']]);
		}
		//msg::success(gm("Changes saved"),'success');
		json_out($in);
		return false;
	}
	function update_account_signature(&$in)
	{
		$in['user_id']=$_SESSION['u_id'];
		if(!$this->update_signature_validate($in))
		{
			return false;
		}

		//$this->db_users->query("UPDATE users SET text_signature = '".$in['text_signature']."' WHERE user_id='".$_SESSION['u_id']."'	");
		$this->db_users->query("UPDATE users SET text_signature = :text_signature WHERE user_id= :user_id	",['text_signature'=>$in['text_signature'],'user_id'=>$_SESSION['u_id']]);
		msg::success ( gm("Account signature has been successfully updated"),'success');
		return true;
	}




/****************************************************************
	* function update_signature_validate(&$in)                                   *
	****************************************************************/
	function update_signature_validate(&$in)
	{
		$v = new validation($in);
		$is_ok = true;
		$v->field('user_id', gm('ID'), "required:exist[users.user_id.( database_name='".DATABASE_NAME."')]", gm("Invalid ID"),$this->database_2);
		$is_ok = $v->run();

		return $is_ok;
	}

	function upload(&$in)
	{
		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME.'/';
			$in['name'] = 'user_signature_'.$_SESSION['u_id'].'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type

			$fileTypes = array('jpg','jpeg','gif'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
					global $database_config;
					$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
					);
					$db_upload = new sqldb($database_2);

/*				foreach (array('ACCOUNT_LOGO','ACCOUNT_LOGO_QUOTE','ACCOUNT_LOGO_ORDER','ACCOUNT_LOGO_WEBSHOP','ACCOUNT_LOGO_PO_ORDER','ACCOUNT_LOGO_TIMESHEET','ACCOUNT_MAINTENANCE_LOGO') as $constant){
					$db_upload->query("UPDATE settings SET
			                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
			                           long_value=1
			                           WHERE constant_name='".$constant."'");
				}*/
				$size = getimagesize("../../upload/".$in['folder']."/".$in['name']);
		    	$ratio = 250 / 77;

				if($size[0]/$size[1] > $ratio ){
					$attr = 'width="250"';
				}else{
					$attr = 'height="77"';
				}
				$response['success'] = $attr;
				msg::success($attr,'success');
				// echo json_encode($response);
			} else {
				$response['error'] = gm('Invalid file type.');
				// echo json_encode($response);
				msg::error($attr,'error');
			}
		}
		return true;
	}
	function do_query(&$in)
	{

		global $database_config;
		global $config;
		if ($in['users_only']==true)
		{
			$in['done'] = 'done';
			$in['database'] = $config['db_users'];
		}

		$dbq = new sqldb(array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $in['database'],
		));
		
		$dbq->exitOnError = false;
		if(($_SERVER['REMOTE_ADDR'] != '188.26.120.86' && $_SERVER['HTTP_X_FORWARDED_FOR'] != '188.26.120.86') && $_SERVER['HTTP_HOST'] != '10.0.0.1'){
			console::log('You have not power');
			return false;
		}
		$queries = explode('###',$in['query']);
		foreach ($queries as $query)
		{
			$query = trim($query);
			if(!empty($query)){
				if(!$dbq->query(stripslashes($query))){
					msg::error(('Fail to run query:'.$query.'<br>On database:'.$in['database']),'error');
				}else{
					msg::success(('Execute successfully ON: '.$in['database']),'success');
				}
			}
		}
		
		json_out($in);
		return true;
	}


	function nylas_deactivate(){
		global $config;
		$ch = curl_init();
		
		//$token_id=$this->db_users->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$token_id=$this->db_users->field("SELECT token FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		//$account_id=$this->db_users->field("SELECT account_id FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
		$account_id=$this->db_users->field("SELECT account_id FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/oauth/revoke');

        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
    		$err=json_decode($put);
    		msg::error($err->message,"error");
    		json_out($in);
	    }else{
	    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		    curl_setopt($ch, CURLOPT_USERPWD, $config['nylas_app_secret'].':');
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/a/'.$config['nylas_app_id'].'/accounts/'.$account_id.'/downgrade');

	        $put = curl_exec($ch);
		    $info = curl_getinfo($ch);

		    if($info['http_code']>300 || $info['http_code']==0){
	    		$err=json_decode($put);
	    		msg::error($err->message,"error");
	    		json_out($in);
		    }else{
		    	//$this->db_users->query("UPDATE nylas_data SET active='0' WHERE user_id='".$_SESSION['u_id']."' ");
		    	$this->db_users->query("UPDATE nylas_data SET active= :active WHERE user_id= :user_id ",['active'=>'0','user_id'=>$_SESSION['u_id']]);
				msg::success ( gm("Changes have been saved."),'success');
				json_out($in);
		    }
	    }
	}


	function set_rights(&$in)
	{
		$is_shared = $this->db->query("SELECT * FROM customer_shared_calendars WHERE user_id='".$_SESSION['u_id']."' AND to_user='".$in['to_user_id']."'");
		if($is_shared->next())
		{
			$this->db->query("UPDATE customer_shared_calendars SET rights='".$in['right']."' WHERE share_calendar_id='".$is_shared->f('share_calendar_id')."'");
		}else
		{
			$this->db->query("INSERT INTO customer_shared_calendars SET
										  user_id='".$_SESSION['u_id']."',
										  to_user='".$in['to_user_id']."',
										  rights='".$in['right']."'");
		}
		msg::success(gm("Changes saved"),"success");
		json_out($in);
	}


	function sendValidationEmail($in){
		global $config;
		//$user_send = $this->db_users->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_send = $this->db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		
		$mail = new PHPMailer();
		$mail->CharSet = "UTF-8";
		$mail->WordWrap = 50;
		
		$in['e_message'] = stripslashes($in['e_message']);
		$body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');

		$def_email = $this->default_email();

    	$mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
		$mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
    	

    	$code = str_replace('/', '.',  openssl_encrypt(strrev(base64_encode($_SESSION['u_id'].'-'.$in['email'].'-'.time())),'AES-256-CBC','97uD7nDnFQ') );

		$validate_email = include(ark::$controllerpath . 'validate_email.php');
		$body=$validate_email;
	    $body=str_replace('[!FIRSTNAME!]',"".$user_send->f('first_name')."",$body);
	    $body=str_replace('[!LASTNAME!]',"".$user_send->f('last_name')."",$body);
	    $body=str_replace('[!LINK_VALIDATE!]',"<a href='".$config['site_url']."confirm/".$code."'>link</a>",$body);
		$mail->Subject = stripslashes(htmlspecialchars_decode(utf8_encode($mail_subject)));

/*		$subject='Email verification';

		$mail->Subject = $subject;
*/
		//$mail->MsgHTML(nl2br($body));
		
/*		$head = '<style>p {	margin: 0px; padding: 0px; }</style>';

      	$mail->IsHTML(true);
     	$body = "<p>Hi ".$in['first_name']." ".$in['last_name'].", </p>\n".
     	"<p>Please click on this <a href=\"".$config['site_url']."confirm/".$code."\">link<a> in order to confirm your email address </p>\n".
     	"<br><p>Thank you.</p>";*/
		//$this->db_users->query("UPDATE users SET change_email_token='".$code."' WHERE user_id='".$_SESSION['u_id']."' ");
		$this->db_users->query("UPDATE users SET change_email_token= :change_email_token WHERE user_id= :user_id ",['change_email_token'=>$code,'user_id'=>$_SESSION['u_id']]);
	   	$mail->MsgHTML($body);
	       

		$mail->AddAddress(trim($in['email']));
		
		$mail->Send();
		// console::log($mail);
		
		if($mail->IsError()){
			msg::error ( $mail->ErrorInfo,'error');
        	// json_out($in);
			return false;
		}

		if($mail->ErrorInfo){
			msg::notice( $mail->ErrorInfo,'notice');
		}

		
		msg::success( gm('Email validation sent').'.','success');

		return true;
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		$this->db->query("SELECT * FROM default_data WHERE type='quote_email' ");
		//if($this->db->move_next()){
		if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
			$array['from']['name'] = $this->db->f('default_name');
		}
		return $array;
	}

	function sendNotificationEmail($in){
		global $config;
		//$user_send = $this->db_users->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_send = $this->db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		
		$mail = new PHPMailer();
		$mail->CharSet = "UTF-8";
		$mail->WordWrap = 50;
		
		
		$body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');

		$def_email = $this->default_email();

    	$mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
		$mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
    	

		$validate_email = include(ark::$controllerpath . 'verification_email.php');
		$body=$validate_email;
	    $body=str_replace('[!FIRSTNAME!]',"".$user_send->f('first_name')."",$body);
	    $body=str_replace('[!LASTNAME!]',"".$user_send->f('last_name')."",$body);
	    $mail->Subject = stripslashes(htmlspecialchars_decode(utf8_encode($mail_subject)));

		//$mail->MsgHTML(nl2br($body));
		
		/*$head = '<style>p {	margin: 0px; padding: 0px; }</style>';

      	$mail->IsHTML(true);
     	$body = "<p>Hi ".$in['first_name']." ".$in['last_name'].", </p>\n".
     	"<p>There was a requtest to change your email in your Akti account.</p>\n".
     	"<br><p>If you are not aware of this please log into your Akti account and change your password.</p>";
		
	    $mail->Body    = $head.$body;*/
	    $mail->MsgHTML($body);
	       
		$mail->AddAddress(trim($user_send->f('email')));
		
		$mail->Send();
		// console::log($mail);
		
		if($mail->IsError()){
			// msg::error ( $mail->ErrorInfo,'error');
        	// json_out($in);
			// return false;
		}

		if($mail->ErrorInfo){
			// msg::notice( $mail->ErrorInfo,'notice');
		}

		
		msg::success( gm('Email validation sent').'.','success');

		return true;
	}

	function start_aplication(&$in)
	{
		
		global $config;
		/*if(!empty($in['emails'])){
			$in['check'] =   $in['emails'];
	        $in['check']= explode( ';', $in['check']);
	        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
	        $in['valid_check'] = true;
	        foreach ($in['check'] as $key => $value) {
	        	if(!preg_match($regex, $value)){
	                  $in['valid_check'] = false;
	        	}
	        }
			if($in['valid_check']==false){
				msg::error(gm("Please separate emails by ;"),"error");
				json_out($in);
				
			}
		}*/
		

		if($in['email']){

			$in['database'] = DATABASE_NAME;

			$name_user = $this->db_users->query("SELECT first_name,last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

			foreach ($in['email'] as $key => $value) {
				$in['username']=$value;
				$in['email'] = $value;
				$lang_code=$_SESSION['l'];
				$mail = new PHPMailer();
				$mail->WordWrap = 50;
			  	$fromMail='noreply@akti.com';
		 	 	$mail->SetFrom($fromMail, 'Akti');	

			 	$forgot_email = include(ark::$controllerpath . 'forgot_email.php');
				$body=$forgot_email;
	         	$body=str_replace('[!FIRSTNAME!]',"".$name_user->f('first_name')."",$body);
	         	$body=str_replace('[!LASTNAME!]',"".$name_user->f('last_name')."",$body);
	            $body=str_replace('[!LINK_VALIDATE!]',"<a href='".$site."'>'".$site."'</a>",$body);
				$mail->Subject = $mail_subject;
			 	$mail->MsgHTML($body);
			  	$mail->AddAddress($value);
			 	$mail->Send();
			}
		}	

		$aditional_plus=array(
			array(
				'txt'		=> gm('CRM'),
				'price'		=> 20,
				'model'		=> 'crm_adv',
				'crm_adv'	=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Products'),
				'price'		=> 10,
				'model'		=> 'products',
				'products'	=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Quotes'),
				'price'		=> 10,
				'model'		=> 'quotes_adv',
				'quotes_adv'=>false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Orders'),
				'price'		=> 10,
				'model'		=> 'orders',
				'orders'	=> false,
				'credential'=> 6
				),
			array(
				'txt'		=> gm('Stocks'),
				'price'		=> 30,
				'model'		=> 'stocks',
				'stocks'	=> false,
				'credential'=> 16
				),
			array(
				'txt'		=> gm('Cashregister'),
				'price'		=> 50,
				'model'		=> 'cashregister',
				'cashregister'	=> false,
				'credential'=> 15
				),
			array(
				'txt'		=> gm('Projects'),
				'price'		=> 20,
				'model'		=> 'projects',
				'projects'	=> false,
				'credential'=> 3
				),
			array(
				'txt'		=> gm('Interventions'),
				'price'		=> 20,
				'model'		=> 'intervention',
				'intervention'=>false,
				'credential'=> 13
				),
			array(
				'txt'		=> gm('Contrats'),
				'price'		=> 20,
				'model'		=> 'contracts',
				'contracts'	=> false,
				'credential'=> 11
				),
			array(
				'txt'		=> gm('SEPA'),
				'price'		=> 10,
				'model'		=> 'sepa',
				'sepa'		=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Reports'),
				'price'		=> 10,
				'model'		=> 'report_adv',
				'report_adv'=> false,
				'credential'=> 0
				),
			array(
                'txt'       => gm('API and webhooks'),
                'price'     => 0,
                'model'     => 'api_webhooks',
                'api_webhooks'=> false,
                'credential'=> 0,
                ),
            array(
                'txt'       => gm('Market place'),
                'price'     => 0,
                'model'     => 'marketplace',
                'marketplace'=> false,
                'credential'=> 0,
                ),
            array(
                'txt'       => gm('Purchase Orders'),
                'price'     => 0,
                'model'     => 'p_orders',
                'p_orders'    => false,
                'credential'=> 14
                ),
		);

		if($in['crm']){
			$credentials.='1;';
		}
		if($in['invoice']){
			$credentials.='4;';
		}
		if($in['quote']){
			$credentials.='5;';
		}
		if($in['catalogue']){
			$credentials.='12;';
		}
		if($in['project']){
			$credentials.='3;19;';
			$aditional_plus[6]['projects']=true;
		}
		if($in['order']){
			$credentials.='6;';
			$aditional_plus[3]['orders']=true;
		}
		if($in['contract']){
			$credentials.='11;';
			$aditional_plus[8]['contracts']=true;
		}
		if($in['intervention']){
			$credentials.='13;17;';
			$aditional_plus[7]['intervention']=true;
		}
		if($in['cash']){
			$credentials.='15;';
			//$this->db_users->query("UPDATE users SET register='true' WHERE user_id='".$_SESSION['u_id']."'");
			$this->db_users->query("UPDATE users SET register= :register WHERE user_id= :user_id",['register'=>'true','user_id'=>$_SESSION['u_id']]);
			$aditional_plus[5]['cashregister']=true;
		}
		if($in['stock']){
			$credentials.='16;14;';
			$aditional_plus[4]['stocks']=true;
		}
		$credentials.='10;7;18;';
		if($in['catalogueplus']){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ADV_PRODUCT'");
		}
		if($in['webshop']){
			//$this->db_users->query("UPDATE users SET webshop='true' WHERE user_id='".$_SESSION['u_id']."'");
			$this->db_users->query("UPDATE users SET webshop= :webshop WHERE user_id= :user_id",['webshop'=>'true','user_id'=>$_SESSION['u_id']]);
		}

		$credentials=rtrim($credentials,';');

		$aditional_basic=array(
			array(
				'txt'		=> gm('CRM'),
				'price'		=> 20,
				'model'		=> 'crm_adv',
				'crm_adv'	=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Products'),
				'price'		=> 10,
				'model'		=> 'products',
				'products'	=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Quotes'),
				'price'		=> 10,
				'model'		=> 'quotes_adv',
				'quotes_adv'=>false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Orders'),
				'price'		=> 10,
				'model'		=> 'orders',
				'orders'	=> false,
				'credential'=> 6
				),
			array(
				'txt'		=> gm('Stocks'),
				'price'		=> 30,
				'model'		=> 'stocks',
				'stocks'	=> false,
				'credential'=> 16
				),
			array(
				'txt'		=> gm('Cashregister'),
				'price'		=> 50,
				'model'		=> 'cashregister',
				'cashregister'	=> false,
				'credential'=> 15
				),
			array(
				'txt'		=> gm('Timetracker'),
				'price'		=> 10,
				'model'		=> 'timetracker',
				'timetracker'	=> false,
				'credential'=> 19
				),
			array(
				'txt'		=> gm('Interventions'),
				'price'		=> 20,
				'model'		=> 'intervention',
				'intervention'=>false,
				'credential'=> 13
				),
			array(
				'txt'		=> gm('Contrats'),
				'price'		=> 20,
				'model'		=> 'contracts',
				'contracts'	=> false,
				'credential'=> 11
				),
			array(
				'txt'		=> gm('SEPA'),
				'price'		=> 10,
				'model'		=> 'sepa',
				'sepa'		=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Reports'),
				'price'		=> 10,
				'model'		=> 'report_adv',
				'report_adv'=> false,
				'credential'=> 0
				),
			array(
                'txt'       => gm('API and webhooks'),
                'price'     => 0,
                'model'     => 'api_webhooks',
                'api_webhooks'=> false,
                'credential'=> 0,
                ),
            array(
                'txt'       => gm('Market place'),
                'price'     => 0,
                'model'     => 'marketplace',
                'marketplace'=> false,
                'credential'=> 0,
                ),
            array(
                'txt'       => gm('Purchase Orders'),
                'price'     => 0,
                'model'     => 'p_orders',
                'p_orders'    => false,
                'credential'=> 14
                ),
		);

		if($in['crm'] && $in['version']==2){
			$aditional_plus[0]['crm_adv']=true;
		}
		if($in['catalogueplus']){
			$aditional_plus[1]['products']=true;
		}
		if($in['quote'] && $in['version']==2){
			$aditional_plus[2]['quotes_adv']=true;
		}

		if($in['stockManageOption'] && !$in['serviceOption']){
			//growth stock plan
			$this->db_users->query("UPDATE users SET pricing_plan_id = '3', base_plan_id = '3', users = '2' WHERE user_id='".$_SESSION['u_id']."' ");
		}
		if(!$in['stockManageOption'] && $in['serviceOption']){
			//growth services
			$this->db_users->query("UPDATE users SET pricing_plan_id = '4', base_plan_id = '4', users = '5' WHERE user_id='".$_SESSION['u_id']."' ");
		}
		if($in['stockManageOption'] && $in['serviceOption']){
			//advanced account
			$this->db_users->query("UPDATE users SET pricing_plan_id = '5', base_plan_id = '5', users = '5' WHERE user_id='".$_SESSION['u_id']."' ");
		}
		$nr_users= '2';
		if($in['version']==1){
			//$this->db_users->query("UPDATE users SET credentials='1;4;5;7;10;12',company_size_id='".$in['company_id']."',activity_id='".$in['activity_id']."',credentials_serial='".serialize($aditional_basic)."',users='2' WHERE user_id='".$_SESSION['u_id']."'");
			$this->db_users->query("UPDATE users SET credentials= :credentials,company_size_id= :company_size_id,activity_id= :activity_id,credentials_serial= :credentials_serial,users= :users WHERE user_id= :user_id",['credentials'=>'1;4;5;7;10;12','company_size_id'=>$in['company_id'],'activity_id'=>$in['activity_id'],'credentials_serial'=>serialize($aditional_basic),'users'=>'2','user_id'=>$_SESSION['u_id']]);
		}else{
			//$this->db_users->query("UPDATE users SET credentials='".$credentials."',company_size_id='".$in['company_id']."',activity_id='".$in['activity_id']."',credentials_serial='".serialize($aditional_plus)."',users='2' WHERE user_id='".$_SESSION['u_id']."'");
			$hubspot_vid=$this->db->field("SELECT value FROM settings WHERE `constant_name`='HUBSPOT_VID' ");

			if(!$hubspot_vid){
				$credentials = $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
				$selected_plan = $this->db_users->field("SELECT pricing_plan_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
				$cred = explode(';', $credentials);
				$aditional =get_aditional();

				$aditional_plus[0]['crm_adv']=true;
				$aditional_plus[2]['quotes_adv']=true;
				if(in_array($selected_plan, ['3','4','5'])){
			 		$aditional[1]['products']=true;
			 		$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ADV_PRODUCT'");
			 	}
				foreach ($aditional as $key => $value) {

					if($value['credential'] && in_array($value['credential'], $cred)){
						$aditional[$key][$value['model']]=true;
					}	
				}

				$nr_users = $this->db_users->field("SELECT users FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
				$aditional_plus = $aditional;
			}

		if($in['stockManageOption'] && !$in['serviceOption']){
			//growth stock plan
			$this->db_users->query("UPDATE users SET pricing_plan_id = '3', base_plan_id = '3', users = '2' WHERE user_id='".$_SESSION['u_id']."' ");
			$aditional_plus[0]['crm_adv']=true;
			$aditional_plus[1]['products']=true;
			$aditional_plus[2]['quotes_adv']=true;
			$aditional_plus[3]['orders']=true;
			$aditional_plus[4]['stocks']=true;
			$aditional_plus[12]['marketplace']=true;
			$aditional_plus[13]['p_orders']=true;
			$credentials = $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
			$credentials.=';6;16;14';

		}
		if(!$in['stockManageOption'] && $in['serviceOption']){
			//growth services
			$this->db_users->query("UPDATE users SET pricing_plan_id = '4', base_plan_id = '4', users = '5' WHERE user_id='".$_SESSION['u_id']."' ");
			$aditional_plus[0]['crm_adv']=true;
			$aditional_plus[1]['products']=true;
			$aditional_plus[2]['quotes_adv']=true;
			$aditional_plus[7]['intervention']=true;
			$aditional_plus[8]['contracts']=true;
			$aditional_plus[12]['marketplace']=true;
			$credentials = $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
			$credentials.=';11;13;17';

		}
		if($in['stockManageOption'] && $in['serviceOption']){
			//advanced account
			$this->db_users->query("UPDATE users SET pricing_plan_id = '5', base_plan_id = '5', users = '5' WHERE user_id='".$_SESSION['u_id']."' ");
			$aditional_plus[0]['crm_adv']=true;
			$aditional_plus[1]['products']=true;
			$aditional_plus[2]['quotes_adv']=true;
			$aditional_plus[3]['orders']=true;
			$aditional_plus[4]['stocks']=true;
			$aditional_plus[7]['intervention']=true;
			$aditional_plus[8]['contracts']=true;
			$aditional_plus[13]['api_webhooks']=true;
			$aditional_plus[12]['marketplace']=true;
			$aditional_plus[13]['p_orders']=true;
			$credentials = $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
			$credentials.=';6;16;14;11;13;17';
		}

			$this->db_users->query("UPDATE users SET credentials= :credentials,company_size_id= :company_size_id,activity_id= :activity_id,credentials_serial= :credentials_serial,users= :users WHERE user_id= :user_id",['credentials'=>$credentials,'company_size_id'=>$in['company_id'],'activity_id'=>$in['activity_id'],'credentials_serial'=>serialize($aditional_plus),'users'=>$nr_users,'user_id'=>$_SESSION['u_id']]);
			

		}	
		

	 $vat_countrys=array(
	        19 => array(20,13,10,0),
	        26 => array(21,12,6,0),
	        79 => array(20,10,5.5,2.1,0),
	        86 => array(19,7,0),
	        111=> array(23,13.5,9,4.8,0),
	        18 => array(22,10,5,4,0),
	        132=> array(17,14,8,5,3),
	        159=> array(21,6,0),
	        17 => array(19,9,5,0),
	        210=> array(21,10,4,0),
	        2  => array(20,5,0),
	        38 => array(20,9,0),
	        59 => array(25,15,5,0),
	        61 => array(19,9,5),
	        62 => array(21,15,10,0),
	        63 => array(25,0),
	        73 => array(20,9,0),
	        78 => array(24,14,10,0),
	        89 => array(24,13,6,0),
	        104=> array(27,18,5,0),
	        125=> array(21,12,0),
	        131=> array(21,9,5,0),
	        140=> array(18,7,5,0),
	        183=> array(23,8,5,0),
	        184=> array(23,13,6,0),
	        203=> array(20,10,0),
	        204=> array(22,9.5,0),
	        216=> array(25,12,6,0),
	    );
	 foreach ($vat_countrys as $key => $value) {
	 	if($key==$in['country_id']){
	 		foreach ($value as $keys => $values) {
	 			$value_exists = $this->db->field("SELECT vat_id FROM vats WHERE value='".$values."' ");
	 			if(empty($value_exists)){
	 				$this->db->query("INSERT INTO vats SET value='".$values."' ");	
	 			} 
	 		}
	 		$this->db->query("UPDATE settings SET value='".$value[0]."' WHERE constant_name='ACCOUNT_VAT'");
	 	}
	 }

	 $this->db->query("CREATE TABLE IF NOT EXISTS `vat_new` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name_id` varchar(256) NOT NULL,
			  `description` varchar(256) NOT NULL,
			  `value` decimal(10,2) NOT NULL,
			  `vat_id` int(11) NOT NULL,
			  `vat_regime_id` int(11) NOT NULL,
			  `no_vat` tinyint(3) NOT NULL,
			   `default` tinyint(3) NOT NULL,
			   `regime_type` TINYINT( 3 ) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=10000 ");

	$acc_vat=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT' ");
	if(!is_null($acc_vat)){
		$acc_vat=str_replace(',','.',$acc_vat);
	}
	$vat_copy=$this->db->query("SELECT * FROM vats");
	while($vat_copy->next()){
		$is_def=0;
		if($acc_vat && $acc_vat==$vat_copy->f('value')){
			$is_def=1;
		}
		$this->db->query("INSERT INTO vat_new SET
							description='".$vat_copy->f('value')."',
							vat_id='".$vat_copy->f('vat_id')."',
							`default`='".$is_def."',
							`regime_type`='1' ");	
	}
	$vat_regim = array(
		array('id' => "2" , 
			'name' => "Intra-EU" , 
			'regime_type'=>'2',
			'legal_en'=>"VAT exemption under Article 39 bis of the VAT Code.", 
			'legal_fr'=>"Exemption de TVA sous couvert de l\'article 39 bis du CTVA.", 
			'legal_nl'=>"Vrijgesteld van BTW overeenkomstig art. 39bis W. BTW", 
			'legal_de'=>""),
		array('id' => "3" , 
			'name' => "Export", 
			'regime_type'=>'3',
			'legal_en'=>"", 
			'legal_fr'=>"", 
			'legal_nl'=>"", 
			'legal_de'=>""),
		array('id' => "4" , 
			'name' => "Contracting Partner", 
			'regime_type'=>'3',
			'legal_en'=>"VAT recharge", 
			'legal_fr'=>"Autoliquidation de la TVA", 
			'legal_nl'=>"Btw verlegd" , 
			'legal_de'=>""),
	);
	foreach($vat_regim as $key=>$value){
		$no_vat=$value['id']=='3' ? '1' : '0';
		$vat_id = $this->db->insert("INSERT INTO vat_new SET
							description='".$value['name']."',
							no_vat='".$no_vat."',
							vat_regime_id='".$value['id']."',
							`regime_type`='".$value['regime_type']."' ");
		if($value['legal_en']){
			$this->db->query("INSERT INTO vat_lang SET id='".$vat_id."',
										en='".addslashes($value['legal_en'])."', 
										fr='".addslashes($value['legal_fr'])."', 
										nl='".addslashes($value['legal_nl'])."', 
										de='".addslashes($value['legal_de'])."' ");
		}
	}

		if($in['crm'] && $in['version']==2){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ADV_CRM'");
		}else{
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ADV_CRM'");
		}
		if($in['quote'] && $in['version']==2){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ADV_QUOTE'");
		}else{
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ADV_QUOTE'");
		}
		/*$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ADV_PRODUCT'");*/
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ADV_STOCK'");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ADV_SEPA'");
		$this->db->query("UPDATE settings SET value='".addslashes($in['address'])."' WHERE constant_name='ACCOUNT_BILLING_ADDRESS'");
		if($in['company']!=''){
			$this->db->query("UPDATE settings SET value='".$in['company']."' WHERE constant_name='ACCOUNT_COMPANY'");
			//$this->db_users->query("UPDATE users SET ACCOUNT_COMPANY='".utf8_decode($in['company'])."' WHERE user_id='".$_SESSION['u_id']."' ");
			$this->db_users->query("UPDATE users SET ACCOUNT_COMPANY= :a WHERE user_id= :user_id ",['a'=>utf8_decode($in['company']),'user_id'=>$_SESSION['u_id']]);
		}	
		if(aktiUser::get('is_easy_invoice_billtobox')){
			$this->db_users->query("UPDATE users SET
					first_name 	    = :first_name,
					last_name 		= :last_name
					WHERE user_id= :user_id",
				[	'first_name' 	    => htmlspecialchars($in['first_name']),
					'last_name' 		=> htmlspecialchars($in['last_name']),
					'user_id'		=> $_SESSION['u_id']]
			);
		}
		
		$this->db->query("UPDATE settings SET long_value='".addslashes($in['address'])."' WHERE constant_name='ACCOUNT_DELIVERY_ADDRESS'");
		$this->db->query("UPDATE settings SET long_value='".addslashes($in['address'])."' WHERE constant_name='ACCOUNT_BILLING_ADDRESS'");
		$this->db->query("UPDATE settings SET value='".$in['zip']."' WHERE constant_name='ACCOUNT_BILLING_ZIP'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_BIC_CODE']."' WHERE constant_name='ACCOUNT_BIC_CODE'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_IBAN']."' WHERE constant_name='ACCOUNT_IBAN'");
		$this->db->query("UPDATE settings SET value='".$in['zip']."' WHERE constant_name='ACCOUNT_DELIVERY_ZIP'");
		$this->db->query("UPDATE settings SET value='".addslashes($in['city'])."' WHERE constant_name='ACCOUNT_BILLING_CITY'");
		$this->db->query("UPDATE settings SET value='".addslashes($in['city'])."' WHERE constant_name='ACCOUNT_DELIVERY_CITY'");
		
		$this->db->query("UPDATE settings SET value='".$in['city']."' WHERE constant_name='ACCOUNT_DELIVERY_CITY'");
		$this->db->query("UPDATE settings SET value='".$in['country_id']."' WHERE constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
		$this->db->query("UPDATE settings SET value='".$in['country_id']."' WHERE constant_name='ACCOUNT_DELIVERY_COUNTRY_ID'");
		$this->db->query("UPDATE settings SET value='".$in['btw_nr']."' WHERE constant_name='ACCOUNT_VAT_NUMBER'");
		$this->db->query("UPDATE settings SET value='".$in['annual_id']."' WHERE constant_name='ACCOUNT_FISCAL_YEAR_START'");
		$this->db->query("UPDATE settings SET value='1' WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
		$this->db->query("INSERT INTO settings SET value='1', constant_name='USE_COMPANY_NUMBER' ");

		$this->db->query("UPDATE dispatch_stock_address SET zip='".$in['zip']."', city='".addslashes($in['city'])."', country_id='".$in['country_id']."', address='".addslashes($in['address'])."' WHERE address_id='1'");
/*		if($_SESSION['l']=='en'){
			$this->db->query("UPDATE pim_lang SET active='1', sort_order='1' WHERE language='English'");
		}elseif($_SESSION['l']=='fr'){
			$this->db->query("UPDATE pim_lang SET active='1', sort_order='1' WHERE language='French'");
			$this->db->query("UPDATE pim_lang SET active='0', sort_order='0' WHERE language='English'");
		}elseif($_SESSION['l']=='nl') {
			$this->db->query("UPDATE pim_lang SET active='1', sort_order='1' WHERE language='Dutch'");
			$this->db->query("UPDATE pim_lang SET active='0', sort_order='0' WHERE language='English'");
		}*/
		if(strpos($config['site_url'],'my.akti')){
		//Values for Gaetan account
			global $database_config;
				$database_new = array(
						'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
						'username' => 'admin',
						'password' => 'hU2Qrk2JE9auQA',
						'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
						//'database' => '32ada082_5cf4_1518_3aa09bca7f29'
				);
					
			$db_user_new = new sqldb($database_new);
			$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
			$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
			if($in['company']){
				$db_user_new->query("UPDATE customers SET name='".utf8_decode($in['company'])."' WHERE customer_id='".$select_customer."' ");
			}
			$address_company = $db_user_new->insert("INSERT INTO customer_addresses SET
					customer_id='".$select_customer."',
					country_id='".$in['country_id']."',
					city='".addslashes(utf8_decode($in['city']))."',
					address='".addslashes(utf8_decode($in['address']))."',
					zip='".utf8_decode($in['zip'])."',
					billing = '1',
					is_primary = '1'
			");
			if(aktiUser::get('is_easy_invoice_billtobox')){
				$db_user_new->query("UPDATE customer_contacts SET
						firstname='".htmlspecialchars($in['first_name'])."',
						lastname='".htmlspecialchars($in['last_name'])."'
						WHERE contact_id='".$select_contact."'
				");
			}
		//
		}

		$this->db->query("UPDATE default_data SET default_name=(SELECT value FROM settings WHERE constant_name='ACCOUNT_COMPANY'), value=(SELECT value FROM settings WHERE constant_name='ACCOUNT_EMAIL') WHERE default_data.type='quote_email' OR default_data.type='order_email' OR default_data.type='p_order_email' OR default_data.type='invoice_email' ");	
		$this->db->query("UPDATE settings SET value='1' WHERE constant_name='WIZZARD_COMPLETE'");

		$user = $this->db_users->query("SELECT database_name, pricing_plan_id, base_plan_id,email,lang_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

		$database = DATABASE_NAME;
		if(!$database){
			$database = $user->f('database_name');
		}

		if(strpos($config['site_url'],'my.akti') && aktiUser::get('is_easy_invoice_billtobox')){
			//$active_c_lang_id=array('1'=>'40','2'=>'41','3'=>'42');
			$active_c_lang_id=array('1'=>'Dutch','2'=>'English','3'=>'French');
			//send data to active campaign
	        syncDataToActiveCampaign($database,array(
	        	'account_name' 		=> $in['company'],
	        	'contact_email' 	=> $user->f('email'),
	        	'contact_first_name' => $in['first_name'],
	        	'contact_last_name'  => $in['last_name'],
	        	'contact_lang'	     => array_key_exists($user->f('lang_id'), $active_c_lang_id) ? $active_c_lang_id[$user->f('lang_id')] : 'English'
	        ));
	        //end data send to active campaign
		}	

		$subscription_type = get_name_subscription_plan_ac($user->f('pricing_plan_id'));

		doManageLog('',$database,array( array("property"=>'onboarding_completed',"value"=>'Yes'), array("property"=>'subscription_type',"value"=>$subscription_type) ));	

		msg::success(gm("Changes saved"),"success");
			return true;
		
	}
	function check_email(&$in)
	{
		$in['check'] =   $in['emails'];
        $in['check']= explode( ';', $in['check']);
        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
        $in['valid_check'] = true;
        foreach ($in['check'] as $key => $value) {
        	if(!preg_match($regex, $value)){
                  $in['valid_check'] = false;
        	}
        }
		/*if($in['valid_check']==false){
			msg::error(gm("Please separate emails by ;"),"error");
			json_out($in);
			return true;
		}else{
			json_out($in);
			return true;
		}*/
	}
	function change_language(&$in)
	{
		$_SESSION['l']=$in['lang'];
		setcookie("l",$in['lang'],time()+(60*60*24*356),'/');
		$in['default_pag']='board';
		msg::success(gm("Changes saved"),"success");
		json_out($in);
		return true;
	}
	function add_user_board(&$in){
		global $config;

		$in['address'] = explode( ',', $in['emails'] );
		$in['database'] = DATABASE_NAME;
		
		foreach ($in['address'] as $key => $value) {
			$in['email'] = $value;
			$lang_code=$_SESSION['l'];
			$mail = new PHPMailer();
			$mail->WordWrap = 50;
		  	$fromMail='noreply@akti.com';
	 	 	$mail->SetFrom($fromMail, 'Akti');	
		 	$forgot_email = include(ark::$controllerpath . 'forgot_email.php');
			$body=$forgot_email;
			$subject= 'You have been invited to akti';
			$mail->Subject = $subject;
		 	$mail->MsgHTML($body);
		  	$mail->AddAddress($value);
		 	$mail->Send();
	
		}
	}
	function add_user(&$in)
	{	

		$this->db_users->query("select user_id,database_name, migrated from users where database_name='".DATABASE_NAME."' and main_user_id=0");
		$this->db_users->move_next();
		$main_user_id=$this->db_users->f('user_id');
		$migrated = $this->db_users->f('migrated');

		$u_role = '2';
		$pr_admin = '0';
		$in['user_type']=2;
		$cred = 8;

		$payment_type = $this->db_users->field("SELECT payment_type FROM users WHERE main_user_id='0' AND database_name='".DATABASE_NAME."' limit 1");
		/*$in['user_id'] = $this->db_users->insert("INSERT INTO users SET
                                                    main_user_id  	= '".$main_user_id."',
                                                    user_role     	= '".$u_role."',
                                                    database_name 	= '".DATABASE_NAME."',
                                                    first_name 		= '".utf8_decode($in['first_name'])."',
                                                    last_name 		= '".utf8_decode($in['last_name'])."',
                                                    payment_type	= '".$payment_type."',
													username  		= '".$in['username']."',
													password 	    = '".md5($in['password'])."',
													email 	    	= '".$in['email']."',
													country_id 		= '".$in['country_id']."',
													lang_id 		= '1',
													group_id		= '".$in['group_id']."',
													credentials		= '".$cred."',
													user_type		= '".$in['user_type']."',
													h_rate			= '".return_value($in['h_rate'])."',
													h_cost 			= '".return_value($in['h_cost'])."',
													daily_rate			= '".return_value($in['daily_rate'])."',
													daily_cost 			= '".return_value($in['daily_cost'])."',
													migrated		= '".$migrated."'");*/
		$in['user_id'] = $this->db_users->insert("INSERT INTO users SET
                                                    main_user_id  	= :main_user_id,
                                                    user_role     	= :user_role,
                                                    database_name 	= :d,
                                                    first_name 		= :first_name,
                                                    last_name 		= :last_name,
                                                    payment_type	= :payment_type,
													username  		= :username,
													password 	    = :password,
													email 	    	= :email,
													country_id 		= :country_id,
													lang_id 		= :lang_id,
													group_id		= :group_id,
													credentials		= :credentials,
													user_type		= :user_type,
													h_rate			= :h_rate,
													h_cost 			= :h_cost,
													daily_rate		= :daily_rate,
													daily_cost 		= :daily_cost,
													migrated		= :migrated",
												[	'main_user_id'  	=> $main_user_id,
                                                    'user_role'     	=> $u_role,
                                                    'd' 				=> DATABASE_NAME,
                                                    'first_name' 		=> utf8_decode($in['first_name']),
                                                    'last_name' 		=> utf8_decode($in['last_name']),
                                                    'payment_type'		=> $payment_type,
													'username'  		=> $in['username'],
													'password' 	    	=> md5($in['password']),
													'email' 	    	=> $in['email'],
													'country_id' 		=> $in['country_id'],
													'lang_id' 			=> '1',
													'group_id'			=> $in['group_id'],
													'credentials'		=> $cred,
													'user_type'			=> $in['user_type'],
													'h_rate'			=> return_value($in['h_rate']),
													'h_cost' 			=> return_value($in['h_cost']),
													'daily_rate'		=> return_value($in['daily_rate']),
													'daily_cost' 		=> return_value($in['daily_cost']),
													'migrated'			=> $migrated]
											);													

		//$this->db_users->query("INSERT INTO users_settings SET user_id= '".$in['user_id']."' ");
		$this->db_users->insert("INSERT INTO users_settings SET user_id= :user_id ",['user_id'=>$in['user_id']]);
		//$this->db_users->query("INSERT INTO user_meta SET name='project_admin', value='".$pr_admin."', user_id='".$in['user_id']."' ");
		$this->db_users->insert("INSERT INTO user_meta SET name= :name, value= :value, user_id= :user_id ",['name'=>'project_admin','value'=>$pr_admin,'user_id'=>$in['user_id']]);
		$globfo = $this->db_users->query("select * from user_info where user_id='".$main_user_id."' ");
		$globfo->next();
		//$this->db_users->query("INSERT INTO user_info SET user_id='".$in['user_id']."', start_date='".time()."', is_trial='".$globfo->f('is_trial')."', pricing_type='".$globfo->f('pricing_type')."', end_date='".$globfo->f('end_date')."' ");
		$this->db_users->insert("INSERT INTO user_info SET user_id= :user_id, start_date= :start_date, is_trial= :is_trial, pricing_type= :pricing_type, end_date= :end_date ",['user_id'=>$in['user_id'],'start_date'=>time(),'is_trial'=>$globfo->f('is_trial'),'pricing_type'=>$globfo->f('pricing_type'),'end_date'=>$globfo->f('end_date')]);

		msg::success ( gm("User added"),'success');
		return true;
	}
	function add_group(&$in){
		$in['group_id'] = $this->db->insert("INSERT INTO groups SET name='".$in['name']."' ");
		//		$this->db_users->query("UPDATE users SET group_id='".$in['group_id']."' WHERE user_id='".$in['admin_id']."'");
		if($in['credential']){
			foreach ($in['credential'] as $key => $value ){
				$credential .= $value.';';
			}
			$credential = trim($credential,';');
			$this->db->query("UPDATE groups SET credentials='".$credential."' WHERE group_id='".$in['group_id']."'");
		}
		msg::success ( gm('Group added'),'success');
		return true;
	}
	function upload_board(&$in)
	{

		$response=array();

		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME.'/';
			$in['name'] = 'logo_img_'.DATABASE_NAME.'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type

			$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				$c = move_uploaded_file($tempFile,$targetFile);
					
					global $database_config;
					$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
					);
					$db_upload = new sqldb($database_2);

				foreach (array('ACCOUNT_LOGO','ACCOUNT_LOGO_QUOTE','ACCOUNT_LOGO_ORDER','ACCOUNT_LOGO_WEBSHOP','ACCOUNT_LOGO_PO_ORDER','ACCOUNT_LOGO_TIMESHEET','ACCOUNT_MAINTENANCE_LOGO') as $constant){
					$db_upload->query("UPDATE settings SET
			                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
			                           long_value=1
			                           WHERE constant_name='".$constant."'");
				}
				
				$size = getimagesize("../../upload/".$in['folder']."/".$in['name']);
		    	$ratio = 250 / 77;

				if($size[0]/$size[1] > $ratio ){
					$attr = 'width="250"';
				}else{
					$attr = 'height="77"';
				}
				$response['success'] = $attr;
				msg::success($attr,'success');
				// echo json_encode($response);
			} else {
				$response['error'] = gm('Invalid file type.');
				// echo json_encode($response);
				msg::error($attr,'error');
			}
		}
		return true;
	}
	function removeLogo(&$in){	
		global $database_config;
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => DATABASE_NAME,
		);
		$db_upload = new sqldb($database_2);
		foreach (array('ACCOUNT_LOGO','ACCOUNT_LOGO_QUOTE','ACCOUNT_LOGO_ORDER','ACCOUNT_LOGO_WEBSHOP','ACCOUNT_LOGO_PO_ORDER','ACCOUNT_LOGO_TIMESHEET','ACCOUNT_MAINTENANCE_LOGO') as $constant){
			$file_url=$db_upload->field("SELECT value FROM settings WHERE constant_name='".$constant."'");
			if(file_exists($file_url)){
				unlink($file_url);
			}
			$db_upload->query("UPDATE settings SET value = '', long_value=1 WHERE constant_name='".$constant."'");
		}
		$in['default_upload_logo']=true;
		msg::success(gm('Logo unset'),'success');
		return true;
	}
	function upload_board1(&$in)
	{

		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'][0];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME.'/';
			$in['name'] = 'logo_img_'.DATABASE_NAME.'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type

			$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
			$fileParts = pathinfo($_FILES['file']['name'][0]);
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
					global $database_config;
					$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
					);
					$db_upload = new sqldb($database_2);

				foreach (array('ACCOUNT_LOGO','ACCOUNT_LOGO_QUOTE','ACCOUNT_LOGO_ORDER','ACCOUNT_LOGO_WEBSHOP','ACCOUNT_LOGO_PO_ORDER','ACCOUNT_LOGO_TIMESHEET','ACCOUNT_MAINTENANCE_LOGO') as $constant){
					$db_upload->query("UPDATE settings SET
			                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
			                           long_value=1
			                           WHERE constant_name='".$constant."'");
				}
				$size = getimagesize("../../upload/".$in['folder']."/".$in['name']);
		    	$ratio = 250 / 77;

				if($size[0]/$size[1] > $ratio ){
					$attr = 'width="250"';
				}else{
					$attr = 'height="77"';
				}
				$response['success'] = $attr;
				msg::success($attr,'success');
				// echo json_encode($response);
			} else {
				$response['error'] = gm('Invalid file type.');
				// echo json_encode($response);
				msg::error($attr,'error');
			}
		}
		return true;
	}

	function generateToken(){
		$is_token=true;
		while($is_token){
			$in['token'] = md5(uniqid(mt_rand(),true));
			//$user_token=$this->db_users->field("SELECT user_id FROM users WHERE zen_token='".$in['token']."' ");
			$user_token=$this->db_users->field("SELECT user_id FROM users WHERE zen_token= :zen_token ",['zen_token'=>$in['token']]);
			if(!$user_token){
				//$this->db_users->query("UPDATE users SET zen_token='".$in['token']."' WHERE user_id='".$_SESSION['u_id']."' ");
				$this->db_users->query("UPDATE users SET zen_token= :zen_token WHERE user_id= :user_id ",['zen_token'=>$in['token'],'user_id'=>$_SESSION['u_id']]);
				$is_token=false;
			}
		}
		json_out($in);
	}

	function setFactor(&$in){
		//$this->db_users->query("UPDATE users SET two_factor='".$in['value']."' WHERE user_id='".$_SESSION['u_id']."' ");
		$this->db_users->query("UPDATE users SET two_factor= :two_factor WHERE user_id= :user_id ",['two_factor'=>($in['value'] ? '1' : '0'),'user_id'=>$_SESSION['u_id']]);
		if($in['value']){
			$gauth=new GoogleAuthenticator();
			//$has_secret=$this->db_users->field("SELECT google_auth_secret FROM users google_auth_secret WHERE user_id='".$_SESSION['u_id']."' ");
			$has_secret=$this->db_users->field("SELECT google_auth_secret FROM users google_auth_secret WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
			if(!$has_secret){		
				$secretkey=$gauth->createSecret();
				//$this->db_users->query("UPDATE users SET google_auth_secret='".$secretkey."' WHERE user_id='".$_SESSION['u_id']."' ");
				$this->db_users->query("UPDATE users SET google_auth_secret= :google_auth_secret WHERE user_id= :user_id ",['google_auth_secret'=>$secretkey,'user_id'=>$_SESSION['u_id']]);
			}
			//$user_data=$this->db_users->query("SELECT email,google_auth_secret FROM users google_auth_secret WHERE user_id='".$_SESSION['u_id']."' ");
			$user_data=$this->db_users->query("SELECT email,google_auth_secret FROM users google_auth_secret WHERE user_id=:user_id ",['user_id'=>$_SESSION['u_id']]);
			$in['qr']=$gauth->getQRCodeGoogleUrl($user_data->f('email'),$user_data->f('google_auth_secret'),'Akti');
		}
		if(!$in['value'] && !$_SESSION['logedAsAdmin']){
    		setcookie("trust".$_SESSION['u_id'], "canci", time() - 60*60*24); 
			//$this->db_users->query("DELETE FROM user_trust WHERE user_id='".$_SESSION['u_id']."' ");
			$this->db_users->query("DELETE FROM user_trust WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		}
		json_out($in);
	}

	function attachAvatar(&$in){
		//$this->db_users->query("UPDATE users SET avatar='".$in['avatar']."' WHERE user_id='".$_SESSION['u_id']."' ");
		$this->db_users->query("UPDATE users SET avatar= :avatar WHERE user_id= :user_id ",['avatar'=>$in['avatar'],'user_id'=>$_SESSION['u_id']]);
		msg::success(gm('Avatar set'),'success');
		return true;
	}

	function attachCustomAvatar(&$in){
		$response=array();
	    if (!empty($_FILES)) {
	    	$size=$_FILES['Filedata']['size'];
	    	$fileImages=array('jpg','jpeg','gif','png');
	    	if(!defined('MAX_IMAGE_SIZE') &&  $size>512000 && in_array(strtolower($fileParts['extension']), $fileImages)){
	  			$response['error'] = gm('Size exceeds 500kb');
	  			$response['filename'] = $_FILES['Filedata']['name'];
				echo json_encode($response);
				exit();
	  	  }
	      $tempFile = $_FILES['Filedata']['tmp_name'];
	      $ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);
	      $in['name'] = 'tmp_file_'.time().'.'.$ext;
	      
	      $targetPath = INSTALLPATH.'upload/'.DATABASE_NAME.'/avatars';
	      if(!file_exists($targetPath)){
			mkdir($targetPath,0775,true);
		  }
	      
	      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
	      // Validate the file type
	      $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
	      $fileParts = pathinfo($_FILES['Filedata']['name']);
	      mkdir(str_replace('//','/',$targetPath), 0775, true);
	      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
	        if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE && in_array(strtolower($fileParts['extension']), $fileImages)){
	      		$image = new SimpleImage();
            	$image->load($_FILES['Filedata']['tmp_name']);
            	$image->scale(MAX_IMAGE_SIZE,$size);
            	$image->save($targetFile);
	      	}else{
	      		move_uploaded_file($tempFile,$targetFile);
	      	}
	        //$this->db_users->query("UPDATE users SET avatar='".$targetFile."' WHERE user_id='".$_SESSION['u_id']."' ");
	        $this->db_users->query("UPDATE users SET avatar= :avatar WHERE user_id= :user_id ",['avatar'=>$targetFile,'user_id'=>$_SESSION['u_id']]);
	        $response['success'] = gm('Avatar set');
	       	echo json_encode($response);
	      } else {
	        $response['error'] = gm('Invalid file type.');
	       	echo json_encode($response);
	      }
	      exit();
	    }
	}

	function removeAvatar(&$in){
		//$this->db_users->query("UPDATE users SET avatar='' WHERE user_id='".$_SESSION['u_id']."' ");
		$this->db_users->query("UPDATE users SET avatar= :avatar WHERE user_id= :user_id ",['avatar'=>'','user_id'=>$_SESSION['u_id']]);
		if(strpos($in['url'],'upload/')!==false){
			if(file_exists($in['url'])){
				unlink($in['url']);
			}
		}
		msg::success(gm('Avatar unset'),'success');
		return true;
	}

	function changePassword(&$in){
		if(!$this->changePassword_validate($in)){
			json_out($in);
		}
		$session = session_id();
        //$id = $this->db_users->field("SELECT activity_id FROM user_activity WHERE session_id='".$session."' AND user_id='".$_SESSION['u_id']."' ");
        $id = $this->db_users->field("SELECT activity_id FROM user_activity WHERE session_id= :session_id AND user_id= :user_id ",['session_id'=>$session,'user_id'=>$_SESSION['u_id']]);
		//$this->db_users->query("UPDATE users SET password = '".md5($in['new_password'])."'  WHERE user_id='".$_SESSION['u_id']."' ");
		$this->db_users->query("UPDATE users SET password = :password  WHERE user_id= :user_id ",['password'=>md5($in['new_password']),'user_id'=>$_SESSION['u_id']]);
		//$this->db_users->query("UPDATE user_info SET reset_request='' WHERE user_id='".$_SESSION['u_id']."' ");
		$this->db_users->query("UPDATE user_info SET reset_request= :reset_request WHERE user_id= :user_id ",['reset_request'=>'','user_id'=>$_SESSION['u_id']]);
		$this->db_users->query("UPDATE user_activity SET expired='1' WHERE user_id='".$_SESSION['u_id']."'  AND activity_id<>'".$id."' ");

		msg::success(gm('Password changed'),'success');
		json_out($in);
	}

	function changePassword_validate(&$in){
		$v=new validation($in);
		$v->field('password',gm('Old Password'),'required:min_length[6]');
		$v->field('new_password',gm('New Password'),'required:min_length[6]');
		$is_ok=$v->run();
		if($is_ok){
			//$correct_pass=$this->db_users->field("SELECT password FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$correct_pass=$this->db_users->field("SELECT password FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
			if(md5($in['password'])!=$correct_pass){
				msg::error(gm('Incorrect password'),'password');
				$is_ok=false;
			}
		}
		return $is_ok;
	}

}

?>