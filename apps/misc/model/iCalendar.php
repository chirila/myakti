<?php

class iCalendar
{
	private $db;
  	private $db_users;
  	private $database_name;

  function __construct($in, $dbase='', $title='', $start ='', $end ='', $description='', $location='',$uid='' ) {
    global $database_config,$config;

    if($dbase){
      $database_1 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $dbase,
        );
      $this->db = new sqldb($database_1);
      $this->database_name=$dbase;
    } else {
      $this->db = new sqldb();
      $this->database_name=DATABASE_NAME;
    }

    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
      );

    $this->db_users = new sqldb($db_config);

    $this->title =  $title;
    $this->event_start =  $start;
	$this->event_end = $end;
	$this->description = $description;
	$this->location = $location;
	$this->uid = $uid;

   // $this->event_start = DateTime::createFromFormat ( "Y-m-d H:i:s", $start );
	//$this->event_end = DateTime::createFromFormat ( "Y-m-d H:i:s", $end );

   }

   function get_iCalendar($in){
   	    $output = array();

	   	ark::loadLibraries(array('icalendar/zapcallib'));

		// create the ical object
		$icalobj = new ZCiCal();

		// create the event within the ical object
		$eventobj = new ZCiCalNode("VEVENT", $icalobj->curnode);

		// add title
		$eventobj->addNode(new ZCiCalDataNode("SUMMARY:" . $this->title));

		// add start date
		$eventobj->addNode(new ZCiCalDataNode("DTSTART:" . ZCiCal::fromSqlDateTime($this->event_start)));

		// add end date
		$eventobj->addNode(new ZCiCalDataNode("DTEND:" . ZCiCal::fromSqlDateTime($this->event_end)));

		// UID is a required item in VEVENT, create unique string for this event
		// Adding your domain to the end is a good way of creating uniqueness
		$uid = date('Y-m-d-H-i-s') . $this->uid;
		$eventobj->addNode(new ZCiCalDataNode("UID:" . $uid));

		// DTSTAMP is a required item in VEVENT
		$eventobj->addNode(new ZCiCalDataNode("DTSTAMP:" . ZCiCal::fromSqlDateTime()));


		$eventobj->addNode(new ZCiCalDataNode("LOCATION:" . ZCiCal::formatContent($this->location)));

		// Add description
		$eventobj->addNode(new ZCiCalDataNode("DESCRIPTION:" . ZCiCal::formatContent($this->description)));


	    $output['calendar'] = $icalobj->export();

	    return $output;


	  }

}
