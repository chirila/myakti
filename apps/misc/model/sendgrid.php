<?php
/**
 * undocumented class
 *
 * @package default
 * @author PM
 **/
class sendgrid
{
  private $db;
  private $db_users;
  private $database_name;

  function __construct($in, $pag, $field_n,$item_id, $body='', $def_email, $dbase='', $attach='') {
    global $database_config,$config;

    if($dbase){
      $database_1 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $dbase,
        );
      $this->db = new sqldb($database_1);
      $this->database_name=$dbase;
    } else {
      $this->db = new sqldb();
      $this->database_name=DATABASE_NAME;
    }

    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
      );

    $this->db_users = new sqldb($db_config);
    $this->config = $config;
    $this->pag = $pag;
    $this->field_n = $field_n;
    $this->item_id = $item_id;
    $this->message_body = $body;
    $this->default_email = $def_email;
    $this->multiple_ids ='';
    $this->account_send_email = (defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL == 1)? 1:0;
    $this->attach = $attach;
    $this->account_company = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_COMPANY' ");
    $this->account_vat_number = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT_NUMBER' ");

    switch ($this->pag) {
      case 'invoice': 
        $this->table = 'tblinvoice';
        $this->id_name = 'id';
        $this->document_name = gm('Invoice');
        $this->document_name_logging = '{l}Invoice{endl}';
        $this->pdf_name='invoice_'; 
      break;
      case 'reccuring_invoice':
        $this->table = 'reccuring_invoice';
        $this->id_name = 'recurring_invoice_id';
        $this->document_name = gm('Recurring invoice');
        $this->document_name_logging = '{l}Recurring invoice{endl}';
        $this->pdf_name='invoice_'; 
      break;
      case 'invoice_incomming':
        $this->table = 'tblinvoice_incomming';
        $this->id_name = 'invoice_id';
        $this->document_name = gm('Purchase invoice');
        $this->document_name_logging = '{l}Purchase invoice{endl}';
        $this->pdf_name='invoice_'; 
      break;
      case 'invoice_reminder':   
        $this->table = '';
        $this->id_name = '';
        $this->multiple_ids = $item_id;
        $this->item_id = '';
        $this->document_name = gm('Invoice reminder');
        $this->document_name_logging = '{l}Invoice reminder{endl}';
        $this->pdf_name='invoice_'; 
      break;
      case 'quote':
        $this->table = 'tblquote';
        $this->id_name = 'id';
        $this->document_name = gm('Quote');
        $this->document_name_logging = '{l}Quote{endl}';
        $this->pdf_name='quote_'; 
      break;
      case 'contract':
        $this->table = 'contracts';
        $this->id_name = 'contract_id';
        $this->document_name = gm('Contract');
        $this->document_name_logging = '{l}Contract{endl}';
        $this->pdf_name='contract_'; 
      break;
      case 'order': 
        $this->table = 'pim_orders';
        $this->id_name = 'order_id';
        $this->document_name = gm('Sales order');
        $this->document_name_logging = '{l}Sales order{endl}';
        $this->pdf_name='order_'; 
      break;
      case 'p_order': 
        $this->table = 'pim_p_orders';
        $this->id_name = 'p_order_id';
        $this->document_name = gm('Purchase order');
        $this->document_name_logging = '{l}Purchase order{endl}';
        $this->pdf_name='p_order_'; 
      break;
      case 'service':
        $this->table = 'servicing_support';
        $this->id_name = 'service_id';
        $this->document_name = gm('Intervention');
        $this->document_name_logging = '{l}Intervention{endl}';
        $this->pdf_name='intervention'; 
      break;

    }
   
  }


  function get_sendgrid($in){
  
    $output = array('success'=> array(), 'error'=>array());
    if($_SESSION['u_id']){
      $is_trial=$this->db_users->field("SELECT is_trial FROM user_info WHERE user_id='".$_SESSION['u_id']."' ");
      if(!$is_trial){
        global $config;
        $mails_sent=$this->db->field("SELECT value FROM settings WHERE constant_name='MAIL_SENT_NUMBER'");
        if((int)$mails_sent == $config['mail_sending_limit']){
          $msg=gm('Mail sending limit reached');
          array_push($output['error'], $msg);     
          return $output;
        }else{
          $this->db->field("UPDATE settings SET `value`='".((int)$mails_sent+1)."' WHERE constant_name='MAIL_SENT_NUMBER'");
        }
      }
    }   
    $successfullSendGridBatch=false;
        $destinations = $in['new_email'];
        $batch_invoice_data = array('webservice_name' => 'mail/batch',
                    'webservice_id' => $this->item_id,
                    'module' => $this->pag,
                    'module_id' => $this->item_id, 
                    'method'  => 'POST',
                    'call_link' => '/mail/batch',
                    'webservice_outgoing' => array(),
                    'module_multiple_ids' =>'',
            );
        $batchDataResponse = sendgridCall($batch_invoice_data,$this->database_name);
        $batchData = json_decode($batchDataResponse['response']);
        if($batchDataResponse['headers']['http_code'] >= 200 && $batchDataResponse['headers']['http_code'] < 300){  
             $in['batch_id'] = $batchData->batch_id;
             $successfullSendGridBatch=true;
        }else{
          if(isset($batchData->errors[0]->message)){
            $msg = gm('SendGrid batch_id error: ').$batchData->errors[0]->message;
          } else {
            $msg = gm('There was an error trying to get the batch_id from SendGrid. Please try again later');
          }
          msg::error ($msg,'error');
          insert_error_call($this->database_name,"sendgrid",$this->pag,$this->item_id,$sendgrid_call_response['response']);
          addToExceptionList(array(
              'destination' =>  $destinations,
              'location_id' => '',
              'module'      => $this->pag,
              'module_id'   =>  $this->item_id,
              'source'      => 'sendgrid',
              'message'     => $msg
            ));
            
          array_push($output['error'], $msg);     
            return $output;               
        }

        if($successfullSendGridBatch){
          
          $sendGridData = $this->getSendGridData($in);

           $sendgrid_send_at_date = $sendGridData['send_at'];

           $successfullSendGridCall = false;

           $sendgrid_invoice_data = array('webservice_name' => 'mail/send',
                    'webservice_id' => $this->item_id,
                    'module' => $this->pag,
                    'module_id' => $this->item_id, 
                    'method'  => 'POST',
                    'call_link' => '/mail/send',
                    'webservice_outgoing' => $sendGridData,
                    'module_multiple_ids' => $this->multiple_ids
            );

            $sendgrid_call_response = sendgridCall($sendgrid_invoice_data,$this->database_name);


            if($sendgrid_call_response['headers']['http_code'] >= 200 && $sendgrid_call_response['headers']['http_code'] < 300){
                $successfullSendGridCall = true;
            }else{
              $errors_data = false;

              $data_headers_c = explode("\n",$sendgrid_call_response['response']);

              foreach($data_headers_c as $key => $h_data){
                  if(strpos($h_data,'"errors":[{"message')){
                      $errors_data = $h_data;                    
                  }
              }

              $sendGridResp = json_decode($errors_data);

              if(isset($sendGridResp->errors[0]->message)){

                  $msg = $this->document_name.' '.gm(' sent to ').' '.$destinations.' '.gm(' could not be delivered ').' '.date(ACCOUNT_DATE_FORMAT,time()).gm('. Reason:').' '.$sendGridResp->errors[0]->message;
                  $mess_log = $this->document_name_logging.' {l}sent to{endl} '.$destinations.' {l}could not be delivered{endl} '.date(ACCOUNT_DATE_FORMAT,time()).'{l}. Reason:{endl} '.$sendGridResp->errors[0]->message;
                } else {
                  $msg = gm('There was an error trying to send using SendGrid. Please try again later');
                   $mess_log ='{l}There was an error trying to send using SendGrid. Please try again later{endl}';
                }
                msg::error ($msg,'error');
                insert_error_call($this->database_name,"sendgrid",$this->pag,$this->item_id,$sendgrid_call_response['response']);

                insert_message_log($this->pag,$mess_log,$this->field_n,$this->item_id);
                addToExceptionList(array(
                    'destination' => $destinations,
                    'location_id' => '',
                    'module'      => $this->pag,
                    'module_id'   =>  $this->item_id,
                    'source'      => 'sendgrid',
                    'message'     => $msg
                  ));
                                
              array_push($output['error'], $msg);     
                return $output;
                           
            }

            if($successfullSendGridCall){
              $x_message_id = '';

              $resp_headers = array();
              $data_headers = explode("\n",$sendgrid_call_response['response']);
              $data_headers['status'] = $data_headers[0];
              array_shift($resp_headers);

              foreach($data_headers as $h){
                  $middle=explode(":",$h);
                  $resp_headers[trim($middle[0])] = trim($middle[1]);
              }

              if(!empty($resp_headers['X-Message-Id'])){
                  $x_message_id = $resp_headers['X-Message-Id'];
              }

              if($this->item_id){
                $this->markSentToSendGrid($x_message_id, $sendgrid_send_at_date, $in['batch_id'], $in['new_email'] );
              }
                        
              $msg = gm("Email sent using SendGrid");
        
              array_push($output['success'], $msg);     
                return $output;
              
            }
        } 

    
  }

  function markSentToSendGrid($x_message_id, $sendgrid_send_at_date, $batch_id, $dest ){
     $sent_to_sendgrid_orig=$this->db->field("SELECT sent_to_sendgrid FROM ".$this->table." WHERE '".$this->id_name."'='".$this->item_id."' ");
      if(!$sent_to_sendgrid_orig){
      $this->db->query("UPDATE ".$this->table." SET sent_to_sendgrid='1',sendgrid_send_at_date='".$sendgrid_send_at_date."',batch_id='".$batch_id."',x_message_id='".$x_message_id."' WHERE ".$this->id_name." = '".$this->item_id."'");
      }
      $sent_to_sendgrid=$this->db->field("SELECT sent_to_sendgrid FROM ".$this->table." WHERE ".$this->id_name." ='".$this->item_id."' ");

      //$mess_log=$this->document_name.' {l}was sent to{endl}: '.$dest.' {l}using Sendgrid by{endl} '.get_user_name($_SESSION['u_id']);
      // $mess_log=$this->document_name.' {l}has been sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.$dest.' {l}using Sendgrid{endl} ';
      $mess_log=$this->document_name_logging.' {l}has been sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to:{endl} '.$dest.' ';

      if(!$this->attach){ //not from invoice cron
          insert_message_log($this->pag,$mess_log,$this->field_n,$this->item_id);
      }

  }


  function getSendGridData($in){

    switch ($this->pag) {
       case 'invoice': //done
        $sendGridData = $this->getSendGridDataInvoice($in);
      break;
      case 'invoice_incomming':
        $sendGridData =$this->getSendGridDataInvoiceIncomming($in);
      break;
      case 'invoice_reminder':   //done
        $sendGridData = $this->getSendGridDataInvoiceReminder($in);
      break;
      case 'quote':
      case 'contract':
         $sendGridData = $this->getSendGridDataQuote($in);
      break;
      case 'order':
      case 'p_order':
         $sendGridData = $this->getSendGridDataOrder($in);
      break;
      case 'service':
         $sendGridData = $this->getSendGridDataService($in);
      break;
    }

   return $sendGridData;  

  }

  function getSendGridDataInvoice($in){

   // var_dump($in, $this);exit();

    global $config;

    $document = $this->db->query("SELECT id, serial_number, buyer_name, seller_bwt_nr, acc_manager_id, sent_to_sendgrid FROM ".$this->table." WHERE ".$this->id_name."='".$this->item_id."'");

    $b_name= $document->f('buyer_name');
    $seller_bwt_nr=$document->f('seller_bwt_nr');
     
    $sendgrid_data = array('personalizations'=> array( array( 'to'=>array(),'cc'=>array(), 'bcc'=>array() ) ),'attachments' => array() ,'content' => array());

      //All email values must be unique (according to SendGrid)
   
      $bcc_email = array();

      $bcc = str_replace(' ', '', $this->default_email['bcc']['email']);
        $email_arr = array();
        $email_arr = explode(";", $bcc);
        foreach($email_arr as $key=> $value){
          if(!empty($value) && !in_array($value,array_column($bcc_email,'email'))){
            $tmp_rec = array('email'=>$value);
            array_push($bcc_email, $tmp_rec);
          }
        }
        //la invoice,functia sendNewEmail se apeleaza pt fiecare email in parte, inclusiv pt copy si acc_email; emailul va fi in variabila $in['new_email']
       if(!in_array($in['new_email'],array_column($sendgrid_data['personalizations'][0]['to'],'email'))){
          $tmp_rec = array('email'=>$in['new_email']);
          array_push($sendgrid_data['personalizations'][0]['to'], $tmp_rec);
        }

      if(!empty($bcc_email)){
        $sendgrid_data['personalizations'][0]['bcc'] = $bcc_email;
      }
          

   $from_email = array('email' => $this->default_email['from']['email'],'name' => $this->default_email['from']['name'] );
    $reply_to_email = array('email' => $this->default_email['reply']['email'],'name' => $this->default_email['reply']['name']);
    
   
    //CC cannot be send empty
    if(empty($sendgrid_data['personalizations'][0]['cc'])){
      unset($sendgrid_data['personalizations'][0]['cc']);
    }

    //BCC cannot be send empty

    if(empty($sendgrid_data['personalizations'][0]['bcc'])){
      unset($sendgrid_data['personalizations'][0]['bcc']);
    }

   // $full_contact_name = get_sendgrid_full_contact_name($document->f('contact_id'));

    /*$dynamic_template_data = array('Contact_first_name' => $full_contact_name['first_name'],
                    'Constact_last_name'=> $full_contact_name['last_name'],
                    'Account_name'    => addslashes($document->f('buyer_name')),
                    'Account_reference' => get_account_reference($document->f('buyer_id')),
                    'Invoice_date'    => date(ACCOUNT_DATE_FORMAT,$document->f('invoice_date')),
                    'Due_date'      => date(ACCOUNT_DATE_FORMAT,$document->f('due_date')),
                    'Total_Amount'    => $in['sendgrid_total_amount'],
                    'Invoice_number'  => $document->f('serial_number'),
                    'Our_reference'   => addslashes($document->f('our_ref')),
                    'OGM'       => $document->f('ogm'),
                  );

    $sendgrid_data['personalizations'][0]['dynamic_template_data'] = $dynamic_template_data;*/

    $sendgrid_data['from'] = $from_email;
    if(!empty($reply_to_email['email']) && !$this->account_send_email){
      $sendgrid_data['reply_to'] = $reply_to_email;
    }

     $sendgrid_data['personalizations'][0]['subject']= stripslashes($in['e_subject']);

    $sendgrid_data['content'][0]['type'] ='text/plain';
    $sendgrid_data['content'][0]['value'] =$this->get_plain_text($this->message_body);

    $sendgrid_data['content'][1]['type'] ='text/html';
    $sendgrid_data['content'][1]['value'] =$this->message_body;

    $sendgrid_data['headers']['List-Unsubscribe'] ='';

   /* $e_lang = $in['email_language'];
    if(!$e_lang || $e_lang > 4){
      $e_lang=1;
    }*/

    /*if($document->f('type')==0){
      $is_downpayment_invoice=$this->db->field("SELECT id FROM tbldownpayments WHERE invoice_id='".$in['invoice_id']."' ORDER BY id DESC LIMIT 1");
      if($is_downpayment_invoice){
        $suffix = 'D';
      }else{
          $suffix = 'S';
      }          
    }
    if($document->f('type')==2){
      $suffix = 'C';
    }

    switch ($e_lang) {
      case '3':
        $const_name = $suffix.'_TEMPLATE_NL';
        break;
      case '2':
        $const_name = $suffix.'_TEMPLATE_FR';
        break;
      default:
        $const_name = $suffix.'_TEMPLATE_EN';
        break;
    }

    $template_id = $this->db->field("SELECT value from settings where constant_name='".'SENDGRID_SETTING_'.$const_name."'");
    $sendgrid_data['template_id'] = $template_id;*/

    $name=$this->pdf_name.'.pdf'; 

    if($document->f('serial_number')){
      $name=$document->f('serial_number').'.pdf';
    }elseif($document->f('id')){
      $name=$this->pdf_name.$document->f('id').'.pdf';
    }

     if($in['include_pdf']){
        $tmp_file_name = $this->pdf_name.'.pdf';
        if($in['attach_file_name']){
          $tmp_file_name = $in['attach_file_name'];
        }
        /*if($this->attach){
          $targetFile =__DIR__.'/../../../upload/32ada082_5cf4_1518_3aa09bca7f29/test.txt';
            file_put_contents($targetFile,$this->attach);
        }*/
        if($this->attach){ //from invoice cron
          $main_content = $this->attach;
        }else{
            $main_content = file_get_contents($tmp_file_name);
        }
        
        if($main_content){
          $content = base64_encode($main_content); //chunk_split(base64_encode($main_content), 76, "\r\n");
          $mime_content_type = 'application/pdf';
          $main_content_tmp = array('content' => $content,'filename' => $name);
          array_push($sendgrid_data['attachments'], $main_content_tmp);
        }
      }

      if($in['files']){
        foreach ($in['files'] as $key => $value) {
          if($value['checked'] == 1){
            $a_link = $value['path'].$value['file'];
            $a_content = file_get_contents($a_link);
            if($a_content){
              $a_c = base64_encode($a_content); //chunk_split(base64_encode($a_content), 76, "\r\n");  
              $a_content_tmp = array('content' => $a_c,'filename' => $value['file']);
              array_push($sendgrid_data['attachments'], $a_content_tmp);   
            }
          }
        }
      }

      if($in['dropbox_files']){
            $path_dropbox = 'upload/'.$this->database_name.'/dropbox_files/';
            if(!file_exists($path_dropbox)){
              mkdir($path_dropbox,0775,true);
            }
            $dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

            foreach ($in['dropbox_files'] as $key => $value) {
              if($value['checked'] == 1){
                $value['file'] = str_replace('/', '_', $value['file']);

                $file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
                          
                $mime = mime_content_type($path_dropbox.$value['file']);
                
              $a_content = file_get_contents($path_dropbox.$value['file']);

                if($a_content){
                  $a_c = base64_encode($a_content); //chunk_split(base64_encode($a_content), 76, "\r\n");  
                  $a_content_tmp = array('content' => $a_c,'filename' => $value['file']);
                  array_push($sendgrid_data['attachments'], $a_content_tmp);   
                }
              }
            }
      }
  
      if($in['include_xml']){

        if($this->attach){ //from invoice cron
         $in['efff_str'] =base64_encode($this->attach);
         $in['include_xml'] ='';
         $in['efff_xml']=true;
         $in['dbase']=$this->database_name;
         $xml_tmp_file_name=__DIR__."/../../../eff_".str_replace(array('.',' ','-','_'), '', $this->account_vat_number)."_".substr(str_replace(array('.',' ','-','_'), '', $this->account_company),0,10)."_".$document->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $seller_bwt_nr)."_".mb_substr(str_replace(array('.',' ','-','_'), '', $b_name),0,10, 'utf-8').".xml";
        }else{
           $xml_tmp_file_name="eff_".$this->database_name."_".$document->f('serial_number').".xml";
        }

        $in['remove_accounting_cost']=true;
        include(__DIR__.'/../../invoice/controller/xml_invoice_print.php');
       
        $a_xml_c = file_get_contents($xml_tmp_file_name);
        if($a_xml_c){
            $a_xml = base64_encode($a_xml_c); //chunk_split(base64_encode($a_xml_c), 76, "\r\n");
            $a_xml_name = "eff_".str_replace(array('.',' ','-','_'), '', $this->account_vat_number)."_".substr(str_replace(array('.',' ','-','_','"'), '', $this->account_company),0,10)."_".$document->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $seller_bwt_nr)."_".mb_substr(str_replace(array('.',' ','-','_','"'), '', $b_name),0,10, 'utf-8').".xml";
            $a_xml_tmp = array('content' => $a_xml,'filename' => $a_xml_name);
            array_push($sendgrid_data['attachments'], $a_xml_tmp);
        }
      }

      //Cannot be send empty
      if(empty($sendgrid_data['attachments'])){
        unset($sendgrid_data['attachments']);
      }

    //Sent date => send only if the document was not sent to SendGrid 
    if(!$document->f('sent_to_sendgrid')){
        $sent_date = new DateTime();
        $sent_date->setTimestamp(time());
        // $sent_date->setTimezone(new DateTimeZone('Europe/Brussels'));
        //$sent_date->modify('+20 minutes');
        $send_at = $sent_date->getTimestamp();
        $sendgrid_data['send_at'] = $send_at;
    }  
 
    $sendgrid_data['batch_id'] = $in['batch_id'];
    $sendgrid_data['tracking_settings'] = array('open_tracking'=>array('enable'=>true));
    $sendgrid_data['mail_settings'] = array('bypass_list_management'=>array('enable'=>true));

   //print_r($sendgrid_data);
    //var_dump($sendgrid_data['personalizations'][0]);exit();
    return $sendgrid_data;  
  }

  function getSendGridDataInvoiceReminder($in){

    global $config;
     
    $sendgrid_data = array('personalizations'=> array( array( 'to'=>array(),'cc'=>array(), 'bcc'=>array() ) ),'attachments' => array() ,'content' => array());

    $bcc_email = array();

    $bcc = str_replace(' ', '', $this->default_email['bcc']['email']);
      $email_arr = array();
      $email_arr = explode(";", $bcc);
      foreach($email_arr as $key=> $value){
        if(!empty($value) && !in_array($value,array_column($bcc_email,'email'))){
          $tmp_rec = array('email'=>$value);
          array_push($bcc_email, $tmp_rec);
        }
      }
      //la invoice,functia sendNewEmail se apeleaza pt fiecare email in parte, inclusiv pt copy si acc_email; emailul va fi in variabila $in['new_email']
     if(!in_array($in['new_email'],array_column($sendgrid_data['personalizations'][0]['to'],'email'))){
        $tmp_rec = array('email'=>$in['new_email']);
        array_push($sendgrid_data['personalizations'][0]['to'], $tmp_rec);
      }

    if(!empty($bcc_email)){
      $sendgrid_data['personalizations'][0]['bcc'] = $bcc_email;
    }      
  
    $from_email = array('email' => $this->default_email['from']['email'],'name' => $this->default_email['from']['name'] );
    $reply_to_email = array('email' => $this->default_email['reply']['email'],'name' => $this->default_email['reply']['name']);   
   
    if(empty($sendgrid_data['personalizations'][0]['cc'])){
      unset($sendgrid_data['personalizations'][0]['cc']);
    }

    if(empty($sendgrid_data['personalizations'][0]['bcc'])){
      unset($sendgrid_data['personalizations'][0]['bcc']);
    }

   
    $sendgrid_data['from'] = $from_email;
    if(!empty($reply_to_email['email'])  && !$this->account_send_email){
      $sendgrid_data['reply_to'] = $reply_to_email;
    }

    $sendgrid_data['personalizations'][0]['subject']= stripslashes($in['subject']);

    $sendgrid_data['content'][0]['type'] ='text/plain';
    $sendgrid_data['content'][0]['value'] =$this->get_plain_text($this->message_body);

    $sendgrid_data['content'][1]['type'] ='text/html';
    $sendgrid_data['content'][1]['value'] =$this->message_body;

    $sendgrid_data['headers']['List-Unsubscribe'] ='';

    $name=$this->pdf_name.'.pdf'; 

    if($in['include_pdf']){
        $tmp_file_name = $this->pdf_name.'.pdf';
        $main_content = file_get_contents($tmp_file_name);
        
        if($main_content){
          $content = base64_encode($main_content); //chunk_split(base64_encode($main_content), 76, "\r\n");
          $mime_content_type = 'application/pdf';
          $main_content_tmp = array('content' => $content,'filename' => $name);
          array_push($sendgrid_data['attachments'], $main_content_tmp);
        }
      }  

    if(empty($sendgrid_data['attachments'])){
      unset($sendgrid_data['attachments']);
    }

    $sendgrid_data['batch_id'] = $in['batch_id'];
    $sendgrid_data['tracking_settings'] = array('open_tracking'=>array('enable'=>true));
    $sendgrid_data['mail_settings'] = array('bypass_list_management'=>array('enable'=>true));
   
    //var_dump($sendgrid_data['personalizations'][0]);exit();
    return $sendgrid_data;  
  }

  function getSendGridDataOrder($in){
   // var_dump($in, $this);exit();
    global $config;

    $document = $this->db->query("SELECT ".$this->id_name.", serial_number, sent_to_sendgrid FROM ".$this->table." WHERE ".$this->id_name."='".$this->item_id."'");
     
    $sendgrid_data = array('personalizations'=> array( array( 'to'=>array(),'cc'=>array(), 'bcc'=>array() ) ),'attachments' => array() ,'content' => array());

    $bcc_email = array();

    $bcc = str_replace(' ', '', $this->default_email['bcc']['email']);
      $email_arr = array();
      $email_arr = explode(";", $bcc);
      foreach($email_arr as $key=> $value){
        if(!empty($value) && !in_array($value,array_column($bcc_email,'email'))){
          $tmp_rec = array('email'=>$value);
          array_push($bcc_email, $tmp_rec);
        }
      }
     
     if(!in_array($in['new_email'],array_column($sendgrid_data['personalizations'][0]['to'],'email'))){
        $tmp_rec = array('email'=>$in['new_email']);
        array_push($sendgrid_data['personalizations'][0]['to'], $tmp_rec);
      }

    if(!empty($bcc_email)){
      $sendgrid_data['personalizations'][0]['bcc'] = $bcc_email;
    }

    $from_email = array('email' => $this->default_email['from']['email'],'name' => $this->default_email['from']['name'] );
    $reply_to_email = array('email' => $this->default_email['reply']['email'],'name' => $this->default_email['reply']['name']);
    
    if(empty($sendgrid_data['personalizations'][0]['cc'])){
      unset($sendgrid_data['personalizations'][0]['cc']);
    }

    if(empty($sendgrid_data['personalizations'][0]['bcc'])){
      unset($sendgrid_data['personalizations'][0]['bcc']);
    }
 
    $sendgrid_data['from'] = $from_email;
    if(!empty($reply_to_email['email']) && !$this->account_send_email){
      $sendgrid_data['reply_to'] = $reply_to_email;
    }

    $sendgrid_data['personalizations'][0]['subject']= stripslashes($in['e_subject']);

    $sendgrid_data['content'][0]['type'] ='text/plain';
    $sendgrid_data['content'][0]['value'] =$this->get_plain_text($this->message_body);

    $sendgrid_data['content'][1]['type'] ='text/html';
    $sendgrid_data['content'][1]['value'] =$this->message_body;

    $sendgrid_data['headers']['List-Unsubscribe'] ='';

    $name=$this->pdf_name.'.pdf'; 

    if($document->f('serial_number')){
      $name=$document->f('serial_number').'.pdf';
    }elseif($document->f($this->id_name)){
      $name=$this->pdf_name.$document->f($this->id_name).'.pdf';    
    }

     if($in['include_pdf']){
        $tmp_file_name = $this->pdf_name.'.pdf';
        if($in['attach_file_name']){
          $tmp_file_name = $in['attach_file_name'];
        }
        $main_content = file_get_contents($tmp_file_name);
        
        if($main_content){
          $content = base64_encode($main_content); //chunk_split(base64_encode($main_content), 76, "\r\n");
          $mime_content_type = 'application/pdf';
          $main_content_tmp = array('content' => $content,'filename' => $name);
          array_push($sendgrid_data['attachments'], $main_content_tmp);
        }
      }

      if($in['files']){
        foreach ($in['files'] as $key => $value) {
          if($value['checked'] == 1){
            $a_link = $value['path'].$value['file'];
            $a_content = file_get_contents($a_link);
            if($a_content){
              $a_c = base64_encode($a_content); //chunk_split(base64_encode($a_content), 76, "\r\n");  
              $a_content_tmp = array('content' => $a_c,'filename' => $value['file']);
              array_push($sendgrid_data['attachments'], $a_content_tmp);   
            }
          }
        }
      }

     if($in['dropbox_files']){
            $path_dropbox = 'upload/'.$this->database_name.'/dropbox_files/';
            if(!file_exists($path_dropbox)){
              mkdir($path_dropbox,0775,true);
            }
            $dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

            foreach ($in['dropbox_files'] as $key => $value) {
              if($value['checked'] == 1){
                $value['file'] = str_replace('/', '_', $value['file']);

                $file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
                          
                $mime = mime_content_type($path_dropbox.$value['file']);
                
              $a_content = file_get_contents($path_dropbox.$value['file']);

                if($a_content){
                  $a_c = base64_encode($a_content); //chunk_split(base64_encode($a_content), 76, "\r\n");  
                  $a_content_tmp = array('content' => $a_c,'filename' => $value['file']);
                  array_push($sendgrid_data['attachments'], $a_content_tmp);   
                }
              }
            }
      }

    if(empty($sendgrid_data['attachments'])){
      unset($sendgrid_data['attachments']);
    }

    if(!$document->f('sent_to_sendgrid')){
        $sent_date = new DateTime();
        $sent_date->setTimestamp(time());
        $send_at = $sent_date->getTimestamp();
        $sendgrid_data['send_at'] = $send_at;
    }  
 
    $sendgrid_data['batch_id'] = $in['batch_id'];
    $sendgrid_data['tracking_settings'] = array('open_tracking'=>array('enable'=>true));
    $sendgrid_data['mail_settings'] = array('bypass_list_management'=>array('enable'=>true));

     /*var_dump($sendgrid_data['personalizations'][0]);exit();*/
    return $sendgrid_data;  
  }

  function getSendGridDataQuote($in){
    //var_dump($in, $this);exit();
    global $config;

    $document = $this->db->query("SELECT ".$this->id_name.", serial_number, acc_manager_id, sent_to_sendgrid FROM ".$this->table." WHERE ".$this->id_name."='".$this->item_id."'");
     
    $sendgrid_data = array('personalizations'=> array( array( 'to'=>array(),'cc'=>array(), 'bcc'=>array() ) ),'attachments' => array() ,'content' => array());

    $bcc_email = array();

    $bcc = str_replace(' ', '', $this->default_email['bcc']['email']);
      $email_arr = array();
      $email_arr = explode(";", $bcc);
      foreach($email_arr as $key=> $value){
        if(!empty($value) && !in_array($value,array_column($bcc_email,'email'))){
          $tmp_rec = array('email'=>$value);
          array_push($bcc_email, $tmp_rec);
        }
      }
     
     if(!in_array($in['new_email'],array_column($sendgrid_data['personalizations'][0]['to'],'email'))){
        $tmp_rec = array('email'=>$in['new_email']);
        array_push($sendgrid_data['personalizations'][0]['to'], $tmp_rec);
      }

      //Send me a copy
   /* if($in['copy']){
        $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
        if($u->f('email')){
          if( !in_array($u->f('email'),array_column($sendgrid_data['personalizations'][0]['bcc'],'email')) && !in_array($u->f('email'),array_column($sendgrid_data['personalizations'][0]['to'],'email'))){
            $copy_tmp =array('email'=>$u->f('email'));
            array_push($sendgrid_data['personalizations'][0]['cc'],$copy_tmp);
          }
        }
    }

    //Send a copy to account manager 
    if($in['copy_acc']){
        $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$document->f('acc_manager_id')."' ");
        if($u->f('email')){
          if(!in_array($u->f('email'),array_column($sendgrid_data['personalizations'][0]['bcc'],'email')) && !in_array($u->f('email'),array_column($sendgrid_data['personalizations'][0]['to'],'email')) && !in_array($u->f('email'),array_column($sendgrid_data['personalizations'][0]['cc'],'email'))){
            $copy_acc_tmp =array('email'=>$u->f('email'));
            array_push($sendgrid_data['personalizations'][0]['bcc'],$copy_acc_tmp);  
          }
        }
    }*/



    if(!empty($bcc_email)){
      $sendgrid_data['personalizations'][0]['bcc'] = $bcc_email;
    }

   $from_email = array('email' => $this->default_email['from']['email'],'name' => $this->default_email['from']['name'] );
    $reply_to_email = array('email' => $this->default_email['reply']['email'],'name' => $this->default_email['reply']['name']);
    
   
    //CC cannot be send empty
    if(empty($sendgrid_data['personalizations'][0]['cc'])){
      unset($sendgrid_data['personalizations'][0]['cc']);
    }

    //BCC cannot be send empty

    if(empty($sendgrid_data['personalizations'][0]['bcc'])){
      unset($sendgrid_data['personalizations'][0]['bcc']);
    }
 
    $sendgrid_data['from'] = $from_email;
    if(!empty($reply_to_email['email']) && !$this->account_send_email){
      $sendgrid_data['reply_to'] = $reply_to_email;
    }

    $sendgrid_data['personalizations'][0]['subject']= stripslashes($in['e_subject']);

    $sendgrid_data['content'][0]['type'] ='text/plain';
    $sendgrid_data['content'][0]['value'] =$this->get_plain_text($this->message_body);

    $sendgrid_data['content'][1]['type'] ='text/html';
    $sendgrid_data['content'][1]['value'] =$this->message_body;

    $sendgrid_data['headers']['List-Unsubscribe'] ='';

    $name=$this->pdf_name.'.pdf'; 

    if($document->f('serial_number')){
      $name=$document->f('serial_number').'.pdf';
    }elseif($document->f($this->id_name)){
      $name=$this->pdf_name.$document->f($this->id_name).'.pdf';    
    }

     if($in['include_pdf']){
        $tmp_file_name = $this->pdf_name.'.pdf';
        if($in['attach_file_name']){
          $tmp_file_name = $in['attach_file_name'];
        }
        $main_content = file_get_contents($tmp_file_name);
        
        if($main_content){
          $content = base64_encode($main_content); //chunk_split(base64_encode($main_content), 76, "\r\n");
          $mime_content_type = 'application/pdf';
          $main_content_tmp = array('content' => $content,'filename' => $name);
          array_push($sendgrid_data['attachments'], $main_content_tmp);
        }
      }

      if($in['files']){
        foreach ($in['files'] as $key => $value) {
          if($value['checked'] == 1){
            $a_link = $value['path'].$value['file'];
            $a_content = file_get_contents($a_link);
            if($a_content){
              $a_c = base64_encode($a_content); //chunk_split(base64_encode($a_content), 76, "\r\n");  
              $a_content_tmp = array('content' => $a_c,'filename' => $value['file']);
              array_push($sendgrid_data['attachments'], $a_content_tmp);   
            }
          }
        }
      }

     if($in['dropbox_files']){
            $path_dropbox = 'upload/'.$this->database_name.'/dropbox_files/';
            if(!file_exists($path_dropbox)){
              mkdir($path_dropbox,0775,true);
            }
            $dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

            foreach ($in['dropbox_files'] as $key => $value) {
              if($value['checked'] == 1){
                $value['file'] = str_replace('/', '_', $value['file']);

                $file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
                          
                $mime = mime_content_type($path_dropbox.$value['file']);
                
              $a_content = file_get_contents($path_dropbox.$value['file']);

                if($a_content){
                  $a_c = base64_encode($a_content); //chunk_split(base64_encode($a_content), 76, "\r\n");  
                  $a_content_tmp = array('content' => $a_c,'filename' => $value['file']);
                  array_push($sendgrid_data['attachments'], $a_content_tmp);   
                }
              }
            }
      }

      //Cannot be send empty
    if(empty($sendgrid_data['attachments'])){
      unset($sendgrid_data['attachments']);
    }

    //Sent date => send only if the document was not sent to SendGrid 
    if(!$document->f('sent_to_sendgrid')){
        $sent_date = new DateTime();
        $sent_date->setTimestamp(time());
        $send_at = $sent_date->getTimestamp();
        $sendgrid_data['send_at'] = $send_at;
    }  
 
    $sendgrid_data['batch_id'] = $in['batch_id'];
    $sendgrid_data['tracking_settings'] = array('open_tracking'=>array('enable'=>true));
    $sendgrid_data['mail_settings'] = array('bypass_list_management'=>array('enable'=>true));
    /*print_r($sendgrid_data['content'][0]['value']);
     var_dump($sendgrid_data['personalizations'][0]);exit();*/
    return $sendgrid_data;  
  }

  function getSendGridDataService($in){

    global $config;

    $document = $this->db->query("SELECT ".$this->id_name.", serial_number, sent_to_sendgrid, acc_manager_id FROM ".$this->table." WHERE ".$this->id_name."='".$this->item_id."'");
     
    $sendgrid_data = array('personalizations'=> array( array( 'to'=>array(),'cc'=>array(), 'bcc'=>array() ) ),'attachments' => array() ,'content' => array());

    $bcc_email = array();

    $bcc = str_replace(' ', '', $this->default_email['bcc']['email']);
      $email_arr = array();
      $email_arr = explode(";", $bcc);
      foreach($email_arr as $key=> $value){
        if(!empty($value) && !in_array($value,array_column($bcc_email,'email'))){
          $tmp_rec = array('email'=>$value);
          array_push($bcc_email, $tmp_rec);
        }
      }
     
     if(!in_array($in['new_email'],array_column($sendgrid_data['personalizations'][0]['to'],'email'))){
        $tmp_rec = array('email'=>$in['new_email']);
        array_push($sendgrid_data['personalizations'][0]['to'], $tmp_rec);
      }

    if(!empty($bcc_email)){
      $sendgrid_data['personalizations'][0]['bcc'] = $bcc_email;
    }

    $from_email = array('email' => $this->default_email['from']['email'],'name' => $this->default_email['from']['name'] );
    $reply_to_email = array('email' => $this->default_email['reply']['email'],'name' => $this->default_email['reply']['name']);
    
    if(empty($sendgrid_data['personalizations'][0]['cc'])){
      unset($sendgrid_data['personalizations'][0]['cc']);
    }

    if(empty($sendgrid_data['personalizations'][0]['bcc'])){
      unset($sendgrid_data['personalizations'][0]['bcc']);
    }
 
    $sendgrid_data['from'] = $from_email;
    if(!empty($reply_to_email['email']) && !$this->account_send_email){
      $sendgrid_data['reply_to'] = $reply_to_email;
    }

    $sendgrid_data['personalizations'][0]['subject']= stripslashes($in['e_subject']);

    $sendgrid_data['content'][0]['type'] ='text/plain';
    $sendgrid_data['content'][0]['value'] =$this->get_plain_text($this->message_body);

    $sendgrid_data['content'][1]['type'] ='text/html';
    $sendgrid_data['content'][1]['value'] =$this->message_body;

    $sendgrid_data['headers']['List-Unsubscribe'] ='';

    $name=$this->pdf_name.'.pdf'; 

    if($document->f('serial_number')){
      $name=$document->f('serial_number').'.pdf';
    }elseif($document->f($this->id_name)){
      $name=$this->pdf_name.$document->f($this->id_name).'.pdf';    
    }

     if($in['include_pdf']){
        $tmp_file_name = $this->pdf_name.'.pdf';
        if($in['attach_file_name']){
          $tmp_file_name = $in['attach_file_name'];
        }
        $main_content = file_get_contents($tmp_file_name);
        
        if($main_content){
          $content = base64_encode($main_content); //chunk_split(base64_encode($main_content), 76, "\r\n");
          $mime_content_type = 'application/pdf';
          $main_content_tmp = array('content' => $content,'filename' => $name);
          array_push($sendgrid_data['attachments'], $main_content_tmp);
        }
      }

     if($in['dropbox_files']){
          
            foreach ($in['dropbox_files'] as $key => $value) {
              if($value['checked'] == 1){
                $main_content = file_get_contents($value['url']);
        
                if($main_content){
                  $content = base64_encode($main_content); //chunk_split(base64_encode($main_content), 76, "\r\n");
                  //$mime_content_type = 'application/pdf';
                  $main_content_tmp = array('content' => $content,'filename' => $value['name']);
                  array_push($sendgrid_data['attachments'], $main_content_tmp);
                }
               } 
            }
      }

    if(empty($sendgrid_data['attachments'])){
      unset($sendgrid_data['attachments']);
    }

    if(!$document->f('sent_to_sendgrid')){
        $sent_date = new DateTime();
        $sent_date->setTimestamp(time());
        $send_at = $sent_date->getTimestamp();
        $sendgrid_data['send_at'] = $send_at;
    }  
 
    $sendgrid_data['batch_id'] = $in['batch_id'];
    $sendgrid_data['tracking_settings'] = array('open_tracking'=>array('enable'=>true));
    $sendgrid_data['mail_settings'] = array('bypass_list_management'=>array('enable'=>true));

     //var_dump($sendgrid_data['personalizations'][0]);exit();
    return $sendgrid_data;  
  }

  function get_plain_text($text){
    $text = strip_tags($text, '<style>');
    $start = strpos($text, '<style');
    // All of occurrences of <style>.
    while ($start !== false) {
        $end = strpos($text, '</style>');
        if (!$text) {
            break;
        }
        $diff = $end - $start + strlen('</style>');
        $substring = substr($text, $start, $diff);
        $text = str_replace($substring, '', $text);
        $start = strpos($text, '<style');
    }

    // Remaining <style> if any.
    $text = strip_tags($text);

    // Remove all new lines and tabs and use a space instead.
    //$text = str_replace(["\n", "\r", "\t"], ' ', $text);

    // Trim left and right.
    $text = trim($text);

    // Remove all spaces that have more than one occurrence.
    //$text = preg_replace('!\s+!', ' ', $text);

    return $text;
  }

  
}

?>