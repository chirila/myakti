<?php
/************************************************************************
* @Author: MedeeaWeb Works
***********************************************************************/
class exact_online
{
	var $db;
	var $user;
	var $pass;
	var $api_key;
	var $access_token;
	var $refresh_token;
	var $database_name;
	var $connection;

	function exact_online(&$in,$dbase='')
	{
		global $database_config;
		if($dbase){
			$database_1 = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $dbase,
				);
				$this->db =  new sqldb($database_1); 
				$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();	
			$this->database_name=DATABASE_NAME;
		}	
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($database_2); 

		require __DIR__ . '/../../../libraries/exact-php-client/vendor/autoload.php';

		$app_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Exact online' AND app_type='accountancy' AND main_app_id='0' ");
		$this->api_key=$app_data->f('api');
		$this->access_token = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='access_token' ");
		$this->refresh_token = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='refresh_token' ");
		$this->expires_in = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='expires_in' ");
		$this->authorizationcode = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='authorizationcode' ");
		$this->user = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='username' ");
		$this->pass = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='password' ");



	}

	function connect()
	{
		global $config;

	    $connection = new \Picqer\Financials\Exact\Connection();
	    $connection->setRedirectUrl($config['exact_redirect_url']);
	    $connection->setExactClientId($config['exact_client_id']);
	    $connection->setExactClientSecret($config['exact_client_secret']);

	    $authorizationcode = $this->authorizationcode;
	    if ($authorizationcode) {
	        $connection->setAuthorizationCode($authorizationcode);
	    }
	    // Retrieves accesstoken from database
	    $access_token = $this->access_token;
	    if ($access_token) {
	        $connection->setAccessToken($access_token);
	    }

	    // Retrieves refreshtoken from database
	    $refresh_token = $this->refresh_token;

	    if ($refresh_token) {
	        $connection->setRefreshToken($refresh_token);
	    }

	    // Retrieves expires timestamp from database
	    $expires_in = $this->expires_in;
	    if ($expires_in) {
	        $connection->setTokenExpires($expires_in);
	    }

	    // Set callback to save newly generated tokens

	    $exact_obj=new exact_online($in, $this->database_name);

	    $connection->setAcquireAccessTokenLockCallback(array($exact_obj,'acquireAccessTokenLockCallback'));        

	    $connection->setTokenUpdateCallback(array($exact_obj, 'tokenUpdateCallback'));

	    $connection->setAcquireAccessTokenUnlockCallback(array($exact_obj,'acquireAccessTokenUnlockCallback'));

	   // var_dump($connection); exit();
	    // Make the client connect and exchange tokens
	    try {
	        $connection->connect();
	    } catch (\Exception $e) {
	        throw new Exception('Could not connect to Exact: ' . $e->getMessage());
	    }

	    return $connection;
	}

	function getValue($key)
	{
	    return $this->$key;
	}

	/**
	 * Function to persist some data for the example.
	 *
	 * @param string $key
	 * @param string $value
	 */
	function setValue($key, $value)
	{
	    $this->$key=$value;
	    if($key){
	    	$this->db->query("UPDATE apps SET api ='".$value."' WHERE main_app_id='".$app_data->f('app_id')."' AND type='".$key."'");
	    }
	    
	}

	function acquireAccessTokenLockCallback(\Picqer\Financials\Exact\Connection $connection)
	{

	    $this->db->query("SELECT GET_LOCK('exact-locked', 10)");
	    
	}

		function acquireAccessTokenUnlockCallback(\Picqer\Financials\Exact\Connection $connection)
	{

	    $this->db->query("SELECT RELEASE_LOCK('exact-locked')");
	    
	}

	/**
	 * Function to authorize with Exact, this redirects to Exact login promt and retrieves authorization code
	 * to set up requests for oAuth tokens.
	 */
	function authorize()
	{
		global $config;
	    $connection = new \Picqer\Financials\Exact\Connection();
	    $connection->setRedirectUrl($config['exact_redirect_url']);
	    $connection->setExactClientId($config['exact_client_id']);
	    $connection->setExactClientSecret($config['exact_client_secret']);
	    $connection->redirectForAuthorization();
	}

	/**
	 * Callback function that sets values that expire and are refreshed by Connection.
	 *
	 * @param \Picqer\Financials\Exact\Connection $connection
	 */
	function tokenUpdateCallback(\Picqer\Financials\Exact\Connection $connection)
	{
		//var_dump($connection->getAccessToken(),$connection->getRefreshToken(), $connection->getTokenExpires());exit();
		$app_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Exact online' AND app_type='accountancy' AND main_app_id='0' ");
	    // Save the new tokens for next connections
	    $this->access_token= $connection->getAccessToken();
	    if($this->access_token){
	    	$this->db->query("UPDATE apps SET api ='".$this->access_token."' WHERE main_app_id='".$app_data->f('app_id')."' AND type='access_token'");
	    }
	    $this->refresh_token= $connection->getRefreshToken();
	    if($this->refresh_token){
	    	$this->db->query("UPDATE apps SET api ='".$this->refresh_token."' WHERE main_app_id='".$app_data->f('app_id')."' AND type='refresh_token'");
	    }

	    // Save expires time for next connections
	    $this->expires_in= $connection->getTokenExpires();
	    $expires_time = $this->expires_in;
	     if($this->expires_in){
	    	$this->db->query("UPDATE apps SET api ='".$expires_time."' WHERE main_app_id='".$app_data->f('app_id')."' AND type='expires_in'");
	    }
	}


	function generate_pdf(&$in)
	{
	    include(__DIR__.'/../controller/invoice_print.php');
	    return $str;
	}


	function getItemExact($article_id = 0, $invoice_line = 0){
		/*$item_group = new \Picqer\Financials\Exact\ItemGroup($this->connection);
		$items_group=$item_group->filter("Code eq '700'");
		$item_group_id = $items_group[0]->ID;*/

		$item = new \Picqer\Financials\Exact\Item($this->connection);

		$result = array();
		if($article_id){

			if($invoice_line){
				$line = $this->db->query("SELECT * FROM tblinvoice_line WHERE id='".$invoice_line."' ")->getAll();
				$article_code = $line[0]['item_code'];
			}else{
				$article_code = $this->db->field("SELECT item_code FROM pim_articles WHERE article_id='".$article_id."' ");
			}
			
			$items=$item->filter("Code eq '$article_code'");

			if($items[0]->ID){
				$this->db->query("UPDATE pim_articles SET exact_id='".$items[0]->ID."' WHERE article_id='".$article_id."'");
				$ID = $items[0]->ID;
				$find_items=$item->filter("ID eq guid'$ID'");

				if($find_items[0]->ID){
					$result['ID'] = $find_items[0]->ID;
					$result['Code'] = $find_items[0]->Code;
				}
				
			}else{
				$art=$this->db->query("SELECT * FROM pim_articles WHERE article_id='".$article_id."' ")->getAll();
				
				$item->Code = trim($art[0]['item_code']);
				$item->CostPriceStandard = $art[0]['price'];
				$item->Description = $art[0]['internal_name'];
				/*$item->ItemGroup = $item_group_id;
				$item->ItemGroupCode = '700';*/
				//$item->IsSalesItem = 'true';

				try {
			        $item->save();
			    } catch (\Exception $e) {
			        $in['actiune']=gm('Fail:').' '. $e->getMessage();
			 		msg::error(' ','error');
			 		insert_error_call($this->database_name,"exact_online","invoice",$line[0]['invoice_id'],$in['actiune']);
			 		if($in['from_acc']){
						return true;
					}else{
						json_out($in);	
					}
			    }
				
				if($item->ID){
					$this->db->query("UPDATE pim_articles SET exact_id='".$item->ID."' WHERE article_id='".$article_id."'");
					$result['ID'] = $item->ID;
					$result['Code'] = $item->Code;
				}
			}
		}else if($invoice_line){
			$line = $this->db->query("SELECT * FROM tblinvoice_line WHERE id='".$invoice_line."' ")->getAll();

			/*$item->ItemGroup = $item_group_id;
			$item->ItemGroupCode = '700';*/
			$item->Code ='999999999';
			$item->CostPriceStandard = $line[0]['price'];
			$item->Description = $line[0]['name'];
			//$item->IsSalesItem = 'true';

			$out_item = new \Picqer\Financials\Exact\Item($this->connection);
			$out_items=$out_item->filter("Code eq '999999999'");

			if($out_items[0]->ID){
				$item->ID = $out_items[0]->ID;
				try {
			        $item->update();
			    } catch (\Exception $e) {
			        $in['actiune']=gm('Fail:').' '. $e->getMessage();
			 		msg::error(' ','error');
			 		insert_error_call($this->database_name,"exact_online","invoice",$line[0]['invoice_id'],$in['actiune']);
			 		if($in['from_acc']){
						return true;
					}else{
						json_out($in);	
					}
			    }
			}else{
				try {
			        $item->save();
			    } catch (\Exception $e) {
			        $in['actiune']=gm('Fail:').' '. $e->getMessage();
			 		msg::error(' ','error');
			 		insert_error_call($this->database_name,"exact_online","invoice",$line[0]['invoice_id'],$in['actiune']);
			 		if($in['from_acc']){
						return true;
					}else{
						json_out($in);	
					}
			    }
			}

			if($item->ID){
				$result['ID'] = $item->ID;
				$result['Code'] = $item->Code;
			}
		}

		return $result;
	}

	function get_property(object $object, string $property) {
	    $array = (array) $object;
	    $propertyLength = strlen($property);
	    foreach ($array as $key => $value) {
	        if (substr($key, -$propertyLength) === $property) {
	            return $value;
	        }
	    }
	}

	function exportExactOnline(&$in){
		global $config;	
	
		if(!$this->access_token){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],"Invalid credentials");
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}
		}

		$this->connection = $this->connect();

		$user_acc=$this->db_users->field("SELECT user_id FROM users WHERE database_name='".$this->database_name."' AND active=1000");
	
		$accountant=$this->db_users->field("SELECT first_name FROM users WHERE user_id='".$user_acc."' ");

		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

		$in['serial_number'] = $invoice->f('serial_number');
		$params = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'dbase'=>$this->database_name);
		if($invoice->f('pdf_layout')){
			$params['type']=$invoice->f('pdf_layout');
			$params['logo']=$invoice->f('pdf_logo');
		}else{
			$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
			$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		$str = $this->generate_pdf($params);
		$in['str'] = $str;

		$general_discount=$invoice->f('discount')/100;

		if($invoice->f('apply_discount') <2){
			$general_discount = 0;
		}
		
		$currency = currency::get_currency($invoice->f('currency_type'),'code');
		$invoice_type=$invoice->f('type')!='2' ? '8020' : '8021';
		$contact = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."' ");

		$customer_reference=$this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");

		$customer = $this->db->query("SELECT * FROM customers 
										INNER JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id AND customer_addresses.billing = 1
										WHERE customers.customer_id='".$invoice->f('buyer_id')."' ")->getAll();
		if(empty($customer)){
			$customer = $this->db->query("SELECT * FROM customers 
										LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id AND customer_addresses.is_primary = 1
										WHERE customers.customer_id='".$invoice->f('buyer_id')."' ")->getAll();
		}


		$account = new \Picqer\Financials\Exact\Account($this->connection);
		$account_code = str_pad($invoice->f('buyer_id'), 18,' ',STR_PAD_LEFT);
		$accounts=$account->filter("Code eq '$account_code'");

		if($accounts[0]->ID){
			$this->db->query("UPDATE customers SET exact_id='".$accounts[0]->ID."' WHERE customer_id='".$invoice->f('buyer_id')."'");
			$account_id = $accounts[0]->ID;
		}else{
			$account->AddressLine1 = $customer[0]['address'];
			$account->City = $customer[0]['city'];
			$account->Code = $customer[0]['customer_id'];
			//$account->Country = $customer[0]['country_id'];
			$account->CountryName = $customer[0]['country_name'];
			$account->IsSales = 'true';
			$account->Name = $customer[0]['name'];
			$account->Postcode = $customer[0]['zip'];
			$account->Status = 'C';
			$account->VATNumber = $customer[0]['btw_nr'];
			$account->Email = $customer[0]['c_email'];
			$account->Phone = $customer[0]['comp_phone'];
			$account->Website = $customer[0]['website'];
			$account->ChamberOfCommerce = $customer[0]['company_number'];


			try {
				$account->save();

				if($account->ID){
					$this->db->query("UPDATE customers SET exact_id='".$account->ID."' WHERE customer_id='".$invoice->f('buyer_id')."'");
					$account_id = $account->ID;
				}
		    } catch (\Exception $e) {
		    	$in['actiune']=gm('Fail:'). $e->getMessage();
		 		msg::error(' ','error');
		 		insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
		 		if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
		    }		
		}
		
		$journal = new \Picqer\Financials\Exact\Journal($this->connection);

		try {
			$journals=$journal->filter("Type eq 20");
			$journal_code = $journals[0]->Code;
	    } catch (\Exception $e) {
	    	$in['actiune']=gm('Fail:'). $e->getMessage();
	 		msg::error(' ','error');
	 		insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
	 		if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}
	    }

	    $in['exact_account_id'] = $account_id;

    	$salesInvoice = new \Picqer\Financials\Exact\SalesInvoice($this->connection);
		$salesInvoice->InvoiceTo = $account_id;
		$salesInvoice->OrderedBy = $account_id;
		$salesInvoice->YourRef = $invoice->f('serial_number');
		$salesInvoice->Journal = $journal_code;
		$salesInvoice->Type = $invoice_type;
		$salesInvoice->Currency = $currency;
		$salesInvoice->Discount = $general_discount;

	/*	
		$salesInvoice->DueDate =  date( "Y-m-d", $invoice->f('due_date') ) ;
		$salesInvoice->PaymentReference = $invoice->f('ogm');*/

		
		$general_ledger =  $this->db->field("SELECT value FROM settings WHERE constant_name='EXACT_LEDGER_ID' ");

		if($general_ledger){
	    	$ledger = new \Picqer\Financials\Exact\GLAccount($this->connection);
			try {
				$ledgers=$ledger->filter("Code eq '$general_ledger' ");
				$general_ledger_id = $ledgers[0]->ID;
		    } catch (\Exception $e) {
		    	$in['actiune']=gm('Fail:'). $e->getMessage();
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
	 			if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
		    }
	    }
		

		$i = 0;
		$total_amount = 0;
		$vat_percent = array();
		$sub_t = array();
		$sub_disc = array();
		$vat_percent_val = 0;
		$discount = $invoice->f('discount');
		$invoiceLines = array();

		$get_invoice_rows = $this->db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");

		while ($get_invoice_rows->next()){
			$line_d = $get_invoice_rows->f('discount');
			if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
				$line_d = 0;
			}
			$amount = $get_invoice_rows->f('amount') - $get_invoice_rows->f('amount') * $line_d /100;
			$price_with_discount = $get_invoice_rows->f('price') - $get_invoice_rows->f('price') * $line_d /100;
			$net_price = $price_with_discount + $price_with_discount*$get_invoice_rows->f('vat')/100;
			$vat_amount_dc = $price_with_discount*$get_invoice_rows->f('vat')/100;

			if($invoice->f('vat_regime_id') < 2){
					$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats INNER JOIN vats ON exactvats.vat_id = vats.vat_id WHERE vats.value='".$get_invoice_rows->f('vat')."' ");
					if(!$vat_type){
						$vat_old=$this->db->field("SELECT vat_new.id FROM vat_new 
							INNER JOIN vats ON vat_new.vat_id=vats.vat_id
							WHERE vats.value='".$get_invoice_rows->f('vat')."' AND vat_new.regime_type='1' ");
						if($vat_old){
							$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats WHERE exactvats.vat_id='".$vat_old."' ");
							if(!$vat_type){
								$vat_type=0;
							}
						}else{
							$vat_type=0;
						}
					}
					
				}else if($invoice->f('vat_regime_id') < 5){
					$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats WHERE exactvats.vat_id='r".$invoice->f('vat_regime_id')."' ");
					if(!$vat_type){
						$new_reg=$this->db->field("SELECT id FROM vat_new WHERE vat_regime_id='r.".$invoice->f('vat_regime_id')."' ");
						if($new_reg){
							$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats WHERE exactvats.vat_id='".$new_reg."' ");
							if(!$vat_type){
								$vat_type = 0;
							}
						}else{
							$vat_type = 0;
						}		
					}
				}else{

					$regime_type=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
					if($regime_type=='1'){
						$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats INNER JOIN vats ON exactvats.vat_id = vats.vat_id WHERE vats.value='".$get_invoice_rows->f('vat')."' ");
						if(!$vat_type){
							$vat_old=$this->db->field("SELECT vat_new.id FROM vat_new 
								INNER JOIN vats ON vat_new.vat_id=vats.vat_id
								WHERE vats.value='".$get_invoice_rows->f('vat')."' AND vat_new.regime_type='1' ");
							if($vat_old){
								$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats WHERE exactvats.vat_id='".$vat_old."' ");
								if(!$vat_type){
									$vat_type=0;
								}
							}else{
								$vat_type=0;
							}
						}
						
					}else{
						$exact_vat_reg=$this->db->field("SELECT exact_vat_id FROM exactvats WHERE exactvats.vat_id='".$invoice->f('vat_regime_id')."'  ");

						if($exact_vat_reg){
							$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats WHERE exactvats.vat_id='".$invoice->f('vat_regime_id')."' ");
							if(!$vat_type){
								$vat_type = 0;
							}
						}else{
							$old_vat_reg=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
							if($old_vat_reg){
								$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats WHERE exactvats.vat_id='r".$old_vat_reg."' ");
								if(!$vat_type){
									$vat_type = $this->db->field("SELECT exactvats.value FROM exactvats WHERE exactvats.vat_id='".$invoice->f('vat_regime_id')."' ");
									if(!$vat_type){
										$vat_type=0;
									}
								}
							}
						}
						
					}			
				}

			$vat = $vat_type;

			/*$vat_code = new \Picqer\Financials\Exact\VatCode($this->connection);
			$vat_percentage = twoDecNumber($get_invoice_rows->f('vat')/100);
			try {

				$vat_codes=$vat_code->filter("Percentage eq $vat_percentage and ( VATTransactionType eq 'B' or VATTransactionType eq 'S' ) ");
				$vat = $vat_codes[0]->Code;
				$vat_ID = $vat_codes[0]->ID;
		    } catch (\Exception $e) {
		    	$in['actiune']=gm('Fail:'). $e->getMessage();
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
		        //throw new Exception('Error: ' . $e->getMessage());
		    }*/
		    //var_dump($vat_codes[0], $vat_percentage);
/*
		    if(!$vat_ID){
		    	$vatCode = new \Picqer\Financials\Exact\VatCode($this->connection);
		    	$vatCode->Code = $get_invoice_rows->f('vat');
		    	$vatCode->Description = $get_invoice_rows->f('vat').'%';
		    	$vatCode->VATTransactionType = 'B';
		    	$vatCode->Type = 'I';

		    	$vatCode->VATPercentages =  array(array('Percentage' => floatval($get_invoice_rows->f('vat'))/100,	 'Type' => '0' ));

		    	try {
					$vatCode->save();
					$vat = $vatCode->Code;
			    } catch (\Exception $e) {
			        throw new Exception('Error: ' . $e->getMessage());
			    }
		    }*/

		    $ledger_code='';
			if($get_invoice_rows->f('article_id')){
				$ledger_account_id=$this->db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' ");
				if($ledger_account_id){
					$ledger_code=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$ledger_account_id."' ");
				}
			}

		    if($ledger_code){
		    	$ledger = new \Picqer\Financials\Exact\GLAccount($this->connection);
				try {
					$ledgers=$ledger->filter("Code eq '$ledger_code' ");
					$ledger_id = $ledgers[0]->ID;
			    } catch (\Exception $e) {
			    	$in['actiune']=gm('Fail:'). $e->getMessage();
		 			msg::error(' ','error');
		 			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
		 			if($in['from_acc']){
						return true;
					}else{
						json_out($in);	
					}
			    }
		    }else{
		    	$ledger_id = $general_ledger_id;
		    }
			

			$item = array();
			$item = $this->getItemExact($get_invoice_rows->f('article_id'), $get_invoice_rows->f('id'));

			$line = array(
				'ItemCode'      => $item['Code'],
				'Item' 			=> $item['ID'],
				'Quantity'  	=> $get_invoice_rows->f('quantity'),
				'UnitPrice' 	=> $get_invoice_rows->f('price'),
				'Discount'  	=> $line_d/100,
				'VATCode' 		=> $vat,
				'VATAmountDC'	=> $vat_amount_dc,
				'GLAccount' 	=> $ledger_id
			);

			array_push($invoiceLines, $line);		
			
		}	
	

		$salesInvoice->SalesInvoiceLines = $invoiceLines;
		try {
			$salesInvoice->save();
			$exact_id = $salesInvoice->InvoiceID;
	 		if($exact_id){
	 			$in['exact_invoice_id'] =$exact_id;
	 			//$this->addExactXMLAttachment($in);
	 			$this->addExactPDFAttachment($in);
	 			$this->db->query("UPDATE tblinvoice SET exact_id='".$exact_id."' WHERE id='".$in['invoice_id']."' ");
		 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', exact_online='1' ");
		 		if(array_key_exists('invoice_to_export', $_SESSION) === true){
		 			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		 		}
			 	if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
		 		$in['actiune']=gm("Invoice exported successfully");
		 		if($in['from_acc']){
			 		$msg = '{l}Invoice was exported automatically to{endl} '.'Exact Online ';
			 	}else{
			 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Exact Online ';
			 	}
		 		insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

		 		if($_SESSION['main_u_id']=='0'){
					//$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");         
					$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
				}else{
					$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
				}
				if($is_accountant){
					$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
					if($accountant_settings=='1'){
					    $acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
					    if($acc_block_id=='1'){
					    	$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
					    }
					}else{
						$block_id=$this->db_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
						if($block_id==1){
						    $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
						}
					}		
				}
	 		}
	    } catch (\Exception $e) {
	 		$in['actiune']=gm('Fail:'). $e->getMessage();
	 		msg::error(' ','error');
	 		insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
	 		if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}

	    }
				
	 	unlink($attach_file_name);
	 	if($in['from_acc']){
	 		return true;
	 	}else{
	 		json_out($in);
	 	}
	}


	function addExactXMLAttachment(&$in){

		global $config;

		if(!$this->access_token){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],"Invalid credentials");
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}
		}

		//$this->connection = $this->connect();

		$user_acc=$this->db_users->field("SELECT user_id FROM users WHERE database_name='".$this->database_name."' AND active=1000");

		$accountant=$this->db_users->field("SELECT first_name FROM users WHERE user_id='".$user_acc."' ");
		$vat_number=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");

		$vat_number=trim($vat_number," ");
		$vat_number_base=substr($vat_number,0,2);
		$vat_number=substr($vat_number,2);
		$vat_number=str_replace(" ","",$vat_number);
		$vat_number=str_replace(".","",$vat_number);
		$vat_number=str_replace(",","",$vat_number);
		$vat_number=str_replace("-","",$vat_number);
		$vat_number=str_replace("_","",$vat_number);
		$vat_number=str_replace("/","",$vat_number);
		$vat_number=str_replace("|","",$vat_number);

		$ACCOUNT_ADDRESS_INVOICE=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
		$ACCOUNT_DELIVERY_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
		$ACCOUNT_DELIVERY_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
		$ACCOUNT_DELIVERY_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
		$ACCOUNT_DELIVERY_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
		$ACCOUNT_BILL_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
		$ACCOUNT_BILL_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
		$ACCOUNT_BILL_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
		$ACCOUNT_BILL_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");

		$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
		$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
		$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
		$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;

		//$str = '';
		
		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$invoice_type=$invoice->f('type')<2 ? '8020' : '8021';

		$currency = currency::get_currency($invoice->f('currency_type'),'code');

		$in['serial_number'] = $invoice->f('serial_number');

		$i = 0;
		$total_amount = 0;
		$vat_percent = array();
		$sub_t = array();
		$sub_disc = array();
		$vat_percent_val = 0;
		$discount = $invoice->f('discount');
		$invoiceLines = array();
		$xml_lines='';

		$get_invoice_rows = $this->db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");

		while ($get_invoice_rows->next()){
			$i++;
			$line_d = $get_invoice_rows->f('discount');
			if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
				$line_d = 0;
			}
			$amount = $get_invoice_rows->f('amount') - $get_invoice_rows->f('amount') * $line_d /100;
			$price_with_discount = $get_invoice_rows->f('price') - $get_invoice_rows->f('price') * $line_d /100;
			$net_price = $price_with_discount + $price_with_discount*$get_invoice_rows->f('vat')/100;
			$vat_amount_dc = $price_with_discount*$get_invoice_rows->f('vat')/100;
			$net_price_quantity = $net_price * $get_invoice_rows->f('quantity') ;

			$vat_percentage = twoDecNumber($get_invoice_rows->f('vat')/100);

			$item_code_xml = '';
			if(htmlspecialchars($get_invoice_rows->f('item_code'))){
				$item_code_xml = '<Item  code="'.htmlspecialchars($get_invoice_rows->f('item_code')).'">
					<Description>'.htmlspecialchars($get_invoice_rows->f('name')).'</Description>
				</Item>';
			}

			$ledger_xml='';
			if($get_invoice_rows->f('article_id')){
				$ledger_account_id=$this->db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' ");
				if($ledger_account_id){
					$ledger_xml=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$ledger_account_id."' ");
				}
			}

			$vat_code = new \Picqer\Financials\Exact\VatCode($this->connection);

			try {

				$vat_codes=$vat_code->filter("Percentage eq $vat_percentage and ( VATTransactionType eq 'B' or VATTransactionType eq 'S' ) ");
				$vat = $vat_codes[0]->Code;
				$vat_ID = $vat_codes[0]->ID;
				$vat_desc = $vat_codes[0]->Description;
		    } catch (\Exception $e) {
		    	$in['actiune']=gm('Fail:'). $e->getMessage();
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
		        //throw new Exception('Error: ' . $e->getMessage());
		    }

		    $line_disc = $line_d /100;

			$line_xml = '<InvoiceLine line="'.$i.'">
				<Description>'.htmlspecialchars($get_invoice_rows->f('name')).'</Description>
				'.$item_code_xml.'
				<Quantity>'.$get_invoice_rows->f('quantity').'</Quantity>
				<Unit code="pc">
					<Description termid="56916">Piece</Description>
				</Unit>
				<UnitPrice>
					<Currency code="'.$currency.'" />
					<Value>'.$get_invoice_rows->f('price').'</Value>
					<VAT code="'.$vat.'">
						<Description >'.$vat_desc.'</Description>
					</VAT>
					<VATPercentage>'.$vat_percentage.'</VATPercentage>
				</UnitPrice>
				<ForeignAmount>
					<Currency code="'.$currency.'" />
					<Value>'.$net_price_quantity.'</Value>
					<Rate>1</Rate>
					<VATBaseAmount>'.$net_price_quantity.'</VATBaseAmount>
					<VATAmount>'.$vat_amount_dc.'</VATAmount>
				</ForeignAmount>
				<DiscountPercentage>'.$line_disc.'</DiscountPercentage>
				<GLAccount code="700000">
					<Description termid="161561550">Sales &amp; Provided services</Description>
				</GLAccount>
			</InvoiceLine>';

			$xml_lines.=$line_xml;	
			
		}

		
		$xml ='<Invoice type="'.$invoice_type.'" status="20">
			<Journal code="700">
				<Description termid="90003">Sales</Description>
			</Journal>
			<OrderDate></OrderDate>
			<InvoiceDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</InvoiceDate>
			<DueDate>'.date('Y-m-d',$invoice->f('due_date')).'</DueDate>
			<YourRef>'.$invoice->f('serial_number').'</YourRef>
			<OrderedBy code="'.$invoice->f('buyer_id').'">
				<Name>'.htmlspecialchars($invoice->f('buyer_name')).'</Name>
			</OrderedBy>
			<DeliverTo  code="'.$invoice->f('buyer_id').'">
				<Name>'.htmlspecialchars($invoice->f('buyer_name')).'</Name>
			</DeliverTo>
			<DeliveryAddress AddressLine1="bank" AddressLine2="iban" AddressLine3="" PostalCode="28610" City="MADRID" StateCode="" CountryCode="BE" />
			<InvoiceTo code="'.$invoice->f('buyer_id').'">
				<Name>'.htmlspecialchars($invoice->f('buyer_name')).'</Name>
			</InvoiceTo>
			<PaymentCondition code="1">
				<Description termid="90010">Payment on delivery</Description>
			</PaymentCondition>
			<ShippingMethod code="">
				<Description />
			</ShippingMethod>
			<EntryDiscount>
				<AmountInclVAT>0</AmountInclVAT>
				<AmountExclVAT>0</AmountExclVAT>
				<Percentage>0</Percentage>
			</EntryDiscount>
			<Document>
				<Subject>Invoice: '.$invoice->f('serial_number').'</Subject>
				<DocumentType number="10" />
				<Attachments>
					<Attachment>
						<Name>'.$invoice->f('serial_number').'.PDF</Name>
						<BinaryData>'.$in['str'].'</BinaryData>
					</Attachment>
				</Attachments>
			</Document>
			'.$xml_lines.'
		</Invoice>
			';
		
		
		$document = new \Picqer\Financials\Exact\Document($this->connection);
		$document->Subject = gm('XML file for invoice').': '.$invoice->f('serial_number');
		$document->Account = $in['exact_account_id'];
		
		$document->FinancialTransactionEntryID = $in['exact_invoice_id'];
		$document->HID = $invoice->f('id');
		$document->Type = '10';
		$document->Currency = $currency;
		$document->Language = $this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$invoice->f('email_language')."' ");
		$document->SalesInvoiceNumber = $invoice->f('id');

		try {

				$document->save();
				$document_id = $document->ID;
				$in['exact_doc_id'] = $document_id;
				if($document_id){
					$document_attach = new \Picqer\Financials\Exact\DocumentAttachment($this->connection);
					$document_attach->Attachment = base64_encode($xml);
					$document_attach->Document = $document_id;
					$document_attach->FileName = $invoice->f('serial_number').'.XML';
					$document_attach->save();

				}
	 			
		    } catch (\Exception $e) {
		    	$in['actiune']=gm('Fail:'). $e->getMessage();
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
		    }
		


	 	unlink($attach_file_name);
	 	
	 	return true;

	}

	function addExactPDFAttachment(&$in){

		global $config;

		//$this->connection = $this->connect();	

		//$str = '';
		
		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$invoice_type=$invoice->f('type')<2 ? '8020' : '8021';

		$currency = currency::get_currency($invoice->f('currency_type'),'code');

		$in['serial_number'] = $invoice->f('serial_number');


		if(!$in['str']){
			msg::error(gm('PDF required'),'error');
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
			}
		}

		try {
				$document_attach = new \Picqer\Financials\Exact\DocumentAttachment($this->connection);
					$document_attach->Attachment = $in['str'];
					$document_attach->FileName = $invoice->f('serial_number').'.PDF';
				if($in['exact_doc_id']){
					$document_attach->Document = $in['exact_doc_id'];
					$document_attach->save();
				}else{
					$document = new \Picqer\Financials\Exact\Document($this->connection);
					$document->Subject = gm('PDF file for invoice').': '.$invoice->f('serial_number');
					$document->Account = $in['exact_account_id'];
					
					$document->FinancialTransactionEntryID = $in['exact_invoice_id'];
					$document->HID = $invoice->f('id');
					$document->Type = '10';
					$document->Currency = $currency;
					$document->Language = $this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$invoice->f('email_language')."' ");
					$document->SalesInvoiceNumber = $invoice->f('id');
					$document->save();
					$document_id = $document->ID;
					if($document_id){
						$document_attach->Document = $document_id;
						$document_attach->save();
					}
				}
				
	 			
		    } catch (\Exception $e) {
		    	$in['actiune']=gm('Fail:'). $e->getMessage();
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],$in['actiune']);
	 			if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
		    }

	 	unlink($attach_file_name);
	 	return true;
	}



	function checkPay(&$in)
	{ 
		global $config;

		if(!$this->access_token){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			insert_error_call($this->database_name,"exact_online","invoice",$in['invoice_id'],"Invalid credentials");
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}
		}

		$invs = $this->db->query("SELECT id,serial_number FROM tblinvoice WHERE f_archived='0' and tblinvoice.status = '0' and tblinvoice.sent = '1' AND (type='0' OR type='3') and tblinvoice.exact_id != '' and tblinvoice.paid != '1' ORDER BY due_date ASC ");
		while ($invs->next()) {
			$inv[$invs->f('id')] = $invs->f('serial_number');
		}
		if(!empty($inv)){
			$this->connection = $this->connect();
			foreach ($inv as $key => $value) {
				$exact_id = $this->db->field("SELECT exact_id FROM tblinvoice WHERE id='".$key."' ");
				$exact_invoice ='';
				$inv = new \Picqer\Financials\Exact\SalesInvoice($this->connection);
	
				$invoice=$inv->filter("InvoiceID eq guid'$exact_id'");
				if($invoice[0]->InvoiceID){
					$exact_invoice = $invoice[0]->InvoiceNumber;
				}
				if($exact_invoice){
					$receivable = new \Picqer\Financials\Exact\Receivable($this->connection);
	
					$receivables=$receivable->filter("Status eq 50 and InvoiceNumber eq $exact_invoice", '', 'ID,InvoiceNumber, InvoiceDate, AmountDC, Status, TransactionType, IsFullyPaid');
					$receives_arr = (array) $receivables;

					foreach($receives_arr as $key2 => $value2){
			
						$attr = $this->get_property($value2, 'attributes');
						$in['invoice_id'] = $key;
						$in['exact_payment_id'] = $attr['ID'];
						if($attr['IsFullyPaid']){
							$amount = $this->get_payment($in);													
						}else{
							$amount = $attr['AmountDC'];
						}

						$inv_up=array();
						array_push($inv_up,array('invoice_id'=>$key,'amount'=>display_number($amount)));
						$this->marked_payed($inv_up);
					}
				}
				
			}
		}
		if($in['from_acc']){
			return true;
		}else{
			json_out($in);	
		}
	
	}

	function get_payment(&$in){
		$tblinvoice=$this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$subtotal_vat = 0;
		$total_n = 0;
		$show_disc = false;
		$negative = 1;
	    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
	    if($use_negative==1){
	      $negative = -1;
	    }

	    $big_discount = $tblinvoice->f("discount");
	    if($tblinvoice->f('apply_discount') < 2){
	      $big_discount =0;
	    }
		$lines = $this->db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
		
		while ($lines->next()) {
			$line_discount = $lines->f('discount');
			if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
				$line_discount = 0;
			}
			$amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
			$amount_line_disc = $amount_line*$big_discount/100;
			$discount_value += $amount_line_disc;
			$subtotal_vat +=  ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
			$total_n += $amount_line - $amount_line_disc;
		}

		if($tblinvoice->f('discount') != 0 || $show_disc === true){
			$o['show_disc']=$show_disc;
		}

		if($tblinvoice->f('quote_id') && $tblinvoice->f('downpayment_drawn')){
			$total = $total_n+$subtotal_vat-$tblinvoice->f('downpayment_value');
		}else{
			$total = $total_n+$subtotal_vat;
		}

		//already payed
		$proforma_id=$this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
	    $payments_filter=" invoice_id='".$in['invoice_id']."'";
	    if($proforma_id){
	      $payments_filter.=" OR invoice_id='".$proforma_id."'";
	    }

		$already_payed1 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter." AND credit_payment='0' ");
		$already_payed1->move_next();

		$already_payed2 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter." AND credit_payment='1' ");
		$already_payed2->move_next();


		$total_payed = $already_payed1->f('total_payed')+($negative*$already_payed2->f('total_payed'));

		$req_payment_value=$total* $tblinvoice->f('req_payment')/100;
		if($tblinvoice->f('req_payment') == 100){
			$amount_due = round($total - $total_payed,2);
		}else{
			$amount_due = round($req_payment_value - $total_payed ,2);
		}
	
		return $amount_due;
	}

	function marked_payed($data){
		foreach($data as $key=>$value){
      		$start_payment_date = mktime(23,59,59,date('n'),date('j'),date('y'));
		
      		$total = 0;
    		$tblinvoice=$this->db->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE id='".$value['invoice_id']."'");
    		$tblinvoice->next();
    		$currency = get_commission_type_list($tblinvoice->f('currency_type'));
    		$big_discount = $tblinvoice->f("discount");
    		if($tblinvoice->f('apply_discount') < 2){
      			$big_discount =0;
    		}
    		$lines = $this->db->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$value['invoice_id']."' ");
		    while ($lines->next()) {
		      $line_disc = $lines->f('discount');
		      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
		        $line_disc = 0;
		      }
		      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
		      $line = $line - $line * $big_discount / 100;

		      $total += $line + ($line * ( $lines->f('vat') / 100));

		    }
		    //already payed
		    if($tblinvoice->f('proforma_id')){
		      $filter = " OR invoice_id='".$tblinvoice->f('proforma_id')."' ";
		    }
		    $tot_payed=$this->db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='0' ");
		    $total_payed = $tot_payed;

		    $negative = 1;
		    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
		    if($use_negative==1){
		      $negative = -1;
		    }
    		$this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='1' ");
		    $this->db->move_next();
		    $total_payed += ($negative*$this->db->f('total_payed'));

		    $req_payment_value = $total;
		    if($tblinvoice->f('type')==1){
		      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
		    }
		    $amount_due = round($req_payment_value - $total_payed,2);
		    $general_amount = return_value($value['amount']);

    		$payment_date=date("Y-m-d",$start_payment_date);
    		$this->db->query("UPDATE tblinvoice SET not_paid='0' WHERE id='".$value['invoice_id']."'");

		    //Mark as payed
		    if($general_amount == $amount_due || $general_amount>$amount_due){
		      $this->db->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$value['invoice_id']."'");
		      $p = $this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$value['invoice_id']."' ");
		      if($p){
		        $this->db->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
		      }
		    }
		    else {
		      $this->db->query("UPDATE tblinvoice SET paid='2' WHERE id='".$value['invoice_id']."'");
		    }
		    $date_format=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DATE_FORMAT' ");

		    $payment_view_date=date($date_format,$start_payment_date);
		    $info=$payment_view_date.'  -  '.place_currency(display_number($general_amount),$currency).' - Exact Online';

    		$this->db->query("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$value['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$general_amount."',
                          exact_payment_id ='".$in['exact_payment_id']."',
                          info        = '".$info."' ");
    		$this->db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Payment has been successfully recorded by {endl} Exact Online Cron')."', field_name='invoice_id', field_value='".$value['invoice_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
    		 $this->db->query("UPDATE tblinvoice SET exact_online_paid='1' WHERE id='".$value['invoice_id']."'");
			//$this->dbu_users->query("INSERT INTO cron_clearfacts SET `database_name`='".$this->database_name."',invoice_id='".$value['invoice_id']."'  ");
		}
	}

	function get_payment_inc(&$in){
		$total=$this->db->field("SELECT total_with_vat FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");


		$already_payed1 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' AND credit_payment='0' ");
		$already_payed1->move_next();

		$already_payed2 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' AND credit_payment='1' ");
		$already_payed2->move_next();

		$negative = 1;
	    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
	    if($use_negative==1){
	      $negative = -1;
	    }

		$total_payed = $already_payed1->f('total_payed')+($negative*$already_payed2->f('total_payed'));
		$amount_due = round($total - $total_payed,2);
	
		return $amount_due;
	}

	function marked_payed_inc($data){
		foreach($data as $key=>$value){
      		$start_payment_date = mktime(23,59,59,date('n'),date('j'),date('y'));
		
      		$total = 0;
      		
    		$tblinvoice=$this->db->query("SELECT currency_type, total_with_vat FROM tblinvoice_incomming WHERE invoice_id='".$value['invoice_id']."'");
    		$tblinvoice->next();
    		$currency = get_commission_type_list($tblinvoice->f('currency_type'));
    		$total = $tblinvoice->f('total_with_vat');
		    $total_payed=$this->db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$value['invoice_id']."'  AND credit_payment='0' ");
	
		    $negative = 1;
		    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
		    if($use_negative==1){
		      $negative = -1;
		    }
    		$this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$value['invoice_id']."'  AND credit_payment='1' ");
		    $this->db->move_next();
		    $total_payed += ($negative*$this->db->f('total_payed'));

		    $amount_due = round($total - $total_payed,2);
		    $general_amount = return_value($value['amount']);

    		$payment_date=date("Y-m-d",$start_payment_date);

		    //Mark as payed
		    if($general_amount == $amount_due || $general_amount>$amount_due){
		      $this->db->query("UPDATE tblinvoice_incomming SET paid='1',status='1' WHERE invoice_id='".$value['invoice_id']."'");
		    }
		    else {
		      $this->db->query("UPDATE tblinvoice_incomming SET paid='2',status='1' WHERE invoice_id='".$value['invoice_id']."'");
		    }
		    $date_format=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DATE_FORMAT' ");

		    $payment_view_date=date($date_format,$start_payment_date);
		    $info=$payment_view_date.'  -  '.place_currency(display_number($general_amount),$currency).' - Exact Online';

    		$this->db->query("INSERT INTO tblinvoice_incomming_payments SET
                          invoice_id  = '".$value['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$general_amount."',
                          exact_payment_id ='".$in['exact_payment_id']."',
                          info        = '".$info."' ");
    		$this->db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Payment has been successfully recorded by {endl} Exact Online Cron')."', field_name='invoice_id', field_value='".$value['invoice_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
    		 $this->db->query("UPDATE tblinvoice_incomming SET exact_online_paid='1' WHERE invoice_id='".$value['invoice_id']."'");
			//$this->dbu_users->query("INSERT INTO cron_clearfacts SET `database_name`='".$this->database_name."',invoice_id='".$value['invoice_id']."'  ");
		}
	}

}
?>