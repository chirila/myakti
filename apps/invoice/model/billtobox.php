<?php
/************************************************************************
* @Author: MedeeaWeb Works
***********************************************************************/
class billtobox
{
	var $db;
	var $user;
	var $pass;
	var $arUrl;
	var $apUrl;
	var $database_name;

	function billtobox(&$in,$dbase='')
	{
		global $database_config;
		if($dbase){
			$database_1 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $dbase,
			);
			$this->db = new sqldb($database_1);
			$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();
			$this->database_name=DATABASE_NAME;
		}		
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($database_2); // recurring billing
		$this->send_sales_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='EXPORT_SEND_INVOICE_BILLTOBOX'");
		$app =array();
		$app_id=$this->db->field("SELECT app_id FROM apps WHERE name='BillToBox' AND app_type='accountancy'");
		$actions = array('username','password','active_sales_api_key');
		foreach ($actions as $key => $value) {
			$app[$value] = $this->db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='".$value."' ");
		}
		$this->user = $app['username'];
		//$this->user_ap = 'aktiTest';
		$this->api_key = $this->send_sales_invoice == '1' ? $app['active_sales_api_key'] : $app['password'];
	}

	function markExport(&$in)
	{
		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', type='".$in['type']."', ".$in['app']."=1 ");
		insert_message_log('invoice','{l}Invoice was exported by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		return true;
	}

	function unmarkExport(&$in)
	{
		$this->db->query("DELETE FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND ".$in['app']."=1 AND type='".$in['type']."' ");
		insert_message_log('invoice','{l}Invoice was put back on export on list by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		return true;
	}

	function generate_pdf(&$in)
	  {
	    if($in['type_pdf']==2){
	      include(__DIR__.'/../controller/invoice_credit_print.php');
	    }else{
	      include(__DIR__.'/../controller/invoice_print.php');
	    }
	    return $str;
	  }

	function formatVAT($vat)
	{
		$vat = str_replace(array('.',' ','-','_'), '', $vat);
		return $vat;
	}

	function exportBillToBox(&$in){
		global $config;
		/*if(!DATABASE_NAME){
			define(DATABASE_NAME,$this->db->field("SELECT DATABASE()"));
		}*/
		if(!$this->user || !$this->api_key){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			insert_error_call($this->database_name,"billtobox","invoice",$in['invoice_id'],"Invalid credentials");
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}
		}

		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$ACCOUNT_VAT_NUMBER=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");
		$params = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'dbase'=>$this->database_name);
		if($invoice->f('type')=='2'){
			if($invoice->f('pdf_layout')){
				$params['type']=$invoice->f('pdf_layout');
				$params['logo']=$invoice->f('pdf_logo');
			}else{
				$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			#if we are using a customer pdf template
			if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
				$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			$params['type_pdf']=2;
			$params['type']=ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
			$in['efff_str'] = $this->generate_pdf($params);
		}else{
			if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==0){
				$params['type']=$invoice->f('pdf_layout');
				$params['logo']=$invoice->f('pdf_logo');
			}else if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==1){
				$params['custom_type']=$invoice->f('pdf_layout');
				$params['logo']=$invoice->f('pdf_logo');
			}else{
				$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			#if we are using a customer pdf template
			if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
				$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			$in['efff_str'] = $this->generate_pdf($params);
		}
		$in['billtobox_xml']=true;
		$in['dbase']=$this->database_name;
		$in['id']=$in['invoice_id'];
		include(__DIR__.'/../controller/xml_invoice_print.php');
      	$xml_tmp_file_name=__DIR__."/../../../file_".$invoice->f('serial_number').".xml";
      	
      	$billtobox_base_url = $this->send_sales_invoice == '1' ? $config['billtobox_base_url_active'] : $config['billtobox_base_url'];
      	if($this->database_name =='4a5f9937_4f34_21bb_631eb43c4876' || $this->database_name =='salesassist_2'){
      		$billtobox_base_url = $config['billtobox_base_url_test'];
		}
		$data=array(
			'file'				=> new CURLFile($xml_tmp_file_name,'application/xml')
			//'file'				=> '@'.$xml_tmp_file_name
			);

		$ch = curl_init();
    	$headers=array('Content-Type: multipart/form-data');
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $this->user.":".$this->api_key);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_URL, $billtobox_base_url);

		$put = curl_exec($ch);
	 	$info = curl_getinfo($ch);

	 	if($info['http_code']>300 || $info['http_code']==0){
	 		$resp_error=json_decode($put, true);
	 		$in['actiune']=gm('Fail:');
			msg::error($resp_error['error'],'error');
	 		insert_error_call($this->database_name,"billtobox","invoice",$in['invoice_id'],$in['actiune'].$resp_error['error']);
	 	}else{
	 		$billtobox_data=json_decode($put,true);
	 		
	 		
	 		$billtobox_id = $billtobox_data['item']['id'];
	 		if($billtobox_id){
	 			//$this->db->query("UPDATE tblinvoice SET billtobox_id='".$billtobox_id."' WHERE id='".$in['invoice_id']."' ");
		 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', billtobox='1',type=0 ");
		 		if(array_key_exists('invoice_to_export', $_SESSION) === true){
		 			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		 		}
			 	if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
		 		$in['actiune']=gm("Invoice exported successfully");

		 		if($in['from_acc']){
			 		$msg = '{l}Invoice was exported automatically to{endl} '.'BillToBox ';
			 	}else{
			 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'BillToBox ';
			 	}
		 		
		 		insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

		 		if($_SESSION['main_u_id']=='0'){
					//$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");         
					$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
				}else{
					$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
				}
				if($is_accountant){
					$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
					if($accountant_settings=='1'){
					    $acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
					    if($acc_block_id=='1'){
					    	$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
					    }
					}else{
						$block_id=$this->db_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
						if($block_id==1){
						    $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
						}
					}		
				}
	 		}else{ 		
	 			$in['actiune']=$billtobox_data['errors'];
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"billtobox","invoice",$in['invoice_id'],$in['actiune']);
	 		}
	 		
	 	}
	 	unlink($xml_tmp_file_name);
	 	if($in['from_acc'] || $in['return_data']){
	 		return true;
	 	}else{
	 		json_out($in);
	 	}
	}

	function exportIncBillToBox(&$in){
		global $config;
		/*if(!DATABASE_NAME){
			define(DATABASE_NAME,$this->db->field("SELECT DATABASE()"));
		}*/
		if(!$this->user || !$this->api_key){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			insert_error_call($this->database_name,"billtobox","invoice",$in['invoice_id'],"Invalid credentials");
			json_out($in);
		}

		$id = $this->createIncFiles($in);

		if($id === false){
			msg::error(gm('Could not create file'),'error');
			json_out($in);
		}

		$invoice = $this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");

	    $e = str_replace('/', '', $invoice->f('booking_number'));

      	$xml_tmp_file_name=__DIR__."/../../../file_inc_".$e.".xml";
      	$inv_tmp_file_name=__DIR__."/../../../invoice_".$e.".pdf";

      	
      	$billtobox_base_url = $config['billtobox_base_url_ap'];
      	if($this->database_name =='4a5f9937_4f34_21bb_631eb43c4876' || $this->database_name =='salesassist_2'){
      		$billtobox_base_url = $config['billtobox_base_url_ap_test'];
		}

		$data=array(
			'file'				=> new CURLFile($xml_tmp_file_name)
			//'file'				=> '@'.$xml_tmp_file_name
			);

		$app_id=$this->db->field("SELECT app_id FROM apps WHERE name='BillToBox' AND app_type='accountancy'");
		$key1 = $this->db->query("SELECT api, active FROM apps WHERE type='export_billtobox_to_dropbox' AND main_app_id='".$app_id."' ");
		$key2 = $this->db->query("SELECT api, active FROM apps WHERE type='export_pinv_to_billtobox' AND main_app_id='".$app_id."' ");
		
		if($key1->f('active')){
			$ap_key = $key1->f('api');
		}elseif($key2->f('active')){
			$ap_key = $key2->f('api');
		}

	/*	if($this->database_name =='1ba3e6b0_e624_0def_e33578f046f9'){
      		$ap_key = '66a5c967-db90-47a6-9fb6-9a9235502da6';
      		//var_dump($billtobox_base_url,$this->user, $ap_key);exit();
		}*/

		$ch = curl_init();
    	$headers=array('Content-Type: multipart/form-data');
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $this->user.":".$ap_key);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_URL, $billtobox_base_url);
	   
        

		$put = curl_exec($ch);
	 	$info = curl_getinfo($ch);
	 	
	 	//var_dump($put,$info);exit();
	 	//var_dump($billtobox_base_url,$this->user, $ap_key);exit();

	 	if($info['http_code']>300 || $info['http_code']==0){
	 		if(is_string($put)){
	 			$resp_error['error'] = $put;
	 		}else{
	 			$resp_error=json_decode($put, true);
	 		}
	 		
	 		$in['actiune']=gm('Fail:');
			msg::error($resp_error['error'],'error');
	 		insert_error_call($this->database_name,"billtobox","invoice",$in['invoice_id'],$in['actiune'].$resp_error['error']);
	 	}else{
	 		$billtobox_data=json_decode($put,true);
	 		
	 		
	 		$billtobox_id = $billtobox_data['item']['id'];
	 		if($billtobox_id){
	 			//$this->db->query("UPDATE tblinvoice SET billtobox_id='".$billtobox_id."' WHERE id='".$in['invoice_id']."' ");
		 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', billtobox='1',type=1 ");
		 		if(array_key_exists('invoice_to_export', $_SESSION) === true){
		 			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		 		}
			 	if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
		 		$in['actiune']=gm("Invoice exported successfully");

		 		if($in['from_acc']){
			 		$msg = '{l}Invoice was exported automatically to{endl} '.'BillToBox ';
			 	}else{
			 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'BillToBox ';
			 	}
				insert_message_log('purchase_invoice',$msg,'purchase_invoice_id',$in['invoice_id']);
		 		
	 		}else{ 		
	 			$in['actiune']=$billtobox_data['errors'];
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"billtobox","invoice",$in['invoice_id'],$in['actiune']);
	 		}
	 		
	 	}
	 	unlink($xml_tmp_file_name);
	 	unlink($inv_tmp_file_name);
	 	json_out($in);
	}

		  function generate_pdf_oO($in)
	{
		include(__DIR__.'/../controller/winbooks_pdf.php');
		return $str;
	}

		function createIncFiles(&$in)
	{
		global $config;
		$user_acc=$this->db_users->field("SELECT user_id FROM users WHERE database_name='".$this->database_name."' AND active=1000");

		$accountant=$this->db_users->field("SELECT first_name FROM users WHERE user_id='".$user_acc."' ");

		$invoice = $this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");

		$customer_reference=$this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('supplier_id')."' ");

		$code_supplier_CBE=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE:CBE' : '';
		$code_supplier_VAT=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE:VAT' : '';
		$code_supplier_simple=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE' : '';

		$payment_reference = $invoice->f('ogm');
		if($payment_reference){
			$payment_reference ='+++'.$payment_reference.'+++';
		}

		$vat_number=$this->db->field("SELECT btw_nr FROM customers WHERE customer_id='".$invoice->f('supplier_id')."' ");

		$vat_number=trim($vat_number," ");
		$vat_number_base=substr($vat_number,0,2);
		$vat_number=substr($vat_number,2);
		$vat_number=str_replace(" ","",$vat_number);
		$vat_number=str_replace(".","",$vat_number);
		$vat_number=str_replace(",","",$vat_number);
		$vat_number=str_replace("-","",$vat_number);
		$vat_number=str_replace("_","",$vat_number);
		$vat_number=str_replace("/","",$vat_number);
		$vat_number=str_replace("|","",$vat_number);

		$str = '';
		$is_active = $this->db->field("SELECT active FROM apps WHERE name = 'Dropbox'");
		
		$invoice_type=$invoice->f('type')<2 ? '380' : '381';
		$customer_reference=$this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");
	
		if($invoice->f('file_path') && $is_active==1 && $invoice->f('amazon') !=1){
			$d = new drop('invoices',null,null,false,$this->database_name);
			@mkdir('upload/'.$this->database_name.'/temp_drop_pdfs/',0775,true);
			$name = substr(urldecode($invoice->f('file_path')), strrpos(urldecode($invoice->f('file_path')), '/')+1);
			$f = fopen("upload/".$this->database_name."/temp_drop_pdfs/".$name, 'w+b');
			$d->getFile(urldecode($invoice->f('file_path')), $f);
			fclose($f);
			$content = file_get_contents("upload/".$this->database_name."/temp_drop_pdfs/".$name );
			$str = base64_encode($content);
			$in['attach_file_name'] = 'invoice_'.$invoice->f('booking_number').'.pdf';
			$file = $d->getFile(urldecode($invoice->f('file_path')), $in['attach_file_name']);
		}
		if($invoice->f('amazon') ==1){
			ark::loadLibraries(array('aws'));
			$aws = new awsWrap($this->database_name);
			$exist =  $aws->doesObjectExist($config['awsBucket'].$this->database_name.'/'.$invoice->f('file_name'));
			if($exist){
				$link = $aws->getLink($config['awsBucket'].$this->database_name.'/'.$invoice->f('file_name'));
				$content = file_get_contents($link);
				$str = base64_encode($content);
				$in['attach_file_name'] = 'invoice_'.$invoice->f('booking_number').'.pdf';
	    		$file = $aws->getItem($invoice->f('file_name'),$in['attach_file_name']);
			}
		}

		if($invoice->f('supplier_id')){
			$bName= $this->db->query("SELECT * FROM customers WHERE customer_id='".$invoice->f('supplier_id')."'");
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$invoice->f('supplier_id')."' AND billing='1' ");
			$supplier_vat=$bName->f('btw_nr');
			$supplier_vat=trim($supplier_vat," ");
			$supplier_vat_base=substr($supplier_vat,0,2);
			$supplier_vat=substr($supplier_vat,2);
			$supplier_vat=str_replace(" ","",$supplier_vat);
			$supplier_vat=str_replace(".","",$supplier_vat);
			$supplier_vat=str_replace(",","",$supplier_vat);
			$supplier_vat=str_replace("-","",$supplier_vat);
			$supplier_vat=str_replace("_","",$supplier_vat);
			$supplier_vat=str_replace("/","",$supplier_vat);
			$supplier_vat=str_replace("|","",$supplier_vat);
		}elseif ($invoice->f('contact_id')) {
			$bName= $this->db->query("SELECT CONCAT_WS(' ',firstname, lastname) AS name FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."'");
			$address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$invoice->f('contact_id')."' AND is_primary='1' ");
			$supplier_vat='';
			$supplier_vat_base='';
		}

		if(!$str){
			msg::error(gm('PDF required'),'error');
			json_out($in);
		}

		$vat_arr = array();
		$sub_arr = array();
		$tax_str = '';
		$discontoo = 0;
		$disconto_total = 0;
		$disconto_total_no_vat = 0;
		$xml_lines='';
		$i = 0;
		$lines = $this->db->query("SELECT * FROM tblinvoice_incomming_line WHERE invoice_id='".$in['invoice_id']."' ");
		while ($lines->next()) {
			$discontoo_line = 0;
			if($invoice->f('allow_disconto') == 1){
				$line = $lines->f('total')-$lines->f('total')*$invoice->f('disconto')/100;
				$discontoo += $lines->f('total')*$invoice->f('disconto')/100;
				$discontoo_line = $lines->f('total')*$invoice->f('disconto')/100;
			}else{
				$line = $lines->f('total');
			}
			$total_with_vat = $line + ($line/100*$lines->f('vat_line'));
			$ln_amount_vat=$lines->f('vat_line')*$line/100;
			$disconto_total_no_vat +=$discontoo_line + $line;

			$disconto_total +=$discontoo_line + $total_with_vat;
			$vat_arr[$lines->f('vat_line')] += $lines->f('vat_line')*$line/100;
			$sub_arr[$lines->f('vat_line')] += $line;
			$i++;
			$line_xml = '
			  <cac:InvoiceLine>
				<cbc:ID>'.$i.'</cbc:ID>
				<cbc:InvoicedQuantity unitCode="C62" unitCodeListID="UNECERec20">1.00</cbc:InvoicedQuantity>
				<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($discontoo_line + $line).'</cbc:LineExtensionAmount>
				<cbc:AccountingCost></cbc:AccountingCost>
				<cac:TaxTotal>
				  <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount_vat).'</cbc:TaxAmount>
				  <cac:TaxSubtotal>
				    <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($discontoo_line + $line).'</cbc:TaxableAmount>
				    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount_vat).'</cbc:TaxAmount>
				    <cac:TaxCategory>
				      <cbc:ID schemeID="UNCL5305">S</cbc:ID>
				      <cbc:Name>03</cbc:Name>
				      <cbc:Percent>'.twoDecNumber($lines->f('vat_line')).'</cbc:Percent>
				      <cac:TaxScheme>
			            <cbc:ID>VAT</cbc:ID>
			          </cac:TaxScheme>
				    </cac:TaxCategory>
				  </cac:TaxSubtotal>
				</cac:TaxTotal>
				<cac:Item>
				  <cbc:Description>Invoice Line</cbc:Description>
				  <cac:ClassifiedTaxCategory>
			        <cbc:ID schemeID="UNCL5305">S</cbc:ID>
			        <cbc:Name>03</cbc:Name>
			        <cbc:Percent>'.twoDecNumber($lines->f('vat_line')).'</cbc:Percent>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:ClassifiedTaxCategory>	        
				</cac:Item>
				<cac:Price>
			      <cbc:PriceAmount currencyID="EUR">'.twoDecNumber($discontoo_line + $line).'</cbc:PriceAmount>
			    </cac:Price>         
		       </cac:InvoiceLine>';
	       		$xml_lines.=$line_xml;
    	}
    	foreach ($vat_arr as $key => $val) {
    		$line_xml='<cac:TaxSubtotal>
		      <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($sub_arr[$key]).'</cbc:TaxableAmount>
		      <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:TaxAmount>     
		      <cac:TaxCategory>
		        <cbc:ID schemeID="UNCL5305">S</cbc:ID>
		        <cbc:Name>03</cbc:Name>
		        <cbc:Percent>'.$key.'</cbc:Percent>
		        <cac:TaxScheme>
		          <cbc:ID>VAT</cbc:ID>
		        </cac:TaxScheme>
		      </cac:TaxCategory>
		    </cac:TaxSubtotal>';
    			$tax_str.=$line_xml;
    	}
    	$disconto_total = number_format($disconto_total,2);
    	$disconto_total_no_vat = number_format($disconto_total_no_vat,2);

		$currency = currency::get_currency(1,'code');
		$xml ='<?xml version="1.0" encoding="UTF-8"?>
			<Invoice xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2">
			  <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
			  <cbc:CustomizationID>1.0</cbc:CustomizationID>
			  <cbc:ProfileID>FFF.BE - Logistics 6.0</cbc:ProfileID>
			  <cbc:ID>'.$invoice->f('invoice_number').'</cbc:ID>
			  <cbc:IssueDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</cbc:IssueDate>
			  <cbc:InvoiceTypeCode listID="UNCL1001">'.$invoice_type.'</cbc:InvoiceTypeCode>
			  <cbc:Note> </cbc:Note>
			  <cbc:DocumentCurrencyCode listID="ISO4217"
			    >'.$currency.'</cbc:DocumentCurrencyCode>
			  <cac:AdditionalDocumentReference>
			    <cbc:ID>Invoice-'.$invoice->f('invoice_number').'</cbc:ID>
			    <cbc:DocumentType>CommercialInvoice</cbc:DocumentType>
			    <cac:Attachment>
			      <cbc:EmbeddedDocumentBinaryObject mimeCode="application/pdf"
			        >'.$str.'</cbc:EmbeddedDocumentBinaryObject>
			    </cac:Attachment>
			  </cac:AdditionalDocumentReference>
			   <cac:AccountingSupplierParty>
			    <cac:Party>
			      <cbc:EndpointID schemeID="'.$code_supplier_VAT.'">'.$supplier_vat_base.$supplier_vat.'</cbc:EndpointID>
			      <cac:PartyIdentification>
			        <cbc:ID schemeID="'.$code_supplier_VAT.'">'.$supplier_vat_base.$supplier_vat.'</cbc:ID>
			      </cac:PartyIdentification>
			      <cac:PartyName>
			        <cbc:Name>'.htmlspecialchars($bName->f('name')).'</cbc:Name>
			      </cac:PartyName>
			      <cac:PostalAddress>
			        <cbc:StreetName>'.$address->f('address').'</cbc:StreetName>
			        <cbc:CityName>'.$address->f('city').'</cbc:CityName>
			        <cbc:PostalZone>'.$address->f('zip').'</cbc:PostalZone>
			        <cac:Country>
			          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($address->f('country_id')).'</cbc:IdentificationCode>
			        </cac:Country>
			      </cac:PostalAddress>
			      <cac:PartyTaxScheme>
			        <cbc:CompanyID schemeID="'.$code_supplier_VAT.'">'.$supplier_vat_base.$supplier_vat.'</cbc:CompanyID>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:PartyTaxScheme>
			      <cac:PartyLegalEntity>
			        <cbc:CompanyID schemeID="'.$code_supplier_CBC.'">'.$supplier_vat.'</cbc:CompanyID>
			      </cac:PartyLegalEntity>
			    </cac:Party>
			  </cac:AccountingSupplierParty>
			  <cac:AccountingCustomerParty>
			  	<cbc:SupplierAssignedAccountID>'.htmlspecialchars(stripslashes($customer_reference),ENT_QUOTES | ENT_HTML5).'</cbc:SupplierAssignedAccountID>
			    <cac:Party>
			      <cbc:EndpointID schemeID="">'.$vat_number_base.$vat_number.'</cbc:EndpointID>
			      <cac:PartyIdentification>
			        <cbc:ID schemeID="">'.$vat_number_base.$vat_number.'</cbc:ID>
			      </cac:PartyIdentification>
			      <cac:PartyName>
			        <cbc:Name>'.htmlspecialchars(ACCOUNT_COMPANY).'</cbc:Name>
			      </cac:PartyName>
			      <cac:PostalAddress>
			        <cbc:StreetName>'.ACCOUNT_BILLING_ADDRESS.'</cbc:StreetName>
			        <cbc:CityName>'.ACCOUNT_BILLING_CITY.'</cbc:CityName>
			        <cbc:PostalZone>'.ACCOUNT_BILLING_ZIP.'</cbc:PostalZone>
			        <cac:Country>
			          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code(ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
			        </cac:Country>
			      </cac:PostalAddress>
			      <cac:PartyTaxScheme>
			        <cbc:CompanyID schemeID="">'.$vat_number_base.$vat_number.'</cbc:CompanyID>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:PartyTaxScheme>
			       <cac:PartyLegalEntity>
			        <cbc:CompanyID schemeID="">'.$vat_number_base.$vat_number.'</cbc:CompanyID>
			      </cac:PartyLegalEntity>
			    </cac:Party>
			  </cac:AccountingCustomerParty>
			  <cac:PaymentMeans>
			    <cbc:PaymentMeansCode listID="UNCL4461" listName="Payment Means" listURI="http://docs.oasis-open.org/ubl/os-UBL-2.0-update/cl/gc/default/PaymentMeansCode-2.0.gc">1</cbc:PaymentMeansCode>
			    <cbc:PaymentDueDate>'.date('Y-m-d',$invoice->f('due_date')).'</cbc:PaymentDueDate>
			    <cbc:InstructionID>'.$payment_reference.'</cbc:InstructionID>
			    <cbc:PaymentID>'.$payment_reference.'</cbc:PaymentID>
			    <cac:PayeeFinancialAccount>
			      <cbc:ID schemeID="IBAN">'.$invoice->f('buyer_iban').'</cbc:ID>
					<cac:FinancialInstitutionBranch>
						<cac:FinancialInstitution>
							<cbc:ID schemeID="BIC">'.$invoice->f('buyer_bic').'</cbc:ID>
						</cac:FinancialInstitution>
					</cac:FinancialInstitutionBranch>
			    </cac:PayeeFinancialAccount>
			  </cac:PaymentMeans>
			  <cac:PaymentTerms>
				<cbc:SettlementDiscountPercent>'.$invoice->f('disconto').'</cbc:SettlementDiscountPercent>
			  </cac:PaymentTerms>
			  <cac:TaxTotal>
				<cbc:TaxAmount currencyID="EUR">'.twoDecNumber($invoice->f('total_with_vat')-$invoice->f('total')).'</cbc:TaxAmount>
				'.$tax_str.'
			  </cac:TaxTotal>
			  <cac:LegalMonetaryTotal>
			    <cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total_no_vat).'</cbc:LineExtensionAmount>
			    <cbc:TaxExclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total_no_vat).'</cbc:TaxExclusiveAmount>
			    <cbc:TaxInclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:TaxInclusiveAmount>
			    <cbc:PrepaidAmount currencyID="'.$currency.'">0.00</cbc:PrepaidAmount>
			    <cbc:PayableAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:PayableAmount>
			  </cac:LegalMonetaryTotal>
			  '.$xml_lines.'
			</Invoice>
			';

		$e = str_replace('/', '', $invoice->f('booking_number'));
		if(file_put_contents(__DIR__.'/../../../file_inc_'.$e.'.xml', $xml) === false){
			return false;
		}
		return $e;
	}

}//end class
?>

