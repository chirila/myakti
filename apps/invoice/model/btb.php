<?php
/************************************************************************
* @Author: MedeeaWeb Works
***********************************************************************/
class btb
{
	var $db;
	var $user;
	var $pass;
	var $arUrl;
	var $apUrl;
	var $database_name;

	function btb(&$in,$dbase='')
	{
		global $database_config;
		if($dbase){
			//var_dump('b');
			$database_1 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $dbase,
			);
			$this->db = new sqldb($database_1);
			$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();
			$this->database_name=DATABASE_NAME;
		}		
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->dbu_users =  new sqldb($database_2); // recurring billing
		$app =array();
		$app_id=$this->db->field("SELECT app_id FROM apps WHERE name='WinBooks' AND app_type='accountancy'");
		$actions = array('push_user','ap_push_key','ar_push_key','pull_user','ap_pull_key','ar_pull_key');
		foreach ($actions as $key => $value) {
			$app[$value] = $this->db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='".$value."' ");
		}
		$this->user = $app['push_user'];
		$this->pass = $app['ar_push_key'];
		$this->host = $app['ap_push_key'];

	}


	/**
	 * Mark as exported
	 *
	 * @return boolean
	 * @author Mp
	 **/
	function markExport(&$in)
	{
		$to_app_text="";
		switch($in['app']){
			case 'yuki':
				$to_app_text = "Yuki";
			break;
		case 'codabox':
				$to_app_text = "Codabox";
			break;
		case 'clearfacts':
				$to_app_text = "ClearFacts";
			break;	
		case 'billtobox':
				$to_app_text = "BillToBox";
			break;
		case 'exact_online':
				$to_app_text = "Exact online";
			break;
		case 'netsuite':
				$to_app_text = "NetSuite";
			break;
		default:
				$to_app_text = 'E-FFF';
			break;
		}
		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', type='".$in['type']."', ".$in['app']."=1 ");
		insert_message_log('invoice','{l}Invoice was marked as exported for{endl} '.$to_app_text.' {l}by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		if($_SESSION['invoice_to_export']){
			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND `time`='".$_SESSION['invoice_to_export']."'  ");
			if($_SESSION['tmp_export_add_to_app'] && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
	          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
	        }
		}
		return true;
	}
	/**
	 * Mark as unexported
	 *
	 * @return boolean
	 * @author Mp
	 **/
	function unmarkExport(&$in)
	{
		$this->db->query("DELETE FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND ".$in['app']."=1 AND type='".$in['type']."' ");
		insert_message_log('invoice','{l}Invoice was put back on export on list by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		if($_SESSION['invoice_to_export']){
			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND `time`='".$_SESSION['invoice_to_export']."'  ");
			if($_SESSION['tmp_export_add_to_app'] && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
	          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
	        }
		}
		return true;
	}

	function exportCoda(&$in){
		global $config;
		//if($in['from_cron']){
			ark::loadCronLibraries(array('aws'));
		/*}else{
			ark::loadLibraries(array('aws'));
		}	
		if(!DATABASE_NAME){
			define(DATABASE_NAME,$this->db->field("SELECT DATABASE()"));
		}*/
		$aws = new awsWrap($this->database_name);
		$exist =  $aws->doesObjectExist($config['awsBucket'].$this->database_name.'/invoice/invoice_'.$in['invoice_id'].'.pdf');
		if($exist){
			$time=time();
			//$attach_file_name='invoice_'.$in['invoice_id'].'_'.$time.'.pdf';
			$attach_file_name=__DIR__.'/../../../invoice_'.$in['invoice_id'].'_'.$time.'.pdf';
			$file = $aws->getItem('invoice/invoice_'.$in['invoice_id'].'.pdf',$attach_file_name);

			$ch = curl_init();
			$coda_api=$this->db->query("SELECT api,app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
			$coda_user=$this->db->field("SELECT api FROM apps WHERE type='username' AND main_app_id='".$coda_api->f('app_id')."' ");
			$coda_pass=$this->db->field("SELECT api FROM apps WHERE type='password' AND main_app_id='".$coda_api->f('app_id')."' ");
			$headers=array('X-Software-Company: '.$coda_api->f('api'));
			//$coda_data=array('file'=>'@'.$attach_file_name);
			$coda_data=array('file'=>new CURLFile($attach_file_name));

	        	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	        	curl_setopt($ch, CURLOPT_USERPWD, $coda_user.":".$coda_pass);
	        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        	curl_setopt($ch, CURLOPT_POST, true);
        		curl_setopt($ch, CURLOPT_POSTFIELDS, $coda_data);
	       	curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/upload/');

		  	$put = curl_exec($ch);
	 		$info = curl_getinfo($ch);
	 		//console::log('1cons');
	 		//console::log($put);
	 		//console::log($info);
	 		if($info['http_code']>300 || $info['http_code']==0){
	 			$resp_error=json_decode($put);
	 			$in['actiune']=gm('Fail:');
	 			if($info['http_code']==413){
	 				msg::error(gm('Uploaded PDF size too large'),"error");
	 				insert_error_call($this->database_name,"codabox","invoice",$in['invoice_id'],"Uploaded PDF size too large");
	 			}else{
	 				msg::error($resp_error->detail,"error");
	 				insert_error_call($this->database_name,"codabox","invoice",$in['invoice_id'],$resp_error->detail);
	 			}
	 		}else{
	 			$pdf_id=json_decode($put);
	 			//console::log('2cons');
	 			//console::log($pdf_id);
	 			$ACCOUNT_VAT_NUMBER=trim($this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ")," ");
				$ACCOUNT_VAT_NUMBER=str_replace(" ","",$ACCOUNT_VAT_NUMBER);
				$ACCOUNT_VAT_NUMBER=str_replace(".","",$ACCOUNT_VAT_NUMBER);
				$ACCOUNT_VAT_NUMBER=str_replace(",","",$ACCOUNT_VAT_NUMBER);
				$ACCOUNT_VAT_NUMBER=str_replace("-","",$ACCOUNT_VAT_NUMBER);
				$ACCOUNT_VAT_NUMBER=str_replace("_","",$ACCOUNT_VAT_NUMBER);
				$ACCOUNT_VAT_NUMBER=str_replace("/","",$ACCOUNT_VAT_NUMBER);
				$ACCOUNT_VAT_NUMBER=str_replace("|","",$ACCOUNT_VAT_NUMBER);
				$ACCOUNT_VAT_NUMBER=strtoupper($ACCOUNT_VAT_NUMBER);

				$ACCOUNT_ADDRESS_INVOICE=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
				$ACCOUNT_DELIVERY_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
				$ACCOUNT_DELIVERY_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
				$ACCOUNT_DELIVERY_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
				$ACCOUNT_DELIVERY_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
				$ACCOUNT_BILL_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
				$ACCOUNT_BILL_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
				$ACCOUNT_BILL_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
				$ACCOUNT_BILL_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
				$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");

				$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
				$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
				$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
				$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;

				$ACCOUNT_IBAN=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_IBAN' ");
				$ACCOUNT_BIC=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BIC_CODE' ");

	 			$headers=array('Content-Type: application/json','X-Software-Company: '.$coda_api->f('api'));
	 			$coda_data=array();
	 			$coda_data['pdf_upload_id']=$pdf_id->pdf_upload_id;
	 			$coda_data['pdf_filename']='invoice_'.$in['invoice_id'].'_'.$time.'.pdf';
	 			$coda_data['sha1']=sha1_file($attach_file_name);
	 			$coda_data['supplier_vat']=$ACCOUNT_VAT_NUMBER; //- this should be the real line, the below is hardcoded for test
	 			//$coda_data['supplier_vat']='BE0010060977';
	 			$coda_data['supplier_name']=html_entity_decode($ACCOUNT_COMPANY);
	 			$coda_data['supplier_address']=html_entity_decode($ACCOUNT_BILLING_ADDRESS);
	 			$coda_data['supplier_city']=html_entity_decode($ACCOUNT_BILLING_CITY);
	 			$coda_data['supplier_postal_code']=$ACCOUNT_BILLING_ZIP;
	 			$coda_data['supplier_country']=get_country_code($ACCOUNT_BILLING_COUNTRY_ID);
	 			$coda_data['bank_account_number']=trim(str_replace(" ","",$ACCOUNT_IBAN)," ");
	 			$coda_data['bank_account_bic']=trim(str_replace(" ","",$ACCOUNT_BIC_CODE)," ");

	 			$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
	 			$in['serial_number'] = $invoice->f('serial_number');
				$currency = $this->db->field("SELECT code FROM currency WHERE id='".$invoice->f('currency_type')."' ");
				if(!$currency){
					$currency = $this->db->field("SELECT code FROM currency WHERE id='".ACCOUNT_CURRENCY_TYPE."' ");
				}
				$type=$invoice->f('type');
				//$our_ref = $this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");
				if($invoice->f('seller_bwt_nr')){
					$btw_nr=$invoice->f('seller_bwt_nr');
				}else{
					$btw_nr = $this->db->field("SELECT btw_nr FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");
				}

				$btw_nr=trim($btw_nr," ");
				$btw_nr=str_replace(" ","",$btw_nr);
				$btw_nr=str_replace(".","",$btw_nr);
				$btw_nr=str_replace(",","",$btw_nr);
				$btw_nr=str_replace("-","",$btw_nr);
				$btw_nr=str_replace("_","",$btw_nr);
				$btw_nr=str_replace("/","",$btw_nr);
				$btw_nr=str_replace("|","",$btw_nr);
				$btw_nr=strtoupper($btw_nr);

	 			$coda_data['customer_vat']=$btw_nr;
	 			$coda_data['customer_name']=html_entity_decode($invoice->f('buyer_name'));
	 			$coda_data['customer_address']=html_entity_decode($invoice->f('buyer_address'));
				$coda_data['customer_city']=html_entity_decode($invoice->f('buyer_city'));
				$coda_data['customer_postal_code']=$invoice->f('buyer_zip');
				$coda_data['customer_country']=get_country_code($invoice->f('buyer_country_id'));
				$coda_data['invoice_number']=$invoice->f('serial_number');
				$coda_data['invoice_date']=date('Y-m-d',$invoice->f('invoice_date'));
				$coda_data['invoice_type']=$invoice->f('type')=='2' ? 'credit-note' : 'invoice';
				$coda_data['invoice_currency']=$currency;
				$coda_data['invoice_due_date']=date('Y-m-d',$invoice->f('due_date'));
				$coda_data['invoice_amount']=number_format($invoice->f('amount_vat'),2,'.','');
				$coda_data['invoice_amount_vat_excluded']=number_format($invoice->f('amount'),2,'.','');
				$coda_data['invoice_amount_vat']=number_format($invoice->f('amount_vat')-$invoice->f('amount'),2,'.','');
				//$coda_data['payment_reference']=$our_ref;
				$coda_data['payment_reference']=html_entity_decode($invoice->f('our_ref'));
				$inv_date=date('Y',$invoice->f('invoice_date'));
				$coda_data['invoice_folder']='sales_'.$inv_date;
				$coda_data['lines']=array();
				$coda_data['taxes']=array();

				$tax_id=array(
					array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
					array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
					array('0'=>'00','6'=>'','12'=>'','21'=>''),
					array('0'=>'00','6'=>'','12'=>'','21'=>''),
					array('0'=>'00','6'=>'','12'=>'','21'=>''),
				);
				$tax= array();
				if($invoice->f('vat_regime_id')>=10000){
					$regime_type=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
				}		
				$get_invoice_rows = $this->db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");
				$discount = $invoice->f('discount');
				if($invoice->f('apply_discount')<2){
					$discount=0;
				}
				while ($get_invoice_rows->next()){
					$vat_percent_val = 0;
					$item_code='1000000';
					$gl_account='';
					if($get_invoice_rows->f('article_id')){
						$item=$this->db->field("SELECT customers.serial_number_customer FROM pim_articles
						INNER JOIN customers ON pim_articles.supplier_id=customers.customer_id WHERE pim_articles.article_id='".$get_invoice_rows->f('article_id')."' ");
						if($item){
							$item_code=$item;
						}
						$line_gl=$this->db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' ");
						if($line_gl){
							$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$line_gl."' ");
						}
					}
					if((!$gl_account || $gl_account=='') && !$get_invoice_rows->f('content')){
						$app_gl=$this->db->field("SELECT value FROM settings WHERE constant_name='CODA_LEDGER_ID' ");
						if($app_gl){
							$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$app_gl."' ");
						}
					}
					$line = array();
					//$tax= array();
					$line_tax=array();
					$line['code']=$item_code;
					$line['description']=html_entity_decode($get_invoice_rows->f('name'));
					$line['quantity']=number_format($get_invoice_rows->f('quantity'),2,'.','');
					if($gl_account && $gl_account!=''){
						$line['gl_account']=$gl_account;
					}				

					$line_d = $get_invoice_rows->f('discount');
					if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
						$line_d = 0;
					}
					$line_unit_price=($get_invoice_rows->f('price') - $get_invoice_rows->f('price') * $line_d /100)-($get_invoice_rows->f('price') - $get_invoice_rows->f('price') * $line_d /100)*$discount/100;
					$amount = $get_invoice_rows->f('amount') - $get_invoice_rows->f('amount') * $line_d /100;

					$amount_d = $amount * $discount/100;
					if($invoice->f('apply_discount') < 2){
						$amount_d = 0;
					}
					$line['unit_price']=number_format($line_unit_price,2,'.','');
					$line['amount_vat_excluded']=number_format($amount - $amount_d,2,'.','');
					$line_tax['amount_vat_excluded']=number_format($amount - $amount_d,2,'.','');

					$vat_percent_val = round(($amount - $amount_d) * $get_invoice_rows->f('vat')/100,2);

					$line['amount_vat']=number_format($vat_percent_val,2,'.','');
					$line_tax['amount_vat']=number_format($vat_percent_val,2,'.','');

					/*if($invoice->f('vat_regime_id') < 2){
						$vat_type = $this->db->field("SELECT codavats.value FROM codavats INNER JOIN vats ON codavats.vat_id = vats.vat_id WHERE vats.value='".$get_invoice_rows->f('vat')."' ");
						if(!$vat_type){
								$vat_type = '00';
						}
						$line['tax_category']=$vat_type;
						$line_tax['tax_category']=$vat_type;
					}else{
						$vat_type = $this->db->field("SELECT codavats.value FROM codavats WHERE codavats.vat_id='r".$invoice->f('vat_regime_id')."' ");
						if(!$vat_type){
								$vat_type = '00';
						}
						$line['tax_category']=$vat_type;
						$line_tax['tax_category']=$vat_type;
					}*/
					if($invoice->f('vat_regime_id')<5){
						$vat_type=$tax_id[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')];
					}else{
						if($regime_type==1){
							$vat_type=$tax_id[0][$get_invoice_rows->f('vat')];
						}else{
							$vat_type=$tax_id[2][$get_invoice_rows->f('vat')];
						}
					}	
					if(!$vat_type || $vat_type==''){
						$vat_type = '00';
					}
					$line['tax_category']=$vat_type;
					$line_tax['tax_category']=$vat_type;
					array_push($coda_data['lines'], $line);
					//array_push($coda_data['taxes'], $tax);
					$is_already=false;
					foreach ($tax as $key => $value) {
						if($value['tax_category'] == $line_tax['tax_category']){
							$tax[$key]['amount_vat_excluded']=number_format($tax[$key]['amount_vat_excluded']+$amount - $amount_d,2,'.','');
							$tax[$key]['amount_vat']=number_format($tax[$key]['amount_vat']+$vat_percent_val,2,'.','');
							$is_already=true;
							break;
						}
					}
					if(!$is_already){
						array_push($tax, $line_tax);
					}
				}
				if(empty($coda_data['lines'])){
					$line = array();
					$line['code']='';
					$line['description']='';
					$line['quantity']=number_format(1,2,'.','');
					$line['unit_price']=number_format(0,2,'.','');
					$line['amount_vat_excluded']=number_format(0,2,'.','');
					$line['amount_vat']=number_format(0,2,'.','');
					$line['tax_category']='00';
					array_push($coda_data['lines'], $line);

				}

				$coda_data['taxes']=$tax;
				$coda_data = json_encode($coda_data);

				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
		        	curl_setopt($ch, CURLOPT_USERPWD, $coda_user.":".$coda_pass);
		        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		        	curl_setopt($ch, CURLOPT_POST, true);
	        		curl_setopt($ch, CURLOPT_POSTFIELDS, $coda_data);
		       	curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/invoices/');


			  	$put = curl_exec($ch);
		 		$info = curl_getinfo($ch);

		 		//console::log('3cons');
		 		//console::log($put);
		 		//console::log($info);

		 		if($info['http_code']>300 || $info['http_code']==0){
		 			$in['actiune']=gm('Fail:');
		 			$put = json_decode($put);
		 			//console::log($put);
		 			$error="";
		 			foreach ($put as $key => $value) {
		 				if($key=='sha1'){
		 					$error=gm("File already exported");
		 				}else if($key=='lines'){
		 					/*if(is_array($value)){
		 						$error.='invoice line -'.$key.':'.$value[0].'<br/>';
		 					}else{*/
		 						foreach ($value as $k => $val) {
									$arr = (array)$val;
									if(count($arr)>0){
										foreach($val as $k1=>$v1){
											$error.='invoice line -'.$k1.':'.$v1[0].'<br/>';
										}
									}
				 				}
		 					//}
		 				}else if($key=='taxes'){
							/*if(is_array($value)){
		 						$error.='invoice line -'.$key.':'.$value[0].'<br/>';
		 					}else{*/
		 						foreach ($value as $k => $val) {
									$arr = (array)$val;
									if(count($arr)>0){
										foreach($val as $k1=>$v1){
											$error.='invoice line -'.$k1.':'.$v1[0].'<br/>';
										}
									}
				 				}
		 					//}
		 				}else{
		 					if(is_array($value)){
			 					$error.=$key.'-'.$value[0].'<br/>';
			 				}else{
			 					foreach ($value as $k => $val) {
			 						$error.=$key.'-'.$val[0].'<br/>';
			 					}
			 				}
		 				}
		 			}
		 			msg::error(rtrim($error,'<br/>'),"error");
		 			insert_error_call($this->database_name,"codabox","invoice",$in['invoice_id'],$error);
		 		}else{
		 			$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', codabox='1', codabox_pdf_id='".$pdf_id->pdf_upload_id."' ");
		 			if(array_key_exists('invoice_to_export', $_SESSION) === true){
		 				$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		 			}
		 			if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          		unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        	}
		 			$in['actiune']=gm("Invoice exported successfully");

		 			if($in['from_acc']){
				 		$msg = '{l}Invoice was exported automatically to{endl} '.'Codabox ';
				 	}else{
				 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Codabox ';
				 	}
		 			insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

		 			if($_SESSION['main_u_id']=='0'){
				          //$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
				          $is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);         
				    }else{
				          $is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
				    }
				    if($is_accountant){
				    	$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
				    	if($accountant_settings=='1'){
				    		$acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
				    		if($acc_block_id=='1'){
				    			$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
				    		}
				    	}else{
				    		$block_id=$this->dbu_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
					        if($block_id==1){
					            $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
					        }
				    	}	        
				    }
		 		}
	 		}
	 		unlink($attach_file_name);
		}else{
			$in['actiune']=gm('Fail:');
			msg::error(gm('Please upload first invoice to Amazon'),"error");
			insert_error_call($this->database_name,"codabox","invoice",$in['invoice_id'],"Please upload first invoice to Amazon");
		}
		if($in['from_acc']){
			return true;
		}else{
			json_out($in);	
		}		
	}

	function coda_payment(&$in){
		global $config;
		$ch = curl_init();
		$coda_api=$this->db->query("SELECT api,app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
		$coda_user=$this->db->field("SELECT api FROM apps WHERE type='username' AND main_app_id='".$coda_api->f('app_id')."' ");
		$coda_pass=$this->db->field("SELECT api FROM apps WHERE type='password' AND main_app_id='".$coda_api->f('app_id')."' ");
		$headers=array('X-Software-Company: '.$coda_api->f('api'));

		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
		curl_setopt($ch, CURLOPT_USERPWD, $coda_user.":".$coda_pass);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/delivery/download/'.$in['feed_index'].'/xml/');

		$put = curl_exec($ch);
		$info = curl_getinfo($ch);

		if($put === false){		
			json_out($in);
		}else{
			if($info['http_code']>300 || $info['http_code']==0){
				$resp_error=json_decode($put);	 						 
				msg::error($resp_error->detail,"error");
				json_out($in);
			}else{
				$xml=new SimpleXMLElement(stripslashes($put));
				$xml=json_decode(json_encode($xml), true);
				//$old_balance=strpos($xml['OldBalance']['Value'], 'e') ? display_number(number_format(str_replace("e","",$xml['OldBalance']['Value']),2,'.','')) : display_number(number_format($xml['OldBalance']['Value'],2,'.',''));
				//$new_balance=strpos($xml['NewBalance']['Value'], 'e') ? display_number(number_format(str_replace("e","",$xml['NewBalance']['Value']),2,'.','')) : display_number(number_format($xml['NewBalance']['Value'],2,'.',''));
				$old_balance=$xml['OldBalance']['Value'];
				$new_balance=$xml['NewBalance']['Value'];
				$feed_id=$this->db->insert("INSERT INTO codapayments SET feed_id='".$in['feed_client']."', 
																		feed_index='".$in['feed_index']."',
																		content='".addslashes(serialize($put))."',
																		start_date='".strtotime($xml['OldBalance']['Date'])."',
																		end_date='".strtotime($xml['NewBalance']['Date'])."',
																		bank_account='".$xml['ClientAccount']."',
																		old_balance='".$old_balance."',
																		new_balance='".$new_balance."' ");
				foreach($xml['PaymentList'] as $key=>$value){
					if($value['StatementNumber']){
						$ref=is_string($value['Communication']) ? addslashes($value['Communication']) : '';
						$cd_line=$this->db->insert("INSERT INTO codapayments_line SET feed_index='".$feed_id."',
																			name='".addslashes($value['Counterparty']['Name'])."',
																			bank='".$value['Counterparty']['BankAccount']."',
																			amount='".$value['Amount']."',
																			amount_left='".$value['Amount']."',
																			currency='".$value['Currency']."',
																			date='".strtotime($value['ValueDate'])."',
																			reference='".$ref."' ");
						$search_new_ogm=false;
						$ogm_101=substr($ref,0,3);
						if($ogm_101 == 101){
							$ogm_101_fin=substr($ref,3,12);
							if(strlen($ogm_101_fin) == 12){
								$inv=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat, SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice
													LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id
													WHERE REPLACE(tblinvoice.ogm,'/','')='".$ogm_101_fin."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
								if($inv->next()){
									$search_new_ogm=true;
									$in['amount']=display_number(0);
									$in['id']=$cd_line;
									//$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($inv->f('amount_vat')-$inv->f('amount_payed'))));
									$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($value['Amount'])));
									$this->saveFetch($in);
								}
							}
						}

						$ogm1=strpos($ref,'+++');
						$ogm3=strpos($ref,'***');
						if($ogm1 !== false && !$search_new_ogm){
							$ogm2=strpos($ref,'+',$ogm1+3);
							if($ogm2){
								$ogm=substr($ref,$ogm1+3,$ogm2-($ogm1+3));
								$ogm=str_replace(' ','',$ogm);
								$inv=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat, SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice
													LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id
													WHERE tblinvoice.ogm='".$ogm."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
								//if($inv->next() && ($inv->f('amount_vat')-$inv->f('amount_payed') == $value['Amount'])){
								if($inv->next()){
										$in['amount']=display_number(0);
										$in['id']=$cd_line;
										//$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($inv->f('amount_vat')-$inv->f('amount_payed'))));
										$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($value['Amount'])));
										$this->saveFetch($in);
								}else{
									if($value['Counterparty']['BankAccount'] !=''){
										$inv2=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat,customers.bank_iban,SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice 
											INNER JOIN customers ON tblinvoice.buyer_id=customers.customer_id
											LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id 
											WHERE customers.bank_iban='".$value['Counterparty']['BankAccount']."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
										if($inv2->next() && ($inv2->f('amount_vat')-$inv2->f('amount_payed') == $value['Amount'])){
											$in['amount']=display_number(0);
											$in['id']=$cd_line;
											$in['invoices_fetched']=array(0=>array('id'=>$inv2->f('id'),'amount_vat'=>display_number($value['Amount'])));
											$this->saveFetch($in);
										}
									}								
								}
							}
						}else if($ogm3 !== false && !$search_new_ogm){
							$ogm4=strpos($ref,'*',$ogm3+3);
							if($ogm4){
								$ogm=substr($ref,$ogm3+3,$ogm4-($ogm3+3));
								$ogm=str_replace(' ','',$ogm);
								$inv=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat, SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice
													LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id
													WHERE tblinvoice.ogm='".$ogm."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
								if($inv->next()){
										$in['amount']=display_number(0);
										$in['id']=$cd_line;
										//$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($inv->f('amount_vat')-$inv->f('amount_payed'))));
										$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($value['Amount'])));
										$this->saveFetch($in);
								}else{
									if($value['Counterparty']['BankAccount'] !=''){
										$inv2=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat,customers.bank_iban,SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice 
											INNER JOIN customers ON tblinvoice.buyer_id=customers.customer_id
											LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id 
											WHERE customers.bank_iban='".$value['Counterparty']['BankAccount']."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
										if($inv2->next() && ($inv2->f('amount_vat')-$inv2->f('amount_payed') == $value['Amount'])){
											$in['amount']=display_number(0);
											$in['id']=$cd_line;
											$in['invoices_fetched']=array(0=>array('id'=>$inv2->f('id'),'amount_vat'=>display_number($value['Amount'])));
											$this->saveFetch($in);
										}
									}								
								}
							}
						}else if(!$search_new_ogm){
							if($value['Counterparty']['BankAccount'] !=''){
								$inv2=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat,customers.bank_iban,SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice 
										INNER JOIN customers ON tblinvoice.buyer_id=customers.customer_id
										LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id 
										WHERE customers.bank_iban='".$value['Counterparty']['BankAccount']."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
								if($inv2->next() && ($inv2->f('amount_vat')-$inv2->f('amount_payed') == $value['Amount'])){
									$in['amount']=display_number(0);
									$in['id']=$cd_line;
									$in['invoices_fetched']=array(0=>array('id'=>$inv2->f('id'),'amount_vat'=>display_number($value['Amount'])));
									$this->saveFetch($in);
								}
							}						
						}
					 }else{
					 	foreach($value as $key1=>$value1){
					 		$ref=is_string($value1['Communication']) ? addslashes($value1['Communication']) : '';
					 		$cd_line=$this->db->insert("INSERT INTO codapayments_line SET feed_index='".$feed_id."',
																			name='".addslashes($value1['Counterparty']['Name'])."',
																			bank='".$value1['Counterparty']['BankAccount']."',
																			amount='".$value1['Amount']."',
																			amount_left='".$value1['Amount']."',
																			currency='".$value1['Currency']."',
																			date='".strtotime($value1['ValueDate'])."',
																			reference='".$ref."' ");
					 		$search_new_ogm=false;
							$ogm_101=substr($ref,0,3);
							if($ogm_101 == 101){
								$ogm_101_fin=substr($ref,3,12);
								if(strlen($ogm_101_fin) == 12){
									$inv=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat, SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice
													LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id
													WHERE REPLACE(tblinvoice.ogm,'/','')='".$ogm_101_fin."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
									if($inv->next()){
										$search_new_ogm=true;
										$in['amount']=display_number(0);
										$in['id']=$cd_line;
										//$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($inv->f('amount_vat')-$inv->f('amount_payed'))));
										$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($value1['Amount'])));
										$this->saveFetch($in);
									}
								}
							}

							$ogm1=strpos($ref,'+++');
							$ogm3=strpos($ref,'***');
							if($ogm1 !== false && !$search_new_ogm){
								$ogm2=strpos($ref,'+',$ogm1+3);
								if($ogm2){
									$ogm=substr($ref,$ogm1+3,$ogm2-($ogm1+3));
									$ogm=str_replace(' ','',$ogm);
									$inv=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat, SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice
													LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id
													WHERE tblinvoice.ogm='".$ogm."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
									//if($inv->next() && ($inv->f('amount_vat')-$inv->f('amount_payed') == $value['Amount'])){
									if($inv->next()){
										$in['amount']=display_number(0);
										$in['id']=$cd_line;
										//$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($inv->f('amount_vat')-$inv->f('amount_payed'))));
										$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($value1['Amount'])));
										$this->saveFetch($in);
									}else{
										if($value1['Counterparty']['BankAccount'] !=''){
											$inv2=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat,customers.bank_iban,SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice 
											INNER JOIN customers ON tblinvoice.buyer_id=customers.customer_id
											LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id 
											WHERE customers.bank_iban='".$value1['Counterparty']['BankAccount']."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
											if($inv2->next() && ($inv2->f('amount_vat')-$inv2->f('amount_payed') == $value1['Amount'])){
												$in['amount']=display_number(0);
												$in['id']=$cd_line;
												$in['invoices_fetched']=array(0=>array('id'=>$inv2->f('id'),'amount_vat'=>display_number($value1['Amount'])));
												$this->saveFetch($in);
											}
										}									
									}
								}
							}else if($ogm3 !== false && !$search_new_ogm){
								$ogm4=strpos($ref,'*',$ogm3+3);
								if($ogm4){
									$ogm=substr($ref,$ogm3+3,$ogm4-($ogm3+3));
									$ogm=str_replace(' ','',$ogm);
									$inv=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat, SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice
													LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id
													WHERE tblinvoice.ogm='".$ogm."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
									if($inv->next()){
										$in['amount']=display_number(0);
										$in['id']=$cd_line;
										$in['invoices_fetched']=array(0=>array('id'=>$inv->f('id'),'amount_vat'=>display_number($value1['Amount'])));
										$this->saveFetch($in);
									}else{
										if($value1['Counterparty']['BankAccount'] !=''){
											$inv2=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat,customers.bank_iban,SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice 
											INNER JOIN customers ON tblinvoice.buyer_id=customers.customer_id
											LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id 
											WHERE customers.bank_iban='".$value1['Counterparty']['BankAccount']."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
											if($inv2->next() && ($inv2->f('amount_vat')-$inv2->f('amount_payed') == $value1['Amount'])){
												$in['amount']=display_number(0);
												$in['id']=$cd_line;
												$in['invoices_fetched']=array(0=>array('id'=>$inv2->f('id'),'amount_vat'=>display_number($value1['Amount'])));
												$this->saveFetch($in);
											}
										}									
									}
								}
							}else if(!$search_new_ogm){
								if($value1['Counterparty']['BankAccount'] !=''){
									$inv2=$this->db->query("SELECT tblinvoice.id,tblinvoice.amount_vat,customers.bank_iban,SUM(tblinvoice_payments.amount) as amount_payed FROM tblinvoice 
										INNER JOIN customers ON tblinvoice.buyer_id=customers.customer_id
										LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id 
										WHERE customers.bank_iban='".$value1['Counterparty']['BankAccount']."' AND tblinvoice.type='0' AND tblinvoice.status='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' GROUP BY tblinvoice.id");
									if($inv2->next() && ($inv2->f('amount_vat')-$inv2->f('amount_payed') == $value1['Amount'])){
										$in['amount']=display_number(0);
										$in['id']=$cd_line;
										$in['invoices_fetched']=array(0=>array('id'=>$inv2->f('id'),'amount_vat'=>display_number($value1['Amount'])));
										$this->saveFetch($in);
									}
								}						
							}					 					 
					 	}							
					}	
				}			
				$header=array('Content-Type: application/json','X-Software-Company: '.$coda_api->f('api'));
				$array=array('feed_offset'=>$in['feed_index']);
				$array=json_encode($array);
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
				curl_setopt($ch, CURLOPT_USERPWD, $coda_user.":".$coda_pass);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
				curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/delivery/feed/'.$in['feed_client'].'/');
				$put = curl_exec($ch);
				$info = curl_getinfo($ch);
			}
		}		
		json_out($in);
	}

	function saveFetch(&$in){

		$status=0;
		if(return_value($in['amount']) == 0){
			$status=1;
		}
		$left_to_pay=$this->db->field("SELECT amount_left FROM codapayments_line WHERE id='".$in['id']."' ");
		$this->db->query("UPDATE codapayments_line SET amount_left='".return_value($in['amount'])."', status='".$status."' WHERE id='".$in['id']."' ");
		$date_pay=$this->db->field("SELECT date FROM codapayments_line WHERE id='".$in['id']."' ");
		include_once('invoice.php');
		$pay=new invoice();
		foreach($in['invoices_fetched'] as $key=>$value){
			$data=array();
			$data['invoice_id']=$value['id'];
			$data['payment_date']=date('Y-m-d',$date_pay);
			$data['amount']=(count($in['invoices_fetched']) == 1 && $status == 1 ? display_number($left_to_pay) : $value['amount_vat']);
			$data['coda_pay']=$in['id'];
			$data['notes']='Codabox';
			$pay->pay($data);
			if($in['iban']){
				$customer=$this->db->query("SELECT bank_iban, customer_id FROM tblinvoice 
											INNER JOIN customers ON tblinvoice.buyer_id = customers.customer_id
											WHERE id='".$value['id']."' ");
				if(!$customer->f('bank_iban')){
					$this->db->query("UPDATE customers SET bank_iban ='".$in['iban']."' WHERE customer_id='".$customer->f('customer_id')."' ");
				}
			}
		}
		unset($_SESSION['add_to_fetch']);
		return true;
	}

	 function saveAddToFetch(&$in)
	  {

	      if($in['checked'] == 1){
	        $_SESSION['add_to_fetch'][$in['item']]= $in['checked'];
	      }else{
	        unset($_SESSION['add_to_fetch'][$in['item']]);
	      }

	      json_out($_SESSION['add_to_fetch']);
	    return true;
	  }

	  function unsetAddToFetch(&$in)
	  {
	    unset($_SESSION['add_to_fetch']);

	    return true;
	  }

	/**
	 * Mark as exported
	 *
	 * @return boolean
	 * @author Mp
	 **/
	function markFetched(&$in)
	{
		$this->db->query("UPDATE codapayments SET status='".$in['fetched']."' WHERE id='".$in['id']."' ");
		$this->db->query("UPDATE codapayments_line SET status='".$in['fetched']."' WHERE feed_index='".$in['id']."' ");
		return true;
	}

	function getConverted(&$in){
		$result=array();
		$result['rate']=floatval(currency::getCurrency($in['from'],$in['to'], 1,'.'));
		json_out($result);
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function exportBtb(&$in)
	{
		/*if(!DATABASE_NAME){
			define(DATABASE_NAME,$this->db->field("SELECT DATABASE()"));
		}*/
		$dropbox_data=$this->db->query("SELECT app_id,active FROM apps WHERE name='Dropbox' AND type='main'");
		$eff_drop_data=$this->db->query("SELECT api,active FROM apps WHERE main_app_id='".$dropbox_data->f('app_id')."' AND type='eff_drop'");
		if($dropbox_data->f('active') && $eff_drop_data->f('active')=='1'){
			$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
			$ACCOUNT_VAT_NUMBER=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
			$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");
			$params = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'dbase'=>$this->database_name);
			if($invoice->f('type')=='2'){
				if($invoice->f('pdf_layout')){
					$params['type']=$invoice->f('pdf_layout');
					$params['logo']=$invoice->f('pdf_logo');
				}else{
					$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
				}
				#if we are using a customer pdf template
				if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
					$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
				}
				$params['type_pdf']=2;
				$params['type']=ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
				$in['efff_str'] = $this->generate_pdf($params);
			}else{
				if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==0){
					$params['type']=$invoice->f('pdf_layout');
					$params['logo']=$invoice->f('pdf_logo');
				}else if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==1){
					$params['custom_type']=$invoice->f('pdf_layout');
					$params['logo']=$invoice->f('pdf_logo');
				}else{
					$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
				}
				#if we are using a customer pdf template
				if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
					$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
				}
				$in['efff_str'] = $this->generate_pdf($params);
			}
			$in['efff_xml']=true;
			$in['dbase']=$this->database_name;
			$in['id']=$in['invoice_id'];
			include(__DIR__.'/../controller/xml_invoice_print.php');
          	$xml_tmp_file_name=__DIR__."/../../../eff_".str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER)."_".substr(str_replace(array('.',' ','-','_'), '', $ACCOUNT_COMPANY),0,10)."_".$invoice->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr'))."_".substr(str_replace(array('.',' ','-','_'), '', $invoice->f('buyer_name')),0,10).".xml";
          	$d=new drop($eff_drop_data->f('api'),null,null,false,$this->database_name,0,null,null,null,true);
          	$someth=$d->upload("eff_".str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER)."_".substr(str_replace(array('.',' ','-','_'), '', $ACCOUNT_COMPANY),0,10)."_".$invoice->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr'))."_".mb_substr(str_replace(array('.',' ','-','_'), '', $invoice->f('buyer_name')),0,10, 'utf-8').".xml",$xml_tmp_file_name,null);
      		unlink($xml_tmp_file_name);
      		if(!is_null($someth)){
      			$in['actiune'] = gm('Exported');
	      		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."' ,winbooks=1 ");	
	      		if(array_key_exists('invoice_to_export', $_SESSION) === true){
	      			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
	      		}	
				if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
				if($in['from_acc']){
				 		$msg = '{l}Invoice was exported automatically to{endl} '.'E-FFF ';
				 	}else{
				 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'E-FFF ';
				 	}
		 		insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

				if($_SESSION['main_u_id']=='0'){
					//$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
					$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);         
				}else{
					$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
				}
				if($is_accountant){
					$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
				    if($accountant_settings=='1'){
				    	$acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
				    	if($acc_block_id=='1'){
				    		$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
				    	}
				    }else{
				    	$block_id=$this->dbu_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
						if($block_id==1){
							   $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
						}
				    }		
				}
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}     		
      		}else{
      			$in['actiune'] = gm('Fail');
      			msg::error(gm('Error uploading file'),'error');
				insert_error_call($this->database_name,"btb","invoice",$in['invoice_id'],"Error uploading file");
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);
				}
      		}	
		}else{
			if(!$this->user || !$this->pass || !$this->host){
				msg::error(gm('Invalid credentials'),'error');
				insert_error_call($this->database_name,"btb","invoice",$in['invoice_id'],"Invalid credentials");
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);
				}
			}

			$type = $this->db->field("SELECT type FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
			if($type=='0' || $type=='3'){
				#files to be created
				$prep = 'file_';
				$id = $this->createFiles($in);
			}elseif ($type=='2' || $type=='4') {
				$prep = 'file_credit_';
				$id = $this->createFilesCredit($in);
			}else{
				msg::error(gm('Invalid Invoice type'),'error');
				insert_error_call($this->database_name,"btb","invoice",$in['invoice_id'],"Invalid Invoice type");
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
			}
			if($id === false){
				msg::error(gm('Could not create file'),'error');
				insert_error_call($this->database_name,"btb","invoice",$in['invoice_id'],"Could not create file");
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
			}

			$array = array();
			// $array['file[0]'] = __DIR__.'/file_'.$id.'.json';//;type=application/json
			$array['file[1]'] = __DIR__.'/'.$prep.$id.'.xml';//;type=application/xml

			// $ftp = new ftp('188.64.51.139', 'test', 'W6852g4ulO89fxq');
			$ftp = new ftp($this->host, $this->user, $this->pass);
			// $ftp = new ftp('saftp.cloudapp.net', 'Oneitadmin', 'Oneit007//');
			$server_file = $id.'.xml';
			// $server_file = 'text.xml';
			$local_file = $array['file[1]'];
			$f = $ftp->writeFile( $server_file,$local_file);
			if(!$f){
				msg::error($result,"error");
				insert_error_call($this->database_name,"btb","invoice",$in['invoice_id'],$result);
				$in['actiune'] = gm('Failed');
			}else{
				$in['actiune'] = gm('Exported');
				$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."' ,winbooks=1 ");	
				if(array_key_exists('invoice_to_export', $_SESSION) === true){
					$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
				}	
				if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }

				if($in['from_acc']){
				 		$msg = '{l}Invoice was exported automatically to{endl} '.'E-FFF ';
				 	}else{
				 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'E-FFF ';
				 	}
		 		insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

				if($_SESSION['main_u_id']=='0'){
					//$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
					$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);         
				}else{
					$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
				}
				if($is_accountant){
					$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
				    if($accountant_settings=='1'){
				    	$acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
				    	if($acc_block_id=='1'){
				    		$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
				    	}
				    }else{
				    	$block_id=$this->dbu_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
						if($block_id==1){
						    $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
						}
				    }			
				}
			}

			// unlink($array['file[0]']);
			unlink($array['file[1]']);
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
			}
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function exportIncBtb(&$in)
	{
		$dropbox_data=$this->db->query("SELECT app_id,active FROM apps WHERE name='Dropbox' AND type='main'");
		$eff_drop_data=$this->db->query("SELECT api,active FROM apps WHERE main_app_id='".$dropbox_data->f('app_id')."' AND type='eff_drop'");
		if($dropbox_data->f('active') && $eff_drop_data->f('active')=='1'){
			
			$id = $this->createIncFiles($in);
			if($id === false){
				msg::error(gm('Could not create file'),'error');
				insert_error_call($this->database_name,"btb","purchase_invoice",$in['invoice_id'],"Could not create file");
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
			}

			$array = array();
			$array['file[0]'] = __DIR__.'/file_inc_'.$id.'.json';//;type=application/json
			$array['file[1]'] = __DIR__.'/file_inc_'.$id.'.xml';//;type=application/xml
          	$d=new drop($eff_drop_data->f('api'),null,null,false,$this->database_name,0,null,null,null,true);
          	$someth=$d->upload($id.'.xml',$array['file[1]'],null);
      		unlink($array['file[0]']);
			unlink($array['file[1]']);
      		if(!is_null($someth)){
      			$in['actiune'] = gm('Exported');
	      		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."' ,winbooks=1,type=1 ");
	      		if(array_key_exists('invoice_to_export', $_SESSION) === true){
	      			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");	
	      		}
				if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
				if($in['from_acc']){
				 		$msg = '{l}Invoice was exported automatically to{endl} '.'E-FFF ';
				 	}else{
				 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'E-FFF ';
				 	}
				insert_message_log('purchase_invoice',$msg,'purchase_invoice_id',$in['invoice_id']);

				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}    		
      		}else{
      			$in['actiune'] = gm('Fail');
      			msg::error(gm('Error uploading file'),'error');
				insert_error_call($this->database_name,"btb","purchase_invoice",$in['invoice_id'],"Error uploading file");
      			if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
      		}
		}else{
			if(!$this->user || !$this->pass || !$this->host){
				msg::error(gm('Invalid credentials'),'error');
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
			}

			$id = $this->createIncFiles($in);
			if($id === false){
				msg::error(gm('Could not create file'),'error');
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
			}
			// return;
			$array = array();
			$array['file[0]'] = __DIR__.'/file_inc_'.$id.'.json';//;type=application/json
			$array['file[1]'] = __DIR__.'/file_inc_'.$id.'.xml';//;type=application/xml

			$ftp = new ftp($this->host, $this->user, $this->pass);
			// $ftp = new ftp('saftp.cloudapp.net', 'Oneitadmin', 'Oneit007//');
			// $ftp = new ftp('188.64.51.139', 'test', 'W6852g4ulO89fxq');
			$server_file = $id.'.xml';
			$local_file = $array['file[1]'];
			$f = $ftp->writeFile( $server_file,$local_file);
	        	if(!$f){
				msg::error($result,'error');
				$in['actiune'] = gm('Failed');
			}else{
				$in['actiune'] = gm('Exported');
				$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."' ,winbooks=1,type=1 ");
				if(array_key_exists('invoice_to_export', $_SESSION) === true){
					$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
				}
				if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
				if($in['from_acc']){
				 		$msg = '{l}Invoice was exported automatically to{endl} '.'E-FFF ';
				 	}else{
				 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'E-FFF ';
				 	}
				insert_message_log('purchase_invoice',$msg,'purchase_invoice_id',$in['invoice_id']);
			}
			unlink($array['file[0]']);
			unlink($array['file[1]']);

			if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}
		}
	}

	/**
	 * delete validate
	 *
	 * @return bool
	 * @author PM
	 **/
	function generate_pdf(&$in)
	  {
	    if($in['type_pdf']==2){
	      include(__DIR__.'/../controller/invoice_credit_print.php');
	    }else{
	      include(__DIR__.'/../controller/invoice_print.php');
	    }
	    return $str;
	  }

	  function generate_pdf_oO($in)
	{
		include(__DIR__.'/../controller/winbooks_pdf.php');
		return $str;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function formatVAT($vat)
	{
		$vat = str_replace(array('.',' ','-','_'), '', $vat);
		return $vat;
	}

		/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function createFiles(&$in)
	{
		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$in['serial_number'] = $invoice->f('serial_number');
		$params = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'dbase'=>$this->database_name);

		if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==0){
			$params['type']=$invoice->f('pdf_layout');
			$params['logo']=$invoice->f('pdf_logo');
		}else if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==1){
			$params['custom_type']=$invoice->f('pdf_layout');
			$params['logo']=$invoice->f('pdf_logo');
		}else{
			$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		#if we are using a customer pdf template
		if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
			$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		$str = $this->generate_pdf($params);
		$currency = currency::get_currency($invoice->f('currency_type'),'code');

		// if($invoice->f('contact_id')){
			$contact = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."' ");
		// }
		$tax_id=array(
				array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
				array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
				array('0'=>'00','6'=>'','12'=>'','21'=>''),
				array('0'=>'00','6'=>'','12'=>'','21'=>''),
				array('0'=>'00','6'=>'','12'=>'','21'=>''),
			);
		$tax_name=array(
			array('0'=>'Z','6'=>'AA','12'=>'AA','21'=>'S'),
			array('0'=>'Z','6'=>'AA','12'=>'AA','21'=>'S'),
			array('0'=>'AE','6'=>'','12'=>'','21'=>''),
			array('0'=>'E','6'=>'','12'=>'','21'=>''),
			array('0'=>'AE','6'=>'','12'=>'','21'=>''),
			);
		if($invoice->f('vat_regime_id')>=10000){
			$regime_type=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
		}
		$ACCOUNT_ADDRESS_INVOICE=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
		$ACCOUNT_DELIVERY_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
		$ACCOUNT_DELIVERY_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
		$ACCOUNT_DELIVERY_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
		$ACCOUNT_DELIVERY_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
		$ACCOUNT_BILL_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
		$ACCOUNT_BILL_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
		$ACCOUNT_BILL_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
		$ACCOUNT_BILL_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
		$ACCOUNT_VAT_NUMBER=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		$ACCOUNT_IBAN=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_IBAN' ");
		$ACCOUNT_BIC=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BIC_CODE' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");

		$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
		$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
		$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
		$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;
		$get_invoice_rows = $this->db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");
		$customer_reference=$this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");

		$i = 0;
		$total_amount = 0;
		$vat_percent = array();
		$sub_t = array();
		$sub_disc = array();
		$vat_percent_val = 0;
		$discount = $invoice->f('discount');
		if($invoice->f('apply_discount')<2){
			$discount=0;
		}
		while ($get_invoice_rows->next()){
			$line_d = $get_invoice_rows->f('discount');
			if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
				$line_d = 0;
			}
			$amount = $get_invoice_rows->f('quantity') *($get_invoice_rows->f('price') - ( $get_invoice_rows->f('price') * $line_d / 100 ) );
			$amount = round($amount,ARTICLE_PRICE_COMMA_DIGITS);

			$total_amount += $get_invoice_rows->f('amount');
			$i++;

			$amount_d = $amount * $discount/100;
			// console::log($amount_d);
			if($invoice->f('apply_discount') < 2){
				$amount_d = 0;
			}
			$gl_account='';
			if($get_invoice_rows->f('article_id')){
				$line_gl=$this->db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' ");
				if($line_gl){
					$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$line_gl."' ");
				}
			}
			if((!$gl_account || $gl_account=='') && !$get_invoice_rows->f('content')){
				$app_gl=$this->db->field("SELECT value FROM settings WHERE constant_name='BTB_LEDGER_ID' ");
				if($app_gl){
					$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$app_gl."' ");
				}
			}

			// console::log($amount , $amount_d);
			$vat_percent_val = ($amount - $amount_d) * $get_invoice_rows->f('vat')/100;

			$vat_percent[$get_invoice_rows->f('vat')] += $vat_percent_val;
			$sub_t[$get_invoice_rows->f('vat')] += $amount;
			$sub_disc[$get_invoice_rows->f('vat')] += $amount_d;
			$ln_amount=twoDecNumber($amount-$amount_d);
			$ln_amount_vat = twoDecNumber($ln_amount*$get_invoice_rows->f('vat')/100);
			$unit_price = twoDecNumber(($get_invoice_rows->f('price')- $get_invoice_rows->f('price') * $line_d /100)-($get_invoice_rows->f('price')- $get_invoice_rows->f('price') * $line_d /100)*$discount/100);
			$item_code_xml = '';
			if(htmlspecialchars($get_invoice_rows->f('item_code'))){
				$item_code_xml.= '<cac:SellersItemIdentification>
                <cbc:ID>'.htmlspecialchars($get_invoice_rows->f('item_code')).'</cbc:ID>
            </cac:SellersItemIdentification>';
        	}
            	$item_code_xml.='<cac:ClassifiedTaxCategory>';
					if($invoice->f('vat_regime_id')<5){
						$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:ID>
						<cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:Name>';
					}else{
						if($regime_type==1){
							$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$get_invoice_rows->f('vat')].'</cbc:ID>
						<cbc:Name>'.$tax_id[0][$get_invoice_rows->f('vat')].'</cbc:Name>';
						}else if($regime_type==2){
							$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$get_invoice_rows->f('vat')].'</cbc:ID>
						<cbc:Name>'.$tax_id[2][$get_invoice_rows->f('vat')].'</cbc:Name>';
						}else{
							$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$get_invoice_rows->f('vat')].'</cbc:ID>
						<cbc:Name>'.$tax_id[3][$get_invoice_rows->f('vat')].'</cbc:Name>';
						}
					}		
					$item_code_xml.='<cbc:Percent>'.twoDecNumber($get_invoice_rows->f('vat')).'</cbc:Percent>
					<cac:TaxScheme>
						<cbc:ID>VAT</cbc:ID>
					</cac:TaxScheme>
				</cac:ClassifiedTaxCategory>';
			//}
			$line_xml = '
	  <cac:InvoiceLine>
		<cbc:ID>'.$i.'</cbc:ID>
		<cbc:InvoicedQuantity unitCode="NAR" unitCodeListID="UNECERec20">'.twoDecNumber($get_invoice_rows->f('quantity')).'</cbc:InvoicedQuantity>
		<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount).'</cbc:LineExtensionAmount>
		<cbc:AccountingCost>'.$gl_account.'</cbc:AccountingCost>
		<cac:TaxTotal>
		  <cbc:TaxAmount currencyID="'.$currency.'">'.$ln_amount_vat.'</cbc:TaxAmount>';
		  /*<cac:TaxSubtotal>
		    <cbc:TaxableAmount currencyID="'.$currency.'">'.$ln_amount.'</cbc:TaxableAmount>
		    <cbc:TaxAmount currencyID="'.$currency.'">'.$ln_amount_vat.'</cbc:TaxAmount>		 
		    <cac:TaxCategory>';
		    if($invoice->f('vat_regime_id')<5){
		    	$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:Name>';
		    }else{
				if($regime_type==1){
					$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[0][$get_invoice_rows->f('vat')].'</cbc:Name>';
				}else if($regime_type==2){
					$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[2][$get_invoice_rows->f('vat')].'</cbc:Name>';
				}else{
					$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[3][$get_invoice_rows->f('vat')].'</cbc:Name>';
				}	    	
		    }
		      $line_xml.='<cbc:Percent>'.twoDecNumber($get_invoice_rows->f('vat')).'</cbc:Percent>
		      <cbc:TaxExemptionReason/>
			<cac:TaxScheme>
				<cbc:ID>VAT</cbc:ID>
			</cac:TaxScheme>
		    </cac:TaxCategory>
		  </cac:TaxSubtotal>*/
		$line_xml.='</cac:TaxTotal>
		<cac:Item>
		  <cbc:Description>'.htmlspecialchars($get_invoice_rows->f('name')).'</cbc:Description>
		  <cbc:Name>'.htmlspecialchars($get_invoice_rows->f('name')).'</cbc:Name>
		  '.$item_code_xml.'	        
		</cac:Item>
		<cac:Price>
			<cbc:PriceAmount currencyID="'.$currency.'">'.twoDecNumber($unit_price).'</cbc:PriceAmount>
		</cac:Price>         
       </cac:InvoiceLine>';
	       		$xml_lines.=$line_xml;
	    //    		<Description>'.htmlspecialchars($get_invoice_rows->f('name')).'</Description>
     //     <Remarks/>
     //     <ProductQuantity>'.$get_invoice_rows->f('quantity').'</ProductQuantity>
  		 // <LineAmount>'.$ln_amount.'</LineAmount>
     //     <LineVATAmount>'.$ln_amount_vat.'</LineVATAmount>
     //     <Product>
     //     	<Description>'.$productDesc.'</Description>
     //     	<SalesPrice>'.$unit_price.'</SalesPrice>
     //     	<VATPercentage>'.$get_invoice_rows->f('vat').'</VATPercentage>
     //     	<VATIncluded>false</VATIncluded>
     //     	<VATType>'.$vat_type.'</VATType>
     //     </Product>
		}
		$tax_str ='';
		$vat_v = 0;
		$total_no_vat = 0;
		$total_vat = 0;
		// console::log($vat_percent);
		foreach ($vat_percent as $key => $val){
			 if($sub_t[$key] - $sub_disc[$key]){
			 	$line_xml='<cac:TaxSubtotal>
      <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($sub_t[$key] - $sub_disc[$key]).'</cbc:TaxableAmount>
      <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:TaxAmount>
      <cac:TaxCategory>';
      if($invoice->f('vat_regime_id')<5){
      	$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$key].'</cbc:Name>';
      }else{
      	if($regime_type==1){
      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[0][$key].'</cbc:Name>';
      	}else if($regime_type==2){
      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[2][$key].'</cbc:Name>';
      	}else{
      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[3][$key].'</cbc:Name>';
      	}
      }
        $line_xml.='<cbc:Percent>'.$key.'</cbc:Percent>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:TaxCategory>
    </cac:TaxSubtotal>';
    			$tax_str.=$line_xml;
    			$vat_v += $val;
    			$total_no_vat += $sub_t[$key] - $sub_disc[$key];
    			$total_vat +=$sub_t[$key] - $sub_disc[$key]+$val;
			}
		}

	$code_supplier=get_country_code($ACCOUNT_DELIVERY_COUNTRY_ID)=='BE' ? 'BE:CBE' : '';
	$code_supplier_simple=get_country_code($ACCOUNT_DELIVERY_COUNTRY_ID)=='BE' ? 'BE' : '';
	$code_customer=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE:CBE' : '';
	$code_customer_simple=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE' : '';
	$contactUblEmail=ublContactEmail(['db'=>$this->database_name,'customer_id'=>$invoice->f('buyer_id'),'contact_id'=>$invoice->f('contact_id')]);
		// $json = '{"owner":"'.$invoice->f('seller_bwt_nr').'","owner_id_type":"VAT","destination":"'.$this->formatVAT(ACCOUNT_VAT_NUMBER).'","dest_id_type":"VAT","format":"pdf","docid":"file_'.$invoice->f('serial_number').'","immediate":"Y","source":"SalesAssist V 2.56","channel":""}';
		$xml ='<?xml version="1.0" encoding="UTF-8"?>
<Invoice xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2">
  <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
  <cbc:CustomizationID>urn:www.cenbii.eu:transaction:biitrns010:ver2.0:extended:urn:www.peppol.eu:bis:peppol5a:ver2.0:extended:e-fff:ver3.0</cbc:CustomizationID>
  <cbc:ProfileID>urn:www.cenbii.eu:profile:bii05:ver2.0</cbc:ProfileID>
  <cbc:ID>'.$invoice->f('serial_number').'</cbc:ID>
  <cbc:IssueDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</cbc:IssueDate>
  <cbc:InvoiceTypeCode listID="UNCL1001">380</cbc:InvoiceTypeCode>
  <cbc:DocumentCurrencyCode listID="ISO4217">'.$currency.'</cbc:DocumentCurrencyCode>
  <cac:OrderReference>
    <cbc:ID>'.$invoice->f('your_ref').'</cbc:ID>
  </cac:OrderReference>
  <cac:AdditionalDocumentReference>
    <cbc:ID>Invoice-'.$invoice->f('serial_number').'</cbc:ID>
    <cbc:DocumentType>CommercialInvoice</cbc:DocumentType>
    <cac:Attachment>
      <cbc:EmbeddedDocumentBinaryObject mimeCode="application/pdf"
        >'.$str.'</cbc:EmbeddedDocumentBinaryObject>
    </cac:Attachment>
  </cac:AdditionalDocumentReference>
  <cac:AdditionalDocumentReference>
	<cbc:ID>eFFF</cbc:ID>
	<cbc:DocumentType>Akti</cbc:DocumentType>
  </cac:AdditionalDocumentReference>
   <cac:AccountingSupplierParty>
    <cac:Party>
      <cbc:EndpointID schemeID="'.$code_supplier.'">'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:EndpointID>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars($ACCOUNT_COMPANY).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.$ACCOUNT_BILLING_ADDRESS.'</cbc:StreetName>
        <cbc:CityName>'.$ACCOUNT_BILLING_CITY.'</cbc:CityName>
        <cbc:PostalZone>'.$ACCOUNT_BILLING_ZIP.'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID schemeID="'.$code_supplier_simple.'">'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:CompanyID>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:PartyTaxScheme>
      <cac:PartyLegalEntity>
		<cbc:CompanyID>'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:CompanyID>
	</cac:PartyLegalEntity>
    </cac:Party>
  </cac:AccountingSupplierParty>
  <cac:AccountingCustomerParty>
    <cbc:SupplierAssignedAccountID>'.htmlspecialchars(stripslashes($customer_reference),ENT_QUOTES | ENT_HTML5).'</cbc:SupplierAssignedAccountID>
    <cac:Party>
      <cbc:EndpointID schemeID="'.$code_customer.'">'.$this->formatVAT($invoice->f('seller_bwt_nr')).'</cbc:EndpointID>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars($invoice->f('buyer_name')).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.$invoice->f('buyer_address').'</cbc:StreetName>
        <cbc:CityName>'.$invoice->f('buyer_city').'</cbc:CityName>
        <cbc:PostalZone>'.$invoice->f('buyer_zip').'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($invoice->f('buyer_country_id')).'</cbc:IdentificationCode>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID schemeID="'.$code_customer_simple.'">'.$this->formatVAT($invoice->f('seller_bwt_nr')).'</cbc:CompanyID>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:PartyTaxScheme>
      <cac:PartyLegalEntity>
		<cbc:CompanyID>'.$this->formatVAT($invoice->f('seller_bwt_nr')).'</cbc:CompanyID>
	</cac:PartyLegalEntity>
      <cac:Contact>
        <cbc:Name>'.($contact->f('fistname') ? $contact->f('fistname').' '.$contact->f('lastname') : $contact->f('lastname')).'</cbc:Name>
        <cbc:Telephone>'.$contact->f('phone').'</cbc:Telephone>
        <cbc:Telefax></cbc:Telefax>
        <cbc:ElectronicMail>'.$contactUblEmail.'</cbc:ElectronicMail>
      </cac:Contact>
    </cac:Party>
  </cac:AccountingCustomerParty>
  <cac:PaymentMeans>
    <cbc:PaymentMeansCode listID="UNCL4461">1</cbc:PaymentMeansCode>
    <cbc:PaymentDueDate>'.date('Y-m-d',$invoice->f('due_date')).'</cbc:PaymentDueDate>
    <cbc:PaymentID>'.str_replace('/', '', $invoice->f('ogm')).'</cbc:PaymentID>
    <cac:PayeeFinancialAccount>
        <cbc:ID schemeName="IBAN">'.$ACCOUNT_IBAN.'</cbc:ID>
        <cac:FinancialInstitutionBranch>
          <cac:FinancialInstitution>
            <cbc:ID schemeName="BIC">'.$ACCOUNT_BIC.'</cbc:ID>
          </cac:FinancialInstitution>
        </cac:FinancialInstitutionBranch>
      </cac:PayeeFinancialAccount>
  </cac:PaymentMeans>
  <cac:PaymentTerms></cac:PaymentTerms>
  <cac:TaxTotal>
    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($vat_v).'</cbc:TaxAmount>
    '.$tax_str.'
  </cac:TaxTotal>
  <cac:LegalMonetaryTotal>
    <cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:LineExtensionAmount>
    <cbc:TaxExclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:TaxExclusiveAmount>
    <cbc:TaxInclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:TaxInclusiveAmount>
    <cbc:PrepaidAmount currencyID="'.$currency.'">0.00</cbc:PrepaidAmount>
    <cbc:PayableAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:PayableAmount>
  </cac:LegalMonetaryTotal>
  '.$xml_lines.'
</Invoice>
';

		if(file_put_contents(__DIR__.'/file_'.addcslashes( $invoice->f('serial_number'),'/').'.xml', $xml) === false){
			return false;
		}
		return $invoice->f('serial_number');
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function createFilesCredit(&$in)
	{
		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$in['serial_number'] = $invoice->f('serial_number');
		$params = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'dbase'=>$this->database_name);

		if($invoice->f('pdf_layout')){
			$params['type']=$invoice->f('pdf_layout');
			$params['logo']=$invoice->f('pdf_logo');
		}else{
			$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		#if we are using a customer pdf template
		if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
			$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		$params['type_pdf']=2;
		$params['type']=ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
		$str = $this->generate_pdf($params);
		$currency = currency::get_currency($invoice->f('currency_type'),'code');

		// if($invoice->f('contact_id')){
			$contact = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."' ");
		// }
		$tax_id=array(
				array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
				array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
				array('0'=>'00','6'=>'','12'=>'','21'=>''),
				array('0'=>'00','6'=>'','12'=>'','21'=>''),
				array('0'=>'00','6'=>'','12'=>'','21'=>''),
			);
		$tax_name=array(
			array('0'=>'Z','6'=>'AA','12'=>'AA','21'=>'S'),
			array('0'=>'Z','6'=>'AA','12'=>'AA','21'=>'S'),
			array('0'=>'AE','6'=>'','12'=>'','21'=>''),
			array('0'=>'E','6'=>'','12'=>'','21'=>''),
			array('0'=>'AE','6'=>'','12'=>'','21'=>''),
			);
		if($invoice->f('vat_regime_id')>=10000){
			$regime_type=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
		}
		$ACCOUNT_ADDRESS_INVOICE=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
		$ACCOUNT_DELIVERY_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
		$ACCOUNT_DELIVERY_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
		$ACCOUNT_DELIVERY_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
		$ACCOUNT_DELIVERY_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
		$ACCOUNT_BILL_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
		$ACCOUNT_BILL_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
		$ACCOUNT_BILL_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
		$ACCOUNT_BILL_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
		$ACCOUNT_VAT_NUMBER=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		$ACCOUNT_IBAN=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_IBAN' ");
		$ACCOUNT_BIC=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BIC_CODE' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");

		$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
		$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
		$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
		$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;
		$get_invoice_rows = $this->db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");
		$customer_reference=$this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");
		$USE_NEGATIVE_CREDIT=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");

		$i = 0;
		$total_amount = 0;
		$vat_percent = array();
		$sub_t = array();
		$sub_disc = array();
		$vat_percent_val = 0;
		$discount = $invoice->f('discount');
		if($invoice->f('apply_discount')<2){
			$discount=0;
		}
		while ($get_invoice_rows->next()){
			$line_d = $get_invoice_rows->f('discount');
			if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
				$line_d = 0;
			}
			$amount = $get_invoice_rows->f('quantity') *($get_invoice_rows->f('price') - ( $get_invoice_rows->f('price') * $line_d / 100 ) );
			$amount = round($amount,ARTICLE_PRICE_COMMA_DIGITS);

			$total_amount += $get_invoice_rows->f('amount');
			$i++;

			$amount_d = $amount * $discount/100;
			if($invoice->f('apply_discount') < 2){
				$amount_d = 0;
			}
			$vat_percent_val = ($amount - $amount_d) * $get_invoice_rows->f('vat')/100;

			$gl_account='';
			if($get_invoice_rows->f('article_id')){
				$line_gl=$this->db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' ");
				if($line_gl){
					$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$line_gl."' ");
				}
			}
			if((!$gl_account || $gl_account=='') && !$get_invoice_rows->f('content')){
				$app_gl=$this->db->field("SELECT value FROM settings WHERE constant_name='BTB_LEDGER_ID' ");
				if($app_gl){
					$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$app_gl."' ");
				}
			}

			$vat_percent[$get_invoice_rows->f('vat')] += $vat_percent_val;
			$sub_t[$get_invoice_rows->f('vat')] += $amount;
			$sub_disc[$get_invoice_rows->f('vat')] += $amount_d;
			$ln_amount=twoDecNumber($amount-$amount_d);
			$ln_amount_vat = twoDecNumber($ln_amount*$get_invoice_rows->f('vat')/100);
			$unit_price = twoDecNumber(($get_invoice_rows->f('price')- $get_invoice_rows->f('price') * $line_d /100)-($get_invoice_rows->f('price')- $get_invoice_rows->f('price') * $line_d /100)*$discount/100);
			$item_code_xml = '';
			if(htmlspecialchars($get_invoice_rows->f('item_code'))){
				$item_code_xml.= '<cac:SellersItemIdentification>
                <cbc:ID>'.htmlspecialchars($get_invoice_rows->f('item_code')).'</cbc:ID>
            </cac:SellersItemIdentification>';
        	}
            		$item_code_xml.='<cac:ClassifiedTaxCategory>';
					if($invoice->f('vat_regime_id')<5){
						$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:ID>
						<cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:Name>';
					}else{
						if($regime_type==1){
							$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$get_invoice_rows->f('vat')].'</cbc:ID>
							<cbc:Name>'.$tax_id[0][$get_invoice_rows->f('vat')].'</cbc:Name>';
						}else if($regime_type==2){
							$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$get_invoice_rows->f('vat')].'</cbc:ID>
							<cbc:Name>'.$tax_id[2][$get_invoice_rows->f('vat')].'</cbc:Name>';
						}else{
							$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$get_invoice_rows->f('vat')].'</cbc:ID>
							<cbc:Name>'.$tax_id[3][$get_invoice_rows->f('vat')].'</cbc:Name>';
						}
					}
					$item_code_xml.='<cbc:Percent>'.twoDecNumber($get_invoice_rows->f('vat')).'</cbc:Percent>
					<cac:TaxScheme>
						<cbc:ID>VAT</cbc:ID>
					</cac:TaxScheme>
				</cac:ClassifiedTaxCategory>';
			//}
			$line_xml = '
	  <cac:CreditNoteLine>
		<cbc:ID>'.$i.'</cbc:ID>
		<cbc:CreditedQuantity unitCode="NAR" unitCodeListID="UNECERec20">'.twoDecNumber($get_invoice_rows->f('quantity')).'</cbc:CreditedQuantity>
		<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount).'</cbc:LineExtensionAmount>
		<cbc:AccountingCost>'.$gl_account.'</cbc:AccountingCost>
		<cac:TaxTotal>
		  <cbc:TaxAmount currencyID="'.$currency.'">'.$ln_amount_vat.'</cbc:TaxAmount>
		  <cac:TaxSubtotal>
		    <cbc:TaxableAmount currencyID="'.$currency.'">'.$ln_amount.'</cbc:TaxableAmount>
		    <cbc:TaxAmount currencyID="'.$currency.'">'.$ln_amount_vat.'</cbc:TaxAmount>	    
		    <cac:TaxCategory>';
		    if($invoice->f('vat_regime_id')<5){
		    	$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$get_invoice_rows->f('vat')].'</cbc:Name>';
		    }else{
		    	if($regime_type==1){
		    		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[0][$get_invoice_rows->f('vat')].'</cbc:Name>';
		    	}else if($regime_type==2){
		    		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[2][$get_invoice_rows->f('vat')].'</cbc:Name>';
		    	}else{
		    		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$get_invoice_rows->f('vat')].'</cbc:ID>
		      <cbc:Name>'.$tax_id[3][$get_invoice_rows->f('vat')].'</cbc:Name>';
		    	}
		    }
		      $line_xml.='<cbc:Percent>'.twoDecNumber($get_invoice_rows->f('vat')).'</cbc:Percent>
		      <cac:TaxScheme>
			   <cbc:ID>VAT</cbc:ID>
			</cac:TaxScheme>
		    </cac:TaxCategory>
		  </cac:TaxSubtotal>
		</cac:TaxTotal>
		<cac:Item>
		  <cbc:Description>'.htmlspecialchars($get_invoice_rows->f('name')).'</cbc:Description>
		  <cbc:Name>'.htmlspecialchars($get_invoice_rows->f('name')).'</cbc:Name>
		  '.$item_code_xml.'		        
		</cac:Item>
		<cac:Price>
			<cbc:PriceAmount currencyID="'.$currency.'">'.twoDecNumber($unit_price).'</cbc:PriceAmount>
		</cac:Price>         
       </cac:CreditNoteLine>';
	       		$xml_lines.=$line_xml;
		}
		$tax_str ='';
		$vat_v = 0;
		$total_no_vat = 0;
		$total_vat = 0;
		foreach ($vat_percent as $key => $val){
			 if($sub_t[$key] - $sub_disc[$key]){
			 	$line_xml='<cac:TaxSubtotal>
      <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($sub_t[$key] - $sub_disc[$key]).'</cbc:TaxableAmount>
      <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:TaxAmount>
      <cac:TaxCategory>';
      if($invoice->f('vat_regime_id')<5){
      	$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$key].'</cbc:Name>';
      }else{
      	if($regime_type==1){
      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[0][$key].'</cbc:Name>';
      	}else if($regime_type==2){
      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[2][$key].'</cbc:Name>';
      	}else{
      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$key].'</cbc:ID>
        <cbc:Name>'.$tax_id[3][$key].'</cbc:Name>';
      	}
      }
        $line_xml.='<cbc:Percent>'.$key.'</cbc:Percent>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:TaxCategory>
    </cac:TaxSubtotal>';
    			$tax_str.=$line_xml;
    			$vat_v += $val;
    			$total_no_vat += $sub_t[$key] - $sub_disc[$key];
    			$total_vat +=$sub_t[$key] - $sub_disc[$key] + $val;
			}
		}
		$code_supplier=get_country_code($ACCOUNT_DELIVERY_COUNTRY_ID)=='BE' ? 'BE:VAT' : '';
		$code_supplier_simple=get_country_code($ACCOUNT_DELIVERY_COUNTRY_ID)=='BE' ? 'BE' : '';
		$code_customer=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE:VAT' : '';
		$code_customer_simple=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE' : '';
		$contactUblEmail=ublContactEmail(['db'=>$this->database_name,'customer_id'=>$invoice->f('buyer_id'),'contact_id'=>$invoice->f('contact_id')]);

		if($USE_NEGATIVE_CREDIT){
			if($USE_NEGATIVE_CREDIT == 1){
				if($total_vat < 0){ #negativ amounts
					$doc_type = '381';
				}else{
					$doc_type = '380';
				}
			}else{
				if($total_vat < 0){ #negativ amounts
					$doc_type = '380';
				}else{
					$doc_type = '381';
				}
			}
		}else{
			if($total_vat < 0){ #negativ amounts
				$doc_type = '380';
			}else{
				$doc_type = '381';
			}
		}

		// $json = '{"owner":"'.$invoice->f('seller_bwt_nr').'","owner_id_type":"VAT","destination":"'.$this->formatVAT(ACCOUNT_VAT_NUMBER).'","dest_id_type":"VAT","format":"pdf","docid":"file_'.$invoice->f('serial_number').'","immediate":"Y","source":"SalesAssist V 2.56","channel":""}';
		$xml ='<?xml version="1.0" encoding="UTF-8"?>
<CreditNote xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2">
  <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
  <cbc:CustomizationID>urn:www.cenbii.eu:transaction:biitrns010:ver2.0:extended:urn:www.peppol.eu:bis:peppol5a:ver2.0:extended:e-fff:ver3.0</cbc:CustomizationID>
  <cbc:ProfileID>urn:www.cenbii.eu:profile:bii05:ver2.0</cbc:ProfileID>
  <cbc:ID>'.$invoice->f('serial_number').'</cbc:ID>
  <cbc:IssueDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</cbc:IssueDate>
  <cbc:DocumentCurrencyCode listID="ISO4217">'.$currency.'</cbc:DocumentCurrencyCode>
  <cac:OrderReference>
    <cbc:ID>'.$invoice->f('your_ref').'</cbc:ID>
</cac:OrderReference>
  <cac:BillingReference>
	<cac:InvoiceDocumentReference>
		<cbc:ID>'.$invoice->f('our_ref').'</cbc:ID>
	</cac:InvoiceDocumentReference>
  </cac:BillingReference>
  <cac:AdditionalDocumentReference>
    <cbc:ID>Invoice-'.$invoice->f('serial_number').'</cbc:ID>
    <cbc:DocumentType>CommercialInvoice</cbc:DocumentType>
    <cac:Attachment>
      <cbc:EmbeddedDocumentBinaryObject mimeCode="application/pdf"
        >'.$str.'</cbc:EmbeddedDocumentBinaryObject>
    </cac:Attachment>
  </cac:AdditionalDocumentReference>
  <cac:AdditionalDocumentReference>
	<cbc:ID>eFFF</cbc:ID>
	<cbc:DocumentType>Akti</cbc:DocumentType>
  </cac:AdditionalDocumentReference>
   <cac:AccountingSupplierParty>
    <cac:Party>
    	<cbc:EndpointID schemeID="'.$code_supplier.'">'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:EndpointID>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars($ACCOUNT_COMPANY).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.$ACCOUNT_BILLING_ADDRESS.'</cbc:StreetName>
        <cbc:CityName>'.$ACCOUNT_BILLING_CITY.'</cbc:CityName>
        <cbc:PostalZone>'.$ACCOUNT_BILLING_ZIP.'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID schemeID="'.$code_supplier_simple.'">'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:CompanyID>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:PartyTaxScheme>
      <cac:PartyLegalEntity>
      	<cbc:RegistrationName>'.htmlspecialchars($ACCOUNT_COMPANY).'</cbc:RegistrationName>
		<cbc:CompanyID>'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:CompanyID>
		<cac:RegistrationAddress>
		<cbc:CityName>'.$ACCOUNT_BILLING_CITY.'</cbc:CityName>
			<cac:Country>
				<cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
			</cac:Country>
		</cac:RegistrationAddress>
	</cac:PartyLegalEntity>
    </cac:Party>
  </cac:AccountingSupplierParty>
  <cac:AccountingCustomerParty>
    <cbc:SupplierAssignedAccountID>'.htmlspecialchars(stripslashes($customer_reference),ENT_QUOTES | ENT_HTML5).'</cbc:SupplierAssignedAccountID>
    <cac:Party>
      <cbc:EndpointID schemeID="'.$code_customer.'">'.$this->formatVAT($invoice->f('seller_bwt_nr')).'</cbc:EndpointID>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars($invoice->f('buyer_name')).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.$invoice->f('buyer_address').'</cbc:StreetName>
        <cbc:CityName>'.$invoice->f('buyer_city').'</cbc:CityName>
        <cbc:PostalZone>'.$invoice->f('buyer_zip').'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($invoice->f('buyer_country_id')).'</cbc:IdentificationCode>
          <cbc:Name></cbc:Name>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID schemeID="'.$code_customer_simple.'">'.$this->formatVAT($invoice->f('seller_bwt_nr')).'</cbc:CompanyID>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:PartyTaxScheme>
      cac:PartyLegalEntity>
      	<cbc:RegistrationName>'.htmlspecialchars($invoice->f('buyer_name')).'</cbc:RegistrationName>
		<cbc:CompanyID>'.$this->formatVAT($invoice->f('seller_bwt_nr')).'</cbc:CompanyID>
		<cac:RegistrationAddress>
		<cbc:CityName>'.$invoice->f('buyer_city').'</cbc:CityName>
			<cac:Country>
				<cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($invoice->f('buyer_country_id')).'</cbc:IdentificationCode>
			</cac:Country>
		</cac:RegistrationAddress>
	</cac:PartyLegalEntity>
      <cac:Contact>
        <cbc:Name>'.($contact->f('fistname') ? $contact->f('fistname').' '.$contact->f('lastname') : $contact->f('lastname')).'</cbc:Name>
        <cbc:Telephone>'.$contact->f('phone').'</cbc:Telephone>
        <cbc:Telefax></cbc:Telefax>
        <cbc:ElectronicMail>'.$contactUblEmail.'</cbc:ElectronicMail>
      </cac:Contact>
    </cac:Party>
  </cac:AccountingCustomerParty>
  <cac:PaymentMeans>
    <cbc:PaymentMeansCode listID="UNCL4461">1</cbc:PaymentMeansCode>
    <cbc:PaymentDueDate>'.date('Y-m-d',$invoice->f('due_date')).'</cbc:PaymentDueDate>
    <cbc:InstructionID>'.str_replace('/', '', $invoice->f('ogm')).'</cbc:InstructionID>
    <cac:PayeeFinancialAccount>
        <cbc:ID schemeName="IBAN">'.$ACCOUNT_IBAN.'</cbc:ID>
        <cac:FinancialInstitutionBranch>
          <cac:FinancialInstitution>
            <cbc:ID schemeName="BIC">'.$ACCOUNT_BIC.'</cbc:ID>
          </cac:FinancialInstitution>
        </cac:FinancialInstitutionBranch>
      </cac:PayeeFinancialAccount>
  </cac:PaymentMeans>
  <cac:PaymentTerms></cac:PaymentTerms>
  <cac:TaxTotal>
    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($vat_v).'</cbc:TaxAmount>
    '.$tax_str.'
  </cac:TaxTotal>
  <cac:LegalMonetaryTotal>
    <cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:LineExtensionAmount>
    <cbc:TaxExclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:TaxExclusiveAmount>
    <cbc:TaxInclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:TaxInclusiveAmount>
    <cbc:PayableAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:PayableAmount>
  </cac:LegalMonetaryTotal>
  '.$xml_lines.'
</CreditNote>
';

		if(file_put_contents(__DIR__.'/file_credit_'.$invoice->f('serial_number').'.xml', $xml) === false){
			return false;
		}
		return $invoice->f('serial_number');
	}

		/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function createIncFiles(&$in)
	{
		global $config;
		$str = '';
		$ACCOUNT_ADDRESS_INVOICE=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
		$ACCOUNT_DELIVERY_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
		$ACCOUNT_DELIVERY_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
		$ACCOUNT_DELIVERY_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
		$ACCOUNT_DELIVERY_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
		$ACCOUNT_BILL_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
		$ACCOUNT_BILL_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
		$ACCOUNT_BILL_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
		$ACCOUNT_BILL_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
		$ACCOUNT_VAT_NUMBER=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		$ACCOUNT_IBAN=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_IBAN' ");
		$ACCOUNT_BIC=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BIC_CODE' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");

		$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
		$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
		$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
		$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;


		$is_active = $this->db->field("SELECT active FROM apps WHERE name = 'Dropbox'");
		$invoice = $this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");
		if($invoice->f('file_path') && $is_active==1 && $invoice->f('amazon') !=1){
			$d = new drop('invoices',null,null,false,$this->database_name);
			// $link=$d->getLink(urldecode($invoice->f('file_path')));
			// $str = base64_encode(file_get_contents($link));
			@mkdir('upload/'.$this->database_name.'/temp_drop_pdfs/',0775,true);
			$name = substr(urldecode($invoice->f('file_path')), strrpos(urldecode($invoice->f('file_path')), '/')+1);
			$f = fopen("upload/".$this->database_name."/temp_drop_pdfs/".$name, 'w+b');
			$d->getFile(urldecode($invoice->f('file_path')), $f);
			fclose($f);
			$str = base64_encode(file_get_contents("upload/".$this->database_name."/temp_drop_pdfs/".$name ));
		}
		if($invoice->f('amazon') ==1){
			ark::loadCronLibraries(array('aws'));
			$aws = new awsWrap($this->database_name);
			$exist =  $aws->doesObjectExist($config['awsBucket'].$this->database_name.'/'.$invoice->f('file_name'));
			if($exist){
				$link = $aws->getLink($config['awsBucket'].$this->database_name.'/'.$invoice->f('file_name'));
				$content = file_get_contents($link);
				$str = base64_encode($content);
			}
		}
		if($invoice->f('supplier_id')){
			$bName= $this->db->query("SELECT * FROM customers WHERE customer_id='".$invoice->f('supplier_id')."'");
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$invoice->f('supplier_id')."' AND billing='1' ");
		}elseif ($invoice->f('contact_id')) {
			$bName= $this->db->query("SELECT CONCAT_WS(' ',firstname, lastname) AS name FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."'");
			$address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$invoice->f('contact_id')."' AND is_primary='1' ");
		}
		$ExpCategory='';
		if($invoice->f('expense_category_id')){
			$ExpCategory= $this->db->field("SELECT name FROM tblinvoice_expense_categories WHERE id='".$invoice->f('expense_category_id')."'");
		}

		if(!$str){
			$params = array('invoice_id'=>$in['invoice_id'],'dbase'=>$this->database_name);
			$str = $this->generate_pdf_oO($params);
		}

		$vat_arr = array();
		$sub_arr = array();
		$tax_str = '';
		$discontoo = 0;
		$disconto_total = 0;
		$lines = $this->db->query("SELECT * FROM tblinvoice_incomming_line WHERE invoice_id='".$in['invoice_id']."' ");
		while ($lines->next()) {
			$discontoo_line = 0;
			if($invoice->f('allow_disconto') == 1){
				$line = $lines->f('total')-$lines->f('total')*$invoice->f('disconto')/100;
				$discontoo += $lines->f('total')*$invoice->f('disconto')/100;
				$discontoo_line = $lines->f('total')*$invoice->f('disconto')/100;
			}else{
				$line = $lines->f('total');
			}
			$total_with_vat = $line + ($line/100*$lines->f('vat_line'));
			$disconto_total +=$discontoo_line + $total_with_vat;
			$vat_arr[$lines->f('vat_line')] += $lines->f('vat_line')*$line/100;
			$sub_arr[$lines->f('vat_line')] += $line;
    	}
    	foreach ($vat_arr as $key => $val) {
    		$line_xml='<cac:TaxSubtotal>
      <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($sub_arr[$key]).'</cbc:TaxableAmount>
      <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:TaxAmount>
      <cbc:Percent>'.$key.'</cbc:Percent>
      <cac:TaxCategory>
        <cbc:ID schemeName="Duty or tax or fee category" schemeID="UN/EDIFACT 5305">S</cbc:ID>
        <cbc:Name>03</cbc:Name>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:TaxCategory>
    </cac:TaxSubtotal>';
    			$tax_str.=$line_xml;
    	}

    	$i=0;
    	$invoice_line_str='';
    	foreach($sub_arr as $key => $val){
    		$i++;
    		$ln_amount_vat = twoDecNumber($val*$key/100);
    		$invoice_line_xml = '<cac:InvoiceLine>
	<cbc:ID>'.$i.'</cbc:ID>
	<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:LineExtensionAmount>
	<cac:TaxTotal>
	  <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount_vat).'</cbc:TaxAmount>
      <cbc:Percent>'.$key.'</cbc:Percent>
	</cac:TaxTotal>
	<cac:Item>
	  <cbc:Description>Invoice Line</cbc:Description>       
	</cac:Item>        
  </cac:InvoiceLine>';
		       $invoice_line_str.=$invoice_line_xml;
    	}
    	$disconto_total = number_format($disconto_total,2);

		$currency = currency::get_currency(1,'code');
		// $json = '{"owner":"'.$invoice->f('seller_bwt_nr').'","owner_id_type":"VAT","destination":"'.$this->formatVAT(ACCOUNT_VAT_NUMBER).'","dest_id_type":"VAT","format":"pdf","docid":"file_'.$invoice->f('invoice_number').'","immediate":"Y","source":"SalesAssist V 2.56","channel":""}';
		$xml ='<?xml version="1.0" encoding="UTF-8"?>
<Invoice xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:ccts="urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2" xmlns:stat="urn:oasis:names:specification:ubl:schema:xsd:DocumentStatusCode-1.0" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:udt="urn:un:unece:uncefact:data:draft:UnqualifiedDataTypesSchemaModule:2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2">
  <cbc:UBLVersionID>2.0</cbc:UBLVersionID>
  <cbc:CustomizationID>1.0</cbc:CustomizationID>
  <cbc:ProfileID>FFF.BE - Logistics 6.0</cbc:ProfileID>
  <cbc:ID>'.htmlspecialchars(stripslashes($invoice->f('invoice_number')),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
  <cbc:CopyIndicator>false</cbc:CopyIndicator>
  <cbc:IssueDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</cbc:IssueDate>
  <cbc:InvoiceTypeCode listID="UN/ECE 1001 Subset" listAgencyID="6">380</cbc:InvoiceTypeCode>
  <cbc:Note> </cbc:Note>
  <cbc:DocumentCurrencyCode listID="ISO 4217 Alpha" listAgencyID="6"
    >'.$currency.'</cbc:DocumentCurrencyCode>
  <cac:AdditionalDocumentReference>
    <cbc:ID>Invoice-'.htmlspecialchars(stripslashes($invoice->f('invoice_number')),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
    <cbc:DocumentType>Drawing</cbc:DocumentType>
    <cbc:Expense_category>'.htmlspecialchars(stripslashes($ExpCategory),ENT_QUOTES | ENT_HTML5).'</cbc:Expense_category>
    <cac:Attachment>
      <cbc:EmbeddedDocumentBinaryObject mimeCode="application/pdf"
        >'.$str.'</cbc:EmbeddedDocumentBinaryObject>
    </cac:Attachment>
  </cac:AdditionalDocumentReference>
   <cac:AccountingSupplierParty>
    <cac:Party>
      <cac:PartyIdentification>
        <cbc:ID schemeAgencyID="'.htmlspecialchars(stripslashes($bName->f('name')),ENT_QUOTES | ENT_HTML5).'" schemeAgencyName="VAT" schemeURI="http://www.FFF.be/KBO">'.$this->formatVAT($bName->f('btw_nr')).'</cbc:ID>
      </cac:PartyIdentification>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars(stripslashes($bName->f('name')),ENT_QUOTES | ENT_HTML5).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.htmlspecialchars(stripslashes($address->f('address')),ENT_QUOTES | ENT_HTML5).'</cbc:StreetName>
        <cbc:CityName>'.htmlspecialchars(stripslashes($address->f('city')),ENT_QUOTES | ENT_HTML5).'</cbc:CityName>
        <cbc:PostalZone>'.htmlspecialchars(stripslashes($address->f('zip')),ENT_QUOTES | ENT_HTML5).'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode>'.get_country_code($address->f('country_id')).'</cbc:IdentificationCode>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID>'.$this->formatVAT($bName->f('btw_nr')).'</cbc:CompanyID>
        <cac:TaxScheme/>
      </cac:PartyTaxScheme>
    </cac:Party>
  </cac:AccountingSupplierParty>
  <cac:AccountingCustomerParty>
  	<cbc:CustomerAssignedAccountID></cbc:CustomerAssignedAccountID>
    <cac:Party>
      <cac:PartyIdentification>
        <cbc:ID schemeAgencyID="'.htmlspecialchars(stripslashes($ACCOUNT_COMPANY),ENT_QUOTES | ENT_HTML5).'" schemeAgencyName="VAT" schemeURI="http://www.FFF.be/KBO">'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:ID>
      </cac:PartyIdentification>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars(stripslashes($ACCOUNT_COMPANY),ENT_QUOTES | ENT_HTML5).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.htmlspecialchars(stripslashes($ACCOUNT_BILLING_ADDRESS),ENT_QUOTES | ENT_HTML5).'</cbc:StreetName>
        <cbc:CityName>'.htmlspecialchars(stripslashes($ACCOUNT_BILLING_CITY),ENT_QUOTES | ENT_HTML5).'</cbc:CityName>
        <cbc:PostalZone>'.htmlspecialchars(stripslashes($ACCOUNT_BILLING_ZIP),ENT_QUOTES | ENT_HTML5).'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode>'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID>'.$this->formatVAT($ACCOUNT_VAT_NUMBER).'</cbc:CompanyID>
        <cac:TaxScheme/>
      </cac:PartyTaxScheme>
       <cac:Contact>
        <cbc:Name></cbc:Name>
        <cbc:Telephone></cbc:Telephone>
        <cbc:Telefax></cbc:Telefax>
        <cbc:ElectronicMail></cbc:ElectronicMail>
      </cac:Contact>
    </cac:Party>
  </cac:AccountingCustomerParty>
  <cac:PaymentMeans>
    <cbc:PaymentMeansCode listURI="http://docs.oasis-open.org/ubl/os-UBL-2.0-update/cl/gc/default/PaymentMeansCode-2.0.gc" listName="Payment Means" listID="UN/ECE 4461">1</cbc:PaymentMeansCode>
    <cbc:PaymentDueDate>'.date('Y-m-d',$invoice->f('due_date')).'</cbc:PaymentDueDate>
    <cbc:InstructionID></cbc:InstructionID>
	<cbc:InstructionNote></cbc:InstructionNote>
    <cac:PayeeFinancialAccount>
      <cbc:ID schemeName="IBAN">'.htmlspecialchars(stripslashes($bName->f('bank_iban')),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
		<cac:FinancialInstitutionBranch>
			<cac:FinancialInstitution>
				<cbc:ID schemeName="BIC">'.htmlspecialchars(stripslashes($bName->f('bank_bic_code')),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
			</cac:FinancialInstitution>
		</cac:FinancialInstitutionBranch>
    </cac:PayeeFinancialAccount>
  </cac:PaymentMeans>
  <cac:PaymentTerms>
	<cbc:SettlementDiscountPercent>'.$invoice->f('disconto').'</cbc:SettlementDiscountPercent>
  </cac:PaymentTerms>
  <cac:TaxTotal>
	<cbc:TaxAmount currencyID="EUR">'.twoDecNumber($invoice->f('total_with_vat')-$invoice->f('total')).'</cbc:TaxAmount>
	'.$tax_str.'
  </cac:TaxTotal>
  <cac:LegalMonetaryTotal>
    <cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:LineExtensionAmount>
    <cbc:TaxExclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($invoice->f('total')).'</cbc:TaxExclusiveAmount>
    <cbc:TaxInclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:TaxInclusiveAmount>
    <cbc:PayableAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:PayableAmount>
  </cac:LegalMonetaryTotal>
  '.$invoice_line_str.'
</Invoice>
';
		$e = str_replace('/', '', $invoice->f('booking_number'));
		if(file_put_contents(__DIR__.'/file_inc_'.$e.'.xml', $xml) === false){
			return false;
		}
		return $e;
	}

	function archiveSubline(&$in){
		$this->db->query("UPDATE codapayments_line SET archived='1' WHERE id='".$in['id']."' ");
		msg::success(gm('Data saved'),'success');
		json_out($in);
	}

	function mark_as_linked(&$in){
		$this->db->query("UPDATE codapayments_line SET status='1' WHERE id='".$in['id']."' ");
		msg::success(gm('Data saved'),'success');
		json_out($in);
	}

	function unlink(&$in){
		$v=new validation($in);
		$v->field("id",'Id',"required:exist[codapayments_line.id]");
		if(!$v->run()){
			json_out($in);
		}
		$invoice_ids=array();
		$linked_invoices = $this->db->query("SELECT payment_id,invoice_id, amount FROM tblinvoice_payments WHERE codapayment_id='".$in['id']."'");
		$total_linked_invoices = 0;
		$amount_left = 0;
		while($linked_invoices->next()) {
			$total_linked_invoices += $linked_invoices->f('amount');
			$this->db->query("DELETE FROM tblinvoice_payments WHERE payment_id='".$linked_invoices->f('payment_id')."'  ");

			$from_proforma_id = $this->db->field("SELECT id FROM tblinvoice WHERE proforma_id = '".$linked_invoices->f('invoice_id')."' ");
		    $has_proforma_id = $this->db->field("SELECT proforma_id FROM tblinvoice WHERE id = '".$linked_invoices->f('invoice_id')."' ");

		    $payments = $this->db->field("SELECT count(payment_id) FROM tblinvoice_payments WHERE invoice_id = '".$linked_invoices->f('invoice_id')."' ");
		  
		    if($payments>=1){
		      $this->db->query("UPDATE tblinvoice SET paid='2' WHERE id='".$linked_invoices->f('invoice_id')."'");
		      if($from_proforma_id){
		       $this->db->query("UPDATE tblinvoice SET paid='2' WHERE id='".$from_proforma_id."'"); 
		      }
		    }else{
		      $this->db->query("UPDATE tblinvoice SET paid='0', status='0' WHERE id='".$linked_invoices->f('invoice_id')."'");
		      if($from_proforma_id){
		        $this->db->query("UPDATE tblinvoice SET paid='0', status='0' WHERE id='".$from_proforma_id."'"); 
		      }
		      if($has_proforma_id){
		        $payments_proforma = $this->db->field("SELECT count(id) FROM tblinvoice WHERE proforma_id = '".$has_proforma_id."' AND paid!=0");
		         if($payments_proforma>=1){
		            $this->db->query("UPDATE tblinvoice SET  paid='2' WHERE id='".$has_proforma_id."'"); 
		         }else{
		            $this->db->query("UPDATE tblinvoice SET  paid='0', status='0' WHERE id='".$has_proforma_id."'"); 
		         }
		        
		      }
		    }
		    $invoice_ids[$linked_invoices->f('invoice_id')]=1;
		}

		$old_balance = $this->db->field("SELECT amount_left FROM codapayments_line WHERE id='".$in['id']."' ");
		$amount_left = $old_balance + $total_linked_invoices;
		$this->db->query("UPDATE codapayments_line SET status='0', amount_left ='".$amount_left."' WHERE id='".$in['id']."' ");

		if(!empty($invoice_ids)){
			foreach($invoice_ids as $inv_id=>$val){
		    	$inv = $this->db->query("SELECT * FROM tblinvoice WHERE id = '".$inv_id."' ");
			    $params = array();
			    $params['use_custom'] = 0;
			    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
			      	$params['logo'] = $inv->f('pdf_logo');
			      	$params['type']=$inv->f('pdf_layout');
			      	$params['logo']=$inv->f('pdf_logo');
			      	$params['template_type'] = $inv->f('pdf_layout');
			    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
			      	$params['custom_type']=$inv->f('pdf_layout');
			      	unset($params['type']);
			      	$params['logo']=$inv->f('pdf_logo');
			      	$params['template_type'] = $inv->f('pdf_layout');
			      	$params['use_custom'] = 1;
			    }else{
			    	$params['type']= $inv->f('type')==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
			    	$params['template_type'] =$inv->f('type')==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
			    }
			    #if we are using a customer pdf template
			    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
			      	$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
			      	unset($params['type']);
			    }
			    $params['id'] = $inv_id;
			    $params['lid'] = $inv->f('email_language');
			    $params['type_pdf'] =  $inv->f('type');
			    $params['save_as'] = 'F';
			    $this->generate_pdf($params);
			}
		}	

		msg::success(gm('Data saved'),'success');
		json_out($in);
	}

}//end class
?>