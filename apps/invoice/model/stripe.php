<?php


class stripe
{
	var $db;
	var $user;
	var $pass;
	var $arUrl;
	var $apUrl;
	var $database_name;

	function __construct(&$in,$dbase='')
	{
		global $database_config;
		if($dbase){
			$database_1 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $dbase,
			);
			$this->db = new sqldb($database_1);
			$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();
			$this->database_name=DATABASE_NAME;
		}	
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->dbu_users =  new sqldb($database_2);

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function checkPay(&$in)
	{ 	
		$invoices = $this->get_stripe_invoices();
		foreach ($invoices as $key => $value) {
			$amount_stripe = $value/100;							
			$invoice_id = $this->db->field("SELECT id FROM tblinvoice WHERE stripe_invoice_id='".$key."' ");
			$in['invoice_id']= $invoice_id;

			if($invoice_id && $amount_stripe>0 ){
					$payment_id = $this->db->field("SELECT payment_id FROM tblinvoice_payments WHERE invoice_id='".$invoice_id."' AND CAST(amount AS CHAR) ='".$amount_stripe."' AND payment_method = '12'");
					if($payment_id){
						continue;
					}else{
						$inv_up=array();
						array_push($inv_up,array('invoice_id'=>$invoice_id,'amount'=>display_number($amount_stripe)));
						$this->marked_payed($inv_up);
					}

			}
		}
		return true;

	}

function get_stripe_invoices(){
    global $config;
    $db = new sqldb();
    ark::loadLibraries(array('stripe/init'));

    if($this->database_name =='04ed5b47_e424_5dd4_ad024bcf2e1d'){
    	$stripe_api_key = $config['stripe_api_key'];
    }else{
    	$app_id=$db->query("SELECT * FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
    	$app_id->next();
        
    	$stripe_api_key = $app_id->f('api');
    }

    //$stripe_api_key = 'sk_test_frrSZiyO3TUXxh7jU00pK89100Ns1shFw4';

    try{
        \Stripe\Stripe::setApiKey($stripe_api_key); 
    }catch(Exception $e){
        msg::error(gm("Incorrect login"),'error');
        if($in['from_cron']){
            insert_error_call($this->database_name,"stripe_check","invoice","","Incorrect login");
        }           
        return true;
    }

    $today_start = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    $str = strtotime('-1 day');
    $yesterday_start = mktime(0, 0, 0, date('m', $str), date('d', $str), date('Y', $str));
    $inv=array();

    try{
        $inv=\Stripe\Charge::all(["created" => array("gte" => $yesterday_start),
                                "status"=> "succeeded",
                                "captured" => true,
                                "limit" => 100]);
    }catch(Exception $e){
        msg::notice('Error',"notice");
    }

    $invoices=array();
  
    if($inv['data']){
    	    foreach($inv['data'] as $key => $value){
		        $amount = $value['amount'] - $value['amount_refunded'];
		        if($value['invoice'] && $amount){     
		            $invoices[$value['invoice']] += $amount;
		        }
		    }
    }

    return $invoices;
}

	function marked_payed($data){
		foreach($data as $key=>$value){
      		$start_payment_date = mktime(23,59,59,date('n'),date('j'),date('y'));
		
      		$total = 0;
    		$tblinvoice=$this->db->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE id='".$value['invoice_id']."'");
    		$tblinvoice->next();
    		$currency = get_commission_type_list($tblinvoice->f('currency_type'));
    		$big_discount = $tblinvoice->f("discount");
    		if($tblinvoice->f('apply_discount') < 2){
      			$big_discount =0;
    		}
    		$lines = $this->db->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$value['invoice_id']."' ");
		    while ($lines->next()) {
		      $line_disc = $lines->f('discount');
		      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
		        $line_disc = 0;
		      }
		      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
		      $line = $line - $line * $big_discount / 100;

		      $total += $line + ($line * ( $lines->f('vat') / 100));

		    }
		    //already payed
		    if($tblinvoice->f('proforma_id')){
		      $filter = " OR invoice_id='".$tblinvoice->f('proforma_id')."' ";
		    }
		    $tot_payed=$this->db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='0' ");
		    $total_payed = $tot_payed;

		    $negative = 1;
		    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
		    if($use_negative==1){
		      $negative = -1;
		    }
    		$this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='1' ");
		    $this->db->move_next();
		    $total_payed += ($negative*$this->db->f('total_payed'));

		    $req_payment_value = $total;
		    if($tblinvoice->f('type')==1){
		      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
		    }
		    $amount_due = round($req_payment_value - $total_payed,2);
		    $general_amount = return_value($value['amount']);

    		$payment_date=date("Y-m-d",$start_payment_date);
    		$this->db->query("UPDATE tblinvoice SET not_paid='0' WHERE id='".$value['invoice_id']."'");

		    //Mark as payed
		    if($general_amount == $amount_due || $general_amount>$amount_due){
		      $this->db->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$value['invoice_id']."'");
		      $p = $this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$value['invoice_id']."' ");
		      if($p){
		        $this->db->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
		      }
		    }
		    else {
		      $this->db->query("UPDATE tblinvoice SET paid='2',status='1' WHERE id='".$value['invoice_id']."'");
		    }
		    $date_format=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DATE_FORMAT' ");

		    $payment_view_date=date($date_format,$start_payment_date);
		    $info=$payment_view_date.'  -  '.place_currency(display_number($general_amount),$currency).'  -  Stripe Cron';

    		$this->db->query("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$value['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$general_amount."',
                          info        = '".$info."', 
                          payment_method = '12' ");
    		$this->db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Payment has been successfully recorded by {endl} Stripe Cron')."', field_name='invoice_id', field_value='".$value['invoice_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
			$this->dbu_users->query("INSERT INTO cron_stripe SET `database_name`='".$this->database_name."',invoice_id='".$value['invoice_id']."'  ");
		}
	}


}