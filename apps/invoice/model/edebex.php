<?php
class edebex{
	var $dbu;

	function __construct() {
		global $database_config;
	    	$this->dbu = new sqldb();
	    	$database_2 = array(
	    		'hostname' => $database_config['mysql']['hostname'],
	    		'username' => $database_config['mysql']['username'],
	    		'password' => $database_config['mysql']['password'],
	    		'database' => $database_config['user_db'],
	    	);
	    	$this->dbu_users =  new sqldb($database_2);
	}

	function offer(&$in){
		global $config;
    	$ch = curl_init();
    	$edebex_user=$this->dbu->field("SELECT api FROM apps WHERE name='Edebex' AND type='main' AND main_app_id='0' ");
    	if($edebex_user==''){
			$headers=array('Content-Type: application/json','X-PARTNER-KEY: akti_sandbox');
   		}else{
    		$headers=array('Content-Type: application/json','X-PARTNER-KEY: akti_sandbox','X-USER-KEY:'.$edebex_user);
   		}
    	if($_SESSION['main_u_id']==0){
		      //$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id='".$_SESSION['u_id']."' ");
    		$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		}else{
		      //$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
			$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['main_u_id']]);
		}
		$ACCOUNT_VAT_NUMBER=trim(ACCOUNT_VAT_NUMBER," ");
		$ACCOUNT_VAT_NUMBER=str_replace(" ","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace(".","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace(",","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("-","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("_","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("/","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("|","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=strtoupper($ACCOUNT_VAT_NUMBER);   		
		$tblinvoice=$this->dbu->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$btw_nr=$this->dbu->field("SELECT btw_nr FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."'");
		$btw_nr=trim($btw_nr," ");
		$btw_nr=str_replace(" ","",$btw_nr);
		$btw_nr=str_replace(".","",$btw_nr);
		$btw_nr=str_replace(",","",$btw_nr);
		$btw_nr=str_replace("-","",$btw_nr);
		$btw_nr=str_replace("_","",$btw_nr);
		$btw_nr=str_replace("/","",$btw_nr);
		$btw_nr=str_replace("|","",$btw_nr);
		$btw_nr=strtoupper($btw_nr);

		$edebex_data=array(
	            'user'             	=> array(
	                                      	'first_name'    => $customer->f('first_name'),
	                                      	'last_name'   	=> $customer->f('last_name'),
	                                      	'email'     	=> $customer->f('email'),
	                                      	'phone'  		=> $customer->f('phone'),
	                                      	'locale'      	=> $_SESSION['l']	                                      
	                                    ),
	            'seller'               	=> array(
	                                      	'country'    	=> get_country_code(ACCOUNT_BILLING_COUNTRY_ID),
	                                      	'vat'   		=> $ACCOUNT_VAT_NUMBER,
	                                      	'name'			=> ACCOUNT_COMPANY                                    
	                                    ), 
	            'invoices'               => array(0=>array(
	            								'debtor'		=> array(
					            									'country'	=> get_country_code($tblinvoice->f('buyer_country_id')),
					            									'vat'		=> $btw_nr,
					            									'name'		=> $tblinvoice->f('buyer_name')
	            								),
	                                      	'amount'    	=> number_format($tblinvoice->f('amount_vat'),2,'.','')*100,
	                                      	'currency'   	=> $this->dbu->field("SELECT code FROM currency WHERE id='".$tblinvoice->f('currency_type')."' "),                                      	
	                                      	'reference'		=> $tblinvoice->f('serial_number'),
	                                      	'issue_date'    => date('Y-m-d',$tblinvoice->f('invoice_date')),
	                                      	'due_date'  	=> date('Y-m-d',$tblinvoice->f('due_date'))                                     
	                                    ))
	      );	  
	    	$edebex_data = json_encode($edebex_data);
          	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
          	curl_setopt($ch, CURLOPT_POST, true);
          	curl_setopt($ch, CURLOPT_POSTFIELDS, $edebex_data);
          	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          	curl_setopt($ch, CURLOPT_URL, $config['edebex_base_url'].'/offers.json');

    		$put = curl_exec($ch);
    		$info = curl_getinfo($ch);
    		/*console::log($put);
    		console::log('space');
    		console::log($info);*/
    		$edebex_data=json_decode($put,true);
    		if(($edebex_data['code'] && $edebex_data['code']=='500') || $info['http_code']=='301'){
    			$result=array('error'=>array('error'=>gm("Incorrect customer/client data")),'notice'=>false,'success'=>false);
    		}else if($edebex_data['invoices'][0]['error']){
    			$error_msg='';
    			foreach($edebex_data['invoices'][0]['error'] as $key=>$value){
    				$error_msg.=$key.' '.$value.';';		
    							
    			}
    			$result=array('error'=>array('error'=>rtrim($error_msg,';')),'notice'=>false,'success'=>false);
    		}else{
    			$result=array();
    			$result['amount']=display_number(number_format($tblinvoice->f('amount_vat'),2,'.',''));
    			$result['amount_offer']=display_number(number_format($edebex_data['invoices'][0]['offer']['sale_price']/100,2,'.',''));
    			$result['do_next']='invoice--edebex-sell';
    			$result['invoice_id']=$in['invoice_id'];
    			$result['currency']=$this->dbu->field("SELECT code FROM currency WHERE id='".$tblinvoice->f('currency_type')."' "); 			
    		} 
    		json_out($result);		
	}

	function sell(&$in){
		global $config;
    	$ch = curl_init();
    	$edebex_user=$this->dbu->field("SELECT api FROM apps WHERE name='Edebex' AND type='main' AND main_app_id='0' ");
    	if($edebex_user==''){
			$headers=array('Content-Type: application/json','X-PARTNER-KEY: akti_sandbox');
    	}else{
    		$headers=array('Content-Type: application/json','X-PARTNER-KEY: akti_sandbox','X-USER-KEY:'.$edebex_user);
    	}
    	if($_SESSION['main_u_id']==0){
		      //$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id='".$_SESSION['u_id']."' ");
    		$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		}else{
		      //$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
			$customer=$this->dbu_users->query("SELECT first_name,last_name,email,phone FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['main_u_id']]);
		}
		$ACCOUNT_VAT_NUMBER=trim(ACCOUNT_VAT_NUMBER," ");
		$ACCOUNT_VAT_NUMBER=str_replace(" ","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace(".","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace(",","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("-","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("_","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("/","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=str_replace("|","",$ACCOUNT_VAT_NUMBER);
		$ACCOUNT_VAT_NUMBER=strtoupper($ACCOUNT_VAT_NUMBER);   		
		$tblinvoice=$this->dbu->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$btw_nr=$this->dbu->field("SELECT btw_nr FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."'");
		$btw_nr=trim($btw_nr," ");
		$btw_nr=str_replace(" ","",$btw_nr);
		$btw_nr=str_replace(".","",$btw_nr);
		$btw_nr=str_replace(",","",$btw_nr);
		$btw_nr=str_replace("-","",$btw_nr);
		$btw_nr=str_replace("_","",$btw_nr);
		$btw_nr=str_replace("/","",$btw_nr);
		$btw_nr=str_replace("|","",$btw_nr);
		$btw_nr=strtoupper($btw_nr);

		ark::loadLibraries(array('aws'));
	    $aws = new awsWrap(DATABASE_NAME);
	    $in['attach_file_name'] = 'invoice_'.$tblinvoice->f('serial_number').'.pdf';
	    $file = $aws->getItem('invoice/invoice_'.$in['invoice_id'].'.pdf',$in['attach_file_name']);
	    if($file !== true){
	        $in['attach_file_name'] = null;
	        msg::error(gm("Generate PDF first"),"error");
	        json_out($in);
	    }
	    $handle = fopen($in['attach_file_name'], "r");   
      	$contents = fread($handle, filesize($in['attach_file_name'])); 
      	fclose($handle);                               
      	$encodedDoc = base64_encode($contents);
      	unlink($in['attach_file_name']);

		$edebex_data=array(
	            'user'             	=> array(
	                                      	'first_name'    => $customer->f('first_name'),
	                                      	'last_name'   	=> $customer->f('last_name'),
	                                      	'email'     	=> $customer->f('email'),
	                                      	'phone'  		=> $customer->f('phone'),
	                                      	'locale'      	=> $_SESSION['l']	                                      
	                                    ),
	            'seller'               	=> array(
	                                      	'country'    	=> get_country_code(ACCOUNT_BILLING_COUNTRY_ID),
	                                      	'vat'   		=> $ACCOUNT_VAT_NUMBER,
	                                      	'name'			=> ACCOUNT_COMPANY                                    
	                                    ), 
	            'invoice'               => array(
	            								'debtor'		=> array(
				            									'country'	=> get_country_code($tblinvoice->f('buyer_country_id')),
				            									'vat'		=> $btw_nr,
				            									'name'		=> $tblinvoice->f('buyer_name')
	            								),
	                                      	'amount'    	=> number_format($tblinvoice->f('amount_vat'),2,'.','')*100,
	                                      	'currency'   	=> $this->dbu->field("SELECT code FROM currency WHERE id='".$tblinvoice->f('currency_type')."' "),
	                                      	'reference'		=> $tblinvoice->f('serial_number'),
	                                      	'issue_date'    => date('Y-m-d',$tblinvoice->f('invoice_date')),
	                                      	'due_date'  	=> date('Y-m-d',$tblinvoice->f('due_date')),
	                                      	'mimetype'      => 'image/jpeg',
	                                      	'file_content'	=> $encodedDoc	                                      
	                                    )
	      );	  
	    	$edebex_data = json_encode($edebex_data);
          	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
          	curl_setopt($ch, CURLOPT_POST, true);
          	curl_setopt($ch, CURLOPT_POSTFIELDS, $edebex_data);
          	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          	curl_setopt($ch, CURLOPT_URL, $config['edebex_base_url'].'/invoices/sell.json');

    		$put = curl_exec($ch);
    		$info = curl_getinfo($ch);
    		/*console::log($put);
    		console::log('space');
    		console::log($info);*/
    		if($info['http_code']>300 || $info['http_code']==0){ 						 
				msg::error("Error in data sent","error");
				json_out($in);
    		}else{
    			$edebex_data=json_decode($put,true);
    			$result=array();
    			if($edebex_data['redirect']){
    				$result['redirect']=$edebex_data['redirect'];
    			}else if($edebex_data['invoice']['action_required'] && $edebex_data['invoice']['action_required']['redirect']){
    				$result['redirect']=$edebex_data['invoice']['action_required']['redirect'];
    			}		
    			if($edebex_data['invoices'][0]['id']){
    				$this->dbu->query("UPDATE tblinvoice SET edebex_id='".$edebex_data['invoices'][0]['id']."' WHERE id='".$in['invoice_id']."' ");
    			}else if($edebex_data['invoice']['id']){
    				$this->dbu->query("UPDATE tblinvoice SET edebex_id='".$edebex_data['invoice']['id']."' WHERE id='".$in['invoice_id']."' ");
    			}
    			json_out($result);
    		} 	
	}

	function status(&$in){
		global $config;
		$result=array();
    	$ch = curl_init();
    	$edebex_user=$this->dbu->field("SELECT api FROM apps WHERE name='Edebex' AND type='main' AND main_app_id='0' ");
    	$edebex_id=$this->dbu->field("SELECT edebex_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
    
    	$headers=array('Content-Type: application/json','X-PARTNER-KEY: akti_sandbox','X-USER-KEY:'.$edebex_user);

    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $config['edebex_base_url'].'/invoices/'.$edebex_id.'/status.json');

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	/*console::log($put);
    	console::log('space');
    	console::log($info);*/

    	if($info['http_code']>300 || $info['http_code']==0){ 						 
		msg::error("Error","error");
		json_out($in);
    	}else{
    		$data=json_decode($put);
    		$result['status']=$data->status;
    	}

    	json_out($result);
	}
}
?>