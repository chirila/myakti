<?php

require_once('btb.php');
class yuki
{
	var $db;
	var $user;
	var $pass;
	var $arUrl;
	var $apUrl;
	var $database_name;

	function yuki(&$in,$dbase='')
	{
		global $database_config;
		if($dbase){
			$database_1 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $dbase,
			);
			$this->db = new sqldb($database_1);
			$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();
			$this->database_name=DATABASE_NAME;
		}	
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->dbu_users =  new sqldb($database_2);

		$app_id=$this->db->query("SELECT * FROM apps WHERE name='Yuki' AND app_type='accountancy'");
		$app_id->next();
		$val = explode(';', $app_id->f('api'));
		$this->key = $val[0];
		$this->admin_id = $val[1];
		$this->drop_folder = $val[2];
		$this->sess_id = '';
		$this->webservice_url = 'https://api.yukiworks.nl/ws/Sales.asmx?WSDL';
		$this->webservice_url2 ='https://api.yukiworks.nl/ws/Accounting.asmx?WSDL';
		$this->webservice_url3 ='https://api.yukiworks.nl/ws/AccountingInfo.asmx?WSDL';

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function checkPay(&$in)
	{ 
		
		$inv = array();
		$in['inv_up']=array();
		$yuki_filter="";
		if($in['from_cron']){
			$yuki_counter=$this->db->field("SELECT value FROM settings WHERE `constant_name`='YUKI_COUNTER_CHECK' ");
			$last_yuki_check=$this->db->field("SELECT value FROM settings WHERE `constant_name`='YUKI_TIME_CHECK' ");
			if(is_null($last_yuki_check)){
				$this->db->query("INSERT INTO settings SET `constant_name`='YUKI_TIME_CHECK',value='".time()."' ");
				$this->db->query("INSERT INTO settings SET `constant_name`='YUKI_COUNTER_CHECK',value='0' ");
				$yuki_filter=" LIMIT 100 ";
			}else{
				if($last_yuki_check+82800>time()){
					if($yuki_counter>=100){
						msg::error(gm("Payments check limit per day achieved"),'error');
						if($in['from_cron']){
							insert_error_call($this->database_name,"yuki_check","invoice","","Payments check limit per day achieved");
						}			
						return true;
					}else{
						$val_filter=100-$yuki_counter;
						$yuki_filter=" LIMIT ".$val_filter." ";
					}
				}else{
					$this->db->query("UPDATE settings SET `value`='".time()."' WHERE `constant_name`='YUKI_TIME_CHECK' ");
					$this->db->query("UPDATE settings SET `value`='0' WHERE `constant_name`='YUKI_COUNTER_CHECK' ");
					$yuki_filter=" LIMIT 100 ";
				}
			}
		}else{
			$yuki_filter=" LIMIT 5000 ";
		}
		$invs = $this->db->query("SELECT id,serial_number FROM tblinvoice WHERE f_archived='0' AND tblinvoice.not_paid!='1' AND tblinvoice.paid != '1' AND tblinvoice.sent = '1' AND (type='0' OR type='3') ORDER BY due_date ASC ".$yuki_filter." ");
		while ($invs->next()) {
			$inv[$invs->f('id')] = $invs->f('serial_number');
		}
		$yuki_daily_limit_exceeded = false;
		if(!empty($inv)){
			if(!$in['from_cron']){
				include_once(__DIR__.'/invoice.php');
				$invModel = new invoice();
			}		
			try
			{
				$soap = new SoapClient($this->webservice_url2,array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY));
				$res = $soap->Authenticate(array('accessKey' => $this->key));
	
				if (!isset($res->AuthenticateResult)){
					msg::error(gm("Incorrect login"),'error');
					if($in['from_cron']){
						insert_error_call($this->database_name,"yuki_check","invoice","","Incorrect login");
					}			
					return true;
				}
				$this->sess_id = $res->AuthenticateResult;
				$times_run=0;
				foreach ($inv as $key => $value) {
					if(!$in['from_cron'] && $yuki_daily_limit_exceeded){
						break;  
					}
					$res = $soap->CheckOutstandingItem(array('sessionID' => $this->sess_id, 'Reference' => $value));
					if($res->CheckOutstandingItemResult->any){
						$x = simplexml_load_string($res->CheckOutstandingItemResult->any);
						//console::log("Invoice ".$value." - Open Amount: ".$x->Item->OpenAmount." Original Amount: ".$x->Item->OriginalAmount."\n");

							foreach($x->Item as $key2 =>$value2){
								$z = (array)$value2;
								$y = (string)$value2->Type[0]["ID"];
								//echo($z['Type']);
								//if($z['Type']=='Verkoopfactuur' || $z['Type']=='Facture de vente'){
								if($y!='2'){
									//"Reference by Invoice Number might return several items", we want only the 'Verkoopfactuur' type
									//echo " Open Amount: ".$z['OpenAmount']." Original Amount: ".$z['OriginalAmount']."\n";
									console::log("Invoice ".$value." - Open Amount: ".$z['OpenAmount']." Original Amount: ".$z['OriginalAmount']."\n");
									if((float)$z['OpenAmount']!=(float)$z['OriginalAmount']){
										$payed_total = (float)$z['OpenAmount'] == 0 ? true : false; 

										### pay ###
										$amount = (float)$z['OriginalAmount']-(float)$z['OpenAmount'];
										$amount = round($amount,2);

										$old_amount = $this->db->field("SELECT ROUND(sum(amount),2) FROM tblinvoice_payments WHERE invoice_id='".$key."' ");
										if($old_amount && $old_amount!=$amount){
											if($old_amount > $amount){
												continue;
											}else{
												$amount = $amount - $old_amount;
											}
										}elseif($amount == $old_amount){
											continue;
										}
										if(!$in['from_cron']){
											if($amount>0){
												if($payed_total){
													$in['invoice_id'] = $key;
													$amount = $this->get_payment($in);
												}
												$params = array('invoice_id'=>$key,'amount'=>display_number($amount),'payment_date'=>date('Y-m-d',time()),'method_pay'=>2);
												//console::log($params);
												array_push($in['inv_up'],array('id'=>$value,'amount'=>display_number($amount)));
												$invModel->pay($params);
												### pay ###
											}
										}else{
											//$cron_inserted=$this->dbu_users->field("SELECT id FROM cron_yuki WHERE `database_name`='".$this->database_name."' AND invoice_id='".$key."'  ");
											//if(!$cron_inserted && $amount>0){
											if($amount>0){
												if($payed_total){
													$in['invoice_id'] = $key;
													$amount = $this->get_payment($in);
												}
												$inv_up=array();
												array_push($inv_up,array('invoice_id'=>$key,'amount'=>display_number($amount)));
												//$this->marked_payed($in);
												$this->marked_payed($inv_up);
											}
										}
									}






								}

							}
						/*if((float)$x->Item->OpenAmount!=(float)$x->Item->OriginalAmount){
							$payed_total = (float)$x->Item->OpenAmount == 0 ? true : false; 

							### pay ###
							$amount = (float)$x->Item->OriginalAmount-(float)$x->Item->OpenAmount;
							$amount = round($amount,2);

							$old_amount = $this->db->field("SELECT ROUND(sum(amount),2) FROM tblinvoice_payments WHERE invoice_id='".$key."' ");
							if($old_amount && $old_amount!=$amount){
								if($old_amount > $amount){
									continue;
								}else{
									$amount = $amount - $old_amount;
								}
							}elseif($amount == $old_amount){
								continue;
							}
							if(!$in['from_cron']){
								if($amount>0){
									if($payed_total){
										$in['invoice_id'] = $key;
										$amount = $this->get_payment($in);
									}
									$params = array('invoice_id'=>$key,'amount'=>display_number($amount),'payment_date'=>date('Y-m-d',time()),'method_pay'=>2);
									//console::log($params);
									array_push($in['inv_up'],array('id'=>$value,'amount'=>display_number($amount)));
									$invModel->pay($params);
									### pay ###
								}
							}else{
								//$cron_inserted=$this->dbu_users->field("SELECT id FROM cron_yuki WHERE `database_name`='".$this->database_name."' AND invoice_id='".$key."'  ");
								//if(!$cron_inserted && $amount>0){
								if($amount>0){
									if($payed_total){
										$in['invoice_id'] = $key;
										$amount = $this->get_payment($in);
									}
									$inv_up=array();
									array_push($inv_up,array('invoice_id'=>$key,'amount'=>display_number($amount)));
									//$this->marked_payed($in);
									$this->marked_payed($inv_up);
								}
							}
						}*/
					}
					$times_run++;
				}
				if($in['from_cron']){
					$this->db->query("UPDATE settings SET `value`='".$times_run."' WHERE `constant_name`='YUKI_COUNTER_CHECK' ");
				}	
				return true;
			}
			catch(SoapFault $fault)
			{
				if($in['from_cron']){
					insert_error_call($this->database_name,"yuki_check","invoice","",addslashes($fault->faultstring));
				}else{
					msg::error ( $fault->faultstring, 'error');
	
					if($fault->faultstring =='Daily limit exceeded'){
						$yuki_daily_limit_exceeded = true;
					}
					
					return false;
				}		
			}
		}

		return true;
	}

	function get_payment(&$in){
		$tblinvoice=$this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$subtotal_vat = 0;
		$total_n = 0;
		$show_disc = false;
		$negative = 1;
	    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
	    if($use_negative==1){
	      $negative = -1;
	    }

	    $big_discount = $tblinvoice->f("discount");
	    if($tblinvoice->f('apply_discount') < 2){
	      $big_discount =0;
	    }
		$lines = $this->db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
		
		while ($lines->next()) {
			$line_discount = $lines->f('discount');
			if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
				$line_discount = 0;
			}
			$amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
			$amount_line_disc = $amount_line*$big_discount/100;
			$discount_value += $amount_line_disc;
			$subtotal_vat +=  ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
			$total_n += $amount_line - $amount_line_disc;
		}

		if($tblinvoice->f('discount') != 0 || $show_disc === true){
			$o['show_disc']=$show_disc;
		}

		if($tblinvoice->f('quote_id') && $tblinvoice->f('downpayment_drawn')){
			$total = $total_n+$subtotal_vat-$tblinvoice->f('downpayment_value');
		}else{
			$total = $total_n+$subtotal_vat;
		}

		//already payed
		$proforma_id=$this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
	    $payments_filter=" invoice_id='".$in['invoice_id']."'";
	    if($proforma_id){
	      $payments_filter.=" OR invoice_id='".$proforma_id."'";
	    }

		$already_payed1 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter." AND credit_payment='0' ");
		$already_payed1->move_next();

		$already_payed2 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter." AND credit_payment='1' ");
		$already_payed2->move_next();


		$total_payed = $already_payed1->f('total_payed')+($negative*$already_payed2->f('total_payed'));

		$req_payment_value=$total* $tblinvoice->f('req_payment')/100;
		if($tblinvoice->f('req_payment') == 100){
			$amount_due = round($total - $total_payed,2);
		}else{
			$amount_due = round($req_payment_value - $total_payed ,2);
		}
	
		return $amount_due;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function exportYuki(&$in)
	{
		$in['actiune'] = gm('Failed');
	
		/*if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '188.26.120.86'){
						    $in['invoice_id']='1068';
				}*/
			/*if(!DATABASE_NAME){
				define(DATABASE_NAME,$this->db->field("SELECT DATABASE()"));
			}*/
			$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
			$in['serial_number'] = $invoice->f('serial_number');
			$currency = currency::get_currency($invoice->f('currency_type'),'code');
			$type=$invoice->f('type');
			$contact = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."' ");
			$our_ref = '';
			$btw_nr = $invoice->f('seller_bwt_nr');
			$digits_nr=$this->db->field("SELECT value FROM settings WHERE constant_name='ARTICLE_PRICE_COMMA_DIGITS' ");
			/*if($invoice->f('buyer_id')){
				$our_ref = $this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");
				if($invoice->f('seller_bwt_nr')){
					$btw_nr=$invoice->f('seller_bwt_nr');
				}else{
					$btw_nr = $this->db->field("SELECT btw_nr FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");
				}	
			}*/
			$cost_centre='';
			/*if(DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'){			
				$cost_centre=$this->db->field("SELECT cost_centre FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
			}*/
			$use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
			$invoice_sum = $this->db->field("SELECT SUM( amount - amount * discount /100 ) FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");

			$get_invoice_rows = $this->db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");
			
			$xml_lines = '';
			$i = 0;
			$total_amount = 0;
			$vat_percent = array();
			$sub_t = array();
			$sub_disc = array();
			$vat_percent_val = 0;
			$discount = $invoice->f('discount');
			if($invoice->f('apply_discount')<2){
				$discount=0;
			}
			while ($get_invoice_rows->next()){
				$line_d = $get_invoice_rows->f('discount');
				if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
					$line_d = 0;
				}
				$amount = $get_invoice_rows->f('amount') - $get_invoice_rows->f('amount') * $line_d /100;

				$total_amount += $get_invoice_rows->f('amount');
				$i++;

				$amount_d = $amount * $discount/100;
				// console::log($amount_d);
				if($invoice->f('apply_discount') < 2){
					$amount_d = 0;
				}
				// console::log($amount , $amount_d);
				$vat_percent_val = round(($amount - $amount_d) * $get_invoice_rows->f('vat')/100,2);

				$vat_percent[$get_invoice_rows->f('vat')] += $vat_percent_val;
				$sub_t[$get_invoice_rows->f('vat')] += $amount;
				$sub_disc[$get_invoice_rows->f('vat')] += $amount_d;
				if($invoice->f('vat_regime_id') < 2){
					/*$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats INNER JOIN vats ON yukivats.vat_id = vats.vat_id WHERE vats.value='".$get_invoice_rows->f('vat')."' ");
					if(!$vat_type){
							$vat_type = 0;
					}
					if($vat_type==100){
						$vat_type=0;
					}*/
					$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats INNER JOIN vats ON yukivats.vat_id = vats.vat_id WHERE vats.value='".$get_invoice_rows->f('vat')."' ");
					if(!$vat_type){
						$vat_old=$this->db->field("SELECT vat_new.id FROM vat_new 
							INNER JOIN vats ON vat_new.vat_id=vats.vat_id
							WHERE vats.value='".$get_invoice_rows->f('vat')."' AND vat_new.regime_type='1' ");
						if($vat_old){
							$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='".$vat_old."' ");
							if(!$vat_type){
								$vat_type=0;
							}
						}else{
							$vat_type=0;
						}
					}
					if($vat_type==100){
						$vat_type=0;
					}
				}else if($invoice->f('vat_regime_id') < 5){
					$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='r".$invoice->f('vat_regime_id')."' ");
					if(!$vat_type){
						$new_reg=$this->db->field("SELECT id FROM vat_new WHERE vat_regime_id='r.".$invoice->f('vat_regime_id')."' ");
						if($new_reg){
							$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='".$new_reg."' ");
							if(!$vat_type){
								$vat_type = 0;
							}
						}else{
							$vat_type = 0;
						}		
					}
				}else{
					/*$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='r".$invoice->f('vat_regime_id')."' ");
					if(!$vat_type){
							$vat_type = 0;
					}*/
					$regime_type=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
					if($regime_type=='1'){
						$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats INNER JOIN vats ON yukivats.vat_id = vats.vat_id WHERE vats.value='".$get_invoice_rows->f('vat')."' ");
						if(!$vat_type){
							$vat_old=$this->db->field("SELECT vat_new.id FROM vat_new 
								INNER JOIN vats ON vat_new.vat_id=vats.vat_id
								WHERE vats.value='".$get_invoice_rows->f('vat')."' AND vat_new.regime_type='1' ");
							if($vat_old){
								$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='".$vat_old."' ");
								if(!$vat_type){
									$vat_type=0;
								}
							}else{
								$vat_type=0;
							}
						}
						if($vat_type==100){
							$vat_type=0;
						}
					}else{
						$yuki_vat_reg=$this->db->field("SELECT yuki_id FROM yukivats WHERE yukivats.vat_id='".$invoice->f('vat_regime_id')."'  ");

						if($yuki_vat_reg){
							$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='".$invoice->f('vat_regime_id')."' ");
							if(!$vat_type){
								$vat_type = 0;
							}
						}else{
							$old_vat_reg=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
							if($old_vat_reg){
								$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='r".$old_vat_reg."' ");
								if(!$vat_type){
									$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='".$invoice->f('vat_regime_id')."' ");
									if(!$vat_type){
										$vat_type=0;
									}
								}
							}
						}
						

						/*$old_vat_reg=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
						if($old_vat_reg){
							$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='r".$old_vat_reg."' ");
							if(!$vat_type){
								$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='".$invoice->f('vat_regime_id')."' ");
								if(!$vat_type){
									$vat_type=0;
								}
							}
						}else{
							$vat_type = $this->db->field("SELECT yukivats.value FROM yukivats WHERE yukivats.vat_id='".$invoice->f('vat_regime_id')."' ");
							if(!$vat_type){
								$vat_type = 0;
							}
						}*/
					}			
				}
				$productDesc = 'free article';
				if($get_invoice_rows->f('article_id')) {
					$productDesc=htmlspecialchars($this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' "));
				}
				$productRef = '';
				if($get_invoice_rows->f('article_id')) {
					$productRef = htmlspecialchars($get_invoice_rows->f('item_code'));
				}
				// $ln_amount = twoDecNumber($get_invoice_rows->f('amount'));
				$ln_amount=twoDecNumber($amount-$amount_d);
				$ln_amount_vat = twoDecNumber($ln_amount*$get_invoice_rows->f('vat')/100);
				$unit_price = twoDecNumber(($get_invoice_rows->f('price')- $get_invoice_rows->f('price') * $line_d /100)-($get_invoice_rows->f('price')- $get_invoice_rows->f('price') * $line_d /100)*$discount/100);
				$quantity = twoDecNumber($get_invoice_rows->f('quantity'));
				
				if($type==2 || $type==4){
					if($quantity>0){
						if($invoice_sum>0){
							$ln_amount = -$ln_amount;
							$ln_amount_vat = -$ln_amount_vat;
							$unit_price = -$unit_price;
						}
					}
					/*//if($ln_amount>0){
						$ln_amount = -$ln_amount;
					//}
					//if($ln_amount_vat>0){
						$ln_amount_vat = -$ln_amount_vat;
					//}
					//if($unit_price>0){
						$unit_price = -$unit_price;
					//}
*/
				}
				$gl_account='';
				if($get_invoice_rows->f('article_id')){
					$line_gl=$this->db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' ");
					if($line_gl){
						$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$line_gl."' ");
					}
				}
				if((!$gl_account || $gl_account=='') && !$get_invoice_rows->f('content')){
					$app_gl=$this->db->field("SELECT value FROM settings WHERE constant_name='YUKI_LEDGER_ID' ");
					if($app_gl){
						$gl_account=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$app_gl."' ");
					}
				}

				$line_xml = '
	<InvoiceLine>
         <Description>'.htmlspecialchars($get_invoice_rows->f('name')).'</Description>
         <Remarks/>
         <ProductQuantity>'.$quantity.'</ProductQuantity>
  		 <LineAmount>'.$ln_amount.'</LineAmount>
         <LineVATAmount>'.$ln_amount_vat.'</LineVATAmount>      
         <Product>
         	<Description>'.$productDesc.'</Description>
         	<Reference>'.$productRef.'</Reference>
         	<SalesPrice>'.$unit_price.'</SalesPrice>
         	<VATPercentage>'.$get_invoice_rows->f('vat').'</VATPercentage>
         	<VATIncluded>false</VATIncluded>
         	<VATType>'.$vat_type.'</VATType>
         	<GLAccountCode>'.$gl_account.'</GLAccountCode>
         </Product>
       </InvoiceLine>';
	       		$xml_lines.=$line_xml;
			}

			if(!$xml_lines){
				//add an empty line
				$line_xml = '
	<InvoiceLine>
         <Description>999999999</Description>
         <Remarks/>
         <ProductQuantity>1</ProductQuantity>
  		 <LineAmount>0</LineAmount>
         <LineVATAmount>0</LineVATAmount>      
         <Product>
         	<Description>999999999</Description>
         	<SalesPrice>0</SalesPrice>
         	<VATPercentage>0</VATPercentage>
         	<VATIncluded>false</VATIncluded>
         	<VATType>0</VATType>
         	<GLAccountCode></GLAccountCode>
         </Product>
       </InvoiceLine>';
	       		$xml_lines.=$line_xml;
			}

			$all_payments=$this->db->query("SELECT payment_method FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' ")->getAll();
			if(empty($all_payments) || count($all_payments)>1){
			    $PaymentMethod='Unspecified';
			}else{
			    $PaymentMethod=get_invoice_payment_method_en(invoice_payment_method_dd_en(),$all_payments[0]['payment_method']);
			}

 			$xml = '<SalesInvoices>
 			<SalesInvoice>
 				<Reference>'.$invoice->f('serial_number').'</Reference>
 				<Subject>'.htmlspecialchars($invoice->f('buyer_name')).' '.htmlspecialchars($invoice->f('our_ref')).'</Subject>
 				<PaymentMethod>'.$PaymentMethod.'</PaymentMethod>
 				<PaymentID>'.$invoice->f('ogm').'</PaymentID>
 				<Process>true</Process>
     			<EmailToCustomer>false</EmailToCustomer>
     			<Layout/>
				<Date>'.date('Y-m-d',$invoice->f('invoice_date')).'</Date>';
				if($type!=2 && $type!=4){
					$xml .='<DueDate>'.date('Y-m-d',$invoice->f('due_date')).'</DueDate>';
				}			
				$xml .='<PriceList/>
				<Currency/>
				<ProjectID/>
				<ProjectCode>'.($invoice->f('yuki_project_id') ? htmlspecialchars(get_yuki_project_code($invoice->f('yuki_project_id'))) : '').'</ProjectCode>
				<Remarks>'.htmlspecialchars($invoice->f('your_ref')).' '.htmlspecialchars(strip_tags($invoice->f('notes2'))).'</Remarks>
				<Contact>
					<ContactCode>'.$our_ref.'</ContactCode>
       				<FullName>'.htmlspecialchars($invoice->f('buyer_name')).'</FullName>
       				<CountryCode>'.get_country_code($invoice->f('buyer_country_id')).'</CountryCode>
       				<City>'.$invoice->f('buyer_city').'</City>
 				    <Zipcode>'.$invoice->f('buyer_zip').'</Zipcode>
       				<AddressLine_1>'.str_replace('&','&amp;',$invoice->f('buyer_address')).'</AddressLine_1>
       				<VATNumber>'.$btw_nr.'</VATNumber>
			     </Contact>
			     <InvoiceLines>'.$xml_lines.'
			     </InvoiceLines>
 			</SalesInvoice>
 			</SalesInvoices>';

 			/*<ContactType>'.($invoice->f('buyer_id') ? 'Company' : 'Contact' ).'</ContactType>*/
 			
 		$app = $this->db->query("SELECT * FROM apps WHERE name='Yuki' AND app_type='accountancy'");
		$dropbox_data=$this->db->query("SELECT app_id,active FROM apps WHERE name='Dropbox' AND type='main'");
		$apps = explode(';',$app->f("api"));
 		if($app->f('active')=='2'){
 			$target_path=count($apps)>3 ? $apps[3] : "";
 				/*if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '188.26.120.86'){
						    file_put_contents(__DIR__.'/../../../file.xml', $xml); exit();
				}*/
 			
          	$d=new drop($target_path,null,null,false,$this->database_name,0,null,null,null,true);
          	$someth=$d->upload($invoice->f('serial_number').".xml",__DIR__.'/../../../file.xml',null);
      		if(!is_null($someth)){
      			$in['actiune'] = gm('Exported');
	      		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."' ,yuki=1,type=0 ");
	      		if(array_key_exists('invoice_to_export', $_SESSION) === true){
	      			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
	      		}
				if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }

				if($in['from_acc']){
			 		$msg = '{l}Invoice was exported automatically to{endl} '.'Yuki ';
			 	}else{
			 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Yuki ';
			 	}
		 		insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

				if($_SESSION['main_u_id']=='0'){
					//$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
					$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);         
				}else{
					$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
				}
				if($is_accountant){
					$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
				    if($accountant_settings=='1'){
				    	$acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
				    	if($acc_block_id=='1'){
				    		$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
				    	}
				    }else{
				    	$block_id=$this->dbu_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
						if($block_id==1){
						     $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
						}
				    }		
				}
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);	
				}
      		}else{
      			$in['actiune'] = gm('Fail');
      			msg::error(gm('Error uploading file'),'error');
				insert_error_call($this->database_name,"yuki","invoice",$in['invoice_id'],"Error uploading file");
				if($in['from_acc']){
					return true;
				}else{
					json_out($in);
				}
      		}
 		}else{
 			try
			{
	 			$soap = new SoapClient($this->webservice_url,array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY));
				$res = $soap->Authenticate(array('accessKey' => $this->key));
				if (!isset($res->AuthenticateResult)){
					msg::error(gm("Incorrect login"),"error");
					insert_error_call($this->database_name,"yuki","invoice",$in['invoice_id'],"Incorrect login");
					if($in['from_acc']){
						return true;
					}else{
						json_out($in);	
					}
				}
				$this->sess_id = $res->AuthenticateResult;
				/*if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '188.26.120.86'){
					file_put_contents(__DIR__.'/../../../file.xml', $xml); exit();
				}*/
				$xmlvar = new SoapVar('<ns1:xmlDoc>'.$xml.'</ns1:xmlDoc>', XSD_ANYXML);
				// $res = $soap->Language(array('sessionId' => $this->sess_id));
				$res = $soap->ProcessSalesInvoices(array('sessionId' => $this->sess_id, 'administrationId' => $this->admin_id, 'xmlDoc' => $xmlvar));
				if($res->ProcessSalesInvoicesResult->any){
					$x = simplexml_load_string($res->ProcessSalesInvoicesResult->any);
					if($x->Invoice->Succeeded=='true'){
						#all is good
						$in['actiune'] = gm('Exported');
						$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."' ,yuki=1,type=0 ");
						if(array_key_exists('invoice_to_export', $_SESSION) === true){
							$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
						}
						if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
				          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
				        }
						if($in['from_acc']){
					 		$msg = '{l}Invoice was exported automatically to{endl} '.'Yuki ';
					 	}else{
					 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Yuki ';
					 	}
		 				insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

						if($_SESSION['main_u_id']=='0'){
					        //$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
					        $is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);         
					    }else{
					          $is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
					    }
					    if($is_accountant){
					    	$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
							if($accountant_settings=='1'){
							    $acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
							    if($acc_block_id=='1'){
							    	$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
							    }
							}else{
							    $block_id=$this->dbu_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
						        if($block_id==1){
						            $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
						        }
							}          
					    }
					}else{
						msg::error(' - '.$x->Invoice->Message->__toString(),"error");
						insert_error_call($this->database_name,"yuki","invoice",$in['invoice_id'],$x->Invoice->Message->__toString());
					}
				}
				// msg::$success = $res;
			}
			catch(SoapFault $fault)
			{
				msg::error($fault->faultstring,"error");
				insert_error_call($this->database_name,"yuki","invoice",$in['invoice_id'],$fault->faultstring);
			}
 		}

		if($in['from_acc']){
			return true;
		}else{
			json_out($in);	
		}
	}

	/**
	 * Mark as exported
	 *
	 * @return boolean
	 * @author Mp
	 **/
	function markExport(&$in)
	{
		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', type='".$in['type']."', yuki=1 ");
		return true;
	}
	/**
	 * Mark as exported
	 *
	 * @return boolean
	 * @author Mp
	 **/
	function unmarkExport(&$in)
	{
		$this->db->query("DELETE FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki=1 AND type='".$in['type']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function exportIncYuki(&$in)
	{
		//$btb = new btb();
		$btb = new btb($in,$this->database_name);
		$id = $btb->createIncFiles($in);
		$prep = 'file_inc_';
		$array = array();
		// $array['file[0]'] = __DIR__.'/file_'.$id.'.json';//;type=application/json
		$array['file[1]'] = __DIR__.'/'.$prep.$id.'.xml';//;type=application/xml
		// $f = fopen($array['file[1]'], 'r');
		$d = new drop($this->drop_folder,null,null,false,$this->database_name,0,null,null,null,true);
		$e = $d->upload($id.'.xml',$array['file[1]'],filesize($array['file[1]']));
		// fclose($f);
		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."' ,yuki=1,type=1 ");
		if(array_key_exists('invoice_to_export', $_SESSION) === true){
			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		}
		if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
        }
		$in['actiune'] = gm('Exported');

		if($in['from_acc']){
	 		$msg = '{l}Invoice was exported automatically to{endl} '.'Yuki ';
	 	}else{
	 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Yuki ';
	 	}
		insert_message_log('purchase_invoice',$msg,'purchase_invoice_id',$in['invoice_id']);

		unlink($array['file[1]']);
		if($in['from_acc']){
			return true;
		}else{
			json_out($in);	
		}
	}

	function marked_payed($data){
		foreach($data as $key=>$value){
      		$start_payment_date = mktime(23,59,59,date('n'),date('j'),date('y'));
		
      		$total = 0;
    		$tblinvoice=$this->db->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE id='".$value['invoice_id']."'");
    		$tblinvoice->next();
    		$currency = get_commission_type_list($tblinvoice->f('currency_type'));
    		$big_discount = $tblinvoice->f("discount");
    		if($tblinvoice->f('apply_discount') < 2){
      			$big_discount =0;
    		}
    		$lines = $this->db->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$value['invoice_id']."' ");
		    while ($lines->next()) {
		      $line_disc = $lines->f('discount');
		      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
		        $line_disc = 0;
		      }
		      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
		      $line = $line - $line * $big_discount / 100;

		      $total += $line + ($line * ( $lines->f('vat') / 100));

		    }
		    //already payed
		    if($tblinvoice->f('proforma_id')){
		      $filter = " OR invoice_id='".$tblinvoice->f('proforma_id')."' ";
		    }
		    $tot_payed=$this->db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='0' ");
		    $total_payed = $tot_payed;

		    $negative = 1;
		    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
		    if($use_negative==1){
		      $negative = -1;
		    }
    		$this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='1' ");
		    $this->db->move_next();
		    $total_payed += ($negative*$this->db->f('total_payed'));

		    $req_payment_value = $total;
		    if($tblinvoice->f('type')==1){
		      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
		    }
		    $amount_due = round($req_payment_value - $total_payed,2);
		    $general_amount = return_value($value['amount']);

    		$payment_date=date("Y-m-d",$start_payment_date);
    		$this->db->query("UPDATE tblinvoice SET not_paid='0' WHERE id='".$value['invoice_id']."'");

		    //Mark as payed
		    if($general_amount == $amount_due || $general_amount>$amount_due || round($amount_due-$general_amount,2)==0.01){
		      $this->db->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$value['invoice_id']."'");
		      $p = $this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$value['invoice_id']."' ");
		      if($p){
		        $this->db->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
		      }
		    }
		    else {
		      $this->db->query("UPDATE tblinvoice SET paid='2',status='1' WHERE id='".$value['invoice_id']."'");
		    }
		    $date_format=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DATE_FORMAT' ");

		    $payment_view_date=date($date_format,$start_payment_date);
		    $info=$payment_view_date.'  -  '.place_currency(display_number($general_amount),$currency);

    		$this->db->query("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$value['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$general_amount."',
                          info        = '".$info."' ");
    		$this->db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Payment has been successfully recorded by {endl} Cron')."', field_name='invoice_id', field_value='".$value['invoice_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
			$this->dbu_users->query("INSERT INTO cron_yuki SET `database_name`='".$this->database_name."',invoice_id='".$value['invoice_id']."'  ");
		}
	}



	function getProjects(&$in)
	{ 
			try
			{
				$soap = new SoapClient($this->webservice_url3,array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY));
				$res = $soap->Authenticate(array('accessKey' => $this->key));
	
				if (!isset($res->AuthenticateResult)){
					msg::error(gm("Incorrect login"),'error');
					if($in['from_cron']){
						insert_error_call($this->database_name,"yuki_check","invoice","","Incorrect login");
					}			
					return true;
				}
				$this->sess_id = $res->AuthenticateResult;
			
					
				$res = $soap->GetProjectsAndID(array('sessionID' => $this->sess_id, 'searchOption' => 'All'));
					if($res->GetProjectsAndIDResult){
						$x =$res->GetProjectsAndIDResult;

							foreach($x->Project as $key =>$value){

								$yuki_project_id = $this->db->field("SELECT yuki_project_id FROM yuki_projects WHERE yuki_id='".$value->id."' ");

								$time = strtotime($value->endDate);

								if( ($time >0  &&  $time> time() ) ||  $time <0){
									if(!$yuki_project_id){
										$this->db->query("INSERT INTO yuki_projects SET
								              yuki_id            	   =   '".$value->id."',
								              yuki_code 		       =   '".$value->code."',							              
								              description 		       =   '".addslashes($value->description)."'
								          ");
									}else{
										$this->db->query("UPDATE yuki_projects SET
								              yuki_code 		       =   '".$value->code."',
								              description 		       =   '".addslashes($value->description)."'
								              WHERE yuki_project_id='".$yuki_project_id."'
								          ");
									}
								}elseif($time >0  &&  $time< time()){
										if($yuki_project_id){
											$this->db->query("DELETE FROM yuki_projects  WHERE yuki_project_id='".$yuki_project_id."'  ");
										}
										
								}		

							}
						
					}
					

				return true;
			}
			catch(SoapFault $fault)
			{
				if($in['from_cron']){
					insert_error_call($this->database_name,"yuki_check","invoice","",addslashes($fault->faultstring));
				}else{
					msg::error ( $fault->faultstring, 'error');
				
					return false;
				}		
			}
		

		return true;
	
	}




}