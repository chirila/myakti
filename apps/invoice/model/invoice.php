<?php

class invoice{

  var $dbu;
  var $dbu2;
  var $pag = 'invoice';
  var $pag2 = 'recurring_ninvoice';
  var $field_n = 'invoice_id';
  var $field_n2 = 'recurring_invoice_id';
  var $httpHeader = array();
  var $exact_setup = array();

  function __construct() {
    global $database_config;
    $this->db = new sqldb();
    $this->db2 = new sqldb();
    $this->dbu = new sqldb();
    $this->dbu2 = new sqldb();
    $this->db_users = new sqldb();
    $this->db_import = new sqldb;
    $database_2 = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );
    $this->dbu_users =  new sqldb($database_2); // recurring billing
  }

  /**
   * archive invoice
   *
   * @return bool
   * @author PM
   **/
  function archive(&$in)
  {
    if(!$this->delete_validate($in)){
      return false;
    }
    $serial_number=$this->dbu->field("SELECT serial_number FROM tblinvoice WHERE  id='".$in['id']."' ");
    $invoice_type=$this->dbu->field("SELECT `type` FROM tblinvoice WHERE  id='".$in['id']."' ");
    if($serial_number){
      $inv_type=0;
      switch($invoice_type){
        case '2':
        case '4':
          $inv_type=2;
          break;
        case '1':
          $inv_type=1;
          break;
        default:
          $inv_type=0;
      } 
      $this->dbu->query("DELETE FROM tblinvoice_serial_numbers WHERE serial_number='".$serial_number."' AND `type`='".$inv_type."' ");
    }
    $this->dbu->query("UPDATE tblinvoice SET f_archived='1', serial_number='' WHERE id = '".$in['id']."' ");
    $proforma = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE  id='".$in['id']."' ");
    if($proforma){
      $this->dbu->query("UPDATE tblinvoice SET f_archived='0' WHERE id='".$proforma."' ");
    }    
    msg::success (gm("Invoice Archived"),'success');
    insert_message_log($this->pag,'{l}Invoice has been archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['id'],false);
    $in['pagl'] = $this->pag;
    $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['id']."' ");
    if($tracking_trace){
      $this->dbu->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
      $inv_source=$this->dbu->query("SELECT tracking_line.origin_type,tracking_line.origin_id FROM tracking
            INNER JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
            WHERE tracking.target_id='".$in['id']."' AND (tracking.target_type='1' OR tracking.target_type='7') ");
      while($inv_source->next()){
        if($inv_source->f('origin_type')=='4'){
          $this->dbu->query("UPDATE pim_orders SET invoiced='0' WHERE order_id='".$inv_source->f('origin_id')."' ");
          $this->dbu->query("UPDATE pim_orders_delivery SET invoiced='0' WHERE order_id='".$inv_source->f('origin_id')."'");
        }
      }
/*      $inv_source=$this->dbu->query("SELECT tracking_line.origin_type,tracking_line.origin_id FROM tracking
            INNER JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
            WHERE tracking.target_id='".$in['id']."' AND (tracking.target_type='1' OR tracking.target_type='7') ");
      while($inv_source->next()){
        if($inv_source->f('origin_type')=='5'){
          $this->dbu->query("UPDATE servicing_support SET billed='0' WHERE service_id='".$inv_source->f('origin_id')."' ");
          $this->dbu->query("UPDATE service_delivery SET invoiced='0' WHERE service_id='".$inv_source->f('origin_id')."'");
          $this->dbu->query("UPDATE servicing_support_articles SET billed='0' WHERE service_id='".$inv_source->f('origin_id')."'");
        }
      }*/
    }
    return true;
  }
  /**
   * delete validate
   *
   * @return bool
   * @author PM
   **/
  function delete_validate(&$in)
  {
    $is_ok = true;
    if(!$in['id']){
      msg::$error = gm('Invalid ID').'.';
      return false;
    }
    else{
      $this->dbu->query("SELECT id FROM tblinvoice WHERE id='".$in['id']."'");
      if(!$this->dbu->move_next()){
        msg::$error = gm('Invalid ID').'.';
        return false;
      }
    }
    return $is_ok;
  }

  function CanEdit(&$in){
    if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
      return true;
    }
    $c_id = $in['customer_id'];
    if(!$in['customer_id'] && $in['contact_id']) {
      $c_id = $this->dbu->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
    }
    if($c_id){
      if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
        $u = $this->dbu->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
        if($u){
          $u = explode(',', $u);
          if(in_array($_SESSION['u_id'], $u)){
            return true;
          }
          else{
            msg::$warning = gm("You don't have enought privileges");
            return false;
          }
        }else{
          msg::$warning = gm("You don't have enought privileges");
          return false;
        }
      }
    }
    return true;
  }

  /**
   * delete validate
   *
   * @return bool
   * @author PM
   **/
  function activate(&$in)
  {
    if(!$this->delete_validate($in)){
      return false;
    }


    $type=$this->dbu->field("SELECT type FROM tblinvoice  WHERE id = '".$in['id']."' ");
    if($type==0 && !DRAFT_INVOICE_NO_NUMBER){
        $serial_number=generate_invoice_number(DATABASE_NAME,false);
    }elseif($type==2  && !DRAFT_INVOICE_NO_NUMBER){
         $serial_number=generate_credit_invoice_number(DATABASE_NAME,false);
    }elseif($type==1  && !DRAFT_INVOICE_NO_NUMBER){
         $serial_number=generate_proforma_invoice_number(DATABASE_NAME,false);
    }

    $this->dbu->query("UPDATE tblinvoice SET f_archived='0',serial_number='".$serial_number."' WHERE id = '".$in['id']."' ");

      msg::success ( gm("Invoice Activated"),'success');
      insert_message_log($this->pag,'{l}Invoice has been activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['id'],false);
      $in['pagl'] = $this->pag;
      # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);

    if($inv->f('trace_id')){
      $this->dbu->query("UPDATE tracking SET archived='0' WHERE trace_id='".$inv->f('trace_id')."' ");
    }
    # for pdf
    return true;
  }

  /**
   * delete validate
   *
   * @return bool
   * @author PM
   **/
  function generate_pdf(&$in)
  {
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == '1' ){
      $in['custom_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    // console::log('generate_pdf 2');
    if($in['type_pdf']==2 || $in['type_pdf']==4 ){
      include(__DIR__.'/../controller/invoice_credit_print.php');
    }else{
      include(__DIR__.'/../controller/invoice_print.php');
    }

    return $str;
  }

  /**
   * delete validate
   *
   * @return bool
   * @author PM
   **/
  function delete(&$in)
  {
    if(!$this->delete_validate($in)){
      return false;
    }
    $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['id']."' ");
    $this->dbu->query("DELETE FROM tblinvoice_line WHERE invoice_id = '".$in['id']."'");
    $this->dbu->query("DELETE FROM tblinvoice_history WHERE invoice_id = '".$in['id']."'");
    $this->dbu->query("DELETE FROM tblinvoice_payments WHERE invoice_id = '".$in['id']."'");
    $this->dbu->query("DELETE FROM tbldownpayments WHERE invoice_id = '".$in['id']."'");
    $this->dbu->query("DELETE FROM logging WHERE field_name='invoice_id' AND field_value='".$in['id']."' ");
    $this->dbu->query("DELETE FROM tblinvoice WHERE id = '".$in['id']."'");
    if($tracking_trace){
      $this->dbu->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
      $this->dbu->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
    }

    msg::success ( gm("Invoice Deleted"),'success');
    return true;
  }

  /**
   * save the invoces for later use
   *
   * @return bool
   * @author PM
   **/
  function saveAddToPdf(&$in)
  {

    if($in['all']){
      if($in['value'] == 1){
        foreach ($in['item'] as $key => $value) {
          $_SESSION['add_to_pdf'][$value]= $in['value'];
        }
      }else{
        foreach ($in['item'] as $key => $value) {
          unset($_SESSION['add_to_pdf'][$value]);
        }
      }
    }else{
      if($in['value'] == 1){
        $_SESSION['add_to_pdf'][$in['item']]= $in['value'];
      }else{
        unset($_SESSION['add_to_pdf'][$in['item']]);
      }
    }
    $all_pages_selected=false;
    $minimum_selected=false;
    if($_SESSION['add_to_pdf'] && count($_SESSION['add_to_pdf'])){
      if($_SESSION['tmp_add_to_pdf'] && count($_SESSION['tmp_add_to_pdf']) == count($_SESSION['add_to_pdf'])){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
    //json_out($_SESSION['add_to_pdf']);
    //return true;
  }

  function saveAddToPdfAll(&$in){
    $all_pages_selected=false;
    $minimum_selected=false;
    if($in['value']){
      unset($_SESSION['add_to_pdf']);
      if($_SESSION['tmp_add_to_pdf'] && count($_SESSION['tmp_add_to_pdf'])){
        $_SESSION['add_to_pdf']=$_SESSION['tmp_add_to_pdf'];
        $all_pages_selected=true;
      }    
    }else{
      unset($_SESSION['add_to_pdf']);
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

    function saveAddToXls(&$in)
  {

    if($in['all']){
      if($in['value'] == 1){
        foreach ($in['item'] as $key => $value) {
          $_SESSION['add_to_xls'][$value]= $in['value'];
        }
      }else{
        foreach ($in['item'] as $key => $value) {
          unset($_SESSION['add_to_xls'][$value]);
        }
      }
    }else{
      if($in['value'] == 1){
        $_SESSION['add_to_xls'][$in['item']]= $in['value'];
      }else{
        unset($_SESSION['add_to_xls'][$in['item']]);
      }
    }
    $all_pages_selected=false;
    $minimum_selected=false;
    if($_SESSION['add_to_xls'] && count($_SESSION['add_to_xls'])){
      if($_SESSION['tmp_add_to_xls'] && count($_SESSION['tmp_add_to_xls']) == count($_SESSION['add_to_xls'])){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
      //json_out($_SESSION['add_to_xls']);
      //return true;
  }

  function saveAddToXlsAll(&$in){
    $all_pages_selected=false;
    $minimum_selected=false;
    if($in['value']){
      unset($_SESSION['add_to_xls']);
      if($_SESSION['tmp_add_to_xls'] && count($_SESSION['tmp_add_to_xls'])){
        $_SESSION['add_to_xls']=$_SESSION['tmp_add_to_xls'];
        $all_pages_selected=true;
      }    
    }else{
      unset($_SESSION['add_to_xls']);
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

    /**
   * save the invoces for later use
   *
   * @return bool
   * @author PM
   **/
  function saveAddToPurchase(&$in)
  {
    if($in['all']){
      if($in['value'] == 1){
        foreach ($in['item'] as $key => $value) {
          $_SESSION['add_to_purchase'][$value]= $in['value'];
        }
      }else{
        foreach ($in['item'] as $key => $value) {
          unset($_SESSION['add_to_purchase'][$value]);
        }
      }
    }else{
      if($in['value'] == 1){
        $_SESSION['add_to_purchase'][$in['item']]= $in['value'];
      }else{
        unset($_SESSION['add_to_purchase'][$in['item']]);
      }
    }
    $all_pages_selected=false;
    $minimum_selected=false;
    if($_SESSION['add_to_purchase'] && count($_SESSION['add_to_purchase'])){
      if($_SESSION['tmp_add_to_purchase'] && count($_SESSION['tmp_add_to_purchase']) == count($_SESSION['add_to_purchase'])){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
      //json_out($_SESSION['add_to_purchase']);
    //return true;
  }

  function saveAddToPurchaseAll(&$in){
    $all_pages_selected=false;
    $minimum_selected=false;
    if($in['value']){
      unset($_SESSION['add_to_purchase']);
      if($_SESSION['tmp_add_to_purchase'] && count($_SESSION['tmp_add_to_purchase'])){
        $_SESSION['add_to_purchase']=$_SESSION['tmp_add_to_purchase'];
        $all_pages_selected=true;
      }
    }else{
      unset($_SESSION['add_to_purchase']);
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

  /**
   * Delete Bulk Invoices
   *
   * @return bool
   * @author PM
   **/  
  function deleteBulkInvoices(&$in){
    if(empty($_SESSION['add_to_pdf'])){
      msg::error(gm('No invoices selected'),'error');
      json_out($in);
    }

    $add_to_pdf=$this->db->query("SELECT id FROM tblinvoice WHERE f_archived='1' AND id IN (".implode(',',array_keys($_SESSION['add_to_pdf'])).")")->getValues('id');

    if(empty($add_to_pdf)){
      msg::error(gm('No invoices selected'),'error');
      json_out($in);
    }

    foreach($add_to_pdf as $value){
      $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice WHERE id='".$value."' ");
      $this->dbu->query("DELETE FROM tblinvoice_line WHERE invoice_id = '".$value."'");
      $this->dbu->query("DELETE FROM tblinvoice_history WHERE invoice_id = '".$value."'");
      $this->dbu->query("DELETE FROM tblinvoice_payments WHERE invoice_id = '".$value."'");
      $this->dbu->query("DELETE FROM logging WHERE field_name='invoice_id' AND field_value='".$value."' ");
      $this->dbu->query("DELETE FROM tblinvoice WHERE id = '".$value."'");
      if($tracking_trace){
        $this->dbu->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
        $this->dbu->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
      }
      unset($_SESSION['add_to_pdf'][$value]);
    }

    msg::success(gm('Invoices successfully deleted'),'success');
    return true;
  }

    function markSentBulkInvoices(&$in){

    if(empty($_SESSION['add_to_pdf'])){
      msg::error(gm('No invoices selected'),'error');
      json_out($in);
    }

    foreach($_SESSION['add_to_pdf'] as $key=>$value){

      $sent = $this->dbu->field("SELECT sent FROM tblinvoice WHERE id = '".$key."' ");
      if(!$sent){
          $in['invoice_id'] = $key;
          $invoice_date = $this->dbu->field("SELECT invoice_date FROM tblinvoice WHERE id = '".$key."' ");
          
          $inv_date= new DateTime();
          date_timestamp_set($inv_date, $invoice_date);
          $invoice_date = (array)$inv_date;
          $in['invoice_date'] = $invoice_date['date'];

          $this->mark_sent($in);
      }
      unset($_SESSION['add_to_pdf'][$key]);
    }

    msg::success(gm('Invoices successfully marked as sent'),'success');
    return true;
  }

  /**
   * pay invoice
   *
   * @return bool
   * @author PM
   **/
  function pay(&$in){

    if(!$this->pay_validate($in))
    {
      return false;
    }
    if(!empty($in['payment_date'])){
      $in['payment_date'] =strtotime($in['payment_date']);
      $in['payment_date'] = mktime(23,59,59,date('n',$in['payment_date']),date('j',$in['payment_date']),date('y',$in['payment_date']));
    }

    $total = 0;
    $tblinvoice=$this->dbu->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE  id='".$in['invoice_id']."'");
    $tblinvoice->next();
    $currency = get_commission_type_list($this->dbu->f('currency_type'));
    $big_discount = $tblinvoice->f("discount");
    if($tblinvoice->f('apply_discount') < 2){
      $big_discount =0;
    }
    $lines = $this->dbu->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
    while ($lines->next()) {
      $line_disc = $lines->f('discount');
      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
        $line_disc = 0;
      }
      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
      $line = $line - $line * $big_discount / 100;

      $total += $line + ($line * ( $lines->f('vat') / 100));

    }

    $filter = " invoice_id='".$in['invoice_id']."' ";
    //already payed
    if($tblinvoice->f('proforma_id')){
      $filter = " (invoice_id='".$in['invoice_id']."' OR invoice_id='".$tblinvoice->f('proforma_id')."') ";
    }
    $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE {$filter}  AND credit_payment='0' ");
    $this->dbu->move_next();
    $total_payed = $this->dbu->f('total_payed');

    $negative = 1;
    if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
      $negative = -1;
    }
    $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE {$filter}  AND credit_payment='1' ");
    $this->dbu->move_next();
    $total_payed += ($negative*$this->dbu->f('total_payed'));

    $req_payment_value = $total;
    if($tblinvoice->f('type')==1){
      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
    }

    $amount_due = round($req_payment_value - $total_payed,2);
 //console::log($req_payment_value,$total_payed);
    $in['amount'] = return_value($in['amount']);

    if($in['cost']){
      $in['amount'] += return_value($in['cost']);
    }

    $amount_val=$in['payment_method'] == '14' && $in['c_invoice_id'] && !empty($in['c_invoice_id']) ? abs($in['amount']) : $in['amount'];

    /*if(($in['amount'] > $amount_due) || $in['amount'] == 0){
      msg::$error =  gm('Invalid Amount');
      return false;
    }*/

    $payment_date=date("Y-m-d",$in['payment_date']);

    //Mark as Will not be paid
    if($in['not_paid']){
      $this->dbu->query("UPDATE tblinvoice SET not_paid='1' WHERE id='".$in['invoice_id']."'");
      /*msg::success(gm('Payment has been successfully recorded'),'success');*/
      insert_message_log($this->pag,'{l}Invoice was marked as will not be paid by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
      return true; 
    } else {
      $this->dbu->query("UPDATE tblinvoice SET not_paid='0' WHERE id='".$in['invoice_id']."'");
    }

    //Mark as payed
    if($amount_val == $amount_due || $amount_val>$amount_due || round($amount_due-$amount_val,2)==0.01){
      $this->dbu->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$in['invoice_id']."'");
      $p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($p){
          $invoices_from_proforma=$this->dbu->query("SELECT id,req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE proforma_id='".$p."'");
          $sum_amount_inv = $amount_val;
          $amount_due_proforma = $this->dbu->field("SELECT amount FROM tblinvoice WHERE id='".$p."' ");
          while($invoices_from_proforma->next()){
             $sum = $this->dbu->field("SELECT SUM(amount) AS sum_amount_inv FROM tblinvoice_payments WHERE invoice_id= '".$invoices_from_proforma->f('id')."' AND credit_payment='0' ");
             $sum_amount_inv +=$sum;
          }
          //console::log($sum_amount_inv , $amount_due_proforma);
          if($sum_amount_inv == $amount_due_proforma || $sum_amount_inv>$amount_due_proforma || round($amount_due_proforma-$sum_amount_inv,2)==0.01){
            //Proforma should be marked as paid when sum of all invoices is >= amount of proforma invoice. - task 3649
            $this->dbu->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
          }else{
            $this->dbu->query("UPDATE tblinvoice SET paid='2', status='1' WHERE id='".$p."' ");
          }
        
      }
    }
    else {
      $this->dbu->query("UPDATE tblinvoice SET paid='2',status='1' WHERE id='".$in['invoice_id']."'");
    }

    //$info='{l}Payment of{endl}'.'@/'.place_currency(display_number($in['amount']),$currency).'@/'.'{l}successfully recorded by{endl}'.'@/'.get_user_name($_SESSION['u_id']);
    if($in['notes']){
      $in['notes']='  -  '.$in['notes'];
    }

    $no_cost="";
    if($in['cost'] == '0,00' || $in['cost'] == ''){
      $in['cost']=$no_cost;
    }else{
       $in['cost']='  -  '.$in['cost'];
    }

    $payment_view_date=date(ACCOUNT_DATE_FORMAT,$in['payment_date']);
    $info=$payment_view_date.'  -  '.place_currency(display_number($in['amount']),$currency).$in['notes'].$in['cost'];
    
    //INSERT PAYMENT
    $coda_pay=0;
    if($in['coda_pay']){
      $coda_pay=$in['coda_pay'];
    }
    if($in['payment_method'] == '14' && $in['c_invoice_id'] && !empty($in['c_invoice_id'])){
      $this->dbu->query("UPDATE tblinvoice SET status='1', paid='1' WHERE id='".$in['c_invoice_id']."'");
      $info = '<a href="index.php?do=invoice-invoice&invoice_id='.$in['c_invoice_id'].'" >'.gm('Credit Invoice').'</a>'.$in['notes'];
      $new_pay_id=$this->dbu->query("INSERT INTO tblinvoice_payments SET
                              invoice_id  = '".$in['invoice_id']."',
                              date    = '".$payment_date."',
                              amount    = '".$in['amount']."',
                              info        = '".$info."',
                              codapayment_id='".$coda_pay."',
                              payment_method='".$in['payment_method']."',
                              credit_payment='1' ");
      $trace_line_origins=$this->dbu->query("SELECT tracking_line.origin_id FROM tracking INNER JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id WHERE target_id='".$in['invoice_id']."' AND target_type='8' AND (tracking_line.origin_type='1' OR tracking_line.origin_type='7') ")->getAll();
      $has_origin_tracking=false; 
      foreach($trace_line_origins as $origin){
        if($origin['origin_id'] == $in['invoice_id']){
          $has_origin_tracking=true; 
          break;
        }
      }
      if(!$has_origin_tracking){
        $tracking_data=array(
            'target_id'         => $in['c_invoice_id'],
            'target_type'       => '8',
            'target_buyer_id'   => $tblinvoice->f('buyer_id'),
            'lines'             => array(
                                          array('origin_id'=>$in['invoice_id'],'origin_type'=>($tblinvoice->f('type')=='1' ? '7' : '1')
                                        )
                                    )
          );
        $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['c_invoice_id']."' ");
        addTracking($tracking_data,$tracking_trace);
      }
    }else{
      $new_pay_id=$this->dbu->insert("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$in['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$in['amount']."',
                          info        = '".$info."',
                          codapayment_id='".$coda_pay."', 
                          payment_method='".$in['payment_method']."' ");
    }   
    if($tblinvoice->f('bPaid_debt_id') != ''){
      $payment_row=$this->dbu->query("SELECT * FROM tblinvoice_payments WHERE payment_id='".$new_pay_id."' ")->getAll();
      foreach($payment_row as $key => $value){
        $this->bPaid_payment($value,$tblinvoice->f('bPaid_debt_id'),$tblinvoice->f('serial_number'),1);
      }
    }

    /*msg::success(gm('Payment has been successfully recorded'),'success');*/

    insert_message_log($this->pag,'{l}Payment has been successfully recorded by {endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
    $in['pagl'] = $this->pag;
    if($in['method_pay']==2){
      # this is for import;
    }else if($in['coda_pay']){
      //this is payment from coda so nothing
    }
    elseif($in['from_list'] == 1){
      // $in['do'] = 'invoice-invoices';
      ark::$controller='invoices';
    }else{
      ark::$controller='invoice';
    }


    # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    if($in['method_pay']==2){

    }else{
      $this->generate_pdf($params);
    }

    # for pdf
    return true;
  }

  /**
   * pay invoice validate
   *
   * @return bool
   * @author PM
   **/
  function pay_validate(&$in)
  {
    if($in['total_amount_due1']){
      $in['amount']=$in['total_amount_due1'];
    }
    $v = new validation($in);
    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
    $v->field('payment_date','Payment date','required');

    $v->field('amount','Amount','required:numeric');
    $is_ok = $v->run();
    if ($is_ok == true )
    {
      $this->dbu->query("SELECT paid,discount,sent FROM tblinvoice WHERE id='".$in['invoice_id']."'");
      $this->dbu->move_next();
      /*if($this->dbu->f('sent') != '1'){
        msg::notice(gm('Please mark as sent the invoice and then make a payment'),'notice');
        $is_ok = false;
      }*/

      if($this->dbu->f('paid') == 1){
        msg::notice(gm('The invoice has already been paid'),'notice');
        $is_ok = false;
      }
      if($this->dbu->f('not_paid') == 1){
          $is_ok= false;
      }
      $in['discount'] = $this->dbu->f('discount');
    }

    return $is_ok;
  }

  function external_id(&$in)
  {
    if(defined('USE_INVOICE_WEB_LINK') && USE_INVOICE_WEB_LINK == 1){
      //$exist_url = $this->dbu_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['invoice_id']."' AND `type`='i'  ");
      $exist_url = $this->dbu_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type  ",['d'=>DATABASE_NAME,'item_id'=>$in['invoice_id'],'type'=>'i']);
      if(!$exist_url){
        $url = generate_chars();
        while (!isUnique($url)) {
          $url = generate_chars();
        }
        //$this->dbu_users->query("INSERT INTO urls SET `url_code`='".$url."', `database`='".DATABASE_NAME."', `item_id`='".$in['invoice_id']."', `type`='i'  ");
        $this->dbu_users->insert("INSERT INTO urls SET `url_code`= :url_code, `database`= :d, `item_id`= :item_id, `type`= :type  ",['url_code'=>$url,'d'=>DATABASE_NAME,'item_id'=>$in['invoice_id'],'type'=>'i']);
        $exist_url = $url;
      }
      return $exist_url;
    }
  }

  /****************************************************************
  * function mark_sent(&$in)                                         *
  ****************************************************************/
  function mark_sent(&$in){ 
    if(!$this->mark_sent_validate($in))
    {
      json_out($in);
      return false;
    }
     $t=$this->dbu->field("SELECT type FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
     $s=$this->dbu->field("SELECT serial_number FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

     if(!$s){
         if($t==0){
            $in['serial_number']=generate_invoice_number(DATABASE_NAME,false);
          }
          if($t==1){
            $in['serial_number']=generate_proforma_invoice_number(DATABASE_NAME,false);
          }
           if($t==2){
            $in['serial_number']=generate_credit_invoice_number(DATABASE_NAME,false);
          }
    $invoice_payment_type = $this->dbu->field("SELECT payment_term_type FROM tblinvoice WHERE id='".$in['invoice_id']."'");
   // $payment_term = $this->dbu->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
    $payment_term =$this->dbu->field("SELECT due_days FROM tblinvoice WHERE id='".$in['invoice_id']."'");
    if($invoice_payment_type==1) { # invoice date
      $due_date = strtotime($in['invoice_date']) + ( $payment_term * ( 60*60*24 )-1);
    } else { # next month first day
      /*$curMonth = date('n',strtotime($in['invoice_date']));
      $curYear  = date('Y',strtotime($in['invoice_date']));
          $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
      $due_date = $firstDayNextMonth + ($payment_term * ( 60*60*24 )-1);*/
      $tmstmp_month=strtotime($in['invoice_date']) + ( $payment_term * ( 60*60*24 )-1);
      $lastday = date('t',$tmstmp_month);
      $due_date = mktime(23, 59, 59,date('m',$tmstmp_month), $lastday,date('Y',$tmstmp_month));


    }

         $this->dbu->query("UPDATE tblinvoice SET serial_number='".$in['serial_number']."',invoice_date='".strtotime($in['invoice_date'])."',due_date='".$due_date."' WHERE id='".$in['invoice_id']."'");
         $ogm_number = $this->dbu->field("SELECT ogm FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
        if(!$ogm_number && ($t == 0 || $t ==3)){
          $ogm = generate_ogm(DATABASE_NAME,$in['invoice_id']);
          $this->dbu->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$in['invoice_id']."' ");
        }
            # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['type_pdf'] = $inv->f('type');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);
    # for pdf
     }

    $this->dbu->query("UPDATE tblinvoice SET sent='1',sent_date='".strtotime($in['sent_date'])."' WHERE id='".$in['invoice_id']."'");

    $p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
    if($p){
      $proformaInv = $this->dbu->query("SELECT req_payment,status,paid FROM tblinvoice WHERE id='".$p."' ");
      $this->dbu->query("UPDATE tblinvoice SET sent='1', sent_date='".strtotime($in['sent_date'])."' WHERE id='".$p."' ");
      $status = $proformaInv->f('status');
      if($proformaInv->f('req_payment')==100){
        $paid=$proformaInv->f('paid');
      }else{
        $paid=2;
        $status=0;
      }
      //$this->dbu->query("UPDATE tblinvoice SET status='".$status."', paid='".$paid."' WHERE id='".$in['invoice_id']."' ");
    }
   

    $yuki_active=$this->dbu->field("SELECT active FROM apps WHERE name='Yuki' AND main_app_id='0' ");
    $coda_active=$this->dbu->field("SELECT active FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
    $clearfacts_active=$this->dbu->field("SELECT active FROM apps WHERE name='Clearfacts' AND main_app_id='0' "); 
    $billtobox_active=$this->dbu->field("SELECT active FROM apps WHERE name='BillToBox' AND main_app_id='0' "); 
    $btb_active=$this->dbu->field("SELECT active FROM apps WHERE name='WinBooks' AND main_app_id='0' ");  
    $invoice_type = $t;

    $accountant_settings=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
    if($accountant_settings=='1' && $invoice_type!='1' && !$in['without_send_to_accountant']){
       $acc_app_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_APP_ID' ");
       $acc_block_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
       $acc_export_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
      if($acc_block_id=='2'){
        $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
      }
      if($acc_export_id=='2' && $acc_app_id){
        if($acc_app_id=='1'){
          if($yuki_active=='1'){
              $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
              if(!$is_exported){
                include_once(__DIR__.'/yuki.php');
                $yuki_obj=new yuki($in);
                $in['from_acc']=1;
                $yuki_obj->exportYuki($in);
                if($acc_block_id=='1'){
                  $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
                }
              }
         }
        }else if($acc_app_id=='2'){
          if($coda_active=='1'){
              $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
              $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
              $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
              if(!$is_exported && (is_null($codabox_sales) || $codabox_sales=='1')){
                include_once(__DIR__.'/btb.php');
                //$btb_obj=new btb();
                $btb_obj=new btb($in);
                $in['from_acc']=1;
                $btb_obj->exportCoda($in);
                if($acc_block_id=='1'){
                  $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
                }
              }
           }   
        }else if($acc_app_id=='3'){
          if($clearfacts_active=='1'){
              $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
              if(!$is_exported ){
                include_once(__DIR__.'/clearfacts.php');
                $clearfacts_obj=new clearfacts($in);
                $in['from_acc']=1;
                $clearfacts_obj->exportFacts($in);
                if($acc_block_id=='1'){
                  $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
                }
              }
          }
        }else if($acc_app_id=='4'){
            if($billtobox_active=='1'){
                $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
                if(!$is_exported ){
                  include_once(__DIR__.'/billtobox.php');
                  $billtobox_obj=new billtobox($in);
                  $in['from_acc']=1;
                  $billtobox_obj->exportBillToBox($in);
                  if($acc_block_id=='1'){
                    $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
                  }
                }
            }
        }
      }else if($acc_export_id=='2' && !$acc_app_id){
        if($yuki_active=='1'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
            if(!$is_exported ){
              include_once(__DIR__.'/yuki.php');
              $yuki_obj=new yuki($in);
              $in['from_acc']=1;
              $yuki_obj->exportYuki($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
        }
        if($coda_active=='1'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
            $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
            $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
            if(!$is_exported  && (is_null($codabox_sales) || $codabox_sales=='1')){
              include_once(__DIR__.'/btb.php');
              //$btb_obj=new btb();
              $btb_obj=new btb($in);
              $in['from_acc']=1;
              $btb_obj->exportCoda($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
        }
        if($clearfacts_active=='1'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
            if(!$is_exported ){
              include_once(__DIR__.'/clearfacts.php');
              $clearfacts_obj=new clearfacts($in);
              $in['from_acc']=1;
              $clearfacts_obj->exportFacts($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            } 
        }   
        if($billtobox_active=='1'){   
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
            if(!$is_exported ){
              include_once(__DIR__.'/billtobox.php');
              $billtobox_obj=new billtobox($in);
              $in['from_acc']=1;
              $billtobox_obj->exportBillToBox($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
        }
        if($btb_active=='1'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type='0'");
            if(!$is_exported ){
              include_once(__DIR__.'/btb.php');
              $btb_obj=new btb($in);
              $in['from_acc']=1;
              $btb_obj->exportBtb($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }  
        }      
      }else if(!$acc_export_id){
        if($yuki_active=='1'  && defined('EXPORT_AUTO_INVOICE_YUKI') && EXPORT_AUTO_INVOICE_YUKI=='2'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
            if(!$is_exported){
              include_once(__DIR__.'/yuki.php');
              $yuki_obj=new yuki($in);
              $in['from_acc']=1;
              $yuki_obj->exportYuki($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
          }
          if($coda_active=='1' && defined('EXPORT_AUTO_INVOICE_CODABOX') && EXPORT_AUTO_INVOICE_CODABOX=='2'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
            $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
            $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
            if(!$is_exported && (is_null($codabox_sales) || $codabox_sales=='1') ){
              include_once(__DIR__.'/btb.php');
              //$btb_obj=new btb();
              $btb_obj=new btb($in);
              $in['from_acc']=1;
              $btb_obj->exportCoda($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
          }
          if($clearfacts_active=='1' && defined('EXPORT_AUTO_INVOICE_CLEARFACTS') && EXPORT_AUTO_INVOICE_CLEARFACTS=='2'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
            if(!$is_exported ){
              include_once(__DIR__.'/clearfacts.php');
              $clearfacts_obj=new clearfacts($in);
              $in['from_acc']=1;
              $clearfacts_obj->exportFacts($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
          }
          if($billtobox_active=='1' &&  defined('EXPORT_AUTO_INVOICE_BILLTOBOX') && EXPORT_AUTO_INVOICE_BILLTOBOX=='2'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
            if(!$is_exported ){
              include_once(__DIR__.'/billtobox.php');
              $billtobox_obj=new billtobox($in);
              $in['from_acc']=1;
              $billtobox_obj->exportBillToBox($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
          }
          if($btb_active=='1' && defined('EXPORT_AUTO_INVOICE_EFFF') && EXPORT_AUTO_INVOICE_EFFF=='2'){
            $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type='0'");
            if(!$is_exported ){
              include_once(__DIR__.'/btb.php');
              $btb_obj=new btb($in);
              $in['from_acc']=1;
              $btb_obj->exportBtb($in);
              if($acc_block_id=='1'){
                $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
              }
            }
          }
      }
    }else if($invoice_type != '1' && !$in['without_send_to_accountant']){
      if($yuki_active=='1' && defined('EXPORT_AUTO_INVOICE_YUKI') && EXPORT_AUTO_INVOICE_YUKI=='2'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
          if(!$is_exported){
            include_once(__DIR__.'/yuki.php');
            $yuki_obj=new yuki($in);
            $in['from_acc']=1;
            $yuki_obj->exportYuki($in);
          }
      }
      if($coda_active=='1' && defined('EXPORT_AUTO_INVOICE_CODABOX') && EXPORT_AUTO_INVOICE_CODABOX=='2'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
          $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
          $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
          if(!$is_exported && (is_null($codabox_sales) || $codabox_sales=='1') ){
            include_once(__DIR__.'/btb.php');
            //$btb_obj=new btb();
            $btb_obj=new btb($in);
            $in['from_acc']=1;
            $btb_obj->exportCoda($in);
          }
        }
        if($clearfacts_active=='1' && defined('EXPORT_AUTO_INVOICE_CLEARFACTS') && EXPORT_AUTO_INVOICE_CLEARFACTS=='2'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
          if(!$is_exported ){
            include_once(__DIR__.'/clearfacts.php');
            $clearfacts_obj=new clearfacts($in);
            $in['from_acc']=1;
            $clearfacts_obj->exportFacts($in);
          }
        }
        if($billtobox_active=='1' && defined('EXPORT_AUTO_INVOICE_BILLTOBOX') && EXPORT_AUTO_INVOICE_BILLTOBOX=='2' ){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
          if(!$is_exported ){
            include_once(__DIR__.'/billtobox.php');
            $billtobox_obj=new billtobox($in);
            $in['from_acc']=1;
            $billtobox_obj->exportBillToBox($in);
          }
        }
        if($btb_active=='1'  && defined('EXPORT_AUTO_INVOICE_EFFF') && EXPORT_AUTO_INVOICE_EFFF=='2'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type='0'");
          if(!$is_exported ){
            include_once(__DIR__.'/btb.php');
            $btb_obj=new btb($in);
            $in['from_acc']=1;
            $btb_obj->exportBtb($in);
            if($acc_block_id=='1'){
              $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
            }
          }
        }
    }       
    //$in['act'] = '';
    $this->external_id($in);
    msg::success(gm('Invoice has been successfully marked as sent'),'success');
    insert_message_log($this->pag, '{l}Invoice was marked as sent by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
    $in['pagl'] = $this->pag;

    return true;
  }

  /****************************************************************
  * function mark_sent_validate(&$in)                                *
  ****************************************************************/
  function mark_sent_validate(&$in)
  {
    $v = new validation($in);

    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
    $v->field('sent_date','Sent date','required');

    if($v->run()){
      $tblinvoice = $this->dbu->query("SELECT invoice_date FROM tblinvoice WHERE id='".$in['invoice_id']."'");

      // $invoiceDate =  new DateTime(gmdate("Y-m-d\TH:i:s\Z",(int)$tblinvoice->f('invoice_date')));
      // $invoiceDate->setTime(0,0,0);

      $invoiceDate =  mktime(0,0,0,
                  date("n",(int)$tblinvoice->f('invoice_date')),
                  date("j",(int)$tblinvoice->f('invoice_date')), 
                  date("Y",(int)$tblinvoice->f('invoice_date'))
                );

      // $sentDate = new DateTime($in['sent_date']);
      // $sentDate->setTime(0,0,0);

      // $sendDate = strtotime($in['sent_date']);
      if(is_numeric($in['sent_date'])){
              $sentDate = mktime(0,0,0,
                  date("n",$in['sent_date']),
                  date("j",$in['sent_date']), 
                  date("Y",$in['sent_date'])
                );
      }else{
        $sentDate = mktime(0,0,0,
            date("n",strtotime($in['sent_date'])),
            date("j",strtotime($in['sent_date'])), 
            date("Y",strtotime($in['sent_date']))
          );
      }

      if($sentDate < $invoiceDate){
        msg::error(gm('Sent Date must be equal or be grater than Invoice Date'),'sent_date');
        return false;
      }
    }

    return $v->run();
  }

  /****************************************************************
  * function mark_as_balanced(&$in)                                         *
  ****************************************************************/
  function mark_as_balanced(&$in){ 
    if(!$this->mark_as_balanced_validate($in))
    {
      json_out($in);
      return false;
    }
     
    if($in['type'] == 2){
      $this->dbu->query("UPDATE tblinvoice SET status='1', paid='1' WHERE id='".$in['invoice_id']."'");
      $invoice_paid = $this->dbu->field("SELECT paid FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      //if($in['c_invoice_id'] && $invoice_paid !=1){
      /*if($in['c_invoice_id']){
          $amount_c = $this->dbu->query("SELECT amount, amount_vat FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
          $amount = $this->dbu->query("SELECT amount, amount_vat FROM tblinvoice WHERE id='".$in['c_invoice_id']."' ");
          $info = '<a href="index.php?do=invoice-invoice&invoice_id='.$in['invoice_id'].'" >'.gm('Credit Invoice').'</a>';
          $payment_date = date('Y-m-d');

          $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['c_invoice_id']."' AND credit_payment='0' ");
          $this->dbu->move_next();
          $total_payed = $this->dbu->f('total_payed');

          $negative = 1;
          if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
            $negative = -1;
          }

          $this->dbu->query("SELECT amount FROM tblinvoice_payments WHERE invoice_id='".$in['c_invoice_id']."'  AND credit_payment='1' ");
          while($this->dbu->next()){
            $total_payed +=($this->dbu->f('amount') < 0 ? abs($this->dbu->f('amount')) : $this->dbu->f('amount'));
          }

          $c_invoice_amount=$amount_c->f('amount_vat') < 0 ? abs($amount_c->f('amount_vat')) : $amount_c->f('amount_vat');
          if($c_invoice_amount + $total_payed >= $amount->f('amount_vat')){
            $this->dbu->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$in['c_invoice_id']."' ");
            $paid = 1;
          }else{
            $this->dbu->query("UPDATE tblinvoice SET paid='2',status='1' WHERE id='".$in['c_invoice_id']."' ");
            $paid = 2;
          }
          $this->dbu->query("INSERT INTO tblinvoice_payments SET
                              invoice_id  = '".$in['c_invoice_id']."',
                              date    = '".$payment_date."',
                              amount    = '".$amount_c->f('amount_vat')."',
                              info        = '".$info."',
                              credit_payment='1' ");

      }*/
      
    }
       
    msg::success(gm('Invoice has been successfully marked as balanced'),'success');
    insert_message_log($this->pag, '{l}Invoice was marked as balanced by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
    $in['pagl'] = $this->pag;

    return true;
  }

  /****************************************************************
  * function mark_as_balanced_validate(&$in)                                *
  ****************************************************************/
  function mark_as_balanced_validate(&$in)
  {
    $v = new validation($in);

    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
    return $v->run();
  }


    /****************************************************************
  * function sendNewEmail(&$in)                                         *
  ****************************************************************/
  function sendNewEmail(&$in)
  {
    if(!$this->sendNewEmailValidate($in)){
      msg::$error = 'Invalid email address';
      return false;
    }

    $t=$this->dbu->field("SELECT type FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
     $s=$this->dbu->field("SELECT serial_number FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

     if(!$s){
         if($t==0){
            $in['serial_number']=generate_invoice_number(DATABASE_NAME,false);
          }
          if($t==1){
            $in['serial_number']=generate_proforma_invoice_number(DATABASE_NAME,false);
          }
           if($t==2){
            $in['serial_number']=generate_credit_invoice_number(DATABASE_NAME,false);
          }

         $this->dbu->query("UPDATE tblinvoice SET serial_number='".$in['serial_number']."',invoice_date='".$in['i_date']."' WHERE id='".$in['invoice_id']."'");

        $ogm_number = $this->dbu->field("SELECT ogm FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
        if(!$ogm_number && ($t == 0 || $t ==3)){
          $ogm = generate_ogm(DATABASE_NAME,$in['invoice_id']);
          $this->dbu->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$in['invoice_id']."' ");
        }
               # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['type_pdf'] = $inv->f('type');
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';

    $this->generate_pdf($params);
    # for pdf

  }

    global $config;
    $in['weblink_url'] =$this->external_id($in);
    $this->dbu->query("SELECT tblinvoice.id,tblinvoice.serial_number,tblinvoice.discount,tblinvoice.invoice_date,tblinvoice.buyer_name,tblinvoice.currency_type,tblinvoice.pdf_layout,tblinvoice.pdf_logo,tblinvoice.email_language,tblinvoice.identity_id,
                           SUM(tblinvoice_line.amount) AS total,
                           SUM(tblinvoice_payments.amount) AS total_payed,
                           tblinvoice.seller_bwt_nr,tblinvoice.buyer_name, tblinvoice.acc_manager_id
                           FROM tblinvoice
                           LEFT JOIN tblinvoice_line ON tblinvoice_line.invoice_id=tblinvoice.id
                           LEFT JOIN tblinvoice_payments ON tblinvoice_payments.invoice_id=tblinvoice.id
                           WHERE tblinvoice.id='".$in['invoice_id']."'");
    $this->dbu->move_next();
    $e_lang = $this->dbu->f('email_language');
    $acc_manager_id = $this->dbu->f('acc_manager_id');
    $identity_id=$this->dbu->f('identity_id');
    if($this->dbu->f('pdf_layout'))
    {
      $in['type']=$this->dbu->f('pdf_layout');
      $in['logo']=$this->dbu->f('pdf_logo');
    }else {
      $in['type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    $in['id'] = $in['invoice_id'];
    $b_name=$this->dbu->f('buyer_name');
    $seller_bwt_nr=$this->dbu->f('seller_bwt_nr');
    $this->generate_pdf($in);


     //start email body
    $mail = new PHPMailer();
    //add images to mail
    $xxx=$this->getImagesFromMsg($in['e_message'],'upload');
    foreach($xxx as $location => $value){
      $tmp_start=strpos($location,'/');
      $tmp_end=strpos($location,'.');
      $tmp_cid_name=substr($location,$tmp_start+1,$tmp_end-1-$tmp_start);
      $tmp_file_name=$tmp_cid_name.substr($location, $tmp_end, strlen($location)-$tmp_end);
      $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].$config['install_path'].$location, 'sign_'.$tmp_cid_name, $tmp_file_name); 
      //$in['e_message']=str_replace($value,'<img src="cid:'.'sign_'.$tmp_cid_name.'">',$in['e_message']);
      $in['e_message']=str_replace($value,generate_img_with_cid($value,$tmp_cid_name),$in['e_message']);
    }

    $in['e_message'] = stripslashes($in['e_message']);
    /*$body= stripslashes(htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8'));*/
    $body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');

    if($in['sendgrid_selected']){
      $e_message_encoded = $in['e_message'];
    } else {
      //$e_message_encoded = utf8_decode( $in['e_message']);
      $e_message_encoded = $in['e_message'];
    }  

    $head = '<style>p { margin: 0px; padding: 0px; }</style>';
    $body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 35px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 30px;">'.$e_message_encoded.'</div>';
  
    $body = stripslashes($body_style);

    if(!$e_lang || $e_lang > 4){
       $e_lang=1;
    }
    $text_array = array('1' => array('simple' => array('1' => 'INVOICE', '2'=> 'You can download your invoice HERE','3'=>"HERE",'4'=>'Web Link'),
                       'pay' => array('1' => 'INVOICE', '2'=> 'Check it by clicking on the link above','3'=>"HERE",'4'=>'Web Link')
                       ),
              '2' => array('simple' => array('1' => 'FACTURE', '2'=> 'Vous pouvez télécharger votre facture ICI','3'=>'ICI','4'=>'Lien web'),
                       'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Cliquez sur le lienweb pour visualer votre facture.','3'=>'ICI','4'=>'Lien web')
                       ),
              '3' => array('simple' => array('1' => 'FACTUUR', '2'=> 'U kan uw factuur HIER downloaden','3'=>'HIER','4'=>'Weblink'),
                       'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'Klik op de link hierboven om de factuur te bekijken.','3'=>'HIER','4'=>'Weblink')
                       ),
              '4' => array('simple' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
                       'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER','4'=>'WEB-LINK')
                       )
    );

    $date = explode('-',$this->dbu->f('invoice_date'));
    $factur_date = $date[2].'/'.$date[1].'/'.$date[0];
    $total=$this->dbu->f('total');
    $currency=get_commission_type_list($this->dbu->f('currency_type'));

    $discount_value = $total*$this->dbu->f('discount')/100;
    $total -= $discount_value;

    $total_payed = $this->dbu->f('total_payed');
    $amount_due = round($total - $total_payed,2);
    $emails = '';

    $tblinvoice = $this->dbu->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."'");
    $contact =  $this->dbu->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'");

    $title_cont =  $this->dbu->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
         
    $customer =  $this->dbu->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."'");

    if($title_cont==''){
      $title_cont='';
    }
    if($contact->f('firstname')==''){
      $contactfirstname='';
    }else{
      $contactfirstname=$contact->f('firstname');
    }
    if($contact->f('lastname')==''){
      $contactlasttname='';
    }else{
      $contactlasttname=$contact->f('lastname');
    }
       
    $body=str_replace('[!CONTACT_FIRST_NAME!]',"".utf8_decode($contactfirstname)."",$body);
    $body=str_replace('[!CONTACT_LAST_NAME!]',"".utf8_decode($contactlasttname)."",$body);
    $body=str_replace('[!SALUTATION!]',"".$title_cont."",$body);
    $body=str_replace('[!YOUR_REFERENCE!]',"".$tblinvoice->f('your_ref')."",$body);
    $body=str_replace('[!OWN_REFERENCE!]',"".$tblinvoice->f('our_ref')."",$body);
    $body=str_replace('[!SERIAL_NUMBER!]',"".$tblinvoice->f('serial_number')."",$body);
    $body=str_replace('[!OGM!]',"".$tblinvoice->f('ogm')."",$body);

    if($identity_id){
      $identity_logo = $this->dbu->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$identity_id."'");
      if($identity_logo){
        $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].$identity_logo."\" alt=\"\">",$body);
      }else{
        if($in['copy']){
         $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$body);
        }elseif($in['copy_acc']){
          $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
        }else{
          $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$body);
        }
      }
    }else{
      if($in['copy']){
       $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$body);
      }elseif($in['copy_acc']){
        $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
      }else{
        $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$body);
      }
    }
          

    if(defined('USE_INVOICE_WEB_LINK') && USE_INVOICE_WEB_LINK == 1){
      $exist_url = $this->dbu_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type  ",['d'=>DATABASE_NAME,'item_id'=>$in['invoice_id'],'type'=>'i']);

      /*$extra = "<p style=\"background: #dedede; width: 500px; text-align:center; padding-bottom: 15px;\"><br />";
      $extra .="<b>".$text_array[$e_lang]['simple']['1']."</b><br />";
      if(defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT== 1){
        $extra .= str_replace($text_array[$e_lang]['pay']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['pay']['3']."</a>", $text_array[$e_lang]['pay']['2'])."<br /></p>";
      }else{
        $extra .= str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2'])."<br /></p>";
      }*/

      $extra = "<p style=\"background: #e6e6e6; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #e6e6e6; color:#868d91; border-radius:8px;\">";

      $extra .="<b style=\"color: #5199b7; text-align:center; font-size:32px;\"><a style=\"text-decoration:none; text-transform: lowercase; color:#6399c6;\" href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b><br />";


      if(defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT== 1){
          $extra .=htmlspecialchars(utf8_decode($text_array[$e_lang]['pay']['2']))."<br /></p>";
      }else{
          $extra .=htmlspecialchars(utf8_decode($text_array[$e_lang]['simple']['2']))."<br /></p>";
      }
      // $body .=$extra;
      $body=str_replace('[!WEB_LINK!]', $extra, $body);
      $body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
      $body=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$exist_url, $body);
    }else{
      $body=str_replace('[!WEB_LINK!]', '', $body);
      $body=str_replace('[!WEB_LINK_2!]', '', $body);
      $body=str_replace('[!WEB_LINK_URL!]', '', $body);
    }
       //end for body   

    $def_email = $this->default_email();
     
    if($in['sendgrid_selected']){

      $body = '<html>'.$head.$body.'</html>';
   
       //SendGrid Send
      include_once(__DIR__."/../../misc/model/sendgrid.php");
      $sendgrid = new sendgrid($in, $this->pag, $this->field_n,$in['invoice_id'], $body,$def_email, DATABASE_NAME);

      $sendgrid_data = $sendgrid->get_sendgrid($in);

      if($sendgrid_data['error']){
        msg::error($sendgrid_data['error'],'error');
      }elseif($sendgrid_data['success']){
        msg::success(gm("Email sent by Sendgrid"),'success');
      }
      
    }else{

              /* $mail = new PHPMailer();*/ //mutat mai sus pentru embedded images
            $mail->WordWrap = 50;
                
            if($def_email['from']['email']){
                $mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']));
              }
            if($def_email['reply']['email']){
                $mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
              }
            // $mail->set('Sender',$def_email['reply']['email']);

            $subject= stripslashes(utf8_decode($in['e_subject']));

            $mail->Subject = $subject;

            if($in['include_pdf']){
              $tmp_file_name = 'invoice_.pdf';
              if($in['attach_file_name']){
                $tmp_file_name = $in['attach_file_name'];
              }
           
               $mail->AddAttachment($tmp_file_name, $in['serial_number'].'.pdf'); // attach files/invoice-user-1234.pdf, and rename it to invoice.pdf
            }
            if($in['include_xml']){
              $in['remove_accounting_cost']=true;
              include(__DIR__.'/../controller/xml_invoice_print.php');
              $xml_tmp_file_name="eff_".DATABASE_NAME."_".$in['serial_number'].".xml";
              
              $mail->AddAttachment($xml_tmp_file_name, "eff_".str_replace(array('.',' ','-','_'), '', ACCOUNT_VAT_NUMBER)."_".substr(str_replace(array('.',' ','-','_','"'), '', ACCOUNT_COMPANY),0,10)."_".$in['serial_number']."_".str_replace(array('.',' ','-','_'), '', $seller_bwt_nr)."_".substr(str_replace(array('.',' ','-','_','"'), '', $b_name),0,10).".xml");
          
            }
            /*if($in['file_id']){
              foreach ($in['file_id'] as $key => $value) {
                $mime = mime_content_type($in['file_path'][$key].$in['file_name'][$key]);
                $mail->AddAttachment($in['file_path'][$key].$in['file_name'][$key], $in['file_name'][$key]); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
              }
            }*/
            if($in['files']){
              foreach ($in['files'] as $key => $value) {
                if($value['checked'] == 1){
                  $mime = mime_content_type($value['path'].$value['file']);
                  $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
                }
              }
            }

            if($in['dropbox_files']){
                  $path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
                  if(!file_exists($path_dropbox)){
                    mkdir($path_dropbox,0775,true);
                  }
                  $dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

                  foreach ($in['dropbox_files'] as $key => $value) {
                    if($value['checked'] == 1){
                      $value['file'] = str_replace('/', '_', $value['file']);

                      $file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
                  
                  
                      $mime = mime_content_type($path_dropbox.$value['file']);
                      $mail->AddAttachment($path_dropbox.$value['file'], $value['file']);
                    }
                  }
            }

              $mail->IsHTML(true);
              $mail->Body    = '<html>'.$head.$body.'</html>';

         /*       if($in['copy']){
                    //$u = $this->dbu_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
                    $u = $this->dbu_users->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
                    if($u->f('email')){
                      $emails .=$u->f('email').', ';
                      $mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
                    }
                    // $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
                }

                if($in['copy_acc']){
                    $u = $this->dbu_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$acc_manager_id."' ");
                    if($u->f('email')){
                      $emails .=$u->f('email').', ';
                      $mail->AddBCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
                    }
                    // $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
                }*/
        /*
                if($in['invoice_email']){
                  $emails .= $in['invoice_email'].', ';
                  $mail->AddAddress($in['invoice_email'],$in['invoice_email']);
                }*/

            /*if($in['send_to']){
              $emails .= $in['send_to'].', ';
              $mail->AddAddress($in['send_to']);
            }

            if($in['contact_id']){
              $contact = $this->dbu->query("SELECT email, lastname, firstname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
              if($contact->next()){
                  $emails .= $contact->f('email').', ';
                $mail->AddAddress($contact->f('email'),$contact->f('lastname').' '.$contact->f('firstname'));
              }
            }*/
            $mail->AddAddress(trim($in['new_email']));
            $bcc = $this->dbu->query("SELECT * FROM default_data WHERE type='bcc_invoice_email' ");
            if($bcc->f('value')){
                $bcc_email = str_replace(' ', '', $bcc->f('value'));
                $email_arr = array();
                $email_arr = explode(";", $bcc_email);
                foreach($email_arr as $key=> $value){
                  if(trim($value)!=trim($in['new_email'])){
                    $mail->AddBCC($value);
                  }
                }
             
            }

              $mail->Send();

             //     if($mail->ErrorInfo){
                //  msg::$error_big = gm("Attachments to big.");
                //  return false;
                // }

              if($mail->IsError()){
                msg::error ($mail->ErrorInfo,'error');
                json_out($in);
                return false;
              }

           /*   if($mail->ErrorInfo){
                msg::notice($mail->ErrorInfo,'notice');
              }*/

              msg::success(gm("Email sent"),'success');

            if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
              update_log_message($in['logging_id'],' '.$in['new_email']);
            }else{
              $in['logging_id'] = insert_message_log($this->pag,'{l}Invoice was sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to:{endl} '.$in['new_email'],$this->field_n,$in['invoice_id']);
            }

    } //end else sendgrid

      foreach($xxx as $location => $value){
        unlink($location);
      }
      if($in['attach_file_name']){
        unlink($in['attach_file_name']);
      }
      if($in['dropbox_files']){
        $path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
        
            foreach ($in['dropbox_files'] as $key => $value) {
              if($value['checked'] == 1){
                $value['file'] = str_replace('/', '_', $value['file']);
                unlink($path_dropbox.$value['file']);
              }
            }
        }
      if($xml_tmp_file_name){
        unlink($xml_tmp_file_name);
      }
    

    if($in['mark_as_sent']=='1'){
      $sent_date= time();

      $this->dbu->query("UPDATE tblinvoice SET sent='1',sent_date='".$sent_date."' WHERE id='".$in['invoice_id']."'");
      /*$p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($p){
        $this->dbu->query("UPDATE tblinvoice SET sent='1', sent_date='".$sent_date."' WHERE id='".$p."' ");
      }*/
      $p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($p){
        $proformaInv = $this->dbu->query("SELECT req_payment,status,paid FROM tblinvoice WHERE id='".$p."' ");
        $this->dbu->query("UPDATE tblinvoice SET sent='1', sent_date='".$sent_date."' WHERE id='".$p."' ");
        $status = $proformaInv->f('status');
        if($proformaInv->f('req_payment')==100){
          $paid=$proformaInv->f('paid');
        }else{
          $paid=2;
          $status=0;
        }
        $this->dbu->query("UPDATE tblinvoice SET status='".$status."', paid='".$paid."' WHERE id='".$in['invoice_id']."' ");
        $in['inv_status'] = $status;
        $in['inv_paid'] = $paid;
      }
      $in['send_time'] = date(ACCOUNT_DATE_FORMAT,$sent_date);
    }
    if($in['typee'] == '2' && $in['c_invoice_id']){
      //$this->dbu->query("UPDATE tblinvoice SET status='1', paid='1' WHERE id='".$in['invoice_id']."'");
      //removed on task 4056
   /*   $amount_c = $this->dbu->query("SELECT amount, amount_vat FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      $amount = $this->dbu->field("SELECT amount FROM tblinvoice WHERE id='".$in['c_invoice_id']."' ");
      $info = '<a href="index.php?do=invoice-invoice&invoice_id='.$in['invoice_id'].'" >'.gm('Credit Invoice').'</a>';
      $payment_date = date('Y-m-d');
      $negative = 1;
      if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
        $negative = -1;
      }
      if($negative*$amount_c->f('amount') >= $amount){
        $this->dbu->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$in['c_invoice_id']."' ");

      }else{
        $this->dbu->query("UPDATE tblinvoice SET paid='2' WHERE id='".$in['c_invoice_id']."' ");
      }
      $credit_attached=$this->dbu->field("SELECT payment_id FROM tblinvoice_payments WHERE credit_payment='1' AND invoice_id  = '".$in['c_invoice_id']."' ");
      if(!$credit_attached){
        $this->dbu->query("INSERT INTO tblinvoice_payments SET
                            invoice_id  = '".$in['c_invoice_id']."',
                            date    = '".$payment_date."',
                            amount    = '".$amount_c->f('amount_vat')."',
                            info        = '".$info."',
                            credit_payment='1'  ");
      }*/
      /*if($in['many_email_addresses']!='1'){
        $this->dbu->query("INSERT INTO tblinvoice_payments SET
                            invoice_id  = '".$in['c_invoice_id']."',
                            date    = '".$payment_date."',
                            amount    = '".$amount_c->f('amount_vat')."',
                            info        = '".$info."',
                            credit_payment='1'  ");
      }*/
    }

    
    $yuki_active=$this->dbu->field("SELECT active FROM apps WHERE name='Yuki' AND main_app_id='0' ");
    $coda_active=$this->dbu->field("SELECT active FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
    $clearfacts_active=$this->dbu->field("SELECT active FROM apps WHERE name='Clearfacts' AND main_app_id='0' "); 
    $billtobox_active=$this->dbu->field("SELECT active FROM apps WHERE name='BillToBox' AND main_app_id='0' "); 
    $btb_active=$this->dbu->field("SELECT active FROM apps WHERE name='WinBooks' AND main_app_id='0' ");  
     $invoice_type = $t;

    $accountant_settings=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
    if($accountant_settings=='1' && $invoice_type!='1'){
       $acc_app_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_APP_ID' ");
       $acc_block_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
       $acc_export_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
      if($acc_block_id=='2'){
        $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
      }
      if($acc_export_id=='2' && $acc_app_id){
        if($acc_app_id=='1'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
          if(!$is_exported && $yuki_active=='1'){
            include(__DIR__.'/yuki.php');
            $yuki_obj=new yuki($in);
            $in['from_acc']=1;
            $yuki_obj->exportYuki($in);
            if($acc_block_id=='1'){
              $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
            }
          }
        }else if($acc_app_id=='2'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
          $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
          $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
          if(!$is_exported && $coda_active=='1' && (is_null($codabox_sales) || $codabox_sales=='1')){
            include(__DIR__.'/btb.php');
            //$btb_obj=new btb();
            $btb_obj=new btb($in);
            $in['from_acc']=1;
            $btb_obj->exportCoda($in);
            if($acc_block_id=='1'){
              $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
            }
          }
        }else if($acc_app_id=='3'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
          if(!$is_exported && $clearfacts_active=='1'){
            include(__DIR__.'/clearfacts.php');
            $clearfacts_obj=new clearfacts($in);
            $in['from_acc']=1;
            $clearfacts_obj->exportFacts($in);
            if($acc_block_id=='1'){
              $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
            }
          }
        }else if($acc_app_id=='4'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
          if(!$is_exported && $billtobox_active=='1'){
            include(__DIR__.'/billtobox.php');
            $billtobox_obj=new billtobox($in);
            $in['from_acc']=1;
            $billtobox_obj->exportBillToBox($in);
            if($acc_block_id=='1'){
              $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
            }
          }
        }
      }else if($acc_export_id=='2' && !$acc_app_id){
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
        if(!$is_exported && $yuki_active=='1'){
          include(__DIR__.'/yuki.php');
          $yuki_obj=new yuki($in);
          $in['from_acc']=1;
          $yuki_obj->exportYuki($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
        $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
        $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
        if(!$is_exported && $coda_active=='1' && (is_null($codabox_sales) || $codabox_sales=='1')){
          include(__DIR__.'/btb.php');
          //$btb_obj=new btb();
          $btb_obj=new btb($in);
          $in['from_acc']=1;
          $btb_obj->exportCoda($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
        if(!$is_exported && $clearfacts_active=='1'){
          include(__DIR__.'/clearfacts.php');
          $clearfacts_obj=new clearfacts($in);
          $in['from_acc']=1;
          $clearfacts_obj->exportFacts($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }       
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
        if(!$is_exported && $billtobox_active=='1'){
          include(__DIR__.'/billtobox.php');
          $billtobox_obj=new billtobox($in);
          $in['from_acc']=1;
          $billtobox_obj->exportBillToBox($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type='0'");
        if(!$is_exported && $btb_active=='1'){
          include(__DIR__.'/btb.php');
          $btb_obj=new btb($in);
          $in['from_acc']=1;
          $btb_obj->exportBtb($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }        
      }else if(!$acc_export_id){
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
        if(!$is_exported && $yuki_active=='1' && defined('EXPORT_AUTO_INVOICE_YUKI') && EXPORT_AUTO_INVOICE_YUKI=='2'){
          include(__DIR__.'/yuki.php');
          $yuki_obj=new yuki($in);
          $in['from_acc']=1;
          $yuki_obj->exportYuki($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
        $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
        $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
        if(!$is_exported && $coda_active=='1' && (is_null($codabox_sales) || $codabox_sales=='1') && defined('EXPORT_AUTO_INVOICE_CODABOX') && EXPORT_AUTO_INVOICE_CODABOX=='2'){
          include(__DIR__.'/btb.php');
          //$btb_obj=new btb();
          $btb_obj=new btb($in);
          $in['from_acc']=1;
          $btb_obj->exportCoda($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
        if(!$is_exported && $clearfacts_active=='1' && defined('EXPORT_AUTO_INVOICE_CLEARFACTS') && EXPORT_AUTO_INVOICE_CLEARFACTS=='2'){
          include(__DIR__.'/clearfacts.php');
          $clearfacts_obj=new clearfacts($in);
          $in['from_acc']=1;
          $clearfacts_obj->exportFacts($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
        if(!$is_exported && $billtobox_active=='1' && defined('EXPORT_AUTO_INVOICE_BILLTOBOX') && EXPORT_AUTO_INVOICE_BILLTOBOX=='2'){
          include(__DIR__.'/billtobox.php');
          $billtobox_obj=new billtobox($in);
          $in['from_acc']=1;
          $billtobox_obj->exportBillToBox($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type='0'");
        if(!$is_exported && $btb_active=='1' && defined('EXPORT_AUTO_INVOICE_EFFF') && EXPORT_AUTO_INVOICE_EFFF=='2'){
          include(__DIR__.'/btb.php');
          $btb_obj=new btb($in);
          $in['from_acc']=1;
          $btb_obj->exportBtb($in);
          if($acc_block_id=='1'){
            $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
          }
        }
      }
    }else if($invoice_type != '1'){
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type='0'");
      if(!$is_exported && $yuki_active=='1' && defined('EXPORT_AUTO_INVOICE_YUKI') && EXPORT_AUTO_INVOICE_YUKI=='2'){
        include(__DIR__.'/yuki.php');
        $yuki_obj=new yuki($in);
        $in['from_acc']=1;
        $yuki_obj->exportYuki($in);
      }
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND codabox='1' AND type='0'");
      $coda_app_id=$this->dbu->field("SELECT app_id FROM apps WHERE name='CodaBox' AND main_app_id='0' ");
      $codabox_sales=$this->dbu->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$coda_app_id."' ");
      if(!$is_exported && $coda_active=='1' && (is_null($codabox_sales) || $codabox_sales=='1') && defined('EXPORT_AUTO_INVOICE_CODABOX') && EXPORT_AUTO_INVOICE_CODABOX=='2'){
        include(__DIR__.'/btb.php');
        //$btb_obj=new btb();
        $btb_obj=new btb($in);
        $in['from_acc']=1;
        $btb_obj->exportCoda($in);
      }
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type='0'");
      if(!$is_exported && $clearfacts_active=='1' && defined('EXPORT_AUTO_INVOICE_CLEARFACTS') && EXPORT_AUTO_INVOICE_CLEARFACTS=='2'){
        include(__DIR__.'/clearfacts.php');
        $clearfacts_obj=new clearfacts($in);
        $in['from_acc']=1;
        $clearfacts_obj->exportFacts($in);
      }
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
      if(!$is_exported && $billtobox_active=='1' && defined('EXPORT_AUTO_INVOICE_BILLTOBOX') && EXPORT_AUTO_INVOICE_BILLTOBOX=='2'){
        include(__DIR__.'/billtobox.php');
        $billtobox_obj=new billtobox($in);
        $in['from_acc']=1;
        $billtobox_obj->exportBillToBox($in);
      }
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type='0'");
      if(!$is_exported && $btb_active=='1' && defined('EXPORT_AUTO_INVOICE_EFFF') && EXPORT_AUTO_INVOICE_EFFF=='2'){
        include(__DIR__.'/btb.php');
        $btb_obj=new btb($in);
        $in['from_acc']=1;
        $btb_obj->exportBtb($in);
        if($acc_block_id=='1'){
          $this->dbu->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
        }
      }
    }      

    return true;
  }

  /****************************************************************
  * function sendNewEmailValidate(&$in)                                *
  ****************************************************************/
  function sendNewEmailValidate(&$in)
  {
    $v = new validation($in);

    $in['email'] = trim($in['email']);
    $v->field('email', 'Email', 'email');
    return $v->run();
  }


  /**
   * return the default email address
   *
   * @return array
   * @author
   **/
  function default_email()
  {
    $array = array();
    $array['from']['name'] = ACCOUNT_COMPANY;
    $array['from']['email'] = 'noreply@akti.com';
    $array['reply']['name'] = ACCOUNT_COMPANY;
    $array['reply']['email'] = ACCOUNT_EMAIL;
    $array['bcc']['name'] = '';
    $array['bcc']['email'] = '';
    /*if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
      $array['from']['email'] = ACCOUNT_EMAIL;
    }*/
    if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
      if(MAIL_SETTINGS_PREFERRED_OPTION==2){
        if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
          $array['from']['email'] = MAIL_SETTINGS_EMAIL;
        }
      }else{
        if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
          $array['from']['email'] = ACCOUNT_EMAIL;
        }
      }
    }
    $this->dbu->query("SELECT * FROM default_data WHERE type='invoice_email' ");
    //if($this->dbu->move_next()){
    if($this->dbu->move_next() && $this->dbu->f('value') && $this->dbu->f('default_name')){
      $array['reply']['name'] = $this->dbu->f('default_name');
      $array['reply']['email'] = $this->dbu->f('value');
      $array['from']['name'] = $this->dbu->f('default_name');
      if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)  ) && $this->dbu->f('value')!=''){
        $array['from']['email'] = $this->dbu->f('value');
      }
    }

    //bcc email
    $this->dbu->query("SELECT * FROM default_data WHERE type='bcc_invoice_email' ");
    if($this->dbu->move_next() && $this->dbu->f('value')){
      $array['bcc']['email'] = $this->dbu->f('value'); // pot fi valori multiple, delimitate de ;
    }
    return $array;
  }

  /****************************************************************
  * function dradt(&$in)                                          *
  ****************************************************************/
  function draft(&$in){
    if(!$in['sent']){ $in['sent'] = 0; }
    // , sent_date=''

    /*$debt_id=$this->dbu->field("SELECT bPaid_debt_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
    if($debt_id != ''){
          $payments_info = $this->dbu->query("SELECT bPaid_payment_id FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' ORDER BY payment_id ASC ")->getAll();
          foreach($payments_info as $key=>$value){
            if($value['bPaid_payment_id'] != ''){
              $this->bPaid_payment($value,'','',2);
            }
          }
          $this->bPaid_debt($in['invoice_id'],2);
    }*/

    // $this->dbu->query("DELETE FROM tblinvoice_payments WHERE invoice_id = '".$in['invoice_id']."'");
    // $this->dbu->query("UPDATE tblinvoice SET status='0', sent='".$in['sent']."',paid='0',bPaid_debt_id='' WHERE id='".$in['invoice_id']."' ");

    $this->dbu->query("UPDATE tblinvoice SET sent='".$in['sent']."' WHERE id='".$in['invoice_id']."' ");

    msg::success(gm('Invoice has been returned to draft'),'success');
    insert_message_log($this->pag,'{l}Invoice has been returned to draft{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
    $in['pagl'] = $this->pag;
   /* if($in['type'] == 2){
      $this->dbu->query("DELETE FROM tblinvoice_payments WHERE invoice_id = '".$in['credit_invoice_id']."' AND credit_payment='1' ");
      $payments = $this->dbu->field("SELECT count(payment_id) FROM tblinvoice_payments WHERE invoice_id = '".$in['credit_invoice_id']."' ");
      if($payments > 0){
        $amount = $this->dbu->field("SELECT amount FROM tblinvoice WHERE id='".$in['credit_invoice_id']."' ");
        $amount_p = $this->dbu->field("SELECT SUM(amount) FROM tblinvoice_payments WHERE invoice_id='".$in['credit_invoice_id']."' ");
        if($amount_p < $amount){
          $this->dbu->query("UPDATE tblinvoice SET paid='2',status='0' WHERE id='".$in['credit_invoice_id']."' ");
        }
      }else{
        $this->dbu->query("UPDATE tblinvoice SET status='0',paid='0' WHERE id='".$in['credit_invoice_id']."' ");
      }
    }*/
    // $this->external_id_rm($in);

    # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);
    # for pdf
    return true;
  }

    /****************************************************************
  * function sent(&$in)                                          *
  ****************************************************************/
  function sent(&$in){
  //used, for now, only for set back to sent status of balanced credit invoices instead of back to draft
    if(!$in['sent']){ $in['sent'] = 1; }

    $debt_id=$this->dbu->field("SELECT bPaid_debt_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
    if($debt_id != ''){
          $payments_info = $this->dbu->query("SELECT bPaid_payment_id FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' ORDER BY payment_id ASC ")->getAll();
          foreach($payments_info as $key=>$value){
            if($value['bPaid_payment_id'] != ''){
              $this->bPaid_payment($value,'','',2);
            }
          }
          $this->bPaid_debt($in['invoice_id'],2);
    }

    $this->dbu->query("DELETE FROM tblinvoice_payments WHERE invoice_id = '".$in['invoice_id']."'");
    $this->dbu->query("UPDATE tblinvoice SET status='0', sent='".$in['sent']."',paid='0',bPaid_debt_id='' WHERE id='".$in['invoice_id']."' ");
    msg::success(gm('Invoice has been returned to sent'),'success');
    insert_message_log($this->pag,'{l}Invoice has been returned to sent{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
    $in['pagl'] = $this->pag;
    /*if($in['credit_invoice_id']){
          if($in['type'] == 2){
            $this->dbu->query("DELETE FROM tblinvoice_payments WHERE invoice_id = '".$in['credit_invoice_id']."' AND credit_payment='1' ");
            $payments = $this->dbu->field("SELECT count(payment_id) FROM tblinvoice_payments WHERE invoice_id = '".$in['credit_invoice_id']."' ");
            if($payments > 0){
              $amount = $this->dbu->field("SELECT amount FROM tblinvoice WHERE id='".$in['credit_invoice_id']."' ");
              $amount_p = $this->dbu->field("SELECT SUM(amount) FROM tblinvoice_payments WHERE invoice_id='".$in['credit_invoice_id']."' ");
              if($amount_p < $amount){
                $this->dbu->query("UPDATE tblinvoice SET paid='2',status='0' WHERE id='".$in['credit_invoice_id']."' ");
              }
            }else{
              $this->dbu->query("UPDATE tblinvoice SET status='0',paid='0' WHERE id='".$in['credit_invoice_id']."' ");
            }
          }
    }*/



    # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);
    # for pdf
    return true;
  }

  /****************************************************************
  * function add_nr(&$in)                                         *
  ****************************************************************/
  function add_nr(&$in){
    if(!$this->add_nr_validate($in))
    {
      return false;
    }
    $this->dbu->query("SELECT req_payment,status,paid, buyer_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
    $this->dbu->move_next();
    $status = $this->dbu->f('status');
    $buyer_id = $this->dbu->f('buyer_id');
    $invoice_date=time();
    if($this->dbu->f('req_payment')==100){
      $paid=$this->dbu->f('paid');
    }else{
      $paid=2;
      $status=0;
    }

    if($in['is_proforma']){
      $paid=0;
      $status=0;
    }
    //copy the existing invoice into a new one
    $this->dbu->query("CREATE TEMPORARY TABLE tmptable_1 SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."'");
    $this->dbu->query("UPDATE tmptable_1 SET id = NULL,proforma_id = '".$in['invoice_id']."',invoice_date='".$invoice_date."'");
    $in['new_invoice_id']= $this->dbu->insert("INSERT INTO tblinvoice SELECT * FROM tmptable_1");
    $this->dbu->query("DROP TEMPORARY TABLE IF EXISTS tmptable_1");

    //copy lines
    $this->dbu->query("CREATE TEMPORARY TABLE tmptable_1 SELECT * FROM tblinvoice_line WHERE invoice_id  = '".$in['invoice_id']."'");
    $this->dbu->query("UPDATE tmptable_1 SET id = NULL,invoice_id  = '".$in['new_invoice_id']."'");
    $this->dbu->insert("INSERT INTO tblinvoice_line SELECT * FROM tmptable_1");
    $this->dbu->query("DROP TEMPORARY TABLE IF EXISTS tmptable_1");

    // copy notes
    $this->dbu->query("CREATE TEMPORARY TABLE tmptable_1 SELECT * FROM note_fields WHERE item_id  = '".$in['invoice_id']."'");
    $this->dbu->query("UPDATE tmptable_1 SET note_fields_id = NULL,item_id  = '".$in['new_invoice_id']."'");
    $this->dbu->insert("INSERT INTO note_fields SELECT * FROM tmptable_1");
    $this->dbu->query("DROP TEMPORARY TABLE IF EXISTS tmptable_1");

    $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
      $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
      if($buyer_id) {
        $payment_term = $this->db->field("SELECT payment_term FROM customers WHERE customer_id = '".$buyer_id."' ");
        $payment_term_type = $this->db->field("SELECT payment_term_type FROM customers WHERE customer_id = '".$buyer_id."' ");
      }

    if($payment_term_type==1) { # invoice date
      $due_date =  $invoice_date + ( $payment_term * ( 60*60*24 )-1);
    } else { # next month first day
      /*$curMonth = date('n', $invoice_date);
      $curYear  = date('Y', $invoice_date);
          $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
      $due_date = $firstDayNextMonth + ($payment_term * ( 60*60*24 )-1);*/
      $tmstmp_month=$invoice_date + ( $payment_term * ( 60*60*24 )-1);
      $lastday = date('t',$tmstmp_month);
      $due_date = mktime(23, 59, 59,date('m',$tmstmp_month), $lastday,date('Y',$tmstmp_month));
    }

    $created = date(ACCOUNT_DATE_FORMAT,$invoice_date);

    $this->dbu->query("UPDATE tblinvoice SET serial_number='".$in['serial_number']."',type=0,req_payment=100,paid='".$paid."',status='0',sent='0',sent_date='',f_archived='0', due_date='".$due_date."', due_date_vat='".$invoice_date."', created ='".$created."', last_upd='".$created."' WHERE id='".$in['new_invoice_id']."' and type=1");
    
    // Add OGM to invoice created from Proforma invoice if the setting ogm is activated
    $DRAFT_INVOICE_NO_NUMBER = $this->dbu->field("SELECT value FROM settings WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
    if($DRAFT_INVOICE_NO_NUMBER!='1' && $in['type'] == 1){
      $ogm = generate_ogm(DATABASE_NAME,$in['new_invoice_id']);
      $this->dbu->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$in['new_invoice_id']."' ");
    }
    
    $this->dbu->query("UPDATE tblinvoice SET f_archived='3' WHERE id='".$in['invoice_id']."' ");
    msg::success(gm('Invoice has been successfully created'),'success');

    $tracking_trace=$this->dbu->query("SELECT * FROM tblinvoice WHERE id='".$in['new_invoice_id']."' ");
    $tracking_data=array(
            'target_id'         => $in['new_invoice_id'],
            'target_type'       => '1',
            'target_buyer_id'   => $tracking_trace->f('buyer_id'),
            'lines'             => array(
                                          array('origin_id'=>$in['invoice_id'],'origin_type'=>'7')
                                    )
    );
    addTracking($tracking_data);

    $ord_nr = $this->dbu->query("SELECT pim_orders.order_id FROM tracking
              INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
              INNER JOIN pim_orders ON tracking_line.origin_id = pim_orders.order_id AND tracking_line.origin_type ='4'
              WHERE tracking.target_id = '".$in['invoice_id']."' AND tracking.target_type='7' ")->getAll();
    if(!empty($ord_nr)){
      //$this->dbu->query("UPDATE pim_orders SET invoiced='1',rdy_invoice='1' WHERE order_id='".$ord_nr[0]['order_id']."' ");
      $this->dbu->query("UPDATE pim_orders SET invoiced='1' WHERE order_id='".$ord_nr[0]['order_id']."' ");
    }

    insert_message_log($this->pag,'{l}Invoice has been successfully created by {endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['new_invoice_id']);
    $in['invoice_id']=$in['new_invoice_id'];
    $in['pagl'] = $this->pag;

    return true;
  }
    /****************************************************************
     * function add_nr_purchase(&$in)                                *
     ****************************************************************/
    function add_nr_purchase(&$in)
    {
        $invoice_date=time();
        //copy the existing invoice into a new one
        $this->dbu->query("CREATE TEMPORARY TABLE tmptable_1 SELECT * FROM tblinvoice_incomming WHERE invoice_id = '".$in['id']."'");
        $this->dbu->query("UPDATE tmptable_1 SET invoice_id = NULL, invoice_number='', booking_number = '".$in['booking_number_new']."',invoice_date='".$invoice_date."'");
        $in['new_invoice_id']= $this->dbu->insert("INSERT INTO tblinvoice_incomming SELECT * FROM tmptable_1");
        $this->dbu->query("DROP TEMPORARY TABLE IF EXISTS tmptable_1");


        //copy lines
        $this->dbu->query("CREATE TEMPORARY TABLE tmptable_1 SELECT * FROM tblinvoice_incomming_line WHERE invoice_id  = '".$in['id']."'");
        $this->dbu->query("UPDATE tmptable_1 SET line_id = NULL, invoice_id  = '".$in['new_invoice_id']."'");
        $this->dbu->insert("INSERT INTO tblinvoice_incomming_line SELECT * FROM tmptable_1");
        $this->dbu->query("DROP TEMPORARY TABLE IF EXISTS tmptable_1");

        // copy notes
        $this->dbu->query("CREATE TEMPORARY TABLE tmptable_1 SELECT * FROM note_fields WHERE item_id  = '".$in['id']."'");
        $this->dbu->query("UPDATE tmptable_1 SET note_fields_id = NULL,item_id  = '".$in['new_invoice_id']."'");
        $this->dbu->insert("INSERT INTO note_fields SELECT * FROM tmptable_1");
        $this->dbu->query("DROP TEMPORARY TABLE IF EXISTS tmptable_1");

        $this->dbu->query("UPDATE tblinvoice_incomming SET status=0, approved_by=0, booking_number='".$in['booking_number_new']."',type=0 WHERE invoice_id='".$in['new_invoice_id']."' and type=1");
        $this->dbu->query("UPDATE tblinvoice_incomming SET f_archived='1' WHERE invoice_id='".$in['id']."' ");
        msg::success(gm('Invoice has been successfully created'),'success');
       // insert_message_log('invoice','{l}Invoice was automatically created by{endl} <a href="[SITE_URL]recinvoice/view/'.$in['recurring_invoice_id'].'">{l}Recurring Invoice{endl}</a>','invoice_id',$in['created_invoice_id'],0,'', false, 0, $log_date);
        insert_message_log('purchase_invoice','{l}Invoice ('.$in['booking_number_new'].') was created from {endl} <a class="text-link" href="[SITE_URL]purchase_invoice/view/'.$in['id'].'">{l}Pro forma invoice {endl}</a> {l} by {endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['new_invoice_id']);
        $in['invoice_id']=$in['new_invoice_id'];
        $in['pagl'] = 'purchase_invoice';

        return true;
    }

  /****************************************************************
  * function add_nr_validate(&$in)                                *
  ****************************************************************/
  function add_nr_validate(&$in)
  {
    $v = new validation($in);
    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));

    $v->field('serial_number', 'Invoice Nr', 'unique[tblinvoice.serial_number]',gm('Invoice Nr').'*:'.gm('is already in use').'.<br />');
    return $v->run();
  }

  /****************************************************************
* function pdf_settings(&$in)                                          *
****************************************************************/
function pdf_settings(&$in)
{

  if(!$in['logo']){
    $in['logo'] = ACCOUNT_LOGO;
  }
  if(!$in['logo']){
    $in['logo'] = 'images/no-logo.png';
  }
  if(!($in['invoice_id']&&$in['pdf_layout']&&$in['logo']))
    {
      return false;
    }
  $use_custom_template = 0;
  if($in['pdf_layout']>7){
    $in['pdf_layout'] = $in['pdf_layout']-7;
    $use_custom_template = 1;
  }
  // echo $in['pdf_layout'];
  // echo $use_custom_template;
  // echo "UPDATE tblinvoice SET pdf_layout='".$in['pdf_layout']."', use_custom_template = '".$use_custom_template."' WHERE id = '".$in['invoice_id']."'";
  // exit();
  $invoice_type = $this->dbu->field("SELECT type FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
  if ($invoice_type =='2'){
      if(defined('USE_CUSTOME_INVOICE_CREDIT_PDF') && USE_CUSTOME_INVOICE_CREDIT_PDF == 1 ){
        $use_custom_template = 1;
          $this->dbu->query("UPDATE tblinvoice SET pdf_layout='0', use_custom_template = '".$use_custom_template."' WHERE id = '".$in['invoice_id']."' ");
        }else{
          $this->dbu->query("UPDATE tblinvoice SET pdf_layout='".$in['pdf_layout']."', use_custom_template = '".$use_custom_template."' WHERE id = '".$in['invoice_id']."' ");
        }
  }else{
      if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 ){
        $use_custom_template = 1;
          $this->dbu->query("UPDATE tblinvoice SET pdf_layout='0', use_custom_template = '".$use_custom_template."' WHERE id = '".$in['invoice_id']."' ");
        }else{
          $this->dbu->query("UPDATE tblinvoice SET pdf_layout='".$in['pdf_layout']."', use_custom_template = '".$use_custom_template."' WHERE id = '".$in['invoice_id']."' ");
        }
  }


  $this->dbu->query("UPDATE tblinvoice SET pdf_logo='".$in['logo']."' WHERE id = '".$in['invoice_id']."' ");
  $this->dbu->query("UPDATE tblinvoice SET identity_id='".$in['identity_id']."' WHERE id = '".$in['invoice_id']."' ");


  # for pdf
  $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
  $params = array();
  $params['use_custom'] = 0;
  if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
    $params['logo'] = $inv->f('pdf_logo');
    $params['type']=$inv->f('pdf_layout');
    $params['logo']=$inv->f('pdf_logo');
    $params['template_type'] = $inv->f('pdf_layout');
  }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
    $params['custom_type']=$inv->f('pdf_layout');
    unset($params['type']);
    $params['logo']=$inv->f('pdf_logo');
    $params['template_type'] = $inv->f('pdf_layout');
    $params['use_custom'] = 1;
  }else{
    $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
    $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
  }
  #if we are using a customer pdf template
if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
    if($inv->f('type')==1 || $inv->f('type')==3 ){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
    }
    unset($params['type']);
  }
  if(defined('USE_CUSTOME_INVOICE_CREDIT_PDF') && USE_CUSTOME_INVOICE_CREDIT_PDF == 1 && $inv->f('pdf_layout') == 0){
    if($inv->f('type')==2 || $inv->f('type')==4){
      $params['custom_type']=ACCOUNT_INVOICE_CREDIT_PDF_FORMAT;
    }
    unset($params['type']);
  }
  $params['id'] = $in['invoice_id'];
  $params['type_pdf'] = $in['type'];
  $params['lid'] = $inv->f('email_language');
  $params['save_as'] = 'F';
  $this->generate_pdf($params);
  # for pdf

    msg::success(gm("Pdf settings have been updated"),'success');
    return true;
}

  function updateCustomerData(&$in)
  {

    if($in['field'] == 'contact_id'){
      $in['buyer_id'] ='';
    }
    if($in['invoice_id']){
      $buyer_exist = $this->dbu->field("SELECT buyer_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
      if($buyer_exist!=$in['buyer_id']){
        $in['save_final']=1;
      }elseif($buyer_exist==$in['buyer_id']){
        $in['save_final']=0;
      }
    }/*elseif($in['duplicate_invoice_id']){
      $buyer_exist = $this->dbu->field("SELECT buyer_id FROM tblinvoice WHERE id='".$in['duplicate_invoice_id']."'");
      if($buyer_exist!=$in['buyer_id']){
        $in['save_final']=1;
      }elseif($buyer_exist==$in['buyer_id']){
        $in['save_final']=0;
      }
    }*/

    $sql = "UPDATE tblinvoice SET ";

    if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.comp_fax,customers.name, customers.no_vat, customers.btw_nr,
                  customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, customers.vat_regime_id,
                  customer_addresses.address_id, customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount, customers.cat_id
                  FROM customers
                  INNER JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.billing=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
      if(!$buyer_info->next()){
        $buyer_info = $this->dbu->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.comp_fax, customers.name, customers.no_vat, customers.btw_nr,
                  customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, customers.vat_regime_id,
                  customer_addresses.address_id, customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount, customers.cat_id
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
      }

      if($in['main_address_id']){
        $is_customer_address=$this->dbu->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['main_address_id']."' AND customer_id='".$in['buyer_id']."' ");
        if(!$is_customer_address){
          unset($in['main_address_id']);
        }
      }
      

      /*if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
        $apply_discount = "3";
      }else{
        if($buyer_info->f('apply_fix_disc')){
          $apply_discount = "2";
        }
        if($buyer_info->f('apply_line_disc')){
          $apply_discount = "1";
        }
      }*/
      $apply_discount =INVOICE_APPLY_DISCOUNT;
      /*if(!$buyer_info->f('apply_line_disc')){
          if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
            $apply_discount = 3;
          }else{
            if($buyer_info->f('apply_fix_disc')){
              $apply_discount = 2;
            }
            if($buyer_info->f('apply_line_disc')){
              $apply_discount = 1;
            }
          }
      } else if($buyer_info->f('apply_line_disc')){
        $apply_discount = 1;
      }*/
      if(!$buyer_info->f('apply_line_disc')){
          if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
            $apply_discount = 3;
          }else{
            if($buyer_info->f('apply_fix_disc')){
              $apply_discount = 2;
            }
          }
      } else {
        $apply_discount = 1;
        if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
          $apply_discount = 3;
        }else{
          if($buyer_info->f('apply_fix_disc')){
            $apply_discount = 2;
          }
        }
      }
      
      $in['apply_fix_disc'] = $apply_fix_disc;

      /*if($in['invoice_id'] == 'tmp'){
        $in['delivery_address_id'] = $buyer_info->f('address_id');
      }*/
      if($in['third_party_applied'] && $in['third_party_id']){
        $in['discount']=$in['discount'] ? return_value($in['discount']) : 0;
      }else{
        $in['apply_discount'] = $apply_discount;
        $in['discount']=$buyer_info->f('apply_fix_disc') ? $buyer_info->f('fixed_discount') : 0;
      }    

      $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
      $in['contact_name'] ='';

      //Set email language as account / contact language
      $emailMessageData = array('buyer_id'    => $in['buyer_id'],
                      'contact_id'    => $in['contact_id'],
                      'item_id'     => $in['invoice_id'],
                      'email_language'  => $in['email_language'],
                      'table'     => 'tblinvoice',
                      'table_label'   => 'id',
                      'table_buyer_label' => 'buyer_id',
                      'table_contact_label' => 'contact_id',
                      'param'     => 'update_customer_data');
      $in['email_language'] = get_email_language($emailMessageData);
      //End Set email language as account / contact language

      $in['identity_id'] = get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);

      $sql .= " buyer_id = '".$in['buyer_id']."', ";
      $sql .= " buyer_name = '".addslashes($buyer_info->f('name'))."', ";
      $sql .= " buyer_email = '".$buyer_info->f('c_email')."', ";
      $sql .= " buyer_phone = '".$buyer_info->f('comp_phone')."', ";
      $sql .= " buyer_fax = '".$buyer_info->f('comp_fax')."', ";
      $sql .= " main_address_id = '".($in['main_address_id'] ? $in['main_address_id'] : $buyer_info->f('address_id'))."', ";
      $sql .= " email_language = '".$in['email_language']."', ";
      // $sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
      //$sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
      $sql .= " identity_id = '".$in['identity_id']."', ";
      $sql .= " vat = '".get_customer_vat($in['buyer_id'])."', ";
      $sql .= " vat_regime_id = '".$buyer_info->f('vat_regime_id')."', ";
      $sql .= " seller_bwt_nr = '".$buyer_info->f('btw_nr')."', ";
      $sql .= " cat_id = '".$buyer_info->f('cat_id')."', ";
      $sql .= " discount = '".$in['discount']."', ";
      $sql .= " third_party_id = '".($in['third_party_applied'] && $in['has_third_party'] && $in['third_party_id'] ? $in['third_party_id'] : '0')."', ";

      if($in['contact_id']){
        $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
        // $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
        $sql .= " contact_id = '".$in['contact_id']."', ";
        $in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
      }else{
        // $sql .= " contact_name = '', ";
        $sql .= " contact_id = '', ";
      }
      $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
      if($in['main_address_id'] && $buyer_info->f('address_id') != $in['main_address_id']){
        $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
        $sql .= " buyer_country_id = '".$buyer_addr->f('country_id')."', ";
        $sql .= " buyer_city = '".addslashes($buyer_addr->f('city'))."', ";
        $sql .= " buyer_zip = '".$buyer_addr->f('zip')."', ";
        $sql .= " buyer_address = '".addslashes($buyer_addr->f('address'))."', ";
      }else{
        $sql .= " buyer_country_id = '".$buyer_info->f('country_id')."', ";
        $sql .= " buyer_city = '".addslashes($buyer_info->f('city'))."', ";
        $sql .= " buyer_zip = '".$buyer_info->f('zip')."', ";
        $sql .= " buyer_address = '".addslashes($buyer_info->f('address'))."', ";

        $in['main_address_id']=$buyer_info->f('address_id');
      }
     // if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
      /*}else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " free_field = '".addslashes($new_address_txt)."' ";
      }*/

      // $sql .= " field = 'customer_id' ";
      if($in['changePrices']==1){
        if($in['apply_discount_line']){
            $sql .= ",  apply_discount = '".$in['apply_discount_line']."' ";
            $sql .= ",  discount_line_gen = '".$in['discount_line_gen']."' ";
        } else {
           $sql .= ",  apply_discount = '".$in['apply_discount']."' ";
           // $sql .= ",  remove_vat = '".$buyer_info->f('no_vat')."' ";
           if($in['third_party_applied'] && $in['third_party_id']){
            $sql .= ",  discount_line_gen = '".($in['discount_line_gen'] ? return_value($in['discount_line_gen']) : '0')."' ";
           }else{
              $sql .= ",  discount_line_gen = '".$buyer_info->f('line_discount')."' ";
           }
            
        }
        if($in['third_party_applied'] && $in['third_party_id']){
          $sql .= ",  discount = '".($in['discount'] ? return_value($in['discount']) : '0')."' ";
        }else{
          $sql .= ",  discount = '".$buyer_info->f('fixed_discount')."' ";
        }
        
        foreach ($in['invoice_line'] as $key => $value) {
          if($value['article_id']){
            $params = array(
              'article_id' => $value['article_id'],
              'price' => return_value($value['price']),
              'quantity' => return_value($value['quantity']),
              'customer_id' => $in['buyer_id'],
              'cat_id' => $buyer_info->f('cat_id'),
              'asString' => true
            );
            $in['invoice_line'][$key]['price'] = display_number($this->getArticlePrice($params));
            $article_vat=$this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$value['article_id']."' ");
            $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article_vat."'");
      
            if($in['buyer_id']){
              if($buyer_info->f('vat_regime_id')){
                if($buyer_info->f('vat_regime_id')<10000){
                  if($buyer_info->f('vat_regime_id')==2){
                    $vat=0;
                  }
                }else{
                  $vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
                    LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
                    WHERE vat_new.id='".$buyer_info->f('vat_regime_id')."'");
                  if(!$vat_regime){
                    $vat_regime=0;
                  }
                  if($vat>$vat_regime){
                    $vat=$vat_regime;
                  }
                }     
              }    
            }
            $in['invoice_line'][$key]['vat']= $in['remove_vat'] == 1 ? display_number('0') : display_number($vat);
          }else{
            if($buyer_info->f('vat_regime_id')){
              if($buyer_info->f('vat_regime_id')<10000){
                if($buyer_info->f('vat_regime_id')==2){
                  $vat=0;
                }
              }else{
                $vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
                  LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
                  WHERE vat_new.id='".$buyer_info->f('vat_regime_id')."'");
                if(!$vat_regime){
                  $vat_regime=0;
                }
                if($vat>$vat_regime){
                  $vat=$vat_regime;
                }
              }     
            }
            $in['invoice_line'][$key]['vat']= $in['remove_vat'] == 1 ? display_number('0') : display_number($vat);
          }
        }
        if($in['invoice_id'] && is_numeric($in['invoice_id'])){
          $doRun=true;
        }
      }

    }else{
      if(!$in['contact_id']){
        msg::error ( gm('Please select a company or a contact'),'error');
        return false;
      }
      $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
      $sql .= " buyer_id = '', ";
      $sql .= " buyer_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
      // $sql .= " contact_name = '', ";
      $sql .= " contact_id = '".$in['contact_id']."', ";
      $sql .= " buyer_email = '".$contact_info->f('email')."', ";
      // $sql .= " customer_vat_number = '', ";
      $sql .= " buyer_country_id = '".$contact_address->f('country_id')."', ";
      $sql .= " buyer_city = '".addslashes($contact_address->f('city'))."', ";
      $sql .= " buyer_zip = '".$contact_address->f('zip')."', ";
      $sql .= " buyer_phone = '".$contact_info->f('phone')."', ";
      $sql .= " buyer_address = '".addslashes($contact_address->f('address'))."', ";
      $sql .= " email_language = '".DEFAULT_LANG_ID."', ";
      // $sql .= " delivery_address = '', ";
      // $sql .= " delivery_address_id = '', ";
      $sql .= " identity_id = '".$in['identity_id']."', ";
      // $sql .= " customer_ref = '', ";
      $in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));

      if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " free_field = '".addslashes($new_address_txt)."' ";
      }

      // $sql .= " field = 'contact_id' ";
      if($in['changePrices']==1){
        $sql .= ",  apply_discount = '0' ";
        // $sql .= ",  remove_vat = '0' ";
        $sql .= ",  discount_line_gen = '0' ";
        $sql .= ",  discount = '0' ";
        foreach ($in['invoice_line'] as $key => $value) {
          if($value['article_id']){
            $params = array(
              'article_id' => $value['article_id'],
              'price' => return_value($value['price']),
              'quantity' => return_value($value['quantity']),
              'cat_id' => '0',
              'asString' => true
            );
            $in['invoice_line'][$key]['price'] = display_number($this->getArticlePrice($params));
          }
        }
      }
    }
      if($in['duplicate_invoice_id']){
        $sql .=" WHERE id ='".$in['duplicate_invoice_id']."' ";
      }else{
        $sql .=" WHERE id ='".$in['item_id']."' ";
      }
/*    $sql .=" WHERE id ='".$in['item_id']."' ";*/
 /*   if(!$in['isAdd']){
      $this->dbu->query($sql);
    }*/
    if(!$in['isAdd'] && !$in['duplicate_invoice_id']){
      $this->dbu->query($sql);
      if($in['item_id'] && is_numeric($in['item_id'])){
        $trace_id=$this->db->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['item_id']."' ");
        if($trace_id && $in['buyer_id']){
          $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
        }
        if($in['third_party_applied'] && $in['has_third_party'] && $in['third_party_id']){
          $this->update_invoice_lines($in);
        }
      }         
    }

    /*$in['invoice_id'] = $in['item_id'];*/
    if($in['duplicate_invoice_id']){
      /*$in['invoice_id'] = $in['duplicate_invoice_id'];*/
    }else{
      $in['invoice_id'] = $in['item_id'];
    }

    msg::success(gm('Sync successfull.'),'success');
    if($doRun){
         $this->update_invoice_lines($in);
    }
    return true;
  }

  function updateCustomerDataRec(&$in)
  {
    if($in['field'] == 'contact_id'){
      $in['buyer_id'] ='';
    }

    $sql = "UPDATE recurring_invoice SET ";
    if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.comp_fax,customers.name, customers.no_vat, customers.btw_nr,
                  customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id,
                  customer_addresses.address_id,customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.billing=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
      $buyer_info->next();

      /*if($in['recurring_invoice_id'] == 'tmp'){
        $in['delivery_address_id'] = $buyer_info->f('address_id');
      }*/
      if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
        $apply_discount = "3";
      }else{
        if($buyer_info->f('apply_fix_disc')){
          $apply_discount = "2";
        }
        if($buyer_info->f('apply_line_disc')){
          $apply_discount = "1";
        }
      }
      $in['apply_discount'] = $apply_discount;

      $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
      $in['contact_name'] ='';

      $sql .= " buyer_id = '".$in['buyer_id']."', ";
      $sql .= " buyer_name = '".addslashes($buyer_info->f('name'))."', ";
      $sql .= " buyer_email = '".$buyer_info->f('c_email')."', ";
      $sql .= " buyer_country_id = '".$buyer_info->f('country_id')."', ";
      $sql .= " buyer_city = '".addslashes($buyer_info->f('city'))."', ";
      $sql .= " buyer_zip = '".$buyer_info->f('zip')."', ";
      $sql .= " buyer_phone = '".$buyer_info->f('comp_phone')."', ";
      $sql .= " buyer_fax = '".$buyer_info->f('comp_fax')."', ";
      $sql .= " seller_bwt_nr = '".$buyer_info->f('btw_nr')."', ";
      $sql .= " buyer_address = '".addslashes($buyer_info->f('address'))."', ";
      $sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
      $sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
      $sql .= " vat = '".get_customer_vat($in['buyer_id'])."', ";

      if($in['contact_id']){
        $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
        // $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
        $sql .= " contact_id = '".$in['contact_id']."', ";
        $in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
      }else{
        // $sql .= " contact_name = '', ";
        $sql .= " contact_id = '', ";
      }
      $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
      if($buyer_info->f('address_id') != $in['main_address_id']){
        $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
      }

      // console::log($in['delivery_address_id']);
      if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " free_field = '".addslashes($new_address_txt)."' ";
      }

      // $sql .= " field = 'customer_id' ";
      if($in['changePrices']==1){
        $sql .= ",  apply_discount = '".$in['apply_discount']."' ";
        // $sql .= ",  remove_vat = '".$buyer_info->f('no_vat')."' ";
        $sql .= ",  discount_line_gen = '".$buyer_info->f('line_discount')."' ";
        $sql .= ",  discount = '".$buyer_info->f('fixed_discount')."' ";
        foreach ($in['invoice_line'] as $key => $value) {
          if($value['article_id']){
            $params = array(
              'article_id' => $value['article_id'],
              'price' => return_value($value['price']),
              'quantity' => return_value($value['quantity']),
              'customer_id' => $in['buyer_id'],
              'cat_id' => $buyer_info->f('cat_id'),
              'asString' => true
            );
            $in['invoice_line'][$key]['price'] = display_number($this->getArticlePrice($params));
            $article_vat=$this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$value['article_id']."' ");
            $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article_vat."'");
            if($buyer_info->f('vat_regime_id')){
              if($buyer_info->f('vat_regime_id')<10000){
                if($buyer_info->f('vat_regime_id')==2){
                  $vat=0;
                }
              }else{
                $vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
                  LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
                  WHERE vat_new.id='".$buyer_info->f('vat_regime_id')."'");
                if(!$vat_regime){
                  $vat_regime=0;
                }
                if($vat>$vat_regime){
                  $vat=$vat_regime;
                }
              }     
            } 
            $in['invoice_line'][$key]['vat']= $in['remove_vat'] == 1 ? display_number('0') : display_number($vat);
          }else{
            if($buyer_info->f('vat_regime_id')){
              if($buyer_info->f('vat_regime_id')<10000){
                if($buyer_info->f('vat_regime_id')==2){
                  $vat=0;
                }
              }else{
                $vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
                  LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
                  WHERE vat_new.id='".$buyer_info->f('vat_regime_id')."'");
                if(!$vat_regime){
                  $vat_regime=0;
                }
                if($vat>$vat_regime){
                  $vat=$vat_regime;
                }
              }     
            }
            $in['invoice_line'][$key]['vat']= $in['remove_vat'] == 1 ? display_number('0') : display_number($vat);
          }
        }
      }

    }else{
      if(!$in['contact_id']){
        msg::error ( gm('Please select a company or a contact'),'error');
        return false;
      }
      $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
      $sql .= " buyer_id = '', ";
      $sql .= " buyer_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
      // $sql .= " contact_name = '', ";
      $sql .= " contact_id = '".$in['contact_id']."', ";
      $sql .= " buyer_email = '".$contact_info->f('email')."', ";
      // $sql .= " customer_vat_number = '', ";
      $sql .= " buyer_country_id = '".$contact_address->f('country_id')."', ";
      $sql .= " buyer_city = '".addslashes($contact_address->f('city'))."', ";
      $sql .= " buyer_zip = '".$contact_address->f('zip')."', ";
      $sql .= " buyer_phone = '".$contact_info->f('phone')."', ";
      $sql .= " buyer_address = '".addslashes($contact_address->f('address'))."', ";
      $sql .= " email_language = '".DEFAULT_LANG_ID."', ";
      // $sql .= " delivery_address = '', ";
      // $sql .= " delivery_address_id = '', ";
      $sql .= " identity_id = '".$in['identity_id']."', ";
      // $sql .= " customer_ref = '', ";
      $in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));

      if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " free_field = '".addslashes($new_address_txt)."' ";
      }

      // $sql .= " field = 'contact_id' ";
      if($in['changePrices']==1){
        $sql .= ",  apply_discount = '0' ";
        // $sql .= ",  remove_vat = '0' ";
        $sql .= ",  discount_line_gen = '0' ";
        $sql .= ",  discount = '0' ";
        foreach ($in['invoice_line'] as $key => $value) {
          if($value['article_id']){
            $params = array(
              'article_id' => $value['article_id'],
              'price' => return_value($value['price']),
              'quantity' => return_value($value['quantity']),
              'cat_id' => '0',
              'asString' => true
            );
            $in['invoice_line'][$key]['price'] = display_number($this->getArticlePrice($params));
          }
        }
      }
    }
    $sql .=" WHERE recurring_invoice_id ='".$in['item_id']."' ";
    if(!$in['isAdd']){
      $this->dbu->query($sql);
    }
    $in['recurring_invoice_id'] = $in['item_id'];
    msg::success(gm('Sync successfull.'),'success');
    return true;
  }

  function check_vies_vat_number(&$in){
    $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

    if(!$in['value'] || $in['value']==''){
      if(ark::$method == 'check_vies_vat_number'){
        msg::error ( gm('Error'),'error');
        json_out($in);
      }
      return false;
    }
    $value=trim($in['value']," ");
    $value=str_replace(" ","",$value);
    $value=str_replace(".","",$value);
    $value=strtoupper($value);

    $vat_numeric=is_numeric(substr($value,0,2));

    /*if($vat_numeric || substr($value,0,2)=="BE"){
      $trends_access_token=$this->dbu->field("SELECT api FROM apps WHERE type='trends_access_token' ");
      $trends_expiration=$this->dbu->field("SELECT api FROM apps WHERE type='trends_expiration' ");
      $trends_refresh_token=$this->dbu->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

      if(!$trends_access_token || $trends_expiration<time()){
        $ch = curl_init();
        $headers=array(
          "Content-Type: x-www-form-urlencoded",
          "Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
          );

        $trends_data="grant_type=password&username=akti_api&password=akti_api";

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
          curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
          curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

          $put = json_decode(curl_exec($ch));
        $info = curl_getinfo($ch);

        if($info['http_code']==400){
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( $put->error,'error');
            json_out($in);
          }
          return false;
        }else{
          if(!$trends_access_token){
            $this->dbu->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
            $this->dbu->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
            $this->dbu->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
          }else{
            $this->dbu->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
            $this->dbu->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
            $this->dbu->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
          }

          $ch = curl_init();
          $headers=array(
            "Authorization: Bearer ".$put->access_token
            );
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            if($vat_numeric){
                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
            }else{
                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
            }
            $put = json_decode(curl_exec($ch));
          $info = curl_getinfo($ch);

            if($info['http_code']==400 || $info['http_code']==429){
              if(ark::$method == 'check_vies_vat_number'){
              msg::error ( $put->error,'error');
              json_out($in);
            }
            return false;
          }else if($info['http_code']==404){
            if($vat_numeric){
              if(ark::$method == 'check_vies_vat_number'){
                msg::error ( gm("Not a valid vat number"),'error');
                json_out($in);
              }
              return false;
            }
          }else{
            if($in['customer_id']){
              $country_id=$this->dbu->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
              if($country_id != 26){
                $this->dbu->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
                $in['remove_v']=1;
              }else if($country_id == 26){
                $this->dbu->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
                $in['add_v']=1;
              }
            }
            $in['comp_name']=$put->officialName;
            $in['comp_address']=$put->street.' '.$put->houseNumber;
            $in['comp_zip']=$put->postalCode;
            $in['comp_city']=$put->city;
            $in['comp_country']='26';
            $in['trends_ok']=true;
            $in['trends_lang']=$_SESSION['l'];
            $in['full_details']=$put;
            if(ark::$method == 'check_vies_vat_number'){
              msg::success(gm('Success'),'success');
              json_out($in);
            }
            return false;
          }
        }
      }else{
        $ch = curl_init();
        $headers=array(
          "Authorization: Bearer ".$trends_access_token
          );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_TIMEOUT, 60);
          if($vat_numeric){
              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
          }else{
              curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
          }
          $put = json_decode(curl_exec($ch));
        $info = curl_getinfo($ch);

          if($info['http_code']==400 || $info['http_code']==429){
            if(ark::$method == 'check_vies_vat_number'){
            msg::error ( $put->error,'error');
            json_out($in);
          }
          return false;
        }else if($info['http_code']==404){
          if($vat_numeric){
            if(ark::$method == 'check_vies_vat_number'){
              msg::error (gm("Not a valid vat number"),'error');
              json_out($in);
            }
            return false;
          }
        }else{
          if($in['customer_id']){
            $country_id=$this->dbu->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
            if($country_id != 26){
              $this->dbu->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
              $in['remove_v']=1;
            }else if($country_id == 26){
              $this->dbu->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
              $in['add_v']=1;
            }
          }
          $in['comp_name']=$put->officialName;
          $in['comp_address']=$put->street.' '.$put->houseNumber;
          $in['comp_zip']=$put->postalCode;
          $in['comp_city']=$put->city;
          $in['comp_country']='26';
          $in['trends_ok']=true;
          $in['trends_lang']=$_SESSION['l'];
          $in['full_details']=$put;
          if(ark::$method == 'check_vies_vat_number'){
            msg::success(gm('Success'),'success');
            json_out($in);
          }
          return false;
        }
      }
    }*/


    if(!in_array(substr($value,0,2), $eu_countries)){
      $value='BE'.$value;
    }
    if(in_array(substr($value,0,2), $eu_countries)){
      $search   = array(" ", ".");
      $vat = str_replace($search, "", $value);
      $_GET['a'] = substr($vat,0,2);
      $_GET['b'] = substr($vat,2);
      $_GET['c'] = '1';
      $dd = include('../valid_vat.php');
    }else{
      if(ark::$method == 'check_vies_vat_number'){
        msg::error ( gm('Error'),'error');
        json_out($in);
      }
      return false;
    }
    if(isset($response) && $response == 'invalid'){
      if(ark::$method == 'check_vies_vat_number'){
        msg::error ( gm('Error'),'error');
        json_out($in);
      }
      return false;
    }else if(isset($response) && $response == 'error'){
      if(ark::$method == 'check_vies_vat_number'){
        msg::error ( gm('Error'),'error');
        json_out($in);
      }
      return false;
    }else if(isset($response) && $response == 'valid'){
      $full_address=explode("\n",$result->address);
      switch($result->countryCode){
        case "RO":
          $in['comp_address']=$full_address[1];
          $in['comp_city']=$full_address[0];
          $in['comp_zip']=" ";
          break;
        case "NL":
          $zip=explode(" ",$full_address[2],2);
          $in['comp_address']=$full_address[1];
          $in['comp_zip']=$zip[0];
          $in['comp_city']=$zip[1];
          break;
        default:
          $zip=explode(" ",$full_address[1],2);
          $in['comp_address']=$full_address[0];
          $in['comp_zip']=$zip[0];
          $in['comp_city']=$zip[1];
          break;
      }

      $in['comp_name']=$result->name;

      $in['comp_country']=(string)$this->dbu->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
      $in['comp_country_name']=gm($this->dbu->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
      $in['full_details']=$result;
      $in['vies_ok']=1;
      if($in['customer_id']){
        $country_id=$this->dbu->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
        if($in['comp_country'] != $country_id){
          $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
          $this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
          $in['remove_v']=1;
        }else if($in['comp_country'] == $country_id){
          $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
          $this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
          $in['add_v']=1;
        }
      }
      if(ark::$method == 'check_vies_vat_number'){
        msg::success ( gm('VAT Number is valid'),'success');
        json_out($in);
      }
      return true;
    }else{
      if(ark::$method == 'check_vies_vat_number'){
        msg::error ( gm('Error'),'error');
        json_out($in);
      }
      return false;
    }
    if(ark::$method == 'check_vies_vat_number'){
      json_out($in);
    }
  }

  /**
  * undocumented function
  *
  * @return void
  * @author PM
  **/
  function tryAddC(&$in){
    if(!$this->tryAddCValid($in)){
      json_out($in);
      return false;
    }
    if($in['app_mod']=='incomminginvoice'){
      $in['incomming_invoice_id']=$in['item_id'];
      $supplier_filter=" is_supplier='1', ";
    }else if($in['app_mod']=='recinvoice'){
      $in['recurring_invoice_id']=$in['item_id'];
      $supplier_filter="";
    } else if($in['app_mod']=='sepa'){
      $in['mandate_id']=$in['item_id'];
    } else {
      $in['invoice_id'] = $in['item_id'];
      $supplier_filter="";
    }
    //$name_user= addslashes($this->dbu_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'"));
    $name_user= addslashes($this->dbu_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id=:user_id",['user_id'=>$_SESSION['u_id']]));

    //Set account default vat on customer creation
    $vat_regime_id = $this->dbu->field("SELECT id FROM vat_new WHERE `default`='1'");

    if(empty($vat_regime_id)){
      $vat_regime_id = 0;
    }

    $c_types = '';
    $c_type_name = '';
    if($in['c_type']){
      /*foreach ($in['c_type'] as $key) {
        if($key){
          $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
          $c_type_name .= $type.',';
        }
      }
      $c_types = implode(',', $in['c_type']);
      $c_type_name = rtrim($c_type_name,',');*/
      if(is_array($in['c_type'])){
        foreach ($in['c_type'] as $key) {
          if($key){
            $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
            $c_type_name .= $type.',';
          }
        }
        $c_types = implode(',', $in['c_type']);
        $c_type_name = rtrim($c_type_name,',');
      }else{
        $c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");        
        $c_types = $in['c_type'];
      }
    }


    if($in['user_id']==''){
      $in['user_id']=$_SESSION['u_id'];
    }


    if($in['user_id']){
      /*foreach ($in['user_id'] as $key => $value) {
        $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
          $acc_manager .= $manager_id.',';
      }     
      $in['acc_manager'] = rtrim($acc_manager,',');
      $acc_manager_ids = implode(',', $in['user_id']);*/
      if(is_array($in['user_id'])){
        foreach ($in['user_id'] as $key => $value) {
          $manager_id = $this->dbu_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
            $acc_manager .= $manager_id.',';
        }     
        $in['acc_manager'] = rtrim($acc_manager,',');
        $acc_manager_ids = implode(',', $in['user_id']);
      }else{
        //$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
        $in['acc_manager'] =$this->dbu_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
        $acc_manager_ids = $in['user_id'];
      }
    }else{
      $acc_manager_ids = '';
      $in['acc_manager'] = '';
    }
    $vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
          $vat_default=str_replace(',', '.', $vat);
    $selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");

    if($in['add_customer']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
        if(SET_DEF_PRICE_CAT == 1){
        $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
      }
        $in['buyer_id'] = $this->dbu->insert("INSERT INTO customers SET
                                            name='".addslashes($in['name'])."',
                                            our_reference = '".$account_reference_number."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            type='0',
                                            ".$supplier_filter."
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."',
                                            commercial_name     = '".$in['commercial_name']."',
                                            legal_type        = '".$in['legal_type']."',
                                            c_type          = '".$c_types."',
                                            website         = '".$in['website']."',
                                            language        = '".$in['language']."',
                                            sector          = '".$in['sector']."',
                                            comp_fax        = '".$in['comp_fax']."',
                                            activity        = '".$in['activity']."',
                                            comp_size       = '".$in['comp_size']."',
                                            lead_source       = '".$in['lead_source']."',
                                            various1        = '".$in['various1']."',
                                            various2        = '".$in['various2']."',
                                            c_type_name       = '".$c_type_name."',
                                            vat_id          = '".$selected_vat."',
                                            invoice_email_type    = '1',
                                            sales_rep       = '".$in['sales_rep_id']."',
                                            internal_language   = '".$in['internal_language']."',
                                            is_customer       = '".$in['is_customer']."',
                                            stripe_cust_id      = '".$in['stripe_cust_id']."',
                                            cat_id          = '".$in['cat_id']."'");

        $this->dbu->query("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
          foreach ($in['extra'] as $key => $value) {
            $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
            if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
              $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
            }
          }
        }

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->dbu->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->dbu->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->dbu_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
          $this->dbu_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                          ['user_id' => $_SESSION['u_id'],
                           'name'    => 'company-customers_show_info',
                           'value'   => '1']
                        );                            
        } else {
          /*$this->dbu_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
          $this->dbu_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->dbu->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          if($in['app_mod']=='incomminginvoice'){
            $this->dbu->query("UPDATE tblinvoice_incomming SET
                supplier_id='".$in['buyer_id']."',
                contact_id='',
                free_field = '".$address."'
                WHERE invoice_id='".$in['item_id']."' ");
          }else if($in['app_mod']=='recinvoice'){
            $this->dbu->query("UPDATE recurring_invoice SET
                buyer_id='".$in['buyer_id']."',
                contact_id='',
                free_field = '".$address."'
                WHERE recurring_invoice_id='".$in['item_id']."' ");
          } else if($in['app_mod']=='sepa'){
            $this->dbu->query("UPDATE mandate SET
                buyer_id='".$in['buyer_id']."',
                contact_id=''
                WHERE mandate_id='".$in['item_id']."' ");
          }else{
              $this->dbu->query("UPDATE tblinvoice SET
                buyer_id='".$in['buyer_id']."',
                buyer_name='".$in['name']."',
                contact_id='',
                free_field = '".$address."'
                WHERE id='".$in['item_id']."' ");
          }
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
    if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
        if(SET_DEF_PRICE_CAT == 1){
          $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
        }
        $in['buyer_id'] = $this->dbu->insert("INSERT INTO customers SET
                                            name='".addslashes($in['lastname'])."',
                                            firstname = '".addslashes($in['firstname'])."',
                                            our_reference = '".$account_reference_number."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            type = 1,
                                            ".$supplier_filter."
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."',
                                            commercial_name     = '".$in['commercial_name']."',
                                            legal_type        = '".$in['legal_type']."',
                                            c_type          = '".$c_types."',
                                            website         = '".$in['website']."',
                                            language        = '".$in['language']."',
                                            sector          = '".$in['sector']."',
                                            comp_fax        = '".$in['comp_fax']."',
                                            activity        = '".$in['activity']."',
                                            comp_size       = '".$in['comp_size']."',
                                            lead_source       = '".$in['lead_source']."',
                                            various1        = '".$in['various1']."',
                                            various2        = '".$in['various2']."',
                                            c_type_name       = '".$c_type_name."',
                                            vat_id          = '".$selected_vat."',
                                            invoice_email_type    = '1',
                                            sales_rep       = '".$in['sales_rep_id']."',
                                            internal_language   = '".$in['internal_language']."',
                                            is_customer       = '".$in['is_customer']."',
                                            stripe_cust_id      = '".$in['stripe_cust_id']."',
                                            cat_id          = '".$in['cat_id']."'");

        $this->dbu->query("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
          foreach ($in['extra'] as $key => $value) {
            $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
            if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
              $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
            }
          }
        }

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->dbu->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->dbu->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->dbu_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
          $this->dbu_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                          ['user_id' => $_SESSION['u_id'],
                           'name'    => 'company-customers_show_info',
                           'value'   => '1']
                        );                            
        } else {
          /*$this->dbu_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
          $this->dbu_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->dbu->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          if($in['app_mod']=='incomminginvoice'){
            $this->dbu->query("UPDATE tblinvoice_incomming SET
                supplier_id='".$in['buyer_id']."',
                contact_id='',
                free_field = '".$address."'
                WHERE invoice_id='".$in['item_id']."' ");
          }else if($in['app_mod']=='recinvoice'){
            $this->dbu->query("UPDATE recurring_invoice SET
                buyer_id='".$in['buyer_id']."',
                contact_id='',
                free_field = '".$address."'
                WHERE recurring_invoice_id='".$in['item_id']."' ");
          } else if($in['app_mod']=='sepa'){
            $this->dbu->query("UPDATE mandate SET
                buyer_id='".$in['buyer_id']."',
                contact_id=''
                WHERE mandate_id='".$in['item_id']."' ");
          }else{
            $this->dbu->query("UPDATE tblinvoice SET
                buyer_id='".$in['buyer_id']."',
                buyer_name='".$in['name']."',
                contact_id='',
                free_field = '".$address."'
                WHERE id='".$in['item_id']."' ");
          }
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
    if($in['add_contact']){
      $customer_name='';
      if($in['buyer_id']){
        $customer_name = addslashes($this->dbu->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
      }
      if($in['birthdate']){
        $in['birthdate'] = strtotime($in['birthdate']);
      }
        $in['contact_id'] = $this->dbu->insert("INSERT INTO customer_contacts SET
            customer_id = '".$in['buyer_id']."',
            firstname = '".$in['firstname']."',
            lastname  = '".$in['lastname']."',
            email   = '".$in['email']."',
            birthdate = '".$in['birthdate']."',
            cell    = '".$in['cell']."',
            sex     = '".$in['sex']."',
            title   = '".$in['title_contact_id']."',
            language  = '".$in['language']."',
            company_name= '".$customer_name."',
            `create`  = '".time()."'");
        $this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
      if($in['extra']){
        foreach ($in['extra'] as $key => $value) {
          $this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
        }
      }

        if($in['buyer_id']){
        $contact_accounts_sql="";
        $accepted_keys=["position","department","title","e_title","email","phone","fax","s_email","customer_address_id"];
        foreach($in['accountRelatedObj'] as $key=>$value){
          if(in_array($value['model'],$accepted_keys)){
            $contact_accounts_sql.="`".$value['model']."`='".addslashes($value['model_value'])."',";
          }
        }
        if($in['email']){
          $contact_accounts_sql.="`email`='".$in['email']."',";
        }
        if(!empty($contact_accounts_sql)){
          $contact_accounts_sql=rtrim($contact_accounts_sql,",");
          $this->db->query("INSERT INTO customer_contactsIds SET 
                        customer_id='".$in['buyer_id']."', 
                        contact_id='".$in['contact_id']."',
                        ".$contact_accounts_sql." ");
        } 
      }
        $in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
        if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
                $vars=array();
                if($in['buyer_id']){
                    $vars['customer_id']=$in['buyer_id'];
                    $vars['customer_name']=$customer_name;
                }
                $vars['contact_id']=$in['contact_id'];
                $vars['firstname']=$in['firstname'];
                $vars['lastname']=$in['lastname'];
                $vars['table']='customer_contacts';
                $vars['email']=$in['email'];
                $vars['op']='add';
                synctoZendesk($vars);
        }
        if($in['country_id']){
          $this->dbu->query("INSERT INTO customer_contact_address SET
                          address='".$in['address']."',
                          zip='".$in['zip']."',
                          city='".$in['city']."',
                          country_id='".$in['country_id']."',
                          contact_id='".$in['contact_id']."',
                          is_primary='1',
                          delivery='1' ");
        }
        /*$show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                    AND name    = 'company-contacts_show_info'  ");*/
        $show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                    AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);                                    
        if(!$show_info->move_next()) {
          /*$this->dbu_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                  name    = 'company-contacts_show_info',
                                  value   = '1' ");*/
          $this->dbu_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                  name    = :name,
                                  value   = :value ",
                                ['user_id' => $_SESSION['u_id'],
                                 'name'    => 'company-contacts_show_info',
                                 'value'   => '1']
                              );                                  
        } else {
          /*$this->dbu_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                      AND name    = 'company-contacts_show_info' ");*/
          $this->dbu_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                      AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);                                      
        }
        $count = $this->dbu->field("SELECT COUNT(contact_id) FROM customer_contacts ");
        if($count == 1){
          doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          if($in['app_mod']=='incomminginvoice'){
             $this->dbu->query("UPDATE tblinvoice_incomming SET contact_id='".$in['contact_id']."' WHERE invoice_id='".$in['item_id']."' ");
          }else if($in['app_mod']=='recinvoice'){
            $this->dbu->query("UPDATE recurring_invoice SET
                contact_id='".$in['contact_id']."'
                WHERE recurring_invoice_id='".$in['item_id']."' ");
          } else if($in['app_mod']=='sepa'){
            $this->dbu->query("UPDATE mandate SET
                contact_id='".$in['contact_id']."'
                WHERE mandate_id='".$in['item_id']."' ");
          }else{
              $this->dbu->query("UPDATE tblinvoice SET contact_id='".$in['contact_id']."' WHERE id='".$in['item_id']."' ");
          }

        }
        insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
        msg::success (gm('Success'),'success');
    }
    return true;
  }

  /**
  * undocumented function
  *
  * @return void
  * @author PM
  **/
  function tryAddCValid(&$in)
  {
    if($in['add_customer']){
        $v = new validation($in);
      $v->field('name', 'name', 'required:unique[customers.name]');
      $v->field('country_id', 'country_id', 'required');
      return $v->run();
    }
    if($in['add_individual']){
      $v = new validation($in);
      //$v->field('firstname', 'firstname', 'required');
      $v->field('lastname', 'lastname', 'required');
      // $v->field('email', 'Email', 'required:email:unique[customers.c_email]');
      $v->field('country_id', 'country_id', 'required');
      return $v->run();
    }
    return true;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author PM
   **/
  function tryAddAddress(&$in){
    if(!$this->CanEdit($in)){
      json_out($in);
      return false;
    }
    if(!$this->tryAddAddressValidate($in)){
      json_out($in);
      return false;
    }

    $country_n = get_country_name($in['country_id']);
    if($in['field']=='customer_id'){
      Sync::start(ark::$model.'-'.ark::$method);

      $address_id = $this->dbu->insert("INSERT INTO customer_addresses SET
                                        customer_id     = '".$in['customer_id']."',
                                        country_id      = '".$in['country_id']."',
                                        state_id      = '".$in['state_id']."',
                                        city        = '".$in['city']."',
                                        zip         = '".$in['zip']."',
                                        address       = '".$in['address']."',
                                        billing       = '".$in['billing']."',
                                        is_primary      = '".$in['primary']."',
                                        delivery      = '".$in['delivery']."'");
      if($in['billing']){
        $this->dbu->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
      }

      Sync::end($address_id);

      /*if($in['delivery']){
        $this->dbu->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
      }*/
      if($in['primary']){
        if($in['address'])
        {
          if(!$in['zip'] && $in['city'])
          {
            $address = $in['address'].', '.$in['city'].', '.$country_n;
          }elseif(!$in['city'] && $in['zip'])
          {
            $address = $in['address'].', '.$in['zip'].', '.$country_n;
          }elseif(!$in['zip'] && !$in['city'])
          {
            $address = $in['address'].', '.$country_n;
          }else
          {
            $address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
          }
          $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
          $output= json_decode($geocode);
          $lat = $output->results[0]->geometry->location->lat;
          $long = $output->results[0]->geometry->location->lng;
        }
        $this->dbu->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
        $this->dbu->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
        $this->dbu->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
      }
    }else{
      $address_id = $this->dbu->insert("INSERT INTO customer_contact_address SET
                                        contact_id      = '".$in['contact_id']."',
                                        country_id      = '".$in['country_id']."',                                        city        = '".$in['city']."',
                                        zip         = '".$in['zip']."',
                                        address       = '".$in['address']."',
                                        is_primary      = '".$in['primary']."',
                                        delivery      = '".$in['delivery']."'");
    }
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
    $in['delivery_address_id'] = $address_id;
    $in['country'] = $country_n;
    if($in['app_mod']=='incomminginvoice'){
      $in['incomming_invoice_id']=$in['item_id'];
    }else if($in['app_mod']=='sepa'){
      $in['mandate_id']=$in['item_id'];
    }else{
        $in['invoice_id'] = $in['item_id'];
    }

    if($in['item_id'] && is_numeric($in['item_id'])){
      $delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
      if($in['app_mod']=='incomminginvoice'){
        $this->dbu->query("UPDATE tblinvoice_incomming SET
          free_field='".$delivery_address."',
          same_address='".$in['delivery_address_id']."' WHERE invoice_id='".$in['item_id']."'  ");
      }else if($in['app_mod']=='sepa'){
        $this->dbu->query("UPDATE mandate SET
          address_info='".addslashes($delivery_address)."'
          WHERE mandate_id='".$in['item_id']."'  ");
      }else{
        $this->dbu->query("UPDATE tblinvoice SET
          free_field='".$delivery_address."',
          same_address='".$in['delivery_address_id']."' WHERE id='".$in['item_id']."'  ");
      }
    }elseif ($in['item_id'] == 'tmp') {
        $in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
    }
    $in['buyer_id'] = $in['customer_id'];
    // $in['pagl'] = $this->pag;

    // json_out($in);
    return true;

  }

  /**
   * undocumented function
   *
   * @return void
   * @author PM
   **/
  function tryAddAddressValidate(&$in)
  {
    $v = new validation($in);
    if($in['customer_id']){
      $v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
      if(!$in['primary'] && !$in['billing'] && !$in['site']){
        $v->field('primary', 'primary', 'required',gm("Main Address is mandatory"));
        $v->field('delivery', 'delivery', 'required',gm("Delivery Address is mandatory"));
        $v->field('billing', 'billing', 'required',gm("Billing Address is mandatory"));
        $v->field('site', 'site', 'required',gm("Site Address is mandatory"));

        $message = 'You must select an address type.';
        $is_ok = $v->run();
        if(!$is_ok){
          msg::error(gm($message),'error');
          return false;   
        }
      } else {
        $v->field('address', 'Address', 'required:text',gm("Street is mandatory"));
        $v->field('zip', 'Zip', 'required:text',gm("Zip Code is mandatory"));
        $v->field('city', 'City', 'required:text',gm("City is mandatory"));
        $v->field('country_id', 'Country', 'required:exist[country.country_id]');
      }
    }else if($in['contact_id']){
      $v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
    }else{
      msg::error(gm('Invalid ID'),'error');
      return false;
    }
    $v->field('country_id', 'Country', 'required:exist[country.country_id]',gm("Country is mandatory"));

    return $v->run();
  }

  /****************************************************************
  * function update(&$in)                                         *
  ****************************************************************/
  function update(&$in)
  {

    if(!$this->update_validate($in))
    {
      json_out($in);
      return false;
    }

    // printvar($in);
    // exit();
        $created_date= date(ACCOUNT_DATE_FORMAT);
    $updatedd_date= date(ACCOUNT_DATE_FORMAT);
    //$in['due_date'] = $in['quote_ts'] + ($in['invoice_due']*60*60*24);
    //update invoice
    $payment_days = $this->dbu->field("SELECT due_days FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
    $payment_days_type = $this->dbu->field("SELECT payment_term_type FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

    $in['invoice_due_date']=strtotime($in['invoice_due_date']);
    if($in['due_days'] != $payment_days || $in['payment_type_choose']!= $payment_days_type) {
      // we need to recalculate again:)
      $in['due_days'] != $payment_days ? $payment_days = $in['due_days'] : '';
      $in['payment_type_choose'] != $payment_days_type ? $payment_days_type = $in['payment_type_choose'] : '';

      if($payment_days_type==1) {
        $start_date = strtotime($in['quote_ts']);
/*        $curMonth = date('n',strtotime($in['quote_ts']));
        $curYear  = date('Y',strtotime($in['quote_ts']));
        $firstDayNextMonth = mktime(0, 0, 0,$curMonth, 1,$curYear);
        $start_date = $firstDayNextMonth;*/
        $in['invoice_due_date'] = $start_date + ($payment_days*(60*60*24)-1);
      } else {
        // $curMonth = date('n',strtotime($in['quote_ts']));
        // $curYear  = date('Y',strtotime($in['quote_ts']));
        /*if ($curMonth == 12) {
            $firstDayNextMonth = mktime(0, 0, 0, 0, 0, $curYear+1);
        } else {
            $firstDayNextMonth = mktime(0, 0, 0, $curMonth+1, 1,$curYear);
          }*/
        /*$firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
        $start_date = $firstDayNextMonth;*/
        $tmstmp_month=strtotime($in['quote_ts']) + ( $payment_days * ( 60*60*24 )-1);
        $lastday = date('t',$tmstmp_month);
        $in['invoice_due_date'] = mktime(23, 59, 59,date('m',$tmstmp_month), $lastday,date('Y',$tmstmp_month));
      }      
    }

    if(!$in['attach_timesheet']){
      $in['attach_timesheet'] = 0;
    }

    if(!$in['attach_intervention']){
      $in['attach_intervention'] = 0;
    }

/*if(!$in['account_manager_name']){
     $in['acc_manager_id']=0;
     $in['acc_manager_name']='';
}*/

    if($in['buyer_id']){
      /*if($in['sameAddress']==1){
        $filter= ' AND customer_addresses.billing=1 ';
      }else{
        $filter= ' AND customer_addresses.is_primary=1 ';
      }*/
      $customer_data=$this->dbu->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers 
        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
        WHERE customer_id='".$in['buyer_id']."' ");
          $in['buyer_name']=addslashes(stripslashes($customer_data->f('name').' '.$customer_data->f('l_name')));
          $in['buyer_email']=$customer_data->f('c_email');
          $in['buyer_phone']=$customer_data->f('comp_phone');
          $in['buyer_fax']=$customer_data->f('comp_fax');
          $in['seller_bwt_nr']=$customer_data->f('btw_nr');

      $buyer_info = $this->dbu->query("SELECT customer_addresses.*
                  FROM customers
                  INNER JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.billing=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");

      if(!$buyer_info->next()){
        $buyer_info = $this->dbu->query("SELECT customer_addresses.*
                  FROM customers
                  INNER JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
      }
      $in['main_address_id']=$buyer_info->f('address_id');
        $same_address=0;
       //if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            if($buyer_info->f('address_id') != $in['main_address_id']){
                $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
                $address_info = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
            }
            $free_field=addslashes($address_info);
            $in['buyer_address']=addslashes($buyer_info->f('address'));
            $in['buyer_zip']=$buyer_info->f('zip');
            $in['buyer_city']=addslashes($buyer_info->f('city'));
            $in['buyer_country_id']=$buyer_info->f('country_id');
       /*}else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);
        }*/
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");

      $same_address=0;
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);
      }
   }

   //Set email language as account / contact language
    $emailMessageData = array('buyer_id'    => $in['buyer_id'],
                    'contact_id'    => $in['contact_id'],
                    'item_id'     => $in['invoice_id'],
                    'email_language'  => $in['email_language'],
                    'table'     => 'tblinvoice',
                    'table_label'   => 'id',
                    'table_buyer_label' => 'buyer_id',
                    'table_contact_label' => 'contact_id',
                    'param'     => 'edit');
    $in['email_language'] = get_email_language($emailMessageData);
      //End Set email language as account / contact language

   //$default_email_language = $this->dbu_users->field("SELECT lang_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
    $default_email_language = $this->dbu_users->field("SELECT lang_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    if($in['email_language']==0){
      $in['email_language'] = $default_email_language;
    }

    if($in['payment_method']!='3'){
      $in['sepa_mandates'] =0;
    }

    if($in['serial_number']){
      $serial_number_date = " serial_number_date           =  '".time()."', ";
    }

    $current_serial_number=$this->dbu->field("SELECT serial_number FROM tblinvoice WHERE id = '".$in['invoice_id']."'");
    if($current_serial_number){
      $invoice_type=$this->dbu->field("SELECT `type` FROM tblinvoice WHERE  id = '".$in['invoice_id']."' ");
      if($current_serial_number){
        $inv_type=0;
        switch($invoice_type){
          case '2':
          case '4':
            $inv_type=2;
            break;
          case '1':
            $inv_type=1;
            break;
          default:
            $inv_type=0;
        } 
        $this->dbu->query("DELETE FROM tblinvoice_serial_numbers WHERE serial_number='".$current_serial_number."' AND `type`='".$inv_type."' ");
      }
    }

    $this->dbu->query("UPDATE tblinvoice SET

                                                serial_number           =   '".$in['serial_number']."',"
                                                .$serial_number_date ."
                                                invoice_date            =   '".strtotime($in['quote_ts'])."',
                                                due_date          = '".$in['invoice_due_date']."',
                                                due_days          =   '".$in['due_days']."',
                                                discount                =   '".return_value($in['discount'])."',
                                                req_payment               =   '".return_value($in['req_payment'])."',
                                                currency_type             =   '".$in['currency_type']."',
                                                c_invoice_id              =   '".$in['c_invoice_id']."',
                                                notes2                    =   '".utf8_encode($in['notes2'])."',
                                                type                      =   '".$in['type']."',
                                                step                      =   '2',
                                                contact_id          =   '".$in['contact_id']."',
                                                attention_of            =   '".$in['attention_of']."',
                                                buyer_id                  =   '".$in['buyer_id']."',
                                                general_conditions_new_page     =  '".$in['page']."',
                                                buyer_name            =   '".$in['buyer_name']."',                              
                                                buyer_email             =   '".$in['buyer_email']."',
                                                buyer_phone             =   '".$in['buyer_phone']."',
                                                buyer_fax             =   '".$in['buyer_fax']."', 
                                                buyer_address           =   '".$in['buyer_address']."',
                                                buyer_zip             =   '".$in['buyer_zip']."',
                                                buyer_city            =   '".$in['buyer_city']."',
                                                buyer_country_id          =   '".$in['buyer_country_id']."',
                                                seller_id                 =   '".$in['seller_id']."',
                                                seller_name               =   '".$in['seller_name']."',
                                                seller_d_address          =   '".$in['seller_d_address']."',
                                                seller_d_zip              =   '".$in['seller_d_zip']."',
                                                seller_d_city         =   '".$in['seller_d_city']."',
                                                seller_d_country_id       =   '".$in['seller_d_country_id']."',
                                                seller_d_state_id         =   '".$in['seller_d_state_id']."',
                                                seller_b_address          =   '".$in['seller_b_address']."',
                                                seller_b_zip              =   '".$in['seller_b_zip']."',
                                                seller_b_city           =   '".$in['seller_b_city']."',
                                                seller_b_country_id       =   '".$in['seller_b_country_id']."',
                                                seller_b_state_id         =   '".$in['seller_b_state_id']."',
                                                seller_bwt_nr             =   '".$in['seller_bwt_nr']."',
                                                last_upd                  =   '".$updatedd_date."',
                                                last_upd_by               =   '".$_SESSION['u_id']."',
                                                email_language        = '".$in['email_language']."',
                                                vat             = '".$in['vat']."',
                                                due_date_vat      =   '".strtotime($in['due_date_vat'])."',
                                                our_ref           =   '".$in['our_ref']."',
                                                your_ref          =   '".$in['your_ref']."',
                                                remove_vat          =   '".$in['remove_vat']."',
                                                quote_id          = '".$in['quote_id']."',
                                                contract_id         = '".$in['contract_id']."',
                                                free_field          = '".$free_field."',
                                                order_id          = '".$in['order_inv']."',
                                                apply_discount        = '".$in['apply_discount']."',
                                                discount_line_gen     =   '".return_value($in['discount_line_gen'])."',
                                                attach_timesheet        =   '".$in['attach_timesheet']."',
                                                attach_intervention       =   '".$in['attach_intervention']."',
                                                vat_regime_id           ='".$in['vat_regime_id']."',
                                                payment_term_type       =   '".$payment_days_type."',
                                                acc_manager_id           ='".$in['acc_manager_id']."',
                                                acc_manager_name           ='".$in['acc_manager_name']."',
                                                mandate_id='".$in['sepa_mandates']."',
                                                identity_id ='".$in['identity_id']."',
                                                main_address_id           = '".$in['main_address_id']."',
                                                same_address       ='".$same_address."',
                                                currency_rate      = '".$in['currency_rate']."',
                                                source_id          ='".$in['source_id']."',
                                                type_id            ='".$in['type_id']."',
                                                segment_id         ='".$in['segment_id']."',
                                                payment_method     ='".$in['payment_method']."',
                                                yuki_project_id     ='".$in['yuki_project_id']."',
                                                cat_id             ='".$in['cat_id']."',
                                                subject ='".addslashes($in['subject'])."',
                                                third_party_id='".($in['third_party_applied'] && $in['has_third_party'] && $in['third_party_id'] ? $in['third_party_id'] : '0')."'
                                                WHERE id = '".$in['invoice_id']."' ");


    // notes                    =   '".utf8_encode($in['notes'])."',

    // update multilanguage order note in note_fields
    $lang_note_id = $in['email_language'];
    if($lang_note_id == $in['email_language']){
      $active_lang_note = 1;
    }else{
      $active_lang_note = 0;
    }
        $langs = $this->dbu->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");

        $exist_notes = $this->dbu->field("SELECT active FROM note_fields WHERE item_id='".$in['invoice_id']."' AND lang_id='".$in['email_language']."'");
        if($exist_notes){
          $this->dbu->query("UPDATE note_fields SET
              active          =   '".$active_lang_note."',
              item_value        =   '".$in['NOTES']."'
              WHERE   item_type         =   'invoice'
              AND   item_name         =   'notes'
              AND   lang_id         =   '".$in['email_language']."'
              AND   item_id         =   '".$in['invoice_id']."'
          ");
        }else{
          //Set as inactive all the languages
          $this->dbu->query("UPDATE note_fields SET
              active          =   '0'
              WHERE   item_type         =   'invoice'
              AND   item_name         =   'notes'
              AND   item_id         =   '".$in['invoice_id']."'
          ");

          $this->dbu->query("INSERT INTO note_fields SET
              active            =   '".$active_lang_note."',
              item_value        =   '".$in['NOTES']."',
              item_type         =   'invoice',
              item_name          =   'notes',
              lang_id           =   '".$in['email_language']."',
              item_id           =   '".$in['invoice_id']."'
          ");
        }

/*        while($langs->next()){
          foreach ($in['translate_loop'] as $key => $value) {
            if($value['lang_id'] == $langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($lang_note_id == $langs->f('lang_id')){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }
          $this->dbu->query("UPDATE note_fields SET
                            active          =   '".$active_lang_note."',
                            item_value        =   '".$in['nota']."'
                        WHERE   item_type         =   'invoice'
                        AND   item_name         =   'notes'
                        AND   lang_id         =   '".$langs->f('lang_id')."'
                        AND   item_id         =   '".$in['invoice_id']."'

          ");
        }*/

        #custom langs
/*        $custom_langs = $this->dbu->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
        while($custom_langs->next()) {
          foreach ($in['custom_translate_loop'] as $key => $value) {
            if($value['lang_id'] == $custom_langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($lang_note_id == $custom_langs->f('lang_id')) {
            $active_lang_note = 1;
          } else {
            $active_lang_note = 0;
          }
          $this->dbu->query("UPDATE note_fields SET   active    = '".$active_lang_note."',
                                item_value  = '".$in['nota']."'
                            WHERE   item_type   = 'invoice'
                            AND   item_name   = 'notes'
                            AND   lang_id   = '".$custom_langs->f('lang_id')."'
                            AND   item_id   = '".$in['invoice_id']."'
          ");
        }*/

    //INSERT LINES
    $this->update_invoice_lines($in);

      msg::success(gm('Invoice has been updated'),'success');
      // ark::run('invoice-invoice');
    // $in['pag']='invoice';

    $trace_id=$this->db->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
    if($in['deal_id']){
      //$trace_id=$this->db->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($trace_id){
        $linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
            INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
            WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
        if($linked_doc){
          $this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' "); 
        }else{
          $this->db->query("INSERT INTO tracking_line SET
              origin_id='".$in['deal_id']."',
              origin_type='11',
              trace_id='".$trace_id."' ");
        }
      }else{
        $tracking_data=array(
                'target_id'         => $in['invoice_id'],
                'target_type'       => '1',
                'target_buyer_id'   => $in['buyer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
                                        )
              );
        addTracking($tracking_data);
      }
    }else{
      //$trace_id=$this->db->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($trace_id){
        $linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
            INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
            WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
        if($linked_doc){
          $this->db->query("DELETE FROM tracking_line WHERE line_id='".$linked_doc."' "); 
        }
      }
    }
    if($trace_id){
      $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
    }

    insert_message_log($this->pag,'{l}Invoice has been updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
    $in['pagl'] = $this->pag;
    $in['pag']='invoice';
    $in['add_problem'] = $this->pag;

    # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $in['email_language'];
    $params['type_pdf'] = $inv->f('type');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);
    # for pdf
    return true;
  }

  function update_invoice_lines($in){
    $this->dbu->query("DELETE FROM tblinvoice_line WHERE invoice_id = '".$in['invoice_id']."'");
    $invoice_total=0;
    $line_vat = 0;
    $discount = return_value($in['discount']);
    if($in['apply_discount'] < 2){
      $discount = 0;
    }
    $invoice_total_vat = 0;
    $disc = 0;
    $invoice_total_articles=0;
    $invoice_total_articles_purchase=0;
    $invoice_total_articles_for_margin =0;

    if(is_array($in['invoice_line'])){
      $i=0;
        $margin_total=0;
        $discount_total = 0 ;
      foreach ($in['invoice_line'] AS $nr => $nothing ){
        $line_total=0;

//if($in['description'][$nr]){
            // $line_total=(return_value($in['quantity'][$nr]) * return_value($in['price'][$nr])) ;
            // $discount_vat = $line_total * (return_value($in['discount']) / 100);
            // $line_vat += ($line_total - $discount )*(return_value($in['vat_val'][$nr])/100);

            $line_total=(return_value($nothing['quantity']) * return_value($nothing['price']));
          $line_discount = return_value($nothing['discount_line']);
          if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
            $line_discount = 0;
          }
          $line_total = round($line_total,ARTICLE_PRICE_COMMA_DIGITS);
          $disc_line = round($line_total * $line_discount / 100,ARTICLE_PRICE_COMMA_DIGITS);
            $discount_vat = ( $line_total - $disc_line ) * ( $discount / 100);
            $discount_total += $discount_vat;
            $invoice_total += $line_total - $disc_line - $discount_vat;
                   if($nothing['article_id']){
                      $invoice_total_articles += $line_total - $disc_line - $discount_vat;
                      $invoice_total_articles_purchase += return_value($nothing['purchase_price'])*return_value($nothing['quantity']);
                    }
              /*$line_total=(return_value($in['quantity'][$nr]) * return_value($in['price'][$nr])) ;
            $discount_vat = ( $line_total - $line_total * return_value($in['discount_line'][$nr]) / 100 ) * (return_value($in['discount']) / 100);
            $invoice_total += $line_total - $line_total * return_value($in['discount_line'][$nr]) / 100 - $discount_vat;*/

            // if($in['discount_line'][$nr]){
            //  $disc += (return_value($in['quantity'][$nr]) *  (return_value($in['price'][$nr])*$in['discount_line'][$nr]/100) );
            //  $discount_vat = (return_value($in['quantity'][$nr]) *  (return_value($in['price'][$nr])*$in['discount_line'][$nr]/100) );
            // }
            // article_id       = '".$in['article_id'][$nr]."',
            $invoice_total_vat += ($line_total - $disc_line - $discount_vat) * (return_value($nothing['vat']) / 100);
            // $invoice_total_vat += ($line_total - $line_total * return_value($in['discount_line'][$nr]) / 100 - $discount_vat) * (return_value($in['vat_val'][$nr]) / 100);
            // $invoice_total+= $line_total;
            // $modifiable =($in['readonly'][$nr])? ", readonly = '1'":"";
         if($nothing['is_tax'] != 1&& $line_total>0){
                     $margin_total+=($line_total -$disc_line - $discount_vat )-(return_value($nothing['purchase_price'])*return_value($nothing['quantity']));
                     $invoice_total_articles_for_margin += $line_total - $disc_line - $discount_vat;

                   }
            $use_combined = $this->dbu->field("SELECT use_combined FROM pim_articles WHERE article_id='".$nothing['article_id']."'");
                if($nothing['is_combined'] || (!$use_combined && !$nothing['component_for'])){
                    $last_combined = '';
                }

                if( $nothing['has_variants'] ){
                  $last_variant_parent = '';
                } 

                   if($nothing['is_tax'] != 1){
                           $line_id = $this->dbu->insert("INSERT INTO tblinvoice_line SET
                                                        name                =   '".htmlspecialchars($nothing['description'],ENT_QUOTES,'UTF-8')."',
                                                        invoice_id          =   '".$in['invoice_id']."',
                                                        quantity            =   '".return_value($nothing['quantity'])."',
                                                        price               =   '".return_value($nothing['price'])."',
                                                        amount              =   '".$line_total."',
                                                        f_archived          =   '0',
                                                        created             =   '".$created_date."',
                                                        created_by          =   '".$_SESSION['u_id']."',
                                                        last_upd            =   '".$created_date."',
                                                        last_upd_by         =   '".$_SESSION['u_id']."',
                                                        vat                 =   '".return_value($nothing['vat'])."',
                                                        discount            =   '".return_value($nothing['discount_line'])."',
                                                        content             = '".htmlentities($nothing['content'])."',
                                                        content_title             = '".htmlentities($nothing['title'])."',
                                                        article_id        =     '".$nothing['article_id']."',
                                                        purchase_price = '".return_value($nothing['purchase_price'])."',
                                                        item_code         = '".addslashes($nothing['article_code'])."',


                                                        sort_order          =   '".$i."',
                                                        is_combined         = '".$nothing['is_combined']."',
                                                        component_for       ='".$last_combined."',
                                                        visible             ='".$nothing['visible']."',
                                                        has_variants        = '".$nothing['has_variants']."',
                                                        has_variants_done   ='".$nothing['has_variants_done']."',
                                                        is_variant_for      ='".$nothing['is_variant_for']."',
                                                        is_variant_for_line ='".$last_variant_parent."',
                                                        variant_type        ='".$nothing['variant_type']."' "
                                                        .$modifiable
                                                    );
                            if($nothing['is_combined']){
                                $last_combined = $line_id;
                              }

                            if($nothing['has_variants'] && $nothing['has_variants_done']){
                                $last_variant_parent = $line_id;
                              }
                            if($nothing['is_variant_for']){
                                $last_variant_parent = '';
                              }
                   }else{
                    $tax_id = $nothing['tax_id'];
                    if(!$tax_id){$tax_id = $nothing['article_id'];}
                        $line_id = $this->dbu->insert("INSERT INTO tblinvoice_line SET
                                                        name                =   '".htmlspecialchars($nothing['description'],ENT_QUOTES,'UTF-8')."',
                                                        invoice_id          =   '".$in['invoice_id']."',
                                                        quantity            =   '".return_value($nothing['quantity'])."',
                                                        price               =   '".return_value($nothing['price'])."',
                                                        amount              =   '".$line_total."',
                                                        f_archived          =   '0',
                                                        created             =   '".$created_date."',
                                                        created_by          =   '".$_SESSION['u_id']."',
                                                        last_upd            =   '".$created_date."',
                                                        last_upd_by         =   '".$_SESSION['u_id']."',
                                                        vat                 =   '".return_value($nothing['vat'])."',
                                                        discount            =   '".return_value($nothing['discount_line'])."',
                                                        content             = '".htmlentities($nothing['content'])."',
                                                        content_title             = '".htmlentities($nothing['title'])."',
                                                        purchase_price = '".return_value($nothing['purchase_price'])."',
                                                        item_code         = '".addslashes($nothing['article_code'])."',
                                                        tax_id = '".$tax_id."',
                                                        tax_for_article_id = '".$nothing['tax_for_article_id']."',
                                                        visible             ='1',
                                                        sort_order          =   '".$i."'"
                                                        .$modifiable
                                                    );

                   }

               // }
              $i++;
      }
    }
    /*$discount = $invoice_total * (return_value($in['discount']) / 100);
    if($disc){
      $discount = $disc;
    }*/
    // $invoice_total = $invoice_total - $discount + $line_vat;
    // $invoice_total = $invoice_total - $discount;


    $invoice_total=round($invoice_total,ARTICLE_PRICE_COMMA_DIGITS);
    $invoice_total_vat=round($invoice_total_vat,ARTICLE_PRICE_COMMA_DIGITS);
    if($in['downpayment']==1){
      $downpayment_value=$this->dbu->field("SELECT downpayment_value FROM tblinvoice WHERE tblinvoice.id='".$in['invoice_id']."' ");
      $invoice_total-=$downpayment_value;
      $invoice_total_vat += $invoice_total;
    }else{
      $invoice_total_vat += $invoice_total;
    }
    $margin_total=$margin_total-$discount_total;

    $margin_percent_total =0;
    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
         if($invoice_total_articles_purchase){
           $margin_percent_total=($margin_total/$invoice_total_articles_purchase)*100;
          }else{
            $margin_percent_total=100;
          }
        
     }else{
         if($invoice_total_articles_for_margin){
            $margin_percent_total=($margin_total/$invoice_total_articles_for_margin)*100;
          }else{
            $margin_percent_total=100;
          }
     }
  
    if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
      if($in['currency_rate']){
        $invoice_total = $invoice_total*return_value($in['currency_rate']);
        $invoice_total_articles = $invoice_total_articles*return_value($in['currency_rate']);
        $invoice_total_vat = $invoice_total_vat*return_value($in['currency_rate']);

      }
    }

    $this->dbu->query("UPDATE tblinvoice SET margin='".$margin_total."', margin_percent='".$margin_percent_total."',amount='".$invoice_total."', amount_vat='".$invoice_total_vat."' WHERE id='".$in['invoice_id']."'");
  }

  /****************************************************************
  * function update_validate(&$in)                                *
  ****************************************************************/
  function update_validate(&$in)
  {
    $is_ok = true;
    $v = new validation($in);
    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
    $is_ok = $v->run();

    if (!$this->add_validate($in))
    {
      $is_ok = false;
    }
    return $is_ok;
  }

  /****************************************************************
  * function add_validate(&$in)                                   *
  ****************************************************************/
  function add_validate(&$in)
  {
    $v = new validation($in);
    $x = new validation($in);
    $is_ok = true;

    if(!$in['serial_number'] && $in['type']!=1 && !DRAFT_INVOICE_NO_NUMBER && ark::$method=='update')
    {
      $v->field('serial_number','Invoice Nr','required',gm('Invoice Nr').'* '.gm('is required'));
    }
    elseif($in['type']==0 || $in['type']==2 ) {
      if($in['do']=='invoice-ninvoice-invoice-add'){
        $v->field('serial_number', 'Invoice Nr', 'unique[tblinvoice.serial_number.( type=\''.$in['type'].'\')]',utf8_decode(gm('Invoice Nr')).' '.utf8_decode(gm('is already in use')));
      }else{
         $v->field('serial_number', 'Invoice Nr', "unique[tblinvoice.serial_number.( id!='".$in['invoice_id']."' AND type='".$in['type']."')]",utf8_decode(gm('Invoice Nr')).' '.utf8_decode(gm('is already in use')));
      }
    }
   /* if(!check_vat_regime($in['vat_regime_id'])){
      msg::error ( gm('Vat regime invalid'),'error');
        json_out($in);
      return false;
    };*/
    if(!$v->run()){
      /*if($in['do']=='invoice-ninvoice-invoice-add' && $in['type'] != 2){
         $in['serial_number']= generate_invoice_number(DATABASE_NAME);
      }else{*/
        return false;
      //}
    }

    $x->field('quote_ts','Date','required',gm('Date').' *:'.gm('is required'));
/*    if($in['type']==2 && !$in['c_invoice_id']){
      $x->field('c_invoice_id','Invoice','required',gm('Please select a invoice'));
    }*/
    if($in['do'] != 'invoice-recurring_ninvoice'){
      $x->field('invoice_due_date','Due Date','required',gm('Due Date').'*:'.gm('is required'));
    }

    return $x->run();
  }

  /****************************************************************
  * function add(&$in)                                            *
  ****************************************************************/
  function add(&$in)
  {
    if(!$this->add_validate($in))
    {
      json_out($in);
      return false;
    }

    $force_draft=false;
    if($_SESSION['main_u_id']=='0'){
      //$is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
      $is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    }else{
      $is_accountant=$this->dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
    }
    if($is_accountant){
        $accountant_settings=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");

       if($accountant_settings=='1'){
            $acc_force=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_FORCE_DRAFT' ");
            if($acc_force){
                if(!defined('DRAFT_INVOICE_NO_NUMBER')){
                    $this->dbu->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
                }else{
                    $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
                }
            }
       }else{
           $force_draft=$this->dbu_users->field("SELECT force_draft FROM accountants WHERE account_id='".$is_accountant."' ");
          if($force_draft){
            if(!defined('DRAFT_INVOICE_NO_NUMBER')){
                  $this->dbu->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
            }else{
                  $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
            }
          }
       }   
    }
    $DRAFT_INVOICE_NO_NUMBER=$this->dbu->field("SELECT value FROM settings WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
    $created_date= date(ACCOUNT_DATE_FORMAT);

      //insert into invoice
    if($DRAFT_INVOICE_NO_NUMBER=='1'){
       $serial_number='';
       $in['serial_number']='no_number';
       $serial_number_date='';
    }else{
       //$serial_number=$in['serial_number'];
      $serial_number_date=time();
      if(($in['type']==0 || $in['type']==3)){
        $serial_number=generate_invoice_number(DATABASE_NAME,false);
      }elseif(($in['type'] == 2 || $in['type']==4)){
        $serial_number=generate_credit_invoice_number(DATABASE_NAME,false);
      }else if($in['type'] == 1){
        $serial_number=generate_proforma_invoice_number(DATABASE_NAME,false);
      }
      $serial_number_date=time();
    }

      //$in['due_date'] = $in['quote_ts'] + ($in['invoice_due']*60*60*24);
    $created_by = $_SESSION['u_id'];
    if(ark::$method == 'add_recurring'){
      $created_by = 'System';
      $subject=$in['title'];
    }else{
      $subject=$in['subject'];
    }
    $req_pay= $in['req_payment']? $in['req_payment']:'100';

    if(!$in['attach_timesheet']){
      $in['attach_timesheet'] = 0;
    }

    if(!$in['attach_intervention']){
      $in['attach_intervention'] = 0;
    }
    if(!$in['pdf_layout'] && $in['identity_id']){
      $in['pdf_layout'] = $this->dbu->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_id']."' and module='inv'");
    }
    $downpayment_invoice=0;
    if($in['downpayment']==1){
      $in['discount']=0;
      $downpayment_invoice=1;
    }
   if($in['contact_id']) {
      $in['contact_name']=addslashes($this->dbu->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE customer_contacts.contact_id='".$in['contact_id']."'"));
   }

   if($in['buyer_id']){
      /*if($in['sameAddress']==1){
        $filter = ' AND customer_addresses.billing=1';
      }else{
        $filter = ' AND customer_addresses.is_primary=1';
      }*/
      $customer_data=$this->dbu->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers 
        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
        WHERE customer_id='".$in['buyer_id']."' ");
      $in['buyer_name']=addslashes(stripslashes($customer_data->f('name').' '.$customer_data->f('l_name')));
      $in['buyer_email']=$customer_data->f('c_email');
      $in['buyer_phone']=$customer_data->f('comp_phone');
      $in['buyer_fax']=$customer_data->f('comp_fax');
      $in['seller_bwt_nr']=$customer_data->f('btw_nr');

      $buyer_info = $this->dbu->query("SELECT customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id
                  FROM customers
                  INNER JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.billing=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");

      if(!$buyer_info->next()){
        $buyer_info = $this->dbu->query("SELECT customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id
                  FROM customers
                  INNER JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
      }

        $same_address=0;
        $in['main_address_id']=$buyer_info->f('address_id');
       //if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            if($buyer_info->f('address_id') != $in['main_address_id']){
                $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
                $address_info = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
            }
            $free_field=addslashes($address_info);
            $in['buyer_address']=addslashes($buyer_info->f('address'));
            $in['buyer_zip']=$buyer_info->f('zip');
            $in['buyer_city']=addslashes($buyer_info->f('city'));
            $in['buyer_country_id']=$buyer_info->f('country_id');
       /*}else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);
        }*/
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");

      $same_address=0;
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);
      }
   }
$in['save_final']=0;
    
    //Set email language as account / contact language
    $emailMessageData = array('buyer_id'    => $in['buyer_id'],
                    'contact_id'    => $in['contact_id'],
                    'param'     => 'add');
    $in['email_language'] = get_email_language($emailMessageData);
    //End Set email language as account / contact language

    //$default_email_language = $this->dbu_users->field("SELECT lang_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
    $default_email_language = $this->dbu_users->field("SELECT lang_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    if($in['email_language']==0){
      $in['email_language'] = $default_email_language;
    }

    if($in['quote_id'] && empty($in['invoice_id'])){
      $has_invoice_id = false;
      $quote_data = $this->dbu->query("SELECT id,email_language,tblquote_version.version_id FROM tblquote
                                INNER JOIN tblquote_version ON tblquote_version.quote_id = tblquote.id AND tblquote_version.active ='1'
                                WHERE id = '".$in['quote_id']."' 
                                ")->getAll();
      $quote_data = $quote_data[0];

      $quote_notes = $this->db->field("SELECT item_value FROM note_fields
                 WHERE item_id= '".$quote_data['id']."' AND lang_id ='".$quote_data['email_language']."' AND item_type='quote' AND item_name='notes' AND buyer_id='".$in['buyer_id']."' AND active ='1'");

      $free_text_content = $this->db->field("SELECT item_value FROM  note_fields
                     WHERE item_type  = 'quote'
                     AND  item_name   = 'free_text_content'
                     AND  version_id  = '".$quote_data['version_id']."'
                     AND  item_id   = '".$quote_data['id']."'
                     AND  buyer_id  = '".$in['buyer_id']."'
                     AND active ='1' ");

      if($in['notes2'] == $quote_notes){
          $in['notes2'] = addslashes($quote_notes);
      }
    }

    if($in['service_id'] && $in['invoice_id'] == 'tmp'){
        $in['notes2'] = addslashes($in['notes2']);
    }

    if(!$in['currency_rate']){
      $in['currency_rate'] = 1;
    }

 /*   if(!$in['service_id'] && !$in['quote_id'] && !$in['project_id'] && !$in['orders_id'] && !$in['contract_id'] && !$in['deal_id'] ){
      $in['identity_id']= $this->dbu_users->field("SELECT default_identity_id FROM user_info WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    }else{
      //$in['identity_id']=0;
    }*/

    $in['invoice_id']=$this->dbu->insert("INSERT INTO tblinvoice SET
                                                serial_number           =   '".$serial_number."',
                                                serial_number_date      =   '".$serial_number_date."',
                                                invoice_date            =   '".(is_numeric($in['quote_ts']) ? $in['quote_ts'] : strtotime($in['quote_ts']))."',
                                                due_date          = '".(is_numeric($in['invoice_due_date']) ? $in['invoice_due_date'] : strtotime($in['invoice_due_date']))."',
                                                due_days          =   '".$in['due_days']."',
                                                payment_term_type     =   '".$in['payment_type_choose']."',
                                                discount                =   '".return_value($in['discount'])."',
                                                req_payment               =   '".return_value($req_pay)."',
                                                currency_type           =   '".$in['currency_type']."',
                                                c_invoice_id              =   '".$in['c_invoice_id']."',
                                                notes2                    =   '".utf8_encode($in['notes2'])."',
                                                type                    =   '".$in['type']."',
                                                step                    = '1',
                                                contact_id          =   '".$in['contact_id']."',
                                                contact_name         =   '".$in['contact_name']."',
                                                status                  =   '0',
                                                f_archived              =   '0',
                                                paid                  =   '0',
                                                general_conditions_new_page   =   '".$in['page']."',
                                                attention_of            =   '".$in['attention_of']."',
                                                buyer_id                  =   '".$in['buyer_id']."',
                                                buyer_name            =   '".$in['buyer_name']."',
                                                buyer_address           =   '".$in['buyer_address']."',
                                                buyer_email             =   '".$in['buyer_email']."',
                                                buyer_phone             =   '".$in['buyer_phone']."',
                                                buyer_fax             =   '".$in['buyer_fax']."',
                                                buyer_zip             =   '".$in['buyer_zip']."',
                                                buyer_city            =   '".$in['buyer_city']."',
                                                buyer_country_id          =   '".$in['buyer_country_id']."',
                                                buyer_state_id          =   '".$in['buyer_state_id']."',
                                                seller_id             =   '".$in['seller_id']."',
                                                seller_name             =   '".$in['seller_name']."',
                                                seller_d_address          =   '".$in['seller_d_address']."',
                                                seller_d_zip            =   '".$in['seller_d_zip']."',
                                                seller_d_city             =   '".$in['seller_d_city']."',
                                                seller_d_country_id       =   '".$in['seller_d_country_id']."',
                                                seller_d_state_id         =   '".$in['seller_d_state_id']."',
                                                seller_b_address          =   '".$in['seller_b_address']."',
                                                seller_b_zip            =   '".$in['seller_b_zip']."',
                                                seller_b_city           =   '".$in['seller_b_city']."',
                                                seller_b_country_id       =   '".$in['seller_b_country_id']."',
                                                seller_b_state_id         =   '".$in['seller_b_state_id']."',
                                                seller_bwt_nr             =   '".$in['seller_bwt_nr']."',
                                                created                 =   '".$created_date."',
                                                created_by              =   '".$created_by."',
                                                last_upd                =   '".$created_date."',
                                                last_upd_by             =   '".$_SESSION['u_id']."',
                                                vat             =   '".$in['vat']."',
                                                due_date_vat      =   '".(is_numeric($in['due_date_vat']) ? $in['due_date_vat']: strtotime($in['due_date_vat']))."',
                                                our_ref           =   '".$in['our_ref']."',
                                                your_ref          =   '".$in['your_ref']."',
                                                remove_vat          =   '".$in['remove_vat']."',
                                                currency_rate       = '".$in['currency_rate']."',
                                                quote_id          = '".$in['quote_id']."',
                                                contract_id         = '".$in['contract_id']."',
                                                email_language        = '".$in['email_language']."',
                                                order_id          = '".$in['order_inv']."',
                                                free_field          = '".$free_field."',
                                                same_invoice        = '".$in['same_invoice']."',
                                                apply_discount        = '".$in['apply_discount']."',
                                                service_id          = '".$in['service_id']."',
                                                attach_timesheet        =   '".$in['attach_timesheet']."',
                                                attach_intervention       =     '".$in['attach_intervention']."',
                                                vat_regime_id           ='".$in['vat_regime_id']."',
                                                acc_manager_id           ='".$in['acc_manager_id']."',
                                                acc_manager_name           ='".addslashes($in['acc_manager_name'])."',
                                                discount_line_gen     =   '".return_value($in['discount_line_gen'])."',
                                                identity_id ='".$in['identity_id']."',
                                                pdf_layout='".$in['pdf_layout']."',
                                                mandate_id='".$in['sepa_mandates']."',
                                                same_address='".$same_address."',
                                                main_address_id           = '".$in['main_address_id']."',
                                                downpayment_inv='".$downpayment_invoice."',
                                                payment_method  ='".$in['payment_method']."',
                                                source_id     ='".$in['source_id']."',
                                                type_id     ='".$in['type_id']."',
                                                segment_id  ='".$in['segment_id']."',
                                                yuki_project_id     ='".$in['yuki_project_id']."',
                                                cat_id      ='".$in['cat_id']."',
                                                subject ='".addslashes($subject)."',
                                                third_party_id='".($in['third_party_applied'] && $in['has_third_party'] && $in['third_party_id'] ? $in['third_party_id'] : '0')."' ");

    if($in['from_to_invoice']) {
        insert_message_log('invoice', '{l}Invoice created by{endl} '.get_user_name($_SESSION['u_id']), 'invoice_id', $in['invoice_id'], false, $_SESSION['u_id']);
    }

     if($in['order_inv']) {

       $order_id=str_replace(";","",$in['order_inv']);
          if($order_id){
             $this->dbu->query("UPDATE pim_articles_return SET invoiced='1' WHERE order_id='".$order_id."'");
          }
        }


    if($DRAFT_INVOICE_NO_NUMBER!='1' && ($in['type'] == 0 || $in['type'] ==3)){
      $ogm = generate_ogm(DATABASE_NAME,$in['invoice_id']);
      $this->dbu->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$in['invoice_id']."' ");
    }

    //notes                     =   '".utf8_encode($in['notes'])."',
    // add multilanguage order notes in note_fields
        $lang_note_id = $in['email_language'];
          if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }
/*        $langs = $this->dbu->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        while($langs->next()){
          foreach ($in['translate_loop'] as $key => $value) {
            if($value['lang_id'] == $langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($lang_note_id == $langs->f('lang_id')){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }*/
          // we have $in['notes'] only when we create invoices from to_invoice
         /* if($in['notes']){
            $lang_id = $this->dbu->field("SELECT lang_id FROM pim_lang WHERE active = '1' AND sort_order = 1");
            if($lang_id == $langs->f('lang_id')){
              $act_lang_note = 1;
            }else{
              $act_lang_note = 0;
            }
            if($in['from_to_invoice'] === true){
              if($lang_note_id == $langs->f('lang_id')){
                $act_lang_note = 1;
              }else{
                $act_lang_note = 0;
              }
            }
            $this->dbu->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['invoice_id']."',
                            lang_id         =   '".$langs->f('lang_id')."',
                            active          =   '".$act_lang_note."',
                            item_type         =   'invoice',
                            item_name         =   'notes',
                            item_value        =   '".$in['notes']."'
            ");
          }else{*/

            //Add default notes values based on language
            $emailLanguageDefaultNotesData = array('email_language' => $in['email_language'],
                                  'default_name_note' => 'invoice_terms',
                                  'default_type_note' => 'invoice_terms',
                                  'default_name_free_text_content' => '',
                                  'default_type_free_text_content' => '',
                                );

            $defaultNotesData = get_email_message_default_notes($emailLanguageDefaultNotesData);
            //End Add default notes values based on language

            $defaultNotesData['notes'] = stripslashes($in['NOTES']); 

            if($in['quote_id'] && !$has_invoice_id){
              if(stripslashes($in['NOTES']) == stripslashes($free_text_content)){
                  $defaultNotesData['notes'] = $free_text_content;
              } else {
                  $defaultNotesData['notes'] = stripslashes($in['NOTES']); 
              }
            }

            if($in['orders_id'] && empty($in['NOTES'])){
                $defaultNotesData['notes'] = '';
            } else if($in['orders_id'] && !empty($in['NOTES'])){
               $defaultNotesData['notes'] = stripslashes($in['NOTES']); 
            }

            if($in['project_id'] && empty($in['NOTES'])){
                $defaultNotesData['notes'] = '';
            } else if($in['project_id'] && !empty($in['NOTES'])){
               $defaultNotesData['notes'] = stripslashes($in['NOTES']); 
            }

            if($in['service_id'] && empty($in['NOTES'])){
                $defaultNotesData['notes'] = '';
            } else if($in['service_id'] && !empty($in['NOTES'])){
               $defaultNotesData['notes'] = stripslashes($in['NOTES']); 
            }
            if($in['contract_id'] && empty($in['NOTES'])){
                $defaultNotesData['notes'] = '';
            } else if($in['contract_id'] && !empty($in['NOTES'])){
               $defaultNotesData['notes'] = stripslashes($in['NOTES']); 
            }

            $this->dbu->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['invoice_id']."',
                            lang_id         =   '".$in['email_language']."',
                            active          =   '".$active_lang_note."',
                            item_type         =   'invoice',
                            item_name         =   'notes',
                            item_value        =   '".addslashes($defaultNotesData['notes'])."'
            ");
            //$in['NOTES']
         /* }*/

        /*}*/
        #custom extra languages
        $custom_lang_note_id = $in['email_language'];
        $custom_langs = $this->dbu->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
        while($custom_langs->next()) {
          foreach ($in['custom_translate_loop'] as $key => $value) {
            if($value['lang_id'] == $custom_langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($custom_lang_note_id == $custom_langs->f('lang_id')) {
            $custom_active_lang_note = 1;
          } else {
            $custom_active_lang_note = 0;
          }
          // we have in['notes'] only when we create invoices from to_invoice
         /* if($in['notes']) {
            $cust_lang_id = $this->dbu->field("SELECT lang_id FROM pim_custom_lang WHERE active = '1' AND sort_order = 1 ");
            if($cust_lang_id == $custom_langs->f('lang_id')) {
              $cust_act_lang_note = 1;
            } else {
              $cust_act_lang_note = 0;
            }
            if($in['from_to_invoice'] ===true) {
              if($custom_lang_note_id == $custom_langs->f('lang_id')) {
                $cust_act_lang_note = 1;
              } else {
                $cust_act_lang_note = 0;
              }
            }
            $this->dbu->insert("INSERT INTO note_fields SET   item_id     = '".$in['invoice_id']."',
                                      lang_id     = '".$custom_langs->f('lang_id')."',
                                      active      = '".$cust_act_lang_note."',
                                      item_type     = 'invoice',
                                      item_name     = 'notes',
                                      item_value    = '".$in['notes']."'
            ");
          } else {*/
            // if($in['orders_id']){
            //     if(empty($in['NOTES'])){
            //       $in['nota'] = '';  
            //     } else {
            //       $in['nota'] = stripslashes($in['NOTES']);
            //     }   
            // }

            $this->dbu->insert("INSERT INTO note_fields SET   item_id     = '".$in['invoice_id']."',
                                      lang_id     = '".$custom_langs->f('lang_id')."',
                                      active      = '".$custom_active_lang_note."',
                                      item_type     = 'invoice',
                                      item_name     = 'notes',
                                      item_value    = '".$in['nota']."' ");
         /* }*/
        }


    //INSERT LINES

    $invoice_total=0;
    $invoice_total_articles=0;
    $invoice_total_articles_purchase=0;
    $invoice_total_articles_for_margin =0;
    $line_vat = 0;
    $discount = return_value($in['discount']);
    if($in['apply_discount'] < 2){
      $discount = 0;
    }
    $disc = 0;
    $invoice_total_vat = 0;
    if(is_array($in['invoice_line'])){
    $i = 0;
    $margin_total=0;
    $discount_total=0;
    $last_combined = '';
      foreach ($in['invoice_line'] AS $nr => $nothing ){
        $line_total=0;

        //if($in['description'][$nr]){

          // $vat = (return_value($in['quantity'][$nr]) * return_value($in['price'][$nr])) * return_value($in['vat'][$nr])/100;

          $line_total=(return_value($nothing['quantity']) * return_value($nothing['price']));
          $line_discount = return_value($nothing['discount_line']);
          if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
            $line_discount = 0;
          }
          $line_total = round($line_total,ARTICLE_PRICE_COMMA_DIGITS);
          $disc_line = round($line_total * $line_discount / 100,ARTICLE_PRICE_COMMA_DIGITS);
            $discount_vat = ( $line_total - $disc_line ) * ( $discount / 100);
             $discount_total += $discount_vat;
            $invoice_total += $line_total - $disc_line - $discount_vat;
                  if($nothing['article_id']){
                    $invoice_total_articles += $line_total - $disc_line - $discount_vat;
                    $invoice_total_articles_purchase += return_value($nothing['purchase_price'])*return_value($nothing['quantity']);
                   }
            /*if($in['discount_line'][$nr]){
              $disc += (return_value($in['quantity'][$nr]) * (return_value($in['price'][$nr])*$in['discount_line'][$nr]/100) );
              $discount_vat = (return_value($in['quantity'][$nr]) * (return_value($in['price'][$nr])*$in['discount_line'][$nr]/100) );
            }*/
            //$invoice_total_vat += round(($line_total - $disc_line - $discount_vat) * (return_value($nothing['vat']) / 100),ARTICLE_PRICE_COMMA_DIGITS);
            $invoice_total_vat += ($line_total - $disc_line - $discount_vat) * (return_value($nothing['vat']) / 100);
            // article_id       = '".$in['article_id'][$nr]."',
            // $modifiable =($in['readonly'][$nr])? ", readonly = '1'":"";
                if($nothing['is_tax'] != 1 && $line_total>0){
                  $margin_total+=($line_total - $disc_line - $discount_vat)-(return_value($nothing['purchase_price'])*return_value($nothing['quantity']));
                  $invoice_total_articles_for_margin += $line_total - $disc_line - $discount_vat;
                }

                $use_combined = $this->dbu->field("SELECT use_combined FROM pim_articles WHERE article_id='".$nothing['article_id']."'");
                if($nothing['is_combined'] || (!$use_combined && !$nothing['component_for'])){
                    $last_combined = '';
                }

                if( $nothing['has_variants'] ){
                  $last_variant_parent = '';
                } 

                if($nothing['is_tax'] != 1){
                             $line_id = $this->dbu->insert("INSERT INTO tblinvoice_line SET
                                                name                =   '".htmlspecialchars($nothing['description'],ENT_QUOTES,'UTF-8')."',
                                                invoice_id          =   '".$in['invoice_id']."',
                                                quantity            =   '".return_value($nothing['quantity'])."',
                                                price               =   '".return_value($nothing['price'])."',
                                                amount              =   '".$line_total."',
                                                f_archived          =   '0',
                                                created             =   '".$created_date."',
                                                created_by          =   '".$_SESSION['u_id']."',
                                                last_upd            =   '".$created_date."',
                                                last_upd_by         =   '".$_SESSION['u_id']."',
                                                vat                                 =   '".return_value($nothing['vat'])."',
                                                discount                        =   '".return_value($nothing['discount_line'])."',
                                                content_title             = '".htmlentities($nothing['title'])."',
                                                content             = '".htmlentities($nothing['content'])."',
                                                article_id        = '".$nothing['article_id']."',
                                                purchase_price = '".return_value($nothing['purchase_price'])."',
                                                item_code         = '".addslashes($nothing['article_code'])."',
                                                sort_order            =   '".$i."',
                                                is_combined          = '".$nothing['is_combined']."',
                                                component_for         ='".$last_combined."',
                                                visible               ='".$nothing['visible']."',
                                                has_variants          = '".$nothing['has_variants']."',
                                                has_variants_done     ='".$nothing['has_variants_done']."',
                                                is_variant_for        ='".$nothing['is_variant_for']."',
                                                is_variant_for_line   ='".$last_variant_parent."',
                                                variant_type            ='".$nothing['variant_type']."' "
                                                .$modifiable);

                              if($nothing['is_combined']){
                                $last_combined = $line_id;
                              }

                              if($nothing['has_variants'] && $nothing['has_variants_done']){
                                $last_variant_parent = $line_id;
                              }
                            if($nothing['is_variant_for']){
                                $last_variant_parent = '';
                              }
                }else{



                   $line_id= $this->dbu->insert("INSERT INTO tblinvoice_line SET
                                                name                =   '".htmlspecialchars($nothing['description'],ENT_QUOTES,'UTF-8')."',
                                                invoice_id          =   '".$in['invoice_id']."',
                                                quantity            =   '".return_value($nothing['quantity'])."',
                                                price               =   '".return_value($nothing['price'])."',
                                                amount              =   '".$line_total."',
                                                f_archived          =   '0',
                                                created             =   '".$created_date."',
                                                created_by          =   '".$_SESSION['u_id']."',
                                                last_upd            =   '".$created_date."',
                                                last_upd_by         =   '".$_SESSION['u_id']."',
                                                vat                 = '".return_value($nothing['vat'])."',
                                                discount            = '".return_value($nothing['discount_line'])."',
                                                 content_title             = '".htmlentities($nothing['title'])."',
                                                content             = '".htmlentities($nothing['content'])."',

                                                purchase_price      = '".return_value($nothing['purchase_price'])."',
                                                item_code           = '".addslashes($nothing['article_code'])."',
                                                tax_id              = '".$nothing['tax_id']."',
                                                tax_for_article_id  = '".$nothing['tax_for_article_id']."',

                                                is_combined          = '',
                                                component_for         ='',
                                                visible               ='1',

                                                sort_order          = '".$i."' "
                                                .$modifiable);
                }
        //}

        if($nothing['task_time_ids']){
           $this->dbu->query("UPDATE task_time SET submited='1', billed='1', unapproved='0', invoice_id='".$in['invoice_id']."' WHERE task_time_id in (".rtrim($nothing['task_time_ids'],',').")");
        }

        if($nothing['expenses_ids']){
          $this->dbu->query("UPDATE project_expenses SET billed='1' WHERE id IN (".rtrim($nothing['expenses_ids'],',').")");
        }

        if($nothing['tasks_ids']){
          $this->dbu->query("UPDATE tasks SET closed='2' WHERE task_id='".$nothing['tasks_ids']."' ");
        }

        if($nothing['purchase_ids']){
          $this->dbu->query("UPDATE project_purchase SET invoiced='1' WHERE project_purchase_id='".$nothing['purchase_ids']."' ");
        }

        if($nothing['project_article_ids']){
          //$this->dbu->query("UPDATE project_articles SET billed='1' WHERE a_id='".$nothing['project_article_ids']."' ");
          $this->dbu->query("UPDATE service_delivery SET invoiced='1' WHERE id IN (".$nothing['project_article_ids'].") ");
        }

        if($nothing['deliveries']){
          $this->dbu->query("UPDATE pim_orders_delivery SET invoiced='1' WHERE order_delivery_id IN (".rtrim($nothing['deliveries'],',').") ");
        }

        if($nothing['order_ids']){
          /*$this->dbu->query("SELECT delivered FROM pim_order_articles WHERE order_id='".$nothing['order_ids']."' AND delivered='0'  AND content='0' and article_id!=0");*/
          $this->dbu->query("SELECT delivered FROM pim_order_articles WHERE order_id='".$nothing['order_ids']."' AND delivered='0'  AND content='0' and (article_id!=0 OR (article_id=0 AND tax_id=0)) ");
          if(!$this->dbu->next()){
            $this->dbu->query("UPDATE pim_orders SET invoiced='1' WHERE order_id='".$nothing['order_ids']."' ");
          }
        }

        if($nothing['service_delivery_ids']){
          $this->dbu->query("UPDATE service_delivery SET invoiced='1' WHERE id IN (".$nothing['service_delivery_ids'].") ");
        }

        $i++;
      }
    }
    if(!empty($in['projects_ids'])){
      foreach ($in['projects_ids'] as $key) {
        $this->dbu->query("INSERT INTO item_links SET `from`='project_".$key."', `to`='invoice_".$in['invoice_id']."' ");
        if(!empty($in['timeframes'][$key])){
          $this->dbu->query("INSERT INTO tblinvoice_projects SET `invoice_id`='{$in['invoice_id']}', `project_id`='{$key}', `start_date`='{$in['timeframes'][$key]['timeframe_start']}', `end_date`='{$in['timeframes'][$key]['timeframe_end']}' ");
        }
      }
    }

    /*$discount = $invoice_total * (return_value($in['discount']) / 100);
    if($disc){
      $discount = $disc;
    }*/
    // $invoice_total = $invoice_total - $discount + $line_vat;
    // $invoice_total = $invoice_total - $discount;

    $invoice_total=round($invoice_total,ARTICLE_PRICE_COMMA_DIGITS);
    $invoice_total_vat=round($invoice_total_vat,ARTICLE_PRICE_COMMA_DIGITS);
    if($in['c_quote_id'] && $in['downpayment']==2){
      /*$downpayment_percent=$this->dbu->field("SELECT downpayment_val FROM tblquote WHERE id='".$in['c_quote_id']."' ");
      $downpayment_val=$invoice_total*$downpayment_percent/100;*/
      $downpayment_val=$this->dbu->field("SELECT amount FROM tblinvoice WHERE downpayment_inv='1' AND quote_id='".$in['c_quote_id']."' ");
      $invoice_total-=$downpayment_val;
      $invoice_total_vat += $invoice_total;
      $this->dbu->query("UPDATE tblinvoice SET downpayment_drawn='1', downpayment_value='".$downpayment_val."' WHERE id='".$in['invoice_id']."'");
    }else{
      $invoice_total_vat += $invoice_total;
    }
    $margin_total=$margin_total-$discount_total;
    $margin_percent_total =0;
    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
         if($invoice_total_articles_purchase){
           $margin_percent_total=($margin_total/$invoice_total_articles_purchase)*100;
          }else{
            $margin_percent_total=100;
          }
        
     }else{
         if($invoice_total_articles_for_margin){
            $margin_percent_total=($margin_total/$invoice_total_articles_for_margin)*100;
          }else{
            $margin_percent_total=100;
          }
     }

    if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
      if($in['currency_type']){
        $invoice_total = $invoice_total*return_value($in['currency_rate']);
        $invoice_total_articles = $invoice_total_articles*return_value($in['currency_rate']);
        $invoice_total_vat = $invoice_total_vat*return_value($in['currency_rate']);
      }
    }
    $this->dbu->query("UPDATE tblinvoice SET amount='".$invoice_total."', amount_vat='".$invoice_total_vat."' WHERE id='".$in['invoice_id']."'");
    if(!$in['debug']){ # we dont need any output when we create invoice from the to_invoice controller
      msg::success (gm('Invoice was created'),'success');
      //$in['pag'] = 'invoice';
      insert_message_log($this->pag,'{l}Invoice was created by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);
      $in['pagl'] = $this->pag;
    }
    if($in['type'] == 2){
      // $this->dbu->query("UPDATE tblinvoice SET status='1', sent='1',paid='1' WHERE id='{$in['invoice_id']}' ");
    }

    if($in['quote_id']){
      $this->dbu->query("UPDATE tblquote SET transformation = '1' WHERE id = '".$in['quote_id']."' ");
    }    

    if($in['c_quote_id']){
      $this->dbu->query("UPDATE tblquote SET pending = '0' WHERE id = '".$in['c_quote_id']."' ");
      if($in['downpayment']){
        $this->dbu->query("UPDATE tblquote SET downpayment='".$in['downpayment']."' WHERE id = '".$in['c_quote_id']."' ");
      }
    }
    $this->dbu->query("INSERT INTO icepay_orders SET item='".$in['invoice_id']."', type='1' ");
    #type=1 for invoices

    /*$show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                        AND name    = 'invoice-invoices_show_info'  ");*/
    $show_info=$this->dbu_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                        AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'invoice-invoices_show_info']);                                        
    if(!$show_info->move_next()) {
      /*$this->dbu_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                name = 'invoice-invoices_show_info',
                                value = '1' ");*/
      $this->dbu_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                name = :name,
                                value = :value ",
                              ['user_id' => $_SESSION['u_id'],
                               'name' => 'invoice-invoices_show_info',
                               'value' => '1']
                            );                                
    } else {
      /*$this->dbu_users->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
                                    AND name    = 'invoice-invoices_show_info' ");*/
      $this->dbu_users->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
                                    AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'invoice-invoices_show_info']);                                    
    }

    if(count($in['art_serial_numbers'])>0){
      foreach ($in['art_serial_numbers'] as $art_s_n_id => $art_s_n_val) {
        $this->dbu->query("UPDATE serial_numbers  SET invoice_id = '".$in['invoice_id']."' WHERE id= '".$art_s_n_id."' ");
      }
    }

    if($in['service_id']){
      $this->dbu->query("UPDATE servicing_support SET billed='1' WHERE service_id='".$in['service_id']."' ");
      //$this->dbu->query("UPDATE servicing_support_articles SET billed='1' WHERE service_id='".$in['service_id']."' AND billable='1' AND article_delivered='1' ");
      $cost_center=$this->dbu->field("SELECT cost_centre FROM servicing_support WHERE service_id='".$in['service_id']."' ");
      if($cost_center && DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'){
          $this->db->query("UPDATE tblinvoice SET cost_centre='".$cost_center."' WHERE id='".$in['invoice_id']."' ");
      }
    }
    if($in['ticket_id']){
      $this->dbu->query("UPDATE cash_tickets SET invoice_id='".$in['invoice_id']."' WHERE id='".$in['ticket_id']."' ");
    }

    if($in['c_invoice_id']){
          $amount_c = $this->dbu->query("SELECT amount, amount_vat FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
          $amount = $this->dbu->query("SELECT amount, amount_vat FROM tblinvoice WHERE id='".$in['c_invoice_id']."' ");
          $info = '<a href="index.php?do=invoice-invoice&invoice_id='.$in['invoice_id'].'" >'.gm('Credit Invoice').'</a> ';
          $payment_date = date('Y-m-d');

          $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['c_invoice_id']."' AND credit_payment='0' ");
          $this->dbu->move_next();
          $total_payed = $this->dbu->f('total_payed');

          $negative = 1;
          if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
            $negative = -1;
          }

          $this->dbu->query("SELECT amount FROM tblinvoice_payments WHERE invoice_id='".$in['c_invoice_id']."'  AND credit_payment='1' ");
          while($this->dbu->next()){
            $total_payed +=($this->dbu->f('amount') < 0 ? abs($this->dbu->f('amount')) : $this->dbu->f('amount'));
          }

          $c_invoice_amount=$amount_c->f('amount_vat') < 0 ? abs($amount_c->f('amount_vat')) : $amount_c->f('amount_vat');
          if($c_invoice_amount + $total_payed >= $amount->f('amount_vat')){
            $this->dbu->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$in['c_invoice_id']."' ");
            $paid = 1;
          }else{
            $this->dbu->query("UPDATE tblinvoice SET paid='2',status='1' WHERE id='".$in['c_invoice_id']."' ");
            $paid = 2;
          }
          $this->dbu->query("INSERT INTO tblinvoice_payments SET
                              invoice_id  = '".$in['c_invoice_id']."',
                              date    = '".$payment_date."',
                              amount    = '".$amount_c->f('amount_vat')."',
                              info        = '".$info."',
                              credit_payment='1' ");

           $pro_inv = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['c_invoice_id']."' ");
           if($pro_inv){
              $this->dbu->query("UPDATE tblinvoice SET paid='".$paid."',status='1' WHERE id='".$pro_inv."' ");
           }

      }

    # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
    $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
    $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $in['email_language'];
     $params['type_pdf'] =  $inv->f('type');
    $params['save_as'] = 'F';
    //$this->generate_pdf($params);
    # for pdf

        $this->dbu->query("UPDATE tblinvoice SET margin='".$margin_total."', margin_percent='".$margin_percent_total."' WHERE id='".$in['invoice_id']."'");

        $count = $this->dbu->field("SELECT COUNT(id) FROM tblinvoice ");
      if($count == 1){
        doManageLog('Created the first invoice.','',array( array("property"=>'first_module_usage',"value"=>'Invoice') ));
      }

      if($in['invoice_line'][0]['double_customer']){
        $track_start=1;
      }else{
        $track_start=0;
      }
      if(!empty($in['invoice_line']) && $in['invoice_line'][$track_start]['origin_id']){
        $tracking_type=($in['type']=='2' || $in['type']=='4') ? '8' : ($in['type']=='1' ? '7' : '1');
        $tracking_data=array(
            'target_id'         => $in['invoice_id'],
            'target_type'       => $tracking_type,
            'target_buyer_id'   => $in['buyer_id'],
            'lines'             => array(
                                          array('origin_id'=>$in['invoice_line'][$track_start]['origin_id'],'origin_type'=>$in['invoice_line'][$track_start]['origin_type'])
                                    )
          );
        addTracking($tracking_data);
      }
      if((empty($in['invoice_line']) || (!empty($in['invoice_line']) && !$in['invoice_line'][$track_start]['origin_id']))){
        $tracking_line=array();
        if($in['duplicate_invoice_id']){
          $original_duplicate_serial=$this->db->field("SELECT serial_number FROM tblinvoice WHERE id='".$in['duplicate_invoice_id']."' ");
          $origin_type=$this->db->field("SELECT type FROM tblinvoice WHERE id='".$in['duplicate_invoice_id']."' ");
          $tracking_line=array(array('origin_id'=>$in['duplicate_invoice_id'],'origin_type'=>($origin_type=='2' || $origin_type=='4') ? '8' : ($origin_type=='1' ? '7' : '1')));

          //on the original invoice
          insert_message_log('invoice','{l}This invoice was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} <a href="[SITE_URL]invoice/view/'.$in['invoice_id'].'">{l}Invoice{endl} '.$serial_number.'</a>',$this->field_n,$in['duplicate_invoice_id']);


          //on the resulting invoice
          insert_message_log('invoice','{l}This invoice was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}from{endl} <a href="[SITE_URL]invoice/view/'.$in['duplicate_invoice_id'].'">{l}Invoice{endl} '.$original_duplicate_serial.'</a>',$this->field_n,$in['invoice_id']);

          //insert_message_log($this->pag,'{l}Invoice was created by duplicating{endl} '.$original_duplicate_serial,$this->field_n,$in['invoice_id']);
        }else{
          if($in['base_type']){
              $lineTr = $this->getTrackingLine($in);
              if(!empty($lineTr)){
                $tracking_line = array($lineTr); 
                if($lineTr['origin_type'] =='4'){
                  $order_items_delivered=$this->dbu->query("SELECT delivered FROM pim_order_articles WHERE order_id='".$lineTr['origin_id']."' AND delivered='0'  AND content='0' and (article_id!=0 OR (article_id=0 AND tax_id=0)) ");
                  if(!$order_items_delivered->next()){
                    $this->dbu->query("UPDATE pim_orders SET invoiced='1' WHERE order_id='".$lineTr['origin_id']."' ");
                  }
                } 
              }
          }
        }
        $tracking_type=($in['type']=='2' || $in['type']=='4') ? '8' : ($in['type']=='1' ? '7' : '1');      

        $tracking_data=array(
            'target_id'         => $in['invoice_id'],
            'target_type'       => $tracking_type,
            'target_buyer_id'   => $in['buyer_id'],
            'lines'             => $tracking_line
          );
        addTracking($tracking_data);
      }
      if($in['deal_id']){
        $trace_id=$this->db->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
        if($trace_id){
          $linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
              INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
              WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
          if($linked_doc){
            $this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' "); 
          }else{
            $this->db->query("INSERT INTO tracking_line SET
                origin_id='".$in['deal_id']."',
                origin_type='11',
                trace_id='".$trace_id."' ");
          }
        }else{
          $tracking_data=array(
                  'target_id'         => $in['invoice_id'],
                  'target_type'       => '1',
                  'target_buyer_id'   => $in['buyer_id'],
                  'lines'             => array(
                                                array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
                                          )
                );
          addTracking($tracking_data);
        }
      }
      $this->generate_pdf($params);
    return true;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author PM
   **/
  function getArticlePrice($in){
    $cat_id = $in['cat_id'];
    $article = $this->dbu->query("SELECT article_category_id, block_discount,price_type FROM pim_articles WHERE article_id='".$in['article_id']."' ");
    if($article->f('price_type')==1){

        $price_value_custom_fam=$this->dbu->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");
          $pim_article_price_category_custom=$this->dbu->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$in['article_id']."' and category_id='".$cat_id."' ");

          if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
              $price=$this->dbu->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$in['article_id']."'");
          }else{
              $price_value=$price_value_custom_fam;

             //we have to apply to the base price the category spec
          $cat_price_type=$this->dbu->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
            $cat_type=$this->dbu->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
            $price_value_type=$this->dbu->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

            if($cat_price_type==2){
                  $article_base_price=get_article_calc_price($in['article_id'],3);
              }else{
                  $article_base_price=get_article_calc_price($in['article_id'],1);
              }

            switch ($cat_type) {
          case 1:                  //discount
            if($price_value_type==1){  // %
              $price = $article_base_price - $price_value * $article_base_price / 100;
            }else{ //fix
              $price = $article_base_price - $price_value;
            }
            break;
          case 2:                 //profit margin
            if($price_value_type==1){  // %
              $price = $article_base_price + $price_value * $article_base_price / 100;
            }else{ //fix
              $price =$article_base_price + $price_value;
            }
            break;
        }
          }

        if(!$price || $article->f('block_discount')==1 ){
            $price=$this->dbu->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$in['article_id']."' AND base_price=1");
          }
      }else{
        $price = $in['price'];
        // return $this->get_article_quantity_price($in);
      }

      $start= mktime(0, 0, 0);
      $end= mktime(23, 59, 59);
      $promo_price=$this->dbu->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$in['article_id']."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
      if($promo_price->move_next()){
        if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

          }else{
              $price=$promo_price->f('price');
          }
      }
    if($in['customer_id']){
        $customer_custom_article_price=$this->dbu->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
        if($customer_custom_article_price->move_next()){
              $price = $customer_custom_article_price->f('price');
          }
      }
      return $price;
  }

  function get_article_quantity_price(&$in)
  {
      if($in['customer_id']){
        $customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
        if($customer_custom_article_price->move_next()){
          $price = $customer_custom_article_price->f('price');
          if($in['asString']){
            return $price;
          }
          $in['new_price']=$price;
          json_out($in);
          return true;
        }
      }
    $price_type=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
      $is_block_discount=$this->db->query("SELECT pim_articles.block_discount FROM pim_articles
                               WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.block_discount='1'");

      if($is_block_discount->next()){
        $price=return_value($in['price']);
    }else{
      if($price_type==1){
          $price=return_value($in['price']);
        }else{
          $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices
                                 WHERE pim_article_prices.from_q<='".$in['quantity']."'
                                 AND  pim_article_prices.article_id='".$in['article_id']."'
                                     ORDER BY pim_article_prices.from_q DESC
                                     LIMIT 1
                                 ");

          if(!$price){
            $price=return_value($in['price']);
          }
        }
    }
    if($in['asString']){
          return $price;
        }
      $in['new_price']=$price;
      json_out($in);
    return true;
  }

  function uploadify(&$in)
  {
    $response=array();
    if (!empty($_FILES)) {
      $tempFile = $_FILES['Filedata']['tmp_name'];
      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
      $targetPath = 'upload/'.DATABASE_NAME;
      $in['name'] = 'logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
      // Validate the file type
      $fileTypes = array('jpg','jpeg','gif'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      @mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
        move_uploaded_file($tempFile,$targetFile);
        global $database_config;

        $database_2 = array(
          'hostname' => $database_config['mysql']['hostname'],
          'username' => $database_config['mysql']['username'],
          'password' => $database_config['mysql']['password'],
          'database' => DATABASE_NAME,
        );
        $db_upload = new sqldb($database_2);
        $logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO' ");
        if($logo == ''){
          if(defined('ACCOUNT_LOGO')){
            $db_upload->query("UPDATE settings SET
                               value = 'upload/".DATABASE_NAME."/".$in['name']."'
                               WHERE constant_name='ACCOUNT_LOGO' ");
          }else{
            $db_upload->query("INSERT INTO settings SET
                               value = 'upload/".DATABASE_NAME."/".$in['name']."',
                               constant_name='ACCOUNT_LOGO' ");
          }
        }
        $response['success'] = 'success';
        echo json_encode($response);
        // ob_clean();
      } else {
        $response['error'] = gm('Invalid file type.');
        echo json_encode($response);
        // echo gm('Invalid file type.');
      }
    }
  }
  function delete_logo(&$in)
  {
      if($in['name'] == ACCOUNT_LOGO){
        msg::error ( gm('You cannot delete the default logo'),'error');
        json_out($in);
        return true;
      }
      if($in['name'] == '../img/no-logo.png'){
        msg::error ( gm('You cannot delete the default logo'),'error');
        json_out($in);
        return true;
      }
      $exists = $this->dbu->field("SELECT COUNT(service_id) FROM servicing_support WHERE pdf_logo='".$in['name']."' ");
      if($exists>=1){
        msg::error ( gm('This logo is set for one ore more invoices.'),'error');
        json_out($in);
        return false;
      }
      @unlink($in['name']);
      msg::success ( gm('File deleted'),'success');
      return true;
  }
  function set_default_logo(&$in)
  {

      $this->dbu->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_LOGO' ");
      msg::success (gm("Default logo set."),'success');
      json_out($in);
      return true;
  }
  function settings(&$in){

    $this->dbu->query("UPDATE settings SET value='".(int)$in['show_bank']."' WHERE constant_name='SHOW_BANK_DETAILS' ");
    $this->dbu->query("UPDATE settings SET value='".(int)$in['show_stamp']."' WHERE constant_name='SHOW_STAMP_SIGNATURE' ");
    $this->dbu->query("UPDATE settings SET value='".(int)$in['show_page']."' WHERE constant_name='USE_INVOICE_PAGE_NUMBERING' ");
    $this->dbu->query("UPDATE settings SET value='".(int)$in['show_paid']."' WHERE constant_name='SHOW_PAID_ON_INVOICE_PDF' ");

    msg::success ( gm('Changes saved'),'success');
    /*json_out($in);*/
    return true;
  }
  function default_pdf_format_header(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_INVOICE_HEADER_PDF_FORMAT'");
      $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      $this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOICE_PDF_MODULE'");
    }
      global $config;
      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");
      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function default_pdf_format_body(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_INVOICE_BODY_PDF_FORMAT'");
      $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      $this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOIE_PDF_MODULE'");
    }
      global $config;
      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function default_pdf_format_footer(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_INVOICE_FOOTER_PDF_FORMAT'");
      $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      $this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOICE_PDF_MODULE'");
    }
      global $config;
      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
    function default_pdf_format_header_credit(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT'");
      $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      $this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOICE_CREDIT_PDF_MODULE'");
    }
      global $config;
      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");
      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function default_pdf_format_body_credit(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_INVOICE_CREDIT_BODY_PDF_FORMAT'");
      $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      $this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOIE_CREDIT_PDF_MODULE'");
    }
      global $config;
      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function default_pdf_format_footer_credit(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_INVOICE_CREDIT_FOOTER_PDF_FORMAT'");
      $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      $this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOICE_CREDIT_PDF_MODULE'");
    }
      global $config;
      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }

      function default_pdf_format_header_reminder(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

        $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");

        $setting_exist=$this->dbu->field("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_INVOICE_REMINDER_HEADER_PDF_FORMAT' ");
        if(is_null($setting_exist)){
          $this->dbu->query("INSERT INTO settings SET constant_name='ACCOUNT_INVOICE_REMINDER_HEADER_PDF_FORMAT', value='".$in['type']."', module='billing' ");
        }else{
          $this->dbu->query("UPDATE settings SET value='".$in['type']."' WHERE constant_name='ACCOUNT_INVOICE_REMINDER_HEADER_PDF_FORMAT' ");
        }

     // $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      //$this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      //$this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOICE_REMINDER_PDF_MODULE'");
    }

      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function default_pdf_format_body_reminder(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");

      $setting_exist=$this->dbu->field("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_INVOICE_REMINDER_BODY_PDF_FORMAT' ");
        if(is_null($setting_exist)){
          $this->dbu->query("INSERT INTO settings SET constant_name='ACCOUNT_INVOICE_REMINDER_BODY_PDF_FORMAT', value='".$in['type']."', module='billing' ");
        }else{
          $this->dbu->query("UPDATE settings SET value='".$in['type']."' WHERE constant_name='ACCOUNT_INVOICE_REMINDER_BODY_PDF_FORMAT' ");
        }

      //$this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
     // $this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      //$this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOIE_REMINDER_PDF_MODULE'");
    }
 
      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function default_pdf_format_footer_reminder(&$in)
  {

/*    if(!$in['type']){
      msg::$error = gm('Invalid ID');
      return false;
    }*/

    if($in['identity_id']>0){
        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='invoice' ");
        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='invoice' ");

      }else{

      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");

      $setting_exist=$this->dbu->field("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_INVOICE_REMINDER_FOOTER_PDF_FORMAT' ");
        if(is_null($setting_exist)){
          $this->dbu->query("INSERT INTO settings SET constant_name='ACCOUNT_INVOICE_REMINDER_FOOTER_PDF_FORMAT', value='".$in['type']."', module='billing' ");
        }else{
          $this->dbu->query("UPDATE settings SET value='".$in['type']."' WHERE constant_name='ACCOUNT_INVOICE_REMINDER_FOOTER_PDF_FORMAT' ");
        }

     // $this->dbu->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
      //$this->dbu->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

      //$this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOICE_REMINDER_PDF_MODULE'");
    }

      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }

  function reset_data(&$in){
    if($in['header'] && $in['header']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
    }elseif($in['footer'] && $in['footer']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
    }else if($in['body'] && $in['body']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice' AND type='".$in['body']."' AND initial='0' AND layout='".$in['layout']."'");
    }
    $result=array("var_data" => $variable_data);
    json_out($result);
    return true;
  }
  function reset_datacredit(&$in){
    if($in['header'] && $in['header']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
    }elseif($in['footer'] && $in['footer']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
    }else if($in['body'] && $in['body']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['body']."' AND initial='0' AND layout='".$in['layout']."'");
    }
    $result=array("var_data" => $variable_data);
    json_out($result);
    return true;
  }
    function reset_datareminder(&$in){
    if($in['header'] && $in['header']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_reminder' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
    }elseif($in['footer'] && $in['footer']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_reminder' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
    }else if($in['body'] && $in['body']!='undefined'){
      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_reminder' AND type='".$in['body']."' AND initial='0' AND layout='".$in['layout']."'");
    }
    $result=array("var_data" => $variable_data);
    json_out($result);
    return true;
  }
  function pdfSaveData(&$in){

    if($in['header']=='header'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice' AND type='".$in['header']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice',
                          type='".$in['header']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
    }
    if($in['footer']=='footer'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice' AND type='".$in['footer']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice',
                          type='".$in['footer']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
    }
    if($in['body']=='body'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice' AND type='".$in['body']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice' AND type='".$in['body']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice',
                          type='".$in['body']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
      $body_l=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_INVOICE_BODY_POS'");
      if(!is_null($body_l)){
        $this->db->query("UPDATE settings SET value='".$in['body_l']."' WHERE `constant_name`='ACCOUNT_INVOICE_BODY_POS'");
      }else{
        $this->db->query("INSERT INTO settings SET `constant_name`='ACCOUNT_INVOICE_BODY_POS',value='".$in['body_l']."' ");
      }
    }
    msg::success ( gm('Data saved'),'success');
    return true;
  }
  function pdfSaveDataCredit(&$in){

    if($in['header']=='header'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice_credit' AND type='".$in['header']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice_credit',
                          type='".$in['header']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
    }
    if($in['footer']=='footer'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice_credit' AND type='".$in['footer']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice_credit',
                          type='".$in['footer']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
    }
    if($in['body']=='body'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice_credit' AND type='".$in['body']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['body']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice_credit',
                          type='".$in['body']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
      $body_l=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_CREDIT_INVOICE_BODY_POS'");
      if(!is_null($body_l)){
        $this->db->query("UPDATE settings SET value='".$in['body_l']."' WHERE `constant_name`='ACCOUNT_CREDIT_INVOICE_BODY_POS'");
      }else{
        $this->db->query("INSERT INTO settings SET `constant_name`='ACCOUNT_CREDIT_INVOICE_BODY_POS',value='".$in['body_l']."' ");
      }
    }
    msg::success ( gm('Data saved'),'success');
    return true;
  }
  function pdfSaveDataReminder(&$in){

    if($in['header']=='header'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice_reminder' AND type='".$in['header']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_reminder' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice_reminder',
                          type='".$in['header']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
    }
    if($in['footer']=='footer'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice_reminder' AND type='".$in['footer']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_reminder' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice_reminder',
                          type='".$in['footer']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
    }
    if($in['body']=='body'){
      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='invoice_reminder' AND type='".$in['body']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_reminder' AND type='".$in['body']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

      if($exist){
        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
      }else{
        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='invoice_reminder',
                          type='".$in['body']."',
                          content='".$in['variable_data']."',
                          initial='1',
                          layout='".$in['layout']."',
                          `default`='1',
                          identity_id='".$in['identity_id']."' ");
      }
      $body_l=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_REMINDER_INVOICE_BODY_POS'");
      if(!is_null($body_l)){
        $this->db->query("UPDATE settings SET value='".$in['body_l']."' WHERE `constant_name`='ACCOUNT_REMINDER_INVOICE_BODY_POS'");
      }else{
        $this->db->query("INSERT INTO settings SET `constant_name`='ACCOUNT_REMINDER_INVOICE_BODY_POS',value='".$in['body_l']."' ");
      }
    }
    msg::success ( gm('Data saved'),'success');
    return true;
  }

  function Convention(&$in){

    if($in['account_number']){
      $this->dbu->query("UPDATE settings SET value='".$in['account_number']."' WHERE constant_name='ACCOUNT_INVOICE_START'");
    }

    if($in['account_digits']){
      if($in['account_digits'] > 11){
        $in['account_digits'] = 11;
      }
      if(!defined('ACCOUNT_DIGIT_NR')){
        $this->dbu->query("INSERT INTO settings SET value='".$in['account_digits']."', constant_name='ACCOUNT_DIGIT_NR', module='billing' ");
      }else{
        $this->dbu->query("UPDATE settings SET value='".$in['account_digits']."' WHERE constant_name='ACCOUNT_DIGIT_NR'");
      }
    }
    if($in['account_credit']){
      $this->dbu->query("UPDATE settings SET value='".$in['account_credit']."' WHERE constant_name='ACCOUNT_CREDIT_INVOICE_START'");
    }
    if($in['account_credit_digits']){
      if($in['account_credit_digits'] > 11){
        $in['account_credit_digits'] = 11;
      }
      if(!defined('ACCOUNT_CREDIT_DIGIT_NR')){
        $this->dbu->query("INSERT INTO settings SET value='".$in['account_credit_digits']."', constant_name='ACCOUNT_CREDIT_DIGIT_NR', module='billing' ");
      }else{
        $this->dbu->query("UPDATE settings SET value='".$in['account_credit_digits']."' WHERE constant_name='ACCOUNT_CREDIT_DIGIT_NR'");
      }
    }
    if($in['account_proforma']){
      $this->dbu->query("UPDATE settings SET value='".$in['account_proforma']."' WHERE constant_name='ACCOUNT_PROFORMA_INVOICE_START'");
    }
    if($in['account_proforma_digits']){
      if($in['account_proforma_digits'] > 11){
        $in['account_proforma_digits'] = 11;
      }
      if(!defined('ACCOUNT_PROFORMA_DIGIT_NR')){
        $this->dbu->query("INSERT INTO settings SET value='".$in['account_proforma_digits']."', constant_name='ACCOUNT_PROFORMA_DIGIT_NR', module='billing' ");
      }else{
        $this->dbu->query("UPDATE settings SET value='".$in['account_proforma_digits']."' WHERE constant_name='ACCOUNT_PROFORMA_DIGIT_NR'");
      }
    }
    if($in['account_purchase']){
      if(!defined('ACCOUNT_PURCHASE_INVOICE_START')){
        $this->dbu->query("INSERT INTO settings SET value='".$in['account_purchase']."', constant_name='ACCOUNT_PURCHASE_INVOICE_START', module='billing' ");
      }else{
        $this->dbu->query("UPDATE settings SET value='".$in['account_purchase']."' WHERE constant_name='ACCOUNT_PURCHASE_INVOICE_START'");
      }
    }
    if($in['account_purchase_digits']){
      if($in['account_purchase_digits'] > 11){
        $in['account_purchase_digits'] = 11;
      }
      if(!defined('ACCOUNT_PURCHASE_DIGIT_NR')){
        $this->dbu->query("INSERT INTO settings SET value='".$in['account_purchase_digits']."', constant_name='ACCOUNT_PURCHASE_DIGIT_NR', module='billing' ");
      }else{
        $this->dbu->query("UPDATE settings SET value='".$in['account_purchase_digits']."' WHERE constant_name='ACCOUNT_PURCHASE_DIGIT_NR'");
      }
    }
    $this->dbu->query("UPDATE settings SET value='".$in['use_credit_number']."' WHERE constant_name='USE_CREDIT_PURCHASE_NO'");
    if($in['account_purchase_credit_digits']){
      if($in['account_purchase_credit_digits'] > 11){
        $in['account_purchase_credit_digits'] = 11;
      }
      if(!defined('USE_CREDIT_PURCHASE_NO')){
        $this->dbu->query("INSERT INTO settings SET value='".$in['use_credit_number']."', constant_name='USE_CREDIT_PURCHASE_NO', module='billing' ");
      }else{
        $this->dbu->query("UPDATE settings SET value='".$in['use_credit_number']."' WHERE constant_name='USE_CREDIT_PURCHASE_NO'");
      }
      //if($in['use_credit_purchase']){
      if($in['use_credit_number']){
        $this->dbu->query("UPDATE settings SET value='".$in['account_purchase_credit']."' WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_INVOICE_START'");
        $this->dbu->query("UPDATE settings SET value='".$in['account_purchase_credit_digits']."' WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_DIGIT_NR'");
      }
    }
    if(!defined('USE_OGM')){
      $this->dbu->query("INSERT INTO settings SET value='".$in['payment_instruction']."', constant_name='USE_OGM', module='billing' ");
    }else{
      $this->dbu->query("UPDATE settings SET value='".$in['payment_instruction']."' WHERE constant_name='USE_OGM'");
    }
    if(!defined('DRAFT_INVOICE_NO_NUMBER')){
      $this->dbu->query("INSERT INTO settings SET value='".$in['draft_invoices']."', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
    }else{
      $this->dbu->query("UPDATE settings SET value='".$in['draft_invoices']."' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
    }
    if($in['random_digits']){
      if(!defined('ACCOUNT_PAYMENT_INS_OPTION')){
        $this->dbu->query("INSERT INTO settings SET value='".$in['random_digits']."', constant_name='ACCOUNT_PAYMENT_INS_OPTION', module='billing' ");
      }else{
        $this->dbu->query("UPDATE settings SET value='".$in['random_digits']."' WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION'");
      }
    }
    if(!defined('ACCOUNT_PAYMENT_INS_OPTION_NR')){
      $this->dbu->query("INSERT INTO settings SET value='".$in['last_serial_number']."', constant_name='ACCOUNT_PAYMENT_INS_OPTION_NR', module='billing' ");
    }else{
      $this->dbu->query("UPDATE settings SET value='".$in['last_serial_number']."' WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION_NR'");
    }
    $invoice_starting_number=$this->dbu->field("SELECT value FROM settings WHERE constant_name='INVOICE_STARTING_NUMBER' ");
    if(is_null($invoice_starting_number)){
      $this->dbu->query("INSERT INTO settings SET value='".$in['invoice_starting_number']."', constant_name='INVOICE_STARTING_NUMBER'");
    }else{
      $this->dbu->query("UPDATE settings SET value='".$in['invoice_starting_number']."' WHERE constant_name='INVOICE_STARTING_NUMBER'");
    }
    /*$this->dbu->query("UPDATE settings SET value='".$in['account_digits']."' WHERE constant_name='ACCOUNT_DIGIT_NR' ");*/
    /*$this->dbu->query("UPDATE settings SET value='".$in['account_credit']."' WHERE constant_name='ACCOUNT_CREDIT_INVOICE_START'");*/
    /*$this->dbu->query("UPDATE settings SET value='".$in['account_credit_digits']."' WHERE constant_name='ACCOUNT_CREDIT_DIGIT_NR'");*/
    /*$this->dbu->query("UPDATE settings SET value='".$in['account_purchase']."' WHERE constant_name='ACCOUNT_PURCHASE_INVOICE_START'");*/
    /*$this->dbu->query("UPDATE settings SET value='".$in['account_purchase_digits']."' WHERE constant_name='ACCOUNT_PURCHASE_DIGIT_NR'");*/
/*    $this->dbu->query("UPDATE settings SET value='".$in['use_credit_number']."' WHERE constant_name='USE_CREDIT_PURCHASE_NO'");
    $this->dbu->query("UPDATE settings SET value='".$in['account_purchase_credit']."' WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_INVOICE_START'");
    $this->dbu->query("UPDATE settings SET value='".$in['account_purchase_credit_digits']."' WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_DIGIT_NR'");*/
    /*$this->dbu->query("UPDATE settings SET value='".$in['payment_instruction']."' WHERE constant_name='USE_OGM'");*/
    /*$this->dbu->query("UPDATE settings SET value='".$in['draft_invoices']."' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");*/
/*    $this->dbu->query("UPDATE settings SET value='".$in['random_digits']."' WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION'");
    $this->dbu->query("UPDATE settings SET value='".$in['last_serial_number']."' WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION_NR'");*/


      msg::success ( gm("Changes have been saved."),'success');
    return true;

  }
  function default_language(&$in){
   msg::success ( gm("Changes have been saved."),'success');
    return true;

  }
  function default_message(&$in)
  {
    $set = "text  = '".$in['text']."', ";
    /*if($in['use_html']){*/
      $set1 = "html_content  = '".$in['html_content']."' ";
    /*}*/
    $this->dbu->query("UPDATE sys_message SET
          subject = '".$in['subject']."',
          ".$set."
          ".$set1."
          WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
    /*$this->dbu->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
    msg::success( gm('Message has been successfully updated'),'success');
    // json_out($in);
    return true;
  }
  function update_default_email(&$in)
  {
    if($in['bcc_email']){
      $bcc_email=$in['bcc_email'];
      $in['bcc_email'] = str_replace(';', ',', $in['bcc_email']);   
    }
    if(!$this->validate_default_email($in)){
      return false;
    }
    $in['bcc_email']=$bcc_email;
     $this->dbu->query("SELECT * FROM default_data WHERE type='invoice_email' ");
     if($this->dbu->next()){
        $this->dbu->query("UPDATE default_data SET default_name='".$in['default_name']."', value='".$in['email_value']."' WHERE type='invoice_email' ");
     }else{
      $this->dbu->query("INSERT INTO default_data SET type='invoice_email',default_name='".$in['default_name']."', value='".$in['email_value']."' ");
     } 
    $this->dbu->query("SELECT * FROM default_data WHERE type='invoice_manager_email' ");
    if($this->dbu->next()){
      $this->dbu->query("UPDATE default_data SET value='".$in['manag_email']."' WHERE type='invoice_manager_email' ");
    }else{
      $this->dbu->query("INSERT INTO default_data SET value='".$in['manag_email']."', type='invoice_manager_email' ");
    }
    $this->dbu->query("SELECT * FROM default_data WHERE type='bcc_invoice_email' ");
    if($this->dbu->next()){
      $this->dbu->query("UPDATE default_data SET value='".$in['bcc_email']."' WHERE type='bcc_invoice_email' ");
    }else{
      $this->dbu->query("INSERT INTO default_data SET value='".$in['bcc_email']."', type='bcc_invoice_email' ");
    }
    msg::success ( gm('Default email updated'),'success');
    return true;
  }
  function validate_default_email(&$in)
  {
    $v = new validation($in);
    $v->field('email_value', 'Email', 'email:required');
    $v->field('default_name', 'Email', 'required');
    $v->field('manag_email', 'Email', 'email:required');
    if($in['bcc_email']){   
      $v->field('bcc_email', 'Type of link', 'emails');
    }
    return $v->run();
  }


  function save_language(&$in){
    switch ($in['languages']){
      case '1':
        $lang = '';
        break;
      case '2':
        $lang = '_2';
        break;
      case '3':
        $lang = '_3';
        break;
      case '4':
        $lang = '_4';
        break;
      default:
        $lang = '';
        break;
    }

    //for invoice notes
    if ($in['type'] == 'invoice') {
    # extra check for custom_langs
        if($in['languages']>=1000) {
            $lang = '_'.$in['languages'];

            //Custom languages may not have a default data entry
            $this->dbu->query("DELETE FROM default_data WHERE type='invoice_note".$lang."' AND default_main_id='0' ");
            $this->dbu->query("INSERT INTO default_data SET value='".$in['notes']."', type='invoice_note".$lang."',  default_name='invoice_terms'  ");
        }

        $this->dbu->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='invoice_note".$lang."' AND default_main_id='0' ");
        $this->dbu->query("DELETE FROM default_data WHERE type='invoice_terms".$lang."' AND default_main_id='0' ");
        $this->dbu->query("INSERT INTO default_data SET value='".$in['terms']."', type='invoice_terms".$lang."',  default_name='invoice_terms'  ");
    } else if ($in['type'] == 'credit') {
        // for notes
        $credit_note = $this->dbu->field("SELECT default_name FROM default_data WHERE default_main_id = '0' AND type='credit_note".$lang."'");
        if ($credit_note) {
            $this->dbu->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='credit_note".$lang."' AND default_main_id='0' ");
        } else {
            $this->dbu->query("INSERT INTO default_data SET default_name = 'credit_note', default_main_id = '0', value='".$in['notes']."', type='credit_note".$lang."', active = '1'");
        }
        // for terms
        $credit_terms = $this->dbu->field("SELECT default_name FROM default_data WHERE default_main_id = '0' AND type='credit_terms".$lang."'");
        if ($credit_terms) {
            $this->dbu->query("UPDATE default_data SET value='".$in['terms']."' WHERE type='credit_terms".$lang."' AND default_main_id='0' ");
        } else {
            $this->dbu->query("INSERT INTO default_data SET default_name = 'credit_terms', default_main_id = '0', value='".$in['terms']."', type='credit_terms".$lang."', active = '1'");
        }
    }

    msg::success ( gm("Changes have been saved."),'success');
    return true;
  }
  function web_link(&$in)
  {

/*    $this->dbu->query("UPDATE settings SET value='".$in['invoice_link']."' WHERE constant_name='USE_INVOICE_WEB_LINK' ");
    $this->dbu->query("UPDATE settings SET value='".$in['invoice_balance']."' WHERE constant_name='SHOW_INVOICE_BALANCE' ");
    $this->dbu->query("UPDATE settings SET value='".$in['invoice_comment']."' WHERE constant_name='ALLOW_INVOICE_COMMENTS' ");
    $this->dbu->query("UPDATE settings SET value='".$in['invoice_pdf']."' WHERE constant_name='WEB_INCLUDE_PDF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['invoice_payment']."' WHERE constant_name='ALLOW_WEB_PAYMENT' ");
    if($in['icepay_active']==1 && $in['stripe_active']==1){
      msg::error ( gm('Select just one'),'success');
      return false;
    }else{
      $this->dbu->query("UPDATE settings SET value='".$in['icepay_active']."' WHERE constant_name='ICEPAY_ACTIVE2' ");
      $this->dbu->query("UPDATE settings SET value='".$in['stripe_active']."' WHERE constant_name='STRIPE_ACTIVE2' ");
    }*/


/*    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_INVOICE_WEB_LINK' ");*/
    if(!$this->payment_processors_validate($in)){
      msg::error(gm("Select one payment gateway"),"error");
      return true;
    }
    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='SHOW_INVOICE_BALANCE' ");
    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_INVOICE_COMMENTS' ");
    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='WEB_INCLUDE_PDF' ");
    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_WEB_PAYMENT' ");
    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='WEB_INCLUDE_XML' ");
      if($in['invoice_balance']){
        $this->dbu->query("SELECT * FROM settings WHERE constant_name='SHOW_INVOICE_BALANCE' ");
        if($this->dbu->move_next()){
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='SHOW_INVOICE_BALANCE' ");
        }else{
          $this->dbu->query("INSERT INTO settings SET constant_name='SHOW_INVOICE_BALANCE', value='1', module='invoice' ");
        }
      }
      if($in['invoice_comment']){
        $this->dbu->query("SELECT * FROM settings WHERE constant_name='ALLOW_INVOICE_COMMENTS' ");
        if($this->dbu->move_next()){
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_INVOICE_COMMENTS' ");
        }else{
          $this->dbu->query("INSERT INTO settings SET constant_name='ALLOW_INVOICE_COMMENTS', value='1', module='invoice' ");
        }
      }
      if($in['invoice_pdf']){
        $this->dbu->query("SELECT * FROM settings WHERE constant_name='WEB_INCLUDE_PDF' ");
        if($this->dbu->move_next()){
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='WEB_INCLUDE_PDF' ");
        }else{
          $this->dbu->query("INSERT INTO settings SET constant_name='WEB_INCLUDE_PDF', value='1', module='invoice' ");
        }
      }
      if($in['invoice_payment']){
        $this->dbu->query("SELECT * FROM settings WHERE constant_name='ALLOW_WEB_PAYMENT' ");
        if($this->dbu->move_next()){
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_WEB_PAYMENT' ");
        }else{
          $this->dbu->query("INSERT INTO settings SET constant_name='ALLOW_WEB_PAYMENT', value='1', module='invoice' ");
        }
      }
      if($in['invoice_xml']){
        $this->dbu->query("SELECT * FROM settings WHERE constant_name='WEB_INCLUDE_XML' ");
        if($this->dbu->move_next()){
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='WEB_INCLUDE_XML' ");
        }else{
          $this->dbu->query("INSERT INTO settings SET constant_name='WEB_INCLUDE_XML', value='1', module='invoice' ");
        }
      }

    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='ICEPAY_ACTIVE2' ");
    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='STRIPE_ACTIVE2' ");
    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='MOLLIE_ACTIVE2' ");
    if($in['icepay_active']){
      if(!$this->icepay_validate($in))
      {
        return false;
      }
      $this->dbu->query("SELECT * FROM settings WHERE constant_name='ICEPAY_ACTIVE2' ");
      if($this->dbu->move_next()){
        $this->dbu->query("UPDATE settings SET value='".$in['icepay_active']."' WHERE constant_name='ICEPAY_ACTIVE2' ");
      }else{
        $this->dbu->query("INSERT INTO settings SET constant_name='ICEPAY_ACTIVE2', value='".$in['icepay_active']."', module='pim' ");
      }

      if($in['icepay_merchant_id']){
        $this->dbu->query("SELECT * FROM settings WHERE constant_name='ICEPAY_MERCHANTID2' ");
        if($this->dbu->move_next()){
          $this->dbu->query("UPDATE settings SET value='".$in['icepay_merchant_id']."' WHERE constant_name='ICEPAY_MERCHANTID2' ");
        }else{
          $this->dbu->query("INSERT INTO settings SET constant_name='ICEPAY_MERCHANTID2', value='".$in['icepay_merchant_id']."', module='pim' ");
        }

      }
      if($in['icepay_secret_code']){
        $this->dbu->query("SELECT * FROM settings WHERE constant_name='ICEPAY_SECRETCODE2' ");
        if($this->dbu->move_next()){
          $this->dbu->query("UPDATE settings SET value='".$in['icepay_secret_code']."' WHERE constant_name='ICEPAY_SECRETCODE2' ");
        }else{
          $this->dbu->query("INSERT INTO settings SET constant_name='ICEPAY_SECRETCODE2', value='".$in['icepay_secret_code']."', module='pim' ");
        }
      }
    }else if($in['stripe_active']){
      $this->dbu->query("SELECT * FROM settings WHERE constant_name='STRIPE_ACTIVE2' ");
      if($this->dbu->move_next()){
        $this->dbu->query("UPDATE settings SET value='".$in['stripe_active']."' WHERE constant_name='STRIPE_ACTIVE2' ");
      }else{
        $this->dbu->query("INSERT INTO settings SET constant_name='STRIPE_ACTIVE2', value='".$in['stripe_active']."', module='pim' ");
      }
    }else if($in['mollie_active']){
      $this->dbu->query("SELECT * FROM settings WHERE constant_name='MOLLIE_ACTIVE2'");
      if($this->dbu->move_next()){
        $this->dbu->query("UPDATE settings SET value='".$in['mollie_active']."' WHERE constant_name='MOLLIE_ACTIVE2' ");
      }else{
        $this->dbu->query("INSERT INTO settings SET constant_name='MOLLIE_ACTIVE2', value='".$in['mollie_active']."', module='pim' ");
      }
    }



    msg::success ( gm("Changes have been saved."),'success');
      // $in['failure']=false;

      return true;
  }

  function payment_processors_validate(&$in){
    $is_ok=true;
    if($in['pays_activated']>0){
      if($in['icepay_active'] && $in['stripe_active'] && $in['mollie_active']){
        $is_ok=false;
      }else if(($in['icepay_active'] && $in['stripe_active']) || ($in['icepay_active'] && $in['mollie_active']) || ($in['stripe_active'] && $in['mollie_active'])){
        $is_ok=false;
      }
    }
    return $is_ok;
  }

  function icepay_validate(&$in)
  {
    $v=new validation($in);

    if($in['icepay_active']){
      $v->field('icepay_merchant_id','Merchant ID','required');
      $v->field('icepay_secret_code','Secret code','required');
    }

    return $v->run();
  }


  function creditupdate(&$in)
  {

/*    $this->dbu->query("UPDATE settings SET value='".$in['use_negative']."' WHERE constant_name='USE_NEGATIVE_CREDIT' ");
    $this->dbu->query("UPDATE settings SET value='".$in['notes']."' WHERE constant_name='CREDIT_EXTRA' ");*/


    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_NEGATIVE_CREDIT' ");
    $this->dbu->query("UPDATE settings SET long_value='' WHERE constant_name='CREDIT_EXTRA' ");
    if($in['use_negative']){
      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='USE_NEGATIVE_CREDIT' ");
    }
    if($in['notes']){
      $this->dbu->query("UPDATE settings SET long_value='".$in['notes']."' WHERE constant_name='CREDIT_EXTRA' ");
    }
    msg::success ( gm("Changes have been saved."),'success');
      return true;
  }
  function atach(&$in)
  {


    $response=array();
    if (!empty($_FILES)) {
      $fileImages = array('jpg','jpeg','gif'); // File extensions
    $size = $_FILES['Filedata']['size'];

      if(!defined('MAX_IMAGE_SIZE') && $size>512000 && in_array(strtolower($fileParts['extension']),$fileImages)){
          $response['error'] = gm('Size exceeds 500kb');
          $response['filename'] = $_FILES['Filedata']['name'];
        echo json_encode($response);
        exit();
      }

      $tempFile = $_FILES['Filedata']['tmp_name'];
      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
/*      $targetPath = $in['path'];
*/      $in['name'] = $_FILES['Filedata']['name'];
      $targetPath = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice';
      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

      // Validate the file type
      $fileTypes = array('jpg','jpeg','gif','pdf','docx'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      @mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
         if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE && in_array(strtolower($fileParts['extension']),$fileImages)){
            $image = new SimpleImage();
            $image->load($_FILES['Filedata']['tmp_name']);
            $image->scale(MAX_IMAGE_SIZE,$size);
            $image->save($targetFile);
        }else{
            move_uploaded_file($tempFile,$targetFile);             
        }
        global $database_config;

        $database_2 = array(
          'hostname' => $database_config['mysql']['hostname'],
          'username' => $database_config['mysql']['username'],
          'password' => $database_config['mysql']['password'],
          'database' => DATABASE_NAME,
        );
        $db_upload = new sqldb($database_2);
        $file_id = $db_upload->insert("INSERT INTO attached_files SET `path` = 'upload/".DATABASE_NAME."/invoice/', name = '".$in['name']."', type='2' ");
        $response['success'] = 'success';
        $response['filename'] = $_FILES['Filedata']['name'];
        echo json_encode($response);
        // ob_clean();
      } else {
        $response['error'] = gm('Invalid file type.');
        $response['filename'] = $_FILES['Filedata']['name'];
        echo json_encode($response);
        // echo gm('Invalid file type.');
      }
    }
  }
  function set_custom_pdf(&$in)
  {
    $this->dbu->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
    $this->dbu->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_INVOICE_PDF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_INVOICE_PDF_FORMAT'");
    //$this->db->query("UPDATE tblinvoice SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
    //$this->db->query("UPDATE tblinvoice_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
    function set_credit_custom_pdf(&$in)
  {
    $this->dbu->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
    $this->dbu->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_INVOICE_CREDIT_PDF_FORMAT'");
    //$this->db->query("UPDATE tblinvoice SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
    //$this->db->query("UPDATE tblinvoice_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function set_reminder_custom_pdf(&$in)
  {
    $this->dbu->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");
    $this->dbu->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_INVOICE_REMINDER_PDF_FORMAT'");
    //$this->db->query("UPDATE tblinvoice SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
    //$this->db->query("UPDATE tblinvoice_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
      msg::success ( gm('Changes saved'),'success');
      // json_out($in);

    return true;
  }
  function deleteatach(&$in){
    $path=$this->dbu->field("SELECT `path` FROM attached_files WHERE file_id='".$in['id']."' ");
           unlink($path.stripslashes($in['name']));
    $this->dbu->query("DELETE FROM attached_files WHERE file_id='".$in['id']."' AND name='".$in['name']."' ");
    msg::success ( gm("Changes have been saved."),'success');
    return true;
  }
  function defaultcheck(&$in){
    if($in['default_id']==1){
      $this->dbu->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
      msg::success ( gm("Changes have been saved."),'success');
    }else{
      $this->dbu->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
      msg::success ( gm("Changes have been saved."),'success');
    }
        return true;
  }
  function timesheetup(&$in){

    $this->dbu->query("UPDATE settings SET value='".$in['attach_report']."' WHERE constant_name='ATTACH_TIMESHEET_INV' ");
    $this->dbu->query("UPDATE default_data SET active='".$in['client']."' WHERE value='client' AND default_name='timesheet_print_inv' ");
    $this->dbu->query("UPDATE default_data SET active='".$in['task']."' WHERE value='task' AND default_name='timesheet_print_inv' ");
    $this->dbu->query("UPDATE default_data SET active='".$in['project']."' WHERE value='project' AND default_name='timesheet_print_inv' ");
    $this->dbu->query("UPDATE default_data SET active='".$in['comment']."' WHERE value='comment' AND default_name='timesheet_print_inv' ");
    msg::success ( gm("Timesheet print has been updated"),'success');
    return true;
  }
  function chargevatup(&$in){

    $this->dbu->query("UPDATE settings SET value='".$in['vat']."' WHERE constant_name='SHOW_DUE_DATE_VAT_PDF' ");
    msg::success ( gm("Changes have been saved."),'success');
    return true;
  }

  function articlefieldup(&$in){
    //Check if User Has Catalog +
    //Check if Article Name, Article Name 2 or Description are written in text input
    if(!$in['ADV_PRODUCT']){
      $fieldExist = false;
      $fields = array('[!ITEM_NAME!]','[!ITEM_NAME2!]','[!DESCRIPTION!]');
      foreach ($fields as $value) {
        if(strpos($in['text'],$value) !== false){
          $fieldExist = true;
          break;
        }
      }
      if($fieldExist){
        msg::error(gm('You must have Catalag+ in order to add the article desired fields'),'error');
        return false;
      }
    }

      $this->dbu->query("UPDATE settings SET value='".$in['not_copy_article_inv']."' WHERE constant_name='NOT_COPY_ARTICLE_INV' ");
      $this->dbu->query("UPDATE settings SET long_value='".$in['text']."' WHERE constant_name='INVOICE_FIELD_LABEL' ");
      msg::success ( gm("Changes have been saved."),'success');
      return true;
  }
  function sepaupdate(&$in){
      $this->dbu->query("UPDATE settings SET value='".$in['sepa']."' WHERE constant_name='SHOW_INVOICE_SEPA' ");
      msg::success ( gm("Changes have been saved."),'success');
      return true;
  }

  function update_export_settings(&$in){
      if(!$in['field_name'] || $in['field_id']==''){
          msg::$error = gm('Invalid ID');
          return false;
      }
      switch ($in['field_name']) {
        case 'no_border':
          $data_exists = $this->dbu->field("SELECT COUNT(field_name) FROM invoice_export_settings
                      WHERE field_name  = 'vat_classification'
                      AND field_id  = '".$in['field_id']."'
                      AND field_id_2  = '".$in['field_id_2']."' ");
          if($data_exists > 0){
            $this->dbu->query("UPDATE invoice_export_settings
                  SET   field_value   = '".$in['field_value']."'
                  WHERE field_name    = 'vat_classification'
                  AND field_id    = '".$in['field_id']."'
                  AND field_id_2    = '".$in['field_id_2']."' ");
          }else{
            $this->dbu->query("INSERT INTO invoice_export_settings
                   SET  field_name  = 'vat_classification',
                    field_id  = '".$in['field_id']."',
                    field_id_2  = '".$in['field_id_2']."',
                    field_value = '".$in['field_value']."' ");
          }
          break;
        default:
          $this->dbu->query("UPDATE invoice_export_settings SET field_value = '".$in['field_value']."' WHERE field_name = '".$in['field_name']."' AND field_id = '".$in['field_id']."' ");
          break;
      }
      msg::success ( gm("Changes have been saved."),'success');
      return true;

  }

  function label_update(&$in){

      $fields = '';
      $table = 'label_language';

      $exist = $this->dbu->query("SELECT label_language_id FROM $table WHERE label_language_id='".$in['label_language_id']."' ");
      if($exist->next()){
        $this->dbu->query("UPDATE $table SET
        invoice_note                          ='".$in['invoice_note']."',
        invoice                                 ='".$in['invoice']."',
        description                             ='".$in['description']."',
        invoice_type                            ='".$in['invoice_type']."',
        payment_term_type_1                     ='".$in['payment_term_type_1']."',
        payment_term_type_2                     ='".$in['payment_term_type_2']."',
        payment_condition                       ='".$in['payment_condition']."',
        billing_address                         ='".$in['billing_address']."',
        credit_number                           ='".$in['credit_number']."',
        date                                    ='".$in['date']."',
        customer                                ='".$in['customer']."',
        item                                    ='".$in['item']."',
        quantity                                ='".$in['quantity']."',
        unit_price                              ='".$in['unit_price']."',
        amount                                  ='".$in['amount']."',
        subtotal                                ='".$in['subtotal']."',
        subtotal_no_vat                         ='".$in['subtotal_no_vat']."',
        discount                                ='".$in['discount']."',
        vat                                     ='".$in['vat']."',
        payments                                ='".$in['payments']."',
        amount_due                              ='".$in['amount_due']."',
        notes                                   ='".$in['notes']."',
        bank_details                            ='".$in['bankd']."',
        invoice_note2                           ='".$in['notes2']."',
        duedate                                 ='".$in['duedate']."',
        bank_name                               ='".$in['bank_name']."',
        iban                                    ='".$in['iban']."',
        fax                                     ='".$in['fax']."',
        url                                     ='".$in['url']."',
        attention_of                            ='".$in['attention_of']."',
        email                                   ='".$in['email']."',
        our_ref                                 ='".$in['our_ref']."',
        your_ref                                ='".$in['your_ref']."',
        vat_number                              ='".$in['vat_number']."',
        pay_instructions                        ='".$in['pay_instructions']."',
        credit_pay_instructions                 ='".$in['credit_pay_instructions']."',
        pro_invoice                             ='".$in['pro_invoice']."',
        net_amount                              ='".$in['net_amount']."',
        credit_inv                              ='".$in['credit_inv']."',
        page                                    ='".$in['page']."',
        due_date_vat                            ='".$in['due_date_vat']."',
        comp_reg_number                         ='".$in['comp_reg_number']."',
        stamp_signature                         ='".$in['stamp_signature']."',
        paymentError_webl                       ='".$in['paymentError_webl']."',
        paymentErrorContact_webl                ='".$in['paymentErrorContact_webl']."',
        paymentPending_webl                     ='".$in['paymentPending_webl']."',
        paymentPendingContact_webl              ='".$in['paymentPendingContact_webl']."',
        paymentConfirmation_webl                ='".$in['paymentConfirmation_webl']."',
        paymentConfirmationContact_webl         ='".$in['paymentConfirmationContact_webl']."',
        creditNote_webl                         ='".$in['creditNote_webl']."',
        invoice_webl                            ='".$in['invoice_webl']."',
        date_webl                               ='".$in['date_webl']."',
        invoiceBalance_webl                     ='".$in['invoiceBalance_webl']."',
        proformaBalance_webl                    ='".$in['proformaBalance_webl']."',
        paidObsProforma_webl                    ='".$in['paidObsProforma_webl']."',
        paidObsInvoice_webl                     ='".$in['paidObsInvoice_webl']."',
        regularInvFromProforma_webl             ='".$in['regularInvFromProforma_webl']."',
        bankDetails_webl                        ='".$in['bankDetails_webl']."',
        bankName_webl                           ='".$in['bankName_webl']."',
        bicCode_webl                            ='".$in['bicCode_webl']."',
        ibanCode_webl                           ='".$in['ibanCode_webl']."',
        noData_webl                             ='".$in['noData_webl']."',
        ogm                                     ='".$in['ogm']."',
        paid                                    ='".$in['paid']."',
        reminder_due                            ='".$in['reminder_due']."',
        additional_ref                          ='".$in['additional_ref']."',
        bic_code                                ='".$in['bic_code']."',
        phone                                   ='".$in['phone']."',
        req_payment                             ='".$in['req_payment']."',
        row_amount_no_vat                       ='".$in['row_amount_no_vat']."',
        row_amount_with_vat                     ='".$in['row_amount_with_vat']."',
        billing_information                     ='".$in['billing_information']."'
                WHERE label_language_id='".$in['label_language_id']."'");
      }else{
            if($in['custom_language']){
              $label_language_id = $this->dbu->field("SELECT label_language_id FROM $table WHERE lang_code='".$in['label_language_id']."' ");

              if($label_language_id ){
                $in['label_custom_language_id']=$in['label_language_id'];
                $in['label_language_id']='';
                $this->dbu->query("UPDATE $table SET
                    invoice_note                          ='".$in['invoice_note']."',
                    invoice                                 ='".$in['invoice']."',
                    description                             ='".$in['description']."',
                    invoice_type                            ='".$in['invoice_type']."',
                    payment_term_type_1                     ='".$in['payment_term_type_1']."',
                    payment_term_type_2                     ='".$in['payment_term_type_2']."',
                    payment_condition                       ='".$in['payment_condition']."',
                    billing_address                         ='".$in['billing_address']."',
                    credit_number                           ='".$in['credit_number']."',
                    date                                    ='".$in['date']."',
                    customer                                ='".$in['customer']."',
                    item                                    ='".$in['item']."',
                    quantity                                ='".$in['quantity']."',
                    unit_price                              ='".$in['unit_price']."',
                    amount                                  ='".$in['amount']."',
                    subtotal                                ='".$in['subtotal']."',
                    discount                                ='".$in['discount']."',
                    vat                                     ='".$in['vat']."',
                    payments                                ='".$in['payments']."',
                    amount_due                              ='".$in['amount_due']."',
                    notes                                   ='".$in['notes']."',
                    bank_details                            ='".$in['bank_details']."',
                    invoice_note2                           ='".$in['notes2']."',
                    duedate                                 ='".$in['duedate']."',
                    bank_name                               ='".$in['bank_name']."',
                    iban                                    ='".$in['iban']."',
                    fax                                     ='".$in['fax']."',
                    url                                     ='".$in['url']."',
                    attention_of                            ='".$in['attention_of']."',
                    email                                   ='".$in['email']."',
                    our_ref                                 ='".$in['our_ref']."',
                    your_ref                                ='".$in['your_ref']."',
                    vat_number                              ='".$in['vat_number']."',
                    pay_instructions                        ='".$in['pay_instructions']."',
                    credit_pay_instructions                 ='".$in['credit_pay_instructions']."',
                    pro_invoice                             ='".$in['pro_invoice']."',
                    net_amount                              ='".$in['net_amount']."',
                    credit_inv                              ='".$in['credit_inv']."',
                    page                                    ='".$in['page']."',
                    due_date_vat                            ='".$in['due_date_vat']."',
                    comp_reg_number                         ='".$in['comp_reg_number']."',
                    stamp_signature                         ='".$in['stamp_signature']."',
                    paymentError_webl                       ='".$in['paymentError_webl']."',
                    paymentErrorContact_webl                ='".$in['paymentErrorContact_webl']."',
                    paymentPending_webl                     ='".$in['paymentPending_webl']."',
                    paymentPendingContact_webl              ='".$in['paymentPendingContact_webl']."',
                    paymentConfirmation_webl                ='".$in['paymentConfirmation_webl']."',
                    paymentConfirmationContact_webl         ='".$in['paymentConfirmationContact_webl']."',
                    creditNote_webl                         ='".$in['creditNote_webl']."',
                    invoice_webl                            ='".$in['invoice_webl']."',
                    date_webl                               ='".$in['date_webl']."',
                    invoiceBalance_webl                     ='".$in['invoiceBalance_webl']."',
                    proformaBalance_webl                    ='".$in['proformaBalance_webl']."',
                    paidObsProforma_webl                    ='".$in['paidObsProforma_webl']."',
                    paidObsInvoice_webl                     ='".$in['paidObsInvoice_webl']."',
                    regularInvFromProforma_webl             ='".$in['regularInvFromProforma_webl']."',
                    bankDetails_webl                        ='".$in['bankDetails_webl']."',
                    bankName_webl                           ='".$in['bankName_webl']."',
                    bicCode_webl                            ='".$in['bicCode_webl']."',
                    ibanCode_webl                           ='".$in['ibanCode_webl']."',
                    noData_webl                             ='".$in['noData_webl']."',
                    ogm                                     ='".$in['ogm']."',
                    paid                                    ='".$in['paid']."',
                    reminder_due                            ='".$in['reminder_due']."',
                    additional_ref                          ='".$in['additional_ref']."',
                    bic_code                                ='".$in['bic_code']."',
                    phone                                   ='".$in['phone']."',
                    row_amount_no_vat                       ='".$in['row_amount_no_vat']."',
                    row_amount_with_vat                     ='".$in['row_amount_with_vat']."',
                    req_payment                             ='".$in['req_payment']."'
                            WHERE label_language_id='".$label_language_id."'");
              }
              
            }
        
    }


      /*msg::success ( gm('Labels has been successfully updated'),'success');*/
      msg::success ( gm("Changes have been saved."),'success');
      // $in['failure']=false;

      return true;
    }

    function postRegister(&$in){
        global $config;

        $env_usr = $config['postgreen_user'];
        $env_pwd = $config['postgreen_pswd'];

        $post_green_data = array('user_id' => $_SESSION['u_id'],
                      'database_name' => DATABASE_NAME,
                      'item_id'   => '',
                      'module'    => 'invoice',
                      'date'      => time(),
                      'action'    => 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'],
                      'content'   => '',
                      'login_user'  => addslashes($in['user']),
                      'error_message' => ''
              );

        try{

        $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
        $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
        $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
        $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
        $soap->__setSoapHeaders(array($objHeader_Session_Outside));
        $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$in['user'],'password'=>$in['pass']));

        if($res->action->data[0]->_ == '000'){
          $post_id=$this->dbu->insert("INSERT INTO apps SET
                                                  main_app_id='0',
                                                  name='PostGreen',
                                                  api='".$in['user']."',
                                                  type='main',
                                                  active='1' ");
          $this->dbu->query("INSERT INTO apps SET main_app_id='".$post_id."', api='".$in['pass']."' ");
          /*msg::success(gm("PostGreen service activated"),"success");*/
          msg::success ( gm("Changes have been saved."),'success');
        }else{
          msg::error(gm('Incorrect credentials provided'),"error");

        }
      } catch (Exception $e) {
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");
        $post_green_data['error_message'] = addslashes($e->getMessage());
        error_post($post_green_data);
        return true;
    }
        return true;
    }

  function postIt(&$in){
      global $config;
      if($in['post_reminder']){
        $id=$in['multiple_ids'][0];
      }else{
        $id=$in['id'];
      }

      //serial number, ogm and pdf update
      $t=$this->dbu->field("SELECT type FROM tblinvoice WHERE id='".$id."' ");
      $s=$this->dbu->field("SELECT serial_number FROM tblinvoice WHERE id='".$id."' ");
      if(!$s && !$in['post_reminder']){
        if($t==0){
            $serial_n=generate_invoice_number(DATABASE_NAME,false);
        }
        if($t==1){
            $serial_n=generate_proforma_invoice_number(DATABASE_NAME,false);
        }
        if($t==2){
            $serial_n=generate_credit_invoice_number(DATABASE_NAME,false);
        }
        $this->dbu->query("UPDATE tblinvoice SET serial_number='".$serial_n."' WHERE id='".$id."'");
        $ogm_number = $this->dbu->field("SELECT ogm FROM tblinvoice WHERE id='".$id."' ");
        if(!$ogm_number && ($t == 0 || $t ==3)){
          $ogm = generate_ogm(DATABASE_NAME,$id);
          $this->dbu->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$id."' ");
        }
        # for pdf
        $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$id."' ");
        $params = array();
        $params['use_custom'] = 0;
        if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
          $params['logo'] = $inv->f('pdf_logo');
          $params['type']=$inv->f('pdf_layout');
          $params['logo']=$inv->f('pdf_logo');
          $params['template_type'] = $inv->f('pdf_layout');
        }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
          $params['custom_type']=$inv->f('pdf_layout');
          unset($params['type']);
          $params['logo']=$inv->f('pdf_logo');
          $params['template_type'] = $inv->f('pdf_layout');
          $params['use_custom'] = 1;
        }else{
          $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
          $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
        }
        #if we are using a customer pdf template
        if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
          $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
          unset($params['type']);
        }
        $params['id'] = $id;
        $params['type_pdf'] = $inv->f('type');
        $params['lid'] = $inv->f('email_language');
        $params['save_as'] = 'F';
        $this->generate_pdf($params);
        # for pdf
      }

      $tblinvoice=$this->dbu->query("SELECT tblinvoice.*,country.name as cname FROM tblinvoice
                              LEFT JOIN country ON tblinvoice.seller_b_country_id=country.country_id
                              WHERE id='".$id."' ");

      $buyer_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".$tblinvoice->f('buyer_country_id')."' ");
      $seller_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".ACCOUNT_BILLING_COUNTRY_ID."' ");

      if($in['post_reminder']){
        $in['attach_file_name']='invoice_.pdf';
        $doc_name=gm('reminder for').$tblinvoice->f('buyer_name').'.pdf';
        $f_exist=file_exists($in['attach_file_name']);
        if(!$f_exist){
          $in['attach_file_name'] = null;
          msg::error(gm("Generate PDF first"),"error");
          json_out($in);
        }
      }else{
        ark::loadLibraries(array('aws'));
        $aws = new awsWrap(DATABASE_NAME);
        $in['attach_file_name'] = 'invoice_'.$tblinvoice->f('serial_number').'.pdf';
        $file = $aws->getItem('invoice/invoice_'.$in['id'].'.pdf',$in['attach_file_name']);
        $doc_name='invoice_'.$tblinvoice->f('serial_number').'.pdf';
        if($file !== true){
          $in['attach_file_name'] = null;
          msg::error(gm("Generate PDF"),"error");
          json_out($in);
        }
      }

      $handle = fopen($in['attach_file_name'], "r");
      $contents = fread($handle, filesize($in['attach_file_name']));
      fclose($handle);
      $decodeContent = base64_encode($contents);

      $filename ="addresses.csv";
      ark::loadLibraries(array('PHPExcel'));
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Addresses")
               ->setSubject("Addresses")
               ->setDescription("Addresses")
               ->setKeywords("Addresses")
               ->setCategory("Addresses");

      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', "TITLE")
            ->setCellValue('B1', "FIRST_NAME")
            ->setCellValue('C1', "LAST_NAME")
            ->setCellValue('D1', "EMAIL")
            ->setCellValue('E1', "PHONE")
            ->setCellValue('F1', "MOBILE")
            ->setCellValue('G1', "FAX")
            ->setCellValue('H1', "COMPANY")
            ->setCellValue('I1', "ADDRESS_LINE_1")
            ->setCellValue('J1', "ADDRESS_LINE_2")
            ->setCellValue('K1', "ADDRESS_LINE_3")
            ->setCellValue('L1', "ADDRESS_POSTCODE")
            ->setCellValue('M1', "ADDRESS_CITY")
            ->setCellValue('N1', "ADDRESS_STATE")
            ->setCellValue('O1', "ADDRESS_COUNTRY");

      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', "")
            ->setCellValue('B2', "")
            ->setCellValue('C2', "")
            ->setCellValue('D2', $tblinvoice->f('buyer_email'))
            ->setCellValue('E2', $tblinvoice->f('buyer_phone'))
            ->setCellValue('F2', "")
            ->setCellValue('G2', $tblinvoice->f('buyer_fax'))
            ->setCellValue('H2', $tblinvoice->f('buyer_name'))
            ->setCellValue('I2', $tblinvoice->f('buyer_address'))
            ->setCellValue('J2', "")
            ->setCellValue('K2', "")
            ->setCellValue('L2', $tblinvoice->f('buyer_zip'))
            ->setCellValue('M2', $tblinvoice->f('buyer_city'))
            ->setCellValue('N2', "")
            ->setCellValue('O2', $buyer_country);

      $objPHPExcel->getActiveSheet()->setTitle('Addresses');
      $objPHPExcel->setActiveSheetIndex(0);
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
      $objWriter->setDelimiter(';');
      $objWriter->save($filename);

      $handle_csv = fopen($filename, "r");
      $contents_csv = fread($handle_csv, filesize($filename));
      fclose($handle_csv);
      $csvContent   = base64_encode($contents_csv);

      $env_usr = $config['postgreen_user'];
      $env_pwd = $config['postgreen_pswd'];
      $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
      $panel_usr=$postg_data->f("api");
      $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
      
      $post_green_data = array('user_id' => $_SESSION['u_id'],
                      'database_name' => DATABASE_NAME,
                      'item_id'   => $in['id'],
                      'module'    => 'invoice',
                      'date'      => time(),
                      'action'    => 'ORDER_CREATE'.' - '.$config['postgreen_wsdl'],
                      'login_user'  => addslashes($panel_usr),
                      'content'   => '',
                      'error_message' => ''
              );

    try {

      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));
      $soapVr='
        <order_create_IN>
          <action type="string">ORDER_CREATE</action>
          <attachment_list type="stringlist"></attachment_list>';
      if($in['stationary_id']){
        $soapVr.='<background type="string">'.$in['stationary_id'].'</background>';
      }else{
        $soapVr.='<background type="string"></background>';
      }
      $soapVr.='<content_parameters type="indstringlist"></content_parameters>
          <csv type="file" extension="csv">
            <nir:data>'.$csvContent.'</nir:data>
          </csv>
          <document type="file" extension="pdf">
            <nir:data>'.$decodeContent.'</nir:data>
          </document>
          <document_name type="string">'.$doc_name.'</document_name>
          <get_proof type="string">NO</get_proof>
          <identifier type="indstringlist">
            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
          </identifier>
          <insert_list type="indstringlist"></insert_list>
          <mailing_type type="string">NONE</mailing_type>
          <order_parameters type="indstringlist"></order_parameters>
          <page_list type="stringlist"></page_list>
          <return_address type="indstringlist">
            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
            <nir:data key="ADDRESSLINE2"></nir:data>
            <nir:data key="ADDRESSLINE3"></nir:data>
            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
            <nir:data key="ADDRESSLINE6"></nir:data>
            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
          </return_address>
          <sender_address type="indstringlist">
            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
            <nir:data key="ADDRESSLINE2"></nir:data>
            <nir:data key="ADDRESSLINE3"></nir:data>
            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
            <nir:data key="ADDRESSLINE6"></nir:data>
            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
          </sender_address>
          <service_profile type="indstringlist">
            <nir:data key="service_id">CUSTOM_ENVELOPE</nir:data>
            <nir:data key="print_mode">1SIDE</nir:data>
            <nir:data key="envelop_type">CUSTOM</nir:data>
            <nir:data key="paper_weight">80g</nir:data>
            <nir:data key="print_color">COLOR</nir:data>
            <nir:data key="mail_type">EXPRESS</nir:data>
            <nir:data key="address_page">EXTRA_PAGE</nir:data>
            <nir:data key="validation">NO</nir:data>
          </service_profile>
        </order_create_IN>';

      $post_green_data['content'] = addslashes($soapVr);
        
      $params = new \SoapVar($soapVr, XSD_ANYXML);

      $result = $soap->order_create($params);
      if($result->action->data[0]->_ == '000'){
        if($in['post_reminder']){
          foreach($in['multiple_ids'] as $key=>$value){
              $this->dbu->query("UPDATE tblinvoice SET postgreen_id='".$result->order_id->_."' WHERE id='".$value."' ");
              // $in['invoice_id']=$value;
              // $in['sent_date']= time();
              // $this->mark_sent($in);
          }
          $msg_txt = gm('Reminder successfully exported');
          // msg::success(gm('Reminder successfully exported'),"success");
        }else{
            $this->dbu->query("UPDATE tblinvoice SET postgreen_id='".$result->order_id->_."' WHERE id='".$id."' ");
            $in['invoice_id']=$id;
            $in['sent_date']= date('c');
            $this->mark_sent($in);
            $msg_txt =gm('Invoice successfully exported');
            // msg::success(gm('Invoice successfully exported'),"success");
        }
      }else{
         unlink($in['attach_file_name']);
         unlink($filename);

        msg::error($result->action->data[1]->_,"error");
        return true;
      }
    } catch (Exception $e) {
      unlink($in['attach_file_name']);
      unlink($filename);
      msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");
      $post_green_data['error_message'] = addslashes($e->getMessage());
      if(empty($soapVr)){
        $post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
      }
      error_post($post_green_data);
      
      json_out($in);        
    }

      unlink($in['attach_file_name']);
      unlink($filename);

      msg::success($msg_txt,"success");
      if($in['related']=='view'){
        // $this->dbu->query("UPDATE tblinvoice SET sent='1',sent_date='".time()."' WHERE id='".$id."'");
        // return true;
      }else{
        unset($in['invoice_id']);
        unset($in['c_invoice_id']);
        // json_out($in);
      }
      return true;
  }

  function postInvoiceData(&$in){
    if($in['old_obj']['no_status']){
        json_out($in);
    }else{
        try{
          global $config;
          $env_usr = $config['postgreen_user'];
          $env_pwd = $config['postgreen_pswd'];
          $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
          $panel_usr=$postg_data->f("api");

          $post_green_data = array('user_id' => $_SESSION['u_id'],
                      'database_name' => DATABASE_NAME,
                      'item_id'   => $in['invoice_id'],
                      'module'    => 'invoice',
                      'date'      => time(),
                      'action'    => 'ORDER_DETAILS'.' - '.$config['postgreen_wsdl'],
                      'login_user'  => addslashes($panel_usr),
                      'content'   => '',
                      'error_message' => ''
              );

          $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
          $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
          $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
          $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
          $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
          $soap->__setSoapHeaders(array($objHeader_Session_Outside));
          $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));

          $order_id=$this->dbu->field("SELECT postgreen_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

          $post_green_data['content'] = addslashes('<order_details_IN>
              <action type="string">ORDER_DETAILS</action>
              <get_billing>NO</get_billing>
              <get_document>NO</get_document>
              <get_proof>NO</get_proof>
              <identifier type="indstringlist">
                <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
              </identifier>
              <order_id type="string">'.$order_id.'</order_id>
            </order_details_IN>');

          $params = new \SoapVar('
            <order_details_IN>
              <action type="string">ORDER_DETAILS</action>
              <get_billing>NO</get_billing>
              <get_document>NO</get_document>
              <get_proof>NO</get_proof>
              <identifier type="indstringlist">
                <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
              </identifier>
              <order_id type="string">'.$order_id.'</order_id>
            </order_details_IN>',
             XSD_ANYXML);

          $result=$soap->order_details($params);
      }catch(Exception $e){
            $post_green_data['error_message'] = addslashes($e->getMessage());

            if(empty($post_green_data['content'])){
              $post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
            } 

            error_post($post_green_data);
            $post_status=array('status'=>gm('Postgreen service is temporarily unavailable. Please retry later.'));
          if($in['related']=='minilist'){
            json_out($post_status);
          }else if($in['related']=='status'){
            $in['status']=$post_status['status'];
            json_out($in);
          }else{
            return json_encode($post_status);
          }
      }
      $PGreen_status=array(
        '100'   => gm('PostGreen Received'),
        '140'   => gm('PostGreen Waiting for validation'),
        '-140'  => gm('PostGreen Rejected'),
        '160'   => gm('PostGreen Saved'),
        '200'   => gm('PostGreen Confirmed'),
        '210'   => gm('PostGreen Processing'),
        '300'   => gm('PostGreen Processed'),
        '400'   => gm('PostGreen Validated'),
        '500'   => gm('PostGreen Printed'),
        '600'   => gm('PostGreen Posted'),
        '700'   => gm('PostGreen Sent'),
        '800'   => gm('PostGreen Arrived'),
        '810'   => gm('PostGreen Opened'),
        '900'   => gm('PostGreen Canceled')
      );
      $post_status=array('status'=>gm('Post status').": ".$PGreen_status[$result->order_info->row->col[3]->data]);
      if($in['related']=='minilist'){
        json_out($post_status);
      }else if($in['related']=='status'){
        $in['status']=$post_status['status'];
        json_out($in);
      }else{
        return json_encode($post_status);
      }
    }   
  }

  function bpay_register(&$in){
    if(!$this->bpay_reg_validate($in))
    {
      json_out($in);
      return false;
    }

    global $config;
    $ch = curl_init();

    if($_SESSION['main_u_id']==0){
      //$bPaidCust=$this->dbu_users->query("SELECT user_id,first_name,last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
      $bPaidCust=$this->dbu_users->query("SELECT user_id,first_name,last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
    }else{
      //$bPaidCust=$this->dbu_users->query("SELECT user_id,first_name,last_name FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
      $bPaidCust=$this->dbu_users->query("SELECT user_id,first_name,last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['main_u_id']]);
    }

    $lang=$this->dbu->field("SELECT code FROM pim_lang WHERE lang_id='".$in['languages']."' ");
    if(!$lang){
      $lang=$this->dbu->field("SELECT code FROM pim_custom_lang WHERE lang_id='".$in['languages']."' ");
    }

    $headers=array('Content-Type: application/json');
    $bpay_data=array(
            'provider_id'           => 'd86b4eed-d0e0-4a62-b63f-fe6889963307',
            'address'               => array(
                                      'street'    => $in['invoice_seller_b_address'],
                                      'country'   => $this->dbu->field("SELECT code FROM country WHERE country_id='".$in['invoice_seller_b_country_id']."' "),
                                      'post_code'  => $in['invoice_seller_b_zip'],
                                      'city'      => $in['invoice_seller_b_city'],
                                      'email'     => $in['invoice_seller_email']
                                    ),
            'first_name'            => $bPaidCust->f('first_name'),
            'last_name'             => $bPaidCust->f('last_name'),
            'company_name'          => $in['invoice_seller_name'],
            'claiment_provider_id'  => $in['provider_ref'],
            'tva'                   => $in['invoice_vat_no'],
            'language'              => $lang,
            'bank_iban'             => $in['invoice_seller_iban'],
            'bank_swift'            => $in['invoice_seller_swift']
      );

    $bpay_data = json_encode($bpay_data);

          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
          curl_setopt($ch, CURLOPT_USERPWD, $config['bpay_user'].":".$config['bpay_pass']);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_TIMEOUT, 60);

          //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $bpay_data);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/claiments');

    $put = curl_exec($ch);
    $info = curl_getinfo($ch);
    /*console::log($put);
    console::log('space');
    console::log($info);*/
    if($info['http_code']>=200 && $info['http_code']<300){
      $new_data=json_decode($put,true);
      if(array_key_exists('id',$new_data)){
        $this->dbu_users->query("UPDATE users SET bPaid_cust_id='".$new_data['id']."' WHERE user_id='".$bPaidCust->f('user_id')."' OR main_user_id='".$bPaidCust->f('user_id')."'");
        /*msg::success(gm("BePaid Customer created successfully"),'success');*/
        msg::success ( gm("Changes have been saved."),'success');
      }
    }else{
      /*msg::error(gm('Error on registration'),'error');*/
      msg::error(gm('Error'),'error');
    }
    return true;
  }

  function bpay_reg_validate(&$in){
    $v = new validation($in);

    $v->field('invoice_seller_b_address','invoice_seller_b_address','required');
    $v->field('invoice_seller_b_city','invoice_seller_b_city','required');
    $v->field('invoice_seller_b_zip','invoice_seller_b_zip','required');
    $v->field('invoice_seller_name','invoice_seller_name','required');
    $v->field('invoice_vat_no','invoice_vat_no','required');
    $v->field('invoice_seller_iban','invoice_seller_iban','required:max_length[16]');
    $v->field('invoice_seller_swift','invoice_seller_swift','required');
    $v->field('invoice_seller_email','invoice_seller_email','required');
    return $v->run();
  }

  function bPaid_notification(&$in){
      global $config;
      $ch = curl_init();

      $headers=array('Content-Type: application/json');
      $this->dbu->query("UPDATE tblinvoice SET bPaid_type='".$in['bPaid_type']."' WHERE id='".$in['invoice_id']."' ");
      $invoice_data=$this->dbu->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($in['buyer_id'] || $in['buyer_id']!='0'){
        $contact_data=$this->dbu->query("SELECT btw_nr,name,bPaid_debtor_id FROM customers WHERE customer_id='".$in['buyer_id']."'  ");
        $debtor_provider_id=$in['buyer_id'].'-0';
        $bpay_data=array(
              'claiment_provider_id'  => $in['provider_ref'],
              'first_name'            => '-',
              'last_name'             => '-',
              'company_name'          => $contact_data->f('name'),
              'tva'                   => $contact_data->f('btw_nr'),
              'debtor_provider_id'    => $debtor_provider_id,
              'address'               => array(
                                      'street'    => $invoice_data->f('buyer_address'),
                                      'country'   => get_country_name($invoice_data->f('buyer_country_id')),
                                      'post_code'  => $invoice_data->f('buyer_zip'),
                                      'city'      => $invoice_data->f('buyer_city'),
                                      'email'     => $invoice_data->f('buyer_email')
                                    )
        );
      }else{
        $contact_data=$this->dbu->query("SELECT firstname,lastname,bPaid_debtor_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
        $debtor_provider_id='0-'.$in['contact_id'];
        $bpay_data=array(
              'claiment_provider_id'  => $in['provider_ref'],
              'first_name'            => $contact_data->f('firstname'),
              'last_name'             => $contact_data->f('lastname'),
              'debtor_provider_id'    => $debtor_provider_id,
              'address'               => array(
                                      'street'    => $invoice_data->f('buyer_address'),
                                      'country'   => get_country_name($invoice_data->f('buyer_country_id')),
                                      'post_code'  => $invoice_data->f('buyer_zip'),
                                      'city'      => $invoice_data->f('buyer_city'),
                                      'email'     => $invoice_data->f('buyer_email')
                                    )
        );
      }
      if($contact_data->f('bPaid_debtor_id')!=''){
        //1 - create, 2 - delete
        if(!$this->bPaid_debt($in['invoice_id'],1)){
          msg::error(gm("Notification already sent"),"error");
          return true;
        }else{
          /*msg::success(gm("Notification done successfully"),"success");*/
          msg::success ( gm("Changes have been saved."),'success');
        }
      }else{
        $bpay_data = json_encode($bpay_data);

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
        curl_setopt($ch, CURLOPT_USERPWD, $config['bpay_user'].":".$config['bpay_pass']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        /*curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');*/
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $bpay_data);
        curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/debtors');

        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        /*console::log($put);
        console::log('spacenotif');
        console::log($info);*/

        if($info['http_code']>=200 && $info['http_code']<300){
          $new_data=json_decode($put,true);
          if(array_key_exists('id',$new_data)){
            if($in['buyer_id'] || $in['buyer_id']!='0'){
                $this->dbu->query("UPDATE customers SET bPaid_debtor_id='".$new_data['id']."' WHERE customer_id='".$in['buyer_id']."' ");
            }else{
                $this->dbu->query("UPDATE customer_contacts SET bPaid_debtor_id='".$new_data['id']."' WHERE contact_id='".$in['contact_id']."' ");
            }
            $this->bPaid_debt($in['invoice_id'],1);
            /*msg::success(gm("Notification done successfully"),'success');*/
            msg::success ( gm("Changes have been saved."),'success');
          }
        }else{
          $err_data=json_decode($put,true);
          msg::error($err_data['message'],"error");
        }
      }
      if($in['related']=='list'){
          unset($in['buyer_id']);
          unset($in['contact_id']);
      }
      return true;
  }

  function bPaid_debt($invoice_id,$type){
        /*$this->bPaid_payment("","","",2);*/

      global $config,$in;
      $ch = curl_init();

      $headers=array('Content-Type: application/json');

      $invoice_data=$this->dbu->query("SELECT * FROM tblinvoice WHERE id='".$invoice_id."' ");
      $web_url = $this->dbu_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$invoice_id."' AND `type`='i'  ");
      if($invoice_data->f('bPaid_debt_id')!='' && $type==1){
        return false;
      }
      if($invoice_data->f('buyer_id') || $invoice_data->f('buyer_id')!='0'){
        $debtor_provider_id=$invoice_data->f('buyer_id').'-0';
      }else{
        $debtor_provider_id='0-'.$invoice_data->f('contact_id');
      }
      if($invoice_data->f('pdf_layout') && $tblinvoice->f('use_custom_template')==0){
        $link_end='&type='.$invoice_data->f('pdf_layout').'&logo='.$invoice_data->f('pdf_logo');
      }elseif($invoice_data->f('pdf_layout') && $tblinvoice->f('use_custom_template')==1){
        $link_end = '&custom_type='.$invoice_data->f('pdf_layout').'&logo='.$invoice_data->f('pdf_logo');
      }else{
        $link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
      }
      $invoice_currency=$this->dbu->field("SELECT currency.code FROM currency
        INNER JOIN tblinvoice ON currency.id=tblinvoice.currency_type
        WHERE tblinvoice.id='".$invoice_id."' ");
      $bp_data=array(
              'debt_provider_id'        => $invoice_data->f('serial_number'),
              'debtor_provider_id'      => $debtor_provider_id,
              'debt_status'             => $invoice_data->f('bPaid_type'),
              'invoice_amount'          => $invoice_data->f('amount_vat'),
              'invoice_emission_date'   => date(ACCOUNT_DATE_FORMAT,$invoice_data->f('invoice_date')),
              'invoice_due_date'        => $invoice_data->f('due_date'),
              'invoice_currency'        => $invoice_currency,
              'invoice_url'             => $config['web_link_url'].'index.php?q='.$web_url.'&do=invoice-invoice_print&id='.$invoice_data->f('id').'&lid='.$invoice_data->f('email_language').$link_end,
        );

      $bp_data = json_encode($bp_data);

      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
      curl_setopt($ch, CURLOPT_USERPWD, $config['bpay_user'].":".$config['bpay_pass']);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);

      if($type==1){
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $bp_data);
        curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/debts');
      }else if($type==2){
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/debts/'.$invoice_data->f('bPaid_debt_id'));
      }

      $put = curl_exec($ch);
      $info = curl_getinfo($ch);

      /*console::log($put);
      console::log('spacedebt');
      console::log($info);*/

      if($info['http_code']>=200 && $info['http_code']<300 && $type==1){
        $new_data=json_decode($put,true);
        if(array_key_exists('id',$new_data)){
          $in['bPaid_modUrl']=$new_data['modal_url'];
          $this->dbu->query("UPDATE tblinvoice SET bPaid_debt_id='".$new_data['id']."' WHERE id='".$invoice_id."' ");
          $payments_info = $this->dbu->query("SELECT date,amount,payment_id FROM tblinvoice_payments WHERE invoice_id='".$invoice_id."' ORDER BY payment_id ASC ")->getAll();
          foreach($payments_info as $key=>$value){
            //1 - create, 2 -delete
              $this->bPaid_payment($value,$new_data['id'],$invoice_data->f('serial_number'),1);
          }
        }
        return true;
      }else{
        return false;
      }
  }

  function bPaid_payment($row,$debt_id,$serial_number,$type){
      global $config;
      $ch = curl_init();

      $headers=array('Content-Type: application/json');
      $payment_data=array(
            'debt_id'                 => $debt_id,
            'debt_provider_id'        => $serial_number,
            'payment_amount'          => $row['amount'],
            'payment_date'            => $row['date']
      );

      $payment_data = json_encode($payment_data);

      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
      curl_setopt($ch, CURLOPT_USERPWD, $config['bpay_user'].":".$config['bpay_pass']);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);

      if($type==1){
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payment_data);
        curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/payments');
      }else if($type==2){
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_URL, $config['bpay_base_url'].'/payments/'.$row['bPaid_payment_id']);
      }

      $put = curl_exec($ch);
      $info = curl_getinfo($ch);

      /*console::log($put);
      console::log('spacepaym');
      console::log($info);*/
      if($info['http_code']>=200 && $info['http_code']<300 && $type==1){
        $new_data=json_decode($put,true);
        if(array_key_exists('id',$new_data)){
          $this->dbu->query("UPDATE tblinvoice_payments SET bPaid_payment_id='".$new_data['id']."' WHERE payment_id='".$row['payment_id']."' ");
        }
      }
      return true;
  }

  function bePaidData(&$in){
    global $config;
    $ch = curl_init();
    /*$in['debt_id']=$this->dbu->field("SELECT bPaid_debt_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
    $in['bp_usr']=$config['bpay_user'];
    $in['bp_pss']=$config['bpay_pass'];*/
    $debt_id=$this->dbu->field("SELECT bPaid_debt_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

    $headers=array('Content-Type: application/json');
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
    curl_setopt($ch, CURLOPT_USERPWD, $config['bpay_user'].":".$config['bpay_pass']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_URL, 'https://api.bepaid.be/base/site/url/'.$debt_id);

    $put = curl_exec($ch);
    $info = curl_getinfo($ch);

    $in['bp_link']=$put.'&lang='.strtoupper($_SESSION['l']);

    json_out($in);
  }

  /****************************************************************
  * function delete(&$in)                                          *
  ****************************************************************/
  function activater(&$in)
  {
    if(!$this->delete_recurring_validate($in)){
      return false;
    }

    $this->dbu->query("UPDATE recurring_invoice SET f_archived='0' WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."' ");

      msg::success (gm("Invoice Activated"),'success');
      insert_message_log('recurring_invoice','{l}Recurring invoice has been activated by{endl} '.get_user_name($_SESSION['u_id']),'recurring_invoice_id',$in['recurring_invoice_id'],false);
      $inv = $this->dbu->query("SELECT trace_id FROM recurring_invoice WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."' ");
      if($inv->f('trace_id')){
          $this->dbu->query("UPDATE tracking SET archived='0' WHERE trace_id='".$inv->f('trace_id')."' ");
        }

      return true;
  }

  /****************************************************************
  * function delete_recurring_validate(&$in)                                          *
  ****************************************************************/
  function delete_recurring_validate(&$in)
  {
    $is_ok = true;
    if(!$in['recurring_invoice_id']){
      msg::error (gm('Invalid ID'),'error');
      return false;
    }
    else{
      $this->dbu->query("SELECT recurring_invoice_id FROM recurring_invoice WHERE recurring_invoice_id='".$in['recurring_invoice_id']."'");
      if(!$this->dbu->move_next()){
        msg::error (gm('Invalid ID'),'error');
        return false;
      }
    }

      return $is_ok;
  }

  /****************************************************************
  * function delete_recurring(&$in)                                          *
  ****************************************************************/
  function archive_recurring(&$in)
  {
    if(!$this->delete_recurring_validate($in)){
      return false;
    }
    $this->dbu->query("UPDATE recurring_invoice SET f_archived='1' WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."' ");

      msg::success (gm("Invoice Archived"),'success');
      insert_message_log('recurring_invoice','{l}Recurring invoice has been archived by{endl} '.get_user_name($_SESSION['u_id']),'recurring_invoice_id',$in['recurring_invoice_id'],false);
      $tracking_trace=$this->dbu->field("SELECT trace_id FROM recurring_invoice WHERE recurring_invoice_id='".$in['recurring_invoice_id']."' ");
      if($tracking_trace){
        $this->dbu->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
      }
      return true;
  }

  /****************************************************************
  * function delete_recurring(&$in)                                          *
  ****************************************************************/
  function delete_recurring(&$in)
  {
    if(!$this->delete_recurring_validate($in)){
      return false;
    }
      $tracking_trace=$this->dbu->field("SELECT trace_id FROM recurring_invoice WHERE recurring_invoice_id='".$in['recurring_invoice_id']."' ");
      $this->dbu->query("DELETE FROM recurring_invoice_line WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."'");
      $this->dbu->query("DELETE FROM recurring_email_contact WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."'");
      $this->dbu->query("DELETE FROM logging WHERE field_name='recurring_invoice_id' AND field_value='".$in['recurring_invoice_id']."' ");
      $this->dbu->query("DELETE FROM recurring_invoice WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."'");     
      if($tracking_trace){
        $this->dbu->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
        $this->dbu->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
      }

      msg::success (gm("Invoice Deleted"),'success');
      return true;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function update_grade(&$in)
  {
    $invoice_grade = $this->dbu->field("SELECT invoice_grade FROM tblinvoice WHERE id='".$in['invoice_id']."'");
    $new_grade = $invoice_grade;
    if($invoice_grade<3 && $in['dir'] =='up')
    {
      $new_grade = $invoice_grade+1;
    }else if($invoice_grade>1 && $in['dir'] == 'down'){
      $new_grade = $invoice_grade-1;
    }
    $this->dbu->query("UPDATE tblinvoice SET invoice_grade='".$new_grade."' WHERE id='".$in['invoice_id']."'");
    $in['new_nr'] = $new_grade;
    json_out($in);
  }

  /**
   * undocumented function
   *
   * @return 
   * @author
   **/
  function getImagesFromMsg($msg, $tmpFolderPath)
    {
        $arrSrc = array();
        if (!empty($msg))
        {
            preg_match_all('/<img[^>]+>/i', stripcslashes($msg), $imgTags);
            preg_match_all('/<img[^>]+>/i', $msg, $imgTags_1);

            for ($i=0; $i < count($imgTags[0]); $i++)
            {
                preg_match('/src="([^"]+)/i', $imgTags[0][$i], $withSrc);
                //Remove src
                $withoutSrc = str_ireplace('src="', '', $withSrc[0]);

                //data:image/png;base64,
                if (strpos($withoutSrc, ";base64,"))
                {
                    //data:image/png;base64,.....
                    list($type, $data) = explode(";base64,", $withoutSrc);
                    //data:image/png
                    list($part, $ext) = explode("/", $type);
                    //Paste in temp file
                    $withoutSrc = $tmpFolderPath."/".uniqid("temp_").".".$ext;
                    @file_put_contents($withoutSrc, base64_decode($data));
                    $arrSrc[$withoutSrc] = $imgTags_1[0][$i];
                }      
                //$arrSrc[$withoutSrc] = $imgTags_1[0][$i];
            }
        }
        return $arrSrc;
    }

  function noticeNew(&$in){
      global $config;
        $mail = new PHPMailer();
        $mail->WordWrap = 50;
        $mail->set('CharSet','UTF-8');
        $def_email = $this->default_email();
       // $xxx = stripslashes($in['e_message']);
        $xxx=$this->getImagesFromMsg($in['e_message'],'upload');
        foreach($xxx as $location => $value){
          $tmp_start=strpos($location,'/');
          $tmp_end=strpos($location,'.');
          $tmp_cid_name=substr($location,$tmp_start+1,$tmp_end-1-$tmp_start);
          $tmp_file_name=$tmp_cid_name.substr($location, $tmp_end, strlen($location)-$tmp_end);
          $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].$config['install_path'].$location, 'sign_'.$tmp_cid_name, $tmp_file_name); 
          //$in['e_message']=str_replace($value,'<img src="cid:'.'sign_'.$tmp_cid_name.'">',$in['e_message']);
          $in['e_message']=str_replace($value,generate_img_with_cid($value,$tmp_cid_name),$in['e_message']);
        }
        $body = stripslashes($in['e_message']);
        $body = str_replace(array("<br />","<br/>","<br>"), array("<br />\r\n","<br/>\r\n","<br>\r\n"), $body);

        $subject=$in['subject'];

        $in['reminder']=1;

        $in['customer_id']=$in['buyer_id'].'-'.$in['contact_id'];

        if($in['include_pdf']){
            $this->send_pdf($in);
            $mail->AddAttachment("invoice_.pdf",'invoice_.pdf');
          }

         if($in['new_email']){
            $mail->AddAddress($in['new_email']);
             $email_list = $in['new_email'];
          }
          if($in['copy']){
              if($in['USER_EMAIL']){
                $mail->AddBCC($in['USER_EMAIL'],ACCOUNT_COMPANY);
                 $email_list .= ', '.$in['USER_EMAIL'];
              }  
          }


       if($in['sendgrid_selected']){

          include_once(__DIR__."/../../misc/model/sendgrid.php");
          $sendgrid = new sendgrid($in, 'invoice_reminder', 'invoice_reminder',implode(',',$in['invoice_ids']), $body,$def_email, DATABASE_NAME);

          $sendgrid_data = $sendgrid->get_sendgrid($in);

          if($sendgrid_data['error']){
            msg::error($sendgrid_data['error'],'error');
          }elseif($sendgrid_data['success']){
            msg::success($in['new_email'].' - '.gm("Email sent by Sendgrid"),'success');
          }
          
        }else{

          $mail->SetFrom($def_email['from']['email'],  utf8_decode($def_email['from']['name']));
          if($def_email['reply']['email']){
            $mail->AddReplyTo($def_email['reply']['email'],  utf8_decode($def_email['reply']['name']));
          }else{
            $mail->AddReplyTo('noreply@akti.com', 'noreply@akti.com');
          }

          $mail->Subject = $subject;

          $mail->MsgHTML($body);
                         
          $mail->Send();


          if($mail->IsError()){
            msg::error($mail->ErrorInfo,"error");
            json_out($in);
            return false;
          }
          /*if($mail->ErrorInfo){
            msg::notice($mail->ErrorInfo,"notice");
          }
            if($mail->ErrorInfo){
            msg::error(gm("Attachments to big."),"error");
            json_out($in);
            return false;
          }*/

        }

      foreach($xxx as $location => $value){
        unlink($location);
      }


    $msg = gm("Email sent");    
    $time = time();
    $account_manager_id = $_SESSION['u_id'];
    $user_name = get_user_name($_SESSION['u_id']);
    //$msg_log = '{l}Reminder email was sent by{endl} '.$user_name.' {l}to{endl}: '.$email_list;
            
    if($in['sendgrid_selected']){
      //$msg_log .= " {l}via SendGrid{endl}.";
      $msg = $in['new_email'].' - '.gm("Email sent");
    }

      $invoice_id = $in['invoice_ids'][0];
      $company_id = $this->dbu->field("SELECT buyer_id FROM tblinvoice WHERE id= '".$invoice_id."' ");
      $in['log_id']=$this->dbu->insert("INSERT INTO reminders_logs SET
                        account_manager_id = '".$account_manager_id."',
                        sent_date= '".$time."',
                        company_id = '".$company_id."',
                        email='".$in['new_email']."',
                        invoices='".implode(',',$in['invoice_ids'])."'
                      ");
      if(!empty($in['invoice_ids'])){
        $invoiceIds = $in['invoice_ids'];
        foreach ($invoiceIds as $invoiceId) {    
            $invoiceGrade = $this->dbu->field("SELECT invoice_grade FROM tblinvoice WHERE id='".$invoiceId."'");
            $newGrade = $invoiceGrade;
            $set_grade = '';
            if($invoiceGrade<3)
            {
              if($in['many_email_addresses']!=1){
                $newGrade = $invoiceGrade+1;
                $set_grade = " , invoice_grade='".$newGrade."' ";
                //$this->dbu->query("UPDATE tblinvoice SET invoice_grade='".$newGrade."' WHERE id='".$invoiceId."'");
              }
            }
             $this->dbu->query("UPDATE tblinvoice SET last_notice_sent='".$time."' ".$set_grade." WHERE id='".$invoiceId."'");
          switch($invoiceGrade){
            case 1:
              $msg_log = '{l}First reminder email was sent by{endl} '.$user_name.' {l}to{endl}: '.$email_list;
              break;
            case 2:
              $msg_log = '{l}Second reminder email was sent by{endl} '.$user_name.' {l}to{endl}: '.$email_list;
              break;
            case 3:
              $msg_log = '{l}Third reminder email was sent by{endl} '.$user_name.' {l}to{endl}: '.$email_list;
              break;
            default:
              $msg_log = '{l}Reminder email was sent by{endl} '.$user_name.' {l}to{endl}: '.$email_list;
          }
           $in['logging_id'] = insert_message_log($this->pag,$msg_log,$this->field_n,$invoiceId);  
        }
      }

    msg::success($msg,"success");
    return true;
  }

  function send_pdf(&$in)
  {
      if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == '1' ){
        $in['custom_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
      }
      include(__DIR__.'/../controller/send_invoices_print.php');
      /*include('../apps/invoice/admin/controller/send_invoices_print.php');*/
  }

  function postMultiple(&$in){
      $in['multiple_ids']=$in['invoice_ids'];
      $in['reminder']=1;
      $in['lid'] = $this->dbu->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
      $in['type'] = ACCOUNT_INVOICE_PDF_FORMAT;
      $this->send_pdf($in);
      $in['post_reminder']=1;
      $this->postIt($in);
  }

  /****************************************************************
* function add_recurring_validate(&$in)               *
****************************************************************/
  function add_recurring_validate(&$in)
  {
    /*if(strtotime($in['quote_ts']) <= 7200){
      unset($in['quote_ts']);
    }*/
    $v = new validation($in);
    $v->field("start_date", gm("Start Date"), 'required', gm('Start Date required'));
    if($in['invoice_delivery']==1){
      $v->field('recipients','Recipients','required:not_empty:multi_email');
    }
    $is_ok=$v->run();
    if($is_ok){
        $st_date = mktime(0,0,0,date('n',strtotime($in['quote_ts'])),date('j',strtotime($in['quote_ts'])),date('Y',strtotime($in['quote_ts'])));
        $time_date = mktime(0,0,0,date('n'),date('j'),date('Y'));
        /*if($time_date>$st_date && ark::$method=='add_recurring'){
            msg::error(gm('Not allowed start date in the past'),'start_date');
            $is_ok=false;
        }*/  //removed in AKTI 3343
        if($in['end_date']){
          $en_date = mktime(0,0,0,date('n',strtotime($in['end_date'])),date('j',strtotime($in['end_date'])),date('Y',strtotime($in['end_date'])));
          if($en_date<=$st_date){
            msg::error(gm('Set end date to future'),'end_date');
            $is_ok=false;
          }
        }
    }
    /*$is_ok = true;
    //$v->field('quote_ts','Date','required');
    if(!$in['start_date']){
      msg::error(gm("Start Date required"),"error");
      $is_ok=false;
    }*/
   return $is_ok;

    //return $v->run();
  }

  /****************************************************************
  * function update_recurring_validate(&$in)                                *
  ****************************************************************/
  function update_recurring_validate(&$in)
  {
    $is_ok = true;
    $v = new validation($in);
    $v->field('recurring_invoice_id', 'ID', 'required:exist[recurring_invoice.recurring_invoice_id]', gm('Invalid ID'));
    if($in['invoice_delivery']==1){
      $v->field('recipients','Recipients','required:not_empty:multi_email');
    }
    $is_ok = $v->run();
    if($is_ok){
      if($in['send_to']){
        $v->field('send_to', 'ID', 'email');
        $is_ok = $v->run();
        if(!$is_ok){
          msg::$error_big = gm("Please fill in a valid email address.");
          return $is_ok;
        }
      }
    }else{
      return $is_ok;
    }


    if (!$this->add_recurring_validate($in))
    {
      $is_ok = false;
    }
    return $is_ok;
  }

    /****************************************************************
  * function update_recurring(&$in)                               *
  ****************************************************************/
  function update_recurring(&$in)
  {
    if(!$this->update_recurring_validate($in))
    {
        json_out($in);
      return false;
    }

    $created_date= date(ACCOUNT_DATE_FORMAT);
    $updatedd_date= date(ACCOUNT_DATE_FORMAT);
    $fields = "";
    $start_date = mktime(0,0,0,date('n',strtotime($in['quote_ts'])),date('j',strtotime($in['quote_ts'])),date('Y',strtotime($in['quote_ts'])));
    /*if($start_date > time()){
        if(date('I', $start_date)=='1'){
          $start_date+=3600;
        }
        $next_date = $start_date;
        $fields = " start_date          =   '".$start_date."',
            next_date               =   '".$next_date."',";
    }*/ //according to akti-4095
    if($in['end_date']){
      $end_date = mktime(0,0,0,date('n',strtotime($in['end_date'])),date('j',strtotime($in['end_date'])),date('Y',strtotime($in['end_date'])));
      if(date('I',$end_date)=='1'){
        $end_date+=3600;
      }
      $fields = " end_date  =   '".$end_date."',";
    }else if(!$in['end_date']){
      $fields = " end_date  =   '0',";
    }

    if($in['buyer_id']){
      $customer_data=$this->dbu->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers 
        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
        WHERE customer_id='".$in['buyer_id']."' ");
        $in['buyer_name']=addslashes(stripslashes($customer_data->f('name').' '.$customer_data->f('l_name')));
        $in['buyer_email']=$customer_data->f('c_email');
        $in['buyer_phone']=$customer_data->f('comp_phone');
        $in['buyer_fax']=$customer_data->f('comp_fax');
        $in['seller_bwt_nr']=$customer_data->f('btw_nr');

      $buyer_info = $this->dbu->query("SELECT customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       //if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            $free_field=addslashes($address_info);
            $in['buyer_address']=addslashes($buyer_info->f('address'));
            $in['buyer_zip']=$buyer_info->f('zip');
            $in['buyer_city']=addslashes($buyer_info->f('city'));
            $in['buyer_country_id']=$buyer_info->f('country_id');
       /*}else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);
        }*/
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");

      $same_address=0;
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);
      }
    }

    if($in['payment_method']!='3'){
      $in['sepa_mandates'] =0;
    }

  //  $next_date = mktime(0,0,0,date('n',$in['quote_ts']),date('j',$in['quote_ts']),date('Y',$in['quote_ts']));
    //update invoice
    $this->dbu->query("UPDATE recurring_invoice SET ".$fields."
                                                frequency              =   '".$in['frequency']."',
                                                title                  =   '".$in['title']."',
                                                days                   =   '".return_value($in['days'])."',
                                                invoice_delivery       =   '".$in['invoice_delivery']."',
                                                discount               =   '".return_value($in['discount'])."',
                                                currency_type          =   '".$in['currency_type']."',
                                                type                   =   '".$in['type']."',
                                                notes2                 =   '".$in['notes2']."',
                                                buyer_name             =   '".$in['buyer_name']."',
                                                buyer_address          =   '".$in['buyer_address']."',
                                                buyer_email            =   '".$in['buyer_email']."',
                                                buyer_phone            =   '".$in['buyer_phone']."',
                                                buyer_fax              =   '".$in['buyer_fax']."',
                                                buyer_zip              =   '".$in['buyer_zip']."',
                                                buyer_city             =   '".$in['buyer_city']."',
                                                buyer_country_id       =   '".$in['buyer_country_id']."',
                                                buyer_state_id         =   '".$in['buyer_state_id']."',
                                                seller_id              =   '".$in['seller_id']."',
                                                contact_id             =   '".$in['contact_id']."',
                                                seller_name            =   '".$in['seller_name']."',
                                                seller_d_address       =   '".$in['seller_d_address']."',
                                                seller_d_zip           =   '".$in['seller_d_zip']."',
                                                seller_d_city          =   '".$in['seller_d_city']."',
                                                seller_d_country_id    =   '".$in['seller_d_country_id']."',
                                                seller_d_state_id      =   '".$in['seller_d_state_id']."',
                                                seller_b_address       =   '".$in['seller_b_address']."',
                                                seller_b_zip           =   '".$in['seller_b_zip']."',
                                                seller_b_city          =   '".$in['seller_b_city']."',
                                                seller_b_country_id    =   '".$in['seller_b_country_id']."',
                                                seller_b_state_id      =   '".$in['seller_b_state_id']."',
                                                seller_bwt_nr          =   '".$in['seller_bwt_nr']."',
                                                last_upd               =   '".$updatedd_date."',
                                                last_upd_by            =   '".$_SESSION['u_id']."',
                                                apply_discount         =   '".$in['apply_discount']."',
                                                vat                    =   '".$in['vat']."',
                                                use_html               =   '".$in['use_html']."',
                                                email_language         =   '".$in['email_language']."',
                                                remove_vat             =   '".$in['remove_vat']."',
                                                vat_regime_id          =   '".$in['vat_regime_id']."',
                                                mandate_id             =   '".$in['sepa_mandates']."',
                                                discount_line_gen      =   '".return_value($in['discount_line_gen'])."',
                                                free_field             =   '".$free_field."',
                                                same_address           =   '".$same_address."',
                                                main_address_id        =   '".$in['main_address_id']."',
                                                our_ref                =   '".$in['our_ref']."',
                                                your_ref               =   '".$in['your_ref']."',
                                                contract_id            =   '".$in['contract_id']."',
                                                acc_manager_id         =   '".$in['acc_manager_id']."',
                                                currency_rate          =   '".$in['currency_rate']."',
                                                end_contract           =   '".$in['end_contract']."',
                                                source_id              =   '".$in['source_id']."',
                                                type_id                =   '".$in['type_id']."',
                                                segment_id             =   '".$in['segment_id']."',
                                                payment_method         =   '".$in['payment_method']."',
                                                yuki_project_id     ='".$in['yuki_project_id']."'
                           WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."' ");
    //notes                   =   '".utf8_encode($in['notes'])."',

    // update multilanguage order note in note_fields
    $lang_note_id = $in['email_language'];
    if($lang_note_id == $in['email_language']){
      $active_lang_note = 1;
    }else{
      $active_lang_note = 0;
    }
        $langs = $this->dbu->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");

        $exist_notes = $this->dbu->field("SELECT active FROM note_fields WHERE item_id='".$in['recurring_invoice_id']."' AND lang_id='".$in['email_language']."'");
        if($exist_notes){
          $this->dbu->query("UPDATE note_fields SET
              active          =   '".$active_lang_note."',
              item_value        =   '".$in['NOTES']."'
              WHERE   item_type         =   'recurring_invoice'
              AND   item_name         =   'notes'
              AND   lang_id         =   '".$in['email_language']."'
              AND   item_id         =   '".$in['recurring_invoice_id']."'
          ");
        }else{
          $this->dbu->query("INSERT INTO note_fields SET
              active            =   '".$active_lang_note."',
              item_value        =   '".$in['NOTES']."',
              item_type         =   'recurring_invoice',
              item_name          =   'notes',
              lang_id           =   '".$in['email_language']."',
              item_id           =   '".$in['recurring_invoice_id']."'
          ");
        }
/*    $lang_note_id = $in['email_language'];
        $langs = $this->dbu->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        while($langs->next()){
          foreach ($in['translate_loop'] as $key => $value) {
            if($value['lang_id'] == $langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($lang_note_id == $langs->f('lang_id')){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }
          $this->dbu->query("UPDATE note_fields SET
                            active          =   '".$active_lang_note."',
                            item_value        =   '".$in['nota']."'
                        WHERE   item_type         =   'r_invoice'
                        AND   item_name         =   'notes'
                        AND   lang_id         =   '".$langs->f('lang_id')."'
                        AND   item_id         =   '".$in['recurring_invoice_id']."'
          ");
        }
        $cust_lang_id = $in['email_language'];
        $custom_langs =  $this->dbu->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
        while($custom_langs->next()) {
          foreach ($in['custom_translate_loop'] as $key => $value) {
            if($value['lang_id'] == $custom_langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($cust_lang_id == $custom_langs->f('lang_id')) {
            $cust_active_lang_note = 1;
          } else {
            $cust_active_lang_note = 0;
          }
          $this->dbu->query("UPDATE note_fields SET   active      = '".$cust_active_lang_note."',
                                item_value    = '".$in['nota']."'
                            WHERE   item_type     = 'r_invoice'
                            AND   item_name     = 'notes'
                            AND lang_id       = '".$custom_langs->f('lang_id')."'
                            AND item_id       = '".$in['recurring_invoice_id']."'
          ");
        }*/


        //INSERT EMAIL SETTINGS

        $this->dbu->query("UPDATE recurring_invoice SET
                                                email_subject       =   '".$in['subject']."',
                                                email_body      =   '".$in['text']."',
                                                include_pdf         =   '".$in['include_pdf']."',
                                                include_xml         =   '".$in['include_xml']."',
                                                copy                =   '".$in['copy']."',
                                                lid                 =   '".$in['email_language']."'
                           WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."' ");
        $this->dbu->query("DELETE FROM recurring_email_contact WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."'");

        if(!empty($in['recipients'])){
          foreach ($in['recipients'] AS $key => $value){

            $this->dbu->query("INSERT INTO recurring_email_contact SET
                                            recurring_invoice_id   =   '".$in['recurring_invoice_id']."',

                                            email         =   '".$value."' ");
            }
          }

    //INSERT LINES
    $this->dbu->query("DELETE FROM recurring_invoice_line WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."'");
    $invoice_total=0;
    $line_vat = 0;
    $discount = return_value($in['discount']);
    if($in['apply_discount'] < 2){
      $discount = 0;
    }

    $created_date_line=  date("Y-m-d H:i:s");

    $i =0;
    if(is_array($in['invoice_line'])){

      foreach ($in['invoice_line'] AS $nr => $nothing){
                $line_total = 0;
              if($nothing['description']){

            $line_total=(return_value($nothing['quantity']) * return_value($nothing['price']));
          $line_discount = return_value($nothing['discount_line']);
          if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
            $line_discount = 0;
          }
          $disc_line = $line_total * $line_discount / 100;
            $discount_vat = ( $line_total - $disc_line ) * ( $discount / 100);
            $invoice_total += $line_total - $disc_line - $discount_vat;


          $this->dbu->query("INSERT INTO recurring_invoice_line SET
                                                    name                =   '".html_entity_decode($nothing['description'])."',
                                                    recurring_invoice_id  =   '".$in['recurring_invoice_id']."',
                                                    quantity          =   '".return_value($nothing['quantity'])."',
                                                    price           =   '".return_value($nothing['price'])."',
                                                    amount          =   '".$line_total."',
                                                    f_archived            =   '0',
                                                    created               =   '".$created_date_line."',
                                                    created_by            =   '".$_SESSION['u_id']."',
                                                    last_upd              =   '".$created_date_line."',
                                                    vat           = '".return_value($nothing['vat'])."',
                                                    discount        = '".return_value($nothing['discount_line'])."',
                                                    last_upd_by           =   '".$_SESSION['u_id']."',
                                                    article_id      = '".$nothing['article_id']."',
                                                    content             = '".htmlentities($nothing['content'])."',
                                                    content_title             = '".htmlentities($nothing['title'])."',
                                                    item_code       = '".addslashes($nothing['article_code'])."',
                                                    sort_order        =   '".$i."',
                                                    tax_id      = '".$nothing['tax_id']."',
                                                    tax_for_article_id      = '".$nothing['tax_for_article_id']."' ");
              }
              $i++;
      }
    }

    $invoice_total=round($invoice_total,2);
    if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
      if($in['currency_type']){
        $invoice_total = $invoice_total*return_value($in['currency_rate']);
      }
    }
    $this->dbu->query("UPDATE recurring_invoice SET amount='".$invoice_total."' WHERE recurring_invoice_id='".$in['recurring_invoice_id']."'");

    /*msg::success(gm('Recurring invoice has been successfully updated'),'success');*/
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log('recurring_invoice','{l}Recurring invoice has been updated by{endl} '.get_user_name($_SESSION['u_id']),'recurring_invoice_id',$in['recurring_invoice_id']);

    return true;
  }

  /****************************************************************
  * function add_recurring(&$in)                                  *
  ****************************************************************/
  function add_recurring(&$in)
  {
    if(!$this->add_recurring_validate($in))
    {
          return false;
    }
      $time = time();
    $created_date= date(ACCOUNT_DATE_FORMAT,$time);

    $next_date = mktime(0,0,0,date('n',strtotime($in['quote_ts'])),date('j',strtotime($in['quote_ts'])),date('Y',strtotime($in['quote_ts'])));

    if($time > $next_date){

      $payment_term = $this->dbu->field("SELECT payment_term FROM customers WHERE customer_id='".$in['buyer_id']."' ");
      $in['invoice_due_date'] = strtotime($in['quote_ts']) + ( $payment_term * (60*60*24) ) ;
      $in['payment_term_type'] = $this->dbu->field("SELECT payment_term_type FROM customers WHERE customer_id='".$in['buyer_id']."' ");
      if(!$in['payment_term_type']){
        $in['payment_term_type'] = PAYMENT_TERM_TYPE;
      }
      $in['payment_type_choose'] = $in['payment_term_type'];
      if(!$in['invoice_id']){
        if(!$this->add($in)){
          json_out($in);
          return false;
        }
        $in['created_invoice_id']=$in['invoice_id'];

      }
      $m = date('n',$next_date);
      $d = date('j',$next_date);
      $y = date('Y',$next_date);
      switch($in['frequency']){
          case 1:  $next_date= $next_date + (7 * 24 * 60 * 60); break; //week
          case 2:
              $next_date= mktime(0, 0, 0, $m+1, $d, $y);
                      $next_month = $m+2;
                      if($next_month>12){ $next_month=$next_month-12; }
                      $last_day_time = strtotime($y.'-'.$next_month.' last day');
                      $last_day = date('d',$last_day_time);
                      if($d>$last_day)
                      {
                          $next_date= mktime(0, 0, 0, $m+1, $last_day, $y); //month
                      }
          break; //month
          case 3:
              $next_date= mktime(0, 0, 0, $m+3, $d, $y);
              $next_date= mktime(0, 0, 0, $m+3, $d, $y);
                      $next_month = $m+4;
                      if($next_month>12){ $next_month=$next_month-12; }
                      $last_day_time = strtotime($y.'-'.$next_month.' last day');
                      $last_day = date('d',$last_day_time);
                      if($d>$last_day)
                      {
                          $next_date= mktime(0, 0, 0, $m+3, $last_day, $y); //month
                      }
          break; //3 month
          case 4:  $next_date= mktime(0, 0, 0, $m, $d, $y+1); break; //year
          case 5:  $next_date= mktime(0, 0, 0, $m, $d+$in['days'], $y); break; //days;
      }

    }

    if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       //if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            $free_field=addslashes($address_info);
            $in['buyer_address']=addslashes($buyer_info->f('address'));
            $in['buyer_zip']=$buyer_info->f('zip');
            $in['buyer_city']=addslashes($buyer_info->f('city'));
            $in['buyer_country_id']=$buyer_info->f('country_id');

       /*}else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);
        }*/
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");

      $same_address=0;
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);
      }
    }
    if(date('I',$next_date)=='1'){
      $next_date+=3600;
    }
    $strt_date=strtotime($in['quote_ts']);
    if(date('I',$strt_date)=='1'){
      $strt_date+=3600;
    }
    if($in['end_date']){
      $end_date = mktime(0,0,0,date('n',strtotime($in['end_date'])),date('j',strtotime($in['end_date'])),date('Y',strtotime($in['end_date'])));
      if(date('I',$end_date)=='1'){
        $end_date+=3600;
      }
    }

    if($in['payment_method']!='3'){
      $in['sepa_mandates'] =0;
    }

    if(!$in['currency_rate']){
      $in['currency_rate'] = 1;
    }

    $customer_data=$this->dbu->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers 
        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
        WHERE customer_id='".$in['buyer_id']."' ");
    $in['buyer_name']=addslashes(stripslashes($customer_data->f('name').' '.$customer_data->f('l_name')));
    $in['buyer_email']=$customer_data->f('c_email');
    $in['buyer_phone']=$customer_data->f('comp_phone');
    $in['buyer_fax']=$customer_data->f('comp_fax');
    $in['seller_bwt_nr']=$customer_data->f('btw_nr');

    $in['recurring_invoice_id']=$this->dbu->insert("INSERT INTO recurring_invoice SET
                                                 title            =   '".$in['title']."',
                                                start_date          =   '".$strt_date."',
                                                frequency               =   '".$in['frequency']."',
                                                days                    =   '".return_value($in['days'])."',
                                                next_date               =   '".$next_date."',
                                                end_date                = '".$end_date."',
                                                invoice_delivery        =   '".$in['invoice_delivery']."',
                                                discount              =   '".return_value($in['discount'])."',
                                                req_payment               =   '".return_value($in['req_payment'])."',
                                                currency_type         =   '".$in['currency_type']."',
                                                vat_regime_id         =   '".$in['vat_regime_id']."',
                                                type                  =   '0',
                                                status                =   '0',
                                                f_archived            =   '0',
                                                paid                =   '0',
                                                notes2                    =   '".utf8_encode($in['notes2'])."',
                                                buyer_id                =   '".$in['buyer_id']."',
                                                buyer_name              =   '".$in['buyer_name']."',
                                                buyer_address         =   '".$in['buyer_address']."',
                                                buyer_email           =   '".$in['buyer_email']."',
                                                buyer_phone           =   '".$in['buyer_phone']."',
                                                buyer_fax           =   '".$in['buyer_fax']."',
                                                buyer_zip               =   '".$in['buyer_zip']."',
                                                buyer_city              =   '".$in['buyer_city']."',
                                                buyer_country_id        =   '".$in['buyer_country_id']."',
                                                buyer_state_id        =   '".$in['buyer_state_id']."',
                                                seller_id               =   '".$in['seller_id']."',
                                                seller_name             =   '".$in['seller_name']."',
                                                seller_d_address        =   '".$in['seller_d_address']."',
                                                seller_d_zip            =   '".$in['seller_d_zip']."',
                                                seller_d_city           =   '".$in['seller_d_city']."',
                                                seller_d_country_id     =   '".$in['seller_d_country_id']."',
                                                seller_d_state_id       =   '".$in['seller_d_state_id']."',
                                                seller_b_address        =   '".$in['seller_b_address']."',
                                                seller_b_zip          =   '".$in['seller_b_zip']."',
                                                seller_b_city         =   '".$in['seller_b_city']."',
                                                seller_b_country_id     =   '".$in['seller_b_country_id']."',
                                                seller_b_state_id       =   '".$in['seller_b_state_id']."',
                                                seller_bwt_nr           =   '".$in['seller_bwt_nr']."',
                                                created               =   '".$created_date."',
                                                created_by            =   '".$_SESSION['u_id']."',
                                                last_upd              =   '".$created_date."',
                                                last_upd_by           =   '".$_SESSION['u_id']."',
                                                vat           =   '".$in['vat']."',
                                                email_language      = '".$in['email_language']."',
                                                contact_id        = '".$in['contact_id']."',
                                                apply_discount      = '".$in['apply_discount']."',
                                                use_html        = '".$in['use_html']."',
                                                currency_rate     =   '".$in['currency_rate']."',
                                                remove_vat      = '".$in['remove_vat']."',
                                                mandate_id      ='".$in['sepa_mandates']."',
                                                free_field    ='".$free_field."',
                                                same_address  ='".$same_address."',
                                                main_address_id           = '".$in['main_address_id']."',
                                                discount_line_gen       =   '".return_value($in['discount_line_gen'])."',
                                                our_ref               ='".$in['our_ref']."',
                                                your_ref              ='".$in['your_ref']."',
                                                contract_id          ='".$in['contract_id']."',
                                                acc_manager_id      = '".$in['acc_manager_id']."',
                                                end_contract        ='".$in['end_contract']."',
                                                source_id           ='".$in['source_id']."',
                                                type_id             ='".$in['type_id']."',
                                                segment_id          ='".$in['segment_id']."',
                                                payment_method      =   '".$in['payment_method']."',
                                                yuki_project_id     ='".$in['yuki_project_id']."'  ");
    //notes                   =   '".utf8_encode($in['notes'])."',

    // add multilanguage order notes in note_fields
        $lang_note_id = $in['email_language'];
          if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }
/*        $langs = $this->dbu->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        while($langs->next()){
          foreach ($in['translate_loop'] as $key => $value) {
            if($value['lang_id'] == $langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($lang_note_id == $langs->f('lang_id')){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }*/
          // we have $in['notes'] only when we create invoices from to_invoice
         /* if($in['notes']){
            $lang_id = $this->dbu->field("SELECT lang_id FROM pim_lang WHERE active = '1' AND sort_order = 1");
            if($lang_id == $langs->f('lang_id')){
              $act_lang_note = 1;
            }else{
              $act_lang_note = 0;
            }
            if($in['from_to_invoice'] === true){
              if($lang_note_id == $langs->f('lang_id')){
                $act_lang_note = 1;
              }else{
                $act_lang_note = 0;
              }
            }
            $this->dbu->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['invoice_id']."',
                            lang_id         =   '".$langs->f('lang_id')."',
                            active          =   '".$act_lang_note."',
                            item_type         =   'invoice',
                            item_name         =   'notes',
                            item_value        =   '".$in['notes']."'
            ");
          }else{*/
            $this->dbu->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['recurring_invoice_id']."',
                            lang_id         =   '".$in['email_language']."',
                            active          =   '".$active_lang_note."',
                            item_type         =   'recurring_invoice',
                            item_name         =   'notes',
                            item_value        =   '".$in['NOTES']."'
            ");
         /* }*/

        /*}*/
        #custom extra languages
/*        $custom_lang_note_id = $in['email_language'];
        $custom_langs = $this->dbu->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
        while($custom_langs->next()) {
          foreach ($in['custom_translate_loop'] as $key => $value) {
            if($value['lang_id'] == $custom_langs->f('lang_id')){
              $in['nota'] = $value['NOTES'];
              break;
            }
          }
          if($custom_lang_note_id == $custom_langs->f('lang_id')) {
            $custom_active_lang_note = 1;
          } else {
            $custom_active_lang_note = 0;
          }
            $this->dbu->insert("INSERT INTO note_fields SET   item_id     = '".$in['recurring_invoice_id']."',
                                      lang_id     = '".$custom_langs->f('lang_id')."',
                                      active      = '".$custom_active_lang_note."',
                                      item_type     = 'invoice',
                                      item_name     = 'notes',
                                      item_value    = '".$in['nota']."' ");

        }*/

        //INSERT EMAIL SETTINGS

        $this->dbu->query("UPDATE recurring_invoice SET
                                                email_subject       =   '".$in['subject']."',
                                                email_body      =   '".$in['text']."',
                                                include_pdf         =   '".$in['include_pdf']."',
                                                include_xml         =   '".$in['include_xml']."',
                                                copy                =   '".$in['copy']."',
                                                lid                 =   '".$in['email_language']."'
                           WHERE recurring_invoice_id = '".$in['recurring_invoice_id']."' ");

        if(!empty($in['recipients']) ){
              if($in['invoice_delivery'] && $in['created_invoice_id']){
                
                if(defined('DRAFT_INVOICE_NO_NUMBER') && DRAFT_INVOICE_NO_NUMBER == 1 ){
                    $serial_number=generate_invoice_number(DATABASE_NAME,false);
                    $this->dbu->query("UPDATE tblinvoice SET serial_number = '".addslashes($serial_number)."' WHERE id='".$in['invoice_id']."' ");
                    $this->dbu->query("UPDATE tblinvoice SET ogm = '".generate_ogm(DATABASE_NAME,$in['invoice_id'])."' WHERE id='".$in['invoice_id']."' ");
                 }  

                 $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
                  $params = array();
                  $params['use_custom'] = 0;
                  if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
                    $params['logo'] = $inv->f('pdf_logo');
                    $params['type']=$inv->f('pdf_layout');
                    $params['logo']=$inv->f('pdf_logo');
                    $params['template_type'] = $inv->f('pdf_layout');
                  }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
                    $params['custom_type']=$inv->f('pdf_layout');
                    unset($params['type']);
                    $params['logo']=$inv->f('pdf_logo');
                    $params['template_type'] = $inv->f('pdf_layout');
                    $params['use_custom'] = 1;
                  }else{
                  $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
                  $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_BODY_PDF_FORMAT : ACCOUNT_INVOICE_BODY_PDF_FORMAT;
                  }
                  #if we are using a customer pdf template
                  if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
                    $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
                    unset($params['type']);
                  }
                  $params['id'] = $in['invoice_id'];
                  $params['lid'] = $in['email_language'];
                   $params['type_pdf'] =  $inv->f('type');
                  $params['save_as'] = 'F';
                  $this->generate_pdf($params);
                   
              }

              if($in['invoice_delivery']){
                  if($in['copy']){
                      $user_email = $this->dbu_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
                      if($user_email && !in_array($user_email,$in['recipients'])){
                        array_push($in['recipients'],$user_email);
                      }
                     
                  }
              }
                foreach ($in['recipients'] AS $key => $value){
                    $this->dbu->query("INSERT INTO recurring_email_contact SET
                                                  recurring_invoice_id   =   '".$in['recurring_invoice_id']."',
                                                  email         =   '".$value."' ");
                    
               }
               if($in['invoice_delivery'] && $in['created_invoice_id']){  
                 
                 send_invoice_email($in['recurring_invoice_id'],$in['email_language'],$in['invoice_id'],DATABASE_NAME);
                }
           
          }


    //INSERT LINES
    $line_vat = 0;
    // $discount = 0;
    $discount = return_value($in['discount']);
    if($in['apply_discount'] < 2){
      $discount = 0;
    }
    $created_date_line=  date("Y-m-d H:i:s");
    $invoice_total = 0;
    $i = 0;
    if(is_array($in['invoice_line'])){
      foreach ($in['invoice_line'] AS $nr => $nothing){
        $line_total = 0;
                if($nothing['description']){

          $line_total=(return_value($nothing['quantity']) * return_value($nothing['price']));
        $line_discount = return_value($nothing['discount_line']);
        if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
          $line_discount = 0;
        }
        $disc_line = $line_total * $line_discount / 100;
          $discount_vat = ( $line_total - $disc_line ) * ( $discount / 100);
          $invoice_total += $line_total - $disc_line - $discount_vat;

                $this->dbu->query("INSERT INTO recurring_invoice_line SET
                                                name              =   '".html_entity_decode($nothing['description'])."',
                                                recurring_invoice_id    =   '".$in['recurring_invoice_id']."',
                                                quantity          = '".return_value($nothing['quantity'])."',
                                                price                   =   '".return_value($nothing['price'])."',
                                                amount                  =   '".$line_total."',
                                                f_archived              =   '0',
                                                created                 =   '".$created_date_line."',
                                                created_by              =   '".$_SESSION['u_id']."',
                                                last_upd                =   '".$created_date_line."',
                                                vat           = '".return_value($nothing['vat'])."',
                                                discount        = '".return_value($nothing['discount_line'])."',
                                                last_upd_by             =   '".$_SESSION['u_id']."',
                                                article_id      = '".$nothing['article_id']."',
                                                content             = '".htmlentities($nothing['content'])."',
                                                content_title             = '".htmlentities($nothing['title'])."',
                                                item_code       = '".addslashes($nothing['article_code'])."',
                                                sort_order        =   '".$i."',
                                                tax_id    =   '".$nothing['tax_id']."',
                                                tax_for_article_id    =   '".$nothing['tax_for_article_id']."' ");
        }
        $i++;
      }
    }

    $invoice_total=round($invoice_total,2);
    if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
      if($in['currency_type']){
        $invoice_total = $invoice_total*return_value($in['currency_rate']);
      }
    }

    $this->dbu->query("UPDATE recurring_invoice SET amount='".$invoice_total."' WHERE recurring_invoice_id='".$in['recurring_invoice_id']."'");

    $count = $this->dbu->field("SELECT COUNT(recurring_invoice_id) FROM recurring_invoice ");
      if($count == 1){
        doManageLog('Created the first recurring invoice.');
      }
    msg::success(gm('Recurring invoice was successfully created'),"success");

    addTracking(array(
                  'target_id'         => $in['recurring_invoice_id'],
                  'target_type'       => '13',
                  'target_buyer_id'   => $in['buyer_id'],
                  'lines'             => array()
                ));

    insert_message_log('recurring_invoice','{l}Recurring invoice was created by{endl} '.get_user_name($_SESSION['u_id']),'recurring_invoice_id',$in['recurring_invoice_id']);
      if($in['created_invoice_id']){
        global $config;
        insert_message_log('recurring_invoice','<a href="[SITE_URL]invoice/view/'.$in['created_invoice_id'].'">{l}Invoice created from recurring invoice{endl}</a>','recurring_invoice_id',$in['recurring_invoice_id']);

        $log_date = $this->dbu->field("SELECT `date` FROM logging WHERE pag='invoice' AND field_name='invoice_id' AND field_value='".$in['created_invoice_id']."' AND message LIKE '%was created by%' ");

        $this->dbu->query("DELETE FROM logging WHERE pag='invoice' AND field_name='invoice_id' AND field_value='".$in['created_invoice_id']."' AND message LIKE '%was created by%' ");
        insert_message_log('invoice','{l}Invoice was automatically created by{endl} <a href="[SITE_URL]recinvoice/view/'.$in['recurring_invoice_id'].'">{l}Recurring Invoice{endl}</a>','invoice_id',$in['created_invoice_id'],0,'', false, 0, $log_date);

        $this->db->query("DELETE FROM tracking WHERE target_id='".$in['created_invoice_id']."' AND target_type='1' ");
        $tracking_data=array(
                  'target_id'         => $in['created_invoice_id'],
                  'target_type'       => '1',
                  'target_buyer_id'   => $in['buyer_id'],
                  'lines'             => array(
                                                array('origin_id'=>$in['recurring_invoice_id'],'origin_type'=>'13')
                                          )
                );
        addTracking($tracking_data);
      }
      return true;
  }

  function updateRemSettings(&$in){
    $with_name=0;
    if(strpos($in['text'],'[!FIRST_NAME!]')){
      $with_name=1;
    }else if(strpos($in['text'],'[!LAST_NAME!]')){
      $with_name=1;
    }else if(strpos($in['subject'],'[!FIRST_NAME!]')){
      $with_name=1;
    }else if(strpos($in['subject'],'[!LAST_NAME!]')){
      $with_name=1;
    }
    $set = "text  = '".$in['text']."' ";
    if($in['use_html']){
        $set = "html_content  = '".$in['text']."' ";
    }
    $this->dbu->query("UPDATE sys_message SET
          subject = '".$in['subject']."',
          footnote='".$in['footnote']."',
          ".$set."
          WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
    if($in['lang_code'] == 'du'){
      $mess=$this->dbu->field("SELECT sys_message_id FROM sys_message WHERE name='".$in['name']."' AND lang_code='nl' ");
      if(is_null($mess)){
          $this->dbu->query("INSERT INTO sys_message SET subject = '".$in['subject']."',
                                                  footnote='".$in['footnote']."',
                                                  name='".$in['name']."',
                                                  lang_code='nl',
                                                  ".$set." ");
      }else{
        $this->dbu->query("UPDATE sys_message SET
          subject = '".$in['subject']."',
          footnote='".$in['footnote']."',
          ".$set."
          WHERE name='".$in['name']."' AND lang_code='nl' ");
      }
    }
    $this->dbu->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");

    if($in['languages'] >=1000) {
      $lang_code = $this->dbu->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
    } else {
      $lang_code = $this->dbu->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."'");
    }

    $i=1;
    foreach($in['tabs'] as $key=>$value){
      if($i==1){
        $type_g='first';
      }else if($i==2){
        $type_g='second';
      }else{
        $type_g='third';
      }

      $grade = $this->dbu->query("SELECT * FROM tblinvoice_grades WHERE grade='".$type_g."' AND lang_code='".$lang_code."'");
      if(!$grade->next()){
        if(strpos($value['text'],'[!FIRST_NAME!]')){
          $with_name=1;
        }else if(strpos($value['text'],'[!LAST_NAME!]')){
          $with_name=1;
        }else if(strpos($value['title'],'[!FIRST_NAME!]')){
          $with_name=1;
        }else if(strpos($value['title'],'[!LAST_NAME!]')){
          $with_name=1;
        }
        $this->dbu->query("INSERT INTO tblinvoice_grades SET
                    grade='".$type_g."',
                    value='".return_value($value['penalty_value'])."',
                    type='".$value['penalty_type']."',
                    message='".$value['text']."',
                    title= '".$value['title']."',
                    lang_code='".$lang_code."'");
      }else{
        if(strpos($value['text'],'[!FIRST_NAME!]')){
          $with_name=1;
        }else if(strpos($value['text'],'[!LAST_NAME!]')){
          $with_name=1;
        }else if(strpos($value['title'],'[!FIRST_NAME!]')){
          $with_name=1;
        }else if(strpos($value['title'],'[!LAST_NAME!]')){
          $with_name=1;
        }
        $this->dbu->query("UPDATE tblinvoice_grades SET
                  value='".return_value($value['penalty_value'])."',
                  type='".$value['penalty_type']."'
                  WHERE grade='".$type_g."'");
        $this->dbu->query("UPDATE tblinvoice_grades SET
                  message='".$value['text']."',
                  title= '".$value['title']."'
                   WHERE grade='".$type_g."' AND lang_code='".$lang_code."'");

        if($lang_code=='du'){
          $this->dbu->query("UPDATE tblinvoice_grades SET
                  message='".$value['text']."',
                  title= '".$value['title']."'
                   WHERE grade='".$type_g."' AND lang_code='nl'");
        }
      }
      $i++;
    }
    $exist=$this->dbu->field("SELECT value FROM settings WHERE constant_name='USE_REMINDER_NAMES' AND type='1' ");
    if(!$exist && $exist!='0'){
      $this->dbu->query("INSERT INTO settings SET constant_name='USE_REMINDER_NAMES',type='1',value='".$with_name."' ");
    }else{
      $this->dbu->query("UPDATE settings SET value='".$with_name."' WHERE constant_name='USE_REMINDER_NAMES' AND type='1' ");
    }
    /*msg::success( gm('Reminder settings have been successfully updated'),'success');*/
    msg::success ( gm("Changes have been saved."),'success');
    return true;
  }

  function pay_incomming_invoice (&$in){
    if(!empty($in['payment_date'])){
      $in['payment_date'] =strtotime($in['payment_date']);
      $in['payment_date'] = mktime(23,59,59,date('n',$in['payment_date']),date('j',$in['payment_date']),date('y',$in['payment_date']));
    }

    $in['total_amount_due1']=return_value($in['total_amount_due1']);

    if($in['cost']){
      $in['total_amount_due1'] += return_value($in['cost']);
    }

    if(!$this->pay_incomming_validate($in))
    {
      return false;
    }
    $payment_date=date("Y-m-d",$in['payment_date']);
    $payment_view_date=date(ACCOUNT_DATE_FORMAT,$in['payment_date']);
    $no_cost="";
    if($in['cost'] == '0,00'){
      $in['cost']=$no_cost;
    }else{
       $in['cost']='  -  '.$in['cost'];
    }

    $info=$payment_view_date.'  -  '.place_currency(display_number($in['total_amount_due1']),$currency).'  -  '.$in['notes'].$in['cost'];

    $this->dbu->query("INSERT INTO tblinvoice_incomming_payments SET
                          invoice_id  = '".$in['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$in['total_amount_due1']."',
                          info        = '".$info."',
                          payment_method = '".$in['payment_method']."' ");

    $payments = $this->dbu->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' ");
    $total = number_format($this->dbu->field("SELECT total_with_vat FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' "),2,'.','');
    if($total <= $payments){
      $this->dbu->query("UPDATE tblinvoice_incomming SET paid='1' WHERE invoice_id='".$in['invoice_id']."'");
    }else{
      $this->dbu->query("UPDATE tblinvoice_incomming SET paid='2' WHERE invoice_id='".$in['invoice_id']."'");
    }
 if($in['from_list'] == 1){
      // $in['do'] = 'invoice-invoices';
      ark::$controller='purchase_invoices';
    }else{
      ark::$controller='purchase_invoice';
    }

    /* msg::success(('Payment has been successfully recorded'),'success');*/
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log('purchase_invoice','{l}Payment has been successfully recorded by {endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);
    // $in['pagl'] = $this->pag;

  }

  function pay_incomming_invoice_bulk (&$in){
    $v = new validation($in);
    $v->field('payment_date','Payment date','required');
    if(!$v->run()){
      json_out($in);
    }

    if(!empty($in['payment_date'])){
      $in['payment_date'] =strtotime($in['payment_date']);
      $in['payment_date'] = mktime(23,59,59,date('n',$in['payment_date']),date('j',$in['payment_date']),date('y',$in['payment_date']));
    }

    $payment_date=date("Y-m-d",$in['payment_date']);
    $payment_view_date=date(ACCOUNT_DATE_FORMAT,$in['payment_date']);

    $p_invoices = $this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id IN (".implode(",",array_keys($_SESSION['add_to_purchase'])).") ")->getAll();
    foreach($p_invoices as $p_invoice){
      $invoice_total=$p_invoice['total_with_vat'];
      $total_payed = $this->db->field("SELECT COALESCE(SUM(amount),0) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$p_invoice['invoice_id']."'  ");
      $amount_due = round($invoice_total - $total_payed,2);

      $info=$payment_view_date.'  -  '.place_currency(display_number($amount_due),$currency).'  -  '.$in['notes'];

      $this->dbu->query("INSERT INTO tblinvoice_incomming_payments SET
                            invoice_id  = '".$p_invoice['invoice_id']."',
                            date    = '".$payment_date."',
                            amount    = '".$amount_due."',
                            info        = '".$info."',
                            payment_method = '".$in['payment_method']."' ");

      $payments = $this->dbu->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$p_invoice['invoice_id']."' ");
      $total = number_format($this->dbu->field("SELECT total_with_vat FROM tblinvoice_incomming WHERE invoice_id='".$p_invoice['invoice_id']."' "),2,'.','');
      if($total <= $payments){
        $this->dbu->query("UPDATE tblinvoice_incomming SET paid='1' WHERE invoice_id='".$p_invoice['invoice_id']."'");
        unset($_SESSION['add_to_purchase'][$p_invoice['invoice_id']]);
      }else{
        $this->dbu->query("UPDATE tblinvoice_incomming SET paid='2' WHERE invoice_id='".$p_invoice['invoice_id']."'");
      }
      insert_message_log('purchase_invoice','{l}Payment has been successfully recorded by {endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$p_invoice['invoice_id']);

    }
    msg::success ( gm("Changes have been saved."),'success');   
    json_out($in);
  }

  function edit_payment_incomming_invoice($in){
    $v=new validation($in);
    $v->field("invoice_id","Invoice Id","required:exist[tblinvoice_incomming.invoice_id]");
    $v->field("payment_id","Payment Id","required:exist[tblinvoice_incomming_payments.payment_id]");
    $v->field('payment_date','Payment date','required');
    $v->field('total_amount_due1','Amount','required:numeric');
    if(!$v->run()){
      json_out($in);
    }
    if(!empty($in['payment_date'])){
      $in['payment_date'] =strtotime($in['payment_date']);
      $in['payment_date'] = mktime(23,59,59,date('n',$in['payment_date']),date('j',$in['payment_date']),date('y',$in['payment_date']));
    }

    $in['total_amount_due1']=return_value($in['total_amount_due1']);

    if($in['cost']){
      $in['total_amount_due1'] += return_value($in['cost']);
    }

    $payment_date=date("Y-m-d",$in['payment_date']);
    $payment_view_date=date(ACCOUNT_DATE_FORMAT,$in['payment_date']);
    $no_cost="";
    if($in['cost'] == '0,00'){
      $in['cost']=$no_cost;
    }else{
       $in['cost']='  -  '.$in['cost'];
    }

    $info=$payment_view_date.'  -  '.place_currency(display_number($in['total_amount_due1']),$currency).'  -  '.$in['notes'].$in['cost'];

    $this->dbu->query("UPDATE tblinvoice_incomming_payments SET
                          `date`    = '".$payment_date."',
                          amount    = '".$in['total_amount_due1']."',
                          info        = '".$info."',
                          payment_method = '".$in['payment_method']."' 
                          WHERE payment_id='".$in['payment_id']."' ");

    $payments = $this->dbu->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' ");
    $total = number_format($this->dbu->field("SELECT total_with_vat FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' "),2,'.','');
    if($total <= $payments){
      $this->dbu->query("UPDATE tblinvoice_incomming SET paid='1' WHERE invoice_id='".$in['invoice_id']."'");
    }else{
      $this->dbu->query("UPDATE tblinvoice_incomming SET paid='2' WHERE invoice_id='".$in['invoice_id']."'");
    }
    msg::success ( gm("Changes have been saved."),'success');
    return true;

  }

  function delete_payment (&$in){
    $this->dbu->query("DELETE FROM tblinvoice_incomming_payments WHERE payment_id  = '".$in['payment_id']."'");
    $payments = $this->dbu->field("SELECT count(payment_id) FROM tblinvoice_incomming_payments WHERE invoice_id = '".$in['invoice_id']."' ");
    /*if($payments==1){
      $this->dbu->query("UPDATE tblinvoice_incomming SET paid='0' WHERE invoice_id='".$in['invoice_id']."'");
    }*/
    if($payments>=1){
      $this->dbu->query("UPDATE tblinvoice_incomming SET paid='2' WHERE invoice_id='".$in['invoice_id']."'");
    }else{
      $this->dbu->query("UPDATE tblinvoice_incomming SET paid='0' WHERE invoice_id='".$in['invoice_id']."'");
    }  
    if($in['from_list'] == 1){
      // $in['do'] = 'invoice-invoices';
      ark::$controller='purchase_invoices';
    }else{
      ark::$controller='purchase_invoice';
    }

    /* msg::success(('Payment has been successfully recorded'),'success');*/
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log('purchase_invoice','{l}Payment has been removed by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);

  }

  function delete_all_payments(&$in){
    if(!$this->delete_all_payments_validate($in))
    {
      return false;
    }

    $this->dbu->query("DELETE FROM tblinvoice_payments WHERE invoice_id  = '".$in['invoice_id']."'");
    $this->dbu->query("UPDATE tblinvoice SET paid='0', status='0' WHERE id='".$in['invoice_id']."'");
    msg::success(('Payments had been successfully deleted'),'success');
    insert_message_log('invoice','{l}All payments had been removed by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);

  }

  function delete_all_payments_validate(&$in)
  {

    $v = new validation($in);
    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
    $is_ok = $v->run();
    return $is_ok;
  }
    /****************************************************************
  * function delete_payment_invoice(&$in)                                *
  *********************************************************msg::success(('Payment has been successfully recorded'),'success');*******/
  function delete_payment_invoice (&$in){
    /*$payments = $this->dbu->field("SELECT count(payment_id) FROM tblinvoice_payments WHERE invoice_id = '".$in['invoice_id']."' ");
    if($payments==1){
      $this->dbu->query("UPDATE tblinvoice SET paid='0', status='0' WHERE id='".$in['invoice_id']."'");
    }elseif($payments>=2){
      $this->dbu->query("UPDATE tblinvoice SET paid='2', status='0' WHERE id='".$in['invoice_id']."'");
    }*/

    $from_proforma_id = $this->dbu->field("SELECT id FROM tblinvoice WHERE proforma_id = '".$in['invoice_id']."' ");
    $has_proforma_id = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    /*if($from_proforma_id){

       $this->dbu->query("UPDATE tblinvoice SET paid='0', status='0' WHERE id='".$from_proforma_id."'");
    
    }*/
    $payment_data = $this->dbu->query("SELECT credit_payment,info,codapayment_id,amount FROM tblinvoice_payments WHERE payment_id  = '".$in['payment_id']."' ");
    if($payment_data->f('credit_payment')){
      $inv_lb_eq=strpos($payment_data->f('info'), 'invoice_id');
      $str_w_nr=substr($payment_data->f('info'), $inv_lb_eq+1);
      $has_credit_id=(int) preg_replace('/\D/', '', $str_w_nr);
      if($has_credit_id){
        $this->dbu->query("UPDATE tblinvoice SET c_invoice_id='0',paid='0', status='0' WHERE id='".$has_credit_id."'"); 
        $track_line_origin_ids=$this->dbu->query("SELECT tracking_line.line_id FROM tracking INNER JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id WHERE target_id='".$has_credit_id."' AND target_type='8' AND tracking_line.origin_id='".$in['invoice_id']."' AND (tracking_line.origin_type='1' OR tracking_line.origin_type='7') ")->getAll(); 
        if(!empty($track_line_origin_ids)){
          $ids_string="";
          foreach($track_line_origin_ids as $value){
            $ids_string.="'".$value['line_id']."',";
          }
          $ids_string=rtrim($ids_string,",");
          $this->dbu->query("DELETE FROM tracking_line WHERE line_id IN (".$ids_string.") ");
        }
      }
    }
    if($payment_data->f('codapayment_id')){
      $old_balance = $this->dbu->field("SELECT amount_left FROM codapayments_line WHERE id='".$payment_data->f('codapayment_id')."' ");
      $amount_left = $old_balance + $payment_data->f('amount');
      $this->dbu->query("UPDATE codapayments_line SET status='0', amount_left ='".$amount_left."' WHERE id='".$payment_data->f('codapayment_id')."' ");
    }
    $this->dbu->query("DELETE FROM tblinvoice_payments WHERE payment_id  = '".$in['payment_id']."'");
    $payments = $this->dbu->field("SELECT count(payment_id) FROM tblinvoice_payments WHERE invoice_id = '".$in['invoice_id']."' ");
  
    if($payments>=1){
      $this->dbu->query("UPDATE tblinvoice SET paid='2' WHERE id='".$in['invoice_id']."'");
      if($from_proforma_id){
       $this->dbu->query("UPDATE tblinvoice SET paid='2' WHERE id='".$from_proforma_id."'"); 
      }
    }else{
      $this->dbu->query("UPDATE tblinvoice SET paid='0', status='0' WHERE id='".$in['invoice_id']."'");
      if($from_proforma_id){
        $this->dbu->query("UPDATE tblinvoice SET paid='0', status='0' WHERE id='".$from_proforma_id."'"); 
      }
      if($has_proforma_id){
        $payments_proforma = $this->dbu->field("SELECT count(id) FROM tblinvoice WHERE proforma_id = '".$has_proforma_id."' AND paid!=0");
         if($payments_proforma>=1){
            $this->dbu->query("UPDATE tblinvoice SET  paid='2' WHERE id='".$has_proforma_id."'"); 
         }else{
            $this->dbu->query("UPDATE tblinvoice SET  paid='0', status='0' WHERE id='".$has_proforma_id."'"); 
         }
        
      }
    }
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log('invoice','{l}Payment has been removed by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);

     # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);
    # for pdf

    return true;

  }

  /****************************************************************
  * function edit_payment_invoice(&$in)                                *
  ****************************************************************/
  function edit_payment_invoice (&$in){
     $total = 0;
     $tblinvoice=$this->dbu->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE  id='".$in['invoice_id']."'");
      $tblinvoice->next();
      $currency = get_commission_type_list($this->dbu->f('currency_type'));

    $big_discount = $tblinvoice->f("discount");
    if($tblinvoice->f('apply_discount') < 2){
      $big_discount =0;
    }
    $lines = $this->dbu->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
    while ($lines->next()) {
      $line_disc = $lines->f('discount');
      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
        $line_disc = 0;
      }
      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
      $line = $line - $line * $big_discount / 100;

      $total += $line + ($line * ( $lines->f('vat') / 100));

    }

    $filter = " invoice_id='".$in['invoice_id']."' AND payment_id!='".$in['payment_id']."'";
    //already payed
    if($tblinvoice->f('proforma_id')){
      $filter = " (invoice_id='".$in['invoice_id']."' OR invoice_id='".$tblinvoice->f('proforma_id')."') ";
    }
    $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE {$filter}  AND credit_payment='0' ");
    $this->dbu->move_next();

    $total_payed = round($this->dbu->f('total_payed'),2);

    $negative = 1;
    if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
      $negative = -1;
    }
    $this->dbu->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE {$filter}  AND credit_payment='1' ");
    $this->dbu->move_next();
    $total_payed += ($negative*round($this->dbu->f('total_payed'),2));

    $req_payment_value = $total;
    if($tblinvoice->f('type')==1){
      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
    }

    $amount_due = round($req_payment_value - $total_payed,2);

    $total_amount_due1 = abs(return_value($in['total_amount_due1']));

    if($in['cost']){
      $total_amount_due1 += return_value($in['cost']);
    }

   //console::log($in['total_amount_due1'], $amount_due);
    //Mark as payed
    if($total_amount_due1 == $amount_due || $total_amount_due1>$amount_due || round($amount_due-$total_amount_due1,2)==0.01){
      $this->dbu->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$in['invoice_id']."'");
      $p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      if($p){
        $this->dbu->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
      }
    }
    else {
      $this->dbu->query("UPDATE tblinvoice SET paid='2', status='0' WHERE id='".$in['invoice_id']."'");
    }


     $payment_view_date=date(ACCOUNT_DATE_FORMAT,strtotime($in['payment_date']));
     if($in['notes']){
      $in['notes']='  -  '.$in['notes'];
      }
      $no_cost="";
      if($in['cost'] == '0,00'){
        $in['cost']=$no_cost;
      }else{
         $in['cost']='  -  '.$in['cost'];
      }
     $info=$payment_view_date.'  -  '.place_currency(display_number(return_value($in['total_amount_due1'])),$currency).'  -  '.$in['notes'].$in['cost'];
     $coda_pay=0;
      if($in['coda_pay']){
        $coda_pay=$in['coda_pay'];
      }


    //Mark as Will not be paid
    if($in['not_paid']){
      $this->dbu->query("UPDATE tblinvoice SET not_paid='1' WHERE id='".$in['invoice_id']."'");
      msg::success(gm('Payment has been successfully recorded'),'success');
      return true; 
    } else {
      $this->dbu->query("UPDATE tblinvoice SET not_paid='0' WHERE id='".$in['invoice_id']."'");
    }

    $this->dbu->query("UPDATE tblinvoice_payments SET date='".$in['payment_date']."', 
                                                      amount    = '".$total_amount_due1."',
                                                      info        = '".$info."',
                                                      codapayment_id='".$coda_pay."',
                                                      payment_method = '".$in['payment_method']."' 
                       WHERE payment_id='".$in['payment_id']."' ");
    msg::success ( gm("Changes have been saved."),'success');

    insert_message_log('invoice','{l}Payment has been edited by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
       # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);
    # for pdf

    return true;
  }

    /****************************************************************
  * function pay_incomming_validate(&$in)                                *
  ****************************************************************/
  function pay_incomming_validate(&$in)
  {

    $v = new validation($in);
    $v->field('invoice_id', 'ID', 'required:exist[tblinvoice_incomming.invoice_id]', gm('Invalid ID'));
    $v->field('payment_date','Payment date','required');
    $v->field('total_amount_due1','Amount','required:numeric');
    $is_ok = $v->run();
    return $is_ok;
  }

  function deleteExportList(&$in)
  {
    $this->dbu->query("DELETE FROM tblinvoice_exported WHERE list_id='".$in['list_id']."' ");
    $this->dbu->query("DELETE FROM tblinvoice_field WHERE list_id='".$in['list_id']."' ");
    $this->dbu->query("DELETE FROM tblinvoice_export_list WHERE list_id='".$in['list_id']."' ");
    msg::success(gm('List deleted'),'success');
    return true;
  }
    /**
   * undocumented function
   *
   * @return void
   * @author
   **/

  function save_export(&$in)
  {
    if(!$this->save_export_valid($in)){
      json_out($in);
      return false;
    }
    $show = '0';
    if($in['show_headers'] !=''){
      $show = '1';
    }
    $convert = '0';
    if($in['convert'] !=''){
      $convert = '1';
    }
    $in['list_id'] = $this->dbu->insert("INSERT INTO tblinvoice_export_list SET name='".$in['name']."', delimit_er='".$in['delimit_er']."', show_headers='".$show."', `convert`='".$convert."' ");
    $i=0;
    foreach($in['fields'] as $key=>$value){
       $this->dbu->query("INSERT INTO tblinvoice_field SET list_id='".$in['list_id']."',field_id='".$value['id']."', header='".$value['header']."', value='".$value['checked']."', sort_order='".$i."' ");
       $i++;
    }
    foreach($in['lines'] as $key=>$value){
        $this->dbu->query("INSERT INTO tblinvoice_field SET list_id='".$in['list_id']."',field_id='".$value['id']."', header='".$value['header']."', value='".$value['checked']."', sort_order='".$i."' ");
        $i++;
    }
    foreach($in['custom_fields'] as $key=>$value){
      if($value['value'] !=''){
          $this->dbu->query("INSERT INTO tblinvoice_field SET list_id='".$in['list_id']."', header='".$value['header']."', value='".$value['value']."', custom='1', sort_order='".$i."' ");
          $i++;
      }
    }
    msg::success(gm('Changes saved'),'success');
    return true;
  }

  function save_export_valid(&$in)
  {
    $v = new validation();
    if($in['do']=='invoice-export-invoice-save_export'){
      $v->field('name','Invoice Nr','required:unique[tblinvoice_export_list.name]');
    }else{
      $v->field('name','Invoice Nr',"required:unique[tblinvoice_export_list.name.( list_id!='".$in['list_id']."' )]");
    }
    return $v->run();
  }

  function update_export(&$in)
  {
    if(!$this->save_export_valid($in)){
      json_out($in);
      return false;
    }
    if(!$in['list_id']){
      json_out($in);
      return false;
    }
    $show = '0';
    if($in['show_headers'] !=''){
      $show = '1';
    }
    $convert = '0';
    if($in['convert'] !=''){
      $convert = '1';
    }
    $this->dbu->query("UPDATE tblinvoice_export_list SET name='".$in['name']."', delimit_er='".$in['delimit_er']."', show_headers='".$show."', `convert`='".$convert."' WHERE list_id='".$in['list_id']."' ");
    $this->dbu->query("DELETE FROM tblinvoice_field WHERE list_id='".$in['list_id']."' ");
    $i=0;
    foreach($in['fields'] as $key=>$value){
       $this->dbu->query("INSERT INTO tblinvoice_field SET list_id='".$in['list_id']."',field_id='".$value['id']."', header='".$value['header']."', value='".$value['checked']."', sort_order='".$i."' ");
       $i++;
    }
    foreach($in['lines'] as $key=>$value){
        $this->dbu->query("INSERT INTO tblinvoice_field SET list_id='".$in['list_id']."',field_id='".$value['id']."', header='".$value['header']."', value='".$value['checked']."', sort_order='".$i."' ");
        $i++;
    }
    foreach($in['custom_fields'] as $key=>$value){
      if($value['value'] !=''){
          $this->dbu->query("INSERT INTO tblinvoice_field SET list_id='".$in['list_id']."', header='".$value['header']."', value='".$value['value']."', custom='1', sort_order='".$i."' ");
          $i++;
      }
    }
    msg::success(gm('Changes saved'),'success');
    return true;
  }

  function markExport(&$in)
  {
    $this->dbu->query("INSERT INTO tblinvoice_exported SET list_id='".$in['list_id']."', invoice_id='".$in['invoice_id']."', type='".$in['type']."' ");
    $in['value']=0;
    $this->setForImport($in);
    return true;
  }

  function markSelectedExport(&$in)
  {
    if($_SESSION['invoice_to_export']){
      $inv_data=$this->dbu->query("SELECT * FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."' ")->getAll();
      foreach($inv_data as $inv){
        $this->dbu->query("INSERT INTO tblinvoice_exported SET invoice_id='".$inv['invoice_id']."', type='".$in['type']."', ".$in['app']."=1 ");
        insert_message_log('invoice','{l}Invoice was exported by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$inv['invoice_id']);
        if($_SESSION['tmp_export_add_to_app'] && array_key_exists($inv['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
          unset($_SESSION['tmp_export_add_to_app'][$inv['invoice_id']]);
        }
      }
      $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."' ");
    }
    return true;
  }

    function restoreSelectedExport(&$in)
  {
    
    if($_SESSION['invoice_to_export']){
      $inv_data=$this->dbu->query("SELECT * FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."' ")->getAll();
      foreach($inv_data as $inv){
        $this->dbu->query("DELETE FROM tblinvoice_exported WHERE invoice_id='".$inv['invoice_id']."' AND type='".$in['type']."' AND ".$in['app']."=1 ");
        insert_message_log('invoice','{l}Invoice was exported by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$inv['invoice_id']);
        if($_SESSION['tmp_export_add_to_app'] && array_key_exists($inv['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
          unset($_SESSION['tmp_export_add_to_app'][$inv['invoice_id']]);
        }
      }
      $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."' ");
    }
    return true;
  }

  function setForImport(&$in)
  {
    if($in['list']){
      foreach($in['list'] as $key=>$value){
        if($value['value'] == 1){
          $this->dbu->query("INSERT INTO tblinvoice_to_export SET invoice_id='".$value['invoice_id']."', `time`='".$_SESSION['invoice_to_export']."', return_to_list='0' ");
        }else{
          $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$value['invoice_id']."' AND `time`='".$_SESSION['invoice_to_export']."'  AND return_to_list!='1' ");
        }
      }
    }else{
        if($in['value'] == 1){
          $this->dbu->query("INSERT INTO tblinvoice_to_export SET invoice_id='".$in['invoice_id']."', `time`='".$_SESSION['invoice_to_export']."', return_to_list='0' ");
        }else{
          $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND `time`='".$_SESSION['invoice_to_export']."'  AND return_to_list!='1' ");
        }
    }  
    $all_pages_selected=false;
    $minimum_selected=false;
    $invoices_marked=$this->dbu->field("SELECT COUNT('invoice_id') FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."'");
    if($invoices_marked){
      if($_SESSION['tmp_export_add_to_app'] && count($_SESSION['tmp_export_add_to_app']) == $invoices_marked){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

  function setForImportAll(&$in){
    $all_pages_selected=false;
    $minimum_selected=false;
    if($in['value']){
      if($_SESSION['invoice_to_export']){
        $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."' ");
        if($_SESSION['tmp_export_add_to_app'] && count($_SESSION['tmp_export_add_to_app'])){
          foreach($_SESSION['tmp_export_add_to_app'] as $inv_id=>$val){
            $this->dbu->query("INSERT INTO tblinvoice_to_export SET invoice_id='".$inv_id."', `time`='".$_SESSION['invoice_to_export']."', return_to_list='0' ");
          }
          $all_pages_selected=true;
        }
      }         
    }else{
      if($_SESSION['invoice_to_export']){
        $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."' ");
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

 function unmarkExport(&$in)
  {
    $in['actiune'] = gm('Restored');
    $this->dbu->query("DELETE FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND type='".$in['type']."' ");
    $in['value']=0;
    $this->setForImportUnmark($in);
    return true;
  }

  function setForImportUnmark(&$in)
  {
    if($in['list']){
        foreach($in['list'] as $key=>$value){
          if($in['value'] == 1){
            $this->dbu->query("INSERT INTO tblinvoice_to_export SET invoice_id='".$value['invoice_id']."', `time`='".$_SESSION['invoice_to_export']."', return_to_list='1' ");
          }else{
            $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$value['invoice_id']."' AND `time`='".$_SESSION['invoice_to_export']."' AND return_to_list='1' ");
          }
        }
    }else{
        if($in['value'] == 1){
        $this->dbu->query("INSERT INTO tblinvoice_to_export SET invoice_id='".$in['invoice_id']."', `time`='".$_SESSION['invoice_to_export']."', return_to_list='1' ");
      }else{
        $this->dbu->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND `time`='".$_SESSION['invoice_to_export']."' AND return_to_list='1' ");
      }
    }
    $all_pages_selected=false;
    $minimum_selected=false;
    $invoices_marked=$this->dbu->field("SELECT COUNT('invoice_id') FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."'");
    if($invoices_marked){
      if($_SESSION['tmp_export_add_to_app'] && count($_SESSION['tmp_export_add_to_app']) == $invoices_marked){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }


  function updatePurchaseInvoiceOrder(&$in)
  {
    $v=new validation($in);
    $v->field("order_id","Order Id","required");
    $v->field("invoice_id","Invoice Id","required:exist[tblinvoice_incomming.invoice_id]");
    if(!$v->run()){
      json_out($in);
    }

    $orders=$this->dbu->field("SELECT c_order_id FROM tblinvoice_incomming  WHERE invoice_id='".$in['invoice_id']."' ");
    if($orders){
      $orders= $orders.','.$in['order_id'];

    }else{
       $orders= $in['order_id'];
    }

    $this->dbu->query("UPDATE tblinvoice_incomming SET c_order_id='".$orders."' WHERE invoice_id='".$in['invoice_id']."' ");

    $amount_inc=$this->dbu->field("SELECT sum(total_with_vat) FROM tblinvoice_incomming WHERE FIND_IN_SET('".$in['order_id']."',c_order_id) ");
     /*amount inv */
    $amount_ord=$this->dbu->field("SELECT amount FROM pim_p_orders WHERE p_order_id = '".$in['order_id']."'");
     /*amount order*/
     /*update order set full = 2;*/
         /*     if(inv>=order){
      update order set full = 1;
     }*/
     if($amount_inc>=$amount_ord){
      $this->dbu->query("UPDATE pim_p_orders SET order_full=1 WHERE p_order_id = '".$in['order_id']."'");
    }else{
      $this->dbu->query("UPDATE pim_p_orders SET order_full=2 WHERE p_order_id = '".$in['order_id']."'");
    }

    $trace_id=$this->dbu->field("SELECT trace_id FROM tblinvoice_incomming  WHERE invoice_id='".$in['invoice_id']."' ");
    if($trace_id){
      $trace_line_id=$this->dbu->field("SELECT line_id FROM tracking_line WHERE origin_id='".$in['order_id']."' AND origin_type='6' AND trace_id='".$trace_id."' ");
      if(!$trace_line_id){
        $this->dbu->query("INSERT INTO tracking_line SET origin_id='".$in['order_id']."',  origin_type='6', trace_id='".$trace_id."'");
      }
    }

  msg::success(('Settings updated'),'success');
    insert_message_log('purchase_invoice','{l}Purchase Order has been successfully recorded by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);


    return true;
  }
  function delete_order(&$in){

      $this->dbu->query("SELECT c_order_id FROM  tblinvoice_incomming  WHERE invoice_id='".$in['invoice_id']."'");
      $array1 = array($in['order_id']);
      $array2 = explode(',', $this->dbu->f('c_order_id'));
      $array3 = array_diff($array2, $array1);
      $orders = implode(',', $array3);



    $this->dbu->query("UPDATE tblinvoice_incomming SET c_order_id='".$orders."' WHERE invoice_id='".$in['invoice_id']."'");

    $this->dbu->query("UPDATE pim_p_orders SET order_full=2 WHERE p_order_id = '".$in['order_id']."'");

    $trace_id=$this->dbu->field("SELECT trace_id FROM tblinvoice_incomming  WHERE invoice_id='".$in['invoice_id']."' ");
    if($trace_id){
      $this->dbu->query("DELETE FROM tracking_line WHERE origin_id='".$in['order_id']."' AND origin_type='6' AND trace_id='".$trace_id."' ");
    }

    msg::success(('Settings updated'),'success');

    insert_message_log('purchase_invoice','{l}Purchase Order has been removed by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);

    return true;
  }

 function add_incomming(&$in){
    if(!$this->add_incomming_validate($in))
    {
      if($in['from_cron']){      
        return false;
      }else{
        json_out($in);
      }   
      return false;
    }
    if(!$this->validate_invoice_number($in)){
      if($in['from_cron']){
        return false;
      }else{
        json_out($in);
      }    
      return false;
    }

    if((!$in['buyer_id'])&&(!$in['contact_id'])){
      if($in['from_cron']){
        return false;
      }else{
        msg::error(gm('A provider must be selected!'),'error');
        json_out($in);
      }   
      return false;
    }

    if(!$this->validate_iban($in)){
        if($in['from_cron']){
          return false;
        }else{
          msg::set('buyer_iban',gm('IBAN not valid'));
          json_out($in);
        }       
        return false;
      }

    $this->do_purchase_inv_nr($in);
    if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customers.bank_iban,customers.bank_bic_code,customers.bank_name, customer_addresses.address,customer_addresses.zip,customer_addresses.city, customer_addresses.country_id
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            $free_field=addslashes($address_info);
       }else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);
        }

      if($in['buyer_iban']){
          $iban_country = iban_get_country_part($in['buyer_iban']);
          if( $iban_country=='BE'){
              $client = new SoapClient('http://www.ibanbic.be/IBANBIC.asmx?WSDL');
              $bban = $client->getBelgianBBAN(array('Value' => $in['buyer_iban']))->getBelgianBBANResult;
              $in['buyer_bic'] = $client->BBANtoBIC(array('Value' => $bban))->BBANtoBICResult;
              $in['buyer_bank'] = $client->BBANtoBANKNAME(array('Value' => $bban))->BBANtoBANKNAMEResult;
             
            }else{
              $in['buyer_bank'] = iban_get_bank_part($in['buyer_iban']);
              $in['buyer_bic'] = iban_get_branch_part ($in['buyer_iban']);
            }

        }  
        //var_dump($in['buyer_iban'], $in['buyer_bic'], $in['buyer_bank']);exit();
      if($in['buyer_iban'] && !$buyer_info->f('bank_iban')){
        $sql='';
        if($in['buyer_bic'] && !$buyer_info->f('bank_bic_code')){
          $sql .=", bank_bic_code ='".$in['buyer_bic']."'";
        }
        if($in['buyer_bank'] && !$buyer_info->f('bank_name')){
          $sql .=", bank_name ='".$in['buyer_bank']."'";
        }
        $this->dbu->query("UPDATE customers SET
                                    bank_iban          ='".$in['buyer_iban']."'
                                    ".$sql."
                                    WHERE customer_id  ='".$in['buyer_id']."' ");
      }
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");

      $same_address=0;
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);
      }
   }

/*     $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $delivery_address_id=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);   */

    $amazon = 1;
    if($in['file_path']){
      if($in['db_name']){
        $d = new drop('invoices',$in['buyer_id'],null,true,$in['db_name'],null,null,$in['contact_id']);
      }else{
        $d = new drop('invoices',$in['buyer_id'],null,true,DATABASE_NAME,null,null,$in['contact_id']);
      }
      
      $e= $d->moveFromTemp(urldecode($in['linkpath']), substr(urldecode($in['linkpath']),strrpos(urldecode($in['linkpath']),'/')+1));
      if(!$e){
        msg::error(gm("Could not move the file. A file with the same name already exists!"),'error');
        if($in['from_cron']){
          return false;
        } else{
           json_out($in);
        }      
        return false;
      }
      $in['link_url']=$in['from_cron'] ? str_replace("\\","/",$in['drop_file_path']): str_replace("\\","",$in['drop_file_path']);
    }

if($in['type'] == '1'){
    $booking_number='';
}else{
    $booking_number=$in['booking_number'];
}

    $in['invoice_id']=$this->dbu->insert("INSERT INTO tblinvoice_incomming SET

                        booking_number      ='".$booking_number."',
                        invoice_number      ='".$in['invoice_number']."',
                        invoice_date        ='".strtotime($in['invoice_date'])."',
                        due_date            ='".strtotime($in['due_date'])."',
                        supplier_id         ='".$in['buyer_id'] ."',
                        contact_id          ='".$in['contact_id'] ."',
                        buyer_address       ='".$in['buyer_address'] ."',
                        buyer_zip           ='".$in['buyer_zip'] ."',
                        buyer_city          ='".$in['buyer_city'] ."',
                        buyer_country_id    ='".$in['buyer_country_id'] ."',
                        buyer_state_id      ='".$in['buyer_state_id'] ."',
                        buyer_iban          ='".$in['buyer_iban'] ."',
                        buyer_bic           ='".$in['buyer_bic'] ."',
                        buyer_bank          ='".$in['buyer_bank'] ."',
                        delivery_address_id ='".$in['delivery_address_id'] ."',
                        free_field          ='".$free_field ."',
                        type                ='".$in['type'] ."',
                        same_address        ='".$same_address."',
                        main_address_id     ='".$in['main_address_id']."',
                        allow_disconto      ='".$in['allow_disconto'] ."',
                        disconto            ='".return_value($in['disconto'])."',
                        file_path           ='".urlencode($e->path_display)."',
                        ogm                 ='".$in['ogm']."',
                        a_transfer          ='".$in['a_transfer']."',
                        currency_type       ='".$in['currency_type']."',
                        expense_category_id = '".$in['expense_category_id']."',
                        reference           = '".addslashes($in['reference'])."' ");

    $total = 0;
    $total_vat = 0;

    foreach ($in['invoice_line'] as $key => $value) {
        if($in['allow_disconto'] == 1){
          $line = return_value($value['total_no_vat']) - return_value($value['total_no_vat'])*return_value($in['disconto'])/100;
          $total_vat += $line*return_value($value['vat_line'])/100 + $line;
        }else{
          $total_vat += return_value($value['total_no_vat'])*return_value($value['vat_line'])/100 + return_value($value['total_no_vat']);
        }
        $total += return_value($value['total_no_vat']);
        $this->dbu->query("INSERT INTO tblinvoice_incomming_line SET
                              invoice_id = '".$in['invoice_id']."',
                              vat_line   = '".return_value($value['vat_line'])."',
                              total      = '".return_value($value['total_no_vat'])."' ");
    }
    $this->dbu->query("UPDATE tblinvoice_incomming SET
                                    total          ='".$total."',
                                    total_with_vat ='".$total_vat."'
                                    WHERE invoice_id='".$in['invoice_id']."' ");

    if($in['p_order_id']){
      $tracking_data=array(
            'target_id'         => $in['invoice_id'],
            'target_type'       => '10',
            'target_buyer_id'   => $in['buyer_id'],
            'lines'             => array(
                                array('origin_id'=>$in['p_order_id'],'origin_type'=>'6')
              )
      );
      $porders=$this->dbu->field("SELECT c_order_id FROM tblinvoice_incomming  WHERE invoice_id='".$in['invoice_id']."' ");
      if($porders){
        $porders= $porders.','.$in['p_order_id'];
      }else{
         $porders= $in['p_order_id'];
      }
      $this->dbu->query("UPDATE tblinvoice_incomming SET c_order_id='".$porders."' WHERE invoice_id='".$in['invoice_id']."' ");
      $amount_pord=$this->dbu->field("SELECT amount FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
      if($total_vat>=$amount_pord){
        $this->dbu->query("UPDATE pim_p_orders SET order_full=1 WHERE p_order_id = '".$in['p_order_id']."'");
      }else{
        $this->dbu->query("UPDATE pim_p_orders SET order_full=2 WHERE p_order_id = '".$in['p_order_id']."'");
      }
    }else{
      $tracking_data=array(
            'target_id'         => $in['invoice_id'],
            'target_type'       => '10',
            'target_buyer_id'   => $in['buyer_id'],
            'lines'             => array()
      );
    }
  
    if($in['db_name']){
      addTracking($tracking_data,0,$in['db_name']);
    }else{
      addTracking($tracking_data);
    }

    $i=0;
    foreach($in['invoice_row'] as $key=>$value){
      $this->dbu->query("INSERT INTO tblinvoice_incomming_pdf_line SET 
                name                =   '".htmlspecialchars($value['row_description'],ENT_QUOTES,'UTF-8')."',
                invoice_id          =   '".$in['invoice_id']."',
                quantity            =   '".return_value($value['row_quantity'])."',
                price               =   '".return_value($value['row_unit_price'])."',
                amount              =   '".return_value($value['row_amount'])."',
                f_archived          =   '0',
                created             =   '".time()."',
                created_by          =   '".$_SESSION['u_id']."',
                last_upd            =   '".time()."',
                last_upd_by         =   '".$_SESSION['u_id']."',
                vat                 =   '".return_value($value['vat'])."',
                item_code           = '".addslashes($value['article_code'])."',
                sort_order          =   '".$i."'  ");
        $i++;
    }

     $this->saveTmpFileToAmazon($in);

     /** Add Purchase Orders Notes **/
     //Since there is no language option, set the default language to be English
     $default_note = $this->db->field("SELECT value FROM default_data WHERE type='invoice_note' ");

     $note_id = $this->dbu->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['invoice_id']."',
                            lang_id         =   '1',
                            active          =   '1',
                            item_type         =   'purchase_invoice',
                            item_name         =   'notes',
                            item_value        =   '".addslashes($default_note)."'
            ");

     if($in['notes']){
        $this->dbu->query("UPDATE note_fields SET
                item_value        =   '".$in['notes']." '
                WHERE item_type  =   'purchase_invoice'
                AND   item_name         =   'notes'
                AND   note_fields_id         =   '".$note_id."'
              ");    
     }
      // //End Add default notes values based on language      
      /** End Add Purchase Orders Notes **/

     msg::success ( gm("Changes have been saved."),'success');

     if($in['from_cron']){
      $this->dbu->query("INSERT INTO logging SET pag='purchase_invoice', message='".addslashes('{l}Purchase Invoice was created by an automatic CRON{endl}')."', field_name='purchase_invoice_id', field_value='".$in['invoice_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
     }else{
        insert_message_log('purchase_invoice','{l}Purchase Invoice was created by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);
     }   
      // $in['pagl'] = $this->pag;     

    return true;
  }

    /****************************************************************
  * function add_incomming_validate(&$in)                                   *
  ****************************************************************/
  function add_incomming_validate(&$in)
  {
    $v = new validation();
    $v->field('invoice_number','Invoice Nr','required',gm('Invoice Nr').' '.gm('is required'));
    $v->field('invoice_date','Invoice date','required',gm('Invoice date').' '.gm('is required'));
    $is_ok=$v->run();
    if($is_ok){
      $combo_sql="";
      if(ark::$method=='update_incomming'){
        $combo_sql.=" AND invoice_id!='".$in['incomming_invoice_id']."' ";
      }
      $combo_exist=$this->dbu->field("SELECT invoice_id FROM tblinvoice_incomming WHERE invoice_number='".$in['invoice_number']."' AND supplier_id='".$in['buyer_id']."' ".$combo_sql." ");
      if($combo_exist){
        msg::error(gm('Combination of supplier and invoice number should be unique'),'invoice_number');
        $is_ok=false;
      }
    }
    return $is_ok;

  }

  function validate_invoice_number(&$in)
  {
    $v = new validation();

    if(ark::$method == 'update_incomming'){
      $v->field('booking_number', 'Invoice Nr', "unique[tblinvoice_incomming.booking_number.(invoice_id!='".$in['incomming_invoice_id']."')]",gm('Invoice Nr').' '.gm('is already in use'));
    }else{
      $v->field('booking_number', 'Invoice Nr', 'unique[tblinvoice_incomming.booking_number]',gm('Invoice Nr').' '.gm('is already in use'));
    }
    return $v->run();
  }

  function validate_iban(&$in)
  {

    if($in['buyer_iban']){
      require_once 'libraries/php-iban/php-iban.php';
      if(!verify_iban($in['buyer_iban'],false)){
        return false;
      }     
    }
    return true;
  }


    function update_incomming(&$in){
      if(!$this->add_incomming_validate($in))
      {
        json_out($in);
        return false;
      }
      if(!$this->validate_invoice_number($in)){
        json_out($in);
        return false;
      }

      if((!$in['buyer_id'])&&(!$in['contact_id'])){
        msg::error(gm('A provider must be selected!'),'error');
        json_out($in);
        return false;
      }

      if(!$this->validate_iban($in)){
        msg::error(gm('IBAN not valid'),'error');
        json_out($in);
        return false;
      }

/*      $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $delivery_address_id=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);    */
    if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customers.bank_iban,customers.bank_bic_code,customers.bank_name, customer_addresses.*
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            if($buyer_info->f('address_id') != $in['main_address_id']){
                $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
                $address_info = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
            }
            $free_field=addslashes($address_info);
       }else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);
        }
         if($in['buyer_iban']){
            $iban_country = iban_get_country_part($in['buyer_iban']);
            if( $iban_country=='BE'){
                $client = new SoapClient('http://www.ibanbic.be/IBANBIC.asmx?WSDL');
                $bban = $client->getBelgianBBAN(array('Value' => $in['buyer_iban']))->getBelgianBBANResult;
                $in['buyer_bic'] = $client->BBANtoBIC(array('Value' => $bban))->BBANtoBICResult;
                $in['buyer_bank'] = $client->BBANtoBANKNAME(array('Value' => $bban))->BBANtoBANKNAMEResult;
               
              }else{
                $in['buyer_bank'] = iban_get_bank_part($in['buyer_iban']);
                $in['buyer_bic'] = iban_get_branch_part ($in['buyer_iban']);
              }

          }  
          //var_dump($in['buyer_iban'], $in['buyer_bic'], $in['buyer_bank']);exit();
        if($in['buyer_iban'] && !$buyer_info->f('bank_iban')){
          $sql='';
          if($in['buyer_bic'] && !$buyer_info->f('bank_bic_code')){
            $sql .=", bank_bic_code ='".$in['buyer_bic']."'";
          }
          if($in['buyer_bank'] && !$buyer_info->f('bank_name')){
            $sql .=", bank_name ='".$in['buyer_bank']."'";
          }
          $this->dbu->query("UPDATE customers SET
                                      bank_iban          ='".$in['buyer_iban']."'
                                      ".$sql."
                                      WHERE customer_id  ='".$in['buyer_id']."' ");
        }
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");

      $same_address=0;
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);
      }
   }

    $this->dbu->query("UPDATE tblinvoice_incomming SET
                booking_number  ='".$in['booking_number']."',
                invoice_number  ='".$in['invoice_number']."',

                invoice_date    ='".strtotime($in['invoice_date'])."',
                due_date        ='".strtotime($in['due_date'])."',
                total           ='".return_value($in['total'])."',
                total_with_vat  ='".return_value($in['total_vat'])."',
                vat_perc        ='".$in['vat_perc']."',
                vat_amount      ='".return_value($in['vat_val'])."',
                supplier_id     ='".$in['buyer_id'] ."',
                booking_number  ='".$in['booking_number']."',
                allow_disconto  ='".$in['allow_disconto']."',
                disconto        ='".return_value($in['disconto'])."',
                contact_id      ='".$in['contact_id'] ."',
                buyer_address   ='".$in['buyer_address'] ."',
                buyer_zip       ='".$in['buyer_zip'] ."',
                buyer_city      ='".$in['buyer_city'] ."',
                buyer_country_id='".$in['buyer_country_id'] ."',
                buyer_state_id  ='".$in['buyer_state_id'] ."',
                buyer_iban          ='".$in['buyer_iban'] ."',
                buyer_bic           ='".$in['buyer_bic'] ."',
                buyer_bank          ='".$in['buyer_bank'] ."',
                delivery_address_id ='".$in['delivery_address_id'] ."',
                free_field          ='".$free_field ."',
                main_address_id     = '".$in['main_address_id']."',
                same_address        ='".$same_address."',
                type                ='".$in['type'] ."',
                ogm                 ='".$in['ogm']."',
                currency_type       ='".$in['currency_type']."',
                expense_category_id = '".$in['expense_category_id']."',
                reference           = '".addslashes($in['reference'])."'
            WHERE invoice_id='".$in['incomming_invoice_id']."'");

      $this->dbu->query("DELETE FROM  tblinvoice_incomming_line WHERE invoice_id= '".$in['incomming_invoice_id']."'");

    $total = 0;
    $total_vat = 0;
    foreach ($in['invoice_line'] as $key => $value) {


        if($in['allow_disconto'] == 1){
          $line = return_value($value['total_no_vat']) - return_value($value['total_no_vat'])*return_value($in['disconto'])/100;
          $total_vat += $line*return_value($value['vat_line'])/100 + $line;
        }else{
          $total_vat += return_value($value['total_no_vat'])*return_value($value['vat_line'])/100 + return_value($value['total_no_vat']);
        }
        $total += return_value($value['total_no_vat']);
        $this->dbu->query("INSERT INTO tblinvoice_incomming_line SET
                              invoice_id = '".$in['incomming_invoice_id']."',
                              vat_line   = '".return_value($value['vat_line'])."',
                              total      = '".return_value($value['total_no_vat'])."' ");

    }

      $this->dbu->query("UPDATE tblinvoice_incomming SET
                                    total          ='".$total."',
                                    total_with_vat ='".$total_vat."'
                                    WHERE invoice_id='".$in['incomming_invoice_id']."' ");
      $this->dbu->query("DELETE FROM tblinvoice_incomming_pdf_line WHERE invoice_id='".$in['incomming_invoice_id']."' ");
      $i=0;
      foreach($in['invoice_row'] as $key=>$value){
          $this->dbu->query("INSERT INTO tblinvoice_incomming_pdf_line  SET 
                          name                =   '".htmlspecialchars($value['row_description'],ENT_QUOTES,'UTF-8')."',
                          invoice_id          =   '".$in['incomming_invoice_id']."',
                          quantity            =   '".return_value($value['row_quantity'])."',
                          price               =   '".return_value($value['row_unit_price'])."',
                          amount              =   '".return_value($value['row_amount'])."',
                          f_archived          =   '0',
                          created             =   '".time()."',
                          created_by          =   '".$_SESSION['u_id']."',
                          last_upd            =   '".time()."',
                          last_upd_by         =   '".$_SESSION['u_id']."',
                          vat                 =   '".return_value($value['vat'])."',
                          item_code           = '".addslashes($value['article_code'])."',
                          sort_order          =   '".$i."'  ");
        $i++;
      }

    $in['invoice_id']=$in['incomming_invoice_id'];

    // Get Purchase Invoice notes for the default language English
     $exist_notes = $this->dbu->field("SELECT active FROM note_fields WHERE item_id='".$in['invoice_id']."' AND lang_id='1' AND item_type = 'purchase_invoice'");
        if($exist_notes){
          $this->dbu->query("UPDATE note_fields SET
              active          =   '1',
              item_value        =   '".$in['notes']."'
              WHERE   item_type         =   'purchase_invoice'
              AND   item_name         =   'notes'
              AND   lang_id         =   '1'
              AND   item_id         =   '".$in['invoice_id']."'
          ");
        }else{
          //Set as inactive all the languages
          $this->dbu->query("UPDATE note_fields SET
              active          =   '0'
              WHERE   item_type         =   'purchase_invoice'
              AND   item_name         =   'notes'
              AND   item_id         =   '".$in['invoice_id']."'
          ");

          $this->dbu->query("INSERT INTO note_fields SET
              active            =   '1',
              item_value        =   '".$in['notes']."',
              item_type         =   'invoice',
              item_name          =   'notes',
              lang_id           =   '1',
              item_id           =   '".$in['invoice_id']."'
          ");
        }

      // Get Purchase Invoice notes for the default language English

    /*msg::success(('Invoice info successfully updated!'),'success');*/
    msg::success ( gm("Changes have been saved."),'success');

    insert_message_log('purchase_invoice','{l}Purchase Invoice has been updated by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['incomming_invoice_id']);
    // $in['pagl'] = $this->pag;
    // $in['pag']='invoice';

    return true;
  }



 function do_purchase_inv_nr( &$in){
    if(!$in['booking_number']){
        $in['booking_number'] = $in['db_name'] ? generate_binvoice_number($in['db_name']) : generate_binvoice_number();
      }
      if(ark::$method == 'add_incomming'){
        $v = new validation();
        $v->field('booking_number', 'Invoice Nr', 'unique[tblinvoice_incomming.booking_number]',gm('Invoice Nr').' '.gm('is already in use').'.<br />');
      if(!$v->run()){
        $in['booking_number'] = $in['db_name'] ? generate_binvoice_number($in['db_name']) : generate_binvoice_number();
      }
    }
    return true;
  }

  /****************************************************************
* function delete(&$in)                                          *
****************************************************************/
function incomming_invoices_delete(&$in)
{
  if(!$this->delete_inc_validate($in)){
    return false;
  }
  $pos=$this->dbu->field("SELECT c_order_id FROM tblinvoice_incomming WHERE invoice_id='".$in['id']."'");
  if($pos && !empty($pos)){
    $this->dbu->query("UPDATE pim_p_orders SET order_full=2 WHERE p_order_id IN (".implode(",",explode(",", $pos)).") ");
  }
  $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice_incomming WHERE invoice_id='".$in['id']."' ");
  $this->dbu->query("DELETE FROM tblinvoice_incomming_line WHERE invoice_id = '".$in['id']."'");
  $this->dbu->query("DELETE FROM tblinvoice_incomming_pdf_line WHERE invoice_id = '".$in['id']."'");
  $this->dbu->query("DELETE FROM tblinvoice_incomming_split WHERE invoice_id = '".$in['id']."'");
  $this->dbu->query("DELETE FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['id']."' ");
  $this->dbu->query("DELETE FROM tblinvoice_incomming WHERE invoice_id = '".$in['id']."'");
  if($tracking_trace){
      $this->dbu->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
      $this->dbu->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
  }
 msg::success(('Invoice Deleted'),'success');

    return true;
}
/****************************************************************
* function delete_validate(&$in)                                          *
****************************************************************/
function delete_inc_validate(&$in)
{
  $is_ok = true;
  if(!$in['id']){
    msg::$error = gm('Invalid ID').'.';
    return false;
  }
  else{
    $this->dbu->query("SELECT invoice_id FROM tblinvoice_incomming WHERE invoice_id='".$in['id']."'");
    if(!$this->dbu->move_next()){
      msg::$error = gm('Invalid ID').'.';
      return false;
    }
  }

    return $is_ok;

}
/****************************************************************
* function setFinal(&$in)                                          *
****************************************************************/
function setFinal(&$in)
 {
    if(!$in['invoice_id']){
      json_out($in);
    }
    $paid_sql="";
    $total_value = $this->dbu->field("SELECT COALESCE(SUM(total),0) FROM tblinvoice_incomming_line WHERE invoice_id='".$in['invoice_id']."' ");
    if($total_value == 0){
      $paid_sql=", paid='1' ";
    }
    $this->dbu->query("UPDATE tblinvoice_incomming set status=1,approved_by='".$_SESSION['u_id']."' ".$paid_sql." WHERE invoice_id='".$in['invoice_id']."'");
    msg::success(('Invoice updated'),'success');
    insert_message_log('purchase_invoice','{l}Purchase Invoice has been successfully approved by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);

    $yuki_active=$this->dbu->field("SELECT active FROM apps WHERE name='Yuki' AND main_app_id='0' ");
    $clearfacts_active=$this->dbu->field("SELECT active FROM apps WHERE name='Clearfacts' AND main_app_id='0' "); 
    $btb_active=$this->dbu->field("SELECT active FROM apps WHERE name='WinBooks' AND main_app_id='0' "); 

    $accountant_settings=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
    if($accountant_settings=='1'){
       $acc_app_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_APP_ID' ");
       $acc_export_id=$this->dbu->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID_PI' ");
      if($acc_export_id=='2' && $acc_app_id){
        if($acc_app_id=='1'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type=1");
          if(!$is_exported && $yuki_active=='1'){
            include(__DIR__.'/yuki.php');
            $yuki_obj=new yuki($in);
            $in['from_acc']=1;
            $yuki_obj->exportIncYuki($in);
          }
        }else if($acc_app_id=='3'){
          $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type=1");
          if(!$is_exported && $clearfacts_active=='1'){
            include(__DIR__.'/clearfacts.php');
            $clearfacts_obj=new clearfacts($in);
            $in['from_acc']=1;
            $clearfacts_obj->exportIncFacts($in);
          }
        }
      }else if($acc_export_id=='2' && !$acc_app_id){
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type=1");
        if(!$is_exported && $yuki_active=='1'){
          include(__DIR__.'/yuki.php');
          $yuki_obj=new yuki($in);
          $in['from_acc']=1;
          $yuki_obj->exportIncYuki($in);
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type=1 ");
        if(!$is_exported && $clearfacts_active=='1'){
          include(__DIR__.'/clearfacts.php');
          $clearfacts_obj=new clearfacts($in);
          $in['from_acc']=1;
          $clearfacts_obj->exportIncFacts($in);
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type=1");
        if(!$is_exported && $btb_active=='1'){
          include(__DIR__.'/btb.php');
          $btb_obj=new btb($in);
          $in['from_acc']=1;
          $btb_obj->exportIncBtb($in);
        }               
      }else if(!$acc_export_id){
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type=1");
        if(!$is_exported && $yuki_active=='1' && defined('EXPORT_AUTO_PINVOICE_YUKI') && EXPORT_AUTO_PINVOICE_YUKI=='2'){
          include(__DIR__.'/yuki.php');
          $yuki_obj=new yuki($in);
          $in['from_acc']=1;
          $yuki_obj->exportIncYuki($in);
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type=1");
        if(!$is_exported && $clearfacts_active=='1' && defined('EXPORT_AUTO_PINVOICE_CLEARFACTS') && EXPORT_AUTO_PINVOICE_CLEARFACTS=='2'){
          include(__DIR__.'/clearfacts.php');
          $clearfacts_obj=new clearfacts($in);
          $in['from_acc']=1;
          $clearfacts_obj->exportIncFacts($in);
        }
        $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type=1");
        if(!$is_exported && $btb_active=='1' && defined('EXPORT_AUTO_PINVOICE_EFFF') && EXPORT_AUTO_PINVOICE_EFFF=='2'){
          include(__DIR__.'/btb.php');
          $btb_obj=new btb($in);
          $in['from_acc']=1;
          $btb_obj->exportIncBtb($in);
        }
      }
    }else{
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND yuki='1' AND type=1");
      if(!$is_exported && $yuki_active=='1' && defined('EXPORT_AUTO_PINVOICE_YUKI') && EXPORT_AUTO_PINVOICE_YUKI=='2'){
        include(__DIR__.'/yuki.php');
        $yuki_obj=new yuki($in);
        $in['from_acc']=1;
        $yuki_obj->exportIncYuki($in);
      }
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND clearfacts='1' AND type=1");
      if(!$is_exported && $clearfacts_active=='1' && defined('EXPORT_AUTO_PINVOICE_CLEARFACTS') && EXPORT_AUTO_PINVOICE_CLEARFACTS=='2'){
        include(__DIR__.'/clearfacts.php');
        $clearfacts_obj=new clearfacts($in);
        $in['from_acc']=1;
        $clearfacts_obj->exportIncFacts($in);
      }
      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND winbooks='1' AND type=1");
      if(!$is_exported && $btb_active=='1' && defined('EXPORT_AUTO_PINVOICE_EFFF') && EXPORT_AUTO_PINVOICE_EFFF=='2'){
        include(__DIR__.'/btb.php');
        $btb_obj=new btb($in);
        $in['from_acc']=1;
        $btb_obj->exportIncBtb($in);
      }
    }     
    return true;
  }
  /****************************************************************
* function setDraft(&$in)                                          *
****************************************************************/
function setDraft(&$in)
 {
    $paid_sql="";
    $total_value = $this->dbu->field("SELECT COALESCE(SUM(total),0) FROM tblinvoice_incomming_line WHERE invoice_id='".$in['invoice_id']."' ");
    if($total_value == 0){
      $paid_sql=", paid='0' ";
    }
    $this->dbu->query("UPDATE tblinvoice_incomming set status=0,approved_by='' ".$paid_sql." WHERE invoice_id='".$in['invoice_id']."'");
    /*$this->dbu->query("DELETE FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' ");*/
    /*if(!$this->dbu->move_next()){
      msg::$error = gm('Invalid ID').'.';
      return false;
    }*/
    /*msg::success(('Invoice updated'),'success');*/
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log('purchase_invoice','{l}Purchase Invoice has been returned to draft{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);
     return true;

  }

  function setApprove(&$in)
 {

    $this->dbu->query("UPDATE tblinvoice_incomming set status='1' WHERE invoice_id='".$in['invoice_id']."'");
    //$this->dbu->query("DELETE FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' ");
    /*if(!$this->dbu->move_next()){
      msg::$error = gm('Invalid ID').'.';
      return false;
    }*/
    /*msg::success(('Invoice updated'),'success');*/
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log('purchase_invoice','{l}Purchase Invoice has been returned to approved{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);

     return true;

  }

  function delete_purchase_invoice_payments($in){
    if(!$this->delete_inc_validate($in)){
      return false;
    }
    $this->dbu->query("UPDATE tblinvoice_incomming set status='0',paid='0' WHERE invoice_id='".$in['invoice_id']."'");
    $this->dbu->query("DELETE FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' ");
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log('purchase_invoice','{l}All payments had been removed by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['invoice_id']);
    json_out($in);
  }

  function archive_purchase_invoice(&$in)
  {
    if(!$this->delete_inc_validate($in)){
      return false;
    }
    $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice_incomming WHERE invoice_id='".$in['id']."' ");
    $this->dbu->query("UPDATE tblinvoice_incomming SET f_archived='1', booking_number='' WHERE invoice_id = '".$in['id']."' ");
    if($tracking_trace){
        $this->dbu->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
    }
    msg::success(gm("Invoice Archived"),'success');
    insert_message_log('purchase_invoice','{l}Purchase Invoice has been archived by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['id']);
      return true;
  }

  function activate_purchase_invoice(&$in)
  {
    if(!$this->delete_inc_validate($in)){
      return false;
    }
    $type=$this->dbu->field("SELECT `type` FROM tblinvoice_incomming WHERE invoice_id='".$in['id']."' ");
    $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice_incomming WHERE invoice_id='".$in['id']."' ");
    $this->dbu->query("UPDATE tblinvoice_incomming SET f_archived='0', booking_number='".($type==2 ? generate_binvoice_credit_number() : generate_binvoice_number())."' WHERE invoice_id = '".$in['id']."' ");
    if($tracking_trace){
        $this->dbu->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
    }
    msg::success(gm("Invoice Activated"),'success');
    insert_message_log('purchase_invoice','{l}Purchase Invoice has been activated by{endl} '.get_user_name($_SESSION['u_id']),'purchase_invoice_id',$in['id']);
      return true;
  }

function updateCustomerDataInc(&$in)
  {

    if($in['field'] == 'contact_id'){
       $in['buyer_id'] ='';
    }


    $sql = "UPDATE tblinvoice_incomming SET ";
    if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, customers.bank_iban, customers.bank_bic_code, customers.bank_name,
                  customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id,
                  customer_addresses.address_id,customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
      $buyer_info->next();

     /*if($in['item_id'] == 'tmp'){
        $in['delivery_address_id'] = $buyer_info->f('address_id');
      }*/
      $in['buyer_iban']  = $buyer_info->f('bank_iban');
      $in['buyer_bic']  = $buyer_info->f('bank_bic_code');
      $in['buyer_bank']  = $buyer_info->f('bank_name');

      $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
      $in['currency_type']  =  $in['currency_id'];
      $in['contact_name'] ='';

      $sql .= " supplier_id = '".$in['buyer_id']."', ";

      $sql .= " buyer_country_id = '".$buyer_info->f('country_id')."', ";
      $sql .= " buyer_city = '".addslashes($buyer_info->f('city'))."', ";
      $sql .= " buyer_zip = '".$buyer_info->f('zip')."', ";

      $sql .= " buyer_address = '".addslashes($buyer_info->f('address'))."', ";


      if($in['contact_id']){
        $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
        // $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
        $sql .= " contact_id = '".$in['contact_id']."', ";
        $in['contact_name'] = addslashes($contact_info->f('firstname').' '.$contact_info->f('lastname'))."\n";
      }else{
        // $sql .= " contact_name = '', ";
        $sql .= " contact_id = '', ";
      }
      $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
      /*if($buyer_info->f('address_id') != $in['main_address_id']){
        $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
      }*/
      if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " free_field = '".addslashes($new_address_txt)."' ";
      }

    }else{
      if(!$in['contact_id']){
        msg::error ( gm('Please select a company or a contact'),'error');
        return false;
      }
      $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
      $sql .= " supplier_id = '', ";

      // $sql .= " contact_name = '', ";
      $sql .= " contact_id = '".$in['contact_id']."', ";

      $sql .= " buyer_country_id = '".$contact_address->f('country_id')."', ";
      $sql .= " buyer_city = '".addslashes($contact_address->f('city'))."', ";
      $sql .= " buyer_zip = '".$contact_address->f('zip')."', ";

      $sql .= " buyer_address = '".addslashes($contact_address->f('address'))."', ";

      $in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
      if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " free_field = '".addslashes($new_address_txt)."' ";
      }
    }
    $sql .=" WHERE invoice_id ='".$in['item_id']."' ";
    if(!$in['isAdd']){
      $this->dbu->query($sql);
      if($in['item_id'] && is_numeric($in['item_id'])){
        $trace_id=$this->db->field("SELECT trace_id FROM tblinvoice_incomming WHERE invoice_id='".$in['item_id']."' ");
        if($trace_id && $in['buyer_id']){
          $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
        }
      }
    }
    $in['incomming_invoice_id'] = $in['item_id'];

    msg::success(gm('Sync successfull.'),'success');
    return true;
  }

  function link(&$in){
    if(!$in['file_selected']){
      msg::error(gm('Please select a file'),'error');
      json_out($in);
      return false;
    }
    $fileTypes = array('jpg','jpeg','png','pdf','xml');
    $dot_pos=strrpos(urldecode($in['file_selected']),'.');
    if(!in_array(substr(urldecode($in['file_selected']),$dot_pos+1), $fileTypes)){
      msg::error(gm('Please select correct file'),'error');
      json_out($in);
      return false;
    }
    if($in['incomming_invoice_id']=='tmp'){
      if(substr(urldecode($in['file_selected']),$dot_pos+1)=='xml'){
        $in['drop_xml']=1;
      }
      $in['linkpath']=urlencode(urldecode($in['file_selected']));
      $in['file_path']='1';
    }else{
        $invoice=$this->dbu->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['incomming_invoice_id']."'");
        $d = new drop('invoices',$invoice->f('supplier_id'),null,true,DATABASE_NAME,null,null,$invoice->f('contact_id'));
        $target_path=INSTALLPATH.'upload/'. str_replace('/','_',urldecode($in['file_selected']));
        $d->getFile(urldecode($in['file_selected']),$target_path);
        $fileParts = pathinfo($target_path);
        if(strtolower($fileParts['extension']) == 'xml'){
            $file_content=file_get_contents($target_path);
            unlink($target_path);
            $targetPath = INSTALLPATH.'upload';
            $file_content=str_replace(array("cac:","cbc:"),"",$file_content);
            $xml_str=simplexml_load_string(stripslashes($file_content));
            $xml_str=json_decode(json_encode($xml_str), true);
            $pdf_file=$xml_str['AdditionalDocumentReference'][0]['Attachment']['EmbeddedDocumentBinaryObject'];
            if(!$pdf_file){
              $pdf_file=$xml_str['AdditionalDocumentReference']['Attachment']['EmbeddedDocumentBinaryObject'];
            }
            if($pdf_file){
                $pdf_file=base64_decode(str_replace("\r\n","",$pdf_file));
                $targetFile = rtrim($targetPath,'/') . '/tmp_file_'.time().'.pdf';
                mkdir(str_replace('//','/',$targetPath), 0775, true);
                file_put_contents($targetFile,$pdf_file);
                if($xml_str['InvoiceLine'] && !empty($xml_str['InvoiceLine'])){
                    $in['currency_type']=$this->db->field("SELECT id FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");  
                    if($xml_str['InvoiceLine'] && array_keys($xml_str['InvoiceLine']) !== range(0, count($xml_str['InvoiceLine']) - 1)){
                        $in['invoice_row']=array();
                        $in['invoice_row'][0]=array(
                                'article_code'        => $xml_str['InvoiceLine']['Item']['SellersItemIdentification'] ? $xml_str['InvoiceLine']['Item']['SellersItemIdentification']['ID'] : '',
                                'row_description'     => $xml_str['InvoiceLine']['Item']['Description'],
                                'row_quantity'        => display_number($xml_str['InvoiceLine']['InvoicedQuantity']),
                                'row_unit_price'      => display_number($xml_str['InvoiceLine']['LineExtensionAmount']),
                                'vat'                 => display_number($xml_str['InvoiceLine']['Item']['ClassifiedTaxCategory']['Percent']),
                                'row_amount'          => display_number($xml_str['InvoiceLine']['Price']['PriceAmount']),
                              );
                    }else{
                        $in['invoice_row']=array();
                        foreach($xml_str['InvoiceLine'] as $key=>$value){
                                $item=array(
                                  'article_code'        => $value['Item']['SellersItemIdentification'] ? $value['Item']['SellersItemIdentification']['ID'] : '',
                                  'row_description'     => $value['Item']['Description'],
                                  'row_quantity'        => display_number($value['InvoicedQuantity']),
                                  'row_unit_price'      => display_number($value['LineExtensionAmount']),
                                  'vat'                 => display_number($value['Item']['ClassifiedTaxCategory']['Percent']),
                                  'row_amount'          => display_number($value['Price']['PriceAmount']),
                                );
                                array_push($in['invoice_row'],$item);
                        }
                    } 
                }else if($xml_str['CreditNoteLine'] && !empty($xml_str['CreditNoteLine'])){
                    $in['currency_type']=$this->db->field("SELECT id FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");              
                    if($xml_str['CreditNoteLine'] && array_keys($xml_str['CreditNoteLine']) !== range(0, count($xml_str['CreditNoteLine']) - 1)){
                        $in['invoice_row']=array();
                        $in['invoice_row'][0]=array(
                                'article_code'        => $xml_str['CreditNoteLine']['Item']['SellersItemIdentification'] ? $xml_str['CreditNoteLine']['Item']['SellersItemIdentification']['ID'] : '',
                                'row_description'     => $xml_str['CreditNoteLine']['Item']['Description'],
                                'row_quantity'        => display_number($xml_str['CreditNoteLine']['InvoicedQuantity']),
                                'row_unit_price'      => display_number($xml_str['CreditNoteLine']['LineExtensionAmount']),
                                'vat'                 => display_number($xml_str['CreditNoteLine']['Item']['ClassifiedTaxCategory']['Percent']),
                                'row_amount'          => display_number($xml_str['CreditNoteLine']['Price']['PriceAmount']),
                              );
                    }else{
                        $in['invoice_row']=array();
                        foreach($xml_str['CreditNoteLine'] as $key=>$value){
                                $item=array(
                                  'article_code'        => $value['Item']['SellersItemIdentification'] ? $value['Item']['SellersItemIdentification']['ID'] : '',
                                  'row_description'     => $value['Item']['Description'],
                                  'row_quantity'        => display_number($value['CreditedQuantity']),
                                  'row_unit_price'      => display_number($value['LineExtensionAmount']),
                                  'vat'                 => display_number($value['Item']['ClassifiedTaxCategory']['Percent']),
                                  'row_amount'          => display_number($value['Price']['PriceAmount']),
                                );
                                array_push($in['invoice_row'],$item);
                        }
                    }
                }
                $in['link_url'] = $targetFile;
            }else{
                msg::error(gm('Invalid file type.'),'error');
                json_out($in);
            }
        }else{
            $in['link_url']=$target_path;      
        }
        $response['name'] =  $this->saveTmpFileToAmazon($in);
        $e=$d->moveFromTemp(urldecode($in['file_selected']),substr(urldecode($in['file_selected']),strrpos(urldecode($in['file_selected']),'/')+1));
        $this->dbu->query("UPDATE tblinvoice_incomming SET file_path='".urlencode($e->path_display)."' WHERE invoice_id='".$in['incomming_invoice_id']."'");
        if($in['invoice_row'] && !empty($in['invoice_row'])){
            $this->dbu->query("DELETE FROM tblinvoice_incomming_pdf_line WHERE invoice_id='".$in['incomming_invoice_id']."' ");
            $nr=0;
            foreach($in['invoice_row'] as $key=>$value){
                $this->dbu->query("INSERT INTO tblinvoice_incomming_pdf_line  SET 
                                          name                =   '".htmlspecialchars($value['row_description'],ENT_QUOTES,'UTF-8')."',
                                          invoice_id          =   '".$in['incomming_invoice_id']."',
                                          quantity            =   '".return_value($value['row_quantity'])."',
                                          price               =   '".return_value($value['row_unit_price'])."',
                                          amount              =   '".return_value($value['row_amount'])."',
                                          f_archived          =   '0',
                                          created             =   '".time()."',
                                          created_by          =   '".$_SESSION['u_id']."',
                                          last_upd            =   '".time()."',
                                          last_upd_by         =   '".$_SESSION['u_id']."',
                                          vat                 =   '".return_value($value['vat'])."',
                                          item_code           = '".addslashes($value['article_code'])."',
                                          sort_order          =   '".$nr."'  ");
                $nr++;
            }
        }
        if($in['currency_type']){
            $this->dbu->query("UPDATE tblinvoice_incomming SET currency_type='".$in['currency_type']."' WHERE invoice_id='".$in['incomming_invoice_id']."' ");
        }
    }
    return true;
  }



  function uploadInc($in){
    $response=array();
    if (!empty($_FILES)) {
      $tempFile = $_FILES['Filedata']['tmp_name'];
      $ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);
      $in['name'] = 'tmp_file_'.time().'.'.$ext;
      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
      $targetPath = INSTALLPATH.'upload';

      // $in['name'] = 'logo_img_'.$in['folder'].'_'.time().'.jpg';
      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
      // Validate the file type
      $fileTypes = array('jpg','jpeg','png','pdf','xml'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
        if(strtolower($fileParts['extension']) == 'xml'){
          $file_content=file_get_contents($_FILES['Filedata']['tmp_name']);
          $file_content=str_replace(array("cac:","cbc:"),"",$file_content);
          $xml_str=simplexml_load_string(stripslashes($file_content));
          $xml_str=json_decode(json_encode($xml_str), true);
          //print_r($xml_str);exit();
          $pdf_file=$xml_str['AdditionalDocumentReference'][0]['Attachment']['EmbeddedDocumentBinaryObject'];
          if(!$pdf_file){
            $pdf_file=$xml_str['AdditionalDocumentReference']['Attachment']['EmbeddedDocumentBinaryObject'];
          }
          if($pdf_file){
            $response['xml_data']=array();
            $pdf_file=base64_decode(str_replace("\r\n","",$pdf_file));
            $in['name'] = 'tmp_file_'.time().'.pdf';
            $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
            mkdir(str_replace('//','/',$targetPath), 0775, true);
            file_put_contents($targetFile,$pdf_file);
            if($xml_str['InvoiceLine'] && !empty($xml_str['InvoiceLine'])){
              $response['xml_data']['type']='0';
              $currency=$this->dbu->field("SELECT value FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
              if(!$currency){
                $currency='&euro;';
              }
              $response['xml_data']['invoice_number']=$xml_str['ID'];
              $response['xml_data']['invoice_currency']=$currency;
              $response['xml_data']['currency_type']=$this->dbu->field("SELECT id FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
              $response['xml_data']['invoice_total_wo_vat']=display_number($xml_str['LegalMonetaryTotal']['TaxExclusiveAmount']);
              $response['xml_data']['total_amount_due']=display_number($xml_str['LegalMonetaryTotal']['TaxInclusiveAmount']); 
              $response['xml_data']['total']=$response['xml_data']['total_amount_due'];          
              if($xml_str['InvoiceLine'] && array_keys($xml_str['InvoiceLine']) !== range(0, count($xml_str['InvoiceLine']) - 1)){
                $response['xml_data']['invoice_row']=array();
                $response['xml_data']['invoice_row'][0]=array(
                    'article_code'        => $xml_str['InvoiceLine']['Item']['SellersItemIdentification'] ? $xml_str['InvoiceLine']['Item']['SellersItemIdentification']['ID'] : '',
                    'row_description'     => $xml_str['InvoiceLine']['Item']['Description'],
                    'row_quantity'        => display_number($xml_str['InvoiceLine']['InvoicedQuantity']),
                    'row_unit_price'      => display_number($xml_str['InvoiceLine']['LineExtensionAmount']),
                    'vat'                 => display_number($xml_str['InvoiceLine']['Item']['ClassifiedTaxCategory']['Percent']),
                    'row_amount'          => display_number($xml_str['InvoiceLine']['Price']['PriceAmount']),
                  );
              }else{
                $response['xml_data']['invoice_row']=array();
                foreach($xml_str['InvoiceLine'] as $key=>$value){
                    $item=array(
                      'article_code'        => $value['Item']['SellersItemIdentification'] ? $value['Item']['SellersItemIdentification']['ID'] : '',
                      'row_description'     => $value['Item']['Description'],
                      'row_quantity'        => display_number($value['InvoicedQuantity']),
                      'row_unit_price'      => display_number($value['LineExtensionAmount']),
                      'vat'                 => display_number($value['Item']['ClassifiedTaxCategory']['Percent']),
                      'row_amount'          => display_number($value['Price']['PriceAmount']),
                    );
                    array_push($response['xml_data']['invoice_row'],$item);
                }
              }
              $response['xml_data']['invoice_vat_line']=array();
              if(array_keys($xml_str['TaxTotal']['TaxSubtotal']) !== range(0, count($xml_str['TaxTotal']['TaxSubtotal']) - 1)){
                $response['xml_data']['invoice_vat_line'][0]=array(
                    'vat_line'        => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxCategory']['Percent']),
                    'total_no_vat'    => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']),
                    'total'           =>  display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']+$xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
                    'vat_value'       => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
                  );
              }else{
                foreach($xml_str['TaxTotal']['TaxSubtotal'] as $key=>$value){
                  $line_vat=array(
                      'vat_line'      => display_number($value['TaxCategory']['Percent']),
                      'total_no_vat'   => display_number($value['TaxableAmount']),
                      'total'           =>  display_number($value['TaxableAmount']+$value['TaxAmount']),
                      'vat_value'       => display_number($value['TaxAmount']),
                    );
                  array_push($response['xml_data']['invoice_vat_line'],$line_vat);
                }
              }
              $response['xml_data']['invoice_line']=$response['xml_data']['invoice_vat_line'];
              if($xml_str['PaymentMeans']['PaymentID'] && !empty($xml_str['PaymentMeans']['PaymentID'])){
                  $ogm_char=str_split($xml_str['PaymentMeans']['PaymentID']);
                  $new_ogm="";
                  for($o=0;$o<count($ogm_char);$o++){
                    $new_ogm.=($o==3 || $o==7 ? '/'.$ogm_char[$o] : $ogm_char[$o]);
                  }
                  $response['xml_data']['ogm']=$new_ogm;
              }           
            }else if($xml_str['CreditNoteLine'] && !empty($xml_str['CreditNoteLine'])){
              $response['xml_data']['type']='2';
              $currency=$this->dbu->field("SELECT value FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
              if(!$currency){
                $currency='&euro;';
              }
              $response['xml_data']['invoice_currency']=$currency;
              $response['xml_data']['currency_type']=$this->dbu->field("SELECT id FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
              $response['xml_data']['invoice_total_wo_vat']=display_number($xml_str['LegalMonetaryTotal']['TaxExclusiveAmount']);
              $response['xml_data']['total_amount_due']=display_number($xml_str['LegalMonetaryTotal']['TaxInclusiveAmount']);
              $response['xml_data']['total']=$response['xml_data']['total_amount_due'];    
              if($xml_str['CreditNoteLine'] && array_keys($xml_str['CreditNoteLine']) !== range(0, count($xml_str['CreditNoteLine']) - 1)){
                $response['xml_data']['invoice_row']=array();
                $response['xml_data']['invoice_row'][0]=array(
                    'article_code'        => $xml_str['CreditNoteLine']['Item']['SellersItemIdentification'] ? $xml_str['CreditNoteLine']['Item']['SellersItemIdentification']['ID'] : '',
                    'row_description'     => $xml_str['CreditNoteLine']['Item']['Description'],
                    'row_quantity'        => display_number($xml_str['CreditNoteLine']['InvoicedQuantity']),
                    'row_unit_price'      => display_number($xml_str['CreditNoteLine']['LineExtensionAmount']),
                    'vat'                 => display_number($xml_str['CreditNoteLine']['Item']['ClassifiedTaxCategory']['Percent']),
                    'row_amount'          => display_number($xml_str['CreditNoteLine']['Price']['PriceAmount']),
                  );
              }else{
                $response['xml_data']['invoice_row']=array();
                foreach($xml_str['CreditNoteLine'] as $key=>$value){
                    $item=array(
                      'article_code'        => $value['Item']['SellersItemIdentification'] ? $value['Item']['SellersItemIdentification']['ID'] : '',
                      'row_description'     => $value['Item']['Description'],
                      'row_quantity'        => display_number($value['CreditedQuantity']),
                      'row_unit_price'      => display_number($value['LineExtensionAmount']),
                      'vat'                 => display_number($value['Item']['ClassifiedTaxCategory']['Percent']),
                      'row_amount'          => display_number($value['Price']['PriceAmount']),
                    );
                    array_push($response['xml_data']['invoice_row'],$item);
                }
              }
              $response['xml_data']['invoice_vat_line']=array();
              if(array_keys($xml_str['TaxTotal']['TaxSubtotal']) !== range(0, count($xml_str['TaxTotal']['TaxSubtotal']) - 1)){
                $response['xml_data']['invoice_vat_line'][0]=array(
                    'vat_line'      => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxCategory']['Percent']),
                    'total_no_vat'        => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']),
                    'total'           =>  display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']+$xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
                    'vat_value'       => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
                  );
              }else{
                foreach($xml_str['TaxTotal']['TaxSubtotal'] as $key=>$value){
                  $line_vat=array(
                      'vat_line'      => display_number($value['TaxCategory']['Percent']),
                      'total_no_vat'        => display_number($value['TaxableAmount']),
                      'total'           =>  display_number($value['TaxableAmount']+$value['TaxAmount']),
                      'vat_value'       => display_number($value['TaxAmount']),
                    );
                  array_push($response['xml_data']['invoice_vat_line'],$line_vat);
                }
              }
              $response['xml_data']['invoice_line']=$response['xml_data']['invoice_vat_line'];
            }
            if($xml_str['AccountingSupplierParty']['Party']['EndpointID'] && !empty($xml_str['AccountingSupplierParty']['Party']['EndpointID'])){
              $buyer_info=$this->dbu->query("SELECT * FROM customers WHERE REPLACE( REPLACE( REPLACE( REPLACE( btw_nr, '.', '' ) , ' ', '' ) , '-', '' ) , '_', '' )='".$xml_str['AccountingSupplierParty']['Party']['EndpointID']."' LIMIT 1");
              if($buyer_info->next()){
                $response['xml_data']['buyer_id']=$buyer_info->f('customer_id');
                $this->dbu->query("UPDATE customers SET is_supplier='1' WHERE customer_id='".$buyer_info->f('customer_id')."' ");
              }else{
                $response['xml_data']['buyer_data']=array(
                    'name'        => $xml_str['AccountingSupplierParty']['Party']['PartyName']['Name'],
                    'btw_nr'      => $xml_str['AccountingSupplierParty']['Party']['EndpointID'],
                    'address'     => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['StreetName'], 
                    'city'        => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['CityName'], 
                    'zip'         => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['PostalZone'], 
                    'country_id'  => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['Country']['IdentificationCode'] ? $this->dbu->field("SELECT country_id FROM country WHERE `code`='".$xml_str['AccountingSupplierParty']['Party']['PostalAddress']['Country']['IdentificationCode']."' ") : ACCOUNT_DELIVERY_COUNTRY_ID,
                    'btw_nr'      => $xml_str['AccountingSupplierParty']['Party']['EndpointID'],
                  );
              }
            }
            if($xml_str['IssueDate']){
              $response['xml_data']['invoice_date']=(strtotime($xml_str['IssueDate'])+10800)*1000;
            }
            if($xml_str['PaymentMeans']['PaymentDueDate']){
              $response['xml_data']['due_date']=(strtotime($xml_str['PaymentMeans']['PaymentDueDate'])+10800)*1000;
            }
          }else{           
            $response['error'] = gm('Invalid file type.');
            echo json_encode($response);
          }      
        }else{
          move_uploaded_file($tempFile,$targetFile);
        }
        $in['tmp_file_name'] = $in['name'];
        if($in['incomming_invoice_id']!='tmp'){
            $response['name'] =  $this->saveTmpFileToAmazon($in);
        }else{
            $response['name'] =$targetFile;
        }
        $response['success'] = 'success';
        echo json_encode($response);    
        // ob_clean();
      } else {
        $response['error'] = gm('Invalid file type.');
       echo json_encode($response);
      }
      exit();
    }
  }

 /**
   * Save a temp file on the server
   *
   * @return JSON
   * @author Marius
   **/
  function uploadToServer(&$in)
  {
    $response=array();
    global $config;
    if (!empty($_FILES)) {
      $tempFile = $_FILES['Filedata']['tmp_name'];
      $ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);
      $in['name'] = 'tmp_file_'.time().'.'.$ext;
      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
      $targetPath = INSTALLPATH.'upload';

      // $in['name'] = 'logo_img_'.$in['folder'].'_'.time().'.jpg';
      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
      // Validate the file type
      $fileTypes = array('jpg','jpeg','png','pdf'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
        move_uploaded_file($tempFile,$targetFile);
        $response['success'] = 'success';
        $response['name'] =  $this->saveTmpFileToAmazon($in);
        //$response['name'] = $config['install_path'].'upload/'.$in['name'];
        echo json_encode($response);
        // ob_clean();
      } else {
        $response['error'] = gm('Invalid file type.');
        echo json_encode($response);
      }
       exit();
    }
  }

    /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function saveTmpFileToAmazon(&$in)
  {
    if($in['link_url']){
       $in['tmp_file_name']=$in['link_url'];
    }
    if($in['invoice_id']){
       $in['incomming_invoice_id']=$in['invoice_id'];
    }

    if($in['tmp_file_name']){
      global $config;
      ark::loadCronLibraries(array('aws'));
      if($in['db_name']){
        $a = new awsWrap($in['db_name']);
      }else{
        $a = new awsWrap(DATABASE_NAME);
      }
      
     if(!$in['link_url']){
      $pdfPath = INSTALLPATH.'upload/'.$in['tmp_file_name'];
     }else{
      //$pdfPath = str_replace($config['install_path'],'',$in['tmp_file_name'])  ;
        $pdfPath = $in['tmp_file_name'];
     }

      $ext = pathinfo($pdfPath,PATHINFO_EXTENSION);
      $pdfFile = 'invoice/purchase/invoice_'.$in['incomming_invoice_id'].".".$ext;

      $a->uploadFile($pdfPath,$pdfFile);
      unlink($pdfPath);
      $this->dbu->query("UPDATE tblinvoice_incomming SET file_name='".$pdfFile."', amazon=1 WHERE invoice_id='".$in['incomming_invoice_id']."' ");
      if($in['db_name']){
        $link = $a->getLink($config['awsBucket'].$in['db_name'].'/'.$pdfFile);
      }else{
        $link = $a->getLink($config['awsBucket'].DATABASE_NAME.'/'.$pdfFile);
      }   
      $in['link_url']=$link;
      return $link;
   }
 }
   function remove_image(&$in)
  {
    global $config;
    if($in['incomming_invoice_id']=='tmp'){
        $link=strpos($in['link'],'upload');
        unlink(substr($in['link'],$link));
        if($in['type_r']=='1'){
            $in['link_url']='';
        }else{
            $in['file_path']='';
            $in['linkpath']='';
            $in['drop_file_path']='';
        }
    }else{
      $this->db->query("DELETE FROM tblinvoice_incomming_pdf_line WHERE invoice_id='".$in['incomming_invoice_id']."'");
      $info = $this->dbu->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['incomming_invoice_id']."' ");
      if($info->f('amazon') == 1){
        ark::loadLibraries(array('aws'));
        $aws = new awsWrap(DATABASE_NAME);
        $link =  $aws->doesObjectExist($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
        if($link){
          $aws->deleteItem($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
        }
        if($info->f('file_path')){
          $d = new drop('invoices');
          $resp=$d->deleteF(urldecode($info->f('file_path')));
        }
        $this->dbu->query("UPDATE tblinvoice_incomming SET file_path='',
                                    pdf_path='',
                                    file_name='',
                                    amazon='1'
                                     WHERE invoice_id='".$in['incomming_invoice_id']."' ");
        msg::success(('File deleted'),'success');
       }else{
        $d = new drop('invoices');
        $e = $d->deleteF(urldecode($info->f('file_path')));
        if(is_array($e)){
          $this->dbu->query("UPDATE tblinvoice_incomming SET file_path='',
                                    pdf_path='',
                                    file_name='',
                                    amazon='1'
                                     WHERE invoice_id='".$in['incomming_invoice_id']."' ");
           msg::success(('File deleted'),'success');
        }else{
          msg::error($e,"error");
        }
      }
    }
    $in['invoice_id']=$in['incomming_invoice_id'];
    return true;
  }

  function csvRegimes(&$in){
    $vats=$this->dbu->query("SELECT * FROM invoice_export_settings WHERE field_name='vat_regime_id' AND field_id='".$in['vat_id']."' ");
    if($in['vat_id']<10000){
      if($vats->next()){
        $this->dbu->query("UPDATE invoice_export_settings SET field_value='".$in['val']."' WHERE field_name='vat_regime_id' AND field_id='".$in['vat_id']."' ");
      }else{
        $this->dbu->query("INSERT INTO invoice_export_settings SET field_name='vat_regime_id',field_id='".$in['vat_id']."',field_value='".$in['val']."' ");
      }
    }else{   
      if($vats->next()){
        $this->dbu->query("UPDATE invoice_export_settings set field_value='".$in['val']."' WHERE field_name='vat_regime_id' AND field_id='".$in['vat_id']."' ");
      }else{
        $old_vats=$this->dbu->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_id']."' ");
        if($old_vats){
          $this->dbu->query("DELETE FROM invoice_export_settings WHERE field_id='".ltrim($old_vats,'r')."' AND field_name='vat_regime_id' ");
        }   
        $this->dbu->query("INSERT INTO invoice_export_settings SET field_name='vat_regime_id',field_id='".$in['vat_id']."',field_value='".$in['val']."' ");
      }
    }
    
    msg::success(gm('Settings updated').'.',"success");
    json_out($in);
  }

  function csvTypes(&$in){
    $this->dbu->query("UPDATE invoice_export_settings SET field_value = '".$in['val']."' WHERE field_name = 'type' AND field_id = '".$in['field_id']."' ");
    msg::success(gm('Settings updated').'.',"success");
    json_out($in);
  }

 function yukiVats(&$in){
    $vats=$this->dbu->query("SELECT * FROM yukivats WHERE vat_id='".$in['vat_id']."' ");
    if($in['vat_id']<10000){
      if($vats->next()){
        $this->dbu->query("UPDATE yukivats set value='".$in['val']."' WHERE vat_id='".$in['vat_id']."' ");
      }else{
        $this->dbu->query("INSERT INTO yukivats set value='".$in['val']."' , vat_id='".$in['vat_id']."' ");
      }
    }else{   
      if($vats->next()){
        $this->dbu->query("UPDATE yukivats set value='".$in['val']."' WHERE vat_id='".$in['vat_id']."' ");
      }else{
        $old_vats=$this->dbu->query("SELECT vat_regime_id,vat_id FROM vat_new WHERE id='".$in['vat_id']."' ");
        if($old_vats->f('vat_regime_id')){
            $this->dbu->query("DELETE FROM yukivats WHERE vat_id='".$old_vats->f('vat_regime_id')."' ");
        }else if($old_vats->f('vat_id')){
          $this->dbu->query("DELETE FROM yukivats WHERE vat_id='".$old_vats->f('vat_id')."' ");
        }
        $this->dbu->query("INSERT INTO yukivats set value='".$in['val']."' , vat_id='".$in['vat_id']."' ");
      }
    }
    
    msg::success(gm('Settings updated').'.',"success");
    json_out($in);
  }

  function codaVats(&$in){
   $vats=$this->dbu->query("SELECT * FROM codavats WHERE vat_id='".$in['vat_id']."' ");
    if($in['vat_id']<10000){
      if($vats->next()){
        $this->dbu->query("UPDATE codavats set value='".$in['val']."' WHERE vat_id='".$in['vat_id']."' ");
      }else{
        $this->dbu->query("INSERT INTO codavats set value='".$in['val']."' , vat_id='".$in['vat_id']."' ");
      }
    }else{
      if($vats->next()){
        $this->dbu->query("UPDATE codavats set value='".$in['val']."' WHERE vat_id='".$in['vat_id']."' ");
      }else{
        $old_vats=$this->dbu->query("SELECT vat_regime_id,vat_id FROM vat_new WHERE id='".$in['vat_id']."' ");
        if($old_vats->f('vat_regime_id')){
            $this->dbu->query("DELETE FROM codavats WHERE vat_id='".$old_vats->f('vat_regime_id')."' ");
        }else if($old_vats->f('vat_id')){
          $this->dbu->query("DELETE FROM codavats WHERE vat_id='".$old_vats->f('vat_id')."' ");
        }
        $this->dbu->query("INSERT INTO codavats set value='".$in['val']."' , vat_id='".$in['vat_id']."' ");
      }
    }   
    msg::success(gm('Settings updated').'.',"success");
    json_out($in);
  }

  function exactVats(&$in){
   $vats=$this->dbu->query("SELECT * FROM exactvats WHERE vat_id='".$in['vat_id']."' ");
    if($in['vat_id']<10000){
      if($vats->next()){
        $this->dbu->query("UPDATE exactvats set value='".$in['val']."' WHERE vat_id='".$in['vat_id']."' ");
      }else{
        $this->dbu->query("INSERT INTO exactvats set value='".$in['val']."' , vat_id='".$in['vat_id']."' ");
      }
    }else{
      if($vats->next()){
        $this->dbu->query("UPDATE exactvats set value='".$in['val']."' WHERE vat_id='".$in['vat_id']."' ");
      }else{
        $old_vats=$this->dbu->query("SELECT vat_regime_id,vat_id FROM vat_new WHERE id='".$in['vat_id']."' ");
        if($old_vats->f('vat_regime_id')){
            $this->dbu->query("DELETE FROM exactvats WHERE vat_id='".$old_vats->f('vat_regime_id')."' ");
        }else if($old_vats->f('vat_id')){
          $this->dbu->query("DELETE FROM exactvats WHERE vat_id='".$old_vats->f('vat_id')."' ");
        }
        $this->dbu->query("INSERT INTO exactvats set value='".$in['val']."' , vat_id='".$in['vat_id']."' ");
      }
    }   
    msg::success(gm('Settings updated').'.',"success");
    json_out($in);
  }

  function delete_import_invoice(&$in)
  {
    if(!$this->delete_import_invoice_validate($in)){
      return false;
    }

    $this->db->query("DELETE FROM tblinvoice_import_log WHERE invoice_import_log_id='".$in['invoice_import_log_id']."' ");

    msg::success ( gm('Import Log deleted').'.','success');

    return true;
  }
  /****************************************************************
  * function delete_import_validate(&$in)                         *
  ****************************************************************/
  function delete_import_invoice_validate(&$in)
  {
    $is_ok = true;
    if(!$in['invoice_import_log_id']){
      msg::error ( gm('Invalid ID').'.','error');
      return false;
    }
    else{
      $this->db->query("SELECT invoice_import_log_id FROM tblinvoice_import_log WHERE invoice_import_log_id='".$in['invoice_import_log_id']."'");
      if(!$this->db->move_next()){
        msg::error ( gm('Invalid ID').'.','error');
        return false;
      }
    }

    return $is_ok;
  }
  function backup_data(&$in)
  {
    set_time_limit(0);
      ini_set('memory_limit', '-1');
      $folder=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice';
      $folder_exist=file_get_contents($folder);

      if($folder_exist==false){
        @mkdir($folder,0755,true);
      }
      $array_exclude = array('tblinvoice_import');
    $file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup1.xml';
    $file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup2.xml';
    $file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup3.xml';
    $file_fill1 = file_get_contents($file1);
    $file_fill2 = file_get_contents($file2);
    $file_fill3 = file_get_contents($file3);
    if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
      if($in['file_id']!=0){
          ini_set('memory_limit', '-1');
          $this->db->query("show tables");
          $table_names=array();
          while ($this->db->move_next()){
            if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'tblinvoice')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'tblinvoice')!==FALSE)
            {
              if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
                $table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
              }
            }
          }
          $xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');

            for($i=0;$i<count($table_names);$i++){
              /*$cus = $xml->addChild('customers-data');*/
                     $tbl=$xml->addChild($table_names[$i],'');
                       //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
                      $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
                      $this->db->query("SELECT * FROM  ".$table_names[$i]."");
                      while($this->db->move_next()){
                           $chese=$tbl->addChild('line','');
                            foreach ($cols as $key => $value) {
                              $chese->addChild($value['Field'], htmlspecialchars($this->db->f($value['Field']))) ;
                            }
                            /*$this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."");
                        while ($this->db2->move_next()) {
                              $chese->addChild($this->db2->f('Field'), $this->db->f($this->db2->f('Field'))) ;
                            }*/
                      }
            }
            $time=time();
            $xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup'.$in['file_id'].'.xml');
            $this->db->query("UPDATE backup_data_invoice SET backup_time='".$time."' WHERE backup_id=".$in['file_id']."");
          msg::success ( gm('Data successfully saved'),'success');
          $in['step']=2;
          return true;
      }else{
          msg::error ( gm('Select a file'),'error');
          return false;
      }
    }else{
      if($in['file_id']!=0){
        ini_set('memory_limit', '-1');
        $this->db->query("show tables");
        $table_names=array();
        while ($this->db->move_next()){
          if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'tblinvoice')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'tblinvoice')!==FALSE)
          {
            if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
              $table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
            }
          }
        }

        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');
          for($i=0;$i<count($table_names);$i++){
            /*$cus = $xml->addChild('invoice-data');*/
                   $tbl=$xml->addChild($table_names[$i],'');
                     //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
                    $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
                    $this->db->query("SELECT * FROM  ".$table_names[$i]."");
                    while($this->db->move_next()){
                          $chese=$tbl->addChild('line','');
                          foreach ($cols as $key => $value) {
                            $chese->addChild($value['Field'], htmlspecialchars($this->db->f($value['Field'] ) ) ) ;
                          }
                    }
          }
          $time=time();
          if($file_fill1==false){
            $xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup1.xml');
            $this->db->query("UPDATE backup_data_invoice SET backup_time='".$time."' WHERE backup_id='1'");
          }elseif($file_fill1==true&&$file_fill2==false){
            $xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup2.xml');
            $this->db->query("UPDATE backup_data_invoice SET backup_time='".$time."' WHERE backup_id='2'");
          }elseif ($file_fill2==true&&$file_fill3==false) {
            $xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup3.xml');
            $this->db->query("UPDATE backup_data_invoice SET backup_time='".$time."' WHERE backup_id='3'");
          }
          msg::success ( gm('Data successfully saved'),'success');
          $in['step']=2;
          return true;
        }else{
          msg::error ( gm('Select a file'),'error');
          return false;
        }

    }

  }
  function upl(&$in)
  {
    $response=array();
    // Define a destination
    $targetFolder = 'upload/'.DATABASE_NAME.'/invoice/'; // Relative to the root

    if (!empty($_FILES)) {
      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetPath = $targetFolder;
      $targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

      // Validate the file type
      $fileTypes = array('xml','xls','xlsx'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      if (in_array($fileParts['extension'],$fileTypes)) {
        move_uploaded_file($tempFile,$targetFile);
        // $response['success'] = $_FILES['Filedata']['name'];
        msg::success($_FILES['Filedata']['name'],'success');
        $in['xls_name'] = $_FILES['Filedata']['name'];

        json_out($in);
        // $this->import_company($in);
        /*echo json_encode($response);*/
      } else {
        // $response['error'] = 'Invalid file type.';
        msg::error( 'Invalid file type.','error');
        // echo json_encode($response);
      }

    }
  }
  function uploadify_invoice(&$in)
  {
    $response=array();
    // Define a destination
    $targetFolder = 'upload/'.DATABASE_NAME.'/invoice/'; // Relative to the root
    @mkdir($targetFolder,0755,true);
    if (!empty($_FILES)) {
      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetPath = $targetFolder;
      $targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

      // Validate the file type
      $fileTypes = array('xml','xls','xlsx'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      if (in_array($fileParts['extension'],$fileTypes)) {
        move_uploaded_file($tempFile,$targetFile);
        msg::success($_FILES['Filedata']['name'],'success');
        $in['xls_name'] = $_FILES['Filedata']['name'];

        json_out($in);
      } else {
        msg::error( 'Invalid file type.','error');
      }

    }
  }
  function import_invoice(&$in){
    ini_set('memory_limit', '2048M');
    ini_set('max_execution_time', 0);
    $in['timestamp'] = time();
    // $in['debug'] = true;
    global $_FILES;
    global $database_config;
    $database_users = array(
      'hostname' => $database_config['mysql']['hostname'],
      'username' => $database_config['mysql']['username'],
      'password' => $database_config['mysql']['password'],
      'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($database_users);
    require_once 'libraries/PHPExcel.php';

    $inputFileName = 'upload/'.DATABASE_NAME."/invoice/".$in['xls_name'];

    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
    /*$objPHPExcel->setActiveSheetIndex(0);*/
    $objWorksheet = $objPHPExcel->getActiveSheet();
    $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
    $highestColumn = $objWorksheet->getHighestColumn();
    $i=0;
    $x=0;
    $z=1;
    $sdsd = 0;
    $nr_rows=0;
    $nr_cols=0;
    $blabla = 0;
    $headers = array();
    $values = array();
    $final_arr = array();
    // $arrayy = array();
    // print_r($objWorksheet->getRowIterator());
    foreach ($objWorksheet->getRowIterator() as $row) {
      if($i>$highestRow)
      {
        break;
      }
      $cellIterator = $row->getCellIterator();
      $cellIterator->setIterateOnlyExistingCells(false);
      /*if($i!=0)
      {
        $nr_rows++;
      }*/
      $k=0;
      $row_array = array();
      foreach ($cellIterator as $cell) {
        $val = $cell->getValue();
        array_push($row_array, $val);
        if($i==0)
        { if($val)
          {
            $val = str_replace("'", " ", $val);


            array_push($headers, $val);
            $nr_cols++;
          }else{
            break;
          }
        }else
        {
          if($k!=$nr_cols)
          {
            $values[$k] = $cell->getValue();
            //$god = PHPExcel_Shared_Date::isDateTime($cell);
            //if(PHPExcel_Shared_Date::isDateTime($cell)) {
            //     $values[$k] = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($values[$k]));
            //}

            $k++;
          }else
          {
            break;
          }
        }
      }
      if($i!=0)
      {
        if($i < 5){
          for ($j=0;$j<$k;$j++)
          {
            $final_arr[$headers[$j]][$x] = $values[$j];
          }
        }
        $x++;
        $t = 0;
        $nr_rows++;
      }else{
        $t = 1;
      }
      // array_push($arrayy, $row_array);
      $i++;
      // console::memory(addslashes(serialize($row_array)),'line');
      $this->db->query("INSERT INTO tblinvoice_import SET data='".addslashes(serialize($row_array))."', timestamp='".$in['timestamp']."', line='".$z."',type='".$t."' ");
      $j++;
      $z++;
    }


    $make_invoice=$final_arr;
    $company_out=array();
    $company_in=array();
    $invoice_paid=array();
    $invoices_paid=array(1 => 'Mark as sent', 2 => 'Mark as paid');
    $b=1;
    $t=1;
    $h=1;

    foreach ($invoices_paid as $invoices_head => $invoices_value) {

      array_push($invoice_paid,array(
        'id'   => $b,
        'name' => $invoices_value,
      ));
      $b++;
    }

    foreach ($final_arr as $company => $company_field) {
      array_push($company_out,array(
        'id'=>$h,
        'name'=>$company
        ));
      $h++;
    }

    foreach ($make_invoice as $companyes => $company_fields) {
      array_push($company_in,array(
        'id'=>$t,
        'name'=>$companyes
        ));
      $t++;
    }

    $in['customer_name']='0';
    $in['customer_ref']='0';
    $in['invoices_paid_id']='0';
    $in['company_names']=$company_in;
    $in['company_out']=$company_out;
    $in['invoices_paid']=$invoice_paid;

    $in['file_content']=$final_arr;

    $dark = str_replace(array("\r"), "", $in['file_content']);
    foreach ($dark as $header => $values) {
        $i=0;
        $in['match_field_id'] = '0';
        $header = trim($header);
        $header_select = str_replace(array("\r\n","\n","\r"," "), '_', $header);
        $in['match_field_invoice'] = build_match_field_dd_invoice($in[$header_select]);
        $in['names_id'][]=$header_select;
    }

    $final_arr = null;
    $in['total_rows']=$nr_rows;
    $in['step']=3;
    $in['class3']='text-primary';
    $in['mark_as_sent']=1;
    $in['mark_as_paid']=1;

    @unlink('upload/'.DATABASE_NAME."/invoice/".$in['xls_name']);
    //header("Location: index.php?do=company-save_imports");
    json_out($in);
    return true;
  }

  function save_imports(&$in)
  {


    $file_content = array();
    $tmp_header = array();
    $q = $this->db->query("SELECT * FROM tblinvoice_import WHERE `timestamp`='".$in['timestamp']."' ORDER BY line ASC ");
    while ($q->next()) {
      $line = unserialize($q->f('data'));
      foreach ($line as $key => $value) {
        if($q->f('type') == 1 ){
          $file_content[$value] = array();
          $tmp_header[$key] = $value;
        }else{
          array_push($file_content[$tmp_header[$key]],$value);
        }
      }
    }

    $insert_values = "";
    $values_info = array();

    $j=0;
    $nr_headers=0;
    $headers_ids= array();
    $insert_payment = '';
    $begin_payment_query = '';
    $inserted_inv = 0;
    $inserted_inv_arr = array();
    $update_inv = 0;
    $update_inv_arr = array();
    $inserted_line = 0;
    $inserted_line_arr = array();
    $inserted_price_arr = array();
    $inserted_price = 0;
    $update_line = 0;
    $update_line_arr = array();
    $filename = $in['xls_name'];

    for ($k=$in['k'];$k<$in['total_rows'];$k++)
    {

      $no_action_customer = true;
      $no_action_contact = true;
      $no_action_contact_addr = false;// set true to import the address only if there is a country id
      $no_action_cust_addr = false;
      $has_first_name = false;
      $has_last_name = false;
      $first_name="";
      $last_name="";
      $begin_query = "";
      $end_query = "";
      $cmon_id ="";
      $begin_contact_query = "INSERT INTO ";
      $insert_cont = true;
      unset($values_info);


      foreach ($file_content as $headers => $value) {
        $headers = trim($headers);
        $headers = str_replace(array("\r\n","\n","\r"," "), '_', $headers);
        /*$headers = explode(' ', $headers);
        $headers = implode('_', $headers);*/
        $headers = addslashes($headers);

        $value[$k] = addslashes($value[$k]);

        if($in[$headers])
        {

          if($k==0)
          {

            if(in_array($in[$headers], $headers_ids))
            {

              msg::error ( gm("You cannot select the same field twice"),'error');
              $in['error'] = 'error';
              json_out($in);
              //ark::$controller = 'save_imports';
              return false;
            }else
            {
              array_push($headers_ids, $in[$headers]);
            }
          }
          $nr_headers++;
          $labels_info = $this->db_import->query("SELECT field_name, tabel_name, is_custom_dd, type, custom_field FROM import_labels_invoice WHERE import_label_id='".$in[$headers]."'");

          $labels_info->next();
          if($labels_info->f('type')=='invoice')
          {

            $tabel_name = 'tblinvoice';

          }

          if($labels_info->f('custom_field')==0)
          {

            if(trim($value[$k]))
            {
              if($labels_info->f('field_name')=='serial_number'){

                $values_info[$j]['field_name'] = $labels_info->f('field_name');
                $values_info[$j]['tabel_name'] = $tabel_name;
                $values_info[$j]['value'] = $value[$k];

              }elseif($labels_info->f('field_name')=='invoice_date'){
                  //$invoice_date =  ($value[$k] - 25569) * 86400;  
                  //$invoice_date = strtotime($value[$k]);
                  $invoice_date = strtotime(str_replace("/", "-", $value[$k]))+7200;
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $invoice_date;

              }elseif($labels_info->f('field_name')=='due_date'){
                  //$due_date =  ($value[$k] - 25569) * 86400;
                  //$due_date = strtotime($value[$k]);
                  $due_date = strtotime(str_replace("/", "-", $value[$k]))+7200;
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $due_date;

              }elseif($labels_info->f('field_name')=='type'){
                    $values_info[$j]['field_name'] = $labels_info->f('field_name');
                    $values_info[$j]['tabel_name'] = $tabel_name;
                    $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='status'){
                    $values_info[$j]['field_name'] = $labels_info->f('field_name');
                    $values_info[$j]['tabel_name'] = $tabel_name;
                    $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='date')
              {
               /* date('m/d/Y', 1299446702);*/
                //$payment_date = date('Y/m/d',(($value[$k] - 25569) * 86400));
               $payment_date = strtotime(str_replace("/", "-", $value[$k]))+7200;
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                  $values_info[$j]['value'] = $payment_date;
              }elseif($labels_info->f('field_name')=='your_ref')
              {
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='our_ref')
              {
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='acc_manager_id')
              {
                $manager=$this->db->field("SELECT username FROM users WHERE username LIKE '$value[$k]%'");
                $manager_id=$this->db->field("SELECT user_id FROM users WHERE username LIKE '$value[$k]%'");
                if(!$manager){
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $value[$k];
                }else{
                  $values_info[$j]['field_name'] = 'acc_manager_name';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = $manager;
                  $j++;
                  $values_info[$j]['field_name'] = 'acc_manager_id';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = $manager_id;
                }

              }elseif($labels_info->f('field_name')=='payment_term_type')
              {
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='buyer_email')
              {
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='amount')
              {
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='amount_vat')
              {
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='sent_date')
              {
                  $sent_date = ($value[$k] - 25569) * 86400;
                  $values_info[$j]['field_name'] = $labels_info->f('field_name');
                  $values_info[$j]['tabel_name'] = $tabel_name;
                  $values_info[$j]['value'] = $sent_date;
              }elseif($labels_info->f('field_name')=='item_value')
              {
                  $values_info[$j]['field_name'] = 'item_value';
                  $values_info[$j]['tabel_name'] = 'note_fields';
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='identity_id')
              {
                  $values_info[$j]['field_name'] = 'identity_id';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = $value[$k];
              }
              elseif($labels_info->f('field_name')=='buyer_name')
              {
                $company = $this->db->field("SELECT name FROM customers WHERE name LIKE '$value[$k]%'");
                $company_id = $this->db->field("SELECT customer_id FROM customers WHERE name LIKE '$value[$k]%'");
                if(!$company){
                  $values_info[$j]['field_name'] = 'buyer_name';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = htmlspecialchars($value[$k]);
                }else{
                  $values_info[$j]['field_name'] = 'buyer_name';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = htmlspecialchars($company);
                  $j++;
                  $values_info[$j]['field_name'] = 'buyer_id';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = $company_id;
                }
              }elseif($labels_info->f('field_name')=='btw_nr'){
                $exist_btw = $this->db->field("SELECT btw_nr FROM customers WHERE customer_id='".$company_id."'");
                if($exist_btw==$value[$k]){

                }else{
                  $btw = $this->db->field("UPDATE customers SET btw_nr='".$value[$k]."' WHERE customer_id='".$company_id."'");
                }
              }elseif($labels_info->f('field_name')=='buyer_address'){
                $values_info[$j]['field_name'] = $labels_info->f('field_name');
                $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                $values_info[$j]['value'] = addslashes($value[$k]);
              }elseif($labels_info->f('field_name')=='buyer_zip'){
                $values_info[$j]['field_name'] = $labels_info->f('field_name');
                $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                $values_info[$j]['value'] = addslashes($value[$k]);
              }elseif($labels_info->f('field_name')=='buyer_city'){
                $values_info[$j]['field_name'] = $labels_info->f('field_name');
                $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                $values_info[$j]['value'] = addslashes($value[$k]);
              }elseif($labels_info->f('field_name')=='buyer_country_id')
              {
                $country_id = $this->db->field("SELECT country_id FROM country WHERE name LIKE '$value[$k]%'");
                if($country_id){
                  $values_info[$j]['field_name'] = 'buyer_country_id';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = $country_id;
                }
              }elseif($labels_info->f('field_name')=='email_language')
              {
                  $values_info[$j]['field_name'] = 'email_language';
                  $values_info[$j]['tabel_name'] = 'tblinvoice';
                  $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='currency_type')
              {
                $values_info[$j]['field_name'] = 'currency_type';
                $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='vat_regime_id')
              {
                $values_info[$j]['field_name'] = 'vat_regime_id';
                $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='discount')
              {
                $values_info[$j]['field_name'] = 'discount_invoice';
                $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='name')
              {
                $exist_article=$this->db->field("SELECT article_id FROM pim_articles WHERE article_id='".$value[$k]."' ");
                if($exist_article){
                  $values_info[$j]['field_name'] = 'article_id';
                  $values_info[$j]['tabel_name'] = 'tblinvoice_line';
                  $values_info[$j]['value'] = $value[$k];
                }else{
                  $values_info[$j]['field_name'] = 'item_code';
                  $values_info[$j]['tabel_name'] = 'tblinvoice_line';
                  $values_info[$j]['value'] = $value[$k];
                }
              }elseif($labels_info->f('field_name')=='description')
              {
                $values_info[$j]['field_name'] = 'name';
                $values_info[$j]['tabel_name'] = 'tblinvoice_line';
                $values_info[$j]['value'] = addslashes($value[$k]);
              }elseif($labels_info->f('field_name')=='quantity')
              {
                $values_info[$j]['field_name'] = 'quantity';
                $values_info[$j]['tabel_name'] = 'tblinvoice_line';
                $values_info[$j]['value'] = $value[$k];
              }
              elseif($labels_info->f('field_name')=='price')
              {
                $values_info[$j]['field_name'] = 'price';
                $values_info[$j]['tabel_name'] = 'tblinvoice_line';
                $values_info[$j]['value'] = $value[$k];
                $j++;
                $values_info[$j]['field_name'] = 'amount';
                $values_info[$j]['tabel_name'] = 'tblinvoice_line';
                $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='vat')
              {
                $values_info[$j]['field_name'] = 'vat';
                $values_info[$j]['tabel_name'] = 'tblinvoice_line';
                $values_info[$j]['value'] = $value[$k];
              }elseif($labels_info->f('field_name')=='discount_line')
              {
                $values_info[$j]['field_name'] = 'discount';
                $values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
                $values_info[$j]['value'] = $value[$k];
              }
            }

            if($labels_info->f('tabel_name')=='tblinvoice' && $labels_info->f('field_name')=='serial_number'  )
              {  
                $invoice_id='';
                if(trim($value[$k])!=''){
                   $invoice_id = $this->db->field("SELECT id FROM tblinvoice WHERE serial_number LIKE '$value[$k]%' ");
                }
               
                if(!$invoice_id)
                {
                  $insert_inv = true;
                  $begin_query = "INSERT INTO ";

                }else
                {
                  $begin_query = "UPDATE ";
                  $insert_inv = false;
                  $end_query = " WHERE id='".$invoice_id."'";
                  $inv_ids=$invoice_id;

                }
                //$customer_id = 1;
                $no_action_customer = false;
            }

            if($labels_info->f('tabel_name')=='tblinvoice_line' && $labels_info->f('field_name')=='name' && trim($value[$k])!='')
            {

                  $insert_lin = true;
                  $begin_line_query = "INSERT INTO ";

            }
            if($labels_info->f('table_name')=='tblinvoice_payments' && $labels_info->f('field_name')=='date' && trim($value[$k])!=''){
                  $insert_payment = true;
                  $begin_payment_query = "INSERT INTO ";
            }

          }
        }
        $j++;
      }

      if($nr_headers==0)
      {
        msg::error ( gm("No field selected"),'error');
        //ark::$controller = 'save_imports';
        $in['step']=3;
        return false;
      }
      unset($insert_values);

      foreach ($values_info as $key => $val) {
        if($val['value']){
          if($val['tabel_name']=='tblinvoice')
          {
            $insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
          }
          if($val['tabel_name']=='tblinvoice_line')
          {
            $insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
          }
          if($val['tabel_name']=='tblinvoice_payments')
          {
            $insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
          }
        }
      }
      /*krsort($insert_values);*/
      // echo "<pre>";
      // print_r($insert_values);
      // echo "</pre>";
      // exit();

      foreach ($insert_values as $db_name => $query_string) {//db_name = numele tabelei;
        if ($db_name=='tblinvoice' && $begin_query && $db_name)
        {
          if($insert_inv==true)
          {
            
            $invoice_id = $this->db->insert($begin_query.$db_name." SET ".$query_string." f_archived=0, req_payment=100");
            $add_id= $invoice_id;
            $inserted_inv_arr[$inserted_inv] = $invoice_id;
            $inserted_inv++;
          }else
          {
            
            $invoice_id = $this->db->query($begin_query.$db_name." SET ".$query_string.' f_archived=0, req_payment=100 '.$end_query);
            $add_id= $inv_ids;
            $inv_update=true;
            $update_inv_arr[$update_inv] = $invoice_id;
            $update_inv++;
            //echo $begin_query.$db_name." SET ".$query_string.$end_query."<br>";
          }
        }

        if ($db_name=='tblinvoice_line')
        {
          $line_amount_final='';
          $total_vat='';
          $total_amount='';
          $total_amount_vat='';
           $begin_line_query = "INSERT INTO ";
            /*$invoice_line_id = $this->db->field("SELECT id FROM tblinvoice_line WHERE invoice_id='".$invoice_id."'");
            if($invoice_id)
            {

              $insert_line = true;
              $end_line_query = " WHERE article_lang_id='".$article_lang_id."'";

            }else{
              $begin_contact_query = "INSERT INTO ";
              $insert_cont = true;
            }*/
            $line_total='';
            $line_discount='';
            $disc_line='';
            $discount_vat='';
            $discount='';
            $discount_total='';
            $invoice_total='';
            $invoice_total_articles='';
            $discount_invoice='';
            $discount_invoice_total='';
            $article_main_id='';
            $invoice_total_vat='';
              $line_id = $this->db->insert($begin_line_query.$db_name." SET ".$query_string." invoice_id='".$add_id."' ");
              if($line_id){
                    $calcul_line = $this->db->query("SELECT price,quantity,discount,vat,article_id FROM tblinvoice_line WHERE id='".$line_id."'");
                    if($calcul_line->f('article_id')){
                      $article_main_id=$calcul_line->f('article_id');
                      $things=$this->db->query("SELECT internal_name,item_code FROM pim_articles WHERE article_id='".$article_main_id."'");
                      $line_total=(return_value($calcul_line->f('quantity')) * return_value($calcul_line->f('price')));
                     /* $line_total = round($line_total,ARTICLE_PRICE_COMMA_DIGITS);*/
                      $discount = return_value($calcul_line->f('discount'));
                      $discount_vat = ($line_total) * ( $discount / 100);
                      $invoice_total = $line_total - $discount_vat;
                      $this->db->query("UPDATE tblinvoice_line SET amount='".$invoice_total."',item_code='".$things->f('item_code')."',name='".$things->f('internal_name')."' WHERE id='".$line_id."'");
                    }

              }
              if(!$inv_update){
                $fields_invoice= $this->db->query("SELECT quantity,price,vat,discount FROM tblinvoice_line WHERE invoice_id= '".$add_id."' ");
                    $line_amount_final  = $fields_invoice->f('price') * $fields_invoice->f('quantity');
                    $total_vat = $line_amount_final * (return_value($fields_invoice->f('vat')) / 100);
                    $discount_invoice = return_value($fields_invoice->f('discount'));
                    $discount_invoice_total = ($line_amount_final) * ( $discount_invoice / 100);
                    $total_amount = $line_amount_final - $discount_invoice_total;
                    $total_amount_vat = $total_amount + $total_vat;
                    $this->db->query("UPDATE tblinvoice set amount = '".$total_amount."', amount_vat='".$total_amount_vat."' where id = '".$invoice_id."'");
                    if($in['mark_as_sent']==1 && $in['mark_as_paid']==0){
                        $invoice_date_curent= $this->db->field("SELECT invoice_date FROM tblinvoice WHERE id= '".$add_id."' ");
                                  $params=array(
                                        'invoice_id'              => $add_id,
                                        'invoice_date'            => $invoice_date_curent,
                                        'sent_date'               => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                                  );
                       $sent_id = $this->mark_sent($params);
                    }elseif($in['mark_as_sent']==0 && $in['mark_as_paid']==1){
                      $invoice_date_curent= $this->db->field("SELECT invoice_date FROM tblinvoice WHERE id= '".$add_id."' ");
                        $params=array(
                          'invoice_id'              => $add_id,
                          'invoice_date'            => $invoice_date_curent,
                          'sent_date'               => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                        );
                      $sent_id = $this->mark_sent($params);
                      $invoice_totalvat= $this->db->field("SELECT amount_vat FROM tblinvoice WHERE id= '".$add_id."' ");
                        $param=array(
                          'invoice_id'                      => $add_id,
                          'total_amount_due1'               => display_number($invoice_totalvat),
                          'payment_date'                    => date('Y-m-d'),
                        );
                      $pay_id = $this->pay($param);
                    }elseif($in['mark_as_sent']==1 && $in['mark_as_paid']==1){
                      $invoice_date_curent= $this->db->field("SELECT invoice_date FROM tblinvoice WHERE id= '".$add_id."' ");
                        $params=array(
                          'invoice_id'              => $add_id,
                          'invoice_date'            => $invoice_date_curent,
                          'sent_date'               => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                        );
                      $sent_id = $this->mark_sent($params);
                      $invoice_totalvat= $this->db->field("SELECT amount_vat FROM tblinvoice WHERE id= '".$add_id."' ");
                        $param=array(
                          'invoice_id'                      => $add_id,
                          'total_amount_due1'               => display_number($invoice_totalvat),
                          'payment_date'                    => date('Y-m-d'),
                        );
                      $pay_id = $this->pay($param);
                    }

              }else{
                $fields_invoice= $this->db->query("SELECT quantity,price,vat,discount FROM tblinvoice_line WHERE invoice_id= '".$add_id."' ");
                while($fields_invoice->next()){
                    $line_amount_final  = $fields_invoice->f('price') * $fields_invoice->f('quantity');
                    $total_vat += $line_amount_final * (return_value($fields_invoice->f('vat')) / 100);
                    $discount_invoice = return_value($fields_invoice->f('discount'));
                    $discount_invoice_total += ($line_amount_final) * ( $discount_invoice / 100);
                    $total_amount += $line_amount_final - $discount_invoice_total;
                    $total_amount_vat = $total_amount + $total_vat;
                    $this->db->query("UPDATE tblinvoice set amount = '".$total_amount."', amount_vat='".$total_amount_vat."' where id = '".$add_id."'");
                }
              }
              $inserted_line_arr[$inserted_line] = $line_id;
              $inserted_line++;

        } //end invoice_line



        if($db_name=='tblinvoice_payments'){
            $params=array(
                'invoice_id'        => $add_id,
                'amount'            => $total_amount_vat,
                'payment_date'      => $payment_date,
              );
            $payment_id = $this->pay($params);
        }
      } //end foreach
    }

    if($in['k'] == 0){
    $user_info = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
    $user_info->next();
    $this->db->query("INSERT INTO tblinvoice_import_log SET
              `date`='".$in['timestamp']."', filename='".$filename."',
              username='".$user_info->f('first_name')." ".$user_info->f('last_name')."',
              invoice_added='".$inserted_inv."',
              invoice_updated='".$update_inv."'");
    }else{
      $a = $this->db->query("SELECT * FROM tblinvoice_import_log WHERE `date` = '".$in['timestamp']."' ");
      $ca = unserialize($a->f('invoice_added'));
      foreach ($ca as $key => $value) {
        array_push($inserted_inv_arr,$value);
      }
      $cu = unserialize($a->f('invoice_updated'));
      foreach ($cu as $key => $value) {
        array_push($update_inv_arr,$value);
      }
      $this->db->query("UPDATE tblinvoice_import_log SET
              invoice_added=invoice_added+'".$inserted_inv."',
              invoice_updated=invoice_updated+'".$update_inv."'
              WHERE `date`='".$in['timestamp']."' ");
    }
    $in['done'] = '1';
    $in['class3']='text-primary';
    $in['step']=3;
    json_out($in);
    msg::success ( gm('Import completed successfully'),'success');
    return true;
  }
    function PageActive(&$in){
    if ($in['type'] == 'invoice') {
        $this->db->query("UPDATE settings SET value='".(int)$in['page']."' WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE' ");
    } else if ($in['type'] == 'credit'){
        $credit_general_condition_page = $this->db->field("SELECT constant_name FROM settings WHERE constant_name ='CREDIT_GENERAL_CONDITION_PAGE'");
        if ($credit_general_condition_page) {
            $this->db->query("UPDATE settings SET value='".(int)$in['page']."' WHERE constant_name='CREDIT_GENERAL_CONDITION_PAGE' ");
        } else {
            $this->db->query("INSERT INTO settings SET constant_name = 'CREDIT_GENERAL_CONDITION_PAGE', value = '".(int)$in['page']."', long_value = 'NULL', module = 'billing', type = '1'");
        }
    }
    msg::success ( gm('Changes saved'),'success');
    json_out($in);
  }

  function genNotesActive(&$in){
      if ($in['type'] == 'invoice') {
          $this->db->query("UPDATE settings SET value='".(int)$in['gen_notes']."' WHERE constant_name='INVOICE_NOTES_AND_GENERAL_CONDITIONS' ");
      } else if ($in['type'] == 'credit') {
          $credit_notes_and_general_conditions = $this->db->field("SELECT constant_name FROM settings WHERE constant_name ='CREDIT_NOTES_AND_GENERAL_CONDITIONS'");
          if ($credit_notes_and_general_conditions) {
              $this->db->query("UPDATE settings SET value='".(int)$in['gen_notes']."' WHERE constant_name='CREDIT_NOTES_AND_GENERAL_CONDITIONS'");
          } else {
              $this->db->query("INSERT INTO settings SET constant_name = 'CREDIT_NOTES_AND_GENERAL_CONDITIONS', value = '".(int)$in['gen_notes']."', long_value = 'NULL', module = 'billing', type = '1'");
          }
      }
    msg::success ( gm('Changes saved'),'success');
    json_out($in);
  }

  function active_sepa(&$in)
  {
    $this->db->query("UPDATE settings SET value='".$in['sepa_active']."' WHERE constant_name='SHOW_INVOICE_SEPA' ");
    msg::success(gm('Changes have been saved.'),"success");
    json_out($in);
  }
  function sepa_details(&$in){
    $sepa_id=$this->db->field("SELECT sepa_settings_id FROM sepa_settings ");
    if(!$sepa_id){

      $this->db->query("INSERT INTO sepa_settings SET
                                                  sepa_company          = '".$in['ACCOUNT_COMPANY']."',
                                                  sepa_email            = '".$in['ACCOUNT_EMAIL']."',
                                                  sepa_url              = '".$in['sepa_url']."',
                                                  sepa_bank_name        = '".$in['ACCOUNT_BANK_NAME']."',
                                                  sepa_description      = '".$in['sepa_description']."',
                                                  sepa_bic_code         = '".$in['ACCOUNT_BIC_CODE']."',
                                                  sepa_iban             = '".$in['ACCOUNT_IBAN']."',
                                                  sepa_reg_number       = '".$in['sepa_reg_number']."',
                                                  sepa_phone            = '".$in['sepa_phone']."',
                                                  sepa_fax              = '".$in['sepa_fax']."',
                                                  sepa_siret            = '".$in['ACCOUNT_SIRET']."',
                                                  sepa_rpr              = '".$in['sepa_rpr']."',
                                                  sepa_den_sociale      = '".$in['ACCOUNT_DEN_SOCIALE']."',
                                                  sepa_reg_commerce     = '".$in['ACCOUNT_REG_COMMERCE']."',
                                                  sepa_cap_social       = '".$in['ACCOUNT_CAP_SOCIAL']."',
                                                  sepa_code_naf         = '".$in['ACCOUNT_CODE_NAF']."',
                                                  sepa_juridical_form   = '".$in['ACCOUNT_JURIDICAL_FORM']."',
                                                  sepa_invoice_start    = '".$in['ACCOUNT_INVOICE_START']."',
                                                  sepa_inc_part         = '".$in['ACCOUNT_DIGIT_NR']."',
                                                  sepa_comp_id         = '".$in['COMPANY_ID']."',
                                                  sepa_authority         = '".$in['COMPANY_ID_AUTHORITY']."',
                                                  sepa_id         = '".$in['ACCOUNT_SEPA_ID']."',
                                                  sepa_vat_number         = '".$in['ACCOUNT_VAT_NUMBER']."' ");
      msg::success(gm("SEPA Settings has been successfully add"),"success");
    }else{
       $this->db->query("UPDATE sepa_settings SET sepa_company          = '".$in['ACCOUNT_COMPANY']."',
                                                  sepa_email            = '".$in['ACCOUNT_EMAIL']."',
                                                  sepa_url              = '".$in['sepa_url']."',
                                                  sepa_bank_name        = '".$in['ACCOUNT_BANK_NAME']."',
                                                  sepa_description      = '".$in['sepa_description']."',
                                                  sepa_bic_code         = '".$in['ACCOUNT_BIC_CODE']."',
                                                  sepa_iban             = '".$in['ACCOUNT_IBAN']."',
                                                  sepa_reg_number       = '".$in['sepa_reg_number']."',
                                                  sepa_phone            = '".$in['sepa_phone']."',
                                                  sepa_fax              = '".$in['sepa_fax']."',
                                                  sepa_siret            = '".$in['ACCOUNT_SIRET']."',
                                                  sepa_rpr              = '".$in['sepa_rpr']."',
                                                  sepa_den_sociale      = '".$in['ACCOUNT_DEN_SOCIALE']."',
                                                  sepa_reg_commerce     = '".$in['ACCOUNT_REG_COMMERCE']."',
                                                  sepa_cap_social       = '".$in['ACCOUNT_CAP_SOCIAL']."',
                                                  sepa_code_naf         = '".$in['ACCOUNT_CODE_NAF']."',
                                                  sepa_juridical_form   = '".$in['ACCOUNT_JURIDICAL_FORM']."',
                                                  sepa_invoice_start    = '".$in['ACCOUNT_INVOICE_START']."',
                                                  sepa_inc_part         = '".$in['ACCOUNT_DIGIT_NR']."',
                                                  sepa_comp_id         = '".$in['COMPANY_ID']."',
                                                  sepa_authority         = '".$in['COMPANY_ID_AUTHORITY']."',
                                                  sepa_id         = '".$in['ACCOUNT_SEPA_ID']."',
                                                  sepa_vat_number         = '".$in['ACCOUNT_VAT_NUMBER']."'
                          WHERE sepa_settings_id ='".$sepa_id."' ");

      msg::success(gm("SEPA Settings has been successfully update"),"success");
    }
    $this->db->query("UPDATE sys_message SET
          subject = '".$in['subject']."',
          use_html='1',
          html_content  = '".$in['text']."'
          WHERE name='sepamess' AND lang_code='".$in['lang_code']."' ");
    return true;
  }

  function archive_sepa(&$in) {
    $this->db->query("UPDATE tblinvoice SET archived_from_sepa='1' WHERE id = '".$in['id']."' ");
    msg::success(gm("Invoice Archived"),"success");
    return true;
  }

  function activate_sepa(&$in) {
    $this->db->query("UPDATE tblinvoice SET archived_from_sepa='0' WHERE id = '".$in['id']."' ");
    msg::success(gm("Invoice Activated"),"success");
    return true;
  }

  function markAsXML(&$in) {
    $in['invoices_ids']='';
    if($_SESSION['add_to_sepa']){
      foreach ($_SESSION['add_to_sepa'] as $key => $value) {
        if ($value)   $in['invoices_ids'] .= $key.',';
      }
    }
    /*foreach($in['invoice_export'] as $key=>$value){
        $in['invoices_ids'] .= $value.',';
    }*/
    $in['invoices_ids'] = rtrim($in['invoices_ids'],',');
    if(empty($in['invoices_ids'])){
        $in['invoices_ids']= '0';
    }
    $this->dbu->query("UPDATE tblinvoice SET paid_collection_date = '".$in['paid_collection_date']."', export_XML = '1', collection_date ='".strtotime($in['collection_date'])."' WHERE id IN (".$in['invoices_ids'].") ");
    json_out($in);
  }

  function uploadPDF_sepa(&$in){
    global $config;
    if (!empty($_FILES)) {
      $ext = pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);
      if($ext != 'pdf'){
        msg::error(gm('Invalid file type.'),"error");
        json_out($in);
        return false;
      }
      $original_name=$_FILES['Filedata']['name'];
      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetPath = '';
      $in['name'] = 'upload/mandate'.time().'.pdf';
      $targetFile = $in['name'];
      // Validate the file type
      $fileTypes = array('pdf'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      @mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

        move_uploaded_file($tempFile,$targetFile);
        ark::loadLibraries(array('aws'));
        $a = new awsWrap(DATABASE_NAME);
        $pdfPath = $targetFile;//upload server
        $ext = pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);
        $pdfFile = 'invoice/mandate/mandate_'.$in['mandate_id'].".".$ext;

        $b = $a->uploadFile($pdfPath,$pdfFile);//upload amazon

        unlink($targetFile);//delete server
        $link =  $a->getLink($config['awsBucket'].DATABASE_NAME.'/invoice/mandate/mandate_'.$in['mandate_id'].".".$ext);
        //$response['success'] = substr($imgName,0,-4).",".$link.",".$img_id.",".$width.",".$height;

        $this->db->query("UPDATE mandate SET
                             amazon_link = '".$link."',
                             original_name = '".$original_name."'
                             WHERE mandate_id='".$in['mandate_id']."' ");
        $in['original_name'] = $original_name;
        msg::success(gm("File uploaded successfully"),"success");
        json_out($in);
      } else {
        msg::error(gm('Invalid file type.'),"error");
        json_out($in);
      }
    }
  }

  function originalPDF_delete(&$in){
    $this->db->query("UPDATE mandate SET amazon_link = '', original_name = '' WHERE mandate_id='".$in['mandate_id']."' ");
    msg::success(gm('File deleted successfully'),"success");
    json_out($in);
  }

  function mandate_update(&$in){
     if(!$this->mandate_update_validate($in)){
      json_out($in);
      return false;
    }
      $this->db->query("UPDATE mandate SET   sepa_number           = '".$in['sepa_number']."',
                                              bank_iban             = '".$in['bank_iban']."',
                                              bank_bic_code         = '".$in['bank_bic_code']."',
                                              type_mandate          = '".$in['type_mandate']."',
                                              frequency             = '".$in['frequency']."',
                                              active_mandate        = '".$in['mode']."',
                                              creation_date         = '".strtotime($in['signature_date'])."'
                                              WHERE mandate_id ='".$in['mandate_id']."' ");
      msg::success(gm("Mandate has been successfully updated"),"success");
      json_out($in);
  }

  function mandate_update_validate(&$in)
  {
    $v = new validation($in);
    $v->field('bank_iban', gm('IBAN'), "required");
    $v->field('bank_bic_code', gm('BIC'), "required");
    $v->field('sepa_number', gm('Sepa number'), "required");
    return $v->run();
  }

  function delete_mandate(&$in){
    if(!$in['mandate_id']){
      msg::error(gm("Invalid ID."),"error");
      json_out($in);
      return false;
    }
    #query manadate amazon_link
    $link = $this->db->field("SELECT amazon_link FROM mandate WHERE mandate_id='".$in['mandate_id']."'  ");
    if($link){
      ark::loadLibraries(array('aws'));
      $a = new awsWrap(DATABASE_NAME);
      #query s3_links in $link
      $item = $this->dbu->field("SELECT item FROM s3_links WHERE link='".$link."'  ");
      $a->deleteItem($item);
    }

    $this->db->query("DELETE FROM  mandate WHERE mandate_id='".$in['mandate_id']."' ");
    msg::success(gm("Mandate deleted."),"success");
    return true;
  }

  function updateCustomerDataSepa(&$in)
  {

    if($in['field'] == 'contact_id'){
      $in['buyer_id'] ='';
    }
    if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr,
                  customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, customers.vat_regime_id,
                  customer_addresses.address_id, customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.billing=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
      $buyer_info->next();

      $in['delivery_address_id'] = $buyer_info->f('address_id');


      $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
      $in['contact_name'] ='';

      if($in['contact_id']){
        $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
        $in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
      }
      $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));

      if($buyer_info->f('address_id') != $in['main_address_id']){
        $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
      }
    }else{
      if(!$in['contact_id']){
        msg::error ( gm('Please select a company or a contact'),'error');
        return false;
      }
      $contact_info = $this->dbu->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
      $in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));

    }
    $in['mandate_id']=$in['item_id'];
    msg::success(gm('Sync successfull.'),'success');
    return true;
  }

  function add_mandate(&$in){
    if(!$this->mandate_SEPA_validate($in)){
      json_out($in);
      return false;
    }
    if(!$in['frequency']){
      msg::error(gm('Please select frequency'),"error");
      json_out($in);
      return false;
    }
    if(!$in['type_mandate']){
      msg::error(gm('Please select mandate type'),"error");
      json_out($in);
      return false;
    }

    if($in['buyer_id']){
      $buyer_info = $this->db->query("SELECT customer_addresses.*
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            if($buyer_info->f('address_id') != $in['main_address_id']){
                $buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
                $address_info = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
            }
       }else{
            $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $address_info = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        }
   }else{
      $contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
      $same_address=0;
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
      }else{
        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $address_info = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
      }
   }

    $this->db->query("INSERT INTO mandate SET
                                                buyer_id              = '".$in['buyer_id']."',
                                                contact_id            = '".$in['contact_id']."',
                                                creation_date         = '".time()."',
                                                address_info          = '".addslashes($address_info)."',
                                                bank_iban             = '".$in['bank_iban']."',
                                                bank_bic_code         = '".$in['bank_bic_code']."',
                                                type_mandate          = '".$in['type_mandate']."',
                                                frequency             = '".$in['frequency']."',
                                                sepa_number           = '".generate_sepa_number(DATABASE_NAME)."' ");

    $last_number=$this->db->field("SELECT value FROM settings WHERE constant_name='SEPA_START_NUMBER' ");
    $last_number=$last_number+1;
    $this->db->query("UPDATE settings SET value='".$last_number."' WHERE constant_name='SEPA_START_NUMBER' ");
    msg::success(gm("Mandate has been successfully added"),"success");
    json_out($in);
  }
  function mandate_SEPA_validate(&$in)
  {
    $v = new validation($in);

    $v->field('buyer_id', gm('ID'), "required");
    $v->field('bank_iban', gm('IBAN'), "required");
    $v->field('bank_bic_code', gm('BIC'), "required");
    $v->field('frequency', gm('Frequency'), "required");
    $v->field('type_mandate', gm('Mandate type'), "required");

    return $v->run();
  }

  function erase_data(&$in)
  {
    $array_table = array();
    $tables_row = array();
    $column_row = array();
    $i=0;
    $tables = $this->db->query("SHOW TABLES WHERE Tables_in_".DATABASE_NAME." LIKE 'tblinvoice%'")->getAll();
    foreach ($tables as $query => $table) {
      array_push($array_table, $table);
        foreach ($table as $querys => $tab) {
          array_push($tables_row, $tab);
        }
      $i++;
    }
    for($z=0;$z<$i;$z++){
      //$this->db->query("TRUNCATE TABLE ".$tables_row[$z]."");
      $this->db->query("DELETE FROM ".$tables_row[$z]."");
    }
    msg::success ( gm("The data has been erased."),'success');
    return true;
  }
  function restore_data(&$in)
  {
    $this->erase_data($in);
    if($in['backup_id']!=0){
       set_time_limit(0);
        ini_set('memory_limit', '-1');
      $file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup'.$in['backup_id'].'.xml';
      $xmlDoc = simplexml_load_file($file1);
      $i=0;
      foreach ($xmlDoc as $key => $value) {
        foreach ($value as $k => $v) {
          $add = false;
          $sql   = "INSERT INTO {$key} SET ";
          foreach ($v as $field => $val) {
            $add=true;
            $sql .= " `".$field."`='".addslashes($val)."',";
          }
          $sql = rtrim($sql,',');
          if($add){
              $this->db->insert($sql);
          }
        }
        $i++;
      }
      msg::success ( gm('Restore completed successfully'),'success');
      return true;
    }else{
      msg::error ( gm('Select a file'),'error');
      return false;
    }

  }

  function start_to_invoice(&$in)
  {
    $in['debug'] = true;
    $in['same_invoice_time'] = time();
    json_out($in);
  }

  /**
   * here we check to see if we have the items in the database
   *
   * @return bool
   * @author
   *
   **/
  function validate_to_invoice(&$in)
  {
    switch ($in['type']) {
      case 'quotes':
        $id = 'id';
        $table = 'tblquote';
        $val = $in['item'];
        break;
      case 'orders':
        $id = 'order_id';
        $table = 'pim_orders';
        $val = $in['item'];
        break;
      case 'projects':
        $id = 'project_id';
        $table = 'projects';
        break;
      case 'interventions':
        $id = 'service_id';
        $table = 'servicing_support';
        $val = $in['item'];
        break;
      case 'tickets':
        $id = 'id';
        $table = 'cash_tickets';
        $val = $in['item'];
        break;
      default:
        $id='';
        $table='';
        list($project) = explode('&', $in['item']);
        list($name,$id) = explode('=', $project);
        $val = $id;
        break;
    }
    if($id && $table){
      $is_id = $this->dbu->field("SELECT $id FROM $table WHERE $id='".$val."' ");
      if($is_id == $val){
        return true;
      }else{
        msg::error(gm('Invalid ID'),"error");
      }
    }
    msg::error(gm('Invalid ID'),"error");
    return false;
  }

  /**
   * invoice stuff, like quotes, orders, projects etc...
   *
   * @return void
   * @author
   **/
  function to_invoice(&$in)
  {
    if(!$this->validate_to_invoice($in)){
      json_out($in);
      return false;
    }
    $is_contact = false;
    $in['quote_ts'] = time();
    $in['s_date'] = $in['quote_ts'];
    $in['sent_date'] = date(ACCOUNT_DATE_FORMAT,$in['quote_ts']);
    switch ($in['type']) {
      # we need to invoice a quote
      case 'quotes':
        $query = $this->dbu->query("SELECT * FROM tblquote WHERE id='".$in['item']."'");
        $query->next();
        $email_language = $query->f('email_language');
        $langs = $query->f('email_language');
        $disc = $query->f('discount');
        $buyer_id = $query->f('buyer_id');
        $contact_id = $query->f('contact_id');
        $identity_id=$query->f('identity_id');
        $source_id=$query->f('source_id');
        $type_id=$query->f('type_id');
        $segment_id=$query->f('segment_id');
        //$vat = get_customer_vat($buyer_id);
        $vat_regime_id=$query->f('vat_regime_id');
        $vat=$this->dbu->field("SELECT vats.value FROM vat_new 
          LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
          WHERE vat_new.id='".$query->f('vat_regime_id')."' ");
        if(!$vat){
          $vat=0;
        }
        if($vat_regime_id && $vat_regime_id>=1000){
          $extra_eu=$this->dbu->field("SELECT regime_type FROM vat_new WHERE id='".$vat_regime_id."' ");
          if($extra_eu==2){
            $vat=0;
          }
        }
        $i = 0;
        $query_line = $this->dbu->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
               INNER JOIN tblquote_version ON tblquote_line.version_id=tblquote_version.version_id
               WHERE tblquote_line.quote_id='".$in['item']."' AND tblquote_line.content_type='1' AND tblquote_version.active='1' ORDER BY id ASC ");
        while ($query_line->next()) {
          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = $query_line->f('name');
          $in['quantity'][$i] = display_number($query_line->f('quantity')*($query_line->f('package')/$query_line->f('sale_unit')));
          $in['price'][$i] = display_number($query_line->f('price'));
          $in['vat_val'][$i] = display_number($query_line->f('vat'));
          $in['discount_line'][$i] = display_number($query_line->f('line_discount'));
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='2';
          $in['visible'][$i]=$query_line->f('visible');
          $i++;
        }
        break;
      # we need to invoice a order
      case 'orders':
        $in['same_invoice'] = $in['same_time'];
        $in['order_id']=$in['item'];


        $query = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id='".$in['item']."'");
        $query->next();
        $email_language = $query->f('email_language');
        $langs = $query->f('email_language');

        $emailMessageData = array('buyer_id'    => $query->f('customer_id'),
                                  'param'       => 'add');
        $buyer_language = get_email_language($emailMessageData);
       
        $disc = $query->f('discount');
        if($query->f('field') == 'customer_id'){
          $buyer_id = $query->f('customer_id');
        }else{
          $contact_id = $query->f('customer_id');
        }
        $i=1;
        $in['order_inv'] = ";";
        $in['your_ref'] = addslashes($query->f('your_ref'));
        $identity_id=$query->f('identity_id');
        $source_id=$query->f('source_id');
        $type_id=$query->f('type_id');
        $segment_id=$query->f('segment_id');

        $vat_regime_id=$query->f('vat_regime_id');
        $vat=$this->dbu->field("SELECT vats.value FROM vat_new 
          LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
          WHERE vat_new.id='".$query->f('vat_regime_id')."' ");
        if(!$vat){
          $vat=0;
        }
        if($vat_regime_id && $vat_regime_id>=1000){
          $extra_eu=$this->dbu->field("SELECT regime_type FROM vat_new WHERE id='".$vat_regime_id."' ");
          if($extra_eu==2){
            $vat=0;
          }
        }

        $total_order_incomplete = false;
        $invoiced_deliveries = $this->dbu->query("SELECT * FROM pim_orders_delivery WHERE order_id='".$in['item']."' AND invoiced='1' ");
        if(!$invoiced_deliveries->next()){
          $total_order_incomplete = true;
        }
        $total_order_incomplete=false;//nu sunt singur de ce
        //console::log('total_order_incomplete:'.$total_order_incomplete,$query->f('rdy_invoice'));
        // $query_line = $this->dbu->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['item']."' AND content='0' ORDER BY order_articles_id ASC ");
        $query_line = $this->dbu->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['item']."' ORDER BY order_articles_id ASC ");
        while ($query_line->next())
        {

          $q=0;
          if($query->f('rdy_invoice') == 1 && $total_order_incomplete == true){
            // $q = $this->dbu->field("SELECT  pim_order_articles.quantity FROM  pim_order_articles WHERE order_id='".$in['item']."' AND order_articles_id='".$query_line->f('order_articles_id')."'");
            $q += $query_line->f('quantity');
            //console::log('article');
          }else{
            $deliveries = $this->dbu->query("SELECT * FROM pim_orders_delivery WHERE order_id='".$in['item']."' AND order_articles_id='".$query_line->f('order_articles_id')."' AND invoiced='0' ");
            if(ORDER_DELIVERY_STEPS==2){
              $deliveries = $this->dbu->query("SELECT * FROM pim_orders_delivery
                INNER JOIN pim_order_deliveries ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
                WHERE pim_orders_delivery.order_id='".$in['item']."'
                AND pim_orders_delivery.order_articles_id='".$query_line->f('order_articles_id')."'
                AND pim_orders_delivery.invoiced='0'
                AND pim_order_deliveries.delivery_done = '1' ");
            }
            while ($deliveries->next()) {
              $q += $deliveries->f('quantity');
              //console::log('delivery');
              $in['deliveries'][$i] .= $deliveries->f('order_delivery_id').',';
            }
          }
            $return=$this->dbu->field("SELECT   sum(pim_articles_return.quantity) FROM  pim_articles_return WHERE order_id='".$in['item']."' AND article_id='".$query_line->f('article_id')."'");
            $q=$q-$return;
                    if($query_line->f('tax_for_article_id')){
                       $in['is_tax'][$j] = 1;
                       $in['tax_for_article_id'][$j] = $query_line->f('tax_for_article_id');
                      //first we select qunatity of the article
                      $q_a=$this->dbu->field("SELECT sum(pim_orders_delivery.quantity)
                         FROM pim_orders_delivery
            INNER JOIN pim_order_articles on pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
            WHERE pim_order_articles.order_id='".$in['item']."'
            AND pim_order_articles.article_id='".$query_line->f('tax_for_article_id')."' and pim_orders_delivery.invoiced=0
            ");


                         //the we select qunatity of the taxex
                      $q_t=$this->dbu->field("SELECT  pim_order_articles.quantity FROM  pim_order_articles WHERE order_id='".$in['item']."' AND order_articles_id='".$query_line->f('order_articles_id')."'");


                       if($q_a>$q_t){

                          $q=$q_t;
                       }else{
                          $q=$q_a;
                       }


                       //now we have tosee if some taxes was already invoiced
                       $q_i=$this->dbu->field("SELECT   sum(tblinvoice_line.quantity)
                                        FROM   tblinvoice_line
                                        INNER JOIN tblinvoice ON tblinvoice.id=invoice_id
                                        WHERE REPLACE(tblinvoice.order_id, ';','')='".$in['item']."' AND tblinvoice_line.tax_for_article_id='".$query_line->f('tax_for_article_id')."'");
                         /*if($q-$q_i==0){
                           $q=0;
                         }*/

                         if($q_i+$q>$q_t){
                             $q=$q_t-$q_i;
                         }
                         //we have to check if there are some return
                 $return=$this->dbu->field("SELECT  sum(pim_articles_return.quantity)
                                   FROM  pim_articles_return
                                   WHERE pim_articles_return.order_id='".$in['item']."'
                                   AND pim_articles_return.article_id='".$query_line->f('tax_for_article_id')."' AND invoiced=0");



                    }


          //console::log($q,$query_line->f('content'));
          if($q==0 && $query_line->f('content')!=1){ continue; }
          if($q==0 && $query_line->f('tax_for_article_id')){ continue;}

          if($in['typeOfInvoice']==2){
              $your_ref_txt ='';
              if($query->f('your_ref')){
                $your_ref_txt =' - '.$query->f('your_ref');
              }
              $in['tr_id'][0] = 'tmp0';          
              $in['description'][0] =  geto_label_txt('order', $buyer_language).': '.$query->f('serial_number').$your_ref_txt;
              $in['content'][0] = 1;
              $in['title'][0] = 1;
              $in['double_customer'][0]=1;
              $in['visible'][0]=1;
              ksort($in['tr_id']);
          }

          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = (NOT_COPY_ARTICLE_INV==1 && $query_line->f('article_id')) ? get_article_label($query_line->f('article_id'),$email_language,'invoice'): stripslashes(htmlspecialchars_decode($query_line->f('article'),ENT_QUOTES));
           //$query_line->f('article');
          $in['quantity'][$i] = display_number($q*($query_line->f('packing')/$query_line->f('sale_unit')));
          // $in['price'][$i] = display_number($query_line->f('price') - ($query_line->f('price')*$query_line->f('discount')/100));
          $in['price'][$i] = display_number_var_dec($query_line->f('price'));
          //$in['purchase_price'][$i] = $query_line->f('purchase_price');
          $in['purchase_price'][$i] = display_number_var_dec($query_line->f('purchase_price'));
          $in['vat_val'][$i] = $query->f('remove_vat') ? display_number(0) : display_number($query_line->f('vat_percent'));
          $in['order'][$i] = $in['item'];
          $in['discount_line'][$i] = display_number($query_line->f('discount'));
          $in['readonly'][$i] = 1;
          $in['article_id'][$i] = $query_line->f('article_id');
          $in['article_code'][$i] = $query_line->f('article_code');
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='4';
          if($query_line->f('content')==1){
            $in['content'][$i] = $query_line->f('content');
          }
          $in['visible'][$i]=$query_line->f('visible');
          //begin article serial numbers
          if(SHOW_ART_S_N_INVOICE_PDF==1) {
            $order_id =  $in['order_id'];
            $art_serial_numbers = '';
            $count_art_s_n = $this->dbu2->field("SELECT COUNT(serial_numbers.serial_number) AS art_s_n
                        FROM serial_numbers
                        INNER JOIN pim_orders_delivery ON serial_numbers.delivery_id = pim_orders_delivery.delivery_id
                        WHERE pim_orders_delivery.order_id =  '".$in['item']."' AND serial_numbers.article_id = '".$query_line->f('article_id')."'
                        GROUP BY serial_numbers.id");
            if($count_art_s_n > 0){
              $art_s_n_data = $this->dbu2->query("  SELECT serial_numbers.serial_number AS art_s_n
                          FROM serial_numbers
                          INNER JOIN pim_orders_delivery ON serial_numbers.delivery_id = pim_orders_delivery.delivery_id
                          WHERE pim_orders_delivery.order_id =  '".$in['item']."' AND serial_numbers.article_id = '".$query_line->f('article_id')."'
                          GROUP BY serial_numbers.id");
              while($art_s_n_data->next()){
                $art_serial_numbers .= "\n".$art_s_n_data->f('art_s_n');
              }
              $art_serial_numbers = "\n".gm('Serial Numbers').':'.$art_serial_numbers;
            }else{
              $art_serial_numbers = "";
            }
            $in['description'][$i] .= $art_serial_numbers;
          }
          //end serial numbers


          $i++;
        }
        // begin shipping price
        $shipping_price = $this->dbu->field("SELECT shipping_price FROM pim_orders WHERE order_id = '".$in['item']."' ");
        if($shipping_price > 0){
          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = geto_label_txt('shipping_price', $buyer_language);
          $in['quantity'][$i] = 1;
          // $in['price'][$i] = display_number($query_line->f('price') - ($query_line->f('price')*$query_line->f('discount')/100));
          $in['price'][$i] = display_number_var_dec($shipping_price);
          $in['vat_val'][$i] = 0;
          $in['order'][$i] = $in['item'];
          $in['discount_line'][$i] = 0;
          $in['readonly'][$i] = 2;
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='4';
          $in['visible'][$i]=1;
          $i++;
        }


        //end shipping price

        $downpayment_int=$this->dbu->query("SELECT tblinvoice_line.* FROM tbldownpayments
          INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
          INNER JOIN tblinvoice_line ON tblinvoice.id=tblinvoice_line.invoice_id
          WHERE tbldownpayments.order_id='".$in['item']."' AND tblinvoice.f_archived='0' ");
        while($downpayment_int->next()){
          $in['tr_id'][$i] = 'tmp'.$i;
          $in['order_inv'] .= $in['item'].";";
          $in['disc_line'][$i] += 0;
          $in['discount_line'][$i] = 0;
          $in['description'][$i] = $downpayment_int->f('name');
          $in['quantity'][$i] = $downpayment_int->f('quantity');
          $in['price'][$i] =  display_number_var_dec(-$downpayment_int->f('price'));
         //$in['price_with_vat'][$i] = -$downpayment_int->f('price') -($downpayment_int->f('price')*$downpayment_int->f('vat')/100);
          //$in['vat_percent'][$i] = display_number($downpayment_int->f('vat'));
          $in['vat_val'][$i] = display_number($downpayment_int->f('vat'));

          $in['order'][$i] = $in['item'];
          $in['readonly'][$i] = 2;
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='4';
          $in['visible'][$i]=1;
          $i++;
        }

        $in['order_inv'] .= $in['item'].";";
        $in['our_ref'] = $query->f('serial_number');
        $in['remove_vat'] = $query->f('remove_vat');

        //last delivery date if we have one
        $in['due_date_vat'] = $this->dbu->field(" SELECT date FROM pim_order_deliveries
                    INNER JOIN pim_orders_delivery
                    ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
                    WHERE invoiced = '0'
                    AND pim_order_deliveries.order_id = '".$in['item']."'
                    ORDER BY pim_order_deliveries.date DESC LIMIT 1 ");
        if(!$in['due_date_vat']){
          $in['due_date_vat'] = $this->dbu->field("SELECT del_date FROM pim_orders WHERE order_id='".$in['item']."'");
        }
        break;
      # we need to invoice a project
      case 'projects':

        $ALLOW_ARTICLE_PACKING = $this->dbu->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
        $ALLOW_ARTICLE_SALE_UNIT = $this->dbu->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
        $in['same_invoice'] = $in['same_time'];
        $project = array();
        $items = explode('&', $in['item']);
        foreach ($items as $key) {
          if($key){
            list($k,$v) = explode('=', $key);
            $project[$k] = $v;
          }
        }
        $in['projects_ids']=array();
        $in['projects_id'] =array();
        array_push($in['projects_id'], $project['project_id']);
        array_push($in['projects_ids'], $project['project_id']);

        $query = $this->dbu->query("SELECT * FROM projects WHERE project_id='".$project['project_id']."'");
        $query->next();
        if($query->f('quote_id')){
          $email_language = $this->dbu->field("SELECT email_language FROM tblquote WHERE id='".$query->f('quote_id')."' ");
          $langs = $in['email_language'];
        }
        $buyer_id = $query->f('customer_id');
        $identity_id=$query->f('identity_id');
        $source_id=$query->f('source_id');
        $type_id=$query->f('type_id');
        $segment_id=$query->f('segment_id');
        //$vat = get_customer_vat($buyer_id);
        $vat_regime_id=$query->f('vat_regime_id');
        $vat=$this->dbu->field("SELECT vats.value FROM vat_new 
          LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
          WHERE vat_new.id='".$query->f('vat_regime_id')."' ");
        if(!$vat){
          $vat=0;
        }
        if($vat_regime_id && $vat_regime_id>=1000){
          $extra_eu=$this->dbu->field("SELECT regime_type FROM vat_new WHERE id='".$vat_regime_id."' ");
          if($extra_eu==2){
            $vat=0;
          }
        }
        if($query->f('is_contact') == '1'){
          $is_contact = true;
          $contact_id = $buyer_id;
          $buyer_id = 0;
        }
        $i = 1;
         if($in['typeOfInvoice']==2){
          $in['tr_id'][0] = 'tmp0';
          $in['description'][0] =  gm('Project').': '.$query->f('serial_number');
          $in['content'][0] = 1;
          $in['title'][0] = 1;
          $in['double_customer'][0]=1;
          $in['visible'][0]=1;
          ksort($in['tr_id']);
         }

        if(array_key_exists('purchase', $project)){
          $purchase = $this->dbu->query("SELECT * FROM project_purchase WHERE project_purchase_id='".$project['purchase']."' ");
          if($purchase->next()){
            $in['tr_id'][$i] = 'tmp'.$i;
            $margin_price = ($purchase->f('unit_price')*$purchase->f('margin')/100) + $purchase->f('unit_price');
            $in['description'][$i] = $query->f('name').': '.$purchase->f('description');
            $in['quantity'][$i] = display_number(1);
            $in['price'][$i] = display_number($margin_price);
            $in['vat_val'][$i] = display_number($vat);
            $in['project_purchase_id'][$i] = $project['purchase'];
            $in['origin_id'][$i]=$project['project_id'];
            $in['origin_type'][$i]='3';
            $in['visible'][$i]=1;
            $i++;
          }
        }elseif(array_key_exists('article', $project)){
          $article=$this->dbu->query("SELECT SUM(service_delivery.quantity) AS total_quantity,GROUP_CONCAT(service_delivery.id) as delivery_ids,project_articles.name,project_articles.price,projects.quote_id,pim_articles.packing,pim_articles.sale_unit,pim_articles.item_code FROM service_delivery
                  INNER JOIN projects ON service_delivery.project_id=projects.project_id
                  INNER JOIN project_articles ON service_delivery.project_id=project_articles.project_id AND service_delivery.a_id=project_articles.article_id
                  LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
                   WHERE service_delivery.project_id='".$project['project_id']."' AND service_delivery.a_id='".$project['article']."' GROUP BY service_delivery.a_id ");
          if($article->next()){
            $packing_art=($ALLOW_ARTICLE_PACKING && $article->f('packing')>0) ? $article->f('packing') : 1;
            $sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $article->f('sale_unit')>0) ? $article->f('sale_unit') : 1;
            $in['tr_id'][$i] = 'tmp'.$i;
            $in['description'][$i] = $query->f('name').': '.$article->f('name');
            $in['quantity'][$i] = display_number($article->f('total_quantity')*($packing_art/$sale_unit_art));
            $in['price'][$i] = display_number($article->f('price'));
            $in['article_code'][$i]=$article->f('item_code');
            $in['vat_val'][$i] = display_number($vat);
            $in['project_article_id'][$i] = $article->f('delivery_ids');
            $in['origin_id'][$i]=$project['project_id'];
            $in['origin_type'][$i]='3';
            $in['visible'][$i]=1;
            $i++;
            if($article->f('quote_id')>0){
              $article_tax=$this->dbu->query("SELECT pim_article_tax.amount,pim_article_tax.description FROM pim_article_tax
                  INNER JOIN pim_articles_taxes ON pim_article_tax.tax_id=pim_articles_taxes.tax_id
                  WHERE pim_articles_taxes.article_id='".$article->f('article_id')."' ");
              while($article_tax->next()){
                $in['tr_id'][$i] = 'tmp'.$i;
                $in['description'][$i] = $query->f('name').': '.$article_tax->f('description');
                $in['quantity'][$i] = display_number($article->f('quantity'));
                $in['price'][$i] = display_number($article_tax->f('amount'));
                $in['vat_val'][$i] = display_number($vat);
                $i++;
              }
            }
          }
        }elseif(array_key_exists('task', $project)){
          $tasks = $this->dbu->query("SELECT tasks.*, projects.name, projects.serial_number FROM tasks
                           INNER JOIN projects ON tasks.project_id = projects.project_id
                           WHERE task_id='".$project['task']."' ");
          if($tasks->next()){
            $project_ref[$tasks->f('name')] = $tasks->f('serial_number');
            // $our_ref .= $tasks->f('serial_number').',';
            $in['tr_id'][$i] = 'tmp'.$i;
            $in['description'][$i] = $tasks->f('name').": ".$tasks->f('task_name');
            $in['quantity'][$i] = display_number(1);
            $in['price'][$i] = display_number($tasks->f('task_budget'));
            $in['vat_val'][$i] = display_number($vat);
            $in['tasks_id'][$i] = $project['task'];
            $in['origin_id'][$i]=$project['project_id'];
            $in['origin_type'][$i]='3';
            $in['visible'][$i]=1;
            $i++;
          }


          $filter_expense = " ";

          if($invoice_start_date){
            $filter_expense = " AND date >= '".$invoice_start_date."' AND date < '".$invoice_end_date."' ";
          }

          $is_adhoc = $this->dbu->field("SELECT active FROM projects WHERE project_id='".$project['project_id']."' AND contract_id='0' ");

          if($is_adhoc == 2){
            $expense = $this->dbu->query("SELECT  project_expenses.id, sum(project_expenses.amount) AS amount, expense.name AS e_name, projects.name AS p_name,
                                                  expense.unit_price,project_user.user_name AS u_name,project_expenses.expense_id
                      FROM project_expenses
                      INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                      LEFT JOIN projects ON project_expenses.project_id=projects.project_id
                      LEFT JOIN project_user ON projects.project_id=project_user.project_id
                      WHERE project_expenses.project_id='".$project['project_id']."'
                      AND project_expenses.billable='1' AND `project_expenses`.`billed`!='1' $filter_expense
                      GROUP BY project_expenses.expense_id
                        ");
          }else{
            $expense = $this->dbu->query("SELECT project_expenses.id,SUM(project_expenses.amount) AS amount, project_expenses.expense_id,
                         expense.name AS e_name, expense.unit_price, projects.name AS p_name,
                         project_user.user_name AS u_name
                         FROM project_expenses
                         INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                         LEFT JOIN projects ON project_expenses.project_id=projects.project_id
                         LEFT JOIN project_user ON project_expenses.user_id=project_user.user_id
                         WHERE project_expenses.project_id='".$project['project_id']."' AND project_user.project_id='".$project['project_id']."'
                         AND project_expenses.expense_id IN (SELECT expense_id FROM project_expences WHERE project_id='".$project['project_id']."')
                         AND `project_expenses`.`billed`!='1'
                         $filter_expense GROUP BY project_expenses.expense_id ORDER BY project_expenses.user_id ");
          }
          while ($expense->next()){
            $in['tr_id'][$i] = 'tmp'.$i;
            $in['description'][$i] = $expense->f('p_name').': '.$expense->f('u_name').' '."\n".$expense->f('e_name');
            $in['quantity'][$i] = $expense->f('amount');
            $in['price'][$i] = display_number($expense->f('unit_price'));
            $in['origin_id'][$i]=$project['project_id'];
            $in['origin_type'][$i]='3';
            $in['visible'][$i]=1;
            if(!$expense->f('unit_price')){
              $in['quantity'][$i] = display_number(1);
              $in['price'][$i] = display_number($expense->f('amount'));
            }
            $in['vat_val'][$i] = display_number($vat);
            $in['expense'][$i]='';
            if($is_adhoc == 2){
              $in['expense'][$i] = $expense->f('id').",";
            }else{
              $expense_list = $this->dbu->query("SELECT id FROM project_expenses
                            WHERE expense_id IN (SELECT expense_id FROM project_expences WHERE project_id='".$project['project_id']."')
                            AND project_id='".$project['project_id']."' AND expense_id='".$expense->f('expense_id')."' ");
              while ($expense_list->next()) {
                $in['expense'][$i] .= $expense_list->f('id').",";
              }
            }
            $i++;
          }


        }else {
          $filter_task =" 1=1 AND task_time.billable=1 AND task_time.billed=0 AND task_time.customer_id='".$buyer_id."' AND task_time.project_id='".$project['project_id']."' AND task_time.approved='1' ";
          if($is_contact){
            $filter_task =" 1=1 AND task_time.billable=1 AND task_time.billed=0 AND task_time.customer_id='".$contact_id."' AND task_time.project_id='".$project['project_id']."' AND task_time.approved='1' ";
          }
          if(array_key_exists('custom_hours', $project) ){
            $spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
            $project['from'] = urldecode($project['from']);
            $project['to'] = urldecode($project['to']);
            switch ($project['custom_hours']){
              case 1:   //this month
                $invoice_start_date= mktime(0, 0, 0, date('m'), 1, date('Y'));
                $filter_task.=" and task_time.date >= '".$invoice_start_date."'";
                $invoice_end_date= mktime(23, 59, 59, date("m")+1 , 0, date("Y"));
                $filter_task.=" and task_time.date < '".$invoice_end_date."'";
                $time_description ='('.date(ACCOUNT_DATE_FORMAT,$invoice_start_date).' - '.date(ACCOUNT_DATE_FORMAT,$invoice_end_date).')';
                $time_start = $invoice_start_date;
                $time_end = $invoice_end_date;
                break;
              case 2:  //last month
                $invoice_start_date= mktime(0, 0, 0, date('m')-1, 1, date('Y'));
                $filter_task.=" and task_time.date >= '".$invoice_start_date."'";
                $invoice_end_date= mktime(23, 59, 59, date("m") , 0, date("Y"));
                $filter_task.=" and task_time.date < '".$invoice_end_date."'";
                $time_description ='('.date(ACCOUNT_DATE_FORMAT,$invoice_start_date).' - '.date(ACCOUNT_DATE_FORMAT,$invoice_end_date).')';
                $time_start = $invoice_start_date;
                $time_end = $invoice_end_date;
                break;
              case 3:  //custom  period
                $invoice_start_date= mktime(0, 0, 0, date('m',$project['from']), date('d',$project['from']), date('Y',$project['from']));
                $filter_task.=" and task_time.date >= '".$invoice_start_date."'";
                $invoice_end_date= mktime(23, 59, 59, date('m',$project['to']), date('d',$project['to']), date('Y',$project['to']));
                $filter_task.=" and task_time.date < '".$invoice_end_date."'";
                $time_description ='('.$project['from'].' - '.$project['to'].')';
                $time_start = $invoice_start_date;
                $time_end = $invoice_end_date;
              break;
            }
          }else{
            $time_description_start=$this->dbu->field("SELECT task_time.date
                                               FROM task_time
                                               WHERE ".$filter_task."
                                               ORDER BY task_time.date ASC ");

            $time_description_end=$this->dbu->field("SELECT task_time.date
                                               FROM task_time
                                               WHERE ".$filter_task."
                                               ORDER BY task_time.date DESC ");
            $time_description ='('.date(ACCOUNT_DATE_FORMAT,$time_description_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_description_end).')';
            $time_start = $time_description_start;
            $time_end = $time_description_end;
          }

          if($time_end){
            $in['due_date_vat'] = $time_end;
          }
          switch ($query->f('billable_type')){
            case 1: //task hourly rate
            //calculate time and prices
              $task = $this->dbu->query("SELECT task_time.*,tasks.task_name,tasks.t_h_rate,tasks.t_daily_rate,projects.name,SUM(task_time.hours) as total_task_hours, projects.active AS ac
                                    FROM task_time
                                    INNER JOIN tasks ON tasks.task_id=task_time.task_id
                                    INNER JOIN projects ON projects.project_id=task_time.project_id
                                    WHERE ".$filter_task."
                                    GROUP BY task_time.task_id ");
              while ($task->next()) {
                $name = $query->f('name').': '.$task->f('task_name').' '.$time_description;
                if($task->f('ac') == 2){
                  $name = $query->f('name').': '.$task->f('task_name').' '.$time_description;
                }
                $in['tr_id'][$i] = 'tmp'.$i;
                $in['description'][$i]=$name;
                $in['unitmeasure'][$i]='hours';
                $in['quantity'][$i]= display_number($task->f('total_task_hours'));
                $in['price'][$i]= $query->f('status_rate')==0 ? display_number($task->f('t_h_rate')) : display_number($task->f('t_daily_rate'));
                $in['vat_val'][$i]=display_number($vat);
                $in['origin_id'][$i]=$project['project_id'];
                $in['origin_type'][$i]='3';
                $in['task_time_ids'][$i]='';
                $in['visible'][$i]=1;
                $this->dbu->query("SELECT task_time.*
                                     FROM task_time
                                     WHERE ".$filter_task." AND task_time.task_id='".$task->f('task_id')."' ");
                while ($this->dbu->move_next()) {
                  $in['task_time_ids'][$i] .= $this->dbu->f('task_time_id').',';
                }
                $i++;
              }
              break;
            case 2: //person hourly rate
            //calculate time and prices
              $task = $this->dbu->query("SELECT task_time.*,project_user.user_name,project_user.p_h_rate,project_user.p_daily_rate,projects.name,SUM(task_time.hours) as total_task_hours
                                    FROM task_time
                                    INNER JOIN projects ON projects.project_id=task_time.project_id
                                    INNER JOIN project_user ON project_user.user_id=task_time.user_id AND project_user.project_id=task_time.project_id
                                    WHERE ".$filter_task."
                                    GROUP BY task_time.user_id ");
              while ($task->next()) {
                $in['tr_id'][$i] = 'tmp'.$i;
                $in['description'][$i]=$query->f('name').': '.$task->f('user_name').' '.$time_description;
                $in['unitmeasure'][$i]='hours';
                $in['quantity'][$i]=display_number($task->f('total_task_hours'));
                $in['price'][$i]=$query->f('status_rate')==0 ? display_number($task->f('p_h_rate')) : display_number($task->f('p_daily_rate'));
                $in['vat_val'][$i]=display_number($vat);
                $in['origin_id'][$i]=$project['project_id'];
                $in['origin_type'][$i]='3';
                $in['task_time_ids'][$i]='';
                $in['visible'][$i]=1;
                $this->dbu->query("SELECT task_time.*
                                     FROM task_time
                                     WHERE ".$filter_task." AND task_time.user_id='".$task->f('user_id')."' ");
                while ($this->dbu->move_next()) {
                  $in['task_time_ids'][$i].=$this->dbu->f('task_time_id').',';
                }
                $i++;
              }
              break;
            case 3: //project hourly rate
            //calculate time and prices
              $task = $this->dbu->query("SELECT task_time.*,projects.name,projects.pr_h_rate,SUM(task_time.hours) as total_task_hours
                                    FROM task_time
                                    INNER JOIN projects ON projects.project_id=task_time.project_id
                                    WHERE ".$filter_task."
                                    GROUP BY task_time.project_id ");
              while ($task->next()) {
                $in['tr_id'][$i] = 'tmp'.$i;
                $in['description'][$i]=$query->f('name').' : '.$time_description;
                $in['unitmeasure'][$i]='hours';
                $in['quantity'][$i]=display_number($task->f('total_task_hours'));
                $in['price'][$i]=display_number($task->f('pr_h_rate'));
                $in['vat_val'][$i]=display_number($vat);
                $in['origin_id'][$i]=$project['project_id'];
                $in['origin_type'][$i]='3';
                $in['task_time_ids'][$i]='';
                $in['visible'][$i]=1;
                $this->dbu->query("SELECT task_time.*
                                     FROM task_time
                                     WHERE ".$filter_task." AND task_time.project_id='".$task->f('project_id')."' ");
                while ($this->dbu->move_next()) {
                  $in['task_time_ids'][$i].=$this->dbu->f('task_time_id').',';
                }
                $i++;
              }
              break;
            case 4: //no hourly rate
              //calculate time and prices
              $task = $this->dbu->query("SELECT task_time.*,projects.name,SUM(task_time.hours) as total_task_hours
                                    FROM task_time
                                    INNER JOIN projects ON projects.project_id=task_time.project_id
                                    WHERE ".$filter_task."
                                    GROUP BY task_time.project_id ");
              while ($task->next()) {
                $in['tr_id'][$i] = 'tmp'.$i;
                $in['description'][$i]=$query->f('name').' : '.$time_description;
                $in['unitmeasure'][$i]='hours';
                $in['quantity'][$i]=display_number($task->f('total_task_hours'));
                $in['price'][$i]=display_number(0);
                $in['vat_val'][$i]=display_number($vat);
                $in['origin_id'][$i]=$project['project_id'];
                $in['origin_type'][$i]='3';
                $in['task_time_ids'][$i]='';
                $in['visible'][$i]=1;
                $this->dbu->query("SELECT task_time.*
                                     FROM task_time
                                     WHERE ".$filter_task." AND task_time.project_id='".$task->f('project_id')."' ");
                while ($this->dbu->move_next()) {
                  $in['task_time_ids'][$i].=$this->dbu->f('task_time_id').',';
                }
                $i++;
              }
              break;
            case 7: //function hourly rate
              //calculate time and prices
              $task = $this->dbu->query("SELECT task_time.*,tasks.task_name,tasks.t_h_rate,projects.name,SUM(task_time.hours) as total_task_hours, projects.active AS ac
                                    FROM task_time
                                    INNER JOIN tasks ON tasks.task_id=task_time.task_id
                                    INNER JOIN projects ON projects.project_id=task_time.project_id
                                    WHERE ".$filter_task."
                                    GROUP BY task_time.task_id ");
              while ($task->move_next()) {
                $name = $query->f('name').': '.$task->f('task_name').' '.$time_description;
                if($task->f('ac') == 2){
                  $name = $query->f('name').': '.$task->f('task_name').' '.$time_description;
                }
                $in['tr_id'][$i] = 'tmp'.$i;
                $in['description'][$i]=$name;
                $in['unitmeasure'][$i]='hours';
                $in['quantity'][$i]=display_number($task->f('total_task_hours'));
                $in['price'][$i]=display_number($task->f('t_h_rate'));
                $in['vat_val'][$i]=display_number($vat);
                $in['origin_id'][$i]=$project['project_id'];
                $in['origin_type'][$i]='3';
                $in['task_time_ids'][$i]='';
                $in['visible'][$i]=1;
                $this->dbu->query("SELECT task_time.*
                                     FROM task_time
                                     WHERE ".$filter_task." AND task_time.task_id='".$task->f('task_id')."' ");
                while ($this->dbu->move_next()) {
                  $in['task_time_ids'][$i].=$this->dbu->f('task_time_id').',';

                }

                $i++;
              }
              break;
          }

          $filter_expense = " ";

          if($invoice_start_date){
            $filter_expense = " AND date >= '".$invoice_start_date."' AND date < '".$invoice_end_date."' ";
          }

          $is_adhoc = $this->dbu->field("SELECT active FROM projects WHERE project_id='".$project['project_id']."' AND contract_id='0' ");

          if($is_adhoc == 2){
            $expense = $this->dbu->query("SELECT  project_expenses.id, sum(project_expenses.amount) AS amount, expense.name AS e_name, projects.name AS p_name,
                                                  expense.unit_price,project_user.user_name AS u_name,project_expenses.expense_id
                      FROM project_expenses
                      INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                      LEFT JOIN projects ON project_expenses.project_id=projects.project_id
                      LEFT JOIN project_user ON projects.project_id=project_user.project_id
                      WHERE project_expenses.project_id='".$project['project_id']."'
                      AND project_expenses.billable='1' AND `project_expenses`.`billed`!='1' $filter_expense
                      GROUP BY project_expenses.expense_id
                        ");
          }else{
            $expense = $this->dbu->query("SELECT project_expenses.id,SUM(project_expenses.amount) AS amount, project_expenses.expense_id,
                         expense.name AS e_name, expense.unit_price, projects.name AS p_name,
                         project_user.user_name AS u_name
                         FROM project_expenses
                         INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                         LEFT JOIN projects ON project_expenses.project_id=projects.project_id
                         LEFT JOIN project_user ON project_expenses.user_id=project_user.user_id
                         WHERE project_expenses.project_id='".$project['project_id']."' AND project_user.project_id='".$project['project_id']."'
                         AND project_expenses.expense_id IN (SELECT expense_id FROM project_expences WHERE project_id='".$project['project_id']."')
                         AND `project_expenses`.`billed`!='1'
                         $filter_expense GROUP BY project_expenses.expense_id ORDER BY project_expenses.user_id ");
          }
          while ($expense->next()){
            $in['tr_id'][$i] = 'tmp'.$i;
            $in['description'][$i] = $expense->f('p_name').': '.$expense->f('u_name').' '."\n".$expense->f('e_name');
            $in['quantity'][$i] = $expense->f('amount');
            $in['price'][$i] = display_number($expense->f('unit_price'));
            $in['origin_id'][$i]=$project['project_id'];
            $in['origin_type'][$i]='3';
            $in['visible'][$i]=1;
            if(!$expense->f('unit_price')){
              $in['quantity'][$i] = display_number(1);
              $in['price'][$i] = display_number($expense->f('amount'));
            }
            $in['vat_val'][$i] = display_number($vat);
            $in['expense'][$i]='';
            if($is_adhoc == 2){
              $in['expense'][$i] = $expense->f('id').",";
            }else{
              $expense_list = $this->dbu->query("SELECT id FROM project_expenses
                            WHERE expense_id IN (SELECT expense_id FROM project_expences WHERE project_id='".$project['project_id']."')
                            AND project_id='".$project['project_id']."' AND expense_id='".$expense->f('expense_id')."' ");
              while ($expense_list->next()) {
                $in['expense'][$i] .= $expense_list->f('id').",";
              }
            }
            $i++;
          }
          $in['timeframe_start'][$project['project_id']] = $time_start;
          $in['timeframe_end'][$project['project_id']] = $time_end;
          $in['timeframes']=array();
          $timesframes=array(
            'timeframe_start'   => $time_start,
            'timeframe_end'     => $time_end,
            'pr_id'     => $project['project_id'],
          );
          $in['timeframes'][$project['project_id']]=$timesframes;
        }
        $in['attach_timesheet'] = 0;
        if(ATTACH_TIMESHEET_INV==1){
          $in['attach_timesheet'] = 1;
        }
        break;
      # we invoice a service
      case 'interventions':
        $ALLOW_ARTICLE_PACKING = $this->dbu->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
        $ALLOW_ARTICLE_SALE_UNIT = $this->dbu->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
        $in['same_invoice'] = $in['same_time'];
        $in['service_id']=$in['item'];
        $query = $this->dbu->query("SELECT * FROM servicing_support WHERE service_id='".$in['item']."' AND billed=0 ");
        $query->next();
        $email_language = $query->f('email_language');
        $langs = $query->f('email_language');
        // $disc = $query->f('discount');
        $buyer_id = $query->f('customer_id');
        $contact_id = $query->f('contact_id');
        $identity_id=$query->f('identity_id');
        $source_id=$query->f('source_id');
        $type_id=$query->f('type_id');
        $segment_id=$query->f('segment_id');
        $planned_date = $query->f('planeddate');
        //$vat = get_customer_vat($buyer_id);
        $vat_regime_id=$query->f('vat_regime_id');
        $vat=$this->dbu->field("SELECT vats.value FROM vat_new 
          LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
          WHERE vat_new.id='".$query->f('vat_regime_id')."' ");
        if(!$vat){
          $vat=0;
        }
        if($vat_regime_id && $vat_regime_id>=1000){
          $extra_eu=$this->dbu->field("SELECT regime_type FROM vat_new WHERE id='".$vat_regime_id."' ");
          if($extra_eu==2){
            $vat=0;
          }
        }
        $i = 1;
         if($in['typeOfInvoice']==2){
          $in['tr_id'][0] = 'tmp0';
          $in['description'][0] =  getint_label_txt('intervention', $email_language).': '.$query->f('serial_number');
          $in['content'][0] = 1;
          $in['title'][0] = 1;
          $in['double_customer'][0]=1;
          $in['visible'][0]=1;
          ksort($in['tr_id']);
         }

         if($query->f('billable')){
            if(!$query->f('service_type')){
              $has_hour = false;
              $hoursPerUser = $this->dbu->field("SELECT SUM(end_time-start_time-break) FROM servicing_support_sheet WHERE service_id='".$in['item']."' ");
              /*while ($hoursPerUser) {
                $total += $hoursPerUser*$query->f('rate');
                $has_hour = true;
              }*/
              if($hoursPerUser) {
                /*$range = $this->dbu->query("SELECT MAX( `date` ) as dend , MIN( `date` ) as dstart FROM  `servicing_support_sheet` WHERE service_id='".$in['item']."' ");
                $time_description ='('.date(ACCOUNT_DATE_FORMAT,$range->f('dstart')).' - '.date(ACCOUNT_DATE_FORMAT,$range->f('dend')).')';*/
                $time_description ='('.date(ACCOUNT_DATE_FORMAT,$planned_date).')';
                $in['tr_id'][$i] = 'tmp'.$i;
                $in['description'][$i] = $query->f('serial_number') ." - ".$query->f('subject')." : ".$time_description;
                $in['quantity'][$i] = display_number($hoursPerUser);
                $in['price'][$i] = display_number($query->f('rate'));
                $in['vat_val'][$i] = display_number($vat);
                $in['discount_line'][$i] = display_number(0);
                $in['origin_id'][$i]=$in['item'];
                $in['origin_type'][$i]='5';
                $in['visible'][$i]=1;
                $i++;
              }
            }else{
                $int_tasks=$this->dbu->field("SELECT SUM(task_budget) FROM servicing_support_tasks WHERE service_id='".$in['item']."' AND article_id='0' ");
                if($int_tasks>0){
                    $in['tr_id'][$i] = 'tmp'.$i;
                    $in['description'][$i] = $query->f('serial_number') ." - ".$query->f('subject')." : ".gm('Fixed price');
                    $in['quantity'][$i] = display_number(1);
                    $in['price'][$i] = display_number($int_tasks);
                    $in['vat_val'][$i] = display_number($vat);
                    $in['discount_line'][$i] = display_number(0);
                    $in['origin_id'][$i]=$in['item'];
                    $in['origin_type'][$i]='5';
                    $in['visible'][$i]=1;
                    $i++;
                }       
            }
            $int_services=$this->dbu->query("SELECT servicing_support_tasks.*,pim_articles.item_code FROM servicing_support_tasks
              LEFT JOIN pim_articles ON servicing_support_tasks.article_id=pim_articles.article_id
              WHERE servicing_support_tasks.service_id='".$in['item']."' AND servicing_support_tasks.article_id!='0' AND servicing_support_tasks.closed='1'");
            while($int_services->next()){
              $in['tr_id'][$i] = 'tmp'.$i;
              $in['description'][$i] = $query->f('serial_number') ." - ".$int_services->f('task_name');
              $in['quantity'][$i] = display_number($int_services->f('quantity'));
              $in['price'][$i] = display_number($int_services->f('task_budget'));
              $in['vat_val'][$i] = display_number($vat);
              $in['discount_line'][$i] = display_number(0);
              $in['article_code'][$i]=$int_services->f('item_code');
              $in['origin_id'][$i]=$in['item'];
              $in['origin_type'][$i]='5';
              $in['visible'][$i]=1;
              $i++;
            }
        }
        
        $purchase = $this->dbu->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['item']."' AND delivered<>0 AND billable='1' ");
        while ($purchase->next()) {
          // $increment = true;
          // $total += ($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'))*$purchase->f('delivered');

          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = $query->f('serial_number') ." - ".$purchase->f('name');
          $in['quantity'][$i] = display_number($purchase->f('delivered'));
          $in['price'][$i] = display_number($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'));
          $in['vat_val'][$i] = display_number($vat);
          $in['discount_line'][$i] = display_number(0);
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='5';
          $in['visible'][$i]=1;
          $i++;
        }
        $intv_article = $this->dbu->query("SELECT SUM(service_delivery.quantity) AS total_quantity,GROUP_CONCAT(service_delivery.id) as delivery_ids,servicing_support_articles.name, servicing_support_articles.price, pim_articles.packing, pim_articles.sale_unit,pim_articles.item_code FROM service_delivery
              LEFT JOIN pim_articles ON service_delivery.a_id=pim_articles.article_id
              INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
              WHERE service_delivery.service_id='".$query->f('service_id')."' AND service_delivery.invoiced='0' AND servicing_support_articles.billable='1' AND servicing_support_articles.article_id!='0' GROUP BY service_delivery.a_id");
        while($intv_article->next()){
          $packing_art=($ALLOW_ARTICLE_PACKING && $intv_article->f('packing')>0) ? $intv_article->f('packing') : 1;
          $sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $intv_article->f('sale_unit')>0) ? $intv_article->f('sale_unit') : 1;
          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = $query->f('serial_number') ." - ".$intv_article->f('name');
          $in['quantity'][$i] = display_number($intv_article->f('total_quantity')*($packing_art/$sale_unit_art));
          $in['price'][$i] = display_number($intv_article->f('price'));
          $in['vat_val'][$i] = display_number($vat);
          $in['article_code'][$i]=$intv_article->f('item_code');
          $in['discount_line'][$i] = display_number(0);
          $in['service_delivery_ids'][$i]=$intv_article->f('delivery_ids');
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='5';
          $in['visible'][$i]=1;
          $i++;
        }
        $expense = $this->dbu->query("SELECT project_expenses.*, expense.name AS e_name, expense.unit_price, expense.unit, servicing_support.customer_id as customer_id
                  FROM project_expenses
                  INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                  INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
                  WHERE servicing_support.service_id='".$query->f('service_id')."' AND project_expenses.billable='1' ORDER BY id");
        while ($expense->next()) {
          $amount = $expense->f('amount');
          if($expense->f('unit_price')){
            $amount = $expense->f('amount') * $expense->f('unit_price');
          }
          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = $query->f('serial_number') ." ". get_user_name($expense->f('user_id')) ." : ". $expense->f('e_name') . " - " . date(ACCOUNT_DATE_FORMAT,$expense->f('date'));
          $in['quantity'][$i] = display_number($expense->f('amount'));
          $in['price'][$i] = display_number($expense->f('unit_price'));
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='5';
          if(!$expense->f('unit_price')){
            $in['quantity'][$i] = display_number(1);
            $in['price'][$i] = display_number($expense->f('amount'));
          }
          $in['vat_val'][$i] = display_number($vat);
          $in['discount_line'][$i] = display_number(0);
          $in['visible'][$i]=1;
          $i++;
        }
        $downpayment_int=$this->dbu->query("SELECT tblinvoice_line.* FROM tbldownpayments  INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
          INNER JOIN tblinvoice_line ON tblinvoice.id=tblinvoice_line.invoice_id
          WHERE tbldownpayments.service_id='".$query->f('service_id')."' AND tblinvoice.f_archived='0' ");
        while($downpayment_int->next()){
          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = $downpayment_int->f('name');
          $in['quantity'][$i] = display_number($downpayment_int->f('quantity'));
          $in['price'][$i] = -display_number($downpayment_int->f('price'));
          $in['vat_val'][$i] = display_number($downpayment_int->f('vat'));
          $in['discount_line'][$i]=display_number(0);
          $in['origin_id'][$i]=$in['item'];
          $in['origin_type'][$i]='5';
          $in['visible'][$i]=1;
          $i++;
        }
      break;
      # we invoice a ticket
      case 'tickets':
        $in['same_invoice'] = $in['same_time'];
        $in['ticket_id']=$in['item'];
        $query = $this->dbu->query("SELECT * FROM cash_tickets WHERE id='".$in['item']."' ");
        $query->next();
        $email_language = $query->f('email_language');
        $langs = $query->f('email_language');
        $customer=$this->dbu->query("SELECT name,first_name,last_name,our,created_contact,created_customer FROM cash_customers WHERE customer_id='".$query->f('customer_id')."' ");
        if($customer->f('our')){
          $c_id=$this->dbu->query("SELECT customers.customer_id,customer_contacts.contact_id FROM customer_contacts
                INNER JOIN customers ON customer_contacts.customer_id=customers.customer_id
                WHERE customer_contacts.cash_contact_id='".$query->f('customer_id')."' AND customers.active='1' ");
          $buyer_id = $c_id->f('customer_id');
          $contact_id = $c_id->f('contact_id');
        }else if(!$customer->f('our') && $customer->f('created_contact')){
          $c_id=$this->dbu->field("SELECT customers.customer_id FROM customer_contacts
                INNER JOIN customers ON customer_contacts.customer_id=customers.customer_id
                WHERE customer_contacts.contact_id='".$customer->f('created_contact')."' AND customers.active='1' ");
            $buyer_id = $c_id->f('customer_id');
            $contact_id = $customer->f('created_contact');
        }else{
            $buyer_id = $customer->f('created_customer');
            $contact_id = 0;
        }
        $i = 1;
        if($in['typeOfInvoice']==2){
          $in['tr_id'][0] = 'tmp0';
          $in['description'][0] =  gm('Ticket').':#'.$query->f('number');
          $in['content'][0] = 1;
          $in['title'][0] = 1;
          $in['double_customer'][0]=1;
          $in['visible'][0]=1;
          ksort($in['tr_id']);
        }

        global $config;
        $ch = curl_init();
        $tact_data=$this->dbu->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
        $headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));

        $calculated_disc=0;
        $amount_art=0;
        $articles_t=$this->dbu->query("SELECT cash_tickets_articles.*,cash_tickets_articles.article_id as art_id, pim_articles.item_code,pim_articles.internal_name,pim_articles.article_id, pim_articles.cash_article_id  FROM cash_tickets_articles
                  LEFT JOIN pim_articles ON cash_tickets_articles.article_id=pim_articles.cash_article_id
                  WHERE cash_tickets_articles.ticket_id='".$query->f('ticket_id')."' ");
        while($articles_t->next()){
          /*$orig_price=$articles_t->f('sell_price')*100/(100+$articles_t->f('vat'));
          if($articles_t->f('discount_line')){
            if($articles_t->f('discount_type')=='rate'){
              $disc_line=$articles_t->f('discount_line');
              $price_ld=$orig_price-$orig_price*$articles_t->f('discount_line')/100;
            }else{
              $disc_line=$articles_t->f('discount_line')*100/$orig_price;
              $price_ld=$orig_price-$articles_t->f('discount_line')/100;
            }
          }else{
            $disc_line=0;
            $price_ld=$orig_price;
          }*/

          $article_name=$articles_t->f('internal_name')?$articles_t->f('internal_name'):$articles_t->f('description');
          if(!$articles_t->f('cash_article_id') && !$articles_t->f('description')){
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/articles/'.$articles_t->f('art_id').'?api_key='.$tact_data->f('api')); 
                $put = curl_exec($ch);
                $info = curl_getinfo($ch);

                if($info['http_code']>300 || $info['http_code']==0){                         
                   //
                }else{
                    $resp_data=json_decode($put);
                     $article_name=$resp_data->name;
                }
          }
          
                
          $in['tr_id'][$i] = 'tmp'.$i;
          $in['description'][$i] = $article_name;
          $in['quantity'][$i] = display_number($articles_t->f('quantity'));
          //$in['price'][$i] = display_number($orig_price);
          $in['price'][$i] = display_number($articles_t->f('tax_free_price'));
          $in['vat_val'][$i] = display_number($articles_t->f('vat'));
          //$in['discount_line'][$i] = display_number($disc_line);
          $in['discount_line'][$i] = display_number(0);
          $in['article_code'][$i]=$articles_t->f('item_code');
          $in['article_id'][$i]=$articles_t->f('article_id');
          $in['visible'][$i]=1;
          $i++;
          //$amount_art+=($articles_t->f('quantity')*$price_ld);
        }
        /*if($query->f('discount_type')=='numeric'){
          $calculated_disc=$query->f('discount')*100/$amount_art;
        }*/
        break;
    }
    $in['our_ref'] = $query->f('serial_number');
    $in['actiune'] = gm('Added');
    $in['actiune_id']=1;

    if($buyer_id && !$is_contact){
      $buyer_details = $this->dbu->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers
                        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                        WHERE customer_id='".$buyer_id."' ");
      $buyer_address = $this->dbu->query("SELECT * FROM customer_addresses WHERE customer_id='".$buyer_id."' AND billing ='1' ");
      if($buyer_details->f('apply_fix_disc') && $buyer_details->f('apply_line_disc')){
        $apply_discount = "3";
      }else{
        if($buyer_details->f('apply_fix_disc')){
          $apply_discount = "2";
        }
        if($buyer_details->f('apply_line_disc')){
          $apply_discount = "1";
        }
      }
      $in['apply_discount'] = $apply_discount;
    }else{
      $buyer_details = $this->dbu->query("SELECT * FROM customer_contacts WHERE contact_id='".$contact_id."' ");
      $buyer_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$contact_id."' ");
    }
    if($in['type']=='interventions'){
      $in['apply_discount']=0;
    }
    if($in['type']=='orders'){
      $in['apply_discount']=$query->f('apply_discount');
    }
    if($in['type']=='tickets'){
      /*if($query->f('discount')){
        $in['apply_discount']=3;
        if($query->f('discount_type')=='rate'){
          $disc=$query->f('discount');
        }else{
          $disc=$calculated_disc;
        }
      }else{
        $in['apply_discount']=1;
        $disc=0;
      }*/
      $in['apply_discount']=0;
      $disc=0;
    }
    $in['invoice_line']=array();
    if($in['tr_id']){
      foreach ($in['tr_id'] as $key=>$value){

        if(!($in['is_tax'][$key])){
           $in['is_tax'][$key]=0;
        }
        $line_total2 = 0;
        $row_id = 'tmp'.$key;
        $vat_i = $in['vat_val'][$key] ? return_value($in['vat_val'][$key]) : $vat;

        $invoice_line=array(
          'tr_id'               => $row_id,
          'description'         => addslashes($in['description'][$key]),
          'quantity'            => $in['quantity'][$key],
          'price'               => $in['price'][$key],
          'task_time_ids'       => $in['task_time_ids'][$key],
          'expenses_ids'        => $in['expense'][$key],
          'order_ids'           => $in['order'][$key],
          'tasks_ids'           => $in['tasks_id'][$key],
          'purchase_ids'        => $in['project_purchase_id'][$key],
          'project_article_ids' => $in['project_article_id'][$key],
          'deliveries'          => $in['deliveries'][$key],
          'article_id'          => $in['article_id'][$key],
          'tax_id'              => $in['tax_id'][$key],
          'tax_for_article_id'  => $in['is_tax'][$key]==1?$in['tax_for_article_id'][$key]:'',
          'is_tax'              => $in['is_tax'][$key],
          'purchase_price'      => $in['purchase_price'][$key],
          'vat'                 => $in['vat_val'][$key] ? $in['vat_val'][$key] : $vat,
          'discount_line'       => $in['discount_line'][$key],

          'title'               => $in['title'][$key],
          'content'             => $in['content'][$key],
          'colspan'             => $in['content'][$key] ==1 ? 'colspan="'.$cols.'" class="last"' : '',
          'content_class'       => $in['content'][$key] ==1 ? ' type_content ' : '',
          'unmodifiable'        => $in['readonly'][$key] ? 'readonly':'',
          'article_id'          => $in['article_id'][$key],
          'article_code'        => $in['article_code'][$key],
          'show_for_articles'    => $in['article_code'][$key] ? true : false,
          'width_if_article_code'   => $in['article_code'][$key] ? 'width:155px;' : 'width:270px;',
          'service_delivery_ids'  => $in['service_delivery_ids'][$key],
          'origin_id'             => $in['origin_id'][$key],
          'origin_type'           => $in['origin_type'][$key],
          'double_customer'       => $in['double_customer'][$key],
          'visible'               => $in['visible'][$key],
        );

        if($in['type']=='tickets'){
          $invoice_line['price_vat']        = display_number(return_value($in['price'][$key])+(return_value($in['price'][$key])*return_value($in['vat_val'][$key])/100 ));
        }else{
          $invoice_line['price_vat']        = display_number(return_value($in['price'][$key])+(return_value($in['price'][$key])*$vat_i/100));
        }

        $discount_line = return_value($in['discount_line'][$key]);
        if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
          $discount_line = 0;
        }
        $line_total2= return_value($in['quantity'][$key]) * (return_value($in['price'][$key]) -return_value($in['price'][$key]) * $discount_line / 100);

        $invoice_line['line_total']           = display_number($line_total2);
        $invoice_line['hide_currency2']   = ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
        $invoice_line['hide_currency1']   = ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
        array_push($in['invoice_line'], $invoice_line);
      }
    }
    if($in['same_invoice']){
      $in['b_id'] = $buyer_id;
        if($in['typeOfInvoice'] == 2) {
           if(!$this->same_invoice($in,$i)) {
            json_out($in);
            return false;
          }
     }
    }

    if(!$in['remove_vat']){
      //$in['remove_vat'] = $buyer_details->f('no_vat');
      if($vat_regime_id){
          $in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$vat_regime_id."' ");
      }     
    }
    // if(!$in['email_language']){
      $in['email_language'] = $email_language;
      $in['langs'] = $in['email_language'];
    // }
    if(!$in['email_language']){
      $in['email_language'] = $buyer_details->f('internal_language');
      $in['langs'] = $in['email_language'];
    }
    if(!$in['email_language']){
      //$in['email_language'] = 1;
      $in['email_language']= $this->dbu->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'");
      $in['langs'] = $in['email_language'];
    }
    # invoice data
    $in['1']=$query->f('currency_type');
    $in['2']=$buyer_details->f('currency_type');
    $in['3']=ACCOUNT_CURRENCY_TYPE;

    $in['due_days'] = $buyer_details->f('payment_term');
    $in['invoice_due_date'] = $buyer_details->f('payment_term') ? $in['quote_ts'] + ($buyer_details->f('payment_term') * (60*60*24)) : time();
    $in['payment_term_type'] = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
    $in['payment_type_choose'] = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
    if($in['payment_term_type'] == 2){
      /*$curMonth = date('n',$in['quote_ts']);
      $curYear  = date('Y',$in['quote_ts']);
      $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
      $in['invoice_due_date'] = $buyer_details->f('payment_term') ? $firstDayNextMonth + ($buyer_details->f('payment_term') * (60*60*24)-1) : time();*/
      if($buyer_details->f('payment_term')){
        $tmstmp_month=$in['quote_ts'] + ( $buyer_details->f('payment_term') * ( 60*60*24 )-1);
        $lastday = date('t',$tmstmp_month);
        $in['invoice_due_date'] = mktime(23, 59, 59,date('m',$tmstmp_month), $lastday,date('Y',$tmstmp_month));
      }else{
        $in['invoice_due_date']=time();
      }
    }
    $in['discount'] = $disc ? display_number($disc) : ( $buyer_details->f('fixed_discount') ? display_number($buyer_details->f('fixed_discount')) : display_number(0)) ;
    $in['serial_number'] = addslashes(generate_invoice_number(DATABASE_NAME));
    $in['req_payment'] = display_number(100);
    $in['currency_type'] = $query->f('currency_type') ? $query->f('currency_type') : ($buyer_details->f('currency_type') ? $buyer_details->f('currency_type') : ACCOUNT_CURRENCY_TYPE);
    # we are gonna leave notes for now as the default invoice note
    // $in['notes'] = utf8_decode($query->f('notes'));
    $invoice_note_lang_id = $this->dbu->field("SELECT lang_id FROM pim_lang WHERE sort_order='1' AND active='1'");
    // multilanguage notes
    if($invoice_note_lang_id==1){
      $invoice_note = 'invoice_note';
    }else{
      $invoice_note = 'invoice_note_'.$invoice_note_lang_id;
    }
    //$in['apply_discount'] = $query->f('apply_discount');
    $in['notes_1'] = addslashes($this->dbu->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note' "));
    $in['notes_2'] = addslashes($this->dbu->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_2' "));
    $in['notes_3'] = addslashes($this->dbu->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_3' "));
    $in['notes_4'] = addslashes($this->dbu->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_4' "));

    $in['custom_translate_loop']=array();
    $custom_langs_inv = $this->dbu->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ");
    while($custom_langs_inv->next()) {
      $key = 'notes_'.$custom_langs_inv->f('lang_id');
      $in[$key] = addslashes($this->dbu->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type='invoice_note_".$custom_langs_inv->f('lang_id')."' "));
      $custom_translate_loop=array(
        'lang_id'           =>    $custom_langs_inv->f('lang_id'),
        'NOTES'             =>    $in[$key],
      );
      array_push($in['custom_translate_loop'], $custom_translate_loop);
    }

    $in['NOTES']=$in['notes_1'];
    //$in['notes2'] =  addslashes($buyer_details->f('invoice_note2'));
    $in['notes2']=$this->dbu->field("SELECT value FROM default_data
                   WHERE type = '".($in['email_language']==1 ? 'invoice_note' : 'invoice_note_'.$in['email_language'])."' ");

    if($vat_regime_id && $vat_regime_id>=10000 && $in['email_language']){
      $acc_langs=array('en','fr','nl','de');
      $lang_c=$this->dbu->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
      if($lang_c){
        if($lang_c=='du'){
            $lang_c='nl';
        }
        if(in_array($lang_c, $acc_langs)){
          $vat_notes=stripslashes($this->dbu->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$vat_regime_id."' "));
          if($vat_notes){
            if($in['notes2']){
              $in['notes2']=$in['notes2']."\n".$vat_notes;
            }else{
              $in['notes2']=$vat_notes;
            }
          }
        }
      }   
    }
    
    $in['contact_id'] = $buyer_details->f('contact_id');
    $in['buyer_id'] = $buyer_id;
    $in['buyer_name'] = $buyer_id ? addslashes($buyer_details->f('name')).' '.addslashes($buyer_details->f('l_name')) : addslashes($buyer_details->f('firstname')).' '.addslashes($buyer_details->f('lastname'));
    $in['buyer_address'] = addslashes($buyer_address->f('address'));
    $in['buyer_email'] = $buyer_id ? addslashes($buyer_details->f('c_email')) : addslashes($buyer_details->f('email'));
    $in['buyer_phone'] = $buyer_id ? $buyer_details->f('comp_phone') : $buyer_details->f('phone');
    $in['buyer_fax'] = $buyer_id ? $buyer_details->f('comp_fax') : '';
    $in['buyer_zip'] = $buyer_address->f('zip');
    $in['buyer_city'] = addslashes($buyer_address->f('city'));
    $in['buyer_country_id'] = $buyer_address->f('country_id');
    $in['buyer_state_id'] = $buyer_address->f('state_id');
    $in['seller_name'] = addslashes(ACCOUNT_COMPANY);
    $in['seller_d_address'] = addslashes(ACCOUNT_DELIVERY_ADDRESS);
    $in['seller_d_zip'] = ACCOUNT_DELIVERY_ZIP;
    $in['seller_d_city'] = addslashes(ACCOUNT_DELIVERY_CITY);
    $in['seller_d_country_id'] = ACCOUNT_DELIVERY_COUNTRY_ID;
    $in['seller_d_state_id'] = ACCOUNT_DELIVERY_STATE_ID;
    $in['seller_b_address'] = addslashes(ACCOUNT_BILLING_ADDRESS);
    $in['seller_b_zip'] = ACCOUNT_BILLING_ZIP;
    $in['seller_b_city'] = addslashes(ACCOUNT_BILLING_CITY);
    $in['seller_b_country_id'] = ACCOUNT_BILLING_COUNTRY_ID;
    $in['seller_b_state_id'] = ACCOUNT_BILLING_STATE_ID;
    $in['seller_bwt_nr'] = $buyer_details->f('btw_nr');
    //$in['vat'] = get_customer_vat($buyer_id);
    $in['vat'] = $vat;
    $in['vat_regime_id']=$vat_regime_id;
    $in['identity_id']=$identity_id;
    $in['source_id']=$source_id;
    $in['type_id']=$type_id;
    $in['segment_id']=$segment_id;
    $in['currency_rate'] = $in['currency_type'] != ACCOUNT_CURRENCY_TYPE ? $this->get_rate($in['currency_type']) : '';
    $in['quote_id'] = $in['type'] == 'quotes' ? $in['item'] : '';
    $in['sameAddress']=1;

    $acc_manager=$this->dbu->field("SELECT acc_manager_name FROM customers WHERE customer_id = '".$buyer_id."'");
    $acc_manager = explode(',',$acc_manager);
    $acc_manager_id=$this->dbu->field("SELECT user_id FROM customers WHERE customer_id = '".$buyer_id."'");
    $acc_manager_id = explode(',',$acc_manager_id);


     $in['acc_manager_id']=$acc_manager_id[0];
     $in['acc_manager_name']=$acc_manager[0];

    $in['from_to_invoice'] = true;
    # finaly create the invoice
    // $in['debug'] = true;
    // print_r($in);exit();

    if(!$this->add($in)){
      $in['actiune'] = gm('failed');
    }else{

      if($in['typeOfInvoice'] == 1){
        $this->mark_sent($in);
      }
    }
    json_out($in);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function same_invoice(&$in,$i)
  {
    $in['same_invoice_id'] = $this->dbu->field("SELECT id FROM tblinvoice WHERE buyer_id='".$in['b_id']."' AND same_invoice='".$in['same_invoice']."' ");
    if($in['same_invoice_id']){
        $this->add_lines($in);
        return false;
    }
    return true;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function add_lines(&$in)
  {
    //return true;
    $invoice_total=0;
    $line_vat = 0;
    $discount = 0;
    $disc = 0;


    if(is_array($in['invoice_line'])){
      $i=$this->dbu->field("SELECT sort_order FROM tblinvoice_line WHERE invoice_id='".$in['same_invoice_id']."' ORDER BY sort_order DESC LIMIT 1");
      if(!$i){
        $i==0;
      }else{
        $i=$i+1;
      }
      foreach ($in['invoice_line'] AS $nr => $nothing ){
        $line_total=0;
      //  if($in['description'][$nr]){

        $line_total=(return_value($nothing['quantity']) * return_value($nothing['price'])) ;
          // $discount = $line_total * (return_value($in['discount']) / 100);
          // $line_vat += ($line_total - $discount )*(return_value($in['vat_val'][$nr])/100);
        $discount_vat = $line_total * (return_value($in['discount']) / 100);
        $line_total = round($line_total,ARTICLE_PRICE_COMMA_DIGITS);
          $invoice_total+= $line_total;
          if($nothing['discount_line']){
            $disc += (return_value($nothing['quantity']) * (return_value($nothing['price'])*$nothing['discount_line']/100) );
            $discount_vat = (return_value($nothing['quantity']) * (return_value($nothing['price'])*$nothing['discount_line']/100) );
          }
          $invoice_total_vat += round(($line_total - $discount_vat) * (return_value($nothing['vat'])/100),2);
          $modifiable =($nothing['readonly'])? ", readonly = '1'":"";





              $this->dbu->query("INSERT INTO tblinvoice_line SET
                                                name                =   '".htmlspecialchars($nothing['description'],ENT_QUOTES,'UTF-8')."',
                                                invoice_id          =   '".$in['same_invoice_id']."',
                                                quantity            =   '".return_value($nothing['quantity'])."',
                                                price               =   '".return_value($nothing['price'])."',
                                                amount              =   '".$line_total."',
                                                f_archived          =   '0',
                                                created             =   '".$created_date."',
                                                created_by          =   '".$_SESSION['u_id']."',
                                                last_upd            =   '".$created_date."',
                                                last_upd_by         =   '".$_SESSION['u_id']."',
                                                vat         = '".return_value($nothing['vat'])."',
                                                discount      = '".return_value($nothing['discount_line'])."',
                                                  content_title             = '".htmlentities($nothing['title'])."',
                                                content             = '".htmlentities($nothing['content'])."',
                                                article_id          = '".$nothing['article_id']."',
                                                purchase_price      = '".$nothing['purchase_price']."',
                                                item_code           = '".addslashes($nothing['article_code'])."',
                                                sort_order          = '".$i."' ,
                                                visible             = '".$nothing['visible']."'"
                                                .$modifiable);
        //}

        if($nothing['task_time_ids']){
           $this->dbu->query("UPDATE task_time SET billed=1 WHERE task_time_id in (".rtrim($nothing['task_time_ids'],',').")");
        }

        if($nothing['expenses_ids']){
          $this->dbu->query("UPDATE project_expenses SET billed='1' WHERE id IN (".rtrim($nothing['expenses_ids'],',').")");
        }

        if($nothing['tasks_ids']){
          $this->dbu->query("UPDATE tasks SET closed='2' WHERE task_id='".$nothing['tasks_ids']."' ");
        }

        if($nothing['purchase_ids']){
          $this->dbu->query("UPDATE project_purchase SET invoiced='1' WHERE project_purchase_id='".$nothing['purchase_ids']."' ");
        }

        if($nothing['project_article_ids']){
          //$this->dbu->query("UPDATE project_articles SET billed='1' WHERE a_id='".$nothing['project_article_ids']."' ");
          $this->dbu->query("UPDATE service_delivery SET invoiced='1' WHERE id IN (".$nothing['project_article_ids'].") ");
        }

         if($nothing['deliveries']){
          $this->dbu->query("UPDATE pim_orders_delivery SET invoiced='1' WHERE order_delivery_id IN (".rtrim($nothing['deliveries'],',').") ");
        }

        if($nothing['order_ids']){
          $this->dbu->query("SELECT delivered FROM pim_order_articles WHERE order_id='".$nothing['order_ids']."' AND delivered='0'  AND content='0' and (article_id!=0 OR (article_id=0 AND tax_id=0)) ");
          if(!$this->dbu->next()){
            $this->dbu->query("UPDATE pim_orders SET invoiced='1' WHERE order_id='".$nothing['order_ids']."' ");
          }
        }

        if($nothing['service_delivery_ids']){
          $this->dbu->query("UPDATE service_delivery SET invoiced='1' WHERE id IN (".$nothing['service_delivery_ids'].") ");
        }

        $i++;
      }

      $discount = $invoice_total * (return_value($in['discount']) / 100);
      if($disc){
        $discount = $disc;
      }
      // $invoice_total = $invoice_total - $discount + $line_vat;
      $invoice_total = $invoice_total - $discount;
      $invoice_total_vat += $invoice_total;
      if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
        if($in['currency_type']){
          $invoice_total = $invoice_total*return_value($in['currency_rate']);
          $invoice_total_vat = $invoice_total_vat*return_value($in['currency_rate']);
        }
      }
      $am = $this->dbu->query("SELECT amount,amount_vat FROM tblinvoice WHERE id='".$in['same_invoice_id']."' ");
      $this->dbu->query("UPDATE tblinvoice SET amount='".($am->f('amount')+$invoice_total)."', amount_vat='".($am->f('amount_vat')+$invoice_total_vat)."' WHERE id='".$in['same_invoice_id']."'");
    }

    if($in['order_id']){
      $in['order_inv'] = $this->dbu->field("SELECT order_id FROM tblinvoice WHERE id='".$in['same_invoice_id']."' ").ltrim($in['order_inv'],";");
      $this->dbu->query("UPDATE tblinvoice SET order_id='".$in['order_inv']."' WHERE id='".$in['same_invoice_id']."'");

    }
    $in['actiune'] = gm('updated');
    $in['actiune_id']=2;

    $in['serial_number'] = $this->dbu->field("SELECT serial_number FROM tblinvoice WHERE id='".$in['same_invoice_id']."' ");
   if(!$in['serial_number'] || DRAFT_INVOICE_NO_NUMBER){

       $in['serial_number']='no_number';

    }

    if($in['projects_id']){
      foreach ($in['projects_id'] as $key) {
        $this->dbu->query("INSERT INTO item_links SET `from`='project_".$key."', `to`='invoice_".$in['same_invoice_id']."' ");
        if($in['timeframe_start'][$key]){
          $this->dbu->query("INSERT INTO tblinvoice_projects SET `invoice_id`='{$in['same_invoice_id']}', `project_id`='{$key}', `start_date`='{$in['timeframe_start'][$key]}', `end_date`='{$in['timeframe_end'][$key]}' ");
        }
        $exist = $this->dbu->query("SELECT id FROM tblinvoice WHERE id='".$in['same_invoice_id']."' AND our_ref LIKE '%".$in['our_ref']."%' ");
        if(!$exist->next()){
          if($in['our_ref']){
            $select = $this->dbu->field("SELECT our_ref FROM tblinvoice WHERE id='".$in['same_invoice_id']."' ");
            $ref = $in['our_ref'];
            if($select){
              $ref = $select.','.$in['our_ref'];
            }
            $update = $this->dbu->field("UPDATE tblinvoice SET our_ref='".$ref."' WHERE id='".$in['same_invoice_id']."' ");
          }
        }
      }
    }
    # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['same_invoice_id']."' ");
    if($in['service_id']){
      $this->dbu->query("UPDATE servicing_support SET billed='1' WHERE service_id='".$in['service_id']."' ");
      $this->dbu->query("UPDATE tblinvoice SET service_id='".($inv->f('service_id').'-'.$in['service_id'])."' WHERE id='".$in['same_invoice_id']."' ");
    }
    if($in['ticket_id']){
      $this->dbu->query("UPDATE cash_tickets SET invoice_id='".$in['same_invoice_id']."' WHERE id='".$in['ticket_id']."' ");
    }
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['same_invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    $this->generate_pdf($params);

    if(!empty($in['invoice_line']) && $in['invoice_line'][1]['origin_id']){
        $tracking_data=array(
            'target_id'         => $in['same_invoice_id'],
            'target_type'       => '1',
            'target_buyer_id'   => $in['b_id'],
            'lines'             => array(
                                          array('origin_id'=>$in['invoice_line'][1]['origin_id'],'origin_type'=>$in['invoice_line'][1]['origin_type'])
                                    )
          );
        $tracking_trace=$this->dbu->field("SELECT trace_id FROM tblinvoice WHERE id='".$in['same_invoice_id']."' ");
        if($tracking_trace){
          addTracking($tracking_data,$tracking_trace);
        }else{
          addTracking($tracking_data);
        }
    }
      if ($in['same_invoice_id']) {
          insert_message_log('order', '{l}Invoice created by{endl} '.get_user_name($_SESSION['u_id']), 'order_id', $in['order_id'], false, $_SESSION['u_id']);
      }

    # for pdf
    return true;
  }

  function get_rate($from)
  {

    $currency = currency::get_currency($from,'code');
    if(empty($currency)){
      $currency = "USD";
    }
    $separator = ACCOUNT_NUMBER_FORMAT;
    $into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');

    return currency::getCurrency($currency, $into, 1,$separator);
  }
  function gpasta($in)
  {
    # for pdf
    $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
    $params = array();
    $params['use_custom'] = 0;
    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
      $params['logo'] = $inv->f('pdf_logo');
      $params['type']=$inv->f('pdf_layout');
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
      $params['custom_type']=$inv->f('pdf_layout');
      unset($params['type']);
      $params['logo']=$inv->f('pdf_logo');
      $params['template_type'] = $inv->f('pdf_layout');
      $params['use_custom'] = 1;
    }else{
      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
    }
    #if we are using a customer pdf template
    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
      unset($params['type']);
    }
    $params['id'] = $in['invoice_id'];
    $params['lid'] = $inv->f('email_language');
    $params['save_as'] = 'F';
    echo $this->generate_pdf($params);
    return true;
    # for pdf
  }
  function gpastaxml($in)
  {
    $in['include_xml']=1;
    $in['id']=$in['invoice_id'];
    $in['remove_accounting_cost']=true;
    require_once(__DIR__.'/../controller/xml_invoice_print.php');
    return true;
    # for pdf
  }
  function set_identity(&$in)
  {
    $this->db->query("UPDATE settings SET value='".$in['identity_id']."' WHERE constant_name='INVOICE_IDENTITY_SET'");
      msg::success ( gm("Changes Saved."),'success');

      return true;
  }

  function updateDefaultLedger(&$in){
    $exist=$this->db->field("SELECT type FROM settings WHERE constant_name='".$in['name']."' ");
    if($exist){
      $this->db->query("UPDATE settings SET value='".$in['value']."' WHERE constant_name='".$in['name']."' ");
    }else{
      $this->db->query("INSERT INTO settings SET constant_name='".$in['name']."',value='".$in['value']."',module='billing' ");
    }
    if($in['value']){
      msg::success(gm('Default ledger set'),'success');
    }
    json_out($in);
  }
  function saveterm(&$in){
    $exist=$this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_APPLY_DISCOUNT'");
    if(is_null($exist)){
      $this->db->query("INSERT INTO settings SET constant_name='INVOICE_APPLY_DISCOUNT', value='".$in['apply_discount']."' ");
    }else{
      $this->db->query("UPDATE settings SET value='".$in['apply_discount']."' WHERE constant_name='INVOICE_APPLY_DISCOUNT' ");
    }

    $exist2=$this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_ADD_DELIVERY_ADDRESS'");
    if(is_null($exist2)){
      $this->db->query("INSERT INTO settings SET constant_name='INVOICE_ADD_DELIVERY_ADDRESS', value='".$in['add_delivery_address']."' ");
    }else{
      $this->db->query("UPDATE settings SET value='".$in['add_delivery_address']."' WHERE constant_name='INVOICE_ADD_DELIVERY_ADDRESS' ");
    }
    msg::success ( gm("Changes have been saved."),'success');
    return true;
  }

  function clearFailedExport(&$in){
    $this->db->query("DELETE FROM error_call WHERE app='".$in['app']."' AND module='".$in['module']."' ");
    msg::success(gm('Entries cleared'),'success');
    json_out($in);
  }

  //Set email language as account / contact language
  //Use logged in user as default language if are not set
  function get_email_language($buyer_id,$contact_id){ 
    $email_language = DEFAULT_LANG_ID;

      if($buyer_id){
        $customerLang = $this->db->field("SELECT language FROM customers WHERE customer_id='".$buyer_id."' ");
        if($customerLang !='0'){
          $email_language = $customerLang;
        }
      }

      if($contact_id){
        $contactLang = $this->db->field("SELECT language FROM customer_contacts WHERE contact_id='".$contact_id."' ");
        if($contactLang !='0'){
          $email_language = $contactLang;
        }
      }

      if($email_language == '0'){
        $default_email_language = $this->db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
        $email_language = $default_email_language;
      }

      return $email_language;
  }
  function updateSegment(&$in){
    $this->db->query("UPDATE tblinvoice SET segment_id='".$in['segment_id']."' WHERE id='".$in['id']."' ");
    if($in['segment_id']){
      $in['segment'] = $this->db->field("SELECT name FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
    }
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function updateSource(&$in){
    $this->db->query("UPDATE tblinvoice SET source_id='".$in['source_id']."' WHERE id='".$in['id']."' ");
    if($in['source_id']){
      $in['source'] = $this->db->field("SELECT name FROM tblquote_source WHERE id='".$in['source_id']."' ");
    }
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function updateType(&$in){
    $this->db->query("UPDATE tblinvoice SET type_id='".$in['type_id']."' WHERE id='".$in['id']."' ");
    if($in['type_id']){
      $in['xtype']=$this->db->field("SELECT name FROM tblquote_type WHERE id='".$in['type_id']."' ");
    }
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function checkMultipleInt(&$in){
    $is_different=false;
    $code_centre=array();
    foreach($in['services'] as $key=>$value){
      $code=$this->db->field("SELECT cost_centre FROM servicing_support WHERE service_id='".$value."' ");
      if(!$code){
        $code_centre['0']=$code;
      }else{
        $code_centre[$code]=$code;
      }
    }
    if(count($code_centre)>1){
      msg::error(gm('Cost Center different'),'error');
    }else{
      msg::success('Success','success');
    }
    json_out($in);
  }

  function paid_by_automatic_transfer(&$in){
    $this->db->query("UPDATE tblinvoice_incomming SET a_transfer='".$in['a_transfer']."' WHERE invoice_id='".$in['invoice_id']."' ");
    json_out($in);
  }

  function update_sent_date(&$in){

    $in['sent_date'] = strtotime($in['sent_date']);
    $in['formated_sent_date'] = date(ACCOUNT_DATE_FORMAT,$in['sent_date']);

    $log = $this->db->field("SELECT sent_date FROM reminders_logs WHERE company_id ='".$in['customer_id']."' ");
    if($log){
      $this->db->query("UPDATE reminders_logs SET sent_date='".$in['sent_date']."' , date_man_updated = '1' WHERE company_id ='".$in['customer_id']."' ORDER BY log_id DESC LIMIT 1");
    }else{
      $account_manager_id = $_SESSION['u_id'];
      $in['log_id']=$this->dbu->insert("INSERT INTO reminders_logs SET
                        account_manager_id = '".$account_manager_id."',
                        sent_date= '".$in['sent_date']."',
                        company_id = '".$in['customer_id']."',
                        date_man_updated = '1',
                        invoices='".implode(',',$in['invoice_ids'])."'
                      ");
    }
    foreach($in['invoice_ids'] as $key =>$value){
      insert_message_log($this->pag,'{l}Reminder sent date was updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$value);
    }
  
    json_out($in);
  }

    function setVatKPI(&$in)
  {
    $exist=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_VAT_INVOICE_KPI'");
    if(is_null($exist)){
      $this->db->query("INSERT INTO settings SET constant_name='USE_VAT_INVOICE_KPI', value='".$in['use_vat']."', module='invoice', type='1' ");
    }else{
      $this->db->query("UPDATE settings SET value='".$in['use_vat']."' WHERE constant_name='USE_VAT_INVOICE_KPI' ");
    }
      msg::success ( gm("Changes Saved."),'success');

      return true;
  }

  function setVatKPIPurchase(&$in){
    $exist=$this->db->field("SELECT `value` FROM settings WHERE constant_name='USE_VAT_P_INVOICE_KPI' ");
    if(is_null($exist)){
        $this->db->query("INSERT INTO settings SET constant_name='USE_VAT_P_INVOICE_KPI',`value`='".$in['use_vat']."' ");
    }else{
        $this->db->query("UPDATE settings SET `value`='".$in['use_vat']."' WHERE constant_name='USE_VAT_P_INVOICE_KPI'");
    }    
        msg::success ( gm("Changes Saved."),'success');
      return true;
  }

  function push_invoice_to_stripe(&$in){
      $c = push_invoice_to_stripe($in['invoice_id'], DATABASE_NAME, false, false);
      if($c['success']){
         msg::success(gm("Invoice pushed succesfully to Stripe"),'success');
      }elseif($c['error']){
        msg::error($c['error'], 'error');
      }
      //return true;
      json_out($in);
  }


  function saveSepaCheck(&$in){
    if($in['all']){
      if($in['value'] == 1){
        foreach ($in['item'] as $key => $value) {
          $_SESSION['add_to_sepa'][$value]= $in['value'];
        }
      }else{
        foreach ($in['item'] as $key => $value) {
          unset($_SESSION['add_to_sepa'][$value]);
        }
      }
    }else{
      if($in['value'] == 1){
        $_SESSION['add_to_sepa'][$in['item']]= $in['value'];
      }else{
        unset($_SESSION['add_to_sepa'][$in['item']]);
      }
    }
    json_out($in);
  }

  function updatePriceCategory(&$in)
  {
    if($in['invoice_id']){
      $this->db->query("UPDATE tblinvoice SET cat_id ='".$in['cat_id']."' WHERE id ='".$in['invoice_id']."'");

      if($in['buyer_id']){
        if($in['changeCatPrices']==1){
         
            foreach ($in['invoice_line'] as $key => $value) {
                $params = array(
                  'article_id' => $value['article_id'], 
                  'price' => return_value($value['price']), 
                  'quantity' => return_value($value['quantity']),
                  'customer_id' => $in['buyer_id'],
                  'cat_id' => $in['cat_id'],
                  'asString' => true
                );
                $price = $this->getArticlePrice($params);
                if($value['article_id']){
                  $in['invoice_line'][$key]['price'] = display_number($price);
                  $in['invoice_line'][$key]['price_vat'] = display_number($price+$price*return_value($in['invoice_line'][$key]['vat'])/100);
                  
                }                
                
              }
        
        }
        msg::success (gm("Sync successfull."),'success'); 
        ark::run('invoice-ninvoice-invoice-update');
      }
    }

    return true;
   }

    function getQRCode(&$in)
    {
        $url = 'https://epc-qr.eu/?bic='.urlencode($in['bic']).'&iban='.urlencode($in['iban']).'&euro='.urlencode($in['amount']).'&bname='.urlencode($in['bname']).'&info='.urlencode($in['ogm']).'&ver=2';

        $content = file_get_contents($url);
        if($content){
          if(strpos($content,'PNG')){
            $encoded_content = base64_encode($content);
            $headers = get_headers($url, 1); 
            $content_type = $headers[0]['Content-Type'];
            $src = 'data:'.$content_type.';base64,'.$encoded_content;
            $in['src']=$src;
            msg::success(gm("Success"),'success');
            json_out($in);
          }else{
            $in['src']=$src;
            $content = preg_replace('/^(<br\s*\/?>)*|(<br\s*\/?>)*$/i', '', $content);
             msg::error($content,'error');
             json_out($in);
          }

        }else{
          msg::error(gm("Error"),'error');
          return true;
        }

    }

    function purchase_invoice_details($in){
      $dropbox_active=$this->db->field("SELECT active FROM apps WHERE name='Dropbox' AND type='main' and main_app_id='0' ");
      if(!$dropbox_active){
        $this->db->query("UPDATE settings SET value='' WHERE constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE'");
        $this->db->query("UPDATE settings SET value='' WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE'");
        return true;
      }
      $automatic_xml = $this->db->field("SELECT value FROM settings WHERE constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE' ");
      if(is_null($automatic_xml)){
        $this->db->query("INSERT INTO settings SET constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE', value='".$in['automatic_xml']."' ");
      }else{
        $this->db->query("UPDATE settings SET value='".$in['automatic_xml']."' WHERE constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE'");
      }

      

      /*if($in['access_key']){
        $vat_number = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT_NUMBER' ");
          if($this->check_access_key($in['access_key'], $vat_number)){
            $export_billtobox_to_dropbox = $this->db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE' ");
            if(is_null($export_billtobox_to_dropbox)){
              $this->db->query("INSERT INTO settings SET constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE', value='".$in['export_billtobox_to_dropbox']."' ");
            }else{
              $this->db->query("UPDATE settings SET value='".$in['export_billtobox_to_dropbox']."' WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE'");
            } 
      
           $export_billtobox_to_dropbox_key = $this->db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE_KEY' ");
            if(is_null($export_billtobox_to_dropbox_key)){
              $this->db->query("INSERT INTO settings SET constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE_KEY', value='".$in['access_key']."' ");
            }else{
              $this->db->query("UPDATE settings SET value='".$in['access_key']."' WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE_KEY'");
            }
             msg::success ( gm("Changes Saved."),'success');
        }
       
      }*/
  
     msg::success ( gm("Changes Saved."),'success');
      return true;
    }

     /*function check_access_key($access_key, $owner){
      global $config;

      //$billtobox_ar_out_url = $config['billtobox_ar_out_url_test'];
      $billtobox_ar_out_url = $config['billtobox_ar_out_url'];
      $in['username'] ='aktiProd';
          if(DATABASE_NAME =='4a5f9937_4f34_21bb_631eb43c4876' || DATABASE_NAME =='salesassist_2'){
            $billtobox_ar_out_url = $config['billtobox_ar_out_url_test'];
            $in['username'] ='aktiTest';
      }

      $ch = curl_init();

      $data=array(
        'owner'                 => $owner,
        'owner_id_type'         => 'VAT',
        'access_key'            => $access_key,
        'action_type'           => 'all', 
        'limit'                 => 1    
      );
    
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, $in['username'].":".$access_key);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_URL, $billtobox_ar_out_url);

      $put = curl_exec($ch);
      $info = curl_getinfo($ch);

      if($info['http_code']==401){
        $err=json_decode($put);
        msg::error(gm('The authentication failed. Check the username and password.'),"error");
        return false;

      }elseif($info['http_code']==404){
        $err=json_decode($put);
        msg::error(gm('Error').' - '.$err,"error");
        return false;

      }else{
            
        return true;
      }
     
    }*/

    function setDatabase($dbName){
      global $database_config;
      $database_3 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $dbName,
      );
      $this->db = new sqldb($database_3);
      $this->db2 = new sqldb($database_3);
      $this->dbu = new sqldb($database_3);
      $this->dbu2 = new sqldb($database_3);
    }

    function getTrackingLine($in){
        $item = array();
        $base_type = '';
        $origin_id = '';
        switch($in['base_type']){
            case'3':
              // Orders
              $base_type = '4';
              $origin_id = $in['orders_id'];
              $item['origin_id'] = $origin_id;
              $item['origin_type'] = $base_type;
              break;
            case'4':
              // Projects
              $base_type = '3';
              $origin_id = (isset($in['project_id']) && is_array($in['project_id'])) ? $in['project_id'][0] : $in['projects_id'][0];
              $item['origin_id'] = $origin_id;
              $item['origin_type'] = $base_type;
              break;
            case'1':
              // Projects
              $base_type = '3';
              $origin_id = (isset($in['project_id']) && is_array($in['project_id'])) ? $in['project_id'][0] : $in['projects_id'][0];
              $item['origin_id'] = $origin_id;
              $item['origin_type'] = $base_type;
              break;  
            case'6':
              // Interventions
              $base_type = '5';
              $origin_id = $in['service_id'];
              $item['origin_id'] = $origin_id;
              $item['origin_type'] = $base_type;
              break;
        }

        return $item;
    }

    function export_jefacture(&$in){
      $v=new validation($in);
      $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
      if(!$v->run()){
        json_out($in);
      }

      $t=$this->dbu->field("SELECT type FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      $s=$this->dbu->field("SELECT serial_number FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

       if(!$s){
         if($t==0){
            $in['serial_number']=generate_invoice_number(DATABASE_NAME,false);
          }
          if($t==1){
            $in['serial_number']=generate_proforma_invoice_number(DATABASE_NAME,false);
          }
           if($t==2){
            $in['serial_number']=generate_credit_invoice_number(DATABASE_NAME,false);
          }

         $this->dbu->query("UPDATE tblinvoice SET serial_number='".$in['serial_number']."' WHERE id='".$in['invoice_id']."'");

        $ogm_number = $this->dbu->field("SELECT ogm FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
        if(!$ogm_number && ($t == 0 || $t ==3)){
          $ogm = generate_ogm(DATABASE_NAME,$in['invoice_id']);
          $this->dbu->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$in['invoice_id']."' ");
        }
                   # for pdf
        $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
        $params = array();
        $params['use_custom'] = 0;
        if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
          $params['logo'] = $inv->f('pdf_logo');
          $params['type']=$inv->f('pdf_layout');
          $params['logo']=$inv->f('pdf_logo');
          $params['template_type'] = $inv->f('pdf_layout');
        }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
          $params['custom_type']=$inv->f('pdf_layout');
          unset($params['type']);
          $params['logo']=$inv->f('pdf_logo');
          $params['template_type'] = $inv->f('pdf_layout');
          $params['use_custom'] = 1;
        }else{
          $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
          $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
        }
        #if we are using a customer pdf template
        if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
          $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
          unset($params['type']);
        }
        $params['id'] = $in['invoice_id'];
        $params['type_pdf'] = $inv->f('type');
        $params['lid'] = $inv->f('email_language');
        $params['save_as'] = 'F';

        $this->generate_pdf($params);
        # for pdf
      }

      if($in['mark_as_sent']=='1'){
        $sent_date= time();

        $this->dbu->query("UPDATE tblinvoice SET sent='1',sent_date='".$sent_date."' WHERE id='".$in['invoice_id']."'");

        $p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
        if($p){
          $proformaInv = $this->dbu->query("SELECT req_payment,status,paid FROM tblinvoice WHERE id='".$p."' ");
          $this->dbu->query("UPDATE tblinvoice SET sent='1', sent_date='".$sent_date."' WHERE id='".$p."' ");
          $status = $proformaInv->f('status');
          if($proformaInv->f('req_payment')==100){
            $paid=$proformaInv->f('paid');
          }else{
            $paid=2;
            $status=0;
          }
          $this->dbu->query("UPDATE tblinvoice SET status='".$status."', paid='".$paid."' WHERE id='".$in['invoice_id']."' ");
          $in['inv_status'] = $status;
          $in['inv_paid'] = $paid;
        }
        $in['send_time'] = date(ACCOUNT_DATE_FORMAT,$sent_date);
      }

      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND jefacture='1' AND type='0'");
      if(!$is_exported){
        include(__DIR__.'/jefacture.php');
        $jefacture_obj=new jefacture($in);
        $in['return_data']=1;
        $jefacture_obj->exportJefacture($in);
      }

      msg::success(gm('Invoice exported successfully'),'success');
      return true;
    }

    function export_billtobox(&$in){
      $v=new validation($in);
      $v->field('invoice_id', 'ID', 'required:exist[tblinvoice.id]', gm('Invalid ID'));
      if(!$v->run()){
        json_out($in);
      }

      $t=$this->dbu->field("SELECT type FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
      $s=$this->dbu->field("SELECT serial_number FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

       if(!$s){
         if($t==0){
            $in['serial_number']=generate_invoice_number(DATABASE_NAME,false);
          }
          if($t==1){
            $in['serial_number']=generate_proforma_invoice_number(DATABASE_NAME,false);
          }
           if($t==2){
            $in['serial_number']=generate_credit_invoice_number(DATABASE_NAME,false);
          }

         $this->dbu->query("UPDATE tblinvoice SET serial_number='".$in['serial_number']."' WHERE id='".$in['invoice_id']."'");

        $ogm_number = $this->dbu->field("SELECT ogm FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
        if(!$ogm_number && ($t == 0 || $t ==3)){
          $ogm = generate_ogm(DATABASE_NAME,$in['invoice_id']);
          $this->dbu->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$in['invoice_id']."' ");
        }
                   # for pdf
        $inv = $this->dbu->query("SELECT * FROM tblinvoice WHERE id = '".$in['invoice_id']."' ");
        $params = array();
        $params['use_custom'] = 0;
        if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
          $params['logo'] = $inv->f('pdf_logo');
          $params['type']=$inv->f('pdf_layout');
          $params['logo']=$inv->f('pdf_logo');
          $params['template_type'] = $inv->f('pdf_layout');
        }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
          $params['custom_type']=$inv->f('pdf_layout');
          unset($params['type']);
          $params['logo']=$inv->f('pdf_logo');
          $params['template_type'] = $inv->f('pdf_layout');
          $params['use_custom'] = 1;
        }else{
          $params['type']= $in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
          $params['template_type'] =$in['type']==2 ? ACCOUNT_INVOICE_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_PDF_FORMAT;
        }
        #if we are using a customer pdf template
        if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
          $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
          unset($params['type']);
        }
        $params['id'] = $in['invoice_id'];
        $params['type_pdf'] = $inv->f('type');
        $params['lid'] = $inv->f('email_language');
        $params['save_as'] = 'F';

        $this->generate_pdf($params);
        # for pdf
      }

      if($in['mark_as_sent']=='1'){
        $sent_date= time();

        $this->dbu->query("UPDATE tblinvoice SET sent='1',sent_date='".$sent_date."' WHERE id='".$in['invoice_id']."'");

        $p = $this->dbu->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
        if($p){
          $proformaInv = $this->dbu->query("SELECT req_payment,status,paid FROM tblinvoice WHERE id='".$p."' ");
          $this->dbu->query("UPDATE tblinvoice SET sent='1', sent_date='".$sent_date."' WHERE id='".$p."' ");
          $status = $proformaInv->f('status');
          if($proformaInv->f('req_payment')==100){
            $paid=$proformaInv->f('paid');
          }else{
            $paid=2;
            $status=0;
          }
          $this->dbu->query("UPDATE tblinvoice SET status='".$status."', paid='".$paid."' WHERE id='".$in['invoice_id']."' ");
          $in['inv_status'] = $status;
          $in['inv_paid'] = $paid;
        }
        $in['send_time'] = date(ACCOUNT_DATE_FORMAT,$sent_date);
      }

      $is_exported=$this->dbu->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND billtobox='1' AND type='0'");
      if(!$is_exported){
        include(__DIR__.'/billtobox.php');
        $billtobox_obj=new billtobox($in);
        $in['return_data']=1;
        $billtobox_obj->exportBillToBox($in);
      }

      msg::success(gm('Invoice exported successfully'),'success');
      return true;
    }

}