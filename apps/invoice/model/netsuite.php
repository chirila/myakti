<?php
/************************************************************************
* @Author: MedeeaWeb Works
***********************************************************************/
class netsuite
{
	var $db;
	var $user;
	var $pass;
	var $database_name;

	function netsuite(&$in,$dbase='')
	{
		global $database_config;
		if($dbase){
			$database_1 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $dbase,
			);
			$this->db = new sqldb($database_1);
			$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();
			$this->database_name=DATABASE_NAME;
		}		
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($database_2); // recurring billing
		$app =array();
		$app_id=$this->db->field("SELECT app_id FROM apps WHERE name='Netsuite' AND app_type='accountancy'");
/*		$actions = array('username','password');
		foreach ($actions as $key => $value) {
			$app[$value] = $this->db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='".$value."' ");
		}
		$this->user = $app['username'];
		$this->api_key = $app['password'];*/

	}

	function markExport(&$in)
	{
		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', type='".$in['type']."', ".$in['app']."=1 ");
		insert_message_log('invoice','{l}Invoice was exported by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		return true;
	}

	function unmarkExport(&$in)
	{
		$this->db->query("DELETE FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND ".$in['app']."=1 AND type='".$in['type']."' ");
		insert_message_log('invoice','{l}Invoice was put back on export on list by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		return true;
	}

	function exportNetsuite(&$in){
		global $config;
		$i=0;
		$invoices = array();
		if(array_key_exists('invoice_to_export', $_SESSION) === true){
	 			$invoices_to_export = $this->db->query("SELECT invoice_id FROM tblinvoice_to_export WHERE  time='".$_SESSION['invoice_to_export']."' ");
	 			while($invoices_to_export->next()){
	 				$i++;
	 				array_push($invoices, $invoices_to_export->f('invoice_id'));

	 			}
	 		}

		foreach($invoices as $key => $value){

			//$this->db->query("UPDATE tblinvoice SET netsuite_id='".$id."' WHERE id='".$in['invoice_id']."' ");
	 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$value."', netsuite='1',type=0 ");
	 		
	 		$in['actiune']=gm("Invoice exported successfully");

	 		if($in['from_acc']){
		 		$msg = '{l}Invoice was exported automatically to{endl} '.'NetSuite ';
		 	}else{
		 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'NetSuite ';
		 	}
	 		
	 		insert_message_log('invoice',$msg,'invoice_id',$value);
		}


	 	/*if($in['from_acc']){
	 		return true;
	 	}else{
	 		json_out($in);
	 	}*/
	 	return true;
	}
  

}//end class
?>

