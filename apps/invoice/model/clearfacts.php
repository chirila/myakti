<?php
/************************************************************************
* @Author: MedeeaWeb Works
***********************************************************************/
class clearfacts
{
	var $db;
	var $user;
	var $pass;
	var $api_key;
	var $database_name;

	function clearfacts(&$in,$dbase='')
	{
		global $database_config;
		if($dbase){
			$database_1 = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $dbase,
				);
				$this->db =  new sqldb($database_1); 
				$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();	
			$this->database_name=DATABASE_NAME;
		}	
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($database_2); 
		$app_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='ClearFacts' AND app_type='accountancy' AND main_app_id='0' ");
		$this->api_key=$app_data->f('api');
		//$this->user = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='username' ");
		//$this->pass = $this->db->field("SELECT api FROM apps WHERE main_app_id='".$app_data->f('app_id')."' AND type='password' ");
	}

	function generate_pdf(&$in)
	{
	    include(__DIR__.'/../controller/invoice_print.php');
	    return $str;
	}

	function checkPay(&$in)
	{ 
		global $config;
		$inv = array();

		$invs = $this->db->query("SELECT id,serial_number FROM tblinvoice WHERE f_archived='0' and tblinvoice.status = '0' and tblinvoice.sent = '1' AND (type='0' OR type='3') and tblinvoice.cfacts_id != '' and tblinvoice.cfacts_paid = '0' ORDER BY due_date ASC ");
		while ($invs->next()) {
			$inv[$invs->f('id')] = $invs->f('serial_number');
		}
		if(!empty($inv)){

				$times_run=0;
				foreach ($inv as $key => $value) {
						$cfacts_id = $this->db->field("SELECT cfacts_id FROM tblinvoice WHERE id='".$key."'"); 
						$ch = curl_init();
					    $headers=array('Content-Type: multipart/form-data','Authorization: Bearer '.$this->api_key);

				        $query ='query doc {
									  document(id: "'.$cfacts_id.'") {
									    date, 
									    ... on InvoiceDocument {
									      type, 
									      paymentState
									    }
									  }
									}';
					   
					    $data=array(
					    	'query'				=> $query,
							);

				    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
					    curl_setopt($ch, CURLOPT_POST, true);
					    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					    curl_setopt($ch, CURLOPT_URL, $config['clearfacts_api_url']);

						$put = curl_exec($ch);
					 	$info = curl_getinfo($ch);
					 	if($info['http_code']>300 || $info['http_code']==0){
					 		$resp_error=json_decode($put);
					 		$in['actiune']=gm('Fail:');
					 		insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],"Fail");
					 	}else{
					 		$clearfacts_data=json_decode($put,true);
					 		$payment_status = $clearfacts_data['data']['document']['paymentState'];
					 		$type = $clearfacts_data['data']['document']['type'];

					 		if($payment_status == 'PAID'){
					 			if($type == "SALE"){
					 				$in['invoice_id'] = $key;
									$amount = $this->get_payment($in);							
									$inv_up=array();
									array_push($inv_up,array('invoice_id'=>$key,'amount'=>display_number($amount)));
									$this->marked_payed($inv_up);
					 			}
					 			

					 		}
					 	}
					 	curl_close($ch);
	
				}
		}

		$inv_inc = array();

		$invs_inc = $this->db->query("SELECT invoice_id,booking_number FROM tblinvoice_incomming WHERE tblinvoice_incomming.status = '1' AND (tblinvoice_incomming.type='0' OR tblinvoice_incomming.type='3') and tblinvoice_incomming.cfacts_id != '' and tblinvoice_incomming.cfacts_paid = '0' ORDER BY due_date ASC ");
		while ($invs_inc->next()) {
			$inv_inc[$invs_inc->f('invoice_id')] = $invs_inc->f('booking_number');
		}
		if(!empty($inv_inc)){

				$times_run=0;
				foreach ($inv_inc as $key => $value) {
						$cfacts_id = $this->db->field("SELECT cfacts_id FROM tblinvoice_incomming WHERE invoice_id='".$key."'"); 
						$ch = curl_init();
					    $headers=array('Content-Type: multipart/form-data','Authorization: Bearer '.$this->api_key);

				        $query ='query doc {
									  document(id: "'.$cfacts_id.'") {
									    date, 
									    ... on InvoiceDocument {
									      type, 
									      paymentState
									    }
									  }
									}';
					   
					    $data=array(
					    	'query'				=> $query,
							);

				    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
					    curl_setopt($ch, CURLOPT_POST, true);
					    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					    curl_setopt($ch, CURLOPT_URL, $config['clearfacts_api_url']);

						$put = curl_exec($ch);
					 	$info = curl_getinfo($ch);
					 	if($info['http_code']>300 || $info['http_code']==0){
					 		$resp_error=json_decode($put);
					 		$in['actiune']=gm('Fail:');
					 		insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],"Fail");
					 	}else{
					 		$clearfacts_data=json_decode($put,true);
					 		$payment_status = $clearfacts_data['data']['document']['paymentState'];
					 		$type = $clearfacts_data['data']['document']['type'];

					 		if($payment_status == 'PAID'){
					 			
					 				$in['invoice_id'] = $key;
									$amount = $this->get_payment_inc($in);							
									$inv_up=array();
									array_push($inv_up,array('invoice_id'=>$key,'amount'=>display_number($amount)));
									$this->marked_payed_inc($inv_up);
					 		}
					 	}
					 	curl_close($ch);
	
				}
		}
		return true;
	}

	function get_payment(&$in){
		$tblinvoice=$this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$subtotal_vat = 0;
		$total_n = 0;
		$show_disc = false;
		$negative = 1;
	    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
	    if($use_negative==1){
	      $negative = -1;
	    }

	    $big_discount = $tblinvoice->f("discount");
	    if($tblinvoice->f('apply_discount') < 2){
	      $big_discount =0;
	    }
		$lines = $this->db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
		
		while ($lines->next()) {
			$line_discount = $lines->f('discount');
			if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
				$line_discount = 0;
			}
			$amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
			$amount_line_disc = $amount_line*$big_discount/100;
			$discount_value += $amount_line_disc;
			$subtotal_vat +=  ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
			$total_n += $amount_line - $amount_line_disc;
		}

		if($tblinvoice->f('discount') != 0 || $show_disc === true){
			$o['show_disc']=$show_disc;
		}

		if($tblinvoice->f('quote_id') && $tblinvoice->f('downpayment_drawn')){
			$total = $total_n+$subtotal_vat-$tblinvoice->f('downpayment_value');
		}else{
			$total = $total_n+$subtotal_vat;
		}

		//already payed
		$proforma_id=$this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
	    $payments_filter=" invoice_id='".$in['invoice_id']."'";
	    if($proforma_id){
	      $payments_filter.=" OR invoice_id='".$proforma_id."'";
	    }

		$already_payed1 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter." AND credit_payment='0' ");
		$already_payed1->move_next();

		$already_payed2 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter." AND credit_payment='1' ");
		$already_payed2->move_next();


		$total_payed = $already_payed1->f('total_payed')+($negative*$already_payed2->f('total_payed'));

		$req_payment_value=$total* $tblinvoice->f('req_payment')/100;
		if($tblinvoice->f('req_payment') == 100){
			$amount_due = round($total - $total_payed,2);
		}else{
			$amount_due = round($req_payment_value - $total_payed ,2);
		}
	
		return $amount_due;
	}

	function marked_payed($data){
		foreach($data as $key=>$value){
      		$start_payment_date = mktime(23,59,59,date('n'),date('j'),date('y'));
		
      		$total = 0;
    		$tblinvoice=$this->db->query("SELECT req_payment,currency_type,type,proforma_id,discount, apply_discount, bPaid_debt_id, serial_number  FROM tblinvoice WHERE id='".$value['invoice_id']."'");
    		$tblinvoice->next();
    		$currency = get_commission_type_list($tblinvoice->f('currency_type'));
    		$big_discount = $tblinvoice->f("discount");
    		if($tblinvoice->f('apply_discount') < 2){
      			$big_discount =0;
    		}
    		$lines = $this->db->query("SELECT amount, vat, discount FROM tblinvoice_line WHERE invoice_id='".$value['invoice_id']."' ");
		    while ($lines->next()) {
		      $line_disc = $lines->f('discount');
		      if($tblinvoice->f('apply_discount') == 0 || $tblinvoice->f('apply_discount') == 2){
		        $line_disc = 0;
		      }
		      $line = $lines->f('amount') - ( $lines->f('amount') * $line_disc / 100 );
		      $line = $line - $line * $big_discount / 100;

		      $total += $line + ($line * ( $lines->f('vat') / 100));

		    }
		    //already payed
		    if($tblinvoice->f('proforma_id')){
		      $filter = " OR invoice_id='".$tblinvoice->f('proforma_id')."' ";
		    }
		    $tot_payed=$this->db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='0' ");
		    $total_payed = $tot_payed;

		    $negative = 1;
		    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
		    if($use_negative==1){
		      $negative = -1;
		    }
    		$this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$value['invoice_id']."' {$filter}  AND credit_payment='1' ");
		    $this->db->move_next();
		    $total_payed += ($negative*$this->db->f('total_payed'));

		    $req_payment_value = $total;
		    if($tblinvoice->f('type')==1){
		      $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
		    }
		    $amount_due = round($req_payment_value - $total_payed,2);
		    $general_amount = return_value($value['amount']);

    		$payment_date=date("Y-m-d",$start_payment_date);
    		$this->db->query("UPDATE tblinvoice SET not_paid='0' WHERE id='".$value['invoice_id']."'");

		    //Mark as payed
		    if($general_amount == $amount_due || $general_amount>$amount_due){
		      $this->db->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$value['invoice_id']."'");
		      $p = $this->db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$value['invoice_id']."' ");
		      if($p){
		        $this->db->query("UPDATE tblinvoice SET paid='1', status='1' WHERE id='".$p."' ");
		      }
		    }
		    else {
		      $this->db->query("UPDATE tblinvoice SET paid='2' WHERE id='".$value['invoice_id']."'");
		    }
		    $date_format=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DATE_FORMAT' ");

		    $payment_view_date=date($date_format,$start_payment_date);
		    $info=$payment_view_date.'  -  '.place_currency(display_number($general_amount),$currency).' - ClearFacts';

    		$this->db->query("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$value['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$general_amount."',
                          info        = '".$info."' ");
    		$this->db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Payment has been successfully recorded by {endl} ClearFacts Cron')."', field_name='invoice_id', field_value='".$value['invoice_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
    		 $this->db->query("UPDATE tblinvoice SET cfacts_paid='1' WHERE id='".$value['invoice_id']."'");
			//$this->dbu_users->query("INSERT INTO cron_clearfacts SET `database_name`='".$this->database_name."',invoice_id='".$value['invoice_id']."'  ");
		}
	}

	function get_payment_inc(&$in){
		$total=$this->db->field("SELECT total_with_vat FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");


		$already_payed1 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' AND credit_payment='0' ");
		$already_payed1->move_next();

		$already_payed2 = $this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' AND credit_payment='1' ");
		$already_payed2->move_next();

		$negative = 1;
	    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
	    if($use_negative==1){
	      $negative = -1;
	    }

		$total_payed = $already_payed1->f('total_payed')+($negative*$already_payed2->f('total_payed'));
		$amount_due = round($total - $total_payed,2);
	
		return $amount_due;
	}

	function marked_payed_inc($data){
		foreach($data as $key=>$value){
      		$start_payment_date = mktime(23,59,59,date('n'),date('j'),date('y'));
		
      		$total = 0;
      		
    		$tblinvoice=$this->db->query("SELECT currency_type, total_with_vat FROM tblinvoice_incomming WHERE invoice_id='".$value['invoice_id']."'");
    		$tblinvoice->next();
    		$currency = get_commission_type_list($tblinvoice->f('currency_type'));
    		$total = $tblinvoice->f('total_with_vat');
		    $total_payed=$this->db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$value['invoice_id']."'  AND credit_payment='0' ");
	
		    $negative = 1;
		    $use_negative=$this->db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");
		    if($use_negative==1){
		      $negative = -1;
		    }
    		$this->db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$value['invoice_id']."'  AND credit_payment='1' ");
		    $this->db->move_next();
		    $total_payed += ($negative*$this->db->f('total_payed'));

		    $amount_due = round($total - $total_payed,2);
		    $general_amount = return_value($value['amount']);

    		$payment_date=date("Y-m-d",$start_payment_date);

		    //Mark as payed
		    if($general_amount == $amount_due || $general_amount>$amount_due){
		      $this->db->query("UPDATE tblinvoice_incomming SET paid='1',status='1' WHERE invoice_id='".$value['invoice_id']."'");
		    }
		    else {
		      $this->db->query("UPDATE tblinvoice_incomming SET paid='2',status='1' WHERE invoice_id='".$value['invoice_id']."'");
		    }
		    $date_format=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DATE_FORMAT' ");

		    $payment_view_date=date($date_format,$start_payment_date);
		    $info=$payment_view_date.'  -  '.place_currency(display_number($general_amount),$currency).' - ClearFacts';

    		$this->db->query("INSERT INTO tblinvoice_incomming_payments SET
                          invoice_id  = '".$value['invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$general_amount."',
                          info        = '".$info."' ");
    		$this->db->query("INSERT INTO logging SET pag='invoice', message='".addslashes('{l}Payment has been successfully recorded by {endl} ClearFacts Cron')."', field_name='invoice_id', field_value='".$value['invoice_id']."', date='".time()."', type='0', reminder_date='".time()."' ");
    		 $this->db->query("UPDATE tblinvoice_incomming SET cfacts_paid='1' WHERE invoice_id='".$value['invoice_id']."'");
			//$this->dbu_users->query("INSERT INTO cron_clearfacts SET `database_name`='".$this->database_name."',invoice_id='".$value['invoice_id']."'  ");
		}
	}

	function exportFacts(&$in){
		global $config;
		/*if(!DATABASE_NAME){
			define(DATABASE_NAME,$this->db->field("SELECT DATABASE()"));
		}*/
		//if(!$this->user || !$this->pass || !$this->api_key){
		if(!$this->api_key){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],"Invalid credentials");
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);	
			}
		}
		/*$user_acc=$this->db_users->field("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND active=1000");
		if(!$user_acc){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Accountant required to use ClearFacts'),'error');
			insert_error_call(DATABASE_NAME,"clearfacts","invoice",$in['invoice_id'],"Accountant required to use ClearFacts");
			json_out($in);
		}*/
		$accountant=$this->db_users->field("SELECT first_name FROM users WHERE user_id='".$user_acc."' ");
		$vat_number=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		if(!$vat_number){
			$in['actiune']=gm('Fail:');
			msg::error(gm('Vat Number required to use ClearFacts'),'error');
			insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],"Vat Number required to use ClearFacts");
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
			}
		}
		$vat_number=trim($vat_number," ");
		$vat_number_base=substr($vat_number,0,2);
		$vat_number=substr($vat_number,2);
		$vat_number=str_replace(" ","",$vat_number);
		$vat_number=str_replace(".","",$vat_number);
		$vat_number=str_replace(",","",$vat_number);
		$vat_number=str_replace("-","",$vat_number);
		$vat_number=str_replace("_","",$vat_number);
		$vat_number=str_replace("/","",$vat_number);
		$vat_number=str_replace("|","",$vat_number);

		$ACCOUNT_ADDRESS_INVOICE=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
		$ACCOUNT_DELIVERY_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
		$ACCOUNT_DELIVERY_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
		$ACCOUNT_DELIVERY_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
		$ACCOUNT_DELIVERY_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
		$ACCOUNT_BILL_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
		$ACCOUNT_BILL_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
		$ACCOUNT_BILL_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
		$ACCOUNT_BILL_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");

		$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
		$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
		$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
		$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;

		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$in['serial_number'] = $invoice->f('serial_number');
		$params = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'dbase'=>$this->database_name);
		if($invoice->f('pdf_layout')){
			$params['type']=$invoice->f('pdf_layout');
			$params['logo']=$invoice->f('pdf_logo');
		}else{
			$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
			$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
		}
		$str = $this->generate_pdf($params);
		
		$currency = currency::get_currency($invoice->f('currency_type'),'code');
		$invoice_type=$invoice->f('type')<2 ? '380' : '381';
		$contact = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."' ");
		$customer_vat=$this->db->field("SELECT btw_nr FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");
		$customer_vat=trim($customer_vat," ");
		$customer_vat_base=substr($customer_vat,0,2);
		$customer_vat=substr($customer_vat,2);
		$customer_vat=str_replace(" ","",$customer_vat);
		$customer_vat=str_replace(".","",$customer_vat);
		$customer_vat=str_replace(",","",$customer_vat);
		$customer_vat=str_replace("-","",$customer_vat);
		$customer_vat=str_replace("_","",$customer_vat);
		$customer_vat=str_replace("/","",$customer_vat);
		$customer_vat=str_replace("|","",$customer_vat);

		$get_invoice_rows = $this->db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' AND content='0' order by sort_order ASC ");
		$customer_reference=$this->db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");

		$i = 0;
		$total_amount = 0;
		$vat_percent = array();
		$sub_t = array();
		$sub_disc = array();
		$vat_percent_val = 0;
		$discount = $invoice->f('discount');
		$xml_lines='';
		while ($get_invoice_rows->next()){
			$line_d = $get_invoice_rows->f('discount');
			if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
				$line_d = 0;
			}
			$amount = $get_invoice_rows->f('amount') - $get_invoice_rows->f('amount') * $line_d /100;

			$total_amount += $get_invoice_rows->f('amount');
			$i++;

			$amount_d = $amount * $discount/100;
			if($invoice->f('apply_discount') < 2){
				$amount_d = 0;
			}
			$vat_percent_val = ($amount - $amount_d) * $get_invoice_rows->f('vat')/100;

			$vat_percent[$get_invoice_rows->f('vat')] += $vat_percent_val;
			$sub_t[$get_invoice_rows->f('vat')] += $amount;
			$sub_disc[$get_invoice_rows->f('vat')] += $amount_d;
			$ln_amount = twoDecNumber($get_invoice_rows->f('amount'));
			$ln_amount_vat = twoDecNumber($get_invoice_rows->f('amount')*$get_invoice_rows->f('vat')/100);
			$item_code_xml = '';
			if(htmlspecialchars($get_invoice_rows->f('item_code'))){
				$item_code_xml = '<cac:SellersItemIdentification>
                <cbc:ID>'.htmlspecialchars($get_invoice_rows->f('item_code')).'</cbc:ID>
            </cac:SellersItemIdentification>';
			}
			$ledger_xml='';
			if($get_invoice_rows->f('article_id')){
				$ledger_account_id=$this->db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$get_invoice_rows->f('article_id')."' ");
				if($ledger_account_id){
					$ledger_xml=$this->db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$ledger_account_id."' ");
				}
			}
			
			$line_xml = '
			  <cac:InvoiceLine>
				<cbc:ID>'.$i.'</cbc:ID>
				<cbc:InvoicedQuantity unitCode="C62" unitCodeListID="UNECERec20">'.twoDecNumber($get_invoice_rows->f('quantity')).'</cbc:InvoicedQuantity>
				<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount+$ln_amount_vat).'</cbc:LineExtensionAmount>
				<cbc:AccountingCost>'.$ledger_xml.'</cbc:AccountingCost>
				<cac:TaxTotal>
				  <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount_vat).'</cbc:TaxAmount>
				  <cac:TaxSubtotal>
				    <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount).'</cbc:TaxableAmount>
				    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount_vat).'</cbc:TaxAmount>
				    <cac:TaxCategory>
				      <cbc:ID schemeID="UNCL5305">S</cbc:ID>
				      <cbc:Name>03</cbc:Name>
				      <cbc:Percent>'.twoDecNumber($get_invoice_rows->f('vat')).'</cbc:Percent>
				      <cac:TaxScheme>
			            <cbc:ID>VAT</cbc:ID>
			          </cac:TaxScheme>
				    </cac:TaxCategory>
				  </cac:TaxSubtotal>
				</cac:TaxTotal>
				<cac:Item>
				  <cbc:Description>'.htmlspecialchars($get_invoice_rows->f('name')).'</cbc:Description>
				  '.$item_code_xml.'
				  <cac:ClassifiedTaxCategory>
			        <cbc:ID schemeID="UNCL5305">S</cbc:ID>
			        <cbc:Name>03</cbc:Name>
			        <cbc:Percent>'.twoDecNumber($get_invoice_rows->f('vat')).'</cbc:Percent>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:ClassifiedTaxCategory>	        
				</cac:Item>
				<cac:Price>
			      <cbc:PriceAmount currencyID="EUR">'.twoDecNumber(($ln_amount+$ln_amount_vat)/$get_invoice_rows->f('quantity')).'</cbc:PriceAmount>
			    </cac:Price>         
		       </cac:InvoiceLine>';
	       		$xml_lines.=$line_xml;
		}

		if(!$xml_lines){
			$line_xml = '
			  <cac:InvoiceLine>
				<cbc:ID>1</cbc:ID>
				<cbc:InvoicedQuantity unitCode="C62" unitCodeListID="UNECERec20">'.twoDecNumber(1).'</cbc:InvoicedQuantity>
				<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber(0).'</cbc:LineExtensionAmount>
				<cac:TaxTotal>
				  <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber(0).'</cbc:TaxAmount>
				  <cac:TaxSubtotal>
				    <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber(0).'</cbc:TaxableAmount>
				    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber(0).'</cbc:TaxAmount>
				    <cac:TaxCategory>
				      <cbc:ID schemeID="UNCL5305">S</cbc:ID>
				      <cbc:Name>03</cbc:Name>
				      <cbc:Percent>'.twoDecNumber(0).'</cbc:Percent>
				      <cac:TaxScheme>
			            <cbc:ID>VAT</cbc:ID>
			          </cac:TaxScheme>
				    </cac:TaxCategory>
				  </cac:TaxSubtotal>
				</cac:TaxTotal>
				<cac:Item>
				  <cbc:Description></cbc:Description>
				  <cac:ClassifiedTaxCategory>
			        <cbc:ID schemeID="UNCL5305">S</cbc:ID>
			        <cbc:Name>03</cbc:Name>
			        <cbc:Percent>'.twoDecNumber(0).'</cbc:Percent>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:ClassifiedTaxCategory>	        
				</cac:Item>
				<cac:Price>
			      <cbc:PriceAmount currencyID="EUR">'.twoDecNumber(0).'</cbc:PriceAmount>
			    </cac:Price>         
		       </cac:InvoiceLine>';
	       		$xml_lines.=$line_xml;
		}


		$tax_str ='';
		$vat_v = 0;
		$total_no_vat = 0;
		$total_vat = 0;

		foreach ($vat_percent as $key => $val){
			 if($sub_t[$key] - $sub_disc[$key]){
			 	$line_xml='<cac:TaxSubtotal>
		      <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($sub_t[$key] - $sub_disc[$key]).'</cbc:TaxableAmount>
		      <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:TaxAmount>	      
		      <cac:TaxCategory>
		        <cbc:ID schemeID="UNCL5305">S</cbc:ID>
		        <cbc:Name>03</cbc:Name>
		        <cbc:Percent>'.$key.'</cbc:Percent>
		        <cac:TaxScheme>
		          <cbc:ID>VAT</cbc:ID>
		        </cac:TaxScheme>
		      </cac:TaxCategory>
		    </cac:TaxSubtotal>';
    			$tax_str.=$line_xml;
    			$vat_v += $val;
    			$total_no_vat += $sub_t[$key] - $sub_disc[$key];
    			$total_vat +=$sub_t[$key] - $sub_disc[$key]+$val;
			}
		}

		$xml ='<?xml version="1.0" encoding="UTF-8"?>
			<Invoice xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2">
			  <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
			  <cbc:CustomizationID>1.0</cbc:CustomizationID>
			  <cbc:ProfileID>FFF.BE - Akti 2.0</cbc:ProfileID>
			  <cbc:ID>'.$invoice->f('serial_number').'</cbc:ID>
			  <cbc:IssueDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</cbc:IssueDate>
			  <cbc:InvoiceTypeCode listID="UNCL1001">'.$invoice_type.'</cbc:InvoiceTypeCode>
			  <cbc:Note> </cbc:Note>
			  <cbc:TaxPointDate>'.(intval($invoice->f('due_date_vat')) > 86400 ? date('Y-m-d',$invoice->f('due_date_vat')) : '').'</cbc:TaxPointDate>
			  <cbc:DocumentCurrencyCode listID="ISO4217">'.$currency.'</cbc:DocumentCurrencyCode>
			  <cac:AdditionalDocumentReference>
			    <cbc:ID>Invoice-'.$invoice->f('serial_number').'</cbc:ID>
			    <cbc:DocumentType>CommercialInvoice</cbc:DocumentType>
			    <cac:Attachment>
			      <cbc:EmbeddedDocumentBinaryObject mimeCode="application/pdf"
			        >'.$str.'</cbc:EmbeddedDocumentBinaryObject>
			    </cac:Attachment>
			  </cac:AdditionalDocumentReference>
			   <cac:AccountingSupplierParty>
			    <cac:Party>
			      <cbc:EndpointID schemeID="">'.$vat_number_base.$vat_number.'</cbc:EndpointID>
			      <cac:PartyIdentification>
			        <cbc:ID schemeID="">'.$vat_number_base.$vat_number.'</cbc:ID>
			      </cac:PartyIdentification>
			      <cac:PartyName>
			        <cbc:Name>'.htmlspecialchars($ACCOUNT_COMPANY).'</cbc:Name>
			      </cac:PartyName>
			      <cac:PostalAddress>
			        <cbc:StreetName>'.$ACCOUNT_BILLING_ADDRESS.'</cbc:StreetName>
			        <cbc:CityName>'.$ACCOUNT_BILLING_CITY.'</cbc:CityName>
			        <cbc:PostalZone>'.$ACCOUNT_BILLING_ZIP.'</cbc:PostalZone>
			        <cac:Country>
			          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
			          <cbc:Name>'.get_country_name($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:Name>
			        </cac:Country>
			      </cac:PostalAddress>
			      <cac:PartyTaxScheme>
			        <cbc:CompanyID schemeID="">'.$vat_number_base.$vat_number.'</cbc:CompanyID>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:PartyTaxScheme>
			      <cac:PartyLegalEntity>
			        <cbc:CompanyID schemeID="">'.$vat_number_base.$vat_number.'</cbc:CompanyID>
			      </cac:PartyLegalEntity>
			    </cac:Party>
			  </cac:AccountingSupplierParty>
			  <cac:AccountingCustomerParty> 
			    <cbc:SupplierAssignedAccountID>'.htmlspecialchars(stripslashes($customer_reference),ENT_QUOTES | ENT_HTML5).'</cbc:SupplierAssignedAccountID>
			    <cac:Party>
			      <cbc:EndpointID schemeID="">'.$customer_vat_base.$customer_vat.'</cbc:EndpointID>
			      <cac:PartyIdentification>
			        <cbc:ID schemeID="">'.$customer_vat_base.$customer_vat.'</cbc:ID>
			      </cac:PartyIdentification>
			      <cac:PartyName>
			        <cbc:Name>'.htmlspecialchars($invoice->f('buyer_name')).'</cbc:Name>
			      </cac:PartyName>
			      <cac:PostalAddress>
			        <cbc:StreetName>'.htmlspecialchars($invoice->f('buyer_address')).'</cbc:StreetName>
			        <cbc:CityName>'.$invoice->f('buyer_city').'</cbc:CityName>
			        <cbc:PostalZone>'.$invoice->f('buyer_zip').'</cbc:PostalZone>
			        <cac:Country>
			          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($invoice->f('buyer_country_id')).'</cbc:IdentificationCode>
			          <cbc:Name></cbc:Name>
			        </cac:Country>
			      </cac:PostalAddress>
			      <cac:PartyTaxScheme>
			        <cbc:CompanyID schemeID="">'.$customer_vat_base.$customer_vat.'</cbc:CompanyID>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:PartyTaxScheme>
			      <cac:PartyLegalEntity>
			        <cbc:CompanyID schemeID="">'.$customer_vat_base.$customer_vat.'</cbc:CompanyID>
			      </cac:PartyLegalEntity>
			    </cac:Party>
			  </cac:AccountingCustomerParty>
			  <cac:PaymentMeans>
			    <cbc:PaymentMeansCode listID="UNCL4461" listName="Payment Means" listURI="http://docs.oasis-open.org/ubl/os-UBL-2.0-update/cl/gc/default/PaymentMeansCode-2.0.gc">1</cbc:PaymentMeansCode>
			    <cbc:PaymentDueDate>'.date('Y-m-d',$invoice->f('due_date')).'</cbc:PaymentDueDate>
			    <cbc:InstructionID>'.str_replace('/', '', $invoice->f('ogm')).'</cbc:InstructionID>
			    <cbc:PaymentID>'.str_replace('/', '', $invoice->f('ogm')).'</cbc:PaymentID>
			    <cac:PayeeFinancialAccount>
			        <cbc:ID schemeID="IBAN"></cbc:ID>
			        <cac:FinancialInstitutionBranch>
			          <cac:FinancialInstitution>
			            <cbc:ID schemeID="BIC"></cbc:ID>
			          </cac:FinancialInstitution>
			        </cac:FinancialInstitutionBranch>
			      </cac:PayeeFinancialAccount>
			  </cac:PaymentMeans>
			  <cac:TaxTotal>
			    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($vat_v).'</cbc:TaxAmount>
			    '.$tax_str.'
			  </cac:TaxTotal>
			  <cac:LegalMonetaryTotal>
			    <cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:LineExtensionAmount>
			    <cbc:TaxExclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:TaxExclusiveAmount>
			    <cbc:TaxInclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:TaxInclusiveAmount>
			    <cbc:PrepaidAmount currencyID="'.$currency.'">0.00</cbc:PrepaidAmount>
			    <cbc:PayableAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:PayableAmount>
			  </cac:LegalMonetaryTotal>
			  '.$xml_lines.'
			</Invoice>
			';

		if(file_put_contents(__DIR__.'/file_'.addcslashes($invoice->f('serial_number'),'/').'.xml', $xml) === false){
			msg::error(gm('File cannot be created'),'error');
			insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],"File cannot be created");
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
			}
		}
		$attach_file_name=__DIR__.'/file_'.addcslashes($invoice->f('serial_number'),'/').'.xml';
		//$headers=array('Content-Type: application/xml','X-Software-Company: '.$this->api_key);
		$headers=array('Content-Type: multipart/form-data','Authorization: Bearer '.$this->api_key);


		/*ark::loadLibraries(array('aws'));
	    $aws = new awsWrap(DATABASE_NAME);
	    $in['attach_file_name'] = 'invoice_'.$invoice->f('serial_number').'.pdf';
	    $file = $aws->getItem('invoice/invoice_'.$in['invoice_id'].'.pdf',$in['attach_file_name']);
	    if($file !== true){
	        $in['attach_file_name'] = null;
	        msg::error(gm("Generate PDF first"),"error");
	        json_out($in);
	    }*/
	        	
		//$c_data=array('file'=>'@'.$in['attach_file_name']);

		$ch = curl_init();
	    /*curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	    curl_setopt($ch, CURLOPT_USERPWD, $this->user.":".$this->pass);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $c_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, $config['cfacts_url'].'/'.$accountant.'/'.$vat_number.'/doc/sale?ref='.getGUID());*/
	    /*$headers=array('Content-Type: multipart/form-data','Authorization: Bearer '.$this->api_key);
	    $attach_file_name = $in['attach_file_name'];*/
	    $query ='mutation upload($vatnumber: String!, $filename: String!, $invoicetype: InvoiceTypeArgument!) {
             uploadFile(vatnumber: $vatnumber, filename: $filename, invoicetype: $invoicetype) { 
               uuid,
               amountOfPages
             } 
            }';
        $variables = array(
	    	'vatnumber'			=> $vat_number,
	    	'filename'			=> $attach_file_name,
			'invoicetype'		=> "SALE"
			);
        $variables = json_encode($variables);
	   
	    $data=array(
	    	'query'				=> $query,
	    	'variables'			=> $variables,
			'file'				=> new CURLFile($attach_file_name)
			//'file'				=> '@'.$attach_file_name
			);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_URL, $config['clearfacts_api_url']);

		$put = curl_exec($ch);
	 	$info = curl_getinfo($ch);
	 	if($info['http_code']>300 || $info['http_code']==0){
	 		$resp_error=json_decode($put);
	 		$in['actiune']=gm('Fail:');
	 		msg::error(' ','error');
	 		insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],"Fail");
	 	}else{
	 		$clearfacts_data=json_decode($put,true);
	 		
	 		
	 		$cfacts_id = $clearfacts_data['data']['uploadFile']['uuid'];
	 		if($cfacts_id){
	 			$this->db->query("UPDATE tblinvoice SET cfacts_id='".$cfacts_id."' WHERE id='".$in['invoice_id']."' ");
		 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', clearfacts='1' ");
		 		if(array_key_exists('invoice_to_export', $_SESSION) === true){
		 			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		 		}
			 	if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
		 		$in['actiune']=gm("Invoice exported successfully");

		 		if($in['from_acc']){
			 		$msg = '{l}Invoice was exported automatically to{endl} '.'Clearfacts ';
			 	}else{
			 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Clearfacts ';
			 	}
		 		insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

		 		if($_SESSION['main_u_id']=='0'){
					//$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");         
					$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
				}else{
					$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".$this->database_name."' AND main_user_id='0' ");
				}
				if($is_accountant){
					$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
					if($accountant_settings=='1'){
					    $acc_block_id=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_BLOCK_ID' ");
					    if($acc_block_id=='1'){
					    	$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
					    }
					}else{
						$block_id=$this->db_users->field("SELECT block_id FROM accountants WHERE account_id='".$is_accountant."' ");
						if($block_id==1){
						    $this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");
						}
					}		
				}
	 		}else{ 		
	 			$in['actiune']=$clearfacts_data['errors'][0]['message'];
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],$in['actiune']);
	 		}
	 		
	 	}
	 	unlink($attach_file_name);
	 	if($in['from_acc']){
	 		return true;
	 	}else{
	 		json_out($in);
	 	}
	}

	function exportIncFacts(&$in){

		global $config;
		if(!$this->api_key){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
			}
		}
		$user_acc=$this->db_users->field("SELECT user_id FROM users WHERE database_name='".$this->database_name."' AND active=1000");
		/*if(!$user_acc){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Accountant required to use ClearFacts'),'error');
			json_out($in);
		}*/
		$accountant=$this->db_users->field("SELECT first_name FROM users WHERE user_id='".$user_acc."' ");
		$vat_number=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		if(!$vat_number){
			$in['actiune']=gm('Fail:');
			msg::error(gm('Vat Number required to use ClearFacts'),'error');
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
			}
		}
		$vat_number=trim($vat_number," ");
		$vat_number_base=substr($vat_number,0,2);
		$vat_number=substr($vat_number,2);
		$vat_number=str_replace(" ","",$vat_number);
		$vat_number=str_replace(".","",$vat_number);
		$vat_number=str_replace(",","",$vat_number);
		$vat_number=str_replace("-","",$vat_number);
		$vat_number=str_replace("_","",$vat_number);
		$vat_number=str_replace("/","",$vat_number);
		$vat_number=str_replace("|","",$vat_number);

		$ACCOUNT_ADDRESS_INVOICE=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
		$ACCOUNT_DELIVERY_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
		$ACCOUNT_DELIVERY_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
		$ACCOUNT_DELIVERY_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
		$ACCOUNT_DELIVERY_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
		$ACCOUNT_BILL_ADDRESS=$this->db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
		$ACCOUNT_BILL_CITY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
		$ACCOUNT_BILL_ZIP=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
		$ACCOUNT_BILL_COUNTRY_ID=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");

		$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
		$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
		$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
		$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;

		$str = '';
		$is_active = $this->db->field("SELECT active FROM apps WHERE name = 'Dropbox'");
		$invoice = $this->db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");
		$invoice_type=$invoice->f('type')<2 ? '380' : '381';
	
		if($invoice->f('file_path') && $is_active==1 && $invoice->f('amazon') !=1){
			$d = new drop('invoices',null,null,false,$this->database_name);
			@mkdir('upload/'.$this->database_name.'/temp_drop_pdfs/',0775,true);
			$name = substr(urldecode($invoice->f('file_path')), strrpos(urldecode($invoice->f('file_path')), '/')+1);
			$f = fopen("upload/".$this->database_name."/temp_drop_pdfs/".$name, 'w+b');
			$d->getFile(urldecode($invoice->f('file_path')), $f);
			fclose($f);
			$content = file_get_contents("upload/".$this->database_name."/temp_drop_pdfs/".$name );
			$str = base64_encode($content);
			$in['attach_file_name'] = 'invoice_'.$invoice->f('booking_number').'.pdf';
			$file = $d->getFile(urldecode($invoice->f('file_path')), $in['attach_file_name']);
		}
		if($invoice->f('amazon') ==1){
			ark::loadCronLibraries(array('aws'));
			$aws = new awsWrap($this->database_name);
			$exist =  $aws->doesObjectExist($config['awsBucket'].$this->database_name.'/'.$invoice->f('file_name'));
			if($exist){
				$link = $aws->getLink($config['awsBucket'].$this->database_name.'/'.$invoice->f('file_name'));
				$content = file_get_contents($link);
				$str = base64_encode($content);
				$in['attach_file_name'] = 'invoice_'.$invoice->f('booking_number').'.pdf';
	    		$file = $aws->getItem($invoice->f('file_name'),$in['attach_file_name']);
			}
		}

		if($invoice->f('supplier_id')){
			$bName= $this->db->query("SELECT * FROM customers WHERE customer_id='".$invoice->f('supplier_id')."'");
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$invoice->f('supplier_id')."' AND billing='1' ");
			$supplier_vat=$bName->f('btw_nr');
			$supplier_vat=trim($supplier_vat," ");
			$supplier_vat_base=substr($supplier_vat,0,2);
			$supplier_vat=substr($supplier_vat,2);
			$supplier_vat=str_replace(" ","",$supplier_vat);
			$supplier_vat=str_replace(".","",$supplier_vat);
			$supplier_vat=str_replace(",","",$supplier_vat);
			$supplier_vat=str_replace("-","",$supplier_vat);
			$supplier_vat=str_replace("_","",$supplier_vat);
			$supplier_vat=str_replace("/","",$supplier_vat);
			$supplier_vat=str_replace("|","",$supplier_vat);
		}elseif ($invoice->f('contact_id')) {
			$bName= $this->db->query("SELECT CONCAT_WS(' ',firstname, lastname) AS name FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."'");
			$address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$invoice->f('contact_id')."' AND is_primary='1' ");
			$supplier_vat='';
			$supplier_vat_base='';
		}

		if(!$str){
			msg::error(gm('PDF required'),'error');
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
			}
		}

		$vat_arr = array();
		$sub_arr = array();
		$tax_str = '';
		$discontoo = 0;
		$disconto_total = 0;
		$xml_lines='';
		$i = 0;
		$lines = $this->db->query("SELECT * FROM tblinvoice_incomming_line WHERE invoice_id='".$in['invoice_id']."' ");
		while ($lines->next()) {
			$discontoo_line = 0;
			if($invoice->f('allow_disconto') == 1){
				$line = $lines->f('total')-$lines->f('total')*$invoice->f('disconto')/100;
				$discontoo += $lines->f('total')*$invoice->f('disconto')/100;
				$discontoo_line = $lines->f('total')*$invoice->f('disconto')/100;
			}else{
				$line = $lines->f('total');
			}
			$total_with_vat = $line + ($line/100*$lines->f('vat_line'));
			$ln_amount_vat=$lines->f('vat_line')*$line/100;
			$disconto_total +=$discontoo_line + $total_with_vat;
			$vat_arr[$lines->f('vat_line')] += $lines->f('vat_line')*$line/100;
			$sub_arr[$lines->f('vat_line')] += $line;
			$i++;
			$line_xml = '
			  <cac:InvoiceLine>
				<cbc:ID>'.$i.'</cbc:ID>
				<cbc:InvoicedQuantity unitCode="C62" unitCodeListID="UNECERec20">1.00</cbc:InvoicedQuantity>
				<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($discontoo_line + $total_with_vat).'</cbc:LineExtensionAmount>
				<cbc:AccountingCost></cbc:AccountingCost>
				<cac:TaxTotal>
				  <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount_vat).'</cbc:TaxAmount>
				  <cac:TaxSubtotal>
				    <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($discontoo_line + $total_with_vat).'</cbc:TaxableAmount>
				    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount_vat).'</cbc:TaxAmount>
				    <cac:TaxCategory>
				      <cbc:ID schemeID="UNCL5305">S</cbc:ID>
				      <cbc:Name>03</cbc:Name>
				      <cbc:Percent>'.twoDecNumber($lines->f('vat_line')).'</cbc:Percent>
				      <cac:TaxScheme>
			            <cbc:ID>VAT</cbc:ID>
			          </cac:TaxScheme>
				    </cac:TaxCategory>
				  </cac:TaxSubtotal>
				</cac:TaxTotal>
				<cac:Item>
				  <cbc:Description>Invoice Line</cbc:Description>
				  <cac:ClassifiedTaxCategory>
			        <cbc:ID schemeID="UNCL5305">S</cbc:ID>
			        <cbc:Name>03</cbc:Name>
			        <cbc:Percent>'.twoDecNumber($lines->f('vat_line')).'</cbc:Percent>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:ClassifiedTaxCategory>	        
				</cac:Item>
				<cac:Price>
			      <cbc:PriceAmount currencyID="EUR">'.twoDecNumber($discontoo_line + $total_with_vat).'</cbc:PriceAmount>
			    </cac:Price>         
		       </cac:InvoiceLine>';
	       		$xml_lines.=$line_xml;
    	}
    	foreach ($vat_arr as $key => $val) {
    		$line_xml='<cac:TaxSubtotal>
		      <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($sub_arr[$key]).'</cbc:TaxableAmount>
		      <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:TaxAmount>     
		      <cac:TaxCategory>
		        <cbc:ID schemeID="UNCL5305">S</cbc:ID>
		        <cbc:Name>03</cbc:Name>
		        <cbc:Percent>'.$key.'</cbc:Percent>
		        <cac:TaxScheme>
		          <cbc:ID>VAT</cbc:ID>
		        </cac:TaxScheme>
		      </cac:TaxCategory>
		    </cac:TaxSubtotal>';
    			$tax_str.=$line_xml;
    	}
    	$disconto_total = number_format($disconto_total,2);

		$currency = currency::get_currency(1,'code');
		$xml ='<?xml version="1.0" encoding="UTF-8"?>
			<Invoice xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2">
			  <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
			  <cbc:CustomizationID>1.0</cbc:CustomizationID>
			  <cbc:ProfileID>FFF.BE - Logistics 6.0</cbc:ProfileID>
			  <cbc:ID>'.$invoice->f('invoice_number').'</cbc:ID>
			  <cbc:IssueDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</cbc:IssueDate>
			  <cbc:InvoiceTypeCode listID="UNCL1001">'.$invoice_type.'</cbc:InvoiceTypeCode>
			  <cbc:Note> </cbc:Note>
			  <cbc:TaxPointDate></cbc:TaxPointDate>
			  <cbc:DocumentCurrencyCode listID="ISO4217"
			    >'.$currency.'</cbc:DocumentCurrencyCode>
			  <cac:AdditionalDocumentReference>
			    <cbc:ID>Invoice-'.$invoice->f('invoice_number').'</cbc:ID>
			    <cbc:DocumentType>CommercialInvoice</cbc:DocumentType>
			    <cac:Attachment>
			      <cbc:EmbeddedDocumentBinaryObject mimeCode="application/pdf"
			        >'.$str.'</cbc:EmbeddedDocumentBinaryObject>
			    </cac:Attachment>
			  </cac:AdditionalDocumentReference>
			   <cac:AccountingSupplierParty>
			    <cac:Party>
			      <cbc:EndpointID schemeID="">'.$supplier_vat_base.$supplier_vat.'</cbc:EndpointID>
			      <cac:PartyIdentification>
			        <cbc:ID schemeID="">'.$supplier_vat_base.$supplier_vat.'</cbc:ID>
			      </cac:PartyIdentification>
			      <cac:PartyName>
			        <cbc:Name>'.htmlspecialchars($bName->f('name')).'</cbc:Name>
			      </cac:PartyName>
			      <cac:PostalAddress>
			        <cbc:StreetName>'.$address->f('address').'</cbc:StreetName>
			        <cbc:CityName>'.$address->f('city').'</cbc:CityName>
			        <cbc:PostalZone>'.$address->f('zip').'</cbc:PostalZone>
			        <cac:Country>
			          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($address->f('country_id')).'</cbc:IdentificationCode>
			        </cac:Country>
			      </cac:PostalAddress>
			      <cac:PartyTaxScheme>
			        <cbc:CompanyID schemeID="">'.$supplier_vat_base.$supplier_vat.'</cbc:CompanyID>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:PartyTaxScheme>
			      <cac:PartyLegalEntity>
			        <cbc:CompanyID schemeID="">'.$supplier_vat_base.$supplier_vat.'</cbc:CompanyID>
			      </cac:PartyLegalEntity>
			    </cac:Party>
			  </cac:AccountingSupplierParty>
			  <cac:AccountingCustomerParty>
			    <cac:Party>
			      <cbc:EndpointID schemeID="">'.$vat_number_base.$vat_number.'</cbc:EndpointID>
			      <cac:PartyIdentification>
			        <cbc:ID schemeID="">'.$vat_number_base.$vat_number.'</cbc:ID>
			      </cac:PartyIdentification>
			      <cac:PartyName>
			        <cbc:Name>'.htmlspecialchars($ACCOUNT_COMPANY).'</cbc:Name>
			      </cac:PartyName>
			      <cac:PostalAddress>
			        <cbc:StreetName>'.$ACCOUNT_BILLING_ADDRESS.'</cbc:StreetName>
			        <cbc:CityName>'.$ACCOUNT_BILLING_CITY.'</cbc:CityName>
			        <cbc:PostalZone>'.$ACCOUNT_BILLING_ZIP.'</cbc:PostalZone>
			        <cac:Country>
			          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
			        </cac:Country>
			      </cac:PostalAddress>
			      <cac:PartyTaxScheme>
			        <cbc:CompanyID schemeID="">'.$vat_number_base.$vat_number.'</cbc:CompanyID>
			        <cac:TaxScheme>
			          <cbc:ID>VAT</cbc:ID>
			        </cac:TaxScheme>
			      </cac:PartyTaxScheme>
			       <cac:PartyLegalEntity>
			        <cbc:CompanyID schemeID="">'.$vat_number_base.$vat_number.'</cbc:CompanyID>
			      </cac:PartyLegalEntity>
			    </cac:Party>
			  </cac:AccountingCustomerParty>
			  <cac:PaymentMeans>
			    <cbc:PaymentMeansCode listID="UNCL4461" listName="Payment Means" listURI="http://docs.oasis-open.org/ubl/os-UBL-2.0-update/cl/gc/default/PaymentMeansCode-2.0.gc">1</cbc:PaymentMeansCode>
			    <cbc:PaymentDueDate>'.date('Y-m-d',$invoice->f('due_date')).'</cbc:PaymentDueDate>
			    <cbc:InstructionID></cbc:InstructionID>
			    <cbc:PaymentID></cbc:PaymentID>
			    <cac:PayeeFinancialAccount>
			      <cbc:ID schemeID="IBAN">'.$invoice->f('buyer_iban').'</cbc:ID>
					<cac:FinancialInstitutionBranch>
						<cac:FinancialInstitution>
							<cbc:ID schemeID="BIC">'.$invoice->f('buyer_bic').'</cbc:ID>
						</cac:FinancialInstitution>
					</cac:FinancialInstitutionBranch>
			    </cac:PayeeFinancialAccount>
			  </cac:PaymentMeans>
			  <cac:PaymentTerms>
				<cbc:SettlementDiscountPercent>'.$invoice->f('disconto').'</cbc:SettlementDiscountPercent>
			  </cac:PaymentTerms>
			  <cac:TaxTotal>
				<cbc:TaxAmount currencyID="EUR">'.twoDecNumber($invoice->f('total_with_vat')-$invoice->f('total')).'</cbc:TaxAmount>
				'.$tax_str.'
			  </cac:TaxTotal>
			  <cac:LegalMonetaryTotal>
			    <cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:LineExtensionAmount>
			    <cbc:TaxExclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($invoice->f('total')).'</cbc:TaxExclusiveAmount>
			    <cbc:TaxInclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:TaxInclusiveAmount>
			    <cbc:PrepaidAmount currencyID="'.$currency.'">0.00</cbc:PrepaidAmount>
			    <cbc:PayableAmount currencyID="'.$currency.'">'.twoDecNumber($disconto_total).'</cbc:PayableAmount>
			  </cac:LegalMonetaryTotal>
			  '.$xml_lines.'
			</Invoice>
			';
		$e = str_replace('/', '', $invoice->f('booking_number'));
		if(file_put_contents(__DIR__.'/file_inc_'.$e.'.xml', $xml) === false){
			msg::error(gm('File cannot be created'),'error');
			if($in['from_acc']){
				return true;
			}else{
				json_out($in);
				return false;
			}
		}

		$attach_file_name=__DIR__.'/file_inc_'.$e.'.xml';
		/*$headers=array('Content-Type: application/xml','X-Software-Company: '.$this->api_key);
		$c_data=array('file'=>'@'.$attach_file_name);
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	    curl_setopt($ch, CURLOPT_USERPWD, $this->user.":".$this->pass);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $c_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, $config['cfacts_url'].'/'.$accountant.'/'.$vat_number.'/doc/purchase?ref='.getGUID());

		$put = curl_exec($ch);
	 	$info = curl_getinfo($ch);
	 	if($info['http_code']>300 || $info['http_code']==0){
	 		$resp_error=json_decode($put);
	 		$in['actiune']=gm('Fail:');
	 	}else{
	 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', clearfacts='1',type='1' ");
		 	$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
	 		$in['actiune']=gm("Invoice exported successfully");
	 	}
	 	unlink($attach_file_name);*/

	 	/*ark::loadLibraries(array('aws'));
	    $aws = new awsWrap(DATABASE_NAME);
	    $in['attach_file_name'] = 'invoice_'.$invoice->f('serial_number').'.pdf';
	    $file = $aws->getItem('invoice/invoice_'.$in['invoice_id'].'.pdf',$in['attach_file_name']);
	    if($file !== true){
	        $in['attach_file_name'] = null;
	        msg::error(gm("Generate PDF first"),"error");
	        json_out($in);
	    }*/
	    
	    $ch = curl_init();
	    $headers=array('Content-Type: multipart/form-data','Authorization: Bearer '.$this->api_key);
	    //$attach_file_name = $in['attach_file_name'];
	    $query ='mutation upload($vatnumber: String!, $filename: String!, $invoicetype: InvoiceTypeArgument!) {
             uploadFile(vatnumber: $vatnumber, filename: $filename, invoicetype: $invoicetype) { 
               uuid,
               amountOfPages
             } 
            }';
        $variables = array(
	    	'vatnumber'			=> $vat_number,
	    	'filename'			=> $attach_file_name,
			'invoicetype'		=> "PURCHASE"
			);
        $variables = json_encode($variables);
	   
	    $data=array(
	    	'query'				=> $query,
	    	'variables'			=> $variables,
			'file'				=> new CURLFile($attach_file_name)
			//'file'				=> '@'.$attach_file_name
			);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_URL, $config['clearfacts_api_url']);


		$put = curl_exec($ch);
	 	$info = curl_getinfo($ch);
	 	if($info['http_code']>300 || $info['http_code']==0){
	 		$resp_error=json_decode($put);
	 		$in['actiune']=gm('Fail:');
	 		msg::error(' ','error');
	 		insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],"Fail");
	 	}else{
	 		$clearfacts_data=json_decode($put,true);
	 		
	 		$cfacts_id = $clearfacts_data['data']['uploadFile']['uuid'];
	 		if($cfacts_id){
	 			$this->db->query("UPDATE tblinvoice_incomming SET cfacts_id='".$cfacts_id."' WHERE invoice_id='".$in['invoice_id']."' ");
		 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', clearfacts='1',type='1' ");
		 		if(array_key_exists('invoice_to_export', $_SESSION) === true){
		 			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		 		}
			 	if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
		 		$in['actiune']=gm("Invoice exported successfully");
		 		if($in['from_acc']){
			 		$msg = '{l}Invoice was exported automatically to{endl} '.'Clearfacts ';
			 	}else{
			 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Clearfacts ';
			 	}
				insert_message_log('purchase_invoice',$msg,'purchase_invoice_id',$in['invoice_id']);
	 		}else{ 		
	 			$in['actiune']=$clearfacts_data['errors'][0]['message'];
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"clearfacts","invoice",$in['invoice_id'],$in['actiune']);
	 		}
	 		
	 	}
	 	unlink($attach_file_name);
	 	if($in['from_acc']){
	 		return true;
	 	}else{
	 		json_out($in);
	 	}
	}

}
?>