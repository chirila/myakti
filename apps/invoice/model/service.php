<?php
/**
 * Service class
 *
 * @author      Arkweb SRL
 * @link        https://app.akti.com/
 * @copyright   [description]
 * @package 	Service
 */
class service
{
	var $pag = 'service';
	var $field_n = 'article_id';

	/**
	 * [article description]
	 * @return [type] [description]
	 */
	function service()
	{
		$this->db = new sqldb;
	}

	/**
	 * Add service
	 * @param array $in
	 * @return boolean
	 */
	function add(&$in)
	{
	    if(!$this->add_validate($in)){
			return false;
		}
		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
															item_code  							= '".$in['item_code']."',
															ean_code  							= '".$in['ean_code']."',
															
															use_percent_default  				= '".$in['use_percent_default']."',
															supplier_reference      = '".$in['supplier_reference']."',
															vat_id									= '".$in['vat_id']."',
															article_brand_id			= '".$in['article_brand_id']."',
															ledger_account_id			= '".$in['ledger_account_id']."',
															article_category_id			= '".$in['article_category_id']."',
															origin_number   			= '".$in['origin_number']."',
														
															sale_unit        			= '".$in['sale_unit']."',
															internal_name       		= '".$in['internal_name']."',
															packing          			= '".return_value2($in['packing'])."',
															price_type          		= '".$in['price_type']."',
														
															weight    					= '".$in['weight']."',
															show_img_q								= '".$in['show_img_q']."',
															supplier_id								= '".$in['supplier_id']."',
															supplier_name							= '".$in['supplier_name']."',
															aac_price			        = '".return_value($in['aac_price'])."',
															is_service			        = '1',
															billable			        = '".$in['billable']."',
															created_at='".time()."'
															
															");
		$this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".return_value($in['purchase_price'])."'
						");
       update_articles($in['article_id'],DATABASE_NAME);
		msg::success(gm("Service succefully added."),'success');
       return true;
	}

	/**
	 * Validate add article
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('item_code', 'Item Code', 'required:unique[pim_articles.item_code]');
		$v -> f('internal_name', 'Internal name', 'required');	
		return $v -> run();
	}

	

}	
