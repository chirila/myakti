<?php
/************************************************************************
* @Author: MedeeaWeb Works
***********************************************************************/
class jefacture
{
	var $db;
	var $arUrl;
	var $database_name;
	var $user;

	function jefacture(&$in,$dbase='')
	{
		global $config,$database_config;
		if($dbase){
			$database_1 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $dbase,
			);
			$this->db = new sqldb($database_1);
			$this->database_name=$dbase;
		}else{
			$this->db = new sqldb();
			$this->database_name=DATABASE_NAME;
		}		
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($database_2); // recurring billing
		$app_data=$this->db->query("SELECT * FROM apps WHERE name='Jefacture' AND type='main' AND app_type='accountancy'");
		$this->api_key = $app_data->f('api');
		$this->user = $config['jefacture_user'];
	}

	function markExport(&$in)
	{
		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', type='".$in['type']."', ".$in['app']."=1 ");
		insert_message_log('invoice','{l}Invoice was exported by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		return true;
	}

	function unmarkExport(&$in)
	{
		$this->db->query("DELETE FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND ".$in['app']."=1 AND type='".$in['type']."' ");
		insert_message_log('invoice','{l}Invoice was put back on export on list by{endl} '.get_user_name($_SESSION['u_id']),'invoice_id',$in['invoice_id']);
		return true;
	}

	function generate_pdf(&$in)
	  {
	    if($in['type_pdf']==2){
	      include(__DIR__.'/../controller/invoice_credit_print.php');
	    }else{
	      include(__DIR__.'/../controller/invoice_print.php');
	    }
	    return $str;
	  }

	function formatVAT($vat)
	{
		$vat = str_replace(array('.',' ','-','_'), '', $vat);
		return $vat;
	}

	function exportJefacture(&$in){
		global $config;
		if(!$this->api_key){
	 		$in['actiune']=gm('Fail:');
			msg::error(gm('Invalid credentials'),'error');
			insert_error_call($this->database_name,"jefacture","invoice",$in['invoice_id'],"Invalid credentials");
			if($in['from_acc'] || $in['return_data']){
				return true;
			}else{
				json_out($in);	
			}
		}

		$invoice = $this->db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
		$ACCOUNT_VAT_NUMBER=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		$ACCOUNT_COMPANY=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");
		$params = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'dbase'=>$this->database_name);
		if($invoice->f('type')=='2'){
			if($invoice->f('pdf_layout')){
				$params['type']=$invoice->f('pdf_layout');
				$params['logo']=$invoice->f('pdf_logo');
			}else{
				$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			#if we are using a customer pdf template
			if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
				$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			$params['type_pdf']=2;
			$params['type']=ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
			$in['efff_str'] = $this->generate_pdf($params);
		}else{
			if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==0){
				$params['type']=$invoice->f('pdf_layout');
				$params['logo']=$invoice->f('pdf_logo');
			}else if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==1){
				$params['custom_type']=$invoice->f('pdf_layout');
				$params['logo']=$invoice->f('pdf_logo');
			}else{
				$params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			#if we are using a customer pdf template
			if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0){
				$params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
			}
			$in['efff_str'] = $this->generate_pdf($params);
		}
		$in['billtobox_xml']=true;
		$in['dbase']=$this->database_name;
		$in['id']=$in['invoice_id'];
		include(__DIR__.'/../controller/xml_invoice_print.php');
      	$xml_tmp_file_name=__DIR__."/../../../file_".$invoice->f('serial_number').".xml";
      	
      	$jefacture_base_url = $config['jefacture_base_url'].'api/universal_connector/v1/uc/ar';
		$data=array(
			'file'				=> new CURLFile($xml_tmp_file_name,'application/xml')
		);

		$ch = curl_init();
    	$headers=array('Content-Type: multipart/form-data');
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $this->user.":".$this->api_key);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_URL, $jefacture_base_url);

		$put = curl_exec($ch);
	 	$info = curl_getinfo($ch);

	 	if($info['http_code']>300 || $info['http_code']==0){
	 		$resp_error=json_decode($put, true);
	 		$in['actiune']=gm('Fail:');
			msg::error($resp_error['errors'],'error');
	 		insert_error_call($this->database_name,"jefacture","invoice",$in['invoice_id'],$in['actiune'].$resp_error['errors']);
	 	}else{
	 		$jefacture_data=json_decode($put,true); 		
	 		
	 		$jefacture_id = $jefacture_data['item']['up_doc_ref'];
	 		if($jefacture_id){
		 		$this->db->query("INSERT INTO tblinvoice_exported SET invoice_id='".$in['invoice_id']."', jefacture='1',type=0 ");
		 		if(array_key_exists('invoice_to_export', $_SESSION) === true){
		 			$this->db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$in['invoice_id']."' AND time='".$_SESSION['invoice_to_export']."' ");
		 		}
			 	if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($in['invoice_id'], $_SESSION['tmp_export_add_to_app']) === true){
		          unset($_SESSION['tmp_export_add_to_app'][$in['invoice_id']]);
		        }
		 		$in['actiune']=gm("Invoice exported successfully");

		 		if($in['from_acc']){
			 		$msg = '{l}Invoice was exported automatically to{endl} '.'Jefacture ';
			 	}else{
			 		$msg = '{l}Invoice was exported manually by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} '.'Jefacture ';
			 	}
		 		
		 		insert_message_log('invoice',$msg,'invoice_id',$in['invoice_id']);

				$this->db->query("UPDATE tblinvoice SET block_edit='1' WHERE id='".$in['invoice_id']."' ");

				$check_if_sent=$this->db->field("SELECT `sent` FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
				if(!$check_if_sent){
        			$this->db->query("UPDATE tblinvoice SET sent='1',sent_date='".time()."' WHERE id='".$in['invoice_id']."'");
				}
	 		}else{ 		
	 			$in['actiune']=$jefacture_data['errors'];
	 			msg::error(' ','error');
	 			insert_error_call($this->database_name,"jefacture","invoice",$in['invoice_id'],$in['actiune']);
	 		}
	 		
	 	}
	 	unlink($xml_tmp_file_name);
	 	if($in['from_acc'] || $in['return_data']){
	 		return true;
	 	}else{
	 		json_out($in);
	 	}
	}

	

}//end class
?>

