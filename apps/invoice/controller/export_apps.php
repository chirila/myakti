<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
global $database_config;
$database = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );
$dbu_users =  new sqldb($database);
$accountant_prefs=array('1'=>'yuki','2'=>'codabox');
$accountancy_id='0';
if($_SESSION['main_u_id']=='0'){
    //$is_accountant=$dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");         
    $is_accountant=$dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
}else{
    $is_accountant=$dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
}
if($is_accountant){
	$accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
	if($accountant_settings=='1'){
		$acc_app_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_APP_ID' ");
		if($acc_app_id){
			$accountancy_id=$acc_app_id;
		}
	}else{
		$accountancy_id=$dbu_users->field("SELECT accountancy_id FROM accountants WHERE account_id='".$is_accountant."' ");
	}   
}
$result=array();
$show_for_one_user = true;
if(defined('PAYMILL_CLIENT_ID') && (defined('HOW_MANY') && HOW_MANY == 0) ) {
    //$show_for_one_user = false;
}
$filter_coda="";
$codabox_app=$db->query("SELECT app_id,active FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
$codabox_sales=$db->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$codabox_app->f('app_id')."' ");
if($codabox_app->f('active') && !is_null($codabox_sales) && $codabox_sales!='1'){
	$filter_coda=" AND template_file!='codabox' ";
}
$app_active=$db->field("SELECT COUNT(template_file) FROM apps WHERE main_app_id='0' AND type='main' AND app_type='accountancy' AND active>'0'  AND template_file!='syneton' ".$filter_coda." ");
if($app_active){
	$templates_active=array();
	$templates=$db->query("SELECT template_file FROM apps WHERE main_app_id='0' AND type='main' AND app_type='accountancy' AND active>'0' AND template_file!='syneton'  ".$filter_coda." ");
	while($templates->next()){
		array_push($templates_active,$templates->f('template_file'));
	}
}
$csv_allowed=array('9866660a_b374_59ab_779505722e8e','0561881e_90e4_ed68_616f5472328b', '63f19359_3a24_75fc_05d58bfbc037', 'c76f5025_7c04_19f0_fa5008a84d8a', 'salesassist_2');
$accepted_countries=array(26,159,79);
$is_easy_invoice_diy=aktiUser::get('is_easy_invoice_diy');
$is_easy_invoice_billtobox=aktiUser::get('is_easy_invoice_billtobox');
if($show_for_one_user===true){
	$result['accountancy']=array();
	$accountancy_apps = $db->query("SELECT * FROM apps WHERE main_app_id='0' AND type='main' AND app_type='accountancy' ");
	$activated=0;
	while ($accountancy_apps->next()) {
		//if($accountancy_apps->f('name') == 'Exact online' || $accountancy_apps->f('name') == 'Syneton' ){
		if($accountancy_apps->f('name') == 'Syneton' ){
			continue;
		}
		if($accountancy_apps->f('name')=='CodaBox' && $codabox_app->f('active') && !is_null($codabox_sales) && $codabox_sales!='1'){
			continue;
		}
		//AKTI-4658
		if($accountancy_apps->f('name') == 'CodaBox') {
		    continue;
        }
		if($accountancy_id !='0' && $accountancy_apps->f('template_file')!=$accountant_prefs[$accountancy_id] && !$app_active){
			continue;
		}
		if(!in_array(ACCOUNT_DELIVERY_COUNTRY_ID, $accepted_countries)){
			continue;
		}
		if($app_active && !in_array($accountancy_apps->f('template_file'), $templates_active)){
			continue;
		}
		$view_beta = false;
		$accountancy=array(
			'CLASS2'			=> $accountancy_apps->f('template_file'),
			'app'				=> $accountancy_apps->f('template_file')=='btb' ? 'winbooks' : $accountancy_apps->f('template_file'),
			'view_beta'			=> $view_beta,
			'activated'			=> $accountancy_apps->f('active') ? true : false,
		);
		if($is_easy_invoice_diy && $accountancy_apps->f('template_file')!='jefacture'){
			continue;
		}
        if($is_easy_invoice_billtobox && $accountancy_apps->f('template_file')!='billtobox'){
            continue;
        }
		array_push($result['accountancy'], $accountancy);
		if($accountancy_apps->f('active')){
			$activated++;
		}
	}
	$result['can_activate']	=$_SESSION['group']=='admin' ? true : false;
	if(in_array(DATABASE_NAME,$csv_allowed)){
		$result['preferred_acc']='';
		$result['activated']=0;
	}else{
		$result['preferred_acc']=$app_active==1 ? ($result['accountancy'][0]['CLASS2']=='btb' ? 'winbooks' : $result['accountancy'][0]['CLASS2']): ($accountancy_id !='0'? $accountant_prefs[$accountancy_id] : '');
		if($is_easy_invoice_diy){
			$result['preferred_acc']='jefacture';
		}
		$result['activated']=$activated;
	}
}

$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
if($easyinvoice=='1'){
	$result['show_deliveries']	 	=false;
	$result['show_projects']		= false;
	$result['show_interventions']	= false;
}else{
	$result['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
	$result['show_projects']		= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
	$result['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
} 

return json_out($result);
?>