<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 
global $config;

$db =  new sqldb();

$in['comp_name']=ACCOUNT_COMPANY;
$now = time();
$in['export_date']=date('Ymd',$now);
$in['export_date_zip']=date('YmdHis',$now);
$name_type = '';
/*if($in['doc_type'] =='2' || $in['doc_type'] =='4'){
  $name_type = ' - CN';
}*/
/*$in['filename'] ="Netsuiteexport - ".$in['comp_name']." - ".$in['export_date'].$name_type.".csv";*/
$in['filename'] ="Netsuiteexport - ".$in['comp_name']." - ".$in['export_date'].".csv";
$in['filename_cn'] ="Netsuiteexport - ".$in['comp_name']." - ".$in['export_date'].' - CN'.".csv";
$in['filename_zip'] ="Netsuiteexport - ".$in['comp_name']." - ".$in['export_date'].".zip";

ini_set('memory_limit', '1000M');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.$in['filename'].'";');



/*$f = fopen('php://output', 'w');*/
 $from_location='upload/'.DATABASE_NAME.'/'.$in['filename'];
 $f = fopen($from_location, 'w');

 ini_set('memory_limit', '1000M');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.$in['filename_cn'].'";');

 $from_location_c='upload/'.DATABASE_NAME.'/'.$in['filename_cn'];
 $fc = fopen($from_location_c, 'w');

$headers = array("ExternalID",
        "NetsuiteCustomerID",                
        "NetsuiteEndCustomerID",
        "InvoiceDate",
        "Quantity",
        "UnitPrice",
        "PurchaseStartDate",
        "PurchaseEndDate",
        "Item",
        "ItemDescription",
        "PONumber"    
);

$final_data = array();
$final_data_c = array();

$netsuite_field_id = $db->field("SELECT field_id FROM `customer_fields` WHERE `label` = 'Netsuite ID' ");

$in['invoice_list'] = '';

if(array_key_exists('invoice_to_export', $_SESSION) === true){
    $invoices_to_export = $db->query("SELECT invoice_id FROM tblinvoice_to_export WHERE  time='".$_SESSION['invoice_to_export']."' ");
    while($invoices_to_export->next()){
      //array_push($invoices, $invoices_to_export->f('invoice_id'));
      $in['invoice_list'] .= $invoices_to_export->f('invoice_id').',';
      $value = $invoices_to_export->f('invoice_id');
      if(array_key_exists('invoice_to_export', $_SESSION) === true){
       // $db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$value."' AND time='".$_SESSION['invoice_to_export']."' ");
      }
      if(array_key_exists('tmp_export_add_to_app',$_SESSION) === true && array_key_exists($value, $_SESSION['tmp_export_add_to_app']) === true){
            unset($_SESSION['tmp_export_add_to_app'][$value]);
          }
    }
  }

$in['invoice_list'] = rtrim($in['invoice_list'], ',');

if($in['invoice_list']){
        $info = $db->query("SELECT  cast( FROM_UNIXTIME( tblinvoice.`invoice_date` ) AS DATE ) AS t_date, tblinvoice.serial_number AS iii,tblinvoice.buyer_id,tblinvoice.id,tblinvoice.type,tblinvoice.invoice_date,tblinvoice_line.quantity, tblinvoice_line.price, tblinvoice_line.discount, tblinvoice_line.item_code,tblinvoice_line.name, tblinvoice_line.content, tracking_line.origin_id
                        FROM tblinvoice
                        LEFT JOIN tracking_line ON  tblinvoice.trace_id  =   tracking_line.trace_id   AND tracking_line.origin_type = '13'  
                        LEFT JOIN tblinvoice_line ON tblinvoice.id = tblinvoice_line.invoice_id AND tblinvoice_line.content_title = '0'
                        WHERE tblinvoice.id IN (".$in['invoice_list'].")
                        ORDER BY t_date DESC, iii DESC, sort_order ASC ");

    while ($info->next()){  


      $netsuite_customer_id =  $db->field("SELECT value FROM customer_field WHERE customer_id='".$info->f('buyer_id')."' AND field_id='".$netsuite_field_id."' ");
      $date_invoice = date('d/m/Y',  $info->f('invoice_date'));

      $purchase_end_date ='';
      if($info->f('origin_id')){
        $rec_info = $db->query("SELECT recurring_invoice.start_date,recurring_invoice.frequency,recurring_invoice.next_date
                          FROM recurring_invoice     
                          WHERE recurring_invoice.recurring_invoice_id ='".$info->f('origin_id')."' ");

          $m = date('n',$info->f('invoice_date'));
            $d = date('j',$info->f('invoice_date'));
            $y = date('Y',$info->f('invoice_date'));
            $start_date = $info->f('invoice_date');

          switch($rec_info->f('frequency')){
              case 1:  
                  $purchase_end_date= $start_date + (6 * 24 * 60 * 60);
                  break; //week
              case 2:
                  $purchase_end_date= mktime(0, 0, 0, $m+1, $d-1, $y);
              break; //month
              case 3:
                  $purchase_end_date= mktime(0, 0, 0, $m+3, $d-1, $y);
              break; //3 month
              case 4:  
                   $purchase_end_date= mktime(0, 0, 0, $m, $d-1, $y+1); 
          break; //year
              case 5: 
                   $purchase_end_date= mktime(0, 0, 0, $m, $d+$in['days']-2, $y);
               break; //days;
          }

        }

      $purchase_end_date = date('d/m/Y',  $purchase_end_date);

      $description = $info->f('content')? htmlspecialchars(strip_tags(html_entity_decode(stripslashes($info->f('name')), ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8') : preg_replace("/[\n\r]/","",$info->f('name')) ;

      /*$quantity = ( $info->f('quantity')>0 && ($info->f('type') =='2' || $info->f('type') =='4') )? $info->f('quantity') : (-1)*$info->f('quantity');
      $price = $info->f('price') * (1-$info->f('discount')/100) ;
      $price = ( $price>0 && ($info->f('type') =='2' || $info->f('type') =='4') )? $price : (-1)*$price;*/

      $quantity = $info->f('type') =='2' || $info->f('type') =='4' ? abs($info->f('quantity')) : $info->f('quantity');
      $price = $info->f('type') =='2' || $info->f('type') =='4' ? abs($info->f('price')) : $info->f('price');
      $price=$price*1;

      $item=array(
          $info->f('iii'),
          $netsuite_customer_id,
          '',
          $date_invoice,
          ($info->f('content')? '': $quantity),
          ($info->f('content')? '':  $price ),
          ($info->f('content')? '': $date_invoice),
          ($info->f('content')? '': $purchase_end_date),
          ($info->f('content')? 'DESCRIPTION': preg_replace("/[\n\r]/","",$info->f('item_code')) ),
          $description,
          ''
        );

      
        if($info->f('type') =='2' || $info->f('type') =='4'){
          array_push($final_data_c,$item);
        }else{
          array_push($final_data,$item);
        }
    }
}

//var_dump($final_data, ); exit();


if($f){
	fputs($f, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
	fputcsv($f, $headers,";");

	foreach ($final_data as $line) {
        fputcsv($f, $line, ";");
    }
}

fclose($f);

if($fc){
  fputs($fc, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
  fputcsv($fc, $headers,";");

  foreach ($final_data_c as $line) {
        fputcsv($fc, $line, ";");
    }
}

fclose($fc);

$zip = new ZipArchive();


   
if($zip->open($in['filename_zip'], ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)===TRUE)
 {      
     if (file_exists($from_location)) {
        $content = file_get_contents($from_location);
        $zip->addFromString($in['filename'],$content);
    } 
    if (file_exists($from_location_c)) {
        $content_c = file_get_contents($from_location_c);
        $zip->addFromString($in['filename_cn'],$content_c);
    } 

    $zip->close();

    header('Content-type: application/zip');
    header('Content-Disposition: attachment; filename="'.$in['filename_zip'].'";');
    header('Content-Length: ' . filesize($in['filename_zip']));

    readfile($in['filename_zip']);
    unlink($in['filename_zip']);
    //unlink($in['filename']);
    //unlink($in['filename_cn']);
    

 }

exit();
?>