<?php
/* * **********************************************************************
 * @Author: MedeeaWeb Works                                              *
 * ********************************************************************** */
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

$result=array('logo'=>array());
$tblinvoice = $db->query("SELECT pdf_logo, pdf_layout,use_custom_template, identity_id,email_language,type FROM tblinvoice WHERE id='".$in['invoice_id']."'");
$identity = $db->field("SELECT value FROM settings WHERE constant_name='INVOICE_IDENTITY_SET'");
$tblinvoice->move_next();

if(ACCOUNT_LOGO == '')   {
    /*$view->assign('account_logo','images/no-logo.png','logo');
    $view->assign('default','CHECKED','logo');
    $view->loop('logo');*/
    array_push($result['logo'], array(
            'account_logo'=>'images/no-logo.png',
            'default'=>'images/no-logo.png'
        ));
}
else {
    $logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/{logo_img_}*',GLOB_BRACE);
    foreach ($logos as $v) {
        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
        $size = getimagesize($logo);
        $ratio = 250 / 77;
        if($size[0]/$size[1] > $ratio ){
            $view->assign('attr', 'width="250"','logo');
        }else{
            $view->assign('attr', 'height="77"','logo');
        }
         array_push($result['logo'], array(
                'account_logo'=>$logo,
        ));
       /* $view->assign(array(
            'account_logo'  => $logo,
            'default'       => $logo == $tblinvoice->f('pdf_logo') ? 'CHECKED' : ''
        ),'logo');
        $view->loop('logo');*/
    }
}
// echo ($tblinvoice->f('identity_id'));
// exit();

if(!$tblinvoice->f('pdf_layout')) {
  $selected=ACCOUNT_INVOICE_PDF_FORMAT ? ACCOUNT_INVOICE_PDF_FORMAT : '1';
}else{
 $selected=$tblinvoice->f('pdf_layout');
 if($tblinvoice->f('use_custom_template')==1){
    $selected=$tblinvoice->f('pdf_layout')+7;
 }
}
if(!$tblinvoice->f('identity_id')) {
  $selected_id='0';
}else{
 $selected_id=$tblinvoice->f('identity_id');
}
if($identity){
    $selected_id=$identity;
}
/*$view->assign(array('pdf_type_dd'       => build_pdf_type_dd($selected,true),
                    'multiple_identity' => build_identity_dd($selected_id),
                    'identity_id'       => $tblinvoice->f('identity_id'),

                ));
*/
$link_end = '&type='.ACCOUNT_INVOICE_HEADER_PDF_FORMAT;
$link_credit_end = '&type='.ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;

$result['pdf_link']= ($tblinvoice->f('type')==2 || $tblinvoice->f('type')==4) ?'index.php?do=invoice-invoice_credit_print&id='.$in['invoice_id'].'&lid='.$tblinvoice->f('email_language').$link_credit_end :  'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$tblinvoice->f('email_language').$link_end;
$result['pdf_type_dd']=build_pdf_type_dd($selected,true);
$result['multiple_identity']=build_identity_dd($selected_id);
$result['identity_id']=$tblinvoice->f('identity_id');
$result['invoice_id']=$in['invoice_id'];
$result['type']=$tblinvoice->f('type');
$result['do_next']='invoice-invoice-invoice-pdf_settings';
$result['pdf_type_id']= $tblinvoice->f('type')==2 ? ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT : ACCOUNT_INVOICE_HEADER_PDF_FORMAT;
$result['multiple_identity_id'] = $selected_id;
$result['default_logo']=$tblinvoice->f('pdf_logo');
json_out($result);