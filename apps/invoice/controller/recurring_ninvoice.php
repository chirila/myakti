<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

class RecInvoiceEdit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getRecInvoice(){
		if($this->in['recurring_invoice_id'] == 'tmp'){ //add
			$this->getAddRecInvoice();
		}elseif ($this->in['template']) { # create quote template
			$this->getTemplateRecInvoice();
		}else{ # edit
			$this->getEditRecInvoice();
		}
	}

	private function getAddRecInvoice(){
		$in=$this->in;
		if($in['recurring_invoice_id'] == 'tmp' && !$in['invoice_id'] && !$in['contract_id']){

			$page_title= gm('Add Recurring Invoices');
			$do_next='invoice-recurring_ninvoice-invoice-updateCustomerDataRec';

			$default_note = $this->db->field("SELECT value FROM default_data WHERE type='invoice_note' ");
			if(!$in['start_date']){
				//$in['start_date']=time();
				//$in['quote_ts'] = time();
			}else{
				$in['start_date']=strtotime($in['start_date']);
				$in['quote_ts'] = strtotime($in['quote_ts']);
			}

			$details = array('table' 			=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
							 'field'		=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
							 'value'		=> $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],						
							 'filter'		=> $in['buyer_id'] ? ' AND billing =1' : '',
							 );

			$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
			$buyer_details->next();

			$customer_id = $in['buyer_id'];

			$due_date = $in['quote_ts'] + ( 30 * (60*60*24) ) ;
			$due = date(ACCOUNT_DATE_FORMAT,$in['quote_ts'] + ( 30 * (60*60*24) ) );

			$apply_discount = 0;
			if($in['buyer_id']){
				$buyer_info = $this->db->query("SELECT customers.payment_term, customers.fixed_discount, customers.btw_nr, customers.c_email,customers.name as company_name,
						customer_legal_type.name as l_name,customers.vat_regime_id,
						customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc,customers.name AS company_name,customers.internal_language
						FROM customers
						LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
						WHERE customers.customer_id = '".$in['buyer_id']."' ");
				$buyer_info->next();
				$due_date = $in['quote_ts'] + ( $buyer_info->f('payment_term') * (60*60*24) ) ;
				$due = date(ACCOUNT_DATE_FORMAT,$in['quote_ts'] + ( $buyer_info->f('payment_term') * (60*60*24) ) );
				$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');
				$text = gm('Company Name').':';
				$c_email = $buyer_info->f('c_email');
				$c_fax = $buyer_details->f('comp_fax');
				$c_phone = $buyer_details->f('comp_phone');
				$buyer_vat_nr=$buyer_info->f('btw_nr');
				$in['vat_regime_id'] = $buyer_info->f('vat_regime_id');
				if(!$in['vat_regime_id']){
					$vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

				    if(empty($vat_regime_id)){
				      $vat_regime_id = 1;
				    }
					$in['vat_regime_id'] = $vat_regime_id;
				}

				$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
				
				$in['discount_line_gen']= display_number($buyer_info->f('line_discount'));
				if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
					$apply_discount = '3';
				}else{
					if($buyer_info->f('apply_fix_disc')){
						$apply_discount = '2';
					}
					if($buyer_info->f('apply_line_disc')){
						$apply_discount = '1';
					}
				}
				$in['apply_discount'] = $in['apply_discount'] ? $in['apply_discount'] : ($apply_discount ? $apply_discount : '0');
				//$in['vat']=get_customer_vat($in['buyer_id']);
				$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$in['vat_regime_id']."' ");
				$main_address_id			= $buyer_details->f('address_id');
				$sameAddress = false;
				if($buyer_details->f('billing')==1){
					$sameAddress = true;
				}
				$in['email_language'] = (string)$buyer_info->f('internal_language');
				$emailMessageData = array('buyer_id'    => $in['buyer_id'],
		                      'contact_id'    => $in['contact_id'],
		                      'item_id'     => 'tmp',
		                      'email_language'  => $in['email_language'],
		                      'table'     => 'recurring_invoice',
		                      'table_label'   => 'recurring_invoice_id',
		                      'table_buyer_label' => 'buyer_id',
		                      'table_contact_label' => 'contact_id',
		                      'param'     => 'update_customer_data');
		        $in['email_language'] = get_email_language($emailMessageData);

				$in['languages']	= $in['email_language'];

				 if($in['change_currency']){
						$output['currency_type'] 			= $in['currency_type'] ;
						$in['currency_id'] 					= $in['currency_type'] ;
			        	$output['currency_txt'] 			=  currency::get_currency($in['currency_type']);
						
					}
				//$do_next='invoice-recurring_ninvoice-invoice-add_recurring';
			}else{
				$buyer_info = $this->db->query("SELECT phone, cell, email,CONCAT_WS(' ',firstname,lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
				$buyer_info->next();
				$c_email = $buyer_info->f('email');
				$c_fax = '';
				$c_phone = $buyer_info->f('phone');
				$name = $buyer_info->f('name');
				$text = gm('Name').':';
				$buyer_vat_nr='';
				if($in['contact_id']){
					//$do_next='invoice-recurring_ninvoice-invoice-add_recurring';
				}
			}
			$vat = $in['vat'] ? display_number($in['vat']) : display_number(0);
			$free_field = $buyer_details->f('address').'
					'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
					'.get_country_name($buyer_details->f('country_id'));

			if($in['delivery_address_id'] && $in['main_address_id'] && ($in['delivery_address_id'] != $in['main_address_id'])){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
				$free_field = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$sameAddress = false;
			}else if($in['main_address_id'] && $in['main_address_id'] != $main_address_id){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
				$free_field = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
			}
			$discount = $in['discount'];
			if($in['apply_discount'] < 2){
				$discount = 0;
			}

			$item_width=$in['remove_vat'] == 1 ? 5 : 4;
			/*$in['email_language'] 			= DEFAULT_LANG_ID;
	  		$in['languages']				= $in['email_language'];*/
	  		//$delivery_address=
            $acc_manager_id=$this->db->field("SELECT user_id FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
            $acc_manager_id = explode(',',$acc_manager_id);

            $customer_mandat = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' ");
			$customer_mandat_active = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' AND active_mandate ='1' ");
			$existIdentity = $this->db->field("SELECT COUNT(identity_id) FROM multiple_identity ");
			$page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE'");
			$stripe_active =$this->db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
			$allow_sepa = $this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_SEPA' ");

			$name = stripslashes($name);
			$output=array(
				'page'							=> $page_invoice == 1 ? true : false,
				'accmanager'					=> $this->get_accmanager($in),
//				'acc_manager_id'				=> $in['acc_manager_id'],
                                'acc_manager_id'     		=>$acc_manager_id[0],
				//General Details
				'apply_discount'				=> $in['apply_discount'] ? $in['apply_discount'] : '0',
				'apply_discount_line'			=> $in['apply_discount']==1 || $in['apply_discount']==3 ? true : false,
				'language_dd'					=> build_language_dd(),
				'gender_dd'						=> gender_dd(),
				'title_dd'						=> build_contact_title_type_dd(0),
				'title_id'						=> '0', 
				'gender_id'						=> '0',
				'language_id'					=> '0',
			    'main_address_id'				=> $in['main_address_id'] ? $in['main_address_id'] : $main_address_id,
			    'sameAddress'					=> $sameAddress,
				'show_product_save'         	=> true,
				'title'                     	=> $in['title'],
				'type'                      	=> $in['type'],
				'hide_proforma'             	=> $in['type']==1? false : true,
				'vatx'                      	=> $vat,
				'vat'							=> $vat,
				'vat_regime_dd'					=> build_vat_regime_dd($in['vat_regime_id']),
				'vat_regime_id'					=> $in['vat_regime_id'] ? $in['vat_regime_id'] : '0',
				'seller_name'               	=> $in['seller_name'] ? $in['seller_name'] : ACCOUNT_COMPANY,
				'serial_number'             	=> generate_invoice_number(DATABASE_NAME),
				'last_serial_number'        	=> get_last_invoice_number(),
				'start_date'                	=> $in['quote_ts'] ? $in['quote_ts']*1000 : '',
				'quote_ts'				=> $in['quote_ts'] ? $in['quote_ts']*1000 : '',
				'end_date'				=> $in['end_date'] ? strtotime($in['end_date'])*1000 : '',
				'discount'                  	=> display_number($buyer_info->f('fixed_discount')),
				'notes'                     	=> $in['notes'] ? utf8_decode($in['notes']) : ($default_note ? utf8_decode($default_note) : ''),
				'currency_type_list'        	=> build_currency_list($in['currency_id']),
				'payment_methods'           	=> build_payment_methods_list($in['payment_method']),
				'payment_method'				=> $in['payment_method'],
				'allow_sepa'					=> $allow_sepa,
				'frequency_list'            	=> build_frequency_list($in['frequency']),
				'frequency'				=> $in['frequency'] ? $in['frequency'] : '2',
				'currency_type'			=> $in['currency_id'] ? $in['currency_id'] : ACCOUNT_CURRENCY_TYPE,

				//Buyer Details
				//'add_customer'			=> !$in['buyer_id'] && !$in['contact_id'] ? true : false,
				'add_customer'				=> false,
				'buyer_id'                  	=> $in['buyer_id'],
				'contact_id'                	=> $in['contact_id'],
				'buyer_name'               	=> $name,
				'buyer_country_id'          	=> $buyer_details->f('country_id'),
				'buyer_country_name'        	=> get_country_name($buyer_details->f('country_id')),
				'buyer_state_id'            	=> $buyer_details->f('state_id'),
				'buyer_state_name'          	=> get_state_name($buyer_details->f('state_id')),
				'buyer_city'                	=> $buyer_details->f('city'),
				'buyer_zip'                 	=> $buyer_details->f('zip'),
				'buyer_address'             	=> $buyer_details->f('address'),
				'buyer_email'             	=> $c_email,
				'buyer_fax'             	=> $c_fax,
				'buyer_phone'             	=> $c_phone,
				'name_text'				=> $text,
				'field'				=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
				'free_field'			=> $free_field,
				'free_field_txt'			=> nl2br($free_field),

				//Seller Delivery Details
				'seller_d_address'    		=> $in['seller_d_address'] ? $in['seller_d_address']: ACCOUNT_DELIVERY_ADDRESS,
				'seller_d_zip'   	    		=> $in['seller_d_zip'] ? $in['seller_d_zip'] : ACCOUNT_DELIVERY_ZIP,
				'seller_d_city'   		=> $in['seller_d_city'] ? $in['seller_d_city'] : ACCOUNT_DELIVERY_CITY,
				'seller_d_country_dd'	    	=> build_country_list($in['seller_d_country_id'] ? $in['seller_d_country_id'] : ACCOUNT_DELIVERY_COUNTRY_ID),
				'seller_d_country_id'		=> $in['seller_d_country_id'] ? $in['seller_d_country_id'] : ACCOUNT_DELIVERY_COUNTRY_ID,

				//Seller Billing Details

				'seller_b_address'    		=> $in['seller_b_address'] ? $in['seller_b_address'] : ACCOUNT_BILLING_ADDRESS,
				'seller_b_zip'   	    		=> $in['seller_b_zip'] ? $in['seller_b_zip'] : ACCOUNT_BILLING_ZIP,
				'seller_b_city'   		=> $in['seller_b_city'] ? $in['seller_b_city'] : ACCOUNT_BILLING_CITY,
				'seller_b_country_dd'	    	=> build_country_list($in['seller_b_country_id'] ? $in['seller_b_country_id'] : ACCOUNT_BILLING_COUNTRY_ID),
				'seller_b_country_id'		=> $in['seller_b_country_id'] ? $in['seller_b_country_id']:ACCOUNT_BILLING_COUNTRY_ID,
				'seller_bwt_nr'   		=> $buyer_vat_nr,

				'tr_id'                     	=> 'tmp0',
				'language_dd_admin'		=> build_language_dd_new($in['email_language'] ? $in['email_language'] : $in['languages']),
				'country_dd'			=> build_country_list(0),
				'discount_line_gen'		=> display_number($buyer_info->f('line_discount')),
				'remove_vat'			=> $in['remove_vat'],
				'hide_vattd'			=> $in['remove_vat'] == 1 ? false : true,
				'item_width'			=> $in['remove_vat'] == 1 ? 5 : 4,
				'no_vat'				=> $in['remove_vat'] == 1 ? false : true,
				'show_vat'				=> $in['remove_vat'] == 1 ? true : false,
				'autocomplete_file'		=> "../apps/pim/admin/controller/contacts_autocomplete.php?customer_id=".$in['buyer_id'],
				'auto_complete_small'		=> '<input type="text" class="auto_complete_small" name="email_addr" value="" data-autocomplete_file=../apps/pim/admin/controller/contacts_autocomplete.php?customer_id="'.$in['buyer_id'].'" />',

				'multiple_identity_txt' 	=> $this->db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$in['identity_id']."'"),
				'identity_id'			=> $in['identity_id'] ? $in['identity_id'] : 0,
				'multiple_identity_dd' 		=> build_identity_dd($in['identity_id']),
				'main_comp_info'			=> $in['identity_id'] ? $this->getInvoiceIdentity($in['identity_id']) : $this->getInvoiceIdentity('0'),

				'existIdentity'			=> $existIdentity > 0 ? true : false,
				'customer_mandat'			=> $customer_mandat ? true : false,
				'customer_mandat_active'	=> $customer_mandat_active ? true : false,
				'sepa_mandate_txt'		=> $this->db->field("SELECT sepa_number from mandate WHERE buyer_id = '".$in['buyer_id']."' "),
				'sepa_mandate_dd' 		=> build_sepa_mandate_dd($customer_mandat_active, $in['buyer_id'], true),
				'sepa_mandates'			=> $customer_mandat_active? $customer_mandat_active : 0,
				//'delivery_address_id'	=> $in['delivery_address_id'],
				'delivery_address_id'	=> $buyer_details->f('address_id'),
				'our_ref'			=> $in['our_ref'] ? $in['our_ref'] : '', 
				'your_ref'			=> $in['your_ref'] ? $in['your_ref'] : '', 
				'source_dd'			=> get_categorisation_source(),
        			'type_dd'			=> get_categorisation_type(),
        			'source_id'			=> $in['source_id'],
        			'type_id'			=> $in['type_id'],
        			'segment_dd'		=> get_categorisation_segment(),
        			'segment_id'		=> $in['segment_id'],
        		'stripe_active'			=> $stripe_active? 1: 0,
			);

			if($in['vat_regime_id'] && $in['vat_regime_id']>=1000){
				$extra_eu=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
				if($extra_eu==2){
					$output['block_vat']=true;
				}
				if($extra_eu=='1'){
					$output['vat_regular']=true;
				}
				$contracting_partner=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
				if($contracting_partner==4){
					$output['block_vat']=true;
				}
			}

			if($in['contact_id']){
				$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
			}
			$i=0;
			$line_total=0;
			$subtotal=0;
			$vat_percent = array();
			$sub_t = array();
			$vat_percent_val = 0;
			$discount_value =0;
			$netto_amount=0;
			$code_width=0;
			$output['invoice_line']=array();
			if(isset($in['description']) && is_array($in['description'])){
				foreach ($in['description'] as $row_id){
					$row_id = 'tmp'.$i;
					$vat_i = $in['vat_percent'][$i] ? $in['vat_percent'][$i] : $vat;

					$line_item_width=$item_width;
					if(!$in['apply_discount'] || $in['apply_discount'] == 2){
						$line_item_width+=1;
					}
					if(!$in['article_code'][$i]){
						$line_item_width+=2;
					}

					$invoice_line=array(
						'tr_id'         			=> $row_id,
						'unitmeasure'  			=> $in['unitmeasure'][$i],
						'description'  			=> $in['description'][$i],
						'quantity'      			=> $in['quantity'][$i],
						'price'         			=> $in['price'][$i],
						'price_vat'				=> $in['price'][$i]+($in['price'][$i]*$vat_i/100),
						'hide_vattd'			=> $in['remove_vat'] == 1 ? false : true,
						'item_width'			=> $line_item_width,
						'article_code' 			=> $in['article_code'][$i],
					);

					$price =  $in['price'][$i];
					$line_discount = $in['discount_line'][$i];
					if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
						$line_discount = 0;
					}
					$line_total2 = $in['quantity'][$i] * ( $price - ( $price * $line_discount / 100 ) );

					$invoice_line['line_total']   	= display_number($line_total2);

					array_push($output['invoice_line'], $invoice_line);
					$amount_d = $line_total2 * $discount/100;

					$subtotal +=$line_total2;

					$vat_percent_val = ( $line_total2 - $amount_d ) * $vat_i/100;
					$vat_percent[$vat_i] += round($vat_percent_val,2);
					$sub_t[$vat_i] += round($line_total2,2);
					$sub_discount[$vat_i] += $amount_d;
					$i++;
					$discount_value	+= $amount_d;
					$netto_amount+=(round($line_total2,2)-$amount_d);
					$code_width=$in['article_code'][$i] ? $code_width+1 : $code_width+0;
				}
			}else{
				if($in['invoice_line'] && is_array($in['invoice_line'])){
					foreach($in['invoice_line'] as $row){
						$row_id = 'tmp'.$i;
						$vat_i = $row['vat'] ? return_value($row['vat']) : $vat;

						$line_item_width=$item_width;
						if(!$in['apply_discount'] || $in['apply_discount'] == 2){
							$line_item_width+=1;
						}
						if(!$row['article_code']){
							$line_item_width+=2;
						}

						$invoice_line=array(
							'tr_id'         			=> $row_id,
							'unitmeasure'  			=> $row['unitmeasure'],
							'content'  			    => $row['content'],
							'show_for_articles'		=> $row['article_code'] ? true : false,
							'description'  			=> $row['description'],
							'quantity'      		=> $row['quantity'],
							'price'         		=> $row['price'],
							'vat'					=> $row['vat'] ? display_number($row['vat']) : $vat,
							'price_vat'				=> display_number(return_value($row['price'])+(return_value($row['price'])*$vat_i/100)),
							'hide_vattd'			=> $in['remove_vat'] == 1 ? false : true,
							'item_width'			=> $line_item_width,
							'article_code' 			=> $row['article_code'],
						);

						$price =  return_value($row['price']);
						$line_discount = return_value($row['discount_line']);
						if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
							$line_discount = 0;
						}
						$line_total2 = return_value($row['quantity']) * ( $price - ( $price * $line_discount / 100 ) );

						$invoice_line['line_total']   	= display_number($line_total2);

						array_push($output['invoice_line'], $invoice_line);
						$amount_d = $line_total2 * $discount/100;

						$subtotal +=$line_total2;

						$vat_percent_val = ( $line_total2 - $amount_d ) * $vat_i/100;
						$vat_percent[$vat_i] += round($vat_percent_val,2);
						$sub_t[$vat_i] += round($line_total2,2);
						$sub_discount[$vat_i] += $amount_d;
						$i++;
						$discount_value	+= $amount_d;
						$netto_amount+=(round($line_total2,2)-$amount_d);
						$code_width=$row['article_code'] ? $code_width+1 : $code_width+0;
					}
				}
			}
			$output['vat_line']=array();
			foreach ($vat_percent as $key => $val){
				$total_vat += $val;
				$vat_line=array(
					'vat_percent'   				=> display_number($key),
					'vat_value'	   				=> display_number($val),
					'subtotal'					=> display_number($sub_t[$key]),
					'total_novat'	   	 		=> display_number($sub_t[$key]),
					'discount_value'				=> display_number($sub_discount[$key]),
					'view_discount'				=> $in['apply_discount'] < 2 ? false : true,
					'net_amount'			=> display_number($sub_t[$key] - $sub_discount[$key]),
				);
				array_push($output['vat_line'], $vat_line);	
			}

			$total=($subtotal - $discount_value) +$total_vat;
			$total_default = $total*return_value($output['currency_rate']) ;

			$output['discount_value']   			= $discount_value ? display_number($discount_value):'0.00';
			$output['total']            			= $total ? display_number($total):display_number(0);
			$output['total_default_currency']   	= $total_default ? display_number($total_default):display_number(0);
			$output['invoice_delivery']			= !$in['invoice_delivery'] ? 0 : 1;
			$output['net_amount']			      = display_number($netto_amount);
			$output['total_wo_vat']			      = display_number($subtotal);
			
			if($discount){
				$output['view_discount1'] 		= true;
			}else{
				$output['view_discount1']		= false;
			}
			//send invoice data
			/*switch($in['invoice_delivery']){
				case 0: $output['draft_checked']	= true; break;
				case 1: $output['send_checked']	= true; break;
			}*/

			if(!$in['include_pdf'])
			{
				$in['include_pdf'] = 1;
			}
			if(!$in['include_xml'])
			{
				$in['include_xml'] = 1;
			}

			$message_data=get_sys_message('invmess',$in['email_language']);

			$output['view_days']            	= $in['frequency']==5 ? true : false;
			$output['view_send_box']        	= $in['invoice_delivery']==1 ? true : false;
			$output['days']                 	= $in['days'];
			$output['include_pdf']  	      = $in['include_pdf'] ? true : false;
			$output['include_xml']  	      = $in['include_xml'] ? true : false;
			$output['copy_checked']         	= $in['copy'] ? true : false;
			$output['language_dd']	       	= build_pdf_language_dd($in['lid'],1);
			$output['subject']    			= $message_data['subject'];
			//$output['text']       			= $in['text'] ? stripslashes($in['text']) : stripslashes(html_entity_decode($message_data['text'],ENT_COMPAT, 'UTF-8'));
			$output['text']       			= stripslashes(html_entity_decode($message_data['text'],ENT_COMPAT, 'UTF-8'));
			$output['use_html']	 		= $message_data['use_html'];
			$output['generalvats']=build_vat_dd();


			if($in['buyer_id']){
				$invoice_email = $this->db->field("SELECT invoice_email FROM customers WHERE customer_id='{$in['buyer_id']}' ");
				if($invoice_email){
					$output['inv_email']		= true;
					$output['invoice_email']	= $invoice_email;

				}
				$output['isbuyer']			= true;
			}
			if(!$in['email_language']){
				$in['email_language']= $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'");
				$in['languages']= $in['email_language'];
			}
		}else if($in['recurring_invoice_id'] == 'tmp' && $in['contract_id']){
			$page_title= gm('Add Recurring Invoices');
			$do_next='invoice-recurring_ninvoice-invoice-updateCustomerDataRec';
			$contract_id=$in['contract_id'];
			$contract=$this->db->query("SELECT contracts.*,contracts_version.version_id,contracts_version.start_date as v_start_date,contracts_version.end_date as v_end_date,contracts_version.apply_discount as v_apply_discount,contracts_version.discount_line_gen as v_discount_line_gen,contracts_version.discount as v_discount 
				FROM contracts 
				INNER JOIN contracts_version ON contracts.contract_id=contracts_version.contract_id
				WHERE contracts.contract_id='".$in['contract_id']."' AND contracts_version.active='1' ");
			if(!$contract->next()){
				return ark::run('contract-contracts');
			}
			$contract_version=$contract->f('version_id');
			if(!$in['start_date']){
				$in['start_date']=$contract->f('v_start_date');
			}else{
				$in['start_date']=strtotime($in['start_date']);
			}
			$in['apply_discount'] = $contract->f('v_apply_discount');
			$in['discount_line_gen']= display_number($contract->f('v_discount_line_gen'));
			$discount = $contract->f('v_discount');
			if($in['apply_discount'] < 2){
				$discount = 0;
			}
			$buyer_id=$contract->f('customer_id');
			$customer_id = $contract->f('customer_id');
			if($contract->f('customer_id') && !$in['buyer_id']){
				$in['buyer_id']=$contract->f('customer_id');
			}
			if($contract->f('contact_id') && !$in['contact_id']){
				$in['contact_id']=$contract->f('contact_id');
			}
			//$vat = $contract->f('vat') ? display_number($contract->f('vat')) : get_customer_vat($customer_id);

			$details = array('table'	 		=> $buyer_id ? 'customer_addresses' : 'customer_contact_address' ,
							 'field'		=> $buyer_id ? 'customer_id' : 'contact_id',
							 'value'		=> $buyer_id ? $buyer_id : $contract->f('contact_id'),
							 'name'		=> $contract->f('customer_name'),
							 'filter'		=> $buyer_id ? ' AND billing =1' : '',
							 );

			$buyer_details = $this->db->query("SELECT *
										FROM ".$details['table'].
										" WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
			$buyer_details->next();


			if($buyer_id){
				$buyer_info = $this->db->query("SELECT customers.payment_term, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name,customers.vat_regime_id 
						FROM customers
						 LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
						  WHERE customers.customer_id = '".$buyer_id."' ");
				$buyer_info->next();
		//		$due_date = $in['quote_ts'] + ( $buyer_details->f('payment_term') * (60*60*24) ) ;
		//		$due = date(ACCOUNT_DATE_FORMAT,$in['quote_ts'] + ( $buyer_details->f('payment_term') * (60*60*24) ) );
				$name = $details['name'];
				$text = gm('Company Name').':';
				$c_email = $buyer_info->f('c_email');
				$c_fax = $buyer_details->f('comp_fax');
				$c_phone = $buyer_details->f('comp_phone');
				$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$buyer_info->f('vat_regime_id')."' ");
				$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$buyer_info->f('vat_regime_id')."' ");
				//$do_next='invoice-recurring_ninvoice-invoice-add_recurring';
			}else{
				$buyer_info = $this->db->query("SELECT phone, cell, email
										  FROM customer_contacts
										  WHERE ".$details['field']."='".$details['value']."' ");
				$buyer_info->next();
				$c_email = $buyer_info->f('email');
				$c_fax = '';
				$c_phone = $buyer_info->f('phone');
				$name = $details['name'];
				$text = gm('Name').':';
				if($contract->f('contact_id')){
					//$do_next='invoice-recurring_ninvoice-invoice-add_recurring';
				}
			}
			$in['vat_regime_id'] = $contract->f('vat_regime_id');
		      if($in['vat_regime_id'] && $in['vat_regime_id']<10000){
		    		$in['vat_regime_id']=$this->db->field("SELECT id FROM vat_new WHERE vat_regime_id='".$in['vat_regime_id']."' ");
		      }
		      if($in['vat_regime_id']){
		    		$in['remove_vat']=$this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		    		$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$in['vat_regime_id']."' ");
		      }
		      $vat=$in['vat'] ? display_number($in['vat']) : display_number(0);
			$item_width=$contract->f('remove_vat') == 1 ? 5 : 4;

			$free_field = $buyer_details->f('address').'
					'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
					'.get_country_name($buyer_details->f('country_id'));

			$different_address=false;
			if($in['delivery_address_id'] && $in['main_address_id'] && ($in['delivery_address_id'] != $in['main_address_id'])){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
				$free_field = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$different_address=true;
			}else if($in['main_address_id'] && $in['main_address_id'] != $main_address_id){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
				$free_field = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$different_address=true;
			}
			$text = gm('Company Name').':';

			$customer_mandat = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' ");
			$customer_mandat_active = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' AND active_mandate ='1' ");
			$existIdentity = $this->db->field("SELECT COUNT(identity_id) FROM multiple_identity ");

			$contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
									WHERE contact_id='".$contract->f('contact_id')."' AND customer_id='".$contract->f('customer_id')."'");
			$page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE'");
			$stripe_active =$this->db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
			$allow_sepa = $this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_SEPA' ");

			$output=array(
				'page'						=> $page_invoice == 1 ? true : false,
				'view_vat_label'            => ACCOUNT_VAT? true : false,
				'type'                      => 0,
				'buyer_name'                => $in['buyer_id'] ? $buyer_details->f('name') : $contract->f('customer_name'),
				'start_date'		    	=> $in['start_date']*1000,
				'end_date'					=> $in['end_date'] ? strtotime($in['end_date'])*1000: ($contract->f('v_end_date') ? $contract->f('v_end_date')*1000 : ''),
				'end_contract'				=> $in['end_contract']=='1' ? true : false,
				//'start_date'                => time()*1000,
				//'quote_ts'			    => time()*1000,
				'discount'                  => display_number($contract->f('v_discount')),
				'vatx'                      => $vat,
				'vat'                       => $vat,
				'notes'                     => $contract->f('notes'),
				'currency_type_list'        => build_currency_list($contract->f('currency_type')),
				'payment_methods'           => build_payment_methods_list(),
				'payment_method'			=> 0,
				'stripe_active'				=> $stripe_active? 1: 0,
				'allow_sepa'				=> $allow_sepa,
				'frequency_list'            => build_frequency_list($contract->f('frequency')),
				'frequency'	    		    => $contract->f('frequency') ? $contract->f('frequency') : '2',
				'currency_type'		    	=> $contract->f('currency_type'),
				'quote_ts'			    	=> $in['start_date']*1000,
				//Buyer Details

				'buyer_id'                  => $contract->gf('customer_id'),
				'contact_id'                => $contract->gf('contact_id'),
				'buyer_name'                => $name,
				'buyer_country_id'          => $buyer_details->f('country_id'),
				'buyer_country_name'        => get_country_name($buyer_details->f('country_id')),
				'buyer_state_id'            => $buyer_details->f('state_id'),
				'buyer_state_name'          => get_state_name($buyer_details->f('state_id')),
				'buyer_city'                => $buyer_details->f('city'),
				'buyer_zip'                 => $buyer_details->f('zip'),
				'buyer_address'             => $buyer_details->f('address'),
				'buyer_email'               => $c_email,
				'buyer_fax'            	    => $c_fax,
				'buyer_phone'               => $c_phone,
				'name_text'			    	=> $text,
				'field'			    		=> $contract->gf('customer_id')? 'customer_id' : 'contact_id',
				'contact_name'       	    => $in['contact_id'] ? get_contact_name($in['contact_id']) : ($contract->f('contact_id') ? get_contact_name($contract->f('contact_id')) : ''),
				'free_field'		    	=> $contract->gf('free_field') ? $contract->gf('free_field') : $free_field,
				'free_field_txt'		    => nl2br($contract->gf('free_field') ? $contract->gf('free_field') : $free_field ),
				//Seller Delivery Details

				'seller_name'               => ACCOUNT_COMPANY,
				'seller_d_address'    	    => ACCOUNT_DELIVERY_ADDRESS,
				'seller_d_zip'   		    => ACCOUNT_DELIVERY_ZIP,
				'seller_d_city'   	    	=> ACCOUNT_DELIVERY_CITY,
				'seller_d_country_dd'	    => build_country_list(ACCOUNT_DELIVERY_COUNTRY_ID),
				'seller_d_country_id'	    => ACCOUNT_DELIVERY_COUNTRY_ID,
				/*'seller_d_state_dd'	    => build_state_list($invoice_q->gf('seller_d_state_id'),$invoice_q->gf('seller_d_country_id')),*/

				//Seller Billing Details

				'seller_b_address'    	    => ACCOUNT_BILLING_ADDRESS,
				'seller_b_zip'   		    => ACCOUNT_BILLING_ZIP,
				'seller_b_city'   	    	=> ACCOUNT_BILLING_CITY,
				'seller_b_country_dd'	    => build_country_list(ACCOUNT_BILLING_COUNTRY_ID),
				'seller_b_country_id'	    => ACCOUNT_BILLING_COUNTRY_ID,
				/*'seller_b_state_dd'	    => build_state_list($invoice_q->gf('seller_b_state_id'),$invoice_q->gf('seller_b_country_id')),*/
				'seller_bwt_nr'   	    => ACCOUNT_VAT_NUMBER,
				'language_dd_admin'	    => build_language_admin_dd($contract->f('email_language')),
				'discount_line_gen'	    => display_number($contract->f('v_discount_line_gen')),
				'remove_vat'		    => $contract->f('remove_vat'),
				'hide_vattd'		    => $contract->f('remove_vat') == 1 ? false : true,
				'item_width'		    => $contract->f('remove_vat') == 1 ? 5 : 4,
				'no_vat'			    => $contract->f('remove_vat') == 1 ? false : true,
				'show_vat'			    => $contract->f('remove_vat') == 1 ? true : false,
				'vat_regime_dd'			=> build_vat_regime_dd($in['vat_regime_id']),
				'vat_regime_id'			=> $in['vat_regime_id'] ? $in['vat_regime_id'] : '0',
				
				'multiple_identity_txt'     => $this->db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$contract->f('identity_id')."'"),
				'identity_id'		    	=> $contract->gf('identity_id') ? $contract->gf('identity_id') : 0,
				'multiple_identity_dd' 	    => build_identity_dd($contract->gf('identity_id')),
				'main_comp_info'		    => $contract->gf('identity_id') ? $this->getInvoiceIdentity($contract->gf('identity_id')) : $this->getInvoiceIdentity('0'),

				'existIdentity'		    	=> $existIdentity > 0 ? true : false,

				'customer_mandat'		    => $customer_mandat ? true : false,
				'customer_mandat_active'	=> $customer_mandat_active ? true : false,
				'sepa_mandate_txt'          => $this->db->field("SELECT sepa_number from mandate WHERE mandate_id='".$contract->f('mandate_id')."'"),
				'sepa_mandate_dd' 	    	=> build_sepa_mandate_dd($contract->f('mandate_id'), $contract->f('customer_id'), true),
				'sepa_mandates'		    	=> $contract->f('mandate_id')? $contract->f('mandate_id') :  $customer_mandat_active,
				'invoice_delivery'	    	=> !$in['invoice_delivery'] ? 0 : 1,
				'sameAddress'		    	=> $in['delivery_address_id'] && $in['main_address_id'] && ($in['delivery_address_id'] != $in['main_address_id']) ? false : (!$contract->f('same_address') ? true : false),
				//'delivery_address_id'	    => $in['delivery_address_id'] && $in['main_address_id'] && ($in['delivery_address_id'] != $in['main_address_id']) ? $in['delivery_address_id'] : $contract->f('same_address'),
				'our_ref'			    	=> $contract->f('serial_number'),
				'main_address_id'		    => $contract->gf('main_address_id'), 
				'your_ref'			    	=> $contract->gf('your_ref'),
				'contract_id'		   		=> $in['contract_id'],
				'delivery_address_id'	    => $buyer_details->f('address_id'),
				'accmanager'				=> $this->get_accmanager($in),
				'acc_manager_id'			=> $in['acc_manager_id'] ? $in['acc_manager_id'] : $contract->f('acc_manager_id'),
				'source_dd'					=> get_categorisation_source(),
    			'type_dd'				=> get_categorisation_type(),
    			'source_id'				=> $contract->f('source_id'),
    			'type_id'				=> $contract->f('type_id'),
    			'segment_dd'			=> get_categorisation_segment(),
    			'segment_id'			=> $contract->f('segment_id'),
			);

			if($in['vat_regime_id'] && $in['vat_regime_id']>=1000){
				$extra_eu=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
				if($extra_eu==2){
					$output['block_vat']=true;
				}
				if($extra_eu=='1'){
					$output['vat_regular']=true;
				}
				$contracting_partner=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
				if($contracting_partner==4){
					$output['block_vat']=true;
				}
			}

			$tblinvoice_line=$this->db->query("SELECT contract_line.*,contracts_version.discount,contracts_version.apply_discount
						FROM contract_line
						INNER JOIN contracts_version ON contract_line.contract_id=contracts_version.contract_id AND contracts_version.version_id=contract_line.version_id
						WHERE contract_line.contract_id='".$contract_id."' AND contract_line.version_id='".$contract_version."' AND contract_line.content_type='1' AND contract_line.line_type IN ('1','2','4','5') ORDER BY group_id,sort_order,line_order ASC");

			$i=0;
			$line_total=0;
			$subtotal=0;
			$vat_value = 0;
			$vat_percent = array();
			$sub_t = array();
			$vat_percent_val = 0;
			$discount_value = 0;
			$netto_amount=0;
			$code_width=0;
			$output['invoice_line']=array();
			while ($tblinvoice_line->move_next()){
				$row_id = 'tmp'.$i;
				$price =  $tblinvoice_line->f('price');
				$line_discount = $tblinvoice_line->f('discount');
				if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$line_discount = 0;
				}

				$line_item_width=$item_width;
				if(!$in['apply_discount'] || $in['apply_discount'] == 2){
					$line_item_width+=1;
				}
				if(!$tblinvoice_line->f('article_id') && !$tblinvoice_line->f('tax_id')){
					$line_item_width+=2;
				}

				$line_total2 = $tblinvoice_line->f('quantity') * ( $price - ( $price * $line_discount / 100 ) );

				$invoice_line=array(
					'tr_id'         			=> $row_id,
					'description'  				=> $tblinvoice_line->f('name'),
					'quantity'      			=> display_number($tblinvoice_line->f('quantity')),
					'price'         			=> display_number($tblinvoice_line->f('price')),
					'line_total'    		    => display_number($line_total2),
					'price_vat'					=> display_number($tblinvoice_line->f('price')+($tblinvoice_line->f('price')*$tblinvoice_line->f('vat')/100)),
					'vat'						=> display_number($tblinvoice_line->f('vat')),
					'hide_currency2'			=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
					'hide_currency1'			=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
					'discount_line'				=> display_number($tblinvoice_line->f('discount')),
					'article_id'  				=> $tblinvoice_line->f('article_id'),
					'article_code'  			=> $tblinvoice_line->f('article_code'),
					'show_for_articles'			=> ($tblinvoice_line->f('article_id') || $tblinvoice_line->f('tax_id')) ? true : false,
					'width_if_article_code' 	=>( $tblinvoice_line->f('article_id') || $tblinvoice_line->f('tax_id') )? 'width:155px;' : 'width:270px;',
					'hide_vattd'				=> $contract->f('remove_vat') == 1 ? false : true,
					'item_width'				=> $line_item_width,
					'content'					=> $tblinvoice_line->f('content') ? true : false,
				);
				$code_width= $tblinvoice_line->f('article_id') ? $code_width+1 : $code_width+0;
				array_push($output['invoice_line'], $invoice_line);

				$amount_d = $line_total2 * $discount/100;

				$subtotal +=$line_total2;

				$vat_percent_val = ( $line_total2 - $amount_d ) * $tblinvoice_line->f('vat')/100;
				$vat_percent[$tblinvoice_line->f('vat')] += round($vat_percent_val,2);
				$sub_t[$tblinvoice_line->f('vat')] += round($line_total2,2);
				$sub_discount[$tblinvoice_line->f('vat')] += $amount_d;
				$i++;
				$discount_value	+= $amount_d;
				$netto_amount+=(round($line_total2,2)-$amount_d);
			}

			$output['vat_line']=array();
			foreach ($vat_percent as $key => $val){
				$total_vat += $val;
				$vat_line=array(
					'vat_percent'   		=> display_number($key),
					'vat_value'	   			=> display_number($val),
					'subtotal'				=> display_number($sub_t[$key]),
					'total_novat'	   	 	=> display_number($sub_t[$key]),
					'discount_value'		=> display_number($sub_discount[$key]),
					'view_discount'			=> $in['apply_discount'] < 2 ? false : true,
					'net_amount'			=> display_number($sub_t[$key] - $sub_discount[$key]),
				);
				array_push($output['vat_line'], $vat_line);
			}

			//assign the total information
			// $discount_value= $subtotal_novat * $discount/100;

			// $total=($subtotal_novat + $vat_value) - $discount_value;
			$total=($subtotal - $discount_value) +$total_vat;
			$total_default = $total*return_value($output['currency_rate']);

			$output['discount_value']   			= $discount_value ? display_number($discount_value):display_number(0);
			$output['total']            			= $total ? display_number($total):display_number(0);
			$output['total_default_currency']    	= $total_default ? display_number($total_default):display_number(0);
			$output['net_amount']			      	= display_number($netto_amount);
			$output['total_wo_vat']			      	= display_number($subtotal);

			if($discount){
				$output['view_discount1']	= true;
			}else{
				$output['view_discount1']	= false;
			}

			if(!$in['include_pdf'])
			{
				$in['include_pdf'] = 1;
			}
			if(!$in['include_xml'])
			{
				$in['include_xml'] = 1;
			}

			$output['view_days']            	= $in['frequency']==5 ? true : false;
			$output['view_send_box']        	= $in['invoice_delivery']==1 ? true : false;
			$output['days']                		= $in['days'];
			$output['include_pdf']  	      	= $in['include_pdf'] ? true : false;
			$output['include_xml']  	      	= $in['include_xml'] ? true : false;
			$output['copy_checked']         	= $in['copy'] ? true : false;
			$output['language_dd']	       		= build_pdf_language_dd($in['lid'],1);


			//recipients
			$recipients=$this->db->query("SELECT customer_contacts.*
		               FROM customer_contacts
		               WHERE customer_contacts.customer_id='{$buyer_id}' AND active='1' AND front_register='0' ");

			$j = 0;
			/*
				while ($db->move_next()){
					$in['recipients'][$db->f('contact_id')]=1;
					$view->assign(array(
						'recipient_name'        => $db->f('firstname').' '.$db->f('lastname'),
						'recipient_email'       => $db->f('email'),
						'recipient_id'          => $db->f('contact_id'),
						'recipients_checked'    => $in['recipients'][$db->f('contact_id')] ? 'checked="checked"' : ''
					),'recipient_row');

					$j++;
					$view->loop('recipient_row');
				}*/
			if($buyer_id){
				$invoice_email = $this->db->field("SELECT invoice_email FROM customers WHERE customer_id='".$buyer_id."' ");
				if($invoice_email){
					$output['inv_email']		= true;
					$output['invoice_email']	= $invoice_email;

				}
				$output['isbuyer']			= true;
			}

			$message_data=get_sys_message('contract_invmess',$contract->f('email_language'));
			$in['email_language'] 			= $contract->f('email_language');
	  		$in['languages']				= $in['email_language'];
	  		if(!$in['email_language'] && $buyer_id){
	  			$in['email_language'] = $this->db->field("SELECT internal_language
				  	FROM customers			 
				  	WHERE customer_id = '".$buyer_id."' ");
	  			$in['languages']= $in['email_language'];
	  		}
	  		if(!$in['email_language']){
				$in['email_language']= $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'");
				$in['languages']= $in['email_language'];
			}

			// $db->query("SELECT * FROM sys_message");
			// $db->move_next();
				// 'subject'    	=> $db->gf('subject'),
			$output['subject']    		= $message_data['subject'];
				// 'text'       	=> $db->gf('text'),
			$output['text']       		= stripslashes(html_entity_decode($message_data['text'],ENT_COMPAT, 'UTF-8'));
				// 'view_discount' => $discount ? '' : 'hide',
			$output['use_html']	    	= $message_data['use_html'];
			$output['view_vat']		= $vat ? true : false;
			$output['generalvats']=build_vat_dd();

		}else{
			$page_title= gm('Add Recurring Invoices');
			$do_next='invoice-recurring_ninvoice-invoice-updateCustomerDataRec';
			$invoice_id=$in['invoice_id'];

			if(!$in['start_date']){
				//$in['start_date']=time();
			}else{
				$in['start_date']=strtotime($in['start_date']);
			}

			$invoice_q = $this->db->query("SELECT *
									FROM tblinvoice
									WHERE id='".$invoice_id."'");
			if(!$invoice_q->next()){
				return ark::run('invoice-recurring_invoices');
			}
			$in['apply_discount'] = $invoice_q->f('apply_discount');
			$in['yuki_project_id'] = $invoice_q->gf('yuki_project_id');

			$discount = $invoice_q->f('discount');

			$in['discount_line_gen']= display_number($invoice_q->gf('discount_line_gen'));

			if($in['apply_discount'] < 2){
				$discount = 0;
			}
			$buyer_id=$invoice_q->f('buyer_id');
			$customer_id = $invoice_q->f('buyer_id');
			if($invoice_q->f('buyer_id') && !$in['buyer_id']){
				$in['buyer_id']=$invoice_q->f('buyer_id');
			}
			if($invoice_q->f('contact_id') && !$in['contact_id']){
				$in['contact_id']=$invoice_q->f('contact_id');
			}
			//$vat = $invoice_q->f('vat') ? display_number($invoice_q->f('vat')) : get_customer_vat($customer_id);

			$details = array('table'	 		=> $buyer_id ? 'customer_addresses' : 'customer_contact_address' ,
							 'field'		=> $buyer_id ? 'customer_id' : 'contact_id',
							 'value'		=> $buyer_id ? $buyer_id : $invoice_q->f('contact_id'),
							 'name'		=> $invoice_q->f('buyer_name'),
							 'filter'		=> $buyer_id ? ' AND billing =1' : '',
							 );

			$buyer_details = $this->db->query("SELECT *
										FROM ".$details['table'].
										" WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
			$buyer_details->next();

			//	$due_date = $in['quote_ts'] + ( 30 * (60*60*24) ) ;
			//	$due = date(ACCOUNT_DATE_FORMAT,$in['quote_ts'] + ( 30 * (60*60*24) ) );

			if($buyer_id){
				$buyer_info = $this->db->query("SELECT customers.payment_term, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name,customers.vat_regime_id
						FROM customers
						 LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
						  WHERE customers.customer_id = '".$buyer_id."' ");
				$buyer_info->next();
		//		$due_date = $in['quote_ts'] + ( $buyer_details->f('payment_term') * (60*60*24) ) ;
		//		$due = date(ACCOUNT_DATE_FORMAT,$in['quote_ts'] + ( $buyer_details->f('payment_term') * (60*60*24) ) );
				$name = $details['name'];
				$text = gm('Company Name').':';
				$c_email = $buyer_info->f('c_email');
				$c_fax = $buyer_details->f('comp_fax');
				$c_phone = $buyer_details->f('comp_phone');
				$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$buyer_info->f('vat_regime_id')."' ");
				$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$buyer_info->f('vat_regime_id')."' ");
				//$do_next='invoice-recurring_ninvoice-invoice-add_recurring';
			}else{
				$buyer_info = $this->db->query("SELECT phone, cell, email
										  FROM customer_contacts
										  WHERE ".$details['field']."='".$details['value']."' ");
				$buyer_info->next();
				$c_email = $buyer_info->f('email');
				$c_fax = '';
				$c_phone = $buyer_info->f('phone');
				$name = $details['name'];
				$text = gm('Name').':';
				if($invoice_q->f('contact_id')){
					//$do_next='invoice-recurring_ninvoice-invoice-add_recurring';
				}
			}	

			$in['vat_regime_id'] = $invoice_q->f('vat_regime_id');
		      if($in['vat_regime_id'] && $in['vat_regime_id']<10000){
		    		$in['vat_regime_id']=$this->db->field("SELECT id FROM vat_new WHERE vat_regime_id='".$in['vat_regime_id']."' ");
		      }
		      if($in['vat_regime_id']){
		    		$in['remove_vat']=$this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		    		$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$in['vat_regime_id']."' ");
		      }
		      $vat=$in['vat'] ? display_number($in['vat']) : display_number(0);
			$item_width=$invoice_q->f('remove_vat') == 1 ? 5 : 4;

			$free_field = $buyer_details->f('address').'
					'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
					'.get_country_name($buyer_details->f('country_id'));

			$different_address=false;
			if($in['delivery_address_id'] && $in['main_address_id'] && ($in['delivery_address_id'] != $in['main_address_id'])){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
				$free_field = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$different_address=true;
			}else if($in['main_address_id'] && $in['main_address_id'] != $main_address_id){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
				$free_field = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$different_address=true;
			}

			$text = gm('Company Name').':';

			$customer_mandat = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' ");
			$customer_mandat_active = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' AND active_mandate ='1' ");
			$existIdentity = $this->db->field("SELECT COUNT(identity_id) FROM multiple_identity ");

			$contact_name='';
			if($in['contact_id']){
				$contact_name=get_contact_name($in['contact_id']);
			}else if($invoice_q->f('contact_id')){
				$contact_name=get_contact_name($invoice_q->f('contact_id'));
			}
			$page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE'");
			/*$contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
									WHERE contact_id='".$invoice_q->f('contact_id')."' AND customer_id='".$invoice_q->f('buyer_id')."'");*/

			if($invoice_q->f('apply_discount')==0){
					$discount_line=false;
					$discount_global=false;
				}elseif($invoice_q->f('apply_discount')==1){
					$discount_line=true;
					$discount_global=false;
				}elseif($invoice_q->f('apply_discount')==2){
					$discount_line=false;
					$discount_global=true;
				}elseif($invoice_q->f('apply_discount')==3){
					$discount_line=true;
					$discount_global=true;
				}

			if(!$in['change_currency']){
				$currency_type = $invoice_q->f('currency_type');
				$currency_rate = $invoice_q->f('currency_rate');
			}else{
				$separator = $this->db->field("SELECT value from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
				$currency = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
				$currency_type = $in['currency_type'];
				$currency_rate= currency::getCurrency(currency::get_currency($in['currency_type'],'code'),$currency, 1,$separator);
				
			}	
			$stripe_active =$this->db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
			$allow_sepa = $this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_SEPA' ");

			$output=array(
				'page'							=> $page_invoice == 1 ? true : false,
				'view_vat_label'            => ACCOUNT_VAT? true : false,
				'type'                      => $invoice_q->gf('type'),
				'buyer_name'                => $in['buyer_id'] ? $buyer_details->gf('name') : $invoice_q->gf('buyer_name'),
				'start_date'                => $in['start_date'] ? $in['start_date']*1000 : '',
				'quote_ts'			    	=> $in['start_date'] ? $in['start_date']*1000 : '',
				'end_date'			    	=> $in['end_date'] ? strtotime($in['end_date'])*1000 : '',
				'discount'                  => display_number($invoice_q->gf('discount')),
				'apply_discount_line'      	=> $discount_line,
				'apply_discount_global'     => $discount_global,
				'vatx'                      => $vat,
				'vat'                       => $vat,
				'notes'                     => $invoice_q->gf('notes'),
				'currency_type_list'        => build_currency_list($invoice_q->gf('currency_type')),
				'payment_methods'           => build_payment_methods_list(),
				'payment_method'			=> $invoice_q->gf('payment_method'),
				'stripe_active'				=> $stripe_active? 1: 0,
				'allow_sepa'				=> $allow_sepa,
				'frequency_list'            => build_frequency_list($invoice_q->gf('frequency')),
				'frequency'	    		    => $invoice_q->gf('frequency') ? $invoice_q->gf('frequency') : '2',
				'currency_type'		    	=> $currency_type,
				'currency_rate'		    	=> $currency_rate,
				'currency_val'		   		=> get_commission_type_list($currency_type),
				'total_currency_hide'		=> $currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? false : true) : false,
				//Buyer Details

				'buyer_id'                  => $invoice_q->gf('buyer_id'),
				'contact_id'                => $invoice_q->gf('contact_id'),
				'buyer_name'                => $name,
				'buyer_country_id'          => $buyer_details->f('country_id'),
				'buyer_country_name'        => get_country_name($buyer_details->f('country_id')),
				'buyer_state_id'            => $buyer_details->f('state_id'),
				'buyer_state_name'          => get_state_name($buyer_details->f('state_id')),
				'buyer_city'                => $buyer_details->f('city'),
				'buyer_zip'                 => $buyer_details->f('zip'),
				'buyer_address'             => $buyer_details->f('address'),
				'buyer_email'               => $c_email,
				'buyer_fax'            	    => $c_fax,
				'buyer_phone'               => $c_phone,
				'name_text'			    => $text,
				'field'			    => $invoice_q->f('buyer_id')? 'customer_id' : 'contact_id',
				'contact_name'       	    => $contact_name,
				'free_field'		    => $different_address ? $free_field : ($invoice_q->f('free_field') ? $invoice_q->f('free_field') : $free_field),
				'free_field_txt'		    => $different_address ? nl2br($free_field) : (nl2br($invoice_q->f('free_field') ? $invoice_q->f('free_field') : $free_field)),
				//Seller Delivery Details

				'seller_name'               => $invoice_q->gf('seller_name'),
				'seller_d_address'    	    => $invoice_q->gf('seller_d_address'),
				'seller_d_zip'   		    => $invoice_q->gf('seller_d_zip'),
				'seller_d_city'   	    => $invoice_q->gf('seller_d_city'),
				'seller_d_country_dd'	    => build_country_list($invoice_q->gf('seller_d_country_id')),
				'seller_d_country_id'	    => $invoice_q->gf('seller_d_country_id'),
				/*'seller_d_state_dd'	    => build_state_list($invoice_q->gf('seller_d_state_id'),$invoice_q->gf('seller_d_country_id')),*/

				//Seller Billing Details

				'seller_b_address'    	    => $invoice_q->gf('seller_b_address'),
				'seller_b_zip'   		    => $invoice_q->gf('seller_b_zip'),
				'seller_b_city'   	    => $invoice_q->gf('seller_b_city'),
				'seller_b_country_dd'	    => build_country_list($invoice_q->gf('seller_b_country_id')),
				'seller_b_country_id'	    => $invoice_q->gf('seller_b_country_id'),
				/*'seller_b_state_dd'	    => build_state_list($invoice_q->gf('seller_b_state_id'),$invoice_q->gf('seller_b_country_id')),*/
				'seller_bwt_nr'   	    => $invoice_q->gf('seller_bwt_nr'),
				'language_dd_admin'	    => build_language_admin_dd($invoice_q->gf('email_language')),
				'discount_line_gen'	    => display_number($invoice_q->f('discount_line_gen')),
				'remove_vat'		    => $invoice_q->f('remove_vat'),
				'hide_vattd'		    => $invoice_q->gf('remove_vat') == 1 ? false : true,
				'item_width'		    => $invoice_q->f('remove_vat') == 1 ? 5 : 4,
				'no_vat'			    => $invoice_q->gf('remove_vat') == 1 ? false : true,
				'show_vat'			    => $invoice_q->gf('remove_vat') == 1 ? true : false,
				'autocomplete_file'	    => "../apps/pim/admin/controller/contacts_autocomplete.php?customer_id=".$in['buyer_id'],
				'auto_complete_small'	    => '<input type="text" class="auto_complete_small" name="email_addr" value="" data-autocomplete_file=../apps/pim/admin/controller/contacts_autocomplete.php?customer_id="'.$in['buyer_id'].'" />',
				'vat_regime_dd'			=> build_vat_regime_dd($in['vat_regime_id']),
				'vat_regime_id'			=> $in['vat_regime_id'] ? $in['vat_regime_id'] : '0',
				
				'multiple_identity_txt'     => $this->db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$invoice_q->gf('identity_id')."'"),
				'identity_id'		    => $invoice_q->gf('identity_id') ? $invoice_q->gf('identity_id') : 0,
				'multiple_identity_dd' 	    => build_identity_dd($invoice_q->gf('identity_id')),
				'main_comp_info'		    => $invoice_q->gf('identity_id') ? $this->getInvoiceIdentity($invoice_q->gf('identity_id')) : $this->getInvoiceIdentity('0'),

				'existIdentity'		    => $existIdentity > 0 ? true : false,

				'customer_mandat'		    => $customer_mandat ? true : false,
				'customer_mandat_active'	=> $customer_mandat_active ? true : false,
				'sepa_mandate_txt'          => $this->db->field("SELECT sepa_number from mandate WHERE mandate_id='".$invoice_q->gf('mandate_id')."'"),
				'sepa_mandate_dd' 	    =>  build_sepa_mandate_dd($invoice_q->gf('mandate_id'), $invoice_q->gf('buyer_id'), true),
				'sepa_mandates'		    => $invoice_q->gf('mandate_id')?  $invoice_q->gf('mandate_id') :$customer_mandat_active,
				'invoice_delivery'	    => !$in['invoice_delivery'] ? 0 : 1,
				'sameAddress'		    => $in['delivery_address_id'] && $in['main_address_id'] && ($in['delivery_address_id'] != $in['main_address_id']) ? false : (!$invoice_q->f('same_address') ? true : false),
				//'delivery_address_id'	    => $in['delivery_address_id'] && $in['main_address_id'] && ($in['delivery_address_id'] != $in['main_address_id']) ? $in['delivery_address_id'] : $invoice_q->f('same_address'),
				'main_address_id'		    => $in['main_address_id'] ? $in['main_address_id'] : $invoice_q->f('main_address_id'),
				'our_ref'			    => $invoice_q->f('our_ref'), 
				'your_ref'			    => $invoice_q->f('your_ref'),
				'delivery_address_id'	=> $buyer_details->f('address_id'),
				'accmanager'					=> $this->get_accmanager($in),
				'acc_manager_id'			=> $in['acc_manager_id'] ? $in['acc_manager_id'] : $invoice_q->f('acc_manager_id'),
				'source_dd'				=> get_categorisation_source(),
        			'type_dd'				=> get_categorisation_type(),
        			'source_id'				=> $invoice_q->f('source_id'),
        			'type_id'				=> $invoice_q->f('type_id'),
        			'segment_dd'			=> get_categorisation_segment(),
        			'segment_id'			=> $invoice_q->f('segment_id'),
			);

			if($in['vat_regime_id'] && $in['vat_regime_id']>=1000){
				$extra_eu=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
				if($extra_eu==2){
					$output['block_vat']=true;
				}
				if($extra_eu=='1'){
					$output['vat_regular']=true;
				}
				$contracting_partner=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
				if($contracting_partner==4){
					$output['block_vat']=true;
				}
			}

			//send invoice data
			/*switch($in['invoice_delivery']){
				case 0: $output['draft_checked'] = true; break;
				case 1: $output['send_checked']  = true; break;
			}*/

			$tblinvoice_line=$this->db->query("SELECT *
						FROM tblinvoice_line
						WHERE invoice_id='".$invoice_id."' AND (content!='1' OR (content='1' AND content_title='0')) ORDER BY sort_order ASC");

			$i=0;
			$line_total=0;
			$subtotal=0;
			$vat_value = 0;
			$vat_percent = array();
			$sub_t = array();
			$vat_percent_val = 0;
			$discount_value = 0;
			$netto_amount=0;
			$code_width=0;
			$output['invoice_line']=array();
			$last_tr='';
			while ($tblinvoice_line->move_next()){
				$row_id = 'tmp'.$i;
				$price =  $tblinvoice_line->f('price');
				$line_discount = $tblinvoice_line->f('discount');
				if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$line_discount = 0;
				}

				$line_item_width=$item_width;
				if(!$in['apply_discount'] || $in['apply_discount'] == 2){
					$line_item_width+=1;
				}
				if(!$tblinvoice_line->f('article_id') && !$tblinvoice_line->f('tax_id')){
					$line_item_width+=2;
				}

				if (!$tblinvoice_line->f('tax_id')){
					$last_tr = $row_id;
				}

				$line_total2 = $tblinvoice_line->f('quantity') * ( $price - ( $price * $line_discount / 100 ) );

				$invoice_line=array(
					'tr_id'         			=> $row_id,
					//'description'  			=> $tblinvoice_line->f('name'),
					'description'  			=> stripslashes(htmlspecialchars_decode($tblinvoice_line->f('name'),ENT_QUOTES)),
					'quantity'      			=> display_number($tblinvoice_line->f('quantity')),
					'price'         			=> display_number($tblinvoice_line->f('price')),
					'line_total'    		      => display_number($line_total2),
					'price_vat'				=> display_number($tblinvoice_line->f('price')+($tblinvoice_line->f('price')*$tblinvoice_line->f('vat')/100)),
					'vat'					=> display_number($tblinvoice_line->f('vat')),
					'hide_currency2'			=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
					'hide_currency1'			=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
					'discount_line'			=> display_number($tblinvoice_line->f('discount')),
					'article_id'  			=> $tblinvoice_line->f('article_id'),
					'article_code'  			=> $tblinvoice_line->f('item_code'),
					'show_for_articles'		=> ($tblinvoice_line->f('article_id') || $tblinvoice_line->f('tax_id'))? true : false,
					'width_if_article_code' 	=>( $tblinvoice_line->f('article_id') || $tblinvoice_line->f('tax_id') ) ? 'width:155px;' : 'width:270px;',
					'hide_vattd'			=> $invoice_q->gf('remove_vat') == 1 ? false : true,
					'item_width'			=> $line_item_width,
					'content'				=> $tblinvoice_line->f('content') ? true : false,
					'tax_id'  			=> $tblinvoice_line->f('tax_id'),
					'is_tax'  			=> $tblinvoice_line->f('tax_id')?true : false,
					'tax_for_article_id'		=> $tblinvoice_line->f('tax_for_article_id'),
					'for_article'			=> $tblinvoice_line->f('tax_id')? $last_tr:''
				);
				$code_width= $tblinvoice_line->f('article_id') ? $code_width+1 : $code_width+0;
				array_push($output['invoice_line'], $invoice_line);

				$amount_d = $line_total2 * $discount/100;

				$subtotal +=$line_total2;

				$vat_percent_val = ( $line_total2 - $amount_d ) * $tblinvoice_line->f('vat')/100;
				$vat_percent[$tblinvoice_line->f('vat')] += round($vat_percent_val,2);
				$sub_t[$tblinvoice_line->f('vat')] += round($line_total2,2);
				$sub_discount[$tblinvoice_line->f('vat')] += $amount_d;
				$i++;
				$discount_value	+= $amount_d;
				$netto_amount+=(round($line_total2,2)-$amount_d);
			}

			$output['vat_line']=array();
			foreach ($vat_percent as $key => $val){
				$total_vat += $val;
				$vat_line=array(
					'vat_percent'   			=> display_number($key),
					'vat_value'	   			=> display_number($val),
					'subtotal'				=> display_number($sub_t[$key]),
					'total_novat'	   	 	=> display_number($sub_t[$key]),
					'discount_value'			=> display_number($sub_discount[$key]),
					'view_discount'			=> $in['apply_discount'] < 2 ? false : true,
					'net_amount'			=> display_number($sub_t[$key] - $sub_discount[$key]),
				);
				array_push($output['vat_line'], $vat_line);
			}

			//assign the total information
			// $discount_value= $subtotal_novat * $discount/100;

			// $total=($subtotal_novat + $vat_value) - $discount_value;
			$total=($subtotal - $discount_value) +$total_vat;
			$total_default = $total*return_value($output['currency_rate']);

			$output['discount_value']   			= $discount_value ? display_number($discount_value):display_number(0);
			$output['total']            			= $total ? display_number($total):display_number(0);
			$output['total_default_currency']    	= $total_default ? display_number($total_default):display_number(0);
			$output['net_amount']			      = display_number($netto_amount);
			$output['total_wo_vat']			      = display_number($subtotal);

			if($discount){
				$output['view_discount1']	= true;
			}else{
				$output['view_discount1']	= false;
			}

			if(!$in['include_pdf'])
			{
				$in['include_pdf'] = 1;
			}
			if(!$in['include_xml'])
			{
				$in['include_xml'] = 1;
			}

			$output['view_days']            	= $in['frequency']==5 ? true : false;
			$output['view_send_box']        	= $in['invoice_delivery']==1 ? true : false;
			$output['days']                	= $in['days'];
			$output['include_pdf']  	      = $in['include_pdf'] ? true : false;
			$output['include_xml']  	      = $in['include_xml'] ? true : false;
			$output['copy_checked']         	= $in['copy'] ? true : false;
			$output['language_dd']	       	= build_pdf_language_dd($in['lid'],1);


			//recipients
			$recipients=$this->db->query("SELECT customer_contacts.*
		               FROM customer_contacts
		               WHERE customer_contacts.customer_id='{$buyer_id}' AND active='1' AND front_register='0' ");

			$j = 0;
			/*
				while ($db->move_next()){
					$in['recipients'][$db->f('contact_id')]=1;
					$view->assign(array(
						'recipient_name'        => $db->f('firstname').' '.$db->f('lastname'),
						'recipient_email'       => $db->f('email'),
						'recipient_id'          => $db->f('contact_id'),
						'recipients_checked'    => $in['recipients'][$db->f('contact_id')] ? 'checked="checked"' : ''
					),'recipient_row');

					$j++;
					$view->loop('recipient_row');
				}*/
			if($buyer_id){
				$invoice_email = $this->db->field("SELECT invoice_email FROM customers WHERE customer_id='".$buyer_id."' ");
				if($invoice_email){
					$output['inv_email']		= true;
					$output['invoice_email']	= $invoice_email;

				}
				$output['isbuyer']			= true;
			}

			$message_data=get_sys_message('invmess',$invoice_q->f('email_language'));
			$in['email_language'] 			= $invoice_q->f('email_language');
	  		$in['languages']				= $in['email_language'];
	  		if(!$in['email_language'] && $buyer_id){
	  			$in['email_language'] = $this->db->field("SELECT internal_language
				  	FROM customers			 
				  	WHERE customer_id = '".$buyer_id."' ");
	  			$in['languages']= $in['email_language'];
	  		}
	  		if(!$in['email_language']){
				$in['email_language']= $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'");
				$in['languages']= $in['email_language'];
			}

			// $db->query("SELECT * FROM sys_message");
			// $db->move_next();
				// 'subject'    	=> $db->gf('subject'),
			$output['subject']    		= $message_data['subject'];
				// 'text'       	=> $db->gf('text'),
			$output['text']       		= stripslashes(html_entity_decode($message_data['text'],ENT_COMPAT, 'UTF-8'));
				// 'view_discount' => $discount ? '' : 'hide',
			$output['use_html']	    	= $message_data['use_html'];
			$output['view_vat']		= $vat ? true : false;
			$output['generalvats']=build_vat_dd();
		}

		$recipients_auto=array();
		$contact_email_data=array();
		if($in['buyer_id']){
			$c_email = $this->db->field("SELECT invoice_email FROM customers WHERE customer_id='".$in['buyer_id']."' ");
			$invoice_email_type = $this->db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$in['buyer_id']."' ");

			if($invoice_email_type=='1'){
				$customer = $this->db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE customer_id='".$in['buyer_id']."'");

				$customerName = $customer->f('name');
				if(!empty($customerName)){
					$tmp_item = array(
					    'id' => $customer->f('email'), 
					    'name' => $customer->f('name').' - '.$customer->f('email'),
					    'value'=>$customer->f('email')
					);
					array_push($recipients_auto,$tmp_item);
				}
				
				if($in['contact_id']){
					$contacts = $this->db->query("SELECT customer_contactsIds.email,customer_contacts.firstname,customer_contacts.lastname, customers.name
									FROM customer_contacts
									INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
									LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
									WHERE customer_contactsIds.customer_id='".$in['buyer_id']."' AND customer_contactsIds.contact_id='".$in['contact_id']."' ");
				}else{
					$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
							FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
							LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							WHERE customer_contacts.active='1' AND customers.customer_id='".$in['buyer_id']."' ORDER BY lastname ");
				}
				while($contacts->next()){
					if($contacts->f('name')){
						$name = $contacts->f('firstname').' '.$contacts->f('lastname').' > '.$contacts->f('name');
					}else{
						$name = $contacts->f('firstname').' '.$contacts->f('lastname');
					}
					if(!empty($name) && $contacts->f('email')){
						$tmp_item = array(
						    'id' => $contacts->f('email'), 
						    'name' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
						    'value'=>$contacts->f('email')
						);
						array_push($recipients_auto,$tmp_item);
						array_push($contact_email_data,$contacts->f('email'));	
					}
				}
			}else if($invoice_email_type == '2'){
				$c_email = str_replace(' ', '', $c_email);
				$email_arr = array();
				$email_arr = explode(";", $c_email);
				foreach($email_arr as $key=> $value){
					$tmp_item = array(
					    'id' => $value, 
					    'name' => $value,
					    'value'=> $value
					);
					array_push($recipients_auto,$tmp_item);
					array_push($contact_email_data,$value);
				}
			}
		}
		$output['recipients']=$recipients_auto;
		$output['contact_email_data']=$contact_email_data;

		$data_last=$this->get_invoiceLast($in,$page_title,$do_next,$customer_id,$item_width,$code_width);
		foreach($data_last as $key => $value){
			$output[$key]=$value;
		}
		$output['total_currency_hide']			= $output['currency_type'] ? ($output['currency_type'] == ACCOUNT_CURRENCY_TYPE ? false : true) : false;
		$output['currency_val']		   	= get_commission_type_list($output['currency_type']);
		$output['delivery_list']		=array(array('id'=>'0','name'=>gm('Save invoice as draft')),array('id'=>'1','name'=>gm('Send invoice via email')));
		$output['start_date_txt']='';
		$output['frequency_txt']='';

		$yuki_active = $this->db->field("SELECT active FROM apps WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		$output['yuki_project_enabled']				= defined('YUKI_PROJECTS') && YUKI_PROJECTS == 1 && $yuki_active ? true : false;
		$output['yuki_projects']				= get_yuki_projects();
		$output['yuki_project_id']				= $in['yuki_project_id'];

		$this->out = $output;
	}

	private function getEditRecInvoice(){
		$in=$this->in;

		if($in['duplicate_recurring_invoice_id']) {
            $invoice_id = $in['duplicate_recurring_invoice_id'];
        } else {
            $invoice_id = $in['recurring_invoice_id'];
        }

        $page_title= gm('Edit Recurring Invoice');
        $do_next='invoice-recurring_ninvoice-invoice-update_recurring';

        $recurring_invoice = $this->db->query("SELECT *
                                    FROM recurring_invoice
                                    WHERE recurring_invoice_id='" . $invoice_id . "'");

        if (!$recurring_invoice->next()) {
            // unset($view);
            return ark::run('invoice-recurring_invoices');
        }
        if (!$in['buyer_id']) {
            $in['buyer_id'] = $recurring_invoice->f('buyer_id');
        }
        $in['apply_discount'] = $recurring_invoice->f('apply_discount');
        $discount = $recurring_invoice->f('discount');
        if ($in['apply_discount'] < 2) {
            $discount = 0;
        }
        $buyer_id = $recurring_invoice->f('buyer_id');
        $customer_id = $recurring_invoice->f('buyer_id');
        //$vat = $recurring_invoice->f('vat') ? display_number($recurring_invoice->f('vat')) : get_customer_vat($customer_id);

        if ($in['buyer_id']) {
            //get buyer details
            $buyer_details = $this->db->query("SELECT customers.customer_id,customers.name,customer_addresses.*, customer_legal_type.name AS l_name,customers.vat_regime_id
                                FROM customers
                                LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.billing=1
                                LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                                WHERE customers.customer_id='" . $in['buyer_id'] . "'");
            $buyer_details->next();

            $free_field = $buyer_details->f('address') . '
                ' . $buyer_details->f('zip') . ' ' . $buyer_details->f('city') . '
                ' . get_country_name($buyer_details->f('country_id'));
            $in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='" . $buyer_details->f('vat_regime_id') . "' ");
            $in['vat'] = $this->db->field("SELECT vats.value FROM vat_new 
                LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
                WHERE vat_new.id='" . $buyer_details->f('vat_regime_id') . "' ");
        }

        $factur_start_date = $recurring_invoice->f('start_date');
        $in['vat_regime_id'] = $recurring_invoice->f('vat_regime_id');
        if ($in['vat_regime_id'] && $in['vat_regime_id'] < 10000) {
            $in['vat_regime_id'] = $this->db->field("SELECT id FROM vat_new WHERE vat_regime_id='" . $in['vat_regime_id'] . "' ");
        }
        if ($in['vat_regime_id']) {
            $in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='" . $in['vat_regime_id'] . "' ");
            $in['vat'] = $this->db->field("SELECT vats.value FROM vat_new 
                LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
                WHERE vat_new.id='" . $in['vat_regime_id'] . "' ");
        }
        $vat = $in['vat'] ? display_number($in['vat']) : display_number(0);
        $item_width = $recurring_invoice->f('remove_vat') == 1 ? 5 : 4;

        $text = gm('Company Name') . ':';
        $message_data = get_sys_message('invmess', $recurring_invoice->gf('email_language'));
        $customer_mandat = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '" . $in['buyer_id'] . "' ");
        $customer_mandat_active = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '" . $in['buyer_id'] . "' AND active_mandate ='1' ");
        $existIdentity = $this->db->field("SELECT COUNT(identity_id) FROM multiple_identity ");

        $contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
                                WHERE contact_id='" . $recurring_invoice->f('contact_id') . "' AND customer_id='" . $recurring_invoice->f('buyer_id') . "'");
        if ($recurring_invoice->f('apply_discount') == 0) {
            $discount_line = false;
            $discount_global = false;
        } elseif ($recurring_invoice->f('apply_discount') == 1) {
            $discount_line = true;
            $discount_global = false;
        } elseif ($recurring_invoice->f('apply_discount') == 2) {
            $discount_line = false;
            $discount_global = true;
        } elseif ($recurring_invoice->f('apply_discount') == 3) {
            $discount_line = true;
            $discount_global = true;
        }
        $page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE'");

        if (!$in['change_currency']) {
            $currency_type = $recurring_invoice->f('currency_type');
            $currency_rate = $recurring_invoice->f('currency_rate');
        } else {
            $separator = $this->db->field("SELECT value from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
            $currency = currency::get_currency(ACCOUNT_CURRENCY_TYPE, 'code');
            $currency_type = $in['currency_type'];
            $currency_rate = currency::getCurrency(currency::get_currency($in['currency_type'], 'code'), $currency, 1, $separator);

        }
        $stripe_active = $this->db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
        $allow_sepa = $this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_SEPA' ");

        $output = array(
            'acc_manager_id' => $recurring_invoice->gf('acc_manager_id'),
            'page' => $page_invoice == 1 ? true : false,
            'accmanager' => $this->get_accmanager($in),
            'apply_discount_line' => $discount_line,
            'apply_discount_global' => $discount_global,
            'main_address_id' => $recurring_invoice->gf('main_address_id'),
            'show_product_save' => 0,
            'title' => $recurring_invoice->gf('title'),
            'type' => $recurring_invoice->gf('type'),
            'seller_name' => $recurring_invoice->gf('seller_name'),
            'buyer_name' => $in['buyer_id'] ? $buyer_details->gf('name') : $recurring_invoice->gf('buyer_name'),
            'start_date' => $in['duplicate_recurring_invoice_id'] ? '' : $factur_start_date * 1000,
            'start_date_txt' => date(ACCOUNT_DATE_FORMAT, $recurring_invoice->f('start_date')),
            'quote_ts' => $recurring_invoice->f('start_date') * 1000,
            'end_date' => $in['duplicate_recurring_invoice_id'] ? '' : ($recurring_invoice->f('end_date') ? $recurring_invoice->f('end_date') * 1000 : ''),
            'end_contract' => $recurring_invoice->f('end_contract') ? true : false,
            'discount' => display_number($recurring_invoice->gf('discount')),
            'vatx' => $vat,
            'vat' => $vat,
            'vat_regime_dd' => build_vat_regime_dd($in['vat_regime_id']),
            'vat_regime_id' => $in['vat_regime_id'] ? $in['vat_regime_id'] : '0',
            'notes' => $recurring_invoice->gf('notes'),
            'currency_type_list' => build_currency_list($recurring_invoice->gf('currency_type')),
            'payment_methods' => build_payment_methods_list($recurring_invoice->gf('payment_method')),
            'payment_method' => $recurring_invoice->gf('payment_method'),
            'allow_sepa' => $allow_sepa,
            'frequency_list' => build_frequency_list($recurring_invoice->gf('frequency')),
            'frequency' => $recurring_invoice->gf('frequency'),
            'view_days' => $recurring_invoice->gf('frequency') == 5 ? true : false,
            'view_send_box' => $recurring_invoice->gf('invoice_delivery') == 1 ? true : false,
            'days' => $recurring_invoice->gf('days'),
            'include_pdf' => $recurring_invoice->gf('include_pdf') ? true : false,
            'include_xml' => $recurring_invoice->gf('include_xml') ? true : false,
            'copy_checked' => $recurring_invoice->gf('copy') ? true : false,
            'language_dd' => build_pdf_language_dd($recurring_invoice->gf('lid'), 1),
            'subject' => $recurring_invoice->gf('email_subject'),
            // 'text'                      => $recurring_invoice->gf('email_body'),
            'text' => $recurring_invoice->f('email_body') ? stripslashes(html_entity_decode($recurring_invoice->f('email_body'), ENT_COMPAT, 'UTF-8')) : stripslashes(html_entity_decode($message_data['text'], ENT_COMPAT, 'UTF-8')),
            'use_html' => $message_data['use_html'],
            'currency_type' => $currency_type,
            'currency_rate' => $currency_rate,
            'currency_val' => get_commission_type_list($currency_type),
            'total_currency_hide' => $currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? false : true) : false,
            //Buyer Details

            'buyer_id' => $recurring_invoice->f('buyer_id'),
            'contact_id' => $recurring_invoice->f('contact_id'),
            /*'buyer_name'                => $recurring_invoice->f('buyer_name'),
            'buyer_country_id'          => $recurring_invoice->f('buyer_country_id'),
            'buyer_country_name'        => get_country_name($recurring_invoice->f('buyer_country_id')),
            'buyer_state_id'            => $recurring_invoice->f('buyer_state_id'),
            'buyer_state_name'          => get_state_name($recurring_invoice->f('buyer_state_id')),
            'buyer_city'                => $recurring_invoice->f('buyer_city'),
            'buyer_zip'                 => $recurring_invoice->f('buyer_zip'),
            'buyer_address'             => $recurring_invoice->f('buyer_address'),
            'buyer_email'               => $recurring_invoice->f('buyer_email'),
            'buyer_fax'            	    => $recurring_invoice->f('buyer_fax'),
            'buyer_phone'               => $recurring_invoice->f('buyer_phone'),*/
            'buyer_name' => $buyer_details->f('name') . ' ' . $buyer_details->f('l_name'),
            'buyer_country_id' => $buyer_details->f('country_id'),
            'buyer_country_name' => get_country_name($buyer_details->f('country_id')),
            'buyer_state_id' => $buyer_details->f('state_id'),
            'buyer_state_name' => get_state_name($buyer_details->f('state_id')),
            'buyer_city' => $buyer_details->f('city'),
            'buyer_zip' => $buyer_details->f('zip'),
            'buyer_address' => $buyer_details->f('address'),
            'buyer_email' => $buyer_details->f('email'),
            'buyer_fax' => $buyer_details->f('fax'),
            'buyer_phone' => $buyer_details->f('phone'),
            'name_text' => $text,
            'field' => $recurring_invoice->f('buyer_id') ? 'customer_id' : 'contact_id',
            'contact_name' => $recurring_invoice->f('contact_id') && !$recurring_invoice->f('buyer_id') ? $recurring_invoice->f('buyer_name') : trim($contact_name->f('firstname') . ' ' . $contact_name->f('lastname')),
            'free_field' => $recurring_invoice->f('free_field') ? $recurring_invoice->f('free_field') : $free_field,
            'free_field_txt' => nl2br($recurring_invoice->f('free_field') ? $recurring_invoice->f('free_field') : $free_field),
            //Seller Delivery Details

            'seller_name' => $recurring_invoice->gf('seller_name'),
            'seller_d_address' => $recurring_invoice->gf('seller_d_address'),
            'seller_d_zip' => $recurring_invoice->gf('seller_d_zip'),
            'seller_d_city' => $recurring_invoice->gf('seller_d_city'),
            'seller_d_country_dd' => build_country_list($recurring_invoice->gf('seller_d_country_id')),
            'seller_d_country_id' => $recurring_invoice->gf('seller_d_country_id'),
            /*'seller_d_state_dd'	    => build_state_list($recurring_invoice->gf('seller_d_state_id'),$recurring_invoice->gf('seller_d_country_id')),*/

            //Seller Billing Details

            'seller_b_address' => $recurring_invoice->gf('seller_b_address'),
            'seller_b_zip' => $recurring_invoice->gf('seller_b_zip'),
            'seller_b_city' => $recurring_invoice->gf('seller_b_city'),
            'seller_b_country_dd' => build_country_list($recurring_invoice->gf('seller_b_country_id')),
            'seller_b_country_id' => $recurring_invoice->gf('seller_b_country_id'),
            /*'seller_b_state_dd'		=> build_state_list($recurring_invoice->gf('seller_b_state_id'),$recurring_invoice->gf('seller_b_country_id')),*/
            'seller_bwt_nr' => $recurring_invoice->gf('seller_bwt_nr'),
            'disable_start_date' => 'disabled="disabled"',
            'language_dd_admin' => build_language_dd_new($recurring_invoice->gf('email_language')),
            'discount_line_gen' => display_number($recurring_invoice->f('discount_line_gen')),
            'remove_vat' => $recurring_invoice->gf('remove_vat'),
            'hide_vattd' => $recurring_invoice->gf('remove_vat') == 1 ? false : true,
            'item_width' => $recurring_invoice->f('remove_vat') == 1 ? 5 : 4,
            'no_vat' => $recurring_invoice->gf('remove_vat') == 1 ? false : true,
            'show_vat' => $recurring_invoice->gf('remove_vat') == 1 ? true : false,
            'autocomplete_file' => "../apps/pim/admin/controller/contacts_autocomplete.php?customer_id=" . $in['buyer_id'],
            'auto_complete_small' => '<input type="text" class="auto_complete_small" name="email_addr" value="" data-autocomplete_file=../apps/pim/admin/controller/contacts_autocomplete.php?customer_id="' . $in['buyer_id'] . '" />',

            'multiple_identity_txt' => $this->db->field("SELECT identity_name from multiple_identity WHERE identity_id='" . $recurring_invoice->gf('identity_id') . "'"),
            'identity_id' => $recurring_invoice->gf('identity_id') ? $recurring_invoice->gf('identity_id') : 0,
            'multiple_identity_dd' => build_identity_dd($recurring_invoice->gf('identity_id')),
            'main_comp_info' => $recurring_invoice->gf('identity_id') ? $this->getInvoiceIdentity($recurring_invoice->gf('identity_id')) : $this->getInvoiceIdentity('0'),

            'existIdentity' => $existIdentity > 0 ? true : false,

            'customer_mandat' => $customer_mandat ? true : false,
            'customer_mandat_active' => $customer_mandat_active ? true : false,
            'sepa_mandate_txt' => $this->db->field("SELECT sepa_number from mandate WHERE mandate_id='" . $recurring_invoice->gf('mandate_id') . "'"),
            'sepa_mandate_dd' => build_sepa_mandate_dd($recurring_invoice->gf('mandate_id'), $recurring_invoice->gf('buyer_id'), true),
            'sepa_mandates' => $recurring_invoice->gf('mandate_id') ? $recurring_invoice->gf('mandate_id') : $customer_mandat_active,
            'invoice_delivery' => !$recurring_invoice->f('invoice_delivery') ? 0 : 1,
            'sameAddress' => !$recurring_invoice->f('same_address') ? true : false,
            'delivery_address_id' => $recurring_invoice->f('same_address'),
            'our_ref' => $recurring_invoice->f('our_ref'),
            'your_ref' => $recurring_invoice->f('your_ref'),
            'email_language' => $recurring_invoice->gf('email_language'),
            'contract_id' => $recurring_invoice->f('contract_id'),
            'source_dd' => get_categorisation_source(),
            'type_dd' => get_categorisation_type(),
            'source_id' => $recurring_invoice->f('source_id'),
            'type_id' => $recurring_invoice->f('type_id'),
            'segment_dd' => get_categorisation_segment(),
            'segment_id' => $recurring_invoice->f('segment_id'),
            'stripe_active' => $stripe_active ? 1 : 0,

        );

        if ($in['vat_regime_id'] && $in['vat_regime_id'] >= 1000) {
            $extra_eu = $this->db->field("SELECT regime_type FROM vat_new WHERE id='" . $in['vat_regime_id'] . "' ");
            if ($extra_eu == 2) {
                $output['block_vat'] = true;
            }
            if ($extra_eu == '1') {
                $output['vat_regular'] = true;
            }
            $contracting_partner = $this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='" . $in['vat_regime_id'] . "' ");
            if ($contracting_partner == 4) {
                $output['block_vat'] = true;
            }
        }

        $in['email_language'] = $recurring_invoice->f('email_language');
        $in['languages'] = $in['email_language'];
        //send invoice data
        /*switch($recurring_invoice->f('invoice_delivery')){
            case 0: $output['draft_checked'] 	= true; break;
            case 1: $output['send_checked']	= true; break;

        }*/

        $in['discount_line_gen'] = display_number($recurring_invoice->gf('discount_line_gen'));
        $rectblinvoice_line = $this->db->query("SELECT *
            FROM recurring_invoice_line
            WHERE recurring_invoice_id='" . $invoice_id . "' ORDER BY sort_order ASC");

        $i = 0;
        $line_total = 0;
        $subtotal = 0;
        $vat_value = 0;
        $vat_percent = array();
        $sub_t = array();
        $vat_percent_val = 0;
        $discount_value = 0;
        $netto_amount = 0;
        $code_width = 0;
        $output['invoice_line'] = array();
        $last_tr = '';
        while ($rectblinvoice_line->move_next()) {
            $row_id = 'tmp' . $i;
            $line_item_width = $item_width;
            if (!$in['apply_discount'] || $in['apply_discount'] == 2) {
                $line_item_width += 1;
            }
            if (!$rectblinvoice_line->f('article_id') && !$rectblinvoice_line->f('tax_id')) {
                $line_item_width += 2;
            }
            if (!$rectblinvoice_line->f('tax_id')) {
                $last_tr = $row_id;
            }
            $invoice_line = array(
                'tr_id' => $row_id,
                'description' => $rectblinvoice_line->f('name'),
                'quantity' => display_number($rectblinvoice_line->f('quantity')),
                'price' => display_number_var_dec($rectblinvoice_line->f('price')),
                'price_vat' => display_number($rectblinvoice_line->f('price') + ($rectblinvoice_line->f('price') * $rectblinvoice_line->f('vat') / 100)),
                'vat' => display_number($rectblinvoice_line->f('vat')),
                'hide_currency2' => ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
                'hide_currency1' => ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
                'discount_line' => display_number($rectblinvoice_line->f('discount')),
                'article_id' => $rectblinvoice_line->f('article_id'),
                'article_code' => $rectblinvoice_line->f('item_code'),
                'show_for_articles' => ($rectblinvoice_line->f('article_id') || $rectblinvoice_line->f('tax_id')) ? true : false,
                'width_if_article_code' => ($rectblinvoice_line->f('article_id') || $rectblinvoice_line->f('tax_id')) ? 'width:155px;' : 'width:270px;',

                'hide_vattd' => $recurring_invoice->gf('remove_vat') == 1 ? false : true,
                'item_width' => $line_item_width,
                'content' => $rectblinvoice_line->f('content') ? true : false,
                'tax_id' => $rectblinvoice_line->f('tax_id'),
                'tax_for_article_id' => $rectblinvoice_line->f('tax_for_article_id'),
                'for_article' => $rectblinvoice_line->f('tax_id') ? $last_tr : ''
            );

            $code_width = $rectblinvoice_line->f('article_id') ? $code_width + 1 : $code_width + 0;

            // $line_total2=($rectblinvoice_line->f('quantity') *  $rectblinvoice_line->f('price'));
            // $subtotal_novat += $line_total2;

            $price = $rectblinvoice_line->f('price');
            $line_discount = $rectblinvoice_line->f('discount');
            if ($in['apply_discount'] == 0 || $in['apply_discount'] == 2) {
                $line_discount = 0;
            }
            $line_total2 = $rectblinvoice_line->f('quantity') * ($price - ($price * $line_discount / 100));
            $invoice_line['line_total'] = display_number($line_total2);
            array_push($output['invoice_line'], $invoice_line);

            /*$i++;
            $amount_d = $line_total2 * $discount/100;
            $vat_value += ( $line_total2 - $amount_d ) * $rectblinvoice_line->f('vat')/100;
            $vat_percent_val = ( $line_total2 - $amount_d ) * $rectblinvoice_line->f('vat')/100;
            $vat_percent[$rectblinvoice_line->f('vat')] += $vat_percent_val;
            $sub_t[$rectblinvoice_line->f('vat')] +=( $line_total2 - $amount_d );*/

            $amount_d = $line_total2 * $discount / 100;
            // }

            $subtotal += $line_total2;
            // $subtotal_novat += $line_total2;

            // $vat_value += ( $line_total2 - $amount_d ) * $vat_line/100;
            $vat_percent_val = ($line_total2 - $amount_d) * $rectblinvoice_line->f('vat') / 100;
            $vat_percent[$rectblinvoice_line->f('vat')] += round($vat_percent_val, 2);
            $sub_t[$rectblinvoice_line->f('vat')] += round($line_total2, 2);
            $sub_discount[$rectblinvoice_line->f('vat')] += $amount_d;
            $i++;
            $discount_value += $amount_d;
            $netto_amount += (round($line_total2, 2) - $amount_d);
        }
        $output['vat_line'] = array();
        foreach ($vat_percent as $key => $val) {
            $total_vat += $val;
            $vat_line = array(
                'vat_percent' => display_number($key),
                'vat_value' => display_number($val),
                'subtotal' => display_number($sub_t[$key]),
                'total_novat' => display_number($sub_t[$key]),
                'discount_value' => display_number($sub_discount[$key]),
                'view_discount' => $in['apply_discount'] < 2 ? false : true,
                'net_amount' => display_number($sub_t[$key] - $sub_discount[$key]),
            );
            array_push($output['vat_line'], $vat_line);
        }

        //assign the total information
        // $discount_value= $subtotal_novat * $discount/100;
        //	$vat_value= ($subtotal_novat-$discount_value) * $vat/100;
        // $total=($subtotal_novat - $discount_value) +$vat_value;
        $total = ($subtotal - $discount_value) + $total_vat;
        $total_default = $total * return_value($output['currency_rate']);

        $output['discount_value'] = $discount_value ? display_number($discount_value) : display_number(0);
        $output['total'] = $total ? display_number($total) : display_number(0);
        $output['total_default_currency'] = $total_default ? display_number($total_default) : display_number(0);
        $output['net_amount'] = display_number($netto_amount);
        $output['total_wo_vat'] = display_number($subtotal);

        if ($discount) {
            $output['view_discount1'] = true;
        } else {
            $output['view_discount1'] = false;
        }

        $is_contact_email = false;
        //recipients
        /*$tblinvoice_customer_contacts = $this->db->query("SELECT * FROM customer_contacts
                         WHERE customer_id='".$in['buyer_id']."' AND active ='1'");
        $output['recipients']=array();
        while($tblinvoice_customer_contacts->move_next()){
            $recs=array(
                'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
            );
            array_push($output['recipients'], $recs);
        }*/

        $output['recipients'] = array();
        $output['contact_email_data'] = array();
        $recipients = $this->db->query("SELECT customer_contacts.firstname,customer_contacts.lastname, customer_contacts.email AS c_email,recurring_email_contact.*
               FROM recurring_email_contact
               LEFT JOIN customer_contacts ON customer_contacts.contact_id = recurring_email_contact.contact_id
               WHERE recurring_email_contact.recurring_invoice_id={$invoice_id}  ");
        while ($recipients->next()) {
            /*$in['contact_id_email'] = $recipients->f('contact_id');
            $in['contact_name'] = $recipients->f('firstname').' '.$recipients->f('lastname');
            array_push($output['contact_email_data'], $recipients->f('email') ? $recipients->f('email') : $recipients->f('c_email'));
            array_push($output['recipients'],array('recipient_email'=>$recipients->f('email') ? $recipients->f('email') : $recipients->f('c_email')));*/
            $tmp_item = array(
                'id' => $recipients->f('email'),
                'name' => $recipients->f('email'),
                'value' => $recipients->f('email'),
            );
            array_push($output['recipients'], $tmp_item);
            array_push($output['contact_email_data'], $recipients->f('email'));
            $is_contact_email = true;
        }

        $output['is_contact_email'] = $is_contact_email;
        /*
        $j = 0;

        while ($db->move_next()){
            $view->assign(array(
                'recipient_name'        => $db->f('firstname').' '.$db->f('lastname'),
                'recipient_email'       => $db->f('email'),
                'recipient_id'          => $db->f('contact_id'),
                'recipients_checked' 	=> 'checked="checked"',
            ),'recipient_row');

            $j++;
            $view->loop('recipient_row');
        }*/
        $invoice_email = '';
        if ($buyer_id) {
            $invoice_email = $this->db->field("SELECT invoice_email FROM customers WHERE customer_id='" . $buyer_id . "' ");
            if ($invoice_email) {
                $output['inv_email'] = true;
                $output['invoice_email'] = $invoice_email;
            }
            $output['isbuyer'] = true;
        }
        $recurring_email_contact = $this->db->query("SELECT recurring_email_contact.* FROM recurring_email_contact WHERE recurring_invoice_id={$invoice_id} AND email<>'{$invoice_email}' AND contact_id='0' AND email!=1 LIMIT 1 ");
        if ($recurring_email_contact->next()) {
            $in['sent_to'] = $recurring_email_contact->f('email');
        }

        /*if($discount){
            $view->assign('view_discount','');
        }else{
            $view->assign('view_discount','hide');
        }*/
        $output['view_vat'] = $vat ? true : false;

        $data_last = $this->get_invoiceLast($in, $page_title, $do_next, $customer_id, $item_width, $code_width);
        foreach ($data_last as $key => $value) {
            $output[$key] = $value;
        }

        $output['total_currency_hide'] = $output['currency_type'] ? ($output['currency_type'] == ACCOUNT_CURRENCY_TYPE ? false : true) : false;
        $output['currency_val'] = get_commission_type_list($output['currency_type']);
        $output['generalvats'] = build_vat_dd();
        $output['delivery_list'] = array(array('id' => '0', 'name' => gm('Save invoice as draft')), array('id' => '1', 'name' => gm('Send invoice via email')));
        $output['frequency_txt'] = '';
        foreach ($output['frequency_list'] as $frequency) {
            if ($output['frequency'] == $frequency['id']) {
                $output['frequency_txt'] = $frequency['name'];
                break;
            }
        }
        $yuki_active = $this->db->field("SELECT active FROM apps WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
        $output['yuki_project_enabled'] = defined('YUKI_PROJECTS') && YUKI_PROJECTS == 1 && $yuki_active ? true : false;
        $output['yuki_projects'] = get_yuki_projects();
        $output['yuki_project_id'] = $recurring_invoice->f('yuki_project_id');
        $output['duplicate_recurring_invoice_id'] = $in['duplicate_recurring_invoice_id'] ?: '';

        $this->out = $output;
	}
	public function get_accmanager($in)	{
		$q = strtolower($in["term"]);		

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}

		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
											FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
											WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>htmlspecialchars($db_users->f('first_name').' '.$db_users->f('last_name')) ) );
		}
		return $items;
	}
	private function get_invoiceLast($in,$page_title,$do_next,$customer_id,$item_width,$code_width){

		$category=$this->db->query("SELECT customers.cat_id
				FROM customers
				WHERE customer_id = '".$customer_id."'");
		if($category->move_next()){
			$cat = $category->f('cat_id');
		}else{
			$cat = 1 ;
		}
		//$view->assign(array('cat_id'	=> $cat));
		$array=array(
			'page_title'            		=> $page_title,
			'recurring_invoice_id'  		=> $in['recurring_invoice_id'],
			'invoice_id'     		 		=> $in['invoice_id'],
			'style'     				=> ACCOUNT_NUMBER_FORMAT,
			'do_next'					=> $do_next,
			'acc_vat'	 			 	=> get_customer_vat($customer_id),
			'pick_date_format'      		=> pick_date_format(),
			'hide_currency2'		 		=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
			'hide_currency1'		 		=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
			'default_price_val'		 	=> display_number_var_dec(0),
			'default_quantity_val'	 		=> display_number(1),
			'default_currency'			=> ACCOUNT_CURRENCY_TYPE,
			'default_currency_name'			=> build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
			'default_currency_val'			=> get_commission_type_list(ACCOUNT_CURRENCY_TYPE),
			//'total_currency_hide'			=> false,
			'cat_id'					=> $cat,
			'APPLY_DISCOUNT_LIST'       		=> build_apply_discount_list($in['apply_discount']),
			'apply_discount'				=> $in['apply_discount'] ? $in['apply_discount'] : '0',
			'hide_global_discount'			=> $in['apply_discount'] < 2 ? false : true,
			'hide_line_global_discount'	    	=> $in['apply_discount'] == 1 ? true : false,
			'hide_discount_line'			=> !$in['apply_discount'] || $in['apply_discount'] == 2 ? false : true,
			'cc'						=> $this->get_cc($in),
			'contacts'					=> $this->get_contacts($in),
			'addresses'					=> $this->get_addresses($in),
			'articles_list'				=> $this->get_articles_list($in),
			'code_width'				=> $code_width==0 ? false : true,
			'xml_available'  			=> ACCOUNT_DELIVERY_COUNTRY_ID=='26' ? true : false
		);
		$glob_width=$item_width;
		if(!$in['apply_discount'] || $in['apply_discount'] == 2){
			$glob_width+=1;
		}
		if($code_width==0){
			$glob_width+=2;
		}
		$array['item_width']=$glob_width;
		////////// multilanguage for invoice note ////////////////
		$transl_lang_id_active 		= $in['languages'];
		if(!$transl_lang_id_active){
			$transl_lang_id_active = $in['email_language'];
		}

		$langs = $this->db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
		$array['translate_loop']=array();
		while($langs->next()){
			if($in['recurring_invoice_id']=='tmp' && !$in['invoice_id']){
				if($transl_lang_id_active==1){
					$note_types = 'invoice_terms';
					$note2_types = 'invoice_note';
				}else{
					$note_types = 'invoice_terms_'.$transl_lang_id_active;
					$note2_types = 'invoice_note_'.$transl_lang_id_active;
				}
				$invoice_general_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'invoice_terms'
										   AND type = '".$note_types."' ");
				$invoice_note2 = $this->db->field("SELECT value FROM default_data
										   WHERE type = '".$note2_types."' ");
				
				
				
				$array['NOTES']=$invoice_general_note;
				$array['notes2']=$invoice_note2;
				if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
					$acc_langs=array('en','fr','nl','de');
					$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
					if($lang_c){
						if($lang_c=='du'){
							$lang_c='nl';
						}
						if(in_array($lang_c, $acc_langs)){
							$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
							if($in['vat_notes']){
								if($array['notes2']){
									$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
								}else{
									$array['notes2']=$in['vat_notes'];
								}
							}
						}
					}		
				}
			}elseif ($in['invoice_id']) {
				$invoice_note = $this->db->field("SELECT item_value FROM note_fields
										   WHERE item_type 	= 'invoice'
										   AND 	item_name 	= 'notes'
										   AND 	item_id 	= '".$in['invoice_id']."'
										   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

				$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
										   WHERE item_type 	= 'invoice'
										   AND 	item_name 	= 'notes'
										   AND 	active 		= '1'
										   AND 	item_id 	= '".$in['invoice_id']."' ");
			}
			else{
				if($transl_lang_id_active==1){
					$transl_lang_id_active='invoice_terms';
					$note2_types = 'invoice_note';
				}else{
					$transl_lang_id_active='invoice_terms_'.$transl_lang_id_active.'';
					$note2_types = 'invoice_note_'.$in['email_language'];
				}
					$invoice_general_note = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'recurring_invoice'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['recurring_invoice_id']."'
								   AND 	lang_id 	= '".$in['email_language']."' ");
				$invoice_note2 = $this->db->field("SELECT value FROM default_data
										   WHERE type = '".$note2_types."' ");
				$exist_note = $this->db->field("SELECT notes2 FROM recurring_invoice  WHERE recurring_invoice_id='".$in['recurring_invoice_id']."'");
				if($exist_note!=''){
					$array['notes2']=utf8_decode($exist_note);
				}else{
					$array['notes2']=$invoice_note2;
					if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
						$acc_langs=array('en','fr','nl','de');
						$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
						if($lang_c){
							if($lang_c=='du'){
								$lang_c='nl';
							}
							if(in_array($lang_c, $acc_langs)){
								$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
								if($in['vat_notes']){
									if($array['notes2']){
										$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
									}else{
										$array['notes2']=$in['vat_notes'];
									}
								}
							}
						}		
					}
				}
/*				var_dump($in['recurring_invoice_id']);
				var_dump($in['email_language']);
				var_dump($invoice_general_note);*/
				$array['NOTES']=$invoice_general_note;
			}

			$translate_lang = 'form-language-'.$langs->f('code');
			if($langs->f('code')=='du'){
				$translate_lang = 'form-language-nl';
			}

			if($transl_lang_id_active != $langs->f('lang_id')){
				$translate_lang = $translate_lang.' hide';
			}
			$translate_loop=array(
				'translate_cls' 		=> 		$translate_lang,
				'lang_id' 			=> 		$langs->f('lang_id'),
				'NOTES'			=> 		$invoice_note,
			);
			array_push($array['translate_loop'], $translate_loop);
		}
		$custom_transl_lang_id_active = $in['languages'];
		if(!$custom_transl_lang_id_active){
			$custom_transl_lang_id_active = $in['email_language'];
		}

		$custom_langs = $this->db->query("SELECT * FROM pim_custom_lang WHERE active=1 ORDER BY sort_order ");
		$array['custom_translate_loop']=array();
		while($custom_langs->move_next()) {
			if($in['recurring_invoice_id']=='tmp' && !$in['invoice_id']) {
				$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
				$note_inv_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");
				if(!$custom_transl_lang_id_active) {
					$custom_transl_lang_id_active = $this->db->field("SELECT lang_id FROM pim_custom_lang WHERE active='1' AND sort_order = '1' ");
				}
			} elseif($in['invoice_id']) {
				$note_inv_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND item_id = '".$in['invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");
				$custom_transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND active='1' AND item_id = '".$in['invoice_id']."' ");
			} else {
				$note_inv_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'r_invoice' AND item_name = 'notes' AND item_id = '".$in['recurring_invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");
				$custom_transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields WHERE item_type = 'r_invoice' AND item_name = 'notes' AND active = '1' AND item_id = '".$in['recurring_invoice_id']."' ");
			}
			$custom_translate_lang = 'form-language-'.$custom_langs->f('code');
			if($custom_transl_lang_id_active != $custom_langs->f('lang_id')) {
				$custom_translate_lang = $custom_translate_lang.' hide';
			}
			$custom_translate_loop=array(
				'translate_cls'		=> $custom_translate_lang,
				'lang_id'			=> $custom_langs->f('lang_id'),
				'NOTES'			=> $note_inv_custom,
			);
			array_push($array['custom_translate_loop'], $custom_translate_loop);
		}

		if($transl_lang_id_active >=1000) {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$transl_lang_id_active."' ");
		} else {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_lang WHERE lang_id = '".$transl_lang_id_active."'");
		}

		$translate_lang_active = 'form-language-'.$translate_lang_active;
		if($translate_lang_active=='form-language-du'){
			$translate_lang_active = 'form-language-nl';
		}

		$array['translate_lang_active']	= $translate_lang_active;
		$array['language_dd'] 			= build_language_dd_new($in['languages']);
		$array['nr_decimals'] 			= ARTICLE_PRICE_COMMA_DIGITS;
		$array['email_language']		= $in['email_language'];

		return $array;

	}

	public function get_notes($in){
		$array=array();

		$transl_lang_id_active 		= $in['languages'];
		if(!$transl_lang_id_active){
			$transl_lang_id_active = $in['email_language'];
		}
		$langs = $this->db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");

		$transl_general_active 		= $in['languages'];
		if(!$transl_general_active){
			$transl_general_active = $in['email_language'];
		}

		$message_data=get_sys_message('invmess',$transl_general_active);
		$array['subject']	=$message_data['subject'];
		$array['text']      = stripslashes(html_entity_decode($message_data['text'],ENT_COMPAT, 'UTF-8'));

		if($transl_general_active==1){
			$note_types = 'invoice_terms';
			$note2_types = 'invoice_note';
		}else{
			$note_types = 'invoice_terms_'.$transl_general_active;
			$note2_types = 'invoice_note_'.$transl_general_active;
		}
		$invoice_general_note = $this->db->field("SELECT value FROM default_data
								   WHERE default_name = 'invoice_terms'
								   AND type = '".$note_types."' ");
		$invoice_note2 = $this->db->field("SELECT value FROM default_data
								   WHERE type = '".$note2_types."' ");
		$array['NOTES']=$invoice_general_note;
		$array['notes2']=$invoice_note2;
		if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
			$acc_langs=array('en','fr','nl','de');
			$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
			if($lang_c){
				if($lang_c=='du'){
						$lang_c='nl';
				}
				if(in_array($lang_c, $acc_langs)){
					$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
					if($in['vat_notes']){
						if($array['notes2']){
							$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
						}else{
							$array['notes2']=$in['vat_notes'];
						}
					}
				}
			}		
		}

		$array['translate_loop']=array();
		while($langs->next()){
			if($in['recurring_invoice_id'] == 'tmp' && !$in['c_invoice_id'] && !$in['duplicate_invoice_id'] && !$in['a_quote_id'] && !$in['a_contract_id'] ){
				if($langs->f('lang_id')==1){
					$note_type = 'invoice_note';
				}else{
					$note_type = 'invoice_note_'.$langs->f('lang_id');
				}
				$invoice_note = $this->db->field("SELECT value FROM default_data
								   WHERE default_name = 'invoice note'
								   AND type = '".$note_type."' ");
			}else{
				if($in['c_quote_id'] ){
					if($langs->f('lang_id')==1){
						$note_type = 'invoice_note';
					}else{
						$note_type = 'invoice_note_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'invoice note'
										   AND type = '".$note_type."' ");

				}elseif($in['c_contract_id'] ){
					if($langs->f('lang_id')==1){
						$note_type = 'contract_invoice_note';
					}else{
						$note_type = 'contract_invoice_note_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'contract invoice note'
										   AND type = '".$note_type."' ");
				}elseif($in['c_invoice_id']){
					if($langs->f('lang_id')==1){
						$note_type = 'invoice_note';
					}else{
						$note_type = 'invoice_note_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'invoice note'
									   AND type = '".$note_type."' ");

				}elseif ($in['duplicate_invoice_id']) {
					$invoice_note = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['duplicate_invoice_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

				}else{
					$invoice_note = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['invoice_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");
				}
			}

			$translate_lang = 'form-language-'.$langs->f('code');
			if($langs->f('code')=='du'){
				$translate_lang = 'form-language-nl';
			}
			if($transl_lang_id_active != $langs->f('lang_id')){
				$translate_lang = $translate_lang.' hidden';
			}
			$translate_loop=array(
				'translate_cls' 		=> 		$translate_lang,
				'lang_id' 			=> 		$langs->f('lang_id'),
				'NOTES'			=> 		$invoice_note,
			);
			array_push($array['translate_loop'], $translate_loop);
		}

		$transl_lang_id_active_custom = $in['languages'];
		if(!$transl_lang_id_active_custom){
			$transl_lang_id_active_custom = $in['email_language'];
		}

		$custom_langs = $this->db->query("SELECT * FROM pim_custom_lang WHERE active=1 ORDER BY sort_order ");
		$array['custom_translate_loop']=array();
		while($custom_langs->next()) {
			if($in['invoice_id'] == 'tmp' && !$in['c_invoice_id'] && !$in['duplicate_invoice_id'] && !$in['a_quote_id'] && !$in['a_contract_id'] ) {
				$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
				$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");
			} else {
				if($in['c_quote_id'] ) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");		
				}elseif($in['c_contract_id']) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");					
				} elseif($in['c_invoice_id']) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type='".$note_type_custom."' ");
				} elseif($in['duplicate_invoice_id']) {
					$invoice_note_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND item_id = '".$in['duplicate_invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");			
				} else {
					$invoice_note_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND item_id = '".$in['invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");					
				}
			}

			$translate_lang_cust = 'form-language-'.$custom_langs->f('code');
			if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
				$translate_lang_cust = $translate_lang_cust.' hidden';
			}

			$custom_translate_loop=array(
				'translate_cls'			=> 		$translate_lang_cust,
				'lang_id'				=> 		$custom_langs->f('lang_id'),
				'NOTES'				=> 		$invoice_note_custom,
			);
			array_push($array['custom_translate_loop'], $custom_translate_loop);

		}

		if($transl_lang_id_active>=1000) {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$transl_lang_id_active."'");
		} else {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_lang WHERE lang_id = '".$transl_lang_id_active."'");
		}

		$translate_lang_active = 'form-language-'.$translate_lang_active;
		if($translate_lang_active=='form-language-du'){
			$translate_lang_active = 'form-language-nl';
		}

		if($transl_lang_id_active>=1000) {
			$lang_code = $this->db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		} else {
			$lang_code = $this->db->field("SELECT language FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		}

		$array['translate_lang_active']	= $translate_lang_active;
		$array['language_dd'] 			= build_language_dd_new($transl_lang_id_active);
		$array['email_language']		= $transl_lang_id_active;
		$array['nr_decimals'] 			= ARTICLE_PRICE_COMMA_DIGITS;

		$this->out = $array;
	}


	private function getTemplateRecInvoice()
	{
		# code...
	}

	private function getInvoiceIdentity($identity_id){
		$comp_add=$this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ADDRESS_INVOICE'");
		$array = array(			
				'name'		=> ACCOUNT_COMPANY,
				'address'		=> $comp_add=='1' ? nl2br(ACCOUNT_BILLING_ADDRESS) : nl2br(ACCOUNT_DELIVERY_ADDRESS),
				'zip'			=> $comp_add=='1' ? ACCOUNT_BILLING_ZIP : ACCOUNT_DELIVERY_ZIP,
				'city'		=> $comp_add=='1' ? ACCOUNT_BILLING_CITY : ACCOUNT_DELIVERY_CITY,
				'country'		=> $comp_add=='1' ? get_country_name(ACCOUNT_BILLING_COUNTRY_ID) : get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
				'phone'		=> ACCOUNT_PHONE,
				'fax'			=> ACCOUNT_FAX,
				'email'		=> ACCOUNT_EMAIL,
				'url'			=> ACCOUNT_URL,
				'logo'		=>  defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',		
		);
		if($identity_id != '0'){
			$mm = $this->db->query("SELECT * FROM multiple_identity WHERE identity_id='".$identity_id."'")->getAll();
			$value = $mm[0];
			
			$array=array(
				'name'		=> $value['identity_name'],
				'address'		=> nl2br($value['company_address']),
				'zip'			=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'		=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'			=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'			=> $value['company_url'],
				'logo'		=> $value['company_logo'],
			);
			
		}
		return $array;
	}

	public function get_cc($in){
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id					
			ORDER BY name
			LIMIT 50")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				/*"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts($in) {
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customer_contacts.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY customer_contacts.lastname limit 9")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $value['firstname'].' '.$value['lastname']; //$contact_title. removed the title
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
				'bottom' 				=> '',
			);

		}

		$added = false;
		if(count($result)==9){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in){
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		'symbol'			=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	//'right'				=> $address->f('country'),
				  	//'bottom'			=> $address->f('zip').' '.$address->f('city'),
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

	public function get_articles_list($in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}

		if($in['email_language'] && $in['email_language'] != 0){
			$def_lang = $in['email_language'];
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id 
								   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';
		
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_article_categories.name AS family,
						pim_article_categories.name AS categorie,
						pim_articles.supplier_reference,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.use_combined,
						pim_articles.has_variants';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
							   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';
		
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_article_categories.name AS family,
						pim_article_categories.name AS categorie,
						pim_articles.supplier_reference,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.use_combined,
						pim_articles.has_variants';
		}

		$filter.="pim_articles_lang.lang_id='".$def_lang."'";

		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id'] && !is_array($in['article_id'])){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}
		if($in['artadjust']){
			$in['artadjust']=rtrim($in['artadjust'],",");
			$filter.=" AND pim_articles.article_id IN('".$in['artadjust']."') ";
		}
		if($in['customer_id']){
			$in['buyer_id']=$in['customer_id'];
		}

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' AND pim_article_prices.base_price='1' ORDER BY pim_articles.item_code LIMIT 5");
		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='INVOICE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				if($in['vat_regime_id']){
					if($in['vat_regime_id']<10000){
						if($in['vat_regime_id']==2){
							$vat=0;
						}
					}else{
						$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
							LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
							WHERE vat_new.id='".$in['vat_regime_id']."'");
						if(!$vat_regime){
							$vat_regime=0;
						}
						if($vat>$vat_regime){
							$vat=$vat_regime;
						}
					}			
				}else{
					$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
					if($vat_regime<10000){
						if($vat_regime==2){
							$vat=0;
						}
					}else{
						$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
							LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
							WHERE vat_new.id='".$vat_regime."'");
						if(!$vat_regime){
							$vat=0;
						}
					}
				}
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	//'name'						=> htmlspecialchars_decode($article->f('internal_name')),
			  	'name'						=> html_entity_decode(stripslashes($article->f('internal_name')), ENT_QUOTES, 'UTF-8'),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				'price'						=> $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)), 
				'base_price'				=> place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
				'has_variants'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? true : false,
				'has_variants_done'			=> 0,
				'is_variant_for'			=> 0,
				'is_combined'				=> $article->f('use_combined') ? true : false,
				'component_for'				=> '',
				'visible'					=> 1,
				'has_taxes'					=> $nr_taxes? true : false,
				'allow_stock'               =>ALLOW_STOCK == 1 ? true : false,
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		if(in_array(12,explode(';', $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']])))){
			array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));
		}

		return $articles;
	}

}

	$recinvoice = new RecInvoiceEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $recinvoice->output($recinvoice->$fname($in));
	}

	$recinvoice->getRecInvoice();
	$recinvoice->output();



?>