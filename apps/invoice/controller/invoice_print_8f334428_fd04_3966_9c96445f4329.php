<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $config;
$db = new sqldb();
if(!$in['save_as'] && !$in['reminder']){
	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);
	if(ark::$model == 'invoice' && (ark::$method == 'send' || ark::$method == 'sendNewEmail')){
		$in['attach_file_name'] = 'invoice_'.$in['id'].'.pdf';
		$file = $aws->getItem('invoice/invoice_'.$in['id'].'.pdf',$in['attach_file_name']);
		if($file === true){
			return;
		}else{
			$in['attach_file_name'] = null;
		}
	}else{
		$link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/invoice/invoice_'.$in['id'].'.pdf');
		$content = file_get_contents($link);
		$inv_serial_number=$db->field("SELECT serial_number FROM tblinvoice WHERE id='{$in['id']}' ");
		if($inv_serial_number){
             $name=$inv_serial_number.'.pdf';
		}else{
			$name='invoice_'.$in['id'].'.pdf';
		}
		if($content){
			if($in['base64']){
				//$name= 'invoice.pdf';
				$str = 'Content-Type: application/pdf;'."\r\n";
				$str .= ' name="'.$name.'"'."\r\n";
				$str .= 'Content-Transfer-Encoding: base64'."\r\n";
				$str .= 'Content-Disposition: attachment;'."\r\n";
				$str .= ' filename="'.$name.'"'."\r\n\r\n";
				$str = chunk_split(base64_encode($content), 76, "\r\n");
				return $str;
			}
			doQueryLog();
			header('Content-Type: application/pdf');
			header("Content-Disposition:inline;filename=".$name);
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $content;
			exit();
		}

	}
}

$db5 = new sqldb();
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");
ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$special_template = array(4,5,8); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// $pdf->SetFont('helvetica', '', 10, '', false);
// if($custom_lng){
	#we can use both font types but dejavusans looks better
	$pdf->SetFont('dejavusans', '', 9, '', false);
	// $pdf->SetFont('freeserif', '', 10, '', false);
// }

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);




// $pdf->setLanguageArray($l);
if(!USE_INVOICE_PAGE_NUMBERING){
	$pdf->SetFooterMargin(0);
	$pdf->setPrintFooter(false);
}
	$pdf->SetMargins(20, 5, 20);

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();
$html = include('invoice_print_body_8f334428_fd04_3966_9c96445f4329.php');
$pdf->writeHTML($html, true, false, true, false, '');

if($in['custom_type'] == 1 && DATABASE_NAME != '8f334428_fd04_3966_9c96445f4329'){
	if($pdf->GetY() > $height){
		$pdf->AddPage();
	}
}
	$pdf->SetY($height);
	if($in['print']){
		echo($html);
	}

if($in['print']){
	echo($htmls);
	exit();
}


if(!$in['db']){
	$db =  new sqldb();

}else{
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $in['db'],
		);
	$db =  new sqldb($database_2); // recurring billing
}

$attach_timesheet = $db->field("SELECT attach_timesheet FROM tblinvoice WHERE id='{$in['id']}' ");
if($attach_timesheet == 1){
	$select = $db->query("SELECT * FROM tblinvoice_projects WHERE invoice_id='{$in['id']}'");
	while ($select->next()) {
		$in['time_start'] = $select->f('start_date');
		$in['time_end'] = $select->f('end_date');
		$in['project_id'] = $select->f('project_id');
		$time_print = include(__DIR__.'/../../../misc/admin/controller/timesheet_print_body.php');
		$pdf->AddPage();
		$pdf->writeHTML($time_print, true, false, true, false, '');
	}
}
$attach_intervention = $db->query("SELECT attach_intervention,service_id FROM tblinvoice WHERE id='{$in['id']}' ");
if($attach_intervention->f('attach_intervention') == 1){
	$services = explode('-', $attach_intervention->f('service_id'));
	// print_r($services);exit();
	foreach ($services as $value) {
		$in['service_id'] = $value;
		$time_print = include(__DIR__.'/../../../maintenance/admin/controller/print_body.php');
		$pdf->AddPage();
		$pdf->writeHTML($time_print, true, false, true, false, '');
	}
}

$pdf->lastPage();
if(!$serial_number && $in['id']) {
    $serial_number=$in['id'];
}

if($in['save_as'] == 'F'){
	$in['invoice_pdf_name'] = 'invoice_'.time().'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['invoice_pdf_name'], 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['invoice_pdf_name'];
	$pdfFile = 'invoice/invoice_'.$in['id'].".pdf";
	$a->uploadFile($pdfPath,$pdfFile);

	if(ark::$method == 'gpasta'){
		$outputType = "S";
		if($in['attachAsString']==1){
			$outputType = "E";
		}
		//$str = $pdf->Output($serial_number.'.pdf',$outputType);
		$str = $pdf->Output(__DIR__.'/../../../'.$in['invoice_pdf_name'],$outputType);
	}
	unlink($in['invoice_pdf_name']);
	// $db->query("UPDATE tblquote SET preview='1' WHERE id='".$in['id']."' ");
	// $pdf->Output($buyer_reference.' '.$serial_number.'.pdf','I');
}else if($in['do']=='invoice-invoice_print'){
	doQueryLog();
   $pdf->Output($serial_number.'.pdf','I');
}else if($in['base64']){
	$str = $pdf->Output($serial_number.'.pdf','E');
}else{
	$pdf->Output(__DIR__.'/../../../invoice.pdf', 'F');
}
?>
