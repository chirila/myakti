<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class SepaInvoices extends Controller{

	function __construct($in,$db = null){
		parent::__construct($in,$db = null);		
	}

	public function get_invoices(){
		$in=$this->in;
		$result=array('query'=>array());

		$l_r = ROW_PER_PAGE;
		$order_by_array = array('serial_number','t_date','buyer_name', 'sepa_number', 'amount', 'amount_vat', 'due_date');

		if(!empty($in['customer_id'])){
			$in['buyer_id'] = $in['customer_id'];
		}

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$filter = 'WHERE 1=1 ';
		$filter_link = 'block';
		$order_by = " ORDER BY t_date DESC, iii DESC ";
		if($in['buyer_id']){
			$filter .=" AND buyer_id='".$in['buyer_id']."' ";
		}
		if(!$in['archived'] || $in['archived']=='0'){
			$filter.= " AND tblinvoice.archived_from_sepa='0'  ";
		}else{
			$filter.= " AND tblinvoice.archived_from_sepa='1' ";
		}

		if(!empty($in['c_invoice_id'])){
			$filter.=" and (tblinvoice.c_invoice_id = '".$in['c_invoice_id']."')";
			$arguments.="&c_invoice_id=".$in['c_invoice_id'];

		}
		if(!empty($in['search'])){
			$filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' )";
			$arguments_s.="&search=".$in['search'];
		}
		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == '1'){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
			}
		}

		if(!$in['view']){
			$filter.=" AND tblinvoice.not_paid = '0' AND tblinvoice.status = '0' AND tblinvoice.sent = '1'  AND tblinvoice.paid != '2' AND tblinvoice.mandate_id > 0 AND (tblinvoice.export_XML ='0' OR tblinvoice.export_XML ='1') ";
		}
		if($in['view'] == 1){
			$filter.=" AND tblinvoice.not_paid = '0' AND tblinvoice.status = '0' AND tblinvoice.sent = '1'  AND tblinvoice.paid != '2' AND tblinvoice.mandate_id > 0 AND tblinvoice.export_XML ='0'";			
		}
		if($in['view'] ==  2){
			$filter.=" AND tblinvoice.not_paid = '0' AND tblinvoice.status = '0' AND tblinvoice.sent = '1'  AND tblinvoice.paid != '2' AND tblinvoice.mandate_id > 0 AND tblinvoice.export_XML ='1' " ;
		}
			
		$tblinvoice = $this->db->query("SELECT tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, mandate.sepa_number
					FROM tblinvoice 
					INNER JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id ".$filter.$order_by );

		$max_rows=$tblinvoice->records_count();
		$result['max_rows']=$max_rows;
		if($in['view'] == 0){
			$l_r=100000;
		}
		$tblinvoice->move_to($offset*$l_r);
		$j=0;
		$color = '';
		$all_invoices_id = '';
		while($tblinvoice->move_next() && $j<$l_r){
			$status='';
			$status_title='';
			$type='';
			$type_title='';
			$color='';
			switch ($tblinvoice->f('type')){
				case '0':
					$type = 'regular_invoice';
					$type_title = gm('Regular invoice');
					break;
				case '1':
					$type = 'pro-forma_invoice';
					$type_title = gm('Proforma invoice');
					break;
				case '2':
					$type = 'credit_invoice';
					$type_title = gm('Credit Invoice');
					break;
			}

			switch ($tblinvoice->f('status')){
				case '0':
						if($tblinvoice->f('paid') == '2' ){
							// $status = 'Partially Paid';
							$status='partial';
							$status_title= gm('Partially Paid');
							$color = '';
							$class='';
						}
						if($tblinvoice->f('due_date') < time()){
						// $status = 'Late';
						$status = 'late';
						$class='late_img';
						$status_title= gm('Late');
						$color = 'late';
						}
						if($tblinvoice->f('not_paid') == '1'){
							$status='not_paid';
							$class='nop_paid_img';
							$status_title= gm('Will not be paid');
							$color = 'not_paid';
						}
					break;
				case '1':
					// $status = 'Fully Paid';
					if($tblinvoice->f('type') != '2' ){
						$color = '';
						$status='paid';
						$status_title= gm('Paid');
						$class='';
					}
					break;
			}
			if($tblinvoice->f('sent') == '0'){
						// $status = 'Draft';
							if($tblinvoice->f('type')=='2'){
								$type='credit_draft';
								$type_title = gm('Credit Invoice');
							}else{
								$type = 'draft_invoice';
								$type_title = gm('Draft');
							}
							$status='';
							$status_title= '';
							$color='';
							$class='';
						}

			if($tblinvoice->f('created_by') == 'System'){
				$color .= ' recurring';
			}
			if($tblinvoice->f('pdf_layout')){
				$link_end='&type='.$tblinvoice->f('pdf_layout').'&logo='.$tblinvoice->f('pdf_logo');
			}else{
				$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
			}
			#if we are using a customer pdf template
			if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $tblinvoice->f('pdf_layout') == 0){
				$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
			}

			$tblinvoice2 = $this->db->query("SELECT id,c_invoice_id from tblinvoice where c_invoice_id='".$tblinvoice->f('id')."'");
			$proforma_has_invoice = $this->db->field("SELECT id FROM tblinvoice WHERE proforma_id='".$tblinvoice->f('id')."' ");

			$item=array(
				'pdf_link'    			=> 'index.php?do=invoice-invoice_print&id='.$tblinvoice->f('id').'&lid='.$tblinvoice->f('email_language').$link_end,
				'credit_link'   			=> 'index.php?do=invoice-sepa&c_invoice_id='.$tblinvoice->f('id').$arguments,
				'info_link'     			=> 'index.php?do=invoice-invoice&invoice_id='.$tblinvoice->f('id'),
				'edit_link'     			=> 'index.php?do=invoice-ninvoice&invoice_id='.$tblinvoice->f('id'),
				'archive_link' 		 	=> array('do'=>'invoice-sepa-invoice-archive_sepa','id'=>$tblinvoice->f('id'),'tab'=>0),
				'activate_link' 			=> array('do'=>'invoice-sepa-invoice-activate_sepa','id'=>$tblinvoice->f('id'),'archived'=>1,'tab'=>1),
				'is_edit'				=> (isset($in['archived']) && $in['archived'] == 1) ? false : ($tblinvoice->f('sent') == 1 ? false : true),
				'is_money'				=> $tblinvoice2->move_next() ? true : false,
				'can_archive'			=> $tblinvoice->f('archived_from_sepa')==1 ? false : true,
				'can_activate'			=> $tblinvoice->f('archived_from_sepa')==1 ? true : false,
				'can_delete'			=> $_SESSION['access_level'] == 1 ? (!empty($in['archived']) ? true : false) : false,
				'type'				=> $type,
				'type_title'			=> $type_title,
				'status'				=> $status,
				'class'				=> $class,
				'status_title'			=> $status_title,
				'invoice_property'		=> $color,
				'amount'				=> place_currency(display_number($tblinvoice->f('amount'))),
				'amount_vat'			=> place_currency(display_number($tblinvoice->f('amount_vat'))),
				'our_reference'			=> $tblinvoice->f('our_ref'),
				'id'         	    		=> $tblinvoice->f('id'),
				'inv_type'			    	=> $in['view']?'':'all',
				's_number'		     		=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
				'created'         		=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
				'seller_name'      		=> $tblinvoice->f('seller_name'),
				'buyer_name'       		=> $tblinvoice->f('buyer_name'),
				'hide_paid'				=> $tblinvoice->f('status') ? false : true,
				'text'				=> $tblinvoice->f('created_by') == 'System' ? gm('Created from a recurring invoice') : '',
				'invoice_id'			=> $tblinvoice->f('id'),
				'add_to_pdf'			=> $_SESSION['add_to_sepa'][$tblinvoice->f('id')] == 1 ? true : false,
				'check_is_first_sepa'		=> $tblinvoice->f('first_sepa_invoice') == 1 ? true : false,
				'sepa_number'			=>$tblinvoice->f('sepa_number'),
				'due_date'				=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('due_date')),
			);
			array_push($result['query'], $item);
			$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('id');
			$j++;

		}
	
		$default_lang_id = $this->db->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
		$default_pdf_type = ACCOUNT_INVOICE_PDF_FORMAT;

		$result['lr']			= $l_r;
		$result['view_go']		= $_SESSION['access_level'] == 1 ? true : false;
		$result['lid']			= $default_lang_id;
		$result['pdf_type']		= $default_pdf_type;
		$result['all_invoices_id']	= $all_invoices_id;
		$result['yuki_active']		= defined('YUKI_ACTIVE') && YUKI_ACTIVE == 1 ? true :false;
		$result['archived']		= $in['archived'];
		$result['search']			= isset($in['search']) ? $in['search'] : '';
		$result['start_date']		= isset($in['start_date']) ? $in['start_date'] : '';
		$result['stop_date']		= isset($in['stop_date']) ? $in['stop_date'] : '';
		$result['tab']			= 1;

		$easyinvoice=$this->db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
		if($easyinvoice=='1'){
			$result['show_deliveries']	 	=false;
			$result['show_projects']		= false;
			$result['show_interventions']	= false;
		}else{
			$result['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
			$result['show_projects']		= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
			$result['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
		}  

		$this->out = $result;
	}

	function get_mandates(){
		$in=$this->in;
		$result=array('query'=>array());

		$filter = 'WHERE 1=1 ';

		$type_arr = array('1'=>gm('Core'),'2'=>gm('B2B'));
		$freq_arr = array('1'=>gm('One-Off'),'2'=>gm('Recurring'));
		$order_by = " ORDER BY mandate.sepa_number ASC";
		$l_r = ROW_PER_PAGE;
		if(!empty($in['search'])){
			$filter.=" AND (customers.name LIKE '%".$in['search']."%' OR mandate.sepa_number LIKE '%".$in['search']."%' OR mandate.type_mandate LIKE '%".$in['search']."%' OR mandate.bank_iban LIKE '%".$in['search']."%' )";
		}

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		if(!empty($in['order_by'])){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc'] == 'true'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
		}

		$mandate_list = $this->db->query("SELECT mandate.* ,customers.name
			                        FROM mandate 
			                        INNER JOIN customers ON customers.customer_id=mandate.buyer_id
			                        ".$filter." group by mandate.mandate_id ".$order_by);

		$max_rows=$mandate_list->records_count();
		$result['max_rows']=$max_rows;
		$mandate_list->move_to($offset*$l_r);
		$i=0;
		while ($mandate_list->next()) {
			$company_name = $this->db->field("SELECT name FROM customers WHERE  customer_id='".$mandate_list->f('buyer_id')."'  ");
			$item=array(
				'id_mandate'		=> $mandate_list->f('mandate_id'),
				'company_mandate'		=> $company_name,
				'creation_date'		=> date(ACCOUNT_DATE_FORMAT,$mandate_list->f('creation_date')),	
				'type_mandate'		=> $type_arr[$mandate_list->f('type_mandate')],
				'frequency'			=> $freq_arr[$mandate_list->f('frequency')],
				'iban'			=> $mandate_list->f('bank_iban'),
				'delete_link'		=> array('do'=>'invoice-sepa-invoice-delete_mandate','xget'=>'mandates','mandate_id'=>$mandate_list->f('mandate_id')),
				'inactiv_mandate'   	=> array('do'=>'invoice-sepa-invoice-revert_mandate','xget'=>'mandates','mandate_id'=>$mandate_list->f('mandate_id')),
				'status'			=> $mandate_list->f('active_mandate'),
				'hide_active'		=> $mandate_list->f('active_mandate')==1 ? false : true,
				'hide_view'			=> $mandate_list->f('active_mandate')==1 ? true : false,
				'amazon_link'		=> $mandate_list->f('amazon_link'),
				'type'			=> $mandate_list->f('type_mandate'),
				'sepa_number'		=> $mandate_list->f('sepa_number'),
				//'edit_link_mandate'	=> 'index.php?do=invoice-sepa&mandate_id='.$mandate_list->f('mandate_id').'&tab=1',
			);
			array_push($result['query'], $item);
			$i++;
			
		}

		$opt=array('1'=>gm("English"),'2'=>gm("French"),'3'=>gm("Dutch"));
		$result['lr']			= $l_r;
		$result['tab']			= 2;
		$result['pdf_lg']			= build_simple_dropdown($opt);
		$this->out = $result;
	}


}

	$sepa_data = new SepaInvoices($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$sepa_data->output($sepa_data->$fname($in));
	}

	$sepa_data->get_invoices();
	$sepa_data->output();

?>