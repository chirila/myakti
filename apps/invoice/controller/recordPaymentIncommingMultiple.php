<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$o=array();
$info = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id IN (".implode(",",array_keys($_SESSION['add_to_purchase'])).") ")->getAll();
    
if(!$in['payment_date']){
	$in['payment_date'] = time()*1000;

}else {
	$in['payment_date'] = is_numeric($in['payment_date']) ? $in['payment_date']*1000 : strtotime($in['payment_date'])*1000;
}
$total=0;
$total_payed=0;
$buyer_iban="";
$buyer_bic="";
$buyer_name="";
foreach($info as $key=>$p_invoice){
	$total+=$p_invoice['total_with_vat'];
	$total_payed += $db->field("SELECT COALESCE(SUM(amount),0) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$p_invoice['invoice_id']."'  ");
	if($key == 0){
		$buyer_iban=$p_invoice['buyer_iban'];
		$buyer_bic=$p_invoice['buyer_bic'];
		$buyer_name=$db->field("SELECT name FROM customers WHERE customer_id='".$p_invoice['supplier_id']."' ");
	}
}
	$amount_due = round($total - $total_payed,2);

	$o['payment_method']			='0'; 
    //$o['payment_method_dd']  		= payment_method_dd($in['payment_method']) ;
	$o['payment_method_dd']  		= payment_method_dd_pi($in['payment_method']) ;
    $o['payment_date']  			= $in['payment_date'];
	$o['total_amount_due1']			= display_number($amount_due);
	$o['cost']						= display_number(0);
	$o['from_list']            		= $in['from_list'];
	$o['show_QRCode_button'] 		= true;
	$o['show_QRCode'] 				= false;
	$o['disabled_QRCode_button'] 	= $amount_due? false:true;
	$o['disable_amount']			= true;
	$o['bulk_pay']					= true;

	$o['iban'] 		= $buyer_iban;
	$o['bname'] 	= $buyer_name;
	$o['bic']		= $buyer_bic;
	$o['ogm']		= '';

json_out($o);

function get_verify_bulk_payment(&$in,$showin=true,$exit=true){
	$db= new sqldb();

	if(array_key_exists('add_to_purchase', $_SESSION) === false || (array_key_exists('add_to_purchase', $_SESSION) === true && empty($_SESSION['add_to_purchase']))){
		msg::error(gm('No invoice has been selected'),'error');
	}else{
		$buyer_ids=array();
		$info = $db->query("SELECT supplier_id,status,paid FROM tblinvoice_incomming WHERE invoice_id IN (".implode(",",array_keys($_SESSION['add_to_purchase'])).") ")->getAll();
		foreach($info as $value){
			$buyer_ids[$value['supplier_id']]=1;
			if(!$value['status'] || ($value['status'] && $value['paid'] != 1)){
				//can be paid
			}else{
				//cannot be paid
				msg::error(gm('There are invoices that cannot be paid'),'error');
				break;
			}
		}
		if(count($buyer_ids)>1){
			msg::error(gm('Only invoices with the same supplier can be selected'),'error');
		}
	}
	return json_out(array(),$showin,$exit);
}

?>
