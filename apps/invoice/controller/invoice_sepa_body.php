<?php 

$db =  new sqldb();
$db2 =  new sqldb();
$path = ark::$viewpath;

if($in['type']==1){
 $html='invoice_sepa_core.html';
}else{
 $html='invoice_sepa_b2b.html';
}
$view_html = new at($path.$html);
$img = 'images/no-logo.png';
$attr = '';
$print_logo=ACCOUNT_LOGO;
	if($print_logo){
		$img = $print_logo;
		$size = getimagesize($img);
		if($size === false){
			$img=ACCOUNT_LOGO ? ACCOUNT_LOGO :'images/no-logo.png';
			$size = getimagesize($img);
		}
		$ratio = 150 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="150"';
		}else{
			$attr = 'height="77"';
		}
	}
	if(!$img){
		$img = 'images/no-logo.png';
	}

if($in['pdf_lang']==1){
	$view_html->assign('english',true);
}else if($in['pdf_lang']==2){
	$view_html->assign('french',true);
}else{
	$view_html->assign('dutch',true);
}
#query
$mandate = $db->query("SELECT * FROM mandate WHERE mandate_id = '".$in['mandate_id']."'");
while ($mandate->next()) {
	$company = $db2->query("SELECT customers.name,customer_legal_type.name as l_name FROM customers LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id WHERE customer_id='".$mandate->f('buyer_id')."'  ");
	$buyer_details = $db2->query("SELECT * FROM customer_addresses WHERE customer_id='".$mandate->f('buyer_id')."'  ");
	$settings_sepa = $db2->query("SELECT * FROM sepa_settings");
$view_html->assign(array(
		'holder_name'		=> stripslashes($company->f('name').' '.$company->f('l_name')),
		'holder_address' 	=> $buyer_details->f('address'),
		'holder_zip'		=> $buyer_details->f('zip'),
		'holder_city'		=> $buyer_details->f('city'),
		'holder_country'	=> get_country_name($buyer_details->f('country_id')),
		'holder_iban'		=> $mandate->f('bank_iban'),
		'holder_bic_code'	=> $mandate->f('bank_bic_code'),
		'creditor_name'		=> $settings_sepa->f('sepa_company'),
		'creditor_sepa'		=> $settings_sepa->f('sepa_id'),
		'creditor_address'	=> ACCOUNT_DELIVERY_ADDRESS,
		'creditor_zip'		=> ACCOUNT_DELIVERY_ZIP,
		'creditor_city'		=> ACCOUNT_DELIVERY_CITY,
		'creditor_country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
		'frequency'			=> $mandate->f('frequency')==1 ? 'margin-left:50px' : 'margin-left:100px',
		'creditor_logo'     => $img,
		'attr'				=> $attr,
		'sepa_number'		=> $mandate->f('sepa_number'),
		'check1'			=> $mandate->f('frequency')==2 ? 'uncheck2' : 'uncheck1',
		'check2'			=> $mandate->f('frequency')==1 ? 'uncheck2' : 'uncheck1',
	));
}

return $view_html->fetch();
?>

