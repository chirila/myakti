<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$payments_nr=0;
//payments_info
$o=array();
$payments_info = $db->query("SELECT * FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' and credit_payment!=1 ORDER BY payment_id desc");

$o['payments_row']=array();
while($payments_info->move_next()){
	switch ($payments_info->f('payment_method')) {
		case '0':
			$payment_method='-';
			break;
	    case '1':
				$payment_method=gm('Cash');
			break;	
	    case '2':
				$payment_method=gm('Bank transfer');
			break;	
	    case '3':
				$payment_method=gm('Credit Card');
			break;	
	     case '4':
				$payment_method=gm('Cheque');
			break;						
		
		default:
				$payment_method='-';
			break;
	}
$info=explode("-",$payments_info->f('info'));

	$payments_row=array(
		'date'	=>   date(ACCOUNT_DATE_FORMAT,strtotime($payments_info->f('date'))),
		'amount'  =>  display_number($payments_info->f('amount')),
		'payment_method'  =>  $payment_method,
		'cost_pay'  =>  display_number($info['3']),
		'payment_id'	=> $payments_info->f('payment_id'),
		'is_info'       => $info['2']?true:false,
		'info'       => $info['2'],
		'payment_id'  =>  $payments_info->f('payment_id'),
	);
	array_push($o['payments_row'], $payments_row);
	$payments_nr++;
}
if($payments_nr){
	$o['is_payment']=true;
	
}else{
	$o['is_payment']=false;
}
$info = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");


switch ($info->f('status')){
	case '0':
		$status = 'draft';

		 if($info->f('paid') == '1' ){
			
				$status = 'paid';
			
			}
		
		break;
	case '1':
		$status = 'final';
		 if($info->f('paid') == '1' ){
			
				$status = 'paid';
			
			}
		break;
	default:
		$status = 'draft';
		
		break;
}


$total=$info->f('total_with_vat');

$already_payed = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."'  ");
$total_payed = $already_payed->f('total_payed');

$amount_due = round($total - $total_payed,2);

$currency= get_currency($info->f('currency_type'));

$o['c_id'] = $info->f('supplier_id');
$o['buyer_id'] = $info->f('supplier_id');
$o['contact_id'] = $info->f('contact_id');
$o['serial_number']= $info->f('serial_number');
$o['booking_number']= $info->f('booking_number');
$o['invoice_number']= $info->f('invoice_number');
$in['i_date']=$info->f('invoice_date');
$o['i_date']=$info->f('invoice_date');
$o['selectedDraft']	 = $status == 'draft' ? true : false;
$o['selectedFinal']	 = $status == 'final' ? true : false;
$o['selectedPaid']	 = $status == 'paid' ? true : false;

$factur_date = date(ACCOUNT_DATE_FORMAT,  $info->f('invoice_date'));
$due_date = date(ACCOUNT_DATE_FORMAT,  $info->f('due_date'));

$o['i_date']								= $in['i_date'] ? $in['i_date'] : time();
$o['invoice_date']							= $in['i_date'] ? $in['i_date']*1000 : time()*1000;

$o['customer_name'] = $db->field("SELECT customers.name 
	                     FROM customers WHERE customers.customer_id=".$o['c_id']." ");
$o['contact_name'] = $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts
						WHERE customer_id=".$o['c_id']." AND contact_id='".$o['contact_id']."' ");
$o['order_id'] = $info->f('c_order_id');
$o['order_serial'] = $db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id ='".$o['order_id']."' ");



$link = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$info->f('c_invoice_id')."'");

$credit = $db->query("SELECT * FROM tblinvoice_incomming WHERE c_invoice_id='".$in['invoice_id']."'");
$is_not_sent=true;
$sent = $db->query("SELECT * FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND type='1' AND winbooks='1' ");
if($sent->next()){
	$is_not_sent=false;
}
    $o['factur_date']          = $factur_date;
    $o['due_date']             = $due_date;
	$o['is_link']              = $link->f('invoice_id') ? true : false;
	$o['link_id']              =$info->f('c_invoice_id');
	$o['link_invoice']               =$link->f('invoice_number');
	$o['not_credit']            =($info->f('type')==0 || $info->f('type')==1) ? true:false;
	$o['payment_date_show']		=date(ACCOUNT_DATE_FORMAT,time());
	$o['payment_date']			=time();
	$o['total_sum']				=display_number($amount_due);
	$o['total_sum_curr']		=place_currency(display_number($amount_due),$currency);
	$o['style']					=ACCOUNT_NUMBER_FORMAT;
	$o['currency']				=$currency;
	$o['pick_date_format']		= pick_date_format();
	$o['not_paid']				=$info->f('paid')=='1'?  false:true;
	$o['credit_id']				=$credit->f('invoice_id');
	$o['credit_invoice']		=$credit->f('invoice_number');
	$o['if_credit']				=$credit->f('invoice_id') ? true : false;
	$o['not_sent_to_accountent']=$is_not_sent;
	$o['cost']			=        display_number(0);
	$o['page_title']    = $info->f('type')==2?'Credit Invoice':'Purchase Invoice';
	$o['credit_style']  = $info->f('type')==2?'style="color: #ff0000;"':'';
	$o['credit_link']  = 'index.php?do=invoice-incomming_invoices&add_inv=true&type=2&c_invoice_id='.$in['invoice_id'];



$info = $db->query("SELECT tblinvoice_incomming.*, tblinvoice_incomming_line.vat_line AS new_vat, tblinvoice_incomming_line.total AS new_total
					FROM tblinvoice_incomming
					LEFT JOIN tblinvoice_incomming_line ON tblinvoice_incomming.invoice_id = tblinvoice_incomming_line.invoice_id
					WHERE tblinvoice_incomming.invoice_id='".$in['invoice_id']."' ORDER BY line_id ASC ");

$big_total_disco = 0;
$big_total = 0;
$amount = 0;
$total_vat = 0;
$i = 0;
$o['vat_row']=array();
while($info->next()){
	$disconto = $info->f('disconto');
	$allow_disconto = $info->f('allow_disconto');
	$line = $info->f('new_total');
	$line_disco = 0;
	if($allow_disconto == 1){
		$line = $info->f('new_total') - $info->f('new_total')*$disconto/100;
		$line_disco = $info->f('new_total')*$disconto/100;
	}
	$total_with_vat = $line + ($line/100*$info->f('new_vat'));
	$vat_amount = $line/100*$info->f('new_vat');

	$big_total += $total_with_vat;
	$big_total_disco += $line_disco+$total_with_vat;
        $amount += $line;
	$total_vat += $vat_amount;

	$new_vat1 = $info->f('new_vat');
	$new_total1 = $info->f('new_total');
	$total_with_disco1 = $line_disco;
	$total_with_vat1 = $total_with_vat;

	
	$vat_row=array(
		'new_vat'			=> display_number($info->f('new_vat')),
		'new_total'			=> display_number($info->f('new_total')),
		'total_with_vat'	=> display_number($total_with_vat),
		'vat_amount'		=> display_number($vat_amount),
		'allow_disconto'	=> $allow_disconto ? true : false,
		'total_with_disco'	=> display_number($total_with_disco1+$total_with_vat1)
	);
	
	array_push($o['vat_row'], $vat_row);
	$i++;
}
if($i<=1){
		$one_vat = true;
	}else{
		$one_vat = false;
	}


	$o['total'] 		    = display_number($big_total);
	$o['total_disco']		= display_number($big_total_disco);
	$o['total_amount']		= display_number($amount);
	$o['total_vat']			= display_number($total_vat);
	$o['one_vat']			= $one_vat;
	$o['new_vat1']			= display_number($new_vat1);
	$o['new_total1']		= display_number($new_total1);
	$o['total_with_vat1']	= display_number($total_with_vat1);
	$o['total_with_disco1'] = display_number($total_with_disco1+$total_with_vat1);
	$o['disconto']			= display_number($disconto);
	$o['allow_disconto']	= $allow_disconto ? true : false;





$customer_details = $db->query("SELECT customers.*, customer_addresses.*, country.name as country_n, customer_contact_language.name as c_lang
								FROM customers
								LEFT JOIN customer_addresses ON (customer_addresses.customer_id=customers.customer_id) AND is_primary='1'
								LEFT JOIN country ON (country.country_id=customer_addresses.country_id)
								LEFT JOIN customer_contact_language ON customers.language = customer_contact_language.id
								WHERE customers.customer_id=".$o['c_id']." ");

$contacts = $db->query("SELECT customer_contacts.*, customer_contact_language.name as c_lang FROM customer_contacts
						LEFT JOIN customer_contact_language ON customer_contacts.language = customer_contact_language.id
						WHERE customer_id=".$o['c_id']." AND contact_id='".$o['contact_id']."' ");



if($customer_details->next()){
	
		$o['customer_name']				= $customer_details->f('name');
		$o['country_dd']			  = $customer_details->f('country_n');
		$o['state_dd']					= $customer_details->f('state_id');
		$o['city']						= $customer_details->f('city');
		$o['zip']						= $customer_details->f('zip');
		$o['address']					= $customer_details->f('address');
		$o['vat_dd']					= build_vat_dd($customer_details->f('vat_id'));
		$o['btw_nr']					= $in['bwt_nr'];
		$o['btw']						= $customer_details->f('btw_nr');
		$o['comp_start']				= true;
		$o['language']					= $customer_details->f('c_lang');
		$o['payment_term']				= $customer_details->f('payment_term');
		$o['payment_term_type']		    = $customer_details->f('payment_term_type');
	
}
if($contacts->next()){
	
		$o['first_name']			    = $contacts->f('firstname');
		$o['last_name']					= $contacts->f('lastname');
		$o['email']						= $contacts->f('email');
		$o['phone']						= $contacts->f('phone');
		$o['c_start']					= $in['c_id'] && $in['invoice'] ? false : true;
		$o['c_language']				= $contacts->f('c_lang');
	
}


$order_ids=array();


$db->query("SELECT c_order_id FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");

if($db->f('c_order_id')){
   $order_ids=explode(',', $db->f('c_order_id'));


}
$show_info=false;
$o['orders_row']=array();
if($order_ids){
	foreach ($order_ids as $key => $order_id) {
	  $p_order = $db->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$order_id."' ");
   
	  $orders_row=array(
		'serial'		=> $p_order->f('serial_number'),
		'total'			=> place_currency(display_number($p_order->f('amount'))),
		'info_link'     => 'index.php?do=order-p_order&p_order_id='.$p_order->f('p_order_id'),
		'delete_link'	=>'index.php?do=invoice-incomming_invoice-invoice-delete_order&order_id='.$p_order->f('p_order_id').'&invoice_id='.$in['invoice_id']
	    );

        $total_gen+=$p_order->f('amount');

		array_push($o['orders_row'], $orders_row);
	}

$show_info=true;
}
        $o['show_info']	= $show_info;
		$o['total_gen_order'] = place_currency(display_number($total_gen));


$db->query("SELECT pim_p_orders.p_order_id,pim_p_orders.serial_number,pim_p_orders.customer_id,pim_p_orders.order_full
			 FROM pim_p_orders			 
			 WHERE pim_p_orders.customer_id= '".$o['c_id']."' AND pim_p_orders.order_full!=1 ");
$o['orders']=array();

while($db->move_next()) {
  array_push($o['orders'],array("value"=>$db->f('serial_number'),
  	                           'id'=>$db->f('p_order_id'))
            );
}
json_out($o);
?>