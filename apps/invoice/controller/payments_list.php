<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $fname($in,false);
	}

	function get_payments($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data=array('query'=>array());
		$l_r = ROW_PER_PAGE;
		$order_by_array = array('date');
		$filter = "WHERE 1=1 ";
		$filter_arch= "";
		$order_by=" ORDER BY date DESC "; 
		if((!$in['offset']) || (!is_numeric($in['offset']))){
		    $offset=0;
		    $in['offset']=1;
		}else{
		    $offset=$in['offset']-1;
		}
		
		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == 'true'){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;	
			}
		}
		if(!empty($in['search'])){
		    $filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%') ";
		    $arguments_s.="&search=".$in['search'];
		}

		if($in['payment_method_id'] && is_array($in['payment_method_id']) && !empty($in['payment_method_id'])){
			$payment_method="";
			if(count($in['payment_method_id'])==1 && $in['payment_method_id'][0]=='0'){
				foreach($locations as $key=>$value){
					$payment_method.="'".$value['id']."',";
				}
			}else{
				foreach($in['payment_method_id'] as $key=>$value){
					$payment_method.="'".$value."',";
				}		
			}
			$payment_method=rtrim($payment_method,",");
			$filter.= " AND tblinvoice_payments.payment_method IN (".$payment_method.") ";
		}

		if(!empty($in['start_date_js'])){
		    $in['start_date'] =strtotime($in['start_date_js']);
		    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
		}
		if(!empty($in['stop_date_js'])){
		    $in['stop_date'] =strtotime($in['stop_date_js']);
		    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
		}

		if(!empty($in['start_date']) && !empty($in['stop_date'])){
			$filter.=" and UNIX_TIMESTAMP(tblinvoice_payments.date) BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
		}else if(!empty($in['start_date'])){
		 	$filter.=" and UNIX_TIMESTAMP(tblinvoice_payments.date ) > ".$in['start_date']." ";
		}else if(!empty($in['stop_date'])){
		 	$filter.=" and UNIX_TIMESTAMP(tblinvoice_payments.date ) < ".$in['stop_date']." ";
		}	

		$payments_data=$db->query("SELECT tblinvoice_payments.*, tblinvoice.serial_number, tblinvoice.buyer_name, tblinvoice.currency_type
									FROM tblinvoice_payments 
									INNER JOIN tblinvoice ON tblinvoice_payments.invoice_id = tblinvoice.id ".$filter.$order_by);
		$max_rows=$payments_data->records_count();
		$payments_data->move_to($offset*$l_r);
		$j=0;
		$payment_methods_array = invoice_payment_method_dd();
		array_shift($payment_methods_array);
		while($payments_data->next() && $j<$l_r){
			$line=array();
			$line['id']=$payments_data->f('payment_id');
			$line['invoice_id']=$payments_data->f('invoice_id');
			$line['amount']=$payments_data->f('amount');
			$line['amount_currency']=get_commission_type_list($payments_data->f('currency_type')).' '.display_number($payments_data->f('amount'));
			$line['date']=date(ACCOUNT_DATE_FORMAT,strtotime($payments_data->f('date')));
			//$line['date']=$payments_data->f('date');
			$line['payment_method']=$payments_data->f('payment_method');
			$line['payment_method_name']=get_invoice_payment_method($payment_methods_array, $payments_data->f('payment_method'));
			$line['serial_number']=$payments_data->f('serial_number');
			$line['buyer_name']=$payments_data->f('buyer_name');
			$line['check_add_to_product']=$_SESSION['add_to_xls'][$payments_data->f('payment_id')] == 1 ? true : false;
			array_push($data['query'], $line);
			$j++;
		}

		$data['lr'] = $l_r;
		$data['max_rows'] = $max_rows;
		$data['payment_methods']=$payment_methods_array;
		return json_out($data, $showin,$exit);
	}

	function get_codabox_payments($in,$showin=true,$exit=true){

	global $config;
	$db = new sqldb();
	$data=array('query'=>array(),'coda_incoming'=>array(),'coda_clients'=>array());

	$ch = curl_init();
	$coda_api=$db->query("SELECT api,app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
	$coda_user=$db->field("SELECT api FROM apps WHERE type='username' AND main_app_id='".$coda_api->f('app_id')."' ");
	$coda_pass=$db->field("SELECT api FROM apps WHERE type='password' AND main_app_id='".$coda_api->f('app_id')."' ");
	$headers=array('X-Software-Company: '.$coda_api->f('api'));
	

	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	curl_setopt($ch, CURLOPT_USERPWD, $coda_user.":".$coda_pass);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/delivery/pod-client/');

	$put = curl_exec($ch);
	$info = curl_getinfo($ch);

	if($info['http_code']>300 || $info['http_code']==0){
	 	$resp_error=json_decode($put);
	 	$error_message = $resp_error->detail? $resp_error->detail: gm('Error');	 			
	 	msg::error($error_message,"error");
	 	//json_out($data); 
	 	return json_out($data, $showin,$exit);	
	}else{
		$feed=json_decode($put);
		foreach($feed->feed_clients as $key=>$value){
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, $coda_user.":".$coda_pass);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/delivery/feed/'.$value->id.'/');
			$put = curl_exec($ch);
			$info = curl_getinfo($ch);

			if($info['http_code']>300 || $info['http_code']==0){
			 	$resp_error=json_decode($put);	 						 	
			 	msg::error($resp_error->detail,"error");
			 	//json_out($data);	
			 	return json_out($data, $showin,$exit);		 	
			}else{
				$docs=json_decode($put);
				for($i=0;$i<count($docs->feed_entries);$i++){
					if($docs->feed_entries[$i]->document_model == 'coda'){
						array_push($data['coda_incoming'], $docs->feed_entries[$i]->feed_index);
						array_push($data['coda_clients'], $value->id);
					}				
				}
			}
		}
	}

	$l_r = ROW_PER_PAGE;
	$order_by_array = array('start_date');
	$order_by_array_line = array('date', 'amount', 'name', 'reference', 'bank', 'amount_left');
	$filter = "WHERE 1=1 ";
	$filter_arch= " ";
	$order_by=" ORDER BY start_date, id DESC "; 
	$lines_order_by=" ORDER BY date DESC "; 

	if((!$in['offset']) || (!is_numeric($in['offset']))){
	    $offset=0;
	    $in['offset']=1;
	}else{
	    $offset=$in['offset']-1;
	}
	if($in['archived']=='1'){
		$filter.= " AND (codapayments_line.archived='1' OR codapayments.status='1') ";
	}else{
		$filter.= " AND codapayments_line.archived='0' ";
	}
	if(!empty($in['order_by'])){
		/*if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == 'true'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;	
		}*/
		if(in_array($in['order_by'], $order_by_array_line)){
			$order = " ASC ";
			if($in['desc'] == 'true' || $in['desc'] == '1'){
				$order = " DESC ";
			}
			$lines_order_by =" ORDER BY ".$in['order_by']." ".$order;	
		}
	}

	if(!empty($in['search'])){
	    $filter.=" AND (codapayments_line.name LIKE '%".$in['search']."%' OR codapayments_line.reference LIKE '%".$in['search']."%' OR codapayments_line.bank LIKE '%".$in['search']."%') ";
	    $arguments.="&search=".$in['search'];
	}

	if(!isset($in['view'])){
	    $in['view'] = 0;
	}
	if($in['view'] == 0){
	    $filter.=" and codapayments_line.status = '0'  ";
	    $arguments.="&view=1";
	}
	if($in['view'] == 1){
	    $filter.=" and codapayments_line.status = '1' ";
	    $arguments.="&view=1";
	}
	if($in['view'] == 2){
	   $filter.=" and codapayments_line.status = '0'  and codapayments_line.amount > codapayments_line.amount_left ";
	    $arguments.="&view=2";
	}
	if($in['view'] == 3){
	    $filter.=" and codapayments_line.status = '0' and codapayments_line.amount = codapayments_line.amount_left ";
	    $arguments.="&view=3";
	}

	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";



	if(!empty($data['coda_incoming'])){
		$payments_data_check=$db->query("SELECT * FROM codapayments ORDER BY start_date DESC");
		while($payments_data_check->next()){
			if(in_array($payments_data_check->f('feed_index'),$data['coda_incoming'])){
				$key=array_search($payments_data_check->f('feed_index'),$data['coda_incoming']);
				array_splice($data['coda_incoming'],$key,1);
				array_splice($data['coda_clients'],$key,1);
			}
		}
	}
	

		$line_fetch=0;
		$i=0;
		$max_rows=$db->field("SELECT count(codapayments_line.id) FROM codapayments_line LEFT JOIN codapayments on codapayments_line.feed_index = codapayments.feed_id ".$filter.$filter_arch);
		$payment_line=$db->query("SELECT codapayments_line.*, codapayments.status as main_status  FROM codapayments_line LEFT JOIN codapayments on codapayments_line.feed_index = codapayments.feed_id ".$filter.$filter_arch.$lines_order_by.$filter_limit);
		while($payment_line->next()){
			if($payment_line->f('amount')>0){
				if($payment_line->f('status')==0){
					if($payment_line->f('amount')==$payment_line->f('amount_left')){
						$status=gm('Not Linked');
						$status_nr=0;
					}else{
						$status=gm('Partially Linked');
						$status_nr=2;
					}
				}else{
					$status=gm('Linked');
					$status_nr=1;
					$line_fetch++;
				}
				if($payment_line->f('archived')==1){
					
						$status=gm('Archived');
						$status_nr=0;
				}

				$linked_invoices = array();
				$inv_nr = $db->query("SELECT tblinvoice_payments.invoice_id,tblinvoice.serial_number FROM tblinvoice_payments												INNER JOIN tblinvoice ON tblinvoice_payments.invoice_id = tblinvoice.id
											WHERE tblinvoice_payments.codapayment_id = '".$payment_line->f('id')."'	")->getAll();

				foreach($inv_nr as $key=>$value){					  
				    
			    	array_push($linked_invoices, array( 'serial' => $value['serial_number'], 'invoice_id'=> $value['invoice_id']));
			    }


				$query=array(
						'id'				=> $payment_line->f('id'),
						'name'				=> $payment_line->f('name'),
						'bank'				=> $payment_line->f('bank'),
						'amount'			=> display_number($payment_line->f('amount')),
						'currency'			=> $payment_line->f('currency'),
						'amount_currency'	=> get_currency_value_by_code($payment_line->f('currency')).' '.display_number($payment_line->f('amount')),
						'date'				=> date(ACCOUNT_DATE_FORMAT,$payment_line->f('date')),
						'reference'			=> $payment_line->f('reference'),
						'status'			=> $status,
						'nr_status'			=> $status_nr,
						'balance'			=> get_currency_value_by_code($payment_line->f('currency')).' '.display_number($payment_line->f('amount_left')),
						'invoices'			=> $linked_invoices
				);
				array_push($data['query'],$query);
				$i++;
			}
		}
		
	$data['lr'] = $l_r;
	$data['max_rows'] = $max_rows;

	//json_out($data);
	return json_out($data, $showin,$exit);
	}

	$coda_active_pay =false;
	$codabox=$db->query("SELECT app_id,active FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
	if($codabox->f('active')){
	    $payments_check=$db->field("SELECT api FROM apps WHERE type='payments' AND main_app_id='".$codabox->f('app_id')."' ");
	    $coda_active_pay=is_null($payments_check) || $payments_check==1? true : false;
	}

	$result = array(
		'payments'				=> get_payments($in,true,false),
		//'codabox_payments'		=> get_codabox_payments($in,true,false),
		'codabox_active'		=> $coda_active_pay,
	);
json_out($result);
?>