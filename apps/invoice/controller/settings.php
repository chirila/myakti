<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}


/*	function get_logos($in,$showin=true,$exit=true){
		$db = new sqldb();
		$adv_sepa=false;
		if(defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1){
			if(defined('ADV_SEPA') && ADV_SEPA==1){
				$adv_sepa=true;
			}
		}else{
			$adv_sepa=true;AND sort_order != ''
		}
		$data = array( 'logos'=>array(), 'logo'=>array(), 'adv_sepa'=>$adv_sepa);
		$default_logo = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO' ");
		if($default_logo == '' || $default_logo == 'pim/img/no-logo.png')   {
		   	array_push($data['logo'], array(
        'account_logo'=>'images/no-logo.png',
        'default'=>'images/no-logo.png'
        ));
		}
		else {
			$logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/{logo_img}_*',GLOB_BRACE);
		    foreach ($logos as $v) {
		        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
		        $size = @getimagesize($logo);
		        $ratio = 250 / 77;
		        if($size[0]/$size[1] > $ratio ){
		            $attr = 'width="250"';
		        }else{
		            $attr = 'height="77"';
		        }
		        array_push($data['logos'], array(
		        'account_logo'=>$logo,
		        'default'=>$default_logo,
		        'attr' => $attr
		        ));
		    }
		}
		$data['default_logo'] = $default_logo;


			
		return json_out($data, $showin,$exit);
	}*/
	function get_PDFlayout($in,$showin=true,$exit=true){
		$db = new sqldb();

		global $database_config;
		$database = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
				);
		$dbu_users =  new sqldb($database);

		$adv_sepa=false;
		if(defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1){
			if(defined('ADV_SEPA') && ADV_SEPA==1){
				$adv_sepa=true;
			}
		}else{
			$adv_sepa=true;
		}

		$data = array( 'layout_header'=>array(),'custom_layout'=>array(),'layout_body'=>array(),'layout_footer'=>array(),'adv_sepa'=>$adv_sepa);
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_PDF_FORMAT' ");
		$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
		//$in['identity_id'] = $db->field("SELECT value FROM settings WHERE constant_name='INVOICE_IDENTITY_SET' ");
		if(!$in['identity_id']){
			// $identity = $db->field("SELECT identity_id FROM multiple_identity");
			$in['identity_id']='0';
		}

		$data['has_custom_layout']=false;
		if(DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11' || DATABASE_NAME=='9f56e245_4434_157d_9f06320b4bb8' || DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422' || DATABASE_NAME=='7b5cc5d1_e3b4_b17c_be982b8dc3ae'
			|| DATABASE_NAME=='03a0fb2c_c144_cda3_adda851c8e4b' || DATABASE_NAME=='b7b2acca_6944_a954_7151a7548069' || DATABASE_NAME=='1008e331_1c14_2d80_8c2d63f6b832' || DATABASE_NAME=='0de7a47b_be24_9551_7c8275f7d0ba' || DATABASE_NAME=='1b482df5_cad4_6157_b7839ba4b514' || DATABASE_NAME=='1ad760a7_3df4_a15e_099e6ba271d5' || DATABASE_NAME=='c4482c81_7d14_c57e_2068878ad2f4' ){
			$data['has_custom_layout']=true;
		}
		$existIdentity = $db->query("SELECT identity_id FROM identity_layout  ");
		$data['multiple_identity']				= build_identity_dd($in['identity_id']);
		$data['identity']						= $in['identity_id'];

		$data['use_custom_layout']= ($use ==1)? true:false;

		for ($j=1; $j <= 1; $j++) {
		array_push($data['custom_layout'], array(
				'view_invoice_custom_pdf' 						=>'index.php?do=invoice-print&custome_type='.$j.'&lid='.$language,
				'img_href_custom'								=> 'images/custom_type-'.$j.'.jpg',
				'custom_type'									=> $j,
				//'action_href'									=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$j.'&tab=6',
				'selected_custom'								=> $def == $j && $use == 1 ? 'active' : '',
			));
		}
		$bank_details = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_BANK_DETAILS'");
		$stamp = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_STAMP_SIGNATURE'");
		$page_numbering = $db->field("SELECT value FROM settings WHERE constant_name='USE_INVOICE_PAGE_NUMBERING'");
		$paid = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_PAID_ON_INVOICE_PDF'");
		$data['show_bank']							= $bank_details 	== '1' ? true : false;
		$data['show_stamp']							= $stamp 			== '1' ? true : false;
		$data['show_page']							= $page_numbering 	== '1' ? true : false;
		$data['show_paid']							= $paid 			== '1' ? true : false;
		//

		$pdf_active = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_PDF'");
		$pdf_note = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_NOTE'");
		$pdf_conditions = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_CONDITIONS'");
		$pdf_stamp = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_SIGNATURE'");
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$data['lang_id']=$language;
		$data['pdf_active'] = $pdf_active == '1' ? true : false;
		$data['pdf_note'] = $pdf_note == '1' ? true : false;
		$data['pdf_condition'] = $pdf_conditions == '1' ? true : false;
		$data['pdf_stamp'] = $pdf_stamp == '1' ? true : false;

		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
		$number_type = $_SESSION['u_id']<10737 ? 2 : 1;

		//$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		if(!$user->f('main_user_id')){
			if($user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $_SESSION['u_id']<10737 ? 2 : 1;
			}
		}else{
			$main_user= $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$user->f('main_user_id')."' ");
			if($main_user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $user->f('main_user_id')<10737 ? 2 : 1;
			}
		}

		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&header='.$data['header'].'&layout='.$number_type.'&lid='.$language.'&header_id='.$def_header.'&save_as=preview',
					'img_href'						=> 'images/type_header-'.$number_type.'.jpg',
					'type'							=> $z,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&layout='.$number_type.'&lid='.$language.'&body='.$data['body'].'&save_as=preview',
					'img_href'						=> 'images/type_body-'.$number_type.'.jpg',
					'type'							=> $number_type,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',
					'body'							=> 'body',
					'identity_id'					=> $in['identity_id'],
				));
		}
		if($number_type==2){
			for ($i=$def_footer; $i <= $def_footer; $i++) {
					array_push($data['layout_footer'], array(
						'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
						'img_href'						=> 'images/type_footer-'.$def_footer.'.jpg',
						'type'							=> $i,
						'footer'						=> 'footer',
						//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
						'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
						'identity_id'					=> $in['identity_id'],
					));
			}
		}else{
			for ($i=2; $i <= 2; $i++) {
					array_push($data['layout_footer'], array(
						'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
						'img_href'						=> 'images/type_footer-2.jpg',
						'type'							=> $i,
						'footer'						=> 'footer',
						//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
						'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
						'identity_id'					=> $in['identity_id'],
					));
			}
		}
		

		$data['selected_header'] = $def_header;
		$data['selected_body'] = $def_body;
		$data['selected_footer'] = $def_footer;




		//

		return json_out($data, $showin,$exit);
	}
	function get_PDFlayoutCredit($in,$showin=true,$exit=true){
		$db = new sqldb();
		global $database_config;
		$database = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
				);
		$dbu_users =  new sqldb($database);

		$data = array( 'layout_credit_header'=>array(),'layout_credit_body'=>array(),'layout_credit_footer'=>array(), 'custom_credit_layout'=>array());
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$data['lang_id']=$language;

		$data['has_custom_credit_layout']=false;
		if(DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11' || DATABASE_NAME=='9f56e245_4434_157d_9f06320b4bb8' || DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422' || DATABASE_NAME=='7b5cc5d1_e3b4_b17c_be982b8dc3ae'
			|| DATABASE_NAME=='03a0fb2c_c144_cda3_adda851c8e4b' || DATABASE_NAME=='b7b2acca_6944_a954_7151a7548069' || DATABASE_NAME=='0de7a47b_be24_9551_7c8275f7d0ba' || DATABASE_NAME=='1b482df5_cad4_6157_b7839ba4b514' || DATABASE_NAME=='1ad760a7_3df4_a15e_099e6ba271d5' || DATABASE_NAME=='c4482c81_7d14_c57e_2068878ad2f4'){
			$data['has_custom_credit_layout']=true;
		}
		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_CREDIT_PDF_FORMAT' ");
		$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");

		if(!$in['identity_id']){
			$in['identity_id']='0';
		}

		$data['multiple_identity']				= build_identity_dd($in['identity_id']);
		$data['identity']						= $in['identity_id'];

		$data['use_custom_credit_layout']= ($use ==1)? true:false;

		for ($j=1; $j <= 1; $j++) {
		array_push($data['custom_credit_layout'], array(
				'view_invoice_credit_custom_pdf' 				=>'index.php?do=invoice-print&custome_type='.$j.'&lid='.$language,
				'img_href_custom'								=> 'images/custom_type-'.$j.'.jpg',
				'custom_credit_type'							=> $j,
				//'action_href'									=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$j.'&tab=6',
				'selected_custom_credit'						=> $def == $j && $use == 1 ? 'active' : '',
			));
		}

		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_CREDIT_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_CREDIT_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_CREDIT_PDF' ");
		//$number_type = 2;
		$number_type = $_SESSION['u_id']<10737 ? 2 : 1;

		//$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		if(!$user->f('main_user_id')){
			if($user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $_SESSION['u_id']<10737 ? 2 : 1;
			}
		}else{
			$main_user= $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$user->f('main_user_id')."' ");
			if($main_user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $user->f('main_user_id')<10737 ? 2 : 1;
			}
		}

		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_credit_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_credit_print&header='.$data['header'].'&layout='.$z.'&lid='.$language.'&header_id='.$def_header.'&save_as=preview',
					'img_href'						=> 'images/type_header-'.$number_type.'.jpg',
					'type'							=> $z,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_credit_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_credit_print&layout='.$y.'&lid='.$language.'&body='.$data['body'].'&save_as=preview',
					'img_href'						=> 'images/type_body-'.$number_type.'.jpg',
					//'type'							=> $y,
					'type'							=> $number_type,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',
					'body'							=> 'body',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($i=2; $i <= 2; $i++) {
				array_push($data['layout_credit_footer'], array(
					'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_credit_print&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
					'img_href'						=> 'images/type_footer-'.$i.'.jpg',
					'type'							=> $i,
					'footer'						=> 'footer',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		

		$data['selected_credit_header'] = $def_header;
		$data['selected_credit_body'] = $def_body;
		$data['selected_credit_footer'] = $def_footer;




		//

		return json_out($data, $showin,$exit);
	}

	function get_PDFlayoutReminder($in,$showin=true,$exit=true){
		$db = new sqldb();

		global $database_config;
		$database = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
				);
		$dbu_users =  new sqldb($database);

		$adv_sepa=false;
		if(defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1){
			if(defined('ADV_SEPA') && ADV_SEPA==1){
				$adv_sepa=true;
			}
		}else{
			$adv_sepa=true;
		}

		$data = array( 'layout_reminder_header'=>array(),'custom_layout'=>array(),'layout_reminder_body'=>array(),'layout_reminder_footer'=>array(),'adv_sepa'=>$adv_sepa);
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_REMINDER_PDF_FORMAT' ");
		
		//$in['identity_id'] = $db->field("SELECT value FROM settings WHERE constant_name='INVOICE_IDENTITY_SET' ");
		if(!$in['identity_id']){
			// $identity = $db->field("SELECT identity_id FROM multiple_identity");
			$in['identity_id']='0';
		}

		$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");
		$data['has_custom_reminder_layout']=false;
		$data['use_custom_layout']= ($use ==1)? true:false;
/*		if(DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11' || DATABASE_NAME=='9f56e245_4434_157d_9f06320b4bb8' || DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422' || DATABASE_NAME=='7b5cc5d1_e3b4_b17c_be982b8dc3ae'){
			$data['has_custom_layout']=true;
		}*/
		$existIdentity = $db->query("SELECT identity_id FROM identity_layout  ");
		$data['multiple_identity']				= build_identity_dd($in['identity_id']);
		$data['identity']						= $in['identity_id'];

		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_REMINDER_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_REMINDER_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_REMINDER_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_INVOICE_REMINDER_PDF' ");
		$number_type = $_SESSION['u_id']<10737 ? 2 : 1;

		//$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		if(!$user->f('main_user_id')){
			if($user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $_SESSION['u_id']<10737 ? 2 : 1;
			}
		}else{
			$main_user= $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$user->f('main_user_id')."' ");
			if($main_user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $user->f('main_user_id')<10737 ? 2 : 1;
			}
		}

		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_reminder_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&reminder=1&header='.$data['header'].'&layout='.$number_type.'&lid='.$language.'&header_id='.$def_header.'&save_as=preview',
					'img_href'						=> 'images/type_header-'.$number_type.'.jpg',
					'type'							=> $z,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_reminder_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&reminder=1&layout='.$number_type.'&lid='.$language.'&body='.$data['body'].'&save_as=preview',
					'img_href'						=> 'images/type_body-'.$number_type.'.jpg',
					'type'							=> $number_type,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					/*'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',*/
					'selected'						=>  '',
					'body'							=> 'body',
					'identity_id'					=> $in['identity_id'],
				));
		}
		if($number_type==2){
			for ($i=$def_footer; $i <= $def_footer; $i++) {
					array_push($data['layout_reminder_footer'], array(
						'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&reminder=1&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
						'img_href'						=> 'images/type_footer-'.$def_footer.'.jpg',
						'type'							=> $i,
						'footer'						=> 'footer',
						//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
						'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
						'identity_id'					=> $in['identity_id'],
					));
			}
		}else{
			for ($i=2; $i <= 2; $i++) {
					array_push($data['layout_reminder_footer'], array(
						'view_invoice_pdf' 				=>'index.php?do=invoice-invoice_print&reminder=1&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
						'img_href'						=> 'images/type_footer-2.jpg',
						'type'							=> $i,
						'footer'						=> 'footer',
						//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
						'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
						'identity_id'					=> $in['identity_id'],
					));
			}
		}
		

		$data['selected_reminder_header'] = $def_header;
		$data['selected_reminder_body'] = $def_body;
		$data['selected_reminder_footer'] = $def_footer;




		//

		return json_out($data, $showin,$exit);
	}

	function get_Convention($in,$showin=true,$exit=true){
		global $database_config;
		$db = new sqldb();
		$database_2 = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		    );
		$dbu_users =  new sqldb($database_2);
		$force_draft=false;
		if($_SESSION['main_u_id']=='0'){
			//$is_accountant=$dbu_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");					
			$is_accountant=$dbu_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		}else{
			$is_accountant=$dbu_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
		}
		if($is_accountant){
			$accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
			if($accountant_settings=='1'){
				$acc_force=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_FORCE_DRAFT' ");
				if($acc_force){
					$force_draft=$acc_force;
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					      $db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					      $db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}else{
				$force_draft=$dbu_users->field("SELECT force_draft FROM accountants WHERE account_id='".$is_accountant."' ");
				if($force_draft){
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					      $db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					      $db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}		
		}
		$account_number = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_START' ");
		$account_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DIGIT_NR' ");
		$account_credit = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CREDIT_INVOICE_START'");
		$account_credit_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CREDIT_DIGIT_NR'");
		$account_purchase = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_INVOICE_START'");
		$account_purchase_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_DIGIT_NR'");
		$use_credit_number = $db->field("SELECT value FROM settings WHERE constant_name='USE_CREDIT_PURCHASE_NO'");
		$account_purchase_credit = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_INVOICE_START'");
		$account_purchase_credit_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_DIGIT_NR'");
		$account_reference = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_REF' ");
		$account_del = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_DEL' ");
		$payment_instruction = $db->field("SELECT value FROM settings WHERE constant_name='USE_OGM'");
		$draft_invoices = $db->field("SELECT value FROM settings WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
		$random_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION'");
		$nr_account = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION_NR'");
		$account_proforma = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROFORMA_INVOICE_START' ");
		$account_proforma_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROFORMA_DIGIT_NR' ");
		$invoice_starting_number=$db->field("SELECT value FROM settings WHERE constant_name='INVOICE_STARTING_NUMBER' ");
		$data['last_serial_number'] =$nr_account;
		$in['last_serial_number'] = get_last_invoice_number(true);
		$data['account_number']=$account_number;
		$data['random_digits'] = $random_digits;
		$data['invoice_starting_number'] = is_null($invoice_starting_number) ? 1 : $invoice_starting_number;
		$data['account_digits']=$account_digits;
		$data['account_credit']=$account_credit;
		$data['account_credit_digits']=$account_credit_digits;
		$data['account_proforma']=$account_proforma;
		$data['account_proforma_digits']=$account_proforma_digits;
		$data['account_purchase']=$account_purchase;
		$data['account_purchase_digits']=$account_purchase_digits;
		$data['use_credit_number']=$use_credit_number == 1 ? true : false;
		$data['account_purchase_credit']=$account_purchase_credit;
		$data['account_purchase_credit_digits']=$account_purchase_credit_digits;
		$data['example']=$account_number.str_pad(1,$account_digits,"0",STR_PAD_LEFT);
		$data['example2']=$account_credit.str_pad(1,$account_credit_digits,"0",STR_PAD_LEFT);
		$data['example3']=$account_purchase.str_pad(1,$account_purchase_digits,"0",STR_PAD_LEFT);
		$data['example4']=$account_purchase_credit.str_pad(1,$account_purchase_credit_digits,"0",STR_PAD_LEFT);
		$data['example_proforma']=$account_proforma.str_pad(1,$account_proforma_digits,"0",STR_PAD_LEFT);
		$data['reference'] = $account_reference == '1' ? true : false;
		$data['del'] =$account_del;
		$data['payment_instruction'] =$payment_instruction == '1' ? true : false;
		$data['draft_invoices']=$draft_invoices == '1' ? true : false;
		$data['block_draft']=$force_draft ? true : false;
		$example = '';
		if($random_digits == 1){
			$nr = time();
			$first_part = substr($nr,0,3);$second_part = substr($nr,3,4); $last_part = substr($nr,7,3);
			$calc =$first_part.$second_part.$last_part;
			$calc = $calc%97;
			if($calc < 10){
				$calc = '0'.$calc;
			}
			// $s = strpos($calc,'.');
			// $calc = substr($calc,$s+1,2);
			$example = $first_part.'/'.$second_part.'/'.$last_part.$calc;
		}

		if($random_digits == 2){
			$nr = $nr_account;$nr2=$in['last_serial_number'];$nr3='';$calc=0;

			if(strlen($nr) > 3){
				$nr2 = substr($nr,3).$nr2;
				$nr= substr($nr,0,3);
			}else{
				$l = 3-strlen($nr);
				$nr.=substr($nr2,0,$l);
				$nr2 = substr($nr2,$l);
			}
			if(strlen($nr2) > 4){
				$nr3 = substr($nr2,4);
				$nr2 = substr($nr2,0,4);
			}else{
				$l = 4-strlen($nr2);
				for($i=1;$i<=$l;$i++){
					$nr2 .= '0';
				}
			}
			if(strlen($nr3) < 3){
				$l = 3-strlen($nr3);
				for($i=1;$i<=$l;$i++){
					$nr3 .= '0';
				}
			}
			$calc= $nr.$nr2.$nr3;
			console::log($calc);
			$calc = $calc%97;
			console::log($calc);
			if($calc < 10){
				$calc = '0'.$calc;
			}
			// $s = strpos($calc,'.');
			// $calc = substr($calc,$s+1,2);
			$example = $nr.'/'.$nr2.'/'.$nr3.$calc;
		}
		$data['example5']=$example;
		return json_out($data, $showin,$exit);
	}
	function get_invoiceNotes($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('invoice_general_conditions'=>array());
		$filter = '';
		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
			if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
			}
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
				$filter = '_'.$in['languages'];
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
				if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
				}
			}
		}

		$default_table = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='invoice_note".$filter."' ");
		$terms = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='invoice_terms".$filter."' ");
		$page = $db->field("SELECT value FROM settings WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE'");
		$gen_notes = $db->field("SELECT value FROM settings WHERE constant_name='INVOICE_NOTES_AND_GENERAL_CONDITIONS'");

		$data['invoice_general_conditions']= array(
			'page'			=> $page == 1 ? true : false,
			'gen_notes'			=> $gen_notes == 1 ? true : false,
			'terms'			=> $terms,
			'notes'			=> $default_table,
			'translate_cls'	=> 'form-language-'.$code,
			'languages'		=>  $in['languages'],
			'old_languages_value'	=> $in['languages'],
			'do'			=> 'invoice-settings-invoice-default_language',
			'xget'			=> 'invoiceNotes',
			'language_dd' 	=>  build_language_dd_new($in['languages']),
			'save_button_text' =>gm('Save'),
			'type' 			=> 'invoice',
			'disabled_button' =>false
		);
		return json_out($data, $showin,$exit);
	}
	function get_creditNotes($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('credit_general_conditions'=>array());
		$filter = '';
		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
			if($in['languages'] != 1){
				$filter = '_'.$in['languages'];
			}
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
				$filter = '_'.$in['languages'];
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
				if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
				}
			}
		}

		$default_table = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='credit_note".$filter."' ");
		$terms = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='credit_terms".$filter."' ");
		$page = $db->field("SELECT value FROM settings WHERE constant_name='CREDIT_GENERAL_CONDITION_PAGE'");
		$gen_notes = $db->field("SELECT value FROM settings WHERE constant_name='CREDIT_NOTES_AND_GENERAL_CONDITIONS'");

		$data['credit_general_conditions']= array(
			'page'			=> $page == 1 ? true : false,
			'gen_notes'			=> $gen_notes == 1 ? true : false,
			'terms'			=> $terms,
			'notes'			=> $default_table,
			'translate_cls'	=> 'form-language-'.$code,
			'languages'		=>  $in['languages'],
			'old_languages_value'	=> $in['languages'],
			'do'			=> 'invoice-settings-invoice-default_language',
			'xget'			=> 'creditNotes',
			'language_dd' 	=>  build_language_dd_new($in['languages']),
			'save_button_text' =>gm('Save'),
			'type' 			=> 'credit',
			'disabled_button' =>false
		);
		return json_out($data, $showin,$exit);
	}
	function get_customLabel($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'customLabel'=>array());

		$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
		while($pim_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 		=> gm($pim_lang->f('language')),
			'do'							=> 'invoice-settings',
			'xget'							=> 'labels',
			'label_language_id'	     		=> $pim_lang->f('lang_id'),
			'label_custom_language_id'	=> '',
			));
		}
		$pim_custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
		while($pim_custom_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 			=> $pim_custom_lang->f('language'),
			'do'								=> 'invoice-settings',
			'xget'								=> 'labels',
			'label_custom_language_id'	    	=> $pim_custom_lang->f('lang_id'),
			'label_language_id'	     			=> '',
			));
		}

		return json_out($data, $showin,$exit);
	}
	function get_labels($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$const = array();
		$custom_language = 0;

		$table = 'label_language';

		if($in['label_language_id'])
		{
			$filter = "WHERE label_language_id='".$in['label_language_id']."'";
			$id = $in['label_language_id'];
		}elseif($in['label_custom_language_id'])
		{
			$filter = "WHERE lang_code='".$in['label_custom_language_id']."'";
			$id = $in['label_custom_language_id'];
			$custom_language = 1;
		}

		$db->query("SELECT * FROM $table $filter");

		$data['labels']=array(
				'label_language_id'  	=>	$db->f('label_language_id'),
				'invoice_note'		 	=>	$db->f('invoice_note'),
				'invoice'	         	=> $db->f('invoice'),
				'description'	     	=> $db->f('description'),
				'terms_anexe'	     	=> $db->f('terms_anexe'),
				'invoice_type'	     	=> $db->f('invoice_type'),
				'payment_term_type_1'	=> $db->f('payment_term_type_1'),
				'payment_term_type_2'	=> $db->f('payment_term_type_2'),
				'payment_condition'	    => $db->f('payment_condition'),
				'billing_information'	     => $db->f('billing_information'),
				'billing_address'	 => $db->f('billing_address'),
				'date'	             => $db->f('date'),
				'customer'	         => $db->f('customer'),
				'item'	             => $db->f('item'),
				'quantity'	         => $db->f('quantity'),
				'unit_price'	     => $db->f('unit_price'),
				'amount'	         => $db->f('amount'),
				'subtotal'	         => $db->f('subtotal'),
				'subtotal_no_vat'	 => $db->f('subtotal_no_vat'),
				'discount'	         => $db->f('discount'),
				'vat'	             => $db->f('vat'),
				'payments'	         => $db->f('payments'),
				'amount_due'	     => $db->f('amount_due'),
				'notes'	             => $db->f('notes'),
				'notes2'	         => $db->f('invoice_note2'),
				'bankd'				 => $db->f('bank_details'),
				'duedate'			 => $db->f('duedate'),
				'bank_name'			 => $db->f('bank_name'),
				'iban'				 => $db->f('iban'),
				'bic_code'			 => $db->f('bic_code'),
				'phone'				 => $db->f('phone'),
				'fax'				 => $db->f('fax'),
				'url'				 => $db->f('url'),
				'attention_of'				 => $db->f('attention_of'),
				'email'				 => $db->f('email'),
				'our_ref'			 => $db->f('our_ref'),
				'your_ref'			 => $db->f('your_ref'),
				'vat_number'		 => $db->f('vat_number'),
				'pay_instructions'	 => $db->f('pay_instructions'),
				'credit_pay_instructions'	 => $db->f('credit_pay_instructions'),
				'pro_invoice'		 => $db->f('pro_invoice'),
				'net_amount'		 => $db->f('net_amount'),
				'credit_inv'		 => $db->f('credit_inv'),
				'credit_number'		 => $db->f('credit_number'),
				'address_txt'		 => $in['type'] == 2 ? gm('Delivery Address') : gm('Billing Address'),
				'page'		 		=> $db->f('page'),
				'due_date_vat'		 		=> $db->f('due_date_vat'),
				'comp_reg_number'				=> $db->f('comp_reg_number'),
				'stamp_signature'				=> $db->f('stamp_signature'),

				'paymentError_webl'		 			=> $db->f('paymentError_webl'),
				'paymentErrorContact_webl'		 	=> $db->f('paymentErrorContact_webl'),
				'paymentPending_webl'		 		=> $db->f('paymentPending_webl'),
				'paymentPendingContact_webl'		=> $db->f('paymentPendingContact_webl'),
				'paymentConfirmation_webl'		 	=> $db->f('paymentConfirmation_webl'),
				'paymentConfirmationContact_webl'	=> $db->f('paymentConfirmationContact_webl'),
				'creditNote_webl'		 			=> $db->f('creditNote_webl'),
				'invoice_webl'		 				=> $db->f('invoice_webl'),
				'date_webl'		 					=> $db->f('date_webl'),
				'invoiceBalance_webl'		 		=> $db->f('invoiceBalance_webl'),
				'proformaBalance_webl'		 		=> $db->f('proformaBalance_webl'),
				'paidObsProforma_webl'		 		=> $db->f('paidObsProforma_webl'),
				'paidObsInvoice_webl'		 		=> $db->f('paidObsInvoice_webl'),
				'regularInvFromProforma_webl'		=> $db->f('regularInvFromProforma_webl'),
				'bankDetails_webl'		 			=> $db->f('bankDetails_webl'),
				'bankName_webl'		 				=> $db->f('bankName_webl'),
				'bicCode_webl'		 				=> $db->f('bicCode_webl'),
				'ibanCode_webl'		 				=> $db->f('ibanCode_webl'),
				'noData_webl'		 				=> $db->f('noData_webl'),
				'ogm'								=> $db->f('ogm'),
				'paid'								=> $db->f('paid'),
				'reminder_due'						=> $db->f('reminder_due'),
				'additional_ref'					=> $db->f('additional_ref'),
				'req_payment'						=> $db->f('req_payment'),
				'row_amount_no_vat'					=> $db->f('row_amount_no_vat'),
				'row_amount_with_vat'				=> $db->f('row_amount_with_vat'),
				'do'								=> 'invoice-settings-invoice-label_update',
				'xget'								=> 'labels',

			'label_language_id'				=> $id,
			'custom_language'				=> $custom_language,
		);
		return json_out($data, $showin,$exit);
	}
	function get_emailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array(),'message_labels'=>array(),'saved'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Invoice No");
		$array_values['INVOICE_DATE']=gm("Invoice Date");
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['DISCOUNT']=gm("Discount Percent");
		$array_values['DISCOUNT_VALUE']=gm("Discount Value");
		$array_values['PAYMENTS']=gm("Payments");
		$array_values['AMOUNT_DUE']=gm("Amount due");
		$array_values['OGM']=gm("Structured message");
		$array_values['WEB_LINK']=gm("Web Link");
		$array_values['WEB_LINK_2']=gm("Short Weblink");
		$array_values['WEB_LINK_URL']=gm("Weblink URL");
		$array_values['DUE_DATE']=gm("Due Date");
		$array_values['INVOICE_TYPE']=gm("Invoice type");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['LOGO']=gm("Logo");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['CONTACT_FIRST_NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST_NAME']=gm("Contact last name");
		$array_values['OWN_REFERENCE']=gm("Your own reference");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);


$is_normal = true;
if($code=='nl'){
	$code='du';
}
$in['lang_code'] = $code;
$name = 'invmess';
$text = '-----------------------------------
Invoice Summary
-----------------------------------
Invoice ID: [!SERIAL_NUMBER!]
Date: [!INVOICE_DATE!]
Customer: [!CUSTOMER!]
Discount ([!DISCOUNT!]):  [!DISCOUNT_VALUE!]
Payments: [!PAYMENTS!]
Amount Due:  [!AMOUNT_DUE!]

The detailed invoice is attached as a PDF.

Thank you!
-----------------------------------';
$subject = 'Invoice #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'invoice-settings-invoice-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'invoice-settings-invoice-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}

		$data['saved']= array(
				'save_button_text'        			=> gm('Save'),
				'disabled_button'           			=> false
		);



		return json_out($data, $showin,$exit);
	}

	function get_emailMessageCredit($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array(),'message_labels'=>array(),'saved'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Invoice No");
		$array_values['INVOICE_DATE']=gm("Invoice Date");
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['DISCOUNT']=gm("Discount Percent");
		$array_values['DISCOUNT_VALUE']=gm("Discount Value");
		$array_values['PAYMENTS']=gm("Payments");
		$array_values['AMOUNT_DUE']=gm("Amount due");
		$array_values['WEB_LINK']=gm("Web Link");
		$array_values['WEB_LINK_2']=gm("Short Weblink");
		$array_values['WEB_LINK_URL']=gm("Weblink URL");
		$array_values['DUE_DATE']=gm("Due Date");
		$array_values['INVOICE_TYPE']=gm("Invoice type");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['LOGO']=gm("Logo");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['CONTACT_FIRST_NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST_NAME']=gm("Contact last name");
		$array_values['OWN_REFERENCE']=gm("Your own reference");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);


$is_normal = true;
if($code=='nl'){
	$code='du';
}
$in['lang_code'] = $code;
$name = 'invmess_credit';
$text = '-----------------------------------
Invoice Summary
-----------------------------------
Invoice ID: [!SERIAL_NUMBER!]
Date: [!INVOICE_DATE!]
Customer: [!CUSTOMER!]
Discount ([!DISCOUNT!]):  [!DISCOUNT_VALUE!]
Payments: [!PAYMENTS!]
Amount Due:  [!AMOUNT_DUE!]

The detailed invoice is attached as a PDF.

Thank you!
-----------------------------------';
$subject = 'Invoice #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'invoice-settings-invoice-default_message',
				'xget'						=> 'emailMessageCredit',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'invoice-settings-invoice-default_message',
				'xget'						=> 'emailMessageCredit',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}

		$data['saved']= array(
				'save_button_text'        			=> gm('Save'),
				'disabled_button'           			=> false
		);



		return json_out($data, $showin,$exit);
	}

	function get_defaultEmail($in,$showin=true,$exit=true){
			$db = new sqldb();
			$default_email = $db->query("SELECT * FROM default_data WHERE type = 'invoice_email' ");
			$default_email1 = $db->query("SELECT * FROM default_data WHERE type = 'invoice_manager_email' ");
			$default_email_bcc = $db->query("SELECT * FROM default_data WHERE type = 'bcc_invoice_email' ");
			$data['email_default']= array(
				'default_name'	=> $default_email->gf('default_name'),
				'email_value'	=> $default_email->gf('value'),
				'manag_email'	=> $default_email1->gf('value'),
				'bcc_email'		=> $default_email_bcc->gf('value'),
				'do'			=> 'invoice-settings-invoice-update_default_email',
				'xget'			=> 'defaultEmail',
			);
			return json_out($data, $showin,$exit);
	}
	function get_weblink($in,$showin=true,$exit=true){
			$db = new sqldb();
			$invoice_link = $db->field("SELECT value FROM settings WHERE constant_name='USE_INVOICE_WEB_LINK' ");
			$invoice_balance = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_BALANCE' ");
			$invoice_comment = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_INVOICE_COMMENTS' ");
			$invoice_pdf = $db->field("SELECT value FROM settings WHERE constant_name='WEB_INCLUDE_PDF' ");
			$invoice_payment = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_WEB_PAYMENT' ");
			$invoice_xml = $db->field("SELECT value FROM settings WHERE constant_name='WEB_INCLUDE_XML' ");

			$icepay_merchant_id = $db->field("SELECT value FROM settings WHERE constant_name='ICEPAY_MERCHANTID2' ");
			$icepay_secret_code = $db->field("SELECT value FROM settings WHERE constant_name='ICEPAY_SECRETCODE2' ");
			$icepay_active = $db->field("SELECT value FROM settings WHERE constant_name='ICEPAY_ACTIVE2' ");
			$stripe_active=$db->field("SELECT value FROM settings WHERE constant_name='STRIPE_ACTIVE2' ");
			$mollie_active=$db->field("SELECT value FROM settings WHERE constant_name='MOLLIE_ACTIVE2' ");
			$stripe_app_active=$db->field("SELECT active FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0'");
			$icepay_app_active=$db->field("SELECT active FROM apps WHERE name='Icepay' AND type='main' AND main_app_id='0'");
			$mollie_app_active=$db->field("SELECT active FROM apps WHERE name='Mollie' AND type='main' AND main_app_id='0'");
			
			$pays_activated=0;
			if($stripe_app_active){
				$pays_activated++;
			}
			if($icepay_app_active || $icepay_active){
				$pays_activated++;	
			}
			if($mollie_app_active){
				$pays_activated++;	
			}

			global $config;
			$data['weblink']= array(
				'invoice_link'					=>$invoice_link == '1' ? true : false,
				'invoice_balance'				=>$invoice_balance == '1' ? true : false,
				'invoice_pdf'					=>$invoice_pdf == '1' ? true : false,
				'invoice_comment'				=>$invoice_comment == '1' ? true : false,
				'invoice_payment'				=>$invoice_payment == '1' ? true : false,
				'stripe_active'					=>$stripe_active == '1' ? true : false,
				'icepay_active'					=>$icepay_active == '1' ? true : false,
				'mollie_active'					=>$mollie_active == '1' ? true : false,
				'stripe_activated'				=> $stripe_app_active ? true : false,
				'icepay_activated'				=> ($icepay_app_active || $icepay_active) ? true : false,
				'mollie_activated'				=> $mollie_app_active ? true : false,
				'invoice_xml'					=> $invoice_xml == '1' ? true : false,
				'xml_available'					=> ACCOUNT_DELIVERY_COUNTRY_ID=='26' ? true : false,
				'pays_activated'				=> $pays_activated,
				'icepay_merchant_id'			=>$icepay_merchant_id,
				'icepay_secret_code'			=>$icepay_secret_code,
				'icepay_thank_you_url' 			=>$config['web_link_url'],
				'icepay_error_url' 				=>$config['web_link_url'],
				'icepay_postback_url' 			=>$config['web_link_url'].'index.php?do=invoice-icepay_postback',
				'do'							=> 'invoice-settings-invoice-web_link',
				'xget'							=> 'weblink',
			);
			return json_out($data, $showin,$exit);
	}
	function get_creditNote($in,$showin=true,$exit=true){
			$db = new sqldb();
			$use_negative = $db->field("SELECT value FROM settings WHERE constant_name='USE_NEGATIVE_CREDIT' ");
			$notes = $db->field("SELECT long_value FROM settings WHERE constant_name='CREDIT_EXTRA' ");
			$data['creditnote']= array(
				'use_negative'	=> $use_negative == '1' ? true : false,
				'notes'			=> $notes,
				'do'			=> 'invoice-settings-invoice-creditupdate',
				'xget'			=> 'creditNote',
			);
			return json_out($data, $showin,$exit);
	}
	function get_attachments($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('atachment'=>array());
			$f = $db->query("SELECT * FROM attached_files WHERE type='2' ");
			while ($f->next()) {
				array_push($data['atachment'], array(
					'file'		=> $f->f('name'),
					'checked'	=> $f->f('default') == 1 ? true : false,
					'file_id'	=> $f->f('file_id'),
					'path'		=> INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/'.$f->f('name'),
					'checkeddrop'		=> $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_DROP_BOX' ") == '1' ? true : false,
					));
			}

			return json_out($data, $showin,$exit);
	}
	function get_timesheet($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('timesheets'=>array());
			$client = $db->field("SELECT active FROM default_data WHERE value='client' AND default_name='timesheet_print_inv'");
			$project = $db->field("SELECT active FROM default_data WHERE value='project' AND default_name='timesheet_print_inv'");
			$task = $db->field("SELECT active FROM default_data WHERE value='task' AND default_name='timesheet_print_inv'");
			$person = $db->field("SELECT active FROM default_data WHERE value='person' AND default_name='timesheet_print_inv'");
			$comment = $db->field("SELECT active FROM default_data WHERE value='comment' AND default_name='timesheet_print_inv'");
			$attach_report = $db->field("SELECT value FROM settings WHERE constant_name='ATTACH_TIMESHEET_INV'");
				$data['timesheets'] = array(
					'client'   				=> $client 				== 1 ? true : false,
					'project'   			=> $project 			== 1 ? true : false,
					'task'   				=> $task 				== 1 ? true : false,
					'attach_report'   		=> $attach_report 		== 1 ? true : false,
					'comment'   			=> $comment 			== 1 ? true : false,
					'xget'					=> 'timesheet',
					'do'					=> 'invoice-settings-invoice-timesheetup',
					);

			return json_out($data, $showin,$exit);
	}

	function get_chargevat($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('chargevats'=>array());
			$chargevat = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_DUE_DATE_VAT_PDF'");
				$data['chargevats'] = array(
					'vat'   				=> $chargevat == 1 ? true : false,
					'xget'					=> 'chargevat',
					'do'					=> 'invoice-settings-invoice-chargevatup',
					);

			return json_out($data, $showin,$exit);
	}


	function get_exportsetting($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('exportsettings'=>array(),'vats'=>array());
			$datas = array();
			$settings_data = $db->query("SELECT * FROM invoice_export_settings");
			$no_vat = array();
			while($settings_data->next()){

				$datas[$settings_data->f('field_name')][$settings_data->f('field_id')] = $settings_data->f('field_value');

				if($settings_data->f('field_id')=='no_vat'){		
					$no_vat[$settings_data->f('field_id_2')] = $settings_data->f('field_value');
				}

			}
			$data['exportsettings'] = array(
				//vat regime	
				'vat_regime_id'			=> 'vat_regime_id',
				'export_vat_regime_id_1'	=> $datas['vat_regime_id'][1],
				'export_vat_regime_id_2'	=> $datas['vat_regime_id'][2],
				'export_vat_regime_id_3'	=> $datas['vat_regime_id'][3],
				'export_vat_regime_id_4'	=> $datas['vat_regime_id'][4],

				//type
				'type'					=> 'type',
				'export_type_0'			=> $datas['type'][0],
				'export_type_1'			=> $datas['type'][1],
				'export_type_2'			=> $datas['type'][2],	
				'xget'					=> 'exportsetting',
				'do'					=> 'invoice-settings-invoice-update_export_settings',
				'vats'					=> array(),
			);
			$vats_data = $db->query("SELECT * FROM vats ORDER BY value");
			$i=0;
			while($vats_data->next()){
				$field_value = array();
				$field_values = $db->query("SELECT * FROM invoice_export_settings WHERE field_name = 'vat_classification' AND field_id = '".$vats_data->f('vat_id')."' ");		
				while($field_values->next()){
					$field_value[$field_values->f('field_id_2')] = $field_values->f('field_value');
				}	
				array_push($data['exportsettings']['vats'], array(				
					'VAT_VALUE'			=> display_number($vats_data->f('value')),
					'VAT_ID'			=> $vats_data->f('vat_id'),
					'field_name'		=> 'no_border',
					'field_value_1'		=> $field_value[1],
					'field_value_2'		=> $field_value[2],
					'field_value_3'		=> $field_value[3],
					'field_value_4'		=> $field_value[4],
				));
			}

			return json_out($data, $showin,$exit);
	}
	function get_articlefield($in,$showin=true,$exit=true){
			$db = new sqldb();
			$data = array('articlefields'=>array());
			$not_copy_article_inv = $db->field("SELECT value FROM settings WHERE constant_name='NOT_COPY_ARTICLE_INV' ");
			$text = $db->field("SELECT long_value FROM settings WHERE constant_name='INVOICE_FIELD_LABEL'");
				$data['articlefields'] = array(
					'not_copy_article_inv'  				=> $not_copy_article_inv == 1 ? true : false,
					'text'  								=> $text,
					'ADV_PRODUCT' 							=> defined('ADV_PRODUCT') && ADV_PRODUCT == 1 ? true : false,
					'xget'									=> 'articlefield',
					'do'									=> 'invoice-settings-invoice-articlefieldup',
					);

			return json_out($data, $showin,$exit);
	}
	 
	function get_sepa($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('sepa'=>array()); 
		$allow_sepa = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_SEPA' ");
		$data['sepa'] = array(
					'sepa_active' 		=> $allow_sepa == 1 ? true : false,
					'xget'			=> 'sepa',
					'do'				=> 'invoice-settings-invoice-sepaupdate',
					);

		$sepa_id=$db->field("SELECT sepa_settings_id FROM sepa_settings ");

		if(!$sepa_id){

		    $settings = $db->query("SELECT * FROM settings");
		    while ($settings->next()) {
		        if($settings->f('value')){
		            $array_settings[$settings->f('constant_name')] = $settings->f('value');
		        }else{
		            $array_settings[$settings->f('constant_name')] = $settings->f('long_value');
		        }

		    }

		    foreach ($array_settings as $key => $value) {
		    	 $data['sepa'][$key]=$value;
		    }
		    $account_country = $db->query("SELECT * from settings where  constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
		    $account_country->move_next();
		    $account_billing_country_id=$account_country->f('value');
		    if (!$account_billing_country_id)
		    {
		        $account_billing_country_id = $in['account_billing_country_id'];
		    }

		    $constant_sepa = $db->query("SELECT * FROM sepa_settings WHERE sepa_settings_id='".$in['sepa_settings_id']."'  ");
		    $constant_sepa->move_next();

		    $data['sepa']['hide_rpr']                =  $account_billing_country_id == '26' ? true : false;//belgium-26
		    $data['sepa']['hide_siret']              =  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? true : false;//france-79,luxembourg-132
		    $data['sepa']['hide_cap_social']         =  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? true : false;//france-79,luxembourg-132
		    $data['sepa']['hide_juridical_form']     =  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? true : false;//france-79,luxembourg-132
		    $data['sepa']['hide_den_sociale']        =  $account_billing_country_id == '79' ? true : false;//france-79
		    $data['sepa']['hide_code_naf']           =  $account_billing_country_id == '79' ? true : false;//france-79
		    $data['sepa']['hide_reg_commerce']       =  $account_billing_country_id == '132' ? true : false;//luxembourg-132
		    $data['sepa']['hide_no_matricule']       =  $account_billing_country_id == '132' ? true : false;//luxembourg-132	        
		    $data['sepa']['example']       		   = $array_settings['ACCOUNT_INVOICE_START'].str_pad(1,$array_settings['ACCOUNT_DIGIT_NR'],"0",STR_PAD_LEFT);
		   
		}else{
		    $account_country = $db->query("SELECT * from settings where  constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
		    $account_country->move_next();
		    $account_billing_country_id=$account_country->f('value');
		    if (!$account_billing_country_id)
		    {
		        $account_billing_country_id = $in['account_billing_country_id'];
		    }

		    $constant_sepa = $db->query("SELECT * FROM sepa_settings WHERE sepa_settings_id='".$sepa_id."'  ");
		    $constant_sepa->move_next();

		    $data['sepa']['hide_rpr']                =  $account_billing_country_id == '26' ? true : false;//belgium-26
		    $data['sepa']['hide_siret']              =  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? true : false;//france-79,luxembourg-132
		    $data['sepa']['hide_cap_social']         =  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? true : false;//france-79,luxembourg-132
		    $data['sepa']['hide_juridical_form']     =  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? true : false;//france-79,luxembourg-132
		    $data['sepa']['hide_den_sociale']        =  $account_billing_country_id == '79' ? true : false;//france-79
		    $data['sepa']['hide_code_naf']           =  $account_billing_country_id == '79' ? true : false;//france-79
		    $data['sepa']['hide_reg_commerce']       =  $account_billing_country_id == '132' ? true : false;//luxembourg-132
		    $data['sepa']['hide_no_matricule']       =  $account_billing_country_id == '132' ? true : false;//luxembourg-132

		    $data['sepa'][ 'example']                = $constant_sepa->f('sepa_invoice_start').str_pad(1,$constant_sepa->f('sepa_inc_part'),"0",STR_PAD_LEFT);
		    $data['sepa']['ACCOUNT_COMPANY']         = $constant_sepa->f('sepa_company');
		    $data['sepa']['ACCOUNT_EMAIL']           = $constant_sepa->f('sepa_email');
		    $data['sepa']['ACCOUNT_BANK_NAME']       = $constant_sepa->f('sepa_bank_name');
		    $data['sepa']['ACCOUNT_BIC_CODE']        = $constant_sepa->f('sepa_bic_code');
		    $data['sepa']['ACCOUNT_IBAN']            = $constant_sepa->f('sepa_iban');
		    $data['sepa']['ACCOUNT_REG_NUMBER']      = $constant_sepa->f('sepa_reg_number');
		    $data['sepa']['ACCOUNT_SIRET']           = $constant_sepa->f('sepa_siret');
		    // 'ACCOUNT_RPR'             = $constant_sepa->f('sepa_rpr');
		    $data['sepa']['ACCOUNT_DEN_SOCIALE']     = $constant_sepa->f('sepa_den_sociale');
		    $data['sepa'][ 'ACCOUNT_REG_COMMERCE']   = $constant_sepa->f('sepa_reg_commerce');
		    $data['sepa']['ACCOUNT_CAP_SOCIAL']      = $constant_sepa->f('sepa_cap_social');
		    $data['sepa']['ACCOUNT_CODE_NAF']        = $constant_sepa->f('sepa_code_naf');
		    $data['sepa']['ACCOUNT_JURIDICAL_FORM']  = $constant_sepa->f('sepa_juridical_form');
		    $data['sepa']['ACCOUNT_INVOICE_START']   = $constant_sepa->f('sepa_invoice_start');
		    $data['sepa']['ACCOUNT_DIGIT_NR']        = $constant_sepa->f('sepa_inc_part');
		    $data['sepa']['COMPANY_ID']              = $constant_sepa->f('sepa_comp_id');
		    $data['sepa']['COMPANY_ID_AUTHORITY']    = $constant_sepa->f('sepa_authority');
		    $data['sepa']['ACCOUNT_SEPA_ID']         = $constant_sepa->f('sepa_id');
		    $data['sepa']['ACCOUNT_VAT_NUMBER']      = $constant_sepa->f('sepa_vat_number');
  
		}

		if(!$in['languages']){
		        $code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND default_lang='1' ORDER BY sort_order limit 1");
		}else{
		     if($in['languages']>=1000) {
		            $code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
		     }else{
		            $code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
		     }
		}

		$is_normal = true;
		$in['lang_code'] = $code;
		$name = 'sepamess';
		$text = '';
		    // $subject = 'Invoice #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
		$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		$lang_pre=$_SESSION['l'];
		if($lang_pre=='nl'){
		        $lang_pre='du';
		}
		$lang_id=$db->field("SELECT lang_id FROM pim_lang WHERE code='".$lang_pre."' ");
		if(!$sys_message_invmess->move_next()){

		    $insert = $db->query("INSERT INTO sys_message SET text='".$text."', name='".$name."', lang_code='".$code."' ");
		            // 'subject'           => $subject,
		    $data['sepa']['text']              = $text;
		    $data['sepa']['language_dd']       = $in['languages'] ? build_language_dd_new($in['languages']) : build_language_dd_new($lang_id);
		            // 'CHECKED'           => '',
		            // 'use_html'          => false,
		}else{

		            // 'subject'           => $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
		     $data['sepa']['text']              = $sys_message_invmess->f('use_html') == 1 ? $sys_message_invmess->f('html_content') : '';
		     $data['sepa']['language_dd']       = $in['languages'] ? build_language_dd_new($in['languages']) : build_language_dd_new($lang_id);
		            // 'CHECKED'           => $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
		            // 'use_html'          => $sys_message_invmess->f('use_html') == 1 ? true : false,
		}
		switch ($in['lang_code']){
		   case 'en':$data['sepa']['translate_cls']='form-language-en';break;
		   case 'fr':$data['sepa']['translate_cls']='form-language-fr';break;
		   case 'du':$data['sepa']['translate_cls']='form-language-nl';break;
		   case 'de':$data['sepa']['translate_cls']='form-language-de';break;
		}
		if($in['languages']>=1000) {
		    $data['sepa']['translate_cls']='form-language-'.$code;
		}
		$result['language_dd']= $in['languages'] ? build_language_dd_new($in['languages']) : build_language_dd_new($lang_id);
		$data['sepa']['lang_code']= $in['lang_code'];
		$data['sepa']['languages']= $in['languages'] ? $in['languages'] : $lang_id;

		return json_out($data, $showin,$exit);
	}

	function get_reminders($in,$showin=true,$exit=true){
		$db = new sqldb();
		$result=array();

		$array_values=array();
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['FIRST_NAME']=gm("First Name");		
		$array_values['LAST_NAME']=gm("Last Name");
		$array_values['INVOICE_SUMMARY']=gm("Invoice Summary");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['CURRENT_DATE']=gm("Current date");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id'     => $key,
				'name'	   => $value,
			));
		}

		$result['detail_id']=$in['detail_id'];
		if($result['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$result['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);

		$lang_pres=$_SESSION['l'];
		if(!$in['languages']){
			$in['languages']=$db->field("SELECT lang_id FROM pim_lang WHERE code='".$lang_pres."' ");
			if(!$in['languages'] && $lang_pres =='nl'){
				$in['languages']=$db->field("SELECT lang_id FROM pim_lang WHERE code='du' ");
			}
		}

		if(!$in['languages']){
			$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND default_lang='1' ORDER BY sort_order limit 1");
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."'  ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$default_values1= $db->query("SELECT * FROM tblinvoice_grades WHERE grade='first' LIMIT 1");
		$default_values2= $db->query("SELECT * FROM tblinvoice_grades WHERE grade='second' LIMIT 1");
		$default_values3= $db->query("SELECT * FROM tblinvoice_grades WHERE grade='third' LIMIT 1");

		$in['lang_code'] = $code;

		switch ($in['lang_code'])
		{
			case 'en': $result['translate_cls'] 	= 'form-language-en'; break;
			case 'fr': $result['translate_cls']		= 'form-language-fr'; break;
			case 'nl': $result['translate_cls']		= 'form-language-nl'; break;
		}
		if($in['languages']>=1000) {
			$result['translate_cls'] 			= 'form-language-'.$in['lang_code'];
		}


			$name = 'invmess_late';
			$text = 'Dear Customer,

			We want to remind you about this late invoices that still needs to be paid.
			';
		    $subject = 'Late invoice summary';
		    $sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		    $lang_pre=$_SESSION['l'];
		    if($lang_pre=='nl'){
		        $lang_pre='du';
		    }
		    $lang_id=$db->field("SELECT lang_id FROM pim_lang WHERE code='".$lang_pre."' ");
			if(!$sys_message_invmess->move_next()){
			  $insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			    $result['subject']         	= $subject;
			    $result['text']            	= $text;	    	   
			}else{
			    $result['subject']         	= $sys_message_invmess->f('subject');
			    $result['text']            	= $sys_message_invmess->f('text');
			    $result['footnote']	    		= $sys_message_invmess->f('footnote');
			}

		$result['tabs']=array();

		$first_grade = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='first' AND lang_code='".$in['lang_code']."' LIMIT 1");
		$message = $db->field("SELECT message FROM tblinvoice_grades WHERE grade='first' AND lang_code='".$in['lang_code']."'");
		if($first_grade->next())
		{
				$tab=array(
					'penalty_value'			=> display_number($first_grade->f('value')),
					'penalty_type'			=> $first_grade->f('type'),
					'text'				=> $message,
					'title'				=> $first_grade->f('title'),
					'header'				=> gm('First grade Message'),
					'penalty_dd'			=>  $tz=array(
											        array('id' => "1" , 'name' => gm('Percent')),
											        array('id' => "2" , 'name' => gm('Fixed amount')),
											    ),
				);
				array_push($result['tabs'], $tab);
			
		}else{
				$tab=array(
					'penalty_value'			=> display_number($default_values1->f('value')),
					'penalty_type'			=> $default_values1,
					'penalty_dd'			=>  $tz=array(
											        array('id' => "1" , 'name' => gm('Percent')),
											        array('id' => "2" , 'name' => gm('Fixed amount')),
											    ),
					'header'				=> gm('First grade Message'),
				);
				array_push($result['tabs'], $tab);		
		}
		$second_grade = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='second' AND lang_code='".$in['lang_code']."' LIMIT 1");
		$message = $db->field("SELECT message FROM tblinvoice_grades WHERE grade='second' AND lang_code='".$in['lang_code']."'");
		if($second_grade->next())
		{
				$tab=array(
					'penalty_value'			=> display_number($second_grade->f('value')),
					'penalty_type'			=> $second_grade->f('type'),
					'text'				=> $message,
					'title'				=> $second_grade->f('title'),
					'header'				=> gm('Second grade Message'),
					'penalty_dd'			=>  $tz=array(
											        array('id' => "1" , 'name' => gm('Percent')),
											        array('id' => "2" , 'name' => gm('Fixed amount')),
											    ),
				);
				array_push($result['tabs'], $tab);
		}else{
				$tab=array(
					'penalty_value'			=> display_number($default_values2->f('value')),
					'penalty_type'			=> $default_values2->f('type'),
					'penalty_dd'			=>  $tz=array(
											        array('id' => "1" , 'name' => gm('Percent')),
											        array('id' => "2" , 'name' => gm('Fixed amount')),
											    ),
					'header'				=> gm('Second grade Message'),
				);		
				array_push($result['tabs'], $tab);
		}
		$third_grade = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='third' AND lang_code='".$in['lang_code']."' LIMIT 1");
		$message = $db->field("SELECT message FROM tblinvoice_grades WHERE grade='third' AND lang_code='".$in['lang_code']."'");
		if($third_grade->next())
		{
				$tab=array(
					'penalty_value'			=> display_number($third_grade->f('value')),
					'penalty_type'			=> $third_grade->f('type'),
					'text'				=> $message,
					'title'				=> $third_grade->f('title'),
					'header'				=> gm('Third grade Message'),
					'penalty_dd'			=>  $tz=array(
											        array('id' => "1" , 'name' => gm('Percent')),
											        array('id' => "2" , 'name' => gm('Fixed amount')),
											    ),
				);
				array_push($result['tabs'], $tab);
		}else{
				$tab=array(
					'penalty_value'			=> display_number($default_values3->f('value')),
					'penalty_type'			=> $default_values3->f('type'),
					'penalty_dd'			=>  $tz=array(
											        array('id' => "1" , 'name' => gm('Percent')),
											        array('id' => "2" , 'name' => gm('Fixed amount')),
											    ),
					'header'				=> gm('Third grade Message'),
				);	
				array_push($result['tabs'], $tab);
		}

		$result['language_dd']       			= $in['languages'] ? build_language_dd_new($in['languages']) : build_language_dd_new($lang_id);
		$result['languages']				= $in['languages'] ? $in['languages'] : $lang_id;
		$result['lang_code']				= $in['lang_code'];
		$result['translate_special']		= 'form-'.$code;
		$result['checked_grades']			= USE_GRADES==1 ? true : false;
		$result['style']     				= ACCOUNT_NUMBER_FORMAT;
		$result['name']					= $name;

		$data['reminders']=$result;
		return json_out($data, $showin,$exit);
	}

	function get_restore($in,$showin=true,$exit=true){
		$db = new sqldb();
		set_time_limit(0);
	    ini_set('memory_limit', '-1');   
			$db = new sqldb();
			$db2 = new sqldb();
			$in['invoice'] = '0';
			$in['invoice2'] = '0';
			$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup1.xml';
			$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup2.xml';
			$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/invoice/backup3.xml';
			$file_fill1 = file_get_contents($file1);
			$file_fill2 = file_get_contents($file2);
			$file_fill3 = file_get_contents($file3);

			if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
				$show_file=1;
			}
			$data = array('restore'=>array(
				'file'				=>    backupinvoice2($in['invoice']),
				'filebackup'		=>    backupinvoice3($in['invoice']),
				'file_id'			=>	  $in['invoice'],
				'file2_id'			=>	  $in['invoice2'],
				'show_file'			=>    $show_file==1?true:false,
				'class1'			=> 	  $class1,
				'class2'			=> 	  $class2,
				'class3'			=> 	  $class3,
				'history'			=>    array(),
			));


		return json_out($data, $showin,$exit);
	};
	function get_invoiceTerm($in,$showin=true,$exit=true){
		$db = new sqldb();

		/* Get Credit Note */
		$use_negative = $db->field("SELECT value FROM settings WHERE constant_name='USE_NEGATIVE_CREDIT' ");
		$notes = $db->field("SELECT long_value FROM settings WHERE constant_name='CREDIT_EXTRA' ");
		
		$apply_discount = $db->field("SELECT value FROM settings WHERE constant_name = 'INVOICE_APPLY_DISCOUNT' ");
		$add_delivery_address = $db->field("SELECT value FROM settings WHERE constant_name = 'INVOICE_ADD_DELIVERY_ADDRESS' ");

		$data = array('invoiceTerm'=>array(
			'apply_discount' 			=> $apply_discount==1 ? true : false,
			'add_delivery_address' 		=> $add_delivery_address==1 ? true : false,
			'do'						=> 'invoice-settings-invoice-saveterm',
			'xget'						=> 'invoiceTerm',
			'creditnote'	=> array('use_negative'	=> $use_negative == '1' ? true : false,
									'notes'			=> $notes,
									'do'			=> 'invoice-settings-invoice-creditupdate',
									)
		));

		return json_out($data, $showin,$exit);
	}
	function get_p_invoices($in,$showin=true,$exit=true){
		$db = new sqldb();

		$automatic_xml = $db->field("SELECT value FROM settings WHERE constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE' ");

		$dropbox_active=$db->field("SELECT active FROM apps WHERE name='Dropbox' AND type='main' and main_app_id='0' ");
		if(!$dropbox_active && $automatic_xml=='1'){
	        $db->query("UPDATE settings SET value='' WHERE constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE'");
	    }
		$automatic_xml = $db->field("SELECT value FROM settings WHERE constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE' ");

		/*$export_billtobox_to_dropbox = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE' ");

		if(!$dropbox_active && $export_billtobox_to_dropbox=='1'){
	        $db->query("UPDATE settings SET value='' WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE'");
	    }
		$export_billtobox_to_dropbox = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE' ");
		$export_billtobox_to_dropbox_key = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE_KEY' ");*/

		$data = array('p_invoices'=>array(
			'automatic_xml' 						=> $automatic_xml==1 ? true : false,
			'automatic_xml_knob_class'				=> $dropbox_active!=1 ? 'not_allowed' : "",
			'dropbox_active'						=> $dropbox_active==1 ? true : false,
			/*'export_billtobox_to_dropbox' 			=> $export_billtobox_to_dropbox==1 ? true : false,
			'access_key'							=> $export_billtobox_to_dropbox_key,*/
		));
		return json_out($data, $showin,$exit);
	}
	$result = array(
		/*'logos'				=> get_logos($in,true,false),*/
		'PDFlayout'			=> get_PDFlayout($in,true,false),
		'PDFlayoutCredit'	=> get_PDFlayoutCredit($in,true,false),
		'Convention' 		=> get_Convention($in,true,false),
	);
	

	json_out($result);
?>
