<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $config;

$invoice_exists = $db->field("SELECT invoice_id FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");

if(!$invoice_exists){
	msg::error('Invoice does not exist','error');
	$in['item_exists']= false;
    json_out($in);
}
ark::loadLibraries(array('aws'));
$aws = new awsWrap(DATABASE_NAME);
$payments_nr=0;
//payments_info
$o=array();
$o['item_exists']=true;
$payments_info = $db->query("SELECT * FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."' and credit_payment!=1 ORDER BY payment_id desc");

$o['payments_row']=array();
while($payments_info->move_next()){
	switch ($payments_info->f('payment_method')) {
		case '0':
			$payment_method='';
			break;
	    case '1':
				$payment_method=gm('Cash');
			break;	
	    case '2':
				$payment_method=gm('Bank transfer');
			break;	
	    case '3':
				$payment_method=gm('Credit Card');
			break;	
	    case '4':
				$payment_method=gm('Cheque');
			break;
		case '5':
				$payment_method=gm('Automatic payment');
			break;
		
		default:
				$payment_method='';
			break;
	}
$info=explode("  -  ",$payments_info->f('info'));

	$payments_row=array(
		'date'	=>   date(ACCOUNT_DATE_FORMAT,strtotime($payments_info->f('date'))),
		'amount'  =>  place_currency(display_number($payments_info->f('amount'))),
		'payment_method'  =>  $payment_method,
		'is_cost'       => $info['3']?true:false,
		'cost_pay'  =>  display_number($info['3']),
		'payment_id'	=> $payments_info->f('payment_id'),
		'is_info'       => $info['2']?true:false,
		'info'       => $info['2'],
		'payment_id'  =>  $payments_info->f('payment_id'),
		'del_payment_link'	   => 'index.php?do=invoice-purchase_invoice-invoice-delete_payment&payment_id='.$payments_info->f('payment_id').'&invoice_id='.$in['invoice_id']
	);
	array_push($o['payments_row'], $payments_row);
	$payments_nr++;
}
if($payments_nr){
	$o['is_payment']=true;
	
}else{
	$o['is_payment']=false;
}
$info = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");


switch ($info->f('status')){
	case '0':
		$status = 'draft';

		/* if($info->f('paid') == '1' ){
			
				$status = 'paid';
			
			}*/
		
		break;
	case '1':
		$status = 'final';
		 if($info->f('paid')>0){
			
				$status = 'paid';
			
			}
		break;
	default:
		$status = 'draft';
		
		break;
}

switch($info->f('status')){
    /*case '0':
        $nr_status=$info->f('paid') ? 3 : 1;
        $type_title=$info->f('paid') ? gm('Paid') : gm('Uploaded');
        break;*/
    case '1':
        $nr_status=$info->f('paid') ? 3 : 2;
        $type_title=$info->f('paid') ? ($info->f('paid')==2 ? gm('Partially Paid') : gm('Paid')) : gm('Approved');
        break;
    default:
        $nr_status=1;
        $type_title=gm('Uploaded');
        break;
}
if($info->f('paid') == '0' && $info->f('status') == '1' && $info->f('due_date') < time()){
    $nr_status=4;
    $type_title=gm('Late');
}
if($info->f('f_archived')=='1'){
	$type_title = gm('Archived');
	$nr_status=1;
}
$o['type_title']=$type_title;
$o['nr_status']=$nr_status;

$total=$info->f('total_with_vat');

$already_payed = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."'  ");
$total_payed = $already_payed->f('total_payed');

$amount_due = round($total - $total_payed,2);

$currency= get_currency($info->f('currency_type'));
$o['currency_type']= $info->f('currency_type');

$o['c_id'] = $info->f('supplier_id');
$o['buyer_id'] = $info->f('supplier_id');
$o['contact_id'] = $info->f('contact_id');
$o['serial_number']= $info->f('serial_number');
$o['booking_number']= $info->f('booking_number');
$o['invoice_number']= $info->f('invoice_number');
$in['i_date']=$info->f('invoice_date');
$o['i_date']=$info->f('invoice_date');
$o['free_field']=nl2br($info->f('free_field'));

$o['selectedDraft']	 = $status == 'draft' ? true : false;
$o['selectedFinal']	 = $status == 'final' ? true : false;
$o['selectedPaid']	 = $status == 'paid' ? true : false;
$o['paid']=$info->f('paid');
$o['type']= $info->f('type');
if($info->f('type')==1) {
    $o['booking_number_new']=generate_binvoice_number();
}

$factur_date = date(ACCOUNT_DATE_FORMAT,  $info->f('invoice_date'));
$due_date = date(ACCOUNT_DATE_FORMAT,  $info->f('due_date'));

$o['i_date']								= $in['i_date'] ? $in['i_date'] : time();
$o['invoice_date']							= $in['i_date'] ? $in['i_date']*1000 : time()*1000;

if($o['c_id']){
	$o['customer_name'] = $db->field("SELECT customers.name 
	                     FROM customers WHERE customers.customer_id=".$o['c_id']." ");
	if($o['contact_id']){
		$o['contact_name'] = $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts
						WHERE customer_id=".$o['c_id']." AND contact_id='".$o['contact_id']."' ");
	}

}


$o['order_id'] = $info->f('c_order_id');
$o['order_serial'] = $db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id ='".$o['order_id']."' ");



$link = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$info->f('c_invoice_id')."'");

$credit = $db->query("SELECT * FROM tblinvoice_incomming WHERE c_invoice_id='".$in['invoice_id']."'");
$is_not_sent=true;
$sent = $db->query("SELECT * FROM tblinvoice_exported WHERE invoice_id='".$in['invoice_id']."' AND type='1' AND winbooks='1' ");
if($sent->next()){
	$is_not_sent=false;
}
$exist =  $aws->doesObjectExist($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
	$link_url ='';
	$file_link = '';
	$is_image=false;
	if($info->f('amazon') || (!$info->f('file_path') && !$info->f('pdf_path'))){
	   if($exist){
		  $link_url = 'index.php?do=invoice-purchase_print&id='.$in['invoice_id'];
		  $file = explode('/',$info->f('file_name'));
		  $file_link = $aws->getLink($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
		  $fileParts = pathinfo($file['2']);
		  $fileTypes = array('jpg','jpeg','png');
		  if(in_array(strtolower($fileParts['extension']),$fileTypes)){
		    $is_image=true;
		  }
	   }
    }

	//$o['del_payment_link']	   = 'index.php?do=invoice-purchase_invoice-invoice-delete_payment&invoice_id='.$in['invoice_id'];
    $o['factur_date']          = $factur_date;
    $o['due_date']             = $due_date;
	$o['is_link']              = $link->f('invoice_id') ? true : false;
	$o['link_id']              =$info->f('c_invoice_id');
	$o['link_invoice']          =$link->f('invoice_number');
	$o['not_credit']            =($info->f('type')==0 || $info->f('type')==1) ? true:false;
	$o['payment_date_show']		=date(ACCOUNT_DATE_FORMAT,time());
	$o['payment_date']			=time();
	$o['total_sum']				=display_number($amount_due);
	$o['total_sum_curr']		=place_currency(display_number($amount_due),$currency);
	$o['a_transfer']			= $info->f('a_transfer') ? true : false;
	$o['style']					=ACCOUNT_NUMBER_FORMAT;
	$o['currency']				=$currency;
	$o['pick_date_format']		= pick_date_format();
	$o['not_paid']				=$info->f('paid')=='1'?  false:true;
	$o['credit_id']				=$credit->f('invoice_id');
	$o['credit_invoice']		=$credit->f('invoice_number');
	$o['if_credit']				=$credit->f('invoice_id') ? true : false;
	$o['not_sent_to_accountent']=$is_not_sent;
	$o['cost']					= display_number(0);
	$o['page_title']    		= $info->f('type')==2? gm('Credit Note') : gm('Invoice');
	$o['credit_style']  		= $info->f('type')==2?'style="color: #ff0000;"':'';
	$o['credit_link']  			= 'index.php?do=invoice-purchase_invoices&add_inv=true&type=2&c_invoice_id='.$in['invoice_id'];
	$o['approved_by']			= get_user_name($info->f('approved_by'));
	$o['link_url']				= $link_url;
	$o['file_link']				= $file_link;

	$o['download_url']			= $link_url.'&just_download=true';
	$o['is_image']				= $is_image;
	$o['ogm']					= $info->f('ogm');
	$o['reference']				= $info->f('reference');
	$o['expense_category_id']	= $info->f('expense_category_id');
	$o['expense_category']      = $db->field("SELECT name FROM tblinvoice_expense_categories WHERE id='".$info->f('expense_category_id')."'");
	$o['is_active']				= $info->f('f_archived')=='0'? true:false;
	$o['is_archived']			= $info->f('f_archived')=='1'? true:false;

$sql_po="";
if($info->f('type') != 2){
	$sql_po=" AND pim_p_orders.order_full!=1 ";
}

$info = $db->query("SELECT tblinvoice_incomming.*, tblinvoice_incomming_line.vat_line AS new_vat, tblinvoice_incomming_line.total AS new_total
					FROM tblinvoice_incomming
					LEFT JOIN tblinvoice_incomming_line ON tblinvoice_incomming.invoice_id = tblinvoice_incomming_line.invoice_id
					WHERE tblinvoice_incomming.invoice_id='".$in['invoice_id']."' ORDER BY line_id ASC ");

$big_total_disco = 0;
$big_total = 0;
$amount = 0;
$total_vat = 0;
$i = 0;
$o['vat_row']=array();
while($info->next()){
	$disconto = $info->f('disconto');
	$allow_disconto = $info->f('allow_disconto');
	$line = $info->f('new_total');
	$line_disco = 0;
	if($allow_disconto == 1){
		$line = $info->f('new_total') - $info->f('new_total')*$disconto/100;
		$line_disco = $info->f('new_total')*$disconto/100;
	}
	$total_with_vat = $line + ($line/100*$info->f('new_vat'));
	$vat_amount = $line/100*$info->f('new_vat');

	$big_total += $total_with_vat;
	$big_total_disco += $line_disco+$total_with_vat;
        $amount += $line;
	$total_vat += $vat_amount;

	$new_vat1 = $info->f('new_vat');
	$new_total1 = $info->f('new_total');
	$total_with_disco1 = $line_disco;
	$total_with_vat1 = $total_with_vat;

	
	$vat_row=array(
		'new_vat'			=> display_number($info->f('new_vat')),
		'new_total'			=> display_number($info->f('new_total')),
		'total_with_vat'	=> display_number($total_with_vat),
		'vat_amount'		=> display_number($vat_amount),
		'allow_disconto'	=> $allow_disconto ? true : false,
		'total_with_disco'	=> display_number($total_with_disco1+$total_with_vat1)
	);
	
	array_push($o['vat_row'], $vat_row);
	$i++;
}
if($i<=1){
		$one_vat = true;
	}else{
		$one_vat = false;
	}


	$o['total'] 		    = display_number($big_total);
	$o['total_disco']		= display_number($big_total_disco);
	$o['total_amount']		= display_number($amount);
	$o['total_vat']			= display_number($total_vat);
	$o['one_vat']			= $one_vat;
	$o['new_vat1']			= display_number($new_vat1);
	$o['new_total1']		= display_number($new_total1);
	$o['total_with_vat1']	= display_number($total_with_vat1);
	$o['total_with_disco1'] = display_number($total_with_disco1+$total_with_vat1);
	$o['disconto']			= display_number($disconto);
	$o['allow_disconto']	= $allow_disconto ? true : false;





$customer_details = $db->query("SELECT customers.*, customer_addresses.*, country.name as country_n, customer_contact_language.name as c_lang
								FROM customers
								LEFT JOIN customer_addresses ON (customer_addresses.customer_id=customers.customer_id) AND is_primary='1'
								LEFT JOIN country ON (country.country_id=customer_addresses.country_id)
								LEFT JOIN customer_contact_language ON customers.language = customer_contact_language.id
								WHERE customers.customer_id=".$o['c_id']." ");

$contacts = $db->query("SELECT customer_contacts.*, customer_contact_language.name as c_lang FROM customer_contacts
						LEFT JOIN customer_contact_language ON customer_contacts.language = customer_contact_language.id
						WHERE customer_id=".$o['c_id']." AND contact_id='".$o['contact_id']."' ");



if($customer_details->next()){
	
		$o['customer_name']				= $customer_details->f('name');
		$o['country_dd']			  = $customer_details->f('country_n');
		$o['state_dd']					= $customer_details->f('state_id');
		$o['city']						= $customer_details->f('city');
		$o['zip']						= $customer_details->f('zip');
		$o['address']					= $customer_details->f('address');
		$o['vat_dd']					= build_vat_dd($customer_details->f('vat_id'));
		$o['btw_nr']					= $in['bwt_nr'];
		$o['btw']						= $customer_details->f('btw_nr');
		$o['comp_start']				= true;
		$o['language']					= $customer_details->f('c_lang');
		$o['payment_term']				= $customer_details->f('payment_term');
		$o['payment_term_type']		    = $customer_details->f('payment_term_type');
	
}
if($contacts->next()){
	
		$o['first_name']			    = $contacts->f('firstname');
		$o['last_name']					= $contacts->f('lastname');
		$o['email']						= $contacts->f('email');
		$o['phone']						= $contacts->f('phone');
		$o['c_start']					= $in['c_id'] && $in['invoice'] ? false : true;
		$o['c_language']				= $contacts->f('c_lang');
	
}


$order_ids=array();


$db->query("SELECT c_order_id FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");

if($db->f('c_order_id')){
   $order_ids=explode(',', $db->f('c_order_id'));


}
$show_info=false;
$o['orders_row']=array();
if($order_ids){
	foreach ($order_ids as $key => $order_id) {
	  $p_order = $db->query("SELECT pim_p_orders.*,ROUND(cast(amount AS decimal(12, 4))+0.0012,2) AS formatted_amount FROM pim_p_orders WHERE p_order_id='".$order_id."' ");
        console::log($p_order);
		if($p_order->f('sent')==0 ){
			$status = gm('Draft');
		}
		if($p_order->f('invoiced')){
			if($orders->f('rdy_invoice') == 1){
				$status = gm('Received');
			}else{
				$status = gm('Final');
			}
		}else{
			if($p_order->f('rdy_invoice') == 2){
				$status = gm('Final');
			}else{
				if($p_order->f('rdy_invoice') == 1){
					$status = gm('Received');
				}else{
					if($p_order->f('sent') == 1){
						$status = gm('Final');
					}else{
						$status = gm('Draft');
					}
				}
			}
		}

		if(!$p_order->f('active')){
			$status = gm('Archived');
		}

	  $orders_row=array(
	  	'p_order_id'	=> $p_order->f('p_order_id'),
		'serial'		=> $p_order->f('serial_number'),
		'total'			=> place_currency(display_number($p_order->f('formatted_amount')), get_currency($p_order->f('currency_type'))),
		'info_link'     => 'index.php?do=order-p_order&p_order_id='.$p_order->f('p_order_id'),
		'delete_link'	=>'index.php?do=invoice-purchase_invoice-invoice-delete_order&order_id='.$p_order->f('p_order_id').'&invoice_id='.$in['invoice_id'],
		'status'	=> $status
	    );

        $total_gen+=$p_order->f('formatted_amount');

		array_push($o['orders_row'], $orders_row);
	}

$show_info=true;
}
        $o['show_info']	= $show_info;
		$o['total_gen_order'] = place_currency(display_number($total_gen), get_commission_type_list($o['currency_type']));


$db->query("SELECT pim_p_orders.p_order_id,pim_p_orders.serial_number,pim_p_orders.customer_id,pim_p_orders.order_full
			 FROM pim_p_orders			 
			 WHERE pim_p_orders.customer_id= '".$o['c_id']."' ".$sql_po." AND active=1");
$o['orders']=array();

while($db->move_next()) {
  array_push($o['orders'],array("value"=>$db->f('serial_number'),
  	                           'id'=>$db->f('p_order_id'))
            );
}

	$gen_notes = $db->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'purchase_invoice'
								   AND item_name = 'notes'
								   And active = '1'
								   AND lang_id = '1'
								   AND item_id = '".$in['invoice_id']."' ");

	$o['notes'] = $gen_notes;

$allow_po_create=false;
if(in_array(14,perm::$allow_apps)){
    $allow_po_create=true;
}
$o['allow_po_create']=$allow_po_create;

json_out($o);


?>