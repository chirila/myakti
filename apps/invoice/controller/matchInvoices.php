<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

		$db=new sqldb();
		$l_r= 10;
		$result=array('filters'=>array());
		$excepts=array('fetch_iban_nr','fetch_amount_nr','filter_iban','filter_amount');
		foreach($in as $key=>$value){
			if(in_array($key, $excepts)===false){
				$result[$key]=$value;
				$result['filters'][$key]=$value;
			}				
		}
		if((!$in['offset']) || (!is_numeric($in['offset']))){
		    $offset=0;
		    $in['offset']=1;
		}else{
		    $offset=$in['offset']-1;
		}
		$filter="";
		$having=" HAVING total_amount!='0' ";
		if($in['filter_iban']=='true'){
			$filter.=" AND customers.bank_iban='".$in['fetch_iban_nr']."' ";
		}
		if($in['filter_amount']=='true'){
			$having.=" AND total_amount>=".floor(return_value($in['fetch_amount_nr']))." AND total_amount<=".ceil(return_value($in['fetch_amount_nr']))." ";
		}
		if($in['search'] && !empty($in['search'])){
			$filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%') ";
		}
		$coda_line=$db->query("SELECT * FROM codapayments_line WHERE id='".$in['id']."' ");
		$result['id']=$coda_line->f('id');
		$result['currency_value']=get_currency_value_by_code($coda_line->f('currency'));
		$result['amount']=display_number($coda_line->f('amount_left'));
		$result['amount_currency']=$result['currency_value'].' '.display_number($coda_line->f('amount_left'));
		$result['currency_type']=$coda_line->f('currency');
		$result['name']=$coda_line->f('name');
		$result['iban']=$coda_line->f('bank');
		$result['reference']=$coda_line->f('reference');
		$result['currency']=$db->field("SELECT id FROM currency WHERE code='".$coda_line->f('currency')."' ");
		$result['amount_info']=display_number($coda_line->f('amount_left'));
		$result['amount_info_currency']=$result['currency_value'].' '.display_number($coda_line->f('amount_left'));
		$result['invoices']=array();
		$j=0;
		$tblinvoice=$db->query("SELECT tblinvoice.id,tblinvoice.serial_number,ROUND(tblinvoice.amount_vat-IFNULL(SUM(tblinvoice_payments.amount),0),2) AS total_amount,tblinvoice.currency_type,tblinvoice.buyer_id,currency.code,currency.value as currency_value,customers.name as buyer_name,customers.type,customers.firstname, customers.bank_iban FROM tblinvoice 
				LEFT JOIN currency ON tblinvoice.currency_type=currency.id
				LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				LEFT JOIN tblinvoice_payments ON tblinvoice.id=tblinvoice_payments.invoice_id
				WHERE tblinvoice.type='0' AND tblinvoice.sent='1' AND tblinvoice.not_paid!='1' AND tblinvoice.paid !='1' AND tblinvoice.serial_number!='' ".$filter." GROUP BY tblinvoice.id ".$having." ORDER BY tblinvoice.serial_number DESC");
		$max_rows=$tblinvoice->records_count();
		$tblinvoice->move_to($offset*$l_r);
		while($tblinvoice->next() && $j<$l_r){
			$line=array(
				'id'				=> $tblinvoice->f('id'),
				'serial_number'		=> $tblinvoice->f('serial_number'),
				'amount_vat'		=> display_number($tblinvoice->f('total_amount')),
				'amount_vat_currency'		=> $tblinvoice->f('currency_value').' '.display_number($tblinvoice->f('total_amount')),
				'currency_type'		=> $tblinvoice->f('code')? $tblinvoice->f('code') :'EUR',
				'currency'			=> $tblinvoice->f('currency_type')? $tblinvoice->f('currency_type') :'1',
				'currency_value'	=> $tblinvoice->f('currency_value'),
				'buyer_name'		=> $tblinvoice->f('type')=='1'? $tblinvoice->f('firstname').' '.$tblinvoice->f('buyer_name') : $tblinvoice->f('buyer_name'),
				'iban'				=> $tblinvoice->f('bank_iban'),
				'add_to_fetch'		=> $_SESSION['add_to_fetch'][$tblinvoice->f('id')] == 1 ? true : false,
			);
			array_push($result['invoices'], $line);
			$j++;	
		}

		$result['all_invoices']=array();
		$tblinvoice->move_to(-1);
		while($tblinvoice->next()){
			$line=array(
				'id'				=> $tblinvoice->f('id'),
				'serial_number'		=> $tblinvoice->f('serial_number'),
				'amount_vat'		=> display_number($tblinvoice->f('total_amount')),
				'amount_vat_currency'		=> $tblinvoice->f('currency_value').' '.display_number($tblinvoice->f('total_amount')),
				'currency_type'		=> $tblinvoice->f('code')? $tblinvoice->f('code') :'EUR',
				'currency'			=> $tblinvoice->f('currency_type')? $tblinvoice->f('currency_type') :'1',
				'currency_value'	=> $tblinvoice->f('currency_value'),
				'buyer_name'		=> $tblinvoice->f('type')=='1'? $tblinvoice->f('firstname').' '.$tblinvoice->f('buyer_name') : $tblinvoice->f('buyer_name'),
				'iban'				=> $tblinvoice->f('bank_iban'),
				'add_to_fetch'		=> $_SESSION['add_to_fetch'][$tblinvoice->f('id')] == 1 ? true : false,
			);
			array_push($result['all_invoices'], $line);
			$j++;	
		}
		$result['max_rows']=$max_rows;
		$result['lr']=$l_r;
		json_out($result);


?>