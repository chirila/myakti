<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

$result=array('vats'=>array());

$regime_type=array(
      '1'   => gm("Regular"),
      '2'   => gm('Intra-EU'),
      '3'   => gm("Export"),
    );
$vats = $db->query("SELECT vat_new.*  FROM vat_new
          ORDER BY vat_new.name_id ASC");

while($vats->next()){
  $selected=$db->field("SELECT value FROM exactvats WHERE vat_id='".$vats->f('id')."' ");
  if($vats->f('vat_id')){
    if(!$selected){
      $selected=$db->field("SELECT value FROM exactvats WHERE vat_id='".$vats->f('vat_id')."' ");
    }
    $value=$db->field("SELECT value FROM vats WHERE vat_id='".$vats->f('vat_id')."' ");
  }else if($vats->f('vat_regime_id')){
    if(!$selected){
      $selected=$db->field("SELECT value FROM exactvats WHERE vat_id='r".$vats->f('vat_regime_id')."' ");
    }
    $value=0;
  }else if($selected){
    $value=0;
  }else{
    $selected='0';
    $value=0;
  }
  if(!$selected){
    $selected='0';
  }
  $item=array(
    'VAT_VALUE'   => $vats->f('description'),
    'value'         => $value,
    'regime_type'   => $regime_type[$vats->f('regime_type')],
    'id'          => $vats->f('id'),
    'd'           => build_exact_dd(),
    'selected'    => $selected,
  );
  array_push($result['vats'], $item);
}
$result['ledger_account']=build_article_ledger_account_dd();
$ledger_account=$db->field("SELECT value FROM settings WHERE constant_name='EXACT_LEDGER_ID' ");
//$result['is_admin']= $_SESSION['access_level'] == 1 ? true : false;
$result['ledger_account_id']=$ledger_account;
$result['ledger_name']='EXACT_LEDGER_ID';
$result['app']=$in['app'];
$result['title']=gm('Exact Online settings');
$result['do_next']='invoice--invoice-exactVats';
json_out($result);