<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class MandateEdit extends Controller{

	function __construct($in,$db = null){
		parent::__construct($in,$db = null);		
	}

	public function getMandate(){
		if($this->in['mandate_id'] == 'tmp'){ //add
			$this->getAddMandate();
		}else{ # edit
			$this->getEditMandate();
		}
	}

	public function getAddMandate(){
		$in=$this->in;
        $output=array();

		$details = array('table'	 => $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field' 	 => $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'	 => $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],					 
					 'filter'	 => $in['buyer_id'] ? ' AND is_primary =1' : '',
					);

		$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
		$bank_bic='';
		$bank_iban='';
		if($in['buyer_id'] ){
			$buyer_info = $this->db->query("SELECT customers.payment_term,customers.payment_term_type, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name,
					customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.invoice_note2, customers.internal_language,
					customers.attention_of_invoice,customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc, customers.name AS company_name, customers.vat_regime_id,customers.identity_id,
					customers.bank_iban, customers.bank_bic_code
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$in['buyer_id']."' ");
			$buyer_info->next();
			$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');

			$text = gm('Company Name').':';
			$c_email = $buyer_info->f('c_email');
			$c_fax = $buyer_details->f('comp_fax');
			$c_phone = $buyer_details->f('comp_phone');
					
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			$main_address_id			= $buyer_details->f('address_id');
			$sameAddress = false;
			if($buyer_details->f('is_primary')==1){
				$sameAddress = true;
			}
			$bank_bic=$buyer_info->f('bank_bic_code');
			$bank_iban=$buyer_info->f('bank_iban');
		}else{
			$buyer_info = $this->db->query("SELECT phone, cell, email, CONCAT_WS(' ',firstname, lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
			$buyer_info->next();
			$c_email = $buyer_info->f('email');
			$c_fax = '';
			$c_phone = $buyer_info->f('phone');
			$text =gm('Name').':';
			$name = $buyer_info->f('name');
		}

		$free_field = $buyer_details->f('address').'
		'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
		'.get_country_name($buyer_details->f('country_id'));
		$name = stripslashes($name);

		if($in['main_address_id'] && $in['main_address_id']!=$main_address_id){
			$main_address_id=$in['main_address_id'];
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$free_field = $buyer_details->f('address').'
				'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
				'.get_country_name($buyer_details->f('country_id'));
		}

		$output['country_dd']			= $in['country_id']? build_country_list($in['country_id']):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['customer']			= 'customer';
		$output['contact']			= 'contact';
		$output['main_address_id']		= $main_address_id;
		$output['sameAddress']			= $sameAddress;
		$output['buyer_id']             	= $in['buyer_id'];
		$output['customer_id']			= $in['buyer_id'];
		$output['contact_id']           	= $in['contact_id'];
		$output['buyer_name']           	= $name;
		$output['field']				= $in['buyer_id'] ? 'customer_id' : 'contact_id';
		$output['free_field']			= $free_field;
		$output['free_field_txt']		= nl2br($free_field);
		$output['delivery_address_id']	= $in['delivery_address_id'];
		if($in['contact_id']){
			$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
		}
		$output['do_next']		= 'invoice-mandate_edit-invoice-updateCustomerDataSepa';
		$output['cc']			= $this->get_cc($in);
		$output['contacts']		= $this->get_contacts($in);
		$output['addresses']		= $this->get_addresses($in);		
		$output['mandate_id']		= $in['mandate_id'];
		//$output['add_customer']		= !$in['buyer_id'] && !$in['contact_id'] ? true : false;
		$output['add_customer']		= false;
		$output['type_mandate_dd']    = array(array('id'=>'0','name'=>gm('Select')),array('id'=>'1','name'=>gm('Core')));
		$output['frequency_dd']    	= array(array('id'=>'0','name'=>gm('Select')),array('id'=>'1','name'=>'One-Off'),array('id'=>'2','name'=>'Recurring'));
		$output['type_mandate'] 	= '1';
		$output['frequency']		= '2';
		$output['bank_iban']		= $bank_iban;
		$output['bank_bic_code']	= $bank_bic;
		$output['language_dd']      = build_language_dd();
        $output['gender_dd']        = gender_dd();
        $output['title_dd']         = build_contact_title_type_dd(0);

		$this->out = $output;
	}

	public function getEditMandate(){
		$in=$this->in;
		$mandate = $this->db->query("SELECT * FROM mandate WHERE mandate_id = '".$in['mandate_id']."' ");
		
		$result=array(
			'bank_iban'          	=> $mandate->f('bank_iban'),
			'bank_bic_code'       	=> $mandate->f('bank_bic_code'),
			'type_mandate_dd'       => array(array('id'=>'1','name'=>gm('Core'))),
			'type_mandate' 		=> $mandate->f('type_mandate'),
			'frequency_dd'    	=> array(array('id'=>'1','name'=>'One-Off'),array('id'=>'2','name'=>'Recurring')),
			'frequency'			=> $mandate->f('frequency'),
			'signature_date'		=> $mandate->f('creation_date')*1000,
			'signature_value'		=> $mandate->f('creation_date'),
			'do_next' 			=> 'invoice--invoice-mandate_update',
			'mode_active'		=> $mandate->f('amazon_link')!='' ? true : false,
			'mode'			=> $mandate->f('active_mandate') ? '1' : '0',
			'mode_dd'			=> array(array('id'=>'0','name'=>'Inactive'),array('id'=>'1','name'=>'Active')),
			'selected_active'		=> $mandate->f('active_mandate') ? 'selected' : '',
			'selected_inactive'	=> (!$mandate->f('active_mandate') && $mandate->f('amazon_link')!='') ? 'selected' : '',
			'PDF_hide'			=> $mandate->f('original_name')!='' ? false : true,
			'name_PDF'			=>  $mandate->f('original_name')!='' ? true : false,
			'original_name'		=> $mandate->f('original_name'),
			'sepa_number'		=> $mandate->f('sepa_number'),
			'hide_sepa_number'	=> $mandate->f('active_mandate')==1 ? false : true,
			'mandate_id'		=> $in['mandate_id'],
			'title'			=> gm('Edit Mandate')
		);
		$this->out = $result;
	}

	public function get_cc($in)
	{
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		if($in['internal']=='true'){
			$filter=" is_admin>0 ";
		}
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		if($in['internal'] == 'true'){
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name,our_reference,currency_id,internal_language
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id		
			ORDER BY name
			LIMIT 9")->getAll();
		}else{
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id	
			ORDER BY name
			LIMIT 9")->getAll();
		}
		
		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip_name'] ? $value['zip_name'] : '',
				'city'					=> $value['city_name'] ? $value['city_name'] : '',
				/*"bottom"				=> $value['zip_name'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts($in) {
		if(!$in['buyer_id']){
			return [];
		}
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
				'bottom' 				=> '',
			);

		}

		$added = false;
		if(count($result)==9){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in)
	{
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		'symbol'				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

}

	$mandate_data = new MandateEdit($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$mandate_data->output($mandate_data->$fname($in));
	}

	$mandate_data->getMandate();
	$mandate_data->output();

return $view->fetch();