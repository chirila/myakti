<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

$data = array();
$all_data_array = explode('&', $in['val']);
foreach ($all_data_array as $key => $value) {
	if($value){
		list($name,$val)=explode('=', $value);	
		if($name == 'from' || $name == 'to'){
			$val = urldecode($val);
		}
	}	
	$data[$name] = $val;
}
//this month
$this_month_start_date= mktime(0, 0, 0, date('m'), 1, date('Y'));
$this_month_end_date= mktime(23, 59, 59, date("m")+1 , 0, date("Y"));

//last month
$last_month_start_date= mktime(0, 0, 0, date('m')-1, 1, date('Y'));
$last_month_end_date= mktime(23, 59, 59, date("m") , 0, date("Y"));

$result=array(
	'disabled_this_month' 	=> true,
	'disabled_last_month' 	=> true,
	'from'            	=> is_numeric($data['begin_time']) ? $data['begin_time']*1000 : strtotime($data['begin_time'])*1000,
	'begin_time2' 		=> date(ACCOUNT_DATE_FORMAT,$data['begin_time']),
	'begin_time'           => $data['begin_time'],
	'to'            		=> is_numeric($data['end_time']) ? $data['end_time']*1000 : strtotime($data['end_time'])*1000,
	'end_time2'            	=> date(ACCOUNT_DATE_FORMAT,$data['end_time']),
	'end_time'            	=> $data['end_time'],
	'hide_custom' 		=> false,
);

switch ($data['custom_hours']) {
	case '1':
		$result['custom_hours'] = '1';			
		break;
	case '2':
		$result['custom_hours'] = '2';			
		break;
	case '3':
		$result['custom_hours'] = '3';
		$result['hide_custom'] 	= true;
		break;		
}

if($data['from'] || $data['to']){	
	$begin_time3= mktime(0, 0, 0, date('m',$data['from']), date('d',$data['from']), date('Y',$data['from']));	
	$end_time3= mktime(0, 0, 0, date('m',$data['to']), date('d',$data['to']), date('Y',$data['to']));
	$result['from']        = $begin_time3*1000;	
	$result['to']          = $end_time3*1000;
}

$project_query = $db->query("SELECT * FROM projects WHERE billable_type!='4' AND projects.active>'0' AND project_id = '".$data['project_id']."' ");
while ($project_query->next()) {
	$project_task = array();
	$project_purchase = array();
	$project_expenses = array();
	$increment = false;
	$project_amount = 0;
	$is_retainer = false;

	if($project_query->f('billable_type') && $project_query->f('invoice_method')){
		if($project_query->f('billable_type') != '5'){
			$projects_hours_t_m = $db->field("SELECT SUM(hours) FROM task_time WHERE billable='1' AND billed='0' AND approved='1'  AND project_id='".$project_query->f('project_id')."' AND date >= '".$this_month_start_date."' AND date < '".$this_month_end_date."' ");
			if($projects_hours_t_m){
				$result['disabled_this_month'] 	= false;			
			}
			$projects_hours_l_m = $db->field("SELECT SUM(hours) FROM task_time WHERE billable='1' AND billed='0' AND approved='1'  AND project_id='".$project_query->f('project_id')."' AND date >= '".$last_month_start_date."' AND date < '".$last_month_end_date."' ");
			if($projects_hours_l_m){
				$result['disabled_last_month'] 	= false;		
			}
		}else{
			$tasks = $db->query("SELECT * FROM tasks WHERE project_id='".$project_query->f('project_id')."' AND billable='1' AND closed='1' ");

			while($tasks->next()){				
				$result['disabled_this_month'] 	= false;
				$result['disabled_last_month'] 	= false;			
			}
		}		
		$join = " INNER JOIN project_expences ON ( project_expenses.expense_id = project_expences.expense_id AND project_expenses.project_id = project_expences.project_id ) 
		LEFT JOIN task_time ON ( project_expenses.project_id = task_time.project_id AND project_expenses.date = task_time.date )  ";
		$filter = " AND task_time.approved='1' AND task_time.submited='1' ";
		$select = ', task_time.* , project_expences.project_id AS is_ad_hoc ';
		if($project_query->f('active') == 2){
			$join = ' LEFT JOIN task_time ON ( project_expenses.project_id = task_time.project_id AND project_expenses.date = task_time.date )  ';
			$filter = " AND project_expenses.billable='1' AND task_time.submited = '1' AND task_time.approved = '1' ";
			$select = ', task_time.* ';
		}				
		$expenses_t_m = $db->query("SELECT project_expenses . * , expense.unit_price AS u_p,expense.name ".$select."
														FROM project_expenses
														INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
														".$join."
														WHERE project_expenses.project_id='".$project_query->f('project_id')."' AND project_expenses.billed!='1' ".$filter." AND task_time.date >= '".$this_month_start_date."' AND task_time.date < '".$this_month_end_date."' ");		
		while ($expenses_t_m->next()) {	
			$result['disabled_this_month'] 	= false;				
		}		
		$expenses_l_m = $db->query("SELECT project_expenses . * , expense.unit_price AS u_p,expense.name ".$select."
														FROM project_expenses
														INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
														".$join."
														WHERE project_expenses.project_id='".$project_query->f('project_id')."' AND project_expenses.billed!='1' ".$filter." AND task_time.date >= '".$last_month_start_date."' AND task_time.date < '".$last_month_end_date."' ");		
		while ($expenses_l_m->next()) {	
			$result['disabled_last_month'] 	= false;		
		}				
	}	
}


return json_out($result);
?>