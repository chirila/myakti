<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '2000M');
/*$in['payments_id'] ='';
if($_SESSION['add_to_xls']){
  foreach ($_SESSION['add_to_xls'] as $key => $value) {
    $in['payments_id'] .= $key.',';
  }
}
$in['payments_id'] = rtrim($in['payments_id'],',');
if(!$in['payments_id']){
  $in['payments_id']= '0';
}*/
/*if(!$in['export_data']){
    $fp = fopen(INSTALLPATH.'upload/'.DATABASE_NAME.'/export_payments_list.csv', 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $int_array as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    ark::loadLibraries(array('PHPExcel'));
    $objReader = PHPExcel_IOFactory::createReader('CSV');

    $objPHPExcel = $objReader->load(INSTALLPATH.'upload/'.DATABASE_NAME.'/export_payments_list.csv');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    unlink(INSTALLPATH.'upload/'.DATABASE_NAME.'/export_payments_list.csv');

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="export_payments_list.xls"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');   
    exit();
}*/
$export_data=(array) json_decode(urldecode(base64_decode($in['export_data'])));
foreach($export_data as $key=>$val){
  $in[$key]=$val;
}
unset($in['export_data']);

$l_r = ROW_PER_PAGE;
$order_by_array = array('date');
$filter = "WHERE 1=1 ";
$filter_arch= "";
$order_by=" ORDER BY date DESC "; 
if((!$in['offset']) || (!is_numeric($in['offset']))){
    $offset=0;
    $in['offset']=1;
}else{
    $offset=$in['offset']-1;
}

if(!empty($in['order_by'])){
  if(in_array($in['order_by'], $order_by_array)){
    $order = " ASC ";
    if($in['desc'] == 'true'){
      $order = " DESC ";
    }
    $order_by =" ORDER BY ".$in['order_by']." ".$order; 
  }
}
if(!empty($in['search'])){
    $filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%') ";
    $arguments_s.="&search=".$in['search'];
}

if($in['payment_method_id'] && is_array($in['payment_method_id']) && !empty($in['payment_method_id'])){
  $payment_method="";
  if(count($in['payment_method_id'])==1 && $in['payment_method_id'][0]=='0'){
    foreach($locations as $key=>$value){
      $payment_method.="'".$value['id']."',";
    }
  }else{
    foreach($in['payment_method_id'] as $key=>$value){
      $payment_method.="'".$value."',";
    }   
  }
  $payment_method=rtrim($payment_method,",");
  $filter.= " AND tblinvoice_payments.payment_method IN (".$payment_method.") ";
}

if(!empty($in['start_date_js'])){
    $in['start_date'] =strtotime($in['start_date_js']);
    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
    $in['stop_date'] =strtotime($in['stop_date_js']);
    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}

if(!empty($in['start_date']) && !empty($in['stop_date'])){
  $filter.=" and UNIX_TIMESTAMP(tblinvoice_payments.date) BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
}else if(!empty($in['start_date'])){
  $filter.=" and UNIX_TIMESTAMP(tblinvoice_payments.date ) > ".$in['start_date']." ";
}else if(!empty($in['stop_date'])){
  $filter.=" and UNIX_TIMESTAMP(tblinvoice_payments.date ) < ".$in['stop_date']." ";
}

/*require_once 'libraries/PHPExcel.php';
$filename ="export_payments_list.xls";

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Payments list")
							 ->setSubject("Payments list")
							 ->setDescription("Export payments list")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Payments");*/
$db = new sqldb();

/*$info = $db->query("SELECT tblinvoice_payments.*, tblinvoice.serial_number, tblinvoice.buyer_name, tblinvoice.currency_type
                  FROM tblinvoice_payments 
                  INNER JOIN tblinvoice ON tblinvoice_payments.invoice_id = tblinvoice.id 
                    WHERE payment_id IN (".$in['payments_id'].")
                    ORDER BY date DESC");*/
$info = $db->query("SELECT tblinvoice_payments.*, tblinvoice.serial_number, tblinvoice.buyer_name, tblinvoice.currency_type
                  FROM tblinvoice_payments 
                  INNER JOIN tblinvoice ON tblinvoice_payments.invoice_id = tblinvoice.id ".$filter.$order_by);

/*$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1',gm("Date"))
            ->setCellValue('B1',gm("Amount"))
            ->setCellValue('C1',gm("Payment Method"))
            ->setCellValue('D1',gm("Invoice"))
            ->setCellValue('E1',gm("Buyer Name"));

   $xlsRow=2;*/
   $headers=array("DATE",
          "AMOUNT",
          "PAYMENT METHOD",
          "INVOICE",
          "BUYER NAME"
  );
  $filename ="export_payments_list.csv";

  $final_data=array();
  $payment_methods_array = invoice_payment_method_dd();

while ($info->next()){

  $date_payment = date(ACCOUNT_DATE_FORMAT,  strtotime($info->f('date')));
  $amount= $info->f('amount');
  $payment_method_name=get_invoice_payment_method($payment_methods_array, $info->f('payment_method'));
  $serial_number= $info->f('serial_number');
  $buyer_name= $info->f('buyer_name');

  /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $date_payment)
              ->setCellValue('B'.$xlsRow, $amount)
  	          ->setCellValue('C'.$xlsRow, $payment_method_name)
              ->setCellValue('D'.$xlsRow, $serial_number)
              ->setCellValue('E'.$xlsRow, $buyer_name);
  $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
  $xlsRow++;*/
  $item=array(
      $date_payment,
      display_number_exclude_thousand($amount),
      $payment_method_name,
      $serial_number,
      $buyer_name
  );
  array_push($final_data,$item);
}
header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$fp = fopen("php://output", 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
exit();


// set column width to auto
foreach(range('A','W') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>