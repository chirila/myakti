<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

class InvoiceEdit extends Controller{
	private $invoice;
	private $invoice_id;
	private $ALLOW_ARTICLE_PACKING;
	private $ALLOW_ARTICLE_SALE_UNIT;
	private $negative;
	private $spliter;

	function __construct($in,$db = null)
	{
		if($in['duplicate_invoice_id']){
			$in['change']=1;
		}
		parent::__construct($in,$db = null);
		$this->ALLOW_ARTICLE_PACKING=$this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$this->ALLOW_ARTICLE_SALE_UNIT=$this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
		$this->negative=1;
		if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
			$this->negative = -1;
		}
		$this->spliter=substr(ACCOUNT_DATE_FORMAT, 1, 1);		
	}

	public function getInvoice(){
		if($this->in['invoice_id'] == 'tmp'){ //add
			$this->getAddInvoice();
		}elseif ($this->in['template']) { # create quote template
			$this->getTemplateInvoice();
		}else{ # edit
			$this->getEditInvoice();
		}
	}

	private function getAddInvoice(){
		$in = $this->in;
		if($in['type']==1){
			$page_title=gm('Add Proforma Invoice');
		}elseif($in['type']==2 || $in['type']==4){
			$page_title=gm('Credit Invoice');
		}else{
			$page_title=gm('Add Invoice');
		}
		$to_edit=false;
		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_4' ");
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
				$to_edit=true;
				break;
		}
		//$in['email_language'] 			= (string)DEFAULT_LANG_ID;
		$do_next='invoice-ninvoice-invoice-updateCustomerData';
		$default_note = $this->db->field("SELECT value FROM default_data WHERE type='invoice_note' ");
		$our_ref = '';
		$shipping_vat = 0;
		$shipping_price = 0;
		if(!$in['invoice_date']){
			$in['invoice_date']=time();
			$in['quote_ts'] = time();
		}else{
			$in['invoice_date']=strtotime($in['invoice_date']);
			$in['quote_ts'] = strtotime($in['quote_ts']);
		}

		if($in['deal_id']){
			$output['deal_id']=$in['deal_id'];
			$deal_data=$this->db->query("SELECT * FROM tblopportunity WHERE opportunity_id='".$in['deal_id']."' ");
			if($deal_data->next()){
				if(!$in['buyer_id'] && $deal_data->f('buyer_id')){
					$in['buyer_id']=$deal_data->f('buyer_id');
					$output['add_customer']=false;
				}
				if(!$in['contact_id'] && $deal_data->f('contact_id')){
					$in['contact_id']=$deal_data->f('contact_id');
				}
				$in['subject']=$deal_data->f('subject');
				$in['main_address_id']=$deal_data->f('main_address_id');
				$in['delivery_address_id']=$deal_data->f('same_address');
				$in['identity_id']=$deal_data->f('identity_id');
				$in['source_id']=$deal_data->f('source_id');
				$in['type_id']=$deal_data->f('type_id');
				$in['segment_id']=$deal_data->f('segment_id');
			}		
		}

		$details = array('table'	 => $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field' 	 => $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'	 => $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],					 
					 'filter'	 => $in['buyer_id'] ? ' AND billing =1' : '',
					);

		$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
		$buyer_details->next();
		$customer_id = $in['buyer_id'];

		$due = $in['quote_ts'] + ( 30 * (60*60*24) ) ;
		$due_days = 30;
		$apply_discount = "0";
		$cat_id = 0;

		$customer_notes="";
		$customer_credit_limit="";
		$has_third_party=false;
		$third_party_id='';
		$third_party_name='';
		$third_party_address='';
		$third_party_regime_id='';
		if($in['buyer_id'] ){
			$buyer_info = $this->db->query("SELECT customers.payment_term,customers.payment_term_type, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name,
					customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.invoice_note2,  customers.customer_notes, customers.internal_language,
					customers.attention_of_invoice,customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc, customers.name AS company_name, customers.vat_regime_id,customers.identity_id,customers.cat_id,customers.third_party_id
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$in['buyer_id']."' ");
			$buyer_info->next();
			$customer_notes=stripslashes($buyer_info->f('customer_notes'));

			$buyer_credit_limit_info=$this->db->query("SELECT credit_limit,currency_id FROM customers WHERE customer_id='".$in['buyer_id']."' ");
			if($buyer_credit_limit_info->f('credit_limit')){
				$invoices_customer_due=customer_credit_limit($in['buyer_id']);
				if($invoices_customer_due && $invoices_customer_due>$buyer_credit_limit_info->f('credit_limit')){
					$buyer_currency_id=$buyer_credit_limit_info->f('currency_id') ? $buyer_credit_limit_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
					$customer_credit_limit=place_currency(display_number($buyer_credit_limit_info->f('credit_limit')),get_commission_type_list($buyer_currency_id));
				}
			}

			$due_days = $buyer_info->f('payment_term');
			$due = $in['quote_ts'] + ( $buyer_info->f('payment_term') * (60*60*24) ) ;
			$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');

			$text = gm('Company Name').':';
			$c_email = $buyer_info->f('c_email');
			$c_fax = $buyer_details->f('comp_fax');
			$c_phone = $buyer_details->f('comp_phone');
			$cat_id = $buyer_info->f('cat_id');
			
			$in['attention_of'] = $buyer_info->f('attention_of_invoice');
			if($in['third_party_applied'] && $in['third_party_id']){
				$in['discount']= $in['discount'] ? return_value($in['discount']) : 0;
			}else if($buyer_info->f('apply_fix_disc')){
				$in['discount'] = $buyer_info->f('fixed_discount');
			}
			//$in['remove_vat'] = $buyer_info->f('no_vat');
			//$in['notes2'] = $buyer_info->f('invoice_note2');
			$in['notes2'] = $buyer_info->f('customer_notes');
			if($in['third_party_applied'] && $in['third_party_id']){
				$in['discount_line_gen']=$in['discount_line_gen'] ? return_value($in['discount_line_gen']) : 0;
			}else{
				$in['discount_line_gen']= display_number($buyer_info->f('line_discount'));
			}
			
			if(!$in['change_currency']){
				$in['currency_type'] = $buyer_info->f('currency_id');
			}
			
			$in['identity_id']	= $in['identity_id'] ? $in['identity_id'] : get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);
			$default_email_language = $this->db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
			/*if($buyer_info->f('internal_language')==0){
				 $in['email_language'] = $default_email_language;
			}else{*/
				$in['email_language'] = (string)$buyer_info->f('internal_language');
			//}

			//Set email language as account / contact language
		      $emailMessageData = array('buyer_id'    => $in['buyer_id'],
		                      'contact_id'    => $in['contact_id'],
		                      'item_id'     => $in['invoice_id'],
		                      'email_language'  => $in['email_language'],
		                      'table'     => 'tblinvoice',
		                      'table_label'   => 'id',
		                      'table_buyer_label' => 'buyer_id',
		                      'table_contact_label' => 'contact_id',
		                      'param'     => 'update_customer_data');
		      $in['email_language'] = get_email_language($emailMessageData);
		      //End Set email language as account / contact language
		      
			//$in['email_language'] = (string)$buyer_info->f('internal_language');			

			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$apply_discount = "3";
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$apply_discount = "2";
				}
				if($buyer_info->f('apply_line_disc')){
					$apply_discount = "1";
				}
			}
			//$in['vat']=get_customer_vat($in['buyer_id']);
			$in['vat_regime_id'] = $buyer_info->f('vat_regime_id');	
			
			if(!$in['vat_regime_id']){
				$vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

			    if(empty($vat_regime_id)){
			      $vat_regime_id = 1;
			    }
				$in['vat_regime_id'] = $vat_regime_id;
			}

			$in['apply_discount'] = $in['apply_discount'] ? $in['apply_discount'] : ($apply_discount ? $apply_discount : (defined('INVOICE_APPLY_DISCOUNT') ? INVOICE_APPLY_DISCOUNT : '0'));
			$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");		
			$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
						LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
						WHERE vat_new.id='".$in['vat_regime_id']."' ");
			if($buyer_info->f('third_party_id') && !$in['third_party_applied']){
				$has_third_party=true;
				$third_party_info = $this->db->query("SELECT customer_legal_type.name as l_name,customers.name AS company_name,customers.vat_regime_id
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$buyer_info->f('third_party_id')."' ");
				$third_party_id=$buyer_info->f('third_party_id');
				$third_party_name=stripslashes($third_party_info->f('company_name').' '.$third_party_info->f('l_name'));
				$third_party_regime_id=$third_party_info->f('vat_regime_id');
				$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$buyer_info->f('third_party_id')."' AND billing='1' ");
				if(!$third_party_details->next()){
					$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$buyer_info->f('third_party_id')."' AND is_primary='1' ");
				}
				$third_party_address=strip_tags($third_party_details->f('address')).' '.$third_party_details->f('zip').' '.$third_party_details->f('city').' '.get_country_name($third_party_details->f('country_id'));
			}else if($in['third_party_applied'] && $in['third_party_id']){
				$has_third_party=true;
				$third_party_info = $this->db->query("SELECT customer_legal_type.name as l_name,customers.name AS company_name,customers.vat_regime_id
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$in['third_party_id']."' ");
				$third_party_id=$in['third_party_id'];
				$third_party_name=stripslashes($third_party_info->f('company_name').' '.$third_party_info->f('l_name'));
				$third_party_regime_id=$third_party_info->f('vat_regime_id');
				$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['third_party_id']."' AND billing='1' ");
				if(!$third_party_details->next()){
					$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['third_party_id']."' AND is_primary='1' ");
				}
				$third_party_address=strip_tags($third_party_details->f('address')).' '.$third_party_details->f('zip').' '.$third_party_details->f('city').' '.get_country_name($third_party_details->f('country_id'));
			}
			if($in['base_type']){
				if($in['base_type']=='1'){
					if(!empty($in['projects_id'])){
						$in['vat_regime_id']=$this->db->field("SELECT vat_regime_id FROM projects WHERE project_id='".$in['projects_id'][0]."' ");
						if($in['vat_regime_id']){
							$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");		
							$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
									LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
									WHERE vat_new.id='".$in['vat_regime_id']."' ");
						}
						$proj_info=$this->db->query("SELECT identity_id,source_id,type_id,segment_id, contact_id FROM projects WHERE project_id='".$in['projects_id'][0]."'");
						$in['identity_id']=$proj_info->f('identity_id');
						$in['source_id']=$proj_info->f('source_id');
						$in['type_id']=$proj_info->f('type_id');
						$in['segment_id']=$proj_info->f('segment_id');
						$in['contact_id']=$proj_info->f('contact_id');
					}			
				}else if($in['base_type']=='3'){
					if(is_array($in['orders_id'])){
						if(!empty($in['orders_id'])){
							$in['vat_regime_id']=$this->db->field("SELECT vat_regime_id FROM pim_orders WHERE order_id='".$in['orders_id'][0]."' ");
							if($in['vat_regime_id']){
								$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");		
								$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
										LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
										WHERE vat_new.id='".$in['vat_regime_id']."' ");
							}	
							$ord_info=$this->db->query("SELECT identity_id,source_id,type_id,segment_id, yuki_project_id FROM pim_orders WHERE order_id='".$in['orders_id'][0]."'");
							$in['identity_id']=$ord_info->f('identity_id');
							$in['source_id']=$ord_info->f('source_id');
							$in['type_id']=$ord_info->f('type_id');
							$in['segment_id']=$ord_info->f('segment_id');
							$in['cat_id']=$ord_info->f('cat_id');
							$in['yuki_project_id']=$ord_info->f('yuki_project_id');
						}
					}else{
						if(!$in['third_party_applied']){
							$in['vat_regime_id']=$this->db->field("SELECT vat_regime_id FROM pim_orders WHERE order_id='".$in['orders_id']."' ");
						}						
						if($in['vat_regime_id']){
							$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");		
							$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
									LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
									WHERE vat_new.id='".$in['vat_regime_id']."' ");
						}
						$ord_info=$this->db->query("SELECT identity_id,source_id,type_id,segment_id, yuki_project_id,trace_id FROM pim_orders WHERE order_id='".$in['orders_id']."'");
						$in['identity_id']=$ord_info->f('identity_id');
						$in['source_id']=$ord_info->f('source_id');
						$in['type_id']=$ord_info->f('type_id');
						$in['segment_id']=$ord_info->f('segment_id');
						$in['cat_id']=$ord_info->f('cat_id');
						$in['yuki_project_id']=$ord_info->f('yuki_project_id');
						$in['deal_id']=$this->db->field("SELECT tracking_line.origin_id FROM tracking_line
							INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
							LEFT JOIN tblopportunity ON tracking_line.origin_id = tblopportunity.opportunity_id AND tblopportunity.f_archived='0'
							WHERE tracking_line.origin_type='11' AND tracking.target_type='4' AND tracking.target_id='".$in['orders_id']."' AND tracking_line.trace_id='".$ord_info->f('trace_id')."' ");
					}
				}else if($in['base_type']=='4'){
					if(!empty($in['tasks_id'])){
						$in['vat_regime_id']= $this->db->field("SELECT projects.vat_regime_id FROM tasks
                             				INNER JOIN projects ON tasks.project_id = projects.project_id
                             				WHERE task_id='".$in['tasks_id'][0]['id']."' ");
						if($in['vat_regime_id']){
							$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");		
							$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
									LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
									WHERE vat_new.id='".$in['vat_regime_id']."' ");
						}
						$proj_info=$this->db->query("SELECT projects.identity_id,projects.source_id,projects.type_id,projects.segment_id  FROM tasks
                             				INNER JOIN projects ON tasks.project_id = projects.project_id
                             				WHERE task_id='".$in['tasks_id'][0]['id']."' ");
						$in['identity_id']=$proj_info->f('identity_id');
						$in['source_id']=$proj_info->f('source_id');
						$in['type_id']=$proj_info->f('type_id');
						$in['segment_id']=$proj_info->f('segment_id');
					}
				}else if($in['base_type']=='5'){
					if(!empty($in['project_purchases_id'])){
						$in['vat_regime_id']= $this->db->field("SELECT projects.vat_regime_id FROM project_purchase
                             				INNER JOIN projects ON project_purchase.project_id = projects.project_id
                             				WHERE project_purchase_id='".$in['project_purchases_id'][0]['id']."' ");
						if($in['vat_regime_id']){
							$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");		
							$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
									LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
									WHERE vat_new.id='".$in['vat_regime_id']."' ");
						}
						$proj_info=$this->db->query("SELECT projects.identity_id,projects.source_id,projects.type_id,projects.segment_id 
								FROM project_purchase
                             				INNER JOIN projects ON project_purchase.project_id = projects.project_id
                             				WHERE project_purchase_id='".$in['project_purchases_id'][0]['id']."' ");
						$in['identity_id']=$proj_info->f('identity_id');
						$in['source_id']=$proj_info->f('source_id');
						$in['type_id']=$proj_info->f('type_id');
						$in['segment_id']=$proj_info->f('segment_id');
					}
				}else if($in['base_type']=='6'){
					if(!$in['third_party_applied']){
						$in['vat_regime_id'] = $this->db->field("SELECT vat_regime_id FROM servicing_support WHERE service_id='".$in['service_id']."' ");
					}				
					if($in['vat_regime_id']){
						$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");		
						$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
									LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
									WHERE vat_new.id='".$in['vat_regime_id']."' ");
					}
					$int_info=$this->db->query("SELECT identity_id,source_id,type_id,segment_id FROM servicing_support WHERE service_id='".$in['service_id']."'");
					$in['identity_id']=$int_info->f('identity_id');
					$in['source_id']=$int_info->f('source_id');
					$in['type_id']=$int_info->f('type_id');
					$in['segment_id']=$int_info->f('segment_id');
				}
			}
			
			/*if($buyer_info->f('identity_id') && !$in['identity_id']){
				$in['identity_id'] = $buyer_info->f('identity_id');
			}*/
		
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND billing='1' ");
			if(!$buyer_details->next()){
				$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			}
			$main_address_id			= $buyer_details->f('address_id');
			//$sameAddress = false;
			//if($buyer_details->f('billing')==1){
				$sameAddress = true;
			//}
			//$do_next='invoice-ninvoice-invoice-add';
		}else{
			$buyer_info = $this->db->query("SELECT phone, cell, email, CONCAT_WS(' ',firstname, lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
			$buyer_info->next();
			$c_email = $buyer_info->f('email');
			$c_fax = '';
			$c_phone = $buyer_info->f('phone');
			$text =gm('Name').':';
			$name = $buyer_info->f('name');
			if($in['contact_id']){
				//$do_next='invoice-ninvoice-invoice-add';
			}
		}

		$vat = $in['vat'] ? display_number($in['vat']) : display_number(0);
		if(!$in['req_payment'])	{
	       	$in['req_payment']=100;
	 	}
	 	if(!$in['due_days']){
	 		$in['due_days'] = $due_days;
	 	}else{
	 		$in['due_days']=return_value($in['due_days']);
	 	}
		$baset_type_delivery= $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 AND is_primary!=1 ");
	 	if($in['base_type']==3 && $baset_type_delivery->f('address_id')==$buyer_details->f('address_id')){
	 		$free_field = $baset_type_delivery->f('address').'
			'.$baset_type_delivery->f('zip').' '.$baset_type_delivery->f('city').'
			'.get_country_name($baset_type_delivery->f('country_id'));
	 	}else{
	 		$free_field = $buyer_details->f('address').'
		'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
		'.get_country_name($buyer_details->f('country_id'));
	 	}
 		

		$name = stripslashes($name);

		$due_date = $in['quote_ts'] + ( 30 * (60*60*24) ) ;
		$due_date = $in['quote_ts'] + ( $buyer_info->f('payment_term') * (60*60*24) ) ;

		if($in['buyer_id'] == '0') {
			$payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
			$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
		} else {
			$payment_term = $this->db->field("SELECT payment_term FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
			$payment_term_type = $this->db->field("SELECT payment_term_type FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
		}

		if($payment_term_type==1) { # invoice date
			$due_date = $in['quote_ts'] + ( $payment_term * ( 60*60*24 )-1);
		} else { # next month first day
			/*$curMonth = date('n',$in['quote_ts']);
			$curYear  = date('Y',$in['quote_ts']);
		    $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
			$due_date = $firstDayNextMonth + ($payment_term * ( 60*60*24 )-1);*/
			$tmstmp_month=$in['quote_ts'] + ( $payment_term * ( 60*60*24 )-1);
			$lastday = date('t',$tmstmp_month);
			$due_date = mktime(23, 59, 59,date('m',$tmstmp_month), $lastday,date('Y',$tmstmp_month));
		}

		if(!$in['invoice_due_date_vat']){
			$in['invoice_due_date_vat']=time();
			$in['due_date_vat'] = time();
		}else{
			$in['invoice_due_date_vat']=strtotime($in['invoice_due_date_vat']);
			$in['due_date_vat'] = strtotime($in['due_date_vat']);
		}

		$existIdentity = $this->db->field("SELECT COUNT(identity_id) FROM multiple_identity ");
		$customer_mandat = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' ");
		$customer_mandat_active = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' AND active_mandate='1' ");
      	$acc_manager=$this->db->field("SELECT acc_manager_name FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
	  	$acc_manager = explode(',',$acc_manager);
	  	$acc_manager_id=$this->db->field("SELECT user_id FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
	  	$acc_manager_id = explode(',',$acc_manager_id);
	  	$item_width=$in['remove_vat'] == 1 ? 5 : 4;

        if ($in['type'] == 2) {
            $page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='CREDIT_GENERAL_CONDITION_PAGE'");
        } else {
            $page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE'");
        }

	  	if($in['buyer_id']){
			if($in['save_final']==1){
				$final=true;
			}else{
				$final=false;
			}
		}
		if($_SESSION['main_u_id']=='0'){
			$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");					
		}else{
			$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
		}
		if($is_accountant){
			$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
			if($accountant_settings=='1'){
				$acc_force=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_FORCE_DRAFT' ");
				if($acc_force){
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					      $this->db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					      $this->db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}else{
				$force_draft=$this->db_users->field("SELECT force_draft FROM accountants WHERE account_id='".$is_accountant."' ");
				if($force_draft){
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					      $this->db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					      $this->db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}		
		}
		$DRAFT_INVOICE_NO_NUMBER=$this->db->field("SELECT value FROM settings WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
		/*if(($in['type']==0 || $in['type']==3) && $DRAFT_INVOICE_NO_NUMBER!='1' ){
			$generate_number_invoice=generate_invoice_number(DATABASE_NAME);
		}elseif(($in['type'] == 2 || $in['type']==4) && $DRAFT_INVOICE_NO_NUMBER!='1'){
			$generate_number_invoice=generate_credit_invoice_number(DATABASE_NAME);
		}*/
		$baset_type_delivery_address= $this->db->field("SELECT address_id FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 AND is_primary!=1 ");

	  	$in['languages']				= $in['email_language'];

	  	if($in['main_address_id'] && $in['main_address_id']!=$main_address_id){
			$main_address_id=$in['main_address_id'];
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$free_field = $buyer_details->f('address').'
				'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
				'.get_country_name($buyer_details->f('country_id'));
		}
		if($in['delivery_address_id']){
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
			$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
		}
		if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
			$acc_langs=array('en','fr','nl','de');
			$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
			if($lang_c){
				if($lang_c=='du'){
					$lang_c='nl';
				}
				if(in_array($lang_c, $acc_langs)){
					$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
					if($in['vat_notes']){
						if($in['notes2']){
							$in['notes2']=$in['notes2']."\n".$in['vat_notes'];
						}else{
							$in['notes2']=$in['vat_notes'];
						}
					}
				}
			}		
		}

		$output=array(
			//General Details
			'apply_discount_line'			=> $in['apply_discount']==1 || $in['apply_discount']==3 ? true :  (defined('INVOICE_APPLY_DISCOUNT') && INVOICE_APPLY_DISCOUNT=='1' ? true : false ),
			'apply_discount_global'			=> $in['apply_discount_global'] ? true : false,
			'apply_discount'				=> $in['apply_discount'],
			//'show_discount'				=> INVOICE_APPLY_DISCOUNT == 1 ? true : false,
			'ADV_PRODUCT' 					=> defined('ADV_PRODUCT') && ADV_PRODUCT == 1 ? true : false,
			'draft_invoice'					=> $DRAFT_INVOICE_NO_NUMBER == 1 ? true : false,
			'save_final'					=> $final,
		    'main_address_id'				=> $main_address_id,
		    'sameAddress'					=> $in['delivery_address_id'] ? false : true,
		    'language_dd'					=> build_language_dd(),
			'gender_dd'						=> gender_dd(),
			'title_dd'						=> build_contact_title_type_dd(0),
			'title_id'						=> '0', 
			'gender_id'						=> '0',
			'language_id'					=> '0',
		    'main_country_id'				=> @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26',
			'page'							=> $page_invoice == 1 ? true : false,
			'show_product_save'         	=> 1,
			'type'                      	=> $in['type'] ? $in['type'] : 0,
			'hide_proforma'             	=> $in['type']==1 || $DRAFT_INVOICE_NO_NUMBER=='1' ? false : true,
			'hide_regular'              	=> $in['type']==1? true : false,
			'vat'                      		=> $vat,
			'vatx_txt'                  	=> $in['remove_vat'] == 1 ? '-' : $vat.' %',
			'seller_name'               	=> $in['seller_name']?$in['seller_name']:ACCOUNT_COMPANY,
		  	//'serial_number'             	=> $generate_number_invoice,
			'last_serial_number'        	=> get_last_invoice_number(),
			'invoice_date'              	=> is_numeric($in['invoice_date']) ? $in['invoice_date']*1000 : strtotime($in['invoice_date'])*1000,
			'show_due_date_vat_pdf'	    	=> SHOW_DUE_DATE_VAT_PDF == 1 ? true : false,
			'due_date_vat'              	=> $in['due_date_vat'],
			'invoice_due_date_vat'	    	=> is_numeric($in['invoice_due_date_vat']) ? $in['invoice_due_date_vat']*1000 : strtotime($in['invoice_due_date_vat'])*1000,
			'invoice_due_date'				=> $due_date*1000,
			'invoice_due'					=> $due*1000,
			'quote_ts'						=> $in['quote_ts']*1000,

			'due_days'						=> $payment_term,
			'payment_term_type_label'		=> $payment_term_type==1 ? gm('Payment term(days) from the invoice date') : gm('Payment term(days) from the first day of the next month'),
			'payment_type_choose'			=> $payment_term_type==1 ? 1 : 2,
			'serial_number_txt'				=> $in['type']==0 || $in['type']==3 ? gm('Invoice Number') : ($in['type']==2 || $in['type']==4 ? gm('Credit Invoice Number') : gm('Proforma Invoice Number')) ,
			'req_payment'              		=> display_number($in['req_payment']),
			'notes'                     	=> $in['notes'] ? utf8_decode($in['notes']) : ($default_note ? utf8_decode($default_note) : ''),
			'notes2'                    	=> $in['notes2'],
			'invoice_title'					=> gm('Invoice Nr').'*:',
			'due_days2'						=> $due_days,

			//Buyer Details
			'attention_of'              	=> $in['attention_of'],
			/*'add_customer'					=> !$in['buyer_id'] && !$in['contact_id'] ? true : false,*/
			'add_customer'					=> false,

			'buyer_id'                  	=> $in['buyer_id'],
			'customer_id'					=> $in['buyer_id'],
			'contact_id'                	=> $in['contact_id'],
			'buyer_name'                	=> $name,
			'buyer_country_id'          	=> $buyer_details->f('country_id'),
			'buyer_country_name'        	=> get_country_name($buyer_details->f('country_id')),
			'buyer_state_id'            	=> $buyer_details->f('state_id'),
			'buyer_state_name'          	=> get_state_name($buyer_details->f('state_id')),
			'buyer_city'                	=> $buyer_details->f('city'),
			'buyer_zip'                		=> $buyer_details->f('zip'),
			'buyer_address'             	=> $buyer_details->f('address'),
			'buyer_email'             		=> $c_email,
			'buyer_fax'        				=> $c_fax,
			'buyer_phone'             		=> $c_phone,
			'name_text'						=> $text,
			'rel'							=> '',
			'hide_sync'						=> false,
			'no_credit_note'				=> true,
			'your_ref'            	 		=> $in['your_ref'],
			'remove_vat'					=> $in['remove_vat'],
			'field'							=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
			'country_dd'					=> build_country_list(0),

			//Seller Delivery Details
			'seller_d_address'  	  	=> $in['seller_d_address'] ? $in['seller_d_address'] : ACCOUNT_DELIVERY_ADDRESS,
			'seller_d_zip'   		  	=> $in['seller_d_zip'] ? $in['seller_d_zip'] : ACCOUNT_DELIVERY_ZIP,
			'seller_d_city'   			=> $in['seller_d_city'] ? $in['seller_d_city'] : ACCOUNT_DELIVERY_CITY,
			'seller_d_country_dd'	   	=> build_country_list($in['seller_d_country_id']?$in['seller_d_country_id']:ACCOUNT_DELIVERY_COUNTRY_ID),
			'seller_d_country_id'		=> $in['seller_d_country_id']? $in['seller_d_country_id'] : ACCOUNT_DELIVERY_COUNTRY_ID,
			/*'seller_d_state_dd'		=> build_state_list($in['seller_d_state_id']?$in['seller_d_state_id']:ACCOUNT_DELIVERY_STATE_ID,$in['seller_d_country_id']?$in['seller_d_country_id']:ACCOUNT_DELIVERY_COUNTRY_ID),*/

			//Seller Billing Details
			'seller_b_address'    		=> $in['seller_b_address'] ? $in['seller_b_address'] : ACCOUNT_BILLING_ADDRESS,
			'seller_b_zip'   		   	=> $in['seller_b_zip']?$in['seller_b_zip']:ACCOUNT_BILLING_ZIP,
			'seller_b_city'   			=> $in['seller_b_city']?$in['seller_b_city']:ACCOUNT_BILLING_CITY,
			'seller_b_country_dd'	    => build_country_list($in['seller_b_country_id']?$in['seller_b_country_id']:ACCOUNT_BILLING_COUNTRY_ID),
			'seller_b_country_id'		=> $in['seller_b_country_id']?$in['seller_b_country_id']:ACCOUNT_BILLING_COUNTRY_ID,
			/*'seller_b_state_dd'		=> build_state_list($in['seller_b_state_id']?$in['seller_b_state_id']:ACCOUNT_BILLING_STATE_ID,$in['seller_b_country_id']?$in['seller_b_country_id']:ACCOUNT_BILLING_COUNTRY_ID),*/
			'seller_bwt_nr'   			=> $in['seller_bwt_nr']?$in['seller_bwt_nr']:$buyer_info->f('btw_nr'),

			'tr_id'                     => 'tmp0',
			'hide_vattd'				=> $in['remove_vat'] == 1 ? false : true,
			'item_width'				=> $in['remove_vat'] == 1 ? 5 : 4,
			'no_vat'					=> $in['remove_vat'] == 1 ? false : true,
			'show_vat'					=> $in['remove_vat'] == 1 ? true : false,
			'allow_stock'               => ALLOW_STOCK,
			'free_field'				=> $in['delivery_address'] ? $in['delivery_address'] : $free_field,
			'free_field_txt'			=> $in['delivery_address'] ? nl2br($in['delivery_address']) : nl2br($free_field),
			'disc_line'					=> display_number($buyer_info->f('line_discount')),
			'is_duplicate'             	=> true,
			'vat_regime_dd'				=> build_vat_regime_dd($in['vat_regime_id']),
			'vat_regime_id'				=> $in['vat_regime_id'] ? $in['vat_regime_id'] : '0',
			'vatr_txt'					=> $this->db->field("SELECT value from vatregim WHERE id='r".$in['vat_regime_id']."' "),

    		'acc_manager_txt'    		=>$acc_manager[0],
    		'acc_manager_id'     		=>$acc_manager_id[0],
    		'acc_manager_name'   		=>$acc_manager[0],

			'multiple_identity_txt' 	=> $this->db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$in['identity_id']."'"),
			'identity_id'				=> $in['identity_id'] ? $in['identity_id'] : 0,
			'multiple_identity_dd' 		=> build_identity_dd($in['identity_id']),
			'main_comp_info'			=> $in['identity_id'] ? $this->getInvoiceIdentity($in['identity_id']) : $this->getInvoiceIdentity('0'),

			'existIdentity'				=> $existIdentity > 0 ? true : false,
			'customer_mandat'			=> $customer_mandat ? true : false,
			'customer_mandat_active'	=> $customer_mandat_active ? true : false,
			'sepa_mandate_txt'			=> $this->db->field("SELECT sepa_number from mandate WHERE buyer_id = '".$in['buyer_id']."' "),
			'sepa_mandate_dd' 			=> build_sepa_mandate_dd($customer_mandat_active, $in['buyer_id'], true),
			'sepa_mandates'				=> $customer_mandat_active,
			'delivery_address_id'		=> $in['base_type']==3 ? $baset_type_delivery_address : $in['delivery_address_id'],
			'deal_id'					=> $in['deal_id'],
			'can_change'				=> $to_edit,
			'segment_dd'				=> get_categorisation_segment(),
			'source_dd'					=> get_categorisation_source(),
			'type_dd'					=> get_categorisation_type(),
			'source_id'					=> $in['source_id'],
			'type_id'					=> $in['type_id'],
			'segment_id'				=> $in['segment_id'],
			'price_category_dd' 		=> get_price_categories(),
			'cat_id'            		=> $in['cat_id']? $in['cat_id']: $cat_id,
			'customer_notes'			=> $customer_notes,
			'subject'					=> stripslashes($in['subject']),
			'customer_credit_limit'		=> $customer_credit_limit,
			'has_third_party'			=> $has_third_party,
			'third_party_id'			=> $third_party_id,
			'third_party_name'			=> $third_party_name,
			'third_party_address'		=> $third_party_address,
			'third_party_regime_id'		=> $third_party_regime_id,
			'third_party_applied'		=> $in['third_party_applied'] ? '1' : '',
		);

		$has_admin_rights 					= getHasAdminRights(array('module'=>'invoice'));
		$output['hide_margin']				= defined('HIDE_PROFIT_MARGIN') && HIDE_PROFIT_MARGIN == 1 && !$has_admin_rights ? true : false;

		$yuki_active = $this->db->field("SELECT active FROM apps WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		$output['yuki_project_enabled']			= defined('YUKI_PROJECTS') && YUKI_PROJECTS == 1 && $yuki_active ? true : false;
		$output['yuki_projects']				= get_yuki_projects();
		$output['yuki_project_id']				=  $in['yuki_project_id'];

		if($in['buyer_id'] && ($in['type']=='0' || $in['type']=='3')){
			$output['deals']	= $this->get_deals($in);
		}

		if($in['vat_regime_id'] && $in['vat_regime_id']>=1000){
			$extra_eu=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
			if($extra_eu==2){
				$output['block_vat']=true;
			}
			if($extra_eu=='1'){
				$output['vat_regular']=true;
			}
			$contracting_partner=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
			if($contracting_partner==4){
				$output['block_vat']=true;
			}
		}

		if($in['contact_id']){
			$output['contact_id'] = $in['contact_id'];
			$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
		}

		if($in['base_type']==1){ // a base on projects hours invoice so we have to autocomplete the invoice line with something...
			$j=0;

			$output['show_product_save']			=	0;
			$output['is_timesheets'] 			= 	true;
			$output['attach_timesheet'] 			= 	ATTACH_TIMESHEET_INV ? 'checked' : '';
			$output['attach_timesheet_inv_txt']		= 	ATTACH_TIMESHEET_INV == 1 ? gm('Yes') : gm('No');

			$output['projects_ids']=array();
			$output['timeframes']=array();
			$output['projects_id']=array();			
			foreach ($in['projects_id'] as $in['project_id']){
				array_push($output['projects_ids'], $in['project_id']);
				array_push($output['projects_id'], $in['project_id']);
				$p_ref = $this->db->field("SELECT serial_number FROM projects WHERE project_id='".$in['project_id']."' ");
				$our_ref .= $p_ref.',';
				
				$filter_task ="1=1 AND task_time.billable=1 AND task_time.billed=0 AND task_time.customer_id='".($in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'])."' AND task_time.project_id='".$in['project_id']."' AND task_time.approved='1' ";

				$project_status_rate=$this->db->field("SELECT status_rate FROM projects WHERE project_id='".$in['project_id']."' ");
				//first we have to find the project type (task/person/project.. hourly rate)
				$project_type=$this->db->field("SELECT billable_type FROM projects WHERE project_id='".$in['project_id']."' ");

				//get the start and end period interval
				if($in['hours_type'] ){
					switch ($in['custom_hours']){
						case 1:   //this month
						$invoice_start_date= mktime(0, 0, 0, date('m'), 1, date('Y'));
						$filter_task.=" and task_time.date >= '".$invoice_start_date."'";

						$invoice_end_date= mktime(23, 59, 59, date("m")+1 , 0, date("Y"));
						$filter_task.=" and task_time.date < '".$invoice_end_date."'";

						$time_description ='('.date(ACCOUNT_DATE_FORMAT,$invoice_start_date).' - '.date(ACCOUNT_DATE_FORMAT,$invoice_end_date).')';
						$time_start = $invoice_start_date;
						$time_end = $invoice_end_date;
						break;
						case 2:  //last month
						$invoice_start_date= mktime(0, 0, 0, date('m')-1, 1, date('Y'));
						$filter_task.=" and task_time.date >= '".$invoice_start_date."'";

						$invoice_end_date= mktime(23, 59, 59, date("m") , 0, date("Y"));
						$filter_task.=" and task_time.date < '".$invoice_end_date."'";

						$time_description ='('.date(ACCOUNT_DATE_FORMAT,$invoice_start_date).' - '.date(ACCOUNT_DATE_FORMAT,$invoice_end_date).')';
						$time_start = $invoice_start_date;
						$time_end = $invoice_end_date;
						break;
						case 3:  //custom period

						//list($d, $m, $y) = explode($this->spliter,  date(ACCOUNT_DATE_FORMAT,strtotime($in['invoice_start_date'])));
						$d=date('j', is_numeric($in['invoice_start_date']) ? $in['invoice_start_date'] : strtotime($in['invoice_start_date']) );
						$m=date('n',is_numeric($in['invoice_start_date']) ? $in['invoice_start_date'] : strtotime($in['invoice_start_date']));
						$y=date('Y',is_numeric($in['invoice_start_date']) ? $in['invoice_start_date'] : strtotime($in['invoice_start_date']));

						$invoice_start_date= mktime(0, 0, 0, $m, $d, $y);
						$filter_task.=" and task_time.date >= '".$invoice_start_date."'";
						
						//list($d, $m, $y) = explode($this->spliter,  date(ACCOUNT_DATE_FORMAT,strtotime($in['invoice_end_date'])));
						$d=date('j', is_numeric($in['invoice_end_date']) ? $in['invoice_end_date'] : strtotime($in['invoice_end_date']) );
						$m=date('n',is_numeric($in['invoice_end_date']) ? $in['invoice_end_date'] : strtotime($in['invoice_end_date']));
						$y=date('Y',is_numeric($in['invoice_end_date']) ? $in['invoice_end_date'] : strtotime($in['invoice_end_date']));
						$invoice_end_date= mktime(23, 59, 59, $m, $d, $y);
						$filter_task.=" and task_time.date < '".$invoice_end_date."'";

						$time_description ='('.date(ACCOUNT_DATE_FORMAT,$invoice_start_date).' - '.date(ACCOUNT_DATE_FORMAT,$invoice_end_date).')';
						$time_start = $invoice_start_date;
						$output['invoice_start_date']=$invoice_start_date;
						$time_end = $invoice_end_date;
						$output['invoice_end_date']=$invoice_end_date;
						break;
					}
					//echo $invoice_start_date."-".$invoice_end_date;
				}else{
					$time_description_start=$this->db->field("SELECT task_time.date
			                                       FROM task_time
			                                       WHERE ".$filter_task."
			                                       ORDER BY task_time.date ASC LIMIT 1");

					$time_description_end=$this->db->field("SELECT task_time.date
			                                       FROM task_time
			                                       WHERE ".$filter_task."
			                                       ORDER BY task_time.date DESC LIMIT 1");

					$time_description ='('.date(ACCOUNT_DATE_FORMAT,$time_description_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_description_end).')';
					$time_start = $time_description_start;
					$time_end = $time_description_end;
				}

				if($time_end){
					$in['invoice_due_date_vat']=$time_end*1000;
					$in['due_date_vat'] = $time_end*1000;

					$output['due_date_vat']             = $in['due_date_vat'];
					$output['invoice_due_date_vat']	= $in['invoice_due_date_vat'];
					$output['show_due_date_vat_pdf']	= SHOW_DUE_DATE_VAT_PDF == 1 ? true : false;
				}
				switch ($project_type){
					case 1: //task hourly rate

		//			$j=0;
					//calculate time and prices
					$th_c1=$this->db->query("SELECT task_time.*,tasks.task_name,tasks.t_h_rate,tasks.t_daily_rate,projects.name,SUM(task_time.hours) as total_task_hours, projects.active AS ac
		      	                    FROM task_time
		      	                    INNER JOIN tasks ON tasks.task_id=task_time.task_id
		      	                    INNER JOIN projects ON projects.project_id=task_time.project_id
		      	                    WHERE ".$filter_task."
		      	                    GROUP BY task_time.task_id ");
					while ($th_c1->move_next()) {
						$name = $th_c1->f('name').': '.$th_c1->f('task_name').' '.$time_description;
						if($th_c1->f('ac') == 2){
							$name = $th_c1->f('task_name').' '.$time_description;
						}
						$in['description'][$j]=$name;
						$in['unitmeasure'][$j]='hours';
						$in['quantity'][$j]=$th_c1->f('total_task_hours');
						$in['price'][$j]=$project_status_rate==0 ? $th_c1->f('t_h_rate') : $th_c1->f('t_daily_rate');
						//$in['vat'][$j]=get_customer_vat($in['buyer_id']);
						$in['vat'][$j]=$vat;
						$in['origin_id'][$j]=$in['project_id'];
						$in['origin_type'][$j]='3';
						$in['task_time_ids'][$j]='';
						$th1_c1=$this->db->query("SELECT task_time.*
		      	                     FROM task_time
		      	                     WHERE ".$filter_task." AND task_time.task_id='".$th_c1->f('task_id')."' ");
						while ($th1_c1->move_next()) {
							$in['task_time_ids'][$j].=$th1_c1->f('task_time_id').',';

						}

						$j++;
					}

					break;
					case 2: //person hourly rate
		//			$j=0;
					//calculate time and prices
					$th_c2=$this->db->query("SELECT task_time.*,project_user.user_name,project_user.p_h_rate,project_user.p_daily_rate,projects.name,SUM(task_time.hours) as total_task_hours
		      	                    FROM task_time
		      	                    INNER JOIN projects ON projects.project_id=task_time.project_id
		      	                    INNER JOIN project_user ON project_user.user_id=task_time.user_id AND project_user.project_id=task_time.project_id
		      	                    WHERE ".$filter_task."
		      	                    GROUP BY task_time.user_id ");
					while ($th_c2->move_next()) {
						$in['description'][$j]=$th_c2->f('name').': '.$th_c2->f('user_name').' '.$time_description;
						$in['unitmeasure'][$j]='hours';
						$in['quantity'][$j]=$th_c2->f('total_task_hours');
						$in['price'][$j]=$project_status_rate==0 ? $th_c2->f('p_h_rate') : $th_c2->f('p_daily_rate');
						//$in['vat'][$j]=get_customer_vat($in['buyer_id']);
						$in['vat'][$j]=$vat;
						$in['origin_id'][$j]=$in['project_id'];
						$in['origin_type'][$j]='3';
						$in['task_time_ids'][$j]='';
						$th1_c2=$this->db->query("SELECT task_time.*
		      	                     FROM task_time
		      	                     WHERE ".$filter_task." AND task_time.user_id='".$th_c2->f('user_id')."' ");
						while ($th1_c2->move_next()) {
							$in['task_time_ids'][$j].=$th1_c2->f('task_time_id').',';
						}

						$j++;
					}
					break;
					case 3: //project hourly rate
		//			$j=0;
					//calculate time and prices
					$th_c3=$this->db->query("SELECT task_time.*,projects.name,projects.pr_h_rate,SUM(task_time.hours) as total_task_hours
		      	                    FROM task_time
		      	                    INNER JOIN projects ON projects.project_id=task_time.project_id
		      	                    WHERE ".$filter_task."
		      	                    GROUP BY task_time.project_id ");
					while ($th_c3->move_next()) {
						$in['description'][$j]=$th_c3->f('name').' : '.$time_description;
						$in['unitmeasure'][$j]='hours';
						$in['quantity'][$j]=$th_c3->f('total_task_hours');
						$in['price'][$j]=$th_c3->f('pr_h_rate');
						//$in['vat'][$j]=get_customer_vat($in['buyer_id']);
						$in['vat'][$j]=$vat;
						$in['origin_id'][$j]=$in['project_id'];
						$in['origin_type'][$j]='3';
						$in['task_time_ids'][$j]='';
						$th1_c3=$this->db->query("SELECT task_time.*
		      	                     FROM task_time
		      	                     WHERE ".$filter_task." AND task_time.project_id='".$th_c3->f('project_id')."' ");
						while ($th1_c3->move_next()) {
							$in['task_time_ids'][$j].=$th1_c3->f('task_time_id').',';
						}

						$j++;
					}

					break;

					case 4: //no hourly rate
		//			$j=0;
					//calculate time and prices
					$th_c4=$this->db->query("SELECT task_time.*,projects.name,SUM(task_time.hours) as total_task_hours
		      	                    FROM task_time
		      	                    INNER JOIN projects ON projects.project_id=task_time.project_id
		      	                    WHERE ".$filter_task."
		      	                    GROUP BY task_time.project_id ");
					while ($th_c4->move_next()) {
						$in['description'][$j]=$th_c4->f('name').' : '.$time_description;
						$in['unitmeasure'][$j]='hours';
						$in['quantity'][$j]=$th_c4->f('total_task_hours');
						$in['price'][$j]=0;
						//$in['vat'][$j]=get_customer_vat($in['buyer_id']);
						$in['vat'][$j]=$vat;
						$in['origin_id'][$j]=$in['project_id'];
						$in['origin_type'][$j]='3';
						$in['task_time_ids'][$j]='';
						$th1_c4=$this->db->query("SELECT task_time.*
		      	                     FROM task_time
		      	                     WHERE ".$filter_task." AND task_time.project_id='".$th_c4->f('project_id')."' ");
						while ($th1_c4->move_next()) {
							$in['task_time_ids'][$j].=$th1_c4->f('task_time_id').',';
						}

						$j++;
					}
					break;
					case 7: //function hourly rate

		//			$j=0;
					//calculate time and prices
					$th_c7=$this->db->query("SELECT task_time.*,tasks.task_name,tasks.t_h_rate,projects.name,SUM(task_time.hours) as total_task_hours, projects.active AS ac
		      	                    FROM task_time
		      	                    INNER JOIN tasks ON tasks.task_id=task_time.task_id
		      	                    INNER JOIN projects ON projects.project_id=task_time.project_id
		      	                    WHERE ".$filter_task."
		      	                    GROUP BY task_time.task_id ");
					while ($th_c7->move_next()) {
						$name = $th_c7->f('name').': '.$th_c7->f('task_name').' '.$time_description;
						if($th_c7->f('ac') == 2){
							$name = $th_c7->f('task_name').' '.$time_description;
						}
						$in['description'][$j]=$name;
						$in['unitmeasure'][$j]='hours';
						$in['quantity'][$j]=$th_c7->f('total_task_hours');
						$in['price'][$j]=$th_c7->f('t_h_rate');
						//$in['vat'][$j]=get_customer_vat($in['buyer_id']);
						$in['vat'][$j]=$vat;
						$in['origin_id'][$j]=$in['project_id'];
						$in['origin_type'][$j]='3';
						$in['task_time_ids'][$j]='';
						$th1_c7=$this->db->query("SELECT task_time.*
		      	                     FROM task_time
		      	                     WHERE ".$filter_task." AND task_time.task_id='".$th_c7->f('task_id')."' ");
						while ($th1_c7->move_next()) {
							$in['task_time_ids'][$j].=$th1_c7->f('task_time_id').',';
						}

						$j++;
					}
					break;
				}

				$filter_expense = " ";

				if($invoice_start_date){
					$filter_expense = " AND date >= '".$invoice_start_date."' AND date < '".$invoice_end_date."' ";
				}

				$is_adhoc = $this->db->field("SELECT active FROM projects WHERE project_id='".$in['project_id']."' AND contract_id='0' ");

				if($is_adhoc == 2){
					$expense = $this->db->query("SELECT project_expenses.id, sum(project_expenses.amount) AS amount, expense.name AS e_name, projects.name AS p_name, expense.unit_price,project_user.user_name AS u_name,project_expenses.expense_id
										FROM project_expenses
										INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
										LEFT JOIN projects ON project_expenses.project_id=projects.project_id
										LEFT JOIN project_user ON projects.project_id=project_user.project_id
										WHERE project_expenses.project_id='".$in['project_id']."'
										AND project_expenses.billable='1' AND `project_expenses`.`billed`!='1' $filter_expense
										GROUP BY project_expenses.expense_id
									   	");
				}else{
					$expense = $this->db->query("SELECT project_expenses.id,SUM(project_expenses.amount) AS amount, project_expenses.expense_id,
										   expense.name AS e_name, expense.unit_price, projects.name AS p_name,
										   project_user.user_name AS u_name
										   FROM project_expenses
										   INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
										   LEFT JOIN projects ON project_expenses.project_id=projects.project_id
										   LEFT JOIN project_user ON project_expenses.user_id=project_user.user_id
										   WHERE project_expenses.project_id='".$in['project_id']."' AND project_user.project_id='".$in['project_id']."'
										   AND project_expenses.expense_id IN (SELECT expense_id FROM project_expences WHERE project_id='".$in['project_id']."') AND `project_expenses`.`billed`!='1'
										   $filter_expense GROUP BY project_expenses.expense_id ORDER BY project_expenses.user_id ");
				}

				while ($expense->next()){
					$in['description'][$j] = $expense->f('p_name').': '.$expense->f('u_name').' '."\n".$expense->f('e_name');
					$in['quantity'][$j] = $expense->f('amount');
					$in['price'][$j] = $expense->f('unit_price');
					$in['origin_id'][$j]=$in['project_id'];
					$in['origin_type'][$j]='3';
					if(!$expense->f('unit_price')){
						$in['quantity'][$j] = '1';
						$in['price'][$j] = $expense->f('amount');
					}
					$in['expense'][$j]='';
					if($is_adhoc == 2){
						$in['expense'][$i] = $expense->f('id').",";
					}else{
						$expense_list = $this->db->query("SELECT id FROM project_expenses
													WHERE expense_id IN (SELECT expense_id FROM project_expences WHERE project_id='".$in['project_id']."')
													AND project_id='".$in['project_id']."' AND expense_id='".$expense->f('expense_id')."' ");
						while ($expense_list->next()) {
							$in['expense'][$j] .= $expense_list->f('id').",";
						}
					}
					$j++;
				}
				$timesframes=array(
					'timeframe_start' 	=> $time_start,
					'timeframe_end' 		=> $time_end,
					'pr_id'			=> $in['project_id'],
				);
				$output['timeframes'][$in['project_id']]=$timesframes;
			}
			// $in['timeframe_start'] = $time_start;
			// $in['timeframe_end'] = $time_end;
		}elseif ($in['base_type']==3) { # based on an order

			$j=0;
			if($in['type']!=1){
				$in['total_order']=0;//nu sunt singur de ce
			}
			$output['total_order']=$in['total_order'];
			$in['order_inv'] = ";";
			$output['order_inv']=';';
			$output['orders_id']=$in['orders_id'];
			if(is_array($in['orders_id'])){
				foreach ( $in['orders_id'] as $in['order_id']){
					$in['currency_type'] = $this->db->field("SELECT currency_type FROM pim_orders WHERE order_id='".$in['order_id']."'");
					$in['apply_discount'] = $this->db->field("SELECT apply_discount FROM pim_orders WHERE order_id='".$in['order_id']."'");
				    	$in['discount'] = $this->db->field("SELECT discount FROM pim_orders WHERE order_id='".$in['order_id']."'");
					$in['discount_line_gen'] = $this->db->field("SELECT discount_line_gen FROM pim_orders WHERE order_id='".$in['order_id']."'");
					$in['your_ref'] = $this->db->field("SELECT your_ref FROM pim_orders WHERE order_id='".$in['order_id']."'");
					$in['customer_ref'] = $this->db->field("SELECT customer_reference FROM pim_orders WHERE order_id='".$in['order_id']."'");
					$articles = $this->db->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['order_id']."'");
					// AND content='0'
					//last delivery date if we have one
					$in['due_date_vat'] = $this->db->field("	SELECT date FROM pim_order_deliveries
											INNER JOIN pim_orders_delivery
											ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
											WHERE invoiced = '0'
											AND pim_order_deliveries.order_id = '".$in['order_id']."'
											ORDER BY pim_order_deliveries.delivery_id DESC LIMIT 1 ");
					if(!$in['due_date_vat']){
						$in['due_date_vat'] = $this->db->field("SELECT del_date FROM pim_orders WHERE order_id='".$in['order_id']."'");
					}
					$output['due_date_vat']              	= is_numeric($in['due_date_vat']) ? $in['due_date_vat']*1000 : strtotime($in['due_date_vat'])*1000;
					$output['invoice_due_date_vat']		= is_numeric($in['due_date_vat']) ? $in['due_date_vat']*1000 : strtotime($in['due_date_vat'])*1000;
					$output['show_due_date_vat_pdf']	      = SHOW_DUE_DATE_VAT_PDF == 1 ? true : false;

					if(defined('INVOICE_ADD_DELIVERY_ADDRESS') && INVOICE_ADD_DELIVERY_ADDRESS){
						$order = $this->db->query("SELECT address_info, delivery_address FROM pim_orders WHERE order_id='".$in['order_id']."' ");
						if($order->f('delivery_address')){
							$delivery_adr = $order->f('delivery_address');
						}else{
							$delivery_adr = $order->f('address_info');
						}
						$in['disc_line'][$j] = 0;
					    $in['discount_line'][$j] = 0;
					    $in['description'][$j] = geto_label_txt('billing_address',$in['languages']).": \n".$delivery_adr;
						$in['quantity'][$j] = 0;
						$in['price'][$j] = 0;
						$in['purchase_price'][$j] = 0;
						$in['price_with_vat'][$j] = 0;
						$in['vat_percent'][$j] = 0;
						$in['vat_value'][$j] = 0;
						$in['order'][$j] = $in['orders_id'];
						$in['content'][$j] =  true;
						$in['readonly'][$j] = 1;
						$in['order'][$j] = $in['order_id'];
						$in['origin_id'][$j]=$in['order_id'];
						$in['origin_type'][$j]='4';
						$j++;

					}

					while ($articles->next()){
						if(!$in['total_order']) { //for deliveries
				    			$q=0;
							$deliveries = $this->db->query("SELECT * FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$articles->f('order_articles_id')."' AND invoiced='0' ");
							while ($deliveries->next()) {
								$q += $deliveries->f('quantity');
								$in['deliveries'][$j] .= $deliveries->f('order_delivery_id').',';
						 	}
						}else{              //for total order quantity
		                   		$q=$this->db->field("SELECT `pim_order_articles`.`quantity` FROM  pim_order_articles WHERE order_id='".$in['order_id']."' AND order_articles_id='".$articles->f('order_articles_id')."'");
						}
						if(!$q){ continue; }
		                  	$price = $articles->f('price');

						$disc += $q*($articles->f('packing')/$articles->f('sale_unit'))*($articles->f('price')*$articles->f('discount')/100);
		                		$in['disc_line'][$j] = ($articles->f('price')*$articles->f('discount')/100);
					    	$in['discount_line'][$j] = $articles->f('discount');
					    	$in['description'][$j] = ((NOT_COPY_ARTICLE_INV==1 && $articles->f('article_id')) ? get_article_label($articles->f('article_id'),$in['languages'],'invoice'):
					    		stripslashes(htmlspecialchars_decode($articles->f('article'),ENT_QUOTES)));
						$in['quantity'][$j] = $q*($articles->f('packing')/$articles->f('sale_unit'));
						$in['price'][$j] = $price;
						$in['purchase_price'][$j] = $articles->f('purchase_price');
						$in['price_with_vat'][$j] = $price + ($price*$articles->f('vat_percent')/100);
						$in['vat_percent'][$j] = $articles->f('vat_percent');
						$in['vat_value'][$j] = $articles->f('vat_value');
						$in['order'][$j] = $in['orders_id'];
						$in['content'][$j] =  $articles->f('content') ? true : false;
						$in['readonly'][$j] = 1;
						$in['order'][$j] = $in['order_id'];
						$in['origin_id'][$j]=$in['order_id'];
						$in['origin_type'][$j]='4';
						$in['has_variants'][$j]= $articles->f('has_variants');
			 			$in['has_variants_done'][$j]= $articles->f('has_variants_done');
			 			$in['is_variant_for'][$j]= $articles->f('is_variant_for');
			 			$in['is_variant_for_line'][$j]= $articles->f('is_variant_for_line');
			 			$in['variant_type'][$j]= $articles->f('variant_type'); 
			 			$in['is_combined'][$j]= $articles->f('is_combined');
			 			$in['component_for'][$j]= $articles->f('component_for')? $articles->f('component_for'):'';
			 			$in['visible'][$j]= $articles->f('visible'); 
						//begin article serial numbers
						if(SHOW_ART_S_N_INVOICE_PDF==1) {
							$order_id =  $in['order_id'];
							$art_serial_numbers = '';
							$count_art_s_n = $this->db->field("SELECT COUNT(serial_numbers.serial_number) AS art_s_n
													FROM serial_numbers
													INNER JOIN pim_orders_delivery ON serial_numbers.delivery_id = pim_orders_delivery.delivery_id
													WHERE pim_orders_delivery.order_id =  '".$order_id."' AND serial_numbers.article_id = '".$articles->f('article_id')."'
													GROUP BY serial_numbers.id");
							if($count_art_s_n > 0){
								$art_s_n_data = $this->db->query("	SELECT serial_numbers.serial_number AS art_s_n
													FROM serial_numbers
													INNER JOIN pim_orders_delivery ON serial_numbers.delivery_id = pim_orders_delivery.delivery_id
													WHERE pim_orders_delivery.order_id =  '".$order_id."' AND serial_numbers.article_id = '".$articles->f('article_id')."'
													GROUP BY serial_numbers.id");
								while($art_s_n_data->next()){
									$art_serial_numbers .= "\n".$art_s_n_data->f('art_s_n');
								}
								$art_serial_numbers = "\n".gm('Serial Numbers').':'.$art_serial_numbers;
							}else{
								$art_serial_numbers = "";
							}
							$in['description'][$j] .= $art_serial_numbers;
						}
						//end serial numbers

						//begin article batch numbers
						if(SHOW_ART_B_N_INVOICE_PDF==1) {
							$order_id =  $in['order_id'];
							$art_batch_numbers = '';
							$count_art_b_n = $this->db->field("SELECT COUNT(DISTINCT batches_from_orders.batch_id) AS art_b_n_id
													FROM batches_from_orders
													INNER JOIN pim_orders_delivery ON batches_from_orders.delivery_id = pim_orders_delivery.delivery_id
													WHERE pim_orders_delivery.order_id =  '".$order_id."'
													AND batches_from_orders.article_id = '".$articles->f('article_id')."'
													AND batches_from_orders.order_articles_id = '".$articles->f('order_articles_id')."'
													GROUP BY batches_from_orders.batch_id");
							if($count_art_b_n > 0){
								$b_n_data = $this->db->query("SELECT DISTINCT batches_from_orders.batch_id AS art_b_n_id
													FROM batches_from_orders
													INNER JOIN pim_orders_delivery ON batches_from_orders.delivery_id = pim_orders_delivery.delivery_id
													WHERE pim_orders_delivery.order_id =  '".$order_id."'
													AND batches_from_orders.article_id = '".$articles->f('article_id')."'
													AND batches_from_orders.order_articles_id = '".$articles->f('order_articles_id')."'
													GROUP BY batches_from_orders.batch_id");
								while($b_n_data->next()){
									$batch_number = $this->db->field("SELECT batch_number FROM batches WHERE id = '".$b_n_data->f('art_b_n_id')."' ");
									$art_batch_numbers .= "\n".$batch_number;
								}
								$art_batch_numbers = "\n".gm('Batches').':'.$art_batch_numbers;
							}else{
								$art_batch_numbers = "";
							}
							$in['description'][$j] .= $art_batch_numbers;
						}

						//end article batch numbers
						$j++;
					}

					$downpayment_int=$this->db->query("SELECT tblinvoice_line.* FROM tbldownpayments 	
					INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
					INNER JOIN tblinvoice_line ON tblinvoice.id=tblinvoice_line.invoice_id
					WHERE tbldownpayments.order_id='".$in['order_id']."' AND tblinvoice.f_archived='0' ");

					while($downpayment_int->next()){
						$in['order_inv'] .= $in['order_id'].";";
						$in['disc_line'][$j] += 0;
						$in['discount_line'][$j] = 0;
						$in['description'][$j] = $downpayment_int->f('name');
						$in['quantity'][$j] = $downpayment_int->f('quantity');
						$in['price'][$j] = -$downpayment_int->f('price');
						$in['price_with_vat'][$j] = -$downpayment_int->f('price') -($downpayment_int->f('price')*$downpayment_int->f('vat')/100);
						$in['vat_percent'][$j] = display_number($downpayment_int->f('vat'));
						$in['vat_value'][$j] = $downpayment_int->f('vat');

						$in['order'][$j] = $in['order_id'];
						$in['readonly'][$j] = 2;
						$in['origin_id'][$j]=$in['order_id'];
						$in['origin_type'][$j]='4';
						$j++;
					}

					$in['order_inv'] .= $in['order_id'].";";
					$output['order_inv'] = $in['order_id'];
				}
			}else{
				//last delivery date if we have one
				$in['due_date_vat'] = $this->db->field("	SELECT date FROM pim_order_deliveries
										INNER JOIN pim_orders_delivery
										ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
										WHERE invoiced = '0'
										AND pim_order_deliveries.order_id = '".$in['orders_id']."'
										ORDER BY pim_order_deliveries.date DESC LIMIT 1 ");
				if(!$in['due_date_vat']){
					$in['due_date_vat'] = $this->db->field("SELECT del_date FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				}
				if($in['due_date_vat']){
					$output['show_due_date_vat_pdf']	      = SHOW_DUE_DATE_VAT_PDF == 1 ? true : false;
					$output['invoice_due_date_vat']		= is_numeric($in['due_date_vat']) ? $in['due_date_vat']*1000 : strtotime($in['due_date_vat'])*1000;
					$output['due_date_vat']				= is_numeric($in['due_date_vat']) ? $in['due_date_vat']*1000 : strtotime($in['due_date_vat'])*1000;
				}
				$in['currency_type'] = $this->db->field("SELECT currency_type FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$in['our_ref'] = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$in['apply_discount'] = $in['third_party_applied'] ? $in['apply_discount'] : $this->db->field("SELECT apply_discount FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$in['discount'] = $this->db->field("SELECT discount FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$in['discount_line_gen'] = $this->db->field("SELECT discount_line_gen FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$in['your_ref'] = $this->db->field("SELECT your_ref FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$in['customer_ref'] = $this->db->field("SELECT customer_reference FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$output['contact_id'] = $this->db->field("SELECT contact_id FROM pim_orders WHERE order_id='".$in['orders_id']."'");
				$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$output['contact_id']}' ");
				$output['subject']=stripslashes($this->db->field("SELECT subject FROM pim_orders WHERE order_id='".$in['orders_id']."'"));

				if(defined('INVOICE_ADD_DELIVERY_ADDRESS') && INVOICE_ADD_DELIVERY_ADDRESS){
						$order = $this->db->query("SELECT address_info, delivery_address FROM pim_orders WHERE order_id='".$in['orders_id']."' ");
						if($order->f('delivery_address')){
							$delivery_adr = $order->f('delivery_address');
						}else{
							$delivery_adr = $order->f('address_info');
						}
						$in['disc_line'][$j] = 0;
					    $in['discount_line'][$j] = 0;
					    $in['description'][$j] = geto_label_txt('billing_address',$in['languages']).": \n".$delivery_adr;
						$in['quantity'][$j] = 0;
						$in['price'][$j] = 0;
						$in['purchase_price'][$j] = 0;
						$in['price_with_vat'][$j] = 0;
						$in['vat_percent'][$j] = $in['third_party_applied'] && $vat ? $vat : 0;
						$in['vat_value'][$j] = 0;
						$in['order'][$j] = $in['orders_id'];
						$in['content'][$j] =  true;
						$in['readonly'][$j] = 1;
						$in['order'][$j] = $in['orders_id'];
						$in['origin_id'][$j]=$in['orders_id'];
						$in['origin_type'][$j]='4';
						$in['visible'][$j]= 1; 
						$in['title'][$j]=0;
						$j++;

					}

				$articles = $this->db->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['orders_id']."'  ORDER BY order_articles_id ASC ");
				// AND content='0'
				$disc=0;

				while ($articles->next())
				{

					if(!$in['total_order']) //for deliveries
					{
						$q=0;
						$deliveries = $this->db->query("SELECT * FROM pim_orders_delivery
							WHERE order_id='".$in['orders_id']."'
							AND order_articles_id='".$articles->f('order_articles_id')."'
							AND invoiced='0' ");
						if(ORDER_DELIVERY_STEPS==2){
							$deliveries = $this->db->query("SELECT * FROM pim_orders_delivery
							INNER JOIN pim_order_deliveries ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
							WHERE pim_orders_delivery.order_id='".$in['orders_id']."'
							AND order_articles_id='".$articles->f('order_articles_id')."'
							AND invoiced='0'
							AND delivery_done = '1' ");
						}
						while ($deliveries->next()) {
							$q += $deliveries->f('quantity');
							$in['deliveries'][$j] .= $deliveries->f('order_delivery_id').',';


					 	}
					}else{              //for total order quantity
	          				$q=$this->db->field("SELECT  pim_order_articles.quantity FROM  pim_order_articles WHERE order_id='".$in['orders_id']."' AND order_articles_id='".$articles->f('order_articles_id')."'");
					}

	                 		//we have to check if there are some return
	                 		$return=$this->db->field("SELECT  sum(pim_articles_return.quantity)
	                 	                 FROM  pim_articles_return
	                 	                 WHERE pim_articles_return.order_id='".$in['orders_id']."'
	                 	                 AND pim_articles_return.article_id='".$articles->f('article_id')."' AND invoiced=0");

	                		$q=$q-$return;
	                    	if($articles->f('tax_for_article_id')){
	                    		$in['tax_id'][$j] =$articles->f('tax_id');
	                    	 	$in['is_tax'][$j] = 1;
					     		 $in['tax_for_article_id'][$j] = $articles->f('tax_for_article_id');
			                    		//first we select qunatity of the article
			                    		$q_a=$this->db->field("SELECT sum(pim_orders_delivery.quantity)
			                    		 	FROM pim_orders_delivery
									INNER JOIN pim_order_articles on pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
									WHERE pim_order_articles.order_id='".$in['orders_id']."'
									AND pim_order_articles.article_id='".$articles->f('tax_for_article_id')."' and pim_orders_delivery.invoiced=0
									");

			                         //the we select qunatity of the taxex
			                    		$q_t=$this->db->field("SELECT  pim_order_articles.quantity FROM  pim_order_articles WHERE order_id='".$in['orders_id']."' AND order_articles_id='".$articles->f('order_articles_id')."'");

				                       if($q_a>$q_t){
				                       	  $q=$q_t;
				                       }else{
				                       	  $q=$q_a;
				                       }

			                       		//now we have tosee if some taxes was already invoiced
			                       		$q_i=$this->db->field("SELECT   sum(tblinvoice_line.quantity)
			                                        FROM   tblinvoice_line
			                                        INNER JOIN tblinvoice ON tblinvoice.id=invoice_id
			                       	                WHERE REPLACE(tblinvoice.order_id, ';','')='".$in['orders_id']."' AND tblinvoice_line.tax_for_article_id='".$articles->f('tax_for_article_id')."'");

				                         if($q_i+$q>$q_t){
				                             $q=$q_t-$q_i;
				                         }
			                         	//we have to check if there are some return
			                 			$return=$this->db->field("SELECT  sum(pim_articles_return.quantity)
			                 	                 FROM  pim_articles_return
			                 	                 WHERE pim_articles_return.order_id='".$in['orders_id']."'
			                 	                 AND pim_articles_return.article_id='".$articles->f('tax_for_article_id')."' AND invoiced=0");

			                        	$q=$q-$return;

			                        	if($in['total_order']){
			                        		$q=$this->db->field("SELECT  pim_order_articles.quantity FROM  pim_order_articles WHERE order_id='".$in['orders_id']."' AND order_articles_id='".$articles->f('order_articles_id')."'");
			                        	}
	                    	}

					if($q==0 && $articles->f('content')!=1 && $articles->f('article_id')){ continue;}
	                   	if($q==0 && $articles->f('tax_for_article_id')){ continue;}

					// $price = $articles->f('price') - ($articles->f('price')*$articles->f('discount')/100);

					$price = $articles->f('price');
					$disc += $q*($articles->f('packing')/$articles->f('sale_unit'))*($articles->f('price')*$articles->f('discount')/100);
					$in['disc_line'][$j] = ($articles->f('price')*$articles->f('discount')/100);
					$in['discount_line'][$j] = $articles->f('discount');

					$in['description'][$j] = ((NOT_COPY_ARTICLE_INV==1 && $articles->f('article_id'))  ? get_article_label($articles->f('article_id'),$in['languages'],'invoice'): ($articles->f('content')==1 ? html_entity_decode(stripslashes($articles->f('article'))) : stripslashes(htmlspecialchars_decode($articles->f('article'),ENT_QUOTES))));
                    $in['quantity'][$j] = $q*($articles->f('packing')/$articles->f('sale_unit'));
					$in['price'][$j] = $price;
					$in['purchase_price'][$j] = $articles->f('purchase_price');
					$in['price_with_vat'][$j] = $price + ($price*( $in['third_party_applied'] && $vat ? $vat : $articles->f('vat_percent'))/100);
					$in['vat_percent'][$j] = $in['third_party_applied'] && $vat ? $vat : $articles->f('vat_percent');
					$in['vat_value'][$j] = $articles->f('vat_value');
					$in['order'][$j] = $in['orders_id'];
					$in['content'][$j] =  $articles->f('content') ? true : false;
					$in['readonly'][$j] = 1;
					$in['article_id'][$j] = $articles->f('article_id');
					$in['article_code'][$j] = stripslashes($articles->f('article_code'));
					$in['origin_id'][$j]=$in['orders_id'];
					$in['origin_type'][$j]='4';
					$in['has_variants'][$j]= $articles->f('has_variants');
		 			$in['has_variants_done'][$j]= $articles->f('has_variants_done');
		 			$in['is_variant_for'][$j]= $articles->f('is_variant_for');
		 			$in['is_variant_for_line'][$j]= $articles->f('is_variant_for_line');
		 			$in['variant_type'][$j]= $articles->f('variant_type'); 
		 			$in['is_combined'][$j]= $articles->f('is_combined');
		 			$in['component_for'][$j]= $articles->f('component_for')? $articles->f('component_for') : '';
		 			$in['visible'][$j]= $articles->f('visible'); 
		 			$in['title'][$j]=$articles->f('content')==1 ? 0 : '';

					//begin article serial numbers
					if(SHOW_ART_S_N_INVOICE_PDF==1) {
						$order_id =  $in['orders_id'];
						$art_serial_numbers = '';
						$count_art_s_n = $this->db->field("SELECT COUNT(serial_numbers.serial_number) AS art_s_n
												FROM serial_numbers
												INNER JOIN pim_orders_delivery ON serial_numbers.delivery_id = pim_orders_delivery.delivery_id
												WHERE pim_orders_delivery.order_id =  '".$order_id."' AND serial_numbers.article_id = '".$articles->f('article_id')."'
												GROUP BY serial_numbers.id");
						if($count_art_s_n > 0){
							$art_s_n_data = $this->db->query("	SELECT serial_numbers.serial_number AS art_s_n
													FROM serial_numbers
													INNER JOIN pim_orders_delivery ON serial_numbers.delivery_id = pim_orders_delivery.delivery_id
													WHERE pim_orders_delivery.order_id =  '".$order_id."' AND serial_numbers.article_id = '".$articles->f('article_id')."'
													GROUP BY serial_numbers.id");
							while($art_s_n_data->next()){
								$art_serial_numbers .= "\n".$art_s_n_data->f('art_s_n');
							}
							$art_serial_numbers = "\n".gm('Serial Numbers').':'.$art_serial_numbers;
						}else{
							$art_serial_numbers = "";
						}
						$in['description'][$j] .= $art_serial_numbers;
					}
					//end serial numbers

					//begin article batch numbers
					if(SHOW_ART_B_N_INVOICE_PDF==1) {
						$order_id =  $in['orders_id'];
						$art_batch_numbers = '';
						$count_art_b_n = $this->db->field("SELECT COUNT(DISTINCT batches_from_orders.batch_id) AS art_b_n_id
												FROM batches_from_orders
												INNER JOIN pim_orders_delivery ON batches_from_orders.delivery_id = pim_orders_delivery.delivery_id
												WHERE pim_orders_delivery.order_id =  '".$order_id."'
												AND batches_from_orders.article_id = '".$articles->f('article_id')."'
												AND batches_from_orders.order_articles_id = '".$articles->f('order_articles_id')."'
												GROUP BY batches_from_orders.batch_id");
						if($count_art_b_n > 0){
							$b_n_data = $this->db->query("SELECT DISTINCT batches_from_orders.batch_id AS art_b_n_id
												FROM batches_from_orders
												INNER JOIN pim_orders_delivery ON batches_from_orders.delivery_id = pim_orders_delivery.delivery_id
												WHERE pim_orders_delivery.order_id =  '".$order_id."'
												AND batches_from_orders.article_id = '".$articles->f('article_id')."'
												AND batches_from_orders.order_articles_id = '".$articles->f('order_articles_id')."'
												GROUP BY batches_from_orders.batch_id");
							while($b_n_data->next()){
								$batch_number = $this->db->field("SELECT batch_number FROM batches WHERE id = '".$b_n_data->f('art_b_n_id')."' ");
								$art_batch_numbers .= "\n".$batch_number;
							}
							$art_batch_numbers = "\n".gm('Batches').':'.$art_batch_numbers;
						}else{
							$art_batch_numbers = "";
						}
						$in['description'][$j] .= $art_batch_numbers;
					}
					//end article batch numbers
					$j++;
				}

				// begin shipping price
				$shipping_price = $this->db->field("SELECT shipping_price FROM pim_orders WHERE order_id = '".$in['orders_id']."' ");
				if($shipping_price > 0){
					$in['order_inv'] .= $in['orders_id'].";";
					$in['disc_line'][$j] += 0;
					$in['discount_line'][$j] = 0;
					$in['description'][$j] = gm('Delivery Cost');
					$in['quantity'][$j] = 1;
					$in['price'][$j] = $shipping_price;
					$in['price_with_vat'][$j] = $shipping_price + ($shipping_price*$shipping_vat/100);
					$in['vat_percent'][$j] = display_number(0);
					$in['vat_value'][$j] = display_number(0);
					$in['visible'][$j] = 1;

					$in['order'][$j] = $in['orders_id'];
					$in['readonly'][$j] = 2;
					$in['origin_id'][$j]=$in['orders_id'];
					$in['origin_type'][$j]='4';
					$j++;
				}
				//end shipping price


				$downpayment_int=$this->db->query("SELECT tblinvoice_line.* FROM tbldownpayments 	
					INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
					INNER JOIN tblinvoice_line ON tblinvoice.id=tblinvoice_line.invoice_id
					WHERE tbldownpayments.order_id='".$in['orders_id']."' AND tblinvoice.f_archived='0' ");
				while($downpayment_int->next()){
					$in['order_inv'] .= $in['orders_id'].";";
					$in['disc_line'][$j] += 0;
					$in['discount_line'][$j] = 0;
					$in['description'][$j] = $downpayment_int->f('name');
					$in['quantity'][$j] = $downpayment_int->f('quantity');
					$in['price'][$j] = -$downpayment_int->f('price');
					$in['price_with_vat'][$j] = -$downpayment_int->f('price') -($downpayment_int->f('price')*$downpayment_int->f('vat')/100);
					$in['vat_percent'][$j] = display_number($downpayment_int->f('vat'));
					$in['vat_value'][$j] = $downpayment_int->f('vat');
					$in['visible'][$j] = 1;

					$in['order'][$j] = $in['orders_id'];
					$in['readonly'][$j] = 2;
					$in['origin_id'][$j]=$in['orders_id'];
					$in['origin_type'][$j]='4';
					$j++;
				}
				if($in['from_orders']==1) {
					$in['order_inv'] .= $in['orders_id'].";";
				}
				$output['order_inv']=$in['order_inv'];
				# if from orders

			}

			
			/*$baset_type_delivery_exist= $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 AND is_primary!=1 ");
		 	if($in['base_type']==3){
		 		$free_field = $baset_type_delivery->f('address').'
				'.$baset_type_delivery->f('zip').' '.$baset_type_delivery->f('city').'
				'.get_country_name($baset_type_delivery->f('country_id'));
		 	}else{
		 		$free_field = $this->db->field("SELECT address_info FROM pim_orders WHERE order_id = '".$in['orders_id']."' ");
		 	}*/
			$customer_ref= $this->db->field("SELECT customer_reference FROM pim_orders WHERE order_id='".$in['orders_id']."'");
			$output['discount_line_gen']		= display_number($in['discount_line_gen']);
			$output['your_ref']			= $customer_ref ? $in['customer_ref'] : $in['your_ref'];
			//$output['free_field']			= $free_field;
			//$output['free_field_txt']		= nl2br($free_field);

		}elseif ($in['base_type'] == 4 ){ # based on a task
			$j = 0;
			if(!$in['act']){
				//$in['tasks_id'] = explode(',',$in['tasks_id'][0]);
				$tasks_id=array();
				foreach($in['tasks_id'] as $key => $value){
					if($value['checked']){
						array_push($tasks_id, $value['id']);
					}			
				}
			}
			$project_ref = array();
			foreach ($tasks_id as $in['task']){
				$tasks = $this->db->query("SELECT tasks.*, projects.name, projects.serial_number FROM tasks
														 INNER JOIN projects ON tasks.project_id = projects.project_id
														 WHERE task_id='".$in['task']."' ");
				if($tasks->next()){
					$project_ref[$tasks->f('name')] = $tasks->f('serial_number');
					// $our_ref .= $tasks->f('serial_number').',';
					$in['description'][$j] = $tasks->f('name').": ".$tasks->f('task_name');
					$in['quantity'][$j] = 1;
					$in['price'][$j] = $tasks->f('task_budget');
					$in['price_with_vat'][$j] = $tasks->f('task_budget')+($tasks->f('task_budget')*$vat/100);
					$in['vat_percent'][$j] = $vat;
					$in['task_id'][$j] = $in['task'];
					$in['origin_id'][$j]=$tasks->f('project_id');
					$in['origin_type'][$j]='3';
					$in['visible'][$j] = 1;
					$j++;
				}
			}
			foreach ($project_ref as $key => $value) {
				$our_ref .= $value.',';
			}
		}elseif ($in['base_type'] == 5 ){ # based on a purchase

			$j = 0;
			$project_purchases_id=array();
			foreach($in['project_purchases_id'] as $key => $value){
				if($value['checked']){
					array_push($project_purchases_id, $value['id']);
				}			
			}
			foreach ($project_purchases_id as $in['project_purchase_ids']){
				$purchase = $this->db->query("SELECT * FROM project_purchase WHERE project_purchase_id='".$in['project_purchase_ids']."' ");
				if($purchase->next()){
					$margin_price = ($purchase->f('unit_price')*$purchase->f('margin')/100) + $purchase->f('unit_price');
					$in['description'][$j] = $purchase->f('description');
					$in['quantity'][$j] = 1;
					$in['price'][$j] = $margin_price;
					$in['price_with_vat'][$j] = $margin_price + ($margin_price*$vat/100);
					$in['vat_percent'][$j] = $vat;
					$in['project_purchase_id'][$j] = $in['project_purchase_ids'];
					$in['origin_id'][$j]=$purchase->f('project_id');
					$in['origin_type'][$j]='3';
					$in['visible'][$j] = 1;
					$j++;
				}
			}
		}elseif ($in['base_type'] == 6) {
			$in['apply_discount']=0;
			$j = 0;
			$in['item']=$in['service_id'];

			$output['is_from_service'] 					= 	true;
			$output['attach_intervention'] 				= 	$in['attach_intervention'] ? 'checked' : '';
			$output['attach_intervention_inv_txt']			= 	$in['attach_intervention'] ? gm('Yes') : gm('No');
			$output['service_id']						= 	$in['service_id'];

			$query = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['item']."' AND billed=0 ");
			if($query->next()){
				$our_ref = $query->f('serial_number');
				$in['email_language'] = $query->f('email_language');
				$in['languages'] = $query->f('email_language');
		
				$buyer_id = $query->f('customer_id');
				$contact_id = $query->f('contact_id');
				$planned_date = $query->f('planeddate');
				//$vat = get_customer_vat($buyer_id);

				if($query->f('billable')){
					if(!$query->f('service_type')){
						$has_hour = false;
						$hoursPerUser = $this->db->field("SELECT SUM(end_time-start_time-break) FROM servicing_support_sheet WHERE service_id='".$in['item']."' ");
						if($hoursPerUser) {
							//$range = $this->db->query("SELECT MAX( `date` ) as dend , MIN( `date` ) as dstart FROM  `servicing_support_sheet` WHERE service_id='".$in['item']."' ");
							//$time_description ='('.date(ACCOUNT_DATE_FORMAT,$range->f('dstart')).' - '.date(ACCOUNT_DATE_FORMAT,$range->f('dend')).')';
							$time_description ='('.date(ACCOUNT_DATE_FORMAT,$planned_date).')';
							$in['tr_id'][$j] = 'tmp'.$j;
							$in['description'][$j] = $query->f('serial_number') ." - ".$query->f('subject')." : ".$time_description;
							$in['quantity'][$j] = ($hoursPerUser);
							$in['price'][$j] = ($query->f('rate'));
							$in['vat_val'][$j] = ($vat);
							//$in['discount_line'][$j] = $in['discount_line_gen'] ? $in['discount_line_gen'] : (0);
							$in['discount_line'][$j]=0;
							$in['origin_id'][$j]=$query->f('service_id');
							$in['origin_type'][$j]='5';
							$j++;
						}
					}else{
						$int_tasks=$this->db->field("SELECT SUM(task_budget) FROM servicing_support_tasks WHERE service_id='".$in['item']."' AND article_id='0' ");
						if($int_tasks>0){
							$in['tr_id'][$j] = 'tmp'.$j;
							$in['description'][$j] = $query->f('serial_number') ." - ".$query->f('subject')." : ".gm('Fixed price');
							$in['quantity'][$j] = 1;
							$in['price'][$j] = ($int_tasks);
							$in['vat_val'][$j] = ($vat);
							//$in['discount_line'][$j] = $in['discount_line_gen'] ? $in['discount_line_gen'] : (0);
							$in['discount_line'][$j]=0;
							$in['origin_id'][$j]=$query->f('service_id');
							$in['origin_type'][$j]='5';
							$j++;
						}		
					}	

					$def_lang = DEFAULT_LANG_ID;
					$customerLanguage = $this->db->field("SELECT language FROM customers WHERE customer_id ='".$in['buyer_id']."'");

					if($customerLanguage){
						$def_lang= $customerLanguage;
					}
					if($customerLanguage>=1000) {
						$def_lang = DEFAULT_LANG_ID;
					}

					$langFilter =" AND pim_articles_lang.lang_id='".$def_lang."' ";

					$int_services=$this->db->query("SELECT servicing_support_tasks.*,pim_articles.item_code,
												pim_articles_lang.description AS description,
												pim_articles_lang.name2 AS item_name2,
												pim_articles_lang.name AS item_name
				 		FROM servicing_support_tasks 
						LEFT JOIN pim_articles ON servicing_support_tasks.article_id=pim_articles.article_id
						INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
						WHERE servicing_support_tasks.service_id='".$in['item']."' AND servicing_support_tasks.article_id!='0' AND servicing_support_tasks.closed='1' $langFilter");

					//ServicesADDLINE
					while($int_services->next()){
						//Add Service Commercial name 
						$itemDescription = $query->f('serial_number') ." - ".$int_services->f('task_name');
				
						$serviceCommercialName = $int_services->f('item_name');

						if(!empty($serviceCommercialName)){
							$itemDescription = $query->f('serial_number') ." - ".$int_services->f('item_name');
						}

						$in['tr_id'][$j] = 'tmp'.$j;
						$in['description'][$j] = $itemDescription;
						$in['quantity'][$j] = $int_services->f('quantity');
						$in['price'][$j] = $int_services->f('task_budget');
						$in['vat_val'][$j] = ($vat);
						$in['article_code'][$j]=stripslashes($int_services->f('item_code'));
							//$in['discount_line'][$j] = $in['discount_line_gen'] ? $in['discount_line_gen'] : (0);
						$in['discount_line'][$j]=0;
						$in['origin_id'][$j]=$query->f('service_id');
						$in['origin_type'][$j]='5';
						$j++;
					}	
				}
								
				$purchase = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['item']."' AND delivered<>0 AND billable='1' ");

				while ($purchase->next()) {
					$in['tr_id'][$j] = 'tmp'.$j;
					$in['description'][$j] = $query->f('serial_number') ." - ". $purchase->f('name');
					$in['quantity'][$j] = ($purchase->f('delivered'));
					$in['price'][$j] = ($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'));
					$in['vat_val'][$j] = ($vat);
					//$in['discount_line'][$j] = $in['discount_line_gen'] ? $in['discount_line_gen'] : (0);
					$in['discount_line'][$j]=0;
					$in['origin_id'][$j]=$query->f('service_id');
					$in['origin_type'][$j]='5';
					$j++;
				}

				$def_lang = DEFAULT_LANG_ID;
				$customerLanguage = $this->db->field("SELECT language FROM customers WHERE customer_id ='".$in['buyer_id']."'");

				if($customerLanguage){
					$def_lang= $customerLanguage;
				}

				if($customerLanguage>=1000) {
					$def_lang = DEFAULT_LANG_ID;
				}

				$langFilter =" AND pim_articles_lang.lang_id='".$def_lang."' ";
				
				$article = $this->db->query("SELECT SUM(service_delivery.quantity) AS total_quantity,GROUP_CONCAT(service_delivery.id) as delivery_ids,servicing_support_articles.name, servicing_support_articles.price, pim_articles.packing, pim_articles.sale_unit,pim_articles.item_code,pim_articles_lang.description AS description,pim_articles_lang.name2 AS item_name2,pim_articles_lang.name AS item_name,servicing_support_articles.is_combined,servicing_support_articles.component_for FROM service_delivery
					LEFT JOIN pim_articles ON service_delivery.a_id=pim_articles.article_id
					INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
					INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
					WHERE service_delivery.service_id='".$in['item']."' AND service_delivery.invoiced='0' AND servicing_support_articles.billable='1' AND servicing_support_articles.article_id!='0' $langFilter GROUP BY service_delivery.a_id ");

				//ArticleADDLINE
				while ($article->next()) {
					//Add Article Commercial name 
					$itemDescription = $query->f('serial_number') ." - ". $article->f('name');
			
					$articleCommercialName = $article->f('item_name');

					if(!empty($articleCommercialName)){
						$itemDescription = $query->f('serial_number') ." - ". $article->f('item_name');
					}

					$packing_art=($this->ALLOW_ARTICLE_PACKING && $article->f('packing')>0) ? $article->f('packing') : 1;
					$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $article->f('sale_unit')>0) ? $article->f('sale_unit') : 1;
					$in['tr_id'][$j] = 'tmp'.$j;
					$in['description'][$j] = $itemDescription;
					$in['quantity'][$j] = ($article->f('total_quantity')*($packing_art/$sale_unit_art));
					$in['price'][$j] = ($article->f('price'));
					$in['vat_val'][$j] = ($vat);
					//$in['discount_line'][$j] = $in['discount_line_gen'] ? $in['discount_line_gen'] : (0);
					$in['article_code'][$j]=stripslashes($article->f('item_code'));
					$in['discount_line'][$j]=0;
					$in['service_delivery_ids'][$j]=$article->f('delivery_ids');
					$in['origin_id'][$j]=$query->f('service_id');
					$in['origin_type'][$j]='5';
					$in['is_combined'][$j]= $article->f('is_combined');
			 		$in['component_for'][$j]= $article->f('component_for')? $article->f('component_for'):'';
					$j++;
				}
				$expense = $this->db->query("SELECT project_expenses.*, expense.name AS e_name, expense.unit_price, expense.unit, servicing_support.customer_id as customer_id
								FROM project_expenses
								INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
								INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
								WHERE servicing_support.service_id='".$query->f('service_id')."' AND project_expenses.billable='1' ORDER BY id");
				while ($expense->next()) {
					$amount = $expense->f('amount');
					if($expense->f('unit_price')){
						$amount = $expense->f('amount') * $expense->f('unit_price');
					}
					$in['tr_id'][$j] = 'tmp'.$j;
					$in['description'][$j] = $query->f('serial_number') . ' '. get_user_name($expense->f('user_id')) ." : ". $expense->f('e_name') . " - " . date(ACCOUNT_DATE_FORMAT,$expense->f('date'));
					$in['quantity'][$j] = ($expense->f('amount'));
					$in['price'][$j] = ($expense->f('unit_price'));
					if(!$expense->f('unit_price')){
						$in['quantity'][$j] = (1);
						$in['price'][$j] = ($expense->f('amount'));
					}
					$in['vat_val'][$j] = ($vat);
					//$in['discount_line'][$j] = $in['discount_line_gen'] ? $in['discount_line_gen'] : (0);
					$in['discount_line'][$j]=0;
					$in['origin_id'][$j]=$query->f('service_id');
					$in['origin_type'][$j]='5';
					$j++;
				}
				$downpayment_int=$this->db->query("SELECT tblinvoice_line.* FROM tbldownpayments 	INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
					INNER JOIN tblinvoice_line ON tblinvoice.id=tblinvoice_line.invoice_id
					WHERE tbldownpayments.service_id='".$in['service_id']."' AND tblinvoice.f_archived='0' ");
				while($downpayment_int->next()){
					$in['tr_id'][$j] = 'tmp'.$j;
					$in['description'][$j] = $downpayment_int->f('name');
					$in['quantity'][$j] = $downpayment_int->f('quantity');
					$in['price'][$j] = -$downpayment_int->f('price');
					$in['vat_val'][$j] = $downpayment_int->f('vat');
					$in['discount_line'][$j]=0;
					$in['origin_id'][$j]=$query->f('service_id');
					$in['origin_type'][$j]='5';
					$j++;
				}
			}
		}

		if(!$in['act']){
			$project_purchases_id=array();
			foreach($in['project_purchases_id'] as $key => $value){
				if($value['checked']){
					array_push($project_purchases_id, $value['id']);
				}			
			}		
			if(!empty($project_purchases_id)){
				$project_ref = array();
				//$in['project_purchases_id'] = explode(',',$in['project_purchases_id'][0]);
				if(!$j){
					$j = 0;
				}
				foreach ($project_purchases_id as $in['project_purchase_ids']){
					$purchase = $this->db->query("SELECT project_purchase.*,projects.name as p_name,projects.serial_number FROM project_purchase 
						INNER JOIN projects ON project_purchase.project_id=projects.project_id
						WHERE project_purchase.project_purchase_id='".$in['project_purchase_ids']."' ");
					if($purchase->next()){
						$project_ref[$purchase->f('p_name')] = $purchase->f('serial_number');
						$margin_price = ($purchase->f('unit_price')*$purchase->f('margin')/100) + $purchase->f('unit_price');
						$in['description'][$j] = $purchase->f('p_name').': '.$purchase->f('description');
						$in['quantity'][$j] = 1;
						$in['price'][$j] = $margin_price;
						$in['price_with_vat'][$j] = $margin_price + ($margin_price*$vat/100);
						$in['vat_percent'][$j] = $vat;
						$in['project_purchase_id'][$j] = $in['project_purchase_ids'];
						$in['origin_id'][$j]=$purchase->f('project_id');
						$in['origin_type'][$j]='3';
						$j++;
					}
				}
				foreach ($project_ref as $key => $value) {
					$our_ref = $value.',';
				}
			}
			$project_articles_id=array();
			foreach($in['project_a_id'] as $key => $value){
				if($value['checked']){
					//array_push($project_articles_id, $value['id']);
					$temp_art=explode('-',$value['id']);
					$project_articles_id[$temp_art[0]]=$temp_art[1];
				}			
			}
			if(!empty($project_articles_id)){
				$project_ref = array();
				//$in['project_a_id'] = explode(',',$in['project_a_id'][0]);
				if(!$j){
					$j = 0;
				}
				foreach ($project_articles_id as $in['project_a_ids']=>$p_id){
					$article = $this->db->query("SELECT SUM(service_delivery.quantity) AS total_quantity,GROUP_CONCAT(service_delivery.id) as delivery_ids,project_articles.name,project_articles.price,projects.project_id,projects.quote_id,pim_articles.packing,pim_articles.sale_unit,pim_articles.item_code,projects.name as p_name,projects.serial_number FROM service_delivery
						                  INNER JOIN projects ON service_delivery.project_id=projects.project_id
						                  INNER JOIN project_articles ON service_delivery.project_id=project_articles.project_id AND service_delivery.a_id=project_articles.article_id
						                  LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
						                   WHERE service_delivery.project_id='".$p_id."' AND service_delivery.a_id='".$in['project_a_ids']."' GROUP BY service_delivery.a_id");
					if($article->next()){
						$project_ref[$article->f('p_name')] = $article->f('serial_number');
						$packing_art=($this->ALLOW_ARTICLE_PACKING && $article->f('packing')>0) ? $article->f('packing') : 1;
	            			$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $article->f('sale_unit')>0) ? $article->f('sale_unit') : 1;
				            $in['description'][$j] = $article->f('p_name').': '.$article->f('name');
				            $in['quantity'][$j] = $article->f('total_quantity')*($packing_art/$sale_unit_art);
				            $in['price'][$j] = $article->f('price');
				            $in['price_with_vat'][$j] = $article->f('price') + ($article->f('price')*$vat/100);
				            $in['vat_percent'][$j] = $vat;
				            $in['article_code'][$j]=stripslashes($article->f('item_code'));
				            //$in['project_article_id'][$j] = $in['project_a_ids'];
				            $in['project_article_id'][$j] = $article->f('delivery_ids');
				            $in['origin_id'][$j]=$article->f('project_id');
						$in['origin_type'][$j]='3';
						$j++;
						if($article->f('quote_id')>0){
							$article_tax=$this->db->query("SELECT pim_article_tax.amount,pim_article_tax.description FROM pim_article_tax
									INNER JOIN pim_articles_taxes ON pim_article_tax.tax_id=pim_articles_taxes.tax_id
									WHERE pim_articles_taxes.article_id='".$article->f('article_id')."' ");
							while($article_tax->next()){
								$in['description'][$j] = $article_tax->f('description');
								$in['quantity'][$j] = $article->f('quantity');
						            $in['price'][$j] = $article_tax->f('amount');
						            $in['price_with_vat'][$j] = $article_tax->f('amount') + ($article_tax->f('amount')*$vat/100);
						            $in['vat_percent'][$j] = $vat;
								$j++;
							}
						}
					}
				}
				foreach ($project_ref as $key => $value) {
					$our_ref = $value.',';
				}
			}
		}

		if($in['projects_id'] && !$in['description']){
			$output['msg_notice']='<div class="info info_big">'.gm('No uninvoiced billable hours found on selected period for this customer').'. '.gm('You can create a free form invoice below').'.</div>';
		}

		$output['our_ref'] = $our_ref ? rtrim($our_ref,',') : $in['our_ref'];
		$cols=7;
		if(!$in['apply_discount'] || $in['apply_discount'] == 2 ){
			$cols=6;
			if($in['remove_vat'] == 1){
				$cols=5;
			}
		}

		$i=0;
		$line_total=0;
		$sub_discount=array();
		$subtotal=0;
		$vat_percent = array();
		$sub_t = array();
		$vat_percent_val = 0;
		$discount_value = 0;
		$netto_amount=0;

		$output['invoice_line']=array();
		$code_width=0;
		$last_tr='';
		$last_combined='';
		$last_variant_parent='';
		if($in['description']){
			foreach ($in['description'] as $row){

				if(!($in['is_tax'][$i])){
					 $in['is_tax'][$i]=0;
				}

				$line_total2 = 0;
				$vat_values = $in['vat_percent'][$i] ? $in['vat_percent'][$i] : $vat;
				$vat_value += ($in['quantity'][$i] * $in['price'][$i])*$vat_values/100;
				$row_id = 'tmp'.$i;
				$vat_i = $in['vat_percent'][$i] ? $in['vat_percent'][$i] : $vat;
				
				$line_item_width=$item_width;
				if(!$in['apply_discount'] || $in['apply_discount'] == 2){
					$line_item_width+=1;
				}
				if(!$in['article_code'][$i]){
					$line_item_width+=2;
				}

				if(!($in['is_tax'][$i])){
					$last_tr=$row_id;
				}

				if($in['is_combined'][$i]){
					$last_combined=$row_id;
				}

				if($in['has_variants'][$i] && $in['has_variants_done'][$i]){
					$last_variant_parent = $row_id;
				}

				$nr_dec_q = strlen(rtrim(substr(strrchr($in['quantity'][$i], "."), 1),'0'));

				$invoice_line=array(
					'tr_id'         			=> $row_id,
					'description'  			=> $in['description'][$i],
					'quantity'      			=> display_number($in['quantity'][$i], $nr_dec_q),
					'quantity_old'      		=> display_number($in['quantity'][$i], $nr_dec_q),
					'quantity_component'      	=> display_number($in['quantity_component'][$i], $nr_dec_q),
					//'quantity'      			=> display_number_var_dec($in['quantity'][$i]),
					'price'         			=> display_number_var_dec($in['price'][$i]),
					'task_time_ids'      		=> $in['task_time_ids'][$i],
					'expenses_ids'			=> $in['expense'][$i],
					'order_ids'				=> $in['order'][$i],
					'tasks_ids'				=> $in['task_id'][$i],
					'purchase_ids'			=> $in['project_purchase_id'][$i],
					'project_article_ids'		=> $in['project_article_id'][$i],
					'deliveries'			=> $in['deliveries'][$i],
					'article_id'			=> $in['article_id'][$i],
					'tax_id'				=> $in['tax_id'][$i],
					'tax_for_article_id'	    	=> $in['is_tax'][$i]==1?$in['tax_for_article_id'][$i]:'',
					'is_tax'      	            => $in['is_tax'][$i],
					'for_article'			=> $in['is_tax'][$i]? $last_tr: '', 
					'purchase_price'			=> display_number_var_dec($in['purchase_price'][$i]),
					'vat'					=> $in['vat_percent'][$i] ? display_number($in['vat_percent'][$i]) : $vat,
					'discount_line'			=> display_number($in['discount_line'][$i]),

					'title'  			    	=> $in['title'][$i],
					//'title_content'			=> $in['content'][$i] ==1 ? 0 : '',
	                'content'  			    => $in['content'][$i],
			        'colspan'				=> $in['content'][$i] ==1 ? 'colspan="'.$cols.'" class="last"' : '',
			        'content_class'			=> $in['content'][$i] ==1 ? ' type_content ' : '',
					'unmodifiable'			=> $in['readonly'][$i] ? 'readonly':'',
					'article_id' 			=> $in['article_id'][$i],
					'article_code' 			=> $in['article_code'][$i],
					'show_for_articles'		=> $in['article_code'][$i] ? true : false,
					'width_if_article_code' 	=> $in['article_code'][$i] ? 'width:155px;' : 'width:270px;',
					'item_width'			=> $line_item_width,
					'service_delivery_ids'  => $in['service_delivery_ids'][$i],
					'origin_id'				=> $in['origin_id'][$i],
					'origin_type'			=> $in['origin_type'][$i],
					'has_variants' 			=> $in['has_variants'][$i],
				 	'has_variants_done' 	=> $in['has_variants_done'][$i],
				 	'is_variant_for' 		=> $in['is_variant_for'][$i],
				 	'is_variant_for_line' 	=> $in['is_variant_for'][$i]? $last_variant_parent: '',
				 	'variant_type' 			=> $in['variant_type'][$i],
				 	'is_combined' 			=> $in['is_combined'][$i],
				 	'component_for' 		=> $in['component_for'][$i]? $last_combined :'',
				 	'visible' 				=> $in['visible'][$i],
				);

				if ($in['base_type']==3)
				{
					$invoice_line['price_vat']     	= display_number_var_dec($in['price_with_vat'][$i]);
				}elseif ($in['base_type'] == 4){
					$invoice_line['price_vat']     	= display_number_var_dec($in['price_with_vat'][$i]);
				}
				else
				{ 
					$invoice_line['price_vat']      	= display_number_var_dec(($in['price'][$i]+($in['price'][$i]*$vat_i/100)));
				}
				$discount_line = $in['discount_line'][$i];
				if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$discount_line = 0;
				}
				$line_total2= $in['quantity'][$i] * ($in['price'][$i] -$in['price'][$i] * $discount_line / 100);

				$invoice_line['line_total']       		= display_number($line_total2);
				$invoice_line['hide_currency2']		= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
				$invoice_line['hide_currency1']		= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
				array_push($output['invoice_line'], $invoice_line);

				$line_discount= $line_total2 * $in['discount'] / 100;
				if($in['apply_discount'] < 2){
					$line_discount = 0;
				}

				$subtotal +=$line_total2;

				$vat_percent_val = ($line_total2 - $line_discount) * $vat_i/100;
				//$vat_percent[$vat_i] += round($vat_percent_val,ARTICLE_PRICE_COMMA_DIGITS);
				$vat_percent[$vat_i] += $vat_percent_val;
				//$sub_t[$vat_i] += round($line_total2,ARTICLE_PRICE_COMMA_DIGITS);
				$sub_t[$vat_i] += $line_total2;
				$sub_discount[$vat_i] += $line_discount;
				$discount_value += $line_discount;
				$netto_amount+=(round($line_total2,2)-$line_discount);
				$code_width=$in['article_code'][$i] ? $code_width+1 : $code_width+0;
				$i++;
			}
		}else{
			if($in['invoice_line'] && !empty($in['invoice_line'])){
				$last_tr='';
				$last_combined='';
				$last_variant_parent ='';

					foreach ($in['invoice_line'] as $row){

								
								$line_total2 = 0;
								$vat_values = $row['vat'] ? $row['vat'] : $vat;
								$vat_value += ($row['quantity'] * $row['price'])*$vat_values/100;
								$row_id = 'tmp'.$i;
								$vat_i = $row['vat'] ? $row['vat'] : $vat;
								
								$line_item_width=$item_width;
								if(!$in['apply_discount'] || $in['apply_discount'] == 2){
									$line_item_width+=1;
								}
								if(!$row['article_code']){
									$line_item_width+=2;
								}
								if(!($row['is_tax'])){
									 $row['is_tax']=0;
									 $last_tr=$row_id;
								}

								if($row['is_combined']){
									 $last_combined=$row_id;
								}

								if($row['has_variants'] && $row['has_variants_done']){
									$last_variant_parent = $row_id;
								}

								$nr_dec_q = strlen(rtrim(substr(strrchr($row['quantity'], "."), 1),'0'));

								$invoice_line=array(
									'tr_id'         		=> $row_id,
									'description'  			=> $row['description'],
									'quantity'      		=> display_number($row['quantity'], $nr_dec_q),
									'quantity_old'      	=> display_number($row['quantity'], $nr_dec_q),
									'quantity_component'    => display_number($row['quantity_component'], $nr_dec_q),
									//'quantity'      		=> display_number_var_dec($row['quantity']),
									'price'         		=> display_number_var_dec($row['price']),
									'article_id'			=> $row['article_id'],
									'tax_id'				=> $row['tax_id'],
									'tax_for_article_id'	=> $row['is_tax']==1?$row['tax_for_article_id']:'',
									'is_tax'      	        => $row['is_tax'],
									'for_article'			=> $row['is_tax']==1? $last_tr:'',
									'purchase_price'		=> display_number_var_dec($row['purchase_price']),
									'vat'					=> $row['vat'] ? display_number($row['vat']) : $vat,
									'discount_line'			=> display_number($row['discount_line']),

									'title'  			    => $row['title'],
					                'content'  			    => $row['content'],
							        'colspan'				=> $row['content'] ==1 ? 'colspan="'.$cols.'" class="last"' : '',
							        'content_class'			=> $row['content'] ==1 ? ' type_content ' : '',
									'unmodifiable'			=> $row['readonly'] ? 'readonly':'',
									'article_id' 			=> $row['article_id'],
									'article_code' 			=> $row['article_code'],
									'show_for_articles'		=> $row['article_code'] ? true : false,
									'width_if_article_code' => $row['article_code'] ? 'width:155px;' : 'width:270px;',
									'item_width'			=> $line_item_width,
									'service_delivery_ids'  => $row['service_delivery_ids'],
									'origin_id'				=> $row['origin_id'],
									'origin_type'			=> $row['origin_type'],
									'has_variants' 			=> $row['has_variants'],
								 	'has_variants_done' 	=> $row['has_variants_done'],
								 	'is_variant_for' 		=> $row['is_variant_for'],
								 	'is_variant_for_line' 	=> $row['is_variant_for']? $last_variant_parent:'',
								 	'variant_type' 			=> $row['variant_type'],
								 	'is_combined' 			=> $row['is_combined'],
				 					'component_for' 		=> $row['component_for']? $last_combined:'',
				 					'visible' 				=> $row['visible'],
								);

								if ($in['base_type']==3)
								{
									$invoice_line['price_vat']     	= display_number_var_dec($row['price_with_vat']);
								}elseif ($in['base_type'] == 4){
									$invoice_line['price_vat']     	= display_number_var_dec($row['price_with_vat']);
								}
								else
								{ 
									$invoice_line['price_vat']      	= display_number_var_dec(($row['price']+($row['price']*$vat_i/100)));
								}
								$discount_line = $row['discount_line'];
								if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
									$discount_line = 0;
								}
								$line_total2= $row['quantity'] * ($row['price'] -$row['price'] * $discount_line / 100);

								$invoice_line['line_total']       		= display_number($line_total2);
								$invoice_line['hide_currency2']		= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
								$invoice_line['hide_currency1']		= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
								array_push($output['invoice_line'], $invoice_line);

								$line_discount= $line_total2 * $in['discount'] / 100;
								if($in['apply_discount'] < 2){
									$line_discount = 0;
								}

								$subtotal +=$line_total2;

								$vat_percent_val = ($line_total2 - $line_discount) * $vat_i/100;
								//$vat_percent[$vat_i] += round($vat_percent_val,ARTICLE_PRICE_COMMA_DIGITS);
								$vat_percent[$vat_i] += $vat_percent_val;
								//$sub_t[$vat_i] += round($line_total2,ARTICLE_PRICE_COMMA_DIGITS);
								$sub_t[$vat_i] += $line_total2;
								$sub_discount[$vat_i] += $line_discount;
								$discount_value += $line_discount;
								$netto_amount+=(round($line_total2,ARTICLE_PRICE_COMMA_DIGITS)-$line_discount);
								$code_width=$row['article_code'] ? $code_width+1 : $code_width+0;
								$i++;
						}
			}
		}

		$output['vat_line']=array();

		$t=0;
		$output['hide_total'] = true;
		foreach ($vat_percent as $key => $val){
            if($sub_t[$key] - $sub_discount[$key]) {
                $total_vat += $val;
                $vat_line = array(
                    'vat_percent' => display_number($key),
                    'vat_value' => display_number($val),
                    'subtotal' => display_number($sub_t[$key]),
                    'total_novat' => display_number($sub_t[$key]),
                    'is_ord' => $in['base_type'] == 3 ? true : false,
                    'discount_value' => display_number($sub_discount[$key]),
                    'view_discount' => $in['apply_discount'] > 1 ? ($in['discount'] || $disc) ? true : false : false,
                    'net_amount' => display_number($sub_t[$key] - $sub_discount[$key]),
                );
                if ($in['apply_discount'] == 0) {
                    $vat_line['view_discount'] = false;
                }
                array_push($output['vat_line'], $vat_line);
            }
			$t++;
		}
		if($t==1){
			$output['hide_total'] = false;
		}

		$total=($subtotal - $discount_value) +$total_vat;

		$output['discount_value'] 		     = display_number($discount_value);
		$output['total']          		     = $total ? display_number($total):display_number(0);
		$output['total_default_currency']        = $total ? display_number($total):display_number(0);
		$output['net_amount']			     = display_number($netto_amount);
		$output['total_wo_vat']			     = display_number($subtotal);

		if($in['discount'] || $disc ){
			$output['view_discount1'] 		= true;
		}else{
			$output['view_discount1']		= false;
		}

		if($in['type']==1 && $in['req_payment']!=100){
			$output['view_req_payment']		= true;
		}else{
			$output['view_req_payment']		= false;
		}
		$output['project_id']          		= $in['project_id'];
		$output['hours_type']          		= $in['hours_type'];
		$output['custom_hours']        		= $in['custom_hours'];
		$output['base_type']           		= $in['base_type'];
		$output['invoice_start_date']    		= strtotime($in['invoice_start_date']);
		$output['invoice_end_date']    		= strtotime($in['invoice_end_date']);
		$output['currency_type']		   	= $in['currency_type'] ? $in['currency_type'] : ACCOUNT_CURRENCY_TYPE;
		$output['currency_val']		   	= get_commission_type_list($output['currency_type']);
		$output['currency_type_list']    		= build_currency_list($in['currency_type']);
		$output['CURRENCY_TXT']		    		= currency::get_currency($in['currency_type'],'name');


		$separator = $this->db->field("SELECT value from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
		$currency = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
    	$output['currency_rate'] = currency::getCurrency(currency::get_currency($in['currency_type'],'code'),$currency, 1,$separator);
    	$output['discount']              		= display_number($in['discount']);
    	$output['discount_line_gen'] = display_number($in['discount_line_gen']);

    	if($in['buyer_id']){
			$b_inf = $this->db->query("SELECT customers.line_discount, customers.apply_line_disc, customers.apply_fix_disc, customers.fixed_discount
				  	FROM customers
				  	WHERE customers.customer_id = '".$in['buyer_id']."' ");
			$b_inf->next();

			if($b_inf->f('apply_line_disc')){
				if($in['third_party_applied'] && $in['third_party_id']){
					//no reset of discounts
				}else{
					$output['discount']              		= display_number($b_inf->f('line_discount'));
					$output['discount_line_gen']            = display_number($b_inf->f('line_discount'));
				}			
			}
			if($b_inf->f('apply_fix_disc')){
				if($in['third_party_applied'] && $in['third_party_id']){
					//no reset of discounts
				}else{
					$output['discount']              		= display_number($b_inf->f('fixed_discount'));
				}			
			}

    	}

		if(!$in['apply_discount']){
			$in['apply_discount']="0";
		}
		if(!$in['email_language']){
			//$in['email_language']= $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'");
			$in['email_language']=$this->db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
		}
		$output['generalvats']=build_vat_dd();

		//console::log($output['notes2']);
		$data_last=$this->get_invoiceLast($in,$page_title,$do_next,$customer_id,$item_width,$code_width,$type_title,$nr_status);


		foreach($data_last as $key => $value){
			$output[$key]=$value;
		}
		//console::log($output['notes2']);

		$output['total_currency_hide']			= $output['currency_type'] ? ($output['currency_type'] == ACCOUNT_CURRENCY_TYPE ? false : true) : false;

		if($in['from_orders']) {
            insert_message_log('order', '{l}Invoice created by{endl} '.get_user_name($_SESSION['u_id']), 'order_id', $in['orders_id'], false, $_SESSION['u_id']);
        }
        $output['is_first_invoice'] = $this->db->field("SELECT COUNT(id) FROM tblinvoice WHERE type='0'") ? false : true;

        $this->out = $output;
	}

	private function getEditInvoice(){

		$in = $this->in;
		$table = 'tblinvoice';
		$field = 'invoice';
		$invoice_title = gm('Invoice Nr').'*:';
		$is_credit = true;
		$our_ref_number = $this->db->field("SELECT serial_number FROM tblquote WHERE id='".$in['c_quote_id']."' ");
		if($in['c_invoice_id']){
			if(!$in['invoice_id'] || $in['invoice_id']=='tmp'){
				$do_next='invoice-ninvoice-invoice-add';
				$page_title=gm('Add Credit Invoice');
				$type_title = gm('Draft');
				$nr_status = 1;
			}else{
				$do_next='invoice-ninvoice-invoice-update';
				$page_title=gm('Edit Credit Invoice');
			}				
			$invoice_id=$in['c_invoice_id'];
			$invoice_title = gm('Credit Note Nr:');
			$is_credit = false;
		}elseif(!$in['c_invoice_id'] && $in['duplicate_invoice_id'] && !$in['invoice_id']){
			$page_title=gm('Duplicate invoice');
			$do_next='invoice-ninvoice-invoice-add';
			$invoice_id=$in['duplicate_invoice_id'];
			$type_title = gm('Draft');
			$nr_status = 1;
		}elseif($in['c_quote_id']){
			$table = 'tblquote';
			$page_title=gm('Add Invoice');
			$do_next='invoice-ninvoice-invoice-add';
			$invoice_id=$in['c_quote_id'];
			$our_ref_number=$in['a_quote_id'];
			$in['our_ref'] = $our_ref_number ? $our_ref_number : $in['a_quote_id'];
			$field = 'quote';
		}
		elseif($in['c_contract_id']){
			$table = 'contracts';
			$page_title=gm('Add Invoice');
			$do_next='invoice-ninvoice-invoice-add';
			$invoice_id=$in['c_contract_id'];
			$field = 'contract';
			$in['our_ref'] = $in['a_contract_id'];
		}
		else{
			$page_title=gm('Edit Invoice');
			$do_next='invoice-ninvoice-invoice-update';
			$invoice_id=$in['invoice_id'];
		}
		
		$default_note = $this->db->field("SELECT value FROM default_data WHERE type='invoice_note' ");

    	if($in['c_contract_id']){
			$inv_q = $this->db->query("SELECT contracts.*,contracts_version.version_id FROM contracts 
				INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
				WHERE contracts.contract_id='".$invoice_id."' AND contracts_version.active='1' ");
		}else{
        		$inv_q = $this->db->query("SELECT * FROM $table WHERE id='".$invoice_id."' ");
    		}
    		if($inv_q->f('block_edit') && $in['invoice_id']){
    			json_out($in);
    		}
    		$to_edit=false;
		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_4' ");	
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
				$to_edit=true;
				break;
		}

		if(!$in['c_invoice_id']){
			$in['order_inv'] = $inv_q->f('order_id');
			$output['order_inv']=$inv_q->f('order_id');
		}
		if($in['c_quote_id']){
			$in['your_ref']= $in['your_ref'] ? $in['your_ref'] : $inv_q->f('own_reference');
		}

		if($inv_q->f('type')=='1'){
			$page_title= gm('Edit Proforma Invoice');
		}


		if(!$inv_q->next()){
			return ark::run('invoice-invoices');
			/*page_redirect('index.php?do=invoice-invoices&error='.gm('Invalid ID'));
			unset($view_ninvoice);
			return msg_error('Invalid ID');*/
		}
		if($inv_q->f('c_invoice_id')){
			$page_title=gm('Edit Credit Invoice');
			$invoice_title = gm('Credit Note Nr:');
			$is_credit = false;
		}
		if($in['buyer_id'])
		{
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND billing='1' ");
			if(!$buyer_details){
				$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			}
			$main_address_id			= $buyer_details->f('address_id');
			if($in['c_contract_id'] && $in['is_contact']){
				$buyer_details = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['buyer_id']."'");
				$details = array('table' 		=> 'customer_contact_address',
							 'field'		=> 'contact_id',
							 'value'		=> $inv_q->f('buyer_id'),
							 'name'		=> $inv_q->f('customer_name'),
							 'filter'		=> '' );
			}else{
				/*$filter = '';
				if($in['change'] == 1){
					$filter = " AND billing=1 ";
				}*/
				$buyer_details = $this->db->query("SELECT customers.attention_of_invoice, customers.comp_phone, customers.name, customers.comp_fax, customer_addresses.*
											FROM customers
											LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
											WHERE customers.customer_id = '".$in['buyer_id']."' AND billing=1 ");
				if(!$buyer_details->next()){
					$buyer_details = $this->db->query("SELECT customers.attention_of_invoice, customers.comp_phone, customers.name, customers.comp_fax, customer_addresses.*
											FROM customers
											LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
											WHERE customers.customer_id = '".$in['buyer_id']."' AND is_primary=1 ");
				}
				$new_buyer_id = false;
			}

		}else{
			$details = array('table' 			=> $inv_q->f('buyer_id') ? 'customer_addresses' : 'customer_contact_address' ,
							 'field'		=> $inv_q->f('buyer_id') ? 'customer_id' : 'contact_id',
							 'value'		=> $inv_q->f('buyer_id') ? $inv_q->f('buyer_id') : $inv_q->f('contact_id'),
							 'name'		=> $inv_q->f('buyer_name'),
							 'filter'		=> $inv_q->f('buyer_id') ? ' AND billing =1' : '' );

			$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
			$buyer_details->next();
			if(!$in['buyer_id'])
				{
					$in['buyer_id'] = $inv_q->f('buyer_id');
					$new_buyer_id = true;
				}
		}

		$due_date = $inv_q->f('due_date');
		$due = date(ACCOUNT_DATE_FORMAT,$inv_q->f('due_date'));
		$due_days = 30;

		$customer_notes="";
		$has_third_party=false;
		$third_party_id='';
		$third_party_name='';
		$third_party_address='';
		$third_party_regime_id='';
		if($in['third_party_applied'] && $in['third_party_id'] && !$in['c_invoice_id'] && !$in['duplicate_invoice_id']){
			$has_third_party=true;
			$third_party_info = $this->db->query("SELECT customer_legal_type.name as l_name,customers.name AS company_name,customers.vat_regime_id
			  	FROM customers
			  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
			  	WHERE customers.customer_id = '".$in['third_party_id']."' ");
			$third_party_id=$in['third_party_id'];
			$third_party_name=stripslashes($third_party_info->f('company_name').' '.$third_party_info->f('l_name'));
			$third_party_regime_id=$third_party_info->f('vat_regime_id');
			$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['third_party_id']."' AND billing='1' ");
			if(!$third_party_details->next()){
				$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['third_party_id']."' AND is_primary='1' ");
			}
			$third_party_address=strip_tags($third_party_details->f('address')).' '.$third_party_details->f('zip').' '.$third_party_details->f('city').' '.get_country_name($third_party_details->f('country_id'));
		}else if(!$in['third_party_applied'] && $inv_q->f('third_party_id') && !$in['c_invoice_id'] && !$in['duplicate_invoice_id']){
			$in['third_party_applied']='1';
			$has_third_party=true;
			$third_party_info = $this->db->query("SELECT customer_legal_type.name as l_name,customers.name AS company_name,customers.vat_regime_id
			  	FROM customers
			  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
			  	WHERE customers.customer_id = '".$inv_q->f('third_party_id')."' ");
			$third_party_id=$inv_q->f('third_party_id');
			$third_party_name=stripslashes($third_party_info->f('company_name').' '.$third_party_info->f('l_name'));
			$third_party_regime_id=$third_party_info->f('vat_regime_id');
			$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$inv_q->f('third_party_id')."' AND billing='1' ");
			if(!$third_party_details->next()){
				$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$inv_q->f('third_party_id')."' AND is_primary='1' ");
			}
			$third_party_address=strip_tags($third_party_details->f('address')).' '.$third_party_details->f('zip').' '.$third_party_details->f('city').' '.get_country_name($third_party_details->f('country_id'));
		}
		if($in['buyer_id']){
			if($in['c_contract_id'] && $in['is_contact']){
				$buyer_info = $this->db->query("SELECT phone, cell, email FROM customer_contacts WHERE contact_id = '".$in['buyer_id']."' ");
				$buyer_info->next();
				$c_email = $buyer_info->f('email');
				$c_fax = '';
				$c_phone = $buyer_info->f('phone');
				$name = $inv_q->f('customer_name');
				$text =gm('Name').':';
				$payment_term_days_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
				$payment_term_days = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
			}else{
				$buyer_info = $this->db->query("SELECT customers.attention_of_invoice,customers.payment_term,customers.payment_term_type, customers.btw_nr, customers.c_email, customers.vat_regime_id,
													customer_legal_type.name as l_name, customers.no_vat, customers.internal_language, customers.invoice_note2,  customers.customer_notes,customers.third_party_id
																FROM customers
																LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
																WHERE customers.customer_id = '".$in['buyer_id']."' ");
				$buyer_info->next();
				$in['seller_bwt_nr'] = $buyer_info->f('btw_nr');
				$due_days = $buyer_info->f('payment_term');
				if($new_buyer_id)
				{
					$name = $details['name'];
					if($buyer_info->f('l_name') && strpos($name, $buyer_info->f('l_name')) === false )
					{
						$name = $details['name'].' '.$buyer_info->f('l_name');
					}
				}else {
					$name = $buyer_details->f('name');
					if($buyer_info->f('l_name') && strpos($name, $buyer_info->f('l_name')) === false )
					{
						$name = $buyer_details->f('name').' '.$buyer_info->f('l_name');
					}
				}

				$text = gm('Company Name').':';
				$c_email = $buyer_info->f('c_email');
				$c_fax = $buyer_details->f('comp_fax');
				$c_phone = $buyer_details->f('comp_phone');
				//$in['remove_vat'] = $buyer_info->f('no_vat');
				//$in['notes2'] = $buyer_info->f('invoice_note2');
				$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$buyer_info->f('vat_regime_id')."' ");
				$in['notes2'] = $buyer_info->f('customer_notes');
				$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$buyer_info->f('vat_regime_id')."' ");
				$payment_term_days_type = $this->db->field("SELECT payment_term_type FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
				$payment_term_days = $this->db->field("SELECT payment_term FROM customers WHERE customer_id = '".$in['buyer_id']."' ");

				$buyer_credit_limit_info=$this->db->query("SELECT credit_limit,currency_id FROM customers WHERE customer_id='".$in['buyer_id']."' ");
				if($buyer_credit_limit_info->f('credit_limit')){
					$invoices_customer_due=customer_credit_limit($in['buyer_id']);
					if($invoices_customer_due && $invoices_customer_due>$buyer_credit_limit_info->f('credit_limit')){
						$buyer_currency_id=$buyer_credit_limit_info->f('currency_id') ? $buyer_credit_limit_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
						$customer_credit_limit=place_currency(display_number($buyer_credit_limit_info->f('credit_limit')),get_commission_type_list($buyer_currency_id));
					}
				}
				if(!$in['third_party_applied'] && $buyer_info->f('third_party_id')){
					$has_third_party=true;
					$third_party_info = $this->db->query("SELECT customer_legal_type.name as l_name,customers.name AS company_name,customers.vat_regime_id
					  	FROM customers
					  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
					  	WHERE customers.customer_id = '".$buyer_info->f('third_party_id')."' ");
					$third_party_id=$buyer_info->f('third_party_id');
					$third_party_name=stripslashes($third_party_info->f('company_name').' '.$third_party_info->f('l_name'));
					$third_party_regime_id=$third_party_info->f('vat_regime_id');
					$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$buyer_info->f('third_party_id')."' AND billing='1' ");
					if(!$third_party_details->next()){
						$third_party_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$buyer_info->f('third_party_id')."' AND is_primary='1' ");
					}
					$third_party_address=strip_tags($third_party_details->f('address')).' '.$third_party_details->f('zip').' '.$third_party_details->f('city').' '.get_country_name($third_party_details->f('country_id'));
				}
			}
			$customer_notes=stripslashes($buyer_info->f('customer_notes'));

		}else{
			$payment_term_days_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
			$payment_term_days = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
			$buyer_info = $this->db->query("SELECT phone, cell, email FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
			$buyer_info->next();
			$c_email = $buyer_info->f('email');
			$c_fax = '';
			$c_phone = $buyer_info->f('phone');
			$name = $details['name'];
			$text =gm('Name').':';
		}

		$in['email_language'] 			= $inv_q->f('email_language');
	  	$in['languages']				= $in['email_language'];

		$rel = $details['table'].'.'.$details['field'].'.'.$details['value'];

		$customer_id = $inv_q->f('buyer_id');
		$in['vat_regime_id'] = $inv_q->gf('vat_regime_id');
		if($in['vat_regime_id'] && $in['vat_regime_id']<10000){
		     $in['vat_regime_id']=$this->db->field("SELECT id FROM vat_new WHERE vat_regime_id='".$in['vat_regime_id']."' ");
		}
		if($in['vat_regime_id']){
		     $in['remove_vat']=$this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		     $in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$in['vat_regime_id']."' ");
		}
		$vat=$in['vat'] ? display_number($in['vat']) : display_number(0);
		//$vat = ($inv_q->f('vat') && (!$in['c_quote_id'] || !$in['c_contract_id_id'])) ? display_number($inv_q->f('vat')) : get_customer_vat($customer_id);
		$in['apply_discount'] = $inv_q->gf('apply_discount');

		$discount = $inv_q->f('discount');
		if($in['apply_discount'] < 2){
			$discount = 0;
		}
		$in['req_payment']=$inv_q->gf('req_payment');
		if(!$in['req_payment'] && ($in['c_quote_id'] || $in['c_contract_id']) ){
			$in['req_payment'] = 100;
		}
		$req_payment = $in['req_payment'];

		$free_field=$inv_q->f('free_field');
		if(!$free_field){
			$free_field = $inv_q->f('buyer_address').'
			'.$inv_q->f('buyer_zip').' '.$inv_q->f('buyer_city').'
			'.get_country_name($inv_q->f('buyer_country_id'));
		}	
		if($in['change'] || $in['c_contract_id'] || $in['third_party_applied']){
			if($in['is_contact']){
				$free_field = $buyer_details->f('address').'
				'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
				'.get_country_name($buyer_details->f('country_id'));
			}else{
				$free_field = $buyer_details->f('address').'
				'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
				'.get_country_name($buyer_details->f('country_id'));
			}

		}
		if($in['main_address_id'] && $in['main_address_id']!=$inv_q->f('main_address_id')){
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$free_field = $buyer_details->f('address').'
				'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
				'.get_country_name($buyer_details->f('country_id'));
		}
		//console::log($free_field,$inv_q->f('free_field'));
		if($in['duplicate_invoice_id']){
			$in['languages'] = $inv_q->f('email_language');
			if($in['change']){
				$in['languages'] = $buyer_info->f('internal_language');

			}
		}
		if(!$in['due_days']){
	 		$in['due_days'] = $inv_q->gf('due_days');
	 	}
	 	if(!$in['due_days']){
	 		 $ssdate = new DateTime();
	 		 $ssdate->setTimestamp($inv_q->f('invoice_date'));
	 		 $sddate = new DateTime();
	 		 $sddate->setTimestamp($inv_q->f('due_date'));
	 		 $interval = $ssdate->diff($sddate);
			 $in['due_days'] =$interval->format('%d');
	 	}

		$ttime = $inv_q->f('invoice_date');
		if($in['c_quote_id'] || $in['c_invoice_id'] ||  $in['c_contract_id'] || $in['duplicate_invoice_id']){
			$ttime = time();
		}

		if($payment_term_days_type==1) {
			$start_date = time();
		} else {
			$curMonth = date('n',$ttime);
			$curYear  = date('Y',$ttime);
		  	$firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
			$start_date = $firstDayNextMonth;
		}

		if($in['c_quote_id'] || $in['c_invoice_id'] || $in['c_contract_id']) {
			$due_date = $start_date + ($payment_term_days*(60*60*24)-1);
		} else if($in['duplicate_invoice_id']) {
			$due_date = $start_date + ($payment_term_days*(60*60*24)-1);
		} else {
			$due_date = $inv_q->f('due_date');
		}

		$select = $this->db->query("SELECT * FROM tblinvoice_projects WHERE invoice_id='".$in['invoice_id']."'")->records_count();
		$existIdentity = $this->db->field("SELECT COUNT(identity_id) FROM multiple_identity ");
		$customer_mandat = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' ");
		$customer_mandat_active = $this->db->field("SELECT mandate_id FROM mandate WHERE buyer_id = '".$in['buyer_id']."' AND active_mandate ='1' ");

		if($in['invoice_id']){
			$invoice_serial=$this->db->field("SELECT serial_number FROM tblinvoice WHERE id='".$in['invoice_id']."'");
		}
		$item_width=$inv_q->gf('remove_vat') == 1 ? 5 : 4;

		/*$contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
									WHERE contact_id='".$inv_q->f('contact_id')."'");*/
		$in['contact_id'] =$in['third_party_applied'] && !$in['contact_id'] ? '0' : ($in['contact_id']? $in['contact_id']: $inv_q->f('contact_id'));							
		$contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
									WHERE contact_id='".$in['contact_id']."'");
		
		if($in['save_final']==1){
			$final=true;
		}else{
			$final=false;
		}

		if($inv_q->gf('apply_discount')==0){
			$discount_line=false;
			$discount_global=false;
		}elseif($inv_q->gf('apply_discount')==1){
			$discount_line=true;
			$discount_global=false;
		}elseif($inv_q->gf('apply_discount')==2){
			$discount_line=false;
			$discount_global=true;
		}elseif($inv_q->gf('apply_discount')==3){
			$discount_line=true;
			$discount_global=true;
		}
		if($in['buyer_id']){
			$customer_disc = $this->db->query("SELECT line_discount, apply_line_disc,apply_fix_disc FROM customers WHERE customer_id='".$in['buyer_id']."' ");
		}else{
			$customer_disc = $this->db->query("SELECT line_discount, apply_line_disc,apply_fix_disc FROM customers WHERE customer_id='".$in['customer_id']."' ");
		}
		if($customer_disc->f('apply_fix_disc')==1){
			$discount_global=true;
		}else{
			if($inv_q->gf('apply_discount')==0){
				$discount_line=false;
				$discount_global=false;
			}elseif($inv_q->gf('apply_discount')==1){
				$discount_line=true;
				$discount_global=false;
			}elseif($inv_q->gf('apply_discount')==2){
				$discount_line=false;
				$discount_global=true;
			}elseif($inv_q->gf('apply_discount')==3){
				$discount_line=true;
				$discount_global=true;
			}
		} 
	    /*if($in['c_quote_id']!=0){
	      $buyer_vat_regim = $this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
	      $in['vat_regime_id']=$buyer_vat_regim;
	    }else{
	      $in['vat_regime_id'] = $inv_q->f('vat_regime_id');
	    }*/
        if ($in['type'] == 2) {
            $page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='CREDIT_GENERAL_CONDITION_PAGE'");
        } else {
            $page_invoice = $this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_GENERAL_CONDITION_PAGE'");
        }
	    if(!$in['change_currency']){
				$currency_type = $inv_q->f('currency_type');
				$currency_rate = $inv_q->f('currency_rate');
			}else{
				$separator = $this->db->field("SELECT value from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
				$currency = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
				$currency_type = $in['currency_type'];
				$currency_rate= currency::getCurrency(currency::get_currency($in['currency_type'],'code'),$currency, 1,$separator);
				
			}	
		$contact_name->next();
		$stripe_active =$this->db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
		$allow_sepa = $this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_INVOICE_SEPA' ");

		$output=array(
			'ADV_PRODUCT' 							=> defined('ADV_PRODUCT') && ADV_PRODUCT == 1 ? true : false,
			'draft_invoice'				=> !$in['c_invoice_id'] ? false : DRAFT_INVOICE_NO_NUMBER == 1 ? true : false,
			'apply_discount_line'      	=> $discount_line,
			'show_discount'				=> $discount_line,
			'apply_discount_global'     => $discount_global,
			'save_final'				=> $final,
		    //'main_address_id'			=> $inv_q->f('main_address_id'),
		    'main_address_id'			=> $in['main_address_id']? $in['main_address_id'] : ($inv_q->f('main_address_id') ? $inv_q->f('main_address_id') : $main_address_id),
			//'page'						=>  $inv_q->f('general_conditions_new_page') == 1 ? true : ($page_invoice ==1 ? true : false),
			'page'						=>  $page_invoice ==1 ? true : false,
			'is_timesheets' 			=> 	$select > 0 ? true : false,
			'attach_timesheet' 			=> 	$inv_q->f('attach_timesheet') == 1 ? true : false,
			'attach_timesheet_inv_txt'	=> 	$inv_q->f('attach_timesheet') == 1 ? gm('Yes') : gm('No'),

			'is_from_service' 		=> 	$inv_q->f('service_id') ? true : false,
			'service_id'			=> 	$inv_q->f('service_id'),
			'attach_intervention' 		=> 	$inv_q->f('attach_intervention') == 1 ? true : false,
			'attach_intervention_inv_txt'	=> 	$inv_q->f('attach_intervention') == 1 ? gm('Yes') : gm('No'),
			'serial_number_txt'			=> $in['type']==0 || $in['type']==3 ? gm('Invoice Number') : ($in['type']==2 || $in['type']==4 ? gm('Credit Invoice Number') : gm('Proforma Invoice Number')),
			'show_due_date_vat_pdf'	      => SHOW_DUE_DATE_VAT_PDF == 1 ? true : false,
			'invoice_due_date_vat'		=> $in['c_quote_id'] || $in['c_invoice_id'] ||  $in['c_contract_id'] ? time()*1000 : (intval($inv_q->f('due_date_vat')) > 86400 ? $inv_q->f('due_date_vat')*1000 : ''),
			'due_date_vat'			=> $in['c_quote_id'] || $in['c_invoice_id'] ||  $in['c_contract_id'] ? time()*1000 : (intval($inv_q->f('due_date_vat')) > 86400 ? $inv_q->f('due_date_vat')*1000 : 0) ,
			'show_product_save'       	=> 0,
			'last_serial_number'      	=> ($in['type'] == 2 || $inv_q->f('type') == 2 || $in['type'] == 4 || $inv_q->f('type') == 4) ? get_last_credit_invoice_number() : get_last_invoice_number(),
			'hide_proforma'           	=> $inv_q->gf('type')==1 || !$invoice_serial ? false : true,

			'hide_regular'           	=> $inv_q->gf('type')==1? true : false,
			'type'                    	=> $in['c_invoice_id'] || $in['c_quote_id'] ? $in['type'] : $inv_q->f('type'),
			// 'seller_name'             	=> $in['c_contract_id'] ?  $inv_q->f('seller_name'),
			'buyer_name'              	=> $name,

			'invoice_date'            	=> $in['c_quote_id'] || $in['c_invoice_id'] ||  $in['c_contract_id']  ? time()*1000 : ($in['duplicate_invoice_id'] ? time()*1000 : $inv_q->f('invoice_date')*1000 ),
			'invoice_due'			=> $in['c_quote_id'] || $in['c_invoice_id'] ||  $in['c_contract_id'] ? (time() + ( $buyer_info->f('payment_term') * (60*60*24) ))*1000 : ($in['duplicate_invoice_id'] ? (time() + ( $buyer_info->f('payment_term') * (60*60*24) ))*1000 : $inv_q->f('due_date')*1000),
			'invoice_due_date'		=> $due_date*1000,
			'quote_ts'				=> $in['c_quote_id'] || $in['c_invoice_id'] ||  $in['c_contract_id']  ? time()*1000 : ($in['duplicate_invoice_id'] ? time()*1000 : ($inv_q->f('invoice_date')+3600)*1000),

			'due_days'				=> $in['c_quote_id'] || $in['c_invoice_id'] || $in['c_contract_id'] || $in['duplicate_invoice_id'] ? $payment_term_days : $inv_q->f('due_days'),
			'payment_type_choose'		=> $in['c_quote_id'] || $in['c_invoice_id'] || $in['c_contract_id'] || $in['duplicate_invoice_id'] ? ($payment_term_days_type==1? 1 : 2 ) :  ($inv_q->f('payment_term_type')==1 ? 1 : 2),
			'payment_term_type_label'	=> $in['c_quote_id'] || $in['c_invoice_id'] || $in['c_contract_id'] || $in['duplicate_invoice_id'] ? ($payment_term_days_type==1?gm('Payment term(days) from the invoice date') : gm('Payment term(days) from the first day of the next month')) :  ($inv_q->f('payment_term_type')==1 ? gm('Payment term(days) from the invoice date') : gm('Payment term(days) from the first day of the next month')),
			//'serial_number'				=> $in['type']==1 ? gm('Serial Number') : gm('Credit Invoice Number'),
			'discount'                	=> display_number($inv_q->f('discount')),
			'req_payment'               	=> display_number($in['req_payment']),
			'vat'                    	=> $vat,
			'vatx_txt'                 	=> $inv_q->f('remove_vat') == 1 ? '-' : $vat.' %',
			'notes'                 	=> $in['c_quote_id'] ? ($inv_q->f('notes') ? utf8_decode($inv_q->f('notes')) : ($default_note ? utf8_decode($default_note) : '')) : ($inv_q->f('notes') ? utf8_decode($inv_q->f('notes')) : ($default_note ? utf8_decode($default_note) : '')),
			//'notes2'                	=> $in['c_invoice_id'] ? CREDIT_EXTRA."\n".$in['notes2'] :($in['notes2']? $in['notes2'] : utf8_decode($inv_q->f('notes2')) ),
			'notes2'                	=> $in['c_invoice_id'] ? CREDIT_EXTRA."\n".utf8_decode($inv_q->f('notes2')) :($in['notes2']? $in['notes2'] : utf8_decode($inv_q->f('notes2')) ),
			'currency_type_list'   		=> build_currency_list($currency_type),
			'CURRENCY_TXT'		    	=> currency::get_currency($currency_type,'name'),
			'due_days2'				=> $due_days,

			'currency_type'			=> $currency_type,
			'currency_rate'			=> $currency_rate,
			'currency_val'		   	=> get_commission_type_list($currency_type),
			'total_currency_hide'	=> $currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? false : true) : false,
			'invoice_title'			=> $invoice_title,
			'quote_id'				=> $in['c_quote_id'] ? $in['c_quote_id'] : $inv_q->f('quote_id'),
			'contract_id'			=> $in['c_contract_id'] ? $in['c_contract_id'] : $inv_q->f('contract_id'),
			'no_credit_note'			=> $inv_q->f('type') == 2 ? false : $is_credit,
			//Buyer Details
			'buyer_id'                	=> ($in['c_contract_id'] && $in['is_contact']) ? '0' : ( ($in['change'] || $in['third_party_applied']) ? $in['buyer_id'] : ($in['c_contract_id'] ? $inv_q->f('customer_id') : $inv_q->f('buyer_id')) ),
			'duplicate_invoice_id'			=> $in['duplicate_invoice_id'],
			'contact_id'              	=> ($in['c_contract_id'] && $in['is_contact']) ? $in['buyer_id'] : ($in['third_party_applied'] && !$in['contact_id'] ? '0' : ($in['contact_id'] ? $in['contact_id'] : $inv_q->f('contact_id'))),
			'buyer_country_id'        	=> $in['duplicate_invoice_id'] || $in['change'] ? $buyer_details->f('country_id') : $inv_q->f('buyer_country_id'),
			'buyer_country_name'      	=> $in['duplicate_invoice_id'] || $in['change'] ? get_country_name($buyer_details->f('country_id')) : get_country_name($inv_q->f('buyer_country_id')),
			'buyer_state_id'          	=> $in['duplicate_invoice_id'] || $in['change'] ? $buyer_details->f('state_id') : $inv_q->f('buyer_state_id'),
	
			'buyer_city'              	=> $in['duplicate_invoice_id'] || $in['change'] ? $buyer_details->f('city') : $inv_q->f('buyer_city'),
			'buyer_zip'               	=> $in['duplicate_invoice_id'] || $in['change'] ? $buyer_details->f('zip') : $inv_q->f('buyer_zip'),
			'buyer_address'           	=> $in['duplicate_invoice_id'] || $in['change'] ? $buyer_details->f('address') : $inv_q->f('buyer_address'),
			'buyer_email'             	=> $in['duplicate_invoice_id'] || $in['change'] ? $c_email : $inv_q->f('buyer_email'),
			'buyer_fax'            		=> $in['duplicate_invoice_id'] || $in['change'] ? $c_fax : $inv_q->f('buyer_fax'),
			'buyer_phone'             	=> $in['duplicate_invoice_id'] || $in['change'] ? $c_phone : $inv_q->f('buyer_phone'),
			'text_name'					=> $text,
			'is_duplicate'             	=> $in['duplicate_invoice_id'] ? true : $inv_q->f('sent')==0 ? true : false,
			'our_ref'             		=> $our_ref_number ? $our_ref_number : ($in['c_invoice_id'] ? urldecode($in['a_invoice_id']) : $inv_q->gf('our_ref')),
			'your_ref'          	   	=> urldecode($inv_q->gf('your_ref')),
			'remove_vat'				=> $in['c_quote_id'] ? $in['remove_vat'] : $inv_q->f('remove_vat'),
			'field'						=> $inv_q->f('buyer_id') ? 'customer_id' : 'contact_id',
			'contact_name'       		=> $inv_q->f('contact_id') && !$inv_q->f('buyer_id')? $inv_q->f('buyer_name'): trim($contact_name->f('firstname').' '.$contact_name->f('lastname')),
			'country_dd'			=> build_country_list($inv_q->f('buyer_country_id')),
			'main_country_id'			=> @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26',

			//Seller Delivery Details
			'seller_name'            	=> $in['c_contract_id'] ? ACCOUNT_COMPANY : $inv_q->gf('seller_name'),
			'seller_d_address'    		=> $in['c_contract_id'] ? ACCOUNT_DELIVERY_ADDRESS : $inv_q->gf('seller_d_address'),
			'seller_d_zip'   		  	=> $in['c_contract_id'] ? ACCOUNT_DELIVERY_ZIP : $inv_q->gf('seller_d_zip'),
			'seller_d_city'   			=> $in['c_contract_id'] ? ACCOUNT_DELIVERY_CITY : $inv_q->gf('seller_d_city'),
			'seller_d_country_dd'	  	=> $in['c_contract_id'] ? build_country_list(ACCOUNT_DELIVERY_COUNTRY_ID) : build_country_list($inv_q->gf('seller_d_country_id')),
			'seller_d_country_id'		=> $in['c_contract_id'] ? ACCOUNT_DELIVERY_COUNTRY_ID : $inv_q->gf('seller_d_country_id'),

			'seller_b_address'    		=> $in['c_contract_id'] ? ACCOUNT_BILLING_ADDRESS : $inv_q->gf('seller_b_address'),
			'seller_b_zip'   			=> $in['c_contract_id'] ? ACCOUNT_BILLING_ZIP : $inv_q->gf('seller_b_zip'),
			'seller_b_city'   			=> $in['c_contract_id'] ? ACCOUNT_BILLING_CITY : $inv_q->gf('seller_b_city'),
			'seller_b_country_dd'		=> $in['c_contract_id'] ? build_country_list(ACCOUNT_BILLING_COUNTRY_ID) : build_country_list($inv_q->gf('seller_b_country_id')),
			'seller_b_country_id'		=> $in['c_contract_id'] ? ACCOUNT_BILLING_COUNTRY_ID :  $inv_q->f('seller_b_country_id'),
	
			'seller_bwt_nr'   		=> $in['duplicate_invoice_id'] || $in['change'] || $in['a_quote_id'] ? $buyer_info->f('btw_nr') : $inv_q->f('seller_bwt_nr'),
			'rel'					=> $rel.'.'.$in['invoice_id'],
			'hide_sync'				=> ($inv_q->f('status') == 0 && $inv_q->f('sent') == 0) ? true : false,
			'hide_vattd'			=> $inv_q->f('remove_vat') == 1 ? false : true,
			'item_width'			=> $inv_q->f('remove_vat') == 1 ? 5 : 4,
			'no_vat'				=> $inv_q->f('remove_vat') == 1 ? false : true,
			'show_vat'				=> $inv_q->f('remove_vat') == 1 ? true : false,
			'allow_stock'           => ALLOW_STOCK,
			/*'free_field'			=> $inv_q->f('free_field') ? $inv_q->f('free_field') : $free_field,
			'free_field_txt'			=> trim($inv_q->f('free_field')) ? nl2br($inv_q->f('free_field')) : nl2br($free_field),*/
			'free_field'			=> $free_field,
			'free_field_txt'		=> nl2br($free_field),
			'name_text'				=> $text,
			'disc_line'				=> display_number($inv_q->gf('discount_line_gen')),
			'vat_regime_dd'			=> build_vat_regime_dd($in['vat_regime_id']),
			'vat_regime_id'			=> $in['vat_regime_id'],
			'vatr_txt'				=> $this->db->field("SELECT value from vatregim WHERE id='r".$inv_q->gf('vat_regime_id')."' "),

    		'acc_manager_txt'    		=> $inv_q->f('acc_manager_name'),
    		'acc_manager_id'     		=> $inv_q->f('acc_manager_id'),
    		'acc_manager_name'   		=> $inv_q->f('acc_manager_name'),
			'multiple_identity_txt' 	=> $this->db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$inv_q->gf('identity_id')."'"),
			'multiple_identity_dd' 		=> build_identity_dd($inv_q->gf('identity_id')),
			'identity_id'			=> $inv_q->gf('identity_id'),
			'main_comp_info'		=> $this->getInvoiceIdentity($inv_q->gf('identity_id')),
			'existIdentity'			=> $existIdentity > 0 ? true : false,
			'customer_mandat'		=> $customer_mandat ? true : false,
			'customer_mandat_active'		=> $customer_mandat_active ? true : false,
			'sepa_mandate_txt' 		=> $this->db->field("SELECT sepa_number from mandate WHERE mandate_id='".$inv_q->gf('mandate_id')."'"),
			'sepa_mandate_dd' 		=> build_sepa_mandate_dd($inv_q->gf('mandate_id'), $inv_q->gf('buyer_id'), true),
			'sepa_mandates'			=> $inv_q->gf('mandate_id') ? $inv_q->gf('mandate_id') : $customer_mandat_active,
			'existIdentity'			=> $existIdentity > 0 ? true : false,
			'attention_of'          => $inv_q->f('attention_of'),
			'discount_line_gen'		=> display_number($inv_q->gf('discount_line_gen')),
			'sameAddress'			=> !$inv_q->f('same_address') ? true : false,
			'delivery_address_id'	=> $inv_q->f('same_address'),
			'email_language' 		=> $inv_q->f('email_language'),
			'can_change'			=> $to_edit,
			'segment_dd'			=> get_categorisation_segment(),
			'source_dd'				=> get_categorisation_source(),
       		'type_dd'				=> get_categorisation_type(),
       		'source_id'				=> $inv_q->f('source_id'),
        	'type_id'				=> $inv_q->f('type_id'),
        	'segment_id'			=> $inv_q->f('segment_id'),
        	'c_quote_id'			=>$in['c_quote_id'],
        	'stripe_active'			=> $stripe_active? 1: 0,
        	'payment_methods'       => build_payment_methods_list($inv_q->f('payment_method')),
			'payment_method'		=> $inv_q->f('payment_method'),
			'allow_sepa'			=> $allow_sepa,
			'price_category_dd' 	=> get_price_categories(),
			'cat_id'            	=> $inv_q->f('cat_id'),
			'title_dd'				=> build_contact_title_type_dd(0),
			'title_id'				=> '0',
			'copy_free_text'		=> $in['copy_free_text'] ? '1' : '',
			'customer_notes'		=> $customer_notes,
			'subject'				=> stripslashes($inv_q->gf('subject')),
			'customer_credit_limit'	=> $customer_credit_limit,
			'has_third_party'			=> $has_third_party,
			'third_party_id'			=> $third_party_id,
			'third_party_name'			=> $third_party_name,
			'third_party_address'		=> $third_party_address,
			'third_party_regime_id'		=> $third_party_regime_id,
			'third_party_applied'		=> $in['third_party_applied'] ? '1' : '',
		);

		$has_admin_rights 					= getHasAdminRights(array('module'=>'invoice'));
		$output['hide_margin']				= defined('HIDE_PROFIT_MARGIN') && HIDE_PROFIT_MARGIN == 1 && !$has_admin_rights ? true : false;

		$yuki_active = $this->db->field("SELECT active FROM apps WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		$output['yuki_project_enabled']				= defined('YUKI_PROJECTS') && YUKI_PROJECTS == 1 && $yuki_active ? true : false;
		$output['yuki_projects']				= get_yuki_projects();
		$output['yuki_project_id']				=  $inv_q->f('yuki_project_id');

		if($in['vat_regime_id'] && $in['vat_regime_id']>=1000){
			$extra_eu=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
			if($extra_eu==2){
				$output['block_vat']=true;
			}
			if($extra_eu=='1'){
				$output['vat_regular']=true;
			}
			$contracting_partner=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
			if($contracting_partner==4){
				$output['block_vat']=true;
			}
		}
		if($inv_q->f('type')=='0' || $inv_q->f('type')=='3'){
			$output['deals']= $this->get_deals($in);
		}	
		if($inv_q->f('trace_id')){
			$linked_doc=$this->db->field("SELECT tracking_line.origin_id FROM tracking_line
				INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
				WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$inv_q->f('trace_id')."' ");
			if($linked_doc){
				$output['deal_id']=$linked_doc;	
			}
		}

     	$in['discount_line_gen']= display_number($inv_q->gf('discount_line_gen'));

		if($in['c_contract_id']){
	            $output['invoice_date']        = time()*1000;
		}

		if($in['c_invoice_id'] || $in['duplicate_invoice_id'] || $in['a_quote_id'] || $in['a_contract_id']){
			$output['serial_number']=$in['serial_number'];
			/*if(DRAFT_INVOICE_NO_NUMBER==1){
				$output['serial_number'] 	 = ' ';
			}else{
				$output['serial_number'] 	 = ($in['type'] == '2' || $in['type'] == '4') ? generate_credit_invoice_number() : generate_invoice_number(DATABASE_NAME);
			}*/
			
		}else{
			$output['serial_number'] 	 = $inv_q->f('serial_number');
		}
		if($in['c_quote_id']){
			if($in['type']==3 || $in['type'] == '4'){
				$extend='LIMIT 0';
			}
			$quote_line_type_sql=$in['copy_free_text']=='1' ? "('1','2','3','4','5')" : "('1','2','4','5')";
			$line = $this->db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
							   INNER JOIN tblquote_version ON tblquote_line.version_id=tblquote_version.version_id

							   WHERE tblquote_line.quote_id='".$in['c_quote_id']."' AND tblquote_line.content_type='1' AND tblquote_version.active='1'
							   AND line_type IN ".$quote_line_type_sql."
							   ORDER BY id ASC $extend ");


		}elseif($in['c_contract_id']){
	        $line = $this->db->query("SELECT contract_line.*,contract_line.discount as line_discount FROM contract_line

							    WHERE contract_line.contract_id='".$in['c_contract_id']."' AND use_recurring = '0' AND version_id='".$inv_q->f('version_id')."' AND contract_line.content_type='1' AND line_type IN ('1','2','3','4','5') ORDER BY id ASC ");
		}
		else{
			$line = $this->db->query("SELECT * FROM ".$table."_line WHERE ".$field."_id='".$invoice_id."' ORDER BY sort_order ASC");

		}

		$i=0;
		$line_total=0;
		$subtotal=0;
		$vat_value = 0;
		$vat_percent = array();
		$sub_t = array();
		$sub_discount = array();
		$vat_percent_val = 0;
		$discount_value = 0;
		$netto_amount=0;
		$groups=array();
		$servgroup=array();

		$output['generalvats']=build_vat_dd();
		$output['invoice_line']=array();
		$output['vat_line']=array();
		$code_width=0;
		$last_tr = '';
		$last_combined = '';
		
		if($in['downpayment']){
			while ($line->next()){
				$vat_line = $line->f('vat');
				$cols=7;
				if(!$in['apply_discount'] || $in['apply_discount'] == 2 ){
					$cols=6;
					if($in['remove_vat'] == 1){
						$cols = 5;
					}
				}
				$row_id = 'tmp'.$i;

				if($in['c_quote_id'] || $in['c_contract_id']){
					if(!$line->f('is_tax')){
						$last_tr = $row_id;
					}
				} elseif (!$line->f('tax_id')){
					$last_tr = $row_id;
				}
			 	if($in['c_quote_id']){			 		
        				if($line->f('group_id') && !in_array($line->f('group_id'),$groups) ) {
        					$invoice_line=array();
             				$group_line= $this->db->query("SELECT tblquote_group.title FROM tblquote_group WHERE group_id='".$line->f('group_id')."'");
        	  				if($group_line->f('title')){
							$invoice_line['tr_id']         	= $row_id;
							$invoice_line['description']  	= stripslashes(htmlspecialchars_decode($group_line->f('title'),ENT_QUOTES)); 
							$invoice_line['content']            = true;
							$invoice_line['is_article']      	= false;
							$invoice_line['content']		= stripslashes(htmlspecialchars_decode($group_line->f('title'),ENT_QUOTES)); 
			            		$invoice_line['content_class']	= 'type_content';
			            		$invoice_line['colspan']		= 'colspan='.$cols;
			            		$invoice_line['origin_id']		= $in['c_quote_id'];
          						$invoice_line['origin_type']		='2';
			            		if($in['type']==2 || $in['type'] == '4'){
								$invoice_line['quantity']	= display_number($this->negative*$line->f('quantity'));
							}

			            		array_push($output['invoice_line'], $invoice_line);

                  				$i++;
                  				array_push($groups, $line->f('group_id'));
        	   				}
        				}
         				if($line->f('service_bloc_title') && !in_array($line->f('table_id'),$servgroup)){
						$invoice_line=array();
						$invoice_line['tr_id']         		= $row_id;
						$invoice_line['description']  		= stripslashes(htmlspecialchars_decode($line->f('service_bloc_title'),ENT_QUOTES)); 
						$invoice_line['content']            = $group_line->f('line_type') =='4' || $group_line->f('line_type') =='5' ? true : false;
						$invoice_line['title']              = $group_line->f('line_type') =='4' || $group_line->f('line_type') =='5' ? ($group_line->f('line_type') =='4' ? '1' : '2' ) : null;
						$invoice_line['is_article']      	    	= false;
						// $invoice_line['content']			= html_entity_decode($line->f('service_bloc_title')); 
			            	$invoice_line['content_class']		= 'type_content';
			            	$invoice_line['colspan']			= 'colspan='.$cols;
			            	$invoice_line['origin_id']		= $in['c_quote_id'];
          					$invoice_line['origin_type']		='2';
			            	if($in['type']==2 || $in['type'] == '4' ){
							$invoice_line['quantity']		= display_number($this->negative*$line->f('quantity'));
						}
						
			            	array_push($output['invoice_line'], $invoice_line);

                  			$i++;
                    			array_push($servgroup, $line->f('table_id'));
        				}

    				}
				$price =  $line->f('price');
				if($in['c_quote_id']){
					if( !$line->f('article_id') && !$vat_line && ($vat_line != $vat) ){
						$vat_line = $vat;
					}
				}			

				$line_discount = $line->f('discount');
				if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$line_discount = 0;
				}
				if($in['c_quote_id'] ){
					$line_total2=($line->f('quantity')*($line->f('package')/$line->f('sale_unit')) *  ($price-($price*$line->f('line_discount')/100)));
				}else{
					$q = $line->f('quantity');
					if($in['type']==2 || $in['type'] == 4){
						$q = $this->negative*$line->f('quantity');
					}
					$line_total2 = $q * ( $price - ( $price * $line_discount / 100 ) );
				}
				$line_total2 = round($line_total2,ARTICLE_PRICE_COMMA_DIGITS);
				$amount_d = $line_total2 * $discount/100;

				$subtotal +=$line_total2;

				$vat_line = display_number($vat_line);
				$vat_percent_val = ( $line_total2 - $amount_d ) * $vat_line/100;
				$vat_percent[1] = $vat_percent_val;
				$sub_t[1] += round( $line_total2,ARTICLE_PRICE_COMMA_DIGITS );
				$sub_discount[1] += $amount_d;
				$i++;
				$discount_value	+= $amount_d;
				$netto_amount+=($line_total2-$amount_d);
			}
			$downpayment_percent=$this->db->field("SELECT downpayment_val FROM tblquote WHERE id='".$in['c_quote_id']."' ");
			$new_price=($subtotal-$discount_value)*$downpayment_percent/100;

			$o=1;
			while($o<2){
				$line_item_width=$item_width;
				if(!$in['apply_discount'] || $in['apply_discount'] == 2){
					$line_item_width+=1;
				}
				if(!$line->f('article_code') && !$line->f('item_code')){
					$line_item_width+=2;
				}

				$invoice_line=array(
					'tr_id'         		=> $row_id,
					'description'  			=> gm('Downpayment for quote nr').' '.$in['a_quote_id'],
					'unitmeasure'  			=> $line->f('unitmeasure'),
					'quantity'      		=> display_number(1) ,
					'quantity_old'      		=> display_number(1) ,
					'price'         		=> display_number_var_dec($new_price),
					'price_vat'				=> display_number_var_dec($new_price),
					'vat'					=> display_number(0),
					'discount_line'			=> display_number(0),
					'title'					=> $in['c_quote_id'] && ($line->f('line_type') =='4' || $line->f('line_type') =='5') ? ($line->f('line_type') =='4' ? '1' : '2' ) : $line->f('content_title'),
					'content'				=> $in['c_quote_id'] ? ($line->f('line_type') == 4  || $line->f('line_type') ==5 ? true : false) : ( $line->f('content') ? true : false ),
					'content_class'			=> $line->f('content') ? ' type_content ' : '',
					'colspan'				=> $line->f('content') ? ' colspan='.$cols.'' : '',
					'unmodifiable'			=> $in['c_quote_id'] ? ($line->f('article_id') ? 'readonly' : '') : ($line->f('readonly') ? 'readonly':''),
					'article_code'			=> ($in['c_quote_id'] || $in['c_contract_id']) ? stripslashes($line->f('article_code')) : stripslashes($line->f('item_code')),
					'show_for_articles'		=> ($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('article_code') ? true : false ) : ($line->f('item_code') ? true : false),
					'width_if_article_code' => (($in['c_quote_id'] || $in['c_contract_id']) && $line->f('article_code')) || $line->f('item_code') ? 'width:155px;' : 'width:270px;',
				    'is_article'      	    => $line->f('article_id')? true : false,
				    'is_profi'      	    => (USE_PROFI==1 && $line->f('article_id'))? true : false,
				    //'tax_for_article_id'	=> $line->f('tax_for_article_id'),
					//'is_article'      	=> $line->f('article_id')? true : false,
					//'is_tax'      	    => $line->f('tax_id')? true : false,
					'tax_for_article_id'	=>$in['c_quote_id']? ($this->db->field("SELECT article_id FROM  tblquote_line WHERE id='".$line->f('tax_for_line_id')." ' ") ) : ($line->f('tax_for_article_id')),
					'is_article'      	    => ($in['c_quote_id'] || $in['c_contract_id']) ? (($line->f('article_id') && !$line->f('is_tax') ) ? true : false) : ($line->f('article_id') ? true : false),
					'is_tax'      	        =>($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('is_tax')? true : false): ($line->f('tax_id')? true : false),
					'for_article'			=> ($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('is_tax')? true : false): ($line->f('tax_id')? true : false)? $last_tr:'',
					'line_total'   			=> display_number($new_price),
					'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
					'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
					'item_width'			=> $line_item_width,
					'origin_id'			=> $in['c_quote_id'],
          			'origin_type'		=> '2',
          			
				);
				$code_width=($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('article_code') ? $code_width+1 : $code_width+0 ) : ($line->f('item_code') ? $code_width+1 : $code_width+0);
				array_push($output['invoice_line'], $invoice_line);
				$o++;
			}

			$b=1;
		
			while($b<2){
				$vat_line=array(
					'vat_percent'   			=> display_number(0),
					'vat_value'	   			=> display_number(0),
					'total_novat'	   	 	=> display_number($new_price),
					//'discount_value'			=> display_number($sub_discount[$key]),
					'is_ord'				=> $is_ord,
					'view_discount'			=> false,
					'net_amount'			=> display_number($new_price),
				);
				array_push($output['vat_line'], $vat_line);
				$b++;
			}

			$output['discount_value'] 	= display_number($discount_value);
			$output['total']            	= $new_price ? display_number($new_price): '0.00';
			$output['vatx_txt']		= $inv_q->f('remove_vat') == 1 ? '-' : display_number(0).' %';
			$output['vat']			= display_number(0);
			$output['net_amount']		= display_number($netto_amount);
			$output['total_wo_vat']		= display_number($subtotal);

		}else{

			while ($line->next()){
				$vat_line = $line->f('vat');
				$cols=7;
				if(!$in['apply_discount'] || $in['apply_discount'] == 2 ){
					$cols=6;
					if($in['remove_vat'] == 1){
						$cols = 5;
					}
				}
				$row_id = 'tmp'.$i;
				if($in['c_quote_id']){
						if($in['third_party_applied']){
							$vat_line = $vat;
						}
        				if($line->f('group_id') && !in_array($line->f('group_id'),$groups) ) {
        					
        					$invoice_line=array();
             				$group_line= $this->db->query("SELECT tblquote_group.title FROM tblquote_group WHERE group_id='".$line->f('group_id')."'");
				        	if($group_line->f('title')){

							$invoice_line['tr_id']         	= $row_id;
							$invoice_line['description']  	= stripslashes(htmlspecialchars_decode($group_line->f('title'),ENT_QUOTES)); 
							//$invoice_line['content']            = $group_line->f('line_type') =='4' || $group_line->f('line_type') =='5' ? true : false;
							$invoice_line['content']            = 1;
							//$invoice_line['title']              = $group_line->f('line_type') =='4' || $group_line->f('line_type') =='5' ? ($group_line->f('line_type') =='4' ? '1' : '2' ) : null;
							$invoice_line['title']            = 1;
							$invoice_line['is_article']      	= false;
							// $invoice_line['content']		= html_entity_decode($group_line->f('title')); 
							$invoice_line['content_class']	= 'type_content';
							$invoice_line['colspan']		= 'colspan='.$cols;
							$invoice_line['origin_id']		= $in['c_quote_id'];
          						$invoice_line['origin_type']		='2';
							array_push($output['invoice_line'], $invoice_line);

				                  $i++;
				                  array_push($groups, $line->f('group_id'));
				        	}


        				}
         				if($line->f('service_bloc_title') && !in_array($line->f('table_id'),$servgroup)){
                 				$invoice_line=array(
							'tr_id'         	=> $row_id,
							'description'  		=> stripslashes(htmlspecialchars_decode($line->f('service_bloc_title'),ENT_QUOTES)), 
							'content'           => $group_line->f('line_type') =='4' || $group_line->f('line_type') =='5' ? true : false,
							'title'             => $group_line->f('line_type') =='4' || $group_line->f('line_type') =='5' ? ($group_line->f('line_type') =='4' ? '1' : '2' ) : null,
							'is_article'      	=> false,
							'content'			=> stripslashes(htmlspecialchars_decode($line->f('service_bloc_title'),ENT_QUOTES)), 
			           	 	'content_class'		=> 'type_content',
			            	'colspan'			=> 'colspan='.$cols,
			            	'origin_id'			=> $in['c_quote_id'],
          					'origin_type'		=> '2',
						);
                 				
                 				array_push($output['invoice_line'], $invoice_line);
                  			$i++;
                    			array_push($servgroup, $line->f('table_id'));
        				}

    				}
				$price =  $line->f('price');
				if($in['c_quote_id']){
					if( !$line->f('article_id') && !$vat_line && ($vat_line != $vat) ){
						  //$vat_line = $vat;
					}
				}
				if($in['c_contract_id']){
					if( !$line->f('article_id') && !$vat_line && ($vat_line != $vat) ){
						$vat_line = $vat;
					}
				}

				$stock=$this->db->field("SELECT stock from pim_articles WHERE article_id='".$line->f('article_id')."' ");
		    		$threshold_value=$this->db->field("SELECT article_threshold_value from pim_articles WHERE article_id='".$line->f('article_id')."' ");
		 		$pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$line->f('article_id')."' AND delivered=1");

		 		$line_item_width=$item_width;
				if(!$in['apply_discount'] || $in['apply_discount'] == 2){
					$line_item_width+=1;
				}
				if(!$line->f('article_code') && !$line->f('item_code')){
					$line_item_width+=2;
				}

				if($in['c_quote_id'] || $in['c_contract_id']){
					if(!$line->f('is_tax')){
						$last_tr = $row_id;
					}
				} elseif (!$line->f('tax_id')){
					$last_tr = $row_id;
				}

				if ($line->f('is_combined')){
					$last_combined = $row_id;
				}

				if ($line->f('has_variants')){
					$last_variant_parent = $row_id;
				}

				$quantity_component =0;
				$parent_article_id =0;
				if($line->f('component_for')){
					if($in['c_quote_id']){
						$parent_article_id = $this->db->field("SELECT article_id FROM tblquote_line where id ='".$line->f('component_for')."' ");
					}elseif(!$in['c_contract_id']){
						$parent_article_id = $this->db->field("SELECT article_id FROM tblinvoice_line where id ='".$line->f('component_for')."' ");
					}
				    		
				    $quantity_component = get_quantity_component($parent_article_id, $line->f('article_id'));
				}

				$nr_dec_q = strlen(rtrim(substr(strrchr($line->f('quantity'), "."), 1),'0'));
				$nr_taxes = $this->db->field("SELECT COUNT(article_tax_id) FROM pim_articles_taxes WHERE article_id='".$line->f('article_id')."' ");

				$invoice_line=array(
					'tr_id'         			=> $row_id,
					'description'  			=> (NOT_COPY_ARTICLE_INV==1 && $line->f('article_id') && !$in['invoice_id'] && !$in['duplicate_invoice_id']) ? get_article_label($line->f('article_id'),$in['languages'],'invoice',$line->f('is_tax')): ($line->f('content') ? html_entity_decode($line->f('name')) : ($in['c_quote_id'] && $line->f('line_type') == 3 ? html_entity_decode($line->f('name')) : stripslashes(htmlspecialchars_decode($line->f('name'),ENT_QUOTES)))), 

					'unitmeasure'  			=> $line->f('unitmeasure'),
					'quantity'      			=> ($in['c_quote_id'] || $in['c_contract_id']) ? display_number($line->f('quantity')*($line->f('package')/$line->f('sale_unit')),$nr_dec_q) : display_number($line->f('quantity'),$nr_dec_q) ,
					'quantity_old'      	=> ($in['c_quote_id'] || $in['c_contract_id']) ? display_number($line->f('quantity')*($line->f('package')/$line->f('sale_unit')),$nr_dec_q) : display_number($line->f('quantity'),$nr_dec_q) ,
					'quantity_component'      => ($in['c_quote_id'] || $in['c_contract_id']) ? display_number($quantity_component*($line->f('package')/$line->f('sale_unit')),$nr_dec_q) : display_number($quantity_component,$nr_dec_q) ,
					'price'         			=> display_number_var_dec($price),
					'price_vat'				=> display_number_var_dec($price+($price*$vat_line/100)),
					'purchase_price'         	=>  display_number_var_dec($line->f('purchase_price')),
					'vat'					=> ($in['c_quote_id'] || $in['c_contract_id']) ? display_number($vat_line) : display_number($line->f('vat')),
					'discount_line'			=> ($in['c_quote_id'] || $in['c_contract_id'])  ? display_number($line->f('line_discount')) : display_number($line->f('discount')),

					'readonly'				=> ($in['c_quote_id'] || $in['c_contract_id'])  ? ($line->f('article_id') ? 1 : 0) : $line->f('readonly'),
					'article_id'			=> $line->f('article_id'),
					'stock'				=> $stock,
					'pending_articles'          	=> intval($pending_articles),
					'threshold_value'           	=> intval($threshold_value),
					'title'					=> ($in['c_quote_id'] || $in['c_contract_id']) && ($line->f('line_type') == 3 || $line->f('line_type') =='4' || $line->f('line_type') =='5') ? ($line->f('line_type') == 3 ? 0 : ($line->f('line_type') =='4' ? '1' : '2')) : $line->f('content_title'),
					'content'				=> ($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('line_type') == 3 || $line->f('line_type') == 4  || $line->f('line_type') ==5 ? true : false) : ( $line->f('content') ? true : false ),
					'content_class'			=> $line->f('content') ? ' type_content ' : '',
					'colspan'				=> $line->f('content') ? ' colspan='.$cols.'' : '',
					'unmodifiable'			=> $in['c_quote_id'] ? ($line->f('article_id') ? 'readonly' : '') : ($line->f('readonly') ? 'readonly':''),
					'article_code'			=> ($in['c_quote_id'] || $in['c_contract_id']) ? stripslashes($line->f('article_code')) : stripslashes($line->f('item_code')),
					'show_for_articles'		=> ($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('article_code') ? true : false ) : ($line->f('item_code') ? true : false),
					'width_if_article_code' 	=> (($in['c_quote_id'] || $in['c_contract_id']) && $line->f('article_code')) || $line->f('item_code') ? 'width:155px;' : 'width:270px;',
				   	'is_article'      	      => $line->f('article_id')? true : false,
			    	'is_profi'      	            => (USE_PROFI==1 && $line->f('article_id'))? true : false,
			    	'tax_id'			=> ($in['c_quote_id'] || $in['c_contract_id'])? ($line->f('is_tax')? $line->f('article_id') : 0 ):$line->f('tax_id'),
			    	'tax_for_article_id'	    	=>$in['c_quote_id']? ($this->db->field("SELECT article_id FROM  tblquote_line WHERE id='".$line->f('tax_for_line_id')." ' ") ) : ($line->f('tax_for_article_id')),
					'is_article'      	      => ($in['c_quote_id'] || $in['c_contract_id']) ? (($line->f('article_id') && !$line->f('is_tax') ) ? true : false) : ($line->f('article_id') ? true : false),
					'is_tax'      	            =>($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('is_tax')? true : false): ($line->f('tax_id')? true : false),
					'for_article'				=> ($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('is_tax')? true : false): ($line->f('tax_id')? true : false)? $last_tr:'',
					'item_width'			=> $line_item_width,
					'origin_id'			=> $in['c_invoice_id'] ? $in['c_invoice_id'] : ($in['c_quote_id'] ? $in['c_quote_id'] : ($in['c_contract_id'] ? $in['c_contract_id'] : '')),
          				'origin_type'		=> $in['c_invoice_id'] ? ($in['track_base']=='1' ? '7' : '1') : ($in['c_quote_id'] ? '2' : ($in['c_contract_id'] ? '9' : '')),
          			'has_variants' 			=> $line->f('has_variants'),
				 	'has_variants_done' 	=> $line->f('has_variants_done'),
				 	'is_variant_for' 		=> $line->f('is_variant_for'),
				 	'is_variant_for_line' 	=> $line->f('is_variant_for')? $last_variant_parent:'',
				 	'variant_type' 			=> $line->f('variant_type'),
				 	'is_combined' 			=> $line->f('is_combined'),
				 	'component_for' 		=> $line->f('component_for')?$last_combined :'',
				 	'visible' 				=> $line->f('visible'),
				 	'has_taxes'				=> $nr_taxes? true : false,
				);
				$code_width=($in['c_quote_id'] || $in['c_contract_id']) ? ($line->f('article_code') ? $code_width+1 : $code_width+0 ) : ($line->f('item_code') ? $code_width+1 : $code_width+0);
				if($in['type']==2 || $in['type'] == 4){
					$invoice_line['quantity']     = display_number($this->negative*$line->f('quantity'));
				}
				$line_discount = $line->f('discount');
				if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$line_discount = 0;
				}
				if($in['c_quote_id'] ){
					$line_total2=($line->f('quantity')*($line->f('package')/$line->f('sale_unit')) *  ($price-($price*$line->f('line_discount')/100)));
				}else{
					$q = $line->f('quantity');
					if($in['type']==2 || $in['type'] == 4){
						$q = $this->negative*$line->f('quantity');
					}
					$line_total2 = $q * ( $price - ( $price * $line_discount / 100 ) );
				}
				$line_total2 = round($line_total2,ARTICLE_PRICE_COMMA_DIGITS);
				$invoice_line['line_total']   		= display_number($line_total2);
				$invoice_line['hide_currency2']		= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
				$invoice_line['hide_currency1']		= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;

				array_push($output['invoice_line'], $invoice_line);

				$amount_d = $line_total2 * $discount/100;

				$subtotal +=$line_total2;
				$vat_line = display_number($vat_line);
				$vat_percent_val = ( $line_total2 - $amount_d ) * $vat_line/100;
				if($line->f('content') != 1){
					$vat_percent[$vat_line] += $vat_percent_val;
				}				
				$sub_t[$vat_line] += $line_total2;
				$sub_discount[$vat_line] += $amount_d;
				$i++;
				$discount_value	+= $amount_d;
				$netto_amount+=($line_total2-$amount_d);
			}
			if($in['c_quote_id']){
				$downpayment_made=$this->db->field("SELECT downpayment FROM tblquote WHERE id='".$in['c_quote_id']."' ");
				$downpayment_percent=$this->db->field("SELECT downpayment_val FROM tblquote WHERE id='".$in['c_quote_id']."' ");
				$downpayment_val=($subtotal-$discount_value)*$downpayment_percent/100;
			}
			if($inv_q->f('quote_id')){
				$downpayment_val=$this->db->field("SELECT downpayment_value FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
			}
			$t=0;
			$output['hide_total'] = true;
			foreach ($vat_percent as $key => $val){
               if($key !='0,00') {
                    //if($sub_t[$key] - $sub_discount[$key]){

                    //$val = $sub_t[$key]*return_value($key)/100;
                    $total_vat += $val;
                    $vat_line = array(
                        'vat_percent' => display_number($key),
                        'vat_value' => display_number(round($val, ARTICLE_PRICE_COMMA_DIGITS)),
                        'subtotal' => display_number(round($sub_t[$key], ARTICLE_PRICE_COMMA_DIGITS)),
                        'total_novat' => display_number(round($sub_t[$key], ARTICLE_PRICE_COMMA_DIGITS)),
                        'discount_value' => display_number($sub_discount[$key]),
                        'is_ord' => $is_ord,
                        'view_discount' => ($discount || $is_ord) ? true : false,
                        'net_amount' => display_number($sub_t[$key] - $sub_discount[$key]),
                    );
                    array_push($output['vat_line'], $vat_line);
                    //}
                }
			 	$t++;
			}

			if($t==1){
				$output['hide_total'] = false;
			}

			$subtotal=round($subtotal,2);
			$total_vat=round($total_vat,2);
		//assign the total information
			if(($in['c_quote_id'] && $downpayment_made==1) || ($inv_q->f('quote_id')&&$inv_q->f('downpayment_drawn'))){
				$total=($subtotal - $discount_value)+$total_vat - $downpayment_val;
			}else{
				$total=($subtotal - $discount_value) +$total_vat;
			}

			$total_default = $total*return_value($output['currency_rate']) ;

			$output['discount_value'] 	= display_number($discount_value);
			$output['total']            	= $total ? display_number($total): '0.00';
			$output['total_default_currency']            	= $total_default ? display_number($total_default): '0.00';
			$output['hide_downpayment']	= (($in['c_quote_id'] && $downpayment_made==1)||($inv_q->f('quote_id')&&$inv_q->f('downpayment_drawn'))) ? true : false;
			$output['total_downpayment']	= (($in['c_quote_id'] && $downpayment_made==1)||($inv_q->f('quote_id')&&$inv_q->f('downpayment_drawn'))) ? display_number($downpayment_val) : '';
			$output['downpayment_value']	= (($in['c_quote_id'] && $downpayment_made==1)||($inv_q->f('quote_id')&&$inv_q->f('downpayment_drawn'))) ? $downpayment_val : 0;
			$output['net_amount']		= display_number($netto_amount);
			$output['total_wo_vat']		= display_number($subtotal);
		}

		if($discount || $is_ord){
			$output['view_discount1']	= true;
		}else{
			$output['view_discount1']	= false;
		}

		$req_payment_value=$total * $in['req_payment']/100;
    		$output['req_payment_value']		= $req_payment_value ? display_number($req_payment_value):display_number(0);

		if($req_payment!=100 && $inv_q->gf('type')){
			$output['view_req_payment']	= true;
		}else{
			$output['view_req_payment']	= false;
		}
		if($inv_q->f('c_invoice_id')){
			$in['sc_invoice_id'] = $inv_q->f('c_invoice_id');
		}

		if($in['c_quote_id'] && $downpayment_made==1){
			$in['downpayment']=2;
		}
		if($inv_q){
			if($inv_q->f('quote_id') && $inv_q->f('downpayment_drawn')){
				$in['downpayment']=1;
			}
		}
		//console::log($output['NOTES']);
		$data_last=$this->get_invoiceLast($in,$page_title,$do_next,$customer_id,$item_width,$code_width,$type_title,$nr_status);
		foreach($data_last as $key => $value){
			$output[$key]=$value;
		}
		if($in['c_quote_id'] && $in['a_quote_id']){
			$output['email_language']=$in['email_language'];
		}
		//console::log($output['NOTES']);
	     	
		$this->out = $output;
	}

	private function get_invoiceLast($in,$page_title,$do_next,$customer_id,$item_width,$code_width,$type_title,$nr_status){
        $array=array();
		$category=$this->db->query("SELECT customers.cat_id FROM customers WHERE customer_id = '".$customer_id."'");
		$category->move_next();
		if (!$category->f('cat_id'))
		{
			$in['cat_id']	= 1;
		}
		else
		{
			$in['cat_id']	= $category->f('cat_id');
		}

		$array['downpayment']				= $in['downpayment'];
		$array['page_title'] 				= $page_title;
		$array['type_title'] 				= $type_title;
		$array['nr_status'] 				= $nr_status;
		$array['invoice_id']   				= $in['invoice_id'];
		$array['c_invoice_id'] 				= $in['c_invoice_id'] ? $in['c_invoice_id'] : (isset($in['sc_invoice_id']) ? $in['sc_invoice_id'] : '');
		$array['do_next']					= $do_next;
		$array['acc_vat']	 				= get_customer_vat($customer_id);
		$array['style']     				= ACCOUNT_NUMBER_FORMAT;
		$array['pick_date_format']			= pick_date_format();
		$array['hide_currency2']			= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
		$array['hide_currency1']			= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
		$array['default_price_val']			= display_number_var_dec(0);
		$array['default_quantity_val']		= display_number(1);
		$array['default_currency']			= ACCOUNT_CURRENCY_TYPE;
		$array['default_currency_name']		= build_currency_name_list(ACCOUNT_CURRENCY_TYPE);
		$array['default_currency_val']		= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
		//$array['total_currency_hide']		= false;
		$array['apply_discount']			= $in['apply_discount'];
		$array['APPLY_DISCOUNT_LIST']      	= ($in['c_quote_id']&&$in['downpayment']) ? build_apply_discount_list(0) : build_apply_discount_list($in['apply_discount']);
		$array['APPLY_DISCOUNT_LIST_TXT']	= ($in['c_quote_id']&&$in['downpayment']) ? gm('No discount') : build_apply_discount_list($in['apply_discount'],true);
		$array['hide_global_discount']		= ($in['c_quote_id']&&$in['downpayment']) ? false : ($in['apply_discount'] < 2 ? false : true);
		$array['hide_line_global_discount']	= $in['apply_discount'] == 1 ? true : false;
		$array['hide_discount_line']		= ($in['apply_discount'] == 1 || $in['apply_discount'] == 3) ? true : false;
		$array['accmanager']				= $this->get_accmanager($in);
		$array['cc']						= $this->get_cc($in);
		$array['contacts']					= $this->get_contacts($in);
		$array['addresses']					= $this->get_addresses($in);
		$array['code_width']				= $code_width==0 ? false : true;
		$array['articles_list']				= $this->get_articles_list($in);
		$array['is_profi']					= USE_PROFI;
		$array['price_category_id']			= $in['cat_id'];
		$array['invoices_list']				= $this->get_invoices_list($in);
		$array['payment_term_type_dd']		= get_payment_term_type_dd();
		
		$glob_width=$item_width;
		if(!$in['apply_discount'] || $in['apply_discount'] == 2){
			$array['item_width']			= $item_width+1;
			$glob_width+=1;
		}
		if($code_width==0){
			$array['item_width']			= $item_width+2;
			$glob_width+=2;
		}
		$array['item_width']=$glob_width;
		//$transl_lang_id_active 		= $in['languages'];
		if(!$transl_lang_id_active){
			$transl_lang_id_active = $in['email_language'];
		}

		$langs = $this->db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");

        if ($in['type'] == 2) {
            $gen_notes_settings = $this->db->field("SELECT value FROM settings WHERE constant_name='CREDIT_NOTES_AND_GENERAL_CONDITIONS'");
        } else {
            $gen_notes_settings = $this->db->field("SELECT value FROM settings WHERE constant_name='INVOICE_NOTES_AND_GENERAL_CONDITIONS'");
        }

		if($in['invoice_id'] == 'tmp'){
            if($transl_lang_id_active==1){
                $note_types = $in['type'] == 2 ? 'credit_terms' : 'invoice_terms';
                $note2_types = $in['type'] == 2 ? 'credit_note' : 'invoice_note';
            }else{
                $note_types = $in['type'] == 2 ? 'credit_terms_'. $transl_lang_id_active : 'invoice_terms_'.$transl_lang_id_active;
                $note2_types = $in['type'] == 2 ? 'credit_note_'.$transl_lang_id_active : 'invoice_note_'.$transl_lang_id_active;
            }

            if ($in['type'] == 2) {
                $invoice_general_note = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'credit_terms'
									   AND type = '".$note_types."' ");
            } else {
                $invoice_general_note = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'invoice_terms'
									   AND type = '".$note_types."' ");
            }

            $invoice_note2 = $this->db->field("SELECT value FROM default_data
									   WHERE type = '".$note2_types."' ");
			
			$array['NOTES']=$invoice_general_note;
			$array['notes2']= $invoice_note2;

			if($in['orders_id'] && $gen_notes_settings){
				$terms = '';
				if($transl_lang_id_active!=1){		
					$terms = '_'.$transl_lang_id_active;
				}

				$order_note = $this->db->field("SELECT item_value FROM note_fields
								 WHERE item_id= '".$in['orders_id']."' AND lang_id ='".$transl_lang_id_active."' AND item_type='order' AND item_name='notes' AND active='1'");

				$array['NOTES'] = '';
				$array['notes2']= $order_note;
			}

			if($in['project_id'] && $gen_notes_settings){
				$project_note = $this->db->field("SELECT notes FROM projects
								 WHERE project_id = '".$in['project_id']."'");

				$array['NOTES'] = '';
				$array['notes2'] = addslashes($project_note);
			}

			if($in['service_id'] && $gen_notes_settings){
				$array['NOTES'] = '';
				$array['notes2'] = '';
			}

			if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
				$acc_langs=array('en','fr','nl','de');
				$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
				if($lang_c){
					if($lang_c=='du'){
						$lang_c='nl';
					}
					if(in_array($lang_c, $acc_langs)){
						$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
						if($in['vat_notes']){
							if($array['notes2']){
								$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
							}else{
								$array['notes2']=$in['vat_notes'];
							}
						}
					}
				}		
			}		
		}else{
			if($in['c_quote_id']){
				if($transl_lang_id_active==1){
					$note_types='quote_terms';
					$note2_types = 'quote_note';
				}else{
					$note_types='quote_terms_'.$transl_lang_id_active.'';
					$note2_types = 'quote_note_'.$in['email_language'];
				}
				$invoice_general_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name='quote_term'
										   AND type='".$note_types."' ");
				$invoice_note2 = $this->db->field("SELECT value FROM default_data
										   WHERE type = '".$note2_types."' ");
				//$exist_note = $this->db->field("SELECT notes2 FROM tblquote  WHERE id='".$in['c_quote_id']."'");
				/*if($exist_note!=''){
					$array['notes2']=$exist_note;
				}else{*/

					$array['notes2']= stripslashes($invoice_note2);
				/*}*/
				$array['NOTES']=stripslashes($invoice_general_note);

				if(empty($in['invoice_id']) && $gen_notes_settings){
					$quote_data = $this->db->query("SELECT id,email_language,tblquote_version.version_id FROM tblquote
                                INNER JOIN tblquote_version ON tblquote_version.quote_id = tblquote.id AND tblquote_version.active ='1'
                                WHERE id = '".$in['c_quote_id']."' 
				                                ")->getAll();
				      $quote_data = $quote_data[0];

				      $quote_notes = $this->db->field("SELECT item_value FROM note_fields
				                 WHERE item_id= '".$quote_data['id']."' AND lang_id ='".$quote_data['email_language']."' AND item_type='quote' AND item_name='notes' AND buyer_id='".$in['buyer_id']."' AND active ='1'");

				      $free_text_content = $this->db->field("SELECT item_value FROM  note_fields
				                     WHERE item_type  = 'quote'
				                     AND  item_name   = 'free_text_content'
				                     AND  version_id  = '".$quote_data['version_id']."'
				                     AND  item_id   = '".$quote_data['id']."'
				                     AND  buyer_id  = '".$in['buyer_id']."'
				                     AND active ='1' ");

				      if(!empty($quote_notes)){
				      	$array['notes2']= stripslashes($quote_notes);	
				      }
				      if(!empty($free_text_content)){
				      	$array['NOTES']= stripslashes($free_text_content);	
  				      }
				} else if(empty($in['invoice_id']) && !$gen_notes_settings){
					if($transl_lang_id_active==1){
						$note_types = 'invoice_terms';
						$note2_types = 'invoice_note';
					}else{
						$note_types = 'invoice_terms_'.$transl_lang_id_active;
						$note2_types = 'invoice_note_'.$transl_lang_id_active;
					}
					$invoice_general_note = $this->db->field("SELECT value FROM default_data
											   WHERE default_name = 'invoice_terms'
											   AND type = '".$note_types."' ");
					$invoice_note2 = $this->db->field("SELECT value FROM default_data
											   WHERE type = '".$note2_types."' ");
			
					$array['NOTES']=$invoice_general_note;
					$array['notes2']= $invoice_note2;
				}

			}else if($in['c_contract_id']){
				if(empty($in['invoice_id']) && $gen_notes_settings){
					$contract_data = $this->db->query("SELECT contracts.contract_id,email_language,contracts_version.version_id FROM contracts
                                INNER JOIN contracts_version ON contracts_version.contract_id = contracts.contract_id AND contracts_version.active ='1'
                                WHERE contracts.contract_id = '".$in['c_contract_id']."' 
				                                ")->getAll();
				      $contract_data = $contract_data[0];

				      $contract_notes = $this->db->field("SELECT item_value FROM note_fields
				                 WHERE item_id= '".$contract_data['contract_id']."' AND lang_id ='".$contract_data['email_language']."' AND item_type='contract' AND item_name='notes' AND active ='1'");

				      $contract_notes2 = $this->db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'contract'
									   AND 	item_name 	= 'free_text_content'
									   AND 	version_id 	= '".$contract_data['version_id']."'
									   AND 	item_id 	= '".$contract_data['contract_id']."'");
				      if(!empty($contract_notes)){
				      	$array['notes2']= stripslashes($contract_notes);	
				      }
				      if(!empty($contract_notes2)){
				      	$array['NOTES']= stripslashes($contract_notes2);	
  				      }
				} else if(empty($in['invoice_id']) && !$gen_notes_settings){
					if($transl_lang_id_active==1){
						$note_types = 'invoice_terms';
						$note2_types = 'invoice_note';
					}else{
						$note_types = 'invoice_terms_'.$transl_lang_id_active;
						$note2_types = 'invoice_note_'.$transl_lang_id_active;
					}
					$invoice_general_note = $this->db->field("SELECT value FROM default_data
											   WHERE default_name = 'invoice_terms'
											   AND type = '".$note_types."' ");
					$invoice_note2 = $this->db->field("SELECT value FROM default_data
											   WHERE type = '".$note2_types."' ");
			
					$array['NOTES']= $invoice_general_note;
					$array['notes2']= $invoice_note2;
				}
			} else if ($in['c_invoice_id'] && $gen_notes_settings) {
                $invoice_note2 = $this->db->field("SELECT notes2 FROM tblinvoice WHERE id = '". $in['c_invoice_id'] ."'");
                $invoice_general_note = $this->db->field("SELECT item_value FROM note_fields WHERE item_id = '". $in['c_invoice_id'] ."' AND item_type = 'invoice' ");
                $array['NOTES'] = $invoice_general_note;
                $array['notes2'] = $invoice_note2;
            } else {
				if($transl_lang_id_active==1){
					//$transl_lang_id_active='invoice_terms';
					$type= $in['type'] == 2 ? 'credit_terms' : 'invoice_terms';
					$note2_types = $in['type'] == 2 ? 'credit_note' : 'invoice_note';
				}else{
					//$transl_lang_id_active='invoice_terms_'.$transl_lang_id_active.'';
					$type= $in['type'] == 2 ? 'credit_terms_'.$transl_lang_id_active : 'invoice_terms_'.$transl_lang_id_active.'';
					$note2_types = $in['type'] == 2 ? 'credit_note_'.$transl_lang_id_active : 'invoice_note_'.$transl_lang_id_active;
				}

                if ($in['type'] == 2) {
                    $invoice_general_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name='credit_terms'
										   AND type='".$type."' ");

                    $invoice_note2 = $this->db->field("SELECT value FROM default_data
										   WHERE type = '".$note2_types."' ");
                } else {
                    $invoice_general_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name='invoice_terms'
										   AND type='".$type."' ");

                    $invoice_note2 = $this->db->field("SELECT value FROM default_data
										   WHERE type = '".$note2_types."' ");
                }
                $array['NOTES']= $invoice_general_note;
                $array['notes2'] =  $invoice_note2;
				if($in['invoice_id']){
					$exist_note = $this->db->field("SELECT notes2 FROM tblinvoice  WHERE id='".$in['invoice_id']."'");
					if($exist_note!=''){
						$array['notes2']=stripslashes(utf8_decode($exist_note));
					}else{
						$array['notes2']=stripslashes($invoice_note2);
						if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
							$acc_langs=array('en','fr','nl','de');
							$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
							if($lang_c){
								if($lang_c=='du'){
									$lang_c='nl';
								}
								if(in_array($lang_c, $acc_langs)){
									$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
									if($in['vat_notes']){
										if($array['notes2']){
											$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
										}else{
											$array['notes2']=$in['vat_notes'];
										}
									}
								}
							}		
						}
					}
									
					$gen_notes = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'invoice'
								   AND item_name = 'notes'
								   And active = '1'
								   AND lang_id = '".$transl_lang_id_active."'
								   AND item_id = '".$in['invoice_id']."' ");

					$has_order = false;
					$has_project = false;
					$has_service = false;
					$has_contract = false;

					if($in['invoice_id']){
						$ord_id = $this->db->field("SELECT order_id FROM tblinvoice WHERE id = '".$in['invoice_id']."'");	
						if(!empty($ord_id)){
							$has_order = true;
						}

						$proj_id = $this->db->field("SELECT id FROM tblinvoice_projects WHERE invoice_id = '".$in['invoice_id']."'");
						if(!empty($proj_id)){
							$has_project = true;
						}
						$service_id = $this->db->field("SELECT service_id FROM tblinvoice WHERE id = '".$in['invoice_id']."'");
						if(!empty($service_id)){
							$has_service = true;
							$serv_note = $this->db->field("SELECT notes2 FROM tblinvoice  WHERE id='".$in['invoice_id']."'");
							$array['notes2']=stripslashes(utf8_decode($serv_note));	

						}

						$contract_id = $this->db->field("SELECT contract_id FROM tblinvoice WHERE id = '".$in['invoice_id']."'");
						if(!empty($contract_id)){
							$has_contract = true;
							$contract_note = $this->db->field("SELECT notes2 FROM tblinvoice  WHERE id='".$in['invoice_id']."'");
							$array['notes2']=stripslashes(utf8_decode($contract_note));	

						}
					}
					
					if($gen_notes!=''){
						$array['NOTES']=stripslashes($gen_notes);
					}else if($gen_notes == '' && $has_order){
						$array['NOTES']=stripslashes($gen_notes);
					}else if($gen_notes == '' && $has_project){
						$array['NOTES']=stripslashes($gen_notes);
					}else if($gen_notes == '' && $has_service){
						$array['NOTES']=stripslashes($gen_notes);		
					}else if($gen_notes == '' && $has_contract){
						$array['NOTES']=stripslashes($gen_notes);		
					}else{
						$array['NOTES']=stripslashes($invoice_general_note);
					}		
				}
			}

		}
		if($in['duplicate_invoice_id']){
            $invoice_type = $this->db->field("SELECT type FROM tblinvoice WHERE id = '". $in['duplicate_invoice_id'] ."'");
            if($transl_lang_id_active==1){
                $note_types = $invoice_type == 2 ? 'credit_terms' : 'invoice_terms';
                $note2_types = $invoice_type == 2 ? 'credit_note' : 'invoice_note';
            }else{
                $note_types = $invoice_type == 2 ? 'credit_terms_'.$transl_lang_id_active : 'invoice_terms_'.$transl_lang_id_active;
                $note2_types = $invoice_type == 2 ? 'credit_note_'.$transl_lang_id_active :'invoice_note_'.$transl_lang_id_active;
            }

            if ($invoice_type == 2) {
                $invoice_general_note = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'credit_terms'
									   AND type = '".$note_types."' ");
                $invoice_note2 = $this->db->field("SELECT value FROM default_data
									   WHERE type = '".$note2_types."'");
            } else {
                $invoice_general_note = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'invoice_terms'
									   AND type = '" . $note_types . "' ");
                $invoice_note2 = $this->db->field("SELECT value FROM default_data
									   WHERE type = '" . $note2_types . "'");
            }
			$array['NOTES']=$invoice_general_note;
			$array['notes2']=$invoice_note2;
			if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
				$acc_langs=array('en','fr','nl','de');
				$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
				if($lang_c){
					if($lang_c=='du'){
						$lang_c='nl';
					}
					if(in_array($lang_c, $acc_langs)){
						$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
						if($in['vat_notes']){
							if($array['notes2']){
								$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
							}else{
								$array['notes2']=$in['vat_notes'];
							}
						}
					}
				}		
			}
		}

		$array['translate_loop']=array();
		while($langs->next()){
			if($in['invoice_id'] == 'tmp' && !$in['c_invoice_id'] && !$in['duplicate_invoice_id'] && !$in['a_quote_id'] && !$in['a_contract_id'] ){
				if($langs->f('lang_id')==1){
					$note_type = 'invoice_terms';
				}else{
					$note_type = 'invoice_terms_'.$langs->f('lang_id');
				}
				$invoice_note = $this->db->field("SELECT value FROM default_data
								   WHERE default_name = 'invoice_terms'
								   AND type = '".$note_type."' ");

				if($in['orders_id']){
					if(gettype($in['orders_id'])=='array'){
						$transl_lang_id_active = $in['languages'];
					}else{
						$is_front = $this->db->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['orders_id']."' ");
						if(empty($is_front)){
							$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['orders_id']."' AND active = 1 AND item_type = 'order'");
						}else{
							$transl_lang_id_active = $this->db->field("SELECT lang_id FROM pim_lang WHERE active = '1' AND sort_order = '1' ");
						}
					}
				}elseif($in['project_id'] || !empty($in['projects_id'])){

					$transl_lang_id_active = $this->db->field("SELECT lang_id FROM pim_lang WHERE active = '1' AND sort_order = '1' ");
					if($in['buyer_id']){
						$buyer_lang_id = $this->db->field("SELECT internal_language FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
						if($buyer_lang_id){
							$transl_lang_id_active = $buyer_lang_id;
						}
					}
				}
			}else{
				if($in['c_quote_id'] ){
					if($langs->f('lang_id')==1){
						$note_type = 'invoice_terms';
					}else{
						$note_type = 'invoice_terms_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'invoice_terms'
										   AND type = '".$note_type."' ");

					$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	active 		= '1'
										   AND 	item_id 	= '".$in['c_quote_id']."' ");
				}elseif($in['c_contract_id'] ){
					if($langs->f('lang_id')==1){
						$note_type = 'contract_invoice_note';
					}else{
						$note_type = 'contract_invoice_note_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'contract invoice note'
										   AND type = '".$note_type."' ");

					$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
										   WHERE item_type 	= 'contract'
										   AND 	item_name 	= 'notes'
										   AND 	active 		= '1'
										   AND 	item_id 	= '".$in['c_contract_id']."' ");



				}elseif($in['c_invoice_id']){
					if($langs->f('lang_id')==1){
						$note_type = 'invoice_terms';
					}else{
						$note_type = 'invoice_terms_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'invoice_terms'
									   AND type = '".$note_type."' ");

				}elseif ($in['duplicate_invoice_id']) {
					$invoice_note = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['duplicate_invoice_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

					$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	active 		= '1'
								   AND lang_id      = '".$in['email_language']."'
								   AND 	item_id 	= '".$in['duplicate_invoice_id']."' ");
				}else{
					$invoice_note = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['invoice_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

					$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	active 		= '1'
								   AND lang_id      = '".$in['email_language']."'
								   AND 	item_id 	= '".$in['invoice_id']."' ");
				}


			}
			$translate_lang = 'form-language-'.$langs->f('code');
			if($langs->f('code')=='du'){
				$translate_lang = 'form-language-nl';
			}
			if($transl_lang_id_active != $langs->f('lang_id')){
				$translate_lang = $translate_lang.' hidden';
			}
			$translate_loop=array(
				'translate_cls' 		=> 		$translate_lang,
				'lang_id' 			=> 		$langs->f('lang_id'),
				'NOTES'			=> 		$invoice_note,
			);
			array_push($array['translate_loop'], $translate_loop);

		}

		$transl_lang_id_active_custom = $in['languages'];
		if(!$transl_lang_id_active_custom){
			$transl_lang_id_active_custom = $in['email_language'];
		}

		$custom_langs = $this->db->query("SELECT * FROM pim_custom_lang WHERE active=1 ORDER BY sort_order ");
		$array['custom_translate_loop']=array();
		while($custom_langs->next()) {
			if($in['invoice_id'] == 'tmp' && !$in['c_invoice_id'] && !$in['duplicate_invoice_id'] && !$in['a_quote_id'] && !$in['a_contract_id'] ) {
				$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
				$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");

				if($in['orders_id']) {
					if(gettype($in['orders_id']) == 'array') {
						$transl_lang_id_active_custom = $in['languages'];
					} else {
						$is_front_custom = $this->db->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['orders_id']."' ");
						if(empty($is_front_custom)) {
							$transl_lang_id_active_custom = $this->db->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['orders_id']."' AND active=1 and item_type = 'order' ");
						 } else {
						 	$transl_lang_id_active_custom = $this->db->field("SELECT lang_id FROM pim_custom_lang WHERE active='1' AND sort_order = '1' ");
						 }
					}
				} elseif($in['project_id'] || !empty($in['projects_id'])) {
					$transl_lang_id_active_custom = $this->db->field("SELECT lang_id FROM pim_lang WHERE active = '1' AND sort_order='1' ");
				}
			} else {
				if($in['c_quote_id'] ) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");
					$transl_lang_id_active_custom = $this->db->field("SELECT lang_id FROM note_fields WHERE item_type = 'quote' AND item_name = 'notes' AND active='1' AND item_id =  '".$in['c_quote_id']."' ");
				}elseif($in['c_contract_id']) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");
					$transl_lang_id_active_custom = $this->db->field("SELECT lang_id FROM note_fields WHERE item_type = 'contract' AND item_name = 'notes' AND active='1' AND item_id = '".$in['c_contract_id']."' ");
				} elseif($in['c_invoice_id']) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type='".$note_type_custom."' ");
				} elseif($in['duplicate_invoice_id']) {
					$invoice_note_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND item_id = '".$in['duplicate_invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");
					$transl_lang_id_active_custom = $this->db->field("SELECT lang_id FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND active='1' AND item_id = '".$in['duplicate_invoice_id']."' ");
				} else {
					$invoice_note_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND item_id = '".$in['invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");
					$transl_lang_id_active_custom = $this->db->field("SELECT lang_id FROM note_fields WHERE item_type = 'invoice' AND item_name='notes' AND active='1' AND item_id = '".$in['invoice_id']."' ");
				}
			}

			$translate_lang_cust = 'form-language-'.$custom_langs->f('code');
			if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
				$translate_lang_cust = $translate_lang_cust.' hidden';
			}

			$custom_translate_loop=array(
				'translate_cls'			=> 		$translate_lang_cust,
				'lang_id'				=> 		$custom_langs->f('lang_id'),
				'NOTES'				=> 		$invoice_note_custom,
			);
			array_push($array['custom_translate_loop'], $custom_translate_loop);

		}

		if($transl_lang_id_active>=1000) {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$transl_lang_id_active."'");
		} else {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_lang WHERE lang_id = '".$transl_lang_id_active."'");
		}

		$translate_lang_active = 'form-language-'.$translate_lang_active;
		if($translate_lang_active=='form-language-du'){
			$translate_lang_active = 'form-language-nl';
		}

		if($transl_lang_id_active>=1000) {
			$lang_code = $this->db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		} else {
			$lang_code = $this->db->field("SELECT language FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		}

		$array['translate_lang_active']	= $translate_lang_active;
		$array['language_dd'] 			= build_language_dd_new($transl_lang_id_active);
		$array['email_language']		= (string)$transl_lang_id_active;
		$array['nr_decimals'] 			= ARTICLE_PRICE_COMMA_DIGITS;


/*		if($in['buyer_id'] && (!$in['invoice_id'] || $in['invoice_id']=='tmp') &&  !$in['c_invoice_id'] ){
			$primal_lang = $this->db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
			if($primal_lang->f('lang_id')==1){
				$note_types = 'invoice_note';
			}else{
				$note_types = 'invoice_note_'.$primal_lang->f('lang_id');
			}
			$invoice_note_set = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'invoice note'
									   AND type = '".$note_types."' ");
			$buyer_inf=$this->db->field("SELECT invoice_note2 FROM customers WHERE customer_id='".$in['buyer_id']."'");
			if($buyer_inf==""){
				$in['notes2'] = $invoice_note_set;
			}else{

				$in['notes2'] = $buyer_inf;
			}

			 

			 $array['notes2']			= 	$in['notes2'];
		}*/
		//acording to 3179 notes are not kept when editing an invoice
		if($customer_id && ($in['invoice_id'] == 'tmp' || $in['duplicate_invoice_id'] || empty($in['invoice_id']))){
				$customer_notes=$this->db->field("SELECT customers.invoice_note2 FROM customers WHERE customer_id = '".$customer_id."'");
				if($customer_notes){
					$array['notes2']=$customer_notes;
					if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
						$acc_langs=array('en','fr','nl','de');
						$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
						if($lang_c){
							if($lang_c=='du'){
								$lang_c='nl';
							}
							if(in_array($lang_c, $acc_langs)){
								$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
								if($in['vat_notes']){
									if($array['notes2']){
										$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
									}else{
										$array['notes2']=$in['vat_notes'];
									}
								}
							}
						}		
					}
				}
			}
		return $array;

	}

	public function get_notes($in){
		$array=array();

		$transl_lang_id_active 		= $in['languages'];
		if(!$transl_lang_id_active){
			$transl_lang_id_active = $in['email_language'];
		}
		$langs = $this->db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");

		$transl_general_active 		= $in['languages'];
		if(!$transl_general_active){
			$transl_general_active = $in['email_language'];
		}
		if($transl_general_active==1){
			$note_types = 'invoice_terms';
			$note2_types = 'invoice_note';
		}else{
			$note_types = 'invoice_terms_'.$transl_general_active;
			$note2_types = 'invoice_note_'.$transl_general_active;
		}
		$invoice_general_note = $this->db->field("SELECT value FROM default_data
								   WHERE default_name = 'invoice_terms'
								   AND type = '".$note_types."' ");
		$invoice_note2 = $this->db->field("SELECT value FROM default_data
								   WHERE type = '".$note2_types."' ");
		$array['NOTES']=$invoice_general_note;
		$array['notes2']=$invoice_note2;

		if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
			$acc_langs=array('en','fr','nl','de');
			$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
			if($lang_c){
				if($lang_c=='du'){
						$lang_c='nl';
				}
				if(in_array($lang_c, $acc_langs)){
					$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
					if($in['vat_notes']){
						if($array['notes2']){
							$array['notes2']=$array['notes2']."\n".$in['vat_notes'];
						}else{
							$array['notes2']=$in['vat_notes'];
						}
					}
				}
			}		
		}

		$array['translate_loop']=array();
		while($langs->next()){
			if($in['invoice_id'] == 'tmp' && !$in['c_invoice_id'] && !$in['duplicate_invoice_id'] && !$in['a_quote_id'] && !$in['a_contract_id'] ){
				if($langs->f('lang_id')==1){
					$note_type = 'invoice_note';
				}else{
					$note_type = 'invoice_note_'.$langs->f('lang_id');
				}
				$invoice_note = $this->db->field("SELECT value FROM default_data
								   WHERE default_name = 'invoice note'
								   AND type = '".$note_type."' ");
			}else{
				if($in['c_quote_id'] ){
					if($langs->f('lang_id')==1){
						$note_type = 'invoice_note';
					}else{
						$note_type = 'invoice_note_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'invoice note'
										   AND type = '".$note_type."' ");

				}elseif($in['c_contract_id'] ){
					if($langs->f('lang_id')==1){
						$note_type = 'contract_invoice_note';
					}else{
						$note_type = 'contract_invoice_note_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'contract invoice note'
										   AND type = '".$note_type."' ");
				}elseif($in['c_invoice_id']){
					if($langs->f('lang_id')==1){
						$note_type = 'invoice_note';
					}else{
						$note_type = 'invoice_note_'.$langs->f('lang_id');
					}
					$invoice_note = $this->db->field("SELECT value FROM default_data
									   WHERE default_name = 'invoice note'
									   AND type = '".$note_type."' ");

				}elseif ($in['duplicate_invoice_id']) {
					$invoice_note = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['duplicate_invoice_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

				}else{
					$invoice_note = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'invoice'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['invoice_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");
				}
			}

			$translate_lang = 'form-language-'.$langs->f('code');
			if($langs->f('code')=='du'){
				$translate_lang = 'form-language-nl';
			}
			if($transl_lang_id_active != $langs->f('lang_id')){
				$translate_lang = $translate_lang.' hidden';
			}
			$translate_loop=array(
				'translate_cls' 		=> 		$translate_lang,
				'lang_id' 			=> 		$langs->f('lang_id'),
				'NOTES'			=> 		$invoice_note,
			);
			array_push($array['translate_loop'], $translate_loop);
		}

		$transl_lang_id_active_custom = $in['languages'];
		if(!$transl_lang_id_active_custom){
			$transl_lang_id_active_custom = $in['email_language'];
		}

		$custom_langs = $this->db->query("SELECT * FROM pim_custom_lang WHERE active=1 ORDER BY sort_order ");
		$array['custom_translate_loop']=array();
		while($custom_langs->next()) {
			if($in['invoice_id'] == 'tmp' && !$in['c_invoice_id'] && !$in['duplicate_invoice_id'] && !$in['a_quote_id'] && !$in['a_contract_id'] ) {
				$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
				$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");
			} else {
				if($in['c_quote_id'] ) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");		
				}elseif($in['c_contract_id']) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type = '".$note_type_custom."' ");					
				} elseif($in['c_invoice_id']) {
					$note_type_custom = 'invoice_note_'.$custom_langs->f('lang_id');
					$invoice_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type='".$note_type_custom."' ");
				} elseif($in['duplicate_invoice_id']) {
					$invoice_note_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND item_id = '".$in['duplicate_invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");			
				} else {
					$invoice_note_custom = $this->db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND item_id = '".$in['invoice_id']."' AND lang_id = '".$custom_langs->f('lang_id')."' ");					
				}
			}

			$translate_lang_cust = 'form-language-'.$custom_langs->f('code');
			if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
				$translate_lang_cust = $translate_lang_cust.' hidden';
			}

			$custom_translate_loop=array(
				'translate_cls'			=> 		$translate_lang_cust,
				'lang_id'				=> 		$custom_langs->f('lang_id'),
				'NOTES'				=> 		$invoice_note_custom,
			);
			array_push($array['custom_translate_loop'], $custom_translate_loop);

		}

		if($transl_lang_id_active>=1000) {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$transl_lang_id_active."'");
		} else {
			$translate_lang_active = $this->db->field("SELECT code FROM pim_lang WHERE lang_id = '".$transl_lang_id_active."'");
		}

		$translate_lang_active = 'form-language-'.$translate_lang_active;
		if($translate_lang_active=='form-language-du'){
			$translate_lang_active = 'form-language-nl';
		}

		if($transl_lang_id_active>=1000) {
			$lang_code = $this->db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		} else {
			$lang_code = $this->db->field("SELECT language FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		}

		$array['translate_lang_active']	= $translate_lang_active;
		$array['language_dd'] 			= build_language_dd_new($transl_lang_id_active);
		$array['email_language']		= $transl_lang_id_active;
		$array['nr_decimals'] 			= ARTICLE_PRICE_COMMA_DIGITS;

		$this->out = $array;
	}

	private function getTemplateInvoice()
	{
		# code...
	}

	public function get_accmanager($in)	{
		$q = strtolower($in["term"]);		

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}

		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
											FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
											WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>htmlspecialchars_decode(stripslashes($db_users->f('first_name').' '.$db_users->f('last_name'))) ) );
		}
		return $items;
	}

	private function getInvoiceIdentity($identity_id){
		$comp_add=$this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ADDRESS_INVOICE'");
		$array = array(			
				'name'			=> ACCOUNT_COMPANY,
				'address'		=> $comp_add=='1' ? nl2br(ACCOUNT_BILLING_ADDRESS) : nl2br(ACCOUNT_DELIVERY_ADDRESS),
				'zip'			=> $comp_add=='1' ? ACCOUNT_BILLING_ZIP : ACCOUNT_DELIVERY_ZIP,
				'city'			=> $comp_add=='1' ? ACCOUNT_BILLING_CITY : ACCOUNT_DELIVERY_CITY,
				'country'		=> $comp_add=='1' ? get_country_name(ACCOUNT_BILLING_COUNTRY_ID) : get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
				'phone'			=> ACCOUNT_PHONE,
				'fax'			=> ACCOUNT_FAX,
				'email'			=> ACCOUNT_EMAIL,
				'url'			=> ACCOUNT_URL,
				'logo'			=>  defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',		
		);
		if($identity_id != '0'){
			$mm = $this->db->query("SELECT * FROM multiple_identity WHERE identity_id='".$identity_id."'")->getAll();
			$value = $mm[0];
			
			$array=array(
				'name'			=> $value['identity_name'],
				'address'		=> nl2br($value['company_address']),
				'zip'			=> $value['company_zip'],
				'city'			=> $value['city_name'],
				'country'		=> get_country_name($value['country_id']),
				'phone'			=> $value['company_phone'],
				'fax'			=> $value['company_fax'],
				'email'			=> $value['company_email'],
				'url'			=> $value['company_url'],
				'logo'			=> $value['company_logo'],
			);
			
		}
		return $array;
	}

	public function get_cc($in)
	{
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type
			FROM customers
			
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 50")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}

			$address = $this->db->query("SELECT zip,city,address FROM customer_addresses
										WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");
			$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				/*'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],*/
				'zip'					=> $address->f('zip') ? $address->f('zip') : '',
				'city'					=> $address->f('city') ? $address->f('city') : '',
				/*"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts($in) {
		if(!$in['buyer_id']){
			return [];
		}
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id,customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n,customer_contacts.language
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id 
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				/*'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),*/
				'bottom' 				=> '',
				'email_language'		=> $value['language']
			);

		}

		$added = false;
		if(count($result)==9){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id,customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n,customer_contacts.language
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id 
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE 1=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
				'email_language'		=> $value['language']
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in)
	{
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 AND (customer_addresses.is_primary = '1' OR customer_addresses.billing = '1')   
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		'symbol'				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

	public function get_articles_list($in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		if($in['email_language'] && $in['email_language'] != 0){
			$def_lang = $in['email_language'];
		}

		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								  
								   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';
			if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    	$table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
		  	}
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_article_categories.name AS family,
						pim_article_categories.name AS categorie,
						pim_articles.supplier_reference,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.use_combined,
						pim_articles.has_variants';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
							   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';
			if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    	$table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
		  	}
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_article_categories.name AS family,
						pim_article_categories.name AS categorie,
						pim_articles.supplier_reference,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.use_combined,
						pim_articles.has_variants';
		}

		$filter.=" 1=1 ";

		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
		    $filter.=' AND pim_article_variants.article_id IS NULL ';
		}

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id'] && !is_array($in['article_id'])){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}
		if($in['artadjust']){
			$in['artadjust']=rtrim($in['artadjust'],",");
			$filter.=" AND pim_articles.article_id IN('".$in['artadjust']."') ";
		}
		if($in['customer_id']){
			$in['buyer_id']=$in['customer_id'];
		}

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='INVOICE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				if($in['vat_regime_id']){
					if($in['vat_regime_id']<10000){
						if($in['vat_regime_id']==2){
							$vat=0;
						}
					}else{
						$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
							LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
							WHERE vat_new.id='".$in['vat_regime_id']."'");
						if(!$vat_regime){
							$vat_regime=0;
						}
						if($vat>$vat_regime){
							$vat=$vat_regime;
						}
					}			
				}else{
					$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
					if($vat_regime<10000){
						if($vat_regime==2){
							$vat=0;
						}
					}else{
						$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
							LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
							WHERE vat_new.id='".$vat_regime."'");
						if(!$vat_regime){
							$vat=0;
						}
					}
				}			
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		  	if($article->f('is_service') == 1){
		  		$price=$article->f('unit_price');
		        $base_price = $price;
		  	}

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['customer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['customer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");
			$nr_taxes =  $this->db->field("SELECT COUNT(article_tax_id) FROM pim_articles_taxes WHERE article_id='".$article->f('article_id')."' ");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	//'name'						=> htmlspecialchars_decode($article->f('internal_name')),
			  	'name'						=> html_entity_decode(stripslashes($article->f('internal_name')), ENT_QUOTES, 'UTF-8'),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'stock2'					=> remove_zero_decimals_dn(display_number($article->f('stock'))),
			    'supplier_reference'		=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? '' : $article->f('supplier_reference'),
			    'family'					=> $article->f('family') ? $article->f('family') : '',
			    'categorie'					=> $article->f('family') ? $article->f('family') : '',
			    'quantity'		    		=> 1,
			    'quantity_old'		    	=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				/*'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price) ,*/
				'price'						=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price ,
				'price_vat'					=> $in['remove_vat'] == 1 ? (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price)  : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price + (($price*$vat)/100)) ,
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)), 
				/*'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? place_currency(display_number_var_dec(0)) : place_currency(display_number_var_dec($base_price))),*/
				'base_price'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? place_currency(display_number_var_dec(0)) : place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
				'has_variants'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? true : false,
				'has_variants_done'			=> 0,
				'is_variant_for'			=> 0,
				'is_combined'				=> $article->f('use_combined') ? true : false,
				'component_for'				=> '',
				'visible'					=> 1,
				'has_taxes'					=> $nr_taxes? true : false,
				'allow_stock'               =>ALLOW_STOCK == 1 ? true : false,

			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		if(in_array(12,explode(';', $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']])))){
			array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));
		}
		return $articles;
	}

	function get_invoices_list(&$in){
		$q = strtolower($in["term"]);
		$filter = '';

		if($q){
			$filter .=" AND serial_number LIKE '%".$q."%'";
		}
		$q2=$in["buyer_id"];
		$filter2 = '';
		if($q2){
			$filter2 .=" AND buyer_id = '".$q2."' ";
		}
	
		$invoices_list=$this->db->query("SELECT serial_number,id, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date FROM tblinvoice
			WHERE type!=2 AND type!=4
			AND type!=1 $filter $filter2
			AND f_archived='0' ORDER BY t_date DESC , serial_number DESC ");
		
		


		while($this->db->move_next()){
			$destinations[$invoices_list->f('serial_number')]=$invoices_list->f('id');
		}

		$result = array();
		if($destinations){
			foreach ($destinations as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "name" => htmlentities($key)));
					}
				}else{
					array_push($result, array("id"=>$value, "name" => htmlentities($key)));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "name" => 'No matches'));
		}
		return $result;
	}

	public function get_deals(&$in){
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND subject LIKE '".$q."%'";
		}
		$deals=$this->db->query("SELECT * FROM tblopportunity 
			WHERE buyer_id='".$in['buyer_id']."' $filter ORDER BY subject ASC");
		$items = array();
		while($deals->next()){
			array_push( $items, array('deal_id'=>$deals->f('opportunity_id'),'name'=>stripslashes($deals->f('subject'))));
		}
		if(defined('ADV_CRM') && ADV_CRM == 1){
			if($q){
		        array_push($items,array('deal_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		    }else{
		        array_push($items,array('deal_id'=>'99999999999','name'=>''));
		    }
		}
		return $items;
	}
	

}

	$invoice = new InvoiceEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $invoice->output($invoice->$fname($in));
	}

	$invoice->getInvoice();
	$invoice->output();


?>