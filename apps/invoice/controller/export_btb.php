<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $main_menu_item, $sub_menu_item, $config;

$result=array();

if($in['reset_list']){
	if(isset($_SESSION['tmp_export_add_to_app']) && $_SESSION['invoice_to_export']){
		unset($_SESSION['tmp_export_add_to_app']);
		$db->query("DELETE FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."'");
	}	
}

switch ($in['app']) {
	case 'yuki':
		$title = gm("Export to Yuki");
		$thing = 'yuki';
		break;
	case 'codabox':
		$title = gm("Export to Codabox");
		$thing = 'codabox';
		$result['coda']		= true;
		break;
	case 'clearfacts':
		$title = gm("Export to ClearFacts");
		$thing = 'clearfacts';
		$result['clearfacts']		= true;
		break;	
	case 'billtobox':
		$title = gm("Export to BillToBox");
		$thing = 'billtobox';
		$result['billtobox']		= true; 
		break;
	case 'exact_online':
		$title = gm("Export to Exact online");
		$thing = 'exact_online';
		$result['exact_online']		= true;
		break;
	case 'netsuite':
		$title = gm("Export to NetSuite");
		$thing = 'netsuite';
		$result['netsuite']		= true;
		break;
	case 'jefacture':
		$title = gm("Export to Jefacture");
		$thing = 'jefacture';
		$result['jefacture']		= true;
		break;
	default:
		$title = gm('Export to E-FFF');
		$thing = 'winbooks';
		break;
}

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(empty($_SESSION['invoice_to_export'])){
		$_SESSION['invoice_to_export'] = time();
	}
}else{
	$_SESSION['invoice_to_export'] = time();
}
//$db->query("DELETE FROM tblinvoice_to_export WHERE `time` < '".(time()-3600)."' ");

$l_r = ROW_PER_PAGE;

$order_by_array = array('serial_number','t_date','buyer_name','invoice_number','booking_number',);

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$arguments_o='';
$arguments_s='';
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$filter_link = 'block';

$sent_filter="1";
if($thing == 'jefacture' || ($thing == 'billtobox' && defined('EXPORT_SEND_INVOICE_BILLTOBOX') && EXPORT_SEND_INVOICE_BILLTOBOX == '1')){
	$sent_filter="0";
}

if($in['type']=='0'){
	$order_by = " ORDER BY t_date DESC, iii DESC ";
	if($in['archived']=='0' || !$in['archived']){
		$is_archiv = false;
		$join = "LEFT JOIN ";
		$filter = " WHERE tblinvoice.sent=".$sent_filter." AND (tblinvoice.type='0' OR tblinvoice.type='2' OR tblinvoice.type='3' OR tblinvoice.type='4') AND tblinvoice.f_archived='0' AND tblinvoice_exported.invoice_id IS NULL  ";
	}else{
		$is_archiv = true;
		$join = "INNER JOIN ";
		$filter = " WHERE tblinvoice.sent='1' AND (tblinvoice.type='0' OR tblinvoice.type='2' OR tblinvoice.type='3' OR tblinvoice.type='4') AND tblinvoice.f_archived='0' ";
	}

	if(!empty($in['filter'])){
		$arguments.="&filter=".$in['filter'];
	}

	if(!empty($in['search'])){
		$filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' )";
		$arguments_s.="&search=".$in['search'];
	}
	if(!empty($in['order_by'])){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc']=='true'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		}
	}

	if(isset($in['doc_type']) && $in['doc_type']!=-1 ){
	    $filter.=" and tblinvoice.type = '".$in['doc_type']."'  ";
	    $arguments.="&doc_type=".$in['doc_type'];
	}

	if($in['app']=='codabox'){
		$filter.=" and tblinvoice.buyer_id>0 ";
	}
	$arguments = $arguments.$arguments_s;
	$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
	//$db->query("SELECT tblinvoice.*, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii
	$max_rows_data =  $db->query("SELECT tblinvoice.id FROM tblinvoice
						{$join} tblinvoice_exported ON tblinvoice.id=tblinvoice_exported.invoice_id AND tblinvoice_exported.".$thing."='1'  AND tblinvoice_exported.type='0' 
						{$filter} GROUP BY tblinvoice.id " );
	$max_rows=$max_rows_data->records_count();

	if(!$_SESSION['tmp_export_add_to_app'] || ($_SESSION['tmp_export_add_to_app'] && empty($_SESSION['tmp_export_add_to_app']))){
		while($max_rows_data->next()){	
			$_SESSION['tmp_export_add_to_app'][$max_rows_data->f('id')]=1;
		}
	}

	$all_pages_selected=false;
	$minimum_selected=false;
	if($_SESSION['invoice_to_export']){
		$invoices_marked=$db->field("SELECT COUNT('invoice_id') FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."'");
		if($max_rows>0 && $invoices_marked == $max_rows){
			$all_pages_selected=true;
		}else if($invoices_marked){
			$minimum_selected=true;
		}	
	}

	$tblinvoice = $db->query("SELECT tblinvoice.*, tblinvoice.serial_number as iii, cast( FROM_UNIXTIME( tblinvoice.`invoice_date` ) AS DATE ) AS t_date
				FROM tblinvoice
				{$join} tblinvoice_exported ON tblinvoice.id=tblinvoice_exported.invoice_id AND tblinvoice_exported.".$thing."='1'  AND tblinvoice_exported.type='0'  
				{$filter} {$order_by}  LIMIT ".$offset*$l_r.",".$l_r);
}else{
	$order_by = " ORDER BY iii DESC ";

	if($in['archived']=='0' || !$in['archived']){
		$is_archiv = false;
		$join = "LEFT JOIN ";
		$filter = " WHERE tblinvoice_exported.invoice_id IS NULL AND tblinvoice_incomming.type !='1'  ";
	}else{
		$is_archiv = true;
		$join = "INNER JOIN ";
		$filter = " WHERE 1=1 AND tblinvoice_incomming.type !='1' ";
		$arguments.="&archived=".$in['archived'];
	}

	if(!empty($in['filter'])){
		$arguments.="&filter=".$in['filter'];
	}
	if(!empty($in['search'])){
		$filter.=" AND (tblinvoice_incomming.invoice_number LIKE '%".$in['search']."%' OR tblinvoice_incomming.booking_number LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%' ) ";
		$arguments_s.="&search=".$in['search'];
	}
	if(!empty($in['order_by'])){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc']=='true' || $in['desc']=='1'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		}
	}
	$arguments = $arguments.$arguments_s;
	$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
	//$db->query("SELECT tblinvoice.*, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii
	$max_rows_data =  $db->query("SELECT tblinvoice_incomming.invoice_id FROM tblinvoice_incomming
		LEFT JOIN customers ON tblinvoice_incomming.supplier_id=customers.customer_id
						{$join} tblinvoice_exported ON ( tblinvoice_incomming.invoice_id=tblinvoice_exported.invoice_id AND tblinvoice_exported.".$thing."='1' AND tblinvoice_exported.type='1' )
				{$filter} GROUP BY tblinvoice_incomming.invoice_id" );
	$max_rows=$max_rows_data->records_count();

	if(!$_SESSION['tmp_export_add_to_app'] || ($_SESSION['tmp_export_add_to_app'] && empty($_SESSION['tmp_export_add_to_app']))){
		while($max_rows_data->next()){	
			$_SESSION['tmp_export_add_to_app'][$max_rows_data->f('invoice_id')]=1;
		}
	}

	$all_pages_selected=false;
	$minimum_selected=false;
	if($_SESSION['invoice_to_export']){
		$invoices_marked=$db->field("SELECT COUNT('invoice_id') FROM tblinvoice_to_export WHERE `time`='".$_SESSION['invoice_to_export']."'");
		if($max_rows>0 && $invoices_marked == $max_rows){
			$all_pages_selected=true;
		}else if($invoices_marked){
			$minimum_selected=true;
		}	
	}

	$tblinvoice = $db->query("SELECT tblinvoice_incomming.*, tblinvoice_incomming.booking_number as iii, cast( FROM_UNIXTIME( tblinvoice_incomming.`invoice_date` ) AS DATE ) AS t_date
				FROM tblinvoice_incomming
				LEFT JOIN customers ON tblinvoice_incomming.supplier_id=customers.customer_id
				{$join} tblinvoice_exported ON ( tblinvoice_incomming.invoice_id=tblinvoice_exported.invoice_id AND tblinvoice_exported.".$thing."='1' AND tblinvoice_exported.type='1' )
				{$filter} {$order_by} LIMIT ".$offset*$l_r.",".$l_r );
}

//$max_rows=$tblinvoice->records_count();
$result['max_rows']=$max_rows;
$result['all_pages_selected']=$all_pages_selected;
$result['minimum_selected']=$minimum_selected;
$result['query']=array();
//$tblinvoice->move_to($offset*$l_r);
$j=0;
$color = '';
$all_invoices_id = '';
if($in['type']=='0'){
	while($tblinvoice->move_next()){
		$is = $db->field("SELECT count(invoice_id) FROM tblinvoice_to_export WHERE time='".$_SESSION['invoice_to_export']."' AND invoice_id='".$tblinvoice->f('id')."' ");
		$item=array(
			'info_link'     		=> 'index.php?do=invoice-invoice&invoice_id='.$tblinvoice->f('id'),
			'mark_as_sent_link'	=> array('do'=>'invoice-export_btb-btb-markExport','invoice_id'=>$tblinvoice->f('id'),'app'=>$in['app']),		
			'unmark_as_sent_link'	=> array('do'=>'invoice-export_btb-btb-unmarkExport','invoice_id'=>$tblinvoice->f('id'),'archived'=>1,'app'=>$in['app']),
			'our_reference'		=> $tblinvoice->f('our_ref'),
			'id'                	=> $tblinvoice->f('id'),
			's_number'		     	=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
			'created'           	=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
			'buyer_name'        	=> $tblinvoice->f('buyer_name'),
			'text'			=> $tblinvoice->f('created_by') == 'System' ? gm('Created from a recurring invoice') : '',
			'checked'			=> $is == 1 ? true : false,
			'is_archiv'			=> $is_archiv,
		);
		$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('id');
		array_push($result['query'], $item);
		$j++;
	}
}else{
	while($tblinvoice->move_next() ){
		$is = $db->field("SELECT count(invoice_id) FROM tblinvoice_to_export WHERE time='".$_SESSION['invoice_to_export']."' AND invoice_id='".$tblinvoice->f('invoice_id')."' ");
		$item=array(
			'info_link'     		=> 'index.php?do=invoice-incomming_invoice&invoice_id='.$tblinvoice->f('invoice_id'),
			'mark_as_sent_link'	=> array('do'=>'invoice-export_btb-btb-markExport','invoice_id'=>$tblinvoice->f('invoice_id'),'type'=>1,'app'=>$in['app']),
			// 'our_reference'			=> $tblinvoice->f('our_ref'),
			'id'                	=> $tblinvoice->f('invoice_id'),
			's_number'		     	=> $tblinvoice->f('invoice_number') ? $tblinvoice->f('invoice_number') : '',
			'b_number'		     	=> $tblinvoice->f('booking_number') ? $tblinvoice->f('booking_number') : '',
			'created'           	=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
			'buyer_name'        	=> $db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('supplier_id')."'"),
			'checked'			=> $is == 1 ? true : false,
			'is_archiv'			=> $is_archiv,
			'unmark_as_sent_link'	=> array('do'=>'invoice-export_btb-btb-unmarkExport','invoice_id'=>$tblinvoice->f('invoice_id'),'archived'=>1,'type'=>1,'app'=>$in['app']),
		);
		$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('invoice_id');
		array_push($result['query'], $item);
		$j++;
	}
}

	$error_app=$in['app'];
	if($in['app']=='winbooks'){
		$error_app='btb';
	}
	$export_errors=$db->field("SELECT COUNT(id) FROM error_call WHERE app='".$error_app."' AND module='invoice' ");

	$all_invoices_id = trim($all_invoices_id,',');
	$result['title']	 	   	= $title;
	$result['not_yuki']		= $in['app'] == 'yuki' ? false : true;
	$result['is_invoice']		= false;
	$result['all_invoices_id']	= $all_invoices_id;
	$result['lr']			= $l_r;
	$result['col_plus']		= $in['type']==1 ? 4 : 6;
	$result['export_errors']	= $export_errors ? true : false;
	$result['types']  		= build_invoice_type_dd();
	$result['col_search']		= $in['app'] == 'netsuite' ? '10' : '12';
	$result['doc_type']		= $in['doc_type'];

json_out($result);
?>

