<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');
session_write_close();
global $config;
if($in['dbase']){
	global $database_config;
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $in['dbase'],
	);
	$db = new sqldb($database_2);
}else{
	$db = new sqldb();
}
$db2 = new sqldb();
if(!$in['save_as'] && !$in['reminder']){
	ark::loadCronLibraries(array('aws'));
	$aws = new awsWrap(($in['dbase'] ? $in['dbase'] : DATABASE_NAME));
	if(ark::$model == 'invoice' && (ark::$method == 'send' || ark::$method == 'sendNewEmail') && !$in['save_xml']){
		$in['attach_file_name'] = 'invoice_'.$in['id'].'.pdf';
		$file = $aws->getItem('invoice/invoice_'.$in['id'].'.pdf',$in['attach_file_name']);
		if($file === true){
			return;
		}else{
			$in['attach_file_name'] = null;
		}
	}else{
		$link =  $aws->getLink($config['awsBucket'].($in['dbase'] ? $in['dbase'] : DATABASE_NAME).'/invoice/invoice_'.$in['id'].'.pdf');
		$content = file_get_contents($link);
		$inv_serial_number=$db->field("SELECT serial_number FROM tblinvoice WHERE id='{$in['id']}' ");
		if($inv_serial_number){
             $name=$inv_serial_number.'.pdf';
		}else{
			$name='invoice_'.$in['id'].'.pdf';
		}
		if($content){
			if($in['base64']){
				//$name= 'invoice.pdf';
				$str = 'Content-Type: application/pdf;'."\r\n";
				$str .= ' name="'.$name.'"'."\r\n";
				$str .= 'Content-Transfer-Encoding: base64'."\r\n";
				$str .= 'Content-Disposition: attachment;'."\r\n";
				$str .= ' filename="'.$name.'"'."\r\n\r\n";
				//$str = chunk_split(base64_encode($content), 76, "\r\n");
				$str = base64_encode($content);
				return $str;
			}
			doQueryLog();
			header('Content-Type: application/pdf');
			header("Content-Disposition:inline;filename=".$name);
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $content;
			exit();
		}

	}
}
$db5 = new sqldb();
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");

if($in['custom_type']==1 && DATABASE_NAME=='9f56e245_4434_157d_9f06320b4bb8'){
	include(__DIR__.'/invoice_credit_print_9f56e245_4434_157d_9f06320b4bb8.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='SalesAssist_111'){
	include(__DIR__.'/invoice_credit_print_b7b2acca_6944_a954_7151a7548069.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='salesassist_22'){
	include(__DIR__.'/invoice_credit_print_b7b2acca_6944_a954_7151a7548069.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'){ //both are cozie databases
	include(__DIR__.'/invoice_credit_print_a0be249a_ee94_e160_0a8ee28f1b15.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='7b5cc5d1_e3b4_b17c_be982b8dc3ae'){ //cozie test
	include(__DIR__.'/invoice_credit_print_a0be249a_ee94_e160_0a8ee28f1b15.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='03a0fb2c_c144_cda3_adda851c8e4b'){ //brasserie
	include(__DIR__.'/invoice_credit_print_03a0fb2c_c144_cda3_adda851c8e4b.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='b7b2acca_6944_a954_7151a7548069'){ //Mister T
	include(__DIR__.'/invoice_credit_print_b7b2acca_6944_a954_7151a7548069.php');
	return;
}elseif($in['custom_type']==1 && (DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='c4482c81_7d14_c57e_2068878ad2f4' || DATABASE_NAME=='SalesAssist_11') ){ 
	include(__DIR__.'/invoice_credit_print_c4482c81_7d14_c57e_2068878ad2f4.php');
	return;
}elseif($in['custom_type']==1 && (DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='1ad760a7_3df4_a15e_099e6ba271d5' || DATABASE_NAME=='SalesAssist_11') ){ //Ahooga 10866
	include(__DIR__.'/invoice_credit_print_1ad760a7_3df4_a15e_099e6ba271d5.php');
	return;
}elseif($in['custom_type']==1 && (DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='0de7a47b_be24_9551_7c8275f7d0ba' || DATABASE_NAME=='1b482df5_cad4_6157_b7839ba4b514' || DATABASE_NAME=='SalesAssist_11') ){ //4313
	include(__DIR__.'/invoice_credit_print_0de7a47b_be24_9551_7c8275f7d0ba.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='1008e331_1c14_2d80_8c2d63f6b832'){ //dorus
	include(__DIR__.'/invoice_credit_print_1008e331_1c14_2d80_8c2d63f6b832.php');
	return;
}

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$special_template = array(4,5,7,8); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
 $pdf->SetFont('helvetica', '', 10, '', false);
// if($custom_lng){
	#we can use both font types but dejavusans looks better
//	$pdf->SetFont('dejavusans', '', 9, '', false);
// $pdf->SetFont('freeserif', '', 10, '', false);
// }

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetAutoPageBreak(TRUE, 25);


// $pdf->setLanguageArray($l);
/*if(!USE_INVOICE_PAGE_NUMBERING){
	$pdf->SetFooterMargin(0);
	$pdf->setPrintFooter(false);
}
*/

if(in_array($in['type'], $special_template)){
	if($in['type']==6){
		$pdf->SetMargins(6, 37, 10);
		$pdf->SetAutoPageBreak(TRUE, 10);
	}elseif($in['type']==7){
		$pdf->SetMargins(15, 15, 10);
		$pdf->SetAutoPageBreak(TRUE, 15);
	}else{
		$pdf->SetMargins(10, 5, 10);
		$pdf->SetAutoPageBreak(TRUE, 15);
	}
	// 
	// $pdf->setPrintFooter(false);
	$hide_all = 2;
}

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();

$html = include('invoice_credit_print_body.php');
if(strpos($html, "č") !== false){
	$html=str_replace("č", "c", $html);
}

$pdf->writeHTML($html, true, false, true, false, '');


#250 vat si discount
#255 numai discount
#267 fara vat si discount




if($in['custom_type'] == 1){
	if($pdf->GetY() > $height){
		$pdf->AddPage();
	}
}elseif (in_array($in['type'], $special_template) ) {
	if($pdf->GetY() > $height){
		$pdf->AddPage();
	}
}

	$pdf->SetY($height);

if(in_array($in['type'], $special_template)){
	$hide_all = 1;
	$pdf->SetAutoPageBreak(TRUE, 0);
	$htmls = include('invoice_credit_print_body.php');
	$pdf->writeHTML($htmls, true, false, true, false, '');
}
if($in['print']){
	echo($html);
	exit();
}


if(!$in['db']){
	$db =  new sqldb();

}else{
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $in['db'],
		);
	$db =  new sqldb($database_2); // recurring billing
}

$attach_timesheet = $db->field("SELECT attach_timesheet FROM tblinvoice WHERE id='{$in['id']}' ");
if($attach_timesheet == 1){
	$select = $db->query("SELECT * FROM tblinvoice_projects WHERE invoice_id='{$in['id']}'");
	while ($select->next()) {
		$in['time_start'] = $select->f('start_date');
		$in['time_end'] = $select->f('end_date');
		$in['project_id'] = $select->f('project_id');
		$time_print = include(__DIR__.'/../../misc/controller/timesheet_print_body.php');

		$pdf->AddPage();
		$pdf->writeHTML($time_print, true, false, true, false, '');
	}
}
$attach_intervention = $db->query("SELECT attach_intervention,service_id FROM tblinvoice WHERE id='{$in['id']}' ");
if($attach_intervention->f('attach_intervention') == 1){
	$services = explode('-', $attach_intervention->f('service_id'));
	// print_r($services);exit();
	foreach ($services as $value) {
		$in['service_id'] = $value;
		$time_print = include(__DIR__.'/../../maintenance/controller/print_body.php');

		$pdf->AddPage();
		$pdf->writeHTML($time_print, true, false, true, false, '');
	}
}

$pdf->lastPage();
if(!$serial_number && $in['id']) {
    $serial_number=$in['id'];
}

if($in['save_as'] == 'F'){
	$in['invoice_pdf_name'] = 'invoice_'.unique_id(32).'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['invoice_pdf_name'], 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['invoice_pdf_name'];
	$pdfFile = 'invoice/invoice_'.$in['id'].".pdf";
	$e = $a->uploadFile($pdfPath,$pdfFile);
	if(ark::$method == 'gpasta'){
		$outputType = "S";
		if($in['attachAsString']==1){
			$outputType = "E";
		}
		//$str = $pdf->Output($serial_number.'.pdf',$outputType);
		$str = $pdf->Output(__DIR__.'/../../../'.$in['invoice_pdf_name'],$outputType);
	}
	if(ark::$method=='sendNewEmail' || ark::$method=='add_recurring'){
		$pdf->Output(__DIR__.'/../../../invoice_.pdf', 'F');
	}
	unlink($in['invoice_pdf_name']);
	// $db->query("UPDATE tblquote SET preview='1' WHERE id='".$in['id']."' ");
	// $pdf->Output($buyer_reference.' '.$serial_number.'.pdf','I');

}else if($in['do']=='invoice-invoice_print'){
	doQueryLog();
   $pdf->Output($serial_number.'.pdf','I');
}else if($in['do']=='invoice-invoice_credit_print'){
	//Page numbering
		if($in['type']){
			$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_HEADER_PDF_FORMAT' ");
			$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_FOOTER_PDF_FORMAT' ");
			$in['header_id']=$def_header;
			$in['footer_id']=$def_footer;
		}

		$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id='".$in['lid']."'")->getAll();
		$labels = array();
		$j=1;
		foreach ($labels_query['0'] as $key => $value) {
			$labels[$key] = $value;
			$j++;
		}
		if(!$in['id'] && !$in['reminder']){
			
			if(!$in['custom_type']){
		        //$pdf->setQuotePageLabel($labels['page']);
		    }
		}elseif($in['id'] && !$in['reminder']){
			$pdf->setQuotePageLabel($labels['page']);
		}

	//footer1
		$page_numbering = $db->field("SELECT value FROM settings WHERE constant_name='USE_INVOICE_PAGE_NUMBERING' ");
		if($in['header_id']){
			if($page_numbering==0){
			}
		}elseif($in['footer_id']){
			if($page_numbering==0){
			}
		}
   $pdf->Output($serial_number.'.pdf','I');
}else if($in['base64']){
	$str = $pdf->Output($serial_number.'.pdf','E');
}else{
	$pdf->Output(__DIR__.'/../../../invoice_.pdf', 'F');
}

?>
