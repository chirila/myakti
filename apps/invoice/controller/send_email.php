<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

$result=array();
global $config;

global $database_config;
$database = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
$dbu_users =  new sqldb($database);

$rem_names=$db->field("SELECT value FROM settings WHERE constant_name='USE_REMINDER_NAMES' AND type='1' ");
list($buyer_id,$contact_id) = explode('-', $in['c_id']);
$code = '';
if($buyer_id != '0' ){
	$filter =" AND buyer_id='".$buyer_id."' ";
	$language = $db->field("SELECT language FROM customers WHERE customer_id='".$buyer_id."' ");
	$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$language."' ");
	if(!$code){
		$code = $db->field("SELECT code FROM pim_custom_lang WHERE lang_id='".$language."' ");
	}
	// if($code == 'du'){$code = 'nl';}
}else if($contact_id != '0'){
	$filter =" AND contact_id='".$contact_id."' ";
}else{
	json_out($result);
}

$result['invoice_ids']=array();
$invoice_text = gm('Invoice',true);
$days_text = gm('days',true);
$late_text = gm('Late',true);
$invoices = $db->query("SELECT * FROM tblinvoice WHERE sent='1' AND (status='0' OR (status='1' AND paid ='2')) AND not_paid='0' AND f_archived='0' ".$filter." AND due_date < ".time()." ORDER BY invoice_grade DESC");
$text = '<table cellspacing="0" cellpadding="10px" border="0" >';
$old_grade='';
$invoice_ids = '';
$penalty_value = '';
/*if(USE_GRADES=='0')
{
	$text.='<tr><td colspan=\'4\' style="border-bottom: 1px solid #000;">&nbsp;</td></tr>';
}*/
while ($invoices->next()) {
	/*$payments = $db->field("SELECT SUM(amount) FROM tblinvoice_payments WHERE invoice_id='".$invoices->f('id')."' ");*/

	$balance=$invoices->f('amount_vat');
    $all_payments=$db->query("SELECT * FROM tblinvoice_payments WHERE invoice_id='".$invoices->f('id')."'")->getAll();
    foreach($all_payments as $key=>$value){
        if($value['credit_payment']){
            if($value['amount']<0){
                $balance+=$value['amount'];
            }else{
                $balance-=$value['amount'];
            }
        }else{
            $balance-=$value['amount'];
        }
    }

	if(!$code){
		$language = $invoices->f('email_language');
		if(!$language){$language=1;}
		$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$language."' ");
		if(!$code){$code = 'en';}
		// if($code == 'du'){$code = 'nl';}
	}
	// if($invoices->f('email_language')==0) {
	// 	$user_language = $dbu_users->field("SELECT lang_id FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	// 	$user_language_code = $dbu_users->field("SELECT code FROM lang WHERE lang_id = '".$user_language."' ");
	// } else {
	// 	$user_language = $invoices->f('email_language');
	// 	$user_language_code= $db->field("SELECT code FROM pim_lang WHERE lang_id = '".$user_language."' ");
	// }
	if(!$first_grade){ $first_grade = $db->field("SELECT title FROM tblinvoice_grades WHERE grade= 'first' AND lang_code = '".$code."' "); }
	if(!$second_grade){ $second_grade = $db->field("SELECT title FROM tblinvoice_grades WHERE grade= 'second' AND lang_code = '".$code."' "); }
	if(!$third_grade){ $third_grade = $db->field("SELECT title FROM tblinvoice_grades WHERE grade= 'third' AND lang_code = '".$code."' "); }

	$lngCode = $code;
	if($lngCode == 'du'){$lngCode = 'nl';}

	$t1 = htmlentities(utf8_encode($invoice_text['0'][$lngCode]),ENT_COMPAT | ENT_HTML401,'UTF-8');
	$t2 = htmlentities(utf8_encode($days_text['0'][$lngCode]),ENT_COMPAT | ENT_HTML401,'UTF-8');
	$t3 = htmlentities(utf8_encode(strtolower($late_text['0'][$lngCode])),ENT_COMPAT | ENT_HTML401,'UTF-8');

	switch ($invoices->f('invoice_grade')) {
		case '1':
			$grade='first';
			$grade_txt = utf8_encode($first_grade);
			break;
		case '2':
			$grade='second';
			$grade_txt = utf8_encode($second_grade);
			break;
		case '3':
			$grade='third';
			$grade_txt = utf8_encode($third_grade);
			break;
	}
	$buyer_name = $invoices->f('buyer_name');
	$buyer_id = $invoices->f('buyer_id');
	$days = round((time() - $invoices->f('due_date'))/(60*60*24));
	$space = 15-strlen($invoices->f('serial_number'));
	if($old_grade!=$grade)
	{
		$penalty_value = '';
		$grade_info = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='".$grade."' AND lang_code='".$code."'");
		if(!$grade_info->next()){
			$default_values= $db->query("SELECT * FROM tblinvoice_grades WHERE grade='".$grade."' LIMIT 1");

			if($default_values->f('value') && $default_values->f('value') !='0.00' && $default_values->f('value') !=0){
			if($default_values->f('type')==1)
			{
				$penalty_value = ' + '.display_number($default_values->f('value')).'%';
			}else
			{
				$penalty_value = ' + '.place_currency(display_number($default_values->f('value')));
			}}
		}else if($grade_info->f('value')!='0.00')
		{
			if($grade_info->f('value')){
			if($grade_info->f('type')==1)
			{
				$penalty_value = ' + '.display_number($grade_info->f('value')).'%';
			}else
			{
				$penalty_value = ' + '.place_currency(display_number($grade_info->f('value')));
			}}
		}
		$text.='<tr><td colspan=\'4\' style="border: 1px solid #000;padding:4px;">'.$grade_txt.'<br/> '.nl2br($grade_info->f('message')).'</td></tr>';
	}
	/*if(USE_GRADES=='0')
	{
		$penalty_value = '';
	}*/

	// $text .=$t1.": ".$invoices->f('serial_number').$nbsp.date(ACCOUNT_DATE_FORMAT,$invoices->f('invoice_date'))." : ".place_currency(display_number($invoices->f('amount'))).$nbsp2.$days." ".$t2." ".$t3.". \n";
	$text .='<tr>
	';
	$text .='<td width="130" style="border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000;padding:4px;">'.$t1.': '.$invoices->f('serial_number').'</td>
	';
	$text .='<td width="110" style="border-right: 1px solid #000;border-bottom: 1px solid #000;padding:4px;">'.date(ACCOUNT_DATE_FORMAT,$invoices->f('invoice_date')).'</td>
	';
	/*$text .='<td width="170" style="border-right: 1px solid #000;border-bottom: 1px solid #000;padding:4px;">'.place_currency(display_number($invoices->f('amount_vat')-$payments)).$penalty_value.'</td>*/
	$text .='<td width="170" style="border-right: 1px solid #000;border-bottom: 1px solid #000;padding:4px;">'.place_currency(display_number($balance)).$penalty_value.'</td>
	';
	$text .='<td width="150" style="border-right: 1px solid #000;border-bottom: 1px solid #000;padding:4px;">'.$days.' '.$t2.' '.$t3.'</td>
	</tr>
	';
	$old_grade = $grade;

	array_push($result['invoice_ids'], $invoices->f('id'));
}
$text .="</table>";
$message_data=get_sys_message('invmess_late',$code,false);


if($message_data['subject'] == null && $message_data['text'] == null){
	$name = 'invmess_late';
$message_data['text'] = 'Dear Customer: [!CUSTOMER!],

We want to remind you about this late invoices that still needs to be payed.

[!INVOICE_SUMMARY!]

Best regards
';
  $message_data['subject'] = 'Late invoice summary';
  $insert = $db->query("INSERT INTO sys_message SET text='".$message_data['text']."', subject='".$message_data['subject']."', name='invmess_late', lang_code='".$code."' ");
}

$subject=$message_data['subject'];
//$subject=str_replace('[!CUSTOMER!]',htmlentities($buyer_name,ENT_COMPAT | ENT_HTML401,'UTF-8'), $subject);
$subject=str_replace('[!CUSTOMER!]',$buyer_name, $subject);
$subject=str_replace('[!INVOICE_SUMMARY!]',$text, $subject);
$subject=str_replace('[!CURRENT_DATE!]', date(ACCOUNT_DATE_FORMAT), $subject);


$body=nl2br($message_data['text']).nl2br($message_data['footnote']);

$body=str_replace('[!CUSTOMER!]',htmlentities($buyer_name,ENT_COMPAT | ENT_HTML401,'UTF-8'), $body);
$body=str_replace('[!INVOICE_SUMMARY!]',$text, $body);
$body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);
$body=str_replace('[!CURRENT_DATE!]', date(ACCOUNT_DATE_FORMAT), $body);


/*if($buyer_id == '0' && $contact_id != '0'){
	$contact = $db->query("SELECT firstname,lastname
					FROM customer_contacts
					WHERE contact_id='".$contact_id."' AND active=1");

	$subject=str_replace('[!FIRST_NAME!]',$contact->f('firstname'), $subject);
	$subject=str_replace('[!LAST_NAME!]',$contact->f('lastname'), $subject);
	$body=str_replace('[!FIRST_NAME!]',$contact->f('firstname'), $body);
	$body=str_replace('[!LAST_NAME!]',$contact->f('lastname'), $body);
}*/

	$with_names=0;
	if($rem_names){
		$result['contacts']=array();
		if($buyer_id=='0' && $contact_id!='0'){
			$contacts = $db->query("SELECT firstname,lastname,  customer_contact_title.name AS title
					FROM customer_contacts
					LEFT JOIN customer_contact_title ON customer_contacts.title = customer_contact_title.id
					WHERE contact_id='".$contact_id."' AND active=1");
			$count=0;
			while($contacts->next()){
				$c_line=array(
					'id'			=> $contacts->f('contact_id'),
					'name'			=> $contacts->f('name'),
					'firstname'		=> $contacts->f('firstname'),
					'lastname'		=> $contacts->f('lastname'), 
					'title'			=> $contacts->f('title')?$contacts->f('title'):'', 
				);
				array_push($result['contacts'], $c_line);
				$count++;
			}
			if($count>0){
				$with_names=1;
			}
		}else if($buyer_id!='0'){
			$contacts = $db->query("SELECT contact_id,firstname,lastname, CONCAT_WS(' ',firstname,lastname) as name,  customer_contact_title.name AS title
					FROM customer_contacts
					LEFT JOIN customer_contact_title ON customer_contacts.title = customer_contact_title.id
					WHERE customer_id='".$buyer_id."' AND active=1 ORDER BY lastname");
			$count=0;
			while($contacts->next()){
				$c_line=array(
					'id'			=> $contacts->f('contact_id'),
					'name'			=> $contacts->f('name'),
					'firstname'		=> $contacts->f('firstname'),
					'lastname'		=> $contacts->f('lastname'),
					'title'			=> $contacts->f('title')?$contacts->f('title'):'', 
				);
				array_push($result['contacts'], $c_line);
				$count++;
			}
			if($count>0){
				$with_names=1;
			}
		}
	}

//$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

$default_lang_id = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
$default_pdf_type = ACCOUNT_INVOICE_PDF_FORMAT;

	$result['lid']				= $default_lang_id;
	$result['type']				= $default_pdf_type;
	$result['subject']         		= $in['subject'] ? $in['subject'] : $subject;
	$result['has_contacts']			= $with_names>0 ? true : false;
	$result['buyer_id']			= $buyer_id;
	$result['USER_EMAIL']	  		= $user_email;

if($with_names==0){
	$subject=str_replace('[!FIRST_NAME!]',"", $subject);
	$subject=str_replace('[!LAST_NAME!]',"", $subject);
	$body=str_replace('[!FIRST_NAME!]',"", $body);
	$body=str_replace('[!LAST_NAME!]',"", $body);
}

	$result['e_message']       		= $in['e_message'] ? utf8_decode($in['e_message']) : utf8_decode($body);
	$result['e_messages']      		= $in['e_message'] ? $in['e_message'] : $body;

//$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php';
if($buyer_id && $buyer_id != '0'){
	$inv_email = trim($db->field("SELECT invoice_email FROM customers WHERE customer_id='".$buyer_id."' "));
	$invoice_email=explode(";", $inv_email);
	$c_email = $db->field("SELECT c_email FROM customers WHERE customer_id='".$buyer_id."' ");
	if($inv_email){
		$result['inv_email']			= true;
		//$result['invoice_email']		= $invoice_email;
	}
	$inv_reminders_email = trim($db->field("SELECT invoice_reminders_email FROM customers WHERE customer_id='".$buyer_id."' "));
	$invoice_reminders_email=explode(";", $inv_reminders_email);
	$result['isbuyer']			= true;
	//$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php?customer_id='.$buyer_id;


	/*$tblinvoice_customer_contacts = $db->query("SELECT * FROM customer_contacts
				 WHERE customer_id='".$buyer_id."' AND active ='1'");*/
	$tblinvoice_customer_contacts = $db->query("SELECT * FROM customer_contacts
												LEFT JOIN customer_contactsIds
												ON customer_contacts.contact_id=customer_contactsIds.contact_id
												LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				 WHERE customers.customer_id='".$buyer_id."' AND customer_contacts.active ='1'");

	$result['recipients']=array();

	while($tblinvoice_customer_contacts->move_next()){
		if((is_array($invoice_email) && in_array($tblinvoice_customer_contacts->f('email'), $invoice_email)) || (!is_array($invoice_email) && $tblinvoice_customer_contacts->f('email') == $invoice_email) || $tblinvoice_customer_contacts->f('email') == $c_email || (is_array($invoice_reminders_email) && in_array($tblinvoice_customer_contacts->f('email'), $invoice_reminders_email)) || (!is_array($invoice_reminders_email) && $tblinvoice_customer_contacts->f('email') == $invoice_reminders_email) ) {
			continue;
		}
		if($tblinvoice_customer_contacts->f('email')){
			$recipients=array(
				'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
			);
			array_push($result['recipients'], $recipients);
		}		
	}
		if($inv_reminders_email){
			if(is_array($invoice_reminders_email)){
				foreach($invoice_reminders_email as $value){
					array_push($result['recipients'], array(
						'recipient_email' 			=> $value
					));
				}
			}else{
				array_push($result['recipients'], array(
					'recipient_email' 			=> $invoice_reminders_email
				));
			}
		}else if($inv_email){
			if(is_array($invoice_email)){
				foreach($invoice_email as $value){
					array_push($result['recipients'], array(
						'recipient_email' 			=> $value
					));
				}
			}else{
				array_push($result['recipients'], array(
					'recipient_email' 			=> $invoice_email
				));
			}
		}		

		array_push($result['recipients'], array(
			'recipient_email' 			=> $c_email
		));
}

	$user_loged_email = $dbu_users->field("SELECT email FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

	$result['user_loged_email'] 		= $user_loged_email;

	$result['contact_email_data'] 		= $inv_reminders_email ? $invoice_reminders_email : ($inv_email ? $invoice_email : array());
	$result['contact_id_data']			= $buyer_id;
	$result['contact_name_data']			= $inv_reminders_email ? $invoice_reminders_email : ($inv_email ? $invoice_email : array());
	$result['auto_complete_small']		= '<input type="text" class="auto_complete_small" name="email_addr" value="" data-autocomplete_file="'.$autocomplete_path.'" />';
	//$result['autocomplete_file']			= $autocomplete_path;
	$result['view']             		 = $in['view'];
    $result['offset']           		 = $in['offset'];
    $result['archived']         		 = $in['archived'];
    $result['search']           		 = $in['search'];

    $mail_setting_option = $db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
	$result['sendgrid_selected'] = false;
	if($mail_setting_option==3){
		$result['sendgrid_selected'] =true;
	}


json_out($result);
?>