<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$result=array();

if(!$in['languages']){
	$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND default_lang='1' ORDER BY sort_order limit 1");
}else{
	if($in['languages']>=1000) {
		$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."'  ");
	} else {
		$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
	}
}

$default_values1= $db->query("SELECT * FROM tblinvoice_grades WHERE grade='first' LIMIT 1");
$default_values2= $db->query("SELECT * FROM tblinvoice_grades WHERE grade='second' LIMIT 1");
$default_values3= $db->query("SELECT * FROM tblinvoice_grades WHERE grade='third' LIMIT 1");

$in['lang_code'] = $code;

switch ($in['lang_code'])
{
	case 'en': $result['translate_cls'] 	= 'form-language-en'; break;
	case 'fr': $result['translate_cls']		= 'form-language-fr'; break;
	case 'du': $result['translate_cls']		= 'form-language-nl'; break;
}
if($in['languages']>=1000) {
	$result['translate_cls'] 			= 'form-language-'.$in['lang_code'];
}


	$name = 'invmess_late';
	$text = 'Dear Customer: [!CUSTOMER!],

	We want to remind you about this late invoices that still needs to be payed.

	[!INVOICE_SUMMARY!]

	Best regards
	';
    $subject = 'Late invoice summary';
    $sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
    $lang_pre=$_SESSION['l'];
    if($lang_pre=='nl'){
        $lang_pre='du';
    }
    $lang_id=$db->field("SELECT lang_id FROM pim_lang WHERE code='".$lang_pre."' ");
	if(!$sys_message_invmess->move_next()){
	  $insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
	    $result['subject']         	= $subject;
	    $result['text']            	= $text;	    	   
	}else{
	    $result['subject']         	= $sys_message_invmess->f('subject');
	    $result['text']            	= $sys_message_invmess->f('text');
	    $result['footnote']	    		= $sys_message_invmess->f('footnote');
	}

$result['tabs']=array();

$first_grade = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='first' AND lang_code='".$in['lang_code']."' LIMIT 1");
$message = $db->field("SELECT message FROM tblinvoice_grades WHERE grade='first' AND lang_code='".$in['lang_code']."'");
if($first_grade->next())
{
		$tab=array(
			'penalty_value'			=> display_number($first_grade->f('value')),
			'penalty_type'			=> $first_grade->f('type'),
			'text'				=> $message,
			'title'				=> $first_grade->f('title'),
			'header'				=> gm('First grade Message'),
		);
		array_push($result['tabs'], $tab);
	
}else{
		$tab=array(
			'penalty_value'			=> display_number($default_values1->f('value')),
			'penalty_type'			=> $default_values1,
			'header'				=> gm('First grade Message'),
		);
		array_push($result['tabs'], $tab);		
}
$second_grade = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='second' AND lang_code='".$in['lang_code']."' LIMIT 1");
$message = $db->field("SELECT message FROM tblinvoice_grades WHERE grade='second' AND lang_code='".$in['lang_code']."'");
if($second_grade->next())
{
		$tab=array(
			'penalty_value'			=> display_number($second_grade->f('value')),
			'penalty_type'			=> $second_grade->f('type'),
			'text'				=> $message,
			'title'				=> $second_grade->f('title'),
			'header'				=> gm('Second grade Message'),
		);
		array_push($result['tabs'], $tab);
}else{
		$tab=array(
			'penalty_value'			=> display_number($default_values2->f('value')),
			'penalty_type'			=> $default_values2->f('type'),
			'header'				=> gm('Second grade Message'),
		);		
		array_push($result['tabs'], $tab);
}
$third_grade = $db->query("SELECT * FROM tblinvoice_grades WHERE grade='third' AND lang_code='".$in['lang_code']."' LIMIT 1");
$message = $db->field("SELECT message FROM tblinvoice_grades WHERE grade='third' AND lang_code='".$in['lang_code']."'");
if($third_grade->next())
{
		$tab=array(
			'penalty_value'			=> display_number($third_grade->f('value')),
			'penalty_type'			=> $third_grade->f('type'),
			'text'				=> $message,
			'title'				=> $third_grade->f('title'),
			'header'				=> gm('Third grade Message'),
		);
		array_push($result['tabs'], $tab);
}else{
		$tab=array(
			'penalty_value'			=> display_number($default_values3->f('value')),
			'penalty_type'			=> $default_values3->f('type'),
			'header'				=> gm('Third grade Message'),
		);	
		array_push($result['tabs'], $tab);
}

	$result['language_dd']       			= $in['languages'] ? build_language_dd_new($in['languages']) : build_language_dd_new($lang_id);
	$result['languages']				= $in['languages'] ? $in['languages'] : $lang_id;
	$result['lang_code']				= $in['lang_code'];
	$result['checked_grades']			= USE_GRADES==1 ? true : false;
	$result['style']     				= ACCOUNT_NUMBER_FORMAT;
	$result['name']					= $name;


json_out($result);
?>