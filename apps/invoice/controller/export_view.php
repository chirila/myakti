<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $config;
if(!$in['list_id']){
	return ark::run('invoice-export_csvs');
}

ini_set('memory_limit','1G');

$result['title']=$db->field("SELECT name FROM tblinvoice_export_list WHERE list_id='".$in['list_id']."' ");

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(empty($_SESSION['invoice_to_export'])){
		$_SESSION['invoice_to_export'] = time();
	}
}else{
	$_SESSION['invoice_to_export'] = time();
}
$db->query("DELETE FROM tblinvoice_to_export WHERE time < '".(time()-3600)."' ");
$l_r = ROW_PER_PAGE;

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$arguments_o='';
$arguments_s='';
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$arguments='&list_id='.$in['list_id'];
$filter_link = 'block';
$order_by = " ORDER BY t_date DESC, iii DESC ";
if($in['type']=='0'){
	if($in['archived']=='0' || !$in['archived']){
		$join = "LEFT JOIN ";
		$filter = " WHERE tblinvoice.sent=1 AND tblinvoice.f_archived='0' AND tblinvoice_exported.invoice_id IS NULL  ";
	}else{
		$join = "INNER JOIN ";
		$filter = " WHERE tblinvoice.sent=1 AND tblinvoice.f_archived='0' ";
	}
	if(!empty($in['search'])){
		$filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' )";
	}
	if(!empty($in['order_by'])){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
	}
	$tblinvoice = $db->query("SELECT tblinvoice.*, tblinvoice.serial_number as iii, cast( FROM_UNIXTIME( tblinvoice.`invoice_date` ) AS DATE ) AS t_date
			FROM tblinvoice
			{$join} tblinvoice_exported ON tblinvoice.id=tblinvoice_exported.invoice_id AND tblinvoice_exported.list_id='".$in['list_id']."'
			{$filter} {$order_by} ");
}else{
	if($in['archived']=='0' || !$in['archived']){
		$join = "LEFT JOIN ";
		$filter = " WHERE tblinvoice_incomming.type='0' AND tblinvoice_exported.invoice_id IS NULL ";
	}else{
		$join = "INNER JOIN ";
		$filter = " WHERE tblinvoice_incomming.type='0' AND tblinvoice_exported.type='1' ";
	}
	if(!empty($in['search'])){
		$filter.=" AND (tblinvoice_incomming.invoice_number LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%' )";
	}
	if(!empty($in['order_by'])){
		$order = " ASC ";
		if($in['desc']=='true'){
			$order = " DESC ";
		}
		if($in['order_by']=='serial_number'){
			$order_by =" ORDER BY invoice_number ".$order;
		}else if($in['order_by']=='buyer_name'){
			$order_by =" ORDER BY name ".$order;
		}else{
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
		}		
	}
	$tblinvoice = $db->query("SELECT tblinvoice_incomming.*, tblinvoice_incomming.invoice_number as iii, cast( FROM_UNIXTIME( tblinvoice_incomming.`invoice_date` ) AS DATE ) AS t_date, customers.name
			FROM tblinvoice_incomming
			{$join} tblinvoice_exported ON tblinvoice_incomming.invoice_id=tblinvoice_exported.invoice_id AND tblinvoice_exported.list_id='".$in['list_id']."'
			INNER JOIN customers ON tblinvoice_incomming.supplier_id=customers.customer_id
			{$filter} {$order_by} ");
}
$result['query']=array();
$max_rows=$tblinvoice->records_count();
$result['max_rows']=$max_rows;
$tblinvoice->move_to($offset*$l_r);
$j=0;
$color = '';
$all_invoices_id = '';
if($in['type']=='0'){
	while($tblinvoice->move_next() && $j<$l_r){
		$is = $db->field("SELECT count(invoice_id) FROM tblinvoice_to_export WHERE time='".$_SESSION['invoice_to_export']."' AND invoice_id='".$tblinvoice->f('id')."' ");
		$item=array(
			'info_link'     			=> 'index.php?do=invoice-invoice&invoice_id='.$tblinvoice->f('id'),
			'mark_as_sent_link'		=> 'index.php?do=invoice-export_view_list-invoice-markExport&invoice_id='.$tblinvoice->f('id').'&list_id='.$in['list_id'],
			'our_reference'			=> $tblinvoice->f('our_ref'),
			'id'                		=> $tblinvoice->f('id'),
			's_number'		     		=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
			'created'           		=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
			'buyer_name'        		=> $tblinvoice->f('buyer_name'),
			'text'				=> $tblinvoice->f('created_by') == 'System' ? gm('Created from a recurring invoice') : '',
			'checked'				=> $is == 1 ? true : false,
			'delete_link'			=> array('do'=>'invoice-export_view-invoice-markExport','invoice_id'=>$tblinvoice->f('id'),'type'=>$in['type']),
		);
		array_push($result['query'], $item);
		$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('id');
		$j++;
	}
	$do_next='invoice-export_selected_invoices_csv';
	$export_link='index.php?do=invoice-export_selected_invoices_csv&list_id='.$in['list_id'];
}else{
	while($tblinvoice->move_next() && $j<$l_r){
		$is = $db->field("SELECT count(invoice_id) FROM tblinvoice_to_export WHERE time='".$_SESSION['invoice_to_export']."' AND invoice_id='".$tblinvoice->f('invoice_id')."' ");
		$item=array(
			'info_link'     			=> 'index.php?do=invoice-incomming_invoice&invoice_id='.$tblinvoice->f('invoice_id'),
			'mark_as_sent_link'		=> 'index.php?do=invoice-export_view_list-invoice-markExport&invoice_id='.$tblinvoice->f('invoice_id').'&list_id='.$in['list_id'].'&type=1',
			'our_reference'			=> $tblinvoice->f('our_ref'),
			'id'                		=> $tblinvoice->f('invoice_id'),
			's_number'		     		=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
			'created'           		=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
			'buyer_name'        		=> $tblinvoice->f('name'),
			'text'				=> $tblinvoice->f('created_by') == 'System' ? gm('Created from a recurring invoice') : '',
			'checked'				=> $is == 1 ? true : false,
			'delete_link'			=> array('do'=>'invoice-export_view-invoice-markExport','invoice_id'=>$tblinvoice->f('invoice_id'),'type'=>$in['type']),
		);
		array_push($result['query'], $item);
		$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('invoice_id');
		$j++;
	}
	$do_next='invoice-export_incoming_inv_csv';
	$export_link='index.php?do=invoice-export_incoming_inv_csv&list_id='.$in['list_id'];
}

	$result['lr'] 				= $l_r;
	$result['do_next']			= $do_next;
	$result['export_link']			= $export_link;

	json_out($result);
?>