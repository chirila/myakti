<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	session_write_close();
	global $config;
	$info = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['id']."' ");

	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);

	$link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
	$content = file_get_contents($link);
	$name='purchase_invoice_'.$in['id'].'.pdf';
	if($content){
		doQueryLog();
		if($in['just_download']){
			$ext = explode('.',$info->f('file_name'));
			$c_type = 'application/pdf';
			$fileTypes = array('jpg','jpeg','png');
			if(in_array(strtolower($ext['1']),$fileTypes)){
			   $c_type='image/jpeg';
			}

			header('Content-Type: $c_type');

			$name='purchase_invoice_'.$in['id'].'.'.$ext['1'];
			header("Content-Disposition:attachment;filename=".$name);
			print_r($content);
			exit;
		}
		header('Content-Type: application/pdf');
		header("Content-Disposition:inline;filename=".$name);
		header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		echo $content;
		exit();
	}else{
		echo '';
		exit();
	}
?>