<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$o=array();
	
	$tblinvoice=$db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."' ");
	$payment = $db->query("SELECT tblinvoice_payments.*,ROUND(cast(amount AS decimal(12, 4))+0.0012,2) AS formatted_amount FROM tblinvoice_payments WHERE payment_id='".$in['payment_id']."' ");
	 if($in['payment_date']){
	 	$o['payment_date']  = $in['payment_date'];
	 }else{
	 	$o['payment_date']  = $payment->f('date');
	 }
	if($in['total_amount_due1']){
	 	$o['total_amount_due1']  = $in['total_amount_due1'];
	 }else{
	 	$o['total_amount_due1']  = display_number($payment->f('formatted_amount'));
	 }
	if($in['cost']){
	 	$o['cost']  		= $in['cost'];
	 }else{
	 	$info_split = explode('  -  ',$payment->f('info'));
	 	$o['cost']  		= display_number($info_split[3]);
	 }
	if($in['notes']){
	 	$o['notes']  		= $in['notes'];
	 }else{
	 	$info_split = explode('  -  ',$payment->f('info'));
	 	$o['notes']  		= $info_split[2];
	 }
	 if($in['payment_method_dd']){
	 	$o['payment_method_dd']  = $in['payment_method_dd'];
	 }else{
	 	$o['payment_method_dd']  = invoice_payment_method_dd($payment->f('payment_method'));
	 	$o['payment_method'] = $payment->f('payment_method');
	 }
	 if($in['not_paid']){
	 	$o['not_paid']  		= $in['not_paid'];
	 }else{
	 	$o['not_paid']  		= $tblinvoice->f('not_paid')==1? true : false;
	 }
	 if($in['check_not_paid']){
	 	$o['check_not_paid']  		= $in['check_not_paid'];
	 }else{
	 	$o['check_not_paid']  		= $tblinvoice->f('not_paid')==1? true : false;
	 }

	$o['invoice_id']			= $in['invoice_id'];
	$o['payment_id']			= $in['payment_id'];

json_out($o);
?>
