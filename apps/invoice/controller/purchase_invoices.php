<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

if($in['reset_list']){
 	if(isset($_SESSION['tmp_add_to_purchase'])){
 		unset($_SESSION['tmp_add_to_purchase']);
 	}
 	if(isset($_SESSION['add_to_purchase'])){
		unset($_SESSION['add_to_purchase']);
 	}
}

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$result=array();
$l_r = ROW_PER_PAGE;

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

$arguments_o='';
$arguments_s='';
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$arguments='';
$filter = "WHERE 1=1 ";
$filter_link = 'block';
$order_by = " ORDER BY  iii DESC ";
$order_by_array = array('booking_number','invoice_number','t_date','due_date', 'buyer_name', 'total', 'total_with_vat', 'balance', 'expense_category','reference','last_update');


if(!empty($in['filter'])){
	$arguments.="&filter=".$in['filter'];
}
if(!empty($in['search'])){
	$filter.=" AND (tblinvoice_incomming.invoice_number LIKE '%".$in['search']."%' OR tblinvoice_incomming.booking_number LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['expense_category_id'])){
	$filter.=" AND tblinvoice_incomming.expense_category_id = '".$in['expense_category_id']."' ";
}
$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

if(!empty($in['order_by'])){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc']==1 || $in['desc']=='true'){
			$order = " DESC ";
		}
		if($in['order_by']=='balance' || $in['order_by']=='buyer_name' || $in['order_by'] == 'last_update'){
			$order_by='';
			$filter_limit =' ';
		}else{
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
		}
	
	}	
}

if(!isset($in['view'])){
	$in['view'] = 0;
}

if($in['view'] == 1){
	$filter.=" and tblinvoice_incomming.paid = '0' and tblinvoice_incomming.type != '2' ";
	$arguments.="&view=1";
	
}
if($in['view'] == 2){
	$filter.=" and tblinvoice_incomming.paid = '0' and tblinvoice_incomming.status='1' and tblinvoice_incomming.due_date<'".time()."' ";
	$arguments.="&view=2";
	
}
if($in['view'] == 3){
	$filter.=" and tblinvoice_incomming.paid = '1'";
	$arguments.="&view=3";
	
}
if($in['view'] == 4){
	$filter.=" and tblinvoice_incomming.type = '2'";
	$arguments.="&view=4";
	
}
if($in['view'] == 5){
	$filter.=" and tblinvoice_incomming.status = '0' AND paid='0' ";
	$arguments.="&view=5";
	
}
if($in['view'] == 6){
	$filter.=" and tblinvoice_incomming.status = '1' AND paid='0' ";
	$arguments.="&view=6";
	
}

if($in['view'] == 7){
	$filter.=" and ((tblinvoice_incomming.status = '0' AND paid='0') OR (tblinvoice_incomming.paid = '0' and tblinvoice_incomming.type != '2') ) ";
	$arguments.="&view=7";
	
}

// }
if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
if(!empty($in['start_date']) && !empty($in['stop_date'])){
    $filter.=" and tblinvoice_incomming.invoice_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if(!empty($in['start_date'])){
	$filter.=" and cast(tblinvoice_incomming.invoice_date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if(!empty($in['stop_date'])){
	$filter.=" and cast(tblinvoice_incomming.invoice_date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}

if(empty($in['archived'])){
	$filter.= " AND tblinvoice_incomming.f_archived='0' ";
}else{
	$filter.= " AND tblinvoice_incomming.f_archived='1' ";
}

if(isset($in['doc_type']) && $in['doc_type']!=-1 ){
    $filter.=" and tblinvoice_incomming.type = '".$in['doc_type']."'  ";
}

$nav=array();

$arguments = $arguments.$arguments_s;
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;

$max_rows_data = $db->query("SELECT tblinvoice_incomming.invoice_id,  booking_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date,tblinvoice_expense_categories.name as expense_category
			FROM tblinvoice_incomming 
			LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id 
			LEFT JOIN tblinvoice_expense_categories ON tblinvoice_incomming.expense_category_id = tblinvoice_expense_categories.id
			".$filter." ".$order_by);
$max_rows=$max_rows_data->records_count();
if(!$_SESSION['tmp_add_to_purchase'] || ($_SESSION['tmp_add_to_purchase'] && empty($_SESSION['tmp_add_to_purchase']))){
 	while($max_rows_data->next()){
 		$_SESSION['tmp_add_to_purchase'][$max_rows_data->f('invoice_id')]=1;
 		array_push($nav, (object)['invoice_id'=> $max_rows_data->f('invoice_id') ]);
	}
}else{
	while($max_rows_data->next()){
 		array_push($nav, (object)['invoice_id'=> $max_rows_data->f('invoice_id') ]);
	}
}
$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_to_purchase']){
 	if($max_rows>0 && count($_SESSION['add_to_purchase']) == $max_rows){
 		$all_pages_selected=true;
 	}else if(count($_SESSION['add_to_purchase'])){
 		$minimum_selected=true;
 	}
}

$tblinvoice = $db->query("SELECT tblinvoice_incomming.*,customers.name as seller_name, booking_number as iii, 
	cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, tblinvoice_expense_categories.name as expense_category
			FROM tblinvoice_incomming
			LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id 
			LEFT JOIN tblinvoice_expense_categories ON tblinvoice_incomming.expense_category_id = tblinvoice_expense_categories.id 
			".$filter.$order_by.$filter_limit);

//$max_rows=$tblinvoice->records_count();
$result = array('query'=>array(),'max_rows'=>$max_rows,'all_pages_selected'=> $in['exported'] ? false : $all_pages_selected,'minimum_selected'=> $in['exported'] ? false : $minimum_selected, 'nav'=> $nav);
//$tblinvoice->move_to($offset*$l_r);
$j=0;
$color = '';
$all_invoices_id = '';


while($tblinvoice->move_next() ){
	
	$status='';
	$status_title='';
	$type='';
	$type_title='';
	$color='';
	$partial_paid=$db->field("SELECT payment_id FROM tblinvoice_incomming_payments WHERE invoice_id='".$tblinvoice->f('invoice_id')."'");
	/*switch ($tblinvoice->f('type')){
		case '0':
			$type = 'regular_invoice';
			$type_title = gm('Regular invoice');
			break;
		case '1':
			$type = 'pro-forma_invoice';
			$type_title = gm('Proforma invoice');
			break;
		case '2':
			$type = 'credit_invoice';
			$type_title = gm('Credit Invoice');
			break;
	}*/

	switch ($tblinvoice->f('paid')){
		case '0':
					// $status = 'Not Paid';
					$status='';
					$status_title= gm('Not Paid');
					$color = '';
			if($tblinvoice->f('due_date') < time()){
					// $status = 'Late';
					$status = 'late';
					$status_title= gm('Late');
					$color = 'late';
             }elseif($partial_paid){
			        $status='partial';
					$status_title= gm('Partially Paid');
					$color = '';
					$class='';
				}
			break;
		case '1':
			// $status = 'partial Paid';
				$color = '';
				$status='paid';
				$status_title= gm('Paid');
			break;
		/*case '2':
			// $status = 'Fully Paid';
			if($tblinvoice->f('type') != '2' ){
				$color = '';
				$status='paid';
				$status_title= gm('Paid');
			}
			break;*/
	}
	switch($tblinvoice->f('status')){
		/*case '0':
			$nr_status=$tblinvoice->f('paid') ? 3 : 1;
			$type_title=$tblinvoice->f('paid') ? gm('Paid') : gm('Uploaded');
			break;*/
		case '1':
			$nr_status=$tblinvoice->f('paid') ? 3 : 2;
			$type_title=$tblinvoice->f('paid') ? ($tblinvoice->f('paid')==2 ? gm('Partially Paid') : gm('Paid')) : gm('Approved');
			break;
		default:
			$nr_status=1;
			$type_title=gm('Uploaded');
			break;
	}
	$customer=$db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('supplier_id')."'");
	if(!$tblinvoice->f('supplier_id')){
		$customer=$db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id ='".$tblinvoice->f('contact_id')."'");
	}

	if($tblinvoice->f('created_by') == 'System'){
		$color .= ' recurring';
	}
	$late = false;
	if($tblinvoice->f('paid') == '0' && $tblinvoice->f('status') == '1' && $tblinvoice->f('due_date') < time()){
		$late = true;
		$nr_status=4;
		$type_title=gm('Late');
	}
	$a_transfer = false; //This invoice will be paid by automatic transfer
	if($tblinvoice->f('a_transfer') == '1'){
		$a_transfer = true;
	}

	//calculate balance
	$total = $tblinvoice->f('total_with_vat'); 
	$already_payed = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$tblinvoice->f('invoice_id')."'  ");
	$total_payed = $already_payed->f('total_payed');
	$amount_due = round($total - $total_payed,2);

	$last_update = $db->field("SELECT date FROM  `logging` WHERE `pag`='purchase_invoice' AND `field_name` =  'purchase_invoice_id' AND  `field_value` =  '".$tblinvoice->f('invoice_id')."' ORDER BY  `logging`.`date` DESC LIMIT 1");

	$item=array(
		
		
		'info_link'     		=> 'index.php?do=invoice-incomming_invoice&invoice_id='.$tblinvoice->f('invoice_id'),
		'edit_link'     		=> 'index.php?do=invoice-ninvoice&invoice_id='.$tblinvoice->f('invoice_id'),
		
	
	   //'delete_link'  			=>array('do'=>'invoice-incomming_invoices-invoice-incomming_invoices_delete', 'id'=>$tblinvoice->f('invoice_id')),
		'delete_link'  			=>array('do'=>'invoice-purchase_invoices-invoice-incomming_invoices_delete', 'id'=>$tblinvoice->f('invoice_id')),
		
		
		'can_activate'			=> $tblinvoice->f('f_archived')==1 ? true : false,
		'can_pay'				=> !$tblinvoice->f('status') || ($tblinvoice->f('status') && $tblinvoice->f('paid') != 1) ? true: false,
		'type'					=> $type,
		'type_title'			=> $type_title,
		'status'				=> $status,
		'status_title'			=> $status_title,
		'invoice_property'		=> $color,
		'amount'				=> place_currency(display_number($tblinvoice->f('total')),get_commission_type_list($tblinvoice->f('currency_type'))),
		'amount_vat'			=> place_currency(display_number($tblinvoice->f('total_with_vat')),get_commission_type_list($tblinvoice->f('currency_type'))),
		'our_reference'			=> $tblinvoice->f('our_ref'),
		'id'                	=> $tblinvoice->f('id'),
		's_number'		     	=> $tblinvoice->f('invoice_number'),
		'b_number'		     	=> $tblinvoice->f('booking_number'),
		'created'           	=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
		'due_date'           	=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('due_date')),
		'seller_name'       	=> $tblinvoice->f('seller_name'),
        'i_number'              => $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
		'buyer_name'        	=> $customer,
		'buyer_name_ord'        => $customer,
		
		'invoice_id'			=> $tblinvoice->f('invoice_id'),
		'id'					=> $tblinvoice->f('invoice_id'),

		'check_add_to_product'	=> $in['exported'] ? false : ($_SESSION['add_to_purchase'][$tblinvoice->f('invoice_id')] == 1 ? true : false),
		'nr_status'				=> $nr_status,
		'late'					=> $late,
		'a_transfer'			=> $a_transfer,
		'balance'				=> ($tblinvoice->f('paid') == '1' || ($tblinvoice->f('status') == '0' && !$tblinvoice->f('paid')) || $tblinvoice->f('type') == '2')? '' : place_currency(display_number($amount_due),get_commission_type_list($tblinvoice->f('currency_type'))),
		'balance_ord'			=> ($tblinvoice->f('paid') == '1' || ($tblinvoice->f('status') == '0' && !$tblinvoice->f('paid')) || $tblinvoice->f('type') == '2')? '' : $amount_due,
		'expense_category_id' 	=>$tblinvoice->f('expense_category_id'),
		'expense_category'      => $tblinvoice->f('expense_category'),
		'ogm'					=> $tblinvoice->f('ogm'),
		'reference'				=> stripslashes($tblinvoice->f('reference')),
		'last_update'				=> date(ACCOUNT_DATE_FORMAT,$last_update),
		'last_update_ord'			=> $last_update,
        'doc_type'              => $tblinvoice->f('type') ? (($tblinvoice->f('type') == 1) ? gm('Proforma Invoice')  : gm('Credit Invoice')) : gm('Regular Invoice'),
	);
	
    array_push($result['query'], $item);

	$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('invoice_id');
	
	$j++;

}
$all_invoices_id = trim($all_invoices_id,',');



$args_v = preg_replace('/&view=([0-9]+)/','',$arguments);

$default_lang_id = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
$default_pdf_type = ACCOUNT_INVOICE_PDF_FORMAT;

	$result['expense_categories']    = build_expense_category_dd('',1);	
	$result['view_go'] 	= $_SESSION['access_level'] == 1 ? '' : 'hide';
	$result['args']	    = $args_v;
	$result['lid']      = $default_lang_id;
	$result['pdf_type']	= $default_pdf_type;
	$result['all_invoices_id']=$all_invoices_id;
	$result['lr'] = $l_r;

	if($in['order_by']=='balance' || $in['order_by']=='buyer_name' || $in['order_by']=='last_update'){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by']."_ord", SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by']."_ord", SORT_DESC);
	    }
	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	    
	}


$_SESSION['filters'] = $arguments.$arguments_o;

$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
	if($easyinvoice=='1'){
		$result['show_deliveries']	 	=false;
		$result['show_projects']		= false;
		$result['show_interventions']	= false;
	}else{
		$result['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
		$result['show_projects']		= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
		$result['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
	}

	$result['columns']  	= get_p_invoices_cols($in,true,false);

	$result['types']=build_incinv_type_dd();
    array_unshift($result['types'],array('id' => "-1" , 'name' => gm("All types")));


json_out($result);

function get_Amounts(&$in,$showin=true,$exit=true){
    ini_set('memory_limit','1G');
    $db= new sqldb();
    $output=array();
    $output['inv_data']         = array();

    $now = time();
    $invoicesarr = array();
    $lateinvarr = array();
    $invs = $db->query("SELECT total,invoice_id,total_with_vat, due_date FROM tblinvoice_incomming WHERE paid!='1' AND type!='2' AND type!='1' AND c_invoice_id='0' AND status='1' ");
    while ($invs->next()) {
        $invoicesarr[$invs->f('invoice_id')] = array($invs->f('total'),$invs->f('total_with_vat'));
        if($invs->f('due_date') < $now){
        $lateinvarr[$invs->f('invoice_id')] = array($invs->f('total'),$invs->f('total_with_vat'));
        }
    }

    $paysarr = array();
    $paylatesarr = array();
    $invs = $db->query("SELECT tblinvoice_incomming_payments.amount,tblinvoice_incomming_payments.invoice_id FROM tblinvoice_incomming_payments
        INNER JOIN tblinvoice_incomming ON tblinvoice_incomming_payments.invoice_id=tblinvoice_incomming.invoice_id
        WHERE tblinvoice_incomming.paid!='1' AND tblinvoice_incomming.type!='2' AND tblinvoice_incomming.type!='1' AND tblinvoice_incomming.c_invoice_id='0' AND tblinvoice_incomming.status='1' ");
    while ($invs->next()) {
        $paysarr[$invs->f('invoice_id')] += $invs->f('amount');
    }

    $totalNoVat = 0;
    $totalWithVat = 0;
    foreach ($invoicesarr as $key => $value) {
        if($value[0]-$paysarr[$key] >0){
        $totalNoVat+=$value[0]-$paysarr[$key];
        }
        if($value[1]-$paysarr[$key] >0){
        $totalWithVat+=$value[1]-$paysarr[$key];
        }
    }
    #lates
    $totalLateNoVat = 0;
    $totalLateWithVat = 0;
    foreach ($lateinvarr as $key => $value) {
        if($value[0]-$paysarr[$key] >0){
        $totalLateNoVat+=$value[0]-$paysarr[$key];
        }
        if($value[1]-$paysarr[$key] >0){
        $totalLateWithVat+=$value[1]-$paysarr[$key];
        }
    }

    $f_d_month = mktime(0,0,0,date('n'),1,date('Y'));
    $l_d_month = mktime(23,59,59,date('n')+1,0,date('Y'));
    $invoice_amount_month = $db->query("SELECT total AS a, invoice_id AS c, total_with_vat AS av, type, c_invoice_id FROM tblinvoice_incomming WHERE invoice_date BETWEEN '".$f_d_month."' AND '".$l_d_month."' AND status>'0' ")->getAll();
    $invoice_amount_month_a = array('a'=>0,'c'=>0,'av'=>0);
    $credit_amount_month_a = array('a'=>0,'c'=>0,'av'=>0);
    foreach ($invoice_amount_month as $key => $value) {
        if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
        $invoice_amount_month_a['a']+=$value['a'];
        $invoice_amount_month_a['c']++;
        $invoice_amount_month_a['av']+=$value['av'];
        }
        if($value['type'] == '2'){ # credit invoices
        $credit_amount_month_a['a']+=$value['a'];
        $credit_amount_month_a['c']++;
        $credit_amount_month_a['av']+=$value['av'];
        }
    }

    $f_d_last = mktime(0,0,0,date('n')-1,1,date('Y'));
    $l_d_last = mktime(23,59,59,date('n'),0,date('Y'));
    $invoice_amount_last = $db->query("SELECT total AS a, invoice_id AS c, total_with_vat AS av, type, c_invoice_id FROM tblinvoice_incomming WHERE invoice_date BETWEEN '".$f_d_last."' AND '".$l_d_last."' AND status>'0' ")->getAll();
    // AND type!='2' AND type!='1' AND c_invoice_id='0'
    $invoice_amount_last_a = array('a'=>0,'c'=>0,'av'=>0);
    $credit_amount_last_a = array('a'=>0,'c'=>0,'av'=>0);
    foreach ($invoice_amount_last as $key => $value) {
        if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
        $invoice_amount_last_a['a']+=$value['a'];
        $invoice_amount_last_a['c']++;
        $invoice_amount_last_a['av']+=$value['av'];
        }
        if($value['type'] == '2'){ # credit invoices
        $credit_amount_last_a['a']+=$value['a'];
        $credit_amount_last_a['c']++;
        $credit_amount_last_a['av']+=$value['av'];  
        }
    }

    $output['inv_data']['amount_to_be_inv']=place_currency(display_number($totalNoVat));
    $output['inv_data']['amount_to_be_inv_with_btw']=place_currency(display_number($totalWithVat));
    $output['inv_data']['amount_to_be_inv_late']=place_currency(display_number($totalLateNoVat));
    $output['inv_data']['amount_to_be_inv_late_with_btw']=place_currency(display_number($totalLateWithVat));
    $output['inv_data']['amount_to_be_inv_last']=place_currency(display_number($invoice_amount_last_a['a']));
    $output['inv_data']['amount_to_be_inv_month']=place_currency(display_number($invoice_amount_month_a['a']));
    $output['inv_data']['amount_to_be_inv_last_with_btw']=place_currency(display_number($invoice_amount_last_a['av']));
    $output['inv_data']['amount_to_be_inv_month_with_btw']=place_currency(display_number($invoice_amount_month_a['av']));
    $output['inv_data']['use_vat_invoices_kpi']         = defined('USE_VAT_P_INVOICE_KPI') && USE_VAT_P_INVOICE_KPI == 1 ? true :false;

    return json_out($output, $showin,$exit);
}
function get_p_invoices_cols(&$in,$showin=true,$exit=true){
	$db=new sqldb();
	$array=array();
	$cols_default=default_columns_dd(array('list'=>'purchase_invoices'));
	$cols_order_dd=default_columns_order_dd(array('list'=>'purchase_invoices'));
	$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='purchase_invoices' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
	if(!count($cols_selected)){
		$i=1;
        $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'purchase_invoices'));
        foreach($cols_default as $key=>$value){
			$tmp_item=array(
				'id'				=> $i,
				'column_name'		=> $key,
				'name'				=> $value,
				'order_by'			=> $cols_order_dd[$key]
			);
            if ($default_selected_columns_dd[$key]) {
                array_push($array,$tmp_item);
            }			$i++;
		}
	}else{
		foreach($cols_selected as $key=>$value){
			$tmp_item=array(
				'id'				=> $value['id'],
				'column_name'		=> $value['column_name'],
				'name'				=> $cols_default[$value['column_name']],
				'order_by'			=> $cols_order_dd[$value['column_name']]
			);
			array_push($array,$tmp_item);
		}
	}
	return json_out($array, $showin,$exit);
}