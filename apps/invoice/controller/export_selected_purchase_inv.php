<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit', '1000M');
if($_SESSION['add_to_purchase']){
  foreach ($_SESSION['add_to_purchase'] as $key => $value) {
    $in['invoices_id'] .= $key.',';
  }
}
if(isset($_SESSION['tmp_add_to_purchase'])){
    unset($_SESSION['tmp_add_to_purchase']);
}
if(isset($_SESSION['add_to_purchase'])){
    unset($_SESSION['add_to_purchase']);
}
$in['invoices_id'] = rtrim($in['invoices_id'],',');
if(!$in['invoices_id']){
  $in['invoices_id']= '0';
}
/*
require_once 'libraries/PHPExcel.php';
$filename ="export_purchase_invoices_list.xls";

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Invoices list")
							 ->setSubject("Invoices list")
							 ->setDescription("Export invoices list")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Invoices");*/
$db = new sqldb();

$headers= array("TYPE",
        "STATUS",
        "BOOKING NUMBER",
        "INVOICE NUMBER",
        "DATE INVOICE",
        "DUE DATE",
        "COMPANY",
        "COMPANY VAT NUMBER",
        "COMPANY ADDRESS",
        "COMPANY PHONE",
        "COMPANY INVOICE EMAIL",
        "AMOUNT",
        "VAT AMOUNT",
        "AMOUNT+VAT",
        "AMOUNT TO PAY",
        "EXPENSE CATEGORY"
);
$filename ="export_purchase_invoices_list.csv";

$final_data=array();

$info = $db->query("SELECT tblinvoice_incomming.*,customers.name, booking_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date,tblinvoice_expense_categories.name as exp_name
                    FROM tblinvoice_incomming
                    LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id
                    LEFT JOIN tblinvoice_expense_categories ON tblinvoice_incomming.expense_category_id=tblinvoice_expense_categories.id
                    WHERE invoice_id   IN (".$in['invoices_id'].")
                    ORDER BY t_date DESC, iii DESC ");

/*$objPHPExcel->setActiveSheetIndex(0)            
            ->setCellValue('A1',"TYPE")
            ->setCellValue('B1',"STATUS")
            ->setCellValue('C1', "BOOKING NUMBER")
            ->setCellValue('D1',"INVOICE NUMBER")
            ->setCellValue('E1',"DATE INVOICE")
            ->setCellValue('F1',"DUE DATE")
			      ->setCellValue('G1',"COMPANY")
            ->setCellValue('H1',"COMPANY VAT NUMBER")
            ->setCellValue('I1',"COMPANY ADDRESS")
            ->setCellValue('J1',"COMPANY PHONE")
            ->setCellValue('K1',"COMPANY INVOICE EMAIL")            
			      ->setCellValue('L1',"AMOUNT")
            ->setCellValue('M1',"VAT AMOUNT")
			      ->setCellValue('N1',"AMOUNT+VAT")
            ->setCellValue('O1',"AMOUNT TO PAY")
            ->setCellValue('P1',"EXPENSE CATEGORY");

   $xlsRow=2;*/
while ($info->next()){
  //date invoice
  $date_invoice = date(ACCOUNT_DATE_FORMAT,  $info->f('invoice_date')); 

  //due date
  $due_date = date(ACCOUNT_DATE_FORMAT,  $info->f('due_date')); 

  //sent_date
  if(!$info->f('sent_date') || !is_numeric($info->f('sent_date'))) {
    $sent_date = '';
  }else{    
    $sent_date = date(ACCOUNT_DATE_FORMAT,  $info->f('sent_date'));    
  }
  //invoice_type
  $invoice_type='';
  switch ($info->f('type')){
    case '0':   $invoice_type = gm('Regular invoice');        break;
    case '1':   $invoice_type = gm('Proforma invoice');      break;
    case '2':   $invoice_type = gm('Credit Invoice');         break;
  }


  //status
  $invoice_status='';
  switch ($info->f('paid')){
    case '0':
          $invoice_status= gm('Not Paid');
      if($info->f('due_date') < time()){
          $invoice_status= gm('Late');
      }
      break;
    case '1':
        $invoice_status= gm('Paid');
      break;
 }

  // company vat number  , phone, invoice email

  $company_vat_nr = '';
  $phone = '';
  $invoice_email = '';
  if($info->f('supplier_id')){
    $company_data = $db->query("SELECT btw_nr,invoice_email,comp_phone FROM customers WHERE customer_id = '".$info->f('supplier_id')."' ");  
    while($company_data->next()){
      $company_vat_nr = $company_data->f('btw_nr');
      $phone = $company_data->f('comp_phone');
      $invoice_email = $company_data->f('invoice_email');
    }        
  }
  if(!$info->f('supplier_id') && $info->f('contact_id')){
    $contact_data = $db->query("SELECT email, phone FROM customer_contacts WHERE contact_id = '".$info->f('contact_id')."' ");  
    while($contact_data->next()){
      $phone = $contact_data->f('phone');
      $invoice_email = $contact_data->f('email');
    }
  }

  //company address
  if($info->f('free_field')){
    $company_address = $info->f('free_field');  
  }
  else{
    if($info->f('supplier_id')){
      $buyer_details = $db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$info->f('supplier_id')."' AND billing = 1 ");  
    }
    if($info->f('contact_id')){
      $buyer_details = $db->query("SELECT * FROM customer_contact_address WHERE contact_id = '".$info->f('contact_id')."' ");  
    }
    
    $company_address = $buyer_details->f('address').'
'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
'.get_country_name($buyer_details->f('country_id'));

  }

  $nr_of_decimals = 2;

  // vat amount
  $vat_amount = $info->f('total_with_vat')-$info->f('total');


  //amount to pay
  $already_payed = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."'  ");
  $total_payed = $already_payed->f('total_payed');
  $amount_to_pay = round($info->f('total_with_vat') - $total_payed,2);
  if(($info->f('paid') == '1' || ($info->f('status') == '0' && !$info->f('paid')) || $info->f('type') == '2')){
    $amount_to_pay = '0.00';
  }
/*
  $objPHPExcel->setActiveSheetIndex(0)                            
              ->setCellValue('A'.$xlsRow, $invoice_type)
              ->setCellValue('B'.$xlsRow, $invoice_status)
  	          ->setCellValue('C'.$xlsRow, $info->f('iii'))
              ->setCellValue('D'.$xlsRow, $info->f('invoice_number'))
              ->setCellValue('E'.$xlsRow, $date_invoice)
              ->setCellValue('F'.$xlsRow, $due_date)
  			      ->setCellValue('G'.$xlsRow, $info->f('name'))
              ->setCellValue('H'.$xlsRow, $company_vat_nr)
              ->setCellValue('I'.$xlsRow, preg_replace("/[\n\r]/","",$company_address))
              ->setCellValue('J'.$xlsRow, $phone)
              ->setCellValue('K'.$xlsRow, $invoice_email)              
              ->setCellValue('L'.$xlsRow, display_number_exclude_thousand($info->f('total')))
              ->setCellValue('M'.$xlsRow, display_number_exclude_thousand($vat_amount))
              ->setCellValue('N'.$xlsRow, display_number_exclude_thousand($info->f('total_with_vat')))
              ->setCellValue('O'.$xlsRow, $for_credit.display_number_exclude_thousand($amount_to_pay))
              ->setCellValue('P'.$xlsRow, stripslashes($info->f('exp_name')));    
  $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);                         
  $xlsRow++;*/
  $item=array(
    $invoice_type,
    $invoice_status,
    $info->f('iii'),
    $info->f('invoice_number'),
    $date_invoice,
    $due_date,
    $info->f('name'),
    $company_vat_nr,
    preg_replace("/[\n\r]/","",$company_address),
    $phone,
    $invoice_email,
    display_number_exclude_thousand($info->f('total')),
    display_number_exclude_thousand($vat_amount),
    display_number_exclude_thousand($info->f('total_with_vat')),
    $for_credit.display_number_exclude_thousand($amount_to_pay),
    stripslashes($info->f('exp_name'))
  );
  array_push($final_data,$item);
}
header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');
$fp = fopen("php://output", 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
exit();
// set column width to auto
foreach(range('A','O') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true); 
$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('../upload/'.$filename);





define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('../upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>