<?php

if($_SESSION['add_to_sepa']){
  foreach ($_SESSION['add_to_sepa'] as $key => $value) {
  	if ($value)   $in['invoices_id'] .= $key.',';
  }
}
$in['invoices_id'] = rtrim($in['invoices_id'],',');
if(!$in['invoices_id']){
  $in['invoices_id']= '0';
}

$RCUR_count = 0;
$FRST_count = 0;

$invoicesSepaNfo = $db->query("SELECT first_sepa_invoice FROM tblinvoice WHERE tblinvoice.id IN (".$in['invoices_id'].") ");
while($invoicesSepaNfo->next()) {
	if($invoicesSepaNfo->f('first_sepa_invoice') == 1) {
		$FRST_count++;
	} else {
		$RCUR_count++;
	}
}
// $constant=$db->query("SELECT * from settings where type=1");
//     while($constant->move_next())
//     {
//    		$view->assign( $constant->f('constant_name'), $constant->f('value'));

//     }
$info = $db->query("SELECT your_ref,our_ref,serial_number, buyer_name, sent_date, due_date, status, first_sepa_invoice,
					tblinvoice.vat, tblinvoice.amount, amount_vat, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date,
					type, mandate.bank_bic_code, mandate.bank_iban, mandate.sepa_number, mandate.creation_date, tblinvoice.collection_date
                    FROM tblinvoice
                    LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id
                    WHERE tblinvoice.id IN (".$in['invoices_id'].")
                    ORDER BY t_date DESC, serial_number DESC ");


$settings_sepa=$db->query("SELECT * FROM sepa_settings");
$invoices_on_FRST='';
$invoices_on_RCUR='';
$amount_vat_first =0;
$count_first =0;
$amount_vat =0;
$counts =0;
$i=0;
$collection_date=0;
while ($info->next()){

		/*if($info->f('first_sepa_invoice') == 1 ) {

			$invoices_on_FRST.='<DrctDbtTxInf>
				<PmtId>
					<EndToEndId>'.$info->f('serial_number').'</EndToEndId>
				</PmtId>
				<InstdAmt Ccy="EUR">'.number_format($info->f('amount_vat'),2,'.','').'</InstdAmt>
				<DrctDbtTx>
					<MndtRltdInf>
						<MndtId>'.$info->f('sepa_number').'</MndtId>
						<DtOfSgntr>'.date('Y-m-d',$info->f('creation_date')).'</DtOfSgntr>
					</MndtRltdInf>
				</DrctDbtTx>
				<DbtrAgt>
					<FinInstnId>
						<BIC>'.$info->f('bank_bic_code').'</BIC>
					</FinInstnId>
				</DbtrAgt>
				<Dbtr>
					<Nm>'.htmlspecialchars($info->f('buyer_name')).'</Nm>
				</Dbtr>
				<DbtrAcct>
					<Id>
						<IBAN>'.str_replace(' ', '', $info->f('bank_iban')).'</IBAN>
					</Id>
				</DbtrAcct>
				<RmtInf>
					<Ustrd>'.$info->f('serial_number').'</Ustrd>
				</RmtInf>
			</DrctDbtTxInf>';
			$i++;
			$amount_vat_first +=$info->f('amount_vat');
			$count_first++;
		} else {*/

			$invoices_on_RCUR.='<DrctDbtTxInf>
				<PmtId>
					<EndToEndId>'.$info->f('serial_number').'</EndToEndId>
				</PmtId>
				<InstdAmt Ccy="EUR">'.number_format($info->f('amount_vat'),2,'.','').'</InstdAmt>
				<DrctDbtTx>
					<MndtRltdInf>
						<MndtId>'.$info->f('sepa_number').'</MndtId>
						<DtOfSgntr>'.date('Y-m-d',$info->f('creation_date')).'</DtOfSgntr>
					</MndtRltdInf>
				</DrctDbtTx>
				<DbtrAgt>
					<FinInstnId>
						<BIC>'.str_replace(' ', '',$info->f('bank_bic_code')).'</BIC>
					</FinInstnId>
				</DbtrAgt>
				<Dbtr>
					<Nm>'.htmlspecialchars($info->f('buyer_name')).'</Nm>
				</Dbtr>
				<DbtrAcct>
					<Id>
						<IBAN>'.str_replace(' ', '', $info->f('bank_iban')).'</IBAN>
					</Id>
				</DbtrAcct>
				<RmtInf>
					<Ustrd>'.$info->f('serial_number').'</Ustrd>
				</RmtInf>
			</DrctDbtTxInf>';
			$i++;
			$amount_vat +=round($info->f('amount_vat'),2);
			$counts++;
		//}
		$collection_date=$info->f('collection_date');
}

$FRST = '<PmtInf>
			<PmtInfId>1</PmtInfId>
			<PmtMtd>DD</PmtMtd>
			<BtchBookg>true</BtchBookg>
			<NbOfTxs>'.$count_first.'</NbOfTxs>
			<CtrlSum>'.number_format($amount_vat_first,2,'.','').'</CtrlSum>
			<PmtTpInf>
				<SvcLvl>
					<Cd>SEPA</Cd>
				</SvcLvl>
				<LclInstrm>
					<Cd>CORE</Cd>
				</LclInstrm>
				<SeqTp>FRST</SeqTp>
			</PmtTpInf>
			<ReqdColltnDt>'.date('Y-m-d',$collection_date).'</ReqdColltnDt>
			<Cdtr>
				<Nm>'.htmlspecialchars($settings_sepa->f('sepa_company')).'</Nm>
			</Cdtr>
			<CdtrAcct>
				<Id>
					<IBAN>'.str_replace(' ', '',$settings_sepa->f('sepa_iban')).'</IBAN>
				</Id>
			</CdtrAcct>
			<CdtrAgt>
				<FinInstnId>
					<BIC>'.str_replace(' ', '',$settings_sepa->f('sepa_bic_code')).'</BIC>
				</FinInstnId>
			</CdtrAgt>
			<ChrgBr>SLEV</ChrgBr>
			<CdtrSchmeId>
				<Id>
					<PrvtId>
						<Othr>
							<Id>'.$settings_sepa->f('sepa_id').'</Id>
							<SchmeNm>
								<Prtry>SEPA</Prtry>
							</SchmeNm>
						</Othr>
					</PrvtId>
				</Id>
			</CdtrSchmeId>';

/*if($FRST_count > 0) {
	$count = '2';
} else {*/
	$count = '1';
//}

$RCUR = '<PmtInf>
			<PmtInfId>'.$count.'</PmtInfId>
			<PmtMtd>DD</PmtMtd>
			<BtchBookg>true</BtchBookg>
			<NbOfTxs>'.$counts.'</NbOfTxs>
			<CtrlSum>'.number_format($amount_vat,2,'.','').'</CtrlSum>
			<PmtTpInf>
				<SvcLvl>
					<Cd>SEPA</Cd>
				</SvcLvl>
				<LclInstrm>
					<Cd>CORE</Cd>
				</LclInstrm>
				<SeqTp>RCUR</SeqTp>
			</PmtTpInf>
			<ReqdColltnDt>'.date('Y-m-d',$collection_date).'</ReqdColltnDt>
			<Cdtr>
				<Nm>'.htmlspecialchars($settings_sepa->f('sepa_company')).'</Nm>
			</Cdtr>
			<CdtrAcct>
				<Id>
					<IBAN>'.str_replace(' ', '',$settings_sepa->f('sepa_iban')).'</IBAN>
				</Id>
			</CdtrAcct>
			<CdtrAgt>
				<FinInstnId>
					<BIC>'.str_replace(' ', '',$settings_sepa->f('sepa_bic_code')).'</BIC>
				</FinInstnId>
			</CdtrAgt>
			<ChrgBr>SLEV</ChrgBr>
			<CdtrSchmeId>
				<Id>
					<PrvtId>
						<Othr>
							<Id>'.$settings_sepa->f('sepa_id').'</Id>
							<SchmeNm>
								<Prtry>SEPA</Prtry>
							</SchmeNm>
						</Othr>
					</PrvtId>
				</Id>
			</CdtrSchmeId>';


$xml='<?xml version="1.0" encoding="UTF-8"?>
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.008.001.02" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<CstmrDrctDbtInitn>
		<GrpHdr>
			<MsgId>'.date('Y-m-d', time()).'T'.date('H:i:s', time()).'</MsgId>
			<CreDtTm>'.date('Y-m-d', time()).'T'.date('H:i:s', time()).'</CreDtTm>
			<NbOfTxs>'.$i.'</NbOfTxs>
			<InitgPty>
				<Nm>'.htmlspecialchars($settings_sepa->f('sepa_company')).'</Nm>
				<Id>
					<OrgId>
						<Othr>
							<Id>'.$settings_sepa->f('sepa_comp_id').'</Id>
							<Issr>'.$settings_sepa->f('sepa_authority').'</Issr>
						</Othr>
					</OrgId>
				</Id>
			</InitgPty>
		</GrpHdr>';

/*if($FRST_count>0) { // we have first invoices
	$xml .= $FRST;
	$xml .= $invoices_on_FRST;
	$xml .= '</PmtInf>';
	if($RCUR_count>0) {
		$xml .= $RCUR;
		$xml .= $invoices_on_RCUR;
		$xml .= '</PmtInf>';
	}
} else {*/
	$xml .= $RCUR;
	$xml .= $invoices_on_RCUR;
	$xml .= '</PmtInf>';
//}

	$xml.='
	</CstmrDrctDbtInitn>
</Document>';
doQueryLog();
header("Content-type: text/xml; charset=utf-8");
header('Content-Disposition: attachment; filename=SEPA.xml');
// $sxe = new SimpleXMLElement($xml);

//unset($_SESSION['add_to_pdf']);

print_r($xml);//->asXML("test.xml");
// exit();

exit();
?>