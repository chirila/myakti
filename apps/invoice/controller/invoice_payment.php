<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

$act ='invoice-invoice_payment-invoice-pay';
  $tblinvoice = $db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."'");
  if(!$tblinvoice->move_next()){
    $result = array();
    msg::error('Invalid ID','error');
    json_out($result);
    // page_redirect('index.php?do=invoice-to_invoice');
  }

  $currency = get_commission_type_list($tblinvoice->f('currency_type'));
  if($tblinvoice->f('discount') == 0){
    $discount_procent=0;
  } else {
    $discount_procent = $tblinvoice->f('discount');
  }

  if($tblinvoice->f('apply_discount') < 2){
    $discount_procent = 0;
  }

  $subtotal_vat = 0;
  $total_n = 0;
  $lines = $db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
  while ($lines->next()) {
    $line_discount = $lines->f('discount');
    if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
      $line_discount = 0;
    }

    $amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
    $amount_line_disc = $amount_line*$discount_procent/100;
    $discount_value += $amount_line_disc;
    $subtotal_vat +=  ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
    $total_n += $amount_line - $amount_line_disc;
  }

  $total = $total_n+$subtotal_vat;

  $proforma_id=$db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
  $payments_filter=" invoice_id='".$in['invoice_id']."'";
  if($proforma_id){
    $payments_filter.=" OR invoice_id='".$proforma_id."'";
  }

  $already_payed = $db->query("SELECT SUM(ROUND(cast(amount as decimal(12, 4))+0.0012,2)) AS total_payed FROM tblinvoice_payments WHERE ".$payments_filter."  ");
  $already_payed->move_next();
  $total_payed = $already_payed->f('total_payed');

  $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
  if($tblinvoice->f('req_payment') == 100){
    $amount_due = round($total - $total_payed,2);
  }else{
    $amount_due = round($req_payment_value - $total_payed ,2);
  }
  $method_pay= false;
  $result=array(
    'do_next'           => $act,
    'amount'            => display_number($amount_due),
    'currency'          => $currency,
    'cost'              => display_number($in['cost']),
    'check_not_paid'    => $tblinvoice->f('not_paid')==1? 'checked="checked"':'',
    'is_ckecked'        => $tblinvoice->f('not_paid')==1? 'disabled="disabled"' : '',
    'not_paid'        => $tblinvoice->f('not_paid')==1? true : false,
    'method_pay'        => $method_pay ? true : false,
    'from_list'         => '1',
    'invoice_id'        => $in['invoice_id'],
    'view'              => $in['view'],
    'offset'            => $in['offset'],
    'archived'          => $in['archived'],
    'search'            => $in['search'],
    'payment_method_dd' => invoice_payment_method_dd(),
    'payment_method'    => '0',
    'desc'              => $in['desc'],
    'order_by'          => $in['order_by'],
    'doc_type'          => $in['doc_type'],
    'start_date_js'     => $in['start_date_js'],
    'stop_date_js'     => $in['stop_date_js']
  );

  $result['c_invoices']=array();
  $ci_not_linked=$db->query("SELECT tblinvoice.*,COUNT(tblinvoice_payments.payment_id) AS linked_invoice FROM tblinvoice
    LEFT JOIN tblinvoice_payments ON tblinvoice.c_invoice_id=tblinvoice_payments.invoice_id AND tblinvoice_payments.credit_payment='1'
    WHERE tblinvoice.sent='1' AND (tblinvoice.type='2' OR tblinvoice.type='4') AND (tblinvoice.f_archived='0' OR tblinvoice.f_archived='3') AND buyer_id='".$tblinvoice->f('buyer_id')."' GROUP BY tblinvoice.id ORDER BY tblinvoice.serial_number ASC");
  while($ci_not_linked->next()){
    //if(!$ci_not_linked->f('linked_invoice')){
      array_push($result['c_invoices'],array(
        'id'  => $ci_not_linked->f('id'),
        'name'  => $ci_not_linked->f('serial_number'),
        'amount'  => display_number($ci_not_linked->f('amount_vat')),
        'date'    => $ci_not_linked->f('invoice_date')*1000,
      ));
    //}   
  }

    json_out($result);