<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

global $config;
$result=array();

$l_r = ROW_PER_PAGE;

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$rem_names=$db->field("SELECT value FROM settings WHERE constant_name='USE_REMINDER_NAMES' AND type='1' ");
$lang_id = $db->field("SELECT lang_id FROM pim_lang WHERE code='".$_SESSION['l']."'");
$filter = "WHERE tblinvoice.sent='1' AND tblinvoice.not_paid='0' AND tblinvoice.paid !='1' AND tblinvoice.f_archived='0' AND tblinvoice.type!='1' AND tblinvoice.due_date < ".time();
$filter_link = 'block';
$order_by = " ORDER BY tblinvoice.invoice_date DESC, iii DESC ";

if($in['filter']){
	$arguments.="&filter=".$in['filter'];
}
if($in['search']){
	$filter.=" AND tblinvoice.buyer_name LIKE '%".$in['search']."%' ";
	$arguments_s.="&search=".$in['search'];
}
$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
if($in['order_by']){
	$order = " ASC ";
	if($in['desc'] == '1' || $in['desc']=='true'){
		$order = " DESC ";
	}
	if($in['order_by'] == 'sent_date' || $in['order_by'] == 'amount'){
		$order_by='';
		$filter_limit =' ';
	}else{
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	}
}

$arguments = $arguments.$arguments_s;
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;

$tblinvoice = $db->query("SELECT tblinvoice.*, tblinvoice.serial_number as iii, customers.acc_manager_name AS manager_name FROM tblinvoice LEFT JOIN customers ON tblinvoice.buyer_id=customers.customer_id ".$filter."  GROUP BY buyer_id,id ".$order_by);

$all_payments_ar=array();
$all_payments=$db->query("SELECT invoice_id, amount, credit_payment FROM tblinvoice_payments")->getAll();
foreach($all_payments as $key=>$value){
	if(!$all_payments_ar[$value['invoice_id']]){
		$all_payments_ar[$value['invoice_id']]=array();
	}
	$tmp_pay_item=array(
		'amount'			=> $value['amount'],
		'credit_payment'	=> $value['credit_payment'],
	);
	array_push($all_payments_ar[$value['invoice_id']],$tmp_pay_item);
}

while ($tblinvoice->move_next()) {
	/*$payments = $db->field("SELECT SUM(amount) FROM tblinvoice_payments WHERE invoice_id='".$tblinvoice->f('id')."' ");*/

	$balance=$tblinvoice->f('amount_vat');
    /*$all_payments=$db->query("SELECT amount FROM tblinvoice_payments WHERE invoice_id='".$tblinvoice->f('id')."'")->getAll();
    foreach($all_payments as $key=>$value){
        if($value['credit_payment']){
            if($value['amount']<0){
                $balance+=$value['amount'];
            }else{
                $balance-=$value['amount'];
            }
        }else{
            $balance-=$value['amount'];
        }
    }*/
    if($all_payments_ar[$tblinvoice->f('id')]){
    	foreach($all_payments_ar[$tblinvoice->f('id')] as $key1=>$value1){
    		if($value1['credit_payment']){
    			if($value1['amount']<0){
    				$balance+=$value1['amount'];
    			}else{
    				$balance-=$value1['amount'];
    			}
			}else{
				$balance-=$value1['amount'];
			}
    	}
    }
    if(!$balance){
    	continue;
    }
	
	//$key = $tblinvoice->f('buyer_id').'-'.$tblinvoice->f('contact_id');
	$key = $tblinvoice->f('buyer_id').'-0';

	$buyer[$key]['name'] = $tblinvoice->f('buyer_name');
	$buyer[$key]['manager_name'] = $tblinvoice->f('manager_name');
	$buyer[$key]['date_sent'] = $tblinvoice->f('last_notice_sent');
	$buyer[$key]['inv'][] = array('id' 			=> $tblinvoice->f('id'),
								  'amount'		=> $tblinvoice->f('amount'),
								 // 'amount_vat'	=> $tblinvoice->f('amount_vat')-$payments,
								  'amount_vat'	=> $balance,
								  'ref'		=> $tblinvoice->f('our_ref'),
								  'idate'		=> $tblinvoice->f('invoice_date'),
								  'ddate'		=> $tblinvoice->f('due_date'),
								  'grade'		=> $tblinvoice->f('invoice_grade'),
								  'iii'		=> $tblinvoice->f('iii'),
								  'debt_id'		=> $tblinvoice->f('bPaid_debt_id'),
								  'buyer_id'	=> $tblinvoice->f('buyer_id'),
								  'contact_id'	=> $tblinvoice->f('contact_id'),
								  'postgreen_id'	=> $tblinvoice->f('postgreen_id'));
	/*$buyer[$key]['amount'] += $tblinvoice->f('amount_vat')-$payments;*/
	$buyer[$key]['amount'] += $balance;
	$buyer[$key]['email'] = $tblinvoice->f('buyer_email');
	$buyer[$key]['pdf_layout'] = $tblinvoice->f('pdf_layout');
}


$max_rows=count($buyer);
$result['query'] = array();
$result['max_rows']=$max_rows;
// console::log($buyer);
//$buyer = array_slice($buyer, $offset*$l_r, $l_r, true);
if($in['order_by'] != 'sent_date' && $in['order_by'] != 'amount'){
	$buyer = array_slice($buyer, $offset*$l_r, $l_r, true);	
}
// console::log($buyer);
foreach ($buyer as $key => $value) {
	$language = '';
	list($b_id,$c_id) = explode('-', $key);
	$item=array();
	$with_names=0;
	if($rem_names){
		$item['contacts']=array();
		if($b_id==0 && $c_id!=0){
			$contacts = $db->query("SELECT firstname,lastname
					FROM customer_contacts
					WHERE contact_id='".$c_id."' AND active=1");
			$count=0;
			while($contacts->next()){
				$c_line=array(
					'id'			=> $contacts->f('contact_id'),
					'name'		=> $contacts->f('name'),
					'firstname'		=> $contacts->f('firstname'),
					'lastname'		=> $contacts->f('lastname')
				);
				array_push($item['contacts'], $c_line);
				$count++;
			}
			if($count>0){
				$with_names=1;
			}
		}else if($b_id!=0){
			$contacts = $db->query("SELECT contact_id,firstname,lastname, CONCAT_WS(' ',firstname,lastname) as name
					FROM customer_contacts
					WHERE customer_id='".$b_id."' AND active=1 ORDER BY lastname");
			$count=0;
			while($contacts->next()){
				$c_line=array(
					'id'			=> $contacts->f('contact_id'),
					'name'		=> $contacts->f('name'),
					'firstname'		=> $contacts->f('firstname'),
					'lastname'		=> $contacts->f('lastname')
				);
				array_push($item['contacts'], $c_line);
				$count++;
			}
			if($count>0){
				$with_names=1;
			}
		}
	}
	$item['subdata']=array();
	$invoices=array();

	$show_date=false;

	if($b_id){
		$invs = $db->field("SELECT invoices FROM reminders_logs WHERE company_id ='".$b_id."' ORDER BY log_id DESC LIMIT 1");
		$invoices=explode(',',$invs);
	}

	foreach ($value['inv'] as $k => $v) {
		/*if($v['buyer_id']){
			$invs = $db->field("SELECT invoices FROM reminders_logs WHERE company_id ='".$v['buyer_id']."' ORDER BY log_id DESC LIMIT 1");
			$invoices=explode(',',$invs);
		}*/
		if(in_array($v['id'], $invoices)){
			$show_date=true;
		}

		$subdata=array(
			'serial_number_td'     		=> $v['iii'],
			'created'           		=> date(ACCOUNT_DATE_FORMAT,$v['idate']),
			'buyer_name'        		=> $value['name'],
			'our_reference'			=> $v['ref'],
			'amount_inv'			=> place_currency(display_number($v['amount_vat'])),
			'due_date'				=> date(ACCOUNT_DATE_FORMAT,$v['ddate']),
			'nr_days_due_date'		=> ceil((time()-$v['ddate'])/(60*60*24)),
			'info_link'     			=> 'index.php?do=invoice-invoice&invoice_id='.$v['id'],
			'invoice_id'			=> $v['id'],
			'grade'				=> $v['grade'],
			'debt_done'				=> $v['debt_id'] ? true : false,
			'buyer_id'				=> $v['buyer_id'],
			'contact_id'			=> $v['contact_id'],
			'postgreen_id'			=> $v['postgreen_id'],
			'id_in_list'			=> in_array($v['id'], $invoices)? 1:0,
		);
		array_push($item['subdata'], $subdata);
	}

	if($b_id){
		$language = $db->field("SELECT language FROM customers WHERE customer_id='".$b_id."' ");
	}
	/*if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $tblinvoice->f('pdf_layout') == 0){
		$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
	} else {*/
		$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
	//}
	if($b_id){
		$sent_date = $db->field("SELECT sent_date FROM reminders_logs WHERE company_id ='".$b_id."' ORDER BY log_id DESC LIMIT 1");
		/*$id_in_list = 0;
		foreach($item['subdata'] as  $k => $v){
			if($v['id_in_list']==1){
				$id_in_list = 1;
			}
		}
		if(!$id_in_list){
			$sent_date ='';
		}*/

	}
	$item['amount']					= place_currency(display_number($value['amount']));
	$item['amount_ord']					= $value['amount'];
	$item['buyer_name']        			= $value['name'];
	$item['acc_manager_name']			= $value['manager_name'];
	$item['hide_td']					= 'hide';
	//$item['date_sent']				= $value['date_sent'] ? date(ACCOUNT_DATE_FORMAT,$value['date_sent']) : '';
	//$item['date_sent']				= $sent_date? date(ACCOUNT_DATE_FORMAT,$sent_date) : '';
	$item['date_sent']				= ($sent_date && $show_date)? date(ACCOUNT_DATE_FORMAT,$sent_date) : '';
	$item['sent_date_ord']			= $sent_date;
	$item['reminder_link']				= 'index.php?do=invoice-late_invoice-invoice-notice&email='.$value['email'].'&buyer_id='.$key;
	$item['pdf_link']					= 'index.php?do=invoice-invoice_print&reminder=1&lid='.($language ? $language : $lang_id).$link_end.'&customer_id='.$key;
	$item['c_id']					= $key;
	$item['b_id'] 					= $b_id;
	$item['is_date']			 		= $b_id && $sent_date ? true : false;
	$item['with_names']				= $with_names>0 ? true : false;
	array_push($result['query'], $item);
	$i++;
}

	$result['seller_data']=array(
		'invoice_seller_b_address'	=> ACCOUNT_DELIVERY_ADDRESS,
		'invoice_seller_b_city'		=> ACCOUNT_DELIVERYG_CITY,
		'invoice_seller_b_zip'		=> ACCOUNT_DELIVERY_ZIP,
		'invoice_seller_b_country_id'	=> ACCOUNT_DELIVERY_COUNTRY_ID,
		'invoice_seller_b_country_dd'	=> build_country_list(ACCOUNT_DELIVERY_COUNTRY_ID),
		'invoice_seller_name'		=> ACCOUNT_COMPANY,
		'invoice_vat_no'			=> ACCOUNT_VAT_NUMBER,
		'languages'				=> $db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'"),
		'language_dd'			=> build_language_dd_new($db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'")),
		'invoice_seller_iban'		=> ACCOUNT_IBAN,
		'invoice_seller_swift'		=> ACCOUNT_BIC_CODE,
		'invoice_seller_email'		=> ACCOUNT_EMAIL
	);

$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
$bepaid_active=$db->field("SELECT active FROM apps WHERE name='Instapaid' AND main_app_id='0' AND type='main'");

	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
if($_SESSION['main_u_id']==0){
	//$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
}else{
	//$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
	$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['main_u_id']]);
}

	if($in['bPaid_modUrl']){
    		$result['bPaid_modUrl']		= $in['bPaid_modUrl'].'&lang='.strtoupper($_SESSION['l']);
    	}

$args_v = preg_replace('/&view=([0-9]+)/','',$arguments);

	$result['view_go']			 	 = $_SESSION['access_level'] == 1 ? true : false;
	$result['args']				 	 = $args_v;
	$result['archived']			 	 = $in['archived'];
	$result['is_data']			 	 = $max_rows != 0 ? true : false;
	$result['is_pagination']		 = $max_rows > $l_r ? true : false;
	$result['view_active']			 = !$in['archived'] ? false : true;
	$result['archived_list']		 = 'index.php?do=invoice-invoices&archived=1';
	$result['view_archived']		 = $in['archived'] ? false : true;
	$result['active_list']			 = 'index.php?do=invoice-invoices';
	$result['provider_ref']			 = base64_encode(DATABASE_NAME);
	$result['post_active']			 = !$post_active || $post_active==0 ? false : true;
	$result['bPaid_reg']			 = $bPaidCust!='' ? true : false;
	$result['lg']					 = $_SESSION['l'];
	$result['bpaid_active']			 = $bepaid_active ? true : false;

$_SESSION['filters'] = $arguments.$arguments_o;
$result['lr'] = $l_r;
$result['order_by'] = $in['order_by'];
$result['desc'] = $in['desc'];

$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
if($easyinvoice=='1'){
	$result['show_deliveries']	 	=false;
	$result['show_projects']		= false;
	$result['show_interventions']	= false;
}else{
	$result['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
	$result['show_projects']		= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
	$result['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
}    

if($in['order_by']=='sent_date' || $in['order_by']=='amount'){

    if($order ==' ASC '){
       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
    }else{
       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
    }

    $exo = array_slice( $exo, $offset*$l_r, $l_r);
    $result['query']=array();
       foreach ($exo as $key => $value) {
           array_push($result['query'], $value);
       }
}
$result['columns']  	= get_reminders_cols($in,true,false);

json_out($result);

function get_reminders_cols(&$in,$showin=true,$exit=true){
	$db=new sqldb();
	$array=array();
	$cols_default=default_columns_dd(array('list'=>'reminders'));
	$cols_order_dd=default_columns_order_dd(array('list'=>'reminders'));
	$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='reminders' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
	if(!count($cols_selected)){
		$i=1;
        $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'reminders'));
		foreach($cols_default as $key=>$value){
            $tmp_item=array(
                'id'				=> $i,
                'column_name'		=> $key,
                'name'				=> $value,
                'order_by'			=> $cols_order_dd[$key]
            );
            if ($default_selected_columns_dd[$key]) {
                array_push($array,$tmp_item);
            }
            $i++;
        }
	}else{
		foreach($cols_selected as $key=>$value){
			$tmp_item=array(
				'id'				=> $value['id'],
				'column_name'		=> $value['column_name'],
				'name'				=> $cols_default[$value['column_name']],
				'order_by'			=> $cols_order_dd[$value['column_name']]
			);
			array_push($array,$tmp_item);
		}
	}
	return json_out($array, $showin,$exit);
}