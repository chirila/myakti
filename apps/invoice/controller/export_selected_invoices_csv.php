<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');
if(!$in['list_id']){
  return ark::run('invoice-export_csvs');
}
$invoices = $db->field("SELECT count(invoice_id) FROM tblinvoice_to_export WHERE time='".$_SESSION['invoice_to_export']."' ");
if($invoices == 0){
  return ark::run('invoice-export_csvs');
}
$list = $db->query("SELECT * FROM tblinvoice_export_list WHERE list_id='".$in['list_id']."' ");
$delimiter = $list->f('delimit_er');
$convert = $list->f('convert');
if(!$delimiter){
  $delimiter = ';';
}
$show_header = $list->f('show_headers');
$type = array(0 => gm('Regular invoice') ,1 => gm('Proforma invoice'), 2 => gm('Credit Invoice'));
$setting_data = array();
$vat_regime_data = array();
$settings_data = $db->query("SELECT * FROM invoice_export_settings");
while($settings_data->next()){
  $setting_data[$settings_data->f('field_name')][$settings_data->f('field_id')] = $settings_data->f('field_value');
  if($settings_data->f('field_name')=='vat_regime_id'){
    $vat_regime_data[$settings_data->f('field_id')]=$settings_data->f('field_value');
  }
}
// type
if( $setting_data['type'][0] || $setting_data['type'][1] || $setting_data['type'][2] ){
  $type = array(0 => $setting_data['type'][0] ,1 => $setting_data['type'][1], 2 => $setting_data['type'][2]);
}

//vat_regime_id
/*$vat_regime_data = array();
if( $setting_data['vat_regime_id'][1] || $setting_data['vat_regime_id'][2] || $setting_data['vat_regime_id'][3] || $setting_data['vat_regime_id'][4] ){
  $vat_regime_data = array( 1 => $setting_data['vat_regime_id'][1], 2 => $setting_data['vat_regime_id'][2], 3 => $setting_data['vat_regime_id'][3], 4 => $setting_data['vat_regime_id'][4]);
}*/

$apply_disc = array('0' => gm('No Discount'),'1' => gm('Apply at line level'), '2' => gm('Apply at global level'),'3' => gm('Apply at both levels'));
$fname = $list->f('name').'.csv';
#headers
$header = '';
$header_line = '';
$custom_h = ''; 
$fields = array();
#invoice 
$db->query("SELECT tblinvoice_field.*, tblinvoice_fields.field FROM tblinvoice_field 
            LEFT JOIN tblinvoice_fields ON tblinvoice_field.field_id=tblinvoice_fields.field_id 
            WHERE tblinvoice_field.value='1' AND (tblinvoice_fields.type='1' OR tblinvoice_fields.type='3') AND list_id='".$in['list_id']."' ORDER BY sort_order ");
while ($db->next()) {
    $fields[$db->f('field')] = $db->f('header');
    $header .= $db->f('header').$delimiter;   
}
// $header = rtrim($header,';');
// $header .= "\n";
#invoice line
$fields_line = array();
$db->query("SELECT tblinvoice_field.*, tblinvoice_fields.field FROM tblinvoice_field 
            LEFT JOIN tblinvoice_fields ON tblinvoice_field.field_id=tblinvoice_fields.field_id 
            WHERE tblinvoice_field.value='1' AND tblinvoice_fields.type='2' AND list_id='".$in['list_id']."' ORDER BY sort_order ");
while ($db->next()) {
    $fields_line[$db->f('field')] = $db->f('header');
    $header_line .= $db->f('header').$delimiter;  
}

// $header_line = rtrim($header_line,';');
// $header_line .= "\n";

#customer headers
$custom = array();
$db->query("SELECT * FROM tblinvoice_field WHERE custom='1' AND list_id='".$in['list_id']."' ORDER BY sort_order ");
while ($db->next()) {
  $custom[$db->f('header')] = $db->f('value');
  $custom_h .= $db->f('header').$delimiter;
}

// $custom_h = rtrim($custom_h,';');
// $custom_h .= "\n";

#data 
if($show_header){
  $data = $header.$header_line.$custom_h;
  $data = rtrim($data,$delimiter)."\n";
  if($convert == 1){
    $data = gm('Serial Number').$delimiter.$data;
  }
}

$info = $db->query("SELECT * FROM tblinvoice WHERE id IN (SELECT invoice_id FROM tblinvoice_to_export WHERE time='".$_SESSION['invoice_to_export']."' ) ");
while ($info->next()) {
  // $data = rtrim($data,';');
  // $data .="\n";
  // $data .= $header_line;
  if(empty($fields_line)){
    if($convert == 1 ){
      $data .= str_replace(array('+','-'),'',filter_var($info->f('serial_number'), FILTER_SANITIZE_NUMBER_INT)).$delimiter;
    }
    foreach ($fields as $key => $value) {      
    switch ($key) {
        case 'invoice_date':
        case 'due_date':
        case 'sent_date':
          $data .=date(ACCOUNT_DATE_FORMAT, $info->f($key)).$delimiter;
          break;
        case 'currency_type':
          $data .= currency::get_currency($info->f($key),'code').$delimiter;
          break;
        case 'type':
          $data .= $type[$info->f($key)].$delimiter;
          break;
        case 'apply_discount':
          $data .= $apply_disc[$info->f($key)].$delimiter;
          break;
        case 'notes2':  
        case 'buyer_address':      
          $text = str_replace(array("\r\n", "\r"), " ", $info->f($key));
          $data .= $text.$delimiter;
          break;
        case 'amount':
        case 'amount_vat':                
          $data .= display_number($info->f($key)).$delimiter;
          break;
        case 'vat':        
          $vat_val = $info->f($key);
          if($info->f('remove_vat')==1){
            $vat_val = 0;
          }
          $data .= display_number($vat_val).$delimiter;
          break;
        case 'discount':
          $global_disc = $info->f($key);
          $apply_discount = $info->f('apply_discount');
          if($apply_discount == 1 || $apply_discount == 0){
            $global_disc = 0;
          }
          $data .= display_number($global_disc).$delimiter;
          break;

        case 'vat_amount':
          $data .= display_number($info->f('amount_vat') - $info->f('amount')).$delimiter;
          break;
        case 'customer_external_id':
          $data .= $db->field("SELECT external_id FROM customers WHERE customer_id='".$info->f('buyer_id')."' ").$delimiter;
          break;
        case 'siret':
          $data .= $db->field("SELECT siret FROM customers WHERE customer_id='".$info->f('buyer_id')."' ").$delimiter;
          break;
        case 'vat_regime_id':
          $vat_regime_id = $db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$info->f('buyer_id')."' ");          
          /*if($vat_regime_id==0){
            $vat_regime_id = 1;
          }*/            
          if(empty($vat_regime_data)){
            $data .= build_vat_regime_dd($vat_regime_id,1).$delimiter;   
          }else{            
            $data .= $vat_regime_data[$vat_regime_id].$delimiter;
          } 
          break;
        case 'our_reference':
            $data .= $db->field("SELECT our_reference FROM customers WHERE customer_id='".$info->f('buyer_id')."' ").$delimiter;
          break; 

        //change ids or other data into readable data in .csv  
        case 'paid':          
        case 'sent': 
        case 'f_archived':
        case 'remove_vat':
          if($info->f($key)=='1'){
            $data .= gm('Yes').$delimiter;  
          }elseif($info->f($key)=='2'){
            $data .= gm('Partially Paid').$delimiter;
          }else{
            $data .= gm('No').$delimiter;
          }            
          break;   

        case 'status':
          switch ($info->f($key)){
            case '0':
              if($info->f('sent') == '0'){                  
                $data .= gm('Draft').$delimiter;
              }else{                
                if($info->f('due_date') < time()){                   
                  $data .= gm('Late').$delimiter;
                }else{
                  if($info->f('paid') == '2' ){                    
                    $data .= gm('Partially Paid').$delimiter;
                  }else {
                    $data .= gm('Final').$delimiter;
                  }
                }
              }
              break;
            case '1':                                
              $info->f('type')=='2'? $data .= gm('Final').$delimiter : $data .= gm('Paid').$delimiter;
              break;
            default:                
              $data .= gm('Draft').$delimiter;
              break;
          }
          break;

        case 'notes':            
          $notess = $db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND active = '1' AND item_id = '".$info->f('id')."' ");
          $text = str_replace(array("\r\n", "\r"), " ", $notess);            
          $data .= $text.$delimiter;
          break;

        case 'buyer_country_id':
        case 'seller_b_country_id':
          $country_name = $db->field("SELECT code FROM country WHERE country_id = '".$info->f($key)."' ");
          $data .= $country_name.$delimiter;
          break;

        case 'email_language':
          $lang_code = build_language_dd_new($info->f($key),'country_code');
          $data .= $lang_code.$delimiter;
          break;

        default:
          $data .= utf8_decode($info->f($key)).$delimiter;
          break;
      }    
    }
    foreach ($custom as $key => $value) {
      $data .= $value.$delimiter;
    }
    $data = rtrim($data,$delimiter);
    $data .="\n";
  }else{
    $lines = $db->query("SELECT * FROM tblinvoice_line where invoice_id = '".$info->f('id')."' AND content = '0' ");
    while ($lines->next()) {
      if($convert == 1 ){
        $data .= str_replace(array('+','-'),'',filter_var($info->f('serial_number'), FILTER_SANITIZE_NUMBER_INT)).$delimiter;
      }
      foreach ($fields as $key => $value) {       
      switch ($key) {
          case 'invoice_date':
          case 'due_date':
          case 'sent_date':
            $data .=date(ACCOUNT_DATE_FORMAT, $info->f($key)).$delimiter;
            break;
          case 'currency_type':
            $data .= currency::get_currency($info->f($key),'code').$delimiter;
            break;
          case 'type':
            $data .= $type[$info->f($key)].$delimiter;
            break;
          case 'apply_discount':
            $data .= $apply_disc[$info->f($key)].$delimiter;
            break;
          case 'notes2':  
          case 'buyer_address':        
            $text = str_replace(array("\r\n", "\r"), " ", $info->f($key));
            $data .= $text.$delimiter;
            break;
          case 'amount':
          case 'amount_vat':                    
            $data .= display_number($info->f($key)).$delimiter;
            break;
          case 'vat':        
            $vat_val = $info->f($key);
            if($info->f('remove_vat')==1){
              $vat_val = 0;
            }
            $data .= display_number($vat_val).$delimiter;
            break;  
          case 'discount':
            $global_disc = $info->f($key);
            $apply_discount = $info->f('apply_discount');
            if($apply_discount == 1 || $apply_discount == 0){
              $global_disc = 0;
            }
            $data .= display_number($global_disc).$delimiter;
            break;  
          case 'vat_amount':
            $data .= display_number($info->f('amount_vat') - $info->f('amount')).$delimiter;
            break;
          case 'customer_external_id':
            $data .= $db->field("SELECT external_id FROM customers WHERE customer_id='".$info->f('buyer_id')."' ").$delimiter;
            break;
          case 'siret':
            $data .= $db->field("SELECT siret FROM customers WHERE customer_id='".$info->f('buyer_id')."' ").$delimiter;
            break; 
          case 'vat_regime_id':
            $vat_regime_id = $db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$info->f('buyer_id')."' ");          
            /*if($vat_regime_id==0){
              $vat_regime_id = 1;
            }*/
            if(empty($vat_regime_data)){
              $data .= build_vat_regime_dd($vat_regime_id,1).$delimiter;   
            }else{                          
              $data .= $vat_regime_data[$vat_regime_id].$delimiter;
            } 
            break;
          case 'our_reference':
            $data .= $db->field("SELECT our_reference FROM customers WHERE customer_id='".$info->f('buyer_id')."' ").$delimiter;
          break; 
          //change ids or other data into readable data in .csv  
          case 'paid':          
          case 'sent': 
          case 'f_archived':
          case 'remove_vat':
            if($info->f($key)=='1'){
              $data .= gm('Yes').$delimiter;  
            }elseif($info->f($key)=='2'){
              $data .= gm('Partially Paid').$delimiter;
            }else{
              $data .= gm('No').$delimiter;
            }            
            break;   

          case 'status':
            switch ($info->f($key)){
              case '0':
                if($info->f('sent') == '0'){                  
                  $data .= gm('Draft').$delimiter;
                }else{                  
                  if($info->f('due_date') < time()){                   
                    $data .= gm('Late').$delimiter;
                  }else{
                    if($info->f('paid') == '2' ){                    
                      $data .= gm('Partially Paid').$delimiter;
                    }else {
                      $data .= gm('Final').$delimiter;
                    }
                  }
                }
                break;
              case '1':                                
                $info->f('type')=='2'? $data .= gm('Final').$delimiter : $data .= gm('Paid').$delimiter;
                break;
              default:                
                $data .= gm('Draft').$delimiter;
                break;
            }
            break;

          case 'notes':            
            $notess = $db->field("SELECT item_value FROM note_fields WHERE item_type = 'invoice' AND item_name = 'notes' AND active = '1' AND item_id = '".$info->f('id')."' ");
            $text = str_replace(array("\r\n", "\r"), " ", $notess);            
            $data .= $text.$delimiter;
            break;

          case 'buyer_country_id':
          case 'seller_b_country_id':
            $country_name = $db->field("SELECT code FROM country WHERE country_id = '".$info->f($key)."' ");
            $data .= $country_name.$delimiter;
            break;

          case 'email_language':
            $lang_code = build_language_dd_new($info->f($key),'country_code');
            $data .= $lang_code.$delimiter;
            break;

          default:
            $data .= utf8_decode($info->f($key)).$delimiter;
            break;
        }    
      }
      foreach ($fields_line as $k => $val) {
        switch ($k) {
          case 'name':
            $text = str_replace(array("\r\n", "\r"), " ", $lines->f($k));
            $data .= $text.$delimiter;
            break;          
          case 'price':                    
            $data .= display_number($lines->f($k)).$delimiter;
            break;
          case 'vat':        
            $vat_val = $lines->f($k);
            if($info->f('remove_vat')==1){
              $vat_val = 0;
            }
            $data .= display_number($vat_val).$delimiter;
            break;  
          case 'discount':  
            $apply_discount = $info->f('apply_discount');
            $discount_l = $lines->f($k);
            if($apply_discount == 2 || $apply_discount == 0){
              $discount_l = 0;
            }
            $data .= display_number($discount_l).$delimiter;
            break;
          case 'line_vat_amount':            
            $amount_l = $lines->f('amount');
            $discount_l = $lines->f('discount');
            $apply_discount = $info->f('apply_discount');
            if($apply_discount == 2 || $apply_discount == 0){
              $discount_l = 0;
            }
            $amount_discount_l = $amount_l/100*$discount_l;
            $line_vat_amount = ($amount_l - $amount_discount_l)/100*$lines->f('vat');
            $global_discount = $info->f('discount');
            $apply_discount = $info->f('apply_discount');
            if($apply_discount < 2){
              $global_discount = 0;              
            }
            $line_vat_amount = $line_vat_amount - ($line_vat_amount/100*$global_discount);
            if($info->f('remove_vat')==1){
              $line_vat_amount = 0; 
            }
            $data .= display_number($line_vat_amount).$delimiter;
            break;

            case 'line_amount_with_vat':              
              $amount_l = $lines->f('amount');
              $discount_l = $lines->f('discount');
              $apply_discount = $info->f('apply_discount');
              if($apply_discount == 2 || $apply_discount == 0){
                $discount_l = 0;
              }
              $amount_discount_l = $amount_l/100*$discount_l;
              $line_vat_amount = ($amount_l - $amount_discount_l)/100*$lines->f('vat');
              $global_discount = $info->f('discount');
              $apply_discount = $info->f('apply_discount');
              if($apply_discount < 2){
                $global_discount = 0;              
              }
              $line_vat_amount = $line_vat_amount - ($line_vat_amount/100*$global_discount);
              if($info->f('remove_vat')==1){
                $line_vat_amount = 0; 
              }
              $amount_with_discount = (($amount_l - $amount_discount_l)-(($amount_l - $amount_discount_l)/100*$global_discount));
              $line_amount_with_vat = $amount_with_discount+$line_vat_amount;
              $data .= display_number($line_amount_with_vat).$delimiter;
              break;

            case 'line_amount_with_disc':   
            case 'amount':           
              $amount_l = $lines->f('amount');
              $discount_l = $lines->f('discount');
              $apply_discount = $info->f('apply_discount');
              if($apply_discount == 2 || $apply_discount == 0){
                $discount_l = 0;
              }
              $amount_discount_l = $amount_l/100*$discount_l;              
              $global_discount = $info->f('discount');              
              if($apply_discount < 2){
                $global_discount = 0;              
              }              
              $line_amount_with_disc = (($amount_l - $amount_discount_l)-(($amount_l - $amount_discount_l)/100*$global_discount));              
              $data .= display_number($line_amount_with_disc).$delimiter;
              break;  

            case 'vat_classification':
              $vat_regime_id = $db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$info->f('buyer_id')."' ");   
              if(!$vat_regime_id){
                $vat_regime_id = 1;
              }
              $line_vat = $lines->f('vat');   
              $line_vat_id = $db->field("SELECT vat_id FROM vats WHERE value = '".$line_vat."' ");
              if($line_vat_id){
                $vat_classification = $db->field("SELECT field_value FROM invoice_export_settings 
                                          WHERE field_name = 'vat_classification' 
                                          AND field_id = '".$line_vat_id."' 
                                          AND field_id_2 = '".$vat_regime_id."' ");
              }              
              $data .= $vat_classification.$delimiter;
              break;

          default:
            $data .= utf8_decode($lines->f($k)).$delimiter;
            break; 
        }   
      }
      foreach ($custom as $key => $value) {
        $data .= $value.$delimiter;
      }
      $data = rtrim($data,$delimiter);    
      $data .="\n";
    }
  }
  // $exist = $db->query("SELECT * FROM tblinvoice_exported WHERE list_id='".$in['list_id']."' AND invoice_id='".$info->f('id')."'");
  // if(!$exist->next()){
  //   $insert = $db->query("INSERT INTO tblinvoice_exported SET list_id='".$in['list_id']."', invoice_id='".$info->f('id')."' ");    
  // }
  // $delete = $db->query("DELETE FROM tblinvoice_to_export WHERE invoice_id='".$info->f('id')."' AND time='".$_SESSION['invoice_to_export']."' ");
}
$data = rtrim($data,"\n");
// console::log($data);
doQueryLog();
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
echo $data;
// unset($_SESSION['invoice_to_export']);

exit();
?>