<?php
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();
include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');

$q = strtolower($in["term"]);
global $database_config;

$db = new sqldb();

$filter ="";

$db->query("SELECT pim_p_orders.p_order_id,pim_p_orders.serial_number,pim_p_orders.customer_id,pim_p_orders.order_full
			 FROM pim_p_orders			 
			 WHERE pim_p_orders.customer_id= '".$in['current_id']."' AND pim_p_orders.order_full!=1 ");


while ($db -> move_next()) {
	$items[trim($db -> f('serial_number'))] = $db->f('p_order_id');
}

$result = array();
if($items){
	foreach ($items as $key=>$value) {
		if($q){
			if (strpos(strtolower($key), $q) !== false) {
				array_push($result, array("id"=>$value,  "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key))));
			}
		}else{
			array_push($result, array("id"=>$value, "value" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key))));
		}

		
	}
}
json_out($result);
?>