<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

if($in['reset_list']){
 	if(isset($_SESSION['tmp_add_to_xls'])){
 		unset($_SESSION['tmp_add_to_xls']);
 	}
 	if(isset($_SESSION['add_to_xls'])){
 		unset($_SESSION['add_to_xls']);
 	}
}

$result=array();

$l_r = ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$filter = 'WHERE 1=1 ';
$order_by = " ORDER BY recurring_invoice.next_date ASC ";
if(!$in['archived']){
	$filter.= " AND recurring_invoice.f_archived='0' ";
}else{
	$filter.= " AND recurring_invoice.f_archived='1' ";
	$arguments.="&archived=".$in['archived'];
}
if($in['filter']){
	$arguments.="&filter=".$in['filter'];
}

if($in['search']){
	//$filter.=" and recurring_invoice.buyer_name like '%".$in['search']."%'";
	$filter.=" and (recurring_invoice.buyer_name like '%".$in['search']."%' OR recurring_invoice.title like '%".$in['search']."%') ";
	$arguments.="&search=".$in['search'];
}

$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

if($in['order_by']){
	$order = " ASC ";
	if($in['desc'] == '1' || $in['desc']=='true'){
		$order = " DESC ";
	}
	if($in['order_by']=='mrr'){
		$order_by=" ";
		$filter_limit =" ";
	}elseif($in['order_by']=='last_upd'){
		$query_date_format =get_query_date_format();
		$order_by =" ORDER BY STR_TO_DATE(last_upd, '$query_date_format') ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	}else{
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	}	
}
if($in['period_id']==1){
	$filter.=" AND recurring_invoice.frequency=1 ";
}
if($in['period_id']==2){
	$filter.=" AND recurring_invoice.frequency=2 ";
}
if($in['period_id']==3){
	$filter.=" AND recurring_invoice.frequency=3 ";
}
if($in['period_id']==4){
	$filter.=" AND recurring_invoice.frequency=4 ";
}
if($in['period_id']==5){
	if($in['from'] && !$in['to']){
		$today  			= date('d',strtotime($in['from']));
		$month        		= date('n', strtotime($in['from']));
		$year         		= date('Y', strtotime($in['from']));
		$time_start = mktime(0,0,0,$month,$today,$year);
		$time_end = mktime(0,0,0,date('n'),date('d'),date('Y'));
	}else if($in['to'] && !$in['from']){
		$today  			= date('d',strtotime($in['to']));
		$month        		= date('n', strtotime($in['to']));
		$year         		= date('Y', strtotime($in['to']));
		$time_start=946684800;
		$time_end = mktime(0,0,0,$month,$today,$year);
	}else{
		$today  			= date('d',strtotime($in['from']));
		$month        		= date('n', strtotime($in['from']));
		$year         		= date('Y', strtotime($in['from']));
		$time_start = mktime(0,0,0,$month,$today,$year);
		$today  			= date('d',strtotime($in['to']));
		$month        		= date('n', strtotime($in['to']));
		$year         		= date('Y', strtotime($in['to']));
		$time_end = mktime(0,0,0,$month,$today,$year);
	}
	$filter.=" AND recurring_invoice.next_date BETWEEN '".$time_start."' AND '".$time_end."' ";
}

    $result['s_buyer_name']    = $in['s_buyer_id'];
    $result['buyer_id']        = $in['buyer_id'];

$stripe_active =$db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");
$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
	if($easyinvoice=='1'){
		$result['show_deliveries']	 	=false;
		$result['show_projects']		= false;
		$result['show_interventions']	= false;
	}else{
		$result['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
		$result['show_projects']		= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
		$result['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
	}
$nav= array();

$max_rows_data = $db->query("SELECT  recurring_invoice.recurring_invoice_id FROM recurring_invoice
            ".$filter." ".$order_by);

$max_rows=$max_rows_data->records_count();

if(!$_SESSION['tmp_add_to_xls'] || ($_SESSION['tmp_add_to_xls'] && empty($_SESSION['tmp_add_to_xls']))){
	while($max_rows_data->next()){
		$_SESSION['tmp_add_to_xls'][$max_rows_data->f('recurring_invoice_id')]=1;
		array_push($nav, (object)['recurring_invoice_id'=> $max_rows_data->f('recurring_invoice_id') ]);
	}
}else{
	while($max_rows_data->next()){
		array_push($nav, (object)['recurring_invoice_id'=> $max_rows_data->f('recurring_invoice_id') ]);
	}
}
$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_to_xls']){
	if($max_rows>0 && count($_SESSION['add_to_xls']) == $max_rows){
		$all_pages_selected=true;
	}else if(count($_SESSION['add_to_xls'])){
		$minimum_selected=true;
	}	
}

$recurring_invoice = $db->query("SELECT recurring_invoice.*, mandate.sepa_number,vat_new.description AS vat_regime
            FROM recurring_invoice
            LEFT JOIN mandate ON recurring_invoice.mandate_id = mandate.mandate_id
            LEFT JOIN vat_new ON recurring_invoice.vat_regime_id=vat_new.id
            ".$filter." GROUP BY recurring_invoice.recurring_invoice_id ".$order_by.$filter_limit);

//$max_rows=$recurring_invoice->records_count();
$result['query'] = array();
$result['max_rows']=$max_rows;
$result['all_pages_selected']= $in['exported'] ? false : $all_pages_selected;
$result['minimum_selected']= $in['exported'] ? false : $minimum_selected;
$result['nav'] = $nav;
//$recurring_invoice->move_to($offset*$l_r);
$j=0;
$discount_procent=0;
$vat_procent=0;

while($recurring_invoice->move_next()){

	switch($recurring_invoice->f('frequency')){
		    case 1:  
		    	$frequency= gm('Weekly'); 
		    	$mrr_invoice=$recurring_invoice->f('amount')>0 ? $recurring_invoice->f('amount')*52/12 : 0;
		    	break;
		    case 2:  
		    	$frequency= gm('Monthly'); 
		    	$mrr_invoice=$recurring_invoice->f('amount');
		    	break;
		    case 3:  
		    	$frequency= gm('Quarterly'); 
		    	$mrr_invoice=$recurring_invoice->f('amount')>0 ? $recurring_invoice->f('amount')/3 : 0;
		    	break;
		    case 4:  
		    	$frequency= gm('Yearly'); 
		    	$mrr_invoice=$recurring_invoice->f('amount')>0 ? $recurring_invoice->f('amount')/12 : 0;
		    	break;
		    case 5:  
		    	$frequency= gm('Every').' '.$recurring_invoice->f('days').' '.gm('Day'); 
		    	$mrr_invoice=$recurring_invoice->f('amount')>0 ? $recurring_invoice->f('amount')*365/($recurring_invoice->f('days')*12) : 0;
		    	break;
	}

	$recipients="";
	if($recurring_invoice->f('invoice_delivery')){
		$recipients_data=$db->query("SELECT email FROM recurring_email_contact WHERE recurring_invoice_id='".$recurring_invoice->f('recurring_invoice_id')."' ");
		while($recipients_data->next()){
			$recipients.=$recipients_data->f('email').",";
		}
		$recipients=rtrim($recipients,",");
	}
	
	$item=array(
		'edit_link'     		=> 'index.php?do=invoice-recurring_ninvoice&recurring_invoice_id='.$recurring_invoice->f('recurring_invoice_id'),
	  	'activate_link' 		=> array('do'=>'invoice-recurring_invoices-invoice-activater','recurring_invoice_id'=>$recurring_invoice->f('recurring_invoice_id')),
		'archive_link'   		=> array('do'=>'invoice-recurring_invoices-invoice-archive_recurring','recurring_invoice_id'=>$recurring_invoice->f('recurring_invoice_id')),
		'delete_link'   		=> array('do'=>'invoice-recurring_invoices-invoice-delete_recurring','recurring_invoice_id'=>$recurring_invoice->f('recurring_invoice_id')),
		'view_delete_link2'	=> $_SESSION['access_level'] == 1 ? ($in['archived'] ? true : false) : false,
		'hide_on_a'			=> $in['archived'] == 1 ? false : true,
		'alt_for_delete_icon'	=> $in['archived'] == 1 ? 'Delete' : 'Archive',

		'next_date'			=> $recurring_invoice->f('start_date') > time() ? date(ACCOUNT_DATE_FORMAT,$recurring_invoice->f('start_date')) : date(ACCOUNT_DATE_FORMAT,$recurring_invoice->f('next_date')),
		'buyer_name'  		=> $recurring_invoice->f('buyer_name'),
		'title'  	    		=> $recurring_invoice->f('title'),
		'mandate_reference'	=> $recurring_invoice->f('sepa_number'),
		'frequency'   		=> $frequency,
		'id'				=> $recurring_invoice->f('recurring_invoice_id'),
	//	'currency'  		=> get_currency($db->f('currency_type')),
		'amount'      		=> place_currency(display_number($recurring_invoice->f('amount')), get_commission_type_list($recurring_invoice->f('currency_type'))),
		'archived'			=> $in['archived'] ? $in['archived'] : "",
		'view_delete_link'     => $recurring_invoice->f('f_archived')==1 ? false : true,
            'view_archive_link'    => $recurring_invoice->f('f_archived')==1 ? true : false,
            'contract_reference'   => $recurring_invoice->f('contract_id') ? $recurring_invoice->f('our_ref') : '',
        'check_add_to_product'		=> $in['exported'] ? false : ($_SESSION['add_to_xls'][$recurring_invoice->f('recurring_invoice_id')] == 1 ? true : false),
        'start_date'	=> $recurring_invoice->f('start_date') ? date(ACCOUNT_DATE_FORMAT,$recurring_invoice->f('start_date')) : '',
        'end_date'	=> $recurring_invoice->f('end_date') ? date(ACCOUNT_DATE_FORMAT,$recurring_invoice->f('end_date')) : '',
        'invoice_email'		=> $recipients,
        'payment_method'	=> get_payment_method($recurring_invoice->f('payment_method') ),
        'your_ref'			=> $recurring_invoice->f('your_ref'),
        'our_ref'			=> $recurring_invoice->f('our_ref'),
        'mrr'				=> place_currency(display_number($mrr_invoice), get_commission_type_list($recurring_invoice->f('currency_type'))),
        'mrr_ord'			=> $mrr_invoice,
        'last_upd'			=> $recurring_invoice->f('last_upd'),
        'vat_regime'		=> stripslashes($recurring_invoice->f('vat_regime'))
	);

    	array_push($result['query'], $item);
	$j++;
}
	$filter_kpi = $filter." AND recurring_invoice.next_date > UNIX_TIMESTAMP(CURDATE()) AND MONTH( DATE_ADD(FROM_UNIXTIME( `next_date` ), INTERVAL 3 HOUR) ) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(`next_date`)) = YEAR(CURDATE()) ";
	
	$amount = $db->query("SELECT SUM(recurring_invoice.amount) AS to_be_invoiced
			FROM recurring_invoice
			".$filter_kpi);

	/*$filter_kpi2 = $filter." AND MONTH(FROM_UNIXTIME(`next_date`)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(`next_date`)) = YEAR(CURDATE())";
	$amount2 = $db->query("SELECT SUM(amount) AS `amount_this_month`
			FROM recurring_invoice
			".$filter_kpi2);*/

	/*$filter_kpi2 = "WHERE created_by ='System' AND MONTH( DATE_ADD(FROM_UNIXTIME( `invoice_date` ), INTERVAL 3 HOUR) ) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(`invoice_date`)) = YEAR(CURDATE())";
	$amount2 = $db->query("SELECT SUM(amount) AS `amount_this_month`
			FROM tblinvoice
			".$filter_kpi2);*/

	$current_day=mktime(23,59,59,date("n"), date("j"), date("Y"));
	$mrr_year=$db->field("SELECT COALESCE(SUM(amount),0) FROM recurring_invoice WHERE start_date <= '".$current_day."' AND next_date>'".$current_day."' AND (end_date>'".$current_day."' OR end_date='0') AND f_archived='0' AND frequency='4'");
	$mrr_year_final=$mrr_year ? $mrr_year/12 : 0;
	$mrr_quarter=$db->field("SELECT COALESCE(SUM(amount),0) FROM recurring_invoice WHERE start_date <= '".$current_day."' AND next_date>'".$current_day."' AND (end_date>'".$current_day."' OR end_date='0') AND f_archived='0' AND frequency='3'");
	$mrr_quarter_final=$mrr_quarter ? $mrr_quarter/3 : 0;
	$mrr_month_final=$db->field("SELECT COALESCE(SUM(amount),0) FROM recurring_invoice WHERE start_date <= '".$current_day."' AND next_date>'".$current_day."' AND (end_date>'".$current_day."' OR end_date='0') AND f_archived='0' AND frequency='2'");
	$mrr_week=$db->field("SELECT COALESCE(SUM(amount),0) FROM recurring_invoice WHERE start_date <= '".$current_day."' AND next_date>'".$current_day."' AND (end_date>'".$current_day."' OR end_date='0') AND f_archived='0' AND frequency='1'");
	$mrr_week_final=$mrr_week ? $mrr_week*52/12 : 0;
	$mrr_custom_final=$db->field("SELECT COALESCE(SUM(amount*365/(days*12)),0) FROM recurring_invoice WHERE start_date <= '".$current_day."' AND next_date>'".$current_day."' AND (end_date>'".$current_day."' OR end_date='0') AND f_archived='0' AND frequency='5'");
	$mrr=$mrr_year_final+$mrr_quarter_final+$mrr_month_final+$mrr_week_final+$mrr_custom_final;

	$result['pagg']		 = $link;
	$result['view_go']	 = $_SESSION['access_level'] == 1 ? true : false;
	$result['view_active']	 = !$in['archived'] ? false : true;
	$result['archived_list'] = !$in['archived'] ? 'index.php?do='.$in['do'].'&archived=1' : '';
	$result['view_archived'] = $in['archived'] ? false : true;
	$result['active_list']   = $in['archived'] ? 'index.php?do='.$in['do'] : '';
	$result['archived']	 = $in['archived'];
	//$result['amount_this_month'] = place_currency(display_number($amount2->f('amount_this_month')+$amount->f('to_be_invoiced')));
	$result['to_be_invoiced'] = place_currency(display_number($amount->f('to_be_invoiced')));
	$result['mrr']=place_currency(display_number($mrr));

$result['lr'] = $l_r;
$_SESSION['filters'] = $arguments.$arguments_o;
$result['columns']=get_invoices_cols();

	if($in['order_by']=='mrr'){
	   if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
	   }else{
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
	   }
	   $exo = array_slice( $exo, $offset*$l_r, $l_r);
	   $result['query']=array();
       foreach ($exo as $key => $value) {
           array_push($result['query'], $value);
       }	    
	}

json_out($result);

function get_invoices_cols(){
	$db=new sqldb();
	$array=array();
	$cols_default=default_columns_dd(array('list'=>'recinvoices'));
	$cols_order_dd=default_columns_order_dd(array('list'=>'recinvoices'));
	$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='recinvoices' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
	if(!count($cols_selected)){
		$i=1;
        $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'recinvoices'));
        foreach($cols_default as $key=>$value){
			$tmp_item=array(
				'id'				=> $i,
				'column_name'		=> $key,
				'name'				=> $value,
				'order_by'			=> $cols_order_dd[$key]
			);
            if ($default_selected_columns_dd[$key]) {
                array_push($array,$tmp_item);
            }			$i++;
		}
	}else{
		foreach($cols_selected as $key=>$value){
			$tmp_item=array(
				'id'				=> $value['id'],
				'column_name'		=> $value['column_name'],
				'name'				=> $cols_default[$value['column_name']],
				'order_by'			=> $cols_order_dd[$value['column_name']]
			);
			array_push($array,$tmp_item);
		}
	}
	return $array;
}

	
