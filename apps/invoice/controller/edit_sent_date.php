<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array();
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);
$result['invoice_ids']=array();
$invoices = $db->query("SELECT * FROM tblinvoice WHERE sent='1' AND status ='0' AND f_archived='0' AND buyer_id = '".$in['customer_id']."' AND due_date < ".time()." ORDER BY invoice_grade DESC");
while ($invoices->next()) {
	array_push($result['invoice_ids'], $invoices->f('id'));
}

$sent_date = $db->field("SELECT sent_date FROM reminders_logs WHERE company_id ='".$in['customer_id']."' ORDER BY log_id DESC LIMIT 1");

	$result['title'] 			= gm('Manually Edit Sent Date');
	$result['sent_date'] 		= $sent_date;
	$result['customer_id'] 		= $in['customer_id'];
	$result['index']			= $in['index'];
	$result['account_date_format'] = pick_date_format();

json_out($result);