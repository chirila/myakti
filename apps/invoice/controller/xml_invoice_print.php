<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
if($in['dbase']){
	global $database_config;
	$database_1 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $in['dbase'],
	);
	$db = new sqldb($database_1);
}else{
	$db = new sqldb();
}

$ACCOUNT_ADDRESS_INVOICE=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_ADDRESS_INVOICE' ");
$ACCOUNT_DELIVERY_ADDRESS=$db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ADDRESS' ");
$ACCOUNT_DELIVERY_CITY=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_CITY' ");
$ACCOUNT_DELIVERY_ZIP=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_ZIP' ");
$ACCOUNT_DELIVERY_COUNTRY_ID=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_DELIVERY_COUNTRY_ID' ");
$ACCOUNT_BILL_ADDRESS=$db->field("SELECT (CASE WHEN long_value IS NOT NULL THEN long_value ELSE value END) as value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ADDRESS' ");
$ACCOUNT_BILL_CITY=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_CITY' ");
$ACCOUNT_BILL_ZIP=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_ZIP' ");
$ACCOUNT_BILL_COUNTRY_ID=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BILLING_COUNTRY_ID' ");
$ACCOUNT_VAT_NUMBER=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
$ACCOUNT_IBAN=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_IBAN' ");
$ACCOUNT_BIC=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_BIC_CODE' ");
$ACCOUNT_COMPANY=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_COMPANY' ");
$USE_NEGATIVE_CREDIT=$db->field("SELECT value FROM settings WHERE `constant_name`='USE_NEGATIVE_CREDIT' ");

$ACCOUNT_BILLING_ADDRESS=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ADDRESS : $ACCOUNT_BILL_ADDRESS;
$ACCOUNT_BILLING_CITY=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_CITY : $ACCOUNT_BILL_CITY;
$ACCOUNT_BILLING_ZIP=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_ZIP : $ACCOUNT_BILL_ZIP;
$ACCOUNT_BILLING_COUNTRY_ID=!$ACCOUNT_ADDRESS_INVOICE || $ACCOUNT_ADDRESS_INVOICE=='0' ? $ACCOUNT_DELIVERY_COUNTRY_ID : $ACCOUNT_BILL_COUNTRY_ID;

$invoice = $db->query("SELECT * FROM tblinvoice WHERE id='".$in['id']."' ");
$contact = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."' ");
$customer_reference=$db->field("SELECT our_reference FROM customers WHERE customer_id='".$invoice->f('buyer_id')."' ");

$code_supplier=get_country_code($ACCOUNT_DELIVERY_COUNTRY_ID)=='BE' ? ($invoice->f('type')==2 || $invoice->f('type')==4? 'BE:VAT' : 'BE:CBE') : '';
$code_supplier_simple=get_country_code($ACCOUNT_DELIVERY_COUNTRY_ID)=='BE' ? 'BE' : '';
$code_customer=get_country_code($invoice->f('buyer_country_id'))=='BE' ? ($invoice->f('type')==2 || $invoice->f('type')==4? 'BE:VAT' : 'BE:CBE') : '';
$code_customer_simple=get_country_code($invoice->f('buyer_country_id'))=='BE' ? 'BE' : '';
$contactUblEmail=ublContactEmail(['db'=>$in['dbase'],'customer_id'=>$invoice->f('buyer_id'),'contact_id'=>$invoice->f('contact_id')]);
$tax_id=array(
		array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
		array('0'=>'00','6'=>'01','12'=>'02','21'=>'03'),
		array('0'=>'00','6'=>'','12'=>'','21'=>''),
		array('0'=>'00','6'=>'','12'=>'','21'=>''),
		array('0'=>'00','6'=>'','12'=>'','21'=>''),
	);
$tax_name=array(
		array('0'=>'Z','6'=>'AA','12'=>'AA','21'=>'S'),
		array('0'=>'Z','6'=>'AA','12'=>'AA','21'=>'S'),
		array('0'=>'AE','6'=>'','12'=>'','21'=>''),
		array('0'=>'E','6'=>'','12'=>'','21'=>''),
		array('0'=>'AE','6'=>'','12'=>'','21'=>''),
	);
if($invoice->f('vat_regime_id')>=10000){
	$regime_type=$db->field("SELECT regime_type FROM vat_new WHERE id='".$invoice->f('vat_regime_id')."' ");
}	
$in['serial_number'] = $invoice->f('serial_number');
if($in['dbase'] && $in['efff_str']){
	$str=$in['efff_str'];
}else{
	$params1 = array('id'=>$invoice->f('id'),'lid'=>$invoice->f('email_language'),'base64'=>1,'save_xml'=>1,'type_pdf'=>$invoice->f('type'));
	if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==0){
		$params1['type']=$invoice->f('pdf_layout');
		$params1['logo']=$invoice->f('pdf_logo');
	}else if($invoice->f('pdf_layout') && $invoice->f('use_custom_template')==1){
		$params1['custom_type']=$invoice->f('pdf_layout');
		$params1['logo']=$invoice->f('pdf_logo');
	}else if($invoice->f('type')!=2){
		$params1['type']=ACCOUNT_INVOICE_PDF_FORMAT;
	}
	if($invoice->f('type')==2){
		$params1['type']=ACCOUNT_INVOICE_CREDIT_HEADER_PDF_FORMAT;
	}
	#if we are using a customer pdf template
	if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $invoice->f('pdf_layout') == 0 && $invoice->f('type')!=2){
		$params1['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
	}
	require_once(__DIR__.'/../model/btb.php');
	//$invoice_obj=new btb();
	$invoice_obj=new btb($in);
	$str = $invoice_obj->generate_pdf($params1);
}

$get_invoice_rows = $db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['id']."' AND content='0' order by sort_order ASC ")->getAll();
$i = 0;
$total_amount = 0;
$vat_percent = array();
$sub_t = array();
$sub_disc = array();
$vat_percent_val = 0;
$discount = $invoice->f('discount');
if($invoice->f('apply_discount')<2){
	$discount=0;
}
foreach($get_invoice_rows as $key=>$value){
	$line_d = $value['discount'];
	if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
		$line_d = 0;
	}
	$amount = $value['quantity'] *($value['price'] - ( $value['price'] * $line_d / 100 ) );
	$amount = round($amount,ARTICLE_PRICE_COMMA_DIGITS);

	$total_amount += $value['amount'];
	//$i++;
	$amount_d = $amount * $discount/100;
	if($invoice->f('apply_discount') < 2){
		$amount_d = 0;
	}
	$vat_percent_val = ($amount - $amount_d) * $value['vat']/100;

	$vat_percent[$value['vat']] += $vat_percent_val;
	$sub_t[$value['vat']] += $amount;
	$sub_disc[$value['vat']] += $amount_d;
}
$tax_str ='';
$vat_v = 0;
$total_no_vat = 0;
$total_vat =0;
foreach ($vat_percent as $key => $val){
	if($sub_t[$key] - $sub_disc[$key]){
    		$vat_v += $val;
    		$total_no_vat += $sub_t[$key] - $sub_disc[$key];
    		$total_vat +=$sub_t[$key] - $sub_disc[$key]+$val;
	}
}
if($invoice->f('type')==2 || $invoice->f('type')==4){
	if(!is_null($USE_NEGATIVE_CREDIT)){
		if($USE_NEGATIVE_CREDIT == 1){
			if($total_vat < 0){ #negativ amounts
				$doc_type = '381';
			}else{
				$doc_type = '380';
			}
		}else{
			if($total_vat < 0){ #negativ amounts
				$doc_type = '380';
			}else{
				$doc_type = '381';
			}
		}
	}else{
		if($total_vat < 0){ #negativ amounts
			$doc_type = '380';
		}else{
			$doc_type = '381';
		}
	}
}else{
	$doc_type = '380';
}
$currency=currency::get_currency($invoice->f('currency_type'),'code');

foreach($get_invoice_rows as $key=>$value){
	$line_d = $value['discount'];
	if($invoice->f('apply_discount') ==0 || $invoice->f('apply_discount') == 2){
		$line_d = 0;
	}
	$amount = $value['amount'] - $value['amount'] * $line_d /100;
	$i++;

	$amount_d = $amount * $discount/100;
	if($invoice->f('apply_discount') < 2){
		$amount_d = 0;
	}
	$gl_account='';
	if($value['article_id']){
		$line_gl=$db->field("SELECT ledger_account_id FROM pim_articles WHERE article_id='".$value['article_id']."' ");
		if($line_gl){
			$gl_account=$db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$line_gl."' ");
		}
	}
	if((!$gl_account || $gl_account=='') && !$value['content']){
		$app_gl=$db->field("SELECT value FROM settings WHERE constant_name='BTB_LEDGER_ID' ");
		if($app_gl){
			$gl_account=$db->field("SELECT name FROM pim_article_ledger_accounts WHERE id='".$app_gl."' ");
		}
	}
	$ln_amount = twoDecNumber($amount-$amount_d);
	$ln_amount_vat = twoDecNumber($ln_amount*$value['vat']/100);
	$unit_price=twoDecNumber(($value['price']- $value['price'] * $line_d /100)-($value['price']- $value['price'] * $line_d /100)*$discount/100);
	$item_code_xml = '';
	if(htmlspecialchars($value['item_code'])){
		$item_code_xml.= '<cac:SellersItemIdentification>
                <cbc:ID>'.htmlspecialchars(stripslashes($value['item_code']),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
            </cac:SellersItemIdentification>';
    }
           // if($invoice->f('type')==2){
            	$item_code_xml.='<cac:ClassifiedTaxCategory>';
				if($invoice->f('vat_regime_id')<5){
					$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$value['vat']].'</cbc:ID>
					<cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$value['vat']].'</cbc:Name>';
				}else{
					if($regime_type==1){
						$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$value['vat']].'</cbc:ID>
					<cbc:Name>'.$tax_id[0][$value['vat']].'</cbc:Name>';
					}else if($regime_type==2){
						$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$value['vat']].'</cbc:ID>
					<cbc:Name>'.$tax_id[2][$value['vat']].'</cbc:Name>';
					}else{
						$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$value['vat']].'</cbc:ID>
					<cbc:Name>'.$tax_id[3][$value['vat']].'</cbc:Name>';
					}
				}		
				$item_code_xml.='<cbc:Percent>'.twoDecNumber($value['vat']).'</cbc:Percent>
				<cac:TaxScheme>
					<cbc:ID>VAT</cbc:ID>
				</cac:TaxScheme>
			</cac:ClassifiedTaxCategory>';
           // }
	//}
	if($invoice->f('type')==2 || $invoice->f('type')==4){
		$line_xml = '<cac:CreditNoteLine>';
	}else{
		$line_xml = '<cac:InvoiceLine>';
	}
	$line_xml.='<cbc:ID>'.$i.'</cbc:ID>';
	    if($invoice->f('type')==2 || $invoice->f('type')==4){
	    	$line_xml.='<cbc:CreditedQuantity unitCode="NAR" unitCodeListID="UNECERec20">'.twoDecNumber($value['quantity']).'</cbc:CreditedQuantity>';
	    }else{
	    	$line_xml.='<cbc:InvoicedQuantity unitCode="NAR" unitCodeListID="UNECERec20">'.twoDecNumber($value['quantity']).'</cbc:InvoicedQuantity>';
	    }
	    $line_xml.='<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($ln_amount).'</cbc:LineExtensionAmount>';
	    if(!$in['remove_accounting_cost']){
	    	$line_xml.='<cbc:AccountingCost>'.$gl_account.'</cbc:AccountingCost>';
	    }
	    $line_xml.='<cac:TaxTotal>
		  <cbc:TaxAmount currencyID="'.$currency.'">'.$ln_amount_vat.'</cbc:TaxAmount>';
		  /*<cac:TaxSubtotal>
		    <cbc:TaxableAmount currencyID="'.$currency.'">'.$ln_amount.'</cbc:TaxableAmount>
		    <cbc:TaxAmount currencyID="'.$currency.'">'.$ln_amount_vat.'</cbc:TaxAmount>		 
		    <cac:TaxCategory>';
		    if($invoice->f('vat_regime_id')<5){
		    	$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$value['vat']].'</cbc:ID>
		      <cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$value['vat']].'</cbc:Name>';
		    }else{
		    	if($regime_type==1){
		    		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$value['vat']].'</cbc:ID>
		      		<cbc:Name>'.$tax_id[0][$value['vat']].'</cbc:Name>';
				}else if($regime_type==2){
					$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$value['vat']].'</cbc:ID>
		      <cbc:Name>'.$tax_id[2][$value['vat']].'</cbc:Name>';
				}else{
					$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$value['vat']].'</cbc:ID>
		      <cbc:Name>'.$tax_id[3][$value['vat']].'</cbc:Name>';
				}  	
		    }      
		      $line_xml.='<cbc:Percent>'.twoDecNumber($value['vat']).'</cbc:Percent>
		      <cbc:TaxExemptionReason/>
			<cac:TaxScheme>
				<cbc:ID>VAT</cbc:ID>
			</cac:TaxScheme>
		    </cac:TaxCategory>
		  </cac:TaxSubtotal>*/
		$line_xml.='</cac:TaxTotal>
		<cac:Item>
		  <cbc:Description>'.htmlspecialchars(stripslashes($value['name']),ENT_QUOTES | ENT_HTML5).'</cbc:Description>
		  <cbc:Name>'.htmlspecialchars(stripslashes($value['name']),ENT_QUOTES | ENT_HTML5).'</cbc:Name>
		  '.$item_code_xml.'	        
		</cac:Item>
		<cac:Price>
			<cbc:PriceAmount currencyID="'.$currency.'">'.twoDecNumber($unit_price).'</cbc:PriceAmount>
		</cac:Price>';
		if($invoice->f('type')==2 || $invoice->f('type')==4){
			$line_xml.='</cac:CreditNoteLine>';
		}else{
			$line_xml.='</cac:InvoiceLine>';
		}               
	  $xml_lines.=$line_xml;
}

if(!$xml_lines){
	//add an empty line
	$item_code_xml.='<cac:ClassifiedTaxCategory>';
	if($invoice->f('vat_regime_id')<5){
		$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][0].'</cbc:ID>
		<cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][0].'</cbc:Name>';
	}else{
		if($regime_type==1){
			$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][0].'</cbc:ID>
		<cbc:Name>'.$tax_id[0][0].'</cbc:Name>';
		}else if($regime_type==2){
			$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][0].'</cbc:ID>
		<cbc:Name>'.$tax_id[2][0].'</cbc:Name>';
		}else{
			$item_code_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][0].'</cbc:ID>
		<cbc:Name>'.$tax_id[3][0].'</cbc:Name>';
		}
	}		
	$item_code_xml.='<cbc:Percent>'.twoDecNumber(0).'</cbc:Percent>
	<cac:TaxScheme>
		<cbc:ID>VAT</cbc:ID>
	</cac:TaxScheme>
</cac:ClassifiedTaxCategory>';


	if($invoice->f('type')==2 || $invoice->f('type')==4){
		$line_xml = '<cac:CreditNoteLine>';
	}else{
		$line_xml = '<cac:InvoiceLine>';
	}

	$line_xml.='<cbc:ID>1</cbc:ID>';
    if($invoice->f('type')==2 || $invoice->f('type')==4){
    	$line_xml.='<cbc:CreditedQuantity unitCode="NAR" unitCodeListID="UNECERec20">'.twoDecNumber(1).'</cbc:CreditedQuantity>';
    }else{
    	$line_xml.='<cbc:InvoicedQuantity unitCode="NAR" unitCodeListID="UNECERec20">'.twoDecNumber(1).'</cbc:InvoicedQuantity>';
    }
    $line_xml.='<cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber(0).'</cbc:LineExtensionAmount>';

    $line_xml.='<cac:TaxTotal>
	  <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber(0).'</cbc:TaxAmount>';
	$line_xml.='</cac:TaxTotal>
		<cac:Item>
		  <cbc:Description></cbc:Description>
		  <cbc:Name></cbc:Name>
		  '.$item_code_xml.'	        
		</cac:Item>
		<cac:Price>
			<cbc:PriceAmount currencyID="'.$currency.'">'.twoDecNumber(0).'</cbc:PriceAmount>
		</cac:Price>';
		if($invoice->f('type')==2 || $invoice->f('type')==4){
			$line_xml.='</cac:CreditNoteLine>';
		}else{
			$line_xml.='</cac:InvoiceLine>';
		}         

	$xml_lines.=$line_xml;
}

$tax_str ='';
foreach ($vat_percent as $key => $val){
	if($sub_t[$key] - $sub_disc[$key]){
		$line_xml='<cac:TaxSubtotal>
	      <cbc:TaxableAmount currencyID="'.$currency.'">'.twoDecNumber($sub_t[$key] - $sub_disc[$key]).'</cbc:TaxableAmount>
	      <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($val).'</cbc:TaxAmount>
	      <cac:TaxCategory>';
	      if($invoice->f('vat_regime_id')<5){
	      	$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[$invoice->f('vat_regime_id')][$key].'</cbc:ID>
	        <cbc:Name>'.$tax_id[$invoice->f('vat_regime_id')][$key].'</cbc:Name>';
	      }else{
	      	if($regime_type==1){
	      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[0][$key].'</cbc:ID>
	        <cbc:Name>'.$tax_id[0][$key].'</cbc:Name>';
	      	}else if($regime_type==2){
	      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[2][$key].'</cbc:ID>
	        <cbc:Name>'.$tax_id[2][$key].'</cbc:Name>';
	      	}else{
	      		$line_xml.='<cbc:ID schemeID="UNCL5305">'.$tax_name[3][$key].'</cbc:ID>
	        <cbc:Name>'.$tax_id[3][$key].'</cbc:Name>';
	      	}
	      }
	        $line_xml.='<cbc:Percent>'.$key.'</cbc:Percent>
	        <cac:TaxScheme>
	          <cbc:ID>VAT</cbc:ID>
	        </cac:TaxScheme>
	      </cac:TaxCategory>
	    </cac:TaxSubtotal>';
    		$tax_str.=$line_xml;
	}
}
	
$xml ='<?xml version="1.0" encoding="UTF-8"?>';
if($invoice->f('type')==2 || $invoice->f('type')==4){
$xml.='<CreditNote xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2">';
}else{
$xml.='<Invoice xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2">';
}
$xml.='<cbc:UBLVersionID>2.1</cbc:UBLVersionID>
  <cbc:CustomizationID>urn:www.cenbii.eu:transaction:biitrns010:ver2.0:extended:urn:www.peppol.eu:bis:peppol5a:ver2.0:extended:e-fff:ver3.0</cbc:CustomizationID>
  <cbc:ProfileID>urn:www.cenbii.eu:profile:bii05:ver2.0</cbc:ProfileID>
  <cbc:ID>'.htmlspecialchars(stripslashes($invoice->f('serial_number')),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
  <cbc:IssueDate>'.date('Y-m-d',$invoice->f('invoice_date')).'</cbc:IssueDate>';
if($invoice->f('type')!=2 && $invoice->f('type')!=4){
$xml.='<cbc:InvoiceTypeCode listID="UNCL1001">'.$doc_type.'</cbc:InvoiceTypeCode>';
}
$xml.='<cbc:DocumentCurrencyCode listID="ISO4217">'.$currency.'</cbc:DocumentCurrencyCode><cac:OrderReference>
<cbc:ID>'.htmlspecialchars(stripslashes($invoice->f('your_ref')),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
</cac:OrderReference>';
if($invoice->f('type')==2 || $invoice->f('type')==4){
$xml.='<cac:BillingReference>
		<cac:InvoiceDocumentReference>
			<cbc:ID>'.$invoice->f('our_ref').'</cbc:ID>
		</cac:InvoiceDocumentReference>
	</cac:BillingReference>';	
}
$xml.='<cac:AdditionalDocumentReference>
    <cbc:ID>Invoice-'.htmlspecialchars(stripslashes($invoice->f('serial_number')),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
    <cbc:DocumentType>CommercialInvoice</cbc:DocumentType>
    <cac:Attachment>
      <cbc:EmbeddedDocumentBinaryObject mimeCode="application/pdf"
        >'.$str.'</cbc:EmbeddedDocumentBinaryObject>
    </cac:Attachment>
  </cac:AdditionalDocumentReference>
  <cac:AdditionalDocumentReference>
	<cbc:ID>eFFF</cbc:ID>
	<cbc:DocumentType>Akti</cbc:DocumentType>
  </cac:AdditionalDocumentReference>
   <cac:AccountingSupplierParty>
    <cac:Party>
      <cbc:EndpointID schemeID="'.$code_supplier.'">'.str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER).'</cbc:EndpointID>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars(stripslashes($ACCOUNT_COMPANY),ENT_QUOTES | ENT_HTML5).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.htmlspecialchars(stripslashes($ACCOUNT_BILLING_ADDRESS),ENT_QUOTES | ENT_HTML5).'</cbc:StreetName>
        <cbc:CityName>'.htmlspecialchars(stripslashes($ACCOUNT_BILLING_CITY),ENT_QUOTES | ENT_HTML5).'</cbc:CityName>
        <cbc:PostalZone>'.htmlspecialchars(stripslashes($ACCOUNT_BILLING_ZIP),ENT_QUOTES | ENT_HTML5).'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID schemeID="'.$code_supplier_simple.'">'.str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER).'</cbc:CompanyID>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:PartyTaxScheme>
      <cac:PartyLegalEntity>';
if($invoice->f('type')==2 || $invoice->f('type')==4){
$xml.='<cbc:RegistrationName>'.htmlspecialchars(stripslashes($ACCOUNT_COMPANY),ENT_QUOTES | ENT_HTML5).'</cbc:RegistrationName>';	
}
$xml.='<cbc:CompanyID>'.str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER).'</cbc:CompanyID>';
if($invoice->f('type')==2 || $invoice->f('type')==4){
$xml.='<cac:RegistrationAddress>
		<cbc:CityName>'.htmlspecialchars(stripslashes($ACCOUNT_BILLING_CITY),ENT_QUOTES | ENT_HTML5).'</cbc:CityName>
			<cac:Country>
				<cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($ACCOUNT_BILLING_COUNTRY_ID).'</cbc:IdentificationCode>
			</cac:Country>
	</cac:RegistrationAddress>';
}
$xml.='</cac:PartyLegalEntity>
    </cac:Party>
  </cac:AccountingSupplierParty>
  <cac:AccountingCustomerParty>
    <cbc:SupplierAssignedAccountID>'.htmlspecialchars(stripslashes($customer_reference),ENT_QUOTES | ENT_HTML5).'</cbc:SupplierAssignedAccountID>
    <cac:Party>
       <cbc:EndpointID schemeID="'.$code_customer.'">'.str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr')).'</cbc:EndpointID>
      <cac:PartyName>
        <cbc:Name>'.htmlspecialchars(stripslashes($invoice->f('buyer_name')),ENT_QUOTES | ENT_HTML5).'</cbc:Name>
      </cac:PartyName>
      <cac:PostalAddress>
        <cbc:StreetName>'.htmlspecialchars(stripslashes($invoice->f('buyer_address')),ENT_QUOTES | ENT_HTML5).'</cbc:StreetName>
        <cbc:CityName>'.htmlspecialchars(stripslashes($invoice->f('buyer_city')),ENT_QUOTES | ENT_HTML5).'</cbc:CityName>
        <cbc:PostalZone>'.htmlspecialchars(stripslashes($invoice->f('buyer_zip')),ENT_QUOTES | ENT_HTML5).'</cbc:PostalZone>
        <cac:Country>
          <cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($invoice->f('buyer_country_id')).'</cbc:IdentificationCode>
        </cac:Country>
      </cac:PostalAddress>
      <cac:PartyTaxScheme>
        <cbc:CompanyID schemeID="'.$code_customer_simple.'">'.str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr')).'</cbc:CompanyID>
        <cac:TaxScheme>
          <cbc:ID>VAT</cbc:ID>
        </cac:TaxScheme>
      </cac:PartyTaxScheme>
      <cac:PartyLegalEntity>';
if($invoice->f('type')==2 || $invoice->f('type')==4){
$xml.='<cbc:RegistrationName>'.htmlspecialchars(stripslashes($invoice->f('buyer_name')),ENT_QUOTES | ENT_HTML5).'</cbc:RegistrationName>';	
}
$xml.='<cbc:CompanyID>'.str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr')).'</cbc:CompanyID>';
if($invoice->f('type')==2 || $invoice->f('type')==4){
$xml.='<cac:RegistrationAddress>
		<cbc:CityName>'.htmlspecialchars(stripslashes($invoice->f('buyer_city')),ENT_QUOTES | ENT_HTML5).'</cbc:CityName>
			<cac:Country>
				<cbc:IdentificationCode listID="ISO3166-1:Alpha2">'.get_country_code($invoice->f('buyer_country_id')).'</cbc:IdentificationCode>
			</cac:Country>
		</cac:RegistrationAddress>';
}
$xml.='</cac:PartyLegalEntity>
      <cac:Contact>
        <cbc:Name>'.($contact->f('firstname') ? htmlspecialchars(stripslashes($contact->f('firstname')),ENT_QUOTES | ENT_HTML5).' '.htmlspecialchars(stripslashes($contact->f('lastname')),ENT_QUOTES | ENT_HTML5) : htmlspecialchars(stripslashes($contact->f('lastname')),ENT_QUOTES | ENT_HTML5)).'</cbc:Name>
        <cbc:Telephone>'.htmlspecialchars(stripslashes($contact->f('phone')),ENT_QUOTES | ENT_HTML5).'</cbc:Telephone>
        <cbc:Telefax></cbc:Telefax>
        <cbc:ElectronicMail>'.htmlspecialchars(stripslashes($contactUblEmail),ENT_QUOTES | ENT_HTML5).'</cbc:ElectronicMail>
      </cac:Contact>
    </cac:Party>
  </cac:AccountingCustomerParty>
  <cac:PaymentMeans>
    <cbc:PaymentMeansCode listID="UNCL4461">1</cbc:PaymentMeansCode>
    <cbc:PaymentDueDate>'.date('Y-m-d',$invoice->f('due_date')).'</cbc:PaymentDueDate>
    <cbc:PaymentID>'.str_replace('/', '', $invoice->f('ogm')).'</cbc:PaymentID>
    <cac:PayeeFinancialAccount>
        <cbc:ID schemeName="IBAN">'.htmlspecialchars(stripslashes($ACCOUNT_IBAN),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
        <cac:FinancialInstitutionBranch>
          <cac:FinancialInstitution>
            <cbc:ID schemeName="BIC">'.htmlspecialchars(stripslashes($ACCOUNT_BIC),ENT_QUOTES | ENT_HTML5).'</cbc:ID>
          </cac:FinancialInstitution>
        </cac:FinancialInstitutionBranch>
      </cac:PayeeFinancialAccount>
  </cac:PaymentMeans>
  <cac:PaymentTerms></cac:PaymentTerms>
  <cac:TaxTotal>
    <cbc:TaxAmount currencyID="'.$currency.'">'.twoDecNumber($vat_v).'</cbc:TaxAmount>
    '.$tax_str.'
  </cac:TaxTotal>
  <cac:LegalMonetaryTotal>
    <cbc:LineExtensionAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:LineExtensionAmount>
    <cbc:TaxExclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_no_vat).'</cbc:TaxExclusiveAmount>
    <cbc:TaxInclusiveAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:TaxInclusiveAmount>
    <cbc:PrepaidAmount currencyID="'.$currency.'">0.00</cbc:PrepaidAmount>
    <cbc:PayableAmount currencyID="'.$currency.'">'.twoDecNumber($total_vat).'</cbc:PayableAmount>
  </cac:LegalMonetaryTotal>
  '.$xml_lines;
if($invoice->f('type')==2 || $invoice->f('type')==4){
$xml.='</CreditNote>';
}else{
$xml.='</Invoice>';
}
if($in['include_xml']){
	$sxe = new SimpleXMLElement($xml);
	$sxe->asXML(__DIR__."/../../../eff_".DATABASE_NAME."_".$invoice->f('serial_number').".xml");
}else if($in['efff_xml']){

	//$sxe = new SimpleXMLElement($xml);
	//$sxe->asXML(__DIR__."/../../../eff_".str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER)."_".substr(str_replace(array('.',' ','-','_'), '', $ACCOUNT_COMPANY),0,10)."_".$invoice->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr'))."_".substr(str_replace(array('.',' ','-','_'), '', $invoice->f('buyer_name')),0,10).".xml");

	$doc = new DOMDocument('1.0');
	$doc->formatOutput = true;
	$doc->loadXML($xml);
	$doc->save(__DIR__."/../../../eff_".str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER)."_".substr(str_replace(array('.',' ','-','_'), '', $ACCOUNT_COMPANY),0,10)."_".$invoice->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr'))."_".substr(str_replace(array('.',' ','-','_'), '', $invoice->f('buyer_name')),0,10).".xml", LIBXML_NOEMPTYTAG);
}else if($in['billtobox_xml']){
	$doc = new DOMDocument('1.0');
	$doc->formatOutput = true;
	$doc->loadXML($xml);
	$doc->save(__DIR__."/../../../file_".$invoice->f('serial_number').".xml", LIBXML_NOEMPTYTAG);
}else{
	doQueryLog();
	header("Content-type: text/xml; charset=utf-8");
	header("Content-Disposition: attachment; filename=eff_".str_replace(array('.',' ','-','_'), '', $ACCOUNT_VAT_NUMBER)."_".substr(str_replace(array('.',' ','-','_'), '', $ACCOUNT_COMPANY),0,10)."_".$invoice->f('serial_number')."_".str_replace(array('.',' ','-','_'), '', $invoice->f('seller_bwt_nr'))."_".substr(str_replace(array('.',' ','-','_'), '', $invoice->f('buyer_name')),0,10).".xml");
	print_r($xml);
	exit();
}

?>