<?php

session_start();
$view = new at(ark::$viewpath.'post_print_2.html');

	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);

	$view->assign(array(
		'comp_name'		=> ACCOUNT_COMPANY,
		'comp_address'	=> ACCOUNT_BILLING_ADDRESS,
		'comp_zip'		=> ACCOUNT_BILLING_ZIP,
		'comp_city'		=> ACCOUNT_BILLING_CITY,
		'comp_country'	=> get_country_name(ACCOUNT_BILLING_COUNTRY_ID),
		'comp_vat'		=> ACCOUNT_VAT_NUMBER,
		'lang_fr'		=> $acc_lang=='fr' ? true : false
	));

	if($_SESSION['main_u_id']==0){
		//$user_data=$db_users->query("SELECT last_name,first_name,phone,email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_data=$db_users->query("SELECT last_name,first_name,phone,email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	}else{
		//$user_data=$db_users->query("SELECT last_name,first_name,phone,email FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
		$user_data=$db_users->query("SELECT last_name,first_name,phone,email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['main_u_id']]);
	}

	$view->assign(array(
		'usr_name'		=> $user_data->f('last_name'),
		'usr_surname'	=> $user_data->f('first_name'),
		'usr_phone'		=> $user_data->f('phone'),
		'usr_email'		=> $user_data->f('email'),
	));

return $view->fetch();
?>