<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');
$in['invoices_id'] ='';
if($_SESSION['add_to_pdf']){
  foreach ($_SESSION['add_to_pdf'] as $key => $value) {
    $in['invoices_id'] .= $key.',';
  }
}
if(isset($_SESSION['tmp_add_to_pdf'])){
    unset($_SESSION['tmp_add_to_pdf']);
}
if (isset($_SESSION['add_to_pdf'])) {
    unset($_SESSION['add_to_pdf']);
}
$in['invoices_id'] = rtrim($in['invoices_id'],',');
if(!$in['invoices_id']){
  $in['invoices_id']= '0';
}
/*require_once 'libraries/PHPExcel.php';
$filename ="export_invoices_list.xls";

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Invoices list")
							 ->setSubject("Invoices list")
							 ->setDescription("Export invoices list")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Invoices");*/
$db = new sqldb();

$headers = array("TYPE",
        "STATUS",                
        "INVOICE NUMBER",
        "DATE INVOICE",
        "DUE DATE",
        "SENT DATE",
        "COMPANY",
        "COMPANY VAT NUMBER",
        "COMPANY COUNTRY",
        "COMPANY CITY",
        "COMPANY ZIP",
        "COMPANY ADDRESS",
        "COMPANY PHONE",
        "COMPANY INVOICE EMAIL",
        "ACCOUNT MANAGER",
        "AMOUNT",
        "VAT AMOUNT",
        "AMOUNT+VAT",
        "AMOUNT TO PAY",
        "ORDERS",
        "STRUCTURED COMMUNICATION",
        "OUR REFERENCE",
        "YOUR REFERENCE",       
);
$filename ="export_invoices_list.csv";

$final_data=array();

$info = $db->query("SELECT serial_number as iii, id, buyer_name,acc_manager_name,due_date, currency_rate, free_field, buyer_id,contact_id,sent_date, sent, status, amount, amount_vat,invoice_date, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, type, trace_id, ogm, paid,not_paid,f_archived,tblinvoice.our_ref, tblinvoice.your_ref
                    FROM tblinvoice
                    WHERE id IN (".$in['invoices_id'].")
                    ORDER BY t_date DESC, iii DESC ");


/*$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1',"TYPE")
            ->setCellValue('B1',"STATUS")
            ->setCellValue('C1', "SERIAL NUMBER")
            ->setCellValue('D1',"DATE INVOICE")
            ->setCellValue('E1',"DUE DATE")
			      ->setCellValue('F1',"COMPANY")
            ->setCellValue('G1',"COMPANY VAT NUMBER")
            ->setCellValue('H1',"COMPANY ADDRESS")
            ->setCellValue('I1',"COMPANY PHONE")
            ->setCellValue('J1',"COMPANY INVOICE EMAIL")
			      ->setCellValue('K1',"ACCOUNT MANAGER")
            ->setCellValue('L1',"AMOUNT")
            ->setCellValue('M1',"VAT AMOUNT")
			      ->setCellValue('N1',"AMOUNT+VAT")
            ->setCellValue('O1',"AMOUNT TO PAY")
			      ->setCellValue('P1',"SENT DATE");

   $xlsRow=2;*/
while ($info->next()) {
    //date invoice
    $date_invoice = date(ACCOUNT_DATE_FORMAT, $info->f('invoice_date'));

    //due date
    $due_date = date(ACCOUNT_DATE_FORMAT, $info->f('due_date'));

    //sent_date
    if (!$info->f('sent_date') || !is_numeric($info->f('sent_date'))) {
        $sent_date = '';
    } else {
        $sent_date = date(ACCOUNT_DATE_FORMAT, $info->f('sent_date'));
    }
    //invoice_type
    $invoice_type = '';
    switch ($info->f('type')) {
        case '0':
            $invoice_type = gm('Regular invoice');
            break;
        case '1':
            $invoice_type = gm('Proforma invoice');
            break;
        case '2':
            $invoice_type = gm('Credit Invoice');
            break;
    }

    //status
    /*$invoice_status='';
    switch ($info->f('status')){
      case '0':
        if($info->f('sent') == '0'){
          $invoice_status = gm('Draft');
        }else{
          if($info->f('paid') == '2' ){
            $invoice_status = gm('Partially Paid');
          }else {
            $invoice_status = gm('Final');
          }
          if($info->f('due_date') < time()){
            $invoice_status = gm('Late');
          }
        }
        break;
      case '1':
        $info->f('type')=='2'? $invoice_status = gm('Final'):$invoice_status = gm('Fully Paid');
        break;
      default:
        $invoice_status = gm('Draft');
        break;
    }*/

    if ($info->f('sent') == '0') {
        $invoice_status = gm('Draft');
    } else if ($info->f('status') == '1' || ($info->f('status') == '0' && $info->f('paid') == '2')) {
        $invoice_status = $info->f('paid') == '2' ? gm('Partially Paid') : gm('Paid');
    } else if ($info->f('sent') != '0' && $info->f('not_paid') == '0') {
        $invoice_status = $info->f('status') == '0' && $info->f('due_date') < time() && $info->f('type') != '2' && $info->f('type') != '4' ? gm('Late') : gm('Final');
    } else if ($info->f('sent') != '0' && $info->f('not_paid') == '1') {
        $invoice_status = gm('No Payment');
    }

    if ($info->f('f_archived') == '1') {
        $invoice_status = gm('Archived');
    }

    // company vat number  , phone, invoice email

    $company_vat_nr = '';
    $phone = '';
    $invoice_email = '';
    $acc_manager_name = stripslashes($info->f('acc_manager_name'));
    if ($info->f('buyer_id')) {
        $company_data = $db->query("SELECT btw_nr,invoice_email,comp_phone FROM customers WHERE customer_id = '" . $info->f('buyer_id') . "' ");
        while ($company_data->next()) {
            $company_vat_nr = $company_data->f('btw_nr');
            $phone = $company_data->f('comp_phone');
            $invoice_email = $company_data->f('invoice_email');
        }
    }
    if (!$info->f('buyer_id') && $info->f('contact_id')) {
        $contact_data = $db->query("SELECT email, phone FROM customer_contacts WHERE contact_id = '" . $info->f('contact_id') . "' ");
        while ($contact_data->next()) {
            $phone = $contact_data->f('phone');
            $invoice_email = $contact_data->f('email');
        }
    }

////  //company address
//  if($info->f('free_field')){
//    $company_address = $info->f('free_field');
//  }
//  else{
    if ($info->f('buyer_id')) {
        $buyer_details = $db->query("SELECT * FROM customer_addresses WHERE customer_id = '" . $info->f('buyer_id') . "' AND billing = 1 ");
    }
    if ($info->f('contact_id') && !$info->f('buyer_id')) {
        $buyer_details = $db->query("SELECT * FROM customer_contact_address WHERE contact_id = '" . $info->f('contact_id') . "' ");
    }
    if ($buyer_details) {
        $company['country'] = get_country_name($buyer_details->f('country_id')) ? get_country_name($buyer_details->f('country_id')) : '' ;
        $company['city'] = $buyer_details->f('city') ? $buyer_details->f('city') : '';
        $company['zip'] = $buyer_details->f('zip') ? $buyer_details->f('zip') : '';
        $company['address'] = $buyer_details->f('address') ? $buyer_details->f('address') : '';
    }
//}


  $nr_of_decimals = 2;

  // vat amount
  $vat_amount = $info->f('amount_vat')-$info->f('amount');


  //amount to pay
  $already_payed = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE  invoice_id='".$info->f('id')."'");
  $already_payed->move_next();

  $total_payed = $already_payed->f('total_payed');

  if($info->f('currency_rate') && $info->f('currency_rate')!='1'){
    $total_payed = $already_payed->f('total_payed')*return_value($info->f('currency_rate'));
  }

  $amount_to_pay = $info->f('amount_vat') - $total_payed;


  $for_credit = '';
  if($info->f('type')=='2' && $info->f('sent')!=1){
    $for_credit = '-';
  }elseif($info->f('type')=='2' && $info->f('sent')==1){
    $for_credit = '';
    $amount_to_pay = '0.00';
  }

  if($info->f('paid')==1){
    $for_credit = '';
    $amount_to_pay = '0.00';
  }

  $order_s_nr="";
  $orders = $db->query("SELECT pim_orders.serial_number FROM tracking
              INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
              INNER JOIN pim_orders ON tracking_line.origin_id = pim_orders.order_id
              WHERE tracking.trace_id = '".$info->f('trace_id')."' AND tracking_line.origin_type ='4' ")->getAll();

  $orders_more=$db->query("SELECT pim_orders.serial_number FROM tracking
              INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id AND tracking_line.origin_type ='7'
              INNER JOIN tracking AS tracking_proforma ON tracking_proforma.target_id=tracking_line.origin_id AND tracking_proforma.target_type=tracking_line.origin_type
              INNER JOIN tracking_line AS tracking_line_order ON tracking_proforma.trace_id=tracking_line_order.trace_id AND tracking_line_order.origin_type ='4'
              INNER JOIN pim_orders ON tracking_line_order.origin_id = pim_orders.order_id
              WHERE tracking.trace_id = '".$info->f('trace_id')."'  ")->getAll();

  if(!empty($orders_more)){
    $orders=array_merge($orders,$orders_more);
  }

  foreach($orders as $order){
    $order_s_nr.=$order['serial_number']." ";
  }
  $order_s_nr=rtrim($order_s_nr);
  /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $invoice_type)
              ->setCellValue('B'.$xlsRow, $invoice_status)
  	          ->setCellValue('C'.$xlsRow, $info->f('iii'))
              ->setCellValue('D'.$xlsRow, $date_invoice)
              ->setCellValue('E'.$xlsRow, $due_date)
  			      ->setCellValue('F'.$xlsRow, $info->f('buyer_name'))
              ->setCellValue('G'.$xlsRow, $company_vat_nr)
              ->setCellValue('H'.$xlsRow, preg_replace("/[\n\r]/","",$company_address))
              ->setCellValue('I'.$xlsRow, $phone)
              ->setCellValue('J'.$xlsRow, $invoice_email)
              ->setCellValue('K'.$xlsRow, $acc_manager_name)
              ->setCellValue('L'.$xlsRow, number_format($info->f('amount'), $nr_of_decimals, '.', ''))
              ->setCellValue('M'.$xlsRow, number_format($vat_amount, $nr_of_decimals, '.', ''))
              ->setCellValue('N'.$xlsRow, number_format($info->f('amount_vat'), $nr_of_decimals, '.', ''))
              ->setCellValue('O'.$xlsRow, $for_credit.number_format($amount_to_pay, $nr_of_decimals, '.', ''))
              ->setCellValue('P'.$xlsRow, $sent_date);
  $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
  $xlsRow++;*/

  $item=array(
    $invoice_type,
    $invoice_status,
    $info->f('iii'),
    $date_invoice,
    $due_date,
    $sent_date,
    $info->f('buyer_name'),
    $company_vat_nr,
    $company['country'],
    $company['city'],
    $company['zip'],
    $company['address'],
    $phone,
    $invoice_email,
    $acc_manager_name,
      display_number_exclude_thousand($info->f('amount')),
      display_number_exclude_thousand($vat_amount),
      display_number_exclude_thousand($info->f('amount_vat')),
    $for_credit.display_number_exclude_thousand($amount_to_pay),
    $order_s_nr,
    $info->f('ogm')?'+++'.$info->f('ogm').'+++' : '',
    preg_replace("/[\n\r]/","",$info->f('our_ref')),
    preg_replace("/[\n\r]/","",$info->f('your_ref'))
  );

  array_push($final_data,$item);
}

header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$fp = fopen("php://output", 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
exit();


// set column width to auto
foreach(range('A','W') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.$filename);





define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>