<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$o=array();
$info = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."' ");
    
if(!$in['payment_date']){
	$in['payment_date'] = time()*1000;

}else {
	$in['payment_date'] = is_numeric($in['payment_date']) ? $in['payment_date']*1000 : strtotime($in['payment_date'])*1000;
}
$total=$info->f('total_with_vat');

$already_payed = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_incomming_payments WHERE invoice_id='".$in['invoice_id']."'  ");
$total_payed = $already_payed->f('total_payed');

$amount_due = round($total - $total_payed,2);

	$o['payment_method']			='0'; 
    //$o['payment_method_dd']  		= payment_method_dd($in['payment_method']) ;
	$o['payment_method_dd']  		= payment_method_dd_pi($in['payment_method']) ;
    $o['payment_date']  			= $in['payment_date'];
	$o['total_amount_due1']			= display_number($amount_due);
	$o['cost']						= display_number(0);
	$o['invoice_id']				= $in['invoice_id'];
	$o['from_list']            		= $in['from_list'];
	//$o['show_QRCode_button'] 		= $db->field("SELECT value FROM settings WHERE `constant_name`='QRCODE_PURCHASE_INVOICE'")? true : false;
	$o['show_QRCode_button'] 		= true;
	$o['show_QRCode'] 				= false;
	$o['disabled_QRCode_button'] 	= $amount_due? false:true;

	$o['iban'] 		= $info->f('buyer_iban');
	$o['bname'] 	= $db->field("SELECT name FROM customers WHERE customer_id='".$info->f('supplier_id')."' ");
	$o['bic']		= $info->f('buyer_bic');
	$o['ogm']		= $info->f('ogm');

json_out($o);
?>
