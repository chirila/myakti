<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array();
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);

$i=0;
$result['reminders']=array();
$reminder = $db->query("SELECT * FROM reminders_logs WHERE company_id ='".$in['customer_id']."' ");

while($reminder->next()){
	$invoices_list=array();
	//$db_user->query("SELECT first_name, last_name FROM users WHERE user_id = '".$_SESSION['u_id']."'");
	$db_user->query("SELECT first_name, last_name FROM users WHERE user_id = :user_id",['user_id'=>$_SESSION['u_id']]);
	$inv_txt = '';
	if($reminder->f('invoices')){
		$inv = $db->query("SELECT serial_number,id FROM tblinvoice WHERE id IN (".$reminder->f('invoices').") ");		
		while ($inv->next()) {
			if($inv->f('serial_number')){
				$invoices=array(
					'invoice_id'		=> $inv->f('id'),
					'serial_number'		=> $inv->f('serial_number')
				);
				/*$inv_txt .="<a href=\"index.php?do=invoice-invoice&invoice_id=".$inv->f('id')."\" class=\"normal_link\" >".$inv->f('serial_number')."</a>, ";*/
				array_push($invoices_list, $invoices);
			}
		}
		/*$inv_txt = rtrim($inv_txt,', ');*/
	}
	$reminder_line=array(
			//'first_name' 	=> utf8_encode($db_user->f('first_name')),
			//'last_name' 	=> utf8_encode($db_user->f('last_name')),
			//'name'		=> get_user_name($_SESSION['u_id']),
			'name'		=> get_user_name($reminder->f('account_manager_id')),
			'sent_date'		=> date(ACCOUNT_DATE_FORMAT,$reminder->f('sent_date')),
			'email'		=> $reminder->f('date_man_updated')==1? gm('Manually') : $reminder->f('email'),
			'invoices'		=> $invoices_list,
		);
	$i++;
	array_push($result['reminders'], $reminder_line);
}
	$customer_name=$db->field("SELECT name FROM customers WHERE customer_id='".$in['customer_id']."' ");
	$result['title'] 			= gm('Reminders sent to')." ".$customer_name ;

json_out($result);