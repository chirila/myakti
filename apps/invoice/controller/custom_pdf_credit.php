<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_codelabels($in,$showin=true,$exit=true){
		$db = new sqldb();
		
		$content='';
		if(!$in['identity_id']){
			$in['identity_id']=0;
		}
		if($in['header']=='header'){
			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}
		if($in['footer']=='footer'){

			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}
		if($in['body']=='body'){
			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['body']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice_credit' AND type='".$in['body']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
			$body_l=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_CREDIT_INVOICE_BODY_POS'");
			if(is_null($body_l)){
				$data['body_id']='1';
			}else{
				$data['body_id']=$body_l;
			}
		}
		$data['header']=$in['header'];
		$data['footer']=$in['footer'];
		$data['body']=$in['body'];
		$data['variable_data'] = $content;
		$data['layout']=$in['layout'];
		$data['identity_id']=$in['identity_id'];
			
		return json_out($data, $showin,$exit);
	}

	function get_selectlabels($in,$showin=true,$exit=true){
		$db = new sqldb();

		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		// $quote_columns=$db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ORDER BY COLUMN_NAME ");
		$quote=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ");

		$columns=array();
		$make_out=array();
		if(!$in['make_id']){
			$in['make_id']='0';
		}

		$lang_code = $_SESSION['l'];
		$filter_lang= " lang_code ='".$lang_code."' ";
		if($lang_code=='nl' || $lang_code='du'){
			$filter_lang= " lang_code ='nl' OR lang_code ='du' ";
		}

		$labels = $db->query("SELECT * FROM label_language WHERE $filter_lang ")->getAll();		
		$labels = array_map("strtolower",$labels[0]);
		asort($labels);

		foreach ($labels as $key => $value) {
			if(!strpos($key, '_webl') && $value && $key!='label_language_id'){
				array_push($make_out, array(
	            	'id_value'=>$key,
	            	'name'=>ucfirst($value),
	        	));
			}
		}

		/*while($quote_columns->next()){
			$temp_description=$db->field("SELECT `{$quote_columns->f('COLUMN_NAME')}` FROM label_language WHERE lang_code='".$_SESSION['l']."' ");
			if(!$temp_description){
				continue;
			}
			array_push($make_out, array(
            	'id_value'=>$quote_columns->f('COLUMN_NAME'),
            	'name'=>ucfirst($temp_description),
        	));
		}*/

		$data['make_id']=$in['make_id'];
		$data['make']=$make_out;

		asort($columns);
		

		$final_select='';
		foreach($columns as $key=>$value){
			array_push($data['labels'], array(
				'code'		=> $key,
				'value'		=> $value,
			));
		}
		
		$quote_colum=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active')  AND COLUMN_NAME='".$in['make_id']."' ");

		if($data['make_id']!='0'){
			$data['selected_make']='[label]'.$quote_colum.'[/label]';
		}
		return json_out($data, $showin,$exit);
	}
	function get_selectdetails($in,$showin=true,$exit=true){
		$db = new sqldb();
		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		$array_values=array();
		$array_values['delivery_address']=gm("Address delivery");
		$array_values['attention_of']=gm("Attention of");
		$array_values['buyer_address']=gm("Buyer address");
		$array_values['buyer_city']=gm("Buyer city");
		$array_values['buyer_country']=gm("Buyer country");
		$array_values['buyer_name']=gm("Buyer name");
		$array_values['buyer_reference']=gm("Buyer reference");
		$array_values['invoice_buyer_vat']=gm("Buyer vat number");
		$array_values['buyer_zip']=gm("Buyer zip");
		$array_values['seller_address']=gm("Company address");
		$array_values['seller_city']=gm("Company city");
		$array_values['seller_country']=gm("Company country");
		$array_values['seller_email']=gm("Company email");
		$array_values['seller_fax']=gm("Company fax");
		$array_values['ACCOUNT_LOGO']=gm("Company Logo");
		$array_values['ACCOUNT_LOGO_STYLE']=gm("Company Logo - Variable size");
		$array_values['seller_name']=gm("Company name");
		$array_values['seller_phone']=gm("Company phone");
		$array_values['seller_url']=gm("Company url");
		$array_values['seller_vat']=gm("Company vat");
		$array_values['seller_zip']=gm("Company zip");
		$array_values['invoice_contact_name']=gm("Contact name");
		$array_values['invoice_date']=gm("Invoice date");
		$array_values['free_text_content']=gm("General conditions");
		$array_values['notes']=gm("Notes");
		$array_values['serial_number']=gm("Serial number");
		$array_values['own_reference']=gm("Your own reference");
		$array_values['customer_reference']=gm("Customer reference");
		$array_values['invoice_due_date']=gm("Invoice due date");
		$array_values['seller_reg_number']=gm("Company Registration Number");
		$array_values['buyer_reg_number']=gm("Buyer Registration Number");
		$array_values['buyer_contact_mobile']=gm("Buyer contact mobile");
		$array_values['buyer_contact_email']=gm("Buyer contact email");
		$array_values['buyer_company_email']=gm("Buyer company email");
		$array_values['buyer_company_phone']=gm("Buyer company phone");
		$array_values['buyer_contact_first_name']=gm("Buyer contact first name");
		$array_values['buyer_contact_last_name']=gm("Buyer contact last name");
		$array_values['buyer_contact_salutation']=gm("Buyer contact salutation");
		$array_values['account_manager']=gm('Account Manager');
		$array_values['buyer_firstname']=gm('First Name');
		$array_values['subject']=gm('Subject');
		
		asort($array_values);

		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}

		$data['detail_id']=$in['detail_id'];
		$data['detail']=$detail;
		if($data['detail_id']!='0'){
			$data['selected_detail']='[!'.$in['detail_id'].'!]';
		}
		return json_out($data, $showin,$exit);
	}

	$result = array(
		'codelabels'		=> get_codelabels($in,true,false),
		'selectlabels'		=> get_selectlabels($in,true,false),
		'selectdetails'		=> get_selectdetails($in,true,false),
	);

json_out($result);
?>