<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $cfg;
$db = new sqldb();

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$special_template = array(4,5,8); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// $pdf->SetFont('helvetica', '', 10, '', false);
// if($custom_lng){
 #we can use both font types but dejavusans looks better
 $pdf->SetFont('dejavusans', '', 9, '', false);
 // $pdf->SetFont('freeserif', '', 10, '', false);
// }

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Mandate');
$pdf->SetSubject('Mandate');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();

$html = include('invoice_sepa_body.php');
$pdf->writeHTML($html, true, false, true, false, '');

if($in['print']){
 echo($html);
 exit();
}


$pdf->lastPage();
//if(!$in['serial_number'] && $in['mandate_id']) {
    $serial_number=$in['mandate_id'];
//}
if($in['do']=='invoice-invoice_sepa'){
	doQueryLog();
   $pdf->Output($serial_number.'.pdf','I');
}else if($in['base64']){
 $str = $pdf->Output($serial_number.'.pdf','E');
}else{
	doQueryLog();
 $pdf->Output(__DIR__.'/../../../admin/invoice.pdf', 'F');
}
?>