<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['dbase']){
	global $database_config;
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $in['dbase'],
	);
	$db = new sqldb($database_2);
}else{
	$db = new sqldb();
}

ark::loadCronLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$special_template = array(4,5); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetFont('helvetica', '', 10, '', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->AddPage();

$invoice = $db->query("SELECT * FROM tblinvoice_incomming WHERE invoice_id='".$in['invoice_id']."'");
if($invoice->f('supplier_id')){
	$bName= $db->query("SELECT * FROM customers WHERE customer_id='".$invoice->f('supplier_id')."'");
	$address = $db->query("SELECT * FROM customer_addresses WHERE customer_id='".$invoice->f('supplier_id')."' AND billing='1' ");
}else {
	$bName= $db->query("SELECT CONCAT_WS(' ',firstname, lastname) AS name FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."'");
	$address = $db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$invoice->f('contact_id')."' AND is_primary='1' ");
}

$lines = $db->query("SELECT * FROM tblinvoice_incomming_line WHERE invoice_id='".$in['invoice_id']."' ");
while ($lines->next()) {
	if($invoice->f('allow_disconto') == 1){
		$line = $lines->f('total')-$lines->f('total')*$invoice->f('disconto')/100;
		$discontoo += $lines->f('total')*$invoice->f('disconto')/100;
	}else{
		$line = $lines->f('total');
	}
	$vat_arr[$lines->f('vat_line')] += $lines->f('vat_line')*$line/100;
	$sub_arr[$lines->f('vat_line')] += $line;
}
$sadasasda = '';
foreach ($vat_arr as $key => $val) {
	$sadasasda .= 'VAT %:'.$key."<br>VAT:".$val."<br>Total excl. VAT:".$sub_arr[$key]."<br>";
}
if($invoice->f('allow_disconto') == 1 && $discontoo){
	$sadasasda .= 'DISCONTO: '.$discontoo;
}

$countr = get_country_name($address->f('country_id'));
$date1 = date('Y-m-d',$invoice->f('invoice_date'));
$date2 = date('Y-m-d',$invoice->f('due_date'));

$html = <<<EOD
{$bName->f('name')}<br>
{$address->f('address')}<br>
{$address->f('city')} {$address->f('zip')}<br>
{$countr}<br>
{$invoice->f('booking_number')}<br>
{$invoice->f('invoice_number')}<br>
{$date1}<br>
{$date2}<br>
{$sadasasda}<br>
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$str = $pdf->Output($serial_number.'.pdf','E');

?>