<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$o=array();
$db2 = new sqldb();
$db3 = new sqldb();
$recurring_invoice = $db->query("SELECT *
										FROM recurring_invoice
										WHERE recurring_invoice_id='".$in['recurring_invoice_id']."'");

if(!$recurring_invoice->next()){
	//return ark::run('invoice-recurring_invoices');
}
if(!$recurring_invoice->f('recurring_invoice_id')){
	msg::error('Invoice does not exist','error');
	$in['item_exists']= false;
    json_out($in);
}

$o=array();
$o['item_exists'] = true;
$o['start_date'] = date(ACCOUNT_DATE_FORMAT,  $recurring_invoice->f('start_date'));
$o['end_date'] = $recurring_invoice->f('end_date') ? date(ACCOUNT_DATE_FORMAT,$recurring_invoice->f('end_date')) : '';
switch($recurring_invoice->f('frequency')){
	case '1':
		$o['frequency']=gm('Weekly');
		break;
	case '2':
		$o['frequency']=gm('Monthly');
		break;
	case '3':
		$o['frequency']=gm('Quarterly');
		break;
	case '4':
		$o['frequency']=gm('Yearly');
		break;
	case '5':
		$o['frequency']=gm('Every').' '.$recurring_invoice->f('days');
		break;
	default:
	$frequency='';
}
$o['acc_manager_name']=get_user_name($recurring_invoice->f('acc_manager_id'));
$o['payment_method_id']=$recurring_invoice->f('payment_method');
switch($recurring_invoice->f('payment_method')){
	case '1':
		$o['payment_method']=gm('Direct Transfer');
		break;
	case '2':
		$o['payment_method']=gm('Stripe');
		break;
	case '3':
		$o['payment_method']=gm('SEPA');
		break;
	default:
		$o['payment_method']='';
}
if($recurring_invoice->f('f_archived') == '1'){
	$type_title = gm('Archived');
	$nr_status=1;
}else{
	$type_title = gm('Active');
	$nr_status=2;
}
$o['type_title']=$type_title;
$o['nr_status']=$nr_status;
$o['sepa_mandate']='';
if($recurring_invoice->f('payment_method')=='3' && $recurring_invoice->f('mandate_id')){
	$o['sepa_mandate']=$db->field("SELECT sepa_number FROM mandate WHERE mandate_id='".$recurring_invoice->f('mandate_id')."' ");
}

$o['buyer_id']=$recurring_invoice->f('buyer_id');
$o['contact_id']=$recurring_invoice->f('contact_id');
$o['invoice_buyer_name']       		= stripslashes($recurring_invoice->f('buyer_name'));
$o['invoice_buyer_vat']		 	  	= $recurring_invoice->f('seller_bwt_nr');
$o['invoice_contact_name']       	= get_contact_name($recurring_invoice->f('contact_id'));
$o['customer_notes']=stripslashes($db->field("SELECT customer_notes FROM customers WHERE customer_id='".$recurring_invoice->f('buyer_id')."' "));

$buyer_details = $db->query("SELECT * FROM customer_addresses
                WHERE customer_id = '".$recurring_invoice->f('buyer_id')."' AND billing ='1' ");
if(!$buyer_details->next()){
    $buyer_details = $db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$recurring_invoice->f('buyer_id')."' AND is_primary='1' ");
}

$o['free_field'] = $recurring_invoice->f('free_field') ? nl2br($recurring_invoice->f('free_field')) : nl2br($buyer_details->f('address').'
'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
'.get_country_name($buyer_details->f('country_id')));

$o['subject']=stripslashes($recurring_invoice->f('title'));
$o['your_ref']=stripslashes($recurring_invoice->f('your_ref'));
$o['our_ref']=stripslashes($recurring_invoice->f('our_ref'));
$o['delivery_method']=$recurring_invoice->f('invoice_delivery') == '1' ? gm('Send invoice via email') : gm('Save invoice as draft');
$o['invoice_delivery']=$recurring_invoice->f('invoice_delivery');
$o['send_to']='';

$recipients_data=$db->query("SELECT email FROM recurring_email_contact WHERE recurring_invoice_id='".$recurring_invoice->f('recurring_invoice_id')."' ");
while($recipients_data->next()){
	$o['send_to'].=$recipients_data->f('email').';';
}
$o['send_to']=rtrim($o['send_to'],";");
$o['is_active']							= $recurring_invoice->f('f_archived')!='1'? true:false;
$o['is_archived']						= $recurring_invoice->f('f_archived')=='1'? true:false;
$currency_type = $recurring_invoice->f('currency_type');
$currency_rate = $recurring_invoice->f('currency_rate');
$o['hide_vattd']= $recurring_invoice->f('remove_vat') == 1 ? false : true;
$o['hide_discount_line']= !$recurring_invoice->f('apply_discount') || $recurring_invoice->f('apply_discount') == 2 ? false : true;
$o['apply_discount']=$recurring_invoice->f('apply_discount');
$o['no_vat'] = $recurring_invoice->f('remove_vat') == 1 ? false : true;
$o['hide_currency1']= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
$o['hide_currency2']= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
$o['default_currency_val']	= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
$o['default_currency_name']= build_currency_name_list(ACCOUNT_CURRENCY_TYPE);
$o['discount']  = display_number($recurring_invoice->f('discount'));
$o['notes2']=stripslashes(utf8_decode($recurring_invoice->f('notes2')));
if(ADV_QUOTE==0 && ADV_PRODUCT==0 && ADV_CRM==0 && ADV_STOCK==0){
	$hide_invoice=false;
}else{
	$hide_invoice=true;
}
$o['hide_invoice']    						= $hide_invoice;

$notes = $db->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'recurring_invoice'
								   AND item_name = 'notes'
								   And active = '1'
								   AND lang_id = '".$recurring_invoice->f('email_language')."'
								   AND item_id = '".$in['recurring_invoice_id']."' ");
$o['down_notes']= stripslashes($notes);

if($recurring_invoice->f('discount') == 0){
	$discount=0;
} else {
	$discount = $recurring_invoice->f('discount');
}
if($recurring_invoice->f('apply_discount') < 2){
	$discount = 0;
}

$rectblinvoice_line=$db->query("SELECT *
		FROM recurring_invoice_line
		WHERE recurring_invoice_id='".$recurring_invoice->f('recurring_invoice_id')."' ORDER BY sort_order ASC");

$i=0;
$line_total=0;
$subtotal=0;
$vat_value = 0;
$vat_percent = array();
$sub_t = array();
$vat_percent_val = 0;
$discount_value = 0;
$netto_amount=0;
$o['invoice_line']=array();
while ($rectblinvoice_line->move_next()){
	$row_id = 'tmp'.$i;
	$invoice_line=array(
		'tr_id'         			=> $row_id,
		'description'  			=> nl2br($rectblinvoice_line->f('name')),
		'quantity'      			=> display_number($rectblinvoice_line->f('quantity')),
		'price'         			=> display_number_var_dec($rectblinvoice_line->f('price')),
		'price_vat'				=> display_number($rectblinvoice_line->f('price')+($rectblinvoice_line->f('price')*$rectblinvoice_line->f('vat')/100)),
		'vat'					=> display_number($rectblinvoice_line->f('vat')),
		'hide_currency2'			=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
		'hide_currency1'			=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
		'discount_line'			=> display_number($rectblinvoice_line->f('discount')),
		'article_id'  			=> $rectblinvoice_line->f('article_id'),
		'article_code'  			=> $rectblinvoice_line->f('item_code'),
		'show_for_articles'		=> ($rectblinvoice_line->f('article_id') || $rectblinvoice_line->f('tax_id') ) ? true : false,
		'hide_vattd'			=> $recurring_invoice->gf('remove_vat') == 1 ? false : true,
		'content'				=> $rectblinvoice_line->f('content') ? true : false,
		'tax_id'  			=> $rectblinvoice_line->f('tax_id'),
		'tax_for_article_id'  => $rectblinvoice_line->f('tax_for_article_id')
	);

	$price =  $rectblinvoice_line->f('price');
	$line_discount = $rectblinvoice_line->f('discount');
	if($recurring_invoice->f('apply_discount') == 0 || $recurring_invoice->f('apply_discount') == 2){
		$line_discount = 0;
	}
	$line_total2 = $rectblinvoice_line->f('quantity') * ( $price - ( $price * $line_discount / 100 ) );
	$invoice_line['line_total']   = display_number($line_total2);
	array_push($o['invoice_line'], $invoice_line);

	$amount_d = $line_total2 * $discount/100;

	$subtotal +=$line_total2;

	$vat_percent_val = ( $line_total2 - $amount_d ) * $rectblinvoice_line->f('vat')/100;
	$vat_percent[$rectblinvoice_line->f('vat')] += round($vat_percent_val,2);
	$sub_t[$rectblinvoice_line->f('vat')] += round($line_total2,2);
	$sub_discount[$rectblinvoice_line->f('vat')] += $amount_d;
	$i++;
	$discount_value	+= $amount_d;
	$netto_amount+=(round($line_total2,2)-$amount_d);
}
$o['hide_total'] = true;
$o['vat_line']=array();
$t=0;
foreach ($vat_percent as $key => $val){
	$total_vat += $val;
	if($sub_t[$key] - $sub_discount[$key]){
		$vat_line=array(
			'vat_percent'   			=> display_number($key),
			'vat_value'	   			=> display_number($val),
			'subtotal'				=> display_number($sub_t[$key]),
			'total_novat'	   	 	=> display_number($sub_t[$key]),
			'discount_value'			=> display_number($sub_discount[$key]),
			'view_discount'			=> $recurring_invoice->f('apply_discount') < 2 ? false : true,
			'net_amount'			=> display_number($sub_t[$key] - $sub_discount[$key]),
		);
		array_push($o['vat_line'],$vat_line);
	}	
	$t++;
}
if($t==1){
	$o['hide_total'] = false;
}
$total=($subtotal - $discount_value) +$total_vat;
$total_default = $total*return_value($o['currency_rate']) ;

$o['discount_value']   			= $discount_value ? display_number($discount_value):display_number(0);
$o['total']            			= $total ? display_number($total):display_number(0);
$o['total_default_currency']		= $total_default ? display_number($total_default):display_number(0);
$o['net_amount']			      = display_number($netto_amount);
$o['total_wo_vat']			      = display_number($subtotal);

if($discount){
	$o['view_discount1']	= true;
}else{
	$o['view_discount1']	= false;
}
$o['view_vat']=$vat? true : false;

$o['total_currency_hide']			= $currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? false : true) : false;
$o['currency_val']		   	= get_commission_type_list($currency_type);
$o['yuki_project_name']		= get_yuki_project_name($recurring_invoice->f('yuki_project_id'));

json_out($o);

?>