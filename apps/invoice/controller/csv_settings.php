<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

$result=array('vats'=>array(),'types'=>array());

$regime_type=array(
      '1'   => gm("Regular"),
      '2'   => gm('Intra-EU'),
      '3'   => gm("Export"),
    );
$vats = $db->query("SELECT vat_new.*  FROM vat_new
          ORDER BY vat_new.name_id ASC");

while($vats->next()){
  $selected=$db->field("SELECT field_value FROM invoice_export_settings WHERE field_name='vat_regime_id' AND field_id='".$vats->f('id')."' ");
  if(!$selected){
    $selected=$db->field("SELECT field_value FROM invoice_export_settings WHERE field_name='vat_regime_id' AND field_id='".$vats->f('vat_regime_id')."' ");
  }
  if(!$selected){
    $selected='';
  }
  if($vats->f('vat_id')){
    $value=$db->field("SELECT value FROM vats WHERE vat_id='".$vats->f('vat_id')."' ");
  }else if($vats->f('vat_regime_id')){
    $value=0;
  }else{
    $value=0;
  }
  $item=array(
    'VAT_VALUE'     => $vats->f('description'),
    'value'         => $value,
    'regime_type'   => $regime_type[$vats->f('regime_type')],
    'id'            => $vats->f('id'),
    'selected'      => $selected,
  );
  array_push($result['vats'], $item);
}

  $types=$db->query("SELECT * FROM invoice_export_settings WHERE field_name='type'");
  while($types->next()){
    $item=array(
        'name'    => ($types->f('field_id')=='1' ? gm('Proforma invoice') : ($types->f('field_id')=='2' ? gm('Credit Invoice'): gm('Regular Invoice'))),
        'type'    => $types->f('field_id'),
        'selected'   =>  $types->f('field_value'),
      );
    array_push($result['types'], $item);
  }

json_out($result);