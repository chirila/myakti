<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array('query'=>array());

if($in['to_invoice'] == 'deliveries'){
	$in['to_invoice']='Orders';
}
if($in['to_invoice'] == 'projects'){
	$in['to_invoice']='Projects';
}
if($in['to_invoice'] == 'interventions'){
	$in['to_invoice']='Interventions';
}

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

$ALLOW_ARTICLE_PACKING = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
$ALLOW_ARTICLE_SALE_UNIT = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

$data = array();
$data_project = array();
$is_data = false;
$order_date = 0;
$order_total = 0;
$order_downpayment = 0;
$order_nr =0;
$project_date = 0;
$project_total = 0;
$project_nr =0;
$p_hours;
switch ($in['to_invoice']) {
	/*case 'Quotes':
		$quote_query = $db->query("SELECT tblquote.buyer_name, tblquote.discount, tblquote.currency_rate, tblquote.id, tblquote.buyer_id, tblquote.contact_id, tblquote.serial_number, tblquote.quote_date,
							tblquote_version.* ,tblinvoice.quote_id
						   FROM tblquote
						   INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
						   LEFT JOIN tblinvoice ON tblquote.id=tblinvoice.quote_id
						   WHERE status_customer='2' AND tblquote.f_archived='0' AND tblquote_version.active='1' AND tblinvoice.quote_id IS NULL
 						   GROUP BY tblquote.id ");
		while ($quote_query->next()) {
			$quote_line = $db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$quote_query->f('id')."' AND version_id='".$quote_query->f('version_id')."' ");
			if($quote_query->f('discount')){
				$quote_line = $quote_line - ($quote_line*$quote_query->f('discount')/100);
			}
			if($quote_query->f('currency_rate')){
				$quote_line = $quote_line*return_value($quote_query->f('currency_rate'));
			}
			// $data[($quote_query->f('buyer_id') ? $quote_query->f('buyer_id') : $quote_query->f('contact_id'))]['name'] = $quote_query->f('buyer_name');
			// $data[($quote_query->f('buyer_id') ? $quote_query->f('buyer_id') : $quote_query->f('contact_id'))][$quote_query->f('id')]=array('serial'	=> $quote_query->f('serial_number'),
			$data[($quote_query->f('buyer_id') .'-'. $quote_query->f('contact_id'))]['name'] = $quote_query->f('buyer_name');
			$data[($quote_query->f('buyer_id') .'-'. $quote_query->f('contact_id'))][$quote_query->f('id')]=array('serial'	=> $quote_query->f('serial_number'),
																		 	 'amount'	=> $quote_line,
																		 	 'date'		=> $quote_query->f('quote_date'),
																		 	 'id'		=> $quote_query->f('id'),
																		 	 'link'		=> 'index.php?do=quote-quote&quote_id='.$quote_query->f('id'));
			if(count($data) > 0){
				$is_data = true;
			}
		}
		break;*/
	case 'Orders':
		$search_orders="";
		if($in['search'] && !empty($in['search'])){
		    $search_orders.=" AND (pim_orders.serial_number LIKE '%".$in['search']."%' OR pim_orders.customer_name LIKE '%".$in['search']."%' )";
		}

		$order_incomplete_ar=[];
		$delivery_ar=[];
		
		if(ORDER_DELIVERY_STEPS==2){

			$order_incomplete = $db->query("SELECT COUNT(pim_orders_delivery.order_delivery_id) AS order_incomplete,pim_orders.order_id,COUNT(pim_order_deliveries.order_id) as pim_order_deliveries_count
			FROM pim_orders
			LEFT JOIN pim_orders_delivery ON pim_orders.order_id=pim_orders_delivery.order_id AND pim_orders_delivery.invoiced=1
			LEFT JOIN pim_orders_delivery as p_delivery ON pim_orders.order_id = p_delivery.order_id AND p_delivery.invoiced = '0'
			LEFT JOIN pim_order_deliveries ON p_delivery.delivery_id=pim_order_deliveries.delivery_id AND pim_order_deliveries.delivery_done = '1' 		
			WHERE pim_orders.rdy_invoice>'0' AND pim_orders.invoiced='0'
			AND pim_orders.active='1' ".$search_orders." GROUP BY pim_orders.order_id HAVING pim_order_deliveries_count > 0")->getAll();
			
			foreach($order_incomplete as $order_data){
				$order_incomplete_ar[$order_data['order_id']]=$order_data['order_incomplete'];
			}

			$order_incomplete=null;

			$delivery = $db->query("SELECT pim_orders_delivery.*,COUNT(pim_order_deliveries.order_id) as pim_order_deliveries_count
				FROM pim_orders
				LEFT JOIN pim_orders_delivery as p_delivery ON pim_orders.order_id = p_delivery.order_id AND p_delivery.invoiced = '0'
				LEFT JOIN pim_order_deliveries ON p_delivery.delivery_id=pim_order_deliveries.delivery_id AND pim_order_deliveries.delivery_done = '1' 		
				WHERE pim_orders.rdy_invoice>'0' AND pim_orders.invoiced='0'
				AND pim_orders.active='1' ".$search_orders." HAVING pim_order_deliveries_count > 0")->getAll();

			foreach($delivery as $p_o_delivery){
				if(array_key_exists($p_o_delivery['order_id'], $delivery_ar) === false){
					$delivery_ar[$p_o_delivery['order_id']]=[];
				}
				$delivery_ar[$p_o_delivery['order_id']][]=$p_o_delivery;
			}

			$delivery=null;

			//AND 	invoiced='0'
			$order_query = $db->query("SELECT * FROM pim_orders
								WHERE rdy_invoice>'0' AND invoiced='0'
								AND active='1'
								AND (
									SELECT COUNT(pim_order_deliveries.order_id) FROM pim_order_deliveries
									LEFT JOIN pim_orders_delivery
									ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
									WHERE pim_order_deliveries.order_id=pim_orders.order_id
									AND delivery_done = '1'
									AND pim_orders_delivery.invoiced = '0'
									) > 0  ".$search_orders." ");
		}else{
			$order_incomplete=$db->query("SELECT COUNT(pim_orders_delivery.order_delivery_id) AS order_incomplete,pim_orders.order_id
			FROM pim_orders 
			LEFT JOIN pim_orders_delivery ON pim_orders.order_id=pim_orders_delivery.order_id AND pim_orders_delivery.invoiced=1
			WHERE pim_orders.rdy_invoice>'0' AND pim_orders.invoiced='0' AND pim_orders.active='1' ".$search_orders." GROUP BY pim_orders.order_id")->getAll();

			foreach($order_incomplete as $order_data){
				$order_incomplete_ar[$order_data['order_id']]=$order_data['order_incomplete'];
			}

			$order_incomplete=null;

			$delivery=$db->query("SELECT pim_orders_delivery.*
				FROM pim_orders 
				LEFT JOIN pim_orders_delivery ON pim_orders.order_id=pim_orders_delivery.order_id AND pim_orders_delivery.invoiced=0
				WHERE pim_orders.rdy_invoice>'0' AND pim_orders.invoiced='0' AND pim_orders.active='1' ".$search_orders." ")->getAll();

			foreach($delivery as $p_o_delivery){
				if(array_key_exists($p_o_delivery['order_id'], $delivery_ar) === false){
					$delivery_ar[$p_o_delivery['order_id']]=[];
				}
				$delivery_ar[$p_o_delivery['order_id']][]=$p_o_delivery;
			}

			$delivery=null;

			$order_query = $db->query("SELECT * FROM pim_orders WHERE pim_orders.rdy_invoice>'0' AND pim_orders.invoiced='0' AND pim_orders.active='1' ".$search_orders." ");
		}

		while ($order_query->next()) {
			$amount = 0;
			$total_order_incomplete = false;
			//$invoiced_deliveries = $db->query("SELECT * FROM pim_orders_delivery WHERE order_id='".$order_query->f('order_id')."' AND invoiced='1' ");
			//if(!$invoiced_deliveries->next()){
				//$total_order_incomplete = true;
			//}
			if(array_key_exists($order_query->f('order_id'), $order_incomplete_ar) && $order_incomplete_ar[$order_query->f('order_id')] == 0){
				$total_order_incomplete = true;
			}

			//we will take now from delivery only
			$total_order_incomplete = false;

			/*$delivery = $db->query("SELECT * FROM pim_orders_delivery WHERE order_id='".$order_query->f('order_id')."' AND invoiced='0' ");
			if(ORDER_DELIVERY_STEPS == 2){
				$delivery = $db->query("SELECT * FROM pim_orders_delivery
								LEFT JOIN pim_order_deliveries
								ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
								WHERE pim_orders_delivery.order_id='".$order_query->f('order_id')."'
								AND invoiced='0'
								AND delivery_done = '1' ");
			}*/
			//while ($delivery->next()) {
			if(array_key_exists($order_query->f('order_id'), $delivery_ar) && !empty($delivery_ar[$order_query->f('order_id')])){

				foreach($delivery_ar[$order_query->f('order_id')] as $del){
					$line = $db->query("SELECT * FROM pim_order_articles WHERE order_id='".$order_query->f('order_id')."' AND order_articles_id='".$del['order_articles_id']."' AND content='0'   ");
					$line_price = ($line->f('price')-($line->f('price')*$line->f('discount')/100));
					
					// $line_price = $line_price+($line_price*$line->f('vat_percent')/100);
					//we have to get the return quantity
					 $return=$db->field("SELECT COALESCE(SUM(pim_articles_return.quantity),0) FROM  pim_articles_return WHERE order_id='".$order_query->f('order_id')."' AND article_id='".$line->f('article_id')."'");

					if($order_query->f('rdy_invoice') == 1 && $total_order_incomplete == true ){
						$amount += ($line->f('quantity')-$return)*($line->f('packing')/$line->f('sale_unit'))*$line_price;
					}else{
						
						$amount += ($del['quantity']-$return)*($line->f('packing')/$line->f('sale_unit'))*$line_price;
					
					}
				}
				
			}
               /*if($amount<0){
               	    continue;
               }*/
			if($amount !=0 ){
				if($order_query->f('currency_rate')){
					$amount = $amount*return_value($order_query->f('currency_rate'));
				}
				if($order_query->f('discount')!=0.00){
		            $amount = $amount-(($amount*$order_query->f('discount')) /100);
		        }

		     
				//$data[($order_query->f('customer_id') .'-'. $order_query->f('contact_id'))]['name'] = $order_query->f('customer_name');
				//$data[($order_query->f('customer_id') .'-'. $order_query->f('contact_id'))][$order_query->f('order_id')]=array('serial'	=> $order_query->f('serial_number'),
				//															 	 		 'amount'	=> $amount,
				//															 	 		 'date'		=> $order_query->f('date'),
				//															 	 		 'id'		=> $order_query->f('order_id'),
				//		                                                                 'link'		=> 'index.php?do=order-order&order_id='.$order_query->f('order_id'));
				$order_downpayment = $db->query("SELECT * FROM  tbldownpayments 
											WHERE order_id = '".$order_query->f('order_id')."' ");
				$data[($order_query->f('customer_id') )]['name'] = $order_query->f('customer_name');
				$data[($order_query->f('customer_id') )][$order_query->f('order_id')]=array('serial'	=> $order_query->f('serial_number'),
																			 	 		 'amount'	=> $amount,
																			 	 		 'date'		=> $order_query->f('date'),
																			 	 		 'id'		=> $order_query->f('order_id'),
																			 	 		 'downpayment' => $order_downpayment->f('value_fixed'),
																			 	 		 'link'		=> 'index.php?do=order-order&order_id='.$order_query->f('order_id'));


				if(!$order_date && $order_query->f('date')){
					$order_date = $order_query->f('date');
				}elseif ($order_query->f('date') < $order_date && $order_query->f('date')) {
					$order_date = $order_query->f('date');
				}
				

				$order_total += $amount;


				$order_nr ++;
			}
			if(count($data) > 0){
				$is_data = true;
			}
		}

		$result['number_o']		= $order_nr;
		$result['amount_o']		= place_currency(display_number($order_total));
		$result['date_o']			= $order_date ? date(ACCOUNT_DATE_FORMAT,$order_date) : '-';

		break;
	case 'Projects':
		$project_query = $db->query("SELECT * FROM projects WHERE billable_type!='4' AND projects.active>'0' AND projects.stage>'0' ");
		// $project_query = $db->query("SELECT * FROM projects WHERE billable_type!='4' ");
		$amount_from_hours=0;
		$amount_expenses=0;
		$project_total = 0;
		while ($project_query->next()) {
			// console::log($project_query);
			$project_task = array();
			$project_purchase = array();
			$project_expenses = array();
			$project_articles = array();
			$increment = false;
			$is_retainer = false;
			$begin_time = array();
			$end_time = array();
			$project_amount = 0;
			// console::log($project_query->f('project_id'));
			if($project_query->f('billable_type') && $project_query->f('invoice_method')){

				if($project_query->f('billable_type') != '5'){
					$projects_hours = $db->field("SELECT SUM(hours) FROM task_time
								LEFT JOIN projects ON task_time.project_id = projects.project_id
								LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
								LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
								WHERE task_time.billable='1' AND task_time.billed='0' AND task_time.approved='1' AND task_time.service = '0'  AND task_time.project_id='".$project_query->f('project_id')."' AND projects.invoice_method>0 AND task_time.project_status_rate='0' AND ( ( (billable_type = '1' or billable_type = '7') AND t_h_rate !='0' ) OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_h_rate!='0' ) )  ");
					$projects_days = $db->field("SELECT SUM(hours) FROM task_time
						LEFT JOIN projects ON task_time.project_id = projects.project_id
						LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
						LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
						WHERE task_time.billable='1' AND task_time.billed='0' AND task_time.approved='1' AND task_time.service = '0'  AND task_time.project_id='".$project_query->f('project_id')."' AND task_time.project_status_rate='1' AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_daily_rate !='0' ) OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_daily_rate!='0' ) )  ");
					if($projects_hours || $projects_days){	//console::log($project_query->f('project_id'));
						$increment = true;
						if($projects_hours){
							$p_hours += $projects_hours;
						}
						if($projects_days){
							$p_days+=$projects_days;
						}
						$filter_hours = " task_time.billable='1' and billable_type != '4' AND billable_type !=  '5' AND task_time.billed='0' AND service = '0' AND projects.active>'0' AND approved='1' AND task_time.project_id='".$project_query->f('project_id')."' ";

						$amount = billableAmount($filter_hours);
						$amount_from_hours+=$amount['total'];
						$begin_time[$project_query->f('project_id')] = $db->field("SELECT date FROM task_time WHERE billable='1' AND billed='0' AND approved='1'  AND project_id='".$project_query->f('project_id')."' ORDER BY date ASC LIMIT 1 ");
						$end_time[$project_query->f('project_id')] = $db->field("SELECT date FROM task_time WHERE billable='1' AND billed='0' AND approved='1'  AND project_id='".$project_query->f('project_id')."' ORDER BY date DESC LIMIT 1 ");
					}
				}else{
					$tasks = $db->query("SELECT * FROM tasks WHERE project_id='".$project_query->f('project_id')."' AND billable='1' AND closed='1' ");
					while($tasks->next()){
						$increment = true;
						$project_amount += $tasks->f('task_budget');
						$project_task[$tasks->f('task_id')]=$tasks->f('task_name');
					}
				}
				if($project_query->f('billable_type') == '6'){
					#do nothing
					$increment = false;
					$projects_hours = false;
					$is_retainer = true;
				}
				$join = " LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id ";
			$filter = '';
			$expence_field = ', project_expences.expense_id AS expense_id_2 ';
			if($project_query->f('active') == 2){
				$join = '';
				$filter = " AND project_expenses.billable='1' ";
				$expence_field = '';
			}
			$expenses = $db->query("SELECT project_expenses.* , expense.unit_price AS u_p,expense.name, task_time.approved ".$expence_field."
															FROM project_expenses
															LEFT JOIN expense ON project_expenses.expense_id = expense.expense_id
															LEFT JOIN task_time ON project_expenses.project_id = task_time.project_id AND project_expenses.date = task_time.date
															".$join."
															WHERE project_expenses.project_id='".$project_query->f('project_id')."' AND project_expenses.billed !=  '1' AND is_service='0' AND approved = '1' ".$filter);
				// echo "SELECT project_expenses . * , expense.unit_price AS u_p,expense.name ".$select."
				// 												FROM project_expenses
				// 												INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
				// 												".$join."
				// 												WHERE project_expenses.project_id='".$project_query->f('project_id')."' AND project_expenses.billed!='1' ".$filter.'<br>';
				while ($expenses->next()) {
					$increment = true;
					$projects_hours = true;
					if( ( $expenses->f('approved') && $expenses->f('expense_id_2') ) || ( !$expenses->f('expense_id_2') && $expenses->f('billable')==1 && $expenses->f('billed') == 0 ) ){
						if(!$expenses->f('u_p')){
							$unit_price = 1;
						}else{
							$unit_price = $expenses->f('u_p');
						}
						$amount_expenses+= $unit_price*$expenses->f('amount');
					}
					if($begin_time[$project_query->f('project_id')] && $begin_time[$project_query->f('project_id')]>$expenses->f('date')){
						$begin_time[$project_query->f('project_id')] = $expenses->f('date');
					}
					if($end_time[$project_query->f('project_id')] && $end_time[$project_query->f('project_id')]<$expenses->f('date')){
						$end_time[$project_query->f('project_id')] = $expenses->f('date');
					}
				}
				// console::log($project_query->f('projec t_id'),$increment);

				$purchas = $db->query("SELECT * FROM project_purchase WHERE project_id='".$project_query->f('project_id')."' AND delivered='1' AND billable='1' AND invoiced='0' ");
				while($purchas->next()){
					$increment = true;
					$margin_price = ($purchas->f('unit_price')*$purchas->f('margin')/100) + $purchas->f('unit_price');
					$project_amount += $margin_price;
					$project_purchase[$purchas->f('project_purchase_id')]=$purchas->f('description');
				}
				$articles = $db->query("SELECT SUM(service_delivery.quantity) AS total_quantity,service_delivery.a_id,project_articles.price,project_articles.name,pim_articles.packing,pim_articles.sale_unit FROM service_delivery
					INNER JOIN project_articles ON service_delivery.project_id=project_articles.project_id AND service_delivery.a_id=project_articles.article_id
					LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
					 WHERE service_delivery.project_id='".$project_query->f('project_id')."' AND service_delivery.invoiced='0' GROUP BY service_delivery.a_id ");
				while($articles->next()){
					$increment = true;
					$packing_art=($ALLOW_ARTICLE_PACKING && $articles->f('packing')>0) ? $articles->f('packing') : 1;
					$sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $articles->f('sale_unit')>0) ? $articles->f('sale_unit') : 1;
					$article_price = $articles->f('total_quantity')*($packing_art/$sale_unit_art)*$articles->f('price');
					$project_amount += $article_price;
					$project_articles[$articles->f('a_id')]=$articles->f('name');
				}
				if($increment){
					$begin_time = $begin_time[$project_query->f('project_id')];
					$end_time = $end_time[$project_query->f('project_id')];

					$data[$project_query->f('customer_id')]['name'] = $project_query->f('company_name');
					$data[$project_query->f('customer_id')][$project_query->f('project_id')]=
					array('serial'		=> $project_query->f('serial_number'),
						 'amount'		=> $project_query->f('name'),
						'date'			=> $project_query->f('start_date'),
						'id'				=> $project_query->f('project_id'),
						'link'			=> 'index.php?do=project-project&project_id='.$project_query->f('project_id').'&begin_time='.$begin_time.'&end_time='.$end_time,
						'hours'		=> $projects_hours || $projects_days ? '1' : '0',
						'bill'			=> $project_query->f('billable_type') == 5 ? 'task' : 'hours',
						'task'			=> $project_task,
						'retainer'	=> $is_retainer,
						'purchase'	=> $project_purchase,
						'expense'	=> $project_expenses,
						'article'	=> $project_articles,
						'begin_time' 	=> $begin_time,
						 'end_time' 		=> $end_time);
					$project_nr ++;
					if(!$project_date && $project_query->f('start_date')){
						$project_date = $project_query->f('start_date');
					}elseif ($project_query->f('start_date') < $project_date && $project_query->f('start_date')) {
						$project_date = $project_query->f('start_date');
					}

				}
				// console::log($project_query->f('project_id'),$increment,$project_query->f('billable_type') , $project_query->f('invoice_method'));
			}
			// console::log($data);
			if($project_query->f('currency_rate')){
				$project_amount = $project_amount*return_value($project_query->f('currency_rate'));
			}
			$project_total += $project_amount;
		}
		$project_total+=$amount_from_hours+$amount_expenses;

		$result['number_p']		= $project_nr;
		$result['amount_p']		= $p_hours > 0 ? number_as_hour($p_hours)." ".gm('hours') : number_as_hour(0)." ".gm('hours');
		$result['amount_days']		= $p_days > 0 ? $p_days." ".gm('Days') : "0 ".gm('Days');
		$result['total_amount_p']	= place_currency(display_number($project_total));
		$result['date_p']			= $project_date ? date(ACCOUNT_DATE_FORMAT,$project_date) : '-';

		if(count($data) > 0){
			$is_data = true;
		}
		break;
	case 'Interventions':
		$is_data = false;
		$increment = false;
		$total = 0;
		$serv_nr = 0;
		$serv_date = 0;
		$search_interventions="";
		if($in['search'] && !empty($in['search'])){
		    $search_interventions.=" AND (servicing_support.serial_number LIKE '%".$in['search']."%' OR servicing_support.customer_name LIKE '%".$in['search']."%' )";
		}
		$query = $db->query("SELECT * FROM servicing_support WHERE status='2' AND active='1' AND billed='0' ".$search_interventions." ");
		while ($query->next()) {
			$tdisc=0;
			$ldisc=0;
			$srv_amount=0;
			/*if($query->f('customer_id')){
				$cust = $db->query("SELECT apply_fix_disc,apply_line_disc,line_discount,fixed_discount
									FROM customers WHERE customer_id='".$query->f('customer_id')."' ");
				if($cust->f('apply_fix_disc')==1){
					$tdisc=$cust->f('fixed_discount');
				}
				if($cust->f('apply_line_disc')==1){
					$ldisc=$cust->f('line_discount');
				}
			}*/
			$total_intervention=0;
			if($query->f('billable')){
				if(!$query->f('service_type')){
					$hours = $db->field("SELECT SUM(end_time-start_time-break) FROM servicing_support_sheet WHERE service_id='".$query->f('service_id')."' ");
					if($hours){
						$hoursPerUser = $db->field("SELECT SUM(end_time-start_time-break) as hour FROM servicing_support_sheet WHERE service_id='".$query->f('service_id')."' ");
						if ($hoursPerUser) {
							$srv_amount += $hoursPerUser*$query->f('rate')-($hoursPerUser*$query->f('rate')*$ldisc/100);
							// $total += $hoursPerUser*$query->f('rate');
							// $total_intervention += $hoursPerUser*$query->f('rate');
						}
						$increment = true;
					}
				}else{
					$int_tasks=$db->field("SELECT SUM(task_budget) FROM servicing_support_tasks WHERE service_id='".$query->f('service_id')."' AND article_id='0' ");
					$srv_amount+=$int_tasks;
					$increment = true;
				}
				$int_services=$db->query("SELECT quantity,task_budget FROM servicing_support_tasks WHERE service_id='".$query->f('service_id')."' AND article_id!='0' AND `closed`='1' ");
				while($int_services->next()){
					$increment = true;
					$srv_amount += ($int_services->f('quantity')*$int_services->f('task_budget'));
				}			
			}
					
			$purchase = $db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$query->f('service_id')."' AND delivered<>0 AND billable='1' ");
			while ($purchase->next()) {
				$increment = true;
				$srv_amount += (($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'))*$purchase->f('delivered'))-(($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'))*$purchase->f('delivered')*$ldisc/100);
				// $total += ($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'))*$purchase->f('delivered');
				// $total_intervention += ($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'))*$purchase->f('delivered');
			}
			$intv_article = $db->query("SELECT SUM(service_delivery.quantity) AS total_quantity,servicing_support_articles.price, pim_articles.packing, pim_articles.sale_unit FROM service_delivery
				LEFT JOIN pim_articles ON service_delivery.a_id=pim_articles.article_id
				INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
				WHERE service_delivery.service_id='".$query->f('service_id')."' AND service_delivery.invoiced='0' AND servicing_support_articles.billable='1' AND servicing_support_articles.article_id!='0' GROUP BY service_delivery.a_id");
			while ($intv_article->next()) {
				$increment = true;
				$packing_art=($ALLOW_ARTICLE_PACKING && $intv_article->f('packing')>0) ? $intv_article->f('packing') : 1;
				$sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $intv_article->f('sale_unit')>0) ? $intv_article->f('sale_unit') : 1;
				$article_init=$intv_article->f('total_quantity')*($packing_art/$sale_unit_art)*$intv_article->f('price');
				$srv_amount += ($article_init-($article_init*$ldisc/100));
			}

			$expense = $db->query("SELECT project_expenses.*, expense.name AS e_name, expense.unit_price, expense.unit, servicing_support.customer_id as customer_id
									FROM project_expenses
									INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
									INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
									WHERE servicing_support.service_id='".$query->f('service_id')."' AND project_expenses.billable='1' ORDER BY id");
			while ($expense->next()) {
				$amount = $expense->f('amount');
				if($expense->f('unit_price')){
					$amount = $expense->f('amount') * $expense->f('unit_price');
				}
				$srv_amount += $amount;
				// $total += $amount;
				$total_intervention += $amount;
				$increment = true;
			}
			$downpayment_int=$db->query("SELECT tblinvoice_line.* FROM tbldownpayments 	
				INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
				INNER JOIN tblinvoice_line ON tblinvoice.id=tblinvoice_line.invoice_id
				WHERE tbldownpayments.service_id='".$query->f('service_id')."' AND tblinvoice.f_archived='0' ");
			while($downpayment_int->next()){
				$srv_amount -= ($downpayment_int->f('quantity')*$downpayment_int->f('price'));
				$increment = true;
			}
			$srv_amount = ($srv_amount - $srv_amount*$tdisc/100);
			$total += $srv_amount;
			if($increment===true){
				$is_data = true;
				$data[$query->f('customer_id') .'-'. $query->f('contact_id')]['name'] = $query->f('customer_name') ? $query->f('customer_name') : $query->f('contact_name');
				$data[$query->f('customer_id') .'-'. $query->f('contact_id')][$query->f('service_id')] = array(
					'serial' => $query->f('serial_number'),
					'amount'	=> $srv_amount,
				 	'date'		=> $query->f('planeddate'),
				 	'id'		=> $query->f('service_id'),
				 	'link'		=> 'index.php?do=maintenance-services&service_id='.$query->f('service_id')
				);
				$serv_nr++;
				if(!$serv_date && $query->f('planeddate')){
					$serv_date = $query->f('planeddate');
				}elseif ($query->f('planeddate') < $serv_date && $query->f('planeddate')) {
					$serv_date = $query->f('planeddate');
				}
			}

			$result['number_s']		= $serv_nr;
			$result['amount_s']		= place_currency(display_number($total));
			$result['date_s']			= $serv_date ? date(ACCOUNT_DATE_FORMAT,$serv_date) : '-';

		}

		break;
	default:
		$is_data = false;
		break;
}

// console::log('3333',$data);
if($is_data){
	foreach ($data as $key => $value) {
		$item=array(
			'customer_name'	=> $value['name'],
			'class'		=> $in['to_invoice'],
			'id'			=> $key,
			'quote'		=> array()
		);
		foreach ($value as $k => $v) {
			if(is_array($v)){
				$amount = place_currency(display_number($v['amount']));
				if(array_key_exists('task', $v)){
					$amount = $v['amount'];
				}
				$quote_array=array(
					'serial'		=> $v['serial'],
					'amount'		=> $amount,
					'date'		=> $v['date'] ? date(ACCOUNT_DATE_FORMAT,$v['date']) : '',
					'view_link'		=> $v['link'],
					'class'		=> $in['to_invoice'],
					'id_line'		=> $v['id'],
					'type'		=> strtolower($in['to_invoice']),
					'is_project'	=> $in['to_invoice'] == 'Projects' ? false : true,
					'project'		=> array(),
					'amount_downpayment' =>place_currency(display_number($v['downpayment']))
				);			
				if($v['bill'] == 'hours' && !$v['retainer']){
					// console::log($v);
					$project_arr=array(
						'what'		=> gm('Project hours').'/'.gm('Project days'),
						'val'			=> 'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&',
						'default_val'	=> 'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&',
						'is_hour'		=> true,
						'custom_hour'	=> '0',
						'type2'		=> 'hours',
					);
					if($v['hours']>0){
						array_push($quote_array['project'], $project_arr);
					}
				}else{
					if(count($v['task']) > 0){
						foreach ($v['task'] as $keys => $val) {
							$project_arr=array(
								'what'	=> gm('Project task').': '.$val,
								'val'		=> 'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&task='.$keys,
								'default_val'=>'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&task='.$keys,
								'is_hour'	=> false,
								'type2'	=> 'task',
							);
							array_push($quote_array['project'], $project_arr);
						}
					}
				}
				if(count($v['purchase']) > 0){
					foreach ($v['purchase'] as $keys => $val) {
						$project_arr=array(
							'what'	=> gm('Purchase').': '.$val,
							'val'		=> 'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&purchase='.$keys,
							'default_val'=>'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&purchase='.$keys,
							'is_hour'	=> false,
							'type2'	=> 'purchase',
						);
						array_push($quote_array['project'], $project_arr);
					}
				}
				if(count($v['article']) > 0){
					foreach ($v['article'] as $keys => $val) {
						$project_arr=array(
							'what'		=> gm('Article').': '.$val,
							'val'			=> 'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&article='.$keys,
							'default_val'	=> 'project_id='.$v['id'].'&begin_time='.$v['begin_time'].'&end_time='.$v['end_time'].'&article='.$keys,
							'is_hour'		=> false,
							'type2'		=> 'article',
						);
						array_push($quote_array['project'], $project_arr);
					}
				}			
				array_push($item['quote'],$quote_array);
			}
		}
		array_push($result['query'], $item);
	}
}

	$result['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
	$result['show_projects']		= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both')) ? true : false;
	$result['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
	$result['is_cozie']	=DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422' ? true : false;

// console::memory();

return json_out($result);
?>