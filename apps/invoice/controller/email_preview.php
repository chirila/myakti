<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
    global $config;

    if(!$in['invoice_id'])
    {
        return "";
    }
    $tblinvoice = $db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."'");
    $contact = $db->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'");
    $title_cont = $db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
    $customer = $db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."'");

    if(!$tblinvoice->move_next()){
        return "";
    }
    $in['message'] = stripslashes($in['message']);

    global $database_config,$config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $e_lang = $tblinvoice->f('email_language');
    if(!$e_lang || $e_lang > 4){
        $e_lang=1;
    }
    $text_array = array('1' => array('simple' => array('1' => 'INVOICE', '2'=> 'You can download your invoice HERE','3'=>"HERE",'4'=>'Web Link'),
                       'pay' => array('1' => 'INVOICE', '2'=> 'Check it by clicking on the link above','3'=>"HERE",'4'=>'Web Link')
                       ),
              '2' => array('simple' => array('1' => 'FACTURE', '2'=> 'Vous pouvez tÃ©lÃ©charger votre facture ICI','3'=>'ICI','4'=>'Lien web'),
                       'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Cliquez sur le lienweb pour tÃ©lÃ©charger votre facture.','3'=>'ICI','4'=>'Lien web')
                       ),
              '3' => array('simple' => array('1' => 'FACTUUR', '2'=> 'U kan uw factuur HIER downloaden','3'=>'HIER','4'=>'Weblink'),
                       'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'Klik op de link hierboven om de factuur te bekijken.','3'=>'HIER','4'=>'Weblink')
                       ),
              '4' => array('simple' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
                       'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie können Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER','4'=>'WEB-LINK')
                       )
    ); 


    $in['message']=str_replace('[!CONTACT_FIRST_NAME!]',"".$contact->f('firstname')."",$in['message']);
    $in['message']=str_replace('[!CONTACT_LAST_NAME!]',"".$contact->f('lastname')."",$in['message']);
    $in['message']=str_replace('[!SALUTATION!]',"".$title_cont."",$in['message']);
    $in['message']=str_replace('[!YOUR_REFERENCE!]',"".$tblinvoice->f('your_ref')."",$in['message']);
    $in['message']=str_replace('[!OWN_REFERENCE!]',"".$tblinvoice->f('our_ref')."",$in['message']);
    $in['message']=str_replace('[!OGM!]',"".$tblinvoice->f('ogm')."",$in['message']);
    $db_users = new sqldb($database_users);
    $in['user_html']=1;
if($in['use_html']){
    if(defined('USE_INVOICE_WEB_LINK') && USE_INVOICE_WEB_LINK == 1){
        //$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['invoice_id']."' AND `type`='i'  ");
        $exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type  ",['d'=>DATABASE_NAME,'item_id'=>$in['invoice_id'],'type'=>'i']);

        /*$extra = "<p style=\"background: #dedede; width: 500px; text-align:center; padding-bottom: 15px;\"><br />";
        $extra .="<b>".$text_array[$e_lang]['simple']['1']."</b><br />";
        if(defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT== 1){
            $extra .= str_replace($text_array[$e_lang]['pay']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['pay']['3']."</a>", $text_array[$e_lang]['pay']['2'])."<br /></p>";
        }else{
            $extra .= str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2'])."<br /></p>";
        }*/

        $extra = "<p style=\"background: #e6e6e6; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #e6e6e6; color:#868d91; border-radius:8px;\">";

          $extra .="<b style=\"color: #5199b7; text-align:center; font-size:32px;\"><a style=\"text-decoration:none; text-transform: lowercase; color:#6399c6;\" href=\"".$config['web_link_url']."?q=".$exist_url."\">".utf8_decode($text_array[$e_lang]['simple']['1'])."</a></b><br />";


          if(defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT== 1){
              //$extra .=htmlspecialchars(utf8_decode($text_array[$e_lang]['pay']['2']))."<br /></p>";
              $extra .=utf8_decode($text_array[$e_lang]['pay']['2'])."<br /></p>";
          }else{
              //$extra .=htmlspecialchars(utf8_decode($text_array[$e_lang]['simple']['2']))."<br /></p>";
              $extra .=utf8_decode($text_array[$e_lang]['simple']['2'])."<br /></p>";
          }

        // $in['message'] .=$extra;
        if($tblinvoice->f('identity_id')){
            $identity_logo = $db->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$tblinvoice->f('identity_id')."'");
            if($identity_logo){
                $in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].$identity_logo."\" alt=\"\">",$in['message']);
            }else{
                $in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$in['message']);
            }
        }else{
            $in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$in['message']);
        }   
        $in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".utf8_decode($text_array[$e_lang]['simple']['4'])."</a>", $in['message']);
        $in['message']=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$exist_url, $in['message']);
    }else{
        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
        $in['message']=str_replace('[!WEB_LINK_URL!]', '', $in['message']);
    }
}else{
    if(defined('USE_INVOICE_WEB_LINK') && USE_INVOICE_WEB_LINK == 1){
        //$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['invoice_id']."' AND `type`='i' ");
        $exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type ",['d'=>DATABASE_NAME,'item_id'=>$in['invoice_id'],'type'=>'i']);

        $extra = "\n\n-----------------------------------";
        $extra .="\n".$text_array[$e_lang]['simple']['1'];
        $extra .="\n-----------------------------------";
        if($tblinvoice->f('identity_id')){
            $identity_logo = $db->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$tblinvoice->f('identity_id')."'");
            if($identity_logo){
                $in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].$identity_logo."\" alt=\"\">",$in['message']);
            }else{
                $in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$in['message']);
            }
        }else{
            $in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$in['message']);
        }
        
        if(defined('ALLOW_WEB_PAYMENT') && ALLOW_WEB_PAYMENT== 1){
            $extra .="\n". str_replace($text_array[$e_lang]['pay']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['pay']['3']."</a>", $text_array[$e_lang]['pay']['2']);
        }else{
            $extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
        }
        // $in['message'] .=$extra;
        $in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $in['message']);
        $in['message']=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$exist_url, $in['message']);
    }else{
        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
        $in['message']=str_replace('[!WEB_LINK_URL!]', '', $in['message']);
    }
}

    $result=array(
        'e_message'            => $in['message'],
    );

    json_out($result);
?>