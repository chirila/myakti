<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class IncommingInvoiceEdit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);	

		$this->spliter=substr(ACCOUNT_DATE_FORMAT, 1, 1);	
	}

	public function getIncommingInvoice(){
		



		if($this->in['incomming_invoice_id'] == 'tmp'){ //add
			$this->getAddIncommingInvoice();
		}else{ # edit
			$this->getEditIncommingInvoice();
		}
	}
	
	private function getAddIncommingInvoice(){

		$in=$this->in;
        if(!$in['invoice_date']){
			$in['invoice_date']=time();
			
		}else{
			$in['invoice_date']=strtotime($in['invoice_date']);
		
		}

		if(!$in['due_date']){
			$in['due_date']= strtotime('+1 month', time());
			
		}else{
			$in['due_date']=strtotime($in['due_date']);
		
		}


        $do_next='invoice-incomming_ninvoice-invoice-updateCustomerDataInc';
        $details = array('table' 			=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
							 'field'		=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
							 'value'		=> $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],						
							 'filter'		=> $in['buyer_id'] ? ' AND billing =1' : '',
							 );

			$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
			$buyer_details->next();

			$customer_id = $in['buyer_id'];

				if($in['buyer_id']){
				$buyer_info = $this->db->query("SELECT customers.payment_term, customers.fixed_discount, customers.btw_nr, customers.c_email,
						customer_legal_type.name as l_name,
						customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc,customers.name AS company_name
						FROM customers
						LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
						WHERE customers.customer_id = '".$in['buyer_id']."' ");
				$buyer_info->next();
				$due_date = $in['quote_ts'] + ( $buyer_info->f('payment_term') * (60*60*24) ) ;
				$due = date(ACCOUNT_DATE_FORMAT,$in['quote_ts'] + ( $buyer_info->f('payment_term') * (60*60*24) ) );
				$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');
				$text = gm('Company Name').':';
				$c_email = $buyer_info->f('c_email');
				$c_fax = $buyer_details->f('comp_fax');
				$c_phone = $buyer_details->f('comp_phone');
				$buyer_vat_nr=$buyer_info->f('btw_nr');

				$in['discount_line_gen']= display_number($buyer_info->f('line_discount'));

				if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
					$apply_discount = '3';
				}else{
					if($buyer_info->f('apply_fix_disc')){
						$apply_discount = '2';
					}
					if($buyer_info->f('apply_line_disc')){
						$apply_discount = '1';
					}
				}
				$in['apply_discount'] = $apply_discount;
				$do_next='invoice-incomming_ninvoice-invoice-add_incomming';
			}else{
				$buyer_info = $this->db->query("SELECT phone, cell, email ,CONCAT_WS(' ',firstname,lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
				$buyer_info->next();
				$c_email = $buyer_info->f('email');
				$c_fax = '';
				$c_phone = $buyer_info->f('phone');
				$name = $buyer_info->f('name');
				$text = gm('Name').':';
				$buyer_vat_nr='';
				if($in['contact_id']){
					$do_next='invoice-incomming_ninvoice-invoice-add_incomming';
				}
			}

			$free_field = $buyer_details->f('address').'
					'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
					'.get_country_name($buyer_details->f('country_id'));

					$name = stripslashes($name);

        $output=array(
                'invoice_date'              	=> is_numeric($in['invoice_date']) ? $in['invoice_date']*1000 : strtotime($in['invoice_date'])*1000,
                'due_date'              	=> is_numeric($in['due_date']) ? $in['due_date']*1000 : strtotime($in['due_date'])*1000,
                'booking_number'        => $in['type']==2 ? generate_binvoice_credit_number(): generate_binvoice_number(),				
                'invoice_number'        => $in['invoice_number'],
				//Buyer Details
				'add_customer'			=> !$in['buyer_id'] && !$in['contact_id'] ? true : false,
				'buyer_id'                  	=> $in['buyer_id'],
				'contact_id'                	=> $in['contact_id'],
				'buyer_name'               	=> $name,
				'buyer_country_id'          	=> $buyer_details->f('country_id'),
				'buyer_country_name'        	=> get_country_name($buyer_details->f('country_id')),
				'buyer_state_id'            	=> $buyer_details->f('state_id'),
				'buyer_state_name'          	=> get_state_name($buyer_details->f('state_id')),
				'buyer_city'                	=> $buyer_details->f('city'),
				'buyer_zip'                 	=> $buyer_details->f('zip'),
				'buyer_address'             	=> $buyer_details->f('address'),
				'buyer_email'             	=> $c_email,
				'buyer_fax'             => $c_fax,
				'buyer_phone'           => $c_phone,
				'name_text'				=> $text,
				'field'				    => $in['buyer_id'] ? 'customer_id' : 'contact_id',
				'free_field'			=> $free_field,
				'free_field_txt'		=> nl2br($free_field),
				'cc'					=> $this->get_cc($in),
			    'contacts'				=> $this->get_contacts($in),
			    'addresses'				=> $this->get_addresses($in),
			    'do_next'				=> $do_next,
			    'delivery_address_id'                	=> '0',
			    'type_dd'			=> build_incinv_type_dd($in['type']),
			    'type'			    => $in['type'] ? $in['type'] : '0',
			    'allow_disconto'    => $in['allow_disconto']

				

				
			);

			if($in['contact_id']){
				$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
			}

     
      $output['link_url'] = $in['link_url'];
      
      if($in['link_url']){
         $output['view_upload'] = false;
         $output['exist_image'] = true;
      }else{
      	 $output['view_upload'] = true;
      	 $output['exist_image'] = false;
      }


        $this->out = $output;
	}
  
  	private function getEditIncommingInvoice(){
		
		$in=$this->in;
		$page_title= gm('Edit Incomming Invoice');
		$do_next='invoice-incomming_ninvoice-invoice-update_incomming';
		$invoice_id=$in['invoice_id']?$in['invoice_id']:$in['incomming_invoice_id'];
		// print_r($in);
		$incomming_invoice = $this->db->query("SELECT *
										FROM  tblinvoice_incomming
										WHERE invoice_id='".$invoice_id."'");

		if(!$incomming_invoice->next()){
			// unset($view);
			echo "string";
			return ark::run('invoice-incomming_invoices');
		}
		if(!$in['buyer_id']){
			$in['buyer_id'] = $incomming_invoice->f('supplier_id');
		}
		
		$buyer_id=$incomming_invoice->f('supplier_id');
		$customer_id = $incomming_invoice->f('supplier_id');
	

		if($in['buyer_id']){
			//get buyer details
			$buyer_details = $this->db->query("SELECT customers.customer_id,customers.name,customer_addresses.*, customer_legal_type.name AS l_name
				                    FROM customers
				                    LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.billing=1
				                    LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
		                            WHERE customers.customer_id='".$in['buyer_id']."'");
			$buyer_details->next();

			$free_field = $buyer_details->f('address').'
					'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
					'.get_country_name($buyer_details->f('country_id'));
		}	

		
		

		$contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
									WHERE contact_id='".$incomming_invoice->f('contact_id')."' AND customer_id='".$incomming_invoice->f('supplier_id')."'");

		$output=array(
	    	'page_title'            		=> $page_title,
			'incomming_invoice_id'  		=> $invoice_id,
		    'invoice_date'            	    => $incomming_invoice->f('invoice_date')*1000,
		    'due_date'            	        => $incomming_invoice->f('due_date')*1000,
		    'booking_number'                => $incomming_invoice->f('booking_number'), 
		    'invoice_number'                => $incomming_invoice->f('invoice_number'),   
		   
			'style'     				    => ACCOUNT_NUMBER_FORMAT,
			'do_next'					    => $do_next,
			
			'pick_date_format'      		=> pick_date_format(),
			'hide_currency2'		 		=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
			'hide_currency1'		 		=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
			
			'cc'						=> $this->get_cc($in),
			'contacts'					=> $this->get_contacts($in),
			'addresses'					=> $this->get_addresses($in),
			
			'buyer_name'                =>  $buyer_details->gf('name'),
		
			'buyer_id'                  => $incomming_invoice->f('supplier_id'),
			'contact_id'                => $incomming_invoice->f('contact_id'),
			
			'field'			            => $incomming_invoice->f('supplier_id')? 'customer_id' : 'contact_id',
			'contact_name'       	    => trim($contact_name->f('firstname').' '.$contact_name->f('lastname')),
				'free_field'		    => $incomming_invoice->f('free_field') ? $incomming_invoice->f('free_field') : $free_field,
			'free_field_txt'		    => nl2br($incomming_invoice->f('free_field') ? $incomming_invoice->f('free_field') : $free_field ),
			 'delivery_address_id'                	=> $incomming_invoice->f('delivery_address_id'),
			 'type_dd'			=> build_incinv_type_dd($incomming_invoice->gf('type')),
			 'type'			    => $incomming_invoice->gf('type') ? $incomming_invoice->gf('type') : '0',
			 'allow_disconto'    => $incomming_invoice->f('allow_disconto')==1 ? true : false,
			 'disconto'                =>  display_number($incomming_invoice->gf('disconto')),

		
			

		);
		 $output['invoice_line']=array();
		 $total=0;
		 $lines=$this->db->query("SELECT   tblinvoice_incomming_line.* FROM  tblinvoice_incomming_line where invoice_id='".$invoice_id."'");
           	 
             $i=0; 
           	 $row_id = 'tmp'.$i;
           while($lines->move_next()) {
               	$invoice_line=array(
					'tr_id'         		=> $row_id,
					'vat_line'  			=> display_number($lines->f('vat_line')),
					'total'  			    => display_number($lines->f('total')*$lines->f('vat_line')/100 +$lines->f('total')),
					'total_no_vat'  	    => display_number($lines->f('total')),

					);
           	  

           	  array_push($output['invoice_line'], $invoice_line);
           	  $total+=$lines->f('total')*$lines->f('vat_line')/100 +$lines->f('total');
           }  

       $output['total']= display_number($total);
	    	
      global $config;
      ark::loadLibraries(array('aws'));
	  $aws = new awsWrap(DATABASE_NAME);
	  $exist =  $aws->doesObjectExist($config['awsBucket'].DATABASE_NAME.'/'.$incomming_invoice->f('file_name'));
	  if($incomming_invoice->f('amazon') || (!$incomming_invoice->f('file_path') && !$incomming_invoice->f('pdf_path'))){
	      $link ='';
	         if($exist){
		       $link = $aws->getLink($config['awsBucket'].DATABASE_NAME.'/'.$incomming_invoice->f('file_name'));
	         }

	    	 $output['is_amazon'] = true;
	     	 $output['link_url'] = $link;
	     	 $in['exist_image'] = $link ? true : false;
		     $output['exist_image'] = $link ? true : false;
		     $output['view_upload'] = $link ? false : true;

          }else{

		     $is_active = $this->db->field("SELECT active FROM apps WHERE name = 'Dropbox'");
	         if($is_active){
		     
		     $output['is_dropbox']=true;
		     
		    
				if(!$in['file_path']){
					$in['file_path']=$info->f('file_path');
				}
				if($in['file_path']){
					$d = new drop('invoices');
					$link=$d->getLink(urldecode($incomming_invoice->f('file_path')));
					// console::log($d);
					if(!$link){
						
						$output['folder']	    = DATABASE_NAME;
						$output['supplier_id']	= $incomming_invoice->f('supplier_id');
						$output['contact_id']   = $incomming_invoice->f('contact_id');
						$output['serial']		= $incomming_invoice->f('invoice_number');
					
						
					}else{
						
						if($info->f('pdf_path')){

							@mkdir('../upload/'.DATABASE_NAME.'/temp_pdfs/',0775,true);
							$f = fopen("../upload/".DATABASE_NAME."/temp_pdfs/".urldecode($incomming_invoice->f('file_name')), 'w+b');
							$d->getFile(urldecode($incomming_invoice->f('file_path')), $f);
							fclose($f);
							
							
							$output['link_url']     ="../upload/".DATABASE_NAME."/temp_pdfs/".urldecode($incomming_invoice->f('file_name'));
						
									
							
						}else{
							$n = $incomming_invoice->f('file_name');
							if(!$n){
								$a = explode('/', urldecode($incomming_invoice->f('file_path')));
								$b = count($a);
								$n = $a[$b-1];
							}
							@mkdir('../upload/'.DATABASE_NAME.'/temp_pdfs/',0775,true);
							$f = fopen("../upload/".DATABASE_NAME."/temp_pdfs/".$n, 'w+b');
							$d->getFile(urldecode($incomming_invoice->f('file_path')), $f);
							fclose($f);
							$ext = pathinfo("../upload/".DATABASE_NAME."/temp_pdfs/".$n,PATHINFO_EXTENSION);
							
								$output['link_url']		="../upload/".DATABASE_NAME."/temp_pdfs/".$n;
								
								$output['is_pdf']		= $ext == 'pdf' ? '' :'hide';
								$output['is_not_path']	= $ext == 'pdf' ? 'hide' :'';
							
						}
					}
				}else{
					
					$output['folder']			= DATABASE_NAME;
					$output['supplier_id']		= $incomming_invoice->f('supplier_id');
					$output['contact_id']		= $incomming_invoice->f('contact_id');
					$output['serial']			= $incomming_invoice->f('invoice_number');
					
					
				}

	  }else{
		   $output['is_dropbox']=false;
		  
     	}
    }
		

		
		$this->out = $output;
	}

	public function get_cc($in){
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1  ";
		//$filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			//$filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		
		$cust = $this->db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
			FROM customers
			WHERE $filter
			
			ORDER BY name
			LIMIT 5")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);

			$result[]=array(
				"id"					=> $value['customer_id'].'-'.$value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip_name'] ? $value['zip_name'] : '',
				'city'					=> $value['city_name'] ? $value['city_name'] : '',
				"bottom"				=> $value['zip_name'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts($in) {
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customer_contacts.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 5")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
			);

		}

		$added = false;
		if(count($result)==5){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				'right'					=> $value['country_name'],
				'title'					=> $value['position_n'],
				'bottom'				=> $value['position_n'],
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in){
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 5");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 5");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> ($address->f('address')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==5){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> ($address->f('address')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

}

$incomminginvoice = new IncommingInvoiceEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $incomminginvoice->output($incomminginvoice->$fname($in));
	}

	$incomminginvoice->getIncommingInvoice();
	$incomminginvoice->output();
