<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
// $view_list = new at(ark::$viewpath.'invoices_list.html');
class InvoicesList extends Controller{
    function __construct($in,$db = null)
    {	
	parent::__construct($in,$db = null);	
    }

    public function get_List(){
    ini_set('memory_limit','2G');
	$in = $this->in;

	if($in['reset_list']){
		if(isset($_SESSION['tmp_add_to_pdf'])){
			unset($_SESSION['tmp_add_to_pdf']);
		}
		if(isset($_SESSION['add_to_pdf'])){
			unset($_SESSION['add_to_pdf']);
		}	
	}

	global $config;
	if(!empty($in['start_date_js'])){
	    $in['start_date'] =strtotime($in['start_date_js']);
	    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
	}
	if(!empty($in['stop_date_js'])){
	    $in['stop_date'] =strtotime($in['stop_date_js']);
	    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
	}

	$l_r = ROW_PER_PAGE;

	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
	$order_by_array = array('serial_number','t_date','buyer_name','our_ref','amount_vat','amount', 'due_date', 'balance','sepa_number', 'days_expired', 'doc_type_title','acc_manager_name','subject','your_ref');
	$arguments_o='';
	$arguments_s='';

	if($in['customer_id']){
	    $in['buyer_id'] = $in['customer_id'];
	}

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	$arguments='';
	$filter = "WHERE 1=1 ";
	$filter_link = 'block';
	$filter_article='';
	$filter_your_ref = '';
	//$order_by = " ORDER BY t_date DESC, iii DESC ";
	$order_by = " ORDER BY DATE_ADD(t_date, INTERVAL 3 HOUR) DESC, iii DESC ";
	if($in['buyer_id']){
	    $filter .=" AND tblinvoice.buyer_id='".$in['buyer_id']."' ";
	    $arguments .='&buyer_id='.$in['buyer_id'];
	}
	if($in['view'] != 2){
	    if(empty($in['archived'])){
		$filter.= " AND (tblinvoice.f_archived='0' OR tblinvoice.f_archived='3') ";
	    }else{
		$filter.= " AND tblinvoice.f_archived='1' ";
		$arguments.="&archived=".$in['archived'];
	    }
	}

	if(!empty($in['filter'])){
	    $arguments.="&filter=".$in['filter'];
	}
	if(!empty($in['c_invoice_id'])){
	    $filter.=" and (tblinvoice.c_invoice_id = '".$in['c_invoice_id']."')";
	    $arguments.="&c_invoice_id=".$in['c_invoice_id'];

	}
	if(!empty($in['search'])){
	    $final_s="";
		if(is_numeric(str_replace(',','.',$in['search']))){
			$final_s=" OR FLOOR(tblinvoice.amount)='".$in['search']."' OR FLOOR(tblinvoice.amount_vat)='".$in['search']."' ";
		}
	    $filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' ".$final_s." )";
	    $arguments_s.="&search=".$in['search'];
	}
	if($in['article_name_search']){
		$filter_article.=" INNER JOIN tblinvoice_line ON tblinvoice_line.invoice_id=tblinvoice.id 
					AND (tblinvoice_line.name like '%".$in['article_name_search']."%' OR tblinvoice_line.item_code like '%".$in['article_name_search']."%')  ";
	                       
		$arguments.="&article_name_search=".$in['article_name_search'];
	}
	$filter_your_ref ='';
	if($in['your_reference_search']){
		$filter_your_ref =" AND tblinvoice.your_ref LIKE '%".$in['your_reference_search']."%' ";
		$arguments.="&your_reference_search=".$in['your_reference_search'];
	}
	if($in['customer_vat']){
		$filter_your_ref .=" AND tblinvoice.seller_bwt_nr LIKE '%".$in['customer_vat']."%' ";
		$arguments.="&customer_vat=".$in['customer_vat'];
	}
	if($in['ogm']){
		$filter.=" AND REPLACE(tblinvoice.ogm,'/','')='".$in['ogm']."' "; 
	}

	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	
	if(!empty($in['order_by'])){
	    if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
			    $order = " DESC ";
			}
			if($in['order_by']=='balance' || $in['order_by']=='days_expired'){
				$order_by='';
				$filter_limit =' ';
			}else{
				if($in['order_by']=='t_date'){
					$order_by =" ORDER BY ".$in['order_by']." ".$order." , iii ".$order;
				}elseif($in['order_by']=='doc_type_title'){
					$order_by =" ORDER BY tblinvoice.type ".$order;
				}else{
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
				}
				$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];	
				$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";			
			}

	    }
	}

	if(!isset($in['view'])){
	    $in['view'] = 0;
	}
	if($in['view'] == 1){
	    $filter.=" and tblinvoice.not_paid = '0' and (tblinvoice.status = '0' OR tblinvoice.status = '1') AND sent='0' ";
	    $arguments.="&view=1";
	}
	if($in['view'] == 2){
	    $filter.=" and tblinvoice.not_paid = '0' and tblinvoice.type = '1' ";
	    $arguments.="&view=2";
	    if(empty($in['archived'])){
		$filter.= " AND (f_archived='0' OR f_archived='3')";
	    }else{
		$filter.= " AND (f_archived='1' OR f_archived='3') ";
		$arguments.="&archived=".$in['archived'];
	    }
	}
	/*if($in['view'] == 3){
	    $filter.=" and tblinvoice.type = '0' ";
	    $arguments.="&view=3";
	    $view_list->assign('selected_regular','class="selected"');
	}*/
	/*if($in['view'] == 3){
	    $filter.=" and tblinvoice.not_paid = '0' and (tblinvoice.type = '2' || tblinvoice.type = '4')";
	    $arguments.="&view=3";
	}*/
	/*if($in['view'] == 5){
	    $filter.=" and tblinvoice.sent = '1' ";
	    $arguments.="&view=5";
	    $view_list->assign('selected_sent','class="selected"');
	}*/
	if($in['view'] == 4){
	    $filter.=" and tblinvoice.not_paid = '0' AND tblinvoice.paid!='1' and tblinvoice.sent = '1' and tblinvoice.due_date < ".time();
	    $arguments.="&view=4";
	}
	if($in['view'] == 7){
	    $filter.=" and tblinvoice.not_paid = '1' ";
	    $arguments.="&view=7";
	}
	if($in['view'] == 5){
	    $filter.=" and tblinvoice.not_paid = '0' and (tblinvoice.status = '0' || (tblinvoice.status='1' and tblinvoice.paid='2')) and tblinvoice.sent = '1' ";
	    $arguments.="&view=5";
	}
	if($in['view'] == 8){
	    $filter.=" and tblinvoice.not_paid = '0' and tblinvoice.paid = '1' and tblinvoice.status = '1' and tblinvoice.sent = '1' ";
	    $arguments.="&view=8";
	}
	if($in['view'] == 6){
	    $filter.=" and tblinvoice.not_paid = '0' and (tblinvoice.paid = '0' OR tblinvoice.paid = '2') and tblinvoice.sent != '0' ";
	    $arguments.="&view=6";
	}
	/*if($in['view'] == 9){
	    $filter.=" and tblinvoice.type = '1' ";
	    $arguments.="&view=9";

	}*/

	if(isset($in['doc_type']) && $in['doc_type']!=-1 ){
	    $filter.=" and tblinvoice.type = '".$in['doc_type']."'  ";
	    $arguments.="&doc_type=".$in['doc_type'];
	}

	if(!empty($in['start_date']) && !empty($in['stop_date'])){
	    /*list($d, $m, $y) = explode($spliter,  $in['start_date']);
	    $start_date= mktime(0, 0, 0, $m, $d, $y);
	    list($d, $m, $y) = explode($spliter,  $in['stop_date']);
	    $stop_date= mktime(23, 59, 59, $m, $d, $y);*/
	    $filter.=" and tblinvoice.invoice_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	    $arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	    // $view_list->assign('start_date',$in['start_date']);
	    // $view_list->assign('stop_date',$in['stop_date']);
	    $filter_link = 'none';
	}
	else if(!empty($in['start_date'])){
	    /*list($d, $m, $y) = explode($spliter,  $in['start_date']);
	    $start_date= mktime(0, 0, 0, $m, $d, $y);
	    $stop_date= mktime(23, 59, 59, $m, $d, $y);*/
	//	$filter.=" and tblinvoice.invoice_date BETWEEN '".$start_date."' and '".$stop_date."' ";
	    $filter.=" and cast(tblinvoice.invoice_date as signed) >= ".$in['start_date']." ";
	    $arguments.="&start_date=".$in['start_date'];
	}
	else if(!empty($in['stop_date'])){
	    /*list($d, $m, $y) = explode($spliter,  $in['stop_date']);
	    $start_date= mktime(0, 0, 0, $m, $d, $y);
	    $stop_date= mktime(23, 59, 59, $m, $d, $y);*/
	//	$filter.=" and tblinvoice.invoice_date BETWEEN '".$start_date."' and '".$stop_date."' ";
	    $filter.=" and cast(tblinvoice.invoice_date as signed) < ".$in['stop_date']." ";
	    $arguments.="&stop_date=".$in['stop_date'];
	    $filter_link = 'none';
	}
	$arguments = $arguments.$arguments_s;
	$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
	//$db->query("SELECT tblinvoice.*, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii

	$all_payments_ar=array();
	$all_payments=$this->db->query("SELECT invoice_id, amount, credit_payment,ROUND(cast(amount as decimal(12, 4))+0.0012,2) AS formatted_amount FROM tblinvoice_payments")->getAll();
	foreach($all_payments as $key=>$value){
		if(!$all_payments_ar[$value['invoice_id']]){
			$all_payments_ar[$value['invoice_id']]=array();
		}
		$tmp_pay_item=array(
			'amount'			=> $value['formatted_amount'],
			'credit_payment'		=> $value['credit_payment'],
		);
		array_push($all_payments_ar[$value['invoice_id']],$tmp_pay_item);
	}

	$nav = array();

	$max_rows_data = $this->db->query("SELECT  tblinvoice.id, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date
		    FROM tblinvoice
		     ".$filter_article."
		     ".$filter."
		    ".$filter_your_ref." ".$order_by);
	$max_rows=$max_rows_data->records_count();

	if(!$_SESSION['tmp_add_to_pdf'] || ($_SESSION['tmp_add_to_pdf'] && empty($_SESSION['tmp_add_to_pdf']))){
		while($max_rows_data->next()){
			$_SESSION['tmp_add_to_pdf'][$max_rows_data->f('id')]=1;
			array_push($nav, (object)['invoice_id'=> $max_rows_data->f('id') ]);
		}
	}else{
		while($max_rows_data->next()){
			array_push($nav, (object)['invoice_id'=> $max_rows_data->f('id') ]);
		}
	}
	$all_pages_selected=false;
	$minimum_selected=false;
	if($_SESSION['add_to_pdf']){
		if($max_rows>0 && count($_SESSION['add_to_pdf']) == $max_rows){
			$all_pages_selected=true;
		}else if(count($_SESSION['add_to_pdf'])){
			$minimum_selected=true;
		}	
	}

	$tblinvoice = $this->db->query("SELECT tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, mandate.sepa_number,multiple_identity.identity_name,vat_new.description AS vat_regime
		    FROM tblinvoice
		    LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id
		    LEFT JOIN multiple_identity ON tblinvoice.identity_id=multiple_identity.identity_id 
		    LEFT JOIN vat_new ON tblinvoice.vat_regime_id=vat_new.id
		     ".$filter_article."
		     ".$filter."
		    ".$filter_your_ref.$order_by.$filter_limit);
	//de aici
	/*
	$tblinvoice2 = $tblinvoice;
	$all_invoices_id2 = '';
	while ( $tblinvoice2->next()) {
	    $all_invoices_id2 = $all_invoices_id2.','.$tblinvoice2->f('id');
	}
	$all_invoices_id2 = trim($all_invoices_id2,',');
	*/
	//pana aici

	$is_easy_invoice_diy=aktiUser::get('is_easy_invoice_diy');

	$today = mktime(0, 0, 0);
	$result = array('query'=>array(),'max_rows'=>$max_rows,'all_pages_selected'=> $in['exported'] ? false : $all_pages_selected,'minimum_selected'=> $in['exported'] ? false : $minimum_selected, 'nav' =>$nav);
	
		$j=0;
		$color = '';
		$all_invoices_id = '';
		while($tblinvoice->move_next()){

			if($in['order_by']=='balance'){
				if($tblinvoice->f('status')=='1' || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2'){
					continue; //Draft and Paid invoices are not displayed
				}
			}

			$days_expired = intval(($today - $tblinvoice->f('due_date'))/86400);
			if($in['order_by']=='days_expired'){
				//console::log($days_expired);
				if($days_expired<1 || ($tblinvoice->f('status')=='1' || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2')){
					continue; //Draft and Paid invoices or which are not expired - not displayed
				}
			}

		    $status='';
		    $status_title='';
		    $type='';
		    $type_title='';
		    $doc_type=$tblinvoice->f('type');
		    $doc_type_title='';
		    $color='';
		    switch ($tblinvoice->f('type')){
			case '0':
			    $type = 'regular_invoice';
			    //$type_title = gm('Regular invoice');
			    $doc_type_title = gm('Regular invoice');
			    break;
			case '1':
			    $type = 'pro-forma_invoice';
			    //$type_title = gm('Proforma invoice');
			    $doc_type_title = gm('Proforma invoice');
			    break;
			case '2':
			    $type = 'credit_invoice';
			    //$type_title = gm('Credit Invoice');
			    $doc_type_title = gm('Credit invoice');
			    break;
			case '3':
			    $type = 'international_invoice';
			    $doc_type_title = gm('International invoice');
			    break;
			case '2':
			    $type = 'international_credit_invoice';
			    $doc_type_title = gm('International Credit invoice');
			    break;
		    }

		    switch ($tblinvoice->f('status')){
			case '0':
			    if($tblinvoice->f('sent') == '0'){
					$status_b = 'draft';
			    }else{
					if($tblinvoice->f('paid') == '2' ){
					    $status_b = 'partial';
					}else {
					    $status_b = 'final';
					}
			    }
			    break;
			case '1':
				if($tblinvoice->f('sent') == '0'){
					$status_b = 'draft';
			    }else{
			    	$tblinvoice->f('type')=='2' || $tblinvoice->f('type')=='4'? $status_b = 'final': $status_b = 'fully';
			    }
			    break;
			default:
			    $status_b = 'draft';
			    break;
		    }

		    if($tblinvoice->f('sent') == '0' ){
				// $status = 'Draft';
				    /*if($tblinvoice->f('type')=='2'){
					$type='credit_draft';
					$type_title = gm('Credit Invoice');
				    }else{*/
					$type = 'draft_invoice';
					$type_title = gm('Draft');
					$nr_status=1;
				    /*}*/
				    $status='';
				    $status_title= '';
				    $color='';
				    $class='';
		    }else if($tblinvoice->f('status')=='1' || ($tblinvoice->f('status')=='0' && $tblinvoice->f('paid')=='2' ) ){
				$type_title = $tblinvoice->f('paid')=='2' ? gm('Partially Paid') : gm('Paid');
				$nr_status=3;
		    }else if($tblinvoice->f('sent')!='0' && $tblinvoice->f('not_paid')=='0'){
		    	$nr_status=$tblinvoice->f('status')=='0' && $tblinvoice->f('due_date')< time() && $tblinvoice->f('type')!='2' && $tblinvoice->f('type')!='4' ? 4 : 2;
		    	if($tblinvoice->f('status')=='0' && $tblinvoice->f('due_date')< time() && $tblinvoice->f('type')!='2' && $tblinvoice->f('type')!='4'){
		    		$type_title = gm('Late');
		    	}else{		    		
		    		$type_title = gm('Final');
		    		if($is_easy_invoice_diy){
		    			$jefacture_exported=$this->db->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$tblinvoice->f('id')."' AND jefacture='1' AND `type`='0' ");
		    			if($jefacture_exported){
		    				$type_title = gm('Exported');
		    				$nr_status=3;
		    			}
		    		}	    		
		    	}							
		    } else if($tblinvoice->f('sent')!='0' && $tblinvoice->f('not_paid')=='1'){
				$type_title = gm('No Payment');
				$nr_status=2;
		    }

		    if($tblinvoice->f('f_archived')=='1'){
				$type_title = gm('Archived');
				$nr_status=1;
			}

		    if($tblinvoice->f('created_by') == 'System'){
			$color .= ' recurring';
		    }
		    if($tblinvoice->f('pdf_layout')){
			$link_end='&type='.$tblinvoice->f('pdf_layout').'&logo='.$tblinvoice->f('pdf_logo');
		    }else{
			$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
		    }
		    #if we are using a customer pdf template
		    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $tblinvoice->f('pdf_layout') == 0){
			$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
		    }

		    $tblinvoice2 = $this->db->query("SELECT id from tblinvoice where c_invoice_id='".$tblinvoice->f('id')."' AND f_archived='0'");
		    $proforma_has_invoice = $this->db->field("SELECT id FROM tblinvoice WHERE proforma_id='".$tblinvoice->f('id')."' ");

		    // and tblinvoice.not_paid = '0' AND tblinvoice.status = '0' and tblinvoice.sent = '1' and tblinvoice.due_date < ".time()
		    $late = false;
		    if($tblinvoice->f('not_paid') == '0' && $tblinvoice->f('status') == '0' && $tblinvoice->f('sent') == '1' && $tblinvoice->f('due_date') < time()){
			$late = true;
		    }

		    //calculate balance
		    //$already_payedd = $this->db->query("SELECT SUM(amount) AS total_payedd FROM tblinvoice_payments WHERE invoice_id='".$tblinvoice->f('id')."'");
			//$balance = $tblinvoice->f('amount_vat') - ($negative*$already_payedd->f('total_payedd'));
		    $balance=$tblinvoice->f('amount_vat');
		    //console::log($balance);
		    if($all_payments_ar[$tblinvoice->f('id')]){
		    	foreach($all_payments_ar[$tblinvoice->f('id')] as $key1=>$value1){
		    		if($value1['credit_payment']){
		    			if($value1['amount']<0){
		    				$balance+=$value1['amount'];
		    			}else{
		    				$balance-=$value1['amount'];
		    			}
	    			}else{
	    				$balance-=$value1['amount'];
	    			}
		    	}
		    }
		    //console::log($balance);

		    // $view_list->assign(array(
		    $item=array(

			'pdf_link'    			=> ($tblinvoice->f('type') ==2 || $tblinvoice->f('type') ==4 )? 'index.php?do=invoice-invoice_credit_print&id='.$tblinvoice->f('id').'&lid='.$tblinvoice->f('email_language').$link_end
										: 'index.php?do=invoice-invoice_print&id='.$tblinvoice->f('id').'&lid='.$tblinvoice->f('email_language').$link_end,
			'credit_link'   		=> 'index.php?do=invoice-invoices&c_invoice_id='.$tblinvoice->f('id').$arguments,
			'info_link'     		=> 'index.php?do=invoice-invoice&invoice_id='.$tblinvoice->f('id'),
			'edit_link'     		=> 'index.php?do=invoice-ninvoice&invoice_id='.$tblinvoice->f('id'),
			// 'archive_link' 		 	=> 'index.php?do=invoice-invoices_list-invoice-archive&id='.$tblinvoice->f('id').$arguments.$arguments_o,
			'archive_link' 		 	=> array('do'=>'invoice-invoices-invoice-archive', 'id'=>$tblinvoice->f('id')),
			// 'delete_link'  			=> 'index.php?do=invoice-invoices_list-invoice-delete&id='.$tblinvoice->f('id').'&archived=1'.$arguments.$arguments_o,
			'delete_link'  			=> array('do'=>'invoice-invoices-invoice-delete', 'id'=>$tblinvoice->f('id')),
			// 'activate_link' 		=> 'index.php?do=invoice-invoices_list-invoice-activate&id='.$tblinvoice->f('id').'&archived=1'.$arguments.$arguments_o,
			'activate_link' 		=> array('do'=>'invoice-invoices-invoice-activate', 'id'=>$tblinvoice->f('id')),
			'pay_link'      		=> 'index.php?do=invoice-invoices_list-invoice-pay&commission_id='.$tblinvoice->f('comm_id').'&invoice_id='.$tblinvoice->f('id').$arguments,
			'mark_as_payed'			=> 'index.php?do=invoice-invoices_list-invoice-markAsPayed&invoice_id='.$tblinvoice->f('id').$arguments.$arguments_o,
			'is_edit'				=> (isset($in['archived']) && $in['archived'] == 1) ? false : ($tblinvoice->f('sent') == 1 ? false : true),
			'is_money'				=> $tblinvoice2->move_next() ? true : false,
			'can_archive'			=> $tblinvoice->f('f_archived')==1 ? false : ($tblinvoice->f('sent') == 1 ? false : true),
			'can_activate'			=> $tblinvoice->f('f_archived')==1 ? true : false,
			'can_pay'				=> $tblinvoice->f('f_archived')==1 ? false : ((isset($in['archived']) && $in['archived'] == 1) ? false : ($tblinvoice->f('sent') == 1 ? ($tblinvoice->f('paid') == 1 && $tblinvoice->f('status') == 1 ? false : ( !$proforma_has_invoice ? true : false)) : false)),
			'can_delete'			=> $_SESSION['access_level'] == 1 ? (!empty($in['archived']) ? true : false) : false,
			'type'					=> $type,
			'invoice_type'			=> $tblinvoice->f('type'),
			'c_invoice_id'			=> $tblinvoice->f('c_invoice_id'),
			'type_title'			=> $type_title,
			'nr_status'				=> $nr_status,
			'status'				=> $status,
			'class'					=> $class,
			'status_title'			=> $status_title,
			'doc_type_title'		=> $doc_type_title,
			'invoice_property'		=> $color,
			'fully'					=> $status_b != 'draft' && $status_b != 'fully' ? true : false,
			'payed'					=> $status_b == 'fully' ? true : false,
			'bpaid'					=> $tblinvoice->f('not_paid') == '0' && $tblinvoice->f('status') < '2' && $tblinvoice->f('sent') == '1' && $tblinvoice->f('due_date') < time(),
			'no_regular_invoice'	=> $tblinvoice->f('proforma_id') ? false : true,
			'no_credit'				=> $tblinvoice->f('type')== 2 ? false : true,
			'bePaid_debt_done'		=> $tblinvoice->f('bPaid_debt_id')!='' ? true : false,
			'amount'				=> place_currency(display_number($tblinvoice->f('amount')),  get_commission_type_list($tblinvoice->f('currency_type'))),
			'amount_vat'			=> place_currency(display_number($tblinvoice->f('amount_vat')), get_commission_type_list($tblinvoice->f('currency_type'))),
			'our_reference'			=> $tblinvoice->f('our_ref'),
			'mandate_reference'		=> $tblinvoice->f('sepa_number'),
			'id'         	    	=> $tblinvoice->f('id'),
			'inv_type'			    => $in['view']?'':'all',
			's_number'		     	=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
			'created'         		=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
			'due_date'				=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('due_date')),
			'seller_name'      		=> $tblinvoice->f('seller_name'),
			'buyer_name'       		=> $tblinvoice->f('buyer_name'),
			'hide_paid'				=> $tblinvoice->f('status') ? 'hide' : '',
			'text'					=> $tblinvoice->f('created_by') == 'System' ? gm('Created from a recurring invoice') : '',
			'invoice_id'			=> $tblinvoice->f('id'),
			'check_add_to_product'	=> $in['exported'] ? false : ($_SESSION['add_to_pdf'][$tblinvoice->f('id')] == 1 ? true : false),
			'buyer_id'				=> $tblinvoice->f('buyer_id'),
			'contact_id'			=> $tblinvoice->f('contact_id'),
			'late'					=> $late,
			'edebex_show'			=> false, //($tblinvoice->f('amount_vat')>=5000 && $tblinvoice->f('due_days')>=30) ? true : false,	
			'edebex_status'			=> $tblinvoice->f('edebex_id') !='' ? true : false,
			'post_invoice'		    => $tblinvoice->f('postgreen_id')!='' ? true : false,
			/*'balance'				=> ($tblinvoice->f('status')=='1' || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2')? '':place_currency(display_number($amount_due)),*/
			'balance'				=> (($tblinvoice->f('status')=='1' && $tblinvoice->f('paid')!='2') || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2')? '' :place_currency(display_number($balance), get_commission_type_list($tblinvoice->f('currency_type'))),
			'balance_ord'			=> (($tblinvoice->f('status')=='1' && $tblinvoice->f('paid')!='2') || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2')? '' : $balance,
			'days_expired' 			=> (($tblinvoice->f('status')=='1' && $tblinvoice->f('paid')!='2') || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2') || $days_expired<1 ? '': $days_expired ,
			'days_expired_ord' 		=> (($tblinvoice->f('status')=='1' && $tblinvoice->f('paid')!='2') || ($tblinvoice->f('sent') == '0' && $tblinvoice->f('type') != '2') || $tblinvoice->f('type') == '2') || $days_expired<1 ? 0:$days_expired ,
			'identity_name'				=> $tblinvoice->f('identity_name') ? stripslashes($tblinvoice->f('identity_name')) : gm('Main Identity'),
			'hide_unsent'   		=> $tblinvoice->f('sent')==0? false: true,
			'vat_regime'				=> stripslashes($tblinvoice->f('vat_regime')),
			'acc_manager_name'	=> stripslashes($tblinvoice->f('acc_manager_name')),
			'subject'			=> stripslashes($tblinvoice->f('subject')),
			'your_ref'			=> stripslashes($tblinvoice->f('your_ref')),
		    );
			
			if(!$tblinvoice->f('sent')){
				// payments can be recorded for draft invoices, too
				$item['can_pay']= true;
			}

		    array_push($result['query'], $item);
		    $all_invoices_id = $all_invoices_id.','.$tblinvoice->f('id');
		    // $view_list->loop('invoice_row');
		    $j++;

		}
	
	

	$result['seller_data']=array(
	    'invoice_seller_b_address'	=> ACCOUNT_DELIVERY_ADDRESS,
	    'invoice_seller_b_city'		=> ACCOUNT_DELIVERY_CITY,
	    'invoice_seller_b_zip'		=> ACCOUNT_DELIVERY_ZIP,
	    'invoice_seller_b_country_id'	=> ACCOUNT_DELIVERY_COUNTRY_ID,
	    'invoice_seller_b_country_dd'	=> build_country_list(ACCOUNT_DELIVERY_COUNTRY_ID),
	    'invoice_seller_name'		=> ACCOUNT_COMPANY,
	    'invoice_vat_no'			=> ACCOUNT_VAT_NUMBER,
	    'languages'				=> $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'"),
	    'language_dd'			=> build_language_dd_new($this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'")),
	    'invoice_seller_iban'		=> ACCOUNT_IBAN,
	    'invoice_seller_swift'		=> ACCOUNT_BIC_CODE,
	    'invoice_seller_email'		=> ACCOUNT_EMAIL
	);


	$post_active=$this->db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	$bepaid_active=$this->db->field("SELECT active FROM apps WHERE name='Instapaid' AND main_app_id='0' AND type='main'");
	$edebex_active=$this->db->field("SELECT active FROM apps WHERE name='Edebex' AND main_app_id='0' AND type='main'");

	if($_SESSION['main_u_id']==0){
	    $bPaidCust=$this->db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	}else{
	    $bPaidCust=$this->db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
	}

	if($in['bPaid_modUrl']){
    	    $result['bPaid_modUrl']		= $in['bPaid_modUrl'].'&lang='.strtoupper($_SESSION['l']);
    	}

    	$tblinvoice=null;
	$all_invoices_id = trim($all_invoices_id,',');

	$result['view_go']					= $_SESSION['access_level'] == 1 ? '' : 'hide';
	$result['args']							= $args_v;
	$result['lid']							= $default_lang_id;
	$result['pdf_type']					= $default_pdf_type;
	$result['all_invoices_id']	= $all_invoices_id;
	$result['yuki_active']			= defined('YUKI_ACTIVE') && YUKI_ACTIVE == 1 ? true :false;
	$result['post_active']			= !$post_active || $post_active==0 ? false : true;
	$result['bPaid_reg']			= $bPaidCust!='' ? true : false;
	$result['provider_ref']			= base64_encode(DATABASE_NAME);
	$result['lg']					= $_SESSION['l'];
	//$result['bpaid_active']			= $bepaid_active ? true : false;
	$result['bpaid_active']			= false;
	$result['edebex_active']		= $edebex_active ? true : false;
	$result['yuki_active']			= (defined("YUKI_ACTIVE") && YUKI_ACTIVE==1) ? true : false;
	$easyinvoice=$this->db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
	if($easyinvoice=='1'){
		$result['show_deliveries']	 	=false;
		$result['show_projects']		= false;
		$result['show_interventions']	= false;
	}else{
		$result['show_deliveries']	 	= (in_array(6,perm::$allow_apps) && ($_SESSION['acc_type'] == 'goods' || $_SESSION['acc_type']=='both')) ? true : false;
		$result['show_projects']		= (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
		$result['show_interventions']		= in_array(13,perm::$allow_apps) ? true : false;
	}
	$result['lr'] = $l_r;
	$result['adv_search']	= ACCOUNT_DELIVERY_COUNTRY_ID=='26' ? true : false;
	$result['columns']  	= $this->get_invoices_cols();

	$result['types']  		= build_invoice_type_dd();


	if($in['order_by']=='balance' || $in['order_by']=='days_expired'){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
	    }
	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	    
	}

	$this->out = $result;
    }

    public function get_PostLibrary(){
    
	$result=array();

	$post_library=array();
	$post_active=$this->db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
    if($post_active){
    	$in['ret_res']=true;
    	$post_library=include_once('apps/misc/controller/post_library.php');
    }
    $result['post_library']=$post_library;
    return $result;
}

	public function get_KPI($in){
		session_write_close();
		ini_set('memory_limit','1G');
		$result=array();
        if(!empty($in['search'])){
            $final_s="";
            if(is_numeric(str_replace(',','.',$in['search']))){
                $final_s =" OR FLOOR(tblinvoice.amount)='".$in['search']."' OR FLOOR(tblinvoice.amount_vat)='".$in['search']."' ";
            }
            $filter =" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' ".$final_s." )";
        }
        if($in['customer_vat']){
            $filter_your_ref =" AND tblinvoice.seller_bwt_nr LIKE '%".$in['customer_vat']."%' ";
        }
	$opened =0;
	$total_novat = 0;
	$total_vat = 0;
	$invoicesarr = array();

	$now = time();
	$opened_late = 0;
	$total_novat_late = 0;
	$total_vat_late = 0;
	$lateinvarr = array();
	$invs = $this->db->query("SELECT amount,id,amount_vat,not_paid, due_date FROM tblinvoice WHERE paid!='1' AND sent='1' AND f_archived='0' AND type!='2' AND type!='1' AND c_invoice_id='0' ".$filter.$final_s.$filter_your_ref);
    while ($invs->next()) {
	    $opened++;
	    $total_novat +=$invs->f('amount');
	    $total_vat +=$invs->f('amount_vat');
	    if($invs->f('not_paid')==1){
		$not_paid_amount = get_not_paid_invoice($invs->f('id'));
		$invoicesarr[$invs->f('id')] = array($invs->f('amount')-$not_paid_amount[0],$invs->f('amount_vat')-$not_paid_amount[1]);
	    }else{
	        $invoicesarr[$invs->f('id')] = array($invs->f('amount'),$invs->f('amount_vat'));
	    }
	    if($invs->f('due_date') < $now){
		$opened_late++;
		$total_novat_late += $invs->f('amount');
		$total_vat_late += $invs->f('amount_vat');
		if($invs->f('not_paid')==1){
		    $not_paid_amount = get_not_paid_invoice($invs->f('id'));
	            $lateinvarr[$invs->f('id')] = array($invs->f('amount')-$not_paid_amount[0],$invs->f('amount_vat')-$not_paid_amount[1]);
		}else{
		    $lateinvarr[$invs->f('id')] = array($invs->f('amount'),$invs->f('amount_vat'));
		}
	    }
	}

	$paysarr = array();
	$paylatesarr = array();
	$invs = $this->db->query("SELECT tblinvoice_payments.amount,tblinvoice_payments.invoice_id FROM tblinvoice_payments
	    INNER JOIN tblinvoice ON tblinvoice_payments.invoice_id=tblinvoice.id
	    WHERE tblinvoice.paid!='1' AND tblinvoice.sent='1' AND tblinvoice.f_archived='0' AND tblinvoice.type!='2' AND tblinvoice.type!='1' AND tblinvoice.c_invoice_id='0'  ".$filter.$final_s.$filter_your_ref);
	while ($invs->next()) {
	    $paysarr[$invs->f('invoice_id')] += $invs->f('amount');
	}

	$totalNoVat = 0;
	$totalWithVat = 0;
	foreach ($invoicesarr as $key => $value) {
	    if($value[0]-$paysarr[$key] >0){
		$totalNoVat+=$value[0]-$paysarr[$key];
	    }
	    if($value[1]-$paysarr[$key] >0){
		$totalWithVat+=$value[1]-$paysarr[$key];
	    }
	}
	#lates
	$totalLateNoVat = 0;
	$totalLateWithVat = 0;
	foreach ($lateinvarr as $key => $value) {
	    if($value[0]-$paysarr[$key] >0){
		$totalLateNoVat+=$value[0]-$paysarr[$key];
	    }
	    if($value[1]-$paysarr[$key] >0){
		$totalLateWithVat+=$value[1]-$paysarr[$key];
	    }
	}

	$f_d_month = mktime(0,0,0,date('n'),1,date('Y'));
	$l_d_month = mktime(23,59,59,date('n')+1,0,date('Y'));
	$invoice_amount_month = $this->db->query("SELECT amount AS a, id AS c, amount_vat AS av, type, c_invoice_id FROM tblinvoice WHERE invoice_date BETWEEN '".$f_d_month."' AND '".$l_d_month."' AND sent='1' AND f_archived='0' ".$filter.$final_s.$filter_your_ref)->getAll();
	$invoice_amount_month_a = array('a'=>0,'c'=>0,'av'=>0);
	$credit_amount_month_a = array('a'=>0,'c'=>0,'av'=>0);
	foreach ($invoice_amount_month as $key => $value) {
	    if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
		$invoice_amount_month_a['a']+=$value['a'];
		$invoice_amount_month_a['c']++;
		$invoice_amount_month_a['av']+=$value['av'];

	    }
	    if($value['type'] == '2'){ # credit invoices
		$credit_amount_month_a['a']+=$value['a'];
		$credit_amount_month_a['c']++;
		$credit_amount_month_a['av']+=$value['av'];	
			if($value['a']>0){
				$invoice_amount_month_a['a']-=$value['a'];
			}else{
				$invoice_amount_month_a['a']+=$value['a'];
			}

			if($value['av']>0){
				$invoice_amount_month_a['av']-=$value['av'];
			}else{
				$invoice_amount_month_a['av']+=$value['av'];
			}
	    }
	}
	
	$f_d_last = mktime(0,0,0,date('n')-1,1,date('Y'));
	$l_d_last = mktime(23,59,59,date('n'),0,date('Y'));
	$invoice_amount_last = $this->db->query("SELECT amount AS a, id AS c, amount_vat AS av, type, c_invoice_id FROM tblinvoice WHERE invoice_date BETWEEN '".$f_d_last."' AND '".$l_d_last."' AND sent='1' AND f_archived='0' ".$filter.$final_s.$filter_your_ref)->getAll();
	// AND type!='2' AND type!='1' AND c_invoice_id='0'
	$invoice_amount_last_a = array('a'=>0,'c'=>0,'av'=>0);
	$credit_amount_last_a = array('a'=>0,'c'=>0,'av'=>0);
	foreach ($invoice_amount_last as $key => $value) {
	    if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
		$invoice_amount_last_a['a']+=$value['a'];
		$invoice_amount_last_a['c']++;
		$invoice_amount_last_a['av']+=$value['av'];
	    }
	    if($value['type'] == '2'){ # credit invoices
		$credit_amount_last_a['a']+=$value['a'];
		$credit_amount_last_a['c']++;
		$credit_amount_last_a['av']+=$value['av'];
			if($value['a']>0){
				$invoice_amount_last_a['a']-=$value['a'];
			}else{
				$invoice_amount_last_a['a']+=$value['a'];
			}
			if($value['av']>0){
				$invoice_amount_last_a['av']-=$value['av'];
			}else{
				$invoice_amount_last_a['av']+=$value['av'];
			}	
	    }
	}
    
    if($invoice_amount_last_a['a']){
    	$compare2 = $invoice_amount_month_a['a']/$invoice_amount_last_a['a']*100;	
    }else{
    	$compare2 = 0;
    }
	
	if($invoice_amount_last_a['av']){
    	$compare4 = $invoice_amount_month_a['av']/$invoice_amount_last_a['av']*100;	
    }else{
    	$compare4 = 0;
    }

/*	if($invoice_amount_last_a['a'] > $invoice_amount_month_a['a']) {
	    $compare2 = $invoice_amount_month_a['a']/$invoice_amount_last_a['a']*100-100;
	}else if($invoice_amount_last_a['a'] == $invoice_amount_month_a['a']) {
	    $compare2 = 0;
	}else{
	    $compare2 = ($invoice_amount_month_a['a'] - $invoice_amount_last_a['a'])/$invoice_amount_last_a['a']*100;
	}*/

	$start_30=strtotime('-30 days');
	$f_d_30 = mktime(0,0,0,date('n',$start_30),date('j',$start_30),date('Y',$start_30));
	$l_d_30 = mktime(23,59,59,date('n'),date('j'),date('Y'));
	$invoice_amount_month_30 = $this->db->query("SELECT amount AS a, id AS c, amount_vat AS av, type, c_invoice_id FROM tblinvoice WHERE invoice_date BETWEEN '".$f_d_30."' AND '".$l_d_30."' AND sent='1' AND f_archived='0' ".$filter.$final_s.$filter_your_ref)->getAll();
	$invoice_amount_month_a_30 = array('a'=>0,'c'=>0,'av'=>0);
	$credit_amount_month_a_30 = array('a'=>0,'c'=>0,'av'=>0);
	foreach ($invoice_amount_month_30 as $key => $value) {
	    if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
		$invoice_amount_month_a_30['a']+=$value['a'];
		$invoice_amount_month_a_30['c']++;
		$invoice_amount_month_a_30['av']+=$value['av'];
	    }
	    if($value['type'] == '2'){ # credit invoices
		$credit_amount_month_a_30['a']+=$value['a'];
		$credit_amount_month_a_30['c']++;
		$credit_amount_month_a_30['av']+=$value['av'];	
	    }
	}

	$start__last_30=strtotime('-60 days',time());
	$f_d_last_30 = mktime(0,0,0,date('n',$start__last_30),date('j',$start__last_30),date('Y',$start__last_30));
	$l_d_last_30 = mktime(23,59,59,date('n',$start_30-7200),date('j',$start_30-7200),date('Y',$start_30-7200));
	$invoice_amount_last_30 = $this->db->query("SELECT amount AS a, id AS c, amount_vat AS av, type, c_invoice_id FROM tblinvoice WHERE invoice_date BETWEEN '".$f_d_last_30."' AND '".$l_d_last_30."' AND sent='1' AND f_archived='0' ".$filter.$final_s.$filter_your_ref)->getAll();
	// AND type!='2' AND type!='1' AND c_invoice_id='0'
	$invoice_amount_last_a_30 = array('a'=>0,'c'=>0,'av'=>0);
	$credit_amount_last_a_30 = array('a'=>0,'c'=>0,'av'=>0);
	foreach ($invoice_amount_last_30 as $key => $value) {
	    if($value['type']=='0' && $value['c_invoice_id']=='0'){ # normal invoices
		$invoice_amount_last_a_30['a']+=$value['a'];
		$invoice_amount_last_a_30['c']++;
		$invoice_amount_last_a_30['av']+=$value['av'];
	    }
	    if($value['type'] == '2'){ # credit invoices
		$credit_amount_last_a_30['a']+=$value['a'];
		$credit_amount_last_a_30['c']++;
		$credit_amount_last_a_30['av']+=$value['av'];	
	    }
	}

	if($invoice_amount_last_a_30['a']==0){
		$compare3 = 0;
	}elseif($invoice_amount_last_a_30['a'] > $invoice_amount_month_a_30['a']) {
	    $compare3 = $invoice_amount_month_a_30['a']/$invoice_amount_last_a_30['a']*100-100;
	}else if($invoice_amount_last_a_30['a'] == $invoice_amount_month_a_30['a']) {
	    $compare3 = 0;
	}else{
	    $compare3 = ($invoice_amount_month_a_30['a'] - $invoice_amount_last_a_30['a'])/$invoice_amount_last_a_30['a']*100;
	}

$databasename = DATABASE_NAME;
	$result['chart_data']			= array($opened-$opened_late,$opened_late);
	$result['chart_data_month']		= ceil($compare2+100);
	$result['data_month_i']			= number_format($compare2,1);
	$result['data_month_i_30']		= number_format($compare3,1);
	$result['data_month_i_with_btw']			= number_format($compare4,1);
	$result['inv_data']			= array();
	$result['inv_data']['databasename']			= $databasename;	
	$result['inv_data']['amount_to_be_inv']=place_currency(display_number($totalNoVat));
	$result['inv_data']['amount_to_be_inv_with_btw']=place_currency(display_number($totalWithVat));
	$result['inv_data']['amount_to_be_inv_nr']= $opened;
	$result['inv_data']['amount_to_be_inv_btw']=place_currency(display_number($totalWithVat-$totalNoVat));
	$result['inv_data']['amount_to_be_inv_nr_late']=$opened_late;
	$result['inv_data']['amount_to_be_inv_late']=place_currency(display_number($totalLateNoVat));
	$result['inv_data']['amount_to_be_inv_late_with_btw']=place_currency(display_number($totalLateWithVat));
	$result['inv_data']['amount_to_be_inv_late_btw']=place_currency(display_number($totalLateWithVat-$totalLateNoVat));
	$result['inv_data']['amount_to_be_inv_month_nr']=$invoice_amount_month_a['c'];
	$result['inv_data']['amount_to_be_inv_month']=place_currency(display_number($invoice_amount_month_a['a']));
	$result['inv_data']['amount_to_be_inv_month_with_btw']=place_currency(display_number($invoice_amount_month_a['av']));
	$result['inv_data']['amount_to_be_inv_month_btw']=place_currency(display_number($invoice_amount_month_a['av']-$invoice_amount_month_a['a']));
	$result['inv_data']['amount_to_be_inv_month_30']=place_currency(display_number($invoice_amount_month_a_30['a']));
	$result['inv_data']['credit_this_month']=place_currency(display_number($credit_amount_month_a['a']));
	$result['inv_data']['credit_this_month_with_btw']=place_currency(display_number($credit_amount_month_a['av']));
	$result['inv_data']['credit_this_month_btw']=place_currency(display_number($credit_amount_month_a['av']-$credit_amount_month_a['a']));
	$result['inv_data']['amount_to_be_inv_last_nr']=$invoice_amount_last_a['c'];
	$result['inv_data']['amount_to_be_inv_last']=place_currency(display_number($invoice_amount_last_a['a']));
	$result['inv_data']['amount_to_be_inv_last_with_btw']=place_currency(display_number($invoice_amount_last_a['av']));
	$result['inv_data']['amount_to_be_inv_last_btw']=place_currency(display_number($invoice_amount_last_a['av']-$invoice_amount_last_a['a']));
	$result['inv_data']['credit_this_last']=place_currency(display_number($credit_amount_last_a['a']));
	$result['inv_data']['credit_this_last_with_btw']=place_currency(display_number($credit_amount_last_a['av']));
	$result['inv_data']['credit_this_last_btw']=place_currency(display_number($credit_amount_last_a['av']-$credit_amount_last_a['a']));
	$result['inv_data']['use_vat_invoices_kpi']			= defined('USE_VAT_INVOICE_KPI') && USE_VAT_INVOICE_KPI == 1 ? true :false;
	
	return $result;
}

	public function get_Amounts($in){
		session_write_close();
	    ini_set('memory_limit','1G');
	    $result=array();

	    $doReturn=false;
	    if(!$in['refresh_data']){
	    	$amount_o=$this->db->field("SELECT value FROM settings WHERE constant_name='AMOUNT_TO_INVOICE_ORDERS' ");
	    	if(!is_null($amount_o)){
	    		$amount_o_data=explode('-',$amount_o);
	    		if(!empty($amount_o_data) && is_array($amount_o_data) && count($amount_o_data)==2){
	    			if($amount_o_data[1]+10800>time()){
	    				$result['amount_o']=place_currency(display_number($amount_o_data[0]));
	    				$doReturn=true;	
	    			} 			
	    		}
	    	}
	    	$total_amount_p=$this->db->field("SELECT value FROM settings WHERE constant_name='AMOUNT_TO_INVOICE_PROJECTS' ");
	    	if(!is_null($total_amount_p)){
	    		$total_amount_p_data=explode('-',$total_amount_p);
	    		if(!empty($total_amount_p_data) && is_array($total_amount_p_data) && count($total_amount_p_data)==2){
	    			if($total_amount_p_data[1]+10800>time()){
	    				$result['total_amount_p']=place_currency(display_number($total_amount_p_data[0]));
	    				$doReturn=true;
	    			}   			
	    		}
	    	}
	    	$amount_s=$this->db->field("SELECT value FROM settings WHERE constant_name='AMOUNT_TO_INVOICE_INTERVENTIONS' ");
	    	if(!is_null($amount_s)){
	    		$amount_s_data=explode('-',$amount_s);
	    		if(!empty($amount_s_data) && is_array($amount_s_data) && count($amount_s_data)==2){
	    			if($amount_s_data[1]+10800>time()){
	    				$result['amount_s']=place_currency(display_number($amount_s_data[0]));
	    				$doReturn=true;
	    			}
	    		}
	    	}
			
		}
		if($doReturn){
			return $result;
		}

	$ALLOW_ARTICLE_PACKING = ALLOW_ARTICLE_PACKING;
	$ALLOW_ARTICLE_SALE_UNIT = ALLOW_ARTICLE_SALE_UNIT;

	#orders
	$is_order = false;
	$order_nr =0;
	$order_total = 0;
	$order_date = 0;

	if(ORDER_DELIVERY_STEPS==2){
	    //AND 	invoiced='0'
	   /* $order_query = $this->db->query("SELECT order_id,rdy_invoice,`date`,currency_rate,discount FROM pim_orders
				WHERE rdy_invoice>'0' AND 	invoiced='0'
				AND active='1'
				AND (
				    SELECT COUNT(pim_order_deliveries.order_id) FROM pim_order_deliveries
				    LEFT JOIN pim_orders_delivery
				    ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
				    WHERE pim_order_deliveries.order_id=pim_orders.order_id
				    AND delivery_done = '1'
				    AND pim_orders_delivery.invoiced = '0'
				    ) > 0   ")->getAll();*/
		$order_query = $this->db->query("SELECT pim_orders.order_id, pim_orders.rdy_invoice, pim_orders.`date` , pim_orders.currency_rate, pim_orders.discount, pim_order_deliveries.delivery_id, pim_order_deliveries.delivery_done
				FROM pim_orders
				LEFT JOIN pim_order_deliveries ON pim_order_deliveries.order_id = pim_orders.order_id
				AND pim_order_deliveries.delivery_done =  '1'
				LEFT JOIN pim_orders_delivery ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
				WHERE pim_orders.rdy_invoice >  '0'
				AND pim_orders.invoiced =  '0'
				AND pim_orders.active =  '1'
				AND pim_orders_delivery.invoiced =  '0'
				GROUP BY pim_orders.order_id ")->getAll();
	}else{
	    $order_query = $this->db->query("SELECT order_id,rdy_invoice,`date`,currency_rate,discount FROM pim_orders WHERE rdy_invoice>'0' AND invoiced='0' AND active='1' ")->getAll();
	}
	$orders_result = array();
	$orders_ids = '';
	foreach ($order_query as $key => $value) {
	    $orders_ids .=$value['order_id'].',';
	    $orders_result[$value['order_id']] = array(
		'order_id' => $value['order_id'],
		'rdy_invoice' => $value['rdy_invoice'],
		'date' => $value['date'],
		'currency_rate' => $value['currency_rate'],
		'discount' => $value['discount']
	    );
	}
	$orders_ids = rtrim($orders_ids,',');
	$order_query = null;
	if($orders_ids){
	    if(ORDER_DELIVERY_STEPS == 2){
		$delivery = $this->db->query("SELECT order_articles_id,quantity,pim_orders_delivery.order_id FROM pim_orders_delivery
				LEFT JOIN pim_order_deliveries
				ON pim_order_deliveries.delivery_id = pim_orders_delivery.delivery_id
				WHERE invoiced='0' AND pim_orders_delivery.order_id in (".$orders_ids.")
				AND delivery_done = '1' ")->getAll();
		// AND pim_orders_delivery.order_id in (".$orders_ids.")
	    }else{
		$delivery = $this->db->query("SELECT order_articles_id,quantity,pim_orders_delivery.order_id FROM pim_orders_delivery WHERE invoiced='0' AND pim_orders_delivery.order_id in (".$orders_ids.")")->getAll();
		//  AND pim_orders_delivery.order_id in (".$orders_ids.")
	    }
	    $delivery_result = array();
	    foreach ($delivery as $key => $value) {
		
		$delivery_result[$value['order_id']][] = array(
		    'order_articles_id' => $value['order_articles_id'],
		    'quantity' => $value['quantity'],
		    'order_id' => $value['order_id']
		);
	    }
	    $delivery = null;
	    $line_result = array();
	    $line = $this->db->query("SELECT order_articles_id,price, discount, pim_order_articles.quantity, packing, sale_unit
	    FROM pim_order_articles	
	    WHERE  content='0' AND pim_order_articles.order_id in (".$orders_ids.") ")->getALl();

	     // AND pim_order_articles.order_id in (".$orders_ids.")
	    foreach ($line as $key => $value) {
		$line_result[$value['order_articles_id']] = array(
		    'price' => $value['price'],
		    'discount' => $value['discount'],
		    'quantity' => $value['quantity'],
		    'packing' => $value['packing'],
		    'sale_unit' => $value['sale_unit']
		    // 'return' => $value['ret']
		);
	    }
	    $line = null;
	    $returns_result = array();
	    $ret = $this->db->query("SELECT SUM( pim_articles_return.quantity ) as ret, pim_order_articles.order_articles_id 
	                      FROM pim_articles_return 
	                      INNER JOIN pim_order_articles ON pim_order_articles.article_id = pim_articles_return.article_id AND pim_order_articles.order_id = pim_articles_return.order_id 
	                      GROUP BY pim_order_articles.order_id, pim_order_articles.order_articles_id")->getAll();
	    foreach ($ret as $key => $value) {
		$line_result[$value['order_articles_id']]['return'] = $value['ret'];
	    }

	    foreach ($orders_result as $key => $value) {
		$amount = 0;
		if(array_key_exists($key, $delivery_result)){
		    foreach ($delivery_result[$key] as $k1 => $v1) {
			$line_price = ($line_result[$v1['order_articles_id']]['price']-($line_result[$v1['order_articles_id']]['price']*$line_result[$v1['order_articles_id']]['discount']/100));
			$amount += ($v1['quantity']-$line_result[$v1['order_articles_id']]['return'])*($line_result[$v1['order_articles_id']]['packing']/$line_result[$v1['order_articles_id']]['sale_unit'])*$line_price;
		    }
		}
		/*if($amount<=0){
	    	continue;
	        }*/

		if(!$order_date && $value['date']){
		    $order_date = $value['date'];
		}elseif ($value['date'] < $order_date && $value['date']) {
		    $order_date = $value['date'];
		}
		$order_nr ++;

		if($value['currency_rate']){
		    $amount = $amount*return_value($value['currency_rate']);
		}

		if($value['discount']!=0.00){
	            $amount = $amount-(($amount*$value['discount']) /100);
	        }
	        
		$order_total += $amount;
		
	    }
	}

	$is_project = false;
	$project_nr =0;
	$project_total = 0;
	$project_date = 0;
	$project_query = $this->db->query("SELECT * FROM projects WHERE billable_type!='4' AND projects.active>'0' AND projects.stage>'0' ");
	// $project_query = $db->query("SELECT * FROM projects WHERE billable_type!='4' ");
	$p_hours=0;
	$p_days=0;
	$amount_from_hours = 0;
	$amount_expenses = 0;
	$nr_projects = array();
	while ($project_query->next()) {
	    $increment = false;
	    $project_amount = 0;

	    if($project_query->f('billable_type') && $project_query->f('invoice_method')){

		if($project_query->f('billable_type') != '5' ){

		    $projects_hours = $this->db->field("SELECT SUM(hours) FROM task_time
			LEFT JOIN projects ON task_time.project_id = projects.project_id
					LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
					WHERE task_time.billable='1' AND task_time.billed='0' AND task_time.approved='1' AND task_time.service = '0'  AND task_time.project_id='".$project_query->f('project_id')."' AND projects.invoice_method>0 AND task_time.project_status_rate='0' AND ( ( (billable_type = '1' or billable_type = '7') AND t_h_rate !='0' ) OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_h_rate!='0' ) )  ");
		    $projects_days = $this->db->field("SELECT SUM(hours) FROM task_time
			LEFT JOIN projects ON task_time.project_id = projects.project_id
					LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
					WHERE task_time.billable='1' AND task_time.billed='0' AND task_time.approved='1' AND task_time.service = '0'  AND task_time.project_id='".$project_query->f('project_id')."' AND task_time.project_status_rate='1' AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_daily_rate !='0' ) OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_daily_rate!='0' ) )  ");
		    if($projects_hours || $projects_days){
			$is_project = true;
			$increment = true;
			if($projects_hours){
			    $p_hours += $projects_hours;
			}
			if($projects_days){
			    $p_days+=$projects_days;
			}
			$filter_hours = " task_time.billable='1' and billable_type != '4' AND billable_type !=  '5' AND task_time.billed='0' AND service = '0' AND projects.active>'0' AND approved='1' AND task_time.project_id='".$project_query->f('project_id')."' ";

			$amount = billableAmount($filter_hours);
			$amount_from_hours+=$amount['total'];

			if(!in_array($project_query->f('project_id'), $nr_projects)){
			    $nr_projects[] = $project_query->f('project_id');
			}

		    }
		}else{
		    $tasks = $this->db->query("SELECT * FROM tasks WHERE project_id='".$project_query->f('project_id')."' AND billable='1' AND closed='1' ");
		    while($tasks->next()){
			$is_project = true;
			$increment = true;
			$project_amount += $tasks->f('task_budget');

			if(!in_array($project_query->f('project_id'), $nr_projects)){
			    $nr_projects[] = $project_query->f('project_id');
			}

		    }
		}
		if($project_query->f('billable_type') == '6'){
		    #do nothing
		    $increment = false;
		    $projects_hours = false;
		    $projects_days=false;
		}
		$join = " LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id ";
		$filter = '';
		$expence_field = ', project_expences.expense_id AS expense_id_2 ';
		if($project_query->f('active') == 2){
		    $join = '';
		    $filter = " AND project_expenses.billable='1' ";
		    $expence_field = '';
		}
		$expenses = $this->db->query("SELECT project_expenses.* , expense.unit_price AS u_p,expense.name, task_time.approved ".$expence_field."
								FROM project_expenses
								LEFT JOIN expense ON project_expenses.expense_id = expense.expense_id
								LEFT JOIN task_time ON project_expenses.project_id = task_time.project_id AND project_expenses.date = task_time.date
								".$join."
								WHERE project_expenses.project_id='".$project_query->f('project_id')."' AND project_expenses.billed !=  '1' AND is_service='0' AND approved = '1' ".$filter);

		while ($expenses->next()) {

		    if( ( $expenses->f('approved') && $expenses->f('expense_id_2') ) || ( !$expenses->f('expense_id_2') && $expenses->f('billable')==1 && $expenses->f('billed') == 0 ) ){
			$increment = true;
			// echo $expenses->f('id');echo "<br>";
			if(!$expenses->f('u_p')){
			    $unit_price = 1;
			}else{
			    $unit_price = $expenses->f('u_p');
			}
			$amount_expenses+= $unit_price*$expenses->f('amount');

			if(!in_array($project_query->f('project_id'), $nr_projects)){
			    $nr_projects[] = $project_query->f('project_id');
			}

		    }

		}
		$purchas = $this->db->query("SELECT * FROM project_purchase WHERE project_id='".$project_query->f('project_id')."' AND delivered='1' AND billable='1' AND invoiced='0' ");
		while($purchas->next()){
		    // echo $purchas->f('project_id').'<br>';
		    $is_project = true;
		    $increment = true;

		    $purchas_quantity = $purchas->f('quantity');
		    if(!$purchas_quantity){
			$purchas_quantity = 1;
		    }
		    $margin_price = ($purchas_quantity*$purchas->f('unit_price'))+ ($purchas_quantity*$purchas->f('unit_price')*$purchas->f('margin')/100);
		    // $margin_price = ($purchas->f('unit_price')*$purchas->f('margin')/100) + $purchas->f('unit_price');
		    $project_amount += $margin_price;
		    if(!in_array($project_query->f('project_id'), $nr_projects)){
			$nr_projects[] = $project_query->f('project_id');
		    }

		}
		$articles = $this->db->query("SELECT SUM(service_delivery.quantity) AS total_quantity,project_articles.price,pim_articles.packing,pim_articles.sale_unit FROM service_delivery
		    INNER JOIN project_articles ON service_delivery.project_id=project_articles.project_id AND service_delivery.a_id=project_articles.article_id
		    LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
		     WHERE service_delivery.project_id='".$project_query->f('project_id')."' AND service_delivery.invoiced='0' GROUP BY service_delivery.a_id");
		while($articles->next()){
		    $is_project = true;
		    $increment = true;
		    $packing_art=($ALLOW_ARTICLE_PACKING && $articles->f('packing')>0) ? $articles->f('packing') : 1;
		    $sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $articles->f('sale_unit')>0) ? $articles->f('sale_unit') : 1;
		    $article_price = $articles->f('total_quantity')*($packing_art/$sale_unit_art)*$articles->f('price');
		    $project_amount += $article_price;
		    if(!in_array($project_query->f('project_id'), $nr_projects)){
			$nr_projects[] = $project_query->f('project_id');
		    }

		}
		if($increment){
		    $project_nr ++;
		    if(!$project_date && $project_query->f('start_date')){
			$project_date = $project_query->f('start_date');
		    }elseif ($project_query->f('start_date') < $project_date && $project_query->f('start_date')) {
			$project_date = $project_query->f('start_date');
		    }
		}

	    }
	    if($project_query->f('currency_rate')){
		$project_amount = $project_amount*return_value($project_query->f('currency_rate'));
	    }

	    $project_total += $project_amount;

	}

	$project_total+=$amount_from_hours+$amount_expenses;

	$total = 0;
	$serv_nr = 0;
	$serv_date = 0;
	$query = $this->db->query("SELECT * FROM servicing_support WHERE status='2' AND active='1' AND billed='0' ");
	while ($query->next()) {
	    $tdisc=0;
	    $ldisc=0;
	    $srv_amount=0;
	    /*if($query->f('customer_id')){
		$cust = $db->query("SELECT apply_fix_disc,apply_line_disc,line_discount,fixed_discount FROM customers WHERE customer_id='".$query->f('customer_id')."' ");
		if($cust->f('apply_fix_disc')==1){
		    $tdisc=$cust->f('fixed_discount');
		}
		if($cust->f('apply_line_disc')==1){
		    $ldisc=$cust->f('line_discount');
		}
	    }*/
	    if($query->f('billable')){
			if(!$query->f('service_type')){
			    $hours = $this->db->field("SELECT SUM(end_time-start_time-break) FROM servicing_support_sheet WHERE service_id='".$query->f('service_id')."' ");
			    if($hours){
				$hoursPerUser = $this->db->field("SELECT SUM(end_time-start_time-break) as hour FROM servicing_support_sheet WHERE service_id='".$query->f('service_id')."' ");
				if ($hoursPerUser) {
				    $srv_amount += $hoursPerUser*$query->f('rate')-($hoursPerUser*$query->f('rate')*$ldisc/100);
				}
				$increment = true;
			    }
			}else{
			    $int_tasks=$this->db->field("SELECT SUM(task_budget) FROM servicing_support_tasks WHERE service_id='".$query->f('service_id')."' AND article_id='0' ");
			    $srv_amount+=$int_tasks;
			    $increment = true;
			}
			$int_services=$this->db->query("SELECT quantity,task_budget FROM servicing_support_tasks WHERE service_id='".$query->f('service_id')."' AND article_id!='0'");
		    while($int_services->next()){
		    	$srv_amount+=($int_services->f('quantity')*$int_services->f('task_budget'));
		    	$increment=true;
		    }		
	    }	    

	    $purchase = $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$query->f('service_id')."' AND delivered<>0 AND billable='1' ");
	    while ($purchase->next()) {
		$increment = true;
		$srv_amount += (($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'))*$purchase->f('delivered'))-(($purchase->f('price')*$purchase->f('margin')/100+$purchase->f('price'))*$purchase->f('delivered')*$ldisc/100);
	    }
	    $article = $this->db->query("SELECT SUM(service_delivery.quantity) AS total_quantity,servicing_support_articles.price, pim_articles.packing, pim_articles.sale_unit FROM service_delivery
		LEFT JOIN pim_articles ON service_delivery.a_id=pim_articles.article_id
		INNER JOIN servicing_support_articles ON service_delivery.service_id=servicing_support_articles.service_id AND service_delivery.a_id=servicing_support_articles.article_id
		WHERE service_delivery.service_id='".$query->f('service_id')."' AND service_delivery.invoiced='0' AND servicing_support_articles.billable='1' AND servicing_support_articles.article_id!='0' GROUP BY service_delivery.a_id");
	    while ($article->next()) {
		$increment = true;
		$packing_art=($ALLOW_ARTICLE_PACKING && $article->f('packing')>0) ? $article->f('packing') : 1;
		$sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $article->f('sale_unit')>0) ? $article->f('sale_unit') : 1;
		$article_init=$article->f('total_quantity')*($packing_art/$sale_unit_art)*$article->f('price');
		$srv_amount += ($article_init-($article_init*$ldisc/100));
	    }

	    $expense = $this->db->query("SELECT project_expenses.*, expense.name AS e_name, expense.unit_price, expense.unit, servicing_support.customer_id as customer_id
				    FROM project_expenses
				    INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
				    INNER JOIN servicing_support ON project_expenses.service_id=servicing_support.service_id
				    WHERE servicing_support.service_id='".$query->f('service_id')."' AND project_expenses.billable='1' ORDER BY id");
	    while ($expense->next()) {
		$amount = $expense->f('amount');
		if($expense->f('unit_price')){
		    $amount = $expense->f('amount') * $expense->f('unit_price');
		}
		$srv_amount += $amount;
		$increment = true;
	    }
	    $downpayment_int=$this->db->query("SELECT tblinvoice_line.* FROM tbldownpayments 	
		INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
		INNER JOIN tblinvoice_line ON tblinvoice.id=tblinvoice_line.invoice_id
		WHERE tbldownpayments.service_id='".$query->f('service_id')."' AND tblinvoice.f_archived='0' ");
	    while($downpayment_int->next()){
		$srv_amount -= ($downpayment_int->f('quantity')*$downpayment_int->f('price'));
		$increment = true;
	    }

	    if($increment===true){
		$serv_nr++;
		if(!$serv_date && $query->f('planeddate')){
		    $serv_date = $query->f('planeddate');
		}elseif ($query->f('planeddate') < $serv_date && $query->f('planeddate')) {
		    $serv_date = $query->f('planeddate');
		}
	    }

	    $srv_amount = ($srv_amount - $srv_amount*$tdisc/100);
	    $total += $srv_amount;
	}

	$result['amount_o']=place_currency(display_number($order_total));
	$result['total_amount_p']=place_currency(display_number($project_total));
	$result['amount_s']=place_currency(display_number($total));
	$current_time=time();
	$amount_o_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='AMOUNT_TO_INVOICE_ORDERS' ");
	if(is_null($amount_o_exist)){
		$this->db->query("INSERT INTO settings SET constant_name='AMOUNT_TO_INVOICE_ORDERS',value='".($order_total.'-'.$current_time)."' ");
	}else{
		$this->db->query("UPDATE settings SET value='".($order_total.'-'.$current_time)."' WHERE constant_name='AMOUNT_TO_INVOICE_ORDERS' ");
	}
	$total_amount_p_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='AMOUNT_TO_INVOICE_PROJECTS' ");
	if(is_null($total_amount_p_exist)){
		$this->db->query("INSERT INTO settings SET constant_name='AMOUNT_TO_INVOICE_PROJECTS',value='".($project_total.'-'.$current_time)."' ");
	}else{
		$this->db->query("UPDATE settings SET value='".($project_total.'-'.$current_time)."' WHERE constant_name='AMOUNT_TO_INVOICE_PROJECTS' ");
	}
	$amount_s_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='AMOUNT_TO_INVOICE_INTERVENTIONS' ");
	if(is_null($amount_s_exist)){
		$this->db->query("INSERT INTO settings SET constant_name='AMOUNT_TO_INVOICE_INTERVENTIONS',value='".($total.'-'.$current_time)."' ");
	}else{
		$this->db->query("UPDATE settings SET value='".($total.'-'.$current_time)."' WHERE constant_name='AMOUNT_TO_INVOICE_INTERVENTIONS' ");
	}

	//update pdfs when done from yuki cron
	include_once(__DIR__.'/../model/invoice.php');
	$invModel = new invoice();
	$yuki_invoices=$this->db_users->query("SELECT * FROM cron_yuki WHERE `database_name`='".DATABASE_NAME."' AND `done`='0' GROUP BY invoice_id ");
	while($yuki_invoices->next()){
		$this->db_users->query("UPDATE cron_yuki SET `done`='1' WHERE invoice_id='".$yuki_invoices->f('invoice_id')."' AND done='0' AND `database_name`='".DATABASE_NAME."' ");
		$inv = $this->db->query("SELECT * FROM tblinvoice WHERE id = '".$yuki_invoices->f('invoice_id')."' ");
	    $params = array();
	    $params['use_custom'] = 0;
	    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
	      $params['logo'] = $inv->f('pdf_logo');
	      $params['type']=$inv->f('pdf_layout');
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
	      $params['custom_type']=$inv->f('pdf_layout');
	      unset($params['type']);
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	      $params['use_custom'] = 1;
	    }else{
	    $params['type']= ACCOUNT_INVOICE_BODY_PDF_FORMAT;
	    $params['template_type'] =ACCOUNT_INVOICE_BODY_PDF_FORMAT;
	    }
	    #if we are using a customer pdf template
	    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
	      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
	      unset($params['type']);
	    }
	    $params['id'] = $yuki_invoices->f('invoice_id');
	    $params['lid'] = $inv->f('email_language');
	    $params['type_pdf'] =  $inv->f('type');
	    $params['save_as'] = 'F';
	    $invModel->generate_pdf($params);
	}

	return $result;
    }

    public function get_invoices_cols(){
		$db=new sqldb();
		$array=array();
		$cols_default=default_columns_dd(array('list'=>'invoices'));
		$cols_order_dd=default_columns_order_dd(array('list'=>'invoices'));
		$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='invoices' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
		if(!count($cols_selected)){
			$i=1;
            $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'invoices'));
            foreach($cols_default as $key=>$value){
				$tmp_item=array(
					'id'				=> $i,
					'column_name'		=> $key,
					'name'				=> $value,
					'order_by'			=> $cols_order_dd[$key]
				);
                if ($default_selected_columns_dd[$key]) {
                    array_push($array,$tmp_item);
                }
				$i++;
			}
		}else{
			foreach($cols_selected as $key=>$value){
				$tmp_item=array(
					'id'				=> $value['id'],
					'column_name'		=> $value['column_name'],
					'name'				=> $cols_default[$value['column_name']],
					'order_by'			=> $cols_order_dd[$value['column_name']]
				);
				array_push($array,$tmp_item);
			}
		}
		return $array;
	}
}

    $inv_list = new InvoicesList($in,$db);

    if($in['xget']){
        $fname = 'get_'.$in['xget'];
        $inv_list->output($inv_list->$fname($in));
    }

    $inv_list->get_List();
    $inv_list->output();

json_out($result);
?>