<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array('vats'=>array());

/*$vats = $db->query("SELECT vats.vat_id as id,vats.value AS v , yukivats.value as y FROM vats
					LEFT JOIN yukivats ON vats.vat_id=yukivats.vat_id
					ORDER BY vats.value");

while($vats->next()){

  $item=array(
    'VAT_VALUE'     => display_number($vats->f('v')),
    'id'		        => $vats->f('id'),
    'd'			        => build_yuki_dd($vats->f('y')),
    'selected'      => $vats->f('y') ? $vats->f('y') : '0',
  );
  if(ACCOUNT_DELIVERY_COUNTRY_ID=='26'){
    array_push($item['d'],array('id'=>'21','name'=>'Middle VAT - 12%'));
  }
  array_push($result['vats'], $item);
}

$regim = $db->query("SELECT vatregim . * , yukivats.value AS y
					FROM vatregim
					LEFT JOIN yukivats ON vatregim.id = yukivats.vat_id
					ORDER BY vatregim.value");

while ($regim->next()) {
  if($regim->f('value') == 'Regular'){
    continue;
  }
  $item1=array(
    'VAT_VALUE' 	  => $regim->f('value'),
    'id'			      => $regim->f('id'),
    'd'			        => build_yuki_dd($regim->f('y')),
    'selected'      => $regim->f('y') ? $regim->f('y') : '0',
  );
  if(ACCOUNT_DELIVERY_COUNTRY_ID=='26'){
    array_push($item1['d'],array('id'=>'21','name'=>'Middle VAT - 12%'));
  }
  array_push($result['vats'], $item1);
}*/
$regime_type=array(
      '1'   => gm("Regular"),
      '2'   => gm('Intra-EU'),
      '3'   => gm("Export"),
    );
$vats=$db->query("SELECT vat_new.*  FROM vat_new
          ORDER BY vat_new.name_id ASC");
while($vats->next()){
  $selected=$db->field("SELECT value FROM yukivats WHERE vat_id='".$vats->f('id')."' ");
  if($vats->f('vat_id')){
    if(!$selected){
      $selected=$db->field("SELECT value FROM yukivats WHERE vat_id='".$vats->f('vat_id')."' ");
    }
    $value=$db->field("SELECT value FROM vats WHERE vat_id='".$vats->f('vat_id')."' ");
  }else if($vats->f('vat_regime_id')){
    if(!$selected){
      $selected=$db->field("SELECT value FROM yukivats WHERE vat_id='r".$vats->f('vat_regime_id')."' ");
    }
    $value=0;
  }else if($selected){
    $value=0;
  }else{
    $selected='0';
    $value=0;
  }
  if(!$selected){
    $selected='0';
  }

  $item=array(
    'VAT_VALUE'     => $vats->f('description'),
    'value'         => $value,
    'regime_type'   => $regime_type[$vats->f('regime_type')],
    'id'            => $vats->f('id'),
    'd'             => build_yuki_dd(),
    'selected'      => $selected,
  );
  if(ACCOUNT_DELIVERY_COUNTRY_ID=='26'){
    array_push($item['d'],array('id'=>'21','name'=>'Middle VAT - 12%'));
  }
  array_push($result['vats'], $item);
}
$result['ledger_account']=build_article_ledger_account_dd();
$ledger_account=$db->field("SELECT value FROM settings WHERE constant_name='YUKI_LEDGER_ID' ");
//$result['is_admin']= $_SESSION['access_level'] == 1 ? true : false;
$result['ledger_account_id']=$ledger_account;
$result['ledger_name']='YUKI_LEDGER_ID';
$result['app']=$in['app'];
$result['title']=gm('Yuki settings');
$result['do_next']='invoice--invoice-yukiVats';
json_out($result);