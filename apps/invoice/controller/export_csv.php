<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 
global $config;

$result=array('fields'=>array(),'lines'=>array(),'custom_fields'=>array());
if($in['list_id'] == 'tmp'){
	$result['show_headers'] 	= $in['show_headers'] ? true : false;
	$result['convert'] 		= $in['convert'] ? true : false;
	if(!$in['delimit_er']){
		$result['delimit_er'] = ';';
	}
	#type 1 is for invoice fields
	$invoice_fields=$db->query("SELECT * FROM tblinvoice_fields WHERE type='1' ");
	while ($invoice_fields->next()) {
		$inv_fields=array(
			'name'		=> $db->f('name'),
			'checked'		=> $db->f('value') == 1 ? true : false,
			'header'		=> $db->f('header'),
			'id'			=> $db->f('field_id'),
		);
		array_push($result['fields'], $inv_fields);
		$i++;
	}
	#type 3 is for special fields
	$special_fields=$db->query("SELECT * FROM tblinvoice_fields WHERE type='3' ");
	while ($special_fields->next()) {
		$sp_fields=array(
			'name'		=> $db->f('name'),
			'checked'		=> $db->f('value') == 1 ? true : false,
			'header'		=> $db->f('header'),
			'id'			=> $db->f('field_id'),
		);
		array_push($result['fields'], $sp_fields);
		$i++;
	}
	#type 2 is for invoice line fields
	$line_fields=$db->query("SELECT * FROM tblinvoice_fields WHERE type='2' ");
	while ($line_fields->next()) {
		$l_fields=array(
			'name'		=> $db->f('name'),
			'checked'		=> $db->f('value') == 1 ? true : false,
			'header'		=> $db->f('header'),
			'id'			=> $db->f('field_id'),
		);
		array_push($result['lines'], $l_fields);	
	}
	$do_next = 'invoice-export_csv-invoice-save_export';
}else{
	$do_next = 'invoice-export_csv-invoice-update_export';
	$list = $db->query("SELECT * FROM tblinvoice_export_list WHERE list_id='".$in['list_id']."' ");
	
	$result['name'] 		= $list->f('name');
	$result['delimit_er'] 	= $list->f('delimit_er');
	$result['show_headers'] = $list->f('show_headers') ? true : false;
	$result['convert'] 	= $list->f('convert') ? true : false;
	$result['list_id']	= $in['list_id'];

	$invoice_fields=$db->query("SELECT tblinvoice_field.*, tblinvoice_fields.name  FROM tblinvoice_field 
				INNER JOIN tblinvoice_fields ON tblinvoice_field.field_id=tblinvoice_fields.field_id
				WHERE (tblinvoice_fields.type='1' OR tblinvoice_fields.type='3') AND list_id='".$in['list_id']."' ORDER BY sort_order  ");
	while ($invoice_fields->next()) {
		$inv_fields=array(
			'name'		=> $db->f('name'),
			'checked'		=> $db->f('value') == 1 ? true : false,
			'header'		=> $db->f('header'),
			'id'			=> $db->f('field_id'),
		);
		array_push($result['fields'], $inv_fields);
	}

	$line_fields=$db->query("SELECT tblinvoice_field.*, tblinvoice_fields.name FROM tblinvoice_field
				INNER JOIN tblinvoice_fields ON tblinvoice_field.field_id=tblinvoice_fields.field_id
				WHERE type='2' AND list_id='".$in['list_id']."' ORDER BY sort_order  ");
	while ($line_fields->next()) {
		$l_fields=array(
			'name'		=> $db->f('name'),
			'checked'		=> $db->f('value') == 1 ? true : false,
			'header'		=> $db->f('header'),
			'id'			=> $db->f('field_id'),
		);
		array_push($result['lines'], $l_fields);			
	}

	$custom_fields=$db->query("SELECT * FROM tblinvoice_field WHERE list_id='".$in['list_id']."' AND custom='1' ORDER BY sort_order ");
	while ($custom_fields->next()) {
		$c_fields=array(
			'header'		=> $db->f('header'),
			'value'		=> $db->f('value'),
		);
		array_push($result['custom_fields'], $c_fields);	
	}
}

	$result['do_next']		= $do_next;

json_out($result);
?>