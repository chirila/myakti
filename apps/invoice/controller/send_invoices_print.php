<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$special_template = array(4,5,6); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

$db = new sqldb();
// echo "<pre>";
// print_r($in['invoice_ids']);
// exit();
// $invoices_id = explode(',', $in['invoices_id']);
foreach ($in['invoice_ids'] as $key => $value) {

if(in_array($in['type'], $special_template) || ($in['custom_type'] == 1 && DATABASE_NAME == '6522a2ec_2904_313e_fd99d29e57b4')){

	/*
	$pdf->SetMargins(10, 5, 10);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, 50);
	$pdf->setPrintFooter(false);
	$hide_all = 2;
	*/

	if($in['type']==6){
		$pdf->SetMargins(6, 43, 10);
		$pdf->SetAutoPageBreak(TRUE, 10);
	}else{
		$pdf->SetMargins(10, 5, 10);
		$pdf->SetAutoPageBreak(TRUE, 50);
	}
	$pdf->SetFooterMargin(0);
	$pdf->setPrintFooter(false);
	$hide_all = 2;


}

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

	unset($in['logo']);
	$in['id']= $value;
	$is_table = true;
	$is_data = false;
	$height = 0;
	$pdf_layout = $db->field("SELECT pdf_layout FROM tblinvoice WHERE id = '".$in['id']."' ");
	if($pdf_layout){
		$in['type'] = $pdf_layout;
	}
	$pdf_logo = $db->field("SELECT pdf_logo FROM tblinvoice WHERE id = '".$in['id']."' ");
	if($pdf_logo){
		$in['logo'] = $pdf_logo;
	}
	$lid = $db->field("SELECT email_language FROM tblinvoice WHERE id = '".$in['id']."' ");
	if($lid){
		$in['lid'] = $lid;
	}

	/*
	if(in_array($in['type'], $special_template) || ($in['custom_type'] == 1 && DATABASE_NAME == '6522a2ec_2904_313e_fd99d29e57b4') ){
		$pdf->SetAutoPageBreak(TRUE, 0);
	}
	*/
    if (!empty($in['include_pdf'])) {
	    if($in['reminder']){
	        unset($in['reminder']);
        }
        $pdf->startPageGroup();
        $pdf->AddPage();
        $html = include('invoice_print_body.php');
        $pdf->writeHTML($html, true, false, true, false, '');
	}

	#250 vat si discount
	#255 numai discount
	#267 fara vat si discount
	$pdf->SetY($height);
	if(in_array($in['type'], $special_template) || ($in['custom_type'] == 1 && DATABASE_NAME == '6522a2ec_2904_313e_fd99d29e57b4') ){
		$hide_all = 1;
		$pdf->SetAutoPageBreak(TRUE, 0);

		$htmls = include('invoice_print_body.php');

		$pdf->writeHTML($htmls, true, false, true, false, '');
	}

}
$pdf->lastPage();
$pdf->Output(__DIR__.'/../../../invoice_.pdf', 'F');
// exit();
?>