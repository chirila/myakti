<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');
$in['invoices_id'] ='';
if($_SESSION['add_to_xls']){
  foreach ($_SESSION['add_to_xls'] as $key => $value) {
    $in['invoices_id'] .= $key.',';
  }
}
if(isset($_SESSION['tmp_add_to_xls'])){
    unset($_SESSION['tmp_add_to_xls']);
}
if(isset($_SESSION['add_to_xls'])){
    unset($_SESSION['add_to_xls']);
}
$in['invoices_id'] = rtrim($in['invoices_id'],',');
if(!$in['invoices_id']){
  $in['invoices_id']= '0';
}
/*require_once 'libraries/PHPExcel.php';
$filename ="export_recinvoices_list.xls";

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Invoices list")
							 ->setSubject("Invoices list")
							 ->setDescription("Export invoices list")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Invoices");*/
$db = new sqldb();




$info = $db->query("SELECT  *, cast( FROM_UNIXTIME( `start_date` ) AS DATE ) AS t_date
                    FROM recurring_invoice
                    WHERE recurring_invoice_id IN (".$in['invoices_id'].")
                    ORDER BY t_date DESC ");


/*$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1',"CUSTOMER NAME")
            ->setCellValue('B1',"NEXT INVOICE DATE")
            ->setCellValue('C1',"FREQUENCY")
            ->setCellValue('D1',"SUBJECT")
            ->setCellValue('E1',"VALUE")
            ->setCellValue('F1',"CONTRACT REFERENCE")
            ->setCellValue('G1',"START DATE")
            ->setCellValue('H1',"END DATE")
            ->setCellValue('I1',"MRR")
            ->setCellValue('J1',"PAYMENT METHOD")
            ->setCellValue('K1',"DELIVERY");         

   $xlsRow=2;*/
   $headers=array(
          "CUSTOMER NAME",
          "NEXT INVOICE DATE",
          "FREQUENCY",
          "SUBJECT",
          "VALUE",
          "CONTRACT REFERENCE",
          "START DATE",
          "END DATE",
          "MRR",
          "PAYMENT METHOD",
          "DELIVERY"
   );
   $filename ="export_recinvoices_list.csv";

  $final_data=array();

while ($info->next()){

  $next_date = date(ACCOUNT_DATE_FORMAT,  $info->f('next_date'));

  $start_date = date(ACCOUNT_DATE_FORMAT,  $info->f('start_date'));

  if(!$info->f('end_date') || !is_numeric($info->f('end_date'))) {
    $end_date = '';
  }else{
    $end_date = date(ACCOUNT_DATE_FORMAT,  $info->f('end_date'));
  }

  $frequency='';
  switch ($info->f('frequency')){
        case 1:  
          $frequency= gm('Weekly'); 
          $mrr_invoice=$info->f('amount')>0 ? $info->f('amount')*52/12 : 0;
          break;
        case 2:  
          $frequency= gm('Monthly'); 
          $mrr_invoice=$info->f('amount');
          break;
        case 3:  
          $frequency= gm('Quarterly'); 
          $mrr_invoice=$info->f('amount')>0 ? $info->f('amount')/3 : 0;
          break;
        case 4:  
          $frequency= gm('Yearly'); 
          $mrr_invoice=$info->f('amount')>0 ? $info->f('amount')/12 : 0;
          break;
        case 5:  
          $frequency= gm('Every').' '.$info->f('days').' '.gm('Day'); 
          $mrr_invoice=$info->f('amount')>0 ? $info->f('amount')*365/($info->f('days')*12) : 0;
          break;
  }

  $payment_method='';
  switch ($info->f('payment_method')){
        case 0:  
          $payment_method= gm('Unspecified'); 
          break;
        case 1:  
          $payment_method= gm('Direct Transfer'); 
          break;
        case 2:  
          $payment_method= gm('Stripe'); 
          break;
        case 3:  
          $payment_method= gm('SEPA'); 
          break;
  }
  $delivery='';
  switch ($info->f('invoice_delivery')){
        case 0:  
          $delivery= gm('Save invoice as draft'); 
          $send_to='';
          break;
        case 1:  
          $delivery= gm('Send invoice via email'); 
          $send_to='';
          $recipients_data=$db->query("SELECT email FROM recurring_email_contact WHERE recurring_invoice_id='".$info->f('recurring_invoice_id')."' ");
          while($recipients_data->next()){
            $send_to.=$recipients_data->f('email').';';
          }
          $send_to=rtrim($send_to,";");
          $delivery.= ': '.$send_to;
          break;     
  }


  // company vat number  , phone, invoice email

  $company_vat_nr = '';
  $phone = '';
  $invoice_email = '';
  $buyer_name='';
  if($info->f('buyer_id')){
    $company_data = $db->query("SELECT btw_nr,invoice_email,comp_phone,name FROM customers WHERE customer_id = '".$info->f('buyer_id')."' ");
    while($company_data->next()){
      $company_vat_nr = $company_data->f('btw_nr');
      $phone = $company_data->f('comp_phone');
      $invoice_email = $company_data->f('invoice_email');
      $buyer_name=$company_data->f('name');
    }
  }
  if(!$info->f('buyer_id') && $info->f('contact_id')){
    $contact_data = $db->query("SELECT email, phone FROM customer_contacts WHERE contact_id = '".$info->f('contact_id')."' ");
    while($contact_data->next()){
      $phone = $contact_data->f('phone');
      $invoice_email = $contact_data->f('email');
      $buyer_name=$info->f('buyer_name');
    }
  }

  //company address
  if($info->f('free_field')){
    $company_address = $info->f('free_field');
  }
  else{
    if($info->f('buyer_id')){
      $buyer_details = $db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$info->f('buyer_id')."' AND billing = 1 ");
    }
    if($info->f('contact_id')){
      $buyer_details = $db->query("SELECT * FROM customer_contact_address WHERE contact_id = '".$info->f('contact_id')."' ");
    }

    $company_address = $buyer_details->f('address').'
'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
'.get_country_name($buyer_details->f('country_id'));

  }

  $nr_of_decimals = ARTICLE_PRICE_COMMA_DIGITS;
  $contract_reference = $info->f('contract_id') ? $info->f('our_ref') : '';
/*
  $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, stripslashes($buyer_name))
              ->setCellValue('B'.$xlsRow, $next_date)
  	          ->setCellValue('C'.$xlsRow, $frequency)
              ->setCellValue('D'.$xlsRow, preg_replace("/[\n\r]/","",$info->f('title')))
              ->setCellValue('E'.$xlsRow, number_format($info->f('amount'), $nr_of_decimals, '.', ''))
  			      ->setCellValue('F'.$xlsRow, preg_replace("/[\n\r]/","",$contract_reference))
              ->setCellValue('G'.$xlsRow, $start_date)
              ->setCellValue('H'.$xlsRow, $end_date)
              ->setCellValue('I'.$xlsRow,  number_format($mrr_invoice, 2, '.', ''))
              ->setCellValue('J'.$xlsRow, $payment_method)
              ->setCellValue('K'.$xlsRow, $delivery);
  $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
  $xlsRow++;*/
  $item=array(
    stripslashes($buyer_name),
    $next_date,
    $frequency,
    preg_replace("/[\n\r]/","",$info->f('title')),
    display_number_exclude_thousand($info->f('amount')),
    preg_replace("/[\n\r]/","",$contract_reference),
    $start_date,
    $end_date,
    display_number_exclude_thousand($mrr_invoice),
    $payment_method,
    $delivery
  );
  array_push($final_data,$item);
}
header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$fp = fopen("php://output", 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
exit();

// set column width to auto
foreach(range('A','K') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getStyle('F1:F'.$xlsRow)->getAlignment()->setWrapText(true);
$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->setTitle('Recurring invoices list export');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.$filename);





define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>