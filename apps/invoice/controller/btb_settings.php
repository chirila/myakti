<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array('vats'=>array());

$result['ledger_account']=build_article_ledger_account_dd();
$ledger_account=$db->field("SELECT value FROM settings WHERE constant_name='BTB_LEDGER_ID' ");
//$result['is_admin']= $_SESSION['access_level'] == 1 ? true : false;
$result['ledger_account_id']=$ledger_account;
$result['ledger_name']='BTB_LEDGER_ID';
$result['title']=gm('E-FFF settings');
$result['app']=$in['app'];
json_out($result);