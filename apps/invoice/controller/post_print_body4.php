<?php

session_start();
$view = new at(ark::$viewpath.'post_print_4.html');
	$db =  new sqldb();

	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);

	if($_SESSION['main_u_id']==0){
		//$user_data=$db_users->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_data=$db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	}else{
		//$user_data=$db_users->query("SELECT * FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
		$user_data=$db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['main_u_id']]);
	}

	$view->assign(array(
		'usr_name'		=> $user_data->f('last_name'),
		'usr_surname'	=> $user_data->f('first_name'),
		'usr_phone'		=> $user_data->f('phone'),
		'usr_email'		=> $user_data->f('email'),
		'usr_address'	=> $user_data->f('ACCOUNT_BILLING_ADDRESS'),
		'usr_zip'		=> $user_data->f('ACCOUNT_BILLING_ZIP'),
		'usr_city'		=> $user_data->f('ACCOUNT_BILLING_CITY'),
		'usr_country'	=> get_country_name($user_data->f('ACCOUNT_BILLING_COUNTRY_ID')),
		'lang_en'		=> $acc_lang=='en' ? true : false
	));

return $view->fetch();
?>