<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $config;

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('PostGreen Registration');
$pdf->SetSubject('PostGreen Registration');

$pdf->SetFont('dejavusans', '', 9, '', false);

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(8, 6, 13, 0);
$pdf->SetFontSize(10);

$pdf->SetAutoPageBreak(false, 0);

if($_SESSION['l']=='fr'){
	$acc_lang='fr';
	$loop_nr=11;
}else{
	$acc_lang='en';
	$loop_nr=10;
}

for($i=1;$i<$loop_nr;$i++){
	$pdf->AddPage();
	$pdf->Image('images/pdf/pgn'.$i.'_'.$acc_lang.'.jpg', 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
	if($i==2){
		$html = include('post_print_body2.php');
		$pdf->writeHTML($html, true, false, true, false, '');
	}else if($i==4){
		$html = include('post_print_body4.php');
		$pdf->writeHTML($html, true, false, true, false, '');
	}
}
doQueryLog();
$pdf->Output('postgreen.pdf','D');

?>
