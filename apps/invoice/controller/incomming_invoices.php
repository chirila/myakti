<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

$result=array();
$l_r = ROW_PER_PAGE;

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

$arguments_o='';
$arguments_s='';
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$arguments='';
$filter = "WHERE 1=1 ";
$filter_link = 'block';
$order_by = " ORDER BY  iii DESC ";


if(!empty($in['filter'])){
	$arguments.="&filter=".$in['filter'];
}
if(!empty($in['search'])){
	$filter.=" AND (tblinvoice_incomming.invoice_number LIKE '%".$in['search']."%' OR tblinvoice_incomming.booking_number LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['order_by'])){
	$order = " ASC ";
	if($in['desc']){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
}

if(!isset($in['view'])){
	$in['view'] = 0;
}

if($in['view'] == 1){
	$filter.=" and tblinvoice_incomming.paid = '0' and tblinvoice_incomming.type != '2' ";
	$arguments.="&view=1";
	
}
if($in['view'] == 2){
	$filter.=" and tblinvoice_incomming.paid = '0' and tblinvoice_incomming.due_date<'".time()."' ";
	$arguments.="&view=2";
	
}
if($in['view'] == 3){
	$filter.=" and tblinvoice_incomming.paid = '1'";
	$arguments.="&view=3";
	
}
if($in['view'] == 4){
	$filter.=" and tblinvoice_incomming.type = '2'";
	$arguments.="&view=4";
	
}

// }
if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
if(!empty($in['start_date']) && !empty($in['stop_date'])){
    $filter.=" and tblinvoice_incomming.invoice_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if(!empty($in['start_date'])){
	$filter.=" and cast(tblinvoice_incomming.invoice_date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if(!empty($in['stop_date'])){
	$filter.=" and cast(tblinvoice_incomming.invoice_date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}


$arguments = $arguments.$arguments_s;
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;


$tblinvoice = $db->query("SELECT tblinvoice_incomming.*,customers.name, booking_number as iii, 
	cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date
			FROM tblinvoice_incomming
			LEFT JOIN customers ON tblinvoice_incomming.supplier_id = customers.customer_id ".$filter.$order_by );



$max_rows=$tblinvoice->records_count();
$result = array('query'=>array(),'max_rows'=>$max_rows);
$tblinvoice->move_to($offset*$l_r);
$j=0;
$color = '';
$all_invoices_id = '';


while($tblinvoice->move_next() && $j<$l_r){
	
	$status='';
	$status_title='';
	$type='';
	$type_title='';
	$color='';
	$partial_paid=$db->field("SELECT payment_id FROM tblinvoice_incomming_payments WHERE invoice_id='".$tblinvoice->f('invoice_id')."'");
	switch ($tblinvoice->f('type')){
		case '0':
			$type = 'regular_invoice';
			$type_title = gm('Regular invoice');
			break;
		case '1':
			$type = 'pro-forma_invoice';
			$type_title = gm('Proforma invoice');
			break;
		case '2':
			$type = 'credit_invoice';
			$type_title = gm('Credit Invoice');
			break;
	}

	switch ($tblinvoice->f('paid')){
		case '0':
					// $status = 'Not Paid';
					$status='';
					$status_title= gm('Not Paid');
					$color = '';
			if($tblinvoice->f('due_date') < time()){
					// $status = 'Late';
					$status = 'late';
					$status_title= gm('Late');
					$color = 'late';
             }elseif($partial_paid){
			        $status='partial';
					$status_title= gm('Partially Paid');
					$color = '';
					$class='';
				}
			break;
		case '1':
			// $status = 'partial Paid';
				$color = '';
				$status='paid';
				$status_title= gm('Paid');
			break;
		/*case '2':
			// $status = 'Fully Paid';
			if($tblinvoice->f('type') != '2' ){
				$color = '';
				$status='paid';
				$status_title= gm('Paid');
			}
			break;*/
	}
	$customer=$db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('supplier_id')."'");
	if(!$tblinvoice->f('supplier_id')){
		$customer=$db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id ='".$tblinvoice->f('contact_id')."'");
	}

	if($tblinvoice->f('created_by') == 'System'){
		$color .= ' recurring';
	}

	$item=array(
		
		
		'info_link'     		=> 'index.php?do=invoice-incomming_invoice&invoice_id='.$tblinvoice->f('invoice_id'),
		'edit_link'     		=> 'index.php?do=invoice-ninvoice&invoice_id='.$tblinvoice->f('invoice_id'),
		
	
	   'delete_link'  			=>array('do'=>'invoice-incomming_invoices-invoice-incomming_invoices_delete', 'id'=>$tblinvoice->f('invoice_id')),
		
		
		'can_activate'			=> $tblinvoice->f('f_archived')==1 ? true : false,
		'can_pay'				=> $tblinvoice->f('paid') == 1? false:true,
		
		'type'					=> $type,
		'type_title'			=> $type_title,
		'status'				=> $status,
		'status_title'			=> $status_title,
		'invoice_property'		=> $color,
		'amount'				=> place_currency(display_number($tblinvoice->f('total'))),
		'amount_vat'			=> place_currency(display_number($tblinvoice->f('total_with_vat'))),
		'our_reference'			=> $tblinvoice->f('our_ref'),
		'id'                	=> $tblinvoice->f('id'),
		's_number'		     	=> $tblinvoice->f('invoice_number'),
		'b_number'		     	=> $tblinvoice->f('booking_number'),
		'created'           	=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
		'seller_name'       	=> $tblinvoice->f('seller_name'),
        'i_number'              => $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
		'buyer_name'        	=> $customer,
		
		
		'invoice_id'			=> $tblinvoice->f('invoice_id'),
		'id'			=> $tblinvoice->f('invoice_id'),

		'check_add_to_product'	=> $_SESSION['add_to_purchase'][$tblinvoice->f('invoice_id')] == 1 ? 'CHECKED' : '',
	);
	
    array_push($result['query'], $item);

	$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('invoice_id');
	
	$j++;

}
$all_invoices_id = trim($all_invoices_id,',');



$args_v = preg_replace('/&view=([0-9]+)/','',$arguments);

$default_lang_id = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
$default_pdf_type = ACCOUNT_INVOICE_PDF_FORMAT;


	$result['view_go'] 	= $_SESSION['access_level'] == 1 ? '' : 'hide';
	$result['args']	    = $args_v;
	$result['lid']      = $default_lang_id;
	$result['pdf_type']	= $default_pdf_type;
	$result['all_invoices_id']=$all_invoices_id;
	$result['lr'] = $l_r;


$_SESSION['filters'] = $arguments.$arguments_o;




json_out($result);