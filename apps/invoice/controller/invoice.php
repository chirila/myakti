<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $config;

$o=array();
/*$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);*/
$db2 = new sqldb();
$db3 = new sqldb();
$tips = $page_tip->getTip('invoice-invoice');
if(!$in['invoice_id'])
{
	/*page_redirect('index.php?do=invoice-to_invoice');*/
	$o['redirect']='to_invoice';
}

$invoice_exists = $db->field("SELECT id FROM tblinvoice WHERE id='".$in['invoice_id']."' ");

if(!$invoice_exists){
	msg::error('Invoice does not exist','error');
	$in['item_exists']= false;
    json_out($in);
}

$negative = 1;
if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
	$negative = -1;
}

$tblinvoice = $db->query("SELECT * FROM tblinvoice WHERE id='".$in['invoice_id']."'");
if(!$tblinvoice->move_next()){
	return ark::run('invoice-to_invoice');
	// page_redirect('index.php?do=invoice-to_invoice');
}
$o['item_exists']=true;

$currency_rate = $tblinvoice->f('currency_rate');
$discount = $tblinvoice->f('discount');
$in['i_date']=$tblinvoice->f('invoice_date');
$o['i_date']=$tblinvoice->f('invoice_date');

$vat = $tblinvoice->f('vat');

if($tblinvoice->f('type')=='2' || $tblinvoice->f('type')=='4'){
	$page_title= gm('Credit Invoice');
}
else{
	if($tblinvoice->f('type')=='1'){
		$page_title= gm('Proforma Invoice');
	}else{
		$page_title= gm('Invoice Information');
	}
}

$factur_date = date(ACCOUNT_DATE_FORMAT,  $tblinvoice->f('invoice_date'));
$invoice_d_date = $tblinvoice->f('invoice_date');
$currency_type = $tblinvoice->f('currency_type');
$currency = get_commission_type_list($tblinvoice->f('currency_type'));
$serial_number = $tblinvoice->f('serial_number');
$invoice_status = $tblinvoice->f('status');
$invoice_sent = $tblinvoice->f('sent');

if(!$invoice_sent){
	$o['sh_payment_receive']=false;
}
ark::run('invoice--invoice-external_id');
switch ($tblinvoice->f('status')){
	case '0':
		if($tblinvoice->f('sent') == '0'){
			$status = 'draft';
			$status2 = gm('Draft');
		}
		else{

			if($tblinvoice->f('paid') == '2' ){
				$status = 'partial';
				$status2 = gm('Partially Paid');
			}elseif($tblinvoice->f('paid') == '1' ){
				$status = 'fully';
				$status2 = gm('Paid');
			}else {
				$status = 'final';
				$status2 = gm('Final');
			}//console::log($tblinvoice->f('due_date'),time());
			if($tblinvoice->f('due_date') < time() && $tblinvoice->f('type')!='2' && $tblinvoice->f('type')!='4'){
				// credit invoice can not be late
				$status = 'late';
				$status2 = gm('Late');
			}
		}
		$fully_paid= 0;
		break;
	case '1':
		// ark::run('invoice--invoice-external_id');
		if($tblinvoice->f('sent') == '0'){
			$status = 'draft';
			$status2 = gm('Draft');
		}else{
			($tblinvoice->f('type')=='2' || $tblinvoice->f('type')=='4')? $status = 'final':$status = 'fully';
			($tblinvoice->f('type')=='2' || $tblinvoice->f('type')=='4')? $status2 = gm('Final'):$status2 = gm('Paid');
		}
		$fully_paid= 1;
		break;
	default:
		$status = 'draft';
		$status2 = gm('Draft');
		$fully_paid= 0;
		break;
}
$buyer_id=$tblinvoice->f('buyer_id');
$contact_id = $tblinvoice->f('contact_id');
if($db->f('contact_id') && !$buyer_id)
{
	$contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$db->f('contact_id')."'");
}
if($contact_title)
{
	$contact_title .= " ";
}
$in['type'] = $tblinvoice->f('type');
$o['type'] = $tblinvoice->f('type');
$remove_vat = $tblinvoice->f('remove_vat');
$free_field = '<p>'.$tblinvoice->f('buyer_address').'</p><p>'.$tblinvoice->f('buyer_zip').' '.$tblinvoice->f('buyer_city').'</p><p>'.get_country_name($tblinvoice->f('buyer_country_id')).'</p>';

$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'invoice'
								   AND item_name = 'notes'
								   And active = '1'
								   AND lang_id = '".$tblinvoice->f('email_language')."'
								   AND item_id = '".$in['invoice_id']."' ");
$l_code = '';
if($_SESSION['l']=='fr'||$_SESSION['l']=='nl'||$_SESSION['l']=='de'){
	$l_code = '-'.$_SESSION['l'];
}
if($tblinvoice->f('type')==1){
	$generate_number_invoice=generate_invoice_number(DATABASE_NAME);
}elseif($tblinvoice->f('type')==2 || $tblinvoice->f('type')==4){
	$generate_number_invoice=generate_invoice_number(DATABASE_NAME);
}else{
	$generate_number_invoice=$tblinvoice->f('serial_number');
}

	$contact_email = '';
	$contact_phone = '';

if($tblinvoice->f('contact_id') && $tblinvoice->f('buyer_id')){
	$contact = $db3->query(" SELECT email, phone FROM customer_contactsIds	WHERE contact_id='".$tblinvoice->f('contact_id')."' and customer_id = '".$tblinvoice->f('buyer_id')."'");
	$contact_email = $contact->f('email');
	$contact_phone = $contact->f('phone');
}
if($tblinvoice->f('contact_id')){
	if(!$contact_email){
		$contact_email = $db3->field(" SELECT email FROM customer_contacts	WHERE contact_id='".$tblinvoice->f('contact_id')."' ");
	}
	if(!$contact_phone){
		$contact_phone = $db3->field(" SELECT cell FROM customer_contacts	WHERE contact_id='".$tblinvoice->f('contact_id')."' ");
	}
}

$deal_nr_row = array();

$deal_nr = $db->query("SELECT tracking . * , tracking_line . * , tblopportunity.serial_number,tblopportunity_stage.name AS stage_name
						FROM tracking_line
						LEFT JOIN tracking ON tracking_line.trace_id = tracking.trace_id
						LEFT JOIN tblopportunity ON tracking_line.origin_id = tblopportunity.opportunity_id
						LEFT JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
						WHERE tracking.target_id =  '".$in['invoice_id']."' AND tracking.target_type IN ('1','7','8') AND tracking_line.origin_type =  '11'");
while($deal_nr->move_next()){
    	array_push($deal_nr_row, array( 'serial' => $deal_nr->f('serial_number'), 'id'=> $deal_nr->f('origin_id'), 'stage_name'=>$deal_nr->f('stage_name') ) );
    
	}
	
	$o['orig_deal']									= $deal_nr_row; 
	$o['fully_paid']											= $fully_paid;
	$o['st']													= $status;
	$o['bg'] 													= $status2;
	$o['sent_dated']									= date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('sent_date'));
	$o['hide_sentd']									= $tblinvoice->f('sent_date') ? true:false;
	$o['check_acc_manager']							= $tblinvoice->f('acc_manager_id')!=0 ? true:false;
	$o['hide_proforma'] 							= $tblinvoice->f('serial_number')=='' ? false : true;
	$o['hide_serial']   							= $tblinvoice->f('serial_number')=='' ? true : false;
	$o['hide_sent']     							= $tblinvoice->f('sent')==0? true:false;
	$o['hide_unsent']   							= $tblinvoice->f('sent')==0? false: true;
	$o['type_credit']   							= ($tblinvoice->f('type')==2 || $tblinvoice->f('type')=='4')? false: true;
	$o['credit_type']   							= ($tblinvoice->f('type')==3 )? '4': '2';
	$o['factur_nr']									= $tblinvoice->f('serial_number') ? $tblinvoice->f('serial_number'): '';
	$o['factur_date'] 								= $factur_date;
	$o['invoice_d_date']							= $invoice_d_date * 1000;	
	$o['due_date']										= date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('due_date'));
//	'delete_link'   				=> get_link('index.php?pag=invoices&act=invoice-delete&id='.$db->f('id')),
	$o['draft_link']  	 							= 'index.php?do=invoice-invoice-invoice-draft&invoice_id='.$tblinvoice->f('id').'&type='.$tblinvoice->f('type').'&credit_invoice_id='.$tblinvoice->f('c_invoice_id');
	$o['draft_link2']  	 							= 'index.php?do=invoice-invoice-invoice-draft&invoice_id='.$tblinvoice->f('id').'&type='.$tblinvoice->f('type').'&credit_invoice_id='.$tblinvoice->f('c_invoice_id').'&sent=1';
	$o['duplicate_link']    					= 'index.php?do=invoice-ninvoice&duplicate_invoice_id='.$tblinvoice->f('id').'&buyer_id='.$tblinvoice->f('buyer_id');
	$o['recurring_link']   					 	= 'index.php?do=invoice-recurring_ninvoice&invoice_id='.$tblinvoice->f('id');
	$o['credit_link']   			 				= 'index.php?do=invoice-ninvoice&c_invoice_id='.$tblinvoice->f('id').'&type=2&a_invoice_id='.$tblinvoice->f('serial_number').'&base_type=0&languages='.$tblinvoice->f('email_language');
	$o['serial_number1']				=$tblinvoice->f('serial_number');
	$o['invoice_contact_name']       		= get_contact_name($tblinvoice->f('contact_id'));
	//$o['contact_email']       		= $db3->field(" SELECT email FROM customer_contacts	WHERE contact_id='".$tblinvoice->f('contact_id')."' ");
	//$o['contact_phone']       		= $db3->field(" SELECT cell FROM customer_contacts	WHERE contact_id='".$tblinvoice->f('contact_id')."' ");
	$o['contact_email']       			= $contact_email;
	$o['contact_phone']       			= $contact_phone;
	$o['invoice_buyer_name']       		= $contact_title.$tblinvoice->f('buyer_name');
	$o['invoice_buyer_country']    		= get_country_name($tblinvoice->f('buyer_country_id'));
	$o['invoice_buyer_state']      		= get_state_name($tblinvoice->f('buyer_state_id'));
	$o['invoice_buyer_city']       		= $tblinvoice->f('buyer_city');
	$o['invoice_buyer_zip']        		= $tblinvoice->f('buyer_zip');
	$o['invoice_buyer_address']    		= nl2br($tblinvoice->f('buyer_address'));
	$o['invoice_buyer_vat']		 	  	= $tblinvoice->f('seller_bwt_nr');
	$o['invoice_seller_name']        	= $tblinvoice->f('seller_name');
	$o['invoice_seller_d_country']   	= get_country_name($tblinvoice->f('seller_d_country_id'));
//	'invoice_seller_d_state'     		=> get_state_name($db->f('seller_d_state_id')),
	$o['invoice_seller_d_city']      	= $tblinvoice->f('seller_d_city');
	$o['invoice_seller_d_zip']       	= $tblinvoice->f('seller_d_zip');
	$o['invoice_seller_d_address']   	= $tblinvoice->f('seller_d_address');
	$o['invoice_seller_b_country']   	= get_country_name($tblinvoice->f('seller_b_country_id'));
	$o['invoice_seller_b_country_id']   = ACCOUNT_DELIVERY_COUNTRY_ID;
	$o['invoice_seller_b_country_dd']	= build_country_list(ACCOUNT_DELIVERY_COUNTRY_ID);
//	'invoice_seller_b_state'   		=> get_state_name($db->f('seller_b_state_id')),
	$o['invoice_seller_b_city']      	= ACCOUNT_DELIVERY_CITY;
	$o['invoice_seller_b_zip']       	= ACCOUNT_DELIVERY_ZIP;
	$o['invoice_seller_b_address']   	= ACCOUNT_DELIVERY_ADDRESS;
	$o['invoice_vat_no']             	= ACCOUNT_VAT_NUMBER;
	$o['invoice_seller_iban']		= ACCOUNT_IBAN;
	$o['invoice_seller_swift']		= ACCOUNT_BIC_CODE;
	$o['invoice_seller_email']		= ACCOUNT_EMAIL;
	//$o['down_notes']									 		 	= nl2br($notes);
	//$o['notes2']											=nl2br(utf8_decode($tblinvoice->f('notes2')));
	$o['down_notes']									 		 	= stripslashes($notes);
	$o['notes2']											=stripslashes(utf8_decode($tblinvoice->f('notes2')));
//	'hide_sync'				  					=> $status == 'draft' ? '':'hide',
	$o['hide_vat']									 	= $remove_vat ? false : true;
	$o['hide_notes']									= $notes == '' ? false : true;
	$o['hide_notes2']									= $tblinvoice->f('notes2') == '' ? false : true;
	$o['serial_number']								= $generate_number_invoice;
	$o['our_ref']											= $tblinvoice->f('our_ref');
	$o['your_ref']										= $tblinvoice->f('your_ref');
	$o['buyer_id']										= $buyer_id;
	//$o['free_field']									= $tblinvoice->f('free_field') ? '<p style="line-height: 17px;">'.nl2br($tblinvoice->f('free_field')).'</p>' : $free_field;
	$o['free_field']									= $tblinvoice->f('free_field') ? '<p style="line-height: 17px;">'.nl2br($tblinvoice->f('free_field')).'</p>' : $free_field;
	$o['invoice_label']								= ($tblinvoice->f('type')== 2 || $tblinvoice->f('type')== 4)? gm('Credit Note Nr:') : ($tblinvoice->f('type')== 1 ? gm('Proforma') : gm('Invoice Nr').':');
	$o['no_credit']										= ($tblinvoice->f('type')== 2 || $tblinvoice->f('type')== 4)? false : true;
	$o['red_if_credit']								= ($tblinvoice->f('type')== 2  || $tblinvoice->f('type')== 4)? true : false;
	$o['buyer_email']									= $tblinvoice->f('buyer_email');
	
	//do no change these - akti - 3441 (fix in the document itself)
	/*$o['buyer_phone']       					= $tblinvoice->f('buyer_phone') ? $tblinvoice->f('buyer_phone') : $db3->field(" SELECT comp_phone FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."' ");
	$o['buyer_fax']       						= $tblinvoice->f('buyer_fax') ? $tblinvoice->f('buyer_fax') : $db3->field(" SELECT comp_fax FROM customers	WHERE customer_id='".$tblinvoice->f('buyer_id')."' ");*/
	$o['buyer_phone']       					= $tblinvoice->f('buyer_phone');
	$o['buyer_fax']       						= $tblinvoice->f('buyer_fax') ;
	//end

	$o['hide_disc_line']							= ($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2) ? false : true;
	$o['apply_discount']							= $tblinvoice->f('apply_discount');
	$o['no_vat']								= $tblinvoice->f('remove_vat') == 1 ? false : true;
	//$o['selectedDraft']								= $status == 'draft' ? true : false;
	$o['selectedDraft']								= $tblinvoice->f('sent') == 1 ? false : true;
	$o['selectedSent']								= $status != 'draft' && $status != 'fully' && $status != 'partial' && $tblinvoice->f('paid')!=1 ? true : false;
	//$o['selectedWon']								= $status == 'fully' || ($status=='final' && $tblinvoice->f('paid')==1 )? true : false;
	$o['selectedWon']								= ($status == 'fully' || $status == 'partial') || ($status=='final' && $tblinvoice->f('paid')==1 )? true : false;
	//$o['draft']												= $status == 'draft' ? true :false;
	$o['draft']												=$tblinvoice->f('sent') == 1 ? false : true;
	$o['fully']												= $status != 'draft' && $status != 'fully' ? true : false;
	$o['payed']												= $status == 'fully' ? true : false;
	$o['view_pform']									= false;
	$o['is_sent']											= $tblinvoice->f('sent') == 1 ? true : false;
	// 'print'												=> ark::run('invoice-invoice_print_web'),
	$o['is_proforma']									= $tblinvoice->f('type')==1 ? true : false;

	$o['show_due_date_vat_pdf']	      = (SHOW_DUE_DATE_VAT_PDF == 1 && intval($tblinvoice->f('due_date_vat')) > 86400) ? true : false;
	$o['invoice_due_date_vat'] 		= intval($tblinvoice->f('due_date_vat')) > 86400 ? date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('due_date_vat')) : '';
	$o['use_ogm']						= defined('USE_OGM') && USE_OGM == 1 && $tblinvoice->f('ogm') ? true : false;
	$o['ogm']							= $tblinvoice->f('ogm');
	$o['is_from_service']				= $tblinvoice->f('service_id') ? true : false;
	$o['is_credit']				= ($tblinvoice->f('type')=='2' || $tblinvoice->f('type')== 4)? true:false;
	$o['check_not_paid']    = $tblinvoice->f('not_paid')==1? true : false;
	$o['is_ckecked']        = $tblinvoice->f('not_paid')==1? true : false ;
	$o['disable_next']	= $tblinvoice->f('not_paid')==1? 'disable_next' : '';
	$o['languages']		= $tblinvoice->f('email_language');
	$o['language_dd']		= build_language_dd_new($tblinvoice->f('email_language'));
	$o['bpaid']			= ($tblinvoice->f('not_paid') == '0' && $tblinvoice->f('status') < '2'  && $tblinvoice->f('sent') == '1' && $tblinvoice->f('due_date') < time() ) ? true : false;
	$o['edebex_status']  = $tblinvoice->f('edebex_id') !='' ? true : false;
	$o['xml_available']  = ACCOUNT_DELIVERY_COUNTRY_ID=='26' ? true : false;
	$o['block_edit']		= $tblinvoice->f('block_edit') ? true : false;
	$o['cost_centre']		=$tblinvoice->f('cost_centre');
	$has_admin_rights 				= getHasAdminRights(array('module'=>'invoice'));
	$o['hide_margin']				= defined('HIDE_PROFIT_MARGIN') && HIDE_PROFIT_MARGIN == 1 && !$has_admin_rights ? true : false; 
	$o['paid']				=$tblinvoice->f('paid');
	$o['subject_field']		= stripslashes($tblinvoice->f('subject'));

/*if($tblinvoice->f('sent') == '0' && $tblinvoice->f('type')!='2'){*/
if($tblinvoice->f('sent') == '0' ){
    $type_title = gm('Draft');
    $nr_status=1;
}else if($tblinvoice->f('status')=='1' || ($tblinvoice->f('status')=='0' && $tblinvoice->f('paid')=='2' ) ){
    $type_title = $tblinvoice->f('paid')=='2' ? gm('Partially Paid') : gm('Paid');
    $nr_status=3;
}else if($tblinvoice->f('sent')!='0' && $tblinvoice->f('not_paid')=='0'){
	$nr_status=$tblinvoice->f('status')=='0' && $tblinvoice->f('due_date')< time() && $tblinvoice->f('type')!='2' && $tblinvoice->f('type')!='4' ? 4 : 2;

	if($tblinvoice->f('status')=='0' && $tblinvoice->f('due_date')< time() && $tblinvoice->f('type')!='2' && $tblinvoice->f('type')!='4'){
		$type_title =  gm('Late');
	}else{
		$is_easy_invoice_diy=aktiUser::get('is_easy_invoice_diy');
		$type_title =gm('Final');
		if($is_easy_invoice_diy){
			$jefacture_exported=$db->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$tblinvoice->f('id')."' AND jefacture='1' AND `type`='0' ");
			if($jefacture_exported){
				$type_title = gm('Exported');
				$nr_status=3;
			}
		}
	}    
} else if($tblinvoice->f('sent')!='0' && $tblinvoice->f('not_paid')=='1'){
    $type_title = gm('No Payment');
    $nr_status=2;
}
if($tblinvoice->f('f_archived')=='1'){
	$type_title = gm('Archived');
	$nr_status=1;
}
$o['type_title']=$type_title;
$o['nr_status']=$nr_status;

if(!$buyer_id){
	if($tblinvoice->f('buyer_email')){
		$o['contact_email']=$tblinvoice->f('buyer_email');
	}
}
if($tblinvoice->f('discount') == 0){
	$discount_procent=0;
} else {
	$discount_procent = $tblinvoice->f('discount');
	// $view->assign('total_discount_procent',display_number($tblinvoice->f('discount')),'vat_line');
}

if($tblinvoice->f('apply_discount') < 2){
	$discount_procent = 0;
}

if($tblinvoice->f('req_payment') == 100){
	$o['view_req_payment']=false;

} else {
	$o['view_req_payment']=true;
}
$payments_nr=0;
//payments_info
$payments_info = $db->query("SELECT info,payment_id,payment_method,amount,`date` FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' ORDER BY payment_id ASC ");

$o['payments_row']=array();
while($payments_info->move_next()){

	switch ($payments_info->f('payment_method')) {
		case '0':
			$payment_method='';
			break;
	    case '1':
				$payment_method = '- '.gm("Unspecified");
			break;	
	    case '2':
				$payment_method = '- '.gm("ElectronicTransfer"); 
			break;	
	    case '3':
				$payment_method = '- '.gm("DirectCollection");
			break;	
     	case '4':
				$payment_method = '- '.gm("Cash"); 
			break;						
		case '5':
				$payment_method = '- '.gm("DebitCard");
			break;
		case '6':
			$payment_method = '- '.gm("CreditCard");
			break;
		case '7':
			$payment_method = '- '.gm("ReceivedElectronically");
			break;
		case '8':
			$payment_method = '- '.gm("ReceivedCash");
			break;
		case '9':
			$payment_method = '- '.gm("ToSettle");
			break;
		case '10':
			$payment_method = '- '.gm("iDeal");
			break;
		case '11':
			$payment_method = '- '.gm("Online");
			break;
		case '13':
			$payment_method = '- '.gm("Paypal");
			break;
		case '14':
			$payment_method = '- '.gm("Credit invoice");
			break;
		default:
				$payment_method = '';
			break;
	}

	$link=false;
	$link_id=0;
	$payment_data=trim($payments_info->f('info'),"-").' '.$payment_method;
	if(strpos($payments_info->f('info'), 'href')){
		$link=true;
		$inv_lb_eq=strpos($payments_info->f('info'), 'invoice_id');
		$str_w_nr=substr($payments_info->f('info'), $inv_lb_eq+1);
		$link_id=(int) preg_replace('/\D/', '', $str_w_nr);
		$cn_serial_n=$db->field("SELECT serial_number FROM tblinvoice WHERE id='".$link_id."' ");
		$explode = explode('-', strip_tags($payments_info->f('info')));
		$payment_notes = ' -' . $explode[1];
		$payment_data= date(ACCOUNT_DATE_FORMAT,strtotime($payments_info->f('date'))).' - '.get_currency_sign(display_number($payments_info->f('amount'))).' - '.$cn_serial_n;
	}
	$payments_row=array(
		'payment_info'	=> $payment_data,
		'payment_notes' => $payment_notes,
		'link'		=> $link,
		'id'			=> $link_id,
		'del_payment_link'			=> 'index.php?do=invoice-invoice-invoice-delete_payment_invoice&payment_id='.$payments_info->f('payment_id').'&invoice_id='.$in['invoice_id'],
		'payment_id' => $payments_info->f('payment_id'),
		);
	array_push($o['payments_row'], $payments_row);

	$payments_nr++;
}
if($payments_nr){
	$o['is_payment']=true;
}else{
	$o['is_payment']=false;
}

$mail_setting_option = $db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
$o['sendgrid_selected'] = false;
if($mail_setting_option==3){
	$o['sendgrid_selected'] =true;
}

//PAID
if($tblinvoice->f('paid') == 1){
	$subtotal_vat = 0;
	$total_n = 0;
	$netto_amount=0;
	$subtotal=0;
	// $show_disc = false;
	$lines = $db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
	while ($lines->next()) {
		$line_discount = $lines->f('discount');
		if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
			$line_discount = 0;
		}
		/*if($lines->f('discount')){
			$amount_discount = $lines->f('amount')*$lines->f('discount')/100;
			// $show_disc = true;
		}else{
			$amount_discount = $lines->f('amount')*$discount_procent/100;
		}*/
		// $amount_line = $lines->f('amount') - $lines->f('amount') * $line_discount / 100;
		$amount_line = $lines->f('quantity') *($lines->f('price') - ( $lines->f('price') * $line_discount / 100 ) );
		$amount_line = round($amount_line,(int)ARTICLE_PRICE_COMMA_DIGITS);
		$amount_line_disc = $amount_line*$discount_procent/100;
		$discount_value += $amount_line_disc;
		$netto_amount+=(round($amount_line,2)-$amount_line_disc);
		$subtotal_vat += ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
		$subtotal+=$amount_line;
		$total_n += $amount_line - $amount_line_disc;
	}

	// $sub_total = $total_n;
	// if($discount_procent != 0 || $show_disc === true){
	if($discount_procent != 0){
		$o['show_disc']=true;
	}
	$total_n=round($total_n,2);
	$subtotal_vat=round($subtotal_vat,2);
	if($tblinvoice->f('quote_id') && $tblinvoice->f('downpayment_drawn')){
		$total = $total_n+$subtotal_vat-$tblinvoice->f('downpayment_value');
	}else{
		$total = $total_n+$subtotal_vat;
	}
	

    $req_payment_value=$total* $tblinvoice->f('req_payment')/100;
	if($tblinvoice->f('req_payment') != 100){
		$o['sh_pay']=false;
	}

	//select last payment
	$date = $db->query("SELECT date FROM tblinvoice_payments WHERE invoice_id='".$in['invoice_id']."' ORDER BY date DESC LIMIT 1");
	$date->move_next();

    if($date->f('date')){
	   $payment_date = date(ACCOUNT_DATE_FORMAT, strtotime($date->f('date')));
	}else{    //proforma fully paid
        $date = $db->query("SELECT date FROM tblinvoice_payments WHERE invoice_id='".$tblinvoice->f('proforma_id')."' ORDER BY date DESC LIMIT 1");
	    $date->move_next();
        $payment_date = date(ACCOUNT_DATE_FORMAT, strtotime($date->f('date')));
	}

	$total_default = $total * return_value($currency_rate);

		$o['req_payment']				= 100 - $tblinvoice->f('req_payment');
		$o['s_paid']					= true;
		/*$o['e_paid']					= '';*/
		$o['s_notpaid']					= false;
		/*$o['e_notpaid']					= '-->';*/
		$o['paid_observation']			= $tblinvoice->f('type')==1?'Proforma Paid in full on'.' '.$payment_date:'Invoice Paid in full on'.' '.$payment_date;
		$o['currency']					= $currency;
		//'sub_total'					=> place_currency(display_number($sub_total),$currency),
		//'total_sum'					=> place_currency(display_number($tblinvoice->f('type')==1?0: $total),$currency),
		$o['total_sum']			        = place_currency(display_number(0),$currency);
		$o['total_amount_due']			= place_currency(display_number(0),$currency);
		$o['total_amount_due2']			= place_currency(display_number($total),$currency);
		$o['total_amount_due2_default']	= place_currency(display_number($total_default));
		$o['total_amount_due1']			= display_number(0);
		$o['cost']			= display_number(0);
		$o['total_payments']			= place_currency(display_number(0),$currency);
		$o['sh_edit']					= false;
		$o['req_payment_value']         = place_currency(display_number($total-$req_payment_value),$currency);
		$o['is_not_credit']				= ($tblinvoice->f('type')=='2' ||  $tblinvoice->f('type')== 4)? false:true;
		$o['is_credit']				= ($tblinvoice->f('type')=='2' ||  $tblinvoice->f('type')== 4)? true:false;
		$o['downpayment']		= ($tblinvoice->f('quote_id')&&$tblinvoice->f('downpayment_drawn')) ? true : false;
		$o['total_downpayment']	= ($tblinvoice->f('quote_id')&&$tblinvoice->f('downpayment_drawn')) ? place_currency(display_number($tblinvoice->f('downpayment_value')),$currency) : '';


}//NOT PAID / NOT FULLY PAID
else {
	$subtotal_vat = 0;
	$total_n = 0;
	$netto_amount=0;
	$subtotal=0;
	$show_disc = false;
	$lines = $db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' ");
	while ($lines->next()) {
		$line_discount = $lines->f('discount');
		if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
			$line_discount = 0;
		}
		/*if($lines->f('discount')){
			$amount_discount = $lines->f('amount')*$lines->f('discount')/100;
			$show_disc = true;
		}else{
			$amount_discount = $lines->f('amount')*$discount_procent/100;
		}
		$discount_value += $amount_discount;
		$subtotal_vat += ($lines->f('amount')-$amount_discount) * ( $lines->f('vat') / 100);
		$total_n += $lines->f('amount')-$amount_discount;*/

		$amount_line = $lines->f('quantity') *($lines->f('price') - ( $lines->f('price') * $line_discount / 100 ) );
		// $q * ( $price - ( $price * $line_discount / 100 ) );
		$amount_line = round($amount_line,(int)ARTICLE_PRICE_COMMA_DIGITS);
		

		$amount_line_disc = $amount_line*$discount_procent/100;
		$discount_value += $amount_line_disc;
		$netto_amount+=(round($amount_line,2)-$amount_line_disc);
		$subtotal_vat +=  ( $amount_line - $amount_line_disc ) * $lines->f('vat') / 100;
		$subtotal+=$amount_line;
		$total_n += $amount_line - $amount_line_disc;
	}

	$total_n=round($total_n,2);
	$subtotal_vat=round($subtotal_vat,2);
	// $sub_total = $total_n;
	if($discount_procent != 0 || $show_disc === true){
		$o['show_disc']=true;
	}
// console::log($total_n,$subtotal_vat);
	if($tblinvoice->f('quote_id') && $tblinvoice->f('downpayment_drawn')){
		$total = $total_n+$subtotal_vat-$tblinvoice->f('downpayment_value');
	}else{
		$total = $total_n+$subtotal_vat;
	}
// console::log($total);
	//already payed
	$proforma_id=$db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
    $payments_filter=" invoice_id='".$in['invoice_id']."'";
    if($proforma_id){
      $payments_filter.=" OR invoice_id='".$proforma_id."'";
    }

	$already_payed1 = $db->query("SELECT SUM(ROUND(cast(amount as decimal(12, 4))+0.0012,2)) AS total_payed FROM tblinvoice_payments WHERE (".$payments_filter.") AND credit_payment='0' ");
	$already_payed1->move_next();

	$already_payed2 = $db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE (".$payments_filter.") AND credit_payment='1' ");
	$already_payed2->move_next();


	$total_payed = $already_payed1->f('total_payed')+($negative*$already_payed2->f('total_payed'));

	$req_payment_value=$total* $tblinvoice->f('req_payment')/100;
	if($tblinvoice->f('req_payment') == 100){
		$amount_due = round($total - $total_payed,2);
	}else{
		$amount_due = round($req_payment_value - $total_payed ,2);
	}
//console::log($already_payed1->f('total_payed'), $already_payed2->f('total_payed'));
	// SHOW/HIDE PAYMENT FORM
	if($in['act'] == 'invoice-invoice-invoice-pay'){
		$o['sh_payment_receive']=false;
	}else {
		$o['sh_payment_form']=false;
	}

	if($invoice_status == 1){
		$o['sh_edit']=false;
	}
	else {
		if($invoice_sent == 1){
			$o['sh_edit']=false;
		}
	}

	if(!$in['payment_date']){
		$in['payment_date'] = time()*1000;

	}else {
		$in['payment_date'] = is_numeric($in['payment_date']) ? $in['payment_date']*1000 : strtotime($in['payment_date'])*1000;
	}

	
	$total_default = $total*return_value($currency_rate);

	$o['req_payment']=$tblinvoice->f('req_payment');

	$o['s_paid']					=	false;
/*	$o['e_paid']					=	'-->';*/
	$o['s_notpaid']					=	true;
/*	$o['e_notpaid']					=	'';*/
	$o['currency']					= $currency;
	$o['total_payments']			= place_currency(display_number($amount_due),$currency);
	$o['payment_date']  			= $in['payment_date'];
	$o['p_date']  					= time();
	$o['total_amount_due']			= place_currency(display_number($amount_due),$currency);
	$o['total_amount_due1']			= display_number($amount_due);
	$o['cost']			= display_number(0);
	$o['total_amount_due2']			= place_currency(display_number($total),$currency);
	$o['total_bpaid']				= $total;
	$o['total_amount_due2_default']	= place_currency(display_number($total_default));
	$o['vat']						= $vat;
	$o['sh_pay']					= $total_payed ? true : false;
	$o['is_not_credit']				= ($tblinvoice->f('type')=='2' || $tblinvoice->f('type')== 4)? false:true;
    	$o['is_credit']				= ($tblinvoice->f('type')=='2' || $tblinvoice->f('type')== 4)? true:false;
	$o['req_payment_value']         = place_currency(display_number($req_payment_value-$total_payed),$currency);
	$o['downpayment']		= ($tblinvoice->f('quote_id')&&$tblinvoice->f('downpayment_drawn')) ? true : false;
	$o['total_downpayment']	= ($tblinvoice->f('quote_id')&&$tblinvoice->f('downpayment_drawn')) ? place_currency(display_number($tblinvoice->f('downpayment_value')),$currency) : '';

	if($tblinvoice->f('req_payment') != 100){
		$o['sh_pay']=false;
	}
}

//GET invoice rows
$get_invoice_rows = $db->query("SELECT * FROM tblinvoice_line WHERE invoice_id='".$in['invoice_id']."' order by sort_order ASC ");

$o['invoice_row']=array();
$i = 0;
$total_amount = 0;
$amount_purchase =0;
$vat_percent = array();
$sub_t = array();
$sub_disc = array();
$vat_percent_val = 0;
$is_profi=false;
$margin = 0;
while ($get_invoice_rows->move_next()){
	$line_d = $get_invoice_rows->f('discount');
	 if(return_value($get_invoice_rows->f('purchase_price')) && USE_PROFI==1) {
             $is_profi=true;
      }
	if($tblinvoice->f('apply_discount') ==0 || $tblinvoice->f('apply_discount') == 2){
		$line_d = 0;
	}
	// $amount = $get_invoice_rows->f('amount') - $get_invoice_rows->f('amount') * $line_d /100;
	$amount = $get_invoice_rows->f('quantity') *($get_invoice_rows->f('price') - ( $get_invoice_rows->f('price') * $line_d / 100 ) );
	$line_margin=$amount-($get_invoice_rows->f('purchase_price')*$get_invoice_rows->f('quantity'));

	$amount = round($amount,ARTICLE_PRICE_COMMA_DIGITS);
	$amount_purchase += $get_invoice_rows->f('purchase_price')*$get_invoice_rows->f('quantity') ;

	if($get_invoice_rows->f('amount')>0 && !$get_invoice_rows->f('tax_id')){
	    $margin+=$line_margin;
	    $total_amount_for_margin += $get_invoice_rows->f('amount');	
	}

	$nr_dec_q = strlen(rtrim(substr(strrchr($get_invoice_rows->f('quantity'), "."), 1),'0'));

	$invoice_row=array(
		'row_nr'				=> $i+1,
		'row_description'		=> nl2br(stripslashes(htmlspecialchars_decode($get_invoice_rows->f('name'),ENT_QUOTES))),
		'row_unitmeasure'		=> $get_invoice_rows->f('unitmeasure'),
		//'row_quantity'			=> display_number($get_invoice_rows->f('quantity')),
		'row_quantity'			=> display_number($get_invoice_rows->f('quantity'),$nr_dec_q),
		'row_unit_price'		=> display_number_var_dec($get_invoice_rows->f('price')),
		'row_amount'			=> place_currency(display_number($amount),$currency),
		'vat'					=> display_number($get_invoice_rows->f('vat')),
		'disc_line'				=> display_number($get_invoice_rows->f('discount')),
		'content'               => $get_invoice_rows->f('content') ? true : false,
		'title'                 => $get_invoice_rows->f('content_title'),
		'colspan'			    => $get_invoice_rows->f('content') ? 'colspan="7" class="first-of-type  last_nocenter"' : '',
		'article_code' 		    => stripslashes($get_invoice_rows->f('item_code')),
		'show_for_articles' 	=> $get_invoice_rows->f('item_code') ? true : false,
		'has_variants' 			=> $get_invoice_rows->f('has_variants'),
	 	'has_variants_done' 	=> $get_invoice_rows->f('has_variants_done'),
	 	'is_variant_for' 		=> $get_invoice_rows->f('is_variant_for'),
	 	'variant_type' 			=> $get_invoice_rows->f('variant_type'), 
	 	'is_combined' 			=> $get_invoice_rows->f('is_combined'),
	 	'component_for' 		=> $get_invoice_rows->f('component_for')? $get_invoice_rows->f('component_for'):'',
	 	'visible' 				=> $get_invoice_rows->f('visible'), 
	);

	$total_amount += $get_invoice_rows->f('amount');
	$i++;
	array_push($o['invoice_row'], $invoice_row);
	/*$view->loop('invoice_row');*/

	/*if($get_invoice_rows->f('discount')){
		$amount_d = $get_invoice_rows->f('amount') * $get_invoice_rows->f('discount')/100;
	}else{*/
	$amount_d = $amount * $discount/100;
	if($tblinvoice->f('apply_discount') < 2){
		$amount_d = 0;
	}
	// }
	$vat_percent_val = ($amount - $amount_d) * $get_invoice_rows->f('vat')/100;

	//$vat_percent[$get_invoice_rows->f('vat')] += round($vat_percent_val,2);
	$vat_percent[$get_invoice_rows->f('vat')] += $vat_percent_val;
	$sub_t[$get_invoice_rows->f('vat')] += $amount;
	$sub_disc[$get_invoice_rows->f('vat')] += $amount_d;
}
//console::log($sub_disc,$sub_t);
// console::log($vat_percent);
$o['vat_line']=array();
$t=0;
$o['hide_total'] = true;
foreach ($vat_percent as $key => $val){ //console::log($sub_t[$key] - $sub_disc[$key]);
	 if($sub_t[$key] - $sub_disc[$key]){
	 	// $val = $sub_t[$key]*return_value($key)/100;
		$vat_line=array(
			'vat_percent'   					=> display_number($key),
			'vat_value'	   						=> place_currency(display_number($val),$currency),
			'total_novat'	   	 				=> place_currency(display_number($sub_t[$key]),$currency),
			'subtotal'							=> place_currency(display_number($sub_t[$key]),$currency),
			'discount_value'	 		 	 	=> place_currency(display_number($sub_disc[$key]),$currency),
			'sh_discount'							=> ($discount_procent != 0 || $show_disc === true) ? true : false,
			'sh_vat'									=> $tblinvoice->f('remove_vat') == 1 ? false : true,
			'net_amount'						 	=> place_currency(display_number($sub_t[$key] - $sub_disc[$key]),$currency),
			'total_discount_procent'  => $tblinvoice->f('discount') != 0 ? display_number($tblinvoice->f('discount')) : '',
		);
		array_push($o['vat_line'], $vat_line);
		/*$view->loop('vat_line');*/
	}
	$t++;
}
			if($t==1){
				$o['hide_total'] = false;
			}
		$o['discount_value']=place_currency(display_number($discount_value),$currency);
		$o['net_amount']= place_currency(display_number($netto_amount),$currency);
		$o['total_wo_vat']= place_currency(display_number($subtotal),$currency);

		if($discount_procent){
			$o['view_discount1'] 		= true;
		}else{
			$o['view_discount1']		= false;
		}

$total_payments = 0;
$total_amount_due = $total_amount - $total_payments;

//sent mail
if(!$in['include_pdf'])
{
	$in['include_pdf'] = 1;
	$o['include_pdf']=true;
}
if(!$in['include_xml']){
	$in['include_xml'] = 1;
	$o['include_xml']=true;
}

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);
//$user_email = $db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$user_email = $db_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
if(defined('USE_INVOICE_WEB_LINK') && USE_INVOICE_WEB_LINK == 1){
	//$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['invoice_id']."' AND `type`='i'  ");
	$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type  ",['d'=>DATABASE_NAME,'item_id'=>$in['invoice_id'],'type'=>'i']);
	if(defined('WEB_INCLUDE_PDF') && WEB_INCLUDE_PDF == 0){
		$in['include_pdf'] = 0;
		$o['include_pdf']=false;
	}
	if((defined('WEB_INCLUDE_XML') && WEB_INCLUDE_XML == 0) || !defined('WEB_INCLUDE_XML') ){
		$in['include_xml'] = 0;
		$o['include_xml']=false;
	}
}


$tblinvoice123 = $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,tblinvoice.discount, tblinvoice.invoice_date,tblinvoice.buyer_name,tblinvoice.due_date,
									tblinvoice.currency_type,tblinvoice.pdf_logo,tblinvoice.pdf_layout, tblinvoice.type, tblinvoice.amount_vat,
                                  SUM(tblinvoice_line.amount) AS total,
                                  SUM(tblinvoice_payments.amount) AS total_payed
                           FROM tblinvoice
                           LEFT JOIN tblinvoice_line ON tblinvoice_line.invoice_id=tblinvoice.id
                           LEFT JOIN tblinvoice_payments ON tblinvoice_payments.invoice_id=tblinvoice.id
                           WHERE tblinvoice.id='".$in['invoice_id']."'");
$tblinvoice123->move_next();
global $cfg;
if($tblinvoice123->f('type')==2 || $tblinvoice123->f('type')==4){
	$message_data=get_sys_message('invmess_credit',$tblinvoice->f('email_language'));
}else{
	$message_data=get_sys_message('invmess',$tblinvoice->f('email_language'));
}

$subject=$message_data['subject'];
$external_web_link = '<a href="'.$cfg['web_link_url'].'?q='.$exist_url.'">'.$cfg['web_link_url'].'?q='.$exist_url.'</a>';
$disc_percent = $tblinvoice->f('discount');

switch ($tblinvoice123->f('type')) {
	case 1:
		$invoice_type_txt = gm('Proforma invoice');
		break;
	case 2:
		$invoice_type_txt = gm('Credit Invoice');
		$amount_due = $tblinvoice123->f('amount_vat');
		break;
	case 4:
		$invoice_type_txt = gm('Credit Invoice');
		$amount_due = $tblinvoice123->f('amount_vat');
		break;
	default:
		$invoice_type_txt = gm('Invoice');
		break;
}
if($tblinvoice->f('apply_discount') < 2){
	$disc_percent = 0;
}
$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
if($status=='draft' && $tblinvoice123->f('type')!='1' &&  !$tblinvoice123->f('serial_number')){
	$s_nr='';
	if($tblinvoice123->f('type')==0 || $tblinvoice123->f('type')==3 ){
      	$s_nr=generate_invoice_number(DATABASE_NAME);
    	}elseif($tblinvoice123->f('type')==2 || $tblinvoice123->f('type')==4){
    	 	$s_nr=generate_credit_invoice_number(DATABASE_NAME);
    	}
    	$subject=str_replace('[!SERIAL_NUMBER!]',$s_nr, $subject);
}else{
	$subject=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $subject);
}
$subject=str_replace('[!INVOICE_DATE!]',$factur_date, $subject);
$subject=str_replace('[!CUSTOMER!]',$tblinvoice123->f('buyer_name'), $subject);
$subject=str_replace('[!SUBTOTAL!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $subject);
$subject=str_replace('[!DISCOUNT!]',$disc_percent.'%', $subject);
$subject=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_value),$currency), $subject);
$subject=str_replace('[!DUE_DATE!]',date(ACCOUNT_DATE_FORMAT,$tblinvoice123->f('due_date')), $subject);
//$subject=str_replace('[!VAT!]',$db->f('vat').'%', $subject);
$subject=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $subject);
$subject=str_replace('[!PAYMENTS!]',place_currency(display_number($total_payed),$currency), $subject);
$subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount_due),$currency), $subject);
$subject=str_replace('[!INVOICE_TYPE!]', $invoice_type_txt, $subject);

$body=$message_data['text'];
$body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
if($status=='draft' && $tblinvoice123->f('type')!='1' &&  !$tblinvoice123->f('serial_number')){
	$s_nr='';
	if($tblinvoice123->f('type')==0 || $tblinvoice123->f('type')==3){
      	$s_nr=generate_invoice_number(DATABASE_NAME);
    	}elseif($tblinvoice123->f('type')==2 || $tblinvoice123->f('type')==4){
    	 	$s_nr=generate_credit_invoice_number(DATABASE_NAME);
    	}
    	$body=str_replace('[!SERIAL_NUMBER!]',$s_nr, $body);
}else{
	$body=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $body);
}
$body=str_replace('[!INVOICE_DATE!]',$factur_date, $body);
$body=str_replace('[!CUSTOMER!]',$tblinvoice123->f('buyer_name'), $body);
$body=str_replace('[!SUBTOTAL!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $body);
$body=str_replace('[!DISCOUNT!]',$disc_percent.'%', $body);
$body=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_value),$currency), $body);
$body=str_replace('[!DUE_DATE!]',date(ACCOUNT_DATE_FORMAT,$tblinvoice123->f('due_date')), $body);
//$body=str_replace('[!VAT!]',$db->f('vat').'%', $body);
$body=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $body);
$body=str_replace('[!PAYMENTS!]',place_currency(display_number($total_payed),$currency), $body);
$body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount_due),$currency), $body);
$body=str_replace('[!INVOICE_TYPE!]', $invoice_type_txt, $body);
if($message_data['use_html']){
   $body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);
}else{
	 $body=str_replace('[!SIGNATURE!]','', $body);
}
//$account_manager_email = $db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
$account_manager_email = $db_users->field("SELECT email FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$account_user_email = $db_users->field("SELECT email FROM users WHERE user_id='".$tblinvoice->f('acc_manager_id')."'");	

	$o['include_pdf_checked']  = $in['include_pdf'] ? true : false;
	$o['copy_checked']         = $in['copy'] ? true : false;
	$o['subject']              = $in['subject'] ? $in['subject'] : $subject;
	$o['e_message']            = $in['e_message'] ? stripslashes($in['e_message']) : $body;
	$o['language_dd']	        = build_language_dd_new($tblinvoice->f('email_language'));
	$o['use_html']		    	= $message_data['use_html'];
	$o['user_loged_email']		= $account_manager_email;
	$o['user_acc_email']		= $account_user_email;
	$o['user_mail_sent']		= 0;

$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php';
if($buyer_id){
	$invoice_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$buyer_id."' ");
	if($invoice_email){
		$o['inv_email']		= true;
		$o['invoice_email']	= $invoice_email;
	}
	$o['isbuyer']	= true;
	$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php?customer_id='.$buyer_id;
	$o['customer_notes']=stripslashes($db->field("SELECT customer_notes FROM customers WHERE customer_id='".$buyer_id."' "));
}
	$tblinvoice_customer_contacts = $db->query("SELECT * FROM customer_contactsIds
             WHERE customer_id='".$buyer_id."' ");

$o['recipients']=array();
/*while($tblinvoice_customer_contacts->move_next()){
	if($tblinvoice_customer_contacts->f('email') == $invoice_email) {
		continue;
	}

	$recipients=array(
		'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),

	);
	array_push($o['recipients'], $recipients);
}*/
$o['recipients_ids'] = array();
$c_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."' ");
$invoice_email_type = $db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."' ");
/*$cont_email = $db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$tblinvoice->f('contact_id')."' AND customer_id='".$tblinvoice->f('buyer_id')."' ");
$name_cont = $db->query("SELECT firstname,lastname FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'");
	if($tblinvoice->f('buyer_id') && !$tblinvoice->f('contact_id') && !$invoice_email) {
		$items_id = $c_email;
	}elseif($tblinvoice->f('buyer_id') && $tblinvoice->f('contact_id') && !$invoice_email){
		$items_id =$cont_email;
	}
*/
		if($tblinvoice->f('buyer_id')){
			$filter .= " AND customers.customer_id='".$tblinvoice->f('buyer_id')."'";
		}
		if($tblinvoice->f('contact_id')){
			$filter .= " AND customer_contacts.contact_id='".$tblinvoice->f('contact_id')."'";
		}

		$items = array();
		$items2 = array();
		$contact_email='';
		$customer_exist = false;
		if($tblinvoice->f('buyer_id') && $tblinvoice->f('contact_id')){
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$tblinvoice->f('buyer_id')."' ORDER BY lastname ")->getAll();
		}else{
/*			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.customer_id = customer_contactsIds.customer_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1  AND customers.customer_id='".$tblinvoice->f('buyer_id')."'  ORDER BY lastname ")->getAll();	*/
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$tblinvoice->f('buyer_id')."' ORDER BY lastname ")->getAll();
		}
		if($tblinvoice->f('buyer_id') && !$tblinvoice->f('contact_id')){
			$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE 1=1 $filter ");
			$customer_exist = true;


		}else{
			$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ");
		}
		$customerName = $customer->f('name');
		if(!empty($customerName)){
			$items2 = array(
				    'id' => $customer->f('email'), 
				    'label' => $customer->f('name'),
				    'value' => $customer->f('name').' - '.$customer->f('email'),
			);	
		}
		if($invoice_email_type==1 && $tblinvoice->f('buyer_id')){
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$tblinvoice->f('buyer_id')."'  ORDER BY lastname ")->getAll();
		}
		$contacts_exist = false;

		foreach ($contacts as $key => $value) {

			if($value['name']){
				$name = $value['firstname'].' '.$value['lastname'].' > '.$value['name'];
			}else{
				$name = $value['firstname'].' '.$value['lastname'];
			}
			if(!empty($name)){
				$items[] = array(
				    'id' => $value['email'], 
				    'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				    'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)).' - '.$value['email'],
				);	
			}
			
			$contacts_exist = true;
			if($value['contact_id']==$tblinvoice->f('contact_id')){
				$contact_email=$value['email'];
			}
		}

		/*if($customer_exist){
			array_push($o['recipients_ids'], $items2['id']);

		}elseif($contacts_exist && !$customer_exist){
			array_push($o['recipients_ids'], $items[0]['id']);

		}
		if($invoice_email_type==1 && $tblinvoice->f('buyer_id')){
			array_push($o['recipients_ids'],$items2['id']);
		}else{
			if($c_email){
				$o['recipients_ids'] = array();
				array_push($o['recipients_ids'],$c_email);
			}			
		}*/

		if($invoice_email_type==1){
			if(!$tblinvoice->f('contact_id')){
				//array_push($o['recipients_ids'], $items2['id']);
			}else{
				array_push($o['recipients_ids'], $contact_email);
			}
		}else{
			if($c_email){
				$o['recipients_ids'] = array();
				$c_email = str_replace(' ', '', $c_email);
				$email_arr = array();
				$email_arr = explode(";", $c_email);
				foreach($email_arr as $key=> $value){
					array_push($o['recipients_ids'],$value);
				}
				
			}
		}



		$added = false;
		foreach ($items as $key => $value) {
			if($value['id'] == $items2['id']){
				$added = true;
			}
		}
		if(!$added){
			array_push($items, $items2);
		}
		if($invoice_email_type==1 && $tblinvoice->f('buyer_id')){
			array_push($items, $items2);
		}else{	
			if($c_email){
				$c_email = str_replace(' ', '', $c_email);
				$email_arr = array();
				$email_arr = explode(";", $c_email);
				foreach($email_arr as $key=> $value){
					array_push($items, array(
					'id' => $value, 
					'label' =>$value,
					'value' => $value,
				));
				}
				
			}
		}
	//$o['contact_email_data'] 			= $invoice_email;
/*	$o['recipients'] = array_map(function($field){
										return $field['id'];
									},$items);*/

	$o['recipients'] =$items;
/*	array_push($o['recipients'], array(
		'recipient_email' 			=> $items
	));*/
	$o['save_disabled'] 			= sizeof($o['recipients_ids'])? false: true;
	$o['contact_id_data']			= $buyer_id;
	$o['contact_name_data']			= $invoice_email;
	$o['auto_complete_small']		= '<input type="text" class="auto_complete_small" name="email_addr" value="" data-autocomplete_file="'.$autocomplete_path.'" />';
	$o['autocomplete_file']			= $autocomplete_path;


/*if($tblinvoice123->f('pdf_layout')){
	$link_end='&type='.$tblinvoice123->f('pdf_layout').'&logo='.$tblinvoice123->f('pdf_logo');
}else{
	$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
}*/
$in['use_custom'] = 0;
$o['use_custom']=false;
if($tblinvoice123->f('pdf_layout') && $tblinvoice->f('use_custom_template')==0){
	$in['logo'] = $tblinvoice123->f('pdf_logo');
	$o['logo'] = $tblinvoice123->f('pdf_logo');
	$link_end='&type='.$tblinvoice123->f('pdf_layout').'&logo='.$tblinvoice123->f('pdf_logo');
	$in['template_type'] = $tblinvoice123->f('pdf_layout');
	$o['template_type']= $tblinvoice123->f('pdf_layout');
}elseif($tblinvoice123->f('pdf_layout') && $tblinvoice->f('use_custom_template')==1){
	$link_end = '&custom_type='.$tblinvoice123->f('pdf_layout').'&logo='.$tblinvoice123->f('pdf_logo');
	$in['template_type'] = $tblinvoice123->f('pdf_layout');
	$in['use_custom'] = 1;
	$o['template_type'] = $tblinvoice123->f('pdf_layout');
	$o['use_custom'] = true;
}else{
	$link_end = '&type='.ACCOUNT_INVOICE_BODY_PDF_FORMAT;

	$in['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
	$o['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
}
//$link_credit_end = '&type='.ACCOUNT_INVOICE_CREDIT_BODY_PDF_FORMAT;
$link_credit_end = '&type='.ACCOUNT_INVOICE_CREDIT_PDF_FORMAT;
#if we are using a customer pdf template
if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $tblinvoice123->f('pdf_layout') == 0){
	$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
	//$link_credit_end = '&custom_type='.ACCOUNT_INVOICE_CREDIT_BODY_PDF_FORMAT;
}

if(defined('USE_CUSTOME_INVOICE_CREDIT_PDF') && USE_CUSTOME_INVOICE_CREDIT_PDF == 1 && $tblinvoice123->f('pdf_layout') == 0){
		$link_credit_end = '&custom_type='.ACCOUNT_INVOICE_CREDIT_PDF_FORMAT;
	}
if($tblinvoice->f('type')==2 || $tblinvoice->f('type')==4){
	$o['pdf_link']	      = 'index.php?do=invoice-invoice_credit_print&id='.$in['invoice_id'].'&lid='.$tblinvoice->f('email_language').$link_credit_end;
	$o['xml_link'] 		= 'index.php?do=invoice-xml_invoice_print&id='.$in['invoice_id'];
}else{
	$o['pdf_link']	      = 'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$tblinvoice->f('email_language').$link_end;
	$o['xml_link'] 		= 'index.php?do=invoice-xml_invoice_print&id='.$in['invoice_id'];
}

	

/*
$l=0;
//print languages
$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active=1 GROUP BY lang_id ORDER BY sort_order ");
while($pim_lang->move_next()){
	// if($pim_lang->f('default_lang')){
 		$default_lang = $pim_lang->f('lang_id');
 	// }
  	$view->assign(array(
		'language'	        	=> gm($pim_lang->f('language')),
		'pdf_link_loop'         => 'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$pim_lang->f('lang_id').$link_end,
		'web_invoice_link'  	=> 'index_blank.php?do=invoice-invoice_web_print&invoice_id='.$in['invoice_id'].'&lid='.$pim_lang->f('lang_id').$link_end,
	),'languages_pdf');
	$l++;
	$view->loop('languages_pdf');
}
$db->query("select * from pim_custom_lang where active=1 ORDER BY sort_order ");
 while($db->move_next()){
 	$db2->query("SELECT * FROM label_language WHERE lang_code='".$db->f('lang_id')."'");
 	if($db2->move_next())
 	{
	 	$view->assign(array(
			'language'	        	=> $db2->f('name'),
			'pdf_link_loop'         => 'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$db2->f('label_language_id').$link_end,
			'web_invoice_link'  	=> 'index_blank.php?do=invoice-invoice_web_print&invoice_id='.$in['invoice_id'].'&lid='.$db2->f('label_language_id').$link_end,
		),'languages_pdf');
		$l++;
	}
	$view->loop('languages_pdf');
}
if($l==1){
	$view->assign(array(
	   'generate_pdf_id'  		=> '',
	   'web_invoice_id'   		=> '',
	   'hide_language'	  		=> 'hide',
	   'pdf_link'	         	=> 'index.php?do=invoice-invoice_print&id='.$in['invoice_id'].'&lid='.$default_lang.$link_end,
	));
}else{
	$view->assign(array(
	   'generate_pdf_id'  		=> 'generate_pdf',
	   'web_invoice_id'   		=> 'web_invoice',
	   'hide_language'	  		=> '',
	   'pdf_link'	      		=> '#',
	));
}*/
//add_nr box
if($in['do']=='invoice-invoice-invoice-add_nr' ){
	$o['view_add_nr']=true;
}else{
	/*$o['view_add_nr']="style='display:none;'";*/
	$o['view_add_nr']=false;
}

//attached files
$o['files_row']=array();
$files=0;
$f = $db->query("SELECT * FROM attached_files WHERE type='2'");
while ($f->next()) {
	//$path = '../../'.$f->f('path');
	$path = $f->f('path');
	//$p = substr('../../'.$f->f('path'), -1);
	$p = substr($f->f('path'), -1);
	if($p != '/'){
		//$path = '../../'.$f->f('path').'/';
		$path = $f->f('path').'/';
	}
	$files_row=array(
		'file'		=> $f->f('name'),
		'checked'	=> $f->f('default') == 1 ? true : false,
		'file_id'	=> $f->f('file_id'),
		'path'		=> $path,
		);
	array_push($o['files_row'], $files_row);
	/*$view->loop('files_row');*/
	$files++;
}
$is_file = false;
if($files>0){
	$is_file = true;
}
$o['is_file']=$is_file;

//sent box
/*$o['view_sent']="style='display:none;'";*/
$o['view_sent']=false;

//send box
/*$o['view_send']="style='display:none;'";*/
$o['view_send']=false;
if(!$in['s_date']){
	$in['s_date'] = time();
	$o['s_date'] = time();
}
$rel = $buyer_id ? 'customer_addresses.customer_id.'.$buyer_id : 'customer_contact_address.contact_id.'.$contact_id;



$proforma_id=$db->field("SELECT proforma_id FROM tblinvoice WHERE id='".$in['invoice_id']."'");
if($proforma_id){
    $total_payed_proforma = $db->field("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$proforma_id."'");
	$proforma_date=$db->field("SELECT invoice_date FROM tblinvoice WHERE id='".$proforma_id."'");
	if($total_payed_proforma){
	 	$is_proforma_payments=true;
	}else{
	 	$is_proforma_payments=false;
	}
}
if($tblinvoice->f('c_invoice_id')){
	$order_id=$db->field("SELECT order_id FROM tblinvoice WHERE id='".$tblinvoice->f('c_invoice_id')."'");
	$order_id=trim($order_id,';');
}

if($order_id){
  $o['is_order']=true;
  /*$o['return_link']='index.php?do=order-order&order_id='.$order_id;*/
  $o['return_link']=$order_id;
}

$is_invoice_for_proforma=$db->field("SELECT proforma_id FROM tblinvoice WHERE proforma_id='".$in['invoice_id']."' AND f_archived='0' ");

if($is_invoice_for_proforma){
  $o['no_regular_invoice']=false;
  $o['has_regular_invoice']=true;
}else{
  $o['no_regular_invoice']=true;
  $o['has_regular_invoice']=false;
}

if($_SESSION['main_u_id']==0){
	//$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
}else{
	//$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id='".$_SESSION['main_u_id']."' ");
	$bPaidCust=$db_users->field("SELECT bPaid_cust_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['main_u_id']]);
}

	$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	$bepaid_active=$db->field("SELECT active FROM apps WHERE name='Instapaid' AND main_app_id='0' AND type='main'");
	$edebex_active=$db->field("SELECT active FROM apps WHERE name='Edebex' AND main_app_id='0' AND type='main'");

$select = $db->query("SELECT * FROM tblinvoice_projects WHERE invoice_id='{$in['invoice_id']}'")->records_count();

if(ADV_QUOTE==0 && ADV_PRODUCT==0 && ADV_CRM==0 && ADV_STOCK==0){
	$hide_invoice=false;
}else{
	$hide_invoice=true;
}
		$is_contact = 0;
		if($tblinvoice->f('is_contact')) {
			$is_contact = 1;
		}


		$drop_info = array('drop_folder' => 'invoices', 'customer_id' => $tblinvoice->f('buyer_id'), 'item_id' => $in['invoice_id'], 'isConcact' => $is_contact, 'serial_number' => $tblinvoice->f('serial_number'));

		$o['is_customer']		= true;
		$o['is_drop']			= defined('DROPBOX') && (DROPBOX != '') ? true : false;
		//$output['is_data']			= $is_data;
		//$output['drop_info']		= htmlentities(json_encode($drop_info));
		$o['drop_info']		= $drop_info;

	$margin_percent=0;
	if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1) { 
		if($amount_purchase){
			//$margin_percent = $tblinvoice->f('margin')/ $amount_purchase *100; 
			$margin_percent = $margin/ $amount_purchase *100; 
		}elseif($tblinvoice->f('margin')){
			$margin_percent =100;
		}
		
	}else{
		if($total_amount_for_margin){
			//$margin_percent = $tblinvoice->f('margin')/ $total_amount *100; 
			$margin_percent = $margin/ $total_amount_for_margin *100; 
		}elseif($tblinvoice->f('margin')){
			$margin_percent =100;
		}
	}

	$stripe_active =$db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' and active='1' ");

	$o['hide_invoice']    						= $hide_invoice;
	$o['invoice_id']    						= $in['invoice_id'];
	$o['c_invoice_id']    						= $tblinvoice->f('c_invoice_id');
	$o['i_date']								= $in['i_date'] ? $in['i_date'] : time();
	$o['invoice_date']							= $in['i_date'] ? $in['i_date']*1000 : time()*1000;
	$o['s_date']								= $in['s_date'];
	$o['sent_date']								= is_numeric($in['s_date']) ? $in['s_date']*1000 : strtotime($in['s_date'])*1000;
	$o['hide_activity']							= $_SESSION['access_level'] == 1 ? true : false;
	$o['regular_invoice_serial_number']			= $db->field("SELECT serial_number FROM tblinvoice WHERE proforma_id='".$in['invoice_id']."'");
	$o['regular_invoice_id']       		 		= $db->field("SELECT id FROM tblinvoice WHERE proforma_id='".$in['invoice_id']."'");
	$o['has_proforma']            		 	 	= $has_proforma;
	$o['is_proforma_payments']   		  		= $is_proforma_payments;
	$o['proforma_date']          				= date(ACCOUNT_DATE_FORMAT,$proforma_date);
	$o['total_payed_proforma']  		    	= place_currency(display_number($total_payed_proforma),$currency);
	$o['page_title']	    					= $page_title;
	$o['pick_date_format']						= pick_date_format();
	$o['search']								= $in['search']? '&search='.$in['search'] : '';
	$o['view']									= $in['view']? '&view='.$in['view'] : '';
	$o['default_currency_name']					= build_currency_name_list(ACCOUNT_CURRENCY_TYPE);
	$o['hide_total_currency']					= $currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? false : true) : false;
	$o['style']     							= ACCOUNT_NUMBER_FORMAT;
	$o['web_url']								= $config['web_link_url'].'?q=';
	$o['use_weblink']							= defined('USE_INVOICE_WEB_LINK') && USE_INVOICE_WEB_LINK == 1 ? true : false;
	$o['checked_attach']						= $tblinvoice->f('attach_timesheet') == 1 ? true : false;
	$o['is_attached_timesheet']					= ($select > 0 && $tblinvoice->f('attach_timesheet') == 1) ? true : false;
	$o['is_timesheets']							= $select > 0 ? true : false;
	$o['USER_EMAIL']							= $user_email;
	$o['checked_attach_int']					= $tblinvoice->f('attach_intervention') == 1 ? true : false;
	$o['is_attached_intervention']				= ($tblinvoice->f('service_id') && $tblinvoice->f('attach_intervention') == 1) ? true : false;
    $o['is_profi']                   	 		= $is_profi;
    //$o['margin_value']                			= place_currency(display_number($tblinvoice->f('margin')),$currency);
    $o['margin_value']                			= place_currency(display_number($margin),$currency);
    //$o['margin_percent']              		 	= display_number($tblinvoice->f('margin_percent'));
    $o['margin_percent']              		 	= display_number($margin_percent);
    $o['page_tip']								= $tips;
    $o['provider_ref']						= base64_encode(DATABASE_NAME);
    $o['bPaid_reg']						= $bPaidCust!='' ? true : false;
    $o['contact_id']						= $tblinvoice->f('contact_id');
    $o['post_active']						= !$post_active || $post_active==0 ? false : true;
    $o['post_invoice']						= $tblinvoice->f('postgreen_id')!='' ? true : false;
    $o['bePaid_debt_done']					= $tblinvoice->f('bPaid_debt_id')!='' ? true : false;
    $o['bePaid_debt_id']					= $tblinvoice->f('bPaid_debt_id');
    $o['edebex_show']						= false; //($tblinvoice->f('amount_vat')>=5000 && $tblinvoice->f('due_days')>=30) ? true : false;
    $o['lg']								= $_SESSION['l'];
    $o['bpaid_active']						= $bepaid_active ? true : false;
	$o['edebex_active']						= $edebex_active ? true : false;
	$o['has_multiple_identities']			= $db->field("SELECT count(identity_id) FROM multiple_identity")? true: false;
	$o['stripe_active']						= $stripe_active;
	$o['pay_with_stripe']					= $tblinvoice->f('payment_method')=='2'? true:false;
	$o['is_active']							= $tblinvoice->f('f_archived')!='1'? true:false;
	$o['is_archived']						= $tblinvoice->f('f_archived')=='1'? true:false;
	$o['yuki_project_id']					= $tblinvoice->f('yuki_project_id');
	$o['yuki_project_name']					= get_yuki_project_name($tblinvoice->f('yuki_project_id'));


	if($tblinvoice->f('source_id')){
		$o['source'] = $db->field("SELECT name FROM tblquote_source WHERE id='".$tblinvoice->f('source_id')."' ");
		$o['source_id']= $tblinvoice->f('source_id');
	}
	if($tblinvoice->f('type_id')){
		$o['xtype'] = $db->field("SELECT name FROM tblquote_type WHERE id='".$tblinvoice->f('type_id')."' ");
		$o['type_id']						= $tblinvoice->f('type_id');
	}
	if($tblinvoice->f('segment_id')){
		$o['segment'] = $db->field("SELECT name FROM tblquote_segment WHERE id='".$tblinvoice->f('segment_id')."' ");
		$o['segment_id']= $tblinvoice->f('segment_id');
	}
	$o['segment_dd']					= get_categorisation_segment();	
	$o['source_dd']					= get_categorisation_source();	
	$o['type_dd']					= get_categorisation_type();

    if($in['bPaid_modUrl']){
    	$o['bPaid_modUrl']		= $in['bPaid_modUrl'].'&lang='.strtoupper($_SESSION['l']);
    }

    $post_library=array();
    if($tblinvoice->f('postgreen_id')!=''){  	
    	include_once('apps/invoice/model/invoice.php');
	$postg=new invoice(); 
	$post_status=json_decode($postg->postInvoiceData($in));
    	$o['postg_status']		= $post_status->status;
    }
    if($post_active){
    	$in['ret_res']=true;
    	$post_library=include_once('apps/misc/controller/post_library.php');
    }
    $o['post_library']=$post_library;
$o['external_url']=$tblinvoice->f('sent') == '0' ? false : $exist_url;

if($status=='draft' && $tblinvoice->f('type')!='1' &&  !$tblinvoice->f('serial_number')){
   $newest_inv=$db->field("SELECT id FROM tblinvoice WHERE id!='".$tblinvoice->f('serial_number')."' and type!='1' and invoice_date > '".$tblinvoice->f('invoice_date')."'");
     
   $invoice_title = gm('Invoice Nr').'*:';
   if($tblinvoice->f('type')==0 || $tblinvoice->f('type')==3){
      $in['serial_number']=generate_invoice_number(DATABASE_NAME);
      $o['serial_number']=generate_invoice_number(DATABASE_NAME);
    }elseif($tblinvoice->f('type')==2 || $tblinvoice->f('type')==4){
    	 $in['serial_number']=generate_credit_invoice_number(DATABASE_NAME);
    	 $o['serial_number']=generate_credit_invoice_number(DATABASE_NAME);
    }

	$o['invoice_title']    				= $invoice_title;
	$o['DRAFT_INVOICE_NO_NUMBER']       = defined('DRAFT_INVOICE_NO_NUMBER') && DRAFT_INVOICE_NO_NUMBER == '1' ? true : false;
	$o['red_inp']                       =$newest_inv?'red_inp':'';

}

	$quote_nr_row = array();
	$quote_nr = $db->query("SELECT tracking_line.*, tracking.*, tblquote.serial_number, tblquote.sent, tblquote_version.version_status_customer, tblquote.f_archived FROM tracking
							INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
							INNER JOIN tblquote ON tracking_line.origin_id = tblquote.id
							INNER JOIN (SELECT tblquote_version.quote_id,MAX(tblquote_version.version_id) AS v_id FROM tblquote_version GROUP BY tblquote_version.quote_id) AS tblq_version ON tblquote.id=tblq_version.quote_id
            				INNER JOIN tblquote_version ON tblq_version.quote_id=tblquote_version.quote_id AND tblq_version.v_id=tblquote_version.version_id
							WHERE tracking.trace_id = '".$tblinvoice->f('trace_id')."' AND tracking_line.origin_type ='2' ");

	$quote_opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
	while($quote_nr->move_next()){
		$status=$quote_opts[$quote_nr->f('version_status_customer')];
		if($quote_nr->f('sent') == 1 && $quote_nr->f('version_status_customer') == 0){
			$status=$quote_opts[5];
		}
	    array_push($quote_nr_row, array( 'serial' => ($quote_nr->f('f_archived') ? '' : $quote_nr->f('serial_number')), 'id'=> $quote_nr->f('origin_id'), 'status'=> ($quote_nr->f('f_archived') ? gm('Archived') : $status)));   
	}
	$o['quotes']=$quote_nr_row;

	$order_nr_row = array();
	$ord_nr = $db->query("SELECT pim_orders.order_id,pim_orders.serial_number, pim_orders.sent, pim_orders.invoiced, pim_orders.rdy_invoice,pim_orders.active FROM tracking
							INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
							INNER JOIN pim_orders ON tracking_line.origin_id = pim_orders.order_id
							WHERE tracking.trace_id = '".$tblinvoice->f('trace_id')."' AND tracking_line.origin_type ='4' ")->getAll();

	$ord_nr_more=$db->query("SELECT pim_orders.order_id,pim_orders.serial_number, pim_orders.sent, pim_orders.invoiced, pim_orders.rdy_invoice,pim_orders.active FROM tracking
							INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id AND tracking_line.origin_type ='7'
							INNER JOIN tracking AS tracking_proforma ON tracking_proforma.target_id=tracking_line.origin_id AND tracking_proforma.target_type=tracking_line.origin_type
							INNER JOIN tracking_line AS tracking_line_order ON tracking_proforma.trace_id=tracking_line_order.trace_id AND tracking_line_order.origin_type ='4'
							INNER JOIN pim_orders ON tracking_line_order.origin_id = pim_orders.order_id
							WHERE tracking.trace_id = '".$tblinvoice->f('trace_id')."'  ")->getAll();

	if(!empty($ord_nr_more)){
		$ord_nr=array_merge($ord_nr,$ord_nr_more);
	}

	foreach($ord_nr as $key=>$value){
		$status ='';
		if($value['sent']==0 ){
			$status = gm('Draft');
		}else if($value['invoiced']){
			if($value['rdy_invoice'] == 1){
				$status = gm('Delivered');
			}else{
				$status = gm('Confirmed');
			}
		}else{
			if($value['rdy_invoice'] == 2){
				$status = gm('Confirmed');
			}else{
				if($value['rdy_invoice'] == 1){
					$status = gm('Delivered');
				}else{
					if($value['sent'] == 1){
						$status = gm('Confirmed');
					}else{
						$status = gm('Draft');
					}
				}
			}
		}

		$is_delivery_planned=false;
		if(ORDER_DELIVERY_STEPS == '2'){
	   		$is_partial_deliver = false;
	   		if($value['rdy_invoice']==2){
	   			$dels=$db->query("SELECT * FROM pim_order_deliveries WHERE order_id='".$value['order_id']."'");
		   		while($dels->next()){
		   			//daca cel putin una din livrarile partiale sunt confirmate, atunci is_partial_deliver = true
		   			if($dels->f('delivery_done')=='1'){
		   				$is_partial_deliver = true;
		   				$is_delivery_planned=false;
		   				break;
		   			}else{
		   				$is_delivery_planned=true;
		   			}
		   		}
	   		}
	   		

	    }else{
	    	 $is_partial_deliver= $value['rdy_invoice']==2?true:false;
	    }
		
		if($is_partial_deliver && !$is_delivery_planned && $value['sent'] == 1){
			$status = gm('Partially Delivered');
		}else if($is_delivery_planned && $value['sent'] == 1){
			$status = gm('Delivery planned');
		}

    	array_push($order_nr_row, array( 'serial' => (!$value['active'] ? '' : $value['serial_number']), 'id'=> $value['order_id'], 'status'=> (!$value['active'] ? gm('Archived') : $status)));	    
	}
	$o['orders']=$order_nr_row;

	$service_nr_row = array();
	$service_nr = $db->query("SELECT servicing_support.service_id,servicing_support.serial_number, servicing_support.status, servicing_support.active, servicing_support.accept FROM tracking
							INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
							INNER JOIN servicing_support ON tracking_line.origin_id = servicing_support.service_id
							WHERE tracking.trace_id = '".$tblinvoice->f('trace_id')."' AND tracking_line.origin_type ='5' ")->getAll();

	foreach($service_nr as $key=>$value){
		$status ='';
		if($value['status']=='1'){
			$status = gm('Planned');
		}elseif($value['status']=='2'){
			if($value['active']=='1'&&$value['accept']=='1'){
				$status = gm('Accepted');
			}else{
				$status = gm('Closed');
			}
		}else{
			$status = gm('Draft');
		}

    	array_push($service_nr_row, array( 'serial' => (!$value['active'] ? '' : $value['serial_number']), 'id'=> $value['service_id'], 'status'=> (!$value['active'] ? gm('Archived') : $status)));	    
	}
	$o['services']=$service_nr_row;

	$o['recurring_invoice_id']='';
	$recurring_invoice_id=$db->field("SELECT tracking_line.origin_id FROM tracking
	INNER JOIN tracking_line ON tracking.trace_id=tracking_line.trace_id
	INNER JOIN recurring_invoice ON tracking_line.origin_id = recurring_invoice.recurring_invoice_id
	WHERE tracking.trace_id = '".$tblinvoice->f('trace_id')."' AND tracking_line.origin_type ='13' ");
	if($recurring_invoice_id){
		$o['recurring_invoice_id']=$recurring_invoice_id;
	}

	//for proforma invoices - get the sales invoices that are linked to it
	if($tblinvoice->f('type')=='1'){
		$invoice_nr_row = array();
		$inv_nr = $db->query("SELECT tblinvoice.id,tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.due_date, tblinvoice.paid,tblinvoice.f_archived  FROM tracking
									INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
									INNER JOIN tblinvoice ON tracking.target_id = tblinvoice.id
									WHERE tracking_line.origin_id = '".$tblinvoice->f('id')."'
									AND tracking_line.origin_type = '7'
									AND tracking.target_type = '1'")->getAll();

		foreach($inv_nr as $key=>$value){
			$type_title='';
		    switch ($value['type']){
			case '0':
			    $type_title = gm('Regular invoice');
			    break;
			case '1':
			    $type_title = gm('Proforma invoice');
			    break;
			case '2':
			    $type_title = gm('Credit Invoice');
			    break;
		    }	    

		    if($value['sent'] == '0' && $value['type']!='2'){
				$type_title = gm('Draft');
		    }else if($value['status']=='1'){
				$type_title = $value['paid']=='2' ? gm('Partially Paid') : gm('Paid');
		    }else if($value['sent']!='0' && $value['not_paid']=='0'){
		    	if($value['status']=='0' && $value['due_date']< time()){
		    		$type_title =  gm('Late');
		    	}else{
		    		$type_title = gm('Final');
		    		if($is_easy_invoice_diy){
						$jefacture_exported=$db->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$value['id']."' AND jefacture='1' AND `type`='0' ");
						if($jefacture_exported){
							$type_title = gm('Exported');
						}
					}
		    	}
				
		    } else if($value['sent']!='0' && $value['not_paid']=='1'){
				$type_title = gm('No Payment');
		    }
		    
	    	array_push($invoice_nr_row, array( 'serial' => ($value['f_archived']=='1' ? '': $value['serial_number']), 'id'=> $value['id'], 'status'=> ($value['f_archived']=='1' ? gm('Archived') : $type_title)));
	    }

	    $o['invoices']=$invoice_nr_row;
	}

	//for credit invoices - get the sales invoices to which are linked
	if($tblinvoice->f('type')=='2'){
		$invoice_nr_row = array();
		$inv_nr = $db->query("SELECT tblinvoice.id,tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.due_date, tblinvoice.paid,tblinvoice.f_archived  FROM tracking
									INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
									INNER JOIN tblinvoice ON tracking_line.origin_id = tblinvoice.id
									WHERE tracking.target_id = '".$tblinvoice->f('id')."' AND tracking.target_type='8'
									AND (tracking_line.origin_type='1' OR tracking_line.origin_type='7') ")->getAll();

		foreach($inv_nr as $key=>$value){
			$type_title='';
		    switch ($value['type']){
			case '0':
			    $type_title = gm('Regular invoice');
			    break;
			case '1':
			    $type_title = gm('Proforma invoice');
			    break;
			case '2':
			    $type_title = gm('Credit Invoice');
			    break;
		    }	    

		    if($value['sent'] == '0' && $value['type']!='2'){
				$type_title = gm('Draft');
		    }else if($value['status']=='1'){
				$type_title = $value['paid']=='2' ? gm('Partially Paid') : gm('Paid');
		    }else if($value['sent']!='0' && $value['not_paid']=='0'){
		    	if($value['status']=='0' && $value['due_date']< time()){
		    		$type_title = gm('Late');
		    	}else{
		    		$type_title =gm('Final');
		    		if($is_easy_invoice_diy){
						$jefacture_exported=$db->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$value['id']."' AND jefacture='1' AND `type`='0' ");
						if($jefacture_exported){
							$type_title = gm('Exported');
						}
					}
		    	}
				
		    } else if($value['sent']!='0' && $value['not_paid']=='1'){
				$type_title = gm('No Payment');
		    }

		    
	    	array_push($invoice_nr_row, array( 'serial' => ($value['f_archived']=='1' ? '': $value['serial_number']), 'id'=> $value['id'], 'status'=> ($value['f_archived']=='1' ? gm('Archived') : $type_title)));
	    }

	    $o['invoices']=$invoice_nr_row;
	}

	//for sales invoices - get the pro forma invoices that are linked to it
	if($tblinvoice->f('type')!='1'){
		$invoice_nr_row = array();
		$inv_nr = $db->query("SELECT tblinvoice.id, tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid, tblinvoice.due_date, tblinvoice.paid, tblinvoice.f_archived FROM tracking
					INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
					INNER JOIN tblinvoice ON tracking_line.origin_id = tblinvoice.id
					WHERE tracking.target_id = '".$tblinvoice->f('id')."'
					AND tracking_line.origin_type = '7'
					AND tracking.target_type = '1'")->getAll();

		foreach($inv_nr as $key=>$value){
			$type_title='';
		    switch ($value['type']){
			case '0':
			    $type_title = gm('Regular invoice');
			    break;
			case '1':
			    $type_title = gm('Proforma invoice');
			    break;
			case '2':
			    $type_title = gm('Credit Invoice');
			    break;
		    }	    

		    if($value['sent'] == '0' && $value['type']!='2'){
				$type_title = gm('Draft');
		    }else if($value['status']=='1'){
				$type_title = $value['paid']=='2' ? gm('Partially Paid') : gm('Paid');
		    }else if($value['sent']!='0' && $value['not_paid']=='0'){
		    	if($value['status']=='0' && $value['due_date']< time()){
		    		$type_title = gm('Late');
		    	}else{
		    		$type_title = gm('Final');
		    		if($is_easy_invoice_diy){
						$jefacture_exported=$db->field("SELECT invoice_id FROM tblinvoice_exported WHERE invoice_id='".$value['id']."' AND jefacture='1' AND `type`='0' ");
						if($jefacture_exported){
							$type_title = gm('Exported');
						}
					}
		    	}
				
		    } else if($value['sent']!='0' && $value['not_paid']=='1'){
				$type_title = gm('No Payment');
		    }

		    
	    	array_push($invoice_nr_row, array( 'serial' => ($value['f_archived']=='1' ? '': $value['serial_number']), 'id'=> $value['id'], 'status'=> ($value['f_archived']=='1' ? gm('Archived') : $type_title)));
	    }

	    $o['pro_invoices']=$invoice_nr_row;
	}
	

$sendgrid_notice_msg = '';
$sendgrid_cancel_schedule = false;

if($sendgrid_activated){
	$sendgrid_sent = $tblinvoice->f('sent_to_sendgrid');
	if($sendgrid_sent){
		$sendgrid_send_at_date = $tblinvoice->f('sendgrid_send_at_date');

		$sent_date_sg = new DateTime();
        $sent_date_sg->setTimestamp($sendgrid_send_at_date);
        //BELGIUM TIME
        $sent_date_sg->setTimezone(new DateTimeZone('Europe/Brussels'));

        $sendgrid_scheduled = $sent_date_sg->format('H:i');
        $sent_date_sg->modify('-10 minutes');
        $sendgrid_cancel_until = $sent_date_sg->format('H:i');

        $sendgrid_cancel_date = $sent_date_sg->format('Y-m-d H:i');

        $diff1 = new DateTime();
        //BELGIUM TIME
        $diff1->setTimezone(new DateTimeZone('Europe/Brussels'));

        $diff2 = new DateTime($sendgrid_cancel_date);
        //BELGIUM TIME
        $diff2->setTimezone(new DateTimeZone('Europe/Brussels'));
        $diff2->setTime($sent_date_sg->format('H'),$sent_date_sg->format('i'),0);

        $diff_interval = $diff1->diff($diff2);

        $y = $diff_interval->format('%y');
        $m = $diff_interval->format('%m');
        $d = $diff_interval->format('%d');
        $h = $diff_interval->format('%h');
        $i = $diff_interval->format('%i');
        $time_interval = $diff_interval->invert;

        //Show notice and Cancel Sending Button only if the time() < SendGrid Scheduled send -10 miutes
        if($y == 0 && $m == 0 && $d == 0 && $h == 0){
        	if($i <= 10 && $time_interval==0){
        		$sendgrid_cancel_schedule = true;
        		 $sendgrid_notice_msg = gm('This document is currently in the sending queue at the mailprovider with planned sending at').' '.$sendgrid_scheduled.'. '.gm('It is possible to cancel sending until').' '.$sendgrid_cancel_until.'.';
        	}
        }
	}
}

$o['sendgrid_notice_msg'] = $sendgrid_notice_msg;
$o['sendgrid_cancel_schedule'] = $sendgrid_cancel_schedule;
$o['sendgrid_batch_id'] = $tblinvoice->f('batch_id');
$o['jefacture_activated'] = $db->field("SELECT active FROM apps WHERE name='Jefacture' and type='main' and main_app_id='0' ") ? true : false;

$send_by_billtobox=false;
$billtobox_activated=$db->field("SELECT active FROM apps WHERE name='BillToBox' and type='main' and main_app_id='0'")? true : false;
if($billtobox_activated && defined('EXPORT_SEND_INVOICE_BILLTOBOX') && EXPORT_SEND_INVOICE_BILLTOBOX == '1'){
	$send_by_billtobox=true;
}
$o['send_by_billtobox']=$send_by_billtobox;

json_out($o);
?>