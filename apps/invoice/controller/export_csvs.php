<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$allow_order = array('name');
$l_r = ROW_PER_PAGE;

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$arguments_o='';
$arguments_s='';
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$arguments='';
$filter = 'WHERE 1=1 ';
$filter_link = 'block';
$order_by = "";

if(!empty($in['filter'])){
	$arguments.="&filter=".$in['filter'];
}
if(!empty($in['search'])){
	$filter.=" AND name LIKE '%".$in['search']."%' ";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['order_by']) && in_array($in['order_by'], $allow_order)){
	$order = " ASC ";
	if($in['desc'] == 'true'){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
}

$arguments = $arguments.$arguments_s;
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;

$tblinvoice = $db->query("SELECT * FROM tblinvoice_export_list ".$filter.$order_by );
$max_rows=$tblinvoice->records_count();
$result = array('query'=>array(),'max_rows'=>$max_rows);
$tblinvoice->move_to($offset*$l_r);
$j=0;
$all_invoices_id = '';
while($tblinvoice->move_next() && $j<$l_r){
	
	$item=array(
		'name'	    			=> $tblinvoice->f('name'),
		'id'					=> $tblinvoice->f('list_id'),
		'edit_link'				=> 'index.php?do=invoice-export&list_id='.$tblinvoice->f('list_id'),
		'info_link'				=> 'index.php?do=invoice-export_view&list_id='.$tblinvoice->f('list_id'),
		'delete_link'			=> array('do'=>'invoice-export_csvs-invoice-deleteExportList','list_id'=>$tblinvoice->f('list_id')),
	);
	$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('id');
	array_push($result['query'], $item);
	$j++;

}
	$all_invoices_id = trim($all_invoices_id,',');

	$result['view_go']		= $_SESSION['access_level'] == 1 ? true : false;
	$result['all_invoices_id']	= $all_invoices_id;
	$result['is_invoice']		= false;
	$result['lr']			= $l_r;	


return json_out($result);
?>