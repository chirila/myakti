<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class IncommingInvoiceEdit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);	

		$this->spliter=substr(ACCOUNT_DATE_FORMAT, 1, 1);	
	}

	public function getIncommingInvoice(){
		



		if($this->in['incomming_invoice_id'] == 'tmp'){ //add
			$this->getAddIncommingInvoice();
		}else{ # edit
			$this->getEditIncommingInvoice();
		}
	}
	
	private function getAddIncommingInvoice(){

		$in=$this->in;
        $output=array();


        /*if(!$in['invoice_date']){
			$in['invoice_date']=time();
			
		}else{
			$in['invoice_date']=strtotime($in['invoice_date']);
		
		}*/

		if($in['invoice_date'] && !is_numeric($in['invoice_date'])){
			$in['invoice_date']=strtotime($in['invoice_date']);	
		}

		/*if(!$in['due_date']){
			$in['due_date']= strtotime('+1 month', time());
			
		}else{
			$in['due_date']=strtotime($in['due_date']);
		
		}*/

		$output['link_url'] = $in['link_url'];
      
      if($in['link_url']){
         $output['view_upload'] = false;
         $output['exist_image'] = true;
         $output['exist_drop_image'] = false;
         $output['linkpath']='';
      	 $output['file_path']='';
      }else{
      	 $output['view_upload'] = true;
      	 $output['exist_image'] = false;
      	 $output['exist_drop_image'] = false;
      }

      	if($in['p_order_id']){
      		$purchase_order_data=$this->db->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");
      		if($purchase_order_data->next()){
      			$output['p_order_id']=$in['p_order_id'];
				if(!$in['buyer_id'] && $purchase_order_data->f('customer_id')){
					$in['buyer_id']=$purchase_order_data->f('customer_id');
				}
				if(!$in['invoice_line']){
					$in['invoice_line']=array();
					$total=0;
					$i=0;
					$lines=$this->db->query("SELECT * FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content!='1' ORDER BY sort_order ASC");
                    while($lines->next()){
						$sale_unit= $lines->f('sale_unit');
						if(!ALLOW_ARTICLE_SALE_UNIT){
							$sale_unit=1;
						}
						$packing= $lines->f('packing');
						if(!ALLOW_ARTICLE_PACKING){
							$packing=1;
							$sale_unit=1;
						}
						$q = $lines->f('quantity');
						$row_id = 'tmp'.$i;
						$price_line = $this->db->f('price')*$packing/$sale_unit;
						$line_total=$price_line * $q;
                        $line_discount=$lines->f('discount');
                        if($line_discount>0){
                            $line_total=$line_total-$line_total*$line_discount/100;
                        }
						$total += $line_total;
						$tmp_item=array(
							'tr_id' 	=> $row_id,
							'vat_line'	=> display_number(0),
	                		'total_no_vat'	=> display_number($line_total),
	                		'total'			=> display_number($line_total)
						);
						array_push($in['invoice_line'], $tmp_item);
						$i++;
					}
					$in['total']= display_number($total);
				}
			}
      	}

      $is_active = $this->db->field("SELECT active FROM apps WHERE name = 'Dropbox'");
	    if($is_active){     
		    $output['is_dropbox']=true;
		    /*$d = new drop('upload_invoices');
			$content=$d->getContent();
            if(!empty($content->entries)){
				$content_array=array('0'=>gm('Select file'));
				foreach ($content->entries as $key => $value) {
						$file_name=$value->name;
						$content_array[urlencode($value->path_display)]=$file_name;
						$mime_type[urlencode($file_name)]=$value->mime_type;
				}
			}*/
            $output['file_selected']   = $in['file_selected'] ? $in['file_selected'] : '0';
            $output['file_selected_dd']=array();
            //$output['file_selected_dd']= build_simple_dropdown($content_array,$in['file_selected']);
		}else{
		 	 $output['is_dropbox']=false;
		}

		$xml_due_date=false;
		if($in['file_path']){			
			if($in['linkpath']){
				$output['view_upload'] = false;
      	 		$output['exist_drop_image'] = true;
      	 		$output['linkpath']=$in['linkpath'];
      	 		$output['file_path']=$in['file_path'];
				$d = new drop('upload_invoices');
				if($in['drop_xml']){
					$output['drop_xml']=$in['drop_xml'];
					if(!$in['drop_file_path']){
						$temp_path=INSTALLPATH.'upload/tmp_file_'.time().'.xml';
						$d->getFile(urldecode($in['linkpath']),$temp_path);
						$file_content=file_get_contents($temp_path);
						unlink($temp_path);
						$targetPath = INSTALLPATH.'upload';
			           	$file_content=str_replace(array("cac:","cbc:"),"",$file_content);
			          	$xml_str=simplexml_load_string(stripslashes($file_content));
			          	$xml_str=json_decode(json_encode($xml_str), true);
			          	$pdf_file=$xml_str['AdditionalDocumentReference'][0]['Attachment']['EmbeddedDocumentBinaryObject'];
			          	if(!$pdf_file){
			              $pdf_file=$xml_str['AdditionalDocumentReference']['Attachment']['EmbeddedDocumentBinaryObject'];
			            }
			          	if($pdf_file){
				            $pdf_file=base64_decode(str_replace("\r\n","",$pdf_file));
				            $targetFile = rtrim($targetPath,'/') . '/tmp_file_'.time().'.pdf';
				            mkdir(str_replace('//','/',$targetPath), 0775, true);
				            file_put_contents($targetFile,$pdf_file);
				            if($xml_str['InvoiceLine'] && !empty($xml_str['InvoiceLine'])){
				              $in['type']='0';
				              $currency=$this->db->field("SELECT value FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
				              if(!$currency){
				                $currency='&euro;';
				              }
				              $in['invoice_number']=$xml_str['ID'];
				              $in['invoice_currency']=$currency;
				              $in['currency_type']=$this->db->field("SELECT id FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
				              $in['invoice_total_wo_vat']=display_number($xml_str['LegalMonetaryTotal']['TaxExclusiveAmount']);
				              $in['total_amount_due']=display_number($xml_str['LegalMonetaryTotal']['TaxInclusiveAmount']); 
				              $in['total']=$in['total_amount_due'];          
				              if($xml_str['InvoiceLine'] && array_keys($xml_str['InvoiceLine']) !== range(0, count($xml_str['InvoiceLine']) - 1)){
				                $in['invoice_row']=array();
				                $in['invoice_row'][0]=array(
				                    'article_code'        => $xml_str['InvoiceLine']['Item']['SellersItemIdentification'] ? $xml_str['InvoiceLine']['Item']['SellersItemIdentification']['ID'] : '',
				                    'row_description'     => $xml_str['InvoiceLine']['Item']['Description'],
				                    'row_quantity'        => display_number($xml_str['InvoiceLine']['InvoicedQuantity']),
				                    'row_unit_price'      => display_number($xml_str['InvoiceLine']['LineExtensionAmount']),
				                    'vat'                 => display_number($xml_str['InvoiceLine']['Item']['ClassifiedTaxCategory']['Percent']),
				                    'row_amount'          => display_number($xml_str['InvoiceLine']['Price']['PriceAmount']),
				                  );
				              }else{
				                $in['invoice_row']=array();
				                foreach($xml_str['InvoiceLine'] as $key=>$value){
				                    $item=array(
				                      'article_code'        => $value['Item']['SellersItemIdentification'] ? $value['Item']['SellersItemIdentification']['ID'] : '',
				                      'row_description'     => $value['Item']['Description'],
				                      'row_quantity'        => display_number($value['InvoicedQuantity']),
				                      'row_unit_price'      => display_number($value['LineExtensionAmount']),
				                      'vat'                 => display_number($value['Item']['ClassifiedTaxCategory']['Percent']),
				                      'row_amount'          => display_number($value['Price']['PriceAmount']),
				                    );
				                    array_push($in['invoice_row'],$item);
				                }
				              }
				              $in['invoice_vat_line']=array();
				              if(array_keys($xml_str['TaxTotal']['TaxSubtotal']) !== range(0, count($xml_str['TaxTotal']['TaxSubtotal']) - 1)){
				                $in['invoice_vat_line'][0]=array(
				                    'vat_line'        => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxCategory']['Percent']),
				                    'total_no_vat'    => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']),
				                    'total'           =>  display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']+$xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
				                    'vat_value'       => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
				                  );
				              }else{
				                foreach($xml_str['TaxTotal']['TaxSubtotal'] as $key=>$value){
				                  $line_vat=array(
				                      'vat_line'      => display_number($value['TaxCategory']['Percent']),
				                      'total_no_vat'   => display_number($value['TaxableAmount']),
				                      'total'           =>  display_number($value['TaxableAmount']+$value['TaxAmount']),
				                      'vat_value'       => display_number($value['TaxAmount']),
				                    );
				                  array_push($in['invoice_vat_line'],$line_vat);
				                }
				              }
				              $in['invoice_line']=$in['invoice_vat_line'];
				              if($xml_str['PaymentMeans']['PaymentID'] && !empty($xml_str['PaymentMeans']['PaymentID'])){
				                  $ogm_char=str_split($xml_str['PaymentMeans']['PaymentID']);
				                  $new_ogm="";
				                  for($o=0;$o<count($ogm_char);$o++){
				                    $new_ogm.=($o==3 || $o==7 ? '/'.$ogm_char[$o] : $ogm_char[$o]);
				                  }
				                  $in['ogm']=$new_ogm;
				              }           
				            }else if($xml_str['CreditNoteLine'] && !empty($xml_str['CreditNoteLine'])){
				              $in['type']='2';
				              $currency=$this->db->field("SELECT value FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
				              if(!$currency){
				                $currency='&euro;';
				              }
				              $in['invoice_currency']=$currency;
				              $in['currency_type']=$this->db->field("SELECT id FROM currency WHERE code='".$xml_str['DocumentCurrencyCode']."' ");
				              $in['invoice_total_wo_vat']=display_number($xml_str['LegalMonetaryTotal']['TaxExclusiveAmount']);
				              $in['total_amount_due']=display_number($xml_str['LegalMonetaryTotal']['TaxInclusiveAmount']);
				              $in['total']=$in['total_amount_due'];    
				              if($xml_str['CreditNoteLine'] && array_keys($xml_str['CreditNoteLine']) !== range(0, count($xml_str['CreditNoteLine']) - 1)){
				                $in['invoice_row']=array();
				                $in['invoice_row'][0]=array(
				                    'article_code'        => $xml_str['CreditNoteLine']['Item']['SellersItemIdentification'] ? $xml_str['CreditNoteLine']['Item']['SellersItemIdentification']['ID'] : '',
				                    'row_description'     => $xml_str['CreditNoteLine']['Item']['Description'],
				                    'row_quantity'        => display_number($xml_str['CreditNoteLine']['InvoicedQuantity']),
				                    'row_unit_price'      => display_number($xml_str['CreditNoteLine']['LineExtensionAmount']),
				                    'vat'                 => display_number($xml_str['CreditNoteLine']['Item']['ClassifiedTaxCategory']['Percent']),
				                    'row_amount'          => display_number($xml_str['CreditNoteLine']['Price']['PriceAmount']),
				                  );
				              }else{
				                $in['invoice_row']=array();
				                foreach($xml_str['CreditNoteLine'] as $key=>$value){
				                    $item=array(
				                      'article_code'        => $value['Item']['SellersItemIdentification'] ? $value['Item']['SellersItemIdentification']['ID'] : '',
				                      'row_description'     => $value['Item']['Description'],
				                      'row_quantity'        => display_number($value['CreditedQuantity']),
				                      'row_unit_price'      => display_number($value['LineExtensionAmount']),
				                      'vat'                 => display_number($value['Item']['ClassifiedTaxCategory']['Percent']),
				                      'row_amount'          => display_number($value['Price']['PriceAmount']),
				                    );
				                    array_push($in['invoice_row'],$item);
				                }
				              }
				              $in['invoice_vat_line']=array();
				              if(array_keys($xml_str['TaxTotal']['TaxSubtotal']) !== range(0, count($xml_str['TaxTotal']['TaxSubtotal']) - 1)){
				                $in['invoice_vat_line'][0]=array(
				                    'vat_line'      => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxCategory']['Percent']),
				                    'total_no_vat'        => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']),
				                    'total'           =>  display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxableAmount']+$xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
				                    'vat_value'       => display_number($xml_str['TaxTotal']['TaxSubtotal']['TaxAmount']),
				                  );
				              }else{
				                foreach($xml_str['TaxTotal']['TaxSubtotal'] as $key=>$value){
				                  $line_vat=array(
				                      'vat_line'      => display_number($value['TaxCategory']['Percent']),
				                      'total_no_vat'        => display_number($value['TaxableAmount']),
				                      'total'           =>  display_number($value['TaxableAmount']+$value['TaxAmount']),
				                      'vat_value'       => display_number($value['TaxAmount']),
				                    );
				                  array_push($in['invoice_vat_line'],$line_vat);
				                }
				              }
				              $in['invoice_line']=$in['invoice_vat_line'];
				            }
				            if($xml_str['AccountingSupplierParty']['Party']['EndpointID'] && !empty($xml_str['AccountingSupplierParty']['Party']['EndpointID']) && !$in['buyer_id']){
				              $buyer_info=$this->db->query("SELECT * FROM customers WHERE REPLACE( REPLACE( REPLACE( REPLACE( btw_nr, '.', '' ) , ' ', '' ) , '-', '' ) , '_', '' )='".$xml_str['AccountingSupplierParty']['Party']['EndpointID']."' LIMIT 1");
				              if($buyer_info->next()){
				                $in['buyer_id']=$buyer_info->f('customer_id');
				                $this->db->query("UPDATE customers SET is_supplier='1' WHERE customer_id='".$buyer_info->f('customer_id')."' ");
				              }else{
				                $output['buyer_data']=array(
				                    'name'        => $xml_str['AccountingSupplierParty']['Party']['PartyName']['Name'],
				                    'btw_nr'      => $xml_str['AccountingSupplierParty']['Party']['EndpointID'],
				                    'address'     => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['StreetName'], 
				                    'city'        => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['CityName'], 
				                    'zip'         => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['PostalZone'], 
				                    'country_id'  => $xml_str['AccountingSupplierParty']['Party']['PostalAddress']['Country']['IdentificationCode'] ? $this->db->field("SELECT country_id FROM country WHERE `code`='".$xml_str['AccountingSupplierParty']['Party']['PostalAddress']['Country']['IdentificationCode']."' ") : ACCOUNT_DELIVERY_COUNTRY_ID,
				                    'btw_nr'      => $xml_str['AccountingSupplierParty']['Party']['EndpointID'],
				                  );
				              }
				            }
				            if($xml_str['IssueDate']){
				            	$in['invoice_date']=strtotime($xml_str['IssueDate'])+10800;
				            }
				            if($xml_str['PaymentMeans']['PaymentDueDate']){
				            	$in['due_date']=strtotime($xml_str['PaymentMeans']['PaymentDueDate'])+10800;
				            	$xml_due_date=true;
				            }		          		
			          	}
			          	$output['drop_file_path'] = $targetFile;
					}else{
						$output['drop_file_path'] = str_replace("\\","",$in['drop_file_path']);
					}				
				}else{
					$target_path=INSTALLPATH.'upload/'. str_replace("\\","",str_replace('/','_',urldecode($in['linkpath'])));
					$d->getFile(urldecode($in['linkpath']),$target_path);
      				$output['drop_file_path'] = $target_path;
				}		
			}
		}


        $do_next='invoice-purchase_ninvoice-invoice-updateCustomerDataInc';
        $details = array('table' 			=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
							 'field'		=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
							 'value'		=> $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],						
							 'filter'		=> $in['buyer_id'] ? ' AND billing =1' : '',
							 );

			$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
			$buyer_details->next();

			$customer_id = $in['buyer_id'];

			if($in['buyer_id']){
				$buyer_info = $this->db->query("SELECT customers.payment_term, customers.fixed_discount, customers.btw_nr, customers.c_email,
						customer_legal_type.name as l_name,customers.currency_id,
						customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc,customers.name AS company_name,payment_term_type
						FROM customers
						LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
						WHERE customers.customer_id = '".$in['buyer_id']."' ");
				$buyer_info->next();
				$in['payment_term_type']=$buyer_info->f('payment_term_type');
				$in['payment_term']=$buyer_info->f('payment_term');
				if(!$xml_due_date && $in['old_buyer_id'] != $in['buyer_id'] && $in['invoice_date']){				
					if($buyer_info->f('payment_term_type')==1) { # invoice date
						$in['due_date'] = $in['invoice_date'] + ( $buyer_info->f('payment_term') * ( 60*60*24 )-1);
					} else { # next month first day
						$curMonth = date('n',$in['invoice_date']);
						$curYear  = date('Y',$in['invoice_date']);
					    $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
						$in['due_date'] = $firstDayNextMonth + ($buyer_info->f('payment_term') * ( 60*60*24 )-1);
					}
				}
				
				//$due = date(ACCOUNT_DATE_FORMAT,$in['invoice_date'] + ( $buyer_info->f('payment_term') * (60*60*24) ) );
				$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');
				$text = gm('Company Name').':';
				$c_email = $buyer_info->f('c_email');
				$c_fax = $buyer_details->f('comp_fax');
				$c_phone = $buyer_details->f('comp_phone');
				$buyer_vat_nr=$buyer_info->f('btw_nr');

				$in['discount_line_gen']= display_number($buyer_info->f('line_discount'));

				if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
					$apply_discount = '3';
				}else{
					if($buyer_info->f('apply_fix_disc')){
						$apply_discount = '2';
					}
					if($buyer_info->f('apply_line_disc')){
						$apply_discount = '1';
					}
				}
				$in['apply_discount'] = $apply_discount;
				$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
				$main_address_id			= $buyer_details->f('address_id');
				$sameAddress = true;
				/*if($buyer_details->f('billing')==1){
					$sameAddress = true;
				}*/

				if(!$in['currency_type']){
					$in['currency_type'] = $buyer_info->f('currency_id');
				}
				//$do_next='invoice-purchase_ninvoice-invoice-add_incomming';
			}else{
				$buyer_info = $this->db->query("SELECT phone, cell, email ,CONCAT_WS(' ',firstname,lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
				$buyer_info->next();
				$c_email = $buyer_info->f('email');
				$c_fax = '';
				$c_phone = $buyer_info->f('phone');
				$name = $buyer_info->f('name');
				$text = gm('Name').':';
				$buyer_vat_nr='';
				/*if($in['contact_id']){
					$do_next='invoice-purchase_ninvoice-invoice-add_incomming';
				}*/
			}

			$free_field = $buyer_details->f('address').'
					'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
					'.get_country_name($buyer_details->f('country_id'));
			if($in['delivery_address_id']){
				$sameAddress = false;
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
				$free_field = $buyer_addr->f('address').'
				'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
				'.get_country_name($buyer_addr->f('country_id'));
			}

					$name = stripslashes($name);
		$invoice_line_def=array(
				array(
					'tr_id' 	=> "tmp".time(),
	                'vat_line'	=> display_number(0),
	                'total_no_vat'	=> display_number(0),
	                'total'			=> display_number(0)
					)
				);

		$default_note = $this->db->field("SELECT value FROM default_data WHERE type='invoice_note' ");

        $output['main_address_id']				= $main_address_id;
		$output['sameAddress']					= $sameAddress;
        $output['invoice_date']              	= $in['invoice_date'] ? (is_numeric($in['invoice_date']) ? $in['invoice_date']*1000 : strtotime($in['invoice_date'])*1000) : '';
        $output['due_date']              	= $in['due_date'] ? (is_numeric($in['due_date']) ? $in['due_date']*1000 : strtotime($in['due_date'])*1000) : '';
        $output['booking_number']        = $in['type']==2 ? generate_binvoice_credit_number(): generate_binvoice_number();				
        $output['invoice_number']        = $in['invoice_number'];
				//Buyer Details
		//$output['add_customer']			= !$in['buyer_id'] && !$in['contact_id'] ? true : false;
		$output['add_customer']			= false;
		$output['buyer_id']                  	= $in['buyer_id'];
		$output['old_buyer_id']                 = $in['buyer_id'];
		$output['payment_term_type']			= $in['payment_term_type'];
		$output['payment_term']					= $in['payment_term'];
		$output['contact_id']                	= $in['contact_id'];
		$output['buyer_name']               	= $name;
		$output['buyer_country_id']          	= $buyer_details->f('country_id');
		$output['buyer_country_name']       	= get_country_name($buyer_details->f('country_id'));
		$output['buyer_state_id']            	= $buyer_details->f('state_id');
		$output['buyer_state_name']          	= get_state_name($buyer_details->f('state_id'));
		$output['buyer_city']                	= $buyer_details->f('city');
		$output['buyer_zip']                 	= $buyer_details->f('zip');
		$output['buyer_address']             	= $buyer_details->f('address');
		$output['buyer_email']             	= $c_email;
		$output['buyer_fax']             = $c_fax;
		$output['buyer_phone']           = $c_phone;
		$output['name_text']				= $text;
		$output['field']				    = $in['buyer_id'] ? 'customer_id' : 'contact_id';
		$output['free_field']			= $free_field;
		$output['free_field_txt']		= nl2br($free_field);
		$output['cc']					= $this->get_cc($in);
		$output['contacts']				= $this->get_contacts($in);
		$output['addresses']				= $this->get_addresses($in);
		$output['do_next']				= $do_next;
		$output['delivery_address_id']	=$in['delivery_address_id'];
		$output['type_dd']				= build_incinv_type_dd($in['type']);
		$output['type']			    	= $in['type'] ? $in['type'] : '0';
		$output['allow_disconto']   	= $in['allow_disconto'] ? true : false;
		$output['a_transfer']   		= $in['a_transfer'] ? 1 : 0;
		$output['disconto']				= $in['disconto'];
		$output['country_dd']			= build_country_list(0);
		$output['main_country_id']		= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$output['invoice_line']			= $in['invoice_line'] ? $in['invoice_line'] : $invoice_line_def;
		$output['ogm']					= $in['ogm'] ? $in['ogm'] : '';
		$output['err_ogm']				= false;
		$output['notes']                = $in['notes'] ? utf8_decode(stripslashes($in['notes'])) : ($default_note ? utf8_decode(stripslashes($default_note)) : '');
		$output['invoice_row']			= $in['invoice_row'];
		$output['invoice_currency']		= $in['invoice_currency'];
		$output['invoice_total_wo_vat']	= $in['invoice_total_wo_vat'];
		$output['total_amount_due']		= $in['total_amount_due'];
		$output['invoice_vat_line']		= $in['invoice_vat_line'];
		$output['total']				= $in['total'];
		$output['incomming_invoice_id'] = $in['incomming_invoice_id'];
		$output['currency_type']		= $in['currency_type'] ? $in['currency_type'] : ACCOUNT_CURRENCY_TYPE;

			if($in['contact_id']){
				$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
			}
	
		$output['generalvats']=build_vat_dd();	
		$output['expense_category']=build_expense_category_dd();

		$output['buyer_iban'] = $in['buyer_iban'];
      	$output['buyer_bic'] = $in['buyer_bic'];
      	$output['buyer_bank'] = $in['buyer_bank'];	
      	$output['reference'] = $in['reference'];

        $this->out = $output;
	}

	private function getGeneralNotes($invoice_id){

		$gen_notes = $this->db->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'purchase_invoice'
								   AND item_name = 'notes'
								   And active = '1'
								   AND lang_id = '1'
								   AND item_id = '".$invoice_id."' ");
		return $gen_notes;
	}
  
  private function getEditIncommingInvoice(){
		
		$in=$this->in;
		$page_title= gm('Edit Incomming Invoice');
		$do_next='invoice-purchase_ninvoice-invoice-update_incomming';
		$invoice_id=$in['invoice_id']?$in['invoice_id']:$in['incomming_invoice_id'];
// print_r($in);
		$incomming_invoice = $this->db->query("SELECT *
										FROM  tblinvoice_incomming
										WHERE invoice_id='".$invoice_id."'");

		if(!$incomming_invoice->next()){
			// unset($view);
			echo "string";
			return ark::run('invoice-purchase_invoices');
		}
		if(!$in['buyer_id']){
			$in['buyer_id'] = $incomming_invoice->f('supplier_id');
		}
		
		$buyer_id=$incomming_invoice->f('supplier_id');
		$customer_id = $incomming_invoice->f('supplier_id');
	
		if($in['buyer_id']){
			//get buyer details
			$buyer_details = $this->db->query("SELECT customers.customer_id,customers.name,customer_addresses.*, customer_legal_type.name AS l_name
				                    FROM customers
				                    LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.billing=1
				                    LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
		                            WHERE customers.customer_id='".$in['buyer_id']."'");
			$buyer_details->next();

			$free_field = $buyer_details->f('address').'
					'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
					'.get_country_name($buyer_details->f('country_id'));
		}	

		
		

		$contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
									WHERE contact_id='".$incomming_invoice->f('contact_id')."' AND customer_id='".$incomming_invoice->f('supplier_id')."'");

		$output=array(
			'main_address_id'				=> $incomming_invoice->f('main_address_id'),
			'sameAddress'					=> !$incomming_invoice->f('same_address') ? true : false,
	    	'page_title'            		=> $page_title,
			'incomming_invoice_id'  		=> $invoice_id,
		    'invoice_date'            	    => $incomming_invoice->f('invoice_date')*1000,
		    'due_date'            	        => $incomming_invoice->f('due_date')*1000,
		    'booking_number'                => $incomming_invoice->f('booking_number'), 
		    'invoice_number'                => $incomming_invoice->f('invoice_number'),   
		   
			'style'     				    => ACCOUNT_NUMBER_FORMAT,
			'do_next'					    => $do_next,
			
			'pick_date_format'      		=> pick_date_format(),
			'hide_currency2'		 		=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
			'hide_currency1'		 		=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
			
			'cc'						=> $this->get_cc($in),
			'contacts'					=> $this->get_contacts($in),
			'addresses'					=> $this->get_addresses($in),
			
			'buyer_name'                =>  $buyer_details->f('name'),
			'buyer_iban'             	=>  $incomming_invoice->f('buyer_iban'),
			'buyer_bic'             	=>  $incomming_invoice->f('buyer_bic'),
			'buyer_bank'           =>  $incomming_invoice->f('buyer_bank'),
		
			'buyer_id'                  => $incomming_invoice->f('supplier_id'),
			'contact_id'                => $incomming_invoice->f('contact_id'),
			
			'field'			            => $incomming_invoice->f('supplier_id')? 'customer_id' : 'contact_id',
			'contact_name'       	    => trim($contact_name->f('firstname').' '.$contact_name->f('lastname')),
				'free_field'		    => $incomming_invoice->f('free_field') ? $incomming_invoice->f('free_field') : $free_field,
			'free_field_txt'		    => nl2br($incomming_invoice->f('free_field') ? $incomming_invoice->f('free_field') : $free_field ),
			 'delivery_address_id'       => $incomming_invoice->f('same_address'),
			 'type_dd'			=> build_incinv_type_dd($incomming_invoice->f('type')),
			 'type'			    => $incomming_invoice->f('type') ? $incomming_invoice->f('type') : '0',
			 'allow_disconto'    => $incomming_invoice->f('allow_disconto') ? true : false,
			 'a_transfer'    => $incomming_invoice->f('a_transfer') ? 1 : 0,
			 'disconto'                =>  display_number($incomming_invoice->f('disconto')),
			 'country_dd'		=> build_country_list($incomming_invoice->f('buyer_country_id')),
			'main_country_id'			=> @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26',
			'ogm'				=> $incomming_invoice->f('ogm'),
			'err_ogm'			=> false,
			'notes'				=> $this->getGeneralNotes($invoice_id),
			'currency_type'		=> $incomming_invoice->f('currency_type') ? $incomming_invoice->f('currency_type') : ACCOUNT_CURRENCY_TYPE,
			'invoice_currency'	=> $incomming_invoice->f('currency_type') ? $this->db->field("SELECT value FROM currency WHERE id='".$incomming_invoice->f('currency_type')."' ") : $this->db->field("SELECT value FROM currency WHERE id='".ACCOUNT_CURRENCY_TYPE."' "),
			'reference'			=> stripslashes($incomming_invoice->f('reference'))
		);
		 $output['invoice_line']=array();
		 $total=0;
		 $lines=$this->db->query("SELECT tblinvoice_incomming_line.* FROM  tblinvoice_incomming_line where invoice_id='".$invoice_id."'");

             $i=0; 
           	 $row_id = 'tmp'.$i;
           while($lines->move_next()) {
               	$invoice_line=array(
					'tr_id'         		=> $row_id,
					'vat_line'  			=> display_number($lines->f('vat_line')),
					'total'  			    => display_number($lines->f('total')+$lines->f('total')*$lines->f('vat_line')/100 -($lines->f('total')+$lines->f('total')*$lines->f('vat_line')/100)*$incomming_invoice->f('disconto')/100),
					'total_no_vat'  	    => display_number($lines->f('total')),

					);
           	  array_push($output['invoice_line'], $invoice_line);
           	  $total+=$lines->f('total')+$lines->f('total')*$lines->f('vat_line')/100 -($lines->f('total')+$lines->f('total')*$lines->f('vat_line')/100)*$incomming_invoice->f('disconto')/100;
           }  
        $output['generalvats']=build_vat_dd();
        $output['expense_category_id']= $incomming_invoice->f('expense_category_id');
        $output['expense_category']=build_expense_category_dd($output['expense_category_id']);
       	$output['total']= display_number($total);
       	$output['invoice_row']=array();
       	$output['invoice_vat_line']=array();
       	$output['invoice_total_wo_vat']=0;
       	$output['total_amount_due']=0;
       	$pdf_vat_percent = array();
       	$pdf_sub_t = array();
       	$pdf_lines=$this->db->query("SELECT * FROM tblinvoice_incomming_pdf_line where invoice_id='".$invoice_id."' ORDER BY sort_order ASC");
       	while($pdf_lines->next()){
       		$pdf_item=array(
       			'article_code'        => stripslashes($pdf_lines->f('item_code')),
                'row_description'     => stripslashes(htmlspecialchars_decode($pdf_lines->f('name'),ENT_QUOTES)),
                'row_quantity'        => display_number($pdf_lines->f('quantity')),
                'row_unit_price'      => display_number($pdf_lines->f('price')),
                'vat'                 => display_number($pdf_lines->f('vat')),
                'row_amount'          => display_number($pdf_lines->f('amount')),
       		);
       		array_push($output['invoice_row'],$pdf_item);
       		$pdf_vat_percent[$pdf_lines->f('vat')]+=($pdf_lines->f('amount') * $pdf_lines->f('vat')/100);;
       		$pdf_sub_t[$pdf_lines->f('vat')] += $pdf_lines->f('amount');
       		$output['invoice_total_wo_vat']+=$pdf_lines->f('amount');
       		$output['total_amount_due']+=($pdf_lines->f('amount')+($pdf_lines->f('amount') * $pdf_lines->f('vat')/100));
       	}
       	$output['invoice_total_wo_vat']=display_number($output['invoice_total_wo_vat']);
       	$output['total_amount_due']=display_number($output['total_amount_due']);
       	foreach($pdf_vat_percent as $key=>$value){
       		$pdf_vat_line=array(
				'vat_line'   			=> display_number($key),
				'total_no_vat'	   	 	=> display_number($pdf_sub_t[$key]),
				'vat_value'	   			=> display_number($value),
			);			
			array_push($output['invoice_vat_line'], $pdf_vat_line);
       	}
	    $is_active = $this->db->field("SELECT active FROM apps WHERE name = 'Dropbox'");
      	global $config;
      	ark::loadLibraries(array('aws'));
	  	$aws = new awsWrap(DATABASE_NAME);
	  	$exist =  $aws->doesObjectExist($config['awsBucket'].DATABASE_NAME.'/'.$incomming_invoice->f('file_name'));
	  	if($incomming_invoice->f('amazon') || (!$incomming_invoice->f('file_path') && !$incomming_invoice->f('pdf_path'))){
	      $link ='';
	         if($exist){
		       $link = $aws->getLink($config['awsBucket'].DATABASE_NAME.'/'.$incomming_invoice->f('file_name'));
	         }
	    	 $output['is_amazon'] = true;
	     	 $output['link_url'] = $link;
		     $output['exist_image'] = $link ? true : false;
		     $output['view_upload'] = $link ? false : true;
		     if(!$link && $is_active){
		     	/*$d = new drop('upload_invoices');
				$content=$d->getContent();
		        if(!empty($content->entries)){
					$content_array=array('0'=>gm('Select file'));
					foreach ($content->entries as $key => $value) {
						$file_name=$value->name;
						$content_array[urlencode($value->path_display)]=$file_name;
						$mime_type[urlencode($file_name)]=$value->mime_type;
					}
				}*/
				$output['file_selected']='0';
		        //$output['file_selected_dd']= build_simple_dropdown($content_array,$in['file_selected']);
		        $output['file_selected_dd']=array();
			    $output['is_dropbox']=true;	
		     }
        }else{	    
	        if($is_active){	  
		     	$output['is_dropbox']=true;	    
				if(!$in['file_path']){
					$in['file_path']=$incomming_invoice->f('file_path');
				}
				if($in['file_path']){
					$d = new drop('invoices');
					$link=$d->getLink(urldecode($incomming_invoice->f('file_path')));
					if(!$link){				
						$output['folder']	    = DATABASE_NAME;
						$output['supplier_id']	= $incomming_invoice->f('supplier_id');
						$output['contact_id']   = $incomming_invoice->f('contact_id');
						$output['serial']		= $incomming_invoice->f('invoice_number');				
					}else{				
						if($incomming_invoice->f('pdf_path')){
							@mkdir('upload/'.DATABASE_NAME.'/temp_pdfs/',0775,true);
							//$f = fopen("upload/".DATABASE_NAME."/temp_pdfs/".urldecode($incomming_invoice->f('file_name')), 'w+b');
							$f="upload/".DATABASE_NAME."/temp_pdfs/".urldecode($incomming_invoice->f('file_name'));
							$d->getFile(urldecode($incomming_invoice->f('file_path')), $f);
							//fclose($f);				
							$output['link_url']     ="upload/".DATABASE_NAME."/temp_pdfs/".urldecode($incomming_invoice->f('file_name'));						
						}else{
							$n = $incomming_invoice->f('file_name');
							if(!$n){
								$a = explode('/', urldecode($incomming_invoice->f('file_path')));
								$b = count($a);
								$n = $a[$b-1];
							}
							@mkdir('upload/'.DATABASE_NAME.'/temp_pdfs/',0775,true);
							//$f = fopen("../upload/".DATABASE_NAME."/temp_pdfs/".$n, 'w+b');
							$f = "upload/".DATABASE_NAME."/temp_pdfs/".$n;
							$d->getFile(urldecode($incomming_invoice->f('file_path')), $f);
							//fclose($f);
							$ext = pathinfo("upload/".DATABASE_NAME."/temp_pdfs/".$n,PATHINFO_EXTENSION);		
							$output['link_url']		="upload/".DATABASE_NAME."/temp_pdfs/".$n;			
							$output['is_pdf']		= $ext == 'pdf' ? '' :'hide';
							$output['is_not_path']	= $ext == 'pdf' ? 'hide' :'';		
						}
					}
				}else{		
					$output['folder']			= DATABASE_NAME;
					$output['supplier_id']		= $incomming_invoice->f('supplier_id');
					$output['contact_id']		= $incomming_invoice->f('contact_id');
					$output['serial']			= $incomming_invoice->f('invoice_number');		
				}
		  	}else{
			   $output['is_dropbox']=false;  
	     	}
	    }
			
		$this->out = $output;
	}

	public function get_cc($in){
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 AND customers.is_supplier='1' ";
		//$filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			//$filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}
		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		
		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id	
			ORDER BY name
			LIMIT 9")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				//"id"					=> $value['cust_id'].'-'.$value['contact_id'],
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				//"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"bottom"				=> '',
				 "right"					=> ''
				/*"right"					=> $value['acc_manager_name']*/
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts($in) {
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customer_contacts.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 5")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
				'bottom' 				=> '',
			);

		}

		$added = false;
		if(count($result)==5){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in){
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 5");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 5");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		"symbol"				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==5){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

	public function get_dropboxFiles(&$in){
		$res=array('file_selected_dd'=>array());
		$is_active = $this->db->field("SELECT active FROM apps WHERE name = 'Dropbox'");
	    if($is_active){     
		    $d = new drop('upload_invoices');
			$content=$d->getContent();
            if(!empty($content->entries)){
				$content_array=array('0'=>gm('Select file'));
				foreach ($content->entries as $key => $value) {
					$file_name=$value->name;
					$content_array[urlencode($value->path_display)]=$file_name;
					$mime_type[urlencode($file_name)]=$value->mime_type;
				}
			}
            $res['file_selected_dd']= build_simple_dropdown($content_array,$in['file_selected']);
		}
		return $res;
	}

}

$incomminginvoice = new IncommingInvoiceEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $incomminginvoice->output($incomminginvoice->$fname($in));
	}

	$incomminginvoice->getIncommingInvoice();
	$incomminginvoice->output();
