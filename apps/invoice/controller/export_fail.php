<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
	$result=array('list'=>array());
	$l_r =ROW_PER_PAGE;
	$result['lr']=$l_r;
	$result['app']=$in['app'];

	if((!$in['offset']) || (!is_numeric($in['offset']))){
		$offset=0;
	}else{
		$offset=$in['offset']-1;
	}
	$limit=$offset*$l_r.','.$l_r;

	switch ($in['app']) {
		case 'yuki':
			$thing = 'yuki';
			break;
		case 'codabox':
			$thing = 'codabox';
			break;
		case 'clearfacts':
			$thing = 'clearfacts';
			break;	
		case 'billtobox':
			$thing = 'billtobox';
			break;
		case 'winbooks':
			$thing = 'btb';
			break;
		case 'exact_online':
			$thing = 'exact_online';
			break;
		case 'jefacture':
			$thing = 'jefacture';
			break;
	}

	$total_errors=$db->field("SELECT COUNT(id) FROM error_call WHERE app='".$thing."' AND module='invoice' ");
	$max_rows=$total_errors;
	$result['max_rows']=$max_rows;

	$errors=$db->query("SELECT error_call.*,tblinvoice.serial_number FROM error_call 
		INNER JOIN tblinvoice ON error_call.module_id=tblinvoice.id
		WHERE error_call.app='".$thing."' AND error_call.module='invoice' ORDER BY date DESC LIMIT ".$limit." ");
	while($errors->next()){
		$item=array(
			'serial_number'		=> $errors->f('serial_number'),
			'date'			=> date(ACCOUNT_DATE_FORMAT,$errors->f('date')),
			'message'			=> stripslashes($errors->f('message')),
		);
		array_push($result['list'], $item);
	}

	json_out($result);
?>