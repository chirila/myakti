<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps;
global $p_access;
perm::model('all','all', 'admin',in_array($apps['quote'], $p_access));
perm::model('all','all', 'user',in_array($apps['quote'], $p_access));

if(in_array($apps['company'], $p_access)) {
	perm::model('opportunity','all','user',in_array($apps['quote'], $p_access));
	perm::model('opportunity','all','admin',in_array($apps['quote'], $p_access));
}
if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::model('all','all', 'admin',false);
	perm::model('all','all', 'user',false);
}