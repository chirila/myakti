<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $config;
$db5 = new sqldb();
$quote_data = $db5->query("SELECT preview, show_grand, show_vat, discount, apply_discount, vat FROM tblquote WHERE id = '".$in['id']."' ");
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");
$version_data =$db5->field("SELECT preview FROM tblquote_version WHERE quote_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");

if($version_data == 1 && !$in['save_as']){

	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);
	if(ark::$model == 'quote' && (ark::$method == 'send' || ark::$method == 'sendNewEmail')){
		$in['attach_file_name'] = 'quote_'.$in['id'].'_'.$in['version_id'].'.pdf';
		$file = $aws->getItem('quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf',$in['attach_file_name']);
		if($file === true){
			return;
		}else{
			$in['attach_file_name'] = null;
		}
	}else{		
		$link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf');
		$content = file_get_contents($link);
		$q_serial_number=$db->field("SELECT serial_number FROM tblquote WHERE id='{$in['id']}' ");

		if($q_serial_number){
             $name=$q_serial_number.'.pdf';
		}else{
			$name='quote_'.$in['id'].'.pdf';
		}
		if($content){
			doQueryLog();
			if($in['just_download']){
				header('Content-Type: application/pdf');
				header("Content-Disposition:attachment;filename=".$name);
				echo $content;
				exit;
			}
			header('Content-Type: application/pdf');
			header("Content-Disposition:inline;filename=".$name);
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $content;
			exit();
		}else{
			$in['upload']='0';
		}
	}
}


ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetFont('helvetica', '', 10, '', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Quote');
$pdf->SetSubject('Quote');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


$pdf->setLanguageArray($l);


$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);


$pdf->AddPage();

	$pdf->SetMargins(13, 0, 13,0);
	$pdf->setPrintHeader(true);
	$pdf->SetAutoPageBreak(TRUE, 0);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->SetJPEGQuality(1);

	$img_file = K_PATH_IMAGES.'background-mastert.jpg';
	$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
	$pdf->setPageMark();

$pdf->SetAutoPageBreak(TRUE, 21);



$htmll = include('print_body_b7b2acca_6944_a954_7151a7548069.php');
$pdf->SetTopMargin(45);
$pdf->SetLeftMargin(13);
$pdf->SetMargins(13, 15, 13, 0);

$pdf->writeHTML($htmll, true, false, true, false, '');

$pdf->lastPage();

$in['last_page'] = $pdf->getPage();

if(isset($in['print'])){
	echo "<pre>";
	echo $htmll;
	exit();
}

if($in['save_as'] == 'F'){
	#we need to delete all the old images
	$img = glob(__DIR__.'/../../../../upload/'.DATABASE_NAME.'/quote_cache/quote_'.$in['id'].'_'.$in['version_id'].'_*.png');
	foreach ($img as $filename) {
		@unlink($filename);
	}
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$a->deleteItems($config['awsBucket'].DATABASE_NAME.'/quote_cache/quote_'.$in['id'].'_'.$in['version_id'].'_');
	$in['quote_pdf_name'] = 'quote_'.time().'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['quote_pdf_name'], 'F');
	unlink(__DIR__.'/../../../'.$in['quote_pdf_name']);
}else if($in['upload'] == '0'){
	$in['quote_pdf_name'] = 'quote_'.unique_id(32).'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['quote_pdf_name'], 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['quote_pdf_name'];
	$pdfFile = 'quote/quote_'.$in['id'].'_'.$in['version_id'].".pdf";
	$a->uploadFile($pdfPath,$pdfFile);
	unlink($in['quote_pdf_name']);
	$db->query("UPDATE tblquote_version SET preview='1' WHERE quote_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");
	$pdf->Output($buyer_reference.' '.$serial_number.' ['.$version_code.'].pdf','I');
}else if($in['do']=='quote-print'){
	doQueryLog();
	$pdf->Output($buyer_reference.' '.$serial_number.' ['.$version_code.'].pdf','I');
}else{
	$pdf->Output(__DIR__.'/../../../'.'quote_.pdf', 'F');
}
