<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

/*	function get_logos($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'logos'=>array(), 'logo'=>array());
		$default_logo = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_QUOTE' ");
		if($default_logo == '' || $default_logo == 'pim/img/no-logo.png')   {
		   	array_push($data['logo'], array(
        'account_logo'=>'images/no-logo.png',
        'default'=>'images/no-logo.png'
        ));
		}
		else {
			$logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/{q_logo_img}_*',GLOB_BRACE);
		    foreach ($logos as $v) {
		        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
		        $size = @getimagesize($logo);
		        $ratio = 250 / 77;
		        if($size[0]/$size[1] > $ratio ){
		            $attr = 'width="250"';
		        }else{
		            $attr = 'height="77"';
		        }
		        array_push($data['logos'], array(
		        'account_logo'=>$logo,
		        'default'=>$default_logo,
		        'attr' => $attr
		        ));
		    }
		}
		$data['default_logo'] = $default_logo;


			
		return json_out($data, $showin,$exit);
	}*/

	function get_PDFlayout($in,$showin=true,$exit=true){
		$db = new sqldb();

		global $database_config;
		$database = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
				);
		$dbu_users =  new sqldb($database);

		$data = array( 'layout_header'=>array(),'custom_layout'=>array(),'layout_body'=>array(),'layout_footer'=>array());
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_PDF_FORMAT' ");
		$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
		//$in['identity_id'] = $db->field("SELECT value FROM settings WHERE constant_name='QUOTE_IDENTITY_SET' ");
		if(!$in['identity_id']){
			// $identity = $db->field("SELECT identity_id FROM multiple_identity");
			$in['identity_id']='0';
		}
		$data['font_pdf'] = $tz=array(
								    array('id' => "1" , 'name' => gm("Helvetica Neue")),
							        array('id' => "2" , 'name' => gm("Roboto")),
							        array('id' => "3" , 'name' => gm("Lato")),
							        array('id' => "4" , 'name' => gm("Vollkorn")),
							        array('id' => "5" , 'name' => gm("Open Sans")),
							        array('id' => "6" , 'name' => gm("Ubuntu")),
						    );
		$data['font_pdf_id'] = '';
		$existIdentity = $db->query("SELECT identity_id FROM multiple_identity  ");
		$data['multiple_identity']				= build_identity_dd($in['identity_id']);
		$data['identity']						= $in['identity_id'];
		$data['use_custom_layout']= ($use ==1)? true:false;
/*		for ($i=1; $i <= 5; $i++) {

			if(!$in['identity_id']){
				array_push($data['layout'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&type='.$i.'&lid='.$language,
					'img_href'						=> '../pim/img/type-'.$i.'_i.jpg',
					'type'							=> $i,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def == $i && $use == 0 ? 'active' : '',
				));
			}else{
				array_push($data['layout'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&type='.$i.'&lid='.$language,
					'img_href'						=> '../pim/img/type-'.$i.'_i.jpg',
					'type'							=> $i,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_id']."' and module='quote'")==$i ? 'selected' : '',
				));
			}
		}*/
		for ($j=1; $j <= 1; $j++) {
		array_push($data['custom_layout'], array(
				'view_invoice_custom_pdf' 						=>'index.php?do=quote-print&custome_type='.$j.'&lid='.$language,
				'img_href_custom'								=> 'images/custom_type-'.$j.'.jpg',
				'custom_type'									=> $j,
				//'action_href'									=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$j.'&tab=6',
				'selected_custom'								=> $def == $j && $use == 1 ? 'active' : '',
			));
		}
		$page = $db->field("SELECT value FROM settings WHERE constant_name='USE_QUOTE_PAGE_NUMBERING'");
		$stamp = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_QUOTE_STAMP_SIGNATURE'");
		$data['use_page_numbering']					= $page == '1' ? true : false;
		$data['show_stamp']							= $stamp == '1' ? true : false;
		//

		$pdf_active = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_PDF'");
		$pdf_note = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_NOTE'");
		$pdf_conditions = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_CONDITIONS'");
		$pdf_stamp = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_SIGNATURE'");
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$data['lang_id']=$language;
		$data['pdf_active'] = $pdf_active == '1' ? true : false;
		$data['pdf_note'] = $pdf_note == '1' ? true : false;
		$data['pdf_condition'] = $pdf_conditions == '1' ? true : false;
		$data['pdf_stamp'] = $pdf_stamp == '1' ? true : false;

		//$data['databasename']=DATABASE_NAME;
		$data['has_custom_layout']=false;
		if(DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11' || DATABASE_NAME=='9f56e245_4434_157d_9f06320b4bb8' || DATABASE_NAME=='a0be249a_ee94_e160_0a8ee28f1b15' || DATABASE_NAME=='b7b2acca_6944_a954_7151a7548069' || DATABASE_NAME=='caff4267_64a4_1db2_d49f2ab3760c'){
			$data['has_custom_layout']=true;
		}
		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$number_type = $_SESSION['u_id']<10737 ? 2 : 1;

		//$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		if(!$user->f('main_user_id')){
			if($user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $_SESSION['u_id']<10737 ? 2 : 1;
			}
		}else{
			$main_user= $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$user->f('main_user_id')."' ");
			if($main_user->f('migrated')){
				$number_type=1;
			}else{
				$number_type = $user->f('main_user_id')<10737 ? 2 : 1;
			}
		}
		
		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$number_type.'&lid='.$language.'&header='.$data['header'].'&header_id='.$def_header,
					'img_href'						=> 'images/type_header-'.$number_type.'.jpg',
					'type'							=> $def_header,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$number_type.'&lid='.$language.'&body='.$data['body'],
					'img_href'						=> 'images/type_body-'.$number_type.'.jpg',
					'type'							=> $number_type,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',
				));
		}
		if($number_type==2){
			for ($i=$def_footer; $i <= $def_footer; $i++) {
					array_push($data['layout_footer'], array(
						'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$i.'&lid='.$language.'&footer='.$data['footer'].'&footer_id='.$i,
						'img_href'						=> 'images/type_footer-'.$def_footer.'.jpg',
						'type'							=> $i,
						'footer'						=> 'footer',
						//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
						'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
					));
			}
		}else{
			for ($i=2; $i <= 2; $i++) {
					array_push($data['layout_footer'], array(
						'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$i.'&lid='.$language.'&footer='.$data['footer'].'&footer_id='.$i,
						'img_href'						=> 'images/type_footer-2.jpg',
						'type'							=> $i,
						'footer'						=> 'footer',
						//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
						'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
					));
			}			
		}


		$data['selected_header'] = $def_header;
		$data['selected_body'] = $def_body;
		$data['selected_footer'] = $def_footer;
		//

		return json_out($data, $showin,$exit);
	}
	function get_CustomizePDF($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('layout'=>array(), 'layout_footer'=>array());
		$pdf_active = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_PDF'");
		$pdf_note = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_NOTE'");
		$pdf_conditions = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_CONDITIONS'");
		$pdf_stamp = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_SIGNATURE'");
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$data['pdf_active'] = $pdf_active == '1' ? true : false;
		$data['pdf_note'] = $pdf_note == '1' ? true : false;
		$data['pdf_condition'] = $pdf_conditions == '1' ? true : false;
		$data['pdf_stamp'] = $pdf_stamp == '1' ? true : false;
		$data['layouts']=array();
		for($i=0;$i<6;$i++){
			if($i==0){
				array_push($data['layouts'], array('key'=>$i,'layout'=>gm('Select')));
			}else{
				array_push($data['layouts'], array('key'=>$i,'layout'=>'layout'.$i));
			}		
		}

		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['footer']='footer';

		for ($i=1; $i <= 5; $i++) {
				array_push($data['layout'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$i.'&lid='.$language.'&header='.$data['header'].'&footer='.$data['footer'],
					'img_href'						=> '../pim/img/type-'.$i.'_i.jpg',
					'type'							=> $i,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def == $i && $use == 0 ? 'active' : '',
				));
		}
		for ($j=1; $j <= 5; $j++) {
				array_push($data['layout_footer'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$j.'&lid='.$language.'&footer='.$data['footer'].'&header='.$data['header'],
					'img_href'						=> '../pim/img/type-'.$j.'_i.jpg',
					'type'							=> $j,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def == $j && $use == 0 ? 'active' : '',
				));
		}

		return json_out($data, $showin,$exit);
	}
	function get_Convention($in,$showin=true,$exit=true){
		$db = new sqldb();
		$account_number = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_START' ");
		$account_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_DIGIT_NR' ");
		$account_reference = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_REF' ");
		$account_del = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_DEL' ");
		$ACCOUNT_QUOTE_WEIGHT = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_WEIGHT' ");
		$data['account_number']=$account_number;
		$data['account_digits']=$account_digits;
		$data['example']=$account_number.str_pad(1,$account_digits,"0",STR_PAD_LEFT);
		$data['reference'] = $account_reference == '1' ? true : false;
		$data['del'] =$account_del;
		$data['quote_weight']=$ACCOUNT_QUOTE_WEIGHT==1 ? true : false;

		return json_out($data, $showin,$exit);
	}
	function get_general($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('general_condition'=>array());
		$filter = '';
		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
			if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
			}
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
				$filter = '_'.$in['languages'];
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
				if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
				}
			}
		}
		$default_table = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='quote_note".$filter."' ");
		$terms = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='quote_terms".$filter."' ");
		$page_general_condition = $db->field("SELECT value FROM settings WHERE constant_name='QUOTE_GENERAL_CONDITION_PAGE'");
		$data['general_condition']= array(
			'terms'			=> $terms,
			'notes'			=> $default_table,
			'translate_cls'	=> 'form-language-'.$code,
			'languages'		=>  $in['languages'],
			'page'			=>	$page_general_condition == '1' ? true : false,
			'do'			=> 'quote-settings-quote-default_language',
			'xget'			=> 'general',
			'language_dd' 	=>  build_language_dd_new($in['languages']),
		);
		return json_out($data, $showin,$exit);
	}

	function get_customLabel($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'customLabel'=>array());

		$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
		while($pim_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 		=> gm($pim_lang->f('language')),
			'do'												=> 'quote-settings',
			'xget'											=> 'labels',
			'label_language_id'	     		=> $pim_lang->f('lang_id'),
			'label_custom_language_id'	=> '',
			));
		}
		$pim_custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
		while($pim_custom_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 				=> $pim_custom_lang->f('language'),
			'do'														=> 'quote-settings',
			'xget'													=> 'labels',
			'label_custom_language_id'	    => $pim_custom_lang->f('lang_id'),
			'label_language_id'	     				=> '',
			));
		}

		return json_out($data, $showin,$exit);
	}
	function get_labels($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$const = array();


		$table = 'label_language_quote';

		if($in['label_language_id'])
		{
			$filter = "WHERE label_language_id='".$in['label_language_id']."'";
			$id = $in['label_language_id'];
		}elseif($in['label_custom_language_id'])
		{
			$filter = "WHERE lang_code='".$in['label_custom_language_id']."'";
			$id = $in['label_custom_language_id'];
		}

		$db->query("SELECT * FROM $table $filter");

		$data['labels']=array(
			'quote_number'					=> $db->f('quote'),
			'company_name'					=> $db->f('company_name'),
			'comp_reg_number'				=> $db->f('comp_reg_number'),
			'billing_address'				=> $db->f('billing_address'),
			'quote'							=> $db->f('quote_note'),
			'subject'						=> $db->f('subject'),
			'buyer_delivery_address' 		=> $db->f('buyer_delivery_address'),
			'buyer_delivery_zip' 			=> $db->f('buyer_delivery_zip'),
			'buyer_delivery_city' 			=> $db->f('buyer_delivery_city'),
			'buyer_delivery_country' 		=> $db->f('buyer_delivery_country'),
			'buyer_main_address' 			=> $db->f('buyer_main_address'),
			'buyer_main_zip' 				=> $db->f('buyer_main_zip'),
			'buyer_main_city' 				=> $db->f('buyer_main_city'),
			'buyer_main_country' 			=> $db->f('buyer_main_country'),
			'bank_details' 					=> $db->f('bank_details'),
			'item'							=> $db->f('item'),
			'subtotal' 						=> $db->f('subtotal'),
			'unit_price' 					=> $db->f('unit_price'),
			'unit_price_vat'				=> $db->f('unit_price_vat'),
			'vat' 							=> $db->f('vat'),
			'amount_due' 					=> $db->f('amount_due'),
			'notes' 						=> $db->f('notes'),
			'bic_code'						=> $db->f('bic_code'),
			'phone' 						=> $db->f('phone'),
			'email' 						=> $db->f('email'),
			'our_ref' 						=> $db->f('our_ref'),
			'vat_number' 					=> $db->f('vat_number'),
			'package' 						=> $db->f('package'),
			'article_code' 					=> $db->f('article_code'),
			'chapter_total' 				=> $db->f('chapter_total'),
			'subtotal_2' 					=> $db->f('subtotal_2'),
			'reference'	  					=> $db->f('reference'),
			'date' 							=> $db->f('date'),
			'customer'					  	=> $db->f('customer'),
			'quantity'						=> $db->f('quantity'),
			'amount'						=> $db->f('amount'),
			'discount'			 			=> $db->f('discount'),
			'payments'			 			=> $db->f('payments'),
			'grand_total'			 		=> $db->f('grand_total'),
			'bank_name'			 			=> $db->f('bank_name'),
			'iban'			 				=> $db->f('iban'),
			'fax'			 				=> $db->f('fax'),
			'url'			 				=> $db->f('url'),
			'your_ref'			 			=> $db->f('your_ref'),
			'sale_unit'			 			=> $db->f('sale_unit'),
			'author'						=> $db->f('author'),
			'general_conditions'			=> $db->f('general_conditions'),
			'page'			 				=> $db->f('page'),
			'valid_until'			 		=> $db->f('valid_until'),
			'stamp_signature'			 	=> $db->f('stamp_signature'),
			'download_webl'			 		=> $db->f('download_webl'),
			'name_webl'			 			=> $db->f('name_webl'),
			'addCommentLabel_webl'			=> $db->f('addCommentLabel_webl'),
			'quote_webl'			 		=> $db->f('quote_webl'),
			'accept_webl'			 		=> $db->f('accept_webl'),
			'reject_webl'			 		=> $db->f('reject_webl'),
			'bankDetails_webl'			 	=> $db->f('bankDetails_webl'),
			'cancel_webl'			 		=> $db->f('cancel_webl'),
			'bicCode_webl'			 		=> $db->f('bicCode_webl'),
			'bank_webl'			 			=> $db->f('bank_webl'),
			'pdfPrint_webl'			 		=> $db->f('pdfPrint_webl'),
			'email_webl'			 		=> $db->f('email_webl'),
			'submit_webl'			 		=> $db->f('submit_webl'),
			'date_webl'			 			=> $db->f('date_webl'),
			'requestNewVersion_webl'		=> $db->f('requestNewVersion_webl'),
			'status_webl'			 		=> $db->f('status_webl'),
			'payWithIcepay_webl'			=> $db->f('payWithIcepay_webl'),
			'regularInvoiceProForma_webl'	=> $db->f('regularInvoiceProForma_webl'),
			'ibanCode_webl'			 		=> $db->f('ibanCode_webl'),
			'noData_webl'			 		=> $db->f('noData_webl'),
			'discount_total'			 	=> $db->f('discount_total'),
			'no_vat'			 			=> $db->f('no_vat'),
			'do'							=> 'quote-settings-quote-label_update',
			'xget'							=> 'labels',
			'label_language_id'				=> $id
		);

		return json_out($data, $showin,$exit);
	}
	function get_emailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Quote Nr");
		$array_values['QUOTE_DATE']=gm("Invoice Date");
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['CONTACT_FIRST NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST NAME']=gm("Contact last name");
		$array_values['DISCOUNT']=gm("Discount Percent");
		$array_values['DISCOUNT_VALUE']=gm("Discount Value");
		$array_values['AMOUNT_DUE']=gm("Amount due");
		$array_values['WEB_LINK']=gm("Web Link");
		$array_values['SUBJECT']=gm("Subject");
		$array_values['WEB_LINK_2']=gm("Short Weblink");
		$array_values['WEB_LINK_URL']=gm("Weblink URL");
		$array_values['DROP_BOX']=gm("Dropbox files");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['LOGO']=gm("Logo");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['CONTACT_FIRST_NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST_NAME']=gm("Contact last name");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);

		$is_normal = true;
		if($code=='nl'){
			$code='du';
		}
		// $in['lang_code'] = $code;
		$name = 'quomess';
		$text = '-----------------------------------
			Quote Summary
			-----------------------------------
			Quote ID: [!SERIAL_NUMBER!]
			Date: [!QUOTE_DATE!]
			Customer: [!CUSTOMER!]
			Discount ([!DISCOUNT!]):  [!DISCOUNT_VALUE!]

			Amount due: [!AMOUNT_DUE!]


			The detailed quote is attached as a PDF.

			Thank you!
			-----------------------------------';
		$subject = 'Quote #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
		$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'quote-settings-quote-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'quote-settings-quote-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code
			);
		}

		return json_out($data, $showin,$exit);
	}
	function get_defaultEmail($in,$showin=true,$exit=true){
			$db = new sqldb();
			$data = array( 'email_default'=>array());

			$email = $db->query("SELECT * FROM default_data WHERE type='quote_email' ");
			$mail_type1 = $db->field("SELECT value FROM default_data WHERE  type = 'quote_email_type' ");
			$default_email_bcc = $db->query("SELECT * FROM default_data WHERE type = 'bcc_quote_email' ");

			$data['email_default'] = array(
				'default_name'	=> $email->gf('default_name'),
				'email_value'	=> $email->gf('value'),
				'bcc_email'		=> $default_email_bcc->gf('value'),
				'mail_type_1'	=> $mail_type1,
				'do'			=> 'quote-settings-quote-update_default_email',
				'xget'			=> 'defaultEmail',
			);


			return json_out($data, $showin,$exit);
	}
	function get_weblink($in,$showin=true,$exit=true){
			$db = new sqldb();
			$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_QUOTE_WEB_LINK' ");
			$allow = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_COMMENTS' ");
			$allow2 = $db->field("SELECT value FROM settings WHERE constant_name='WEB_INCLUDE_PDF_Q' ");
			$allow3 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_WEB_PAYMENT_Q' ");
			$allow4 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_ACCEPT' ");
			$allow5 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_NEWV' ");
			$allow6 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_REJECT' ");
			$data['weblink'] = array(
				'quote_link'		=>$use == '1' ? true : false,
				'quote_pdf'			=>$allow2 == '1' ? true : false,
				'quote_comment'		=>$allow == '1' ? true : false,
				'quote_accept'		=>$allow4 == '1' ? true : false,
				'quote_new'			=>$allow5 == '1' ? true : false,
				'quote_reject'		=>$allow6 == '1' ? true : false,
				'old_quote_link'		=>$use == '1' ? true : false,
				'old_quote_pdf'			=>$allow2 == '1' ? true : false,
				'old_quote_comment'		=>$allow == '1' ? true : false,
				'old_quote_accept'		=>$allow4 == '1' ? true : false,
				'old_quote_new'			=>$allow5 == '1' ? true : false,
				'old_quote_reject'		=>$allow6 == '1' ? true : false,
				'quote_link_sign'		=>$in['quote_link_change'] ? true : false,
				'quote_pdf_sign'		=>$in['quote_pdf_change'] ? true : false,
				'quote_comment_sign'	=>$in['quote_comment_change'] ? true : false,
				'quote_accept_sign'		=>$in['quote_accept_change'] ? true : false,
				'quote_new_sign'		=>$in['quote_new_change'] ? true : false,
				'quote_reject_sign'		=>$in['quote_reject_change'] ? true : false,
				'do'				=> 'quote-settings-quote-web_link',
				'xget'				=> 'weblink',
			);

			return json_out($data, $showin,$exit);
	}

	function get_categorisation($in,$showin=true,$exit=true){
			$db = new sqldb();
			$i = 0;
			$j = 0;
			$data = array('bigloop'=>array());
			//$array = array('source','type','lost_reason');
			$array = array('lost_reason');

			foreach ($array as $key) {
				$data['bigloop'][$j]=array(
					'name'			=> gm(ucfirst(str_replace('_', ' ', $key))),
					'table' 		=> $key,
					'count'			=> $i,
					'small'			=> array(),
				);
				$i=1;
				$db->query("SELECT * FROM tblquote_".$key." ORDER BY sort_order ");
				while ($db->next()) {
					$data['bigloop'][$j]['small'][]=array(
						'field_id'	=> $db->f('id'),
						'label'		=> gm(ucfirst($key)).' '.$i,
						'value'		=> $db->f('name'),
					);
					$i++;
				}
				$j++;
			}
			return json_out($data, $showin,$exit);
	}
	function get_articlefield($in,$showin=true,$exit=true){
			$db = new sqldb();
			$text = $db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");
			$data['articlefields'] = array(
				'text' 	=>	$text,
				'ADV_PRODUCT' => defined('ADV_PRODUCT') && ADV_PRODUCT == 1 ? true : false,
				'do'	=> 'quote-settings-quote-saveArticleField',
				'xget'	=> 'articlefield',

			);
			return json_out($data, $showin,$exit);
	}
	function get_packingsettings($in,$showin=true,$exit=true){
			$db = new sqldb();
			$allowpack = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_PACKING'");
			$allowsale = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_SALE_UNIT'");
			$data['packing'] = array(
				'allowpack' => $allowpack == '1' ? true : false,
				'allowsale' => $allowsale == '1' ? true : false,

			);
			return json_out($data, $showin,$exit);
	}
	function get_attachments($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('atachment'=>array());
			$f = $db->query("SELECT * FROM attached_files WHERE type='1' ");
			while ($f->next()) {
				array_push($data['atachment'], array(
					'file'		=> $f->f('name'),
					'checked'	=> $f->f('default') == 1 ? true : false,
					'file_id'	=> $f->f('file_id'),
					'path'		=> INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/quotes/'.$f->f('name'),
					'checkeddrop'		=> $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_DROP_BOX' ") == '1' ? true : false,
					));
			}

			return json_out($data, $showin,$exit);
	}
	function get_listsettings($in,$showin=true,$exit=true){
		$db = new sqldb();

		$act = $db->field("SELECT value FROM settings WHERE constant_name='SHORT_QUOTES_BY_SERIAL' ");
		$data['listset'] = array(
			'setlist' => $act == 1 ? true : false,
		);


		return json_out($data, $showin,$exit);
	}
	function get_quoteTerm($in,$showin=true,$exit=true){
		$db = new sqldb();
		$pdf_settings_data=array(
                array(
                    'name'    =>gm('Grand Total'),
                    'model_name'    => 'GRAND_TOTAL',
                    'value'            => true,    
                    'actions'        =>'document'    
                    ),
                array(
                    'name'    =>gm('Total discount value'),
                    'model_name'    => 'SHOW_DISCOUNT_TOTAL',
                    'value'            => false,    
                    'actions'        =>'document'    
                    ),
                array(
                    'name'    => gm('Show VAT on total'),
                    'model_name'    => 'SHOW_VAT_ON_PDF',
                    'value'            => true,
                    'actions'        => 'document'
                    ),
                array(
                    'name'    =>gm('Show VAT column on PDF'),
                    'model_name'    => 'SHOW_VAT_COLUMN_PDF',
                    'value'            => false,
                    'actions'        => 'document'
                    ),
                array(
                    'name'    =>gm('Show article image'),
                    'model_name'    => 'SHOW_ARTICLE_IMAGE',
                    'value'            => false,
                    'actions'        => 'document'
                    ),
                array(
                    'name'    =>gm('Code'),
                    'model_name'    => 'ITEM_CODE',
                    'value'            => true,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Quantity'),
                    'model_name'    => 'QUANTITY',
                    'value'            => true,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Unit Price'),
                    'model_name'    => 'UNIT_PRICE',
                    'value'            => true,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Unit Price + VAT'),
                    'model_name'    => 'PRICE_VAT',
                    'value'            => false,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Pack.').' / '.gm('S.U.'),
                    'model_name'    => 'PACK_SU',
                    'value'            => false,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Discount'),
                    'model_name'    => 'DISCOUNT',
                    'value'            => true,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Line amount'),
                    'model_name'    => 'LINE_AMOUNT',
                    'value'            => true,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Show Capter Total'),
                    'model_name'    => 'SHOW_CHAPTER_TOTAL',
                    'value'            => false,
                    'actions'        => 'chapter'
                    ),
                array(
                    'name'    =>gm('Consider as an option'),
                    'model_name'    => 'CONSIDER_AS_OPTION',
                    'value'            => false,
                    'actions'        => 'chapter'
                    ));
        foreach($pdf_settings_data as $key=>$value){
            $setting_activated=$db->field("SELECT value FROM settings WHERE constant_name='QUOTE_PDF_SETTING_".$value['model_name']."' ");
            if(!is_null($setting_activated)){
                 $pdf_settings_data[$key]['value']=$setting_activated == '1' ? true : false;
            }            
        }
		$apply_discount = $db->field("SELECT value FROM settings WHERE constant_name = 'QUOTE_APPLY_DISCOUNT' ");
		$data = array('quoteTerm'=>array(
			'quote_term'  				=> $db->field("SELECT value FROM settings WHERE constant_name = 'QUOTE_TERM' "),
			'choose_quote_term_type' 	=> $db->field("SELECT value FROM settings WHERE constant_name = 'QUOTE_TERM_TYPE' "),
			'apply_discount' 			=> $apply_discount==1 ? true : false,
			'do'						=> 'quote-settings-quote-saveterm',
			'xget'						=> 'quoteTerm',
			'pdf_settings_data'          => $pdf_settings_data,
			'ADV_QUOTE'						=> defined('NEW_SUBSCRIPTION') ? (ADV_QUOTE == 1 ? true: false ) : true,
		));

		return json_out($data, $showin,$exit);
	}
	$result = array(
		'PDFlayout'			=> get_PDFlayout($in,true,false),
		'CustomizePDF'		=> get_CustomizePDF($in,true,false),
		'convention' 		=> get_Convention($in,true,false),
	);
json_out($result);



function get_weblinkEmailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Quote Nr");
		// $array_values['QUOTE_DATE']=gm("Invoice Date");
		$array_values['CUSTOMER']=gm("Customer Name");
		$array_values['CONTACT_FIRST_NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST_NAME']=gm("Contact last name");
		// $array_values['DISCOUNT']=gm("Discount Percent");
		// $array_values['DISCOUNT_VALUE']=gm("Discount Value");
		// $array_values['AMOUNT_DUE']=gm("Amount due");
		$array_values['WEB_LINK']=gm("Web Link");
		// $array_values['SUBJECT']=gm("Subject");
		// $array_values['WEB_LINK_2']=gm("Short Weblink");
		// $array_values['DROP_BOX']=gm("Dropbox files");
		// $array_values['SIGNATURE']=gm("Signature");
		// $array_values['LOGO']=gm("Logo");
		// $array_values['SALUTATION']=gm("Salutation");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);

		$is_normal = true;
		if($code=='nl'){
			$code='du';
		}
		
		$name = $in['wname'];

		$default_text = quotes_weblink_default_text($name,$in['languages']);
		
		$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".addslashes($default_text['text'])."', subject='".$default_text['subject']."', name='".$name."', lang_code='".$code."', html_content = '".addslashes($default_text['text'])."',use_html='1' ");
			$data['message']= array(
				'subject'        			=> $default_text['subject'],
				'text'           			=> $default_text['text'],
				'html_content'				=> $default_text['text'],
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'quote-settings-quote-weblink_default_message',
				'xget'						=> 'weblinkEmailMessage',
				'name'						=> $name,
				'wname'						=> $name,
				'lang_code'					=> $code
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'quote-settings-quote-weblink_default_message',
				'xget'						=> 'weblinkEmailMessage',
				'name'						=> $name,
				'wname'						=> $name,
				'lang_code'					=> $code
			);
		}

		return json_out($data, $showin,$exit);
	}

	function quotes_weblink_default_text($name,$lang_id){

		$email_text = 'Default email text';
		$email_subject = 'Default subject';
		$action = '';

		#Actions
		#1 #accepted
		#2 #new version
		#3 #rejected
		#4 #commentary
		
			switch($name){
				case 'quowebcommmess':
					$action = 4;
					
					#dutch
					$subject[4][3] = ' Feedback op offerte [!SERIAL_NUMBER!]';
				    $text[4][3]=nl2br('Beste [!CUSTOMER!], 
			Bedankt voor uw feedback aangaande onze offerte [!WEB_LINK!]. Wij komen hier zo snel mogelijk op terug.

			Met vriendelijke groeten,
			[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
			[!ACCOUNT_COMPANY!]');

					#english
					$subject[4][1] = 'Feedback on [!SERIAL_NUMBER!]';
					$text[4][1]=nl2br('Dear [!CUSTOMER!],

					Thank you for your feedback regarding our quote [!WEB_LINK!]. We will get back to you as soon as possible.

					Best regards, 
					[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
					[!ACCOUNT_COMPANY!]');

					#french
					$subject[4][2] = 'Commentaires sur [!SERIAL_NUMBER!]';
					$text[4][2]=nl2br('Bonjour [!CUSTOMER!],
			Merci pour vos commentaires concernant notre devis [!WEB_LINK!]. Nous vous reviendrons le plus vite possible. 

			Bien à vous,
			[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
			[!ACCOUNT_COMPANY!]');


					break;
				case 'quowebaccmess':
					$action = 1;
					#german
				    $subject[1][3] = 'Offerte [!SERIAL_NUMBER!] van [!ACCOUNT_COMPANY!] is aanvaard';
				    $text[1][3]=nl2br('Beste [!CUSTOMER!],

			Bedankt voor uw reactie op onze offerte [!WEB_LINK!]. We zijn verheugd om te zien dat u onze offerte heeft aanvaard, en kunnen niet wachten om aan onze samenwerking te beginnen!

			Als u nog vragen of feedback heeft, aarzel aub niet om ons te contacteren. We zullen binnenkort contact met u opnemen om onze deal te bevestigen.

			Met vriendelijke groet,

			[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
			[!ACCOUNT_COMPANY!]');

					#english
					$subject[1][1] = 'Quote [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] is accepted';
					$text[1][1]=nl2br('Dear [!CUSTOMER!],

			thank you for your reaction to our quote proposal [!WEB_LINK!]. We are delighted to see that you have accepted our quote, and are eager to start our collaboration!
			If you have any more questions or feedback, please don’t hesitate to reach out.
			We will contact you soon to confirm our deal.

			Kind regards,

			[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
			[!ACCOUNT_COMPANY!]');

					#french
				$subject[1][2] = 'Offre [!SERIAL_NUMBER!] de [!ACCOUNT_COMPANY!] est acceptée';
				$text[1][2]= nl2br("Cher [!CUSTOMER!],

		Nous vous remercions de votre réaction à notre offre [!WEB_LINK!]. Nous sommes ravis que vous avez acceptez notre offre, et impatients d'entamer notre collaboration !
		Surtout n'hésitez pas à nous contacter si vous avez des questions ou des suggestions.
		Nous vous contacterons très bientôt pour confirmer le tout.

		Bien à vous,

		[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
		[!ACCOUNT_COMPANY!]");

					break;

				case 'quowebnvermess':
					$action = 2;
					#german
					$subject[2][3] = 'Offerte [!SERIAL_NUMBER!] van [!ACCOUNT_COMPANY!] nieuwe versie';
					$text[2][3]=nl2br('Beste [!CUSTOMER!],

			Bedankt voor uw reactie op onze offerte [!WEB_LINK!]. We hebben begrepen dat onze huidige offerte niet voldoet aan uw verwachtingen, en dat u graag een nieuwe versie van ons wilt ontvangen. We zullen hier zo snel mogelijk voor zorgen!

			Indien de informatie die we op uw offerte gebruikt hebben niet correct is, gelieve ons dit te laten weten. We maken dit dan meteen in orde.

			Met vriendelijke groet,

			[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
			[!ACCOUNT_COMPANY!]');

					#english
				$subject[2][1] = 'Quote [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] new version';
				$text[2][1]=nl2br('Dear [!CUSTOMER!],

		thank you for your reaction to our quote proposal [!WEB_LINK!]. We understand that the current quote does not match your expectations, and that you would like us to make a new proposal. We will get on it as soon as possible!
		In case the information we used on our quote is incorrect, please let us know and we’ll correct it immediately.

		Kind regards,

		[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
		[!ACCOUNT_COMPANY!]');

					#french
				$subject[2][2] = 'Offre [!SERIAL_NUMBER!] de [!ACCOUNT_COMPANY!] nouvelle version';
				$text[2][2]=nl2br("Cher [!CUSTOMER!],

		Nous vous remercions de votre réaction à notre offre [!WEB_LINK!]. Nous comprenons que l'offre actuelle ne correspond pas à vos attentes, et que vous souhaitez nous faire une nouvelle proposition. Nous répondons à cette proposition au plus vite !
		Si les données que nous avons utilisées sur notre offre sont incorrectes, merci de nous le signaler. Nous les corrigerons immédiatement.

		Bien à vous,

		[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
		[!ACCOUNT_COMPANY!]");

					break;

				case 'quowebrefmess':
					$action = 3;
					#german
					$subject[3][3] = 'Offerte [!SERIAL_NUMBER!] van [!ACCOUNT_COMPANY!] is geweigerd';
					$text[3][3]= nl2br('Beste [!CUSTOMER!],

			Bedankt voor uw reactie op onze offerte [!WEB_LINK!]. Helaas blijkt deze offerte niet aan uw eisen voldaan te hebben, en heeft u ze moeten weigeren. Als u verder nog feedback heeft over onze aanbieding, horen we dat graag!

			Indien u graag een nieuwe versie van deze offerte ontvangt, bezorgen we die u graag op simpel verzoek. We blijven hoopvol over een samenwerking in de toekomst.

			Met vriendelijke groet,

			[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
			[!ACCOUNT_COMPANY!]');

					#english
					$subject[3][1] = 'Quote [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] is refused';
					$text[3][1]=nl2br('Dear [!CUSTOMER!],

			thank you for your reaction to our quote proposal [!WEB_LINK!]. Unfortunately, it seems that our quote has not met your standards, and you had to refuse our proposal. If you have any comments you want to share with us, we would be most grateful to hear them.
			If you would like us to make a new version of this quote, please let us know. In the mean while, we remain hopeful for a collaboration in the future.

			Kind regards,

			[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
			[!ACCOUNT_COMPANY!]');

					#french
				$subject[3][2] = 'Offre [!SERIAL_NUMBER!] de [!ACCOUNT_COMPANY!] est refusée';
				$text[3][2]=nl2br("Cher [!CUSTOMER!],

		Nous vous remercions de votre réaction à notre offre [!WEB_LINK!]. Malheureusement, il semble que notre offre n'a pas répondu à vos attentes, et vous avez dû la refuser. Surtout n'hésitez pas à nous transmettre vos commentaires ou suggestions.
		Veuillez nous indiquer si vous souhaitez une nouvelle version de cette offre. Dans l'espoir d'une collaboration fructueuse, nous restons à votre service pour toute question.

		Bien à vous,

		[!CONTACT_LAST_NAME!] [!CONTACT_FIRST_NAME!]
		[!ACCOUNT_COMPANY!]");

					break;		
			}

		if(!empty($action)){
			$email_text = $text[$action][$lang_id];
			$email_subject = $subject[$action][$lang_id];
		}

			return array('text' => $email_text, 'subject' => $email_subject);
	}


?>





