<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

include(__DIR__.'/../model/quote.php');
$invModel = new quote();

$tmps = time();


if(!$_SESSION['add_to_quote']){
	exit();
}
global $config;
ark::loadLibraries(array('aws'));
$aws = new awsWrap(DATABASE_NAME);

$files = array();
foreach ($_SESSION['add_to_quote'] as $key => $value) {
	$version_id = $db->field("SELECT version_id FROM tblquote_version WHERE quote_id = '".$key."' AND active='1' ");
	$link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/quote/quote_'.$key.'_'.$version_id.'.pdf');

	$content = file_get_contents($link);

	if(!$content){

		$inv = $db->query("SELECT * FROM tblquote WHERE id = '".$key."' ");
	    $params = array();
	    $params['use_custom'] = 0;
	    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
	      $params['logo'] = $inv->f('pdf_logo');
	      $params['type']=$inv->f('pdf_layout');
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
	      $params['custom_type']=$inv->f('pdf_layout');
	      unset($params['type']);
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	      $params['use_custom'] = 1;
	    }else{
	      $params['type']=ACCOUNT_QUOTE_PDF_FORMAT;
	      $params['template_type'] = ACCOUNT_QUOTE_PDF_FORMAT;
	    }
	    #if we are using a customer pdf template
	    if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $inv->f('pdf_layout') == 0){
	      $params['custom_type']=ACCOUNT_QUOTE_PDF_FORMAT;
	      unset($params['type']);
	    }
	    $params['id'] = $key;
	    $params['lid'] = $inv->f('email_language');
	    $params['save_as'] = 'F';

	    $invModel->generate_pdf($params);

	    
	    $link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/quote/quote_'.$key.'.pdf');
		$content = file_get_contents($link);
	}
	
	file_put_contents(DATABASE_NAME.'_quote_'.$key.'_'.$tmps.'.pdf', $content);
	array_push($files, DATABASE_NAME.'_quote_'.$key.'_'.$tmps.'.pdf');
}

$content = null;
$outputName = DATABASE_NAME."_merged_".$tmps.".pdf";
$cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName ";
foreach($files as $file) {
    $cmd .= $file." ";
}
shell_exec($cmd);
doQueryLog();
header('Content-Type: application/pdf');
header("Content-Disposition:inline;filename=quotes.pdf");
header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
header('Pragma: public');
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
echo file_get_contents($outputName);

foreach ($files as $file) {
	@unlink($file);
}
@unlink($outputName);
exit();
?>