<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
// $starty = console::getMicroTime();

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

if($in['reset_list']){
    if(isset($_SESSION['add_to_quote'])){
        unset($_SESSION['add_to_quote']);
    }
}

// $view_list = new at(ark::$viewpath.'quotes_list.html');
$result = array('list'=>array(),'max_rows'=>0, 'nav'=>array());
if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
$order_by_array = array('serial_number','quote_date','subject','buyer_name','amount','amount_vat','own_reference','acc_manager_name');
// $db = new sqldb();
$today = mktime(0,0,0,date('n'),date('j'),date('Y'));

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

$l_r = ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$filter_q = 'WHERE 1=1 ';
$order_by = " ORDER BY tblquote.quote_date DESC ";
if(defined('SHORT_QUOTES_BY_SERIAL') && SHORT_QUOTES_BY_SERIAL == 1){
	$order_by = " ORDER BY tblquote.serial_number DESC ";
}
$arguments = '';
$filter_your_ref = '';
if($in['customer_id']){
	$arguments_c .= '&customer_id='.$in['customer_id'];
}
if(!$in['archived']){
	$filter_q.= " AND f_archived = '0' ";
}else{
	$filter_q.= " AND f_archived = '1' ";
	$arguments.="&archived=".$in['archived'];
	$arguments_o.="&archived=".$in['archived'];
}
if($in['search']){
	$filter_q.=" AND (tblquote.serial_number like '%".$in['search']."%' OR tblquote.buyer_name like '%".$in['search']."%'  OR tblquote_version.version_subject like '%".$in['search']."%' )";
	$arguments.="&search=".$in['search'];
}
if($in['your_reference_search']){
	$filter_your_ref =" AND tblquote_version.version_own_reference LIKE '%".$in['your_reference_search']."%' ";
	$arguments.="&your_reference_search=".$in['your_reference_search'];
}
 $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
 
if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == '1' || $in['desc']=='true'){
			$order = " DESC ";
		}
		if($in['order_by']=='amount' || $in['order_by']=='amount_vat' ||  $in['order_by']=='author' ){
			    $filter_limit =' ';
    			$order_by ='';
	    }else if($in['order_by']=='own_reference' || $in['order_by']=='subject'){
	    	$order_by =" ORDER BY tblquote_version.version_".$in['order_by']." ".$order;
	    }else{
	       $order_by =" ORDER BY ".$in['order_by']." ".$order;
	       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	       $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	    }
	
	}
}

if($in['start_date'] && $in['stop_date']){
	$filter_q.=" and tblquote.quote_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if($in['start_date']){
	$filter_q.=" and cast(tblquote.quote_date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if($in['stop_date']){
	$filter_q.=" and cast(tblquote.quote_date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
if(!isset($in['view'])){
	$in['view'] = 0;
}
if($in['view'] == 0){
	# do nothing
}
if($in['view'] == 1){
	$filter_q.=" AND tblquote_version.version_status_customer='0' AND tblquote_version.sent='0' ";
	$arguments.="&view=1";
}
if($in['view'] == 2){
	$filter_q.=" AND tblquote_version.version_status_customer='0' AND tblquote_version.sent = '1' ";
	$arguments.="&view=2";
}
if($in['view'] == 3){
	$filter_q.=" AND tblquote_version.version_status_customer > '1' ";
	$arguments.="&view=3";
}
if($in['view'] == 4){
	$filter_q.=" AND tblquote_version.version_status_customer = '1' ";
	$arguments.="&view=4";
}
if($in['view'] == 5){
	$filter_q.=" AND tblquote.created_by = '".$_SESSION['u_id']."' ";
	$arguments.="&view=5";
}
if($in['view'] == 6 ) {
	$filter_q .= " AND tblquote.pending = '1' AND tblquote_version.version_status_customer > '1' AND tblquote.transformation = '0' ";
	$arguments.="&view=6";
}

$filter_c = " AND 1=1 ";
$cust_id = '';
if($in['customer_id']){
	$cust_id=$in['customer_id'];
	$filter_c = " AND buyer_id='".$in['customer_id']."' ";
	$page_title_q='<h2>'.gm('List quotes').'<a href="index.php?do=quote-quotes&add=true" title="'.gm('Add').'" class="add">'.gm('Add').'</a>';
}
if($in['opportunity_id']){
	$quote_ids = "";
	$opportunity_quotes = $db->query("SELECT quote_id FROM  tblopportunity_quotes WHERE opportunity_id = '".$in['opportunity_id']."' ORDER BY id DESC");
	$j=0;
	while($opportunity_quotes->next()){
		$quote_ids .= "'".$opportunity_quotes->f('quote_id')."',";
		$j++;
	}
	$quote_ids = rtrim($quote_ids,',');
	$filter_q.=" AND tblquote.id IN (".$quote_ids.") ";
	$arguments_c.="&opportunity_id=".$in['opportunity_id'];
}
$arguments = $arguments.$arguments_c;
// LEFT JOIN tblquote_stage ON tblquote_stage.id = tblquote.stage_id
// ,tblquote_stage.name as stage_name
// LEFT JOIN tblquote_stage ON tblquote_stage.id = tblquote.stage_id

//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);

if($admin_licence != '3' && ONLY_IF_ACC_MANAG4 == '1'){
	$filter_q.= " AND CONCAT( ',', acc_manager_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}

$result['nav']=$db->query("SELECT tblquote.id as quote_id FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id  ".$filter_q." AND active=1 ".$filter_c.$filter_your_ref.$order_by)->getAll();
/*$max_rows=$db->field("SELECT count(tblquote.id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id  ".$filter_q." AND active=1 ".$filter_c.$filter_your_ref);*/
$result['max_rows'] = count($result['nav']);

$quote = $db->query("SELECT tblquote.*,tblquote_version.*, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date, tblquote.sent as sent_q,multiple_identity.identity_name,vat_new.description AS vat_regime
            FROM tblquote
            INNER JOIN (SELECT tblquote_version.quote_id,MAX(tblquote_version.version_id) AS v_id FROM tblquote_version GROUP BY tblquote_version.quote_id) AS tblq_version ON tblquote.id=tblq_version.quote_id
            INNER JOIN tblquote_version ON tblq_version.quote_id=tblquote_version.quote_id AND tblq_version.v_id=tblquote_version.version_id
            LEFT JOIN multiple_identity ON tblquote.identity_id=multiple_identity.identity_id
            LEFT JOIN vat_new ON tblquote.vat_regime_id=vat_new.id
            ".$filter_q.$filter_c.$filter_your_ref.$order_by.$filter_limit);

/* normal user privileges */
$u_id = $_SESSION['u_id'];											// $user_credential - 1 (admin)
if($_SESSION['group']=='admin') {									//                  - 2 (quote_admin)
	$user_credential = 1;											//					- 3 (none => check if is author or creator)
} else {
	if(in_array('quote', $_SESSION['admin_sett'])) {
		$user_credential = 2;
	} else {
		$user_credential = 3;
	}
}

$i=0;
while($quote->next()){
	$is_admin = true;
	if($user_credential == 3) {
		$created_by = $db->field("SELECT created_by FROM tblquote WHERE id = '".$quote->f('id')."'");
		$author_id = $db->field("SELECT author_id FROM tblquote WHERE id = '".$quote->f('id')."'");
		if($created_by == $u_id || $author_id == $u_id) {
			$is_admin = true;
			// $view_list->assign('is_admin', true,'template_row');
		} else {
			$is_admin = false;
			// $view_list->assign('is_admin', false,'template_row');
		}
	} /*else {
		$view_list->assign('is_admin', true,'template_row');
	}*/

	$ref = '';
	if(ACCOUNT_QUOTE_REF && $quote->f('buyer_reference')){
		$ref = $quote->f('buyer_reference').ACCOUNT_QUOTE_DEL;
	}

	$amount = $db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$quote->f('id')."' AND version_id='".$quote->f('version_id')."' AND show_block_total='0' ");
	$amount_vat = $db->field("SELECT SUM(amount + amount * vat/100) FROM tblquote_line WHERE quote_id='".$quote->f('id')."' AND version_id='".$quote->f('version_id')."' AND show_block_total='0' ");

	if($quote->f('discount') && $quote->f('apply_discount') > 1){
		$amount = $amount - ($amount*$quote->f('discount')/100);
		$amount_vat = $amount_vat - ($amount_vat*$quote->f('discount')/100);
	}

	if($quote->f('currency_rate')){
		$amount = $amount*return_value($quote->f('currency_rate'));
		$amount_vat = $amount_vat*return_value($quote->f('currency_rate'));
	}
	$status = '';
	if($quote->f('validity_date') && $quote->f('version_status_customer') == 0){
		$valid = mktime(0,0,0,date('n',$quote->f('validity_date')),date('j',$quote->f('validity_date')),date('Y',$quote->f('validity_date')));
		if($valid < $today){
			$status = gm('Expired');
			// $text = gm('Expired');
		}
	}
	$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
	if($quote->f('pdf_layout') && $quote->f('use_custom_template')==0){
		$link_end='&type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
	}elseif($quote->f('pdf_layout') && $quote->f('use_custom_template')==1){
		$link_end = '&custom_type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
	}
	#if we are using a customer pdf template
	if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $quote->f('pdf_layout') == 0){
		$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT.'&logo='.$quote->f('pdf_logo');
		$in['use_custom'] = 1;
	}
	$lng = 1;
	if($quote->f('email_language')){
		$lng = $quote->f('email_language');
	}

	$was_sent = $db->field("SELECT count(version_id) FROM tblquote_version WHERE sent='1' AND quote_id='".$quote->f('id')."' ");
	$q_version_nr = $db->field("SELECT count(version_id) FROM tblquote_version WHERE quote_id='".$quote->f('id')."' ");
	$show_expand = ($q_version_nr >= 2) ? true : false;

	$deal_stage="-";
	if($quote->f('trace_id')){
		$deal_data=$db->query("SELECT tblopportunity.opportunity_id,tblopportunity_stage.name AS stage_name FROM tracking_line 
			INNER JOIN tblopportunity ON tracking_line.origin_id=tblopportunity.opportunity_id
			INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
			WHERE tracking_line.trace_id='".$quote->f('trace_id')."' AND tracking_line.origin_type='11' ")->getAll();
		if(!empty($deal_data)){
			$deal_stage=$deal_data[0]['stage_name'];
		}
	}

	$item=array(
		'info_link'     			=> ('index.php?do=quote-quote&quote_id='.$quote->f('id')).$arguments,
		'edit_link'     			=> ('index.php?do=quote-nquote&quote_id='.$quote->f('id')).$arguments,
		'archive_link' 		 		=> array('do'=>'quote-quotes-quote-archive', 'id'=>$quote->f('id')),
		'delete_link' 		 		=> array('do'=>'quote-quotes-quote-delete', 'id'=>$quote->f('id')),
		'activate_link' 		 	=> array('do'=>'quote-quotes-quote-activate', 'id'=>$quote->f('id')),
		'pdf_link'					=> 'index.php?do=quote-print&customer_id='.$in['customer_id'].'&id='.$quote->f('id').'&version_id='.$quote->f('version_id').'&lid='.$lng.$link_end,
		'check_add_to_product'		=> $_SESSION['add_to_quote'][$quote->f('id')] == 1 ? true : false,
		'view_delete_link2'			=> $_SESSION['access_level'] == 1 ? '' : 'hide',
		'hide_on_a'					=> $in['archived'] == 1 ? 'hide' : '',
		'archived'					=> $quote->f('f_archived') == 1 ? true : false,
		'status'					=> $quote->f('sent') > 0 ? false : true,
		'status2'					=> $quote->f('version_status_customer'),
		'id'            	    	=> $quote->f('id'),
		'serial_number' 	    	=> $ref.$quote->f('serial_number'),
		'quote_version' 	    	=> $in['archived'] == 1 ? '['.$db->field("SELECT version_code FROM tblquote_version WHERE active=1 AND quote_id='".$quote->f('id')."' ").']' : $quote->f('version_code')=='A' ? '' : '['.$quote->f('version_code').']',
		'created'       	   	 	=> $quote->f('version_date') ? date(ACCOUNT_DATE_FORMAT,$quote->f('version_date')) : date(ACCOUNT_DATE_FORMAT,$quote->f('quote_date')),
		'seller_name'   	    	=> $quote->f('seller_name'),
		'buyer_name'    	    	=> $quote->f('buyer_name'),
		'amount'					=> place_currency(display_number($amount),  get_commission_type_list($quote->f('currency_type'))),
		'amount_ord'				=> $amount,
		// 'stage'					=> $quote->f('stage_name'),
		'template_link'				=> 'index.php?do=quote-nquote&quote_id='.$quote->f('id')."&template=1".$arguments,
		'alt_for_delete_icon'		=> $in['archived'] == 1 ? gm('Delete') : gm('Archive'),
		'subject'					=> htmlspecialchars($quote->f('version_subject')),
		'if_subject'				=> $quote->f('version_subject') ? true : false,
		'valid'						=> $status ? true : false,
		'validity'					=> $status,
		// 'status_title'				=> $text,
		'confirm'					=> gm('Confirm'),
		'ok'						=> gm('Ok'),
		'cancel'					=> gm('Cancel'),
		'is_admin'					=> $is_admin,
		'version_id'				=> $quote->f('version_id'),
		'post_order'		    	=> $quote->f('postgreen_id')!='' ? true : false,
		'own_reference' 			=> $quote->f('version_own_reference'),
		'author' 					=> get_user_name($quote->f('author_id')),
		'author_ord' 					=> get_user_name($quote->f('author_id')),
		'acc_manager_name'				=> $quote->f('acc_manager_name'),		
		'amount_vat'					=> place_currency(display_number($amount_vat), get_commission_type_list($quote->f('currency_type'))),
		'amount_vat_ord'				=> $amount_vat,
		'identity_name'				=> $quote->f('identity_name') ? stripslashes($quote->f('identity_name')) : gm('Main Identity'),
		'show_expand'				=> $show_expand,
		'vat_regime'				=> stripslashes($quote->f('vat_regime')),
		'deal_stage'		 =>  $deal_stage,
	);

	$opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
	$field = 'status_customer';
	// if($quote->f('sent')){
		// foreach ($fields as $field){
			// $view_list->assign(array(strtoupper($field),$opts[$quote->f($field)]),'template_row');
			$item[strtolower($field)]=$opts[$quote->f('version_status_customer')];
		// }
	/*}else{
		$view_list->assign('status_customer',$opts[5],'template_row');
	}*/

	// if($was_sent && $quote->f('status_customer') == 0){
	if($quote->f('sent') == 1 && $quote->f('version_status_customer') == 0){
		$item['status_customer']=$opts[5];
		$item['status2']='5';
	}

	if($quote->f('f_archived')=='1'){
		$item['status_customer']=gm('Archived');
	    $item['status2']='0';
	}

	// if($status){
		// $item['status_customer']=$status;
		// $item['status2']='-1';
		// $view_list->assign('status_customer',$status,'template_row');
	// }

	//Pending Quotes that have Related Documents (PROJECT, INTERVENTION, ORDERS or INVOICE) should not appear
	if($in['view'] == 6 ) {
		$tracking = $db->query("SELECT origin_id,trace_id FROM tracking_line WHERE origin_id ='".$quote->f('id')."' AND origin_type = '2' ")->getAll();
		if(empty($tracking)){
			array_push($result['list'], $item);
		}
	} else {
		array_push($result['list'], $item);		
	}

	// array_push($result['list'], $item);
	// $view_list->loop('template_row');
	$i++;
}
$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
$result['lr'] = $l_r;
$result['adv_quotes'] = defined('NEW_SUBSCRIPTION') ? (ADV_QUOTE == 1 ? true: false ) : true;
$result['post_active']= !$post_active || $post_active==0 ? false : true;
$result['columns']  	= get_quotes_cols();
$result['nr_columns']  	= count($result['columns'])+3;

if($in['order_by']=='amount' || $in['order_by']=='amount_vat' || $in['order_by']=='author' ){

    if($order ==' ASC '){
       $exo = array_sort($result['list'], $in['order_by'].'_ord', SORT_ASC);    
    }else{
       $exo = array_sort($result['list'], $in['order_by'].'_ord', SORT_DESC);
    }

    $exo = array_slice( $exo, $offset*$l_r, $l_r);
    $result['list']=array();
       foreach ($exo as $key => $value) {
           array_push($result['list'], $value);
       }
}
$result['view_dd'] = array( array('id'=>'1', 'value'=>gm('Draft')), array('id'=>'2', 'value'=>gm('Sent')), array('id'=>'3', 'value'=>gm('Accepted')), array('id'=>'4', 'value'=>gm('Rejected')), array('id'=>'5', 'value'=>gm('My quotes')), array('id'=>'6', 'value'=>gm('Pending')));
json_out($result);

function get_quote_list($in){
	$db = new sqldb();
	if(!isset($in['quote_id']) || !is_numeric($in['quote_id'])){
		return array();
	}
	$result = array( 'list' => array());
	$today = mktime(0,0,0,date('n'),date('j'),date('Y'));

	$l_r = ROW_PER_PAGE;

	$order_by = " ORDER BY tblquote_version.version_code ASC ";

	$quote = $db->query("SELECT tblquote.*,tblquote_version.*, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date, tblquote.sent as sent_q,tblquote_version.active AS activeV,tblquote_version.version_status_customer
	            FROM tblquote
	            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
	            WHERE quote_id='".$in['quote_id']."'
	            ".$order_by)->getAll();
	$row = count($quote);

	$u_id = $_SESSION['u_id'];											// $user_credential - 1 (admin)
	if($_SESSION['group']=='admin') {									//                  - 2 (quote_admin)
		$user_credential = 1;											//					- 3 (none => check if is author or creator)
	} else {
		if(in_array('quote', $_SESSION['admin_sett'])) {
			$user_credential = 2;
		} else {
			$user_credential = 3;
		}
	}
	if($user_credential == 3) {
		$created_by = $db->field("SELECT created_by FROM tblquote WHERE id = '".$in['quote_id']."' ");
		$author_id = $db->field("SELECT author_id FROM tblquote WHERE id = '".$in['quote_id']."' ");
		if($created_by == $u_id || $author_id == $u_id) {
			$is_admin = true;
			// $view->assign('is_admin', true,'template_row');
		} else {
			$is_admin = false;
			// $view->assign('is_admin', false,'template_row');
		}
	} else {
		$is_admin = true;
		// $view->assign('is_admin', true,'template_row');
	}

	$i=0;
	foreach ($quote as $key => $value) {
	// while($quote->next()){

		$amount = $db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$value['id']."' AND version_id='".$value['version_id']."' AND show_block_total='0' ");
		$amount_vat = $db->field("SELECT SUM(amount + amount * vat/100) FROM tblquote_line WHERE quote_id='".$value['id']."' AND version_id='".$value['version_id']."' AND show_block_total='0' ");

		if($value['discount'] && $value['apply_discount'] > 1){
			$amount = $amount - ($amount*$value['discount']/100);
			$amount_vat = $amount_vat - ($amount_vat*$value['discount']/100);
		}

		if($value['currency_rate']){
			$amount = $amount*return_value($value['currency_rate']);
			$amount_vat = $amount_vat*return_value($value['currency_rate']);
		}
		$status = '';
		$text = '';
		if($value['validity_date'] && $value['version_status_customer'] == 0){
			$valid = mktime(0,0,0,date('n',$value['validity_date']),date('j',$value['validity_date']),date('Y',$value['validity_date']));
			if($valid < $today){
				$status = 'late';
				$text = 'Expired';
			}
		}

		$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
		if($value['pdf_layout'] && $value['use_custom_template']==0){
			$link_end='&type='.$value['pdf_layout'].'&logo='.$value['pdf_logo'];
		}elseif($value['pdf_layout'] && $value['use_custom_template']==1){
			$link_end = '&custom_type='.$value['pdf_layout'].'&logo='.$value['pdf_logo'];
		}
		$lng = 1;
		if($value['email_language']){
			$lng = $value['email_language'];
		}

		$ref = '';
		if(ACCOUNT_QUOTE_REF && $value['buyer_reference']){
			$ref = $value['buyer_reference'].ACCOUNT_QUOTE_DEL;
		}

		$was_sent = $db->field("SELECT count(version_id) FROM tblquote_version WHERE sent='1' AND quote_id='".$value['id']."' ");
		$item = array(
			'is_admin'				=> $is_admin,
		  	'info_link'    			=> 'index.php?do=quote-quote&quote_id='.$value['id'].'&version_id='.$value['version_id'],
		  	'delete_link' 			=> array('do'=>'quote-quotes-quote_version-delete', 'id'=>$value['id'],'xget'=>'quote_list','version_id'=>$value['version_id']),		  	
		  	'pdf_link'				=> 'index.php?do=quote-print&id='.$value['id'].'&version_id='.$value['version_id'].'&lid='.$lng.$link_end,
		  	'status'				=> $value['version_status_customer'] > 0 ? false :($value['sent'] > 0 ? false : ($key+1 == $row) ? true : false),
		  	'id'               		=> $value['id'],
			'quote_version'    		=> $value['version_code'],
			'created'          		=> date(ACCOUNT_DATE_FORMAT,$value['version_date']),
			'amount'				=> place_currency(display_number($amount), get_commission_type_list($value['currency_type'])),
			'amount_vat'			=> place_currency(display_number($amount_vat), get_commission_type_list($value['currency_type'])),
			'alt_for_delete_icon'	=> $in['archived'] == 1 ? gm('Delete') : gm('Archive'),
			'status_title'			=> $text,
			'version_id'			=> $value['version_id'],
			'archived'				=> $value['v_archived'] == 0 ? false : true,
			'post_order'		    => $value['postgreen_id']!='' ? true : false,
			'buyer_name'			=> $value['buyer_name'],
			'subject'				=> $value['version_subject'],
			'own_reference'			=> $value['version_own_reference'],
			'author' 				=> get_user_name($value['author_id']),
			'acc_manager_name'		=> $value['acc_manager_name'],
			'serial_number' 	    => $ref.$value['serial_number'],
		);

		$opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
		// $opts = array('gray','red','green','green','green','orange');
		$fields = 'status_customer';
			// foreach ($fields as $field){
		if($value['sent'] == 0){
				$item[strtolower($fields)]=$opts[0];
				$item['status2']='0';
		}else{
			//if($value['activeV']==1){
				switch ($value['version_status_customer']) {
					case '1':
						$item[strtolower($fields)]=$opts[1];
						$item['status2']='1';
						break;
					case '2':
						$item[strtolower($fields)]=$opts[2];
						$item['status2']='2';
						break;
					default:
						$item[strtolower($fields)]=$opts[5];
						$item['status2']='5';
						break;
				}
			/*}else{
				$item[strtolower($fields)]=$opts[5];
				$item['status2']='5';
			}*/
		}
		if($value['version_status_customer']=='2' && $value['sent']=='0'){
			$item['status2']='9874125486355841568522487568963214'; // when the quote is accepted, no more edit
		}

		array_push($result['list'], $item);
		$i++;
	}

	return $result;
}


function get_quotesTemplates(&$in)
{
	$db = new sqldb();
	$result = array('list'=>array(),'langs'=>array(),'refresh_prices'=>true);

	$arguments_s = '';
	$arguments='';
	$arguments_o='';
	$arguments_a='';
	$l_r =ROW_PER_PAGE;
	$result['lr']=$l_r;
	$order_by_array = array('serial_number');

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	$order_by =" ORDER BY tblquote.serial_number ";

	if($in['order_by']){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
				$order = " DESC ";
			}

		       $order_by =" ORDER BY ".$in['order_by']." ".$order;
		       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
		}
	}

	$filter = " f_archived='2' ";

	$filter_c=' ';

	if(!empty($in['search'])){
		$filter .= " AND serial_number LIKE '%".$in['search']."%' ";
		$arguments_s.="&search=".$in['search'];
	}
	if(!empty($in['lang'])){
		$filter .= " AND email_language='".$in['lang']."' ";
		$result['lang']=$in['lang'];
	}

	$arguments = $arguments.$arguments_s;

	$max_rows =  $db->field("SELECT count(id) FROM tblquote WHERE ".$filter );
	$result['max_rows'] = $max_rows;
	$res=$db->query("SELECT tblquote.*,tblquote_version.*,tblquote_stage.name as stage_name
	            FROM tblquote
	            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
	            LEFT JOIN tblquote_stage ON tblquote_stage.id = tblquote.stage_id
	            WHERE ".$filter.$order_by." LIMIT ".$offset*$l_r.",".$l_r );
	$j=0;
	while($res->move_next()){
		$lang_f=$db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$res->f('email_language')."' ");
		if(!$lang_f){
			$lang_f='EN';
		}else if($lang_f=='du'){
			$lang_f='NL';
		}
		$list=array(
			'name'						=> $res->f('serial_number'),
			'id'						=> $res->f('id'),
			// 'edit_link'					=> 'index.php?do=quote-nquote&quote_template_id='.$db->f('id').'&template=1',
			// 'delete_link'				=> 'index.php?do=quote-quote_templates_list-quote-delete_template&id='.$db->f('id').$arguments.$arguments_o,
			'delete_link' 		 		=> array('do'=>'quote-quotes-quote-delete_template', 'id'=>$res->f('id'),'xget'=>'quotesTemplates'),
			'lang_f'			=> $lang_f ? '('.strtoupper($lang_f).')' : '',
		);
		$result['list'][]=$list;
	}
	$languages=$db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order ");
	while($languages->next()){
		array_push($result['langs'],array('id'=>$languages->f('lang_id'),'name'=>gm($languages->f('language'))));
	}
	if($in['index']){
		$result['index'] = $in['index'];
	}
	return $result;	
}

 function get_quotes_cols(){
		$db=new sqldb();
		$array=array();
		$cols_default=default_columns_dd(array('list'=>'quotes'));
		$cols_order_dd=default_columns_order_dd(array('list'=>'quotes'));
		$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='quotes' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
		if(!count($cols_selected)){
			$i=1;
            $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'quotes'));
            foreach($cols_default as $key=>$value){
				$tmp_item=array(
					'id'				=> $i,
					'column_name'		=> $key,
					'name'				=> $value,
					'order_by'			=> $cols_order_dd[$key]
				);
                if ($default_selected_columns_dd[$key]) {
                    array_push($array,$tmp_item);
                }
				$i++;
			}
		}else{
			foreach($cols_selected as $key=>$value){
				$tmp_item=array(
					'id'				=> $value['id'],
					'column_name'		=> $value['column_name'],
					'name'				=> $cols_default[$value['column_name']],
					'order_by'			=> $cols_order_dd[$value['column_name']]
				);
				array_push($array,$tmp_item);
			}
		}
		return $array;
	}