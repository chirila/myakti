<?php

class article
{
	
	function __construct()
	{
		$this->db = new sqldb();
	}

	/**
	 * Add article
	 * @param array $in
	 * @return boolean
	 */
	function add(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_validate($in)){
			$in['failure'] = true;
			return false;
		}
		
		$query_sync=array();
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['sale_unit']=1;
		}
        if($in['article_threshold_value']!=ARTICLE_THRESHOLD_VALUE){
        	$custom_threshold_value=1;
        }else{
        	$custom_threshold_value=0;
        }

        	if(!$in['exclude_serial_no']){
        		$in['exclude_serial_no']=0;
        	}
        	if(!$in['exclude_batch_no']){
        		$in['exclude_batch_no']=0;
        	}
		if(!$in['supplier_id']){
           $in['supplier_name']='';
		}
		if($in['price_type']==1){
            $in['use_percent_default']=0;
		}

		$NEXT_AUTOMATIC_ARTICLE_BARCODE = $this->db->field("SELECT value FROM settings WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE'");
		if($NEXT_AUTOMATIC_ARTICLE_BARCODE){			
			$in['ean_code']=$NEXT_AUTOMATIC_ARTICLE_BARCODE;
		}

		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
															item_code  							= '".$in['item_code']."',
															ean_code  							= '".$in['ean_code']."',
															hide_stock  						= '".$in['hide_stock']."',
															use_percent_default  				= '".$in['use_percent_default']."',
															supplier_reference      = '".$in['supplier_reference']."',
															vat_id									= '".$in['vat_id']."',
															article_brand_id			= '".$in['article_brand_id']."',
															ledger_account_id			= '".$in['ledger_account_id']."',
															article_category_id			= '".$in['article_category_id']."',
															origin_number   			= '".$in['origin_number']."',
															stock        	    		= '".$in['stock']."',
															max_stock        	    		= '".$in['max_stock']."',
															sale_unit        			= '".$in['sale_unit']."',
															internal_name       		= '".$in['internal_name']."',
															packing          			= '".return_value2($in['packing'])."',
															price_type          		= '".$in['price_type']."',
															article_threshold_value    	= '".$in['article_threshold_value']."',
															weight    					= '".$in['weight']."',
															show_img_q								= '".$in['show_img_q']."',
															supplier_id								= '".$in['supplier_id']."',
															supplier_name							= '".$in['supplier_name']."',
															custom_threshold_value    	= '".$custom_threshold_value."',
															created_at='".time()."' ");
		
		if($NEXT_AUTOMATIC_ARTICLE_BARCODE){			
			$this->db->query("UPDATE settings SET value='".($NEXT_AUTOMATIC_ARTICLE_BARCODE+1)."' WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE' ");
		}

       $this->db->query("INSERT INTO dispatch_stock SET  article_id= '".$in['article_id']."',
				                                              stock= '".return_value2($in['stock'])."',
				                                              address_id='".$main_address_id."',
				                                              main_address=1

				");
		/*start for  stock movements*/
		if($in['stock'] && $in['hide_stock']!=1){
        	$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
           //we add the stock in the main location
			

        	$this->db->insert("INSERT INTO stock_movements SET
        													date 						=	'".time()."',
        													created_by 					=	'".$_SESSION['u_id']."',
        													article_id 					=	'".$in['article_id']."',
        													article_name       			= 	'".$in['internal_name']."',
        													item_code  					= 	'".$in['item_code']."',
        													movement_type				=	'1',
        													quantity 					= 	'".$in['stock']."',
        													stock 						=	'0',
        													new_stock 					= 	'".$in['stock']."'
        	");
        }
        /*end for  stock movements*/
		$query_sync[0]="INSERT INTO pim_articles SET
																article_id      				='".$in['article_id']."',
																item_code  							= '".$in['item_code']."',
																ean_code  							= '".$in['ean_code']."',
																hide_stock  							= '".$in['hide_stock']."',
																supplier_reference      = '".$in['supplier_reference']."',
																vat_id									= '".$in['vat_id']."',
																article_brand_id				= '".$in['article_brand_id']."',
																ledger_account_id			= '".$in['ledger_account_id']."',
																origin_number   			 	= '".$in['origin_number']."',
																article_category_id			= '".$in['article_category_id']."',
																stock        	   	      = '".$in['stock']."',
																internal_name       		= '".$in['internal_name']."',
																sale_unit        				= '".$in['sale_unit']."',
																packing         			 	= '".$in['packing']."' ,
                                price_type          		= '".$in['price_type']."' ,
                                article_threshold_value = '".$in['article_threshold_value']."',
                                weight    							= '".$in['weight']."',
                                show_img_q							= '".$in['show_img_q']."',
                                supplier_id								= '".$in['supplier_id']."',
								supplier_name							= '".$in['supplier_name']."',
                                custom_threshold_value  = '".$custom_threshold_value."' ";
		Sync::start(ark::$model.'-'.ark::$method);
		$active_languages = $this -> db -> query("SELECT * FROM pim_lang GROUP BY lang_id");
		while ($active_languages -> next()) {
			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '" . $in['article_id'] . "',
		                                                                         lang_id                = '" . $active_languages -> f('lang_id') . "',
		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."' ");
		}

		$i=0;
		//***Get Attributes for articles
		$this->db->query("SELECT pim_product_attribute.* ,pim_product_attribute_category.category_id
	        		   FROM pim_product_attribute
	        		   INNER JOIN pim_product_attribute_category ON pim_product_attribute_category.product_attribute_id=pim_product_attribute.product_attribute_id
	        	       WHERE pim_product_attribute.apply_to=2 AND pim_product_attribute_category.category_id='".$in['category_id']."'
	        		   ORDER BY pim_product_attribute.sort_order ASC ,pim_product_attribute.name ASC ");
		while($this->db->move_next())
		{
			$this->attribute[$i]['product_attribute_id']=$this->db->f('product_attribute_id');
			$this->attribute[$i]['name']=$this->db->f('name');
			$this->attribute[$i]['field_name']=$this->db->f('field_name');
			$this->attribute[$i]['type']=$this->db->f('type');
			$this->attribute[$i]['items_per_line_4']=$this->db->f('items_per_line_4');
			$this->attribute[$i]['sort_order']=$this->db->f('sort_order');
			$this->attribute[$i]['searchable']=$this->db->f('searchable');
			$this->attribute[$i]['mandatory']=$this->db->f('mandatory');
			$this->attribute[$i]['filterable']=$this->db->f('filterable');
			$i++;
		}

		$this->db->query("DELETE FROM pim_product_attribute_value WHERE article_id='".$in['article_id']."'");

		if(is_array($this->attribute) && count($this->attribute) > 0){
			foreach($this->attribute as $key => $attribute_array){
				insert_product_attribute_value($in['article_id'], $attribute_array, $in,2);
			}
		}
		//insert the base price
		$this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".return_value($in['purchase_price'])."'
						");


     if($in['price_type']==1){
			$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
				//insert the default price
                 if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }
				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) - ($prices->f('price_value') * return_value($base_price)/ 100);
							}else{ //fix
								$total_price = return_value($base_price) - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) + ($prices->f('price_value') * return_value($base_price) / 100);
							}else{ //fix
								$total_price = return_value($base_price) + $prices->f('price_value');
							}

					break;
				}

					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");



			}
		}else{   //quantity discounts

			$prices = $this->db->query("SELECT * FROM article_price_volume_discount");
				while ($prices->next()){
                     //var price=((percent*base_price)/100)+base_price;
                     $total_price    = (($prices->f('percent')*return_value($in['price']))/100)+return_value($in['price']);

		             $vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		             $total_with_vat = $total_price+($total_price*$vat_value/100);

                      $this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");


				}


		}

		foreach ($in['tax_id'] as $tax_id=>$is_tax){
			$this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$tax_id."'

				");
		}

		if($in['product_id']){
			$this -> db -> query("INSERT INTO pim_product_article (
	                									product_id,
	                									article_id,
	                									block_discount
	                									)
	                									values (
	                									'" . $in['product_id'] . "',
	                									'" . $in['article_id'] . "',
	                									'" . $in['block_discount_article'] . "'


	                									)
	                			");
		}


		Sync::end($in['article_id'],$query_sync);
		if($in['product_id']){
	        $in['article_name']         = '';
			$in['article_number']    	= '';
			$in['price']    			= '';
			$in['origin_number']    	= '';
			$in['ean_code']    			= '';
			$in['article_internal_name']= '';
			$in['stock']    = '';

		}


		$in['view_add_new_article_box']=0;
		msg::success (gm( "Article succefully added."),'success');

		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$dbu = new sqldb($db_config);

		/*$show_info=$dbu->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
															AND name		= 'article-article_show_info'	");*/
		$show_info=$dbu->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
															AND name		= :name	",['user_id'=>$_SESSION['u_id'],'name'=>'article-article_show_info']);															
		if(!$show_info->move_next()) {
			/*$dbu->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
													name 	= 'article-article_show_info',
													value 	= '1' ");*/
			$dbu->insert("INSERT INTO user_meta SET 	user_id = :user_id,
													name 	= :name,
													value 	= :value ",
												['user_id' => $_SESSION['u_id'],
												 'name' 	=> 'article-article_show_info',
												 'value' 	=> '1']
											);													
		} else {
			/*$dbu->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
														  AND name 	 	= 'article-article_show_info' ");*/
			$dbu->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
														  AND name 	 	= :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'article-article_show_info']);														  
		}
		update_articles($in['article_id'],DATABASE_NAME);

		$count = $this->db->field("SELECT COUNT(article_id) FROM pim_articles ");
		if($count == 1){
			doManageLog('Created the first article.','',array( array("property"=>'first_module_usage',"value"=>'Article') ));
		}

		return true;
	}

	/**
	 * Validate add article
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in)
	{

		$v = new validation($in);

		if (ark::$method == 'update') {

			$option = ".( article_id != '" . $in['article_id'] . "')";
		} else {
			$option = "";
		}
		//$v -> f('name', 'Name', 'required:unique[pim_articles_lang.name' . $option_lang . ']');
		//  $v -> f('name', 'Name', 'required');
		//$v -> f('category_id', 'Category', 'required');
		$v -> f('item_code', 'Item Code', 'required:unique[pim_articles.item_code' . $option . ']');
		$v -> f('internal_name', 'Internal name', 'required');
		// $v -> f('vat_id', 'Vat Id', 'required[pim_vats.vat_id]');
		// $v -> f('price', 'Price', 'required:numeric');
		// $v -> f('total_price', 'Total Price', 'required:numeric');

		/*if(ALLOW_STOCK){
			$v -> f('stock', 'Stock', 'numeric');
            }
		if(ALLOW_ARTICLE_PACKING){
			if(ALLOW_ARTICLE_SALE_UNIT){
				$v -> f('sale_unit', 'Sale Unit', 'required:integer:greater_than[1]');
			}
			$v -> f('packing', 'Packing', 'required:greater_than[1]');
		}*/
		return $v -> run();
	}
}

?>