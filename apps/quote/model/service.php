<?php
/**
 * Service class
 *
 * @author      Arkweb SRL
 * @link        https://app.akti.com/
 * @copyright   [description]
 * @package 	Service
 */
class service
{
	var $pag = 'service';
	var $field_n = 'article_id';

	/**
	 * [article description]
	 * @return [type] [description]
	 */
	function service()
	{
		$this->db = new sqldb;
	}

	/**
	 * Add service
	 * @param array $in
	 * @return boolean
	 */
	function add(&$in)
	{
	   $in['failure'] = false;
		if(!$in['product_id']){ //is direct save no product involved
			if(!$this->add_validate($in)){
				
				$in['failure'] = true;
				return false;
			}
		}else{
			$in['name']         	= 	$in['article_name'];
			$in['item_code']    	= 	$in['article_number'];
			$in['internal_name']	=	$in['article_internal_name'];
			if(!$this->add_to_product_validate($in)){
				$in['failure'] = true;
				return false;
			}

		}

		$query_sync=array();
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['sale_unit']=1;
		}
        if($in['article_threshold_value']!=ARTICLE_THRESHOLD_VALUE){
        	$custom_threshold_value=1;
        }else{
        	$custom_threshold_value=0;
        }

        	if(!$in['exclude_serial_no']){
        		$in['exclude_serial_no']=0;
        	}
        	if(!$in['exclude_batch_no']){
        		$in['exclude_batch_no']=0;
        	}
		if(!$in['supplier_id']){
           $in['supplier_name']='';
		}
		$in['price_type'] = PRICE_TYPE;
		if($in['price_type']==1){
            $in['use_percent_default']=0;
		}
		if(!$in['packing']){
			$in['packing'] = 1;
		}
		if(!$in['sale_unit']){
			$in['sale_unit'] = 1;
		}

		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
															item_code  							= '".$in['item_code']."',
															ean_code  							= '".$in['ean_code']."',
															
															use_percent_default  				= '".$in['use_percent_default']."',
															supplier_reference      = '".$in['supplier_reference']."',
															vat_id									= '".$in['vat_id']."',
															article_brand_id			= '".$in['article_brand_id']."',
															ledger_account_id			= '".$in['ledger_account_id']."',
															article_category_id			= '".$in['article_category_id']."',
															origin_number   			= '".$in['origin_number']."',
														
															sale_unit        			= '".$in['sale_unit']."',
															internal_name       		= '".$in['internal_name']."',
															packing          			= '".return_value2($in['packing'])."',
															price_type          		= '".$in['price_type']."',
														
															weight    					= '".$in['weight']."',
															show_img_q								= '".$in['show_img_q']."',
															supplier_id								= '".$in['supplier_id']."',
															supplier_name							= '".$in['supplier_name']."',
															aac_price			        = '".return_value($in['aac_price'])."',
															is_service			        = '1',
															billable			        = '".$in['billable']."',
															created_at='".time()."'
															
															");

        
		$active_languages = $this -> db -> query("SELECT * FROM pim_lang GROUP BY lang_id");
		while ($active_languages -> next()) {
			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '" . $in['article_id'] . "',
		                                                                         lang_id                = '" . $active_languages -> f('lang_id') . "',
		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."' ");
		}

		

		
		//insert the base price
		$this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".return_value($in['purchase_price'])."'
						");


     if($in['price_type']==1){
			$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
				//insert the default price
                 if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }
				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) - ($prices->f('price_value') * return_value($base_price)/ 100);
							}else{ //fix
								$total_price = return_value($base_price) - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) + ($prices->f('price_value') * return_value($base_price) / 100);
							}else{ //fix
								$total_price = return_value($base_price) + $prices->f('price_value');
							}

					break;
				}

					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");



			}
		}else{   //quantity discounts

			$prices = $this->db->query("SELECT * FROM article_price_volume_discount");
				while ($prices->next()){
                     //var price=((percent*base_price)/100)+base_price;
                     $total_price    = (($prices->f('percent')*return_value($in['price']))/100)+return_value($in['price']);

		             $vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		             $total_with_vat = $total_price+($total_price*$vat_value/100);

                      $this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");


				}


		}

		foreach ($in['tax_id'] as $tax_id=>$is_tax){
			$this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$tax_id."'

				");
		}

	


		
		if($in['product_id']){
	        $in['article_name']         = '';
			$in['article_number']    	= '';
			$in['price']    			= '';
			$in['origin_number']    	= '';
			$in['ean_code']    			= '';
			$in['article_internal_name']= '';
			$in['stock']    = '';

		}
       update_articles($in['article_id'],DATABASE_NAME);
		msg::success(gm("Service succefully added."),'success');
       return true;
	}

	/**
	 * Validate add article
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in)
	{
		$v = new validation($in);
		
		$v -> f('item_code', 'Item Code', 'required:unique[pim_articles.item_code]');
		$v -> f('internal_name', 'Internal name', 'required');	
		return $v -> run();
	}

	

}	
