<?php

/**
* 
*/
class quote
{
	
	private $db;
	protected $pag = 'quote'; 						# used for log messages
	protected $field_n = 'quote_id';					# used for log messages

	function __construct()
	{
		$this->db = new sqldb();
		$this->db2 = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($db_config);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->updateCustomer_validate($in)){
			return false;
		}
        $query_sync=array();

		// $c_type = explode(',', $in['c_type']);
		
		foreach ($in['c_type'] as $key) {
			if($key){
				$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
				$c_type_name .= $type.',';
			}
		}
		$c_types = implode(',', $in['c_type']);
		$c_type_name = rtrim($c_type_name,',');


		if($in['user_id']){
			foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT first_name,last_name FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET name 				= '".$in['name']."',
											serial_number_customer  =   '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											active					= '".$in['active']."',
											legal_type				= '".$in['legal_type']."',
											user_id					= '".$acc_manager_ids."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['c_email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['comp_phone']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											c_type_name				= '".$c_type_name."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											acc_manager_name		= '".$in['acc_manager']."',
											sales_rep				= '".$in['sales_rep']."',
											is_supplier				= '".$in['is_supplier']."',
											our_reference			= '".$in['our_reference']."'
					  	WHERE customer_id='".$in['customer_id']."' ");

		if($in['customer_id_linked']){
			$link_id = $this->db->insert("INSERT INTO customer_link SET customer_id='".$in['customer_id']."',
														customer_id_linked='".$in['customer_id_linked']."',
														link_type='".$in['link_type']."'");
			if($in['link_type'] == 1){
				$type = 2;
			}
			else{
				$type = 1;
			}
			$this->db->query("INSERT INTO customer_link SET customer_id='".$in['customer_id_linked']."',
														customer_id_linked='".$in['customer_id']."',
														link_type='".$type."',
														link_id='".$link_id."'");
		}
		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."'	WHERE buyer_id='".$in['customer_id']."' ");

		
		Sync::end($in['customer_id']);

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		// $in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if($in['customer_link']){
			$v->field('link_type', 'Type of link', 'required');
		}
		return $this -> addCustomer_validate($in);

	}

	function CanEdit(&$in){
		if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
			return true;
		}
		$c_id = $in['customer_id'];
		if(!$in['customer_id'] && $in['contact_id']) {
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		}
		if($c_id){
			if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
				$u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
				if($u){
					$u = explode(',', $u);
					if(in_array($_SESSION['u_id'], $u)){
						return true;
					}
					else{
						msg::$warning = gm("You don't have enought privileges");
						return false;
					}
				}else{
					msg::$warning = gm("You don't have enought privileges");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 * @author PM
	 */
	function contact_update(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->contact_update_validate($in)){
			return false;
		}
		if(isset($in['birthdate']) && !empty($in['birthdate']) ){
			$in['birthdate'] = strtotime($in['birthdate']);
		}
		$position = implode(',', $in['position']);
		$pos = '';
		foreach ($in['position_n'] as $key => $value) {
			$pos .= $value['name'].',';
		}
		$pos = addslashes(rtrim($pos,','));
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET
                                            customer_id	=	'".$in['customer_id']."',
                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$position."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title']."',
										    e_title		=	'".$in['e_title']."',
											fax			=	'".$in['fax']."',
											company_name=	'".$in['customer']."',
											title_name	=	'".$in['title_name']."',
											position_n  = 	'".$pos."',
										    last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."'");
		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}

		Sync::end($in['contact_id']);

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}
		if(!$in['customer_id']){
			$this->removeFromAddress($in);
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],'contact_id',$in['contact_id']);

		//save extra fields values
		$this->update_custom_contact_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		// $in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}

		return $is_ok;
	}

	function select_edit(&$in){

		$query_sync=array();
		$i=0;
		foreach ($in['lines'] as $key => $value){
			if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				if(!$id){
					$filter ='';
					if ($in['table']=='customer_extrafield_dd' && is_numeric($in['extra_id']))
					{
						$filter = ", extra_field_id='".$in['extra_id']."'";
					}
					if ($in['table']=='contact_extrafield_dd' && is_numeric($in['extra_id']))
					{
						$filter = ", extra_field_id='".$in['extra_id']."'";
					}
					$inserted_id=$this->db->insert("INSERT INTO ".$in['table']." SET name='".addslashes($value['name'])."', sort_order='".($key+1)."' ".$filter." ");
					$query_sync[$i]="INSERT INTO ".$in['table']." SET id='".$inserted_id."',name='".addslashes($value['name'])."', sort_order='".($key+1)."' ".$filter." ";
					$i++;
				}
			}
		}

		Sync::start(ark::$model.'-'.ark::$method);

        foreach ($in['lines'] as $key=>$value){
			/*if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value['name'])."', sort_order='".($key+1)."' WHERE id='".$id."' ");

				}
			}*/
			if(!empty($value)){
				if($in['table']=='customer_type'){
					$info = $this->db->query("SELECT id,name FROM ".$in['table']." WHERE id='".$value['id']."' ");
					$id = $info->f('id');
					$old_value = $info->f('name');
				}else{
					$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				}
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value['name'])."', sort_order='".($key+1)."' WHERE id='".$id."' ");

					# begin update type of relationship on every customer if has this type.
					if($in['table']=='customer_type' && $old_value != $value){
						$all_c_t = array();
						$all_c_type = $this->db->query("SELECT id,name FROM customer_type ");
						while ($all_c_type->next()) {
							$all_c_t[$all_c_type->f('id')] = $all_c_type->f('name');
						}
						$cust_with_this_rel = $this->db->query("SELECT customer_id, c_type, c_type_name FROM customers");
						while($cust_with_this_rel->next()){
							$update_customer = false;
							$c_type = $cust_with_this_rel->f('c_type');
							$c_type = explode(',', $c_type);
							$type_names = '';
							foreach ($c_type as $key => $value2) {
								if(array_key_exists($value2, $all_c_t)){
									// $type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$value2."' ");
									$type_names .= $all_c_t[$value2].', ';
								}
								if($id == $value2){
									$update_customer = true;
								}
							}
							if($update_customer==true){

								$type_names = addslashes(rtrim(rtrim($type_names,' '),','));
								$this->db->query("UPDATE customers SET c_type_name = '".$type_names."' WHERE customer_id = '".$cust_with_this_rel->f('customer_id')."' ");
							}
						}
					}
					# end update

				}
			}
		}
		$in['lines'] = build_some_dd($in['table'],$in['extra_id']);
		Sync::end(0,$query_sync);
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	/****************************************************************
	* function delete_select(&$in)                                  *
	****************************************************************/
	function delete_select(&$in){
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM ".$in['table']." WHERE id='".$in['id']."' ");
		Sync::end();
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->update_financial_valid($in)){
			return false;
		}
		$apply_fix_disc = 0;
		if($in['apply_fix_disc']){$apply_fix_disc = 1;}
		$apply_line_disc = 0;
		if($in['apply_line_disc']){$apply_line_disc = 1;}
		$check_vat_number = $this->db->field("SELECT check_vat_number FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($in['btw_nr']!=$check_vat_number){
			$check_vat_number = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET
											btw_nr								=	'".$in['btw_nr']."',
											bank_name							=	'".$in['bank_name']."',
											payment_term					=	'".$in['payment_term']."',
											payment_term_type 		= '".$in['payment_term_start']."',
											bank_bic_code					=	'".$in['bank_bic_code']."',
											fixed_discount				=	'".return_value($in['fixed_discount'])."',
											bank_iban							=	'".$in['bank_iban']."',
                      						vat_id								=	'".$in['vat_id']."',
											cat_id								=	'".$in['cat_id']."',
											no_vat								=	'".$in['no_vat']."',
											currency_id						= '".$in['currency_id']."',
											invoice_email					=	'".$in['invoice_email']."',
											attention_of_invoice		   =	'".$in['attention_of_invoice']."',
											invoice_note2					=	'".$in['invoice_note2']."',
											deliv_disp_note				=	'".$in['deliv_disp_note']."',
											external_id						=	'".$in['external_id']."',
											internal_language			=	'".$in['internal_language']."',
											apply_fix_disc				= '".$apply_fix_disc."',
											apply_line_disc				= '".$apply_line_disc."',
											line_discount 				= '".return_value($in['line_discount'])."',
											comp_reg_number 				= '".$in['comp_reg_number']."',
											vat_regime_id				= '".$in['vat_regime_id']."',
											siret						= '".$in['siret']."',
											check_vat_number 			= '".$check_vat_number."',
											identity_id                 = '".$in['identity_id']."'
											WHERE customer_id			= '".$in['customer_id']."' ");
		//quote_reference			=	'".$in['quote_reference']."',

		Sync::end($in['customer_id']);
		// msg::$success = gm("Changes have been saved.");
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial_valid(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		if($in['invoice_email']){
			$v->field('invoice_email', 'Type of link', 'email');
		}

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendCustomerToZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$vars_post = array();
		$data = $this->db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ");
		if($data){
			$bar_code_start = null;
			$bar_code_end = null;
			$custom_fields = $this->db->query("SELECT * FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
			while($custom_fields->next()){
				if($custom_fields->f('field_id') == 1){
					$bar_code_start = $custom_fields->f('value');
				}
				if($custom_fields->f('field_id') == 2){
					$bar_code_end = $custom_fields->f('value');
				}
			}
			//asdkauas
			$primary = array();
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
			if($address) {
				$primary = array(
					"Unformatted"=> null,
			    "Label"=> null,
			    "Building"=> null,
			    "Street"=> $address->f('address'),
			    "PostalCode"=> $address->f('zip'),
			    "City"=> $address->f('city'),
			    "State"=> $address->f('state_id'),
			    "Country"=> get_country_name($address->f('country_id')),
			    "CountryCode"=> get_country_code($address->f('country_id'))
			  );
			}
			$test = array();
			$vars_post = array(
				'Id'											=> (string)$in['customer_id'],
				"Name"										=> $data->f('name'),
			  "VatNumber"								=> $data->f('btw_nr') ? $data->f('btw_nr') : null,
			  "TimeZone"								=> null,
			  "Note"										=> null,
			  "PictureURI"							=> null,
			  "Importance"							=> null,
			  "Blog"										=> null,
			  "WebSite"									=> $data->f('website') ? $data->f('website') : null,
			  "FixedPhoneNumber"				=> $data->f('comp_phone') ? $data->f('comp_phone') : null,
			  "MobilePhoneNumber"				=> $data->f('other_phone') ? $data->f('other_phone') : null,
			  "FaxNumber"								=> $data->f('comp_fax') ? $data->f('comp_fax') : null,
			  "EMailAddress"						=> $data->f('c_email') ? $data->f('c_email') : null,
			  "DeliveryPostalAddress"		=> null,
			  "InvoicePostalAddress"		=> null,
			  "PostalAddress"						=> $primary,
			  "Fields"									=> array(
					array(
					  "Name"								=> "Reference",
					  "Value"								=> $data->f('our_reference')
					),
					array(
					  "Name"								=> "BarcodeFrom",
					  "Value"								=> $bar_code_start
					),
					array(
					  "Name"								=> "BarcodeTo",
					  "Value"								=> $bar_code_end
					)
			  ),
			  "Links"										=> $test,
			  "Categories"							=> null
			);

			$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'put',$vars_post);
			console::log($put);
			$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
		}
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		/*if($vat_numeric || substr($value,0,2)=="BE"){
			$trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
			$trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
			$trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

			if(!$trends_access_token || $trends_expiration<time()){
				$ch = curl_init();
				$headers=array(
					"Content-Type: x-www-form-urlencoded",
					"Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
					);
				/*$trends_data=!$trends_access_token ? "grant_type=password&username=akti_api&password=akti_api" : "grant_type=refresh_token&&refresh_token=".$trends_refresh_token;* /
				$trends_data="grant_type=password&username=akti_api&password=akti_api";

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
		       	curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
			    curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
			 	console::log('aaaaa');
			 	console::log($info);* /

			 	if($info['http_code']==400){
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else{
			 		if(!$trends_access_token){
			 			$this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
					 	$this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
					 	$this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
			 		}else{
			 			$this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
			 			$this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
			 			$this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
			 		}

			 		$ch = curl_init();
					$headers=array(
						"Authorization: Bearer ".$put->access_token
						);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    if($vat_numeric){
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
				    }else{
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
				    }
				    $put = json_decode(curl_exec($ch));
				 	$info = curl_getinfo($ch);

				 	/*console::log($put);
				 	console::log('bbbbb');
				 	console::log($info);* /

				    if($info['http_code']==400 || $info['http_code']==429){
				    	if(ark::$method == 'check_vies_vat_number'){
					 		msg::error ( $put->error,'error');
					 		json_out($in);
					 	}
						return false;
				 	}else if($info['http_code']==404){
				 		if($vat_numeric){
				 			if(ark::$method == 'check_vies_vat_number'){
					 			msg::error ( gm("Not a valid vat number"),'error');
					 			json_out($in);
					 		}
							return false;
				 		}
				 	}else{
				 		if($in['customer_id']){
				 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 			if($country_id != 26){
				 				$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 				$in['remove_v']=1;
				 			}else if($country_id == 26){
					 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
					 			$in['add_v']=1;
					 		}
				 		}
				 		$in['comp_name']=$put->officialName;
				 		$in['comp_address']=$put->street.' '.$put->houseNumber;
						$in['comp_zip']=$put->postalCode;
						$in['comp_city']=$put->city;
				 		$in['comp_country']='26';
				 		$in['trends_ok']=true;
				 		$in['trends_lang']=$_SESSION['l'];
				 		$in['full_details']=$put;
				 		if(ark::$method == 'check_vies_vat_number'){
					 		msg::success(gm('Success'),'success');
					 		json_out($in);
					 	}
				 		return false;
				 	}
			 	}
			}else{
				$ch = curl_init();
				$headers=array(
					"Authorization: Bearer ".$trends_access_token
					);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    if($vat_numeric){
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
			    }else{
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
			    }
			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
				console::log('ccccc');
				console::log($info);* /

			    if($info['http_code']==400 || $info['http_code']==429){
			    	if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else if($info['http_code']==404){
			 		if($vat_numeric){
			 			if(ark::$method == 'check_vies_vat_number'){
				 			msg::error (gm("Not a valid vat number"),'error');
				 			json_out($in);
				 		}
						return false;
			 		}
			 	}else{
			 		if($in['customer_id']){
			 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 		if($country_id != 26){
				 			$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['remove_v']=1;
				 		}else if($country_id == 26){
				 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['add_v']=1;
				 		}
				 	}
			 		$in['comp_name']=$put->officialName;
			 		$in['comp_address']=$put->street.' '.$put->houseNumber;
					$in['comp_zip']=$put->postalCode;
					$in['comp_city']=$put->city;
				 	$in['comp_country']='26';
			 		$in['trends_ok']=true;
			 		$in['trends_lang']=$_SESSION['l'];
			 		$in['full_details']=$put;
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::success(gm('Success'),'success');
				 		json_out($in);
				 	}
			 		return false;
			 	}
			}
		}*/


		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddC(&$in){
		if(!$this->tryAddCValid($in)){ 
			json_out($in);
			return false; 
		}
		$in['quote_id'] = $in['item_id'];

		//$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

		//Set account default vat on customer creation
	    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	    if(empty($vat_regime_id)){
	      $vat_regime_id = 0;
	    }

	    $c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			/*foreach ($in['c_type'] as $key) {
				if($key){
					$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
					$c_type_name .= $type.',';
				}
			}
			$c_types = implode(',', $in['c_type']);
			$c_type_name = rtrim($c_type_name,',');*/
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']==''){
			$in['user_id']=$_SESSION['u_id'];
		}


		if($in['user_id']){
			/*foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);*/
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				//$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}
		$vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
	        $vat_default=str_replace(',', '.', $vat);
		$selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");

		if($in['add_customer']){
			$payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       		$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");

       		$account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
       		if(SET_DEF_PRICE_CAT == 1){
				$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
			}
         		
		  	$in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
		                                        name='".addslashes($in['name'])."',
		                                        our_reference = '".$account_reference_number."',
		                                        btw_nr='".$in['btw_nr']."',
		                                        vat_regime_id     = '".$vat_regime_id."',
		                                        city_name = '".$in['city']."',
		                                        c_email ='".$in['email']."',
                                            	comp_phone ='".$in['phone']."',
		                                        user_id = '".$acc_manager_ids."',
                                            	acc_manager_name = '".addslashes($in['acc_manager'])."',
		                                        country_name ='".get_country_name($in['country_id'])."',
		                                        active='1',
		                                        creation_date = '".time()."',
		                                        payment_term 			= '".$payment_term."',
												payment_term_type 		= '".$payment_term_type."',
												type 					= '0',
		                                        zip_name  = '".$in['zip']."',
		                                        commercial_name 		= '".$in['commercial_name']."',
												legal_type				= '".$in['legal_type']."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												sector					= '".$in['sector']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$c_type_name."',
												vat_id 					= '".$selected_vat."',
												invoice_email_type		= '1',
												sales_rep				= '".$in['sales_rep_id']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												stripe_cust_id			= '".$in['stripe_cust_id']."',
												cat_id					= '".$in['cat_id']."'");
		  
		  	$this->db->query("INSERT INTO customer_addresses SET
		                    address='".$in['address']."',
		                    zip='".$in['zip']."',
		                    city='".$in['city']."',
		                    country_id='".$in['country_id']."',
		                    customer_id='".$in['buyer_id']."',
		                    is_primary='1',
		                    delivery='1',
		                    billing='1',
		                    site='1' ");
		  	$in['customer_name'] = $in['name'];

		  	if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
					if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
						$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
					}
				}
			}

		  // include_once('../apps/company/admin/model/customer.php');
		  	$in['value']=$in['btw_nr'];
		  // $comp=new customer();       
		  // $this->check_vies_vat_number($in);
		  	if($this->check_vies_vat_number($in) != false){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
		  	}else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
		  	}

		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                            AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);		                            
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                        name    = 'company-customers_show_info',
		                        value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                        name    = :name,
		                        value   = :value ",
		                    ['user_id' => $_SESSION['u_id'],
		                     'name'    => 'company-customers_show_info',
		                     'value'   => '1']
		                );		                        
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                            AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);		                            
		 	}
		  	$count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
		  	if($count == 1){
		    	doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
		  	}

		  	if($in['item_id'] && is_numeric($in['item_id'])){
		  		$address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
		  		$this->db->query("UPDATE tblquote SET 
		  			buyer_id='".$in['buyer_id']."', 
		  			buyer_name='".$in['name']."',
		  			acc_manager_id='".$_SESSION['u_id']."',
		  			acc_manager_name='".$name_user."',
		  			contact_id='',
		  			delivery_address='', 
		  			delivery_address_id='', 
		  			free_field = '".$address."'
		  			WHERE id='".$in['item_id']."' ");		  		
		  	}
		  	insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
		  	msg::success (gm('Success'),'success');
		  	return true;
		}
		if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");

       $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
       if(SET_DEF_PRICE_CAT == 1){
			$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
		}
        
        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['lastname'])."',
                                            our_reference = '".$account_reference_number."',
                                            firstname = '".addslashes($in['firstname'])."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            creation_date = '".time()."',
                                            type = 1,
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."',
                                            commercial_name 		= '".$in['commercial_name']."',
											legal_type				= '".$in['legal_type']."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											sector					= '".$in['sector']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											c_type_name				= '".$c_type_name."',
											vat_id 					= '".$selected_vat."',
											invoice_email_type		= '1',
											sales_rep				= '".$in['sales_rep_id']."',
											internal_language		= '".$in['internal_language']."',
											is_supplier				= '".$in['is_supplier']."',
											is_customer				= '".$in['is_customer']."',
											stripe_cust_id			= '".$in['stripe_cust_id']."',
											cat_id					= '".$in['cat_id']."'");
      
        $this->db->query("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
					$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
				}
			}
		}

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                        ['user_id' => $_SESSION['u_id'],
                         'name'    => 'company-customers_show_info',
                         'value'   => '1']
                    );                            
        } else {
          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          $this->db->query("UPDATE tblquote SET 
            buyer_id='".$in['buyer_id']."', 
            buyer_name='".$in['name']."',
            acc_manager_id='".$_SESSION['u_id']."',
		  	acc_manager_name='".$name_user."',
            contact_id='',
            free_field = '".$address."'
            WHERE id='".$in['item_id']."' ");         
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
		if($in['add_contact']){
			$customer_name='';
			if($in['buyer_id']){
				$customer_name = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
			}
			if($in['birthdate']){
				$in['birthdate'] = strtotime($in['birthdate']);
			}
		  	$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
						customer_id	=	'".$in['buyer_id']."',
						firstname	=	'".$in['firstname']."',
						lastname	=	'".$in['lastname']."',
						email		=	'".$in['email']."',
						birthdate	=	'".$in['birthdate']."',
						cell		=	'".$in['cell']."',
						sex			=	'".$in['sex']."',
						title		=	'".$in['title_contact_id']."',
						language	=	'".$in['language']."',
						company_name=	'".$customer_name."',
						`create`	=	'".time()."'");

		  	$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
			if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				}
			}

		  	if($in['buyer_id']){
				$contact_accounts_sql="";
				$accepted_keys=["position","department","title","e_title","email","phone","fax","s_email","customer_address_id"];
		 		foreach($in['accountRelatedObj'] as $key=>$value){
		 			if(in_array($value['model'],$accepted_keys)){
		 				$contact_accounts_sql.="`".$value['model']."`='".addslashes($value['model_value'])."',";
		 			}
		 		}
		 		if($in['email']){
		 			$contact_accounts_sql.="`email`='".$in['email']."',";
		 		}
		 		if(!empty($contact_accounts_sql)){
		 			$contact_accounts_sql=rtrim($contact_accounts_sql,",");
		 			$this->db->query("INSERT INTO customer_contactsIds SET 
			  								customer_id='".$in['buyer_id']."', 
			  								contact_id='".$in['contact_id']."',
			  								".$contact_accounts_sql." ");
		 		}	
		 	}
		  	$in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
		  	if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
	                $vars=array();
	                if($in['buyer_id']){
	                    $vars['customer_id']=$in['buyer_id'];
	                    $vars['customer_name']=$customer_name;
	                }          
	                $vars['contact_id']=$in['contact_id'];
	                $vars['firstname']=$in['firstname'];
	                $vars['lastname']=$in['lastname'];
	                $vars['table']='customer_contacts';
	                $vars['email']=$in['email'];
	                $vars['op']='add';
	                synctoZendesk($vars);
	            }
		  	if($in['country_id']){
		    	$this->db->query("INSERT INTO customer_contact_address SET
		                      address='".$in['address']."',
		                      zip='".$in['zip']."',
		                      city='".$in['city']."',
		                      country_id='".$in['country_id']."',
		                      contact_id='".$in['contact_id']."',
		                      is_primary='1',
		                      delivery='1' ");
		  	}
		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                                AND name    = 'company-contacts_show_info'  ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                                AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                              name    = 'company-contacts_show_info',
		                              value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                              name    = :name,
		                              value   = :value ",
		                            ['user_id' => $_SESSION['u_id'],
		                             'name'    => 'company-contacts_show_info',
		                             'value'   => '1']
		                        );		                              
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                                  AND name    = 'company-contacts_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                                  AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                  
		  	}
		  	$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
		  	if($count == 1){
		    	doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
		  	}
		  	if($in['item_id'] && is_numeric($in['item_id'])){
		  		$this->db->query("UPDATE tblquote SET contact_id='".$in['contact_id']."' WHERE id='".$in['item_id']."' ");	
		  	}
		  	insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		  	msg::success (gm('Success'),'success');
		}
		
		return true;
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddCValid(&$in)
	{
		if($in['add_customer']){
		  	$v = new validation($in);
			$v->field('name', 'name', 'required:unique[customers.name]');
			$v->field('country_id', 'country_id', 'required');
			return $v->run();  
		}
		if($in['add_contact']){
			$v = new validation($in);
			$v->field('firstname', 'firstname', 'required');
			$v->field('lastname', 'lastname', 'required');
			return $v->run();  
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return boolean
	 * @author PM
	 **/
	function updateCustomerData(&$in)
	{

		if($in['field'] == 'contact_id'){
			$in['buyer_id'] ='';
		}
		if($in['quote_id'] && is_numeric($in['quote_id'])){
		    $buyer_exist = $this->db->field("SELECT buyer_id FROM tblquote WHERE id='".$in['quote_id']."'");
		    if($buyer_exist!=$in['buyer_id']){
		      $in['save_final']=1;
		    }elseif($buyer_exist==$in['buyer_id']){
		      $in['save_final']=0;
		    }
		}elseif($in['duplicate_quote_id'] && is_numeric($in['duplicate_quote_id'])){
			$buyer_exist = $this->db->field("SELECT buyer_id FROM tblquote WHERE id='".$in['duplicate_quote_id']."'");
		    if($buyer_exist!=$in['buyer_id']){
		      $in['save_final']=1;
		    }elseif($buyer_exist==$in['buyer_id']){
		      $in['save_final']=0;
		    }
		}

		$sql = "UPDATE tblquote SET ";

		if($in['buyer_id']){
			$buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone,customers.comp_fax, customers.name AS company_name, customers.no_vat, customers.btw_nr, 
									customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, 
									customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount, customer_addresses.address_id,customers.vat_regime_id, customers.cat_id,customer_legal_type.name as l_name
									FROM customers
									LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
									AND customer_addresses.is_primary=1
									LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
									WHERE customers.customer_id='".$in['buyer_id']."' ");
			$buyer_info->next();
			/*if(!$in['delivery_address']){
				$delivery_addr = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 LIMIT 1 ");
				if($delivery_addr){
					$in['delivery_address'] = $delivery_addr->f('address')."\n".$delivery_addr->f('zip').'  '.$delivery_addr->f('city')."\n".get_country_name($delivery_addr->f('country_id'));
					$in['delivery_address_id']=$delivery_addr->f('address_id');
				}
			}*/
			if($in['item_id'] == 'tmp' && !$in['delivery_address_id']){
		        $in['delivery_address_id'] = $buyer_info->f('address_id');
		    }

			$apply_discount =QUOTE_APPLY_DISCOUNT;
			
			/*if($in['duplicate_quote_id']==''){
				$in['main_address_id'] = $buyer_info->f('address_id');
			}*/
			if($in['main_address_id']){
		        $is_customer_address=$this->db->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['main_address_id']."' AND customer_id='".$in['buyer_id']."' ");
		        if(!$is_customer_address){
		          unset($in['main_address_id']);
		        }
		      }

			if(!$in['main_address_id']){
				$in['main_address_id'] = $buyer_info->f('address_id');
			}

			if(!$buyer_info->f('apply_line_disc')){
					if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
						$apply_discount = 3;
					}else{
						if($buyer_info->f('apply_fix_disc')){
							$apply_discount = 2;
						}
					}
			} else {
				$apply_discount = 1;
				if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
					$apply_discount = 3;
				}else{
					if($buyer_info->f('apply_fix_disc')){
						$apply_discount = 2;
					}
				}
			} 
			/*if($buyer_info->f('apply_fix_disc')){
				$apply_fix_disc = 1;
			}*/

			$in['apply_discount'] = $apply_discount;
			$in['apply_fix_disc'] = $apply_fix_disc;
		    
			if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
				$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
			}
			$in['currency_rate']=0;
			$in['currency_id']	= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
			if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
				$in['currency_rate'] = $this->currencyRate(currency::get_currency($in['currency_id'],'code'));
			}
			$in['contact_name'] ='';

			$b_country_id=$buyer_info->f('country_id');
			$b_city=$buyer_info->f('city');
			$b_zip=$buyer_info->f('zip');
			$b_address=$buyer_info->f('address');
			$in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
			if($buyer_info->f('address_id') != $in['main_address_id']){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
				$in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$b_country_id=$buyer_addr->f('country_id');
				$b_city=$buyer_addr->f('city');
				$b_zip=$buyer_addr->f('zip');
				$b_address=$buyer_addr->f('address');
			}

			if($in['sameAddress'] !=1 && $in['delivery_address_id']){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
				$in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$b_country_id=$buyer_addr->f('country_id');
				$b_city=$buyer_addr->f('city');
				$b_zip=$buyer_addr->f('zip');
				$b_address=$buyer_addr->f('address');
			}		
			
			$sql .= " buyer_id = '".$in['buyer_id']."', ";
			$sql .= " buyer_name = '".addslashes(stripslashes($buyer_info->f('company_name').' '.$buyer_info->f('l_name')))."', ";
			$sql .= " buyer_email = '".$buyer_info->f('c_email')."', ";
			$sql .= " buyer_btw_nr = '".addslashes($buyer_info->f('btw_nr'))."', ";
			$sql .= " buyer_country_id = '".$b_country_id."', ";
			$sql .= " buyer_city = '".addslashes($b_city)."', ";
			$sql .= " buyer_zip = '".$b_zip."', ";
			$sql .= " buyer_phone = '".$buyer_info->f('comp_phone')."', ";
			$sql .= " buyer_fax = '".$buyer_info->f('comp_fax')."', ";
			$sql .= " buyer_address = '".addslashes($b_address)."', ";
			$sql .= " main_address_id = '".$in['main_address_id']."', ";
			$sql .= " vat_regime_id = '".$buyer_info->f('vat_regime_id')."', ";
			$sql .= " cat_id = '".$buyer_info->f('cat_id')."', ";
			if($in['vat_regime_changed']){
                unset($in['vat_regime_changed']);
            }
            if($in['vat_regime_id'] != $buyer_info->f('vat_regime_id')){
                $in['vat_regime_changed']=$buyer_info->f('vat_regime_id');
            }

            if($in['buyer_changed_save']){
            	unset($in['buyer_changed_save']);
            }

            if($in['buyer_changed']){
            	unset($in['buyer_changed']);
            	$in['discount_line_gen']=$buyer_info->f('line_discount');
            	$in['buyer_changed_save']=true;
            }
            if($in['save_final']==1){
            	$in['buyer_changed']=true;
            	$in['discount_line_gen']=$buyer_info->f('line_discount');
            }

			//Set email language as account / contact language
			$emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
	    						  'contact_id' 		=> $in['contact_id'],
	    						  'item_id'			=> $in['quote_id'],
	    						  'email_language' 	=> $in['email_language'],
	    						  'table'			=> 'tblquote',
	    						  'table_label'		=> 'id',
	    						  'table_buyer_label' => 'buyer_id',
	    						  'table_contact_label' => 'contact_id',
	    						  'param'		 	=> 'update_customer_data');

			$email_language = get_email_language($emailMessageData);
	    	//End Set email language as account / contact language

	    	$in['identity_id'] = get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);

			$sql .= " email_language = '".$email_language."', ";

			// $sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
			//$sql .= " delivery_address = '".addslashes($in['delivery_address'])."', ";
			$sql .= " delivery_address = '".addslashes($in['address_info'])."', ";
			$sql .= " delivery_address_id = '".$in['delivery_address_id']."', ";
			$sql .= " currency_type = '".$in['currency_id']."', ";
			$sql .= " currency_rate = '".$in['currency_rate']."', ";
			$sql .= " vat = '".get_customer_vat($in['buyer_id'])."', ";
			if($in['sameAddress']==1){
				$sql .= " delivery_address = '', ";
				$sql .= " delivery_address_id = '', ";
			}
			//$sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
			$sql .= " identity_id = '".$in['identity_id']."', ";
			// $sql .= " customer_ref = '".addslashes($buyer_info->f('our_reference'))."', ";
			$sql .= " buyer_reference = '".addslashes($ref)."', ";
			
			if($in['contact_id']){
				$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
				// $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
				$sql .= " contact_id = '".$in['contact_id']."', ";
				$in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
			}else{
				// $sql .= " contact_name = '', ";
				$sql .= " contact_id = '', ";
			}

			$sql .= " free_field = '".addslashes($in['address_info'])."' ";
			// $sql .= " field = 'customer_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '".$in['apply_discount']."' ";
				// $sql .= ",  remove_vat = '".$buyer_info->f('no_vat')."' ";
				$sql .= ",  discount_line_gen = '".$buyer_info->f('line_discount')."' ";
				$sql .= ",  discount = '".$buyer_info->f('fixed_discount')."' ";
				foreach ($in['quote_group'] as $key => $value) {
					foreach($value['table'] as $key2 => $value2){
						foreach($value2['quote_line'] as $key3 => $value3){
                            $params = array(
								'article_id' => $value3['article_id'], 
								'price' => $value['price'], 
								'quantity' => $value['quantity'], 
								'customer_id' => $in['buyer_id'],
								'cat_id' => $in['cat_id'],
								'asString' => true
							);
                            if (!$value3['component_for']) {
                                $price = display_number($this->getArticlePrice($params));
                            } else {
                                $price = display_number(0);
                            }
							if($value3['article_id']){
								$in['quote_group'][$key]['table'][$key2]['quote_line'][$key3]['price'] = $price;
							}								 
						}
					}
				}
			}

		}else{
			if(!$in['contact_id']){
				msg::error ( gm('Please select a company or a contact'),'error');
				return false;
			}
			$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
			$sql .= " buyer_id = '', ";
			$sql .= " buyer_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
			// $sql .= " contact_name = '', ";
			$sql .= " contact_id = '".$in['contact_id']."', ";
			$sql .= " buyer_email = '".$contact_info->f('email')."', ";
			// $sql .= " customer_vat_number = '', ";
			$sql .= " buyer_country_id = '".$contact_address->f('country_id')."', ";
			$sql .= " buyer_city = '".addslashes($contact_address->f('city'))."', ";
			$sql .= " buyer_zip = '".$contact_address->f('zip')."', ";
			$sql .= " buyer_phone = '".$contact_info->f('phone')."', ";
			$sql .= " buyer_address = '".addslashes($contact_address->f('address'))."', ";
			$sql .= " email_language = '".DEFAULT_LANG_ID."', ";
			// $sql .= " delivery_address = '', ";
			// $sql .= " delivery_address_id = '', ";
			$sql .= " identity_id = '".$in['identity_id']."', ";
			$sql .= " currency_type = '".ACCOUNT_CURRENCY_TYPE."', ";
			$sql .= " currency_rate = '0', ";
			// $sql .= " customer_ref = '', ";
			$sql .= " buyer_reference = '', ";
			$in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
			$sql .= " free_field = '".addslashes($in['address_info'])."' ";
			// $sql .= " field = 'contact_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '0' ";
				// $sql .= ",  remove_vat = '0' ";
				$sql .= ",  discount_line_gen = '0' ";
				$sql .= ",  discount = '0' ";
				foreach ($in['quote_group'] as $key => $value) {
					foreach($value['table'] as $key2 => $value2){
						foreach($value2['quote_line'] as $key3 => $value3){
							$params = array(
								'article_id' => $value3['article_id'], 
								'price' => $value['price'], 
								'quantity' => $value['quantity'], 
								'customer_id' => $in['buyer_id'],
								'cat_id' => $in['cat_id'],
								'asString' => true
							);
                            if (!$value3['component_for']) {
                                $price = display_number($this->getArticlePrice($params));
                            } else {
                                $price = display_number(0);
                            }
							if($value3['article_id']){
								$in['quote_group'][$key]['table'][$key2]['quote_line'][$key3]['price'] = $price;
							}								 
						}
					}
				}
			}
		}
		if($in['duplicate_quote_id']){
			$sql .=" WHERE id ='".$in['duplicate_quote_id']."' ";
		}else{
			$sql .=" WHERE id ='".$in['item_id']."' ";
		}
		if(!$in['isAdd'] && !$in['duplicate_quote_id']){
			$this->db->query($sql);	
			if($in['item_id'] && is_numeric($in['item_id'])){
		        $trace_id=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".($in['duplicate_quote_id'] ? $in['duplicate_quote_id'] : $in['item_id'])."' ");
		        if($trace_id && $in['buyer_id']){
		          $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
		        }
		        if(!$in['duplicate_quote_id']){
		        	$in['quote_id'] = $in['item_id'];
		        	$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
					$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
					$this->quote_lines($in);
		        }
		    }		
		}
		if($in['duplicate_quote_id']){
			//$in['quote_id'] = $in['duplicate_quote_id'];
		}else{
			$in['quote_id'] = $in['item_id'];
		}
		$in['changed_customer'] = 1;

		msg::success (gm("Sync successfull."),'success');	
		return true;
	}

		function updatePriceCategory(&$in)
	{
		if($in['quote_id']){
			$this->db->query("UPDATE tblquote SET cat_id ='".$in['cat_id']."' WHERE id ='".$in['quote_id']."'");

			if($in['buyer_id']){
				if($in['changeCatPrices']==1){
					foreach ($in['quote_group'] as $key => $value) {
						foreach($value['table'] as $key2 => $value2){
							foreach($value2['quote_line'] as $key3 => $value3){
								$params = array(
									'article_id' => $value3['article_id'], 
									'price' => $value['price'], 
									'quantity' => $value['quantity'], 
									'customer_id' => $in['buyer_id'],
									'cat_id' => $in['cat_id'],
									'asString' => true
								);
								$price = display_number($this->getArticlePrice($params));
								
								if($value3['article_id']){
									$in['quote_group'][$key]['table'][$key2]['quote_line'][$key3]['price'] = $price;
								}								 
								 //console::log($price, $in['quote_group'][$key]['table'][$key2]['quote_line'][$key3]['price'],$in['quote_group'][$key]['table'][$key2]['quote_line'][$key3]['article_id']);
							}
						}
					}


				}
				msg::success (gm("Sync successfull."),'success');	
				ark::run('quote-nquote-quote-update');
			}
		}

		return true;
   }

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
		if(!$this->CanEdit($in)){
			json_out($in);
			return false;
		}
		if(!$this->tryAddAddressValidate($in)){
			json_out($in);
			return false;
		}

		$country_n = get_country_name($in['country_id']);
		if($in['field']=='customer_id'){

	 		Sync::start(ark::$model.'-'.ark::$method);

			$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																				customer_id			=	'".$in['customer_id']."',
																				country_id			=	'".$in['country_id']."',
																				state_id			=	'".$in['state_id']."',
																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				billing				=	'".$in['billing']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
			if($in['billing']){
				$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}

			Sync::end($address_id);

			/*if($in['delivery']){
				$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}*/
			if($in['primary']){
				if($in['address'])
				{
					if(!$in['zip'] && $in['city'])
					{
						$address = $in['address'].', '.$in['city'].', '.$country_n;
					}elseif(!$in['city'] && $in['zip'])
					{
						$address = $in['address'].', '.$in['zip'].', '.$country_n;
					}elseif(!$in['zip'] && !$in['city'])
					{
						$address = $in['address'].', '.$country_n;
					}else
					{
						$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
					}
					$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
					$output= json_decode($geocode);
					$lat = $output->results[0]->geometry->location->lat;
					$long = $output->results[0]->geometry->location->lng;
				}
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
				$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
				$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
			}
		}else{			
			$address_id = $this->db->insert("INSERT INTO customer_contact_address SET
																				contact_id			=	'".$in['contact_id']."',
																				country_id			=	'".$in['country_id']."',																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
		}
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['address_id'] = $address_id;
		$in['country'] = $country_n;
		$in['quote_id'] = $in['item_id'];
		if($in['quote_id'] && is_numeric($in['quote_id'])){
			if($in['primary']){
				$deliveryAddress = '';
				$deliveryAddressId = '';
				// $this->db->query("UPDATE tblquote SET 
		  // 			delivery_address='', 
		  // 			delivery_address_id='' WHERE id='".$in['quote_id']."'  ");
  			} else {
	  			$delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	  			$deliveryAddress = $delivery_address;
				$deliveryAddressId = $address_id;
				// $this->db->query("UPDATE tblquote SET 
				// 	free_field = '".$delivery_address."',
		  // 			delivery_address='".$delivery_address."', 
		  // 			delivery_address_id='".$address_id."' WHERE id='".$in['quote_id']."'  ");	
  			}
  			//Set in table tblquotes new address values	
			$sql = "UPDATE tblquote SET ";
			$buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, 
									customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, 
									customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount, customer_addresses.address_id
									FROM customers
									LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
									AND customer_addresses.is_primary=1
									WHERE customers.customer_id='".$in['customer_id']."' ");
			$buyer_info->next();

			$b_country_id=$buyer_info->f('country_id');
			$b_city=$buyer_info->f('city');
			$b_zip=$buyer_info->f('zip');
			$b_address=$buyer_info->f('address');

			$in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));

			if($address_id != $in['main_address_id'] && !$in['primary']){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$address_id."' ");
				$in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
				$b_country_id=$buyer_addr->f('country_id');
				$b_city=$buyer_addr->f('city');
				$b_zip=$buyer_addr->f('zip');
				$b_address=$buyer_addr->f('address');
			}
			
			$sql .= " delivery_address = '".$deliveryAddress."', ";
			$sql .= " delivery_address_id = '".$deliveryAddressId."', ";
			$sql .= " buyer_id = '".$in['customer_id']."', ";
			$sql .= " buyer_name = '".addslashes($buyer_info->f('name'))."', ";
			$sql .= " buyer_email = '".$buyer_info->f('c_email')."', ";
			$sql .= " buyer_country_id = '".$b_country_id."', ";
			$sql .= " buyer_city = '".addslashes($b_city)."', ";
			$sql .= " buyer_zip = '".$b_zip."', ";
			$sql .= " buyer_phone = '".$buyer_info->f('comp_phone')."', ";
			$sql .= " buyer_address = '".addslashes($b_address)."', ";
			if($in['primary']){
				$sql .= " main_address_id = '".$address_id."', ";	
			}
			$sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
			$sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
			$sql .= " free_field = '".addslashes($in['address_info'])."' ";
			$sql .=" WHERE id ='".$in['item_id']."' ";
			$this->db->query($sql);
			//End Set in table tblquotes new address values	



	  	}elseif ($in['quote_id'] == 'tmp') {
	  		$in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	  		$in['delivery_address_id'] = $address_id;
	  	}


	  	$in['buyer_id'] = $in['customer_id'];
		// $in['pagl'] = $this->pag;
		
		 // json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddressValidate(&$in)
	{
		$v = new validation($in);

		if($in['customer_id']){
			$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

			if(!$in['primary'] && !$in['delivery'] && !$in['billing'] && !$in['site']){
				$v->field('primary', 'primary', 'required',gm("Main Address is mandatory"));
				$v->field('delivery', 'delivery', 'required',gm("Delivery Address is mandatory"));
				$v->field('billing', 'billing', 'required',gm("Billing Address is mandatory"));
				$v->field('site', 'site', 'required',gm("Site Address is mandatory"));

				$message = 'You must select an address type.';
				$is_ok = $v->run();
				if(!$is_ok){
					msg::error(gm($message),'error');
					return false;		
				}
			} else {
				$v->field('address', 'Address', 'required:text',gm("Street is mandatory"));
				$v->field('zip', 'Zip', 'required:text',gm("Zip Code is mandatory"));
				$v->field('city', 'City', 'required:text',gm("City is mandatory"));
				$v->field('country_id', 'Country', 'required:exist[country.country_id]');
			} 

		}else if($in['contact_id']){
			$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		}else{
			msg::error(gm('Invalid ID'),'error');
			return false;
		}
		$v->field('country_id', 'Country', 'required:exist[country.country_id]',gm("Country is mandatory"));

		return $v->run();
	}

	/**
	 * add quote function
	 *
	 * @return void
	 * @author PM
	 **/
	function add(&$in)
	{

		$f_archived = '0';
		//$serial_number=$in['serial_number'];
		if($in['template']){
			$f_archived='2';
			if(!$in['template_name']){
				msg::error (gm("Template name required"),'template_name');
				json_out($in);
			}

			$pdf_settings_data=array(
                    'GRAND_TOTAL'=> '1',  
                    'SHOW_DISCOUNT_TOTAL'=> '0',  
                    'SHOW_VAT_ON_PDF'=> '1',
                    'SHOW_VAT_COLUMN_PDF'=> '0',
                    'SHOW_ARTICLE_IMAGE'=> '0',
                    'ITEM_CODE'=>'1',
                    'QUANTITY'=> '1',
                	'UNIT_PRICE'=> '1',
                	'PRICE_VAT'=>'0',
                    'PACK_SU'=> '0',
                    'DISCOUNT'=> '1',
                    'LINE_AMOUNT'=> '1',
                    'SHOW_CHAPTER_TOTAL'=> '0',
                    'CONSIDER_AS_OPTION'=> '0');
	        foreach($pdf_settings_data as $key=>$value){
	            $setting_activated=$this->db->field("SELECT value FROM settings WHERE constant_name='QUOTE_PDF_SETTING_".$key."' ");
	            if(!is_null($setting_activated)){
	                 $pdf_settings_data[$key]=$setting_activated == '1' ? '1' : '0';
	            }            
	        }
	        $in['show_vat'] = $pdf_settings_data['SHOW_VAT_ON_PDF'];
			$in['show_vat_pdf'] = $pdf_settings_data['SHOW_VAT_COLUMN_PDF'];
			$in['show_grand'] = $pdf_settings_data['GRAND_TOTAL'];
			$in['show_discount_total'] = $pdf_settings_data['SHOW_DISCOUNT_TOTAL'];
			$in['show_d'] = $pdf_settings_data['DISCOUNT'];
			$in['show_img_q'] = $pdf_settings_data['SHOW_ARTICLE_IMAGE'];
			$in['show_QC'] = $pdf_settings_data['QUANTITY'];
			$in['show_UP'] = $pdf_settings_data['UNIT_PRICE'];
			$in['show_PS'] = $pdf_settings_data['PACK_SU'];
			$in['show_AC'] = $pdf_settings_data['LINE_AMOUNT'];	
			$in['show_chapter_total'] = $pdf_settings_data['SHOW_CHAPTER_TOTAL'];	
			$in['show_block_total'] = $pdf_settings_data['CONSIDER_AS_OPTION'];
			$in['show_price_vat']	= $pdf_settings_data['PRICE_VAT'];
			
			$serial_number=$in['template_name'];
		}else{
			if(!$this->add_validate($in))
			{
				json_out($in);
				return false;
			}
			//$serial_number = $in['serial_number'];
			$serial_number = generate_quote_number(DATABASE_NAME);
		}

		$created_date= date(ACCOUNT_DATE_FORMAT);
		if(!$in['pdf_layout'] && $in['identity_name']){
	      $in['pdf_layout'] = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_name']."' and module='quote'");
	    }

    	if( $in['main_address_id'] ){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$in['free_field'] = addslashes($buyer_addr->f('address'))."\n".addslashes($buyer_addr->f('zip')).'  '.addslashes($buyer_addr->f('city'))."\n".get_country_name($buyer_addr->f('country_id'));
			$in['buyer_address']=$buyer_addr->f('address');
			$in['buyer_zip']=$buyer_addr->f('zip');
			$in['buyer_city']=$buyer_addr->f('city');
			$in['buyer_country_id']=$buyer_addr->f('country_id');
		}
		if( $in['delivery_address_id']){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
			$in['delivery_address'] = addslashes($buyer_addr->f('address'))."\n".addslashes($buyer_addr->f('zip')).'  '.addslashes($buyer_addr->f('city'))."\n".addslashes(get_country_name($buyer_addr->f('country_id')));
		}
	    $customer_data = $this->db->query("SELECT customers.name AS company_name,customer_legal_type.name as l_name FROM customers 
	    	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
	    	WHERE customers.customer_id='".$in['buyer_id']."' ");
	    $buyer_name=stripslashes($customer_data->f('company_name').' '.$customer_data->f('l_name'));
	    $in['validity_date2'] = strtotime($in['validity_date']);
	    $in['quote_date'] = strtotime($in['quote_date']);

	    //Set email language as account / contact language
	    $emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
	    						  'contact_id' 		=> $in['contact_id'],
	    						  'email_language' 	=> $in['email_language'],
	    						  'param'		 	=> 'add');
	    if(!$in['template']){
			$in['email_language'] = get_email_language($emailMessageData);
		}
	    //End Set email language as account / contact language

	    if(!$in['currency_type']){
	    	$in['currency_type']=1;
	    }

	   /* if(!$in['deal_id']){
	      $in['identity_id']= $this->db_users->field("SELECT default_identity_id FROM user_info WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	    }else{
	      $in['identity_id']=0;
	    }*/

	    $customer = $this->db->query(" SELECT c_email, comp_phone, comp_fax, btw_nr FROM customers WHERE customer_id='".$in['buyer_id']."' ");
		$in['buyer_phone'] 			= $customer->f('comp_phone');
		$in['buyer_email'] 			= $customer->f('c_email');
		$in['buyer_fax'] 			= $customer->f('comp_fax');
		$in['buyer_btw_nr']			= addslashes($customer->f('btw_nr'));

		//insert into quote
		$in['quote_id']=$this->db->insert("INSERT INTO tblquote SET
                                      serial_number       		=   '".$serial_number."',
                                      quote_date       			=   '".$in['quote_date']."',
                                      discount      	    	=   '".return_value($in['discount'])."',
                                      discount_line_gen      	=   '".return_value($in['discount_line_gen'])."',
                                      apply_discount            =   '".$in['apply_discount']."',
                                      currency_type       		=   '".$in['currency_type']."',
                                      vat						=   '".return_value($in['vat'])."',
                                      type                		=   '".$in['type']."',
                                      status              		=   '0',
                                      f_archived          		=   '".$f_archived."',
                                      buyer_id       	      	=   '".$in['buyer_id']."',
                                      contact_id				=   '".$in['contact_id']."',
                                      buyer_reference     		=   '".$in['buyer_reference']."',
                                      buyer_name       			=   '".addslashes($buyer_name)."',
                                      buyer_email       		=   '".$in['buyer_email']."',
                                      buyer_phone       		=   '".$in['buyer_phone']."',
                                      buyer_fax       			=   '".$in['buyer_fax']."',
                                      buyer_address       		=   '".addslashes($in['buyer_address'])."',
                                      buyer_zip       			=   '".$in['buyer_zip']."',
                                      buyer_city       			=   '".addslashes($in['buyer_city'])."',
                                      buyer_country_id      	=   '".$in['buyer_country_id']."',
                                      buyer_state_id       		=   '".$in['buyer_state_id']."',
                                      buyer_btw_nr       		=   '".$in['buyer_btw_nr']."',
			                          seller_id             	=   '".$in['seller_id']."',
                                      seller_name       		=   '".$in['seller_name']."',
                                      seller_d_address    	 	=   '".$in['seller_d_address']."',
                                      seller_d_zip       		=   '".$in['seller_d_zip']."',
                                      seller_d_city         	=   '".$in['seller_d_city']."',
                                      seller_d_country_id   	=   '".$in['seller_d_country_id']."',
                                      seller_d_state_id     	=   '".$in['seller_d_state_id']."',
                                      seller_b_address     		=   '".$in['seller_b_address']."',
                                      seller_b_zip       		=   '".$in['seller_b_zip']."',
                                      seller_b_city       		=   '".$in['seller_b_city']."',
                                      seller_b_country_id   	=   '".$in['seller_b_country_id']."',
                                      seller_b_state_id     	=   '".$in['seller_b_state_id']."',
                                      seller_bwt_nr         	=   '".$in['seller_bwt_nr']."',
                                      delivery_address 			=   '".$in['delivery_address']."',
                                      delivery_address_id		=   '".$in['delivery_address_id']."',
                                      use_package             	=   '".ALLOW_QUOTE_PACKING."',
                                      use_sale_unit           	=   '".ALLOW_QUOTE_SALE_UNIT."',
                                      created             		=   '".$created_date."',
                                      created_by          		=   '".$_SESSION['u_id']."',
                                      last_upd            		=   '".$created_date."',
                                      last_upd_by         		=   '".$_SESSION['u_id']."',
                                      author_id					= 	'".$in['author_id']."',
                                      email_language			= 	'".$in['email_language']."',
                                      show_vat					= 	'".$in['show_vat']."',
                                      show_vat_pdf				= 	'".$in['show_vat_pdf']."',
                                      show_grand				= 	'".$in['show_grand']."',
                                      currency_rate         	=   '".$in['currency_rate']."',
                                      validity_date      		=   '".$in['validity_date2']."',
                                      preview 					=	'0',
                                      show_img_q				=   '".$in['show_img_q']."',
                                      acc_manager_id           	=	'".$in['acc_manager_id']."',
                                      acc_manager_name          =	'".$in['acc_manager_name']."',
                                      free_field 				=	'".$in['free_field']."',
                                      identity_id 				=   '".$in['identity_id']."',
                                      show_QC					=	'".$in['show_QC']."',
                                      show_UP					=	'".$in['show_UP']."',
                                      show_V					=	'".$in['show_V']."',
                                      show_PS					=	'".$in['show_PS']."',
                                      show_d					=	'".$in['show_d']."',
                                      main_address_id			= 	'".$in['main_address_id']."',
                                      pdf_layout				=   '".$in['pdf_layout']."',
                                      installation_id 			=   '".$in['installation_id']."',
                                      vat_regime_id 			=	'".$in['vat_regime_id']."',
                                      source_id  				= 	'".$in['source_id']."',
                                      type_id 					=	'".$in['type_id']."',
                                      segment_id 				=	'".$in['segment_id']."',
                                      cat_id 					= 	'".$in['cat_id']."',
                                      yuki_project_id    		=	'".$in['yuki_project_id']."',
                                      show_discount_total		= 	'".$in['show_discount_total']."' ");


		/*if($in['author_id']){
			$this->db->query("UPDATE tblquote SET created_by='".$in['author_id']."' WHERE id='".$in['quote_id']."' ");
		}*/

		/*if($in['duplicate_quote_id']) {
			$this->db->query("UPDATE tblquote SET author_id = '".$_SESSION['u_id']."' ");
		}*/
		//$in['duplicate_quote_id'] = null;
		if($in['general_conditions_new_page']){
			$this->db->query("UPDATE tblquote SET general_conditions_new_page='1' WHERE id='".$in['quote_id']."' ");
		}

		//version
		include_once(__DIR__.'/quote_version.php');
		$version = new quote_version();
		$version->add($in);
		$this->quote_lines($in);

        $lang_note_id = $in['langs'];
		// add multilanguage quote notes in note_fields  DEFAULT_LANGS
		        $lang_note_id = $in['email_language'];
          if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }

          //Add default notes values based on language
          $emailLanguageDefaultNotesData = array('email_language' => $in['email_language'],
          										'default_name_note'	=> 'quote_note',
          										'default_type_note'	=> 'quote_note',
          										'default_name_free_text_content' => 'quote_term',
          										'default_type_free_text_content' => 'quote_terms',
          									);

          $defaultNotesData = get_email_message_default_notes($emailLanguageDefaultNotesData);
          //End Add default notes values based on language
          if(!empty($in['notes'])){
          	$defaultNotesData['notes'] = stripslashes($in['notes']); 
          }
          
          $this->db->insert("INSERT INTO note_fields SET
                            active 			=	'1',
                            item_id         = 	'".$in['quote_id']."',
                            lang_id         =   '".$in['email_language']."',
                            buyer_id 		=	'".$in['buyer_id']."',
                            item_type       =   'quote',
                            item_name       =   'notes',
                            version_id 		= 	'".$in['version_id']."',
                            item_value      =   '".addslashes($defaultNotesData['notes'])."'
        ");
/*            $this->db->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['quote_id']."',
                            lang_id         =   '".$in['email_language']."',
                            item_type         =   'quote',
                            item_name         =   'notes',
                            item_value        =   '".$in['NOTE']."'
            ");*/
/*        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        while($langs->next()){
        	if($lang_note_id == $langs->f('lang_id')){
        		$active_lang_note = 1;
        	}else{
        		$active_lang_note = 0;
        	}
        	$this->db->insert("INSERT INTO note_fields SET
        										item_id 				=	'".$in['quote_id']."',
        										lang_id 				= 	'".$langs->f('lang_id')."',
        										active 					= 	'".$active_lang_note."',
        										item_type 				= 	'quote',
        										item_name 				= 	'notes',
        										version_id 				= 	'".$in['version_id']."',
        										item_value				= 	'".$in['notes_'.$langs->f('lang_id')]."'
        	");
        }*/

        // add multilanguage quote notes in note_fields  CUSTOM LANGS
/*        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
        while($custom_langs->next()) {
        	if($lang_note_id == $custom_langs->f('lang_id')) {
        		$active_lang_note = 1;
        	} else {
        		$active_lang_note = 0;
        	}
        	$this->db->query("INSERT INTO note_fields SET 	item_id 		=	'".$in['quote_id']."',
        													lang_id 		= 	'".$custom_langs->f('lang_id')."',
        													active 			= 	'".$active_lang_note."',
        													item_type 		= 	'quote',
        													item_name 		=   'notes',
        													version_id		= 	'".$in['version_id']."',
        													item_value 		= 	'".$in['notes_.'.$custom_langs->f('lang_id')]."'
        	");
        }*/


        if($in['free_text_content']){

			$this->db->query("INSERT INTO note_fields SET
				active 					=	'1',
				item_value				= 	'".($in['free_text_content'] ? addslashes($in['free_text_content']) : addslashes($defaultNotesData['free_text_content']))."',
				lang_id         		=   '".$in['email_language']."',
				item_type 				= 	'quote',
				buyer_id 				=	'".$in['buyer_id']."',
				item_name 				= 	'free_text_content',
				version_id 				= 	'".$in['version_id']."',
				item_id 				= 	'".$in['quote_id']."' ");
		}
		$in['pdfPreview'] = 'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$in['version_id'].'&lid='.$in['email_language'].'&type='.ACCOUNT_QUOTE_BODY_PDF_FORMAT;
		$in['begin_pdf_link'] = 'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$in['version_id'].'&lid=';
		$in['end_pdf_link'] = '&type='.ACCOUNT_QUOTE_BODY_PDF_FORMAT;
		msg::success (gm("Quote was created"),'success');
		// ark::$controller = $in['controller'];

		$in['add_problem'] = $this->pag;
		insert_message_log($this->pag,'{l}Quote was created by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id'],false,$_SESSION['u_id']);
		$in['pagl'] = $this->pag;

		if($in['duplicate_quote_id']){
			$original_duplicate_serial=$this->db->field("SELECT serial_number FROM tblquote WHERE id='".$in['duplicate_quote_id']."' ");

			//on the original quote
			insert_message_log('quote','{l}This quote was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} <a href="[SITE_URL]quote/view/'.$in['quote_id'].'">{l}Quote{endl} '.$serial_number.'</a>',$this->field_n,$in['duplicate_quote_id']);


			//on the resulting quote
			insert_message_log('quote','{l}This quote was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}from{endl} <a href="[SITE_URL]quote/view/'.$in['duplicate_quote_id'].'">{l}Quote{endl} '.$original_duplicate_serial.'</a>',$this->field_n,$in['quote_id']);

			//insert_message_log($this->pag,'{l}Quote was created by duplicating{endl} '.$original_duplicate_serial,$this->field_n,$in['quote_id'],false,$_SESSION['u_id']);
		}		

		if($in['deal_id']){
			$this->db->query("INSERT INTO tblopportunity_quotes SET opportunity_id = '".$in['deal_id']."', quote_id = '".$in['quote_id']."' ");
			//$source_id = $this->db->field("SELECT source_id FROM tblopportunity WHERE opportunity_id = '".$in['opportunity_id']."' ");
			//$t_id = $this->db->field("SELECT t_id FROM tblopportunity WHERE opportunity_id = '".$in['deal_id']."' ");
			//$this->db->query("UPDATE tblquote SET source_id = '".$source_id."', t_id = '".$t_id."' WHERE id = '".$in['quote_id']."' ");
			$tracking_data=array(
		            'target_id'         => $in['quote_id'],
		            'target_type'       => '2',
		            'target_buyer_id'   => $in['buyer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
		                                    )
		          );
		      addTracking($tracking_data);
		}else{
			$tracking_line=array();
			if($in['duplicate_quote_id']){
				$tracking_line=array(array('origin_id'=>$in['duplicate_quote_id'],'origin_type'=>'2'));
			}
			$tracking_data=array(
		            'target_id'         => $in['quote_id'],
		            'target_type'       => '2',
		            'target_buyer_id'   => $in['buyer_id'],
		            'lines'             => $tracking_line
		          );
		      addTracking($tracking_data);
		}
		$in['duplicate_quote_id'] = null;

		// $in['dropbox'] = new drop('quotes',$in['buyer_id'],$in['quote_id']);
		
		/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
																		AND name	= 'quote-quotes_show_info'	");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
																		AND name	= :name	",['user_id'=>$_SESSION['u_id'],'name'=>'quote-quotes_show_info']);																		
		if(!$show_info->move_next()) {
			/*$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name = 'quote-quotes_show_info',
																value = '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET 	user_id = :user_id,
																name = :name,
																value = :value ",
															['user_id' => $_SESSION['u_id'],
															 'name' => 'quote-quotes_show_info',
															 'value' => '1']
														);																
		} else {
			/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
															   AND name 	 = 'quote-quotes_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
															   AND name 	 = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'quote-quotes_show_info']);															   
		}

		if($in['template']){
			msg::success (gm("Saved"),'success');
			// ark::$controller = 'nquote';
			$in['template_id'] = $in['quote_id'];
			// $in['quote_id'] = null;
		}
		$count = $this->db->field("SELECT COUNT(id) FROM tblquote ");
		if($count == 1){
			doManageLog('Created the first quote.','',array( array("property"=>'first_module_usage',"value"=>'Quote') ));
		}

		//ark::$controller = 'nquote';
		//header("Location:index.php?do=quote-nquote&quote_template_id=".$in['quote_id']."&template=1");
		return true;
	}

	private function quote_lines(&$in){
		
		//INSERT LINES
		// $lines_type = array('6','7');
		$use_package=$this->db->field("SELECT use_package FROM tblquote WHERE id='".$in['quote_id']."'");
		$use_sale_unit=$this->db->field("SELECT use_sale_unit FROM tblquote WHERE id='".$in['quote_id']."'");
		$margin_total=0;
		$quote_total=0;
		$quote_total_articles=0;
		$quote_total_articles_purchase =0;
		$quote_total_vat=0;

		if(is_array($in['quote_group']) && !empty($in['quote_group'])){
			$group_order = 0;
			foreach($in['quote_group'] as $group_code => $group){
				$group_id = $this->db->insert("INSERT INTO tblquote_group SET quote_id = '".$in['quote_id']."',
																				title = '".addslashes($group['title'])."',
																				version_id 	=   '".$in['version_id']."',
																				sort_order = '".$group_order."',
																				show_chapter_total 	=   '".$group['show_chapter_total']."',
																				pagebreak_ch='".$group['pagebreak_ch']."',
																				show_IC = '".$group['show_IC']."' ,
																				show_QC = '".$group['show_QC']."' ,
																				show_UP = '".$group['show_UP']."' ,
																				show_PS = '".$group['show_PS']."' ,
																				show_d = '".$group['show_d']."',
																				show_AC = '".$group['show_AC']."',
																				show_block_total = '".$group['show_block_total']."',
																				show_price_vat='".$group['show_price_vat']."' ");
				$group_order += 1;
				//console::log($group);
				if(is_array($group['table']) || !empty($group['table'])){
					$i = 0;
					foreach($group['table'] as $table_id => $table){
						$sort_order=0;
						if($table['q_table'] == true){
							$last_line_with_article =0;
							$last_combined ='';
							$last_variant_parent =0;
							foreach($table['quote_line'] as $index => $val){
								// console::log()
								if(!$val['is_tax']){$last_line_with_article =0;}
								
								if(empty($val['description']) && ($val['line_type']!='2' && $val['line_type']!='3' ) ){
									continue;
								}
								if(empty($val['description']) &&  $val['line_type']=='3'  ){
									$val['description']= '<p></p>';
								}
								if(ark::$method=='add'){
									if(!ALLOW_QUOTE_PACKING){
										$val['package']=1;
									}
									if(!ALLOW_QUOTE_SALE_UNIT){
										$val['sale_unit']=1;
									}
								}else{ //update
									if(!$use_package){
										$val['package']=1;
									}
									if(!$use_sale_unit){
										$val['sale_unit']=1;
									}
								}
								$line_total = return_value($val['quantity']) * return_value($val['price']) * ($val['package']/ $val['sale_unit']);
								$line_total=$line_total- ($line_total* return_value($val['line_discount'])/100);
								if(!is_numeric($val['line_type'])) {
									$val['line_type']=1;
								}
								$quote_total += $line_total;
				                if($val['article_id']){
				                    $quote_total_articles += $line_total;
				                }
					            $quote_total_vat += ($line_total) * (return_value($val['line_vat']) / 100);
								/*if($val['article_id']){*/
                     				$margin_total+=($line_total)-(return_value($val['purchase_price'])*return_value($val['quantity']));
			                	/*}*/
			                	if(!$val['purchase_price']){
			                		$purchase_price1 = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$val['article_id']."' AND pim_article_prices.base_price='1'");
			                		$val['purchase_price']=$purchase_price1;

			                	}
			                	if($val['article_id']){
				                    $quote_total_articles_purchase +=  return_value($val['quantity']) * return_value($val['purchase_price']) * ($val['package']/ $val['sale_unit']);
				                }
			                	if(!$group['show_AC']){
			                		$hide_AC = '1';
			                	}else{
			                		$hide_AC = '0';
			                	}
			                	$use_combined = $this->db->field("SELECT use_combined FROM pim_articles WHERE article_id='".$val['article_id']."'");
			                	if($val['is_combined'] || (!$use_combined && !$val['component_for'])){
						            $last_combined = '';
						          }
						         if( $val['has_variants'] ){
						            $last_variant_parent = '';
						          } 

								$line_id = $this->db->insert("INSERT INTO tblquote_line SET
								                name       				=   '".htmlspecialchars($val['description'],ENT_COMPAT | ENT_HTML401,"UTF-8")."',
								                quote_id   	    		=   '".$in['quote_id']."',
								                version_id 	    		=   '".$in['version_id']."',
								                content_type        	=   '1',
								                line_type           	=   '".$val['line_type']."',
								                group_id            	=   '".$group_id."',
								                table_id            	=   '".$table['group_table_id']."',
								                article_code        	=   '".addslashes($val['article_code'])."',
	                              				article_id          	=   '".$val['article_id']."',    
	                              				is_tax					=	'".$val['is_tax']."',
	                              				tax_for_line_id			=   '".$last_line_with_article."',
	                              				quantity            	=   '".return_value($val['quantity'])."',
	                              				line_discount       	=   '".return_value($val['line_discount'])."',
								                package             	=   '".return_value2($val['package'])."',
								                sale_unit           	=   '".return_value($val['sale_unit'])."',
								                price               	=   '".return_value($val['price'])."',
								                purchase_price			=	'".return_value($val['purchase_price'])."',
								                amount              	=   '".$line_total."',
								                f_archived          	= 	'0',
								                created             	= 	'".$created_date."',
								                created_by          	= 	'".$_SESSION['u_id']."',
								                last_upd            	= 	'".$created_date."',
								                last_upd_by         	= 	'".$_SESSION['u_id']."'  ,
								                vat						=	'".return_value($val['line_vat'])."',
								                line_order          	=   '".$index."',
							            		sort_order				=	'".$table['sort_order']."',
							            		show_block_total		=	'".$group['show_block_total']."',
							            		show_price_vat			= 	'".$group['show_price_vat']."',
							            		show_IC 				= 	'".$group['show_IC']."',
							            		hide_AC					=	'".$hide_AC."',
							            		hide_QC					=	'".($in['show_QC'] == '1' ? '0': '1')."',
							            		hide_UP 				= 	'".($in['show_UP'] == '1' ? '0': '1')."',
							            		hide_DISC 				= 	'".($in['show_d'] == '1' ? '0': '1')."',
							            		show_block_vat			=	'".$val['show_lVATv']."',
							            		service_bloc_title		=	'".addslashes($table['item_text'])."',
							            		has_variants  			= '".$val['has_variants']."',
							            		has_variants_done		='".$val['has_variants_done']."',
							            		is_variant_for 			='".$val['is_variant_for']."',
							            		is_variant_for_line 	='".$last_variant_parent."',
							            		variant_type 			='".$val['variant_type']."',
							            		is_combined 			='".$val['is_combined']."',
							            		component_for 			='".$last_combined."',
							            		facq 					='".$val['facq']."',
							            		visible 				='".$val['visible']."'
								");
								$i += 1;
								$sort_order++;
								if(!$val['is_tax']){$last_line_with_article =$line_id;}
								if($val['is_combined']){
						            $last_combined = $line_id;
						          }
						        if($val['has_variants'] && $val['has_variants_done']){
						            $last_variant_parent = $line_id;
						          }
						        if($val['is_variant_for']){
						            $last_variant_parent = '';
						          }
						         
							}
						}
						elseif($table['q_content'] == true){
							$content = str_replace('?','&#63;', $table['quote_content']);
							$this->db->query("INSERT INTO tblquote_line SET quote_id   	    	=   '".$in['quote_id']."',
																			version_id 	    	=   '".$in['version_id']."',
																			content_type        =   '2',
																			group_id            =   '".$group_id."',
																			table_id            =   '".$table['group_content_id']."',
																			content             =   '".addslashes($content)."',
																			f_archived          = 	'0',
																			created             = 	'".$created_date."',
																			created_by          = 	'".$_SESSION['u_id']."',
																			last_upd            = 	'".$created_date."',
																			last_upd_by         = 	'".$_SESSION['u_id']."',
                                                							sort_order			=	'".$table['sort_order']."'");
						}
						elseif($table['pagebreak'] == true){
							$this->db->query("INSERT INTO tblquote_line SET quote_id   	    =   '".$in['quote_id']."',
																		 version_id 	    =   '".$in['version_id']."',
																		content_type        =   '3',
																		group_id            =   '".$group_id."',
																		table_id            =   '".$table['group_break_id']."',
																		content             =   '<br pagebreak=\"true\"/>',
																		f_archived          = 	'0',
																		created             = 	'".$created_date."',
																		created_by          = 	'".$_SESSION['u_id']."',
																		last_upd            = 	'".$created_date."',
																		last_upd_by         = 	'".$_SESSION['u_id']."',
                                                						sort_order			=	'".$table['sort_order']."'");
						}
					}
				}
			}
		}
		    $quote_total=round($quote_total,2);
		    $quote_total_vat=round($quote_total_vat,2);
		    $quote_total_vat += $quote_total;
		    $margin_total=$margin_total;

		    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				if($quote_total_articles_purchase){
					$margin_percent_total = $margin_total/$quote_total_articles_purchase *100;
				}elseif($margin_total){
					$margin_percent_total = 100;
				}
			}else{
				if($quote_total_articles){
					$margin_percent_total = $margin_total/$quote_total_articles *100;
				}elseif($margin_total){
					$margin_percent_total = 100;
				}
			}

		    //$margin_percent_total=($margin_total/$quote_total_articles)*100;

		    if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
		      if($in['currency_rate']){
		        $quote_total = $quote_total*return_value($in['currency_rate']);
		        $quote_total_articles = $quote_total_articles*return_value($in['currency_rate']);
		        $quote_total_vat = $quote_total_vat*return_value($in['currency_rate']);

		      }
		    }

		    $this->db->query("UPDATE tblquote SET margin='".$margin_total."', margin_percent='".$margin_percent_total."',amount='".$quote_total."', amount_vat='".$quote_total_vat."' WHERE id='".$in['quote_id']."'");
	}

	/**
	 * add quote validate function
	 *
	 * @return void
	 * @author PM
	 **/
	function add_validate(&$in)
	{
		// $in['quote_date'] = strtotime($in['quote_ts']);
		$is_ok=true;

		$v = new validation($in);

		//$v->field('serial_number','Quote Nr','required','Quote Nr. is required');
		$v->field('quote_date','Date','required','Date is required');

		$is_ok = $v->run();

		if($in['main_quote_id']){
			$filter .=" AND tblquote_version.main_quote_id!= '".$in['main_quote_id']."' ";
		}
		if(ark::$method == 'update') {
			$filter = " AND tblquote.id !='".$in['quote_id']."' AND f_archived='0' ";
			$this->db->query("SELECT tblquote.id FROM tblquote INNER JOIN tblquote_version ON tblquote.id=tblquote_version.quote_id WHERE tblquote.serial_number='".$in['serial_number']."' and tblquote.serial_number!='' $filter");
			if($this->db->move_next()){
				msg::error(gm("Quote Nr. already used"),'error');
				$is_ok=false;
			}
		}
		/*$this->db->query("SELECT tblquote.id FROM tblquote INNER JOIN tblquote_version ON tblquote.id=tblquote_version.quote_id WHERE tblquote.serial_number='".$in['serial_number']."' and tblquote.serial_number!='' $filter");
		if($this->db->move_next()){
			msg::error(gm("Quote Nr. already used"),'error');
			if(ark::$method == 'add'){
				$in['serial_number'] = generate_quote_number(DATABASE_NAME);
			}else{
				$is_ok=false;
			}
		}*/

		return $is_ok;
	}

	/**
	 * update quote validate function
	 *
	 * @return void
	 * @author PM
	 **/
	function update(&$in)
	{

		$initial_quote = $this->get($in['quote_id']);
		if($in['template']){
			if(!$in['template_name']){
				msg::error (gm("Template name required"),'template_name');
				json_out($in);
			}
			$f_archived='2';
			$in['serial_number'] = $in['template_name'];
			$version_id = $this->db->field("SELECT version_id FROM tblquote_version WHERE quote_id = '".$in['quote_id']."'");
			$in['version_id'] = $version_id;
		}else{
			if(!$this->update_validate($in))
			{
				return false;
			}
		}
		$created_date= date(ACCOUNT_DATE_FORMAT.' H:i:s');
		$updatedd_date= date(ACCOUNT_DATE_FORMAT.' H:i:s');

		//update quote

		if($in['buyer_id']){
			$customer = $this->db->query(" SELECT customers.name AS company_name,customers.c_email, customers.comp_phone, customers.comp_fax, customers.btw_nr,customer_legal_type.name as l_name FROM customers
				LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id 
				WHERE customers.customer_id='".$in['buyer_id']."' ");
			$in['buyer_name']=addslashes(stripslashes($customer->f('company_name').' '.$customer->f('l_name')));
			$in['buyer_phone'] 			= $customer->f('comp_phone');
			$in['buyer_email'] 			= $customer->f('c_email');
			$in['buyer_fax'] 			= $customer->f('comp_fax');
			$in['buyer_btw_nr']			= addslashes($customer->f('btw_nr'));
		} else {
			$contact_name = $this->db->query("SELECT firstname, lastname FROM  customer_contacts WHERE contact_id = '".$in['contact_id']."' ");
			$contact_name->next();
			$in['buyer_name'] = addslashes($contact_name->f('firstname').' '.$contact_name->f('lastname'));
		}

		$can_do = false;
		$buyer_id_old = $this->db->field("SELECT buyer_id FROM tblquote WHERE id = '".$in['quote_id']."' ");
		if(($in['buyer_id'] != $buyer_id_old || $in['customer_changed'])) {
			$can_do = true;
		}
		$contact_id_old = $this->db->field("SELECT contact_id FROM tblquote WHERE id = '".$in['quote_id']."' ");
		$can_do_contact = false;
		if($in['contact_id'] != $contact_id_old || $in['contact_changed']) {
			$can_do_contact = true;
		}
		if( $in['main_address_id']){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$in['free_field'] = addslashes($buyer_addr->f('address'))."\n".addslashes($buyer_addr->f('zip')).'  '.addslashes($buyer_addr->f('city'))."\n".addslashes(get_country_name($buyer_addr->f('country_id')));
			$in['buyer_address']=$buyer_addr->f('address');
			$in['buyer_zip']=$buyer_addr->f('zip');
			$in['buyer_city']=$buyer_addr->f('city');
			$in['buyer_country_id']=$buyer_addr->f('country_id');
		}
		if( $in['delivery_address_id']){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
			$in['delivery_address'] = addslashes($buyer_addr->f('address'))."\n".addslashes($buyer_addr->f('zip')).'  '.addslashes($buyer_addr->f('city'))."\n".addslashes(get_country_name($buyer_addr->f('country_id')));
		}
		$in['validity_date2'] = strtotime($in['validity_date']);
		$in['quote_date2'] = strtotime($in['quote_date']);

		//Set email language as account / contact language
		$emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
	    						  'contact_id' 		=> $in['contact_id'],
	    						  'item_id'			=> $in['quote_id'],
	    						  'email_language' 	=> $in['email_language'],
	    						  'table'			=> 'tblquote',
	    						  'table_label'		=> 'id',
	    						  'table_buyer_label' => 'buyer_id',
	    						  'table_contact_label' => 'contact_id',
	    						  'param'		 	=> 'edit');
		$in['email_language'] = get_email_language($emailMessageData);
	    //End Set email language as account / contact language

		$default_email_language = $this->db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
	 	if($in['email_language']==0){
	 		$in['email_language'] = $default_email_language;
	 	}

		$this->db->query("UPDATE tblquote SET
                                                serial_number       				=   '".$in['serial_number']."',
                                                quote_date       					=   '".$in['quote_date2']."',
                                                discount      	    				=   '".return_value($in['discount'])."',
                                                discount_line_gen      	    		=   '".return_value($in['discount_line_gen'])."',
                                                apply_discount        		  		=   '".$in['apply_discount']."',
                                                currency_type      				 	=   '".$in['currency_type']."',
                                                vat									=   '".return_value($in['vat'])."',
                                                type             			 	  	=   '".$in['type']."',
                                                buyer_id       	  		    		=   '".$in['buyer_id']."',
                                                contact_id							=   '".$in['contact_id']."',
                                                buyer_reference   			  		=   '".$in['buyer_reference']."',
                                                buyer_name       					=   '".$in['buyer_name']."',
                                                buyer_email       		=   '".$in['buyer_email']."',
                                      			buyer_phone       		=   '".$in['buyer_phone']."',
                                      			buyer_fax       			=   '".$in['buyer_fax']."',
                                                buyer_address     			  		=   '".addslashes($in['buyer_address'])."',
                                                buyer_zip       					=   '".$in['buyer_zip']."',
                                                buyer_city       					=   '".addslashes($in['buyer_city'])."',
                                                buyer_country_id    		  		=   '".$in['buyer_country_id']."',
                                                buyer_state_id     			  		=   '".$in['buyer_state_id']."',
                                                buyer_btw_nr     			  		=   '".$in['buyer_btw_nr']."',
			                        			seller_id          					=   '".$in['seller_id']."',
                                                seller_name       					=   '".$in['seller_name']."',
                                                seller_d_address    		  		=   '".$in['seller_d_address']."',
                                                seller_d_zip       					=   '".$in['seller_d_zip']."',
                                                seller_d_city       				=   '".$in['seller_d_city']."',
                                                seller_d_country_id  			 	=   '".$in['seller_d_country_id']."',
                                                seller_d_state_id    		 		=   '".$in['seller_d_state_id']."',
                                                seller_b_address      				=   '".$in['seller_b_address']."',
                                                seller_b_zip       					=   '".$in['seller_b_zip']."',
                                                seller_b_city      				 	=   '".$in['seller_b_city']."',
                                                seller_b_country_id   				=   '".$in['seller_b_country_id']."',
                                                seller_b_state_id     				=   '".$in['seller_b_state_id']."',
                                                seller_bwt_nr       	 	 		=   '".$in['seller_bwt_nr']."',
                                                delivery_address 					=   '".$in['delivery_address']."',
                                                delivery_address_id					=   '".$in['delivery_address_id']."',
                                                last_upd          			  		=   '".$updatedd_date."',
                                                author_id							= 	'".$in['author_id']."',
                                                show_vat							= 	'".$in['show_vat']."',
                                                show_vat_pdf						= 	'".$in['show_vat_pdf']."',
                                                show_grand							= 	'".$in['show_grand']."',
                                                last_upd_by   			      		=   '".$_SESSION['u_id']."',
                                                general_conditions_new_page			=	'0',
                                                email_language						= 	'".$in['email_language']."',
                                                free_field							=	'".$in['free_field']."',
                                                validity_date      					=   '".$in['validity_date2']."',
                                                show_img_q							=   '".$in['show_img_q']."',
                                                acc_manager_id           			=	'".$in['acc_manager_id']."',
                                                acc_manager_name           			=	'".$in['acc_manager_name']."',
                                                show_QC								=	'".$in['show_QC']."',
                                                show_UP								=	'".$in['show_UP']."',
                                                show_V								=	'".$in['show_V']."',
                                                show_PS								=	'".$in['show_PS']."',
                                                show_d								=	'".$in['show_d']."',
                                                main_address_id						=	'".$in['main_address_id']."',
                                                identity_id							= 	'".$in['identity_id']."',
                                                downpayment_val						=	'".return_value($in['quote_downpayment'])."',
                                      			installation_id 					=	'".$in['installation_id']."',
                                      			vat_regime_id  						=	'".$in['vat_regime_id']."',
                                      			source_id  							= 	'".$in['source_id']."',
                                      			type_id 							=	'".$in['type_id']."',
                                     			segment_id 							=	'".$in['segment_id']."',
                                      			cat_id 								=	'".$in['cat_id']."',
                                      			yuki_project_id    					=	'".$in['yuki_project_id']."',
                                      			show_discount_total					= 	'".$in['show_discount_total']."'
                           WHERE id = '".$in['quote_id']."' ");

		// source_id 				=	'".$in['source_id']."',
		// t_id 					=	'".$in['t_id']."',
		// stage_id         		=   '".$in['stage_id']."',
		// notes                 	=   '".utf8_encode($in['notes'])."',

		/*if($in['author_id']){
			$this->db->query("UPDATE tblquote SET created_by='".$in['author_id']."' WHERE id='".$in['quote_id']."' ");
		}*/


		if($in['general_conditions_new_page']){
			$this->db->query("UPDATE tblquote SET general_conditions_new_page='1' WHERE id='".$in['quote_id']."' ");
		}
		if($in['buyer_id']) {
			if($can_do == true ) {
				$address = $this->db->query("SELECT city, zip, address, country_id FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' AND is_primary=1 ");
				if($address->next()) {
					$address_field = $address->f('address')."\n".$address->f('zip').' '.$address->f('city')."\n".get_country_name($address->f('country_id'));
					$this->db->query("UPDATE tblquote SET free_field = '".addslashes($address_field)."' WHERE id = '".$in['quote_id']."' ");
					$this->db->query("UPDATE tblquote SET 	buyer_address 		= '".addslashes($address->f('address'))."',
															buyer_zip    	 	= '".$address->f('zip')."',
															buyer_city    		= '".addslashes($address->f('city'))."',
															buyer_country_id 	= '".$address->f('country_id')."' WHERE id = '".$in['quote_id']."' ");
				}


			}
		}

		if($in['contact_id']) {
			if($can_do_contact == true) {
				$this->db->query("UPDATE tblquote SET contact_id = '".$in['contact_id']."' where id = '".$in['quote_id']."' ");
				if(!$in['buyer_id']) {
					$contact_address = $this->db->query("SELECT city, zip, address, country_id FROM customer_contact_address WHERE contact_id = '".$in['contact_id']."' ");
					if($contact_address->move_next()) {
						$address_field_contact = $contact_address->f('address')."\n".$contact_address->f('zip').' '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
						$this->db->query("UPDATE tblquote SET free_field = '".addslashes($address_field_contact)."' WHERE id = '".$in['quote_id']."' ");
						$this->db->query("UPDATE tblquote SET 	buyer_address 		= '".addslashes($contact_address->f('address'))."',
															buyer_zip    	 	= '".$contact_address->f('zip')."',
															buyer_city    		= '".addslashes($contact_address->f('city'))."',
															buyer_country_id 	= '".$contact_address->f('country_id')."' WHERE id = '".$in['quote_id']."' ");
					}

				}
			}

		}

		// update multilanguage quote note in note_fields
		$lang_note_id = $in['email_language'];

		//if we select a custom_lang we update email_language from tblquote.

		// $this->db->query("UPDATE tblquote SET email_language = '".$in['langs']."' WHERE id = '".$in['quote_id']."' ");

		#default langs
        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        // add multilanguage quote notes in note_fields  DEFAULT_LANGS
		        $lang_note_id = $in['email_language'];
        if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
        }else{
            $active_lang_note = 0;
        }
        
        $exist_quote_note = $this->db->field("SELECT buyer_id FROM note_fields WHERE item_id=".$in['quote_id']." AND item_name = 'notes' AND version_id = '".$in['version_id']."'");

        if($exist_quote_note==$in['buyer_id']){
        	$this->db->query("DELETE FROM note_fields WHERE item_id = '".$in['quote_id']."' AND item_type = 'quote' AND item_name = 'notes' AND version_id = '".$in['version_id']."' ");//AND lang_id = '".$in['email_language']."' 
        	$this->db->insert("INSERT INTO note_fields SET
                            active 			=	'1',
                            item_id         = 	'".$in['quote_id']."',
                            lang_id         =   '".$in['email_language']."',
                            buyer_id 		=	'".$in['buyer_id']."',
                            item_type       =   'quote',
                            item_name       =   'notes',
                            version_id 		= 	'".$in['version_id']."',
                            item_value      =   '".addslashes($in['notes'])."'
        	");
        }else{
        	//Buyer was changed 
        	$this->db->query("DELETE FROM note_fields WHERE item_id = '".$in['quote_id']."' AND item_type = 'quote' AND item_name = 'notes' AND version_id = '".$in['version_id']."' ");

        	$this->db->insert("INSERT INTO note_fields SET
                            active 			=	'1',
                            item_id         = 	'".$in['quote_id']."',
                            lang_id         =   '".$in['email_language']."',
                            buyer_id 		=	'".$in['buyer_id']."',
                            item_type       =   'quote',
                            item_name       =   'notes',
                            version_id 		= 	'".$in['version_id']."',
                            item_value      =   '".addslashes($in['notes'])."'
        	");
        }


       /* while($langs->next()){
        	foreach ($in['translate_loop'] as $key => $value) {
	    		if($value['lang_id'] == $langs->f('lang_id')){
	    			$in['nota'] = $value['notes'];
	    			break;
	    		}
	    	}
        	if($lang_note_id == $langs->f('lang_id')){
        		$active_lang_note = 1;
        	}else{
        		$active_lang_note = 0;
        	}
        	$this->db->query("UPDATE note_fields SET
        										active 					= 	'".$active_lang_note."',
        										item_value				= 	'".$in['nota']."'
        								WHERE 	item_type 				= 	'quote'
        								AND		item_name 				= 	'notes'
        								AND 	version_id 				= 	'".$in['version_id']."'
        								AND		lang_id 				= 	'".$langs->f('lang_id')."'
        								AND		item_id 				= 	'".$in['quote_id']."'

        	");
        }*/
        #custom langs
/*        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
        while($custom_langs->next()) {
        	foreach ($in['translate_loop_custom'] as $key => $value) {
	    		if($value['lang_id'] == $custom_langs->f('lang_id')){
	    			$in['nota'] = $value['notes'];
	    			break;
	    		}
	    	}
        	if($lang_note_id == $custom_langs->f('lang_id')) {
        		$active_lang_note_custom = 1;
        	} else {
        		$active_lang_note_custom = 0;
        	}
        	$this->db->query("UPDATE note_fields SET 	active 		=  	'".$active_lang_note_custom."',
        												item_value 	=  	'".$in['nota']."'
        										 WHERE  item_type 	=  	'quote'
        										 AND 	item_name 	=  	'notes'
        										 AND 	version_id	= 	'".$in['version_id']."'
        										 AND 	lang_id 	= 	'".$custom_langs->f('lang_id')."'
        										 AND 	item_id 	= 	'".$in['quote_id']."'
        	");
        }
*/
        $exist_quote_content = $this->db->field("SELECT buyer_id FROM note_fields WHERE item_id=".$in['quote_id']." AND	item_name 	= 'free_text_content'
						  	AND 	version_id 	= '".$in['version_id']."' ORDER BY `note_fields_id` DESC");
        if($exist_quote_content==$in['buyer_id']){
        	$this->db->query("DELETE FROM note_fields
						  	WHERE 	item_type 	= 'quote'
						  	AND 	item_name 	= 'free_text_content'
						  	AND 	version_id 	= '".$in['version_id']."'
						  	AND 	item_id 	= '".$in['quote_id']."' ");

			$this->db->query("INSERT INTO note_fields SET
				active 					=	'1',
				item_value				= 	'".addslashes($in['free_text_content'])."',
				item_type 				= 	'quote',
				lang_id         		=   '".$in['email_language']."',
				buyer_id 		=	'".$in['buyer_id']."',
				item_name 				= 	'free_text_content',
				version_id 				= 	'".$in['version_id']."',
				item_id 				= 	'".$in['quote_id']."' ");
        }else{
        	$this->db->query("INSERT INTO note_fields SET
				active 					=	'1',
				item_value				= 	'".addslashes($in['free_text_content'])."',
				item_type 				= 	'quote',
				lang_id         		=   '".$in['email_language']."',
				buyer_id 		=	'".$in['buyer_id']."',
				item_name 				= 	'free_text_content',
				version_id 				= 	'".$in['version_id']."',
				item_id 				= 	'".$in['quote_id']."' ");
        }


		//INSERT LINES
		$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
		$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
		$this->db->query("UPDATE tblquote_version SET version_date='".$in['quote_date2']."' ,
																									discount      	    				=   '".return_value($in['discount'])."',
                                                	discount_line_gen      	    =   '".return_value($in['discount_line_gen'])."',
                                                	apply_discount        		  =   '".$in['apply_discount']."',
                                                	version_subject='".$in['subject']."',version_own_reference='".$in['own_reference']."'
											WHERE version_id='".$in['version_id']."' ");
		$this->quote_lines($in);
		
		if($in['success_msg']){
			// msg::$success = gm($in['success_msg']);
			msg::success ($in['success_msg'],'success');
		}else{
			msg::success (gm("Quote has been updated"),'success');	
		}

		$trace_id=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".$in['quote_id']."' ");
		if($in['deal_id']){
			//$trace_id=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".$in['quote_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' ");	
				}else{
					$this->db->query("INSERT INTO tracking_line SET
							origin_id='".$in['deal_id']."',
							origin_type='11',
							trace_id='".$trace_id."' ");
				}
			}else{
				$tracking_data=array(
			            'target_id'         => $in['quote_id'],
			            'target_type'       => '2',
			            'target_buyer_id'   => $in['buyer_id'],
			            'lines'             => array(
			                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
			                                    )
			          );
			      addTracking($tracking_data);
			}
		}else{
			//$trace_id=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".$in['quote_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("DELETE FROM tracking_line WHERE line_id='".$linked_doc."' ");	
				}
			}
		}
		if($trace_id){
			$this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
		}

		// ark::$controller=$in['controller'];
		$tmpstp = $this->db->field("SELECT tmpstp FROM tblquote WHERE id='".$in['quote_id']."' ");
		if($in['tmpstp'] != $tmpstp){
			insert_message_log($this->pag,'{l}Quote has been updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id']);
			$this->db->query("UPDATE tblquote SET tmpstp='".$in['tmpstp']."' WHERE id='".$in['quote_id']."' ");
		}
		$in['pagl'] = $this->pag;
		if($in['template']){
			$in['quote_template_id'] = $in['quote_id'];
			$in['template_id']=$in['quote_id'];
			//$in['quote_id']='';
			// ark::$controller = 'nquote';
		}
		$in['add_problem'] = 'quote';
		$updated_quote = $this->get($in['quote_id']);
		// console::log($updated_quote, $initial_quote);
		if(!$this->check_diff($updated_quote, $initial_quote)){
			global $config;
			$this->db->query("UPDATE tblquote_version SET preview='0' WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."' ");
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_".$in['quote_id']."_".$in['version_id']."_%' ");
		}
		return true;
	}

	/**
	 * update quote validate function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('quote_id', 'ID', 'required:exist[tblquote.id]', "Invalid Id");
		$v->field('serial_number','Quote Nr','required','Quote Nr. is required');
		$is_ok = $v->run();

		if (!$this->add_validate($in))
		{
			$is_ok = false;
		}
		return $is_ok;
	}

	/**
	 * get quote info function
	 *
	 * @return void
	 * @author PM
	 **/
	function get($id){
		if(!$id){
			return;
		}
		# main tables
		$q = $this->db->query("SELECT * FROM tblquote WHERE id='".$id."'")->getAll();
		$quote = $this->parse_results($q[0]);

		$qgroup = $this->db->query("SELECT * FROM tblquote_group WHERE quote_id='".$id."' order by group_id asc ")->getAll();
		$groups = $this->parse_results($qgroup,'version_id',true);

		$qhistory = $this->db->query("SELECT *  FROM  `tblquote_history` WHERE quote_id='".$id."' ")->getAll();
		$history = $this->parse_results($qhistory);

		$qlines = $this->db->query("SELECT * FROM  `tblquote_line` WHERE quote_id='".$id."' order by id asc ")->getAll();
		$lines = $this->parse_results($qlines,'version_id',true);

		$qversion = $this->db->query("SELECT * FROM  `tblquote_version` WHERE quote_id='".$id."' order by version_id asc ")->getAll();
		$version = $this->parse_results($qversion,'version_id');

		$notes = $this->db->query("SELECT * FROM `note_fields` WHERE `item_type`='quote' AND `item_id`='".$id."' order by note_fields_id asc ")->getAll();
		$notes = $this->parse_results($notes,'version_id',true);

		# adjacent tables
		$tblquote_lost_reason = $this->db->query("SELECT * FROM  `tblquote_lost_reason` ")->getAll();
		$tblquote_lost_reason = $this->parse_results($tblquote_lost_reason,'id');

		$tblquote_source = $this->db->query("SELECT * FROM  `tblquote_source` ")->getAll();
		$tblquote_source = $this->parse_results($tblquote_source,'id');

		$tblquote_type = $this->db->query("SELECT * FROM  `tblquote_type` ")->getAll();
		$tblquote_type = $this->parse_results($tblquote_type,'id');

		$result = array('tblquote'=>$quote,
										'tblquote_version'=>$version,
										'tblquote_line'=>$lines,
										'tblquote_group'=>$groups,
										'note_fields'=>$notes,
										// 'tblquote_history'=>$history
										);

		// console::log($notes);
		return $result;
	}

	/**
	 * parse initial quote function
	 *
	 * @return void
	 * @author PM
	 **/
	function parse_results($array,$primary_key = '',$group_by_primary = false){
		$ar = array();
		foreach ($array as $key => $value) {
			if(is_array($value)){
				$arr = array();
				$p_k = '';
				foreach ($value as $k => $val) {
					if(is_numeric($k)){
						continue;
					}
					if($k == $primary_key){
						$p_k = $val;
					}
					$arr[$k] = $val;
				}
				if($primary_key && $p_k && !$group_by_primary){
					$ar[$p_k] = $arr;
				}elseif($primary_key && $p_k && $group_by_primary){
					if(!is_array($ar[$p_k])){
						$ar[$p_k] = array();
					}
					array_push($ar[$p_k], $arr);
				}else{
					array_push($ar, $arr);
				}
			}else{
				if(is_numeric($key)){
					continue;
				}
				$ar[$key] = $value;
			}
		}
		return $ar;
	}

	/**
	 * compare initial quote with new function
	 *
	 * @return void
	 * @author PM
	 **/
	function check_diff($arr1,$arr2){
		/*
			what is $igonre for?
			ex: when we update a quote we delete all the lines and add new lines with new ids
			that is why we need to ignore ids fields changes.
		*/
		$is_ok = true;
		$ignore = array('last_upd','id','group_id','preview','note_fields_id');
		foreach ($arr1 as $key => $value) {
			if(is_array($value)){
				if(count($value) != count($arr2[$key])){
					$is_ok = false;
					break;
				}
				if(!$this->check_diff($value,$arr2[$key])){
					$is_ok = false;
					break;
				}
			}else{
				if(in_array($key, $ignore)){
					continue;
				}
				if(trim($value) != trim($arr2[$key])){
					$is_ok = false;
					break;
				}
			}
		}
		return $is_ok;
	}

	# servicing and support
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function uploadify(&$in)
	{
		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME;
			$in['name'] = 'q_logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

				move_uploaded_file($tempFile,$targetFile);
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);
				$db_upload = new sqldb($database_2);
				$logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_LOGO' ");
				if($logo == ''){
					if(defined('ACCOUNT_QUOTE_LOGO')){
						$db_upload->query("UPDATE settings SET
		                           value = 'upload/".DATABASE_NAME."/".$in['name']."'
		                           WHERE constant_name='ACCOUNT_QUOTE_LOGO' ");
					}else{
						$db_upload->query("INSERT INTO settings SET
		                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
		                           constant_name='ACCOUNT_QUOTE_LOGO' ");
					}
				}
				$response['success'] = 'success';
        echo json_encode($response);
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
        echo json_encode($response);
				// echo gm('Invalid file type.');
			}
		}
	}
	function atach(&$in)
	{

		$response=array();
		if (!empty($_FILES)) {
			$fileImages = array('jpg','jpeg','gif'); // File extensions
			$size = $_FILES['Filedata']['size'];
			if(!defined('MAX_IMAGE_SIZE') && $size>512000 && in_array(strtolower($fileParts['extension']),$fileImages)){
                  $response['error'] = gm('Size exceeds 500kb');
                  $response['filename'] = $_FILES['Filedata']['name'];
                echo json_encode($response);
                exit();
            }
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
/*			$targetPath = $in['path'];
*/			$in['name'] = $_FILES['Filedata']['name'];
			$targetPath = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/quotes';

			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','pdf','docx'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				 if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE && in_array(strtolower($fileParts['extension']),$fileImages)){
                    $image = new SimpleImage();
                    $image->load($_FILES['Filedata']['tmp_name']);
                    $image->scale(MAX_IMAGE_SIZE,$size);
                    $image->save($targetFile);
                }else{
                    move_uploaded_file($tempFile,$targetFile);             
                }
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);
				$db_upload = new sqldb($database_2);
				$file_id = $db_upload->insert("INSERT INTO attached_files SET `path` = 'upload/".DATABASE_NAME."/quotes/', name = '".$in['name']."', type='1' ");
				$response['success'] = 'success';
				$response['filename'] = $_FILES['Filedata']['name'];
        echo json_encode($response);
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
				$response['filename'] = $_FILES['Filedata']['name'];
        echo json_encode($response);
				// echo gm('Invalid file type.');
			}
		}
	}
			
				
	function delete_logo(&$in)
	{
	    if($in['name'] == ACCOUNT_LOGO_QUOTE){
	      msg::error ( gm('You cannot delete the default logo'),'error');
	      json_out($in);
	      return true;
	    }
	    if($in['name'] == '../img/no-logo.png'){
	      msg::error ( gm('You cannot delete the default logo'),'error');
	      json_out($in);
	      return true;
	    }
	    $exists = $this->db->field("SELECT COUNT(service_id) FROM servicing_support WHERE pdf_logo='".$in['name']."' ");
	    if($exists>=1){
	      msg::error ( gm('This logo is set for one ore more invoices.'),'error');
	      json_out($in);
	      return false;
	    }
	    @unlink($in['name']);
	    msg::success ( gm('File deleted'),'success');
	    return true;
	}

	function set_default_logo(&$in)
	{
	    $this->db->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_LOGO_QUOTE' ");
	    msg::success (gm("Default logo set."),'success');
			json_out($in);
	    return true;
	}

	function settings(&$in){

		$this->db->query("UPDATE settings SET value='".(int)$in['use_page_numbering']."' WHERE constant_name='USE_QUOTE_PAGE_NUMBERING' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['show_stamp']."' WHERE constant_name='SHOW_QUOTE_STAMP_SIGNATURE' ");

		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	function default_pdf_format(&$in)
	{

		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_header(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_HEADER_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_body(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_BODY_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_footer(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_FOOTER_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}

	function set_custom_pdf(&$in)
	{
		if(!$in['custom_type']){
			msg::$error = gm('Invalid ID');
			return false;
		}
		$this->db->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$this->db->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$this->db->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_QUOTE_PDF_FORMAT'");
		$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
		$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

		global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}

	function PdfAcitve(&$in){
		
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_active']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_PDF' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_note']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_NOTE' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_condition']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_CONDITIONS' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_stamp']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_SIGNATURE' ");
		//$this->db->query("UPDATE settings SET value='".(int)$in['show_stamp']."' WHERE constant_name='SHOW_QUOTE_STAMP_SIGNATURE' ");

		msg::success ( gm('Changes saved'),'success');
		/*json_out($in);*/
		return true;
	}
	function PageActive(&$in){
		$val = $this->db->field("SELECT constant_name FROM settings WHERE constant_name='QUOTE_GENERAL_CONDITION_PAGE'");
		if($val){
			$this->db->query("UPDATE settings SET value='".(int)$in['page']."' WHERE constant_name='QUOTE_GENERAL_CONDITION_PAGE' ");
		}else{
			$this->db->query("INSERT INTO `settings` (`constant_name` ,`value` ,`long_value` ,`module` ,`type`) VALUES ('QUOTE_GENERAL_CONDITION_PAGE',  '".(int)$in['page']."', NULL ,  'quote',  '1') ");
		}

		
		msg::success ( gm('Changes saved'),'success');
		return true;
	}

	function reset_data(&$in){
		if($in['header'] && $in['header']!='undefined'){
			$variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
		}elseif($in['footer'] && $in['footer']!='undefined'){
			$variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
		}
		$result=array("var_data" => $variable_data);

		json_out($result);
		return true;
	}

	function pdfSaveData(&$in){
         
         $in['variable_data']=str_replace_last('<p>&nbsp;</p>','',$in['variable_data']);

		if($in['header']=='header'){
			$this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='quote' AND type='".$in['header']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
			$exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
			if($exist){
				$this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
			}else{
				$this->db->query("INSERT INTO tblquote_pdf_data SET master='quote',
													type='".$in['header']."',
													content='".$in['variable_data']."',
													initial='1', 
													layout='".$in['layout']."',
													`default`='1',
													identity_id='".$in['identity_id']."' ");
			}
		}else{
			$this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='quote' AND type='".$in['footer']."' AND initial='1' AND identity_id='".$in['identity_id']."' ");
			$exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

			if($exist){
				$this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
			}else{
				$this->db->query("INSERT INTO tblquote_pdf_data SET master='quote',
													type='".$in['footer']."',
													content='".$in['variable_data']."',
													initial='1', 
													layout='".$in['layout']."',
													`default`='1',
													identity_id='".$in['identity_id']."' ");
			}
		}
		msg::success ( gm('Data saved'),'success');
		return true;
	}

	function Convention(&$in){
			if($in['account_number']){
				$this->db->query("UPDATE settings SET value='".$in['account_number']."' where constant_name='ACCOUNT_QUOTE_START'");
			}
			if($in['account_digits']){
				if(!defined('ACCOUNT_QUOTE_DIGIT_NR')){
					$this->db->query("INSERT INTO settings SET value='".$in['account_digits']."', constant_name='ACCOUNT_QUOTE_DIGIT_NR', module='billing' ");
					// msg::$success = gm("Setting added.");
				}else{
					$this->db->query("UPDATE settings SET value='".$in['account_digits']."' WHERE constant_name='ACCOUNT_QUOTE_DIGIT_NR'");
					// msg::$success = gm("Settings updated").'.';
				}
			}
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_QUOTE_REF' ");
			if((int)$in['reference']){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_QUOTE_REF' ");
			}
			$this->db->query("UPDATE settings SET value='".$in['del']."' WHERE constant_name='ACCOUNT_QUOTE_DEL' ");
		  $ACCOUNT_QUOTE_WEIGHT=$this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_WEIGHT' ");
	      if(is_null($ACCOUNT_QUOTE_WEIGHT)){
	        $this->db->query("INSERT INTO settings SET constant_name='ACCOUNT_QUOTE_WEIGHT',value='".$in['quote_weight']."' ");
	      }else{
	        $this->db->query("UPDATE settings SET value='".$in['quote_weight']."' WHERE constant_name='ACCOUNT_QUOTE_WEIGHT' ");
	      }

			msg::success ( gm('Data saved'),'success');
			return true;

	}
	function default_language(&$in){
		msg::success ( gm("Changes have been saved."),'success');
		return true;

	}
	function default_message(&$in)
	{
		$set = "text	= '".$in['text']."', ";
		/*if($in['use_html']){*/
			$set1 = "html_content	= '".$in['html_content']."' ";
		/*}*/
		$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					".$set."
					".$set1."
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		/*$this->db->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
		msg::success( gm('Message has been successfully updated'),'success');
		// json_out($in);
		return true;
	}

	function weblink_default_message(&$in)
	{
		$set = "text	= '".$in['text']."', ";
		/*if($in['use_html']){*/
			$set1 = "html_content	= '".$in['html_content']."', ";
		/*}*/

		$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					".$set."
					".$set1."
					use_html='1'
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		/*$this->db->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
		msg::success( gm('Message has been successfully updated'),'success');
		// json_out($in);
		return true;
	}

	function save_language(&$in){
		switch ($in['languages']){
			case '1':
				$lang = '';
				break;
			case '2':
				$lang = '_2';
				break;
			case '3':
				$lang = '_3';
				break;
			case '4':
				$lang = '_4';
				break;
			default:
				$lang = '';
				break;
		}

		# extra check for custom_langs
		if($in['languages']>=1000) {
			$lang = '_'.$in['languages'];
		}
		if($this->db->field("SELECT default_id FROM default_data WHERE type='quote_note".$lang."' AND default_main_id='0' ")){
			$this->db->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='quote_note".$lang."' AND default_main_id='0' ");
		}else{
			$this->db->query("INSERT INTO default_data SET value='".$in['notes']."', type='quote_note".$lang."', default_main_id='0', default_name='quote_note' ");
		}
		// if($in['terms']){
			$this->db->query("DELETE FROM default_data WHERE type='quote_terms".$lang."' AND default_main_id='0' ");
			$this->db->query("INSERT INTO default_data SET value='".$in['terms']."', type='quote_terms".$lang."',  default_name='quote_term'  ");
		/*}*/
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function label_update(&$in){

	    $fields = '';
	    $table = 'label_language_quote';

	    $exist = $this->db->query("SELECT label_language_id FROM $table WHERE label_language_id='".$in['label_language_id']."' ");
	    if($exist->next()){
    		$this->db->query("UPDATE $table SET
    			quote						='".$in['quote_number']."',
    			quote_note 					='".$in['quote']."',
    			company_name 				='".$in['company_name']."',
    			comp_reg_number       		='".$in['comp_reg_number']."',
    			subject 					='".$in['subject']."',
				billing_address 			='".$in['billing_address']."',
				buyer_delivery_address 		='".$in['buyer_delivery_address']."',
				buyer_delivery_zip 			='".$in['buyer_delivery_zip']."',
				buyer_delivery_city 		='".$in['buyer_delivery_city']."',
				buyer_delivery_country 		='".$in['buyer_delivery_country']."',
				buyer_main_address 			='".$in['buyer_main_address']."',
				buyer_main_zip 				='".$in['buyer_main_zip']."',
				buyer_main_city 			='".$in['buyer_main_city']."',
				buyer_main_country 			='".$in['buyer_main_country']."',
				bank_details 				='".$in['bank_details']."',
				item 						='".$in['item']."',
				subtotal 					='".$in['subtotal']."',
				unit_price 					='".$in['unit_price']."',
				unit_price_vat 				='".$in['unit_price_vat']."',
				vat 						='".$in['vat']."',
				amount_due 					='".$in['amount_due']."',
				notes 						='".$in['notes']."',
				bic_code 					='".$in['bic_code']."',
				phone 						='".$in['phone']."',
				email 						='".$in['email']."',
				our_ref 					='".$in['our_ref']."',
				vat_number 					='".$in['vat_number']."',
				package 					='".$in['package']."',
				article_code 				='".$in['article_code']."',
				chapter_total 				='".$in['chapter_total']."',
				subtotal_2 					='".$in['subtotal_2']."',
				reference 					='".$in['reference']."',
				date 						='".$in['date']."',
				customer 					='".$in['customer']."',
				quantity 					='".$in['quantity']."',
				amount 						='".$in['amount']."',
				discount 					='".$in['discount']."',
				payments 					='".$in['payments']."',
				grand_total 				='".$in['grand_total']."',
				bank_name 					='".$in['bank_name']."',
				iban 						='".$in['iban']."',
				fax 						='".$in['fax']."',
				url 						='".$in['url']."',
				your_ref 					='".$in['your_ref']."',
				sale_unit 					='".$in['sale_unit']."',
				author 						='".$in['author']."',
				general_conditions 			='".$in['general_conditions']."',
				page 						='".$in['page']."',
				valid_until 				='".$in['valid_until']."',
				stamp_signature 			='".$in['stamp_signature']."',
				download_webl 				='".$in['download_webl']."',
				name_webl 					='".$in['name_webl']."',
				addCommentLabel_webl 		='".$in['addCommentLabel_webl']."',
				quote_webl 					='".$in['quote_webl']."',
				accept_webl 				='".$in['accept_webl']."',
				reject_webl 				='".$in['reject_webl']."',
				bankDetails_webl 			='".$in['bankDetails_webl']."',
				cancel_webl 				='".$in['cancel_webl']."',
				bicCode_webl 				='".$in['bicCode_webl']."',
				bank_webl 					='".$in['bank_webl']."',
				pdfPrint_webl 				='".$in['pdfPrint_webl']."',
				email_webl 					='".$in['email_webl']."',
				submit_webl 				='".$in['submit_webl']."',
				date_webl 					='".$in['date_webl']."',
				requestNewVersion_webl 		='".$in['requestNewVersion_webl']."',
				status_webl 				='".$in['status_webl']."',
				payWithIcepay_webl 			='".$in['payWithIcepay_webl']."',
				regularInvoiceProForma_webl ='".$in['regularInvoiceProForma_webl']."',
				ibanCode_webl 				='".$in['ibanCode_webl']."',
				noData_webl 				='".$in['noData_webl']."',
				discount_total 				='".$in['discount_total']."',
				no_vat 						='".$in['no_vat']."'
                WHERE label_language_id='".$in['label_language_id']."'");
  		}else{
  			$this->db->query("UPDATE $table SET
  				quote						='".$in['quote_number']."',
    			quote_note 					='".$in['quote']."',
    			company_name 				='".$in['company_name']."',
    			comp_reg_number       		='".$in['comp_reg_number']."',
    			subject 					='".$in['subject']."',
				billing_address 			='".$in['billing_address']."',
				bank_details 				='".$in['bank_details']."',
				item 						='".$in['item']."',
				subtotal 					='".$in['subtotal']."',
				unit_price 					='".$in['unit_price']."',
				unit_price_vat 				='".$in['unit_price_vat']."',
				vat 						='".$in['vat']."',
				amount_due 					='".$in['amount_due']."',
				notes 						='".$in['notes']."',
				bic_code 					='".$in['bic_code']."',
				phone 						='".$in['phone']."',
				email 						='".$in['email']."',
				our_ref 					='".$in['our_ref']."',
				vat_number 					='".$in['vat_number']."',
				package 					='".$in['package']."',
				article_code 				='".$in['article_code']."',
				chapter_total 				='".$in['chapter_total']."',
				subtotal_2 					='".$in['subtotal_2']."',
				reference 					='".$in['reference']."',
				date 						='".$in['date']."',
				customer 					='".$in['customer']."',
				quantity 					='".$in['quantity']."',
				amount 						='".$in['amount']."',
				discount 					='".$in['discount']."',
				payments 					='".$in['payments']."',
				grand_total 				='".$in['grand_total']."',
				bank_name 					='".$in['bank_name']."',
				iban 						='".$in['iban']."',
				fax 						='".$in['fax']."',
				url 						='".$in['url']."',
				your_ref 					='".$in['your_ref']."',
				sale_unit 					='".$in['sale_unit']."',
				author 						='".$in['author']."',
				general_conditions 			='".$in['general_conditions']."',
				page 						='".$in['page']."',
				valid_until 				='".$in['valid_until']."',
				stamp_signature 			='".$in['stamp_signature']."',
				download_webl 				='".$in['download_webl']."',
				name_webl 					='".$in['name_webl']."',
				addCommentLabel_webl 		='".$in['addCommentLabel_webl']."',
				quote_webl 					='".$in['quote_webl']."',
				accept_webl 				='".$in['accept_webl']."',
				reject_webl 				='".$in['reject_webl']."',
				bankDetails_webl 			='".$in['bankDetails_webl']."',
				cancel_webl 				='".$in['cancel_webl']."',
				bicCode_webl 				='".$in['bicCode_webl']."',
				bank_webl 					='".$in['bank_webl']."',
				pdfPrint_webl 				='".$in['pdfPrint_webl']."',
				email_webl 					='".$in['email_webl']."',
				submit_webl 				='".$in['submit_webl']."',
				date_webl 					='".$in['date_webl']."',
				requestNewVersion_webl 		='".$in['requestNewVersion_webl']."',
				status_webl 				='".$in['status_webl']."',
				payWithIcepay_webl 			='".$in['payWithIcepay_webl']."',
				regularInvoiceProForma_webl ='".$in['regularInvoiceProForma_webl']."',
				ibanCode_webl 				='".$in['ibanCode_webl']."',
				noData_webl 				='".$in['noData_webl']."',
				discount_total 				='".$in['discount_total']."',
				no_vat 						='".$in['no_vat']."'
                WHERE label_language_id='".$in['label_language_id']."'");
 		}
    	msg::success ( gm('Account has been successfully updated'),'success');
    	// $in['failure']=false;

    	return true;
  	}

  	function update_default_email(&$in)
	{
			if($in['bcc_email']){
		      $bcc_email=$in['bcc_email'];
		      $in['bcc_email'] = str_replace(';', ',', $in['bcc_email']);   
		    }
			if(!$this->validate_default_email($in)){
				return false;
			}

			$mail_type = $in['mail_type_1'];
			$this->db->query("UPDATE default_data SET default_name='".$in['default_name']."', value='".$in['email_value']."' WHERE type='quote_email' ");
			$this->db->query("UPDATE default_data SET value='".$mail_type."' WHERE type = 'quote_email_type' ");
	    	$this->db->query("SELECT * FROM default_data WHERE type='bcc_quote_email' ");
		    if($this->db->next()){
		      $this->db->query("UPDATE default_data SET value='".$in['bcc_email']."' WHERE type='bcc_quote_email' ");
		    }else{
		      $this->db->query("INSERT INTO default_data SET value='".$in['bcc_email']."', type='bcc_quote_email' ");
		    }
	    	msg::success ( gm('Default email updated'),'success');
	    	// $in['failure']=false;
	    
	    	return true;
	}
	function validate_default_email(&$in)
	{
		$v = new validation($in);
		$v->field('mail_type_1', 'Email', 'required');
		if($in['mail_type_1'] == 2){
			$v->field('default_name', 'Email', 'required');
			$v->field('email_value', 'Email', 'email:required');
		}
		if($in['bcc_email']){   
	      $v->field('bcc_email', 'Type of link', 'emails');
	    }
		return $v->run();
	}
	function web_link(&$in)
	{
		if($in['quote_link']!=$in['old_quote_link']){
			$in['quote_link_change']=true;
		}
		if($in['quote_pdf']!=$in['old_quote_pdf']){
			$in['quote_pdf_change']=true;
		}
		if($in['quote_accept']!=$in['old_quote_accept']){
			$in['quote_accept_change']=true;
		}
		if($in['quote_comment']!=$in['old_quote_comment']){
			$in['quote_comment_change']=true;
		}
		if($in['quote_reject']!=$in['old_quote_reject']){
			$in['quote_reject_change']=true;
		}
		if($in['quote_new']!=$in['old_quote_new']){
			$in['quote_new_change']=true;
		}

		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_QUOTE_WEB_LINK' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_ACCEPT' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_COMMENTS' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='WEB_INCLUDE_PDF_Q' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_REJECT' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_NEWV' ");
		if($in['quote_link']=='1'){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='USE_QUOTE_WEB_LINK' ");
			if($in['quote_accept']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_ACCEPT' ");
			}
			if($in['quote_comment']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_COMMENTS' ");
			}
			if($in['quote_pdf']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='WEB_INCLUDE_PDF_Q' ");
			}
			if($in['quote_reject']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_REJECT' ");
			}
			if($in['quote_new']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_NEWV' ");
			}
		}else{
			return false;
		}


		msg::success ( gm("Changes have been saved."),'success');
    	// $in['failure']=false;
    	return true;
    }

    function getImagesFromMsg($msg, $tmpFolderPath)
    {
        $arrSrc = array();
        if (!empty($msg))
        {
            preg_match_all('/<img[^>]+>/i', stripcslashes($msg), $imgTags);
            preg_match_all('/<img[^>]+>/i', $msg, $imgTags_1);

            for ($i=0; $i < count($imgTags[0]); $i++)
            {
                preg_match('/src="([^"]+)/i', $imgTags[0][$i], $withSrc);
                //Remove src
                $withoutSrc = str_ireplace('src="', '', $withSrc[0]);

                //data:image/png;base64,
                if (strpos($withoutSrc, ";base64,"))
                {
                    //data:image/png;base64,.....
                    list($type, $data) = explode(";base64,", $withoutSrc);
                    //data:image/png
                    list($part, $ext) = explode("/", $type);
                    //Paste in temp file
                    $withoutSrc = $tmpFolderPath."/".uniqid("temp_").".".$ext;
                    @file_put_contents($withoutSrc, base64_decode($data));
                    $arrSrc[$withoutSrc] = $imgTags_1[0][$i];
                }      
                //$arrSrc[$withoutSrc] = $imgTags_1[0][$i];
            }
        }
        return $arrSrc;
    }

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function sendNewEmail(&$in)
	{
		if(!$this->sendNewEmailValidate($in)){
			msg::error( 'Invalid email address','error');
			json_out($in);
			return false;
		}

		global $config;
		$in['weblink_url'] = $this->external_id($in);
		$this->db->query("SELECT tblquote.acc_manager_id,tblquote.id,tblquote.serial_number,tblquote.pdf_layout,tblquote.pdf_logo,tblquote.discount,tblquote.quote_date,tblquote.buyer_name,tblquote.currency_type, SUM(tblquote_line.amount) AS total, tblquote.use_custom_template, customers.quote_reference, customers.customer_id,tblquote.email_language,tblquote.author_id, tblquote.created_by,tblquote.buyer_id,tblquote.contact_id,tblquote.identity_id
                           FROM tblquote
                           LEFT JOIN tblquote_line ON tblquote_line.quote_id=tblquote.id
                           LEFT JOIN customers ON tblquote.buyer_id=customers.customer_id
                           WHERE tblquote.id='".$in['id']."'");

		$this->db->move_next();
		$cust_id = $this->db->f('customer_id');
		$acc_manager_id=$this->db->f('acc_manager_id');
		$e_lang = $this->db->f('email_language');
		$created_by = $this->db->f('created_by');
		$author = $this->db->f('author_id');
		$buyer_id = $this->db->f('buyer_id');
		$contact_id = $this->db->f('contact_id');
		$identity_id=$this->db->f('identity_id');

		$pdf_layout = $this->db->f('pdf_layout');
		if($this->db->f('use_custom_template')==1){
			$in['custom_type'] = $pdf_layout;
			$in['logo']=$this->db->f('pdf_logo');
		}elseif(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0){
			$in['custom_type'] = ACCOUNT_QUOTE_PDF_FORMAT;
		}else{
			if($pdf_layout){
				$in['type']=$pdf_layout;
				$in['logo']=$this->db->f('pdf_logo');
			}else{
				$in['type'] = ACCOUNT_QUOTE_PDF_FORMAT;
			}
		}

		$this->generate_pdf($in);

		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		// $date = explode('-',$this->dbu->f('quote_date'));
		// $factur_date = $date[2].'/'.$date[1].'/'.$date[0];
		$total=$this->db->f('total');
		$currency=get_commission_type_list($this->db->f('currency_type'));

		$discount_value = $total*$this->db->f('discount')/100;
		$total -= $discount_value;

		$amount_due = round($total,2);

		$mail = new PHPMailer();
		$mail->WordWrap = 50;
		if(!$e_lang || $e_lang > 4){
	    	$e_lang=1;
	    }
		$text_array = array('1' => array('simple' => array('1' => 'Your offer', '2'=> 'Check your offer by clicking on the above link','3'=>"Following this link",'4'=>'Web Link'),
        								   'pay' => array('1' => 'INVOICE WEB LINK', '2'=> 'Check your offer by clicking on the above link.','3'=>"HERE")
        								   ),
        					'2' => array('simple' => array('1' => 'Votre Offre', '2'=> 'Visualisez votre offre en cliquant sur le lien ci-dessus','3'=>'Cliquez sur ce lien','4'=>'Lien web'),
        								   'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Vous pouvez tÃ©lÃ©charger votre facture ICI et la payer en ligne','3'=>'ICI')
        								   ),
        					'3' => array('simple' => array('1' => 'Offerte', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'Via deze link','4'=>'Weblink'),
        								   'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'HIER')
        								   ),
        					'4' => array('simple' => array('1' => 'ANGEBOT WIE WEB-LINK', '2'=> 'Sie kÃ¶nnen Ihr Angebot HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
        								   'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie kÃ¶nnen Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER')
        								   )
        );

		//add images to mail
		$xxx=$this->getImagesFromMsg($in['e_message'],'upload');
        foreach($xxx as $location => $value){
          $tmp_start=strpos($location,'/');
          $tmp_end=strpos($location,'.');
          $tmp_cid_name=substr($location,$tmp_start+1,$tmp_end-1-$tmp_start);
          $tmp_file_name=$tmp_cid_name.substr($location, $tmp_end, strlen($location)-$tmp_end);
          $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].$config['install_path'].$location, 'sign_'.$tmp_cid_name, $tmp_file_name); 
          //$in['e_message']=str_replace($value,'<img src="cid:'.'sign_'.$tmp_cid_name.'">',$in['e_message']);
          $in['e_message']=str_replace($value,generate_img_with_cid($value,$tmp_cid_name),$in['e_message']);
        }

		$in['e_message'] = stripslashes($in['e_message']);
		$body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');

		$def_email = $this->default_email();

    	/* 1 - use user logged email
    	   2 - use default email
    	   3 - use author email
    	*/
    	$mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='quote_email_type' ");
    	
    	if($mail_sender_type == 1 || $mail_sender_type == 3) {
    		if($mail_sender_type == 1) {
    			//$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

    				$mail->SetFrom($this->db_users->f('email'), utf8_decode($mail_name ),0);
    				$def_email['from']['email'] = $this->db_users->f('email');//pt sendgrid
    				$def_email['from']['name'] = $mail_name;//pt sendgrid

    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
    				$def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
    				$def_email['reply']['name'] =  $mail_name;//pt sendgrid

    				$mail->set('Sender',$this->db_users->f('email'));
    			}else{
    				$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
    				$def_email['from']['name'] = $mail_name;//pt sendgrid

    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
    				$def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
    				$def_email['reply']['name'] =  $mail_name;//pt sendgrid

    				$mail->set('Sender',$def_email['reply']['email']);
    			}		
    		} else {
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$author."' ");
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

    				$mail->SetFrom($this->db_users->f('email'), utf8_decode( $mail_name),0);
    				$def_email['from']['email'] = $this->db_users->f('email');//pt sendgrid
    				$def_email['from']['name'] = $mail_name;//pt sendgrid

    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));
    				$def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
    				$def_email['reply']['name'] =  $mail_name;//pt sendgrid

    				$mail->set('Sender',$this->db_users->f('email'));
    			}else{
    				$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
    				$def_email['from']['name'] = $mail_name;//pt sendgrid

    				$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));
    				$def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
    				$def_email['reply']['name'] =  $mail_name;//pt sendgrid

    				$mail->set('Sender',$def_email['reply']['email']);
    			}		
    		}
    	}else{
    		$mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']),0);
    		$mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
    		$mail->set('Sender',$def_email['reply']['email']);
    	}

    	$in['sendgrid_selected'] = 1;
    	
		$subject=utf8_decode( stripslashes($in['e_subject']));

		$mail->Subject = $subject;

		if($in['include_pdf']){
			$tmp_file_name = 'quote_.pdf';
			if($in['attach_file_name']){
				$tmp_file_name = $in['attach_file_name'];
			}
			$mail->AddAttachment($tmp_file_name, $this->db->f('quote_reference').$in['serial_number'].'.pdf'); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
		}
		if($in['files']){
	        foreach ($in['files'] as $key => $value) {
	        	if($value['checked'] == 1){
		          $mime = mime_content_type($value['path'].$value['file']);
		          $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
		        }
	        }
	    }
		if($in['dropbox_files']){
			$path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
			if(!file_exists($path_dropbox)){
				mkdir($path_dropbox,0775,true);
			}
			$dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

	        foreach ($in['dropbox_files'] as $key => $value) {
	        	if($value['checked'] == 1){
	        		$value['file'] = str_replace('/', '_', $value['file']);
		        	//$f = fopen($path_dropbox.$value['file'], 'w+');
		        	/*$f=$path_dropbox.$value['file'];*/

		        	$file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
		        	//fclose($f);
		        	//file_get_contents($path_dropbox.$value['file']);
					
					
			        $mime = mime_content_type($path_dropbox.$value['file']);
			        $mail->AddAttachment($path_dropbox.$value['file'], $value['file']);
			        //unlink($path_dropbox.$value['file']);
		        }
	        }
	    }
		$mail->WordWrap = 50;

		$body=str_replace('[!DROP_BOX!]', '', $body);
			 // console::log($body);
		    if($in['file_d_id']){
		    	$body.="\n\n";
			    foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
				}
			}
		$tblinvoice = $this->db->query("SELECT * FROM tblquote WHERE id='".$in['quote_id']."'  ");	
		$contact = $this->db->query("SELECT firstname,lastname FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'  ");
		$title_cont = $this->db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'  ");
		$customer = $this->db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."'  ");


	    //$mail->MsgHTML(nl2br($body));
		if($in['use_html']){

			if($in['sendgrid_selected']){
		      $e_message_encoded = $in['e_message'];
		    } else {
		      $e_message_encoded = utf8_decode( $in['e_message']);
		    }  
			
			$head = '<style>p {	margin: 0px; padding: 0px; }</style>';

	      	$mail->IsHTML(true);
	      	$body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 35px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 30px;">'.$e_message_encoded.'</div>';

	     	$body = stripslashes($body_style);
			$body=str_replace('[!CONTACT_FIRST_NAME!]', "".$contact->f('firstname')."", $body);
			$body=str_replace('[!CONTACT_LAST_NAME!]', "".$contact->f('lastname')."", $body);
			$body=str_replace('[!SALUTATION!]', "".$title_cont."", $body);	
			$body=str_replace('[!YOUR_REFERENCE!]',"".$tblinvoice->f('your_ref')."",$body);
	      	if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
				//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `version_id`='".$in['version_id']."' AND `type`='q'  ");
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `version_id`= :version_id AND `type`= :type  ",['d'=>DATABASE_NAME,'item_id'=>$in['id'],'version_id'=>$in['version_id'],'type'=>'q']);

		     	// $extra = "<p style=\"background: #dedede; width: 500px; text-align:center; padding-bottom: 15px;\"><br />";
		     	// $extra .="<b>".$text_array[$e_lang]['simple']['1']."</b><br />";
		    	// $extra .= str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2'])."<br /></p>";

		     	$extra = "<p style=\"background: #e6e6e6; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #e6e6e6; color:#868d91; border-radius:8px;\">";
	        	
	    		$extra .="<b style=\"color: #5199b7; text-align:center; font-size:32px;\"><a style=\"text-decoration:none; text-transform: lowercase; color:#6399c6;\" href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b><br />";
	        	
	    		$extra .=$text_array[$e_lang]['simple']['2']."<br /></p>";

		     	// $body .=$extra;
	    		if($identity_id){
		     		$identity_logo = $this->db->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$identity_id."'");
		     		if($identity_logo){
		     			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].$identity_logo."\" alt=\"\">",$body);
		     		}else{
		     			if($in['copy']){
			    			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_QUOTE."\" alt=\"\">",$body);
				     	}elseif($in['copy_acc']){
							$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
				     	}else{
				     		$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_QUOTE."\" alt=\"\">",$body);
				     	}
		     		}
		     	}else{
		     		if($in['copy']){
		    			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_QUOTE."\" alt=\"\">",$body);
			     	}elseif($in['copy_acc']){
						$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
			     	}else{
			     		$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_QUOTE."\" alt=\"\">",$body);
			     	}
			    }
		     	
		     	$body=str_replace('[!WEB_LINK!]', $extra, $body);
		     	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
		     	$body=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$exist_url, $body);

		    }else{
			    $body=str_replace('[!WEB_LINK!]', '', $body);
			    $body=str_replace('[!WEB_LINK_2!]', '', $body);
			    $body=str_replace('[!WEB_LINK_URL!]', '', $body);
			}
			if(defined('DROPBOX') && DROPBOX != ''){
			    if(strpos($body, '[!DROP_BOX!]')){
			        $body=str_replace('[!DROP_BOX!]', '', $body);
			        $id = $buyer_id;
			        $is_contact = 0;
			        if(!$id){
			            $id = $contact_id;
			            $is_contact = 1;
			        }
			        $d = new drop('quotes', $id, $in['id'],true,'',$is_contact, $in['serial_number']);
			        $files = $d->getContent();
			        if(!empty($files->entries)){
			            $body .="<br><br>";
			            foreach ($files->entries  as $key => $value) {
			                $l = $d->getLink(urldecode($value->path_display));
			                $link = $l;
			                $file = $in['serial_number'].'/'.str_replace('/','_', $value->path_display);
			                $body.="<p><a href=".$link.">".$file."</a></p>";
			            }
			        }
			    }
			}
			$body=str_replace('[!DROP_BOX!]', '', $body);
// console::log($body);
			if($in['file_d_id']){
				$body .="<br><br>";
				foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=".$in['file_d_path'][$key].">".$in['file_d_name'][$key]."</a></p>";
				}
			}
			$body = $head.$body;
		    $mail->Body    = $body;
	    }else{

	    	if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){console::log('use weblink');
				//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `version_id`='".$in['version_id']."' AND `type`='q'  ");
	    		$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `version_id`= :version_id AND `type`= :type  ",['d'=>DATABASE_NAME,'item_id'=>$in['id'],'version_id'=>$in['version_id'],'type'=>'q']);

		      	$extra = "\n\n-----------------------------------";
		      	$extra .="\n".$text_array[$e_lang]['simple']['1'];
		      	$extra .="\n-----------------------------------";
		      	$extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
						// $body .=$extra;
		      	$body=str_replace('[!WEB_LINK!]', $extra, $body);
		      	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
		      	$body=str_replace('[!WEB_LINK_URL!]', $config['web_link_url']."?q=".$exist_url, $body);
		    }else{
		      	$body=str_replace('[!WEB_LINK!]', '', $body);
		      	$body=str_replace('[!WEB_LINK_2!]', '', $body);
		      	$body=str_replace('[!WEB_LINK_URL!]', '', $body);
		    }
/*		    if(defined('DROPBOX') && DROPBOX != ''){
			    if(strpos($body, '[!DROP_BOX!]')){

			        $id = $buyer_id;
			        $is_contact = 0;
			        if(!$id){
			            $id = $contact_id;
			            $is_contact = 1;
			        }
			        $d = new drop('quotes', $id, $in['id'],true,'',$is_contact, $in['serial_number']);
			        $files = $d->getContent();
			        if(!empty($files->entries)){
			            $body.="\n\n";
			            foreach ($files->entries  as $key => $value) {
			                $l = $d->getLink(urldecode($value->path_display));
			                $link = $l;
			                $file = $in['serial_number'].'/'.str_replace('/','_', $value->path_display);
			                // $body.="<p><a href=".$link.">".$file."</a></p>";
			                $body.="<p><a href=\"".$link."\">".$file."</a></p>\n";
			            }
			        }
			    }
			}*/

			// $body=str_replace('[!DROP_BOX!]', '', $body);
			// console::log($body);
		    /*if($in['file_d_id']){
		    	$body.="\n\n";
			    foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
				}
			}*/
	      	$mail->MsgHTML(nl2br($body));
	    }
    // console::log($body);

		if($in['copy']){
			//$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
			if($u->f('email')){
				$mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
			}
		}
		if($in['copy_acc']){
			$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$acc_manager_id."' ");
			if($u->f('email')){
				$mail->AddBCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
			}
		}


		/*$this->db->query("SELECT customer_contacts.*,tblquote.id,tblquote.buyer_id,tblquote.serial_number
                           FROM tblquote
                           INNER JOIN customer_contacts ON customer_contacts.customer_id=tblquote.buyer_id
                           WHERE tblquote.id ='".$in['id']."'");
		while ($this->db->move_next()) {
			if($in['recipients'][$this->db->f('contact_id')]){
				$mail->AddAddress($this->db->f('email'),$this->db->f('firstname').' '.$this->db->f('lastname'));
			}
		};*/

		/*if($in['send_to']){
			$mail->AddAddress($in['send_to']);
		}*/

		$mail->AddAddress(trim($in['email']));
		if($in['mark_as_sent'] == 1){
			$sent_date= time();
			$this->db->query("UPDATE tblquote_version SET sent='1',sent_date='".$sent_date."' WHERE quote_id='".$in['id']."' AND version_id='".$in['version_id']."' ");
			$this->db->query("UPDATE tblquote SET sent='1', sent_date='".$sent_date."' WHERE id='".$in['id']."' ");
			$this->db->query("UPDATE tblquote SET pending = '1' WHERE id = '".$in['id']."' ");
			$in['external_url']				= $exist_url;
			$in['web_url']					= $config['web_link_url'].'?q=';
            $ver = $this->db->query("SELECT * FROM tblquote_version WHERE quote_id='".$in['id']."' ");
            $in['versions'] = array();
            while($ver->next()){
                $line = array(
                    'VERSION_CODE'	=> $ver->f('version_code'),
                    'version_id'	=> $ver->f('version_id'),
                    /*'active'	    => $ver->f('active') == 1 ? ($this->quote['version_status_customer'] > 0 ? ($this->quote['version_status_customer'] == 1 ? 'lost' : 'active') : ($ver->f('sent') == 1 ? 'sent' : '')) : ($ver->f('sent') == 1 ? 'sent' : ''),*/
                    'active'	    => $ver->f('version_status_customer') > 0 ? ($ver->f('version_status_customer') == 1 ? 'lost' : 'active') : ($ver->f('sent') == 1 ? 'sent' : ''),
                    'selected'		=> $ver->f('version_id') == $in['version_id'] ? 'selected' : '',
                    'VERSION_LINK'  => ('index.php?do=quote-quote&quote_id='.$ver->f('quote_id').'&version_id='.$ver->f('version_id')),
                );
                array_push($in['versions'],$line);
            }
		}

		$msg_log = '{l}Quote has been sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to:{endl} '.$in['email'];

		//$in['sendgrid_selected'] = 0;//pt test

		if($in['sendgrid_selected']){

          $in['new_email'] = $in['email'];

          include_once(__DIR__."/../../misc/model/sendgrid.php");
          $sendgrid = new sendgrid($in, $this->pag, $this->field_n,$in['id'], $body,$def_email, DATABASE_NAME);

          $sendgrid_data = $sendgrid->get_sendgrid($in);

          if($sendgrid_data['error']){
            msg::error($sendgrid_data['error'],'error');
          }elseif($sendgrid_data['success']){
            msg::success(gm("Email sent by Sendgrid"),'success');
          }

          //$msg_log = $msg_log .= " {l}via SendGrid{endl}.";
          
        }else{
        	$mail->Send();
        	if($mail->IsError()){
        		foreach($xxx as $location => $value){
			      unlink($location);
			    }
				if($in['attach_file_name']){
					unlink($in['attach_file_name']);
				}
				if($in['dropbox_files']){
					$path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
					
			        foreach ($in['dropbox_files'] as $key => $value) {
			        	if($value['checked'] == 1){
			        		$value['file'] = str_replace('/', '_', $value['file']);
				        	unlink($path_dropbox.$value['file']);
				        }
			        }
			    }
				msg::error ( $mail->ErrorInfo,'error');
	        	json_out($in);
				return false;
			}

			if($mail->ErrorInfo){
				//msg::notice( $mail->ErrorInfo,'notice');
			}

			msg::success( gm('Email sent').'.','success');
			
			if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
				update_log_message($in['logging_id'],' '.$in['email']);
			}else{
				$in['logging_id'] =insert_message_log($this->pag, $msg_log,$this->field_n,$in['id'],false,$_SESSION['u_id'],true,$cust_id,$sent_date);
			}	
        }


		//console::log($mail);
		foreach($xxx as $location => $value){
	      unlink($location);
	    }
		if($in['attach_file_name']){
			unlink($in['attach_file_name']);
		}
		if($in['dropbox_files']){
			$path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
			
	        foreach ($in['dropbox_files'] as $key => $value) {
	        	if($value['checked'] == 1){
	        		$value['file'] = str_replace('/', '_', $value['file']);
		        	unlink($path_dropbox.$value['file']);
		        }
	        }
	    }	

      	json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendNewEmailValidate(&$in)
	{
		$v = new validation($in);
		$in['email'] = trim($in['email']);
		$v->field('email', 'Email', 'email');
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function external_id(&$in)
	{
		if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
			//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `type`='q' AND version_id='".$in['version_id']."' ");
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type AND version_id= :version_id ",['d'=>DATABASE_NAME,'item_id'=>$in['id'],'type'=>'q','version_id'=>$in['version_id']]);
			if(!$exist_url){
				$url = generate_chars();
				while (!isUnique($url)) {
					$url = generate_chars();
				}
				//$this->db_users->query("INSERT INTO urls SET `url_code`='".$url."', `database`='".DATABASE_NAME."', `item_id`='".$in['id']."', `type`='q', version_id='".$in['version_id']."' ");
				$this->db_users->insert("INSERT INTO urls SET `url_code`= :url_code, `database`= :d, `item_id`= :item_id, `type`= :type, version_id= :version_id ",['url_code'=>$url,'d'=>DATABASE_NAME,'item_id'=>$in['id'],'type'=>'q','version_id'=>$in['version_id']]);
				$exist_url = $url;
			}
			return $exist_url;
		}
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function generate_pdf(&$in)
	{
		include_once(__DIR__.'/../controller/print.php');
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		/*if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
			$array['from']['email'] = ACCOUNT_EMAIL;
		}*/
		if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
			if(MAIL_SETTINGS_PREFERRED_OPTION==2){
				if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
					$array['from']['email'] = MAIL_SETTINGS_EMAIL;
				}
			}else{
				if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
					$array['from']['email'] = ACCOUNT_EMAIL;
				}
			}
		}
		$this->db->query("SELECT * FROM default_data WHERE type='quote_email' ");
		//if($this->db->move_next()){
		if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
			$array['from']['name'] = $this->db->f('default_name');
			if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)) && $this->db->f('value')!=''){
				$array['from']['email'] = $this->db->f('value');
			}
		}

		$this->db->query("SELECT * FROM default_data WHERE type='bcc_quote_email' ");
          if($this->db->move_next() && $this->db->f('value')){
            $array['bcc']['email'] = $this->db->f('value'); // pot fi valori multiple, delimitate de ;
          }
		return $array;
	}

	function mark_sent(&$in){		
		if(!$this->mark_sent_validate($in))
		{
			return false;
		}
		
		$in['s_date'] = strtotime($in['sent_date']);
		$this->db->query("UPDATE tblquote_version SET sent='1',sent_date='".$in['s_date']."' WHERE version_id='".$in['version_id']."' AND quote_id='".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote SET sent='1', sent_date='".$in['s_date']."' WHERE id='".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote SET pending= '1' WHERE id = '".$in['quote_id']."' ");

		msg::success(gm("Quote has been successfully marked as sent"),'success');
		insert_message_log($this->pag,'{l}Quote has been manually marked as sent by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id']);
		$in['id'] = $in['quote_id'];
		$this->external_id($in);
		return true;
	}

	/****************************************************************
	* function mark_sent_validate(&$in)                                *
	****************************************************************/
	function mark_sent_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('quote_id', 'ID', 'required:exist[tblquote.id]', "Invalid Id");
		$v->field('sent_date', 'ID', 'required');
		$is_ok = $v->run();
		if($is_ok){
			$versionDateQ = $this->db->query("SELECT version_date FROM tblquote_version WHERE version_id = '".
				$in['version_id']."' AND quote_id = '".$in['quote_id']."'");
			$in['quote_version_date'] = gmdate("Y-m-d\TH:i:s\Z",(int)$versionDateQ->f('version_date'));

			$quoteVersionDate =  new DateTime($in['quote_version_date']);
			$quoteVersionDate->setTime(0,0,0);
			$quoteVersionDate = $quoteVersionDate->format('U') + 7200;
			
			$sentDate = new DateTime($in['sent_date']);
			$sentDate->add(new DateInterval('P1D'));
			$sentDate->setTime(0,0,0);
			$sentDate = $sentDate->format('U') + 7200;

			if($sentDate < $quoteVersionDate){
				msg::error(gm('Sent Date must be equal or be grater than Quote Date'),'error');
				return false;
			}
		}

		return $is_ok;
	}

	  /**
	 * Send order by email
	 *
	 * @return bool
	 * @author PM
	 **/
	function pdf_settings(&$in){
   		if(!($in['quote_id']&&$in['pdf_layout']&&$in['logo']) )
		{
			msg::error(gm('Invalid ID'),'error');
			json_out($in);
			return false;
		}
    	
		if($in['logo'] == '../img/no-logo.png'){
			$in['logo'] = 'img/no-logo.png';
		}
    
	    //$this->db->query("UPDATE tblquote SET pdf_layout='".ACCOUNT_QUOTE_BODY_PDF_FORMAT."',pdf_logo='".$in['logo']."',email_language='".$in['email_language']."', identity_id='".$in['identity_id']."' WHERE id = '".$in['quote_id']."' ");

		if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 ){
			   $this->db->query("UPDATE tblquote SET email_language='".$in['email_language']."', identity_id='".$in['identity_id']."', pdf_layout='0' WHERE id = '".$in['quote_id']."' ");
			  }else{
			   $this->db->query("UPDATE tblquote SET pdf_layout='".ACCOUNT_QUOTE_BODY_PDF_FORMAT."',pdf_logo='".$in['logo']."',email_language='".$in['email_language']."', identity_id='".$in['identity_id']."' WHERE id = '".$in['quote_id']."' ");
			  }
			  
	    $this->db->query("UPDATE tblquote_version SET preview='0' WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."' ");

	  	msg::success ( gm("Pdf settings have been updated"),'success');
	  	return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function revert(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}

		$this->db->query("UPDATE tblquote SET sent='0' WHERE id='".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote SET pending = '0' WHERE id= '".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote_version SET sent='0' WHERE quote_id='".$in['quote_id']."' AND version_id='".$in['version_id']."' ");
		// $in['quote_id'] = $in['id'];
		msg::success(gm("Quote  has been return to draft"),'success');
		insert_message_log($this->pag,'{l}Quote  has been return to draft by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id']);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('quote_id', 'ID', 'required:exist[tblquote.id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function status(&$in){
		if($in['id']){ $in['quote_id'] = $in['id']; }
		$q = $this->db->query("select id from tblquote where id=".$in['quote_id']);
		$cust_id = $this->db->field("SELECT buyer_id FROM tblquote WHERE id='".$in['quote_id']."'");
		if(!$q->next()){
			$in['error'] = gm("Invalid ID");
			msg::error(gm("Invalid ID"),'error');
			json_out($in);
			return false;
		}
		$time = time();

		$this->db->query("UPDATE tblquote SET status_c_date='".$time."' WHERE id=".$in['quote_id']);
		/*$this->db->query("UPDATE tblquote SET lost_id = 0 WHERE id = '".$in['quote_id']."' ");*/
		// if($in['status_customer'] > 1 ){
			$this->db->query("UPDATE tblquote_version SET active = 0 WHERE quote_id='".$in['quote_id']."'");
			$this->db->query("UPDATE tblquote_version SET active = 1,version_status_customer='".$in['status_customer']."' WHERE quote_id='".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			// msg::$success = gm("Quote Version was selected");
			// insert_message_log($this->pag,'{l}Quote Version was selected{endl} '.get_user_name($_SESSION['u_id']),'quote_id',$in['quote_id'],false,$_SESSION['u_id'],true,$cust_id);
		// }else
		if ($in['status_customer'] == 1) {
				$exist=$this->db->field("SELECT id FROM tblquote_lost_reason WHERE id='".$in['lost_id']."'");
				if($exist){
					$this->db->query("UPDATE tblquote_version SET version_lost_id='".$exist."' WHERE quote_id='".$in['quote_id']."' AND version_id='".$in['version_id']."' ");
				}
		}

		$in['date'] = date(ACCOUNT_DATE_FORMAT,$time);
		$opts = array('{l}Pending{endl}','{l}Rejected{endl}', '{l}Accepted{endl}', '{l}Accepted{endl}', '{l}Accepted{endl}', '{l}Draft{endl}');
		$the_time = time();
		$this->db->query("INSERT INTO tblquote_history SET quote_id='".$in['quote_id']."', date='".$the_time."', message=(SELECT version_code FROM tblquote_version WHERE version_id='".$in['version_id']."' ) ");
		insert_message_log($this->pag,'{l}Approval status changed to{endl} '.$opts[$in['status_customer']].' {l}by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id'],false,$_SESSION['u_id'],true,$cust_id,$the_time);
		msg::success ( gm("Quote has been updated"),'success');
		return true;
	}

	function DeleteField(&$in){

		$this->db->query("DELETE FROM tblquote_".$in['table']." WHERE id='".$in['field_id']."' ");
		/*msg::success ( gm("Changes have been saved."),'success');*/
		return true;
	}

	/**
	 * add new custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function add_field(&$in)
	{
		$in['field_id'] = $this->db->insert("INSERT INTO tblquote_".$in['table']." SET name='".$in['name']."' ");
		/*msg::success(gm('Field added.'),'success');*/
		return true;
	}

	 function UpdateField(&$in){
	 	$this->db->insert("UPDATE tblquote_".$in['table']." SET name='".$in['value']."' WHERE id='".$in['id']."' ");
		/*msg::success(gm('Field updated.'),'success');*/
		return true;
	 }
	function saveArticleField(&$in){
		$v = new validation($in);
		$v->field('text', 'Field', 'required', gm("Invalid Field Label"));

		if(!$v->run()){
			return false;
		}

		//Check if User Has Catalog +
		//Check if Article Name, Article Name 2 or Description are written in text input
		if(!$in['ADV_PRODUCT']){
			$fieldExist = false;
			$fields = array('[!ITEM_NAME!]','[!ITEM_NAME2!]','[!DESCRIPTION!]');
			foreach ($fields as $value) {
				if(strpos($in['text'],$value) !== false){
					$fieldExist = true;
					break;
				}
			}
			if($fieldExist){
				msg::error(gm('You must have Catalag+ in order to add the article desired fields'),'error');
				return false;
			}
		}

	 	$this->db->insert("UPDATE settings SET long_value='".$in['text']."' WHERE constant_name='QUOTE_FIELD_LABEL' ");
	 	msg::success(gm('Field updated.'),'success');
		return true;
	}
	function savepacking(&$in){
		if(!ALLOW_ARTICLE_SALE_UNIT  && $in['sale']){
			msg::error ( gm("There are no support for article Sale Unit.Please check first that option on Article Settings."),'success');
			return false;
		}

		if(!$in['pack']){
			$in['pack']=0;
			$in['sale']=0;
		}

		if(!$in['sale']){
			$in['sale']=0;
		}
	 	$this->db->insert("UPDATE settings SET value='".$in['pack']."' WHERE constant_name='ALLOW_QUOTE_PACKING' ");
	 	$this->db->insert("UPDATE settings SET value='".$in['sale']."' WHERE constant_name='ALLOW_QUOTE_SALE_UNIT' ");
	 	msg::success(gm('Field updated.'),'success');
		return true;
	}
	function deleteatach(&$in){
		$path=$this->db->field("SELECT `path` FROM attached_files WHERE file_id='".$in['id']."' ");
           unlink($path.stripslashes($in['name']));
		$this->db->query("DELETE FROM attached_files WHERE file_id='".$in['id']."' AND name='".$in['name']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function defaultcheck(&$in){

		$this->db->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function setact(&$in){

		$is_set=$this->db->field("SELECT value FROM settings WHERE constant_name='SHORT_QUOTES_BY_SERIAL' ");
		if(is_null($is_set)){
			$this->db->insert("INSERT INTO settings SET constant_name='SHORT_QUOTES_BY_SERIAL',value='".$in['id']."',type='1' ");
		}else{
			$this->db->insert("UPDATE settings SET value='".$in['id']."' WHERE constant_name='SHORT_QUOTES_BY_SERIAL' ");
		}	
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function saveterm(&$in){

		$this->db->insert("UPDATE settings SET value='".$in['quote_term']."' WHERE constant_name='QUOTE_TERM' ");
		$this->db->insert("UPDATE settings SET value='".$in['choose_quote_term_type']."' WHERE constant_name='QUOTE_TERM_TYPE' ");
		$this->db->insert("UPDATE settings SET value='".$in['apply_discount']."' WHERE constant_name='QUOTE_APPLY_DISCOUNT' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	/**
	 * Archive quote
	 *
	 * @param array $in
	 */
	function archive(&$in){
		if(!$this->archive_validate($in)){
			return false;
		}
		$oldQuoteType=$this->db->field("SELECT f_archived FROM tblquote WHERE id='".$in['id']."' ");
		if($oldQuoteType != '2'){
		    $this->db->query("UPDATE tblquote SET f_archived='1', serial_number='' WHERE id='".$in['id']."' ");
		}else{
		    $this->db->query("UPDATE tblquote SET f_archived='1' WHERE id='".$in['id']."' ");
		}
		msg::success ( gm('Quote archived'),'success');
		insert_message_log($this->pag,'{l}Quote archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['id']);
		$tracking_trace=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".$in['id']."' ");
	    if($tracking_trace){
	      $this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
	    }
		return true;

	}

	/**
	 * Archive validation
	 * We check to see if we have the quote with this id
	 *
	 * @param array $in
	 * @return true or false
	 */
	function archive_validate(&$in){
		$v = new validation($in);
		$v->f('id', 'Id', 'required:exist[tblquote.id]',gm('Invalid ID'));
		return $v->run();
	}

	/**
	 * Delete a quote
	 *
	 * @param array $in
	 * @return true
	 */
	function delete(&$in){
		if(!$this->archive_validate($in)){
			return false;
		}

		/*$img = glob(__DIR__.'/../../../../upload/'.DATABASE_NAME.'/quote_cache/quote_'.$in['id'].'_*.png');
		foreach ($img as $filename) {
			@unlink($filename);
		}*/
		$tracking_trace=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".$in['id']."' ");
		$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_history WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_version WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM logging WHERE field_name='quote_id' AND field_value='".$in['id']."' ");
		$this->db->query("DELETE FROM tblquote WHERE id = '".$in['id']."'");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'notes' AND item_id = '".$in['id']."'");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'free_text_content' AND item_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblopportunity_quotes WHERE quote_id = '".$in['id']."'");
		if($tracking_trace){
		      $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
		      $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
		}
	  	msg::success (gm("Quote deleted."),'success');

	  	return true;
	}

	/**
	 * Activate a quote
	 *
	 * @param array $in
	 * @return true
	 */
	function activate(&$in){
		if(!$this->archive_validate($in)){
			return false;
		}
		$oldQuoteType=$this->db->field("SELECT f_archived FROM tblquote WHERE id='".$in['id']."' ");
		if($oldQuoteType != '2'){
		    $this->db->query("UPDATE tblquote SET f_archived='0',serial_number='".generate_quote_number(DATABASE_NAME)."' WHERE id='".$in['id']."' ");
		}else{
		    $this->db->query("UPDATE tblquote SET f_archived='0' WHERE id='".$in['id']."' ");
		}
		$tracking_trace=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".$in['id']."' ");
	    if($tracking_trace){
	      $this->db->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
	    }
	  	msg::success ( gm("Quote activated"),'success');
	  	insert_message_log($this->pag,'{l}Quote activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['id']);
	  	return true;
	}

	function currencyRate($currency=''){
		if(empty($currency)){
			$currency = "USD";
		}
		$separator = ACCOUNT_NUMBER_FORMAT;
		$into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		return currency::getCurrency($currency, $into, 1,$separator);
	}

	/**
	 * Delete a quote template
	 *
	 * @return bool
	 * @author Mp
	 **/
	function delete_template(&$in)
	{
		if(!$this->archive_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_history WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_version WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM logging WHERE field_name='quote_id' AND field_value='".$in['id']."' ");
		$this->db->query("DELETE FROM tblquote WHERE id = '".$in['id']."'");

	  	msg::success ( gm("Template deleted."),'success');

	  	return true;
	}
	  function saveAddToPdf(&$in)
	  {

	    if($in['all']){
	      if($in['value'] == 1){
	        foreach ($in['item'] as $key => $value) {
	          $_SESSION['add_to_quote'][$value]= $in['value'];
	        }
	      }else{
	        foreach ($in['item'] as $key => $value) {
	          unset($_SESSION['add_to_quote'][$value]);
	        }
	      }
	    }else{
	      if($in['value'] == 1){
	        $_SESSION['add_to_quote'][$in['item']]= $in['value'];
	      }else{
	        unset($_SESSION['add_to_quote'][$in['item']]);
	      }
	    }
	      json_out($_SESSION['add_to_quote']);
	    return true;
	  }

	  function deleteBulk(&$in){
	  	if(empty($_SESSION['add_to_quote'])){
	  		msg::error(gm('No quote selected'),'error');
	  		json_out($in);
	  	}

	  	$add_to_quote=$this->db->query("SELECT id FROM tblquote WHERE f_archived='1' AND id IN (".implode(',',array_keys($_SESSION['add_to_quote'])).")")->getValues('id');

	    if(empty($add_to_quote)){
	      msg::error(gm('No quote selected'),'error');
	      json_out($in);
	    }

	  	foreach($add_to_quote as $value){
	  		$tracking_trace=$this->db->field("SELECT trace_id FROM tblquote WHERE id='".$value."' ");
			$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$value."'");
			$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$value."'");
			$this->db->query("DELETE FROM tblquote_history WHERE quote_id = '".$value."'");
			$this->db->query("DELETE FROM tblquote_version WHERE quote_id = '".$value."'");
			$this->db->query("DELETE FROM logging WHERE field_name='quote_id' AND field_value='".$value."' ");
			$this->db->query("DELETE FROM tblquote WHERE id = '".$value."'");
			$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'notes' AND item_id = '".$value."'");
			$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'free_text_content' AND item_id = '".$value."'");
			$this->db->query("DELETE FROM tblopportunity_quotes WHERE quote_id = '".$value."'");
			if($tracking_trace){
			      $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
			      $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
			}
			unset($_SESSION['add_to_quote'][$value]);
	  	}
	  	msg::success(gm('Quotes deleted'),'success');
	  	return true;
	  }
	  
	function set_identity(&$in)
	{
		$this->db->query("UPDATE settings SET value='".$in['identity_id']."' WHERE constant_name='QUOTE_IDENTITY_SET'");
	  	msg::success ( gm("Changes Saved."),'success');

	  	return true;
	}

	function postRegister(&$in){
        global $config;

        $env_usr = $config['postgreen_user'];
        $env_pwd = $config['postgreen_pswd'];

        $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> '',
	      							'module'		=> 'quote',
	      							'date'			=> time(),
	      							'action'		=> 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'],
	      							'content'		=> '',
	      							'login_user'	=> addslashes($in['user']),
	      							'error_message'	=> ''
	      			);

        try{

        $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
        $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
        $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
        $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
        $soap->__setSoapHeaders(array($objHeader_Session_Outside));
        $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$in['user'],'password'=>$in['pass']));

        if($res->action->data[0]->_ == '000'){
          $post_id=$this->db->insert("INSERT INTO apps SET
                                                  main_app_id='0',
                                                  name='PostGreen',
                                                  api='".$in['user']."',
                                                  type='main',
                                                  active='1' ");
          $this->db->query("INSERT INTO apps SET main_app_id='".$post_id."', api='".$in['pass']."' ");
          /*msg::success(gm("PostGreen service activated"),"success");*/
          msg::success ( gm("Changes have been saved."),'success');
        }else{
          msg::error(gm('Incorrect credentials provided'),"error");

        }

    } catch (Exception $e) {
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");

        $post_green_data['error_message'] = addslashes($e->getMessage());
        error_post($post_green_data);
        return true;
    }
        return true;
    }

    function postIt(&$in){
	      global $config;

	      $id=$in['id'];
	      $version_id=$in['version_id'];

	      $quote=$this->db->query("SELECT tblquote_version.*,tblquote.buyer_country_id,tblquote.email_language,tblquote.pdf_layout,tblquote.use_custom_template,tblquote.pdf_logo,tblquote.serial_number,tblquote.buyer_email,tblquote.buyer_phone,tblquote.buyer_fax,tblquote.buyer_name,tblquote.buyer_address,tblquote.buyer_zip,tblquote.buyer_city FROM tblquote_version 
	      	INNER JOIN tblquote ON tblquote_version.quote_id=tblquote.id
	      	WHERE tblquote_version.version_id='".$in['version_id']."' ");

	      $buyer_country=$this->db->field("SELECT name FROM country WHERE country_id='".$quote->f('	buyer_country_id')."' ");
	      $seller_country=$this->db->field("SELECT name FROM country WHERE country_id='".ACCOUNT_BILLING_COUNTRY_ID."' ");

	      ark::loadLibraries(array('aws'));
	      $aws = new awsWrap(DATABASE_NAME);
	      $in['attach_file_name'] = 'quote_'.$in['id'].'_'.$in['version_id'].'.pdf';
	      $file = $aws->getItem('quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf',$in['attach_file_name']);
	      $doc_name='quote_'.$quote->f('serial_number').'['.$quote->f('version_code').'].pdf';
	      if($file !== true){
	          $in['attach_file_name'] = null;
	          msg::error(gm("Generate PDF"),"error");
	          json_out($in);
	      }

	      $handle = fopen($in['attach_file_name'], "r");   
	      $contents = fread($handle, filesize($in['attach_file_name'])); 
	      fclose($handle);                               
	      $decodeContent = base64_encode($contents);

	      $filename ="addresses.csv";
	      ark::loadLibraries(array('PHPExcel'));
	      $objPHPExcel = new PHPExcel();
	      $objPHPExcel->getProperties()->setCreator("Akti")
	               ->setLastModifiedBy("Akti")
	               ->setTitle("Addresses")
	               ->setSubject("Addresses")
	               ->setDescription("Addresses")
	               ->setKeywords("Addresses")
	               ->setCategory("Addresses");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', "TITLE")
	            ->setCellValue('B1', "FIRST_NAME")
	            ->setCellValue('C1', "LAST_NAME")
	            ->setCellValue('D1', "EMAIL")
	            ->setCellValue('E1', "PHONE")
	            ->setCellValue('F1', "MOBILE")
	            ->setCellValue('G1', "FAX")
	            ->setCellValue('H1', "COMPANY")
	            ->setCellValue('I1', "ADDRESS_LINE_1")
	            ->setCellValue('J1', "ADDRESS_LINE_2")
	            ->setCellValue('K1', "ADDRESS_LINE_3")
	            ->setCellValue('L1', "ADDRESS_POSTCODE")
	            ->setCellValue('M1', "ADDRESS_CITY")
	            ->setCellValue('N1', "ADDRESS_STATE")
	            ->setCellValue('O1', "ADDRESS_COUNTRY");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A2', "")
	            ->setCellValue('B2', "")
	            ->setCellValue('C2', "")
	            ->setCellValue('D2', $quote->f('buyer_email'))
	            ->setCellValue('E2', $quote->f('buyer_phone'))
	            ->setCellValue('F2', "")
	            ->setCellValue('G2', $quote->f('buyer_fax'))
	            ->setCellValue('H2', $quote->f('buyer_name'))
	            ->setCellValue('I2', $quote->f('buyer_address'))
	            ->setCellValue('J2', "")
	            ->setCellValue('K2', "")
	            ->setCellValue('L2', $quote->f('buyer_zip'))
	            ->setCellValue('M2', $quote->f('buyer_city'))
	            ->setCellValue('N2', "")
	            ->setCellValue('O2', $buyer_country);

	      $objPHPExcel->getActiveSheet()->setTitle('Addresses');
	      $objPHPExcel->setActiveSheetIndex(0);
	      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
	      $objWriter->setDelimiter(';');
	      $objWriter->save($filename);

	      $handle_csv = fopen($filename, "r");   
	      $contents_csv = fread($handle_csv, filesize($filename)); 
	      fclose($handle_csv);                               
	      $csvContent   = base64_encode($contents_csv);
	     
	      $env_usr = $config['postgreen_user'];
	      $env_pwd = $config['postgreen_pswd'];
	      $postg_data=$this->db->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	      $panel_usr=$postg_data->f("api");
	      $panel_pswd=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");

	      $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> $in['id'],
	      							'module'		=> 'quote',
	      							'date'			=> time(),
	      							'action'		=> 'ORDER_CREATE'.' - '.$config['postgreen_wsdl'],
	      							'login_user'	=> addslashes($panel_usr),
	      							'content'		=> '',
	      							'error_message'	=> ''
	      			);

	      try{

	      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
	      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
	      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
	      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
	      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
	      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));

	      $soapVr='
	        <order_create_IN>
	          <action type="string">ORDER_CREATE</action> 
	          <attachment_list type="stringlist"></attachment_list>'; 
	      if($in['stationary_id']){
	        $soapVr.='<background type="string">'.$in['stationary_id'].'</background>';
	      }else{
	        $soapVr.='<background type="string"></background>';
	      } 
	      $soapVr.='<content_parameters type="indstringlist"></content_parameters> 
	          <csv type="file" extension="csv"> 
	            <nir:data>'.$csvContent.'</nir:data> 
	          </csv> 
	          <document type="file" extension="pdf"> 
	            <nir:data>'.$decodeContent.'</nir:data> 
	          </document> 
	          <document_name type="string">'.$doc_name.'</document_name> 
	          <get_proof type="string">NO</get_proof> 
	          <identifier type="indstringlist"> 
	            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data> 
	          </identifier> 
	          <insert_list type="indstringlist"></insert_list> 
	          <mailing_type type="string">NONE</mailing_type> 
	          <order_parameters type="indstringlist"></order_parameters> 
	          <page_list type="stringlist"></page_list> 
	          <return_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </return_address> 
	          <sender_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </sender_address> 
	          <service_profile type="indstringlist"> 
	            <nir:data key="service_id">CUSTOM_ENVELOPE</nir:data> 
	            <nir:data key="print_mode">1SIDE</nir:data>
	            <nir:data key="envelop_type">CUSTOM</nir:data>
	            <nir:data key="paper_weight">80g</nir:data>
	            <nir:data key="print_color">COLOR</nir:data>
	            <nir:data key="mail_type">EXPRESS</nir:data>
	            <nir:data key="address_page">EXTRA_PAGE</nir:data>
	            <nir:data key="validation">NO</nir:data>
	          </service_profile> 
	        </order_create_IN>';
	      $params = new \SoapVar($soapVr,XSD_ANYXML);
	      
	      $post_green_data['content'] = addslashes($soapVr);

	      $result = $soap->order_create($params);

	      if($result->action->data[0]->_ == '000'){
	        $this->db->query("UPDATE tblquote_version SET postgreen_id='".$result->order_id->_."' WHERE version_id='".$in['version_id']."' ");
	        $in['quote_id']=$in['id'];
	        $in['sent_date']= date('c');
	        $this->mark_sent($in);
	        $msg_txt =gm('Quote successfully exported');
	      }else{
	        msg::error($result->action->data[1]->_,"error");
	        return true;
	      }

	  } catch (Exception $e) {

	  	$post_green_data['error_message'] = addslashes($e->getMessage());

	  	if(empty($soapVr)){
			$post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
	  	}

        unlink($in['attach_file_name']);
        unlink($filename);
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");
        error_post($post_green_data);
        json_out($in);
    }
	      unlink($in['attach_file_name']);
	      unlink($filename);

	      msg::success($msg_txt,"success");
	      if($in['related']=='view'){        
	        // return true;
	      }else{
	        unset($in['id']);
	        unset($in['quote_id']);
	        unset($in['version_id']); 
	      }
	      return true;
	}

	function postQuoteData(&$in){
	    if($in['old_obj']['no_status']){
	        json_out($in);
	    }else{
	    	try{
		    	global $config;
		      $env_usr = $config['postgreen_user'];
		      $env_pwd = $config['postgreen_pswd'];

		      $postg_data=$this->db->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
		      $panel_usr=$postg_data->f("api");

		      $quote_id = $this->db->field("SELECT quote_id FROM tblquote_version WHERE version_id='".$in['version_id']."' "); 
		      
		      $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> $quote_id,
	      							'module'		=> 'quote',
	      							'date'			=> time(),
	      							'action'		=> 'ORDER_DETAILS'.' - '.$config['postgreen_wsdl'],
	      							'login_user'	=> addslashes($panel_usr),
	      							'content'		=> '',
	      							'error_message'	=> ''
	      			);

		      $panel_pswd=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
		      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
		      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
		      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
		      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
		      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
		      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));
		      
		      $order_id=$this->db->field("SELECT postgreen_id FROM tblquote_version WHERE version_id='".$in['version_id']."' ");

		      $post_green_data['content'] = addslashes('<order_details_IN>
				          <action type="string">ORDER_DETAILS</action>
				          <get_billing>NO</get_billing>
				          <get_document>NO</get_document>
				          <get_proof>NO</get_proof>
				          <identifier type="indstringlist">
				            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
				          </identifier>
				          <order_id type="string">'.$order_id.'</order_id>
				        </order_details_IN>');

		      $params = new \SoapVar('
		        <order_details_IN>
		          <action type="string">ORDER_DETAILS</action>
		          <get_billing>NO</get_billing>
		          <get_document>NO</get_document>
		          <get_proof>NO</get_proof>
		          <identifier type="indstringlist">
		            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
		          </identifier>
		          <order_id type="string">'.$order_id.'</order_id>
		        </order_details_IN>',
		         XSD_ANYXML);

		      $result=$soap->order_details($params);
		}catch(Exception $e){

			$post_green_data['error_message'] = addslashes($e->getMessage());

			if(empty($post_green_data['content'])){
				$post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
		  	}	

			error_post($post_green_data);

	            $post_status=array('status'=>gm('Postgreen service is temporarily unavailable. Please retry later.'));
	          if($in['related']=='minilist'){
	            json_out($post_status);
	          }else if($in['related']=='status'){
	            $in['status']=$post_status['status'];
	            json_out($in);
	          }else{
	            return json_encode($post_status);
	          }
	      }
	      $PGreen_status=array(
	        '100'   => gm('PostGreen Received'),
	        '140'   => gm('PostGreen Waiting for validation'),
	        '-140'  => gm('PostGreen Rejected'),
	        '160'   => gm('PostGreen Saved'),
	        '200'   => gm('PostGreen Confirmed'),
	        '210'   => gm('PostGreen Processing'),
	        '300'   => gm('PostGreen Processed'),
	        '400'   => gm('PostGreen Validated'),
	        '500'   => gm('PostGreen Printed'),
	        '600'   => gm('PostGreen Posted'),
	        '700'   => gm('PostGreen Sent'),
	        '800'   => gm('PostGreen Arrived'),
	        '810'   => gm('PostGreen Opened'),
	        '900'   => gm('PostGreen Canceled')
	      );
	      $post_status=array('status'=>gm('Post status').": ".$PGreen_status[$result->order_info->row->col[3]->data]);
	      if($in['related']=='minilist'){
	        json_out($post_status);
	      }else if($in['related']=='status'){
	        $in['status']=$post_status['status'];
	        json_out($in);
	      }else{
	        return json_encode($post_status);
	      } 
	    }  
	}

	function updateSegment(&$in){
		$this->db->query("UPDATE tblquote SET segment_id='".$in['segment_id']."' WHERE id='".$in['id']."' ");
		if($in['segment_id']){
			$in['segment'] = $this->db->field("SELECT name FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function updateSource(&$in){
		$this->db->query("UPDATE tblquote SET source_id='".$in['source_id']."' WHERE id='".$in['id']."' ");
		if($in['source_id']){
			$in['source'] = $this->db->field("SELECT name FROM tblquote_source WHERE id='".$in['source_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function updateType(&$in){
		$this->db->query("UPDATE tblquote SET type_id='".$in['type_id']."' WHERE id='".$in['id']."' ");
		if($in['type_id']){
			$in['xtype']=$this->db->field("SELECT name FROM tblquote_type WHERE id='".$in['type_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	/**
	 * order update validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function get_article_quantity_price(&$in)
	{
		$price_type=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
    	$is_block_discount=$this->db->query("SELECT pim_articles.block_discount FROM pim_articles
                               WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.block_discount='1'");

	    if($is_block_discount->next()){
		  	$price=$in['price'];
		}else{
			if($price_type==1){
		   		$price=$in['price'];
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices
		    		                     WHERE pim_article_prices.from_q<='".$in['quantity']."'
		    		                     AND  pim_article_prices.article_id='".$in['article_id']."'
		                                 ORDER BY pim_article_prices.from_q DESC
		                                 LIMIT 1
		    		                     ");

			    if(!$price){
			     	$price=$in['price'];
			    }
		    }
		}

		if($in['asString']){
        	return $price;
        }
	  	$in['new_price']=$price;
    	json_out($in);
		return true;
	}

	function updatePdfSettings(&$in){
		$setting_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='QUOTE_PDF_SETTING_".$in['model_name']."' ");
		if(is_null($setting_exist)){
			$this->db->query("INSERT INTO settings SET constant_name='QUOTE_PDF_SETTING_".$in['model_name']."',value='".$in['value']."' ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['value']."' WHERE constant_name='QUOTE_PDF_SETTING_".$in['model_name']."' ");
		}
		if($in['model_name']=='GRAND_TOTAL' && !$in['value']){
			$setting_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='QUOTE_PDF_SETTING_SHOW_VAT_ON_PDF' ");
			if(is_null($setting_exist)){
				$this->db->query("INSERT INTO settings SET constant_name='QUOTE_PDF_SETTING_SHOW_VAT_ON_PDF',value='' ");
			}else{
				$this->db->query("UPDATE settings SET value='' WHERE constant_name='QUOTE_PDF_SETTING_SHOW_VAT_ON_PDF' ");
			}
		}
		json_out($in);
	}

	function get_facq_basket(&$in){
      global $config;

      $app=$this->db->query("SELECT * FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");
      $basketitems =array('items'=>array(), 'lang'=>'');
      $basketitems['lang']= $in['lang'] =='2'? 'FR':'NL';

      if($app->next()){
			if($app->f('active') == 1){

					$facq_id = $app->f('app_id');
					$UserID=$this->db->field("SELECT api FROM apps WHERE type='UserID' AND main_app_id='".$facq_id."' ");
					$Login=$this->db->field("SELECT api FROM apps WHERE type='Login' AND main_app_id='".$facq_id."' ");
					$Password=$this->db->field("SELECT api FROM apps WHERE type='Password' AND main_app_id='".$facq_id."' ");


			      $requestParams =array(
			        'UserID'                => $UserID,
			        'Login'         		=> $Login,
			        'Password'            	=> $Password,
			        'CultureID'           	=> $basketitems['lang']  
			      );

			      $opts = array(
				        'http' => array(
				            'user_agent' => 'PHPSoapClient'
				        ),
				        'ssl' => array(
						    'verify_peer' => false,
						    'verify_peer_name' => false,
						    'allow_self_signed' => true
						    )
				    );
				    $context = stream_context_create($opts);

				   try
						{
					      $soap = new SoapClient($config['facq_wsdl'],array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY, 'stream_context' => $context));
						  $res = $soap->GetBasket(array("request" => $requestParams));
	

						  $code = $res->GetBasketResult->Status->Code;
						  $desc = $res->GetBasketResult->Status->Description;
					    
					 
					      if($code=='0'){
					      	//var_dump( $basketitems, $res->GetBasketResult->ListBasketItem->BasketItem); exit();
						     $basketitems['items']=$res->GetBasketResult->ListBasketItem->BasketItem;
							
					      }elseif($code=='1'){
					      	msg::error(gm('Please provide a valid User Id, Login and password to activate the integration'),"error");
						    return false;
					      }else{
					      	msg::error($desc,"error");
						    return false;
					      }
			           }
			     	catch(SoapFault $fault)
						{
							//var_dump($fault); exit();
								msg::error ( gm('Error'), 'error');					
								return false;
					
						}

				
				json_out($basketitems);
			}
		}
    }

    function update_facq_article(&$in){
      global $config;
      $in['article_id'] = $this->db->field("SELECT article_id FROM pim_articles WHERE supplier_reference='".$in['item_code']."' and facq='1' ");
      $in['vat_id'] = $this->db->field("SELECT vat_id FROM vats WHERE value=21 ");
      if($in['article_id']){
      	//update the article

      	$this->db->query("UPDATE pim_articles SET internal_name       = '".$in['internal_name']."'
								WHERE article_id='".$in['article_id']."' ");

      	$active_languages = $this->db->query("SELECT * FROM pim_lang GROUP BY lang_id");
		while ($active_languages->next()) {
			$name='';
			if($active_languages->f('code')=='fr'){
				$name=$in['nameFR'];
				$this->db->query("UPDATE pim_articles_lang SET name                   = '" . $name . "',
                                                               name2                  = '" . $name . "',
                                                               description			  = '" . $name."'
								WHERE item_id='".$in['article_id']."' AND lang_id  = '" . $active_languages -> f('lang_id') . "' ");
			}elseif($active_languages->f('code')=='du'||$active_languages->f('code')=='nl'){
				$name=$in['nameNL'];
				$this->db->query("UPDATE pim_articles_lang SET name                   = '" . $name . "',
                                                               name2                  = '" . $name . "',
                                                               description			  = '" . $name."'
								WHERE item_id='".$in['article_id']."' AND lang_id  = '" . $active_languages -> f('lang_id') . "' ");
			}
			
		}

         $this->db->query("UPDATE stock_movements SET
                                    article_name 	  			= '".$in['internal_name']."'
								WHERE article_id='".$in['article_id']."' ");

         //update base price
		$this->db->query("SELECT * FROM pim_article_prices WHERE base_price=1 AND article_id='".$in['article_id']."'");
		$in['total_price'] = $in['price'] + $in['price']*21/100;

		if($this->db->f('article_id')) {
			$this->db2->query("UPDATE pim_article_prices SET
							purchase_price 		= '".$in['purchase_price']."'
							WHERE base_price=1  AND  article_id='".$in['article_id']."'");
		 }else{
		 	$this->db2->query("INSERT INTO pim_article_prices SET
							price				= '".$in['price']."',
							total_price			= '".$in['total_price']."',
							purchase_price 		= '".$in['purchase_price']."',
							base_price 			= 1,
							article_id 			='".$in['article_id']."'");
		 }

		 $in['price_type'] =$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		 if(!$in['price_type']){
		 	$in['price_type']=1;
		 }

		if($in['price_type']==1){

				$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");

				$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
		               if($prices->f('price_type')==2){
		                      $base_price=$in['purchase_price'];
		                 }else{
		                      $base_price=$in['price'];
		                 }

		            $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
		            	              WHERE article_id='".$in['article_id']."' AND category_id='".$prices->f('category_id')."'");
		            if($this->db2->move_next()){

			            	 if($this->db2->f('price_type')==2){
			                      $base_price=$in['purchase_price'];
			                 }else{
			                      $base_price=$in['price'];
			                 }

							switch ($this->db2->f('type')) {
								case 1:                  //discount
								if($this->db2->f('price_value_type')==1){  // %
									$total_price = $base_price - round(($this->db2->f('price_value') * $base_price / 100),2);

								}else{ //fix
									$total_price = $base_price - $this->db2->f('price_value');
								}

								break;
								case 2:                 //markup margin
								if($this->db2->f('price_value_type')==1){  // %
									$total_price = $base_price +  round(($this->db2->f('price_value') * $base_price / 100),2);
								}else{ //fix
									$total_price = $base_price + $this->db2->f('price_value');
								}

								break;
								case 3:                 //profit margin
								if($this->db2->f('price_value_type')==1){  // %
									$total_price = $base_price / (1- round($this->db2->f('price_value') / 100,2));
								}else{ //fix
									//
								}
								break;
								case 4:                 //manually set price

									$total_price =$this->db2->f('price_value');
								
				              	break;
									}



		              }else{

		                      switch ($prices->f('type')) {
									case 1:                  //discount
									if($prices->f('price_value_type')==1){  // %
										$total_price = $base_price - round(($prices->f('price_value') * $base_price / 100),2);

									}else{ //fix
										$total_price = $base_price - $prices->f('price_value');
									}

									break;
									case 2:                 //markup margin
									if($prices->f('price_value_type')==1){  // %
										$total_price = $base_price +  round(($prices->f('price_value') * $base_price / 100),2);
									}else{ //fix
										$total_price = $base_price + $prices->f('price_value');
									}
									break;
									case 3:                 //profit margin
									if($prices->f('price_value_type')==1){  // %
										$total_price = $base_price / (1- round($prices->f('price_value') / 100,2));
									}else{ //fix
										//
									}
									break;
									case 4:                 //manually set price
										continue;
									break;
					              }
		          	}

		            $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
								article_id			= '".$in['article_id']."',
								price_category_id	= '".$prices->f('category_id')."',
								price				= '".$total_price."',
								total_price			= '".$total_with_vat."'
						");



				}
			}//price_type 2 nu depinde de purchase price (doar modificarile la purchase price se actualizeaza)

      	insert_message_log('article','{l}Article updated by{endl} '.get_user_name($_SESSION['u_id']). ' {l}by retrieving Facq basket{endl}','article_id',$in['article_id'],false,$_SESSION['u_id']);

      }else{
      	//add the article
      	$in['price_type'] = PRICE_TYPE;
		if($in['price_type']==1){
            $in['use_percent_default']=0;
		}

		$in['supplier_id'] = $this->db->field("SELECT customer_id FROM customers WHERE facq='1' ");
		

      	$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
															item_code  							= '".$in['item_code']."',
															use_percent_default  				= '".$in['use_percent_default']."',
															supplier_reference      			= '".$in['item_code']."',
															vat_id								= '".$in['vat_id']."',
															stock        	    				= '0',											
															sale_unit        					= '1',
															internal_name       				= '".$in['internal_name']."',
															packing          					= '1',
															price_type          				= '".$in['price_type']."',	
															price          						= '".$in['price']."',										
															supplier_id							= '".$in['supplier_id']."',
															supplier_name						= 'FACQ',
															facq 								='1' ");

		$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
		
       $this->db->query("INSERT INTO dispatch_stock SET  article_id= '".$in['article_id']."',
				                                              stock= '0',
				                                              address_id='".$main_address_id."',
				                                              main_address=1");


       $active_languages = $this->db->query("SELECT * FROM pim_lang GROUP BY lang_id");
		while ($active_languages->next()) {
			$name='';
			if($active_languages->f('code')=='fr'){
				$name=$in['nameFR'];
			}elseif($active_languages->f('code')=='du'||$active_languages->f('code')=='nl'){
				$name=$in['nameNL'];
			}
			$in['article_lang_id'] = $this->db->insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '" . $in['article_id'] . "',
		                                                                         lang_id                = '" . $active_languages -> f('lang_id') . "',
		                                                                         name                   = '" . $name . "',
		                                                                         name2                  = '" . $name . "',
		                                                                         description			= '" . $name."' ");
		}

       $in['total_price'] = $in['price'] + $in['price']*21/100;

       $this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".$in['price']."',
										total_price		    = '".$in['total_price']."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".$in['purchase_price']."'
						");


     if($in['price_type']==1){
			$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
				//insert the default price
                 if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                      $base_price=$in['price'];
                 }
				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = $base_price - ($prices->f('price_value') * $base_price/ 100);
							}else{ //fix
								$total_price = $base_price - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = $base_price + ($prices->f('price_value') *$base_price/ 100);
							}else{ //fix
								$total_price = $base_price + $prices->f('price_value');
							}

					break;
				}

					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");



			}
		}else{   //quantity discounts

			$prices = $this->db->query("SELECT * FROM article_price_volume_discount");
				while ($prices->next()){
                                       
                     $total_price    = $in['price'] - (($prices->f('percent')*$in['price'])/100);
                
		             $vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		             $total_with_vat = $total_price+($total_price*$vat_value/100);

                      $this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");
				}
		}
		insert_message_log('article','{l}Article added by{endl} '.get_user_name($_SESSION['u_id']). '{l}by retrieving Facq basket{endl}','article_id',$in['article_id'],false,$_SESSION['u_id']);



      }
      json_out($in);

    }

    function PVCheck(&$in){
      global $config;

      $app=$this->db->query("SELECT * FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");
    
      if($app->next()){
			if($app->f('active') == 1){


					$facq_id = $app->f('app_id');
					$UserID=$this->db->field("SELECT api FROM apps WHERE type='UserID' AND main_app_id='".$facq_id."' ");
					$Login=$this->db->field("SELECT api FROM apps WHERE type='Login' AND main_app_id='".$facq_id."' ");
					$Password=$this->db->field("SELECT api FROM apps WHERE type='Password' AND main_app_id='".$facq_id."' ");

					$facq_lines=$this->db->query("SELECT * FROM tblquote_line WHERE quote_id='".$in['quote_id']."' AND version_id='".$in['version_id']."' AND facq='1' ");

					$opts = array(
				        'http' => array(
				            'user_agent' => 'PHPSoapClient'
				        ),
				        'ssl' => array(
						    'verify_peer' => false,
						    'verify_peer_name' => false,
						    'allow_self_signed' => true
						    )
				    );
				    $context = stream_context_create($opts);

					while($facq_lines->next()){
						if($facq_lines->f('article_code') && $facq_lines->f('article_code')!='000000' && $facq_lines->f('article_code')!='990000'){
							
							$requestParams =array(
						        'UserID'                	=> $UserID,
						        'Login'         			=> $Login,
						        'Password'            		=> $Password,
						        'CultureID'           		=> 'FR',   
						        'ListPVCheckRequestProduct' =>array('PVCheckRequestProduct'=>array('ProductID'=>$facq_lines->f('article_code')))
						      );

						   try
								{
							      $soap = new SoapClient($config['facq_wsdl'],array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY, 'stream_context' => $context));
								  $res = $soap->PVCheck(array("request" => $requestParams));
			

								  $status = $res->PVCheckResult->Status;
								  $product = $res->PVCheckResult->ListProduct->Product;
								  $code = $product->Status->Code;
								  $purchase_price = $product->NetPriceTaxExcluded;
								  $supplier_reference = $product->ProductID;
							 
							      if($code=='0'){
							      	$article_id= $this->db->field("SELECT article_id FROM pim_articles WHERE supplier_reference='".$supplier_reference."' and facq='1' ");
							      	if($article_id){
							      		//var_dump($article_id);
							      		$this->update_facq_purchase_price($article_id, $purchase_price);
								     	
								     	$this->db->query("UPDATE tblquote_line SET purchase_price = '".$purchase_price."' WHERE article_id='".$article_id."' AND quote_id='".$in['quote_id']."' AND version_id='".$in['version_id']."' AND facq='1'");
								     	$this->recalculate_margin($in);
								     	insert_message_log($this->pag,'{l}Margins were recalculated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id']);
								     	//json_out($margin);
								
							      	}
								     
							      }else{
							      	msg::error($product->Status->Description,"error");
								    return false;
							      }
					           }
					     	catch(SoapFault $fault)
								{
									//var_dump($fault); exit();
										msg::error ( gm('Error'), 'error');					
										return false;
							
								}

						}//end if
						

					}//end while


						
			}
		}
		msg::success ( gm("Margins were recalculated."),'success');
		json_out($in);
    }




	function getArticlePrice($in){
    $cat_id = $in['cat_id'];
    $article = $this->db->query("SELECT article_category_id, block_discount,price_type FROM pim_articles WHERE article_id='".$in['article_id']."' ");
    if($article->f('price_type')==1){

        $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");
          $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$in['article_id']."' and category_id='".$cat_id."' ");

          if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
              $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$in['article_id']."'");
          }else{
              $price_value=$price_value_custom_fam;

             //we have to apply to the base price the category spec
          $cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
            $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
            $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

            if($cat_price_type==2){
                  $article_base_price=get_article_calc_price($in['article_id'],3);
              }else{
                  $article_base_price=get_article_calc_price($in['article_id'],1);
              }

            switch ($cat_type) {
          case 1:                  //discount
            if($price_value_type==1){  // %
              $price = $article_base_price - $price_value * $article_base_price / 100;
            }else{ //fix
              $price = $article_base_price - $price_value;
            }
            break;
          case 2:                 //profit margin
            if($price_value_type==1){  // %
              $price = $article_base_price + $price_value * $article_base_price / 100;
            }else{ //fix
              $price =$article_base_price + $price_value;
            }
            break;
        }
          }

        if(!$price || $article->f('block_discount')==1 ){
            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$in['article_id']."' AND base_price=1");
          }
      }else{
        $price = $in['price'];
        // return $this->get_article_quantity_price($in);
      }

      $start= mktime(0, 0, 0);
      $end= mktime(23, 59, 59);
      $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$in['article_id']."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
      if($promo_price->move_next()){
        if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

          }else{
              $price=$promo_price->f('price');
          }
      }
    if($in['customer_id']){
        $customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
        if($customer_custom_article_price->move_next()){
              $price = $customer_custom_article_price->f('price');
          }
      }
      return $price;
  }

      function update_facq_purchase_price($article_id, $purchase_price){
      global $config;
      $in['article_id'] = $article_id;
      $in['purchase_price'] = $purchase_price;

      $in['vat_id'] = $this->db->field("SELECT vat_id FROM vats WHERE value=21 ");
      if($in['article_id']){

         //update base price
		$this->db->query("SELECT * FROM pim_article_prices WHERE base_price=1 AND article_id='".$in['article_id']."'");
		$in['price'] =$this->db->f('price');
		$in['total_price'] = $this->db->f('price') + $this->db->f('price')*21/100;

		if($this->db->f('article_id')) {
			$this->db2->query("UPDATE pim_article_prices SET
							purchase_price 		= '".$in['purchase_price']."'
							WHERE base_price=1  AND  article_id='".$in['article_id']."'");
		 }else{
		 	$this->db2->query("INSERT INTO pim_article_prices SET
							price				= '".$in['price']."',
							total_price			= '".$in['total_price']."',
							purchase_price 		= '".$in['purchase_price']."',
							base_price 			= 1,
							article_id 			='".$in['article_id']."'");
		 }

		 $in['price_type'] =$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		 if(!$in['price_type']){
		 	$in['price_type']=1;
		 }

		if($in['price_type']==1){

			$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");

				$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
		               if($prices->f('price_type')==2){
		                      $base_price=$in['purchase_price'];
		                 }else{
		                      $base_price=$in['price'];
		                 }

		            $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
		            	              WHERE article_id='".$in['article_id']."' AND category_id='".$prices->f('category_id')."'");
		            if($this->db2->move_next()){

			            	 if($this->db2->f('price_type')==2){
			                      $base_price=$in['purchase_price'];
			                 }else{
			                      $base_price=$in['price'];
			                 }

							switch ($this->db2->f('type')) {
								case 1:                  //discount
								if($this->db2->f('price_value_type')==1){  // %
									$total_price = $base_price - round(($this->db2->f('price_value') * $base_price / 100),2);

								}else{ //fix
									$total_price = $base_price - $this->db2->f('price_value');
								}

								break;
								case 2:                 //markup margin
								if($this->db2->f('price_value_type')==1){  // %
									$total_price = $base_price +  round(($this->db2->f('price_value') * $base_price / 100),2);
								}else{ //fix
									$total_price = $base_price + $this->db2->f('price_value');
								}

								break;
								case 3:                 //profit margin
								if($this->db2->f('price_value_type')==1){  // %
									$total_price = $base_price / (1- round($this->db2->f('price_value') / 100,2));
								}else{ //fix
									//
								}
								break;
								case 4:                 //manually set price

									$total_price =$this->db2->f('price_value');
								
				              	break;
									}



		              }else{

		                      switch ($prices->f('type')) {
									case 1:                  //discount
									if($prices->f('price_value_type')==1){  // %
										$total_price = $base_price - round(($prices->f('price_value') * $base_price / 100),2);

									}else{ //fix
										$total_price = $base_price - $prices->f('price_value');
									}

									break;
									case 2:                 //markup margin
									if($prices->f('price_value_type')==1){  // %
										$total_price = $base_price +  round(($prices->f('price_value') * $base_price / 100),2);
									}else{ //fix
										$total_price = $base_price + $prices->f('price_value');
									}
									break;
									case 3:                 //profit margin
									if($prices->f('price_value_type')==1){  // %
										$total_price = $base_price / (1- round($prices->f('price_value') / 100,2));
									}else{ //fix
										//
									}
									break;
									case 4:                 //manually set price
										continue;
									break;
					              }
		          	}

		            $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
								article_id			= '".$in['article_id']."',
								price_category_id	= '".$prices->f('category_id')."',
								price				= '".$total_price."',
								total_price			= '".$total_with_vat."'
						");



				}
			}//price_type 2 nu depinde de purchase price (doar modificarile la purchase price se actualizeaza)

      	
      }
      insert_message_log('article','{l}Purchase price updated by{endl} '.get_user_name($_SESSION['u_id']). ' {l}by recalculating margins{endl}','article_id',$in['article_id'],false,$_SESSION['u_id']);
      return true;

    }


    function recalculate_margin(&$in){
      	global $config;

     	$use_package=$this->db->field("SELECT use_package FROM tblquote WHERE id='".$in['quote_id']."'");
		$use_sale_unit=$this->db->field("SELECT use_sale_unit FROM tblquote WHERE id='".$in['quote_id']."'");
		$apply_discount=$this->db->field("SELECT apply_discount FROM tblquote WHERE id='".$in['quote_id']."'");
		$discount=$this->db->field("SELECT discount FROM tblquote WHERE id='".$in['quote_id']."'");
		$currency_type=$this->db->field("SELECT currency_type FROM tblquote WHERE id='".$in['quote_id']."'");

		$amount = 0;
		$grand_total =0;
		$total_vat =0;
		$amount_purchase_price = 0;
		$amount_data = $this->db->query("SELECT * FROM tblquote_line WHERE quote_id='".$in['quote_id']."' AND version_id='".$in['version_id']."' AND show_block_total='0' ");
		while($amount_data->next()){

			$package = $amount_data->f('package');
			$sale_unit = $amount_data->f('sale_unit');

			if(!$use_package){
				$package=1;
			}
			if(!$use_sale_unit){
				$sale_unit=1;
			}


			$line_discount = $amount_data->f('line_discount');
			if($apply_discount == 0 || $apply_discount == 2){
				$line_discount = 0;
			}
			$price = ($amount_data->f('price')-($amount_data->f('price')*$line_discount/100));
			if($apply_discount > 1){
				$price = $price - $price*$discount /100;
			}

			$grand_total += ($amount_data->f('price')-($amount_data->f('price')*$line_discount/100))*$amount_data->f('quantity')*$package/$sale_unit;

			$total_vat += $price*($amount_data->f('quantity')*$package/$sale_unit)* $amount_data->f('vat')/100;

			$amount += $amount_data->f('amount');

				$purchase_price = $amount_data->f('purchase_price');
				$line_purchase_price = $purchase_price*$amount_data->f('quantity')*$package/$sale_unit;
				$amount_purchase_price+=$line_purchase_price;
		}

		if($discount && $apply_discount > 1){
			$amount = $amount - ($amount*$discount/100);
			$grand_total = $grand_total - ($grand_total*$discount/100);
		}

		$grand_total_vat = $grand_total +  $total_vat;

		$amount_margin = $amount-$amount_purchase_price;
		$margin_percent=0;
		if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
			if($amount_purchase_price){
				$margin_percent = $amount_margin/$amount_purchase_price *100;
			}elseif($amount_margin){
				$margin_percent = 100;
			}
		}else{
			if($amount){
				$margin_percent = $amount_margin/$amount *100;
			}elseif($amount_margin){
				$margin_percent = 100;
			}
		}


		$in['amount_margin']=$amount_margin;
		$in['total_margin']= place_currency(display_number($amount_margin),get_commission_type_list($currency_type));
		$in['margin_percent_val']=$margin_percent;
		$in['margin_percent']	= display_number($margin_percent).'%';
		
			$this->db->query("UPDATE tblquote SET margin='".$amount_margin."', margin_percent='".$margin_percent."' WHERE id='".$in['quote_id']."'");

      return true;

    }


    function delete_facq_basket(&$in){
      global $config;

      $app=$this->db->query("SELECT * FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");
    
      if($app->next()){
			if($app->f('active') == 1){


					$facq_id = $app->f('app_id');
					$UserID=$this->db->field("SELECT api FROM apps WHERE type='UserID' AND main_app_id='".$facq_id."' ");
					$Login=$this->db->field("SELECT api FROM apps WHERE type='Login' AND main_app_id='".$facq_id."' ");
					$Password=$this->db->field("SELECT api FROM apps WHERE type='Password' AND main_app_id='".$facq_id."' ");

							
					$requestParams =array(
				        'UserID'                	=> $UserID,
				        'Login'         			=> $Login,
				        'Password'            		=> $Password,
				        'CultureID'           		=> 'FR',   
				        
				      );

					$opts = array(
				        'http' => array(
				            'user_agent' => 'PHPSoapClient'
				        ),
				        'ssl' => array(
						    'verify_peer' => false,
						    'verify_peer_name' => false,
						    'allow_self_signed' => true
						    )
				    );
				    $context = stream_context_create($opts);

				   try
						{
					      $soap = new SoapClient($config['facq_wsdl'],array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY, 'stream_context' => $context));
						  $res = $soap->ClearBasket(array("request" => $requestParams));
							     
					      
			           }
			     	catch(SoapFault $fault)
						{
							//var_dump($fault); exit();
								msg::error ( gm('Error').'-'.$fault->faultstring, 'error'); 			
								return false;
					
						}


						
			}
		}
		
		return true;
    }




}

?>