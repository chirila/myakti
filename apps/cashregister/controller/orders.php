<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class CashOrders extends Controller{

		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}

		public function get_tickets(){
			global $config;
			$in=$this->in;
			$result=array('query'=>array(),'data_incoming'=>array());

			/*$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
    		$tact_node=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='node' ");

    		$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
    		$time_filter='';
    		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_TICKET_FILTER' ");
	        if($const){
	            $time_filter='&filter=updated_at[gt]='.date('Y-m-d',(int)$const-86400).'T'.date('H:i:s',(int)$const-86400).'.0Z';
	        }
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/order/tickets?node_id='.$tact_node.$time_filter);
        	
        	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
        	
        	$data=json_decode($put);
        	foreach($data as $key=>$value){
        		$id_e=$this->db->field("SELECT id FROM cash_tickets WHERE ticket_id='".$value->_id."' ");
        		if(!$id_e){
        			array_push($result['data_incoming'],$value);
        		}		
        	}*/

        	$l_r = ROW_PER_PAGE;
			$order_by_array = array('date','number', 'customer', 'amount_net', 'amount_taxfree', 'serial_number');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY cash_tickets.date DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
			
			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true' || $in['desc'] == '1'){
						$order = " DESC ";
					}
					if($in['order_by']=='customer' || $in['order_by']=='serial_number'){
						$order_by='';
						$filter_limit =' ';
					}else{
						$order_by =" ORDER BY ".$in['order_by']." ".$order;	
						 $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
					}
				}
			}
			if(!empty($in['search'])){
				$filter .= " AND ( cash_tickets.number LIKE '%".$in['search']."%' OR cash_customers.first_name LIKE '%".$in['search']."%' OR cash_customers.last_name LIKE '%".$in['search']."%') ";
			}
			if(isset($in['start_date']) && !isset($in['end_date'])){
				$start_d=mktime(0,0,0,date('n',strtotime($in['start_date'])),date('j',strtotime($in['start_date'])),date('Y',strtotime($in['start_date'])));
				$filter.=" AND cash_tickets.date>='".$start_d."' ";
			}else if(!isset($in['start_date']) && isset($in['end_date'])){
				$end_d=mktime(23,59,59,date('n',strtotime($in['end_date'])),date('j',strtotime($in['end_date'])),date('Y',strtotime($in['end_date'])));
				$filter.=" AND cash_tickets.date<='".$end_d."' ";
			}else if(isset($in['start_date']) && isset($in['end_date'])){
				$start_d=mktime(0,0,0,date('n',strtotime($in['start_date'])),date('j',strtotime($in['start_date'])),date('Y',strtotime($in['start_date'])));
				$end_d=mktime(23,59,59,date('n',strtotime($in['end_date'])),date('j',strtotime($in['end_date'])),date('Y',strtotime($in['end_date'])));
				$filter.=" AND cash_tickets.date BETWEEN '".$start_d."' AND '".$end_d."' ";
			}
			if($in['operation']=='1'){
				$filter.=" AND cash_tickets.operation_type='sale' ";
			}else if($in['operation']=='2'){
				$filter.=" AND cash_tickets.invoice_id>'0' && tblinvoice.f_archived='0' ";
			}else if($in['operation']=='3'){
				$filter.=" AND cash_tickets.invoice_id='0' ";
			}
			if($in['payment']=='1'){
				$filter.=" AND cash_pay_method.type='cash' ";
			}else if($in['payment']=='2'){
				$filter.=" AND cash_pay_method.type='check' ";
			}else if($in['payment']=='3'){
				$filter.=" AND cash_pay_method.type='credit_card' ";
			}else if($in['payment']=='4'){
				$filter.=" AND cash_pay_method.type='credit_note' ";
			}
			$max_rows = $this->db->field("SELECT COUNT(cash_tickets.ticket_id) FROM cash_tickets
        			LEFT JOIN cash_customers ON cash_tickets.customer_id=cash_customers.customer_id
        			LEFT JOIN cash_tickets_pay ON cash_tickets.ticket_id=cash_tickets_pay.ticket_id
        			LEFT JOIN cash_pay_method ON cash_tickets_pay.payment_id=cash_pay_method.payment_id
        			LEFT JOIN tblinvoice ON cash_tickets.invoice_id=tblinvoice.id $filter ");


        	$orders_data=$this->db->query("SELECT cash_tickets.* FROM cash_tickets
        			LEFT JOIN cash_customers ON cash_tickets.customer_id=cash_customers.customer_id
        			LEFT JOIN cash_tickets_pay ON cash_tickets.ticket_id=cash_tickets_pay.ticket_id
        			LEFT JOIN cash_pay_method ON cash_tickets_pay.payment_id=cash_pay_method.payment_id
        			LEFT JOIN tblinvoice ON cash_tickets.invoice_id=tblinvoice.id
        			 ".$filter.$order_by.$filter_limit);
			$j=0;
			while($orders_data->next()){
				$articles_a=array();
				$articles=$this->db->query("SELECT cash_tickets_articles.* FROM cash_tickets_articles
										WHERE cash_tickets_articles.ticket_id='".$orders_data->f('ticket_id')."' ");
				$article_synced=0;
				$articles_nr=0;
				while($articles->next()){
					$art = $this->db->query("SELECT pim_articles.item_code,pim_articles.internal_name,pim_articles.article_id FROM  pim_articles 
									WHERE pim_articles.cash_article_id='".$articles->f('article_id')."' ");
					$article_synced=$articles->f('article_id') ? $article_synced+1 : $article_synced;
					$temp_articles=array(
						'code'			=> $art->f('item_code'),
						'name'		 	=> $art->f('internal_name')?$art->f('internal_name'):$articles->f('description'),
						'quantity'		=> $articles->f('quantity'),
						'amount'		=> display_number($articles->f('tax_free_price')),
						'amount_vat'	=> display_number($articles->f('full_price')),
						'article_id'	=> $art->f('article_id'),
						'vat'			=> display_number($articles->f('vat'))
						);
					array_push($articles_a, $temp_articles);
					$articles_nr++;
				}

				$pay_ms=array();
				$pay_m=$this->db->query("SELECT cash_pay_method.* FROM cash_tickets_pay
										INNER JOIN cash_pay_method ON cash_tickets_pay.payment_id=cash_pay_method.payment_id
										WHERE cash_tickets_pay.ticket_id='".$orders_data->f('ticket_id')."' ");
				while($pay_m->next()){
					switch($pay_m->f('type')){
						case 'cash';
							$p_type=gm('Cash');
							break;
						case 'check':
							$p_type=gm('Check');
							break;
						case 'credit_card':
							$p_type=gm('Credit Card');
							break;
						case 'credit_note':
							$p_type=gm('Credit Note');
							break;
						default:
							$p_type='';
					}
					$temp_pay=array(
							'name' 		=> $pay_m->f('name'),
							'type'		=> $p_type
						);
					array_push($pay_ms, $temp_pay);
				}
				$registered=false;
				$customer_name='';
				if($orders_data->f('customer_id')){
					$customer=$this->db->query("SELECT name,first_name,last_name,our,created_contact,created_customer FROM cash_customers WHERE customer_id='".$orders_data->f('customer_id')."' ");
					if($customer->next()){
						if($customer->f('name')){
							$customer_name=$customer->f('name');
						}else if($customer->f('last_name') || $customer->f('first_name')){
							$customer_name=$customer->f('last_name').' '.$customer->f('first_name');
						}
						if($customer->f('our')){
							$c_id=$this->db->field("SELECT customers.customer_id FROM customer_contacts
								INNER JOIN customers ON customer_contacts.customer_id=customers.customer_id
								WHERE customer_contacts.cash_contact_id='".$orders_data->f('customer_id')."' AND customers.active='1' ");
							if($c_id){
								$registered=true;
							}
						}else if(!$customer->f('our') && $customer->f('created_contact')){
							$c_id=$this->db->field("SELECT customers.customer_id FROM customer_contacts
								INNER JOIN customers ON customer_contacts.customer_id=customers.customer_id
								WHERE customer_contacts.contact_id='".$customer->f('created_contact')."' AND customers.active='1' ");
							if($c_id){
								$registered=true;
							}
						}else if(!$customer->f('our') && $customer->f('created_customer')){
							$c_id=$this->db->field("SELECT customer_id FROM customers				WHERE customer_id='".$customer->f('created_customer')."' AND active='1' ");
							if($c_id){
								$registered=true;
							}
						}
					}
				}
				$invoice_ok=false;
				$invoice_done=false;
				$invoice_sn='';
				if($orders_data->f('invoice_id')){
					$invoice_data=$this->db->query("SELECT serial_number FROM tblinvoice WHERE id='".$orders_data->f('invoice_id')."' AND f_archived='0' ");
					if($invoice_data->next()){
						if(DRAFT_INVOICE_NO_NUMBER){
							$invoice_sn=gm('Invoice');
						}else{
							$invoice_sn=$invoice_data->f('serial_number');
						}	
						$invoice_done=true;
					}
				}
				if($orders_data->f('customer_id') && $registered && !$invoice_done){
					//if(!empty($articles_a) && $articles_nr == $article_synced){
					if(!empty($articles_a)){
						$invoice_ok=true;
					
					}
				}
				$line=array(
						'id'			=> $orders_data->f('id'),
						'ticket_id'		=> $orders_data->f('ticket_id'),
						'amount_vat'	=> display_number($orders_data->f('amount_net')),
						'amount'		=> display_number($orders_data->f('amount_taxfree')),
						'operation'		=> gm($orders_data->f('operation_type')),
						'articles'		=> $articles_a,
						'pay_method' 	=> $pay_ms,
						'date'			=> date(ACCOUNT_DATE_FORMAT,$orders_data->f('date')),
						'customer'		=> $customer_name,
						'no_customer'	=> $orders_data->f('customer_id') ? false : true,
						'registered'	=> $registered,
						'invoice_ok'	=> $invoice_ok,
						'serial_number' => $invoice_sn,
						'invoice_id'	=> $orders_data->f('invoice_id'),
						'invoice_done'	=> $invoice_done,
						'number'		=> $orders_data->f('number'),
					);
				array_push($result['query'], $line);
			}

			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=1;
			$result['do_next']='cashregister--cashregister-saveTicket';

			if($in['order_by']=='customer' || $in['order_by']=='serial_number' ){

			    if($order ==' ASC '){
			       $exo = array_sort($result['query'], $in['order_by'], SORT_ASC);    
			    }else{
			       $exo = array_sort($result['query'], $in['order_by'], SORT_DESC);
			    }
			    $exo = array_slice( $exo, $offset*$l_r, $l_r);
			    $result['query']=array();
			       foreach ($exo as $key => $value) {
			           array_push($result['query'], $value);
			       }
			    
			}
			$this->out = $result;
		}

		public function get_incoming_tickets(){
			global $config;
			$in=$this->in;
			$result=array('data_incoming'=>array());
			ini_set('memory_limit','2G');
			
			$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
    		$tact_node=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='node' ");

    		$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
    		$time_filter='&filter=status=close';
    		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_TICKET_FILTER' ");
	        if($const){
	        	$const = $const - 7*24*60*60;
	            $time_filter.='%26updated_at[gt]='.date('Y-m-d',(int)$const-86400).'T'.date('H:i:s',(int)$const-86400).'.0Z';
	        }
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/order/tickets?node_id='.$tact_node.$time_filter);
        	
        	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
        	
        	$data=json_decode($put);
        	foreach($data as $key=>$value){
        		$id_e=$this->db->field("SELECT id FROM cash_tickets WHERE ticket_id='".$value->_id."' ");
        		if(!$id_e){
        			array_push($result['data_incoming'],$value);
        		}		
        	}
        	$this->out = $result;
        }  

		public function get_pay_method(){
			global $config;
			$in=$this->in;
			$result=array('query'=>array(),'data_incoming'=>array());

			$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
    		$comp_id=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='company' ");

    		$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/order/payment_methods?company_id='.$comp_id);

        	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
        	
        	$data=json_decode($put);
        	foreach($data as $key=>$value){
        		array_push($result['data_incoming'],$value);
        	}

        	$l_r = ROW_PER_PAGE;
			$order_by_array = array('id', 'name', 'type');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY id DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true' || $in['desc'] == '1'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;	
				}
			}

			$orders_data=$this->db->query("SELECT * FROM cash_pay_method ".$filter.$order_by);
			$max_rows=$orders_data->records_count();
			$orders_data->move_to($offset*$l_r);
			$j=0;
			while($orders_data->next() && $j<$l_r){
				$line=array(
						'id'			=> $orders_data->f('id'),
						'payment_id'	=> $orders_data->f('payment_id'),
						'name'			=> $orders_data->f('name'),
						'type'			=> $orders_data->f('type') 
					);
				array_push($result['query'], $line);
			}

			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=2;
			$result['do_next']='cashregister--cashregister-savePay';
			$this->out = $result;
		}

		public function get_cashbooks(){
			global $config;
			$in=$this->in;
			$result=array('query'=>array(),'data_incoming'=>array());

			$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
    		$shop_id=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='shop' ");

    		$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/order/cashbooks?shop_id='.$shop_id);

        	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
        	
        	$data=json_decode($put);
        	foreach($data as $key=>$value){
        		array_push($result['data_incoming'],$value);
        	}

        	$l_r = ROW_PER_PAGE;
			$order_by_array = array('id');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY date DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;	
				}
			}

			$orders_data=$this->db->query("SELECT * FROM cash_books ".$filter.$order_by);
			$max_rows=$orders_data->records_count();
			$orders_data->move_to($offset*$l_r);
			$j=0;
			while($orders_data->next() && $j<$l_r){
				$line=array(
						'id'			=> $orders_data->f('id'),
						'cashbook_id'	=> $orders_data->f('cashbook_id'),
						'seller_name'	=> $orders_data->f('seller_name'),
						'date'			=> date(ACCOUNT_DATE_FORMAT,$orders_data->f('date')),
						'start_amount'	=> display_number($orders_data->f('start_amount')),
						'end_amount'	=> display_number($orders_data->f('end_amount')),
						'total'			=> display_number($orders_data->f('total')),
					);
				array_push($result['query'], $line);
			}

			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=3;
			$result['do_next']='cashregister--cashregister-saveCashbook';
			$this->out = $result;
		}

	}

	$cash_data = new CashOrders($in,$db);

		if($in['xget']){
		    $fname = 'get_'.$in['xget'];
		    $cash_data->output($cash_data->$fname($in));
		}

		$cash_data->get_tickets();
		$cash_data->output();
?>