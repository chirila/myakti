<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class CashCustomer extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}

		public function get_customers(){
			global $config;
			$in=$this->in;
			$result=array('query'=>array(),'data_incoming'=>array());
			$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
			$comp_id=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='company' ");

    		$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
    		$time_filter='';
    		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_CUSTOMER_FILTER' ");
	        if($const){
	            $time_filter='&filter=updated_at[gt]='.date('Y-m-d',(int)$const-86400).'T'.date('H:i:s',(int)$const-86400).'.0Z';
	        }
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/customer/customers?company_id='.$comp_id.$time_filter);

	        $put = curl_exec($ch);
		    $info = curl_getinfo($ch);

	        $data=json_decode($put);
	        foreach($data as $key=>$value){
	        	//$id_e=$this->db->field("SELECT contact_id FROM customer_contacts WHERE cash_contact_id='".$value->_id."' ");
	        	$id2_e=$this->db->field("SELECT id FROM cash_customers WHERE customer_id='".$value->_id."' AND our='1' ");
	        	if(!$id2_e){
	        		array_push($result['data_incoming'],$value);
	        	}   	
	        }

	        $l_r = ROW_PER_PAGE;
			$order_by_array = array('id', 'last_name', 'first_name', 'company', 'city', 'country_code');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY id DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true' || $in['desc'] == '1'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;	
				}
			}

			$customer_data=$this->db->query("SELECT cash_customers.*, cash_company.name as company FROM cash_customers 
											LEFT JOIN cash_company ON cash_company.company_id = cash_customers.company_id
											".$filter.$order_by);
			$max_rows=$customer_data->records_count();
			$customer_data->move_to($offset*$l_r);
			$j=0;
			while($customer_data->next() && $j<$l_r){
				$line=array(
						'id'				=> $customer_data->f('id'),
						'customer_id'		=> $customer_data->f('customer_id'),
						//'company'			=> $this->db->field("SELECT name FROM cash_company WHERE company_id='".$customer_data->f('company_id')."' "),
						'company'			=> $customer_data->f('company'),
						'name'				=> $customer_data->f('name'),
						'email'				=> $customer_data->f('email'),
						'phone'				=> $customer_data->f('phone'),
						'country'			=> $this->db->field("SELECT name FROM country WHERE code='".$customer_data->f('country_code')."' "),
						'first_name'		=> $customer_data->f('first_name'),
						'last_name'			=> $customer_data->f('last_name'),
						'address'			=> $customer_data->f('address'),
						'state'				=> $customer_data->f('state'),
						'city'				=> $customer_data->f('city'),  							
						'zip'				=> $customer_data->f('zip'),
						'place_id'			=> $customer_data->f('place_id'),
						'image'				=> $customer_data->f('image'),
						'note'				=> $customer_data->f('note'),
						'our'				=> $customer_data->f('our') ? true : false,
						'country_id'		=> $this->db->field("SELECT country_id FROM country WHERE code='".$customer_data->f('country_code')."' "),
						'imported'			=> (!$customer_data->f('our') && ($customer_data->f('created_customer') || $customer_data->f('created_contact'))) ? true : false,
					);
				array_push($result['query'], $line);
			}

			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=1;
			$result['do_next']='cashregister--cashregister-saveCustomer';
			$result['country_dd']=build_country_list();
			$result['language_dd']=build_language_dd(0);
			$result['gender_dd']= gender_dd();
			$result['title_dd']= build_contact_title_type_dd(0);
			$result['customer_dd']=$this->get_buyers($in);
			$this->out = $result;

		}

		public function get_contacts(){
			$in=$this->in;
			$output=array('list'=>array());
			$l_r = ROW_PER_PAGE;
			$order_by_array = array('lastname','firstname','cus_name','function');
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}
			$filter = " customer_contacts.front_register = 0 ";
			if(!$in['customer_id']){
				$filter.=" AND customer_contacts.cash_contact_id='' ";
			}else{
				$filter.=" AND customer_contacts.cash_contact_id='".$in['customer_id']."' ";
			}
			if(empty($in['archived'])){
				$filter.= " AND customer_contacts.active=1";
			}else{
				$in['archived'] = 1;
				$filter.= " AND customer_contacts.active=0";
				$arguments.="&archived=".$in['archived'];
				$arguments_a = "&archived=".$in['archived'];
			}

			$order_by = " ORDER BY lastname ";
			$escape_chars = array('(','?','*','+',')');
			if(!empty($in['search'])){
				$filter_x = '';
				// $search_arr = str_split($in['search']);
				$search_arr = str_split(stripslashes($in['search']));
				foreach ($search_arr as $value) {
					if(in_array($value, $escape_chars)){
						$value = '\\'.$value;
					}
					$filter_x .= $value.".?";
				}

				$search_words = explode(' ', $in['search']);
				foreach ($search_words as $word_pos => $searched_word) {
					if(trim($searched_word)){
						
						if($word_pos==0){
							$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
										OR lastname LIKE '%".$searched_word."%'
										OR company_name LIKE '%".$searched_word."%'
										OR customer_contactsIds.phone LIKE '%".$searched_word."%'
										OR cell LIKE '%".$searched_word."%'
										OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
						}else{
							$filter .= " AND (customer_contacts.firstname LIKE '%".$searched_word."%'
										OR lastname LIKE '%".$searched_word."%'
										OR company_name LIKE '%".$searched_word."%'
										OR customer_contactsIds.phone LIKE '%".$searched_word."%'
										OR cell LIKE '%".$searched_word."%'
										OR customer_contactsIds.email LIKE '%".$searched_word."%' )";
						}

					}
				}

				$arguments_s.="&search=".$in['search'];
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					if($in['order_by']=='cus_name'){
						$in['order_by']='customers.name';
					}elseif($in['order_by']=='function'){
						$in['order_by']='customer_contactsIds.position';
					}else{

					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;		
				}
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
				$max_rows =  $this->db->field("SELECT COUNT( contact_id ) AS max_contact_rows 
										FROM customer_contacts
										LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id WHERE ".$filter );
				$c = $this->db->query("SELECT customer_contacts.*, customers.customer_id
							FROM customer_contacts
							LEFT JOIN customers	ON customer_contacts.customer_id = customers.customer_id
							WHERE ".$filter." GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();

			}else{
				$max_rows =  $this->db->field("SELECT count(customer_contacts.contact_id) FROM customer_contacts 
							INNER JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id 
							INNER JOIN customers ON customer_contactsIds.customer_id = customers.customer_id AND `primary`=1 
							WHERE ".$filter );
				$c = $this->db->query("SELECT customer_contacts.indiv_created,customers.name as comp_name,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,
							customer_contacts.position_n,customer_contacts.phone,customer_contacts.cell,customer_contactsIds.email,customer_contacts.fax,customer_contacts.contact_id,
							customer_contactsIds.e_title,customer_contactsIds.title,customer_contactsIds.fax,customer_contactsIds.primary,customer_contactsIds.s_email,
							customer_contactsIds.department,customer_contactsIds.position,customer_contacts.cash_contact_id FROM customer_contacts
							INNER JOIN customer_contactsIds ON customer_contactsIds.contact_id = customer_contacts.contact_id AND customer_contactsIds.customer_id!='0'
							INNER JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
							WHERE ".$filter."  GROUP BY customer_contacts.contact_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
			}
			$output['max_rows'] =  $max_rows;
			foreach ($c as $key => $value) {
				$function = $this->db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value['position']."'");
				$addr=array();
				$bil_address=$this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$value['customer_id']."' AND billing='1' ");
				if($bil_address->next()){
					$addr['country_code']=$bil_address->f('country_id') ? get_country_code($bil_address->f('country_id')) : '';
					$addr['address']=$bil_address->f('address');
					$addr['country']=$bil_address->f('country_id') ? get_country_name($bil_address->f('country_id')) :'';
					$addr['state']=$bil_address->f('state_id') ? get_country_name($bil_address->f('state_id')) : '';
					$addr['city']=$bil_address->f('city');
					$addr['zipcode']=$bil_address->f('zip');
				}else{
					$main_address=$this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$value['customer_id']."' AND is_primary='1' ");
					if($main_address->next()){
						$addr['country_code']=$main_address->f('country_id') ? get_country_code($main_address->f('country_id')) : '';
						$addr['address']=$main_address->f('address');
						$addr['country']=$main_address->f('country_id') ? get_country_name($main_address->f('country_id')) :'';
						$addr['state']=$main_address->f('state_id') ? get_country_name($main_address->f('state_id')) : '';
						$addr['city']=$main_address->f('city');
						$addr['zipcode']=$main_address->f('zip');
					}
				}
				$item=array(
					'indiv_created'			=> $value['indiv_created'] == 1 ? false : true,
					'cus_name'			    => $value['comp_name'],
					'customer_id'		    => $value['customer_id'],
					'con_firstname'			=> $value['firstname'] ? $value['firstname'] : '',
					'con_lastname'			=> $value['lastname'] ? $value['lastname'] : '',
					'con_position'			=> $value['position_n'],
					'con_phone'				=> $value['phone'] ? $value['phone'] : '',
					'con_cell'				=> $value['cell'] ? $value['cell'] : '',
					'con_email'				=> $value['email'],
					'fax'					=> $value['fax'],
					'e_title'				=> $value['e_title'],
					's_email'				=> $value['s_email']==1 ? gm('Yes') : gm('No'),
					'function'				=> $function,
					'contact_id' 			=> $value['contact_id'],
					'sync_done'				=> $value['cash_contact_id'] ? true : false,
					'address'				=> $addr,
					'no_address'			=> empty($addr) ? true : false,
					'cash_contact_id'		=> $value['cash_contact_id'],
				);
				array_push($output['list'], $item);
			}
			$output['lr'] = $l_r;
			$output['do_next'] = 'cashregister--cashregister-sendContact';
			$this->out = $output;
		}

		public function get_buyers($in){
			$q = strtolower(trim($in["term"]));
			$filter =" AND is_admin='0' ";
			if($q){
				$filter = " name LIKE '%".$q."%' ";
			}
			$result = array();
			$func = $this->db->query("SELECT customers.name,customers.customer_id FROM customers
						WHERE customers.active=1 $filter ORDER BY customers.name ")->getAll();
			foreach ($func as $key => $value) {
				$result[] = array('id'=>$value['customer_id'], 'name'=>$value['name']);
			}
			return $result;
		}

	}

	$cash_data = new CashCustomer($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_customers();
	$cash_data->output();
?>