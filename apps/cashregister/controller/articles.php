<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class CashArticles extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}

		public function get_articles(){
			$in=$this->in;

			$l_r =ROW_PER_PAGE;
			$order_by = " ORDER BY  pim_articles.item_code  ";

			$order_by_array = array('item_code','internal_name','article_category','price', 'stock');

			if((!$in['offset']) || (!is_numeric($in['offset'])))
			{
			    $offset=0;
			    $in['offset']=1;
			}
			else
			{
			    $offset=$in['offset']-1;
			}

			//FILTER LIST
			$selected =array();
			$filter = '1=1';
			$filter_attach = '';
			$filter.= " AND (pim_articles.is_service='0' OR pim_articles.is_service='2') ";

			if(!$in['archived']){
				$filter.= " AND pim_articles.active='1' ";
			}else{
				$filter.= " AND pim_articles.active='0' ";
				$arguments.="&archived=".$in['archived'];
				$arguments_a = "&archived=".$in['archived'];
			}


			if(!empty($in['search'])){
				$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
				$arguments_s.="&search=".$in['search'];
			}
			if(!empty($in['supplier_reference'])){
				$filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
				$arguments_s.="&supplier_reference=".$in['supplier_reference'];
			}

			# this parts is obsolete
			if($in['attach_to_product']){
				 if($in['attach_to_product']==1){
				 	$filter_attach.= " INNER JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
				 }else{
				 	$filter_attach.= " LEFT JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
				 	$filter.=" AND pim_product_article.article_id is null";
				 }
				$arguments.="&attach_to_product=".$in['attach_to_product'];
			}
			# end
			if($in['is_picture']){
				 if($in['is_picture']==1){
				 	// $filter_attach.= " INNER JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
				 	$filter.=" AND pim_articles.parent_id <> 0";
				 }else{
				 	// $filter_attach.= " LEFT JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
				 	$filter.=" AND pim_articles.parent_id = 0";
				 }
				$arguments.="&is_picture=".$in['is_picture'];
			}

			if($in['article_category_id']){
				$filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
				$arguments.="&article_category_id=".$in['article_category_id'];
			}
			if($in['first'] && $in['first'] == '[0-9]'){
				$filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
				$arguments.="&first=".$in['first'];
			} elseif ($in['first']){
				$filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
				$arguments.="&first=".$in['first'];
			}

			if($in['order_by']){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == true){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
					$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
					/*$view_list->assign(array(
						'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
					));*/
				}
			}
			$e = $this->db->field("SELECT COUNT(pim_articles.article_id) FROM pim_articles
						WHERE $filter ".$order_by." ");
			$result = array('query'=>array(),'max_rows'=>$e);
			if(!isset($in['lang_id'])){
			  $in['lang_id'] = $this->db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1'");
			}
			if(!$in['lang_id']){
				$in['lang_id']=1;
			}
			$articles = $this->db->query("SELECT pim_articles.use_batch_no,pim_articles.article_id,pim_articles.hide_stock,pim_articles.block_discount,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,
						                   pim_articles.internal_name,pim_articles.price,pim_articles.article_category, pim_articles.weight, pim_articles.cash_article_id,pim_articles.is_service,
						                   vats.value AS vat_value , vats.cash_tax_id, pim_articles_lang.description AS description,pim_articles.ean_code,pim_article_categories.name AS article_category_name,pim_article_categories.id as cat_id,pim_article_categories.cash_category_id,
						                   pim_article_photo.file_name as photo_link,pim_articles.cash_bsync
			            FROM pim_articles
			            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
			            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
			            LEFT JOIN pim_article_photo ON pim_articles.article_id=pim_article_photo.parent_id
						WHERE $filter
					   ".$order_by."
						LIMIT ".$offset*$l_r.",".$l_r."
						");

			$j=0;
			while($articles->move_next()){
			     $taxes=array();
			     if($articles->f('cash_tax_id')!=''){
			     	array_push($taxes, $articles->f('cash_tax_id'));
			     }

			    $cat_id=$this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category='1' ");
			   	$default_price=$this->db->field("SELECT price FROM pim_article_prices WHERE price_category_id='".$cat_id."' AND article_id='".$articles->f('article_id')."' ");
			   	if(is_null($default_price)){
			   		$price=$articles->f('price');
			   		$price_vat=$articles->f('price')+($articles->f('price')*$articles->f('vat_value')/100);
			   	}else{
			   		$price=$default_price;
			   		$price_vat=$this->db->field("SELECT total_price FROM pim_article_prices WHERE price_category_id='".$cat_id."' AND article_id='".$articles->f('article_id')."' ");
			   	}		    

				$item =array(
							'use_batch_no'		=> $articles->f('use_batch_no')? true:false,
							'vat_value'		  	=> $articles->f('vat_value'),
							'description'       =>  $articles->f('description'),
							'ean_code'      =>  $articles->f('ean_code'),
							'category_name' =>  $articles->f('article_category_name'),
							'name'		  		=> $articles->f('internal_name'),
							'article_category'	=> $articles->f('article_category'),
							'article_id'  		=> $articles->f('article_id'),
							'stock'  			=> remove_zero_decimals($articles->f('stock')),
							'show_stock'  		=> $articles->f('hide_stock') ? false:true,
						    'block_discount'    => $articles->f('block_discount')? true:false,
							'base_price'  		=> display_number_var_dec($articles->f('price')),
							'price'				=> display_number_var_dec($price),
							'price_vat'			=> display_number_var_dec($price_vat),
							'check_show_front'	=> $articles->f('show_front')==1? 'checked="checked"':'',
							'code'		  		=> $articles->f('item_code'),
							'allow_stock'         			=> ALLOW_STOCK == '0' ? false:true,
							'stock_multiple_locations'      => STOCK_MULTIPLE_LOCATIONS== '0' ? false:true,
							//'stock_multiple_locations'=> false,
							'adv_product'         			=> !defined('ADV_PRODUCT') || ADV_PRODUCT == '0'? false:true,
							'is_archived'      	=> $in['archived'] ? true: false,
							/*'block_sync'			=> (in_array($articles->f('cat_id'),$in['blocked_fam']) || $articles->f('is_service')==2 || $articles->f('cash_bsync')) ? true : false,*/
							'block_sync'			=> ($articles->f('is_service')==2 || $articles->f('cash_bsync')) ? true : false,
							'sync_done'				=> $articles->f('cash_article_id')!='' ? true : false,
							'cash_article_id'		=> $articles->f('cash_article_id'),
							'weight'				=> $articles->f('weight'),
							'cash_category_id'		=> $articles->f('cash_category_id'),
							'taxes'					=> $taxes,
							'photo_link'			=> $articles->f('photo_link'),
							'is_tactill'			=> $articles->f('is_service')==2 ? true : false,
							);
			     if($articles->f('stock') <= 0){
			     	   $item['stock_status']='text-danger';
			          
			     }elseif($articles->f('stock') > 0  && $articles->f('stock') <= $articles->f('article_threshold_value') ){
			     	   $item['stock_status']='text-warning';
			     }else{
			     	   $item['stock_status']='text-success';

			     }

			    array_push($result['query'], $item);
				$j++;
			}


			$result['allow_stock']=ALLOW_STOCK == '0'?false:true;
			if(ALLOW_STOCK && ADV_PRODUCT ){
				$result['code_width']='col-sm-2  col-md-2';
				$result['fam_width']='col-sm-2  col-md-2';
			}else if(!ALLOW_STOCK && ADV_PRODUCT){
				$result['code_width']='col-sm-2  col-md-2';
				$result['fam_width']='col-sm-2  col-md-2';
			}else{
				$result['code_width']='col-sm-3  col-md-3';
				$result['fam_width']='col-sm-3  col-md-3';
			}
			$result['adv_product']= (!defined('ADV_PRODUCT') || ADV_PRODUCT == '0') ? false:true;
			if($result['adv_product']) {
				$result['code_class']='expand_row';
			}

			$is_p_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' ");
			$is_art_ord_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id = '".$_SESSION['u_id']."' AND name = 'admin_6' ");
			$is_module_admin = false;
			if($is_p_admin == '1' || $is_art_ord_admin == 'order'){
				$is_module_admin = true;
			}
			$result['is_module_admin']=$is_module_admin;
			$_SESSION['filters'] = $arguments.$arguments_o;

			$this->db->query("SELECT * FROM pim_article_categories ORDER BY name ");
			$result['families']=array();
			while($this->db->move_next()){
				$families = array(
					'name'  => $this->db->f('name'),
					'id'=> $this->db->f('id')
				);
				  
				array_push($result['families'], $families);
			}
			$result['lr'] = $l_r;
			$result['tab']=1;
			//$result['do_next']='cashregister--cashregister-sendArticle';
			$result['do_next']='cashregister--cashregister-sendBulkArticle';
			$this->out = $result;
		}

		public function get_categories(){
			$in=$this->in;

			$result = array('query'=>array());
			$l_r =ROW_PER_PAGE;
			if((!$in['offset']) || (!is_numeric($in['offset'])))
			{
			    $offset=0;
			    $in['offset']=1;
			}
			else
			{
			    $offset=$in['offset']-1;
			}
			$filter = "1=1";
			if(!empty($in['search'])){
				$filter .= " AND name LIKE '%".$in['search']."%' ";
			} 

			$max_rows=$this->db->field("SELECT COUNT(id) FROM pim_article_categories WHERE ".$filter." ");
			$result['max_rows']=$max_rows;
			$categories=$this->db->query("SELECT * FROM pim_article_categories WHERE ".$filter." ORDER BY name LIMIT ".$offset*$l_r.",".$l_r."");
			
			while($categories->move_next()){
				$families = array(
					'name'  	=> $categories->f('name'),
					'id'		=> $categories->f('id'),
					'no_art'	=> $this->db->field("SELECT COUNT(article_id) FROM pim_articles WHERE article_category_id='".$categories->f('id')."' "),
					/*'block_sync'	=> (in_array($categories->f('id'),$in['blocked_fam']) || $categories->f('is_tactill')) ? true : false,*/
					'block_sync'	=> ($categories->f('cash_bsync') || $categories->f('is_tactill')) ? true : false,
					'sync_done'		=> $categories->f('cash_category_id')!='' ? true : false,
					'cash_category_id'	=> $categories->f('cash_category_id'),
					'is_tactill'		=> $categories->f('is_tactill') ? true : false,
				);			  
				array_push($result['query'], $families);
			}
			$result['lr'] = $l_r;
			$result['tab']=2;
			$result['adv_product']= (!defined('ADV_PRODUCT') || ADV_PRODUCT == '0') ? false:true;
			//$result['do_next']='cashregister--cashregister-sendCategory';
			$result['do_next']='cashregister--cashregister-sendBulkCategory';
			$this->out = $result;
		}

		public function get_taxes(){
			$in=$this->in;
			$result = array('query'=>array());
			$l_r =ROW_PER_PAGE;
			if((!$in['offset']) || (!is_numeric($in['offset'])))
			{
			    $offset=0;
			    $in['offset']=1;
			}
			else
			{
			    $offset=$in['offset']-1;
			}
			$filter = "1=1";
			if(!empty($in['search'])){
				$filter .= " AND value LIKE '%".$in['search']."%' ";
			}

			$max_rows=$this->db->field("SELECT COUNT(vat_id) FROM vats WHERE ".$filter." ORDER BY value ASC ");
			$result['max_rows']=$max_rows;
			$taxes=$this->db->query("SELECT * FROM vats WHERE ".$filter." ORDER BY value LIMIT ".$offset*$l_r.",".$l_r." ");
			while ($taxes->move_next()) {	    
			    $item =array(  
			                 'vat_id'		=> $taxes->f('vat_id'),
			                 'value'		=> display_number($taxes->f('value')),
			                 'no_art'		=> $this->db->field("SELECT COUNT(article_id) FROM pim_articles WHERE vat_id='".$taxes->f('vat_id')."' "),
			             	 'sync_done'	=> $taxes->f('cash_tax_id')!='' ? true : false,
			             	 'cash_tax_id'	=> $taxes->f('cash_tax_id'),
			             );		   	 
			    array_push($result['query'], $item);
			    $i++;
			}

			$result['lr'] = $l_r;
			$result['tab']=3;			
			$result['adv_product']= (!defined('ADV_PRODUCT') || ADV_PRODUCT == '0') ? false:true;
			$result['do_next']='cashregister--cashregister-sendTax';
			$this->out = $result;
		}

		public function get_bulkArticles(){
			$result=array('list'=>array());
			$articles = $this->db->query("SELECT article_id FROM pim_articles WHERE active='1' AND is_service='0' AND cash_bsync='0' ");
			while($articles->next()){
				array_push($result['list'], array('article_id'=>$articles->f('article_id')));
			}
			return $result;
		}

		public function get_bulkCategories(){
			$result=array('list'=>array());
			$articles = $this->db->query("SELECT id FROM pim_article_categories WHERE is_tactill='0' AND cash_bsync='0'");
			while($articles->next()){
				array_push($result['list'], array('id'=>$articles->f('id')));
			}
			return $result;
		}

	}

	$cash_data = new CashArticles($in,$db);

		if($in['xget']){
		    $fname = 'get_'.$in['xget'];
		    $cash_data->output($cash_data->$fname($in));
		}

		$cash_data->get_articles();
		$cash_data->output();
?>