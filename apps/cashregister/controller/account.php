<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class CashAccount extends Controller{

		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}

		public function get_companies(){
			global $config;
			$in=$this->in;
			$result=array('query'=>array(),'data_incoming'=>array());

			$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");

    		$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
    		$time_filter='';
    		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_COMPANY_FILTER' ");
	        if($const){
	            $time_filter='?filter=updated_at[gt]='.date('Y-m-d',(int)$const-86400).'T'.date('H:i:s',(int)$const-86400).'.0Z';
	        }
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/account/companies'.$time_filter);
        	
        	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
        	
        	$data=json_decode($put);
        	foreach($data as $key=>$value){
        		array_push($result['data_incoming'],$value);
        	}

        	$l_r = ROW_PER_PAGE;
			$order_by_array = array('id', 'name', 'country_code', 'zip', 'address', 'phone', 'city');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY id DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true' || $in['desc'] == '1'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;	
				}
			}

        	$acc_data=$this->db->query("SELECT * FROM cash_company ".$filter.$order_by);
			$max_rows=$acc_data->records_count();
			$acc_data->move_to($offset*$l_r);
			$j=0;
			while($acc_data->next() && $j<$l_r){
				$line=array(
						'id'			=> $acc_data->f('id'),
						'company_id'	=> $acc_data->f('company_id'),
						'name'			=> $acc_data->f('name'),
						'country'		=> $this->db->field("SELECT name FROM country WHERE code='".$acc_data->f('country_code')."' "),
						'zip'			=> $acc_data->f('zip'),
						'address'		=> $acc_data->f('address'),
						'phone'			=> $acc_data->f('phone'),
						'city'			=> $acc_data->f('city') 
					);
				array_push($result['query'], $line);
			}
			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=1;
			$result['do_next']='cashregister--cashregister-saveCompany';
			$this->out = $result;
		}

		public function get_offers(){
			global $config;
			$in=$this->in;
			$result=array('query'=>array(),'data_incoming'=>array());

			$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");

    		$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
    		$time_filter='';
    		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_OFFER_FILTER' ");
	        if($const){
	            $time_filter='?filter=updated_at[gt]='.date('Y-m-d',(int)$const-86400).'T'.date('H:i:s',(int)$const-86400).'.0Z';
	        }
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/account/offers'.$time_filter);

        	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);
        	
        	$data=json_decode($put);
        	foreach($data as $key=>$value){
        		array_push($result['data_incoming'],$value);
        	}

        	$l_r = ROW_PER_PAGE;
			$order_by_array = array('id');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY id DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;	
				}
			}

			$off_data=$this->db->query("SELECT * FROM cash_offers ".$filter.$order_by);
			$max_rows=$off_data->records_count();
			$off_data->move_to($offset*$l_r);
			$j=0;
			while($off_data->next() && $j<$l_r){
				$line=array(
						'id'				=> $off_data->f('id'),
						'offer_id'			=> $off_data->f('offer_id'),
						'name'				=> $off_data->f('name'),
						'billing_frequency'	=> $off_data->f('billing_frequency'),
						'kind'				=> $off_data->f('kind'),  
						'price'				=> display_number($off_data->f('price')),  
						'licenses'			=> display_number($off_data->f('licenses')),
						'plan'				=> $off_data->f('plan_id')
					);
				array_push($result['query'], $line);
			}

			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=2;
			$result['do_next']='cashregister--cashregister-saveOffer';
			$this->out = $result;
		}

		public function get_shops(){
			global $config;
			$in=$this->in;
			$result=array('query'=>array(),'data_incoming'=>array());

			$ch = curl_init();
			$tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
			$comp_id=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='company' ");
			$headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));

			$acc_data=$this->db->query("SELECT company_id FROM cash_company ORDER BY id ASC");
			$time_filter='';
    		$const=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_SHOP_FILTER' ");
	        if($const){
	            $time_filter='?filter=updated_at[gt]='.date('Y-m-d',(int)$const-86400).'T'.date('H:i:s',(int)$const-86400).'.0Z';
	        }
			while($acc_data->next()){	
	    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        	curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/account/companies/'.$acc_data->f('company_id').'/shops'.$time_filter);
	        	$put = curl_exec($ch);
		    	$info = curl_getinfo($ch);
	        	
	        	$data=json_decode($put);
	        	foreach($data as $key=>$value){
	        		array_push($result['data_incoming'],$value);
	        	}
			}

        	$l_r = ROW_PER_PAGE;
			$order_by_array = array('id');
			$filter = "WHERE 1=1 ";
			$order_by=" ORDER BY id DESC "; 
			if((!$in['offset']) || (!is_numeric($in['offset']))){
			    $offset=0;
			    $in['offset']=1;
			}else{
			    $offset=$in['offset']-1;
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;	
				}
			}

			$shop_data=$this->db->query("SELECT * FROM cash_shops ".$filter.$order_by);
			$max_rows=$shop_data->records_count();
			$shop_data->move_to($offset*$l_r);
			$j=0;
			while($shop_data->next() && $j<$l_r){
				$line=array(
						'id'				=> $shop_data->f('id'),
						'shop_id'			=> $shop_data->f('shop_id'),
						'company'			=> $this->db->field("SELECT name FROM cash_company WHERE company_id='".$shop_data->f('company_id')."' "),
						'name'				=> $shop_data->f('name'),
						'city'				=> $shop_data->f('city'),  
						'phone'				=> $shop_data->f('phone'),  
						'address'			=> $shop_data->f('address'),
						'zip'				=> $shop_data->f('zip'),
						'country'			=> $this->db->field("SELECT name FROM country WHERE code='".$shop_data->f('country_code')."' ")
					);
				array_push($result['query'], $line);
			}

			$result['lr'] = $l_r;
			$result['max_rows'] = $max_rows;
			$result['tab']=3;
			$result['do_next']='cashregister--cashregister-saveShop';
			$this->out = $result;
		}

	}

	$cash_data = new CashAccount($in,$db);

		if($in['xget']){
		    $fname = 'get_'.$in['xget'];
		    $cash_data->output($cash_data->$fname($in));
		}

		$cash_data->get_companies();
		$cash_data->output();
?>