<?php

class cashregister {

    function __construct() {
        $this->db = new sqldb();
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $this->db_users = new sqldb($database_users);
    }

    function sendArticle(&$in){
        global $config;
        $ch = curl_init();
        $tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
        $tact_node=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='node' ");

        $headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
        if($in['sync_done']!=''){
            $tact_data=array(
                'category_id'       => $in['cash_category_id'],
                'taxes'             => array(),
                'name'              => $in['name'],
                'reference'         => $in['code'],
                'summary'           => $in['description'],
                'taxfree_price'     => return_value($in['price']),
                'full_price'        => return_value($in['price_vat']),
                'in_stock'          => $in['stock']>0 ? true : false,
                'weight'            => $in['weight'],
                'updated_at'        => date("c",time()),
                //'image'             => $in['photo_link']
            );
            if(!empty($in['taxes'])){
                $tact_data['taxes']=$in['taxes'];
            }
            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/articles/'.$in['cash_article_id']); 
        }else{
            $tact_data=array(
                'node_id'           => $tact_node,
                'category_id'       => $in['cash_category_id'],
                'taxes'             => array(),
                'name'              => $in['name'],
                'reference'         => $in['code'],
                'summary'           => $in['description'],
                'taxfree_price'     => return_value($in['price']),
                'full_price'        => return_value($in['price_vat']),
                'in_stock'          => $in['stock']>0 ? true : false,
                'weight'            => $in['weight'],
                'created_at'        => date("c",time()),
                'updated_at'        => date("c",time()),
               // 'image'             => $in['photo_link']
            );
            if(!empty($in['taxes'])){
                $tact_data['taxes']=$in['taxes'];
            }
            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/articles');        
        }
        
        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        if($info['http_code']>300 || $info['http_code']==0){                         
            $err=json_decode($put);
            msg::error($in['code'].': '.$err->message,"error");
        }else{
            $data=json_decode($put);
            msg::success($in['code'].': '.$data->message,"success");
            if($in['sync_done']==''){
                $this->db->query("UPDATE pim_articles SET cash_article_id='".$data->_id."' WHERE article_id='".$in['article_id']."' ");
            }
        }
        json_out($in);
    }

    function sendCategory(&$in){
        global $config;
        $ch = curl_init();
        $tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
        $tact_company=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='company' ");

        $headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
        if($in['sync_done']!=''){
            $tact_data=array(
                    'name'              => $in['name'],
                    'image'             => $in['photo_link'],
                    'updated_at'        => date("c",time())
            );

            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/categories/'.$in['cash_category_id']);
        }else{
            $tact_data=array(
                    'company_id'        => $tact_company,
                    'name'              => $in['name'],
                    'image'             => $in['photo_link'],
                    'created_at'        => date("c",time()),
                    'updated_at'        => date("c",time())
            );

            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);   
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/categories');
        }
        
        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        if($info['http_code']>300 || $info['http_code']==0){                         
            $err=json_decode($put);
            msg::error($in['name'].': '.$err->message,"error");
        }else{
            $data=json_decode($put);
            msg::success($in['name'].': '.$data->message,"success");
            if($in['sync_done']==''){
                $this->db->query("UPDATE pim_article_categories SET cash_category_id='".$data->_id."' WHERE id='".$in['id']."' ");
            }
        }
        json_out($in);
    }

    function sendTax(&$in){
        global $config;
        $ch = curl_init();
        $tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
        $tact_company=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='company' ");

        $headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
        if($in['sync_done']!=''){
            $tact_data=array(
                    'name'              => $in['value'],
                    'is_default'        => false,
                    'in_price'          => true,
                    'rate'              => return_value($in['value']),
                    'updated_at'        => date("c",time())
            );

            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/taxes/'.$in['cash_tax_id']);
        }else{
            $tact_data=array(
                    'company_id'        => $tact_company,
                    'name'              => $in['value'],
                    'is_default'        => false,
                    'in_price'          => true,
                    'rate'              => return_value($in['value']),
                    'created_at'        => date("c",time()),
                    'updated_at'        => date("c",time())
            );

            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);   
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/taxes');
        }
        
        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        if($info['http_code']>300 || $info['http_code']==0){                         
            $err=json_decode($put);
            msg::error($in['value'].': '.$err->message,"error");
        }else{
            $data=json_decode($put);
            msg::success($in['value'].': '.$data->message,"success");
            if($in['sync_done']==''){
                $this->db->query("UPDATE vats SET cash_tax_id='".$data->_id."' WHERE vat_id='".$in['vat_id']."' ");
            }
        }
        json_out($in);
    }

    function saveTicket(&$in){
        $ticket=$this->db->field("SELECT ticket_id FROM cash_tickets WHERE ticket_id='".$in['_id']."' ");
        if($ticket){
            /*$this->db->query("UPDATE cash_tickets SET                               
                                amount_net='".$in['amount_net']."',
                                date='".strtotime($in['closed_at'])."',
                                amount_taxfree='".$in['amount_taxfree']."',
                                operation_type='".$in['operation_type']."',
                                customer_id='".$in['customer_id']."',
                                number='".$in['number']."',
                                discount='".$in['discounts'][0]['rate']."',
                                discount_type='".$in['discounts'][0]['type']."' 
                                WHERE ticket_id='".$in['_id']."'");
            $this->db->query("DELETE FROM cash_tickets_articles WHERE ticket_id='".$in['_id']."'");
            $this->db->query("DELETE FROM cash_tickets_pay WHERE ticket_id='".$in['_id']."'");
            foreach($in['items'] as $key=>$value){
                $this->db->query("INSERT INTO cash_tickets_articles SET 
                                        ticket_id='".$in['_id']."',
                                        article_id='".$value['article_id']."',
                                        sell_price='".$value['sell_price']."',
                                        quantity='".$value['units']."',
                                        full_price='".$value['full_price']."',
                                        tax_free_price='".$value['taxfree_price']."',
                                        vat='".$value['taxes'][0]['rate']."',
                                        vat_val='".$value['taxes'][0]['amount']."',
                                        discount_line='".$value['discounts'][0]['rate']."',
                                        discount_type='".$value['discounts'][0]['type']."' ");
            }
            foreach($in['movements'] as $key=>$value){
                $this->db->query("INSERT INTO cash_tickets_pay SET
                                                ticket_id='".$in['_id']."',
                                                payment_id='".$value['payment_method_id']."',
                                                amount='".$value['amount']."' ");
            }*/
        }else{
            $ticket_id = $this->db->insert("INSERT INTO cash_tickets SET
                                ticket_id='".$in['_id']."',
                                amount_net='".$in['amount_net']."',
                                date='".strtotime($in['closed_at'])."',
                                amount_taxfree='".$in['amount_taxfree']."',
                                operation_type='".$in['operation_type']."',
                                customer_id='".$in['customer_id']."',
                                number='".$in['number']."',
                                discount='".$in['discounts'][0]['rate']."',
                                discount_type='".$in['discounts'][0]['type']."' ");
            $this->db->query("DELETE FROM cash_tickets_articles WHERE ticket_id='".$in['_id']."'");
            $this->db->query("DELETE FROM cash_tickets_pay WHERE ticket_id='".$in['_id']."'");
            foreach($in['items'] as $key=>$value){
                $this->db->query("INSERT INTO cash_tickets_articles SET 
                                        ticket_id='".$in['_id']."',
                                        article_id='".$value['article_id']."',
                                        description='".addslashes($value['name'])."',
                                        sell_price='".$value['sell_price']."',
                                        quantity='".$value['units']."',
                                        full_price='".$value['full_price']."',
                                        tax_free_price='".$value['taxfree_price']."',
                                        vat='".$value['taxes'][0]['rate']."',
                                        vat_val='".$value['taxes'][0]['amount']."',
                                        discount_line='".$value['discounts'][0]['rate']."',
                                        discount_type='".$value['discounts'][0]['type']."' ");

                $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
             /*   $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
                $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");*/

                if($value['article_id'] && $const['ALLOW_STOCK']){ 
                    $sync_article=$this->db->query("SELECT article_id, stock, packing, sale_unit, internal_name, item_code FROM pim_articles WHERE cash_article_id='".$value['article_id']."' ");
                    while ($sync_article->next()){ //  we change the stock value only if it's a syncronised article 
                        $stock = $sync_article->f('stock');
                        $article_id = $sync_article->f('article_id');

/*                        $stock_packing = $sync_article->f("packing");
                        if(!$const['ALLOW_ARTICLE_PACKING']){
                                $stock_packing = 1;
                            }
                        $stock_sale_unit = $sync_article->f("sale_unit");
                        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
                                $stock_sale_unit = 1;
                            }*/

                        $new_stock = $stock - $value['units'];
                        $this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");

                        $this->db->insert("INSERT INTO stock_movements SET
                                date                        =   '".time()."',
                                created_by                  =   '".$_SESSION['u_id']."',
                                serial_number               =   '".$in['number']."',
                                article_id                  =   '".$article_id."',
                                article_name                =   '".addslashes($sync_article->f("internal_name"))."',
                                item_code                   =   '".addslashes($sync_article->f("item_code"))."',
                                movement_type               =   '18',
                                quantity                    =   '".$value['units']."',
                                stock                       =   '".$stock."',
                                new_stock                   =   '".$new_stock."',
                                ticket_id                   =    '".$ticket_id."'
                                ");
                    $main_dispatch_stock_address_id = $this->db->field("SELECT address_id FROM dispatch_stock_address  WHERE is_default='1'");
                    $from_address_current_stock = $this->db->field("SELECT stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$main_dispatch_stock_address_id."' and customer_id='0'");

                    $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -$value['units']) ."' WHERE article_id='".$article_id."'   AND  address_id='".$main_dispatch_stock_address_id."' and customer_id='0'");

                    }//end while
                } //end if allow stock
                
            } //end foreach
            foreach($in['movements'] as $key=>$value){
                $this->db->query("INSERT INTO cash_tickets_pay SET
                                                ticket_id='".$in['_id']."',
                                                payment_id='".$value['payment_method_id']."',
                                                amount='".$value['amount']."' ");
            }
        }
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_TICKET_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='CASH_TICKET_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='CASH_TICKET_FILTER',value='".time()."'");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
    }

    function savePay(&$in){
        $pay_m=$this->db->field("SELECT payment_id FROM cash_pay_method WHERE payment_id='".$in['_id']."' ");
        if($pay_m){
            $this->db->query("UPDATE cash_pay_method SET 
                                name='".$in['name']."',
                                type='".$in['type']."'
                                WHERE payment_id='".$in['_id']."'");
        }else{
            $this->db->query("INSERT INTO cash_pay_method SET
                                payment_id='".$in['_id']."',
                                name='".$in['name']."',
                                type='".$in['type']."' ");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
    }

    function saveCashbook(&$in){
        $cash_b=$this->db->field("SELECT cashbook_id FROM cash_books WHERE cashbook_id='".$in['_id']."' ");
        if($cash_b){
            $this->db->query("UPDATE cash_books SET 
                                seller_name='".$in['seller_name']."',
                                date='".strtotime($in['start_date'])."',
                                start_amount='".$in['start_amount']."',
                                end_amount='".$in['end_amount']."',
                                total='".$in['total']."'
                                WHERE cashbook_id='".$in['_id']."'");
        }else{
            $this->db->query("INSERT INTO cash_books SET
                                cashbook_id='".$in['_id']."',
                                seller_name='".$in['seller_name']."',
                                date='".strtotime($in['start_date'])."',
                                start_amount='".$in['start_amount']."',
                                end_amount='".$in['end_amount']."',
                                total='".$in['total']."' ");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
    }

    function saveCompany(&$in){
        $cash_comp=$this->db->field("SELECT company_id FROM cash_company WHERE company_id='".$in['_id']."' ");
        if($cash_comp){
            $this->db->query("UPDATE cash_company SET 
                                name='".$in['name']."',
                                country_code='".$in['country_code']."',
                                zip='".$in['zipcode']."',
                                address='".$in['address']."',
                                phone='".$in['phone']."',
                                city='".$in['city']."'
                                WHERE company_id='".$in['_id']."'");
        }else{
            $this->db->query("INSERT INTO cash_company SET
                                company_id='".$in['_id']."',
                                name='".$in['name']."',
                                country_code='".$in['country_code']."',
                                zip='".$in['zipcode']."',
                                address='".$in['address']."',
                                phone='".$in['phone']."',
                                city='".$in['city']."' ");
        }
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_COMPANY_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='CASH_COMPANY_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='CASH_COMPANY_FILTER',value='".time()."'");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
    }

    function saveOffer(&$in){
        $cash_off=$this->db->field("SELECT offer_id FROM cash_offers WHERE offer_id='".$in['_id']."' ");
        if($cash_off){
            $this->db->query("UPDATE cash_offers SET 
                                name='".$in['name']."',
                                billing_frequency='".$in['billing_frequency']."',
                                kind='".$in['kind']."',
                                price='".$in['price']."',
                                licenses='".$in['licenses']."',
                                plan_id='".$in['plan_id']."'
                                WHERE offer_id='".$in['_id']."'");
        }else{
            $this->db->query("INSERT INTO cash_offers SET
                                offer_id='".$in['_id']."',
                                name='".$in['name']."',
                                billing_frequency='".$in['billing_frequency']."',
                                kind='".$in['kind']."',
                                price='".$in['price']."',
                                licenses='".$in['licenses']."',
                                plan_id='".$in['plan_id']."' ");
        }
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_OFFER_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='CASH_OFFER_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='CASH_OFFER_FILTER',value='".time()."'");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
    }

    function saveShop(&$in){
        $cash_shop=$this->db->field("SELECT shop_id FROM cash_shops WHERE shop_id='".$in['_id']."' ");
        if($cash_shop){
            $this->db->query("UPDATE cash_shops SET 
                                name='".$in['name']."',
                                company_id='".$in['company_id']."',
                                city='".$in['city']."',
                                phone='".$in['phone']."',
                                address='".$in['address']."',
                                zip='".$in['zipcode']."',
                                country_code='".$in['country_code']."'
                                WHERE shop_id='".$in['_id']."'");
        }else{
            $this->db->query("INSERT INTO cash_shops SET
                                shop_id='".$in['_id']."',
                                name='".$in['name']."',
                                company_id='".$in['company_id']."',
                                city='".$in['city']."',
                                phone='".$in['phone']."',
                                address='".$in['address']."',
                                zip='".$in['zipcode']."',
                                country_code='".$in['country_code']."' ");
        }
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_SHOP_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='CASH_SHOP_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='CASH_SHOP_FILTER',value='".time()."'");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
    }

    function getArticles(&$in){
        json_out($in);

        global $config;
        $ch = curl_init();
        $tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
        $tact_node=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='node' ");
        $headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/articles?node_id='.$tact_node);
        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        if($info['http_code']>300 || $info['http_code']==0){ 
            $err=json_decode($put);
            msg::error($err->message,"error");
        }else{
            $data=json_decode($put);
            foreach($data as $key=>$value){
                $exist=$this->db->field("SELECT article_id FROM pim_articles WHERE cash_article_id='".$value->_id."' ");
                if(!$exist){
                    $a_id=$this->db->insert("INSERT INTO pim_articles SET 
                                cash_article_id='".$value->_id."',
                                internal_name='".$value->name."',
                                price='".$value->taxfree_price."',
                                item_code='".$value->reference."',
                                ean_code='".$value->reference."',
                                is_service='2' ");
                    $category=$this->db->field("SELECT id FROM pim_article_categories WHERE cash_category_id='".$value->category_id."' ");
                    if($category){
                        $this->db->query("UPDATE pim_articles SET article_category_id='".$category."' WHERE article_id='".$a_id."' ");
                    }
                    if(!empty($value->taxes)){
                        $tax=$this->db->field("SELECT vat_id FROM vats WHERE cash_tax_id='".$value->taxes[0]."' ");
                        if($tax){
                            $this->db->query("UPDATE pim_articles SET vat_id='".$tax."' WHERE article_id='".$a_id."' ");
                        }
                    }
                    $this->db->query("INSERT INTO dispatch_stock SET  
                                            article_id= '".$a_id."',
                                            stock= '0',
                                            address_id='0',
                                            main_address='1' ");
                    $active_languages = $this->db->query("SELECT * FROM pim_lang GROUP BY lang_id");
                    while ($active_languages->next()) {
                        $this->db->query("INSERT INTO pim_articles_lang SET
                            item_id                = '" . $a_id . "',
                            lang_id                = '" . $active_languages -> f('lang_id') . "' ");
                    }
                    $this->db->query("INSERT INTO pim_article_prices SET
                                        article_id          = '".$a_id."',
                                        price_category_id   = '0',
                                        price               = '".$value->taxfree_price."',
                                        total_price         = '".$value->full_price."',
                                        base_price          = '1',
                                        default_price       = '0' ");
                    $prices = $this->db->query("SELECT * FROM pim_article_price_category");
                    while ($prices->next()){
                        $base_price=$value->taxfree_price;
                        $this->db->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
                                          WHERE article_id='".$a_id."' AND category_id='".$prices->f('category_id')."'");
                        if($this->db->move_next()){
                            $base_price=$value->taxfree_price;
                            switch ($this->db->f('type')) {
                                case 1:                  //discount
                                if($this->db->f('price_value_type')==1){  // %
                                    $total_price = return_value($base_price) - round(($this->db->f('price_value') * return_value( $base_price) / 100),2);

                                }else{ //fix
                                    $total_price = return_value($base_price) - $this->db->f('price_value');
                                }

                                break;
                                case 2:                 //profit margin
                                if($this->db->f('price_value_type')==1){  // %
                                    $total_price = return_value( $base_price) +  round(($this->db->f('price_value') * return_value( $base_price) / 100),2);
                                }else{ //fix
                                    $total_price = return_value( $base_price) + $this->db->f('price_value');
                                }
                                break;
                            }
                        }else{
                            switch ($prices->f('type')) {
                                case 1:                  //discount
                                if($prices->f('price_value_type')==1){  // %
                                    $total_price = return_value( $base_price) - round(($prices->f('price_value') * return_value( $base_price) / 100),2);

                                }else{ //fix
                                    $total_price = return_value( $base_price) - $prices->f('price_value');
                                }

                                break;
                                case 2:                 //profit margin
                                if($prices->f('price_value_type')==1){  // %
                                    $total_price = return_value($base_price) +  round(($prices->f('price_value') * return_value($base_price) / 100),2);
                                }else{ //fix
                                    $total_price = return_value($base_price) + $prices->f('price_value');
                                }
                            break;
                          }
                        }
                        $total_with_vat = $value->taxfree_price+($value->taxfree_price*return_value(ACCOUNT_VAT)/100);
                        $this->db->query("INSERT INTO pim_article_prices SET
                                    article_id          = '".$a_id."',
                                    price_category_id   = '".$prices->f('category_id')."',
                                    price               = '".$value->taxfree_price."',
                                    total_price         = '".$total_with_vat."' ");
                    }
                }else{
                    $is_tactill=$this->db->field("SELECT is_service FROM pim_articles WHERE cash_article_id='".$value->_id."' ");
                    if($is_tactill == 2){
                        $this->db->query("UPDATE pim_articles SET
                                internal_name='".$value->name."',
                                price='".$value->taxfree_price."',
                                item_code='".$value->reference."',
                                ean_code='".$value->reference."'
                                WHERE article_id='".$exist."' ");
                        $category=$this->db->field("SELECT id FROM pim_article_categories WHERE cash_category_id='".$value->category_id."' ");
                        if($category){
                            $this->db->query("UPDATE pim_articles SET article_category_id='".$category."' WHERE article_id='".$exist."' ");
                        }
                        if(!empty($value->taxes)){
                            $tax=$this->db->field("SELECT vat_id FROM vats WHERE cash_tax_id='".$value->taxes[0]."' ");
                            if($tax){
                                $this->db->query("UPDATE pim_articles SET vat_id='".$tax."' WHERE article_id='".$exist."' ");
                            }
                        }
                        $this->db->query("UPDATE pim_article_prices SET
                                            price               = '".$value->taxfree_price."',
                                            total_price         = '".$value->full_price."'
                                            WHERE base_price=1 AND article_id='".$exist."'");
                        $this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$exist."' AND base_price!='1'");
                        $prices = $this->db->query("SELECT * FROM pim_article_price_category");
                        while ($prices->next()){
                            $base_price=$value->taxfree_price;
                            $this->db->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
                                              WHERE article_id='".$exist."' AND category_id='".$prices->f('category_id')."'");
                            if($this->db->move_next()){
                                $base_price=$value->taxfree_price;
                                switch ($this->db->f('type')) {
                                    case 1:                  //discount
                                    if($this->db->f('price_value_type')==1){  // %
                                        $total_price = return_value($base_price) - round(($this->db->f('price_value') * return_value( $base_price) / 100),2);

                                    }else{ //fix
                                        $total_price = return_value($base_price) - $this->db->f('price_value');
                                    }

                                    break;
                                    case 2:                 //profit margin
                                    if($this->db->f('price_value_type')==1){  // %
                                        $total_price = return_value( $base_price) +  round(($this->db->f('price_value') * return_value( $base_price) / 100),2);
                                    }else{ //fix
                                        $total_price = return_value( $base_price) + $this->db->f('price_value');
                                    }
                                    break;
                                }
                            }else{
                                switch ($prices->f('type')) {
                                    case 1:                  //discount
                                    if($prices->f('price_value_type')==1){  // %
                                        $total_price = return_value( $base_price) - round(($prices->f('price_value') * return_value( $base_price) / 100),2);

                                    }else{ //fix
                                        $total_price = return_value( $base_price) - $prices->f('price_value');
                                    }

                                    break;
                                    case 2:                 //profit margin
                                    if($prices->f('price_value_type')==1){  // %
                                        $total_price = return_value($base_price) +  round(($prices->f('price_value') * return_value($base_price) / 100),2);
                                    }else{ //fix
                                        $total_price = return_value($base_price) + $prices->f('price_value');
                                    }
                                break;
                              }
                            }
                            $total_with_vat = $value->taxfree_price+($value->taxfree_price*return_value(ACCOUNT_VAT)/100);
                            $this->db->query("INSERT INTO pim_article_prices SET
                                        article_id          = '".$exist."',
                                        price_category_id   = '".$prices->f('category_id')."',
                                        price               = '".$value->taxfree_price."',
                                        total_price         = '".$total_with_vat."' ");
                        }         
                    }
                }
            }
            msg::success(gm("Articles synced"),"success");
        }
         
        json_out($in);
    }

    function getCategories(&$in){
        json_out($in);

        global $config;
        $ch = curl_init();
        $tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
        $tact_company=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='company' ");

        $headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/catalog/categories?company_id='.$tact_company);

        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        if($info['http_code']>300 || $info['http_code']==0){ 
            $err=json_decode($put);
            msg::error($err->message,"error");
        }else{
            $data=json_decode($put);
            foreach($data as $key=>$value){
                $exist=$this->db->field("SELECT id FROM pim_article_categories WHERE cash_category_id='".$value->_id."' ");
                if(!$exist){
                    $last_entry=$this->db->field("SELECT sort_order FROM pim_article_categories ORDER BY id DESC LIMIT 1");
                    $this->db->query("INSERT INTO pim_article_categories SET
                                       name='".$value->name."',
                                       is_tactill='1',
                                       cash_category_id='".$value->_id."',
                                       sort_order='".($last_entry++)."' ");
                }else{
                    $is_tactill=$this->db->field("SELECT is_tactill FROM pim_article_categories WHERE cash_category_id='".$value->_id."' ");
                    if($is_tactill){
                        $this->db->query("UPDATE pim_article_categories SET
                                       name='".$value->name."'
                                       WHERE id='".$exist."' "); 
                    }
                }
            }
            msg::success(gm("Categories synced"),"success");
        }
        json_out($in);
    }

    function saveCustomer(&$in){
        $cash_customer=$this->db->field("SELECT customer_id FROM cash_customers WHERE customer_id='".$in['_id']."' ");
        if($cash_customer){
            $this->db->query("UPDATE cash_customers SET 
                                company_id='".$in['company_id']."',
                                name='".$in['customer']."',
                                email='".$in['email']."',                          
                                phone='".$in['phone']."',
                                country_code='".$in['country_code']."',
                                first_name='".$in['first_name']."',
                                last_name='".$in['last_name']."',
                                address='".$in['address']."',
                                state='".$in['state']."',
                                city='".$in['city']."',
                                zip='".$in['zipcode']."',
                                place_id='".$in['place_id']."',
                                image='".$in['image']."',
                                note='".$in['note']."'
                                WHERE customer_id='".$in['_id']."'");
        }else{
            $this->db->query("INSERT INTO cash_customers SET
                                customer_id='".$in['_id']."',
                                company_id='".$in['company_id']."',
                                name='".$in['customer']."',
                                email='".$in['email']."',                          
                                phone='".$in['phone']."',
                                country_code='".$in['country_code']."',
                                first_name='".$in['first_name']."',
                                last_name='".$in['last_name']."',
                                address='".$in['address']."',
                                state='".$in['state']."',
                                city='".$in['city']."',
                                zip='".$in['zipcode']."',
                                place_id='".$in['place_id']."',
                                image='".$in['image']."',
                                note='".$in['note']."'  ");
        }
        $constant=$this->db->field("SELECT value FROM settings WHERE constant_name='CASH_CUSTOMER_FILTER' ");
        if($constant){
            $this->db->query("UPDATE settings SET value='".time()."' WHERE constant_name='CASH_CUSTOMER_FILTER'");
        }else{
            $this->db->query("INSERT INTO settings SET constant_name='CASH_CUSTOMER_FILTER',value='".time()."'");
        }
        msg::success(gm('Data saved'),"success");
        json_out($in);
    }

    function sendContact(&$in){
        global $config;
        $ch = curl_init();
        $tact_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
        $tact_company=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_data->f('app_id')."' AND type='company' ");

        $headers=array('Content-Type: application/json','x-api-key:'.$tact_data->f('api'));
        if($in['sync_done']!=''){
            $tact_data=array(
                'email'             => $in['con_email'],
                'phone'             => $in['con_cell'],
                'first_name'         => $in['con_firstname'],
                'last_name'         => $in['con_lastname'],
            );
            if(!empty($in['address'])){
                foreach($in['address'] as $key=>$value){
                    $tact_data[$key]=$value;
                }
            }
            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/customer/customers/'.$in['cash_contact_id']); 
        }else{
            $tact_data=array(
                'company_id'        => $tact_company,
                'email'             => $in['con_email'],
                'phone'             => $in['con_cell'],
                'first_name'         => $in['con_firstname'],
                'last_name'         => $in['con_lastname'],
            );
            if(!empty($in['address'])){
                foreach($in['address'] as $key=>$value){
                    $tact_data[$key]=$value;
                }
            }
            $tact_data = json_encode($tact_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tact_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/customer/customers');        
        }
        
        $put = curl_exec($ch);
        $info = curl_getinfo($ch);

        if($info['http_code']>300 || $info['http_code']==0){                         
            $err=json_decode($put);
            msg::error($in['con_lastname'].' '.$in['con_firstname'].': '.$err->message,"error");
        }else{
            $data=json_decode($put);
            msg::success($in['con_lastname'].' '.$in['con_firstname'].': '.$data->message,"success");
            if($in['sync_done']==''){
                $this->db->query("UPDATE customer_contacts SET cash_contact_id='".$data->_id."' WHERE contact_id='".$in['contact_id']."' ");
                $sql="INSERT INTO cash_customers SET
                                customer_id='".$data->_id."',
                                company_id='".$tact_company."',
                                email='".$in['con_email']."',                          
                                phone='".$in['con_cell']."',
                                first_name='".$in['con_firstname']."',
                                last_name='".$in['con_lastname']."',
                                our='1',";
                if(!empty($in['address'])){
                    foreach($in['address'] as $key=>$value){
                        if($key=='country'){
                            continue;
                        }else if($key=='zipcode'){
                            $sql.=" zip='".$value."',";
                        }else{
                            $sql.=" ".$key."='".$value."',";
                        }              
                    }        
                }
                $sql=rtrim($sql,",");
                $this->db->query($sql);
            }else{
                $sql="UPDATE cash_customers SET
                                company_id='".$tact_company."',
                                email='".$in['con_email']."',                          
                                phone='".$in['con_cell']."',
                                first_name='".$in['con_firstname']."',
                                last_name='".$in['con_lastname']."',
                                our='1',";
                if(!empty($in['address'])){
                    foreach($in['address'] as $key=>$value){
                        if($key=='country'){
                            continue;
                        }else if($key=='zipcode'){
                            $sql.=" zip='".$value."',";
                        }else{
                            $sql.=" ".$key."='".$value."',";
                        }
                    }            
                }
                $sql=rtrim($sql,",");
                $sql.=" WHERE customer_id='".$in['cash_contact_id']."' ";
                $this->db->query($sql);
            }
        }
        json_out($in);
    }

    /**
      * undocumented function
      *
      * @return void
      * @author PM
      **/
      function tryAddC(&$in){
        if(!$this->tryAddCValid($in)){ 
          json_out($in);
          return false; 
        }
        //$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
        $name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
        //Set account default vat on customer creation
        $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

        if(empty($vat_regime_id)){
          $vat_regime_id = 0;
        }

        $c_types = '';
        $c_type_name = '';
        if($in['c_type']){
            /*foreach ($in['c_type'] as $key) {
                if($key){
                    $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
                    $c_type_name .= $type.',';
                }
            }
            $c_types = implode(',', $in['c_type']);
            $c_type_name = rtrim($c_type_name,',');*/
            if(is_array($in['c_type'])){
                foreach ($in['c_type'] as $key) {
                    if($key){
                        $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
                        $c_type_name .= $type.',';
                    }
                }
                $c_types = implode(',', $in['c_type']);
                $c_type_name = rtrim($c_type_name,',');
            }else{
                $c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");                
                $c_types = $in['c_type'];
            }
        }


        if($in['user_id']==''){
            $in['user_id']=$_SESSION['u_id'];
        }


        if($in['user_id']){
            /*foreach ($in['user_id'] as $key => $value) {
                $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
                    $acc_manager .= $manager_id.',';
            }           
            $in['acc_manager'] = rtrim($acc_manager,',');
            $acc_manager_ids = implode(',', $in['user_id']);*/
            if(is_array($in['user_id'])){
                foreach ($in['user_id'] as $key => $value) {
                    $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
                        $acc_manager .= $manager_id.',';
                }           
                $in['acc_manager'] = rtrim($acc_manager,',');
                $acc_manager_ids = implode(',', $in['user_id']);
            }else{
                //$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
                $in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
                $acc_manager_ids = $in['user_id'];
            }
        }else{
            $acc_manager_ids = '';
            $in['acc_manager'] = '';
        }
        $vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
            $vat_default=str_replace(',', '.', $vat);
        $selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");


        if($in['add_customer']){
            $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
            $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
            $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                                name='".$in['name']."',
                                                btw_nr='".$in['btw_nr']."',
                                                vat_regime_id = '".$vat_regime_id."',
                                                city_name = '".$in['city']."',
                                                c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
                                                user_id = '".$acc_manager_ids."',
                                                acc_manager_name = '".addslashes($in['acc_manager'])."',
                                                country_name ='".get_country_name($in['country_id'])."',
                                                active='1',
                                                payment_term      = '".$payment_term."',
                                                payment_term_type     = '".$payment_term_type."',
                                                zip_name  = '".$in['zip']."',
                                                commercial_name         = '".$in['commercial_name']."',
                                                legal_type              = '".$in['legal_type']."',
                                                c_type                  = '".$c_types."',
                                                website                 = '".$in['website']."',
                                                language                = '".$in['language']."',
                                                sector                  = '".$in['sector']."',
                                                comp_fax                = '".$in['comp_fax']."',
                                                activity                = '".$in['activity']."',
                                                comp_size               = '".$in['comp_size']."',
                                                lead_source             = '".$in['lead_source']."',
                                                various1                = '".$in['various1']."',
                                                various2                = '".$in['various2']."',
                                                c_type_name             = '".$c_type_name."',
                                                vat_id                  = '".$selected_vat."',
                                                invoice_email_type      = '1',
                                                sales_rep               = '".$in['sales_rep_id']."',
                                                internal_language       = '".$in['internal_language']."',
                                                is_supplier             = '".$in['is_supplier']."',
                                                is_customer             = '".$in['is_customer']."',
                                                stripe_cust_id          = '".$in['stripe_cust_id']."'");
          
            $this->db->query("INSERT INTO customer_addresses SET
                            address='".$in['address']."',
                            zip='".$in['zip']."',
                            city='".$in['city']."',
                            country_id='".$in['country_id']."',
                            customer_id='".$in['buyer_id']."',
                            is_primary='1',
                            delivery='1',
                            billing='1' ");
            $in['customer_name'] = $in['name'];
          // include_once('../apps/company/admin/model/customer.php');
            $in['value']=$in['btw_nr'];

            if($in['extra']){
                foreach ($in['extra'] as $key => $value) {
                    $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
                    if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
                        $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
                    }
                }
            }

          // $comp=new customer();       
          // $this->check_vies_vat_number($in);
            if($this->check_vies_vat_number($in) != false){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
            }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
            }

            /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
            $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                    AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);
            if(!$show_info->move_next()) {
              /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                name    = 'company-customers_show_info',
                                value   = '1' ");*/
                $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                name    = :name,
                                value   = :value ",
                            ['user_id' => $_SESSION['u_id'],
                             'name'    => 'company-customers_show_info',
                             'value'   => '1']
                        );
            } else {
              /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
                $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                    AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);
          }
            $this->db->query("UPDATE cash_customers SET created_customer='".$in['buyer_id']."' WHERE customer_id='".$in['customer_id']."' ");
            $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
            if($count == 1){
              doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
            }
            msg::success (gm('Account created'),'success');
            return true;
        }
        if($in['add_individual']){
            $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
            $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
            $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                                name='".$in['lastname']."',
                                                firstname = '".$in['firstname']."',
                                                user_id = '".$acc_manager_ids."',
                                                acc_manager_name = '".addslashes($in['acc_manager'])."',
                                                btw_nr='".$in['btw_nr']."',
                                                vat_regime_id = '".$vat_regime_id."',
                                                city_name = '".$in['city']."',
                                                c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
                                                type = 1,
                                                country_name ='".get_country_name($in['country_id'])."',
                                                active='1',
                                                payment_term      = '".$payment_term."',
                                                payment_term_type     = '".$payment_term_type."',
                                                zip_name  = '".$in['zip']."',
                                                commercial_name         = '".$in['commercial_name']."',
                                                legal_type              = '".$in['legal_type']."',
                                                c_type                  = '".$c_types."',
                                                website                 = '".$in['website']."',
                                                language                = '".$in['language']."',
                                                sector                  = '".$in['sector']."',
                                                comp_fax                = '".$in['comp_fax']."',
                                                activity                = '".$in['activity']."',
                                                comp_size               = '".$in['comp_size']."',
                                                lead_source             = '".$in['lead_source']."',
                                                various1                = '".$in['various1']."',
                                                various2                = '".$in['various2']."',
                                                c_type_name             = '".$c_type_name."',
                                                vat_id                  = '".$selected_vat."',
                                                invoice_email_type      = '1',
                                                sales_rep               = '".$in['sales_rep_id']."',
                                                internal_language       = '".$in['internal_language']."',
                                                is_supplier             = '".$in['is_supplier']."',
                                                is_customer             = '".$in['is_customer']."',
                                                stripe_cust_id          = '".$in['stripe_cust_id']."'");
          
            $this->db->query("INSERT INTO customer_addresses SET
                            address='".$in['address']."',
                            zip='".$in['zip']."',
                            city='".$in['city']."',
                            country_id='".$in['country_id']."',
                            customer_id='".$in['buyer_id']."',
                            is_primary='1',
                            delivery='1',
                            billing='1' ");
            $in['customer_name'] = $in['name'];
          // include_once('../apps/company/admin/model/customer.php');
            $in['value']=$in['btw_nr'];

            if($in['extra']){
                foreach ($in['extra'] as $key => $value) {
                    $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
                    if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
                        $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
                    }
                }
            }
          // $comp=new customer();       
          // $this->check_vies_vat_number($in);
            if($this->check_vies_vat_number($in) != false){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
            }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
              $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
            }

            /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
            $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                    AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                    
            if(!$show_info->move_next()) {
              /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                name    = 'company-customers_show_info',
                                value   = '1' ");*/
                $this->db_users->insert("INSERT INTO user_meta SET  user_id =  :user_id,
                                name    = :name,
                                value   = :value ",
                            ['user_id' => $_SESSION['u_id'],
                             'name'    => 'company-customers_show_info',
                             'value'   => '1']
                        );
            } else {
              /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                    AND name    = 'company-customers_show_info' ");*/
                $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                    AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);
          }
           $this->db->query("UPDATE cash_customers SET created_customer='".$in['buyer_id']."' WHERE customer_id='".$in['customer_id']."' ");
            $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
            if($count == 1){
              doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
            }
            msg::success (gm('Account created'),'success');
            return true;
        }
        if($in['add_contact']){
          $customer_name='';
          if($in['buyer_id']){
            $customer_name = $this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' ");
          }
            $in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
                                                  firstname='".$in['firstname']."',
                                                  lastname='".$in['lastname']."',
                                                  sex='".$in['gender_id']."',
                                                  title ='".$in['title_id']."',
                                                  language ='".$in['language_id']."',
                                                  `create`  = '".time()."',
                                                  customer_id = '".$in['buyer_id']."',
                                                  company_name= '".$customer_name."',
                                                  email='".$in['email']."',
                                                  cell='".$in['cell']."' ");
            $this->db->query("INSERT INTO customer_contactsIds SET customer_id='".$in['buyer_id']."', contact_id='".$in['contact_id']."' ");
            $in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
            if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
                $vars=array();
                if($in['buyer_id']){
                    $vars['customer_id']=$in['buyer_id'];
                    $vars['customer_name']=$customer_name;
                }          
                $vars['contact_id']=$in['contact_id'];
                $vars['firstname']=$in['firstname'];
                $vars['lastname']=$in['lastname'];
                $vars['table']='customer_contacts';
                $vars['email']=$in['email'];
                $vars['op']='add';
                synctoZendesk($vars);
            }
            if($in['country_id']){
              $this->db->query("INSERT INTO customer_contact_address SET
                              address='".$in['address']."',
                              zip='".$in['zip']."',
                              city='".$in['city']."',
                              country_id='".$in['country_id']."',
                              contact_id='".$in['contact_id']."',
                              is_primary='1',
                              delivery='1' ");
            }
            /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                        AND name    = 'company-contacts_show_info'  ");*/
            $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                        AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);
            if(!$show_info->move_next()) {
              /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                      name    = 'company-contacts_show_info',
                                      value   = '1' ");*/
                $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                      name    = :name,
                                      value   = :value ",
                                ['user_id' => $_SESSION['u_id'],
                                 'name'    => 'company-contacts_show_info',
                                 'value'   => '1']
                            );
            } else {
              /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                          AND name    = 'company-contacts_show_info' ");*/
                $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                          AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);
            }
            $this->db->query("UPDATE cash_customers SET created_contact='".$in['contact_id']."' WHERE customer_id='".$in['customer_id']."' ");
            $count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
            if($count == 1){
              doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
            }
            msg::success (gm('Contact created'),'success');
        }
        return true;
      }

      /**
      * undocumented function
      *
      * @return void
      * @author PM
      **/
      function tryAddCValid(&$in)
      {
        if($in['add_customer']){
            $v = new validation($in);
          $v->field('name', 'name', 'required:unique[customers.name]');
          $v->field('country_id', 'country_id', 'required');
          return $v->run();  
        }
        if($in['add_contact']){
          $v = new validation($in);
          $v->field('firstname', 'firstname', 'required');
          $v->field('lastname', 'lastname', 'required');
          $v->field('buyer_id', 'buyer_id', 'required');
/*        $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
          $v->field('country_id', 'country_id', 'required');*/
          return $v->run();  
        }
        return true;
      }

    function check_vies_vat_number(&$in){
        $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

        if(!$in['value'] || $in['value']==''){      
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }
        $value=trim($in['value']," ");
        $value=str_replace(" ","",$value);
        $value=str_replace(".","",$value);
        $value=strtoupper($value);

        $vat_numeric=is_numeric(substr($value,0,2));

        if(!in_array(substr($value,0,2), $eu_countries)){
          $value='BE'.$value;
        }
        if(in_array(substr($value,0,2), $eu_countries)){
          $search   = array(" ", ".");
          $vat = str_replace($search, "", $value);
          $_GET['a'] = substr($vat,0,2);
          $_GET['b'] = substr($vat,2);
          $_GET['c'] = '1';
          $dd = include('../valid_vat.php');
        }else{
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }
        if(isset($response) && $response == 'invalid'){
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }else if(isset($response) && $response == 'error'){
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }else if(isset($response) && $response == 'valid'){
          $full_address=explode("\n",$result->address);
          switch($result->countryCode){
            case "RO":
              $in['comp_address']=$full_address[1];
              $in['comp_city']=$full_address[0];
              $in['comp_zip']=" ";
              break;
            case "NL":
              $zip=explode(" ",$full_address[2],2);
              $in['comp_address']=$full_address[1];
              $in['comp_zip']=$zip[0];
              $in['comp_city']=$zip[1];
              break;
            default:
              $zip=explode(" ",$full_address[1],2);
              $in['comp_address']=$full_address[0];
              $in['comp_zip']=$zip[0];
              $in['comp_city']=$zip[1];
              break;
          }

          $in['comp_name']=$result->name;

          $in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
          $in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
          $in['full_details']=$result;
          $in['vies_ok']=1;
          if($in['customer_id']){
            $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
            if($in['comp_country'] != $country_id){
                    $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
                    $this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
                    $in['remove_v']=1;
                }else if($in['comp_country'] == $country_id){
                    $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
                    $this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
                    $in['add_v']=1;
                }
          }
          if(ark::$method == 'check_vies_vat_number'){
            msg::success ( gm('VAT Number is valid'),'success');
            json_out($in);
          }
          return true;
        }else{
          if(ark::$method == 'check_vies_vat_number'){
            msg::error ( gm('Error'),'error');
            json_out($in);
          }
          return false;
        }
        if(ark::$method == 'check_vies_vat_number'){
          json_out($in);
        }
    }

    function bsyncArticle(&$in){
        if($in['multiple']){
            $filter=" article_category_id='".$in['id']."' ";
        }else{
            $filter=" article_id='".$in['article_id']."' ";
        }
        $this->db->query("UPDATE pim_articles SET cash_bsync='".$in['value']."' WHERE ".$filter." ");
        if($in['multiple'] || $in['bulk']){
            return true;
        }else{
            json_out($in);
        }
    }

    function bsyncFamily(&$in){
        $this->db->query("UPDATE pim_article_categories SET cash_bsync='".$in['value']."' WHERE id='".$in['id']."' ");
        $in['multiple']=1;
        $this->bsyncArticle($in);
        if($in['bulk']){
            return true;
        }else{
            json_out($in);
        }
    }

    function bulksyncArticle(&$in){
        foreach($in['article_ids'] as $key=>$value){
            $in['bulk']=1;
            $in['article_id']=$value['article_id'];
            $in['value']=$value['value'];
            $this->bsyncArticle($in);
        }
        json_out($in);
    }

    function bulksyncFamily(&$in){
        foreach($in['ids'] as $key=>$value){
            $in['bulk']=1;
            $in['id']=$value['id'];
            $in['value']=$value['value'];
            $this->bsyncFamily($in);
        }
        json_out($in);
    }

    function sendBulkArticle(&$in){
        $in['lang_id'] = $this->db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1'");
        if(!$in['lang_id']){
            $in['lang_id']=1;
        }
        $articles = $this->db->query("SELECT pim_articles.stock, pim_articles.item_code,pim_articles.internal_name,pim_articles.price, pim_articles.weight, pim_articles.cash_article_id,vats.value AS vat_value , vats.cash_tax_id, pim_articles_lang.description AS description,pim_article_categories.cash_category_id,pim_article_photo.file_name as photo_link,pim_articles.cash_bsync FROM pim_articles
                LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
                LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
                LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
                LEFT JOIN pim_article_photo ON pim_articles.article_id=pim_article_photo.parent_id
                WHERE pim_articles.article_id='".$in['article_id']."' ");
        if($articles->f('cash_bsync')){
            json_out($in);
        }
        while($articles->move_next()){
            $taxes=array();
            if($articles->f('cash_tax_id')!=''){
                array_push($taxes, $articles->f('cash_tax_id'));
            }
            $cat_id=$this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category='1' ");
            $default_price=$this->db->field("SELECT price FROM pim_article_prices WHERE price_category_id='".$cat_id."' AND article_id='".$in['article_id']."' ");
            if(is_null($default_price)){
                $price=$articles->f('price');
                $price_vat=$articles->f('price')+($articles->f('price')*$articles->f('vat_value')/100);
            }else{
                $price=$default_price;
                $price_vat=$this->db->field("SELECT total_price FROM pim_article_prices WHERE price_category_id='".$cat_id."' AND article_id='".$in['article_id']."' ");
            }           
            $in['description']       = $articles->f('description');
            $in['name']              = $articles->f('internal_name');
            $in['stock']             = remove_zero_decimals($articles->f('stock'));
            $in['price']             = display_number_var_dec($price);
            $in['price_vat']         = display_number_var_dec($price_vat);
            $in['code']              = $articles->f('item_code');
            $in['sync_done']         = $articles->f('cash_article_id')!='' ? '1' : '';
            $in['cash_article_id']   = $articles->f('cash_article_id');
            $in['weight']            = $articles->f('weight');
            $in['taxes']             = $taxes;
            $in['photo_link']        = $articles->f('photo_link');
            $in['cash_category_id']  = $articles->f('cash_category_id');
        }
        $this->sendArticle($in);
    }

    function sendBulkCategory(&$in){
        $categories=$this->db->query("SELECT * FROM pim_article_categories WHERE id='".$in['id']."' ");
        if($categories->f('cash_bsync')){
            json_out($in);
        }
        $in['name']               = $categories->f('name');
        $in['sync_done']          = $categories->f('cash_category_id')!='' ? '1' : '';
        $in['photo_link']         = $categories->f('file_name');
        $in['cash_category_id']   = $categories->f('cash_category_id');
        $this->sendCategory($in);
    }

}
?>