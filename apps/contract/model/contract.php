<?php
/**
 * undocumented class
 *
 * @package default
 * @author Mp
 **/
class contract
{

	var $pag = 'contract';
	var $field_n = 'contract_id';
	private $db;
	private $dbu;
	private $db2;# holds the pdo object

	function __construct() {

		$this -> db = new sqldb();
		$this -> db2 = new sqldb();
		$this -> dbu = new sqldb();
			global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$this->db_users = new sqldb($db_config);
	}
	
	/****************************************************************
	* function archive(&$in)                                          *
	****************************************************************/
	function archive(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("UPDATE contracts SET active=0 WHERE contract_id='".$in['contract_id']."'");
		$tracking_trace=$this->db->field("SELECT trace_id FROM contracts WHERE contract_id='".$in['contract_id']."' ");
	    if($tracking_trace){
	      $this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
	    }
		msg::success(gm("Contract has been archived"),"success");
		insert_message_log($this->pag,'{l}Contract has been archieved by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contract_id'],false);
		return true;
	}
	/****************************************************************
	* function delete_validate(&$in)                                          *
	****************************************************************/
	function delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contract_id', gm('ID'), 'required:exist[contracts.contract_id]', gm("Invalid ID"));
		return $v->run();
	}
	/****************************************************************
	* function activate(&$in)                                          *
	****************************************************************/
	function activate(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}
		$contract = $this->db->query("SELECT name, customer_id FROM contracts WHERE contract_id='".$in['contract_id']."' ");
		$contract->next();
		$querys['Dublicate contract name for the same company. Please change this project name'] = "SELECT name FROM contracts WHERE contract_id!='".$in['contract_id']."' AND name='".$contract->f('name')."' AND customer_id='".$contract->f('customer_id')."' ";
		if(!$this->verify($querys)){
			return true;
		}
		$this->db->query("UPDATE contracts SET active=1 WHERE contract_id='".$in['contract_id']."'");
		$tracking_trace=$this->db->field("SELECT trace_id FROM contracts WHERE contract_id='".$in['contract_id']."' ");
	    if($tracking_trace){
	      $this->db->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
	    }
		msg::success(gm("Contract has been successfully activated"),"success");
		insert_message_log($this->pag,'{l}Contract has been successfully activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contract_id'],false);
		return true;
	}

	function verify($querys=array())
	{
		foreach ($querys as $key => $value) {
			$this->db->query($value);
			if($this->db->move_next()){
				msg::notice($key,"notice");
				return false;
			}
		}
		return true;
	}

	/****************************************************************
	* function delete(&$in)                                          *
	****************************************************************/
	function delete(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}
		$tracking_trace=$this->db->field("SELECT trace_id FROM contracts WHERE contract_id='".$in['contract_id']."' ");
		$this->db->query("DELETE FROM contracts WHERE contract_id='".$in['contract_id']."'");
		$this->db->query("SELECT * FROM projects WHERE contract_id='".$in['contract_id']."'");
		while ( $this->db->move_next()) {
		        $this->db2->query("DELETE FROM project_user WHERE project_id='".$this->db->f('project_id')."'");
		        $this->db2->query("DELETE FROM tasks WHERE project_id='".$this->db->f('project_id')."'");
		}

        $this->db->query("DELETE FROM projects WHERE contract_id='".$in['contract_id']."'");

        $this->db->query("SELECT contract_invoice_id  FROM  contract_invoice WHERE contract_id='".$in['contract_id']."'");
		while ( $this->db->move_next()) {
		        $this->db2->query("DELETE FROM contract_invoice_email_contact WHERE contract_invoice_id='".$this->db->f('contract_invoice_id')."'");

		}
        $this->db->query("DELETE FROM contract_invoice WHERE contract_id='".$in['contract_id']."'");

        $this->db->query("DELETE FROM mfm_contract_line WHERE contract_id='".$in['contract_id']."'");


        $this->db->query("DELETE FROM contracts_group WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM contract_line WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM contracts_version WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM contracts_history WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM logging WHERE field_name='contract_id' AND field_value='".$in['contract_id']."' ");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'contract' AND item_name = 'notes' AND item_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'contract' AND item_name = 'free_text_content' AND item_id = '".$in['contract_id']."'");
		if($tracking_trace){
		      $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
		      $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
		}
		msg::success(gm("Contract deleted."),"success");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return boolean
	 * @author PM
	 **/
	function updateCustomerData(&$in)
	{
		if($in['field'] == 'contact_id'){
			$in['buyer_id'] ='';
		}
		if($in['contract_id']){
		    $buyer_exist = $this->db->field("SELECT customer_id FROM contracts WHERE contract_id='".$in['contract_id']."'");
		    if($buyer_exist!=$in['buyer_id']){
		      $in['save_final']=1;
		    }elseif($buyer_exist==$in['buyer_id']){
		      $in['save_final']=0;
		    }
		}

		$sql = "UPDATE contracts SET ";
		if($in['buyer_id']){
			$buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, 
									customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, 
									customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount, customer_addresses.address_id
									FROM customers
									LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
									AND customer_addresses.is_primary=1
									WHERE customers.customer_id='".$in['buyer_id']."' ");
			$buyer_info->next();
			/*if(!$in['delivery_address']){
				$delivery_addr = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 LIMIT 1 ");
				if($delivery_addr){
					$in['delivery_address'] = $delivery_addr->f('address')."\n".$delivery_addr->f('zip').'  '.$delivery_addr->f('city')."\n".get_country_name($delivery_addr->f('country_id'));
					$in['delivery_address_id']=$delivery_addr->f('address_id');
				}
			}*/

			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$apply_discount = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$apply_discount = 2;
				}
				if($buyer_info->f('apply_line_disc')){
					$apply_discount = 1;
				}
			}
			$in['apply_discount'] = $apply_discount;
		    
			if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
				$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
			}
			$in['currency_rate']=0;
			$in['currency_id']	= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
			if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
				$in['currency_rate'] = $this->currencyRate(currency::get_currency($in['currency_id'],'code'));
			}
			$in['contact_name'] ='';
			
			//Set email language as account / contact language
			$emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
		    						  'contact_id' 		=> $in['contact_id'],
		    						  'item_id'			=> $in['contract_id'],
		    						  'email_language' 	=> $in['email_language'],
		    						  'table'			=> 'contracts',
		    						  'table_label'		=> 'contract_id',
		    						  'table_buyer_label' => 'customer_id',
		    						  'table_contact_label' => 'contact_id',
		    						  'param'		 	=> 'update_customer_data');
			$in['email_language'] = get_email_language($emailMessageData);
		    //End Set email language as account / contact language

			$sql .= " customer_id = '".$in['buyer_id']."', ";
			$sql .= " customer_name = '".addslashes($buyer_info->f('name'))."', ";
			$sql .= " main_address_id = '".$in['main_address_id']."', ";
			$sql .= " email_language = '".$in['email_language']."', ";
			// $sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
			$sql .= " currency_type = '".$in['currency_id']."', ";
			$sql .= " currency_rate = '".$in['currency_rate']."', ";
			$sql .= " vat = '".get_customer_vat($in['buyer_id'])."', ";
			$sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
			// $sql .= " customer_ref = '".addslashes($buyer_info->f('our_reference'))."', ";
			
			if($in['contact_id']){
				$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
				// $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
				$sql .= " contact_id = '".$in['contact_id']."', ";
				$in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
			}else{
				// $sql .= " contact_name = '', ";
				$sql .= " contact_id = '', ";
			}
			$in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
			if($buyer_info->f('address_id') != $in['main_address_id']){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
				$in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
			}

			if($in['sameAddress']==1){
		        $sql .= " same_address = '0', ";
		        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
		      }else{
		        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
		        $new_address->next();
		        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
		        $sql .= " same_address = '".$in['delivery_address_id']."', ";
		        $sql .= " free_field = '".addslashes($new_address_txt)."' ";      
		      }
			// $sql .= " field = 'customer_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '".$in['apply_discount']."' ";
				// $sql .= ",  remove_vat = '".$buyer_info->f('no_vat')."' ";
				$sql .= ",  discount_line_gen = '".$buyer_info->f('line_discount')."' ";
				$sql .= ",  discount = '".$buyer_info->f('fixed_discount')."' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => $value['price'], 
						'quantity' => $value['quantity'], 
						'customer_id' => $in['buyer_id'],
						'cat_id' => $buyer_info->f('cat_id'),
						'asString' => true
					);
					$in['article'][$key]['price'] = display_number($this->getArticlePrice($params));
				}
			}
		}else{
			if(!$in['contact_id']){
				msg::error ( gm('Please select a company or a contact'),'error');
				return false;
			}
			$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
			$sql .= " customer_id = '', ";
			$sql .= " customer_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
			// $sql .= " contact_name = '', ";
			$sql .= " contact_id = '".$in['contact_id']."', ";
			$sql .= " email_language = '".DEFAULT_LANG_ID."', ";
			// $sql .= " delivery_address = '', ";
			// $sql .= " delivery_address_id = '', ";
			$sql .= " identity_id = '".$in['identity_id']."', ";
			$sql .= " currency_type = '".ACCOUNT_CURRENCY_TYPE."', ";
			$sql .= " currency_rate = '0', ";
			// $sql .= " customer_ref = '', ";
			$in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
			if($in['sameAddress']==1){
		        $sql .= " same_address = '0', ";
		        $sql .= " free_field = '".addslashes($in['address_info'])."' ";
		      }else{
		        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
		        $new_address->next();
		        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
		        $sql .= " same_address = '".$in['delivery_address_id']."', ";
		        $sql .= " free_field = '".addslashes($new_address_txt)."' ";      
		      }
			// $sql .= " field = 'contact_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '0' ";
				// $sql .= ",  remove_vat = '0' ";
				$sql .= ",  discount_line_gen = '0' ";
				$sql .= ",  discount = '0' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => return_value($value['price']), 
						'quantity' => return_value($value['quantity']),
						'cat_id' => '0',
						'asString' => true
					);
					$in['article'][$key]['price'] = display_number($this->getArticlePrice($params));
				}
			}
		}

		$sql .=" WHERE contract_id ='".$in['item_id']."' ";

		if(!$in['isAdd']){
			$this->db->query($sql);
			if($in['item_id'] && is_numeric($in['item_id'])){
		        $trace_id=$this->db->field("SELECT trace_id FROM contracts WHERE contract_id='".$in['item_id']."' ");
		        if($trace_id && $in['buyer_id']){
		          $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
		        }
		      }			
		}

		$in['contract_id'] = $in['item_id'];

		msg::success(gm('Sync successfull.'),"success");
		return true;
	}

	function currencyRate($currency=''){
		if(empty($currency)){
			$currency = "USD";
		}
		$separator = ACCOUNT_NUMBER_FORMAT;
		$into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		return currency::getCurrency($currency, $into, 1,$separator);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
		if(!$this->CanEdit($in)){
			json_out($in);
			return false;
		}
		if(!$this->tryAddAddressValidate($in)){
			json_out($in);
			return false;
		}
		$country_n = get_country_name($in['country_id']);
		if($in['field']=='customer_id'){
	 		Sync::start(ark::$model.'-'.ark::$method);

			$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																				customer_id			=	'".$in['customer_id']."',
																				country_id			=	'".$in['country_id']."',
																				state_id			=	'".$in['state_id']."',
																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				billing				=	'".$in['billing']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
			if($in['billing']){
				$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}

			Sync::end($address_id);

			/*if($in['delivery']){
				$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}*/
			if($in['primary']){
				if($in['address'])
				{
					if(!$in['zip'] && $in['city'])
					{
						$address = $in['address'].', '.$in['city'].', '.$country_n;
					}elseif(!$in['city'] && $in['zip'])
					{
						$address = $in['address'].', '.$in['zip'].', '.$country_n;
					}elseif(!$in['zip'] && !$in['city'])
					{
						$address = $in['address'].', '.$country_n;
					}else
					{
						$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
					}
					$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
					$output= json_decode($geocode);
					$lat = $output->results[0]->geometry->location->lat;
					$long = $output->results[0]->geometry->location->lng;
				}
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
				$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
				$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
			}
		}else{			
			$address_id = $this->db->insert("INSERT INTO customer_contact_address SET
																				contact_id			=	'".$in['contact_id']."',
																				country_id			=	'".$in['country_id']."',																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
		}
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['address_id'] = $address_id;
		$in['country'] = $country_n;
		$in['contract_id'] = $in['item_id'];
		if($in['contract_id'] && is_numeric($in['contract_id'])){
			$delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
			$this->db->query("UPDATE contracts SET 
	  			delivery_address='".$delivery_address."', 
	  			delivery_address_id='".$address_id."' WHERE id='".$in['contract_id']."'  ");	
	  	}elseif ($in['contract_id'] == 'tmp') {
	  		$in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	  		$in['delivery_address_id'] = $address_id;
	  	}
	  	$in['buyer_id'] = $in['customer_id'];
		// $in['pagl'] = $this->pag;
		
		// json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddressValidate(&$in)
	{
		$v = new validation($in);
		if($in['customer_id']){
			$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		      if(!$in['primary'] && !$in['billing'] && !$in['site']){
		        $v->field('primary', 'primary', 'required',gm("Main Address is mandatory"));
		        $v->field('delivery', 'delivery', 'required',gm("Delivery Address is mandatory"));
		        $v->field('billing', 'billing', 'required',gm("Billing Address is mandatory"));
		        $v->field('site', 'site', 'required',gm("Site Address is mandatory"));

		        $message = 'You must select an address type.';
		        $is_ok = $v->run();
		        if(!$is_ok){
		          msg::error(gm($message),'error');
		          return false;   
		        }
		      } else {
		        $v->field('address', 'Address', 'required:text',gm("Street is mandatory"));
		        $v->field('zip', 'Zip', 'required:text',gm("Zip Code is mandatory"));
		        $v->field('city', 'City', 'required:text',gm("City is mandatory"));
		        $v->field('country_id', 'Country', 'required:exist[country.country_id]');
		      }
		}else if($in['contact_id']){
			$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id",gm("Country is mandatory"));
		}else{
			msg::error(gm('Invalid ID'),'error');
			return false;
		}
		$v->field('country_id', 'Country', 'required:exist[country.country_id]', gm("Country is mandatory"));

		return $v->run();
	}

 	function CanEdit(&$in){
	    if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
	      return true;
	    }
	    $c_id = $in['customer_id'];
	    if(!$in['customer_id'] && $in['contact_id']) {
	      $c_id = $this->dbu->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	    }
	    if($c_id){
	      if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
	        $u = $this->dbu->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
	        if($u){
	          $u = explode(',', $u);
	          if(in_array($_SESSION['u_id'], $u)){
	            return true;
	          }
	          else{
	            msg::$warning = gm("You don't have enought privileges");
	            return false;
	          }
	        }else{
	          msg::$warning = gm("You don't have enought privileges");
	          return false;
	        }
	      }
	    }
	    return true;
  	}

	/****************************************************************
	* function add(&$in)                                          *
	****************************************************************/
	function add(&$in)
	{
		$f_archived = '1';
		//$serial_number=$in['serial_number'];
		if($in['template']){
			$f_archived='2';
			if(!$in['template_name']){
				msg::error (gm("Template name required"),'error');
				return false;
			}
			$serial_number=$in['template_name'];
		}else{
			if(!$this->add_validate($in))
			{
				json_out($in);
				return false;
			}
			//$serial_number = $in['serial_number'];
			$serial_number = generate_contract_number(DATABASE_NAME);
		}

		if(!$in['currency_type']){
			$in['currency_type'] = ACCOUNT_CURRENCY_TYPE;
		}

		$created_date= date(ACCOUNT_DATE_FORMAT);
		if(!$in['pdf_layout'] && $in['identity_id']){
	      $in['pdf_layout'] = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_id']."' and module='contracts'");
	    }

	    if( $in['main_address_id'] ){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$in['free_field'] = addslashes($buyer_addr->f('address'))."\n".addslashes($buyer_addr->f('zip')).'  '.addslashes($buyer_addr->f('city'))."\n".get_country_name($buyer_addr->f('country_id'));
		}

        $reminder_date=$in['reminder_date'];
		if($in['end_date']){
            if($in['reminder_type']==1){
			   $reminder_date=strtotime("-1 month", $in['end_date']);
		    }
		     if($in['reminder_type']==2){
			   $reminder_date=strtotime("-2 month", $in['end_date']);
		    }
		}

		if(!$in['quote_id']){
			$in['discount_line_gen'] = 0;
			$in['discount'] = 0;
		}	
		if(!$in['is_contact'] ){
		    $buyer_info = $this->db->query("SELECT customers.*,customer_addresses.*
			                    	FROM customers
			                    	LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.is_primary=1
	                            	WHERE customers.customer_id='".$in['customer_id']."'");
		    $in['remove_vat'] = $buyer_info->f('no_vat');
		    $in['vat'] = $buyer_info->f('vat');

		    if(!$in['quote_id']){
		    	$in['discount_line_gen'] = $buyer_info->f('line_discount');
				$apply_discount = 0;
				if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
					$apply_discount = 3;
				}else{
					if($buyer_info->f('apply_fix_disc')){
						$apply_discount = 2;
					}
					if($buyer_info->f('apply_line_disc')){
						$apply_discount = 1;
					}
				}
				$in['apply_discount'] = $apply_discount;
				$in['discount'] = $buyer_info->f('fixed_discount');
		    }		

			$same_address=0;
		    if($in['sameAddress']!=1){
		    	$same_address=$in['delivery_address_id'];
		    }

		    $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
		      if($buyer_info->f('address_id') != $in['main_address_id']){
		        $buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
		        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
		      }
		      if($in['sameAddress']==1){
		        $in['free_field']=addslashes($in['address_info']);
		      }else{
		        $new_address=$this->db->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
		        $new_address->next();
		        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
		        $in['free_field']=addslashes($new_address_txt);      
		      }

	     }

	     if(!$in['unlimited_period']){
			$in['unlimited_period'] = 0;
		}
		if($in['unlimited_period']==1){
			$in['end_date'] = 0;
		}

		/*if(!$in['deal_id']){
	      $in['identity_id']= $this->db_users->field("SELECT default_identity_id FROM user_info WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	    }else{
	      $in['identity_id']=0;
	    }*/

		//Set email language as account / contact language
	    $emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
		    						  'contact_id' 		=> $in['contact_id'],
		    						  'param'		 	=> 'add');
		$in['email_language'] = get_email_language($emailMessageData);
	    //End Set email language as account / contact language

		$in['contract_id'] = $this->db->insert("INSERT INTO contracts SET
							customer_id    				= '".$in['buyer_id']."',
							name	         				= '".$in['name']."',
							is_contact					= '0',
							our_reference					= '".$in['our_reference']."',
							customer_name					= '".$in['buyer_name']."',
							serial_number					= '".$serial_number."',
							currency_type					= '".$in['currency_type']."',
							email_language				= '".$in['email_language']."',
							type					        = '".$in['type']."',
							vat                           = '".$in['vat']."',
							remove_vat                    = '".$in['remove_vat']."',
							discount      	    		= '".$in['discount']."',
							apply_discount				= '".$in['apply_discount']."',
							quote_id 						= '".$in['quote_id']."',
							budget_type	 		    = '".$in['budget_type']."',
							t_ct_hour	     		= '".$in['t_ct_hour']."',
							send_email_alerts	    = '".$in['send_email_alerts']."',
							alert_percent	        = '".$in['alert_percent']."',
							reminder_type          = '".$in['reminder_type']."',
							invoicing_type         = '".$in['invoicing_type']."',
							reminder_date          = '".$reminder_date."',
							sent 					 = '0',
							no_end_date				= '".$in['unlimited_period']."',
							your_ref				= '".$in['your_ref']."',  
							discount_line_gen			= '".$in['discount_line_gen']."',
							status              		=   '0',
							contact_id				=   '".$in['contact_id']."',
							free_field 			=   '".$in['free_field']."',
                            main_address_id		=   '".$in['main_address_id']."',
                            same_address					='".$same_address."',
                            subject         			=   '".$in['subject']."',
                            use_package             	=   '1',
                            use_sale_unit           	=   '1',
                            show_vat_pdf				= 	'".$in['show_vat_pdf']."',
                            show_grand				= 	'".$in['show_grand']."',
                            pdf_layout				=   '".$in['pdf_layout']."',
                            preview 					=	'0',
                            show_QC					=	'".$in['show_QC']."',
                            show_UP					=	'".$in['show_UP']."',
                            show_V					=	'".$in['show_V']."',
                            show_PS					=	'".$in['show_PS']."',
                            identity_id 				='".$in['identity_id']."',
                            author_id					= 	'".$_SESSION['u_id']."',
                            acc_manager_id           	=	'".$in['acc_manager_id']."',
                            general_conditions_new_page			=	'0',
                            vat_regime_id 		='".$in['vat_regime_id']."',
                            installation_id 						='".$in['installation_id']."',
                            active='".$f_archived."',
                            source_id='".$in['source_id']."',
                            type_id='".$in['type_id']."',
                            segment_id='".$in['segment_id']."' ");

		if($in['general_conditions_new_page']){
			$this->db->query("UPDATE contracts SET general_conditions_new_page='1' WHERE contract_id='".$in['contract_id']."' ");
		}
		//version
		include_once(__DIR__.'/contract_version.php');
		$version = new contract_version();
		$version->add($in);
		$this->contract_lines($in);

        $lang_note_id = $in['langs'];
		// add multilanguage quote notes in note_fields  DEFAULT_LANGS
		  $lang_note_id = $in['email_language'];
          if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }

		switch ($in['email_language']){
			case '1':
				$lang = '';
				break;
			case '2':
				$lang = '_2';
				break;
			case '3':
				$lang = '_3';
				break;
			case '4':
				$lang = '_4';
				break;
			default:
				$lang = '';
				break;
		}

		//Add default notes values based on language
      	$emailLanguageDefaultNotesData = array('email_language' => $in['email_language'],
          										'default_name_note'	=> 'contract_note',
          										'default_type_note'	=> 'contract_note',
          										'default_name_free_text_content' => 'contract_term',
          										'default_type_free_text_content' => 'contract_terms',
          									);

        $defaultNotesData = get_email_message_default_notes($emailLanguageDefaultNotesData);
        //End Add default notes values based on language

		// $default_table =  $this->db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='contract_note".$lang."' ");

          $this->db->query("INSERT INTO default_data SET value='".addslashes($defaultNotesData['notes'])."', active = '1', type='contract_note".$lang."', default_main_id='0', default_name='contract_note' ");

/*          $this->db->insert("INSERT INTO note_fields SET
                            active 			=	'1',
                            item_id         = 	'".$in['contract_id']."',
                            lang_id         =   '".$in['email_language']."',
                            item_type       =   'contract',
                            item_name       =   'notes',
                            version_id 		= 	'".$in['version_id']."',
                            item_value      =   '".$in['notes']."'
        ");*/

         if($in['free_text_content']){
			$this->db->query("INSERT INTO note_fields SET
				item_value				= 	'".addslashes($defaultNotesData['free_text_content'])."',
				item_type 				= 	'contract',
				item_name 				= 	'free_text_content',
				version_id 				= 	'".$in['version_id']."',
				item_id 				= 	'".$in['contract_id']."' ");
		}

  		//insert a project and task with contract info
        if(!$in['is_contact']){
            $in['project_id'] = $this->db->insert("INSERT INTO projects SET contract_id='".$in['contract_id']."', customer_id='".$in['buyer_id']."', name='".$in['serial_number']."',budget_type=4,send_email_alerts='".$in['send_email_alerts']."',alert_percent='".$in['alert_percent']."',t_pr_hour='".$in['t_ct_hour']."' ,active='2', billable_type='1', invoice_method='1', company_name=(SELECT name FROM customers WHERE customer_id='".$in['buyer_id']."') ");
        }else{
			$in['project_id'] = $this->db->insert("INSERT INTO projects SET contract_id='".$in['contract_id']."', customer_id='".$in['buyer_id']."', name='".$in['serial_number']."',budget_type=4,send_email_alerts='".$in['send_email_alerts']."',alert_percent='".$in['alert_percent']."',t_pr_hour='".$in['t_ct_hour']."' ,active='2', billable_type='1', invoice_method='1', company_name=(SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$in['buyer_id']."') ");
		}

		
		if($in['deal_id']){
			$tracking_data=array(
		            'target_id'         => $in['contract_id'],
		            'target_type'       => '9',
		            'target_buyer_id'   => $in['buyer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
		                                    )
		          );
		      addTracking($tracking_data);
		}else if($in['quote_id']){
			$tracking_data=array(
		            'target_id'         => $in['contract_id'],
		            'target_type'       => '9',
		            'target_buyer_id'   => $in['buyer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['quote_id'],'origin_type'=>'2')
		                                    )
		          );
		      addTracking($tracking_data);
		}else{
			$tracking_data=array(
			      'target_id'         => $in['contract_id'],
			      'target_type'       => '9',
			      'target_buyer_id'   => $in['buyer_id'],
			       'lines'             => array()
			);
			addTracking($tracking_data);
		}

		//$u_name = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$u_name = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."',manager=1, user_id='".$_SESSION['u_id']."', user_name='".addslashes($u_name)."' ");
		$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".$in['serial_number']."', billable='1', t_hours='".$in['t_ct_hour']."' ");

		set_first_letter('contracts',$in['buyer_name'],$this->field_n,$in['contract_id']);
        msg::success(gm("Contract added"),"success");
		// $in['new']='1';
        insert_message_log($this->pag,'{l}Contract added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contract_id'],false);

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
																			AND name	= 'contract-contracts_show_info'	");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
																			AND name	= :name	",
																		['user_id'=>$_SESSION['u_id'],
																		 'name'=>'contract-contracts_show_info']
																	);
		if(!$show_info->move_next()) {
			/*$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name 	= 'contract-contracts_show_info',
																value 	= '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET 	user_id = :user_id,
																name 	= :name,
																value 	= :value ",
															['user_id' => $_SESSION['u_id'],
															 'name' 	=> 'contract-contracts_show_info',
															 'value' 	=> '1']
														);																
		} else {
			/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
															   		 AND name 	   = 'contract-contracts_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
															   		 AND name 	   = :name ",
															   	['value'=>'1',
															   	 'user_id'=>$_SESSION['u_id'],
															   	 'name'=>'contract-contracts_show_info']
															);															   		 
		}
		if($in['template']){
			msg::success (gm("Saved"),'success');
			$in['template_id'] = $in['contract_id'];
		}
		$count = $this->db->field("SELECT COUNT(contract_id) FROM contracts ");
	    if($count == 1){
	      doManageLog('Created the first contract.');
	    }
		return true;
	}

	/****************************************************************
	* function add_validate(&$in)                                   *
	****************************************************************/
	function add_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);

		//$v->field('name', gm('Name'), 'required');

		if($in['do']=='contract-contract-contract-add'){
			$v->field('name', gm('Name'), "unique[contracts.name.( customer_id='".$in['customer_id']."' AND contracts.active='1' )]");
			//$v->field('serial_number', gm('Contract number'), 'required:unique[contracts.serial_number]');
		}else{
			$v->field('name', gm('Name'), "unique[contract.name.( contract_id!='".$in['contract_id']."' AND customer_id='".$in['customer_id']."' AND contracts.active='1' )]");
			$v->field('serial_number', gm('Contract number'), "required:unique[contracts.serial_number.( contract_id!='".$in['contract_id']."')]");
		}
		if(!$v->run()){
			$is_ok = false;
		}else{
			if(!$in['buyer_id']){
				msg::error(gm('Invalid customer name. Please select one from the list.'),"error");
				$is_ok = false;
			}
			if($in['main_c_id']){
				$filter .=" AND contracts_version.main_c_id!= '".$in['main_c_id']."' ";
			}
			if($in['do']=='contract-contract-contract-update') {
				$filter = " AND contracts.contract_id !='".$in['contract_id']."' AND contracts.active='1' ";
				$this->db->query("SELECT contracts.contract_id FROM contracts INNER JOIN contracts_version ON contracts.contract_id=contracts_version.contract_id WHERE contracts.serial_number='".$in['serial_number']."' and contracts.serial_number!='' $filter");
				if($this->db->move_next()){
					msg::error(gm("Contract Nr. already used"),'error');
					$is_ok=false;
				}
			}

			/*$this->db->query("SELECT contracts.contract_id FROM contracts INNER JOIN contracts_version ON contracts.contract_id=contracts_version.contract_id WHERE contracts.serial_number='".$in['serial_number']."' and contracts.serial_number!='' $filter");
			if($this->db->move_next()){
				msg::error(gm("Contract Nr. already used"),'error');
				$is_ok=false;
			}*/
		}

		return $is_ok;

	}

	private function contract_lines(&$in){
		
		//INSERT LINES
		// $lines_type = array('6','7');
		$total_amount=0;
		$total_amount_vat=0;
		$use_package=$this->db->field("SELECT use_package FROM contracts WHERE contract_id='".$in['contract_id']."'");
		$use_sale_unit=$this->db->field("SELECT use_sale_unit FROM contracts WHERE contract_id='".$in['contract_id']."'");

		if(is_array($in['quote_group']) && !empty($in['quote_group'])){
			$group_order = 0;
			foreach($in['quote_group'] as $group_code => $group){
				$group_id = $this->db->insert("INSERT INTO contracts_group SET 
					contract_id = '".$in['contract_id']."',
					title = '".addslashes($group['title'])."',
					version_id 	=   '".$in['version_id']."',
					sort_order = '".$group_order."',
					show_chapter_total 	=   '".$group['show_chapter_total']."',
					pagebreak_ch='".$group['pagebreak_ch']."',
					show_QC = '".$group['show_QC']."' ,
					show_UP = '".$group['show_UP']."' ,
					show_PS = '".$group['show_PS']."' ,
					show_d = '".$group['show_d']."'  ");
				$group_order += 1;
				if(is_array($group['table']) || !empty($group['table'])){
					$i = 0;
					foreach($group['table'] as $table_id => $table){
						$sort_order=0;
						if($table['q_table'] == true){
							foreach($table['quote_line'] as $index => $val){
								// console::log()
								if(empty($val['description']) ){
									continue;
								}
								if(ark::$method=='add'){
									$val['package']=1;
									$val['sale_unit']=1;
								}else{ //update
									if(!$use_package){
										$val['package']=1;
									}
									if(!$use_sale_unit){
										$val['sale_unit']=1;
									}
								}
								$line_total = return_value($val['quantity']) * return_value($val['price']) * ($val['package']/ $val['sale_unit']);
								$line_total=$line_total- ($line_total* return_value($val['line_discount'])/100);
								if(!is_numeric($val['line_type'])) {
									$val['line_type']=1;
								}
								$total_amount+=$line_total;
								$total_amount_vat+=($line_total+$line_total*$val['line_vat']/100);
								$this->db->query("INSERT INTO contract_line SET
								                name       				=   '".htmlspecialchars($val['description'],ENT_COMPAT | ENT_HTML401,"UTF-8")."',
								                contract_id   	    		=   '".$in['contract_id']."',
								                version_id 	    		=   '".$in['version_id']."',
								                content_type        	=   '1',
								                line_type           	=   '".$val['line_type']."',
								                group_id            	=   '".$group_id."',
								                table_id            	=   '".$table['group_table_id']."',
								                article_code        	=   '".addslashes($val['article_code'])."',
	                              				article_id          	=   '".$val['article_id']."',    
	                              				is_tax					=	'".$val['is_tax']."',
	                              				quantity            	=   '".return_value($val['quantity'])."',
	                              				line_discount       	=   '".return_value($val['line_discount'])."',
								                package             	=   '".return_value2($val['package'])."',
								                sale_unit           	=   '".return_value($val['sale_unit'])."',
								                price               	=   '".return_value($val['price'])."',
								                amount              	=   '".$line_total."',
								                f_archived          	= 	'0',
								                created             	= 	'".$created_date."',
								                created_by          	= 	'".$_SESSION['u_id']."',
								                last_upd            	= 	'".$created_date."',
								                last_upd_by         	= 	'".$_SESSION['u_id']."'  ,
								                vat						=	'".return_value($val['line_vat'])."',
								                line_order          	=   '".$index."',
							            		sort_order				=	'".$table['sort_order']."',
							            		show_block_total		=	'".$val['show_block_total']."',
							            		hide_AC					=	'".$val['hide_AC']."',
							            		hide_QC					=	'".($in['show_QC'] == '1' ? '0': '1')."',
							            		hide_UP 				= 	'".($in['show_UP'] == '1' ? '0': '1')."',
							            		hide_DISC 				= 	'".($in['show_d'] == '1' ? '0': '1')."',
							            		show_block_vat			=	'".$val['show_lVATv']."',
							            		service_bloc_title		=	'".addslashes($table['item_text'])."'
								");
								$i += 1;
								$sort_order++;
							}
						}
						elseif($table['q_content'] == true){
							$content = str_replace('?','&#63;', $table['quote_content']);
							$this->db->query("INSERT INTO contract_line SET 
								contract_id   	    	=   '".$in['contract_id']."',
								version_id 	    	=   '".$in['version_id']."',
								content_type        =   '2',
								group_id            =   '".$group_id."',
								table_id            =   '".$table['group_content_id']."',
								content             =   '".addslashes($content)."',
								f_archived          = 	'0',
								created             = 	'".$created_date."',
								created_by          = 	'".$_SESSION['u_id']."',
								last_upd            = 	'".$created_date."',
								last_upd_by         = 	'".$_SESSION['u_id']."',
                                sort_order			=	'".$table['sort_order']."'");
						}
						elseif($table['pagebreak'] == true){
							$this->db->query("INSERT INTO contract_line SET 
								contract_id   	    =   '".$in['contract_id']."',
								version_id 	    =   '".$in['version_id']."',
								content_type        =   '3',
								group_id            =   '".$group_id."',
								table_id            =   '".$table['group_break_id']."',
								content             =   '<br pagebreak=\"true\"/>',
								f_archived          = 	'0',
								created             = 	'".$created_date."',
								created_by          = 	'".$_SESSION['u_id']."',
								last_upd            = 	'".$created_date."',
								last_upd_by         = 	'".$_SESSION['u_id']."',
                                sort_order			=	'".$table['sort_order']."'");
						}
					}
				}
			}
			$this->db->query("UPDATE contracts SET amount='".$total_amount."',amount_vat='".$total_amount_vat."' WHERE contract_id='".$in['contract_id']."' ");
		}
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddC(&$in){
		if(!$this->tryAddCValid($in)){ 
			json_out($in);
			return false; 
		}

		//$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		//Set account default vat on customer creation
	    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	    if(empty($vat_regime_id)){
	      $vat_regime_id = 0;
	    }

	    $c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			/*foreach ($in['c_type'] as $key) {
				if($key){
					$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
					$c_type_name .= $type.',';
				}
			}
			$c_types = implode(',', $in['c_type']);
			$c_type_name = rtrim($c_type_name,',');*/
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']==''){
			$in['user_id']=$_SESSION['u_id'];
		}


		if($in['user_id']){
			/*foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);*/
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				//$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}
		$vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
	    $vat_default=str_replace(',', '.', $vat);
		$selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");

		$in['contract_id'] = $in['item_id'];
		if($in['add_customer']){
			$payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       		$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
       		$account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
       		if(SET_DEF_PRICE_CAT == 1){
				$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
			}
		  	$in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
		                                        name='".addslashes($in['name'])."',
		                                        our_reference = '".$account_reference_number."',
		                                        btw_nr='".$in['btw_nr']."',
		                                        vat_regime_id = '".$vat_regime_id."',
		                                        city_name = '".$in['city']."',
		                                        c_email = '".$in['email']."',
                                            	comp_phone ='".$in['phone']."',
		                                        country_name ='".get_country_name($in['country_id'])."',
		                                        active='1',
		                                        creation_date = '".time()."',
		                                        payment_term 			= '".$payment_term."',
												payment_term_type 		= '".$payment_term_type."',
												type 					= '0',
		                                        zip_name  = '".$in['zip']."',
		                                        user_id = '".$acc_manager_ids."',
                                            	acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            	commercial_name 		= '".$in['commercial_name']."',
												legal_type				= '".$in['legal_type']."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												sector					= '".$in['sector']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$c_type_name."',
												vat_id 					= '".$selected_vat."',
												invoice_email_type		= '1',
												sales_rep				= '".$in['sales_rep_id']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												stripe_cust_id			= '".$in['stripe_cust_id']."',
												cat_id					= '".$in['cat_id']."'

                                            	");
		  
		  	$this->db->query("INSERT INTO customer_addresses SET
		                    address='".$in['address']."',
		                    zip='".$in['zip']."',
		                    city='".$in['city']."',
		                    country_id='".$in['country_id']."',
		                    customer_id='".$in['buyer_id']."',
		                    is_primary='1',
		                    delivery='1',
		                    billing='1',
		                    site='1' ");
		  	$in['customer_name'] = $in['name'];

			if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
					if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
						$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
					}
				}
			}

		  // include_once('../apps/company/admin/model/customer.php');
		  	$in['value']=$in['btw_nr'];
		  // $comp=new customer();       
		  // $this->check_vies_vat_number($in);
		  	if($this->check_vies_vat_number($in) != false){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
		  	}else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
		  	}

		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
		    $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                            AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                        name    = 'company-customers_show_info',
		                        value   = '1' ");*/
		        $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                        name    = :name,
		                        value   = :value ",
		                    ['user_id' => $_SESSION['u_id'],
		                     'name'    => 'company-customers_show_info',
		                     'value'   => '1']
		                );
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
		        $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                            AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);
		 	}
		  	$count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
		  	if($count == 1){
		    	doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
		  	}
		  	if($in['item_id'] && is_numeric($in['item_id'])){
		  		$address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
		  		$this->db->query("UPDATE contracts SET 
		  			customer_id='".$in['buyer_id']."', 
		  			customer_name='".$in['name']."',
		  			contact_id='',
		  			main_address_id='', 
		  			free_field = '".$address."'
		  			WHERE contract_id='".$in['item_id']."' ");		  		
		  	}
		  	insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
		  	msg::success (gm('Success'),'success');
		  	return true;
		}
		if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
        if(SET_DEF_PRICE_CAT == 1){
			$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
		}
        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['lastname'])."',
                                            firstname = '".addslashes($in['firstname'])."',
                                            our_reference = '".$account_reference_number."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            type = 1,
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            commercial_name 		= '".$in['commercial_name']."',
											legal_type				= '".$in['legal_type']."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											sector					= '".$in['sector']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											c_type_name				= '".$c_type_name."',
											vat_id 					= '".$selected_vat."',
											invoice_email_type		= '1',
											sales_rep				= '".$in['sales_rep_id']."',
											internal_language		= '".$in['internal_language']."',
											is_supplier				= '".$in['is_supplier']."',
											is_customer				= '".$in['is_customer']."',
											stripe_cust_id			= '".$in['stripe_cust_id']."',
											cat_id					= '".$in['cat_id']."'
                                            ");
      
        $this->db->query("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
					$this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
				}
			}
		}

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
            $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                        ['user_id'=>$_SESSION['u_id'],
                         'name'=>'company-customers_show_info',
                         'value'=>'1']
                    );
        } else {
          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
            $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          $this->db->query("UPDATE contracts SET 
            customer_id='".$in['buyer_id']."', 
            customer_name='".$in['name']."',
            contact_id='',
            free_field = '".$address."'
            WHERE contract_id='".$in['item_id']."' ");         
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
		if($in['add_contact']){
			$customer_name='';
			if($in['buyer_id']){
				$customer_name = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
			}
			if($in['birthdate']){
				$in['birthdate'] = strtotime($in['birthdate']);
			}
		  	$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
						customer_id	=	'".$in['buyer_id']."',
						firstname	=	'".$in['firstname']."',
						lastname	=	'".$in['lastname']."',
						email		=	'".$in['email']."',
						birthdate	=	'".$in['birthdate']."',
						cell		=	'".$in['cell']."',
						sex			=	'".$in['sex']."',
						title		=	'".$in['title_contact_id']."',
						language	=	'".$in['language']."',
						company_name=	'".$customer_name."',
						`create`	=	'".time()."'");
		  	$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
			if($in['extra']){
				foreach ($in['extra'] as $key => $value) {
					$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
				}
			}

		  	if($in['buyer_id']){
				$contact_accounts_sql="";
				$accepted_keys=["position","department","title","e_title","email","phone","fax","s_email","customer_address_id"];
		 		foreach($in['accountRelatedObj'] as $key=>$value){
		 			if(in_array($value['model'],$accepted_keys)){
		 				$contact_accounts_sql.="`".$value['model']."`='".addslashes($value['model_value'])."',";
		 			}
		 		}
		 		if($in['email']){
		 			$contact_accounts_sql.="`email`='".$in['email']."',";
		 		}
		 		if(!empty($contact_accounts_sql)){
		 			$contact_accounts_sql=rtrim($contact_accounts_sql,",");
		 			$this->db->query("INSERT INTO customer_contactsIds SET 
			  								customer_id='".$in['buyer_id']."', 
			  								contact_id='".$in['contact_id']."',
			  								".$contact_accounts_sql." ");
		 		}	
		 	}
		  	$in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
		  	if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
	                $vars=array();
	                if($in['buyer_id']){
	                    $vars['customer_id']=$in['buyer_id'];
	                    $vars['customer_name']=$customer_name;
	                }          
	                $vars['contact_id']=$in['contact_id'];
	                $vars['firstname']=$in['firstname'];
	                $vars['lastname']=$in['lastname'];
	                $vars['table']='customer_contacts';
	                $vars['email']=$in['email'];
	                $vars['op']='add';
	                synctoZendesk($vars);
	            }
		  	if($in['country_id']){
		    	$this->db->query("INSERT INTO customer_contact_address SET
		                      address='".$in['address']."',
		                      zip='".$in['zip']."',
		                      city='".$in['city']."',
		                      country_id='".$in['country_id']."',
		                      contact_id='".$in['contact_id']."',
		                      is_primary='1',
		                      delivery='1' ");
		  	}
		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                                AND name    = 'company-contacts_show_info'  ");*/
		    $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                                AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                              name    = 'company-contacts_show_info',
		                              value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                              name    = :name,
		                              value   = :value ",
		                            ['user_id' => $_SESSION['u_id'],
		                             'name'    => 'company-contacts_show_info',
		                             'value'   => '1']
		                        );		                              
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                                  AND name    = 'company-contacts_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                                  AND name    = :name ",
		                                ['value'=>'1',
		                                 'user_id'=>$_SESSION['u_id'],
		                                 'name'=>'company-contacts_show_info']
		                            );		                                  
		  	}
		  	$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
		  	if($count == 1){
		    	doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
		  	}
		  	if($in['item_id'] && is_numeric($in['item_id'])){
		  		$this->db->query("UPDATE contracts SET contact_id='".$in['contact_id']."' WHERE contract_id='".$in['item_id']."' ");	
		  	}
		  	insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		  	msg::success (gm('Success'),'success');
		}
		
		return true;
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddCValid(&$in)
	{
		if($in['add_customer']){
		  	$v = new validation($in);
			$v->field('name', 'name', 'required:unique[customers.name]');
			$v->field('country_id', 'country_id', 'required');
			return $v->run();  
		}
		if($in['add_contact']){
			$v = new validation($in);
			$v->field('firstname', 'firstname', 'required');
			$v->field('lastname', 'lastname', 'required');
			//$v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
			$v->field('country_id', 'country_id', 'required');
			return $v->run();  
		}
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		/*if($vat_numeric || substr($value,0,2)=="BE"){
			$trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
			$trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
			$trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

			if(!$trends_access_token || $trends_expiration<time()){
				$ch = curl_init();
				$headers=array(
					"Content-Type: x-www-form-urlencoded",
					"Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
					);
				/*$trends_data=!$trends_access_token ? "grant_type=password&username=akti_api&password=akti_api" : "grant_type=refresh_token&&refresh_token=".$trends_refresh_token;* /
				$trends_data="grant_type=password&username=akti_api&password=akti_api";

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
		       	curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
			    curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
			 	console::log('aaaaa');
			 	console::log($info);* /

			 	if($info['http_code']==400){
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else{
			 		if(!$trends_access_token){
			 			$this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
					 	$this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
					 	$this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
			 		}else{
			 			$this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
			 			$this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
			 			$this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
			 		}

			 		$ch = curl_init();
					$headers=array(
						"Authorization: Bearer ".$put->access_token
						);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    if($vat_numeric){
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
				    }else{
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
				    }
				    $put = json_decode(curl_exec($ch));
				 	$info = curl_getinfo($ch);

				 	/*console::log($put);
				 	console::log('bbbbb');
				 	console::log($info);* /

				    if($info['http_code']==400 || $info['http_code']==429){
				    	if(ark::$method == 'check_vies_vat_number'){
					 		msg::error ( $put->error,'error');
					 		json_out($in);
					 	}
						return false;
				 	}else if($info['http_code']==404){
				 		if($vat_numeric){
				 			if(ark::$method == 'check_vies_vat_number'){
					 			msg::error ( gm("Not a valid vat number"),'error');
					 			json_out($in);
					 		}
							return false;
				 		}
				 	}else{
				 		if($in['customer_id']){
				 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 			if($country_id != 26){
				 				$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 				$in['remove_v']=1;
				 			}else if($country_id == 26){
					 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
					 			$in['add_v']=1;
					 		}
				 		}
				 		$in['comp_name']=$put->officialName;
				 		$in['comp_address']=$put->street.' '.$put->houseNumber;
						$in['comp_zip']=$put->postalCode;
						$in['comp_city']=$put->city;
				 		$in['comp_country']='26';
				 		$in['trends_ok']=true;
				 		$in['trends_lang']=$_SESSION['l'];
				 		$in['full_details']=$put;
				 		if(ark::$method == 'check_vies_vat_number'){
					 		msg::success(gm('Success'),'success');
					 		json_out($in);
					 	}
				 		return false;
				 	}
			 	}
			}else{
				$ch = curl_init();
				$headers=array(
					"Authorization: Bearer ".$trends_access_token
					);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    if($vat_numeric){
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
			    }else{
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
			    }
			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
				console::log('ccccc');
				console::log($info);* /

			    if($info['http_code']==400 || $info['http_code']==429){
			    	if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else if($info['http_code']==404){
			 		if($vat_numeric){
			 			if(ark::$method == 'check_vies_vat_number'){
				 			msg::error (gm("Not a valid vat number"),'error');
				 			json_out($in);
				 		}
						return false;
			 		}
			 	}else{
			 		if($in['customer_id']){
			 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 		if($country_id != 26){
				 			$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['remove_v']=1;
				 		}else if($country_id == 26){
				 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['add_v']=1;
				 		}
				 	}
			 		$in['comp_name']=$put->officialName;
			 		$in['comp_address']=$put->street.' '.$put->houseNumber;
					$in['comp_zip']=$put->postalCode;
					$in['comp_city']=$put->city;
				 	$in['comp_country']='26';
			 		$in['trends_ok']=true;
			 		$in['trends_lang']=$_SESSION['l'];
			 		$in['full_details']=$put;
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::success(gm('Success'),'success');
				 		json_out($in);
				 	}
			 		return false;
			 	}
			}
		}*/


		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	/****************************************************************
	* function update(&$in)                                          *
	****************************************************************/
	function update(&$in)
	{
		$f_archived=$this->db->field("SELECT active FROM contracts WHERE contract_id='".$in['contract_id']."' ");
		if($in['template']){
			$f_archived='2';
			$in['serial_number'] = $in['template_name'];
		}else{
			if(!$this->update_validate($in))
			{
				json_out($in);
				return false;
			}
		}

		$created_date= date(ACCOUNT_DATE_FORMAT.' H:i:s');
		$updatedd_date= date(ACCOUNT_DATE_FORMAT.' H:i:s');

		$reminder_date=strtotime($in['reminder_date']);
		if($in['end_date']){
            	if($in['reminder_type']==1){
			   $reminder_date=strtotime("-1 month", $in['end_date']);
		    	}
		     	if($in['reminder_type']==2){
			   $reminder_date=strtotime("-2 month", $in['end_date']);
		    	}
		}
		if($in['apply_discount']==0){
			$in['discount']=0;
		}
		if(!$in['unlimited_period']){
			$in['unlimited_period'] = 0;
		}
		if($in['unlimited_period']==1){
			$in['end_date'] = 0;
		}
		$use_recurring = 0;
		if($in['invoicing_type']==1){
			$use_recurring = 1;
		}
		if($in['buyer_id']){
			//$in['customer_name'] = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id = '".$in['buyer_id']."'"));
			$name = $this->db->query("SELECT firstname, name, type FROM customers WHERE customer_id = '".$in['buyer_id']."'");
			$name->next();
			if($name->f('type')==1){
				$in['customer_name'] = addslashes($name->f('firstname').' '.$name->f('name'));
			}else{
				$in['customer_name'] = addslashes($name->f('name'));
			}
			
		} else {
			$contact_name = $this->db->query("SELECT firstname, lastname FROM  customer_contacts WHERE contact_id = '".$in['contact_id']."' ");
			$contact_name->next();
			$in['customer_name'] = addslashes($contact_name->f('firstname').' '.$contact_name->f('lastname'));
		}
		$can_do = false;
		$buyer_id_old = $this->db->field("SELECT customer_id FROM contracts WHERE contract_id = '".$in['contract_id']."' ");
		if(($in['buyer_id'] != $buyer_id_old || $in['customer_changed'])) {
			$can_do = true;
		}
		$contact_id_old = $this->db->field("SELECT contact_id FROM contracts WHERE contract_id = '".$in['contract_id']."' ");
		$can_do_contact = false;
		if($in['contact_id'] != $contact_id_old || $in['contact_changed']) {
			$can_do_contact = true;
		}

		//Set email language as account / contact language
		$emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
	    						  'contact_id' 		=> $in['contact_id'],
	    						  'item_id'			=> $in['contract_id'],
	    						  'email_language' 	=> $in['email_language'],
	    						  'table'			=> 'contracts',
	    						  'table_label'		=> 'contract_id',
	    						  'table_buyer_label' => 'customer_id',
	    						  'table_contact_label' => 'contact_id',
	    						  'param'		 	=> 'edit');
		$in['email_language'] = get_email_language($emailMessageData);
	    //End Set email language as account / contact language

		$default_email_language = $this->db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
	 	if($in['email_language']==0){
	 		$in['email_language'] = $default_email_language;
	 	}
	 	$in['contract_date2'] = strtotime($in['contract_version_date']);

		$this->db->query("UPDATE contracts  SET	
								serial_number       	=   '".$in['serial_number']."',
								customer_id        		= '".$in['buyer_id']."',
								contact_id				=   '".$in['contact_id']."',
								name	           		= '".$in['name']."',
								is_contact				= '0',
								start_date		   		= '".$in['start_date']."',
								end_date		   		= '".$in['end_date']."',
								serial_number	   		= '".$in['serial_number']."',
								our_reference			= '".$in['our_reference']."',
								customer_name			= '".$in['customer_name']."',
                                                your_ref				= '".$in['your_ref']."',  
								type					= '".$in['type']."',
								vat                     = '".return_value($in['vat'])."',
								remove_vat              = '".$in['remove_vat']."',
								discount      	    	= '".return_value($in['discount'])."',
								discount_line_gen      	  =   '".return_value($in['discount_line_gen'])."',
								currency_type      	=   '".$in['currency_type']."',
								apply_discount			= '".$in['apply_discount']."',
								quote_id 				= '".$in['quote_id']."',
								email_language          = '".$in['email_language']."',
								budget_type	 		    = '".$in['budget_type']."',
								t_ct_hour	     		= '".$in['t_ct_hour']."',
								send_email_alerts	    = '".$in['send_email_alerts']."',
								alert_percent	        = '".$in['alert_percent']."',
								invoicing_type         = '".$in['invoicing_type']."',
								reminder_type           = '".$in['reminder_type']."',
								reminder_date           = '".$reminder_date."',
								no_end_date				= '".$in['unlimited_period']."',
								use_recurring		= '".$use_recurring."',
								discount_line_gen 	= '".$in['discount_line_gen']."',
								subject			= 	'".$in['subject']."',
								identity_id 		=	'".$in['identity_name']."',
								show_QC			=	'".$in['show_QC']."',
								show_vat			=	'".$in['show_vat']."',
                                              	show_UP			=	'".$in['show_UP']."',
                                                show_V			=	'".$in['show_V']."',
                                                show_PS			=	'".$in['show_PS']."',
                                                show_vat_pdf				= 	'".$in['show_vat_pdf']."',
                            				show_grand				= 	'".$in['show_grand']."',
                            				pdf_layout				=   '".$in['pdf_layout']."',
                            				author_id				= 	'".$in['author_id']."',
                            				acc_manager_id           	=	'".$in['acc_manager_id']."',
                            				vat_regime_id 		='".$in['vat_regime_id']."',
                                                active='".$f_archived."',
                                                installation_id ='".$in['installation_id']."',
                                                identity_id='".$in['identity_id']."',
                            				source_id='".$in['source_id']."',
                            				type_id='".$in['type_id']."',
                            segment_id='".$in['segment_id']."' 
							WHERE contract_id='".$in['contract_id']."'");


	if($in['general_conditions_new_page']){
		$this->db->query("UPDATE contracts SET general_conditions_new_page='1' WHERE contract_id='".$in['contract_id']."' ");
	}
        //update invoice info

      $this->db->query("UPDATE  contract_invoice  SET
                                                discount      	    	=   '".return_value($in['discount'])."',
                                                req_payment      	    =   '".return_value($in['req_payment'])."',

                                                vat						= 	'".return_value($in['vat'])."',
                                                email_language			=	'".$in['email_language']."',
                                                apply_discount			=	'".$in['apply_discount']."',

                                                currency_rate			=   '".$in['currency_rate']."',
                                                buyer_id       	        =   '".$in['buyer_id']."',
                                                buyer_name              =   '".$in['customer_name']."'
							WHERE contract_id='".$in['contract_id']."'");



	  $lang_note_id = $in['email_language'];

	  #default langs
        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        // add multilanguage quote notes in note_fields  DEFAULT_LANGS
	  $lang_note_id = $in['email_language'];
        if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
        }else{
            $active_lang_note = 0;
        }
        $this->db->query("DELETE FROM note_fields WHERE item_id = '".$in['contract_id']."' AND item_type = 'contract' AND item_name = 'notes' AND version_id = '".$in['version_id']."' AND lang_id = '".$in['email_language']."' ");

        $this->db->insert("INSERT INTO note_fields SET
                            active 			=	'1',
                            item_id         = 	'".$in['contract_id']."',
                            lang_id         =   '".$in['email_language']."',
                            item_type       =   'contract',
                            item_name       =   'notes',
                            version_id 		= 	'".$in['version_id']."',
                            item_value      =   '".$in['notes']."'
        ");

        $this->db->query("DELETE FROM note_fields
						  	WHERE 	item_type 	= 'contract'
						  	AND 	item_name 	= 'free_text_content'
						  	AND 	version_id 	= '".$in['version_id']."'
						  	AND 	item_id 	= '".$in['contract_id']."' ");

		$this->db->query("INSERT INTO note_fields SET
			item_value				= 	'".$in['free_text_content']."',
			item_type 				= 	'contract',
			item_name 				= 	'free_text_content',
			version_id 				= 	'".$in['version_id']."',
			item_id 				= 	'".$in['contract_id']."' ");


		$this->db->query("DELETE FROM contract_line WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."'");
		$this->db->query("DELETE FROM contracts_group WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."'");
		$this->db->query("UPDATE contracts_version SET version_date='".$in['contract_date2']."' , 
													discount      	    				=   '".return_value($in['discount'])."',
                                                	discount_line_gen      	    =   '".return_value($in['discount_line_gen'])."',
                                                	apply_discount        		  =   '".$in['apply_discount']."'
									WHERE version_id='".$in['version_id']."' ");
		$this->contract_lines($in);

		$this->db->query("SELECT * FROM projects WHERE contract_id='".$in['contract_id']."'");
		while ( $this->db->move_next()) {
		        $this->db2->query("DELETE FROM project_user WHERE project_id='".$this->db->f('project_id')."'");
		        $this->db2->query("DELETE FROM tasks WHERE project_id='".$this->db->f('project_id')."'");
		}

        	$this->db->query("DELETE FROM projects WHERE contract_id='".$in['contract_id']."'");

		//insert a project and task with contract info
        	   if(!$in['is_contact']){
                  $in['project_id'] = $this->db->insert("INSERT INTO projects SET contract_id='".$in['contract_id']."', customer_id='".$in['buyer_id']."', name='".$in['serial_number']."',budget_type=4,send_email_alerts='".$in['send_email_alerts']."',alert_percent='".$in['alert_percent']."',t_pr_hour='".$in['t_ct_hour']."' ,active='2', billable_type='1', invoice_method='1', company_name=(SELECT name FROM customers WHERE customer_id='".$in['buyer_id']."') ");
                }
				else{
				 $in['project_id'] = $this->db->insert("INSERT INTO projects SET contract_id='".$in['contract_id']."', customer_id='".$in['buyer_id']."', name='".$in['serial_number']."',budget_type=4,send_email_alerts='".$in['send_email_alerts']."',alert_percent='".$in['alert_percent']."',t_pr_hour='".$in['t_ct_hour']."' ,active='2', billable_type='1', invoice_method='1', company_name=(SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$in['buyer_id']."') ");
				}

				//$u_name = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."' ");
				$u_name = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
				$this->db->query("INSERT INTO project_user SET project_id='".$in['project_id']."',manager=1, user_id='".$_SESSION['u_id']."', user_name='".addslashes($u_name)."' ");
				$in['task_id'] = $this->db->insert("INSERT INTO tasks set project_id='".$in['project_id']."', task_name='".$in['serial_number']."', billable='1', t_hours='".$in['t_ct_hour']."' ");

        $trace_id=$this->db->field("SELECT trace_id FROM contracts WHERE contract_id='".$in['contract_id']."' ");
		if($in['deal_id']){
			//$trace_id=$this->db->field("SELECT trace_id FROM contracts WHERE contract_id='".$in['contract_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' ");	
				}else{
					$this->db->query("INSERT INTO tracking_line SET
							origin_id='".$in['deal_id']."',
							origin_type='11',
							trace_id='".$trace_id."' ");
				}
			}else{
				$tracking_data=array(
		            'target_id'         => $in['contract_id'],
		            'target_type'       => '9',
		            'target_buyer_id'   => $in['buyer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
		                                    )
		          );
		      addTracking($tracking_data);
			}
		}else{
			//$trace_id=$this->db->field("SELECT trace_id FROM contracts WHERE contract_id='".$in['contract_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("DELETE FROM tracking_line WHERE line_id='".$linked_doc."' ");	
				}
			}
		}
		if($trace_id){
			$this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
		}

   		msg::success(gm("Contract details was updated"),"success");
		insert_message_log($this->pag, "{l}Contract details was updated by{endl} ".get_user_name($_SESSION['u_id']),$this->field_n,$in['contract_id']);
		$in['pagl'] = $this->pag;
		if($in['template']){
			$in['contract_template_id'] = $in['contract_id'];
			$in['contract_id']='';
		}
		global $config;
		$this->db->query("UPDATE contracts_version SET preview='0' WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/contract_cache/contract_".$in['contract_id']."_".$in['version_id']."_%' ");
		// $in['new'] = false;

		return true;
	}

	/****************************************************************
	* function update_validate(&$in)                                          *
	****************************************************************/
	function update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contract_id', gm('ID'), 'required:exist[contracts.contract_id]', gm('Invalid ID'));

		$is_ok = $v->run();

		if( $is_ok === true && !$this->add_validate($in)){
			$is_ok = false;
		}

		return $is_ok;
	}

	/**
	 * Archive validation
	 * We check to see if we have the quote with this id
	 *
	 * @param array $in
	 * @return true or false
	 */
	function archive_validate(&$in){
		$v = new validation($in);
		$v->f('contract_id', 'Id', 'required:exist[contracts.contract_id]',gm('Invalid ID'));
		return $v->run();
	}

	 /**
	 * Send order by email
	 *
	 * @return bool
	 * @author PM
	 **/
	function pdf_settings(&$in){
   		if(!($in['contract_id']&&$in['pdf_layout']&&$in['logo']) )
		{
			msg::error(gm('Invalid ID'),'error');
			json_out($in);
			return false;
		}
    	
		if($in['logo'] == 'images/no-logo.png'){
			$in['logo'] = 'images/no-logo.png';
		}
    
	    $this->db->query("UPDATE contracts SET pdf_layout='".$in['pdf_layout']."',pdf_logo='".$in['logo']."',email_language='".$in['email_language']."', identity_id='".$in['identity_id']."' WHERE contract_id = '".$in['contract_id']."' ");

	    $this->db->query("UPDATE contracts_version SET preview='0' WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");

	  	msg::success ( gm("Pdf settings have been updated"),'success');
	  	return true;
	}

	function mark_sent(&$in){
		if(!$this->mark_sent_validate($in))
		{
			json_out($in);
			return false;
		}
		$in['sent_date'] = strtotime($in['sent_date']);	
		$this->db->query("UPDATE contracts_version SET sent='1',sent_date='".$in['sent_date']."' WHERE version_id='".$in['version_id']."' AND contract_id='".$in['contract_id']."' ");
		$this->db->query("UPDATE contracts SET sent='1', sent_date='".$in['sent_date']."' WHERE contract_id='".$in['contract_id']."' ");
		$this->db->query("UPDATE contracts SET pending= '1' WHERE contract_id = '".$in['contract_id']."' ");

		msg::success(gm("Contract has been successfully marked as sent"),'success');
		insert_message_log($this->pag,'{l}Contract has been manually marked as sent by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contract_id']);
		$in['id'] = $in['contract_id'];
		$this->external_id($in);
		return true;
	}

	/****************************************************************
	* function mark_sent_validate(&$in)                                *
	****************************************************************/
	function mark_sent_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contract_id', 'ID', 'required:exist[contracts.contract_id]', "Invalid Id");
		$v->field('sent_date', 'Sent Date', 'required');
		
		$is_ok = $v->run();
		if($is_ok){
			$versionDateQ = $this->db->query("SELECT version_date FROM contracts_version WHERE version_id = '".
				$in['version_id']."' AND contract_id = '".$in['contract_id']."'");
			$in['contract_version_date'] = gmdate("Y-m-d\TH:i:s\Z",(int)$versionDateQ->f('version_date'));

			$contractVersionDate =  mktime(0,0,0,
                  date("n",(int)$versionDateQ->f('version_date')),
                  date("j",(int)$versionDateQ->f('version_date')), 
                  date("Y",(int)$versionDateQ->f('version_date'))
                );

			// $contractVersionDate =  new DateTime($in['contract_version_date']);
			// $contractVersionDate->setTime(0,0,0);
			// $sentDate = new DateTime($in['sent_date']);
			// $sentDate->add(new DateInterval('P1D'));
			// $sentDate->setTime(0,0,0);

			//if($sentDate->format('U') < $contractVersionDate->format('U'))

			$sentDate = mktime(0,0,0,
                  date("n",strtotime($in['sent_date'])),
                  date("j",strtotime($in['sent_date'])), 
                  date("Y",strtotime($in['sent_date']))
                );

			if($sentDate < $contractVersionDate){
				msg::error(gm('Sent Date must be equal or be grater than Contract Date'),'error');
				return false;
			}
		}

		return $is_ok;


	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function revert(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}

		$this->db->query("UPDATE contracts SET sent='0' WHERE contract_id='".$in['contract_id']."' ");
		$this->db->query("UPDATE contracts SET pending = '0' WHERE contract_id= '".$in['contract_id']."' ");
		$this->db->query("UPDATE contracts_version SET sent='0' WHERE contract_id='".$in['contract_id']."' AND version_id='".$in['version_id']."' ");
		// $in['quote_id'] = $in['id'];
		msg::success(gm("Contract  has been returned to draft"),'success');
		insert_message_log($this->pag,'{l}Contract  has been returned to draft by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contract_id']);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function status(&$in){
		$q = $this->db->query("SELECT contract_id from contracts WHERE contract_id=".$in['contract_id']);
		$cust_id = $this->db->field("SELECT customer_id FROM contracts WHERE contract_id='".$in['contract_id']."'");
		$current_status=$this->db->field("SELECT status_customer FROM contracts WHERE contract_id='".$in['contract_id']."' ");
		if(!$q->next()){
			$in['error'] = gm("Invalid ID");
			msg::error(gm("Invalid ID"),'error');
			json_out($in);
			return false;
		}
		if($in['status_customer']=='2' && $current_status!='3'){
			$v=new validation($in);
			$v->field('start_date', 'Start Date', 'required');
			$is_ok = $v->run();
			if($is_ok && !$in['unlimited_period']){
				if(!$in['end_date']){
					msg::error(gm("End Date required"),"end_date");
					$is_ok=false;
				}
			}
			if(!$is_ok){
				json_out($in);
				return false;
			}else{
				$in['start_date'] = strtotime($in['start_date']);
				if($in['unlimited_period']){
					$in['end_date']=0;
				}else{
					$in['end_date'] = strtotime($in['end_date']);
				}
			}
		}
		$time = time();
		$is_recurring=$this->db->field("SELECT recurring_invoice_id FROM recurring_invoice WHERE contract_id='".$in['contract_id']."' ");
		if($in['status_customer']=='3'){
			$closed=1;			
			if($is_recurring){
				$rec_next_date=strtotime("-1 day",time());
				$this->db->query("UPDATE recurring_invoice SET next_date='".$rec_next_date."' WHERE contract_id='".$in['contract_id']."' ");
			}
		}else{
			$closed=0;
			if($is_recurring){			
				if($current_status == 3){
					$contract_data = $this->db->query("SELECT contracts_version.* FROM contracts 
                				INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
                				WHERE contracts.contract_id='".$in['contract_id']."' AND contracts_version.active='1' ");
					$rec_data=$this->db->query("SELECT * FROM recurring_invoice WHERE contract_id='".$in['contract_id']."' LIMIT 1");
					while($rec_data->next()){
						$rec_next_date=$contract_data->f('start_date');
					      switch($rec_data->f('frequency')){
					          case 1: 
					          	     while($rec_next_date < time()){
					          	     		$rec_next_date+=(7 * 24 * 60 * 60);	
					          	     } 
					          		break; //week
					          case 2:
					          		while($rec_next_date < time()){
					          	     		$rec_next_date=strtotime("+1 month",$rec_next_date);	
					          	     }
					          		break; //month
					          case 3:
					          		while($rec_next_date < time()){
					          	     		$rec_next_date=strtotime("+3 months",$rec_next_date);	
					          	     }
					          		break; //3 month
					          case 4:  
					          		while($rec_next_date < time()){
					          	     		$rec_next_date=strtotime("+1 year",$rec_next_date);	
					          	     }
					          		break; //year
					          case 5:  
					          		while($rec_next_date < time()){
					          	     		$rec_next_date+=($rec_data->f('days') * 24 * 60 * 60);	
					          	     } 
					          		break; //days;
					      }
						$this->db->query("UPDATE recurring_invoice SET next_date='".$rec_next_date."' WHERE contract_id='".$in['contract_id']."' ");
					}		
				}
			}
		}
		$this->db->query("UPDATE contracts SET status_customer='".$in['status_customer']."', status_c_date='".$time."',closed='".$closed."' WHERE contract_id=".$in['contract_id']);
			$this->db->query("UPDATE contracts_version SET active = 0 WHERE contract_id='".$in['contract_id']."'");
			$this->db->query("UPDATE contracts_version SET active = 1 WHERE contract_id='".$in['contract_id']."' AND version_id = '".$in['version_id']."'");
		if ($in['status_customer'] == 1) {
				$exist=$this->db->field("SELECT id FROM contract_lost_reason WHERE id='".$in['lost_id']."'");
				if($exist){
					$this->db->query("UPDATE contracts SET lost_id='".$exist."' WHERE contract_id=".$in['contract_id']);
				}
		}
		if($in['status_customer']=='2'){
			$this->db->query("UPDATE contracts_version SET start_date='".$in['start_date']."',end_date='".$in['end_date']."',no_end_date='".$in['unlimited_period']."',preview='0' WHERE version_id='".$in['version_id']."' AND contract_id='".$in['contract_id']."' ");
			$this->db->query("UPDATE contracts SET start_date='".$in['start_date']."',end_date='".$in['end_date']."',no_end_date='".$in['unlimited_period']."' WHERE contract_id='".$in['contract_id']."' ");
		}

		$in['date'] = date(ACCOUNT_DATE_FORMAT,$time);
		$opts = array('Pending','Rejected', 'Accepted', 'Accepted', 'Accepted', 'Draft');
		$the_time = time();
		$this->db->query("INSERT INTO contracts_history SET contract_id='".$in['contract_id']."', date='".$the_time."', message=(SELECT version_code FROM contracts_version WHERE version_id='".$in['version_id']."' ) ");
		insert_message_log($this->pag,'{l}Approval status changed to{endl} :'.$opts[$in['status_customer']].' {l}by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contract_id'],false,$_SESSION['u_id'],true,$cust_id,$the_time);
		msg::success ( gm("Contract has been updated"),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function sendNewEmail(&$in)
	{
		if(!$this->sendNewEmailValidate($in)){
			msg::error(gm('Invalid email address'),'error');
			json_out($in);
			return false;
		}

		global $config;
		$in['weblink_url'] = $this->external_id($in);

		$this->db->query("SELECT contracts.acc_manager_id,contracts.contract_id,contracts.serial_number,contracts.pdf_layout,contracts.pdf_logo,contracts_version.discount,contracts.customer_name,contracts.currency_type, SUM(contract_line.amount) AS total, customers.customer_id,contracts.email_language,contracts.author_id, contracts.customer_id,contracts.contact_id
                           FROM contracts
                           INNER JOIN contracts_version ON contracts.contract_id=contracts_version.contract_id
                           LEFT JOIN contract_line ON contract_line.contract_id=contracts.contract_id
                           LEFT JOIN customers ON contracts.customer_id=customers.customer_id
                           WHERE contracts.contract_id='".$in['id']."' AND contracts_version.version_id='".$in['version_id']."' ");

		$this->db->move_next();
		$cust_id = $this->db->f('customer_id');
		$acc_manager_id=$this->db->f('acc_manager_id');
		$e_lang = $this->db->f('email_language');
		$author = $this->db->f('author_id');
		$buyer_id = $this->db->f('customer_id');
		$contact_id = $this->db->f('contact_id');

		$pdf_layout = $this->db->f('pdf_layout');
		if($this->db->f('use_custom_template')==1){
			$in['custom_type'] = $pdf_layout;
			$in['logo']=$this->db->f('pdf_logo');
		}elseif(defined('USE_CUSTOME_CONTRACT_PDF') && USE_CUSTOME_CONTRACT_PDF == 1 && $pdf_layout == 0){
			$in['custom_type'] = ACCOUNT_CONTRACT_PDF_FORMAT;
		}else{
			if($pdf_layout){
				$in['type']=$pdf_layout;
				$in['logo']=$this->db->f('pdf_logo');
			}else{
				$in['type'] = ACCOUNT_CONTRACT_PDF_FORMAT;
			}
		}

		if(empty($in['contract_id'])){
			$in['contract_id'] = $in['id'];	
		}
		
		$this->generate_pdf($in);

		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		// $date = explode('-',$this->dbu->f('quote_date'));
		// $factur_date = $date[2].'/'.$date[1].'/'.$date[0];
		$total=$this->db->f('total');
		$currency=get_commission_type_list($this->db->f('currency_type'));

		$discount_value = $total*$this->db->f('discount')/100;
		$total -= $discount_value;

		$amount_due = round($total,2);

		$mail = new PHPMailer();
		$mail->WordWrap = 50;
		if(!$e_lang || $e_lang > 4){
	    	$e_lang=1;
	    }

	    $text_array = array('1' => array('simple' => array('1' => 'Your offer', '2'=> 'Check your offer by clicking on the above link','3'=>"Following this link",'4'=>'Web Link'),
        								   'pay' => array('1' => 'INVOICE WEB LINK', '2'=> 'Check your offer by clicking on the above link.','3'=>"HERE")
        								   ),
        					'2' => array('simple' => array('1' => 'Votre Offre', '2'=> 'Visualisez votre offre en cliquant sur le lien ci-dessus','3'=>'Cliquez sur ce lien','4'=>'Lien web'),
        								   'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Vous pouvez tÃ©lÃ©charger votre facture ICI et la payer en ligne','3'=>'ICI')
        								   ),
        					'3' => array('simple' => array('1' => 'Offerte', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'Via deze link','4'=>'Weblink'),
        								   'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'HIER')
        								   ),
        					'4' => array('simple' => array('1' => 'ANGEBOT WIE WEB-LINK', '2'=> 'Sie kÃ¶nnen Ihr Angebot HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
        								   'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie kÃ¶nnen Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER')
        								   )
        );
		
		$in['e_message'] = stripslashes($in['e_message']);
		$body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');

		$def_email = $this->default_email();

    	/* 1 - use user logged email
    	   2 - use default email
    	   3 - use author email
    	*/
    	$mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='contract_email_type' ");
    	if($mail_sender_type == 1 || $mail_sender_type == 3) {
    		if($mail_sender_type == 1) {
    			//$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);

    			$def_email['from']['name'] = $mail_name;//pt sendgrid

    			$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
    			$def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
    			$def_email['reply']['name'] =  $mail_name;//pt sendgrid


    		} else {
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$author."' ");
    			$mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
    			$mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
    			$def_email['from']['name'] = $mail_name;//pt sendgrid

    			$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));
    			$def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
    			$def_email['reply']['name'] =  $mail_name;//pt sendgrid
    		}
    	}else{
    		$mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']),0);
    		$mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
    	}

		$subject=utf8_decode( htmlspecialchars($in['e_subject']) );

		$mail->Subject = $subject;

		if($in['include_pdf']){
			$tmp_file_name = 'contract_.pdf';
			if($in['attach_file_name']){
				$tmp_file_name = $in['attach_file_name'];
			}
			$mail->AddAttachment($tmp_file_name, $in['serial_number'].'.pdf'); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
		}

		if($in['files']){
	        foreach ($in['files'] as $key => $value) {
	        	if($value['checked'] == 1){
		          $mime = mime_content_type($value['path'].$value['file']);
		          $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
		        }
	        }
	    }

	    if($in['dropbox_files']){
			$path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
			if(!file_exists($path_dropbox)){
				mkdir($path_dropbox,0775,true);
			}
			$dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

	        foreach ($in['dropbox_files'] as $key => $value) {
	        	if($value['checked'] == 1){
	        		$value['file'] = str_replace('/', '_', $value['file']);
		        	//$f = fopen($path_dropbox.$value['file'], 'w+');
		        	/*$f=$path_dropbox.$value['file'];*/

		        	$file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
		        	//fclose($f);
		        	//file_get_contents($path_dropbox.$value['file']);
					
					
			        $mime = mime_content_type($path_dropbox.$value['file']);
			        $mail->AddAttachment($path_dropbox.$value['file'], $value['file']);
			        //unlink($path_dropbox.$value['file']);
		        }
	        }
	    }

		$mail->WordWrap = 50;

		$body=str_replace('[!DROP_BOX!]', '', $body);
			 // console::log($body);
		    if($in['file_d_id']){
		    	$body.="\n\n";
			    foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
				}
			}
		$tblinvoice = $this->db->query("SELECT * FROM contracts WHERE contract_id='".$in['id']."'  ");	
		$contact = $this->db->query("SELECT firstname,lastname FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'  ");
		$title_cont = $this->db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'  ");
		$customer = $this->db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('customer_id')."'  ");


	    //$mail->MsgHTML(nl2br($body));
		if($in['use_html']){

			if($in['sendgrid_selected']){
		      $e_message_encoded = $in['e_message'];
		    } else {
		      $e_message_encoded = utf8_decode( $in['e_message']);
		    }  
			
			$head = '<style>p {	margin: 0px; padding: 0px; }</style>';

	      	$mail->IsHTML(true);
	      	$body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 35px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 30px;">'.$e_message_encoded.'</div>';

	     	$body = stripslashes($body_style);
			$body=str_replace('[!CONTACT_FIRST_NAME!]', "".$contact->f('firstname')."", $body);
			$body=str_replace('[!CONTACT_LAST_NAME!]', "".$contact->f('lastname')."", $body);
			$body=str_replace('[!SALUTATION!]', "".$title_cont."", $body);	
			$body=str_replace('[!YOUR_REFERENCE!]',"".$tblinvoice->f('your_ref')."",$body);
	      	if(defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1){
				//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `version_id`='".$in['version_id']."' AND `type`='c'  ");
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `version_id`= :version_id AND `type`= :t  ",['d'=>DATABASE_NAME,'item_id'=>$in['id'],'version_id'=>$in['version_id'],'t'=>'c']);

		     	$extra = "<p style=\"background: #e6e6e6; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #e6e6e6; color:#868d91; border-radius:8px;\">";
	        	
	    		$extra .="<b style=\"color: #5199b7; text-align:center; font-size:32px;\"><a style=\"text-decoration:none; text-transform: lowercase; color:#6399c6;\" href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b><br />";
	        	
	    		$extra .=$text_array[$e_lang]['simple']['2']."<br /></p>";

		     	// $body .=$extra;
	    		if($in['copy']){
	    			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_CONTRACT."\" alt=\"\">",$body);
		     	}elseif($in['copy_acc']){
					$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
		     	}else{
		     		$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_CONTRACT."\" alt=\"\">",$body);
		     	}
		     	
		     	$body=str_replace('[!WEB_LINK!]', $extra, $body);
		     	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);

		    }else{
			    $body=str_replace('[!WEB_LINK!]', '', $body);
			    $body=str_replace('[!WEB_LINK_2!]', '', $body);
			}

			$body=str_replace('[!DROP_BOX!]', '', $body);

			if($in['file_d_id']){
				$body .="<br><br>";
				foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=".$in['file_d_path'][$key].">".$in['file_d_name'][$key]."</a></p>";
				}
			}
		    $mail->Body    = $head.$body;
	    }else{

	    	if(defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1){console::log('use weblink');
				/*$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `version_id`='".$in['version_id']."' AND `type`='c'  ");*/
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `version_id`=:version_id AND `type`= :t  ",['d'=>DATABASE_NAME,'item_id'=>$in['id'],'version_id'=>$in['version_id'],'t'=>'c']);

		      	$extra = "\n\n-----------------------------------";
		      	$extra .="\n".$text_array[$e_lang]['simple']['1'];
		      	$extra .="\n-----------------------------------";
		      	$extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
						// $body .=$extra;
		      	$body=str_replace('[!WEB_LINK!]', $extra, $body);
		      	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
		    }else{
		      	$body=str_replace('[!WEB_LINK!]', '', $body);
		      	$body=str_replace('[!WEB_LINK_2!]', '', $body);
		    }
		    if(defined('DROPBOX') && DROPBOX != ''){
			    if(strpos($body, '[!DROP_BOX!]')){

			        $id = $buyer_id;
			        $is_contact = 0;
			        if(!$id){
			            $id = $contact_id;
			            $is_contact = 1;
			        }
			        $d = new drop('contracts', $id, $in['id'],true,'',$is_contact, $in['serial_number']);
			        $files = $d->getContent();
			        if(!empty($files->entries)){
			            $body.="\n\n";
			            foreach ($files->entries as $key => $value) {
			                $l = $d->getLink(urldecode($value->path_display));
			                $link = $l;
			                $file = $in['serial_number'].'/'.str_replace('/','_', $value->path_display);
			                // $body.="<p><a href=".$link.">".$file."</a></p>";
			                $body.="<p><a href=\"".$link."\">".$file."</a></p>\n";
			            }
			        }
			    }
			}

	      	$mail->MsgHTML(nl2br($body));
	    }
    // console::log($body);

		if($in['copy']){
			//$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
			if($u->f('email')){
				$mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
			}
		}
		if($in['copy_acc']){
			$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$acc_manager_id."' ");
			if($u->f('email')){
				$mail->AddBCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
			}
		}

		$mail->AddAddress(trim($in['email']));

		if($in['mark_as_sent'] == 1){
			$sent_date= time();
			$this->db->query("UPDATE contracts_version SET sent='1',sent_date='".$sent_date."' WHERE contract_id='".$in['id']."' AND version_id='".$in['version_id']."' ");
			$this->db->query("UPDATE contracts SET sent='1', sent_date='".$sent_date."' WHERE contract_id='".$in['id']."' ");
			$this->db->query("UPDATE contracts SET pending = '1' WHERE contract_id = '".$in['id']."' ");
			$in['sent_date_txt']=date(ACCOUNT_DATE_FORMAT,$sent_date);
		}

		$msg_log = '{l}Contract has been sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl}: '.$in['email'];

		//$in['sendgrid_selected'] = 0;//pt test

		if($in['sendgrid_selected']){

          $in['new_email'] = $in['email'];

          include_once(__DIR__."/../../misc/model/sendgrid.php");
          $sendgrid = new sendgrid($in, $this->pag, $this->field_n,$in['id'], $body,$def_email, DATABASE_NAME);

          $sendgrid_data = $sendgrid->get_sendgrid($in);

          if($sendgrid_data['error']){
            msg::error($sendgrid_data['error'],'error');
          }elseif($sendgrid_data['success']){
            msg::success(gm("Email sent by Sendgrid"),'success');
          }

          //$msg_log = $msg_log .= " {l}via SendGrid{endl}.";
          
        }else{

			$mail->Send();
			if($mail->IsError()){

				foreach($xxx as $location => $value){
			      unlink($location);
			    }
				if($in['attach_file_name']){
					unlink($in['attach_file_name']);
				}
				if($in['dropbox_files']){
					$path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
					
			        foreach ($in['dropbox_files'] as $key => $value) {
			        	if($value['checked'] == 1){
			        		$value['file'] = str_replace('/', '_', $value['file']);
				        	unlink($path_dropbox.$value['file']);
				        }
			        }
			    }
				msg::error ( $mail->ErrorInfo,'error');
	        	json_out($in);
				return false;
			}

			if($mail->ErrorInfo){
				//msg::notice( $mail->ErrorInfo,'notice');
			}

			msg::success( gm('Email sent').'.','success');
			if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
				update_log_message($in['logging_id'],' '.$in['email']);
			}else{
				$in['logging_id'] =insert_message_log($this->pag,$msg_log,$this->field_n,$in['id'],false,$_SESSION['u_id'],true,$cust_id,$sent_date);
			}
		}

		if($in['attach_file_name']){
			unlink($in['attach_file_name']);
		}


      	json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendNewEmailValidate(&$in)
	{
		$v = new validation($in);
		$in['email'] = trim($in['email']);
		$v->field('email', 'Email', 'email');
		return $v->run();
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function generate_pdf(&$in)
	{
		include_once(__DIR__.'/../controller/print.php');
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
			if(MAIL_SETTINGS_PREFERRED_OPTION==2){
				if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
					$array['from']['email'] = MAIL_SETTINGS_EMAIL;
				}
			}else{
				if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
					$array['from']['email'] = ACCOUNT_EMAIL;
				}
			}
		}
		/*if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
			$array['from']['email'] = ACCOUNT_EMAIL;
		}*/
		return $array;
	}

	/**
	 * Delete a quote template
	 *
	 * @return bool
	 * @author Mp
	 **/
	function delete_template(&$in)
	{
		if(!$this->archive_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM contracts_group WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM contract_line WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM contracts_history WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM contracts_version WHERE contract_id = '".$in['contract_id']."'");
		$this->db->query("DELETE FROM logging WHERE field_name='contract_id' AND field_value='".$in['contract_id']."' ");
		$this->db->query("DELETE FROM contracts WHERE contract_id = '".$in['contract_id']."'");

	  	msg::success ( gm("Template deleted."),'success');

	  	return true;
	}

	function postRegister(&$in){
        global $config;

        $env_usr = $config['postgreen_user'];
        $env_pwd = $config['postgreen_pswd'];

        $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> '',
	      							'module'		=> 'contract',
	      							'date'			=> time(),
	      							'action'		=> 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'],
	      							'content'		=> '',
									'login_user'	=> addslashes($in['user']),
	      							'error_message'	=> ''
	      			);

        try {

        $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
        $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
        $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
        $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
        $soap->__setSoapHeaders(array($objHeader_Session_Outside));
        $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$in['user'],'password'=>$in['pass']));

        if($res->action->data[0]->_ == '000'){
          $post_id=$this->db->insert("INSERT INTO apps SET
                                                  main_app_id='0',
                                                  name='PostGreen',
                                                  api='".$in['user']."',
                                                  type='main',
                                                  active='1' ");
          $this->db->query("INSERT INTO apps SET main_app_id='".$post_id."', api='".$in['pass']."' ");
          /*msg::success(gm("PostGreen service activated"),"success");*/
          msg::success ( gm("Changes have been saved."),'success');
        }else{
          msg::error(gm('Incorrect credentials provided'),"error");

        }

    } catch (Exception $e) {
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");
        $post_green_data['error_message'] = addslashes($e->getMessage());
        error_post($post_green_data);
        return true;
    }    


        return true;
    }

    function postIt(&$in){
	      global $config;

	      $id=$in['id'];
	      $version_id=$in['version_id'];

	      $contract=$this->db->query("SELECT contracts_version.*,contracts.customer_id,contracts.customer_name,contracts.same_address,contracts.main_address_id,contracts.free_field,contracts.serial_number FROM contracts_version 
	      	INNER JOIN contracts ON contracts_version.contract_id=contracts.contract_id
	      	WHERE contracts_version.version_id='".$in['version_id']."' ");

	      $buyer_data=$this->db->query("SELECT * FROM customers WHERE customer_id='".$contract->f('customer_id')."' ");
	      if($contract->f('same_address')){
	      	$buyer_address=$this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$contract->f('same_address')."' ");
	      }else{
	      	$buyer_address=$this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$contract->f('main_address_id')."' ");
	      }
	      $seller_country=$this->db->field("SELECT name FROM country WHERE country_id='".ACCOUNT_BILLING_COUNTRY_ID."' ");

	      ark::loadLibraries(array('aws'));
	      $aws = new awsWrap(DATABASE_NAME);
	      $in['attach_file_name'] = 'contract_'.$in['id'].'_'.$in['version_id'].'.pdf';
	      $file = $aws->getItem('contract/contract_'.$in['id'].'_'.$in['version_id'].'.pdf',$in['attach_file_name']);
	      $doc_name='contract_'.$contract->f('serial_number').'['.$contract->f('version_code').'].pdf';
	      if($file !== true){
	          $in['attach_file_name'] = null;
	          msg::error(gm("Generate PDF"),"error");
	          json_out($in);
	      }

	      $handle = fopen($in['attach_file_name'], "r");   
	      $contents = fread($handle, filesize($in['attach_file_name'])); 
	      fclose($handle);                               
	      $decodeContent = base64_encode($contents);

	      $filename ="addresses.csv";
	      ark::loadLibraries(array('PHPExcel'));
	      $objPHPExcel = new PHPExcel();
	      $objPHPExcel->getProperties()->setCreator("Akti")
	               ->setLastModifiedBy("Akti")
	               ->setTitle("Addresses")
	               ->setSubject("Addresses")
	               ->setDescription("Addresses")
	               ->setKeywords("Addresses")
	               ->setCategory("Addresses");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', "TITLE")
	            ->setCellValue('B1', "FIRST_NAME")
	            ->setCellValue('C1', "LAST_NAME")
	            ->setCellValue('D1', "EMAIL")
	            ->setCellValue('E1', "PHONE")
	            ->setCellValue('F1', "MOBILE")
	            ->setCellValue('G1', "FAX")
	            ->setCellValue('H1', "COMPANY")
	            ->setCellValue('I1', "ADDRESS_LINE_1")
	            ->setCellValue('J1', "ADDRESS_LINE_2")
	            ->setCellValue('K1', "ADDRESS_LINE_3")
	            ->setCellValue('L1', "ADDRESS_POSTCODE")
	            ->setCellValue('M1', "ADDRESS_CITY")
	            ->setCellValue('N1', "ADDRESS_STATE")
	            ->setCellValue('O1', "ADDRESS_COUNTRY");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A2', "")
	            ->setCellValue('B2', "")
	            ->setCellValue('C2', "")
	            ->setCellValue('D2', $buyer_data->f('c_email'))
	            ->setCellValue('E2', $buyer_data->f('comp_phone'))
	            ->setCellValue('F2', "")
	            ->setCellValue('G2', $buyer_data->f('comp_fax'))
	            ->setCellValue('H2', $contract->f('customer_name'))
	            ->setCellValue('I2', $contract->f('free_field'))
	            ->setCellValue('J2', "")
	            ->setCellValue('K2', "")
	            ->setCellValue('L2', $buyer_address->f('zip'))
	            ->setCellValue('M2', $buyer_address->f('city'))
	            ->setCellValue('N2', "")
	            ->setCellValue('O2', get_country_name($buyer_address->f('country_id')));

	      $objPHPExcel->getActiveSheet()->setTitle('Addresses');
	      $objPHPExcel->setActiveSheetIndex(0);
	      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
	      $objWriter->setDelimiter(';');
	      $objWriter->save($filename);

	      $handle_csv = fopen($filename, "r");   
	      $contents_csv = fread($handle_csv, filesize($filename)); 
	      fclose($handle_csv);                               
	      $csvContent   = base64_encode($contents_csv);
	     
	      $env_usr = $config['postgreen_user'];
	      $env_pwd = $config['postgreen_pswd'];
	      $postg_data=$this->db->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	      $panel_usr=$postg_data->f("api");
	      $panel_pswd=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");

	      $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> $in['id'],
	      							'module'		=> 'contract',
	      							'date'			=> time(),
	      							'action'		=> 'ORDER_CREATE'.' - '.$config['postgreen_wsdl'],
	      							'login_user'	=> addslashes($panel_usr),
									'content'		=> '',
	      							'error_message'	=> ''
	      			);

	      try{

	      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
	      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
	      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
	      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
	      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
	      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));

	      $soapVr='
	        <order_create_IN>
	          <action type="string">ORDER_CREATE</action> 
	          <attachment_list type="stringlist"></attachment_list>'; 
	      if($in['stationary_id']){
	        $soapVr.='<background type="string">'.$in['stationary_id'].'</background>';
	      }else{
	        $soapVr.='<background type="string"></background>';
	      } 
	      $soapVr.='<content_parameters type="indstringlist"></content_parameters> 
	          <csv type="file" extension="csv"> 
	            <nir:data>'.$csvContent.'</nir:data> 
	          </csv> 
	          <document type="file" extension="pdf"> 
	            <nir:data>'.$decodeContent.'</nir:data> 
	          </document> 
	          <document_name type="string">'.$doc_name.'</document_name> 
	          <get_proof type="string">NO</get_proof> 
	          <identifier type="indstringlist"> 
	            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data> 
	          </identifier> 
	          <insert_list type="indstringlist"></insert_list> 
	          <mailing_type type="string">NONE</mailing_type> 
	          <order_parameters type="indstringlist"></order_parameters> 
	          <page_list type="stringlist"></page_list> 
	          <return_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </return_address> 
	          <sender_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </sender_address> 
	          <service_profile type="indstringlist"> 
	            <nir:data key="service_id">CUSTOM_ENVELOPE</nir:data> 
	            <nir:data key="print_mode">1SIDE</nir:data>
	            <nir:data key="envelop_type">CUSTOM</nir:data>
	            <nir:data key="paper_weight">80g</nir:data>
	            <nir:data key="print_color">COLOR</nir:data>
	            <nir:data key="mail_type">EXPRESS</nir:data>
	            <nir:data key="address_page">EXTRA_PAGE</nir:data>
	            <nir:data key="validation">NO</nir:data>
	          </service_profile> 
	        </order_create_IN>';

	      $post_green_data['content'] = addslashes($soapVr);  

	      $params = new \SoapVar($soapVr, XSD_ANYXML);
	      
	      $result = $soap->order_create($params);
	      if($result->action->data[0]->_ == '000'){
	        $this->db->query("UPDATE contracts_version SET postgreen_id='".$result->order_id->_."' WHERE version_id='".$in['version_id']."' ");
	        $in['contract_id']=$in['id'];
	        $in['sent_date']= time();
	        $this->mark_sent($in);
	        $msg_txt =gm('Contract successfully exported');
	      }else{
	        msg::error($result->action->data[1]->_,"error");
	        return true;
	      }

	  } catch (Exception $e) {
        unlink($in['attach_file_name']);
      	unlink($filename);
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");
        $post_green_data['error_message'] = addslashes($e->getMessage());
		if(empty($soapVr)){
			$post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
	  	}
		error_post($post_green_data);
        json_out($in);
    }

	      unlink($in['attach_file_name']);
	      unlink($filename);

	      msg::success($msg_txt,"success");
	      if($in['related']=='view'){        
	        // return true;
	      }else{
	        unset($in['id']);
	        unset($in['contract_id']);
	        unset($in['version_id']); 
	      }
	      return true;
	}

	function postContractData(&$in){
	    if($in['old_obj']['no_status']){
	        json_out($in);
	    }else{
	    	try{
		    	global $config;
		      $env_usr = $config['postgreen_user'];
		      $env_pwd = $config['postgreen_pswd'];
		      $postg_data=$this->db->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
		      $panel_usr=$postg_data->f("api");

		      $contract_id =  $this->db->field("SELECT contract_id FROM contracts_version WHERE version_id='".$in['version_id']."' ");

		      $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> $contract_id,
	      							'module'		=> 'contract',
	      							'date'			=> time(),
	      							'action'		=> 'ORDER_DETAILS'.' - '.$config['postgreen_wsdl'],
	      							'login_user'	=> addslashes($panel_usr),
	      							'content'		=> '',
	      							'error_message'	=> ''
	      			);

		      $panel_pswd=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
		      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
		      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
		      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
		      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
		      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
		      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));
		      
		      $order_id=$this->db->field("SELECT postgreen_id FROM contracts_version WHERE version_id='".$in['version_id']."' ");

		      $post_green_data['content'] = addslashes('<order_details_IN>
		          <action type="string">ORDER_DETAILS</action>
		          <get_billing>NO</get_billing>
		          <get_document>NO</get_document>
		          <get_proof>NO</get_proof>
		          <identifier type="indstringlist">
		            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
		          </identifier>
		          <order_id type="string">'.$order_id.'</order_id>
		        </order_details_IN>');	

		      $params = new \SoapVar('
		        <order_details_IN>
		          <action type="string">ORDER_DETAILS</action>
		          <get_billing>NO</get_billing>
		          <get_document>NO</get_document>
		          <get_proof>NO</get_proof>
		          <identifier type="indstringlist">
		            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
		          </identifier>
		          <order_id type="string">'.$order_id.'</order_id>
		        </order_details_IN>',
		         XSD_ANYXML);

		      $result=$soap->order_details($params);
		}catch(Exception $e){
			$post_green_data['error_message'] = addslashes($e->getMessage());

			if(empty($post_green_data['content'])){
				$post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
		  	}	
			error_post($post_green_data);
			
                  $post_status=array('status'=>gm('Postgreen service is temporarily unavailable. Please retry later.'));
	            if($in['related']=='minilist'){
	              json_out($post_status);
	            }else if($in['related']=='status'){
	              $in['status']=$post_status['status'];
	              json_out($in);
	            }else{
	              return json_encode($post_status);
	            }
	      }
	      $PGreen_status=array(
	        '100'   => gm('PostGreen Received'),
	        '140'   => gm('PostGreen Waiting for validation'),
	        '-140'  => gm('PostGreen Rejected'),
	        '160'   => gm('PostGreen Saved'),
	        '200'   => gm('PostGreen Confirmed'),
	        '210'   => gm('PostGreen Processing'),
	        '300'   => gm('PostGreen Processed'),
	        '400'   => gm('PostGreen Validated'),
	        '500'   => gm('PostGreen Printed'),
	        '600'   => gm('PostGreen Posted'),
	        '700'   => gm('PostGreen Sent'),
	        '800'   => gm('PostGreen Arrived'),
	        '810'   => gm('PostGreen Opened'),
	        '900'   => gm('PostGreen Canceled')
	      );
	      $post_status=array('status'=>gm('Post status').": ".$PGreen_status[$result->order_info->row->col[3]->data]);
	      if($in['related']=='minilist'){
	        json_out($post_status);
	      }else if($in['related']=='status'){
	        $in['status']=$post_status['status'];
	        json_out($in);
	      }else{
	        return json_encode($post_status);
	      } 
	    }      
	}
	function label_update(&$in){

	    $fields = '';
	    $table = 'label_language_contract';

	    $exist = $this->db->query("SELECT label_language_id FROM $table WHERE label_language_id='".$in['label_language_id']."' ");
	    if($exist->next()){
    		$this->db->query("UPDATE $table SET
    			contract					='".$in['contract']."',
    			contract_note 				='".$in['contract_note']."',
    			company_name 				='".$in['company_name']."',
    			comp_reg_number       		='".$in['comp_reg_number']."',
				billing_address 			='".$in['billing_address']."',
				buyer_delivery_address 		='".$in['buyer_delivery_address']."',
				buyer_delivery_zip 			='".$in['buyer_delivery_zip']."',
				buyer_delivery_city 		='".$in['buyer_delivery_city']."',
				buyer_delivery_country 		='".$in['buyer_delivery_country']."',
				buyer_main_address 			='".$in['buyer_main_address']."',
				buyer_main_zip 				='".$in['buyer_main_zip']."',
				buyer_main_city 			='".$in['buyer_main_city']."',
				buyer_main_country 			='".$in['buyer_main_country']."',
				bank_details 				='".$in['bank_details']."',
				item 						='".$in['item']."',
				subtotal 					='".$in['subtotal']."',
				unit_price 					='".$in['unit_price']."',
				vat 						='".$in['vat']."',
				amount_due 					='".$in['amount_due']."',
				notes 						='".$in['notes']."',
				bic_code 					='".$in['bic_code']."',
				phone 						='".$in['phone']."',
				email 						='".$in['email']."',
				our_ref 					='".$in['our_ref']."',
				vat_number 					='".$in['vat_number']."',
				package 					='".$in['package']."',
				article_code 				='".$in['article_code']."',
				chapter_total 				='".$in['chapter_total']."',
				subtotal_2 					='".$in['subtotal_2']."',
				reference 					='".$in['reference']."',
				date 						='".$in['date']."',
				customer 					='".$in['customer']."',
				quantity 					='".$in['quantity']."',
				amount 						='".$in['amount']."',
				discount 					='".$in['discount']."',
				payments 					='".$in['payments']."',
				grand_total 				='".$in['grand_total']."',
				bank_name 					='".$in['bank_name']."',
				iban 						='".$in['iban']."',
				fax 						='".$in['fax']."',
				url 						='".$in['url']."',
				your_ref 					='".$in['your_ref']."',
				sale_unit 					='".$in['sale_unit']."',
				general_conditions 			='".$in['general_conditions']."',
				page 						='".$in['page']."',
				valid_until 				='".$in['valid_until']."',
				stamp_signature 			='".$in['stamp_signature']."',
				download_webl 				='".$in['download_webl']."',
				name_webl 					='".$in['name_webl']."',
				addCommentLabel_webl 		='".$in['addCommentLabel_webl']."',
				quote_webl 					='".$in['quote_webl']."',
				accept_webl 				='".$in['accept_webl']."',
				reject_webl 				='".$in['reject_webl']."',
				bankDetails_webl 			='".$in['bankDetails_webl']."',
				cancel_webl 				='".$in['cancel_webl']."',
				bicCode_webl 				='".$in['bicCode_webl']."',
				bank_webl 					='".$in['bank_webl']."',
				pdfPrint_webl 				='".$in['pdfPrint_webl']."',
				email_webl 					='".$in['email_webl']."',
				submit_webl 				='".$in['submit_webl']."',
				date_webl 					='".$in['date_webl']."',
				requestNewVersion_webl 		='".$in['requestNewVersion_webl']."',
				status_webl 				='".$in['status_webl']."',
				payWithIcepay_webl 			='".$in['payWithIcepay_webl']."',
				regularInvoiceProForma_webl ='".$in['regularInvoiceProForma_webl']."',
				ibanCode_webl 				='".$in['ibanCode_webl']."',
				noData_webl 				='".$in['noData_webl']."'
                WHERE label_language_id='".$in['label_language_id']."'");
  		}else{
  			$this->db->query("UPDATE $table SET
  				contract					='".$in['contract']."',
    			contract_note 				='".$in['contract_note']."',
    			company_name 				='".$in['company_name']."',
				billing_address 			='".$in['billing_address']."',
				bank_details 				='".$in['bank_details']."',
				item 						='".$in['item']."',
				subtotal 					='".$in['subtotal']."',
				unit_price 					='".$in['unit_price']."',
				vat 						='".$in['vat']."',
				amount_due 					='".$in['amount_due']."',
				notes 						='".$in['notes']."',
				bic_code 					='".$in['bic_code']."',
				phone 						='".$in['phone']."',
				email 						='".$in['email']."',
				our_ref 					='".$in['our_ref']."',
				vat_number 					='".$in['vat_number']."',
				package 					='".$in['package']."',
				article_code 				='".$in['article_code']."',
				chapter_total 				='".$in['chapter_total']."',
				subtotal_2 					='".$in['subtotal_2']."',
				reference 					='".$in['reference']."',
				date 						='".$in['date']."',
				customer 					='".$in['customer']."',
				quantity 					='".$in['quantity']."',
				amount 						='".$in['amount']."',
				discount 					='".$in['discount']."',
				payments 					='".$in['payments']."',
				grand_total 				='".$in['grand_total']."',
				bank_name 					='".$in['bank_name']."',
				iban 						='".$in['iban']."',
				fax 						='".$in['fax']."',
				url 						='".$in['url']."',
				your_ref 					='".$in['your_ref']."',
				sale_unit 					='".$in['sale_unit']."',
				general_conditions 			='".$in['general_conditions']."',
				page 						='".$in['page']."',
				valid_until 				='".$in['valid_until']."',
				stamp_signature 			='".$in['stamp_signature']."',
				download_webl 				='".$in['download_webl']."',
				name_webl 					='".$in['name_webl']."',
				addCommentLabel_webl 		='".$in['addCommentLabel_webl']."',
				quote_webl 					='".$in['quote_webl']."',
				accept_webl 				='".$in['accept_webl']."',
				reject_webl 				='".$in['reject_webl']."',
				bankDetails_webl 			='".$in['bankDetails_webl']."',
				cancel_webl 				='".$in['cancel_webl']."',
				bicCode_webl 				='".$in['bicCode_webl']."',
				bank_webl 					='".$in['bank_webl']."',
				pdfPrint_webl 				='".$in['pdfPrint_webl']."',
				email_webl 					='".$in['email_webl']."',
				submit_webl 				='".$in['submit_webl']."',
				date_webl 					='".$in['date_webl']."',
				requestNewVersion_webl 		='".$in['requestNewVersion_webl']."',
				status_webl 				='".$in['status_webl']."',
				payWithIcepay_webl 			='".$in['payWithIcepay_webl']."',
				regularInvoiceProForma_webl ='".$in['regularInvoiceProForma_webl']."',
				ibanCode_webl 				='".$in['ibanCode_webl']."',
				noData_webl 				='".$in['noData_webl']."'
                WHERE label_language_id='".$in['label_language_id']."'");
 		}
    	msg::success ( gm('Account has been successfully updated'),'success');
    	// $in['failure']=false;

    	return true;
  	}

  	function reset_data(&$in){
	    if($in['header'] && $in['header']!='undefined'){
	      $variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
	    }elseif($in['footer'] && $in['footer']!='undefined'){
	      $variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
	    }
	    $result=array("var_data" => $variable_data);
	    json_out($result);
	    return true;
	  }

  	function default_pdf_format(&$in)
	{

		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='contract' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_header(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='contract' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_CONTRACT_HEADER_PDF_FORMAT'");
			
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_CONTRACT_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_body(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_CONTRACT_BODY_PDF_FORMAT'");
			
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_footer(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_CONTRACT_FOOTER_PDF_FORMAT'");
		
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function pdfSaveData(&$in){
         
         $in['variable_data']=str_replace_last('<p>&nbsp;</p>','',$in['variable_data']);

		if($in['header']=='header'){
			$this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='contract' AND type='".$in['header']."' AND initial='1' ");
			$exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");
			if($exist){
				$this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
			}else{
				$this->db->query("INSERT INTO tblquote_pdf_data SET master='contract',
													type='".$in['header']."',
													content='".$in['variable_data']."',
													initial='1', 
													layout='".$in['layout']."',
													`default`='1' ");
			}
		}else{
			$this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='contract' AND type='".$in['footer']."' AND initial='1' ");
			$exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

			if($exist){
				$this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
			}else{
				$this->db->query("INSERT INTO tblquote_pdf_data SET master='contract',
													type='".$in['footer']."',
													content='".$in['variable_data']."',
													initial='1', 
													layout='".$in['layout']."',
													`default`='1' ");
			}
		}
		msg::success ( gm('Data saved'),'success');
		return true;
	}
	function save_language(&$in){
		switch ($in['languages']){
			case '1':
				$lang = '';
				break;
			case '2':
				$lang = '_2';
				break;
			case '3':
				$lang = '_3';
				break;
			case '4':
				$lang = '_4';
				break;
			default:
				$lang = '';
				break;
		}

		# extra check for custom_langs
		if($in['languages']>=1000) {
			$lang = '_'.$in['languages'];
		}
		if($this->db->field("SELECT default_id FROM default_data WHERE type='contract_note".$lang."' AND default_main_id='0' ")){
			$this->db->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='contract_note".$lang."' AND default_main_id='0' ");
		}else{
			$this->db->query("INSERT INTO default_data SET value='".$in['notes']."', type='contract_note".$lang."', default_main_id='0', default_name='contract_note' ");
		}
		if($in['terms']){
			$this->db->query("DELETE FROM default_data WHERE type='contract_terms".$lang."' AND default_main_id='0' ");
			$this->db->query("INSERT INTO default_data SET value='".$in['terms']."', type='contract_terms".$lang."',  default_name='contract_term'  ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function default_language(&$in){
		msg::success ( gm("Changes have been saved."),'success');
		return true;

	}

	function saveArticleField(&$in){
		$v = new validation($in);
		$v->field('text', 'Field', 'required', gm("Invalid Field Label"));

		if(!$v->run()){
			return false;
		}

		//Check if User Has Catalog +
		//Check if Article Name, Article Name 2 or Description are written in text input
		if(!$in['ADV_PRODUCT']){
			$fieldExist = false;
			$fields = array('[!ITEM_NAME!]','[!ITEM_NAME2!]','[!DESCRIPTION!]');
			foreach ($fields as $value) {
				if(strpos($in['text'],$value) !== false){
					$fieldExist = true;
					break;
				}
			}
			if($fieldExist){
				msg::error(gm('You must have Catalag+ in order to add the article desired fields'),'error');
				return false;
			}
		}

	 	$this->db->insert("UPDATE settings SET long_value='".$in['text']."' WHERE constant_name='CONTRACT_FIELD_LABEL' ");
	 	msg::success(gm('Field updated.'),'success');
		return true;
	}

	function atach(&$in)
	{

		$response=array();
		if (!empty($_FILES)) {
			$fileImages = array('jpg','jpeg','gif'); // File extensions
			$size = $_FILES['Filedata']['size'];
			if(!defined('MAX_IMAGE_SIZE') && $size>512000 && in_array(strtolower($fileParts['extension']),$fileImages)){
                  $response['error'] = gm('Size exceeds 500kb');
                  $response['filename'] = $_FILES['Filedata']['name'];
                echo json_encode($response);
                exit();
            }
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
/*			$targetPath = $in['path'];
*/			$in['name'] = $_FILES['Filedata']['name'];
			$targetPath = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/contracts';

			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','pdf','docx'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				 if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE && in_array(strtolower($fileParts['extension']),$fileImages)){
                    $image = new SimpleImage();
                    $image->load($_FILES['Filedata']['tmp_name']);
                    $image->scale(MAX_IMAGE_SIZE,$size);
                    $image->save($targetFile);
                }else{
                    move_uploaded_file($tempFile,$targetFile);             
                }
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);
				$db_upload = new sqldb($database_2);
				$file_id = $db_upload->insert("INSERT INTO attached_files SET `path` = 'upload/".DATABASE_NAME."/contracts/', name = '".$in['name']."', type='5' ");
				$response['success'] = 'success';
				$response['filename'] = $_FILES['Filedata']['name'];
        echo json_encode($response);
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
				$response['filename'] = $_FILES['Filedata']['name'];
        echo json_encode($response);
				// echo gm('Invalid file type.');
			}
		}
	}

	function deleteatach(&$in){
		$path=$this->db->field("SELECT `path` FROM attached_files WHERE file_id='".$in['id']."' ");
           unlink($path.stripslashes($in['name']));
		$this->db->query("DELETE FROM attached_files WHERE file_id='".$in['id']."' AND name='".$in['name']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function defaultcheck(&$in){

		$this->db->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function default_message(&$in)
	{
		$set = "text	= '".$in['text']."', ";
		/*if($in['use_html']){*/
			$set1 = "html_content	= '".$in['html_content']."', ";
		/*}*/

		$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					".$set."
					".$set1."
					use_html = 1 
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		/*$this->db->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
		msg::success( gm('Message has been successfully updated'),'success');
		// json_out($in);
		return true;
	}

	function web_link(&$in)
	{
		if($in['contract_link']!=$in['old_contract_link']){
			$in['contract_contract_change']=true;
		}
		if($in['contract_pdf']!=$in['old_contract_pdf']){
			$in['contract_pdf_change']=true;
		}
		if($in['contract_accept']!=$in['old_contract_accept']){
			$in['contract_accept_change']=true;
		}
		if($in['contract_comment']!=$in['old_contract_comment']){
			$in['contract_comment_change']=true;
		}
		if($in['contract_reject']!=$in['old_contract_reject']){
			$in['contract_reject_change']=true;
		}
		if($in['contract_new']!=$in['old_contract_new']){
			$in['contract_new_change']=true;
		}

		//Check / Insert Contract Weblink Settings
		$this->check_insert_contract_web_link_settings();

		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CONTRACT_WEB_LINK' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_CONTRACT_ACCEPT' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_CONTRACT_COMMENTS' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='WEB_INCLUDE_PDF_CONTRACT' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_CONTRACT_REJECT' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_CONTRACT_NEWV' ");
		if($in['contract_link']=='1'){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='USE_CONTRACT_WEB_LINK' ");
			if($in['contract_accept']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_CONTRACT_ACCEPT' ");
			}
			if($in['contract_comment']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_CONTRACT_COMMENTS' ");
			}
			if($in['contract_pdf']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='WEB_INCLUDE_PDF_CONTRACT' ");
			}
			if($in['contract_reject']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_CONTRACT_REJECT' ");
			}
			if($in['contract_new']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_CONTRACT_NEWV' ");
			}
		}else{
			return false;
		}


		msg::success ( gm("Changes have been saved."),'success');
    	// $in['failure']=false;
    	return true;
    }

    function check_insert_contract_web_link_settings(){

    	$use_contract_web_link = $this->db->query("SELECT * FROM settings WHERE constant_name='USE_CONTRACT_WEB_LINK' ")->getAll();

    	if(empty($use_contract_web_link)){
    		$this->db->insert("INSERT INTO settings SET constant_name = 'USE_CONTRACT_WEB_LINK'");	
    	}
    	
		$allow_contract_accept = $this->db->query("SELECT * FROM settings WHERE constant_name='ALLOW_CONTRACT_ACCEPT' ")->getAll();
		if(empty($allow_contract_accept)){
    		$this->db->insert("INSERT INTO settings SET constant_name = 'ALLOW_CONTRACT_ACCEPT'");	
    	}


		$allow_contract_comments = $this->db->query("SELECT * FROM settings WHERE constant_name='ALLOW_CONTRACT_COMMENTS' ")->getAll();
		if(empty($allow_contract_comments)){
    		$this->db->insert("INSERT INTO settings SET constant_name = 'ALLOW_CONTRACT_COMMENTS'");	
    	}

		$web_include_pdf_contract = $this->db->query("SELECT * FROM settings WHERE constant_name='WEB_INCLUDE_PDF_CONTRACT' ")->getAll();
		if(empty($web_include_pdf_contract)){
    		$this->db->insert("INSERT INTO settings SET constant_name = 'WEB_INCLUDE_PDF_CONTRACT'");	
    	}

		$allow_contract_reject = $this->db->query("SELECT * FROM settings WHERE constant_name='ALLOW_CONTRACT_REJECT' ")->getAll();
		if(empty($allow_contract_reject)){
    		$this->db->insert("INSERT INTO settings SET constant_name = 'ALLOW_CONTRACT_REJECT'");	
    	}

		$allow_contract_newv = $this->db->query("SELECT * FROM settings WHERE constant_name='ALLOW_CONTRACT_NEWV' ")->getAll();
    	if(empty($allow_contract_newv)){
    		$this->db->insert("INSERT INTO settings SET constant_name = 'ALLOW_CONTRACT_NEWV'");	
    	}
    }

    /**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function external_id(&$in)
	{
		if(defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1){
			//$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `type`='c' AND version_id='".$in['version_id']."' ");
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :t AND version_id= :version_id ",['d'=>DATABASE_NAME,'item_id'=>$in['id'],'t'=>'c','version_id'=>$in['version_id']]);
			if(!$exist_url){
				$url = generate_chars();
				while (!isUnique($url)) {
					$url = generate_chars();
				}
				//$this->db_users->query("INSERT INTO urls SET `url_code`='".$url."', `database`='".DATABASE_NAME."', `item_id`='".$in['id']."', `type`='c', version_id='".$in['version_id']."' ");
				$this->db_users->insert("INSERT INTO urls SET `url_code`= :url_code, `database`= :d, `item_id`= :item_id, `type`= :t, version_id=:version_id ",['url_code'=>$url,'d'=>DATABASE_NAME,'item_id'=>$in['id'],'t'=>'c','version_id'=>$in['version_id']]);
				$exist_url = $url;
			}
			return $exist_url;
		}
	}

	function updateSegment(&$in){
		$this->db->query("UPDATE contracts SET segment_id='".$in['segment_id']."' WHERE contract_id='".$in['contract_id']."' ");
		if($in['segment_id']){
			$in['segment'] = $this->db->field("SELECT name FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function updateSource(&$in){
		$this->db->query("UPDATE contracts SET source_id='".$in['source_id']."' WHERE contract_id='".$in['contract_id']."' ");
		if($in['source_id']){
			$in['source'] = $this->db->field("SELECT name FROM tblquote_source WHERE id='".$in['source_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function updateType(&$in){
		$this->db->query("UPDATE contracts SET type_id='".$in['type_id']."' WHERE contract_id='".$in['contract_id']."' ");
		if($in['type_id']){
			$in['xtype']=$this->db->field("SELECT name FROM tblquote_type WHERE id='".$in['type_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

} // END class
?>