<?php

class contract_version{

	private $dbu;
	var $pag = 'contract'; 						# used for log messages
	var $field_n = 'contract_id';					# used for log messages

	public function __construct(){
		$this->dbu = new sqldb();
		$this->db = new sqldb();
	}

	public function add(&$in){
		//we need to get the quote and duplicate the lines
		$now = time();
		$this->dbu->query("UPDATE contracts_version SET active = 0 WHERE contract_id='".$in['contract_id']."'");
		$in['version_id'] = $this->dbu->insert("INSERT INTO contracts_version SET
					contract_id         =	'".$in['contract_id']."',
					version_code     =	'".$this->next_version_code($in['contract_id'])."',
					active           =	'1',
					discount      	    	=   '".$in['discount']."',
                    discount_line_gen      	=   '".$in['discount_line_gen']."',
                    apply_discount            =   '".$in['apply_discount']."',
					version_date	 =	'".$now."' ");
		return true;

	}

	public function next(&$in){
		if(!$this->next_validate($in)){
			return false;
		}

		//we add a new quote version
		if(!$this->add($in)){
			return false;
		}
		//get all the lines and move them over
		$groups = $this->dbu->query("SELECT * FROM contracts_group WHERE version_id = '".$in['current_version_id']."' AND contract_id = '".$in['contract_id']."'");
		while ($groups->next()) {
			$fields = $groups->next_array();
			//remove old id
			unset($fields['group_id']);

			$fields['version_id'] = $in['version_id'];

			$sql = $this->gen_sql($fields);
			$group_id = $this->dbu->insert("INSERT INTO contracts_group SET ".$sql);

			//update lines of this group;
			$lines = $this->dbu->query("SELECT * FROM contract_line WHERE group_id = '".$groups->f('group_id')."' AND contract_id = '".$in['contract_id']."'");
			while ($lines->next()) {
				$fields = $lines->next_array();
				//remove old id
				unset($fields['id']);

				$fields['version_id'] = $in['version_id'];
				$fields['group_id'] = $group_id;

				$sql = $this->gen_sql($fields);

				$this->dbu->query("INSERT INTO contract_line SET ".$sql);
			}
		}
		// insert contract note for specific version of contract
			$note_data = $this->db->query("SELECT item_value, lang_id, active 	FROM 	note_fields
																				WHERE 	item_id 	= 	'".$in['contract_id']."'
																				AND 	item_type 	= 	'contract'
																				AND 	item_name 	= 	'notes'
																				AND 	version_id 	= 	'".$in['current_version_id']."'

				");
			while($note_data->next()){
				$this->db->query("INSERT INTO note_fields 	SET 	item_value 	= 	'".addslashes($note_data->f('item_value'))."',
																	lang_id 	= 	'".$note_data->f('lang_id')."',
																	active 		= 	'".$note_data->f('active')."',
																	version_id 	= 	'".$in['version_id']."',
																	item_id 	= 	'".$in['contract_id']."',
																	item_type 	= 	'contract',
																	item_name 	= 	'notes'

					");
			}
		// insert the general notes
		$gen = $this->db->query("SELECT * FROM note_fields WHERE item_type='contract' AND item_name='free_text_content' AND version_id='{$in['current_version_id']}' AND item_id='{$in['contract_id']}' ");
		$this->db->query("INSERT INTO note_fields SET
				item_value				= 	'".addslashes($gen->f('item_value'))."',
				item_type 				= 	'contract',
				item_name 				= 	'free_text_content',
				version_id 				= 	'".$in['version_id']."',
				item_id 				= 	'".$in['contract_id']."' ");

		msg::success ( gm("Contract Version was created"),'success');
		insert_message_log($this->pag,'{l}Contract Version was created by{endl} '.get_user_name($_SESSION['u_id']),$field_n,$in['contract_id']);
		// ark::$controller = 'nquote';
		json_out($in);
		return true;
	}

	public function activate(&$in){
		if(!$this->activate_validate($in)){
			return false;
		}

		$this->dbu->query("UPDATE contracts_version SET active = 0 WHERE contract_id='".$in['contract_id']."'");
		$this->dbu->query("UPDATE contracts_version SET active = 1 WHERE contract_id='".$in['contract_id']."' AND version_id = '".$in['version_id']."'");

		msg::$success = gm("Contract Version was selected");
		insert_message_log($this->pag,'{l}Contract Version was selected{endl} '.get_user_name($_SESSION['u_id']),'contract_id',$in['contract_id']);
		return true;
	}

	private function next_validate(&$in){
		$v = new validation($in);
		//quote_id, current_version_id
		$v->field('contract_id','Contract','required');
		$v->field('current_version_id','Contract Version','required');

		return $v->run();
	}

	private function activate_validate(&$in){
		$v = new validation($in);
		//quote_id, current_version_id
		$v->field('contract_id','Contract','required');
		$v->field('version_id','Contract Version','required');

		return $v->run();
	}

	private function gen_sql($fields){
		$sql = '';
		foreach ($fields as $field => $value) {
			$sql .= $field . " = '".addslashes($value)."',";
		}
		$sql = trim($sql,',');
		return $sql;
	}

	private function next_version_code($id){
		$alphas = range('A', 'Z');
		$pos = array_flip($alphas);

		$this->dbu->query("SELECT contracts_version.* FROM contracts_version WHERE contract_id = '".$id."' ORDER BY version_id DESC LIMIT 1");
		if($this->dbu->move_next() ){
			$next_version_code = $alphas[$pos[$this->dbu->f('version_code')]+1];
		}
		else{
		   $next_version_code='A';
		}

		return $next_version_code;
	}

	public function delete_validate(&$in) {
		$v = new validation($in);
		$v->f('contract_id', 'Id', 'required:exist[contracts.contract_id]', gm('Invalid Id'));
		$v->f('version_id', 'Id', 'required:exist[contracts_version.version_id]', gm('Invalid version Id'));
		return $v->run();
	}

	public function delete(&$in) {
		if(!$this->delete_validate($in)) {
			return false;
		}

		$current_version = $this->db->field("SELECT version_id FROM contracts_version WHERE contract_id = '".$in['contract_id']."' AND version_id != '".$in['version_id']."' ");
		if(!$current_version) {
			msg::error ( gm("Last contract version can't be deleted! "),'error');
			return false;
		}

		$this->db->query("DELETE FROM contracts_group WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM contract_line WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM contracts_version WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'contract' AND item_name = 'notes' AND item_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'contract' AND item_name = 'free_text_content' AND item_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");

		$last_contract = $this->db->query("SELECT version_id FROM contracts_version WHERE contract_id = '".$in['contract_id']."' ORDER BY version_id DESC LIMIT 1");
		$this->db->query("UPDATE contracts_version SET active= 1 WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$last_contract->f('version_id')."' ");

		// msg::$success=gm("Quote version delete.");
		msg::success ( gm("Contract version delete."),'success');
		return true;


	}
}