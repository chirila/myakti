<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_codelabels($in,$showin=true,$exit=true){
		$db = new sqldb();
		
		$content='';
		if($in['header']=='header'){
			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");

			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}else{

			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='contract' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}
		$search = array('[!buyer_address!]','[!buyer_zip!]','[!buyer_city!]','[!buyer_country!]');
		$replace = array('[!buyer_delivery_address!]','[!buyer_delivery_zip!]','[!buyer_delivery_city!]','[!buyer_delivery_country!]');
/*		$content = str_replace($search,$replace,$content);*/

		$data['header']=$in['header'];
		$data['footer']=$in['footer'];
		$data['variable_data'] = $content;
		$data['layout']=$in['layout'];
			
		return json_out($data, $showin,$exit);
	}

	function get_selectlabels($in,$showin=true,$exit=true){
		$db = new sqldb();
		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		/*$quote_columns=$db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_quote' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ");*/
		$quote=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_quote' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ");
		$columns=array();
		$make_out=array();
		if(!$in['make_id']){
			$in['make_id']='0';
		}
		$labels = $db->query("SELECT * FROM label_language_quote WHERE lang_code='".$_SESSION['l']."' ")->getAll();		
		$labels = array_map("strtolower",$labels[0]);
		asort($labels);

		foreach ($labels as $key => $value) {
			if(!strpos($key, '_webl') && $value && $key!='label_language_id'){
				array_push($make_out, array(
	            	'id_value'=>$key,
	            	'name'=>ucfirst($value),
	        	));
			}
		}

/*		while($quote_columns->next()){
			$temp_description=$db->field("SELECT `{$quote_columns->f('COLUMN_NAME')}` FROM label_language_quote WHERE lang_code='".$_SESSION['l']."' ");
			if(!$temp_description){
				continue;
			}
			array_push($make_out, array(
            	'id_value'=>$quote_columns->f('COLUMN_NAME'),
            	'name'=>ucfirst($temp_description),
        	));
		}*/

		$data['make_id']=$in['make_id'];
		$data['make']=$make_out;

		asort($columns);
		

		$final_select='';
		foreach($columns as $key=>$value){
			array_push($data['labels'], array(
				'code'		=> $key,
				'value'		=> $value,
			));
		}
		
		$quote_colum=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_quote' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active')  AND COLUMN_NAME='".$in['make_id']."'");
		if($data['make_id']!='0'){
			$data['selected_make']='[label]'.$quote_colum.'[/label]';
		}
		return json_out($data, $showin,$exit);
	}
	function get_selectdetails($in,$showin=true,$exit=true){
		$db = new sqldb();
		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		$array_values=array();
		$array_values['quote_contact_name']=gm("Contact name");
		$array_values['buyer_reference']=gm("Reference of the buyer");
		$array_values['buyer_delivery_name']=gm("Name of the buyer delivery");
		$array_values['buyer_delivery_country']=gm("Country of the buyer delivery");
		$array_values['buyer_delivery_city']=gm("City of the buyer delivery");
		$array_values['buyer_delivery_zip']=gm("Zip of the buyer delivery");
		$array_values['buyer_delivery_address']=gm("Address of the buyer delivery");
		$array_values['buyer_main_name']=gm("Name of the buyer main");
		$array_values['buyer_main_country']=gm("Country of the buyer main");
		$array_values['buyer_main_city']=gm("City of the buyer main");
		$array_values['buyer_main_zip']=gm("Zip of the buyer main");
		$array_values['buyer_main_address']=gm("Address of the buyer main");
		$array_values['serial_number']=gm("Serial number of the quote");
		$array_values['quote_date']=gm("The date of the quote");
		$array_values['own_reference']=gm("Your own reference");
		$array_values['seller_name']=gm("Account company name");
		$array_values['seller_address']=gm("Account company address");
		$array_values['seller_zip']=gm("Account company zip");
		$array_values['sellet_city']=gm("Account company city");
		$array_values['seller_phone']=gm("Account company phone");
		$array_values['seller_fax']=gm("Account company fax");
		$array_values['seller_name']=gm("Account company name");
		$array_values['seller_country']=gm("Account company country");
		$array_values['seller_email']=gm("Account company email");
		$array_values['seller_url']=gm("Account company url");
		$array_values['seller_bank']=gm("Account company bank name");
		$array_values['seller_bic']=gm("Account company bic code");
		$array_values['seller_iban']=gm("Account company iban");
		$array_values['seller_vat']=gm("Account company vat number");
		$array_values['buyer_vat']=gm("Vat of the buyer");
		$array_values['notes']=gm("Notes of the quote");
		$array_values['free_text_content']=gm("General conditions");
		$array_values['delivery_address']=gm("Adrress of the delivery");
		$array_values['validity_date']=gm("Valid date");
		$array_values['seller_reg_number']=gm("Company Registration Number");
		$array_values['buyer_reg_number']=gm("Buyer Registration Number");

		asort($array_values);
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		$data['detail']=$detail;
		if($data['detail_id']!='0'){
			$data['selected_detail']='[!'.$in['detail_id'].'!]';
		}
		return json_out($data, $showin,$exit);
	}



	$result = array(
		'codelabels'		=> get_codelabels($in,true,false),
		'selectlabels'		=> get_selectlabels($in,true,false),
		'selectdetails'		=> get_selectdetails($in,true,false),
	);


json_out($result);
?>