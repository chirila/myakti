<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
session_write_close();
global $config;
$db5 = new sqldb();
$quote_data = $db5->query("SELECT preview, show_grand, remove_vat, discount, apply_discount, vat FROM contracts WHERE contract_id = '".$in['contract_id']."' ");
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");
$version_data =$db5->field("SELECT preview FROM contracts_version WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");

if($version_data == 1 && !$in['save_as']){

	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);
	if(ark::$model == 'contract' && (ark::$method == 'send' || ark::$method == 'sendNewEmail')){
		$in['attach_file_name'] = 'contract_'.$in['contract_id'].'_'.$in['version_id'].'.pdf';
		$file = $aws->getItem('contract/contract_'.$in['contract_id'].'_'.$in['version_id'].'.pdf',$in['attach_file_name']);

		if($file === true){
			return;
		}else{
			$in['attach_file_name'] = null;
		}
	}else{		
		$link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/contract/contract_'.$in['contract_id'].'_'.$in['version_id'].'.pdf');
		$content = file_get_contents($link);
		$q_serial_number=$db->field("SELECT serial_number FROM contracts WHERE contract_id='{$in['contract_id']}' ");

		if($q_serial_number){
             $name=$q_serial_number.'.pdf';
		}else{
			$name='contract_'.$in['contract_id'].'.pdf';
		}
		if($content){
			doQueryLog();
			header('Content-Type: application/pdf');
			header("Content-Disposition:inline;filename=".$name);
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $content;
			exit();
		}else{
			$in['upload']='0';
		}
	}
}

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetFont('helvetica', '', 10, '', false);
if($custom_lng){
	#we can use both font types but dejavusans looks better
	$pdf->SetFont('dejavusans', '', 9, '', false);
	// $pdf->SetFont('freeserif', '', 10, '', false);
}
$pdf_type =array(4,5);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Contract');
$pdf->SetSubject('Contract');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, 30);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

/*if(in_array($in['type'], $pdf_type)){
	$pdf->SetMargins(10, 10, 10);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, $a_custom_page_break);
	$hide_all = 2;
}*/

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();
$pdf->customTmargin = 15;

$htmll = include('print_body.php');
 //print_r($htmll); exit();
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM+20);

//console::benchmark('writeHTML');
$pdf->writeHTML($htmll, true, false, true, false, '');
//console::end('writeHTML');
// if($in['height']){
	// echo 'height = '.$height; echo "<br/>";
	// print_r($pdf->GetY());
// }
$height_generated = $pdf->GetY();
if($height_generated-8 > $height && $in['type'] !=3){
	$pdf->AddPage();
}

$pdf->SetY($height);
if(in_array($in['type'], $pdf_type)){
	$hide_all = 1;
	// $pdf->SetAutoPageBreak(TRUE, 0);
	// if($in['type'] == 5){
		$pdf->SetY($height-11);
		$pdf->SetAutoPageBreak(TRUE, 10);

	$htmls = include('print_body.php');
// print_r($htmls);exit();
	$pdf->writeHTML($htmls, true, false, true, false, '');
}
$pdf->lastPage();
$in['last_page'] = $pdf->getPage();

if(isset($in['print'])){
	echo "<pre>";
	echo $htmll;
	exit();
}
doQueryLog();
if($in['save_as'] == 'F'){
	#we need to delete all the old images
	$img = glob(__DIR__.'/../../../../upload/'.DATABASE_NAME.'/contract_cache/contract_'.$in['contract_id'].'_'.$in['version_id'].'_*.png');
	foreach ($img as $filename) {
		@unlink($filename);
	}
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$a->deleteItems($config['awsBucket'].DATABASE_NAME.'/contract_cache/contract_'.$in['contract_id'].'_'.$in['version_id'].'_');
	$in['contract_pdf_name'] = 'contract_'.time().'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['contract_pdf_name'], 'F');
	unlink(__DIR__.'/../../../'.$in['contract_pdf_name']);
}else if($in['upload'] == '0'){
	$in['contract_pdf_name'] = 'contract_'.unique_id(32).'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['contract_pdf_name'], 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['contract_pdf_name'];
	$pdfFile = 'contract/contract_'.$in['contract_id'].'_'.$in['version_id'].".pdf";
	$a->uploadFile($pdfPath,$pdfFile);
	unlink($in['contract_pdf_name']);
	$db->query("UPDATE contracts_version SET preview='1' WHERE contract_id = '".$in['contract_id']."' AND version_id = '".$in['version_id']."' ");
	$pdf->Output($serial_number.' ['.$version_code.'].pdf','I');
}else if($in['do']=='contract-print'){
	$pdf->Output($serial_number.' ['.$version_code.'].pdf','I');
}else{
	$pdf->Output(__DIR__.'/../../../'.'contract_.pdf', 'F');
}
