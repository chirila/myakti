<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}


	function get_PDFlayout($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'layout_header'=>array(),'custom_layout'=>array(),'layout_body'=>array(),'layout_footer'=>array());
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_PDF_FORMAT' ");
		$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$in['identity_id'] = $db->field("SELECT value FROM settings WHERE constant_name='QUOTE_IDENTITY_SET' ");
		if(!$in['identity_id']){
			// $identity = $db->field("SELECT identity_id FROM multiple_identity");
			$in['identity_id']='0';
		}
		$data['font_pdf'] = $tz=array(
								    array('id' => "1" , 'name' => gm("Helvetica Neue")),
							        array('id' => "2" , 'name' => gm("Roboto")),
							        array('id' => "3" , 'name' => gm("Lato")),
							        array('id' => "4" , 'name' => gm("Vollkorn")),
							        array('id' => "5" , 'name' => gm("Open Sans")),
							        array('id' => "6" , 'name' => gm("Ubuntu")),
						    );
		$data['font_pdf_id'] = '';
		$existIdentity = $db->query("SELECT identity_id FROM multiple_identity  ");
		$data['multiple_identity']				= build_identity_dd($in['identity_id']);
		$data['identity']						= $in['identity_id'];
		for ($j=1; $j <= 1; $j++) {
		array_push($data['custom_layout'], array(
				'view_invoice_custom_pdf' 						=>'index.php?do=quote-print&custome_type='.$j.'&lid='.$language,
				'img_href_custom'								=> 'images/custom_type-'.$j.'.jpg',
				'custom_type'									=> $j,
				//'action_href'									=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$j.'&tab=6',
				'selected_custom'								=> $def == $j && $use == 1 ? 'active' : '',
			));
		}
		$page = $db->field("SELECT value FROM settings WHERE constant_name='USE_QUOTE_PAGE_NUMBERING'");
		$stamp = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_QUOTE_STAMP_SIGNATURE'");
		$data['use_page_numbering']					= $page == '1' ? true : false;
		$data['show_stamp']							= $stamp == '1' ? true : false;
		//

		$pdf_active = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_PDF'");
		$pdf_note = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_NOTE'");
		$pdf_conditions = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_CONDITIONS'");
		$pdf_stamp = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_SIGNATURE'");
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$data['lang_id']=$language;
		$data['pdf_active'] = $pdf_active == '1' ? true : false;
		$data['pdf_note'] = $pdf_note == '1' ? true : false;
		$data['pdf_condition'] = $pdf_conditions == '1' ? true : false;
		$data['pdf_stamp'] = $pdf_stamp == '1' ? true : false;

		$data['databasename']=DATABASE_NAME;
		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CONTRACT_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_CONTRACT_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CONTRACT_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_CONTRACT_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CONTRACT_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_CONTRACT_PDF' ");
		$number_type = $_SESSION['u_id']<10737 ? 1 : 1;
		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=contract-print&layout='.$z.'&lid='.$language.'&header='.$data['header'].'&header_id='.$def_header,
					'img_href'						=> 'images/type_header-'.$z.'.jpg',
					'type'							=> $number_type,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=contract-print&layout='.$y.'&lid='.$language.'&body='.$data['body'],
					'img_href'						=> 'images/type_body-'.$y.'.jpg',
					'type'							=> $number_type,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',
				));
		}
		for ($i=1; $i <= 1; $i++) {
				array_push($data['layout_footer'], array(
					'view_invoice_pdf' 				=>'index.php?do=contract-print&layout='.$i.'&lid='.$language.'&footer='.$data['footer'].'&footer_id='.$i,
					'img_href'						=> 'images/type_footer-'.$i.'.jpg',
					'type'							=> $i,
					'footer'						=> 'footer',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
				));
		}

		$data['selected_header'] = $def_header;
		$data['selected_body'] = $def_body;
		$data['selected_footer'] = $def_footer;




		//

		return json_out($data, $showin,$exit);
	}
	function get_CustomizePDF($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('layout'=>array(), 'layout_footer'=>array());
		$pdf_active = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_PDF'");
		$pdf_note = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_NOTE'");
		$pdf_conditions = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_CONDITIONS'");
		$pdf_stamp = $db->field("SELECT value FROM settings WHERE constant_name='CUSTOMIZABLE_QUOTE_SIGNATURE'");
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$data['pdf_active'] = $pdf_active == '1' ? true : false;
		$data['pdf_note'] = $pdf_note == '1' ? true : false;
		$data['pdf_condition'] = $pdf_conditions == '1' ? true : false;
		$data['pdf_stamp'] = $pdf_stamp == '1' ? true : false;
		$data['layouts']=array();
		for($i=0;$i<6;$i++){
			if($i==0){
				array_push($data['layouts'], array('key'=>$i,'layout'=>gm('Select')));
			}else{
				array_push($data['layouts'], array('key'=>$i,'layout'=>'layout'.$i));
			}		
		}

		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['footer']='footer';

		for ($i=1; $i <= 5; $i++) {
				array_push($data['layout'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$i.'&lid='.$language.'&header='.$data['header'].'&footer='.$data['footer'],
					'img_href'						=> '../pim/img/type-'.$i.'_i.jpg',
					'type'							=> $i,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def == $i && $use == 0 ? 'active' : '',
				));
		}
		for ($j=1; $j <= 5; $j++) {
				array_push($data['layout_footer'], array(
					'view_invoice_pdf' 				=>'index.php?do=quote-print&layout='.$j.'&lid='.$language.'&footer='.$data['footer'].'&header='.$data['header'],
					'img_href'						=> '../pim/img/type-'.$j.'_i.jpg',
					'type'							=> $j,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def == $j && $use == 0 ? 'active' : '',
				));
		}

		return json_out($data, $showin,$exit);
	}
	function get_Convention($in,$showin=true,$exit=true){
		$db = new sqldb();
		$account_number = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_START' ");
		$account_digits = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_DIGIT_NR' ");
		$account_reference = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_REF' ");
		$account_del = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_DEL' ");
		$data['account_number']=$account_number;
		$data['account_digits']=$account_digits;
		$data['example']=$account_number.str_pad(1,$account_digits,"0",STR_PAD_LEFT);
		$data['reference'] = $account_reference == '1' ? true : false;
		$data['del'] =$account_del;

		return json_out($data, $showin,$exit);
	}
	function get_general($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('general_condition'=>array());
		$filter = '';
		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
			if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
			}
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
				$filter = '_'.$in['languages'];
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
				if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
				}
			}
		}
		$default_table = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='contract_note".$filter."' ");
		$terms = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='contract_terms".$filter."' ");
		/*$page_general_condition = $db->field("SELECT value FROM settings WHERE constant_name='QUOTE_GENERAL_CONDITION_PAGE'");*/
		$data['general_condition']= array(
			'terms'			=> $terms,
			'notes'			=> $default_table,
			'translate_cls'	=> 'form-language-'.$code,
			'languages'		=>  $in['languages'],
			//'page'			=>	$page_general_condition == '1' ? true : false,
			'do'			=> 'contract-settings-contract-default_language',
			'xget'			=> 'general',
			'language_dd' 	=>  build_language_dd_new($in['languages']),
		);
		return json_out($data, $showin,$exit);
	}

	function get_customLabel($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'customLabel'=>array());

		$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
		while($pim_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 		=> gm($pim_lang->f('language')),
			'do'												=> 'contract-settings',
			'xget'											=> 'labels',
			'label_language_id'	     		=> $pim_lang->f('lang_id'),
			'label_custom_language_id'	=> '',
			));
		}
		$pim_custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
		while($pim_custom_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 				=> $pim_custom_lang->f('language'),
			'do'														=> 'contract-settings',
			'xget'													=> 'labels',
			'label_custom_language_id'	    => $pim_custom_lang->f('lang_id'),
			'label_language_id'	     				=> '',
			));
		}

		return json_out($data, $showin,$exit);
	}
	function get_labels($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$const = array();


		$table = 'label_language_contract';

		if($in['label_language_id'])
		{
			$filter = "WHERE label_language_id='".$in['label_language_id']."'";
			$id = $in['label_language_id'];
		}elseif($in['label_custom_language_id'])
		{
			$filter = "WHERE lang_code='".$in['label_custom_language_id']."'";
			$id = $in['label_custom_language_id'];
		}

		$db->query("SELECT * FROM $table $filter");

		$data['labels']=array(
			'contract'						=> $db->f('contract'),
			'billing_address'				=> $db->f('billing_address'),
			'contract_note'					=> $db->f('contract_note'),
			'company_name'					=> $db->f('company_name'),
			'comp_reg_number'				=> $db->f('comp_reg_number'),
			'buyer_delivery_address' 		=> $db->f('buyer_delivery_address'),
			'buyer_delivery_zip' 			=> $db->f('buyer_delivery_zip'),
			'buyer_delivery_city' 			=> $db->f('buyer_delivery_city'),
			'buyer_delivery_country' 		=> $db->f('buyer_delivery_country'),
			'buyer_main_address' 			=> $db->f('buyer_main_address'),
			'buyer_main_zip' 				=> $db->f('buyer_main_zip'),
			'buyer_main_city' 				=> $db->f('buyer_main_city'),
			'buyer_main_country' 			=> $db->f('buyer_main_country'),
			'bank_details' 					=> $db->f('bank_details'),
			'item'							=> $db->f('item'),
			'subtotal' 						=> $db->f('subtotal'),
			'unit_price' 					=> $db->f('unit_price'),
			'vat' 							=> $db->f('vat'),
			'amount_due' 					=> $db->f('amount_due'),
			'notes' 						=> $db->f('notes'),
			'bic_code'						=> $db->f('bic_code'),
			'phone' 						=> $db->f('phone'),
			'email' 						=> $db->f('email'),
			'our_ref' 						=> $db->f('our_ref'),
			'vat_number' 					=> $db->f('vat_number'),
			'package' 						=> $db->f('package'),
			'article_code' 					=> $db->f('article_code'),
			'chapter_total' 				=> $db->f('chapter_total'),
			'subtotal_2' 					=> $db->f('subtotal_2'),
			'reference'	  					=> $db->f('reference'),
			'date' 							=> $db->f('date'),
			'customer'					  	=> $db->f('customer'),
			'quantity'						=> $db->f('quantity'),
			'amount'						=> $db->f('amount'),
			'discount'			 			=> $db->f('discount'),
			'payments'			 			=> $db->f('payments'),
			'grand_total'			 		=> $db->f('grand_total'),
			'bank_name'			 			=> $db->f('bank_name'),
			'iban'			 				=> $db->f('iban'),
			'fax'			 				=> $db->f('fax'),
			'url'			 				=> $db->f('url'),
			'your_ref'			 			=> $db->f('your_ref'),
			'sale_unit'			 			=> $db->f('sale_unit'),
			'general_conditions'			=> $db->f('general_conditions'),
			'page'			 				=> $db->f('page'),
			'valid_until'			 		=> $db->f('valid_until'),
			'stamp_signature'			 	=> $db->f('stamp_signature'),
			'download_webl'			 		=> $db->f('download_webl'),
			'name_webl'			 			=> $db->f('name_webl'),
			'addCommentLabel_webl'			=> $db->f('addCommentLabel_webl'),
			'quote_webl'			 		=> $db->f('quote_webl'),
			'accept_webl'			 		=> $db->f('accept_webl'),
			'reject_webl'			 		=> $db->f('reject_webl'),
			'bankDetails_webl'			 	=> $db->f('bankDetails_webl'),
			'cancel_webl'			 		=> $db->f('cancel_webl'),
			'bicCode_webl'			 		=> $db->f('bicCode_webl'),
			'bank_webl'			 			=> $db->f('bank_webl'),
			'pdfPrint_webl'			 		=> $db->f('pdfPrint_webl'),
			'email_webl'			 		=> $db->f('email_webl'),
			'submit_webl'			 		=> $db->f('submit_webl'),
			'date_webl'			 			=> $db->f('date_webl'),
			'requestNewVersion_webl'		=> $db->f('requestNewVersion_webl'),
			'status_webl'			 		=> $db->f('status_webl'),
			'payWithIcepay_webl'			=> $db->f('payWithIcepay_webl'),
			'regularInvoiceProForma_webl'	=> $db->f('regularInvoiceProForma_webl'),
			'ibanCode_webl'			 		=> $db->f('ibanCode_webl'),
			'noData_webl'			 		=> $db->f('noData_webl'),
			'do'							=> 'contract-settings-contract-label_update',
			'xget'							=> 'labels',
			'label_language_id'				=> $id
		);

		return json_out($data, $showin,$exit);
	}

	function get_emailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Contract Nr");
		$array_values['CONTRACT_SERIAL_NUMBER']=gm("Contract");
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['START_DATE']=gm("Start Date");
		$array_values['END_DATE']=gm("End Date");
		$array_values['CONTACT_FIRST NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST NAME']=gm("Contact last name");
		$array_values['SUBTOTAL']=gm("Subtotal");
		$array_values['DISCOUNT']=gm("Discount Percent");
		$array_values['DISCOUNT_VALUE']=gm("Discount Value");
		$array_values['SUBJECT']=gm("Subject");
		$array_values['WEB_LINK']=gm("Web Link");
		$array_values['WEB_LINK_2']=gm("Short Weblink");
		$array_values['DROP_BOX']=gm("Dropbox files");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['LOGO']=gm("Logo");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['AMOUNT_DUE']=gm("Amount Due");
		$array_values['VAT_VALUE']=gm("Vat Value");
	
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id'     => $key,
				'name'	   => $value,
			));
		}

		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);

		$is_normal = true;
		// $in['lang_code'] = $code;
		$name = 'contract_invmess';
		$text = '-----------------------------------
				Contract Invoice Summary
				-----------------------------------
				Contract ID: C-026274 [A]
				Invoice ID: [!SERIAL_NUMBER!]

				Date: [!INVOICE_DATE!]
				Contract Start Date: 01/01/1970
				Contract End Date: 

				Customer: "Cosi" Vermeulen Katrien
				Discount (0.0000%):  &euro; 0,00
				Payments: [!PAYMENTS!]
				Amount Due:  &euro; 125,00

				The detailed invoice is attached as a PDF.

				Thank you!
				-----------------------------------';

		$subject = 'Contract #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
		$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'contract-settings-contract-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'contract-settings-contract-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code
			);
		}

		return json_out($data, $showin,$exit);
	}
	function get_defaultEmail($in,$showin=true,$exit=true){
			$db = new sqldb();
			$data = array( 'email_default'=>array());

		$email = $db->query("SELECT * FROM default_data WHERE type='quote_email' ");
		$mail_type1 = $db->field("SELECT value FROM default_data WHERE  type = 'quote_email_type' ");

			$data['email_default'] = array(
				'default_name'	=> $email->gf('default_name'),
				'email_value'	=> $email->gf('value'),
				'mail_type_1'	=> $mail_type1,
				'do'			=> 'quote-settings-quote-update_default_email',
				'xget'			=> 'defaultEmail',
			);


			return json_out($data, $showin,$exit);
	}
	function get_weblink($in,$showin=true,$exit=true){
			$db = new sqldb();
			$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CONTRACT_WEB_LINK' ");
			$allow = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_CONTRACT_COMMENTS' ");
			$allow2 = $db->field("SELECT value FROM settings WHERE constant_name='WEB_INCLUDE_PDF_CONTRACT' ");
			$allow3 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_WEB_PAYMENT_CONTRACT' ");
			$allow4 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_CONTRACT_ACCEPT' ");
			$allow5 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_CONTRACT_NEWV' ");
			$allow6 = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_CONTRACT_REJECT' ");
			$data['weblink'] = array(
				'contract_link'		=>$use == '1' ? true : false,
				'contract_pdf'			=>$allow2 == '1' ? true : false,
				'contract_comment'		=>$allow == '1' ? true : false,
				'contract_accept'		=>$allow4 == '1' ? true : false,
				'contract_new'			=>$allow5 == '1' ? true : false,
				'contract_reject'		=>$allow6 == '1' ? true : false,
				'old_contract_link'		=>$use == '1' ? true : false,
				'old_contract_pdf'			=>$allow2 == '1' ? true : false,
				'old_contract_comment'		=>$allow == '1' ? true : false,
				'old_contract_accept'		=>$allow4 == '1' ? true : false,
				'old_contract_new'			=>$allow5 == '1' ? true : false,
				'old_contract_reject'		=>$allow6 == '1' ? true : false,
				'contract_link_sign'		=>$in['contract_link_change'] ? true : false,
				'contract_pdf_sign'		=>$in['contract_pdf_change'] ? true : false,
				'contract_comment_sign'	=>$in['contract_comment_change'] ? true : false,
				'contract_accept_sign'		=>$in['contract_accept_change'] ? true : false,
				'contract_new_sign'		=>$in['contract_new_change'] ? true : false,
				'contract_reject_sign'		=>$in['contract_reject_change'] ? true : false,
				'do'					=> 'contract-settings-contract-web_link',
				'xget'					=> 'weblink',
			);

			return json_out($data, $showin,$exit);
	}

	function get_categorisation($in,$showin=true,$exit=true){
			$db = new sqldb();
			$i = 0;
			$j = 0;
			$data = array('bigloop'=>array());
			$array = array('source','type','lost_reason');

			foreach ($array as $key) {
				$data['bigloop'][$j]=array(
					'name'			=> gm(ucfirst(str_replace('_', ' ', $key))),
					'table' 		=> $key,
					'count'			=> $i,
					'small'			=> array(),
				);
				$i=1;
				$db->query("SELECT * FROM tblquote_".$key." ORDER BY sort_order ");
				while ($db->next()) {
					$data['bigloop'][$j]['small'][]=array(
						'field_id'	=> $db->f('id'),
						'label'		=> gm(ucfirst($key)).' '.$i,
						'value'		=> $db->f('name'),
					);
					$i++;
				}
				$j++;
			}
			return json_out($data, $showin,$exit);
	}
	function get_articlefield($in,$showin=true,$exit=true){
			$db = new sqldb();
			$text = $db->field("SELECT long_value FROM settings WHERE constant_name='CONTRACT_FIELD_LABEL'");
			$data['articlefields'] = array(
				'text' 	=>	$text,
				'ADV_PRODUCT' => defined('ADV_PRODUCT') && ADV_PRODUCT == 1 ? true : false,
				'do'	=> 'contract-settings-contract-saveArticleField',
				'xget'	=> 'articlefield',

			);
			return json_out($data, $showin,$exit);
	}
	function get_packingsettings($in,$showin=true,$exit=true){
			$db = new sqldb();
			$allowpack = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_PACKING'");
			$allowsale = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_SALE_UNIT'");
			$data['packing'] = array(
				'allowpack' => $allowpack == '1' ? true : false,
				'allowsale' => $allowsale == '1' ? true : false,

			);
			return json_out($data, $showin,$exit);
	}
	function get_attachments($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('atachment'=>array());
			$f = $db->query("SELECT * FROM attached_files WHERE type='5' ");
			while ($f->next()) {
				array_push($data['atachment'], array(
					'file'		=> $f->f('name'),
					'checked'	=> $f->f('default') == 1 ? true : false,
					'file_id'	=> $f->f('file_id'),
					'path'		=> INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/contracts/'.$f->f('name'),
					// 'checkeddrop'		=> $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_DROP_BOX' ") == '1' ? true : false,
					));
			}

			return json_out($data, $showin,$exit);
	}
	function get_listsettings($in,$showin=true,$exit=true){
		$db = new sqldb();

		$act = $db->field("SELECT value FROM settings WHERE constant_name='SHORT_QUOTES_BY_SERIAL' ");
		$data['listset'] = array(
			'setlist' => $act == 1 ? true : false,
		);


		return json_out($data, $showin,$exit);
	}
	function get_quoteTerm($in,$showin=true,$exit=true){
		$db = new sqldb();
		
		$data = array('quoteTerm'=>array(
			'quote_term'  				=> $db->field("SELECT value FROM settings WHERE constant_name = 'QUOTE_TERM' "),
			'choose_quote_term_type' 	=> $db->field("SELECT value FROM settings WHERE constant_name = 'QUOTE_TERM_TYPE' "),
			'do'						=> 'quote-settings-quote-saveterm',
			'xget'						=> 'quoteTerm',
		));

		return json_out($data, $showin,$exit);
	}
	$result = array(
		'PDFlayout'			=> get_PDFlayout($in,true,false),
		'CustomizePDF'		=> get_CustomizePDF($in,true,false),
		'convention' 		=> get_Convention($in,true,false),
	);
json_out($result);
?>





