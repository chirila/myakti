<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	$result=array('list'=>array());
	$l_r = ROW_PER_PAGE;
	$result['lr']=$l_r;
	$order_by_array = array('serial_number');

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$filter = " contracts.active='2' ";
	$filter_c=' ';

	if(!empty($in['search'])){
		$filter .= " AND contracts.serial_number LIKE '%".$in['search']."%' ";
	}

	if($in['order_by']){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
				$order = " DESC ";
			}

		       $order_by =" ORDER BY ".$in['order_by']." ".$order;
		       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
		}
	}

	$arguments = $arguments.$arguments_s;

	$max_rows =  $db->field("SELECT count(contracts.contract_id) FROM contracts 
		INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
		WHERE ".$filter );
	$result['max_rows'] = $max_rows;
	$db->query("SELECT contracts.*,contracts_version.*
	            FROM contracts
	            INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
	            WHERE ".$filter.$order_by." LIMIT ".$offset*$l_r.",".$l_r );
	$j=0;
	while($db->move_next()){

		$list=array(
			'name'						=> $db->f('serial_number'),
			'id'					=> $db->f('contract_id'),
			'delete_link' 		 		=> array('do'=>'contract-contracts_templates-contract-delete_template', 'contract_id'=>$db->f('contract_id')),
		);
		array_push($result['list'],$list);
	}
	if($in['index']){
		$result['index'] = $in['index'];
	}

return json_out($result);