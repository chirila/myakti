<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class ContractEdit extends Controller{

	private $contract;
	private $contract_id;
	private $discount;
	private $grand_total;
	private $total_vat;
	private $grand_total_vat;
	private $currency_type;
	private $currency_rate;
	private $default_total;
	private $allowPacking;
	private $allowSaleUnit;
	private $is_discount;
	private $is_vat;
	private $version_id;

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);	
		if(isset($in['template_id']) && is_numeric($in['template_id']) ){
			$this->in['contract_id']=$in['template_id'];
			unset($in['version_id']);
			unset($this->in['version_id']);
		}
		$this->contract_id=$in['contract_id'];
		$this->version_id=$in['version_id'];	
	}

	public function getContract(){
		if($this->in['contract_id'] == 'tmp'){ //add
			$this->getAddContract();
		}elseif($this->in['contract_id']){ # edit
			if($this->in['xview']){
				$this->getViewContract();
			}else{
				$this->getEditContract();
			}
		}else{
			$this->output(array('redirect'=>'contracts'));
		}
	}

	public function getAddContract(){
		$user_lang = $_SESSION['l'];
		if ($user_lang=='nl')
		{
			$user_lang='du';
		}
		switch ($user_lang) {
			case 'en':
				$user_lang_id='1';
				break;
			case 'du':
				$user_lang_id='3';
				break;
			case 'fr':
				$user_lang_id='2';
				break;
			case 'de':
				$user_lang_id='4';
				break;
		}
		$in=$this->in;

		$contract_name='';
		if(!$in['budget_type']){
			$in['budget_type']=1;
		}
		$billable_dd = build_contract_budget_type_list($in['budget_type']);	
		if(!$in['is_contact']){
			$in['data_val'] = 1;
		}

		if(!$in['end_date']){
			$in['end_date']=strtotime("+3 month", time());
		}

		if(!$in['email_language']){
			$in['email_language'] = $user_lang_id;
		}

		if($in['contract_verison_date']){
			$in['contract_version_date'] = strtotime($in['contract_version_date'])*1000;
		}else{
			$in['contract_version_date'] = time()*1000;
		}

		$output=array();
		if($in['deal_id']){
			$output['deal_id']=$in['deal_id'];
			$deal_data=$this->db->query("SELECT * FROM tblopportunity WHERE opportunity_id='".$in['deal_id']."' ");
			if($deal_data->next()){
				if(!$in['buyer_id'] && $deal_data->f('buyer_id')){
					$in['buyer_id']=$deal_data->f('buyer_id');
					$output['add_customer']=false;
				}
				if(!$in['contact_id'] && $deal_data->f('contact_id')){
					$in['contact_id']=$deal_data->f('contact_id');
				}
				$in['subject']=$deal_data->f('subject');
				$output['subject']=$deal_data->f('subject');
				$in['main_address_id']=$deal_data->f('main_address_id');
				$in['delivery_address_id']=$deal_data->f('same_address');
				$in['identity_id']=$deal_data->f('identity_id');
				$in['source_id']=$deal_data->f('source_id');
				$in['type_id']=$deal_data->f('type_id');
				$in['segment_id']=$deal_data->f('segment_id');
			}		
		}

			$output['name']			       				= $in['name'];
			$output['your_ref']		       			= $in['your_ref'];
			$output['customer']	           				= $in['customer'];
			$output['customer_id']	       				= $in['customer_id'];
			$output['do_request'] 						= 'contract-contract-contract-updateCustomerData';
			$output['CURRENCY_TYPE_LIST']        		= build_currency_list(ACCOUNT_CURRENCY_TYPE);
			$output['currency_type']						= ACCOUNT_CURRENCY_TYPE;
			$output['default_currency']					= ACCOUNT_CURRENCY_TYPE;
			$output['account_currency']     				= get_currency(ACCOUNT_CURRENCY_TYPE);
			$output['country_dd']						= build_country_list(0);
			$output['country_id']						= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
			$output['main_country_id']					= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
			$output['start_date']						= $in['start_date'] ? (is_numeric($in['start_date']) ? $in['start_date']*1000 : strtotime($in['start_date'])*1000)  : time()*1000;
			$output['end_date']							= $in['end_date'] ? (is_numeric($in['end_date']) ? $in['end_date']*1000 : strtotime($in['end_date'])*1000) : strtotime("+3 month", time())*1000 ;
			$output['reminder_date']						= $in['reminder_date'] ? (is_numeric($in['reminder_date']) ? $in['reminder_date']*1000 : strtotime($in['reminder_date'])*1000) : '';
			//$output['serial_number']						= $in['serial_number'] ? $in['serial_number'] : generate_contract_number(DATABASE_NAME);
			$output['ref']								= 'var ref;';
			$output['ref_contract_number']				= $in['serial_number'] ? $in['serial_number'] : generate_contract_number(DATABASE_NAME);
			$output['last_used']							= get_last_contract_number();
			$output['quote_id']							= $in['quote_id'];
			$output['contract_id']						= $in['contract_id'];
			$output['type_list']	                        = build_contract_list();
			$output['hide_add']                          = false;
			$output['view_contract_type_2']              = $in['type']? true : false;
			$output['budget_type_dd']       	       	    = $billable_dd;
			$output['t_ct_hour']            				= $in['t_ct_hour'];
			$output['view_buget_options']                = $in['budget_type']   == 1 ? false : true;
			$output['budget_type']						= $in['budget_type'];
			$output['send_email_alerts_checked'] 		= $in['send_email_alerts'] == 1 ? true : false;
			$output['alert_percent']           	 		= $in['alert_percent'];
			$output['disabled_alert_percent']    		= $in['send_email_alerts'] == 1 ? false : true;
			$output['close_c']							= 'none';
			$output['reminder_type_dd']       	       	= build_reminder_type_list($in['reminder_type']);
			$output['reminder_type']						= $in['reminder_type'] ? $in['reminder_type'] : '0';
			$output['invoicing_type_dd']       	        = build_invoicing_type_list($in['invoicing_type']);
			$output['invoicing_type']					= $in['invoicing_type'] ? $in['invoicing_type'] : '0';
			$output['view_reminder_options']             = $in['reminder_type'] != 3 ? false : true;
			$output['hide_add_expand']					= false;
			$output['hide_collaps']						= false;
			$output['main_language_dd'] 			        = isset($in['email_language']) ? build_language_admin_dd($in['email_language']) : build_language_admin_dd(0);
		    $output['edit_mode']						    = false;
		    $output['is_opened']							= false;
		    $output['is_closed']							= false;
		    $output['APPLY_DISCOUNT_LIST']      			= build_apply_discount_list(0);
			$output['apply_discount']					= '0';
			$output['language_dd'] 						= build_language_dd_new(0);
			$output['email_language']					= $user_lang_id;
			
			//$output['contacts']							= $in['contact_id'] ? array() : $this->get_contacts($in);	
			
			//$output['add_customer']						= !$in['buyer_id'] && !$in['contact_id'] ? true : false;
			$output['add_customer']						= false;
			$output['seller_name']               		= ACCOUNT_COMPANY;
			$output['contract_version_date']			= $in['contract_version_date'];
			$output['discount' ]                 		= display_number($in['discount']);
			$output['delivery_address'] 	    			= $in['delivery_address'];
	  		$output['delivery_address_txt']	     		= nl2br($in['delivery_address']);
	  		$output['delivery_address_id']				= $in['delivery_address_id'];
	  		$output['main_address_id']					= $in['main_address_id'];
			$output['seller_d_address']	    			= ACCOUNT_DELIVERY_ADDRESS;
			$output['seller_d_zip']   					= ACCOUNT_DELIVERY_ZIP;
			$output['seller_d_city']   					= ACCOUNT_DELIVERY_CITY;
			//Seller Billing Details
			$output['seller_b_address']    				= ACCOUNT_BILLING_ADDRESS;
			$output['seller_b_zip']   		 			= ACCOUNT_BILLING_ZIP;
			$output['seller_b_city']   					= ACCOUNT_BILLING_CITY;
			$output['seller_bwt_nr']   					= ACCOUNT_VAT_NUMBER;
			$output['vat']								= $in['vat'] ? $in['vat'] : display_number(0);
			$output['currency_rate']						= $in['currency_rate'];
			$output['type']								= '0';
			$output['free_field'] 						= '';
			$output['free_field_txt'] 					= '';
			$output['VAT_TOTAL']							= display_number(0);
			$output['GRAND_TOTAL']						= display_number(0);
			$output['TOTAL_DEFAULT_CURRENCY']			= display_number(0);
			$output['TOTAL_WITHOUT']						= display_number(0);
			$output['currency_txt']		        		= currency::get_currency(ACCOUNT_CURRENCY_TYPE);
			$output['hide_currency2'] 					= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
			$output['hide_currency1'] 					= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
			$output['show_QC']							= true;
			$output['show_UP']							= true;
			$output['show_PS']							= true;
			$output['installation_id']				= $in['installation_id'];
			$output['old_installation_id']			= $in['installation_id'];
			$output['pdf_layout']					= $in['pdf_layout'];
			$output['show_img_q']					= $in['show_img_q'];
			$output['authors']						= $this->get_author($in);
			$output['accmanager']					= $this->get_accmanager($in);
			$output['author_id']						= $in['author_id'] ? $in['author_id'] : $_SESSION['u_id'];
			if(in_array(17,perm::$allow_apps)){
	          $output['has_installation']= true;
	        }

		$apply_discount = '0';

		if($in['quote_id'] && $in['version_id']){
			$output['quote_id']=$in['quote_id'];
			$quote = $this->db->query("SELECT tblquote.*,tblquote_version.active,tblquote_version.version_code,tblquote_version.version_id,tblquote_version.active AS version_active,
								tblquote_version.version_date, tblquote_version.discount,tblquote_version.discount_line_gen,tblquote_version.apply_discount,tblquote_version.sent,tblquote_version.version_own_reference,tblquote_version.version_subject,tblquote_version.version_status_customer
	            FROM tblquote
	            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
	            WHERE tblquote.id='".$in['quote_id']."' AND tblquote_version.version_id = '".$in['version_id']."' AND tblquote_version.version_status_customer='2' ");
			if(!$quote->next()){
				$this->output(array('redirect'=>'contracts'));
				return true;
			}
			if(!$in['buyer_id']){
				$in['buyer_id']=$quote->f('buyer_id');
				$in['main_address_id']=$quote->f('main_address_id');
				$in['delivery_address_id']=$quote->f('delivery_address_id');
			}
			if(!$in['contact_id']){
				$in['contact_id']=$quote->f('contact_id');
			}
			if(!$in['subject']){
				$in['subject']=$quote->f('version_subject');
			}
			if(!$in['your_ref']){
				$in['your_ref']=$quote->f('version_own_reference');
			}
			if(!$in['discount']){
				$this->discount=$quote->f('discount');
			}
			$this->is_discount=$quote->gf('apply_discount');

			if($quote->f('apply_discount')==0){
				$discount_line=false;
				$discount_global=false;
			}elseif($quote->f('apply_discount')==1){
				$discount_line=true;
				$discount_global=false;
			}elseif($quote->f('apply_discount')==2){
				$discount_line=false;
				$discount_global=true;
			}elseif($quote->f('apply_discount')==3){
				$discount_line=true;
				$discount_global=true;
			}

			$output['apply_discount']      			= $quote->gf('apply_discount');
	  		$output['apply_discount_line']      	= $discount_line;
	  		$in['apply_discount_global']      	= $discount_global;
			$output['apply_discount_global']      	= $discount_global;
			$in['discount']							= $quote->gf('discount');
			$output['discount']						= display_number($quote->gf('discount'));
			$output['discount_line_gen']			= display_number($quote->gf('discount_line_gen'));
			if($quote->gf('apply_discount') == 1 || $quote->gf('apply_discount') == 3){
				$output['VIEW_DISCOUNT']= true;
			}else{
				$output['VIEW_DISCOUNT']= false;
			}

			$cols = 3;
			$hcols = 5;
			if($quote->gf('apply_discount') == 0 || $quote->gf('apply_discount') == 2){
				$cols++;
				$hcols++;
			}
			if($quote->f('use_package')){
				//
			}else{
				$output['ALLOW_QUOTE_PACKING']=0;
				$cols++;
				$hcols++;
			}
			$output['colum'] = $cols;
			$in['colum'] = $cols;
			$output['hcolum'] = $hcols;

		}

        $cols = 3;
        $hcols = 5;
        if($apply_discount == '0' || $apply_discount == '2'){
            $cols++;
            $hcols++;
        }

        if(defined('ALLOW_QUOTE_PACKING') && ALLOW_QUOTE_PACKING=='1'){
            $output['ALLOW_QUOTE_PACKING']=1;
            $this->allowPacking = true;
        }else{
            $output['ALLOW_QUOTE_PACKING']=0;
            $this->allowPacking = false;
            $cols++;
            $hcols++;
        }
        if(defined('SHOW_ARTICLES_PDF') && SHOW_ARTICLES_PDF == '1'){
            $cols--;
            $hcols--;
        }
        if(defined('QUOTE_APPLY_DISCOUNT') && QUOTE_APPLY_DISCOUNT == '1'){
            $cols--;
            $hcols--;
        }


		$output['sameAddress']						= true;
			$output['identity_id']						= get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);
			$output['main_comp_info'] 					= $this->getQuoteIdentity($output['identity_id']); # get identity	
		$output['cc']								= $this->get_cc($in);
		$output['contacts']							= $this->get_contacts($in);


		if($in['buyer_id']){
			$output['buyer_id'] = $in['buyer_id'];
		    $buyer_info = $this->db->query("SELECT customer_legal_type.name as l_name, customers.c_email, customers.type, customers.acc_manager_name, customers.user_id,
		                                   customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.line_discount, 
		                                   customers.apply_fix_disc, customers.apply_line_disc, customers.name,customers.firstname, customers.identity_id, customers.vat_regime_id,customers.internal_language
		                                   FROM customers
									  LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
									  WHERE customers.customer_id = '".$in['buyer_id']."' ");
		    
	      	$acc_manager_id = explode(',',$buyer_info->f('user_id'));
	      	$acc_manager_name = explode(',',$buyer_info->f('acc_manager_name'));
		  	$output['acc_manager_id'] = $acc_manager_id[0];
		  	$output['acc_manager_name']	= $acc_manager_name[0];
	      
			$output['buyer_name'] = $buyer_info->f('type')==1 ? stripslashes($buyer_info->f('firstname')).' '.stripslashes($buyer_info->f('name')) : ($buyer_info->f('l_name') ? stripslashes($buyer_info->f('name')).' '.$buyer_info->f('l_name') : stripslashes($buyer_info->f('name')) );
			$text = gm('Customer Name');
			
			$output['buyer_reference'] = $buyer_info->f('our_reference');
			$q_ref = $buyer_info->f('our_reference');
			$in['vat_regime_id'] = $buyer_info->f('vat_regime_id');

			if(!$in['vat_regime_id']){
				$vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

			    if(empty($vat_regime_id)){
			      $vat_regime_id = 1;
			    }
				$in['vat_regime_id'] = $vat_regime_id;
			}

			$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");

			$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$in['vat_regime_id']."' ");
			
			$output['REF_SERIAL_NUMBER'] = $q_ref.$output['serial_number'];
	        $output['BUYER_EMAIL'] 				= $buyer_info->f('c_email');
	        $output['no_buyer_vat']	 			= $buyer_info->f("no_vat");
	        if( $output['no_buyer_vat']	){
	        	$output['show_vat']						= '0';
	        	$output['remove_vat']					= '1';
	        }
	        $output['currency_type'] 			= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;

	        if($output['currency_type'] != ACCOUNT_CURRENCY_TYPE){
	        	$params = array(
	        		'currency' 					=> currency::get_currency($output['currency_type'],'code'),
					'acc' 						=> ACCOUNT_NUMBER_FORMAT,
					'into' 						=> currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code')
				);
	        	$output['currency_rate'] 		= $this->get_currency_convertor($params);
	        }

	        $output['vat'] = $in['vat'] ? display_number($in['vat']) : display_number(0);
	        $output['vat_regim_dd']					= build_vat_regime_dd();
			$output['vat_regime_id']				= $in['vat_regime_id'] ? $in['vat_regime_id'] : '';
	        $output['email_language']	 		= $buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : $user_lang_id;

	        $buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
	        
	        $free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
			$b_country_id=$buyer_details->f('country_id');
			$b_city=$buyer_details->f('city');
			$b_zip=$buyer_details->f('zip');
			$b_address=$buyer_details->f('address');
	        $main_address_id= $buyer_details->f('address_id');
	        if($in['main_address_id'] && $in['main_address_id']!=$main_address_id){
				$main_address_id=$in['main_address_id'];
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
				$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
				$b_country_id=$buyer_addr->f('country_id');
				$b_city=$buyer_addr->f('city');
				$b_zip=$buyer_addr->f('zip');
				$b_address=$buyer_addr->f('address');
			}
			if($in['delivery_address_id']){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
				$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
			}

            if( !return_value($in['discount'])&& $buyer_info->f('apply_fix_disc')){
                $output['discount'] = display_number($buyer_info->f("fixed_discount"));
            }else{
                $output['discount']= display_number(0);
            }

            if($buyer_info->f('apply_line_disc')==1){
                $output['discount_line_gen']= display_number($buyer_info->f('line_discount'));
            } else {
                $output['discount_line_gen']= display_number(0);
            }

            $discount_line=false;
            $discount_global=false;
            if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
                $output['apply_discount']= 3;
                $discount_line=true;
                $discount_global=true;
                $cols--;
                $hcols--;
            }else{
                if($buyer_info->f('apply_fix_disc')){
                    $output['apply_discount']= 2;
                    $discount_line=false;
                    $discount_global=true;
                }
                if($buyer_info->f('apply_line_disc')){
                    $output['apply_discount'] = 1;
                    $discount_line=true;
                    $cols--;
                    $hcols--;
                }
            }

            $output['apply_discount_line']      	= $discount_line;
            $output['apply_discount_global']      	= $discount_global;
            $output['colum'] = $cols;
            $output['hcolum'] = $hcols;

            if($output['apply_discount'] == 1 || $output['apply_discount'] == 3){
                $output['VIEW_DISCOUNT']= true;
            }else{
                $output['VIEW_DISCOUNT']= false;
            }

	        $output['buyer_country_id']         = $b_country_id;
			$output['BUYER_COUNTRY_NAME']       = get_country_name($b_country_id);
			$output['buyer_state_id']           = $buyer_details->f('state_id');
			$output['BUYER_STATE_NAME']         = get_state_name($buyer_details->f('state_id'));
			$output['buyer_city']               = $b_city;
			$output['buyer_zip']                = $b_zip;
			$output['buyer_address']            = $b_address;
			$output['free_field'] 				= $in['delivery_address'] ? $in['delivery_address'] : $free_field;
			$output['free_field_txt']           = $in['delivery_address'] ? nl2br($in['delivery_address']) : nl2br($output['free_field']);
	        $output['field']                    = 'customer_id';
	        $output['addresses']				= $this->get_addresses($in);
	        $output['identity_id']				= get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);
	        $output['main_comp_info'] 			= $this->getQuoteIdentity($output['identity_id']); # get identity
	        $output['multiple_identity_dd']				= build_identity_dd($output['identity_id']);
			//$output['do_request'] 				= 'contract-contract-contract-add';
			$output['main_address_id']			= $main_address_id;
			//$output['remove_vat'] 				= $buyer_info->f('no_vat');
			$output['show_vat']						= $in['remove_vat']=='1' ? '0' : '1';
	        $output['remove_vat']					= $in['remove_vat'];
			$output['installations']			= $this->get_installations($in);
			
			if($in['contact_id']){
				$output['contact_id'] = $in['contact_id'];
				$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
			}
			$output['deals']				= $this->get_deals($in);

		}else if($in['contact_id']){
			$buyer_info = $this->db->query("SELECT phone, cell, email, CONCAT_WS(' ',firstname, lastname) as name FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$buyer_details = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='{$in['contact_id']}' AND is_primary='1' ");
			$text =gm('Name');
			$q_ref = ACCOUNT_QUOTE_DEL;
			$output['REF_SERIAL_NUMBER'] 		= $q_ref.$output['serial_number'];
			$output['contact_id'] 				= $in['contact_id'];
			$output['BUYER_EMAIL'] 				= $buyer_info->f('email');
			$output['CONTACT_NAME'] 			= '';
			$output['buyer_name'] 				= $buyer_info->f('name');
			$output['field']                    = 'contact_id';
			//$output['do_request'] 				= 'contract-contract-contract-add';
			$output['buyer_city']               = $buyer_details->f('city');
			$output['buyer_zip']                = $buyer_details->f('zip');
			$output['buyer_address']            = $buyer_details->f('address');
			$output['free_field'] 				= $in['delivery_address'] ? $in['delivery_address'] : $buyer_details->f('address')."\n".$buyer_details->f('zip').' '.$buyer_details->f('city')."\n".get_country_name($buyer_details->f('country_id'));
			$output['free_field_txt']           = $in['delivery_address'] ? nl2br($in['delivery_address']) : nl2br($output['free_field']);
	        $output['addresses']				= $this->get_addresses($in);
		}
		$output['NAME_TEXT']					= $text;

		$to_edit=false;
		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_11' ");
		switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
				$to_edit=true;
				break;
		}
		$in['lang_id'] = $output['email_language'];
		/*$notes = $this->get_notes($in);*/
		
		$notes = $this->get_notes($in);
		
		if($in['quote_group']) {
            $output['quote_group'] = $in['quote_group'];
            foreach ($output['quote_group'] as $key => $value) {
                $output['quote_group'][$key]['show_IC'] = $output['quote_group'][$key]['show_IC'] ? true : false;
                $output['quote_group'][$key]['show_QC'] = $output['quote_group'][$key]['show_QC'] ? true : false;
                $output['quote_group'][$key]['show_UP'] = $output['quote_group'][$key]['show_UP'] ? true : false;
                $output['quote_group'][$key]['show_PS'] = $output['quote_group'][$key]['show_PS'] ? true : false;
                $output['quote_group'][$key]['show_d'] = $output['quote_group'][$key]['show_d'] ? true : false;
                $output['quote_group'][$key]['show_AC'] = $output['quote_group'][$key]['show_AC'] ? true : false;
                $output['quote_group'][$key]['show_chapter_total'] = $output['quote_group'][$key]['show_chapter_total'] ? true : false;
                $output['quote_group'][$key]['show_block_total'] = $output['quote_group'][$key]['show_block_total'] ? true : false;
                $output['quote_group'][$key]['show_price_vat'] = $output['quote_group'][$key]['show_price_vat'] ? true : false;
            }
        } else {
            $uniqueQuoteGroupId=unique_id();
            $output['quote_group'] = array();
            $tmp_quote_group=array(
                'active'			=> '1',
                'chapter_total'		=> display_number(0),
                'group_id'			=> $uniqueQuoteGroupId,
                'quote_group_id'	=> $uniqueQuoteGroupId,
                'quote_group_pb'	=> "0",
                'show_chapter_pb'	=> "hide",
                'show_chapter_subtotal'=> "hide",
                'title'				=> "",
                'table'				=> array(
                    array(
                        'USE_PACKAGE'=> $output['ALLOW_QUOTE_PACKING']==1 ? true : false,
                        'USE_SALE_UNIT'=>defined('ALLOW_QUOTE_SALE_UNIT') && ALLOW_QUOTE_SALE_UNIT=='1' ? true : false,
                        'quote_line'=>array(),
                        'q_table'	=> true
                    )
                )
            );
//            $default_pdf_opts=$this->get_defaultPdfOptions();
//            foreach($default_pdf_opts['default_pdf_options'] as $key=>$value){
//                //console::log($key,$value);
//                $tmp_quote_group[$key]=$value;
//            }
            $output['quote_group'][]=$tmp_quote_group;
        }
		//commented since AKTI-4780
//		}else{
//			$output['quote_group'] = array();
//			if($in['quote_id'] && $in['version_id']){
//				$lines = $this->getContractLines($in);
//
//				$output['sort_order']= $lines['sort_order'];
//				$in['quote_group'] = $lines['quote_group'];
//				$output['quote_group'] = $lines['quote_group'];
//				$output['vat_lines'] = $lines['vat_lines'];
//
//			}
//		}

		$output['free_text_content']				= $notes['free_text_content'];
		$output['notes'] 							= $notes['notes'];
		$output['translate_loop']					= $notes['translate_loop'];
		$output['translate_loop_custom'] 			= $notes['translate_loop_custom'];
		$output['lang_cls'] 						= $notes['lang_cls'];
		$output['can_change']	=$to_edit;
		$output['generalvats']=build_vat_dd();
		$output['source_dd']= get_categorisation_source();
		$output['type_dd']= get_categorisation_type();
		$output['source_id']=$in['source_id'];
		$output['type_id']=$in['type_id'];
		$output['segment_dd']= get_categorisation_segment();
		$output['segment_id']=$in['segment_id'];
		$output['subject']=$in['subject'];
		$output['your_ref']=$in['your_ref'];

		$output['VAT_TOTAL']					= display_number($this->total_vat);
		$output['GRAND_TOTAL'] 					= display_number($this->grand_total_vat);
		$output['TOTAL_DEFAULT_CURRENCY']		= display_number($this->default_total);
		$output['TOTAL_WITHOUT']				= display_number($this->grand_total);
		$output['TOTAL_DISCOUNT']				= display_number(-$this->grand_total * $this->discount / 100);
        $output['articles_list']			    = $this->get_articles_list($in);

		$this->out = $output;
	}

	public function getEditContract(){
		$in=$this->in;
		$this->contract_id=$in['contract_id'];
		$next_function = 'contract-contract-contract-update';
		$output = array();

		if(isset($in['version_id']) && is_numeric($in['version_id'])){
			$filter = 'AND contracts_version.version_id = '.$in['version_id'];

		}else{
			$filter = 'AND contracts_version.active = 1';
		}

    		$contracts = $this->db->query("SELECT contracts.*,contracts_version.active,contracts_version.version_code,contracts_version.version_id,contracts_version.active AS version_active,
								contracts_version.version_date, contracts_version.discount,contracts_version.discount_line_gen,contracts_version.apply_discount
	            FROM contracts
	            INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
	            WHERE contracts.contract_id='".$in['contract_id']."' AND 1=1 ".$filter);

		if(!$contracts->next()){
			$this->output(array('redirect'=>'contracts'));
			return true;
		}
		if(!getItemPerm( array( 'module' => 'contracts' , 'item' => array( $contracts->f('author_id'),$contracts->f('created_by') ) ) ) ) {
			$this->output(array('redirect'=>'contracts'));
			return true;
		}

		$in['remove_vat'] 	= $contracts->f('remove_vat');
		$output['main_comp_info'] = $this->getQuoteIdentity($contracts->f('identity_id')); # get identity
		$output['identity_id'] = $contracts->f('identity_id');
		$output['multiple_identity_dd'] = build_identity_dd($contracts->f('identity_id'));
		$in['remove_vat']=$contracts->f('remove_vat');
		$in['show_grand'] = $contracts->f('show_grand');
		$output['source_dd']					= get_categorisation_source();
		$output['source_id']					= $contracts->f('source_id');
		$output['type_dd']						= get_categorisation_type();
		$output['type_id']						= $contracts->f('type_id');
		$output['segment_dd']					= get_categorisation_segment();
		$output['segment_id']					= $contracts->f('segment_id');

		$this->is_discount=$contracts->gf('apply_discount');
		$cols = 3;
		$hcols = 5;
		if($this->is_discount == 0 || $this->is_discount == 2){
			$cols++;
			$hcols++;
		}
		if($contracts->f('use_package')){
			$output['ALLOW_QUOTE_PACKING']=1;
			$this->allowPacking = true;
			// $use = true;
		}else{
			$output['ALLOW_QUOTE_PACKING']=0;
			$this->allowPacking = false;
			$cols++;
			$hcols++;
			// $use = false;
		}
		if($contracts->f('use_sale_unit')){
		    $output['ALLOW_QUOTE_SALE_UNIT']=1;
		    $this->allowSaleUnit = true;
		    // $use2 = true;
		}else{
			// $use2 = false;
			$output['ALLOW_QUOTE_SALE_UNIT']=0;
			$this->allowSaleUnit = false;
		}
		$output['colum'] = $cols;
		$in['colum'] = $cols;
		$output['hcolum'] = $hcols;
		$customer_id = $contracts->f('customer_id');
		//$vat = display_number($contracts->f('vat'));
		$to_edit=false;
		$perm_admin = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_11' ");
			switch (true) {
			case $_SESSION['group'] == 'admin':
			case $perm_admin:
				$to_edit=true;
				break;
		}

		$contact_name = $this->db->query("SELECT firstname,lastname,customer_contacts.contact_id FROM customer_contacts
		                                 INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
									WHERE customer_contacts.contact_id='".$contracts->f('contact_id')."' AND customer_contactsIds.customer_id='".$contracts->f('customer_id')."'");
		// $contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
		// 							WHERE contact_id='".$quote->f('contact_id')."' AND customer_id='".$quote->f('buyer_id')."'");
		$contact_name->next();
		// console::log($q_ref);
		if(in_array(17,perm::$allow_apps)){
          $output['has_installation']= true;
        }

        $in['buyer_id'] = $contracts->f('customer_id');

		$cat ='0';
		if($contracts->f('customer_id')){
			//get buyer details
			$buyer_details = $this->db->query("SELECT customers.customer_id as buyer_id,customers.name, customers.our_reference, customers.cat_id, customers.vat_regime_id,customer_addresses.*
				                    	FROM customers
				                    	LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.is_primary=1
		                            	WHERE customers.customer_id='".$contracts->f('customer_id')."'");
			$q_ref = $buyer_details->f('our_reference');
			$cat = $buyer_details->f('cat_id') ? $buyer_details->f('cat_id') : $cat;
			$in['remove_vat'] = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$buyer_details->f('vat_regime_id')."' ");
			$in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$buyer_details->f('vat_regime_id')."' ");
			// $output['main_address_id']			= $buyer_details->f('address_id');
			$output['installations']			= $this->get_installations($in);

		}

		$contract_date =$contracts->f('version_date') ? $contracts->f('version_date')*1000 : time()*1000;
		$c_time = $contracts->f('version_date') ? $contracts->f('version_date') : time();
		if($in['duplicate_contract_id']){
			$contract_date=  time()*1000;
			$c_time = time();
		}

		$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
			$name = stripslashes($name);
		// console::log($q_ref);

		$in['show_img_q'] = $contracts->f('show_img_q');

		if($contracts->f('pdf_layout') && $contracts->f('use_custom_template')==0){
			$link_end='&type='.$contracts->f('pdf_layout').'&logo='.$contracts->f('pdf_logo');
		}elseif($contracts->f('pdf_layout') && $contracts->f('use_custom_template')==1){
			$link_end = '&custom_type='.$contracts->f('pdf_layout').'&logo='.$contracts->f('pdf_logo');
		}else{
			if(ACCOUNT_CONTRACT_PDF_FORMAT){
				$link_end = '&type='.ACCOUNT_CONTRACT_PDF_FORMAT;
			}else{
				$link_end = '&type=1';
			}
		}

		$text = gm('Company');
		if(!$contracts->f('customer_id')){
			$text = gm('Name');
		}

		$this->discount = $contracts->f('discount');
		if($this->is_discount < 2){
			$this->discount = 0;
		}

		$in['vat_regime_id'] = $contracts->f('vat_regime_id');
		if($in['vat_regime_id'] && $in['vat_regime_id']<10000){
		     $in['vat_regime_id']=$this->db->field("SELECT id FROM vat_new WHERE vat_regime_id='".$in['vat_regime_id']."' ");
		}
		if($in['vat_regime_id']){
		     $in['remove_vat']=$this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		     $in['vat']=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.id='".$in['vat_regime_id']."' ");
		}
		$vat=$in['vat'] ? display_number($in['vat']) : display_number(0);
		$rel = ($contracts->f('customer_id') ? 'customer_addresses' : 'customer_contact_address').'.'.($contracts->f('customer_id') ? 'customer_id' : 'contact_id').'.'.($contracts->f('customer_id') ? $contracts->f('customer_id') : $quote->f('contact_id'));
		$this->currency_type = $contracts->f('currency_type');
		$this->currency_rate = $contracts->f('currency_rate');

		$in['discount'] = $in['discount'] ? $in['discount'] : $contracts->f('discount');

		//console::log('f','apply_discount: '.$quote->f('apply_discount').' discount: '.$quote->f('discount').' discount_line_gen: '.$quote->f('discount_line_gen'));

		if($in['customer_id']){
			if($in['save_final']==1){
				$final=true;
			}else{
				$final=false;
			}
		}
		// $default_note = $this->db->field("SELECT value FROM default_data WHERE type='quote_note' ");

		if($in['buyer_id']){
			$customer_disc = $this->db->query("SELECT line_discount, apply_line_disc,apply_fix_disc FROM customers WHERE customer_id='".$in['buyer_id']."' ");
		}else{
			$customer_disc = $this->db->query("SELECT line_discount, apply_line_disc,apply_fix_disc FROM customers WHERE customer_id='".$in['customer_id']."' ");
		}
		if($customer_disc->f('apply_line_disc')==1){
			$discount_line=true;
		}else{
			if($contracts->f('apply_discount')==0){
				$discount_line=false;
				$discount_global=false;
			}elseif($contracts->f('apply_discount')==1){
				$discount_line=true;
				$discount_global=false;
			}elseif($contracts->f('apply_discount')==2){
				$discount_line=false;
				$discount_global=true;
			}elseif($contracts->f('apply_discount')==3){
				$discount_line=true;
				$discount_global=true;
			}
		}

		if($customer_disc->f('apply_fix_disc')==1){
			$discount_global=true;
		}else{
			if($contracts->f('apply_discount')==0){
				$discount_line=false;
				$discount_global=false;
			}elseif($contracts->f('apply_discount')==1){
				$discount_line=true;
				$discount_global=false;
			}elseif($contracts->f('apply_discount')==2){
				$discount_line=false;
				$discount_global=true;
			}elseif($contracts->f('apply_discount')==3){
				$discount_line=true;
				$discount_global=true;
			}
		}
		
		$in['show_vat'] = $in['remove_vat']=='1' ? false : true;

		$in['cat_id'] = $cat;
		$version_code=$contracts->f('version_code');

     		$billable_dd = build_contract_budget_type_list($contracts->gf('budget_type'));

     		$output['save_final']					= $final;

     		$output['pdf_layout']					= $contracts->f('pdf_layout');
		$output['main_address_id']				= $contracts->f('main_address_id');
		$output['default_currency']				= ACCOUNT_CURRENCY_TYPE;
		$output['default_currency_code']		= currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		$output['default_currency_value']		= currency::get_currency(ACCOUNT_CURRENCY_TYPE);
		$output['cat_id']						= $cat;
		$output['SHOW_PRODUCT_SAVE']         	= 0;
		$output['HIDE_VERSION']              	= $in['main_c_id']?false:true;
		$output['VIEW_VERSION']              	= $in['main_v_id']?true:false;
		$output['own_reference']           		= $contracts->f('own_reference');
		$output['CURRENT_VERSION']           	= $contracts->f('version_code');
		$output['LAST_SERIAL_NUMBER']        	= '['.$contracts->f('version_code').']';
		$output['version_id']                  	= $contracts->f('version_id');
		$output['type']                      	= $contracts->f('type');
		$output['seller_name']              	= ACCOUNT_COMPANY;
		$output['discount']                  	= display_number($contracts->f('discount'));
		$output['vat']                      	= $vat;
		$output['contract_version_date']				= $contract_date;
		// 'NOTES'                     	=> utf8_decode($in['notes']),
		$output['CURRENCY_TYPE_LIST']        	= build_currency_list($contracts->f('currency_type'));
		$output['currency_txt']		        	= currency::get_currency($contracts->f('currency_type'));
	  	// $output['APPLY_DISCOUNT_LIST']      	= build_apply_discount_list($quote->f('apply_discount') );
	  	$output['apply_discount']      			= $contracts->gf('apply_discount');
	  	$output['apply_discount_line']      	= $discount_line;
	  	$in['apply_discount_global'] =			$discount_global;
		$output['apply_discount_global']      	= $discount_global;
	  	// $output['APPLY_DISCOUNT_LIST_TXT']		= build_apply_discount_list($quote->f('apply_discount'),true);
	  	$output['email_language']				= $contracts->f('email_language');
		//Buyer Details
		$output['buyer_id']                  	= $contracts->f('customer_id');
		$output['buyer_name']                	= $contracts->f('customer_name')? $contracts->f('customer_name'):get_customer_name($contracts->f('customer_id'));
		$output['contact_id']                	= $contracts->f('contact_id');
		$output['contact_name']              	= $contracts->f('contact_id') ? get_contact_name($contracts->f('contact_id')): trim($contact_name->f('firstname').' '.$contact_name->f('lastname'));
		
		$output['is_contact']					= $contracts->f('is_contact') ? true : false;
		$output['buyer_reference']           	= $q_ref;
		/*$output['buyer_country_id']          	= $quote->f('buyer_country_id');
		$output['BUYER_COUNTRY_NAME']        	= get_country_name($quote->f('buyer_country_id'));
		$output['buyer_state_id']            	= $quote->f('buyer_state_id');
		$output['BUYER_STATE_NAME']          	= get_state_name($quote->f('buyer_state_id'));
		$output['buyer_city']                	= $quote->f('buyer_city');
		$output['buyer_zip']                 	= $quote->f('buyer_zip');
		$output['buyer_address']             	= $quote->f('buyer_address');*/
		$output['NAME_TEXT']					= $text;
		$output['currency_type']				= $contracts->f('currency_type');
	  	$output['free_field']     			= $contracts->f('free_field');
	  	$output['free_field_txt']     	= nl2br($contracts->f('free_field'));
	  	$output['delivery_address_id']			= $contracts->f('same_address');
	  	$output['sameAddress']              = !$contracts->f('same_address') ? true : false;
		$output['country_dd']					= build_country_list($buyer_details->f('country_id'));
		//Seller Delivery Details

		// $output['SELLER_NAME']               	= $quote->gf('seller_name');
		$output['seller_d_address']    			= ACCOUNT_DELIVERY_ADDRESS;
		$output['seller_d_zip']   		  		= ACCOUNT_DELIVERY_ZIP;
		$output['seller_d_city']   		  	  	= ACCOUNT_DELIVERY_CITY;
		// $output['SELLER_D_COUNTRY_DD']	  	  	= build_country_list($quote->gf('seller_d_country_id'));
		// 'SELLER_D_STATE_DD'			   	 	=> build_state_list($quote->gf('seller_d_state_id'),$quote->gf('seller_d_country_id')),
		$output['free_field']					= $in['change'] && $in['buyer_id'] ?  $free_field : ( $contracts->f('free_field') ? $contracts->f('free_field') : $free_field);
		$output['free_field_txt']				= $in['change'] && $in['buyer_id'] ?  nl2br($free_field) : ( $contracts->f('free_field') ? nl2br($contracts->f('free_field')) : nl2br($free_field));
		//Seller Billing Details

		$output['seller_b_address']  	  		= ACCOUNT_BILLING_ADDRESS;
		$output['seller_b_zip']   				= ACCOUNT_BILLING_ZIP;
		$output['seller_b_city']   			    = ACCOUNT_BILLING_CITY;
		// $output['SELLER_B_COUNTRY_DD']		    = build_country_list($quote->gf('seller_b_country_id'));
		// 'SELLER_B_STATE_DD'			    	=> build_state_list($quote->gf('seller_b_state_id'),$quote->gf('seller_b_country_id')),
		$output['seller_bwt_nr']   			    = ACCOUNT_VAT_NUMBER;
		$output['PICK_DATE_FORMAT']				= pick_date_format();
		/*$output['STAGE_DD']                     = build_data_dd('tblquote_stage',$quote->gf('stage_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['SOURCE_DD']                    = build_data_dd('tblquote_source',$quote->gf('source_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['TYPE_DD']                	  	= build_data_dd('tblquote_type',$quote->gf('t_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['REL']							= $rel.".".$in['quote_id'];*/
		$output['currency_rate']				= $contracts->f('currency_rate');
		$output['author_id']					= $contracts->f('author_id');
		$output['acc_manager_id']				= $contracts->f('acc_manager_id');
		$output['acc_manager_name']				= get_user_name($contracts->f('acc_manager_id'));
		//$output['ACC_MANAGER']					= $in['duplicate_quote_id'] ? get_user_name( $_SESSION['u_id']) :($quote->gf('author_id') ? get_user_name($quote->gf('author_id')) : '');
		$output['show_vat']						= $contracts->f('show_vat'); //$in['show_vat'] == 1 ? true : false;
		// $output['remove_vat']					= $contracts->f('remove_vat');
		$output['no_vat']						= $contracts->f('remove_vat') == 1 ? false : true;
		$output['checked_no']					= $contracts->f('remove_vat') == 1 ? '' : 'CHECKED';
		$output['show_grand']					= $contracts->f('show_grand') == 1 ? true : false;
		// $output['show_vat_txt']					= $in['show_vat'] == 1 ? gm('Yes') : gm('No');
		$output['hide_grand_t']					= $contracts->f('show_grand') == 1 ? '' : 'hide';
		$output['show_vat_pdf']					= $contracts->f('show_vat_pdf') == 1 ? true : false;
		$output['show_vat_inp']					= $contracts->f('show_grand') == 1 ? '' : 'hide';
		$output['hide_vat_input']				= $in['show_vat'] == 1 ? '' : 'hide';
		$output['hide_contact_name']			= !$contracts->f('customer_id') ? 'hide' : '';
		$output['hide_global_discount']			= $this->is_discount < 2 ? false : true;
		$output['hide_line_global_discount']	= $this->is_discount == 1 ? '' : 'hide';
		$output['not_template']					= true;
		// $output['free_text_content']			= $free_text_content;
		$output['general_conditions_new_page']	= $contracts->f('general_conditions_new_page') == 1 ? true : false;
		$output['subject']						= $contracts->gf('subject');
		$output['field']                        = $contracts->f('customer_id')?'customer_id':'contact_id';
		$output['show_img_q']					= $contracts->f('show_img_q') == 1 ? true : false;
		// $output['checked_show_img_q']			= $quote->f('show_img_q') == 1 ? 'CHECKED' : '';
		$output['save_txt']						= gm('Saved');
		/*$output['PDF_LINK']          			= 'index.php?do=quote-quote_print&id='.$this->quote_id.'&version_id='.$quote->f('version_id').'&lid='.$quote->f('email_language').$link_end;
		$output['begin_pdf_link']          		= 'index.php?do=quote-quote_print&id='.$this->quote_id.'&version_id='.$quote->f('version_id').'&lid=';*/
		$output['end_pdf_link']					= $link_end;
		$output['main_country_id']				= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$output['hide_currency2'] 				= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
		$output['hide_currency1'] 				= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
		$output['show_QC']						= $contracts->gf('show_QC') == 1 ? true : false;
		$output['show_UP']						= $contracts->gf('show_UP') == 1 ? true : false;
		$output['show_V']						= $contracts->gf('show_V') == 1 ? true : false;
		$output['show_PS']						= $contracts->gf('show_PS') == 1 ? true : false;
		$output['installation_id']				= $contracts->f('installation_id');
		$output['old_installation_id']			= $contracts->f('installation_id');
		$output['your_ref']			=  $contracts->gf('your_ref');
		$output['start_date']						= $contracts->f('start_date') ? $contracts->f('start_date') : '';
		$output['end_date']						= $contracts->f('end_date') ? $contracts->f('end_date') : '';
		$output['reminder_date']				= $contracts->f('reminder_date') ? $contracts->f('reminder_date')*1000 : '';
		$output['last_used']					= get_last_contract_number();
		$output['quote_id']					= $contracts->f('quote_id');
		$output['type_list']	                        = build_contract_list($contracts->f('type'));
		$output['type']                              = $contracts->f('type');
		$output['reminder_type_dd']       	       = build_reminder_type_list($contracts->f('reminder_type'));
		$output['reminder_type']			= $contracts->f('reminder_type');
		$output['allow_stock']                 = ALLOW_STOCK;
		$output['product_options']                   =  $contracts->f('type')==1 ? '':'hide';
		$output['view_contract_type_2']              =  $contracts->f('type')==2 ? '':'hide';
		$output['budget_type_dd']       	       	    = $billable_dd;
		$output['t_ct_hour']            				= $contracts->f('t_ct_hour');
		$output['view_buget_options']                = $contracts->f('budget_type')   == 1 ? 'hide' : '';
		$output['budget_type']						= $contracts->f('budget_type');
		$output['send_email_alerts'] 		= $contracts->f('send_email_alerts') == 1 ? true : false;
		$output['alert_percent']	           	 		= $contracts->f('alert_percent');
		$output['disabled_alert_percent']    		= $contracts->f('send_email_alerts') == 1 ? false : true;

		$output['main_language_dd'] 			        = build_language_admin_dd($in['email_language']);
		$output['hide_if_unlimited_period']			= $contracts->f('no_end_date') == 1 ? 'hide' : '' ;
		$output['unlimited_period']				= $contracts->f('no_end_date') == 1 ? true : false;
		$output['authors']					= $this->get_author($in);
		$output['accmanager']					= $this->get_accmanager($in);
		// ));
	   	$output['discount_line_gen']				= display_number($contracts->gf('discount_line_gen'));
	   	$output['vat_regim_dd']					= build_vat_regime_dd();
		$output['vat_regime_id']				= $contracts->f('vat_regime_id') ? $contracts->f('vat_regime_id') : '';
		if($contracts->f('vat_regime_id')>=1000){
			$extra_eu=$this->db->field("SELECT regime_type FROM vat_new WHERE id='".$contracts->f('vat_regime_id')."' ");
			if($extra_eu==2){
				$output['block_vat']=true;
			}
			if($extra_eu=='1'){
				$output['vat_regular']=true;
			}
			$contracting_partner=$this->db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$contracts->f('vat_regime_id')."' ");
			if($contracting_partner==4){
				$output['block_vat']=true;
			}
		}
		$output['deals']				= $this->get_deals($in);
		if($contracts->f('trace_id')){
			$linked_doc=$this->db->field("SELECT tracking_line.origin_id FROM tracking_line
				INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
				WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$contracts->f('trace_id')."' ");
			if($linked_doc){
				$output['deal_id']=$linked_doc;	
			}
		}

		$transl_lang_id_active = $contracts->f('email_language');
		$language_id = $contracts->f('email_language');
		$this->version_id = $contracts->f('version_id');

			// $view->assign(array(
		$output['REF_SERIAL_NUMBER']			= $ref.$contracts->f('serial_number');
		$output['serial_number']				= $contracts->f('serial_number');
		$output['REF']							= $ref ? " var ref= '".$ref."';":' var ref;';
		$output['installation_id']				= $contracts->f('installation_id');	
		$output['old_installation_id']			= $contracts->f('installation_id');
		$output['can_change']			=$to_edit;
			// ));

		$show_img_q = $contracts->f('show_img_q');
		
		$lines = $this->getContractLines($in);
		
		$output['sort_order']= $lines['sort_order'];
		$output['quote_group'] = $lines['quote_group'];
		$output['vat_lines'] = $lines['vat_lines'];
		
		if($this->is_discount == 1 || $this->is_discount == 3){
			$output['VIEW_DISCOUNT']= true;
		}else{
			$output['VIEW_DISCOUNT']= false;
		}

		if(isset($in['template_id']) && is_numeric($in['template_id']) ){
			$output['template_name'] = $contracts->f('serial_number');
		}

		// $view->assign(array(
		$output['VAT_TOTAL']					= display_number($this->total_vat);
		$output['GRAND_TOTAL'] 					= display_number($this->grand_total_vat);
		$output['TOTAL_DEFAULT_CURRENCY']		= display_number($this->default_total);
		$output['TOTAL_WITHOUT']				= display_number($this->grand_total);
		// $output['show_vat']						= $in['show_vat'] ? ' ' : 'hide';
		$output['TOTAL_DISCOUNT']				= display_number(-$this->grand_total * $this->discount / 100);
		// ));
		if($customer_id){
			$in['no_buyer_vat'] = $this->db->field("SELECT no_vat FROM customers WHERE customer_id='".$customer_id."' ");
		}

		$in['buyer_id'] = $contracts->f('customer_id');
		$in['contact_id'] = $contracts->f('contact_id');
		$in['lang_id'] = $contracts->f('email_language');
		$notes = $this->get_notes($in);
		$output['notes'] 			= $notes['notes'];
		$output['free_text_content']= $notes['free_text_content'];
		/*		$notes = $this->get_notes($in);
		
		$output['translate_loop']					= $notes['translate_loop'];
		$output['translate_loop_custom'] 			= $notes['translate_loop_custom'];
		$output['lang_cls'] 						= $notes['lang_cls'];*/

		$output['contacts']							= $this->get_contacts($in);
		$output['addresses']						= $this->get_addresses($in);
		$output['cc']								= $this->get_cc($in);
		$output['articles_list']					= $this->get_articles_list($in);
		$output['language_dd'] 						= build_language_dd_new($in['email_language']);
		$output['APPLY_DISCOUNT_LIST']   			= build_apply_discount_list($in['apply_discount']);
		$output['contract_id']					= $in['contract_id'];
		$output['version_id']						= $this->version_id;
		$output['generalvats']=build_vat_dd();

// console::log($contracts->f('closed'));
		$this->out = $output;
	}

	public function get_cc($in)
	{
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}
		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		
		/*UNION 
			SELECT customer_contacts.customer_id, CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
			FROM customer_contacts
			LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
			LEFT JOIN country ON country.country_id=customer_contact_address.country_id
			WHERE $filter_contact*/

		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 50")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}


			$result[]=array(
				/*"id"					=> $value['cust_id'].'-'.$value['contact_id'],*/
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				/*"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts(&$in) {

		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY customer_contacts.lastname limit 9")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				/*'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), */
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
				'bottom' 				=> '',
			);

		}

		$added = false;
		if(count($result)==9){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_currency_convertor($in)
	{
		$currency = $in['currency'];
		if(empty($currency)){
			$currency = "USD";
		}
		$separator = $in['acc'];
		$into = $in['into'];
		return currency::getCurrency($currency, $into, 1,$separator);		
	}

	public function get_addresses($in)
	{
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		'symbol'				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
					'symbol'				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

	public function getQuoteIdentity($identity_id)
	{
		$array = array(			
				'name'		=> ACCOUNT_COMPANY,
				'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
				'zip'		=> ACCOUNT_DELIVERY_ZIP,
				'city'		=> ACCOUNT_DELIVERY_CITY,
				'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
				'phone'		=> ACCOUNT_PHONE,
				'fax'		=> ACCOUNT_FAX,
				'email'		=> ACCOUNT_EMAIL,
				'url'		=> ACCOUNT_URL,
				'logo'		=>  defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',		
		);
		if($identity_id != '0'){
			$mm = $this->db->query("SELECT * FROM multiple_identity WHERE identity_id='".$identity_id."'")->getAll();
			$value = $mm[0];
			
			$array=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> $value['company_logo'],
			);
			
		}
		return $array;
	}

	public function get_notes($in)
	{

		$terms = '';
		if($in['lang_id']!=1){		
			$terms = '_'.$in['lang_id'];
		}
		$quote_note = $this->db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='contract_note".$terms."' ");
		$free_text_content = $this->db->field("SELECT value FROM default_data
												   WHERE default_name = 'contract_term'
												   AND type = 'contract_terms".$terms."'  ");
		if($this->contract_id){ // the function is called from inside the class

			if($this->contract_id == 'tmp'){
				
				$array['notes'] = $quote_note;
				if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['lang_id']){
					$acc_langs=array('en','fr','nl','de');
					$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['lang_id']."' ");
					if($lang_c){
						if($lang_c=='du'){
								$lang_c='nl';
						}
						if(in_array($lang_c, $acc_langs)){
							$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
							if($in['vat_notes']){
								if($array['notes']){
									$array['notes']=$array['notes']."\n".$in['vat_notes'];
								}else{
									$array['notes']=$in['vat_notes'];
								}
							}
						}
					}		
				}

				$array['free_text_content'] = $free_text_content;
			}else{
				
				$quote_note = $this->db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='contract_note".$terms."' ");
				$quote_notes = $this->db->field("SELECT item_value FROM note_fields
								 WHERE item_id= '".$in['contract_id']."' AND lang_id ='".$in['lang_id']."' AND item_type='contract' AND item_name='notes' AND version_id = '".$this->version_id."'");
				
				if($quote_notes){
					$array['notes']=$quote_notes;
				}else{
					$array['notes']=$quote_note;
					if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['lang_id']){
						$acc_langs=array('en','fr','nl','de');
						$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['lang_id']."' ");
						if($lang_c){
							if($lang_c=='du'){
								$lang_c='nl';
							}
							if(in_array($lang_c, $acc_langs)){
								$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
								if($in['vat_notes']){
									if($array['notes']){
										$array['notes']=$array['notes']."\n".$in['vat_notes'];
									}else{
										$array['notes']=$in['vat_notes'];
									}
								}
							}
						}		
					}
				}			

				$free_text_content = $this->db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'contract'
									   AND 	item_name 	= 'free_text_content'
									   AND 	version_id 	= '".$this->version_id."'
									   AND 	item_id 	= '".$this->contract_id."' ");

				$array['free_text_content'] = $free_text_content;

			}
		}else{ // the function is called when the user changed the language

			$array['notes'] = $quote_note;
			if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['lang_id']){
				$acc_langs=array('en','fr','nl','de');
				$lang_c=$this->db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['lang_id']."' ");
				if($lang_c){
					if($lang_c=='du'){
							$lang_c='nl';
					}
					if(in_array($lang_c, $acc_langs)){
						$in['vat_notes']=stripslashes($this->db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
						if($in['vat_notes']){
							if($array['notes']){
								$array['notes']=$array['notes']."\n".$in['vat_notes'];
							}else{
								$array['notes']=$in['vat_notes'];
							}
						}
					}
				}		
			}
			$array['free_text_content'] = $free_text_content;

		}		 

		return $array;
	}

	private function getContractEditLines(&$in)
	{
		if($in['quote_id']){
			$groups = $this->db->query("SELECT * FROM tblquote_group WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."' ORDER BY sort_order ASC");
		}else{
			$groups = $this->db->query("SELECT * FROM contracts_group WHERE contract_id = '".$this->contract_id."' AND version_id = '".$this->version_id."' ORDER BY sort_order ASC");
		}		
		$in['quote_group'] = array();//clear error data
		$in['quote_group_order'] = array();
		$this->grand_total = 0;
		$this->total_vat = 0;
		while ($groups->next()) {
			$group_code = unique_id();
			$group_content = '';
			$groupI=0;
			if($in['quote_id']){
				$quoteLine = $this->db->query("SELECT * FROM tblquote_line WHERE quote_id='".$in['quote_id']."' AND group_id = '".$groups->f('group_id')."' AND `line_type`!='8' ORDER BY sort_order,line_order ASC");
			}else{
				$quoteLine = $this->db->query("SELECT * FROM contract_line WHERE contract_id='".$this->contract_id."' AND group_id = '".$groups->f('group_id')."' AND use_recurring = 0 ORDER BY sort_order,line_order ASC");
			}		
			while($quoteLine->next()){

				$line_discount = $quoteLine->f('line_discount');
				if($this->is_discount == 0 || $this->is_discount == 2){
					$line_discount = 0;
				}

				if(!isset($in['quote_group'][$group_code])){
					console::log('here');
					$in['quote_group'][$group_code] = array(
					    'title' => stripslashes(htmlspecialchars_decode($groups->f('title'),ENT_QUOTES)),
					    'show_chapter_total'=>$groups->f('show_chapter_total'),
					    'pagebreak_ch' => $groups->f('pagebreak_ch'),
					    'show_QC' => $groups->f('show_QC'),
						'show_UP' => $groups->f('show_UP'),
						'show_PS' => $groups->f('show_PS'),
						'show_d' => $groups->f('show_d'));
				}
				if($quoteLine->f('content_type') == 1 ){
					if(!isset($in['quote_group'][$group_code]['table'])){
						$in['quote_group'][$group_code]['table'] = array(
						$quoteLine->f('table_id') => array(
						'description' => array($quoteLine->f('name')),
	          			'article_code' => array($quoteLine->f('article_code')),
	          			'article_id' => array($quoteLine->f('article_id')),
	          			'is_tax' => array($quoteLine->f('is_tax')),
	          			// 'for_article' => array( $quoteLine->f('is_tax') == 1 ? $this->db->field("SELECT article_id FROM  `pim_articles_taxes`  WHERE tax_id='".$quoteLine->f('is_tax')."'") : '' ),
						'quantity' => array($quoteLine->f('quantity')),
	          			'line_discount' => array($quoteLine->f('line_discount')),
	          			'line_vat' => array($quoteLine->f('vat')),
						'sale_unit' => array($quoteLine->f('sale_unit')),
						'package' => array($quoteLine->f('package')),
						'price' => array($quoteLine->f('price')),
						'vat_val' => array($quoteLine->f('vat')),
						'line_type' => array($quoteLine->f('line_type')),
						'show_block_total' => array($quoteLine->f('show_block_total')),
						'hide_QC' => array($quoteLine->f('hide_QC')),
						'hide_AC' => array($quoteLine->f('hide_AC')),
						'hide_UP' => array($quoteLine->f('hide_UP')),
						'hide_DISC' => array($quoteLine->f('hide_DISC')),
						'show_block_vat' => array($quoteLine->f('show_block_vat')),
						'service_bloc_title' => array($quoteLine->f('service_bloc_title'))						
						));
					}else{
						//if it's set we check for the table if it's set
						if(!isset($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')])){
							$in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')] = array(
							'description' => array($quoteLine->f('name')),
	            			'article_code' => array($quoteLine->f('article_code')),
	            			'article_id' => array($quoteLine->f('article_id')),
	            			'is_tax' => array($quoteLine->f('is_tax')),
	            			// 'for_article' => array( $quoteLine->f('is_tax') == 1 ? $this->db->field("SELECT article_id FROM  `pim_articles_taxes`  WHERE tax_id='".$quoteLine->f('is_tax')."'") : '' ),
							'quantity' => array($quoteLine->f('quantity')),
	            			'line_discount' => array($quoteLine->f('line_discount')),
	            			'line_vat' => array($quoteLine->f('vat')),
							'sale_unit' => array($quoteLine->f('sale_unit')),
							'package' => array($quoteLine->f('package')),
							'price' => array($quoteLine->f('price')),
							'vat_val' => array($quoteLine->f('vat')),
							'line_type' => array($quoteLine->f('line_type')),
							'show_block_total' => array($quoteLine->f('show_block_total')),
							'hide_QC' => array($quoteLine->f('hide_QC')),
							'hide_AC' => array($quoteLine->f('hide_AC')),
							'hide_UP' => array($quoteLine->f('hide_UP')),
							'hide_DISC' => array($quoteLine->f('hide_DISC')),
							'show_block_vat' => array($quoteLine->f('show_block_vat')),
							'service_bloc_title' => array($quoteLine->f('service_bloc_title'))
							);
						}else{
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['description'],$quoteLine->f('name'));
	            			array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['article_code'],$quoteLine->f('article_code'));
	            			array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['article_id'],$quoteLine->f('article_id'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['quantity'],$quoteLine->f('quantity'));
	            			array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['line_discount'],$quoteLine->f('line_discount'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['line_vat'],$quoteLine->f('vat'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['sale_unit'],$quoteLine->f('sale_unit'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['package'],$quoteLine->f('package'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['price'],$quoteLine->f('price'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['vat_val'],$quoteLine->f('vat'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['line_type'],$quoteLine->f('line_type'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['show_block_total'],$quoteLine->f('show_block_total'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_AC'],$quoteLine->f('hide_AC'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_QC'],$quoteLine->f('hide_QC'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_UP'],$quoteLine->f('hide_UP'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['show_block_vat'],$quoteLine->f('show_block_vat'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['is_tax'],$quoteLine->f('is_tax'));
							// array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['for_article'], $quoteLine->f('is_tax') == 1 ? $this->db->field("SELECT article_id FROM  `pim_articles_taxes`  WHERE tax_id='".$quoteLine->f('is_tax')."'") : '' );
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_DISC'],$quoteLine->f('hide_DISC'));
							// array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['service_bloc_title'],$quoteLine->f('service_bloc_title'));
						}
					}
					/*$line_discount = $quoteLine->f('line_discount');
					if($this->is_discount == 0 || $this->is_discount == 2){
						$line_discount = 0;
					}*/
					$price = ($quoteLine->f('price')-($quoteLine->f('price')*$line_discount/100));
					if($this->is_discount > 1){
						$price = $price - $price*$this->discount/100;
					}
					if($quoteLine->f('show_block_total') == 0){
						$this->grand_total += ($quoteLine->f('price')-($quoteLine->f('price')*$quoteLine->f('line_discount')/100))*$quoteLine->f('quantity')*$quoteLine->f('package')/$quoteLine->f('sale_unit');
						// if($in['show_vat']){

							$this->total_vat += $price*($quoteLine->f('quantity')*$quoteLine->f('package')/$quoteLine->f('sale_unit'))* $quoteLine->f('vat')/100;
						// }
					}
				}

				if($quoteLine->f('content_type') == 2){
					if(!isset($in['quote_group'][$group_code]['content'])){
						$in['quote_group'][$group_code]['content'] = array($quoteLine->f('table_id') => $quoteLine->f('content'));
						$in['quote_group'][$group_code]['content_id'] = array($quoteLine->f('table_id') => $quoteLine->f('id'));
					}else{
						$in['quote_group'][$group_code]['content'][$quoteLine->f('table_id')] = $quoteLine->f('content');
						$in['quote_group'][$group_code]['content_id'][$quoteLine->f('table_id')] = $quoteLine->f('id');
					}
				}

				if($quoteLine->f('content_type') == 3){
					if(!isset($in['quote_group'][$group_code]['pagebreak'])){
						$in['quote_group'][$group_code]['pagebreak'] = array($quoteLine->f('table_id') => '');
					}else{
						$in['quote_group'][$group_code]['pagebreak'][$quoteLine->f('table_id')] = '';
					}
				}
				$in['quote_group_order'][$group_code][$quoteLine->f('table_id')] = $quoteLine->f('sort_order');
				$groupI++;
			}
			// console::log($groupI);
			if($groupI==0){
				if(!isset($in['quote_group'][$group_code])){
					
					$in['quote_group'][$group_code] = array('title' => $groups->f('title'),'show_chapter_total'=>$groups->f('show_chapter_total'),'pagebreak_ch' => $groups->f('pagebreak_ch'),
					    'show_QC' => $groups->f('show_QC'),
						'show_UP' => $groups->f('show_UP'),
						'show_PS' => $groups->f('show_PS'),
						'show_d' => $groups->f('show_d'));
					// $default_group_id2 = unique_id();
					$default_table_id2 = unique_id();
					// $default_sort2 = array($default_group_id2 => array($default_table_id2 => 0));
					$in['quote_group_order'][$group_code][$default_table_id2] = 0;
				}
			}

		}		
	}

	private function getContractLines($in)
	{
		
		$this->getContractEditLines($in);
		list($default_group,$default_sort) = $this->setDefaultData();
		
		if(empty($in['quote_group'])){
			//empty array we need to init with something
			$in['quote_group'] = $default_group;
		}

		if(empty($in['quote_group_order'])){
			$in['quote_group_order'] = $default_sort;
		}
		$array = array('sort_order' => array(), 'quote_group' => array() );
		// console::log($in['quote_group_order'],$in['quote_group']);
		if($in['quote_group_order']){
			foreach ($in['quote_group_order'] as $group_id => $group_items) {
				$chapter_total = 0;
				$quote_group_line = array('table'=>array());
				foreach($group_items as $item => $sort_order){
					$quote_table_line = array('quote_line'=>array());
					//check to see if it's a table or content
					$content_type = 0;//don't know what it is yet
					$table = array(); $content = '';
					if(isset($in['quote_group'][$group_id]['table']) && isset($in['quote_group'][$group_id]['table'][$item])){
						$content_type = 1;//it's a table; congrats
						$table = $in['quote_group'][$group_id]['table'][$item];
					}

					if(isset($in['quote_group'][$group_id]['content']) && isset($in['quote_group'][$group_id]['content'][$item])){
						$content_type = 2;//it's content
						$content = $in['quote_group'][$group_id]['content'][$item];
						$content_id = $in['quote_group'][$group_id]['content_id'][$item];
					}

					if(isset($in['quote_group'][$group_id]['pagebreak']) && isset($in['quote_group'][$group_id]['pagebreak'][$item])){
						$content_type = 3;//page break; yeye
					}

					if($content_type == 0){
						//could not find out what it is so just ignore it;
						continue;
					}

					if(!empty($table) && $content_type == 1){
						$i = 0;
						$tableTotal = 0;
						$vat_line = 0;
						foreach($table['description'] as $index => $val){
							if(empty($val)){
								continue;
							}
							// $view->assign('QUOTE_GROUP_ID',$group_id,'quote_group');
							$quote_group_line['quote_group_id'] = $group_id;
							$image = '';
							$show_block_total = true;
							if($table['show_block_total'][$index] == 1){
								$show_block_total = false;
							}
							$show_block_vat = false;
							if($table['show_block_vat'][$index] == 1){
								$show_block_vat = true;
							}
							$hide_QC=false;
							$hide_AC=false;
							$hide_UP=false;
							$hide_DISC=false;
							if($table['hide_AC'][$index] == 1){
								$hide_AC = true;
							}
							if($table['hide_QC'][$index] == 1){
								$hide_QC = true;
							}
							if($table['hide_UP'][$index] == 1){
								$hide_UP = true;
							}
							if($table['hide_DISC'][$index] == 1){
								$hide_DISC = true;
							}
							// $delete_r ='';
							if(!$table['article_code'][$index])  { //is_service
		          				$service_bloc_title = $table['service_bloc_title'][0];
		          				// $delete_r ='delete_more_to_the_left';
		          				// $view->assign(array(
			    		    	$quote_table_line['ONLY_ARTICLE']			= 'hide';
			    		    	// $quote_table_line['item_txt']				= '<span class="bloc">'.gm('Service bloc title').'</span><input name="quote_group['.$group_id.'][table]['.$item.'][service_bloc_title]" value="'.$service_bloc_title.'" type="text" class="large" style="width:250px;margin:0px 0px 0px 10px"/>';
			    		    	$quote_table_line['item_text']				= $service_bloc_title;
			    		    	// $quote_table_line['add_a_line']   	 		= gm('Add line');
			    	    		// ),'quote_table');
		        			}else{
					            // $view->assign(array(
					            $quote_table_line['ONLY_ARTICLE']			= '';
					    //         $quote_table_line['item_txt']					= '<span class="bloc" style="vertical-align:middle;">'.gm('Article bloc').'</span><span class="question_mark atip show_tip_info" style="padding-top:0px;">
									// <a class="tooltip" title="Show tip"  href="#"></a>
									// <div class="tip_info">'.gm('The article content can be changed in the').' <a href="index.php?do=quote-quote_note&tabbq=9" style="text-decoration:underline;">'.gm('Configure Module').'</a> '.gm('section').'</div>
									// </span>';
								$quote_table_line['item_text']				= '';
					            // $quote_table_line['add_a_line']    		=  gm('Add an article');
					            $quote_table_line['hide_not_service']	= 'hide';
					            // ),'quote_table');
		            			$image_q = $this->db->query("SELECT pim_article_photo.file_name,pim_article_photo.name, pim_article_photo.upload_amazon FROM pim_article_photo
		            											INNER JOIN pim_articles ON pim_article_photo.parent_id=pim_articles.article_id
		             								WHERE pim_article_photo.parent_id='".$table['article_id'][$index]."' ORDER BY pim_article_photo.photo_id ASC LIMIT 1 ");
								if($image_q->next() && $show_img_q==1){
									if($image_q->f('upload_amazon')==1){
										$image ='<img src="'.$image_q->f('file_name').'" alt="'.$image_q->f('name').'" width="50" />';
									}else{
										$image ='<br/><img src="upload/'.$database_config['mysql']['database'].'/pim_article_photo/'.$image_q->f('file_name').'" width="50" />';
									}
								}
		          			}
				          	if(!$table['package'][$index]){
				              	$table['package'][$index]=1;
				          	}
		          			if(!$table['sale_unit'][$index]){
		              			$table['sale_unit'][$index]=1;
		          			}
		          			if(!$this->allowPacking){
		          				// $view->assign(array(
								$quote_table_line['USE_SALE_UNIT']= false;
								$quote_table_line['USE_PACKAGE']= false;
								// ),'quote_table');
		          			}else{
		          				$quote_table_line['USE_SALE_UNIT']= true;
								$quote_table_line['USE_PACKAGE']= true;
		          			}


		          			if(!$this->allowSaleUnit){
		          				// $view->assign(array('USE_SALE_UNIT'=> 'class="hide"'),'.quote_table');
		          				$quote_table_line['USE_SALE_UNIT']= false;
		          			}
		          			
		           			/*if($this->is_vat== 1){
		              			// $view->assign(array('VIEW_VAT'=>''),'quote_table');
		              			$quote_table_line['VIEW_VAT']= true;
		          			}else{
		              			// $view->assign(array('VIEW_VAT'=>'hide'),'quote_table');
		              			$quote_table_line['VIEW_VAT']= false;
		          			}*/
		          			$line_total=$table['quantity'][$index] * $table['price'][$index] * ($table['package'][$index]/ $table['sale_unit'][$index]);

				          	$quote_line_line = array(
								'tr_id'         			=> $item,
								'line_type'      			=> $table['line_type'][$index],
								'description'  				=> html_entity_decode($val),
								'quantity'      			=> display_number($table['quantity'][$index]),
								'article_code'      		=> $table['article_code'][$index],
								'article_id'      			=> $table['article_id'][$index],
								'is_tax'					=> $table['is_tax'][$index],
								'for_article'				=> $table['for_article'][$index],
						      	'line_discount'      		=> display_number($table['line_discount'][$index]),
						      	'line_vat'      			=> display_number($table['line_vat'][$index]),
						      	'sale_unit'      			=> $table['sale_unit'][$index],
								'package'      			  	=> remove_zero_decimals($table['package'][$index]),
								'price'         			=> display_number_var_dec($table['price'][$index]),
								'price_vat'					=> display_number($table['price'][$index] + (($table['price'][$index]*$table['line_vat'][$index])/100)),
								'line_total'          		=> display_number($line_total - ($line_total* $table['line_discount'][$index]/100)),
								'hide_currency2'			=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
								'hide_currency1'			=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
								'vat_val'					=> $table['vat_val'][$index],
								'art_img'					=> $image,
								'show_block_total'			=> $table['show_block_total'][$index],
								'hide_QCv'					=> $table['hide_QC'][$index],
								'hide_ACv'					=> $table['hide_AC'][$index],
								'hide_UPv'					=> $table['hide_UP'][$index],
								'hide_DISCv'				=> $table['hide_DISC'][$index],
								'show_lVATv'				=> $table['show_block_vat'][$index],
								'colum'						=> $table['line_type'][$index] == 1 ? $in['colum'] : $in['colum']+2,
								// 'delete_r'						=> $delete_r,
								// 'width'								=> !$table['article_code'][$index] ? '370' : '313'
							);
							// ,'quote_line');
							$tableTotal +=$line_total - ($line_total* $table['line_discount'][$index]/100);
							if($this->is_discount > 1){
								$p = ($line_total - ($line_total* $table['line_discount'][$index]/100));
								$p = $p - $p * $this->discount / 100;
								$vat_line += $p * $table['vat_val'][$index]/100;
							}else{
								$vat_line += ($line_total - ($line_total* $table['line_discount'][$index]/100))*$table['vat_val'][$index]/100;
							}
							// $view->loop('quote_line','quote_table');
							array_push($quote_table_line['quote_line'], $quote_line_line);
							$i++;
						}
		        		// $view->assign('LINE_TYPE'  , $table['line_type'][$index],'quote_table');
						$quote_table_line['line_type']=$table['line_type'][$index];
		        		// $view->assign(array('GROUP_TYPE' => $table['article_code'][$index]?'1':'2'),'quote_table');
		        		$quote_table_line['group_type']= $table['article_code'][$index]?'1':'2';
						if($i > 0){
							// $view->assign(array(
							$quote_table_line['total'] 				= display_number($tableTotal);
							$quote_table_line['hide_currency2'] 	= ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide';
							$quote_table_line['hide_currency1'] 	= ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide';
							$quote_table_line['group_table_id'] 	= $item;
							$quote_table_line['total_vat']			= $vat_line;
							$quote_table_line['total_vat_span']		= display_number($vat_line);
							$quote_table_line['q_table']			= true;
							$quote_table_line['block_total_hide']	= $show_block_total ? '' : 'hide';
							$quote_table_line['checkedBAD']			= $show_block_total ? '' : 'CHECKED';
							$quote_table_line['checkedQC']			= $hide_QC ? 'CHECKED' : '';
							$quote_table_line['checkedAC']			= $hide_AC ? 'CHECKED' : '';
							$quote_table_line['checkedUPC']			= $hide_UP ? 'CHECKED' : '';
							$quote_table_line['checkedDISC']		= $hide_DISC ? 'CHECKED' : '';
							$quote_table_line['hide_QC']			= $hide_QC ? 'hide' : "";
							$quote_table_line['hide_AC']			= $hide_AC ? 'hide' : "";
							$quote_table_line['hide_UP']			= $hide_UP ? 'hide' : "";
							$quote_table_line['hide_DISC']			= $hide_DISC ? 'hide' : "";
							$quote_table_line['show_lVAT']			= $show_block_vat && $show_block_total ? '' : 'hide';
							$quote_table_line['checkedVAT']			= $show_block_vat ? 'CHECKED' : '';
							$quote_table_line['sort_order']			= $sort_order;
							// ),'quote_table');
							// $view->loop('quote_table','quote_group');
							array_push($quote_group_line['table'], $quote_table_line);
						}
						if($show_block_total === true){
							$chapter_total += $tableTotal;
						}
					}
					if(!empty($content) && $content_type == 2){
						// $view->assign(array(
							$quote_table_line['quote_group_id']		= $group_id;
							$quote_table_line['group_content_id']	= $item;
							$quote_table_line['quote_content']		= $content;
							$quote_table_line['content_id']			= $content_id;
							$quote_table_line['q_content']			= true;
							$quote_table_line['sort_order']			= $sort_order;
						// ),'quote_table');
						if(!$use){
		         			// $view->assign(array(
								// $quote_table_line['USE_SALE_UNIT']	= 'class="hide"';
								// $quote_table_line['USE_PACKAGE']	= 'class="hide"';
							// ),'quote_table');
		        		}
				        if(!$use2){
				         	// $view->assign(array('USE_SALE_UNIT'=> 'class="hide"'),'.quote_table');
				         	// $quote_table_line['USE_SALE_UNIT'] = 'class="hide"';
				        }
						// $view->loop('quote_table','quote_group');
						array_push($quote_group_line['table'], $quote_table_line);
					}

					if($content_type == 3){
						// $view->assign(array(
							$quote_table_line['quote_group_id']		= $group_id;
							$quote_table_line['group_break_id']		= $item;
							$quote_table_line['pagebreak']			= true;
							$quote_table_line['sort_order']			= $sort_order;
						// ),'quote_table');
						if(!$use){
		          			// $view->assign(array(
								// $quote_table_line['USE_SALE_UNIT']	= 'class="hide"';
								// $quote_table_line['USE_PACKAGE']	= 'class="hide"';
							// ),'quote_table');
		        		}
				        if(!$use2){
				         	// $view->assign(array('USE_SALE_UNIT'=> 'class="hide"'),'.quote_table');
				         	// $quote_table_line['USE_SALE_UNIT']		= 'class="hide"';
				        }
						// $view->loop('quote_table','quote_group');
						array_push($quote_group_line['table'], $quote_table_line);
					}

					array_push($array['sort_order'], array(
						'group_id' => $group_id,
						'item_id' => $item,
						'order' => $sort_order
					));
					// ,'sort_order');
					// $view->loop('sort_order');

				}

				$quote_group_line['chapter_total'] = display_number($chapter_total);
				$quote_group_line['group_id'] = $group_id;

				$quote_group_line['title'] =$in['quote_group'][$group_id]['title'];
				$quote_group_line['show_QC'] = $in['quote_group'][$group_id]['show_QC'] == '1' ? true : false;
				$quote_group_line['show_UP'] = $in['quote_group'][$group_id]['show_UP'] == '1' ? true : false;
				$quote_group_line['show_PS'] = $in['quote_group'][$group_id]['show_PS'] == '1' ? true : false;
				$quote_group_line['show_d'] = $in['quote_group'][$group_id]['show_d'] == '1' ? true : false;
				$quote_group_line['quote_group_pb'] = $in['quote_group'][$group_id]['pagebreak_ch'];

				$show_chapter_total = '0';
				$show_chapter_subtotal = 'hide';

				// console::log($in['quote_group'][$group_id]);
				if($in['quote_group'][$group_id]['show_chapter_total']==1){
					$show_chapter_total = '1';
					$show_chapter_subtotal = '';
				}

				$quote_group_line['show_chapter_total']=$show_chapter_total == 1 ? true : false;
				$quote_group_line['show_chapter_subtotal']=$show_chapter_subtotal;
				$show_chapter_pb = $in['quote_group'][$group_id]['pagebreak_ch'] == 1 ? '' :'hide';
				$quote_group_line['show_chapter_pb']=$show_chapter_pb;
				$quote_group_line['active'] = count($array['quote_group']) == 0 ? true : false;
				// $quote_group_line['active'] = true;

				// $view->loop('quote_group');
				array_push($array['quote_group'], $quote_group_line);
				
			}
		}
		$this->grand_total = round($this->grand_total,2);
		$this->total_vat = round($this->total_vat,2);

		$this->grand_total_vat = ($this->grand_total - $this->grand_total * $this->discount / 100 ) + $this->total_vat;
		if(($this->currency_type != ACCOUNT_CURRENCY_TYPE) && $this->currency_rate){
			$this->default_total = return_value($this->currency_rate)*$this->grand_total_vat;
		}

		$array['vat_lines'] = $this->get_vatLines($array['quote_group'],$in['discount'],$in['apply_discount_global']);

		// $array['VAT_TOTAL']					= display_number($total_vat);
		// $array['GRAND_TOTAL'] 				= display_number($grand_total_vat);
		// $array['TOTAL_DEFAULT_CURRENCY']	= display_number($default_total);
		// $array['TOTAL_WITHOUT']				= display_number($grand_total);		
		// $array['TOTAL_DISCOUNT']			= display_number(-$grand_total * $this->discount / 100);
		return $array;
	}


	/**
	 * @return array=>
	 * '21' => array('vat-value'=>'10','subtotal'=>'100')
	 * '6' => array('vat-value'=>'6','subtotal'=>'6')
	*/
	private function get_vatLines($array, $global_discount,$show_discount){
		$result = array();
		foreach ($array as $key => $value) { // chapter
			foreach ($value['table'] as $tkey => $tvalue) { // table
				foreach ($tvalue['quote_line'] as $lkey => $lvalue) { // line
	
					if($lvalue['line_type']!=7 && $lvalue['show_block_total']!='1'){
						if(!$result[$lvalue['vat_val']]){
							$result[$lvalue['vat_val']] = array('vat_value'=>0,'subtotal'=>0);
						}
				
						if(empty($lvalue['package']) || empty($lvalue['sale_unit'])){
							$lvalue['package'] = 1;
							$lvalue['sale_unit'] = 1;
						}
						$result[$lvalue['vat_val']]['subtotal']=return_value($result[$lvalue['vat_val']]['subtotal']);
						if($lvalue['package']){							
							/*$result[$lvalue['vat_val']]['subtotal']+=$lvalue['quantity']*
							(($lvalue['price']-$lvalue['price']*$lvalue['line_discount']/100) * ($lvalue['package'] / $lvalue['sale_unit']));*/
							$result[$lvalue['vat_val']]['subtotal']+=return_value($lvalue['quantity'])*
							((return_value($lvalue['price'])-return_value($lvalue['price'])*return_value($lvalue['line_discount'])/100) * ($lvalue['package'] / $lvalue['sale_unit']));
						} else {
							/*$result[$lvalue['vat_val']]['subtotal']+=$lvalue['quantity']*($lvalue['price']-$lvalue['price']*$lvalue['line_discount']/100);*/
							$result[$lvalue['vat_val']]['subtotal']+=return_value($lvalue['quantity'])*(return_value($lvalue['price'])-return_value($lvalue['price'])*return_value($lvalue['line_discount'])/100);
						}

						$result[$lvalue['vat_val']]['discount_value']= $result[$lvalue['vat_val']]['subtotal'] * $global_discount/100;

						/*$result[$lvalue['vat_val']]['net_amount']+= $result[$lvalue['vat_val']]['subtotal'] - $result[$lvalue['vat_val']]['discount_value'];*/
						$result[$lvalue['vat_val']]['net_amount'] = $result[$lvalue['vat_val']]['subtotal'] - $result[$lvalue['vat_val']]['discount_value'];

						if($show_discount){
							$result[$lvalue['vat_val']]['vat_value']=($result[$lvalue['vat_val']]['net_amount'])*($lvalue['vat_val']/100);
						}else{
							/*$result[$lvalue['vat_val']]['vat_value']=$result[$lvalue['vat_val']]['subtotal']*($lvalue['vat_val']/100);*/
							$result[$lvalue['vat_val']]['vat_value'] = return_value($result[$lvalue['vat_val']]['vat_value']) + return_value($lvalue['line_total'])*($lvalue['vat_val']/100);
						}
						$result[$lvalue['vat_val']]['vat_percent']=display_number($lvalue['vat_val']);
						$result[$lvalue['vat_val']]['subtotal']=display_number($result[$lvalue['vat_val']]['subtotal']);
						$result[$lvalue['vat_val']]['vat_value']=display_number($result[$lvalue['vat_val']]['vat_value']);
						$result[$lvalue['vat_val']]['discount_value']=display_number($result[$lvalue['vat_val']]['discount_value']);
						$result[$lvalue['vat_val']]['net_amount']=display_number($result[$lvalue['vat_val']]['net_amount']);
					}
					
				}
			}
		}
		return $result;
	}

	private function setDefaultData()
	{

		$default_group_id = unique_id();
		$default_table_id = unique_id();

		$default_group = array(
			$default_group_id => array(
				'title' => '',
				'show_QC' => true,
				'show_UP' => true,
				'show_PS' => true,
				'show_d' => true,
				'table' => array(
					$default_table_id => array(
						'description' => array(''),
						'quantity' => array(1),
		                'article_code' => array(''),
		                'article_id' => array(''),
						'package' => array(1),
						'sale_unit' => array(1),
						'price' => array(0)
					)
				)
			)
		);

		$default_sort = array($default_group_id => array($default_table_id => 0));

		return array($default_group, $default_sort);
	}

	public function get_articles_list($in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id 
								   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id ';
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_article_categories.name AS family,
						pim_article_categories.name AS categorie,
						pim_articles.supplier_reference,
						pim_articles.price AS unit_price,
						pim_articles.is_service,
						pim_articles.block_discount';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
							   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';

			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.supplier_reference,
						pim_article_categories.name AS family,
						pim_article_categories.name AS categorie,
						pim_articles.price AS unit_price,
						pim_articles.is_service,
						pim_articles.block_discount';
		}

		$filter.=" 1=1 ";

		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_articles.supplier_reference LIKE '%".$in['search']."%' OR pim_article_categories.name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id']){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1'  ORDER BY pim_articles.item_code LIMIT 5");
		
		

		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='CONTRACT_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				if($in['vat_regime_id']){
					if($in['vat_regime_id']<10000){
						if($in['vat_regime_id']==2){
							$vat=0;
						}
					}else{
						$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
							LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
							WHERE vat_new.id='".$in['vat_regime_id']."'");
						if(!$vat_regime){
							$vat_regime=0;
						}
						if($vat>$vat_regime){
							$vat=$vat_regime;
						}
					}			
				}else{
					$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
					if($vat_regime<10000){
						if($vat_regime==2){
							$vat=0;
						}
					}else{
						$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
							LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
							WHERE vat_new.id='".$vat_regime."'");
						if(!$vat_regime){
							$vat=0;
						}
					}
				}			
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		  	if($article->f('is_service') == 1){
		  		$price=$article->f('unit_price');
		        $base_price = $price;
		  	}

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	//'name'						=> htmlspecialchars_decode($article->f('internal_name')),
			  	'name'						=> html_entity_decode(stripslashes($article->f('internal_name')), ENT_QUOTES, 'UTF-8'),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'supplier_reference'		=> $article->f('supplier_reference'),
			    'family'					=> $article->f('family') ? $article->f('family') : '',
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> $article->f('packing')>0 ? remove_zero_decimals($article->f('packing')) : 1,
			  	'code'		  	    		=> $article->f('item_code'),
				//'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
				'price'						=> $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)),
				/*'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),*/
				'base_price'				=> place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
				'allow_stock'               =>ALLOW_STOCK == 1 ? true : false,
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		if(in_array(12,explode(';', $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']])))){
			array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));
		}

		return $articles;
	}

	public function get_taxes($in)
	{
		$taxes = array( 'lines'=>array());
		
	/*	$this->db->query("SELECT pim_articles.vat_id,pim_articles.article_id,vats.value
		                       FROM pim_articles
		                       INNER JOIN vats ON vats.vat_id=pim_articles.vat_id
		                       WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.active='1' ");
		$vat_percent=$this->db->f('value');*/

		$gov_tax=array();
		if(!$in['quantity']){
			$in['quantity']==1;
		}

		$get_article_taxes=$this->db->query("SELECT tax_id FROM pim_articles_taxes WHERE article_id='".$in['article_id']."'");
		while ($get_article_taxes->next()){
			$gov_tax[$get_article_taxes->f('tax_id')]+=$in['quantity'];
		}

		$is_gov_taxes=false;
		$total_gov_taxes=0;
		$i=0;

		if($in['customer_id']){
			$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['customer_id']."'");
			if($vat_regime==2){
			  	$vat_percent=0;
			}
		}

		foreach($gov_tax as $tax_id => $quantity){
			$gov_tax = $this->db->query("SELECT pim_article_tax.* ,pim_article_tax_type.name as type_name, vats.value
			                       FROM pim_article_tax
			                       LEFT JOIN pim_article_tax_type ON pim_article_tax_type.id=pim_article_tax.type_id
			                       LEFT JOIN vats ON vats.vat_id=pim_article_tax.vat_id
			                       WHERE pim_article_tax.tax_id='".$tax_id."'");
			$vat_percent=$gov_tax->f('value');
			if($in['vat_regime_id']){
				$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
						LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
						WHERE vat_new.id='".$in['vat_regime_id']."'");
				if(!$vat_regime){
					$vat_regime=0;
				}
				if($vat_percent>$vat_regime){
					$vat_percent=$vat_regime;
				}
			}
			$vat_value= $gov_tax->f('amount')*($vat_percent/100);

			$linie = array(
				'tr_id'             			=> 'tmp'.$i.strtotime('now'),
				'is_vat'						=> $in['remove_vat'] == 1 ? false : true,
				'tax_id'            			=> $tax_id,
				'tax_for_article_id'			=> $in['article_id'],
				'quantity_old'      			=> $quantity,
				'quantity'          			=> display_number($quantity),
				'percent'           			=> $vat_percent,
				'percent_x'         			=> display_number($vat_percent),
				'vat_value'         			=> $vat_value,
				'vat_value_x'       			=> display_number($vat_value),
				'tax_id'            			=> $tax_id,
				//'tax_name'          			=> $gov_tax->f('type_name'),
				'tax_name'          			=> $gov_tax->f('code'),
				'tax_quantity'      			=> display_number($quantity),
				'disc_val'          			=> display_number(0),
				'price'             			=> $in['exchange']==1 ? display_number_var_dec($gov_tax->f('amount')/return_value($in['ex_rate'])) : display_number_var_dec($gov_tax->f('amount')),
				'price_vat'         			=> $in['exchange']==1 ? display_number(($gov_tax->f('amount')/return_value($in['ex_rate'])) * ($vat_percent/100) ) : display_number($gov_tax->f('amount') * ($vat_percent/100)),
				'line_total'        			=> $in['exchange']==1 ? display_number(($gov_tax->f('amount')/return_value($in['ex_rate'])) * $quantity ) : display_number($gov_tax->f('amount') * $quantity),
				//'tax_code'						=> $gov_tax->f('code'),
				'tax_code'						=> $gov_tax->f('type_name'),
				// 'td_width'						=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
				// 'input_width'					=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
				'hide_disc'						=> $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
				'allow_article_packing' 		=> $in['allow_article_packing'],
				'allow_article_sale_unit'		=> $in['allow_article_sale_unit'],
				'apply_to'						=> $gov_tax->f('apply_to'),
			);
			// $view->assign('allow_article_packing',$in['allow_article_packing'],'tax_line');
			// $view->assign('allow_article_sale_unit',$in['allow_article_sale_unit'],'tax_line');
			$total_gov_taxes+= $gov_tax->f('amount') * $quantity;

			array_push($taxes['lines'], $linie);
			$is_gov_taxes=true;
			// $view->loop('tax_line');
			$i++;
		}
		$taxes['is_gov_taxes'] = $is_gov_taxes;
		$taxes['gov_taxes_value']= display_number($total_gov_taxes);
		// $view->assign(array(
		// 	'td_width'			=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
		// ));
		return $taxes;
	}

	public function getViewContract()
	{
		global $config;

		$in = $this->in;

		$contract_exists = $this->db->field("SELECT contract_id FROM contracts WHERE  contract_id='".$in['contract_id']."'  ");
		if(!$contract_exists){
			msg::error('Contract does not exist','error');
			$in['item_exists']= false;
		    json_out($in);
		}
		$filter = '';
		if(!empty($this->version_id) && is_numeric($this->version_id)){
			$filter = 'AND contracts_version.version_id = '.$this->version_id;
			$ver = $this->db->field("SELECT contract_id FROM contracts_version WHERE  version_id='".$this->version_id."'  ");
			if($ver != $in['contract_id']){
				msg::error('Contract version does not exist','error');
				$in['item_exists']= false;
			    json_out($in);
			}
		}else{
			$filter = 'AND contracts_version.active = 1';
		}

		$data = $this->db->query("SELECT contracts.*, contracts.sent as sent_q,contracts_version.version_code,contracts_version.version_id,contracts_version.active AS version_active,
					contracts_version.version_date, contracts_version.sent as sent, contracts_version.start_date as v_start, 
					contracts_version.end_date as v_end, contracts_version.no_end_date as v_no_end, contracts_version.sent_date as v_sent_date,contracts_version.preview,
					contracts_version.discount as v_discount,contracts.currency_type,contracts_version.postgreen_id
            		FROM contracts
            		INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
            		WHERE contracts.contract_id='".$in['contract_id']."' AND 1=1 ".$filter)->getAll();
		$this->contract = $data[0];
		if(!$this->version_id){
			$this->version_id = $this->contract['version_id'];
		}
		$filter_db2 = '';
		if($this->contract['contact_id'])
		{
			$filter_db2 = " AND customer_contacts.contact_id='".$this->contract['contact_id']."' ";			
		}
		$output = $this->contract;
		
		$u_id = $_SESSION['u_id'];											// $user_credential - 1 (admin)
		if($_SESSION['group']=='admin') {									//                  - 2 (quote_admin)
			$user_credential = 1;											//					- 3 (none => check if is author or creator)
		} else {
			if(in_array('contracts', $_SESSION['admin_sett'])) {
				$user_credential = 2;
			} else {
				$user_credential = 3;
			}
		}
		if($user_credential == 3) {
			if($output['author_id'] == $u_id) {
				$output['is_admin_2'] = true;
			} else {
				$output['is_admin_2'] = false;
			}
		} else {
			$output['is_admin_2'] = true;
		}

		switch ($_SESSION['group']) {
			case 'user':
				$can_revert = false;
				if(in_array(ark::$app, $_SESSION['admin_sett'])){
					$can_revert = true;
				}elseif($this->contract['author_id'] == $_SESSION['u_id']){
					$can_revert = true;
				}
				break;
			case 'admin':
				$can_revert = true;
				break;
			default:
				$can_revert = false;
				break;
		}
		$output['ACC_EMAIL'] = '';
		if($output['acc_manager_id']){
			$output['ACC_EMAIL'] = $this->db_users->field("SELECT email FROM users WHERE user_id='".$output['acc_manager_id']."' ");
		}
		$is_recurring=$this->db->field("SELECT recurring_invoice_id FROM recurring_invoice WHERE contract_id='".$this->contract_id."' ");
		if($is_recurring){
			$rec_frequency=$this->db->query("SELECT frequency,days FROM recurring_invoice WHERE recurring_invoice_id='".$is_recurring."' ");
			$rec_amount=$this->db->field("SELECT SUM(amount) FROM recurring_invoice_line WHERE recurring_invoice_id='".$is_recurring."' ");
			$texts=array(1=>gm('Weekly'),2=>gm('Monthly'),3=>gm('Quarterly'),4=>gm('Yearly'),5=>gm('Custom'));
			switch($rec_frequency->f('frequency')){
				case 1:
					$rec_txt=gm('Weekly');
					break;
				case 2:
					$rec_txt=gm('Monthly');
					break;
				case 3:
					$rec_txt=gm('Quarterly');
					break;
				case 4:
					$rec_txt=gm('Yearly');
					break;
				case 5:
					$rec_txt=gm('Every').' '.$rec_frequency->f('days').' '.gm('days');
					break;
			}
		}

		$contract_date =$contract['version_date'] ? $contract['version_date']*1000 : time()*1000;
		$c_time = $contract['version_date'] ? $contract['version_date'] : time();
		if($in['duplicate_contract_id']){
			$contract_date=  time()*1000;
			$c_time = time();
		}

		$output['item_exists']				= true;
		$output['can_revert']				= $can_revert;
		$output['S_DATE']				= $this->contract['v_start'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['v_start']) : '';
		$output['E_DATE']				= $this->contract['v_no_end'] ? gm('Unlimited') : ($this->contract['v_end'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['v_end']): '') ;
		$output['start_date_txt']			= $this->contract['sent']==1 && $this->contract['v_start'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['v_start']) : '';
		$output['REMINDER_DATE']			= $this->contract['reminder_date'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['reminder_date']) : '';
		$output['unlimited_period']		= $this->contract['v_no_end'] == 1 ? true : false; 
		$output['start_date']		= $this->contract['v_start'] ? $this->contract['v_start']*1000 : '';
		$output['end_date']		= $this->contract['v_end'] ? $this->contract['v_end']*1000 : '';
		$output['sent_date']		= $this->contract['v_sent_date'] ? $this->contract['v_sent_date']*1000 : time()*1000;
		$output['contract_version_date']		= $contract_date;
		$output['sent_date_txt']	= $this->contract['sent']==1 && $this->contract['v_sent_date'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['v_sent_date']) : '';
		$output['contract_version_date_txt'] = $this->contract['version_date'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['version_date']) : ''; 
		$output['status_customer']=$this->contract['status_customer'];
		$output['is_recurring']=$is_recurring ? true : false;
		$output['rec_id']=$is_recurring;
		$output['rec_text']=$is_recurring ? gm('Recurring invoice').':'.$rec_txt.'/'.place_currency(display_number($rec_amount)): '';

		$output['contract_lost'] = '-';
		if($this->contract['lost_id']){
			$output['contract_lost'] = $this->db->field("SELECT name FROM contract_lost_reason WHERE id='".$this->contract['lost_id']."' ");
		}
		$output['is_lost']					= $this->contract['status_customer'] == 1 ? true : false;
		$output['use_weblink']				= defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1 ? true : false;
		$exist_url='';

		if(!$in['include_pdf'])
		{
			$in['include_pdf'] = 1;
		}
		if(defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1){
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$this->contract_id."' AND `type`='c' AND version_id='".$this->version_id."' ");
			if(defined('WEB_INCLUDE_PDF_CONTRACT') && WEB_INCLUDE_PDF_CONTRACT == 0){
				$in['include_pdf'] = 0;
			}
		}
		$output['external_url']				= $this->contract['sent'] == '0' ? false : $exist_url;
		$output['web_url']					= $config['web_link_url'].'?q=';

		$output['contact_name'] 			= $this->db->field("SELECT firstname,lastname,contact_id FROM customer_contacts
																WHERE contact_id='".$this->contract['contact_id']."' ");
		$output['free_field_txt']			= nl2br($this->contract['free_field']);
		$output['invoice_link'] = array('do'=>'invoice-ninvoice','buyer_id'=>$this->contract['customer_id'],'c_contract_id'=>$this->contract['contract_id'],'a_contract_id'=>$this->contract['serial_number'],'currency_type'=>$this->contract['currency_type'],'is_contact'=>$this->contract['is_contact']);

		$ver = $this->db->query("SELECT * FROM contracts_version WHERE contract_id='".$in['contract_id']."' ORDER BY `version_code` ASC  ");
		$output['versions'] = array();
		while($ver->next()){
			$line = array(
				'VERSION_CODE'	=> $ver->f('version_code'),
				'version_id'	=> $ver->f('version_id'),
				'active'	    => $ver->f('active') == 1 ? ($this->contract['status_customer'] > 0 ? ($this->contract['status_customer'] == 1 ? 'lost' : 'active') : ($ver->f('sent') == 1 ? 'sent' : '')) : ($ver->f('sent') == 1 ? 'sent' : ''),
				'selected'		=> $ver->f('version_id') == $this->version_id ? 'selected' : '',
				'VERSION_LINK'  => ('index.php?do=contract-contract&contract_id='.$ver->f('contract_id').'&version_id='.$ver->f('version_id')),
				);
			array_push($output['versions'],$line);			
		}
		$output['WonOrLost']	    = gm('Won');
		$output['lost']				= false;
		if($this->contract['status_customer'] == 0){
			if($this->contract["sent"] == 0){
				$output['selectedDraft']	    ='selected';
			}else{
				$output['selectedSent']	    	='selected';
			}
		}else{		
			$output['selectedWon']	    		='selected';
			if($this->contract['status_customer'] == 1){
				$output['WonOrLost']	    = gm('Lost');
				$output['lost']				= true;				
			}
		}

		if($this->contract['pdf_layout'] && $this->contract['use_custom_template']==0){
			$in['logo'] = $this->contract['pdf_logo'];
			$link_end='&type='.$this->contract['pdf_layout'].'&logo='.$this->contract['pdf_logo'];
			$in['template_type'] = $this->contract['pdf_layout'];
		}elseif($this->contract['pdf_layout'] && $this->contract['use_custom_template']==1){
			$link_end = '&custom_type='.$this->contract['pdf_layout'].'&logo='.$this->contract['pdf_logo'];
			$in['template_type'] = $this->contract['pdf_layout'];
			$in['use_custom'] = 1;
		}else{
			if(ACCOUNT_CONTRACT_PDF_FORMAT){
				$link_end = '&type='.ACCOUNT_CONTRACT_PDF_FORMAT;
				$in['template_type'] = ACCOUNT_CONTRACT_PDF_FORMAT;
			}else{
				$link_end = '&type=1';
				$in['template_type'] = 1;
			}	
		}
		#if we are using a customer pdf template
		/*if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $this->quote['pdf_layout'] == 0){
			$link_end = '&custom_type='.ACCOUNT_CONTRACT_PDF_FORMAT;
			$in['use_custom'] = 1;
		}*/
		$output['PDF_LINK2']='index.php?do=contract-print&contract_id='.$this->contract_id.'&version_id='.$this->version_id.'&lid='.$this->contract['email_language'].'&upload='.($in['redo_img'] === true ? 0 : $this->contract['preview']).$link_end;
		$output['pdf_link']='index.php?do=contract-print&contract_id='.$this->contract_id.'&version_id='.$this->version_id.'&lid='.$this->contract['email_language'].$link_end;

		$in['customer_id'] = $this->contract['customer_id'];
		$in['contact_id'] = $this->contract['contact_id'];
		$output['recipients'] = $this->get_recipients($in);
		$output['post_contract']= $this->contract['postgreen_id']!='' ? true : false;

		# email #
		$email_q = $this->db->query("SELECT customer_contacts.firstname,customer_contacts.lastname,
            SUM(contract_line.amount) AS total
            FROM contracts
            LEFT JOIN contract_line ON contract_line.contract_id=contracts.contract_id
            LEFT JOIN customer_contacts ON customer_contacts.customer_id=contracts.customer_id
            WHERE contract_line.use_recurring = 0 AND contracts.contract_id='".$this->contract_id."' ".$filter_db2)->getAll();

		$message_data=get_sys_message('contract_invmess',$this->contract['email_language']);
		/*console::log($message_data,$this->quote['email_language']);*/
		$subject=$message_data['subject'];

		## email ##
		$amount = 0;
		$amount_purchase_price = 0;
		$amount_data = $this->db->query("SELECT * FROM contract_line WHERE contract_id='".$this->contract_id."' AND version_id='".$this->version_id."' AND show_block_total='0' AND use_recurring = 0");
		while($amount_data->next()){
			$amount += $amount_data->f('amount');
			if($amount_data->f('article_id')>0){
				$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id = '".$amount_data->f('article_id')."' AND base_price = '1' ");
				$line_purchase_price = $purchase_price*$amount_data->f('quantity')*$amount_data->f('package')/$amount_data->f('sale_unit');
				$amount_purchase_price+=$line_purchase_price;
			}
		}
		if($this->contract['discount'] && $this->contract['apply_discount'] > 1){
			$amount = $amount - ($amount*$this->contract['discount']/100);
		}
		$amount_margin = $amount-$amount_purchase_price;
		$amount2 = 0;
		$amount2_margin = 0;
		if($this->contract['currency_rate']){
			$amount2 = $amount*return_value($this->contract['currency_rate']);
			$amount2_margin = $amount_margin*return_value($this->contract['currency_rate']);
		}
		
		//$external_web_link = '<a href="'.$config['web_link_url'].'?q='.$exist_url.'">'.$config['web_link_url'].'?q='.$exist_url.'</a>';
		
		$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
		$subject=str_replace('[!SERIAL_NUMBER!]',$this->contract['serial_number'], $subject);
		$subject=str_replace('[!CONTRACT_SERIAL_NUMBER!]',$this->contract['serial_number'].' ['.$this->contract['version_code'].']', $subject);
		$subject=str_replace('[!START_DATE!]',date(ACCOUNT_DATE_FORMAT,$this->contract['v_start']), $subject);
		$subject=str_replace('[!END_DATE!]',($this->contract['v_end'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['v_end']): ''), $subject);
		$subject=str_replace('[!CUSTOMER!]',get_customer_name($this->contract['customer_id']), $subject);
		$subject=str_replace('[!SUBTOTAL!]',place_currency(display_number($email_q[0]['total']),get_commission_type_list($this->contract['currency_type'])), $subject);
		$subject=str_replace('[!DISCOUNT!]',$this->contract['v_discount'].'%', $subject);
		$subject=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discountValue),get_commission_type_list($this->contract['currency_type'])), $subject);
		//$subject=str_replace('[!VAT!]',$this->contract['vat'].'%', $subject);
		$subject=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),get_commission_type_list($this->contract['currency_type'])), $subject);
		$subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount),get_commission_type_list($this->contract['currency_type'])), $subject);
		$subject=str_replace('[!CONTACT_FIRST_NAME!]',$email_q[0]['firstname'], $subject);
		$subject=str_replace('[!CONTACT_LAST_NAME!]',$email_q[0]['lastname'], $subject);
		$subject=str_replace('[!SUBJECT!]',$this->contract['subject'], $subject);

		$title_cont = '';
		if(isset($email_q[0]['title_id']) && !empty($email_q[0]['title_id'])){
			$title_cont = $this->db->field("SELECT name FROM customer_contact_title WHERE id='".$email_q[0]['title_id']."'  ");	
		}

		$body=$message_data['text'];
		$body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
		$body=str_replace('[!SERIAL_NUMBER!]',$this->contract['serial_number'], $body);
		$body=str_replace('[!CONTRACT_SERIAL_NUMBER!]',$this->contract['serial_number'].' ['.$this->contract['version_code'].']', $body);
		$body=str_replace('[!CONTRACT_START_DATE!]',date(ACCOUNT_DATE_FORMAT,$this->contract['v_start']), $body);
		$body=str_replace('[!CONTRACT_END_DATE!]',($this->contract['v_end'] ? date(ACCOUNT_DATE_FORMAT,$this->contract['v_end']): ''), $body);
		$body=str_replace('[!CUSTOMER!]',get_customer_name($this->contract['customer_id']), $body);
		$body=str_replace('[!SUBTOTAL!]',place_currency(display_number($email_q[0]['total']),get_commission_type_list($this->contract['currency_type'])), $body);
		$body=str_replace('[!DISCOUNT!]',$this->contract['v_discount'].'%', $body);
		$body=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discountValue),get_commission_type_list($this->contract['currency_type'])), $body);
		//$body=str_replace('[!VAT!]',$this->contract['vat'].'%', $body);
		$body=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),get_commission_type_list($this->contract['currency_type'])), $body);
		$body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount),get_commission_type_list($this->contract['currency_type'])), $body);
		$body=str_replace('[!CONTACT_FIRST_NAME!]',$email_q[0]['firstname'], $body);
		$body=str_replace('[!CONTACT_LAST_NAME!]',$email_q[0]['lastname'], $body);
		$body=str_replace('[!SUBJECT!]',$this->contract['subject'], $body);
		$body=str_replace('[!SALUTATION!]',$title_cont, $body);

		$mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='contract_email_type' ");
	 	if($mail_sender_type == 1){
					if($message_data['use_html']){
	  					 $body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);
					}else{
		 				 $body=str_replace('[!SIGNATURE!]','', $body);
	 		}
	    }
	    $account_manager_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
	    $account_user_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$this->contract['acc_manager_id']."'");
		$output['use_html'] = $message_data['use_html'];
		
		if($in['contact_id']){
			if($in['customer_id']){
				$contact_email = $this->db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND customer_id = '".$in['customer_id']."'");
			}else{
				$contact_email = $this->db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."'");
			}
			if($contact_email){
				$c_email = $contact_email;
			}else{
				$customer_email = $this->db->field("SELECT c_email FROM customers WHERE customer_id='".$in['customer_id']."'");
				if($customer_email){
					$c_email = $customer_email;
				}
			}
		}
		$output['email'] = array(
			'e_subject'			=> $subject,
			'dropbox_files'		=> '',
			'language_email' 	=> $this->contract['email_language'],
			'e_message' 		=> $in['e_message'] ? $in['e_message'] : ($body),
			'lid'				=> $this->contract['email_language'],
			'include_pdf'		=> $in['include_pdf'] ? true : false,
			'id'				=> $this->contract_id,
			'mark_as_sent'		=> $output['selectedDraft']	=='selected' ? '1':'0',
			'version_id'		=> $this->version_id,
			'files'				=> array(),
			'use_html'			=> $message_data['use_html'],
			'serial_number'		=> $this->contract['serial_number'],
			/*'recipients'		=> array_map(function($field){
										return $field['id'];
									},$output['recipients']),*/
			'recipients'		=> $c_email,
			'save_disabled'		=> $c_email? false:true,
			'alerts'			=> array(),
			'user_loged_email'	=> $account_manager_email,
			'user_acc_email'	=> $account_user_email,
// mail la userul logat.
		);

		//attached files
		$output['is_file'] = false;
		
		$f = $this->db->query("SELECT * FROM attached_files WHERE type='5'");
		while ($f->next()) {
			$output['email']['files'][]=array(
				'file'		=> $f->f('name'),
				'checked'	=> $f->f('default') == 1 ? true : false,
				'file_id'	=> $f->f('file_id'),
				'path'		=> $f->f('path'),
				);
			$output['is_file'] = true;
		}

		$output['is_admin']			= $_SESSION['access_level'] == 1 ? true : false;
		$output['id_contract']			= $in['contract_id'];
		$output['language_dd']		= build_pdf_language_dd($this->contract['email_language'],1);
		$output['show_margin']		= true;
		$output['total']			= place_currency(display_number($amount),get_commission_type_list($this->contract['currency_type']));
		$output['total2']			= place_currency(display_number($amount2));
		$output['is_total2']		= $amount2 ? true : false;
		$output['total_margin']		= place_currency(display_number($amount_margin),get_commission_type_list($this->contract['currency_type']));
		$output['total2_margin']	= place_currency(display_number($amount2_margin));
		$output['is_total2_margin']	= $amount2_margin ? true : false;
		$output['default_currency']	= currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		$output['can_do'] 			= $this->db->query("SELECT contract_line.*, contracts_version.active FROM contract_line
									   INNER JOIN contracts_version ON contract_line.version_id = contracts_version.version_id
									   WHERE contract_line.contract_id='".$this->contract_id."' AND contract_line.content_type='1' AND contracts_version.active='1' AND contract_line.article_id='0' AND use_recurring = 0 ORDER BY id ASC  ")->records_count();
		/*$output['LOST_DD']          = build_data_dd('tblquote_lost_reason',$this->quote['lost_id'],'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));*/
		$output['LOST_QQ']			= build_data_cc();
		$ref = '';

		$output['contract_nr'] 	    = $ref.$this->contract['serial_number'];	
		$output['account_manager']  = get_user_name($this->contract['acc_manager_id']);
		$output['order_author']  = get_user_name($this->contract['author_id']);
		$output['buyer_name']	= $this->contract['customer_name'];
		$output['contact_name']	= get_contact_name($this->contract['contact_id']);
		$buyer_data=$this->db->query("SELECT * FROM customers WHERE customer_id='".$this->contract['customer_id']."' ");
		$output['buyer_email']=$buyer_data->f('c_email');
		$output['buyer_phone']=$buyer_data->f('comp_phone');
		$output['buyer_fax']=$buyer_data->f('comp_fax');

	      $drop_info = array('drop_folder' => 'contracts', 'customer_id' => $this->contract['customer_id'], 'item_id' => $in['contract_id'], 'isConcact' => $this->contract['is_contact'], 'serial_number' => $this->contract['serial_number']);
		$output['is_drop']		= defined('DROPBOX') && (DROPBOX != '') ? true : false;
		$output['drop_info']		= $drop_info;

		$output['service_link']	= array('buyer_id'=>($this->contract['customer_id'] ? $this->contract['customer_id'] : $this->contract['contact_id']),'source_id'=>$this->contract['source_id'],'type_id'=>$this->contract['type_id'],'customer'=> $this->contract['customer_name'],'contract_id'=>$in['contract_id'],'currency_type'=>$this->contract['currency_type'],'languages'=>$this->contract['email_language'],'acc_manager_id'=>$this->contract['acc_manager_id'],'identity_id'=>$this->contract['identity_id'], 'segment_id'=>$this->contract['segment_id']);
		$output['recurring_service_link']	= array('buyer_id'=>($this->contract['customer_id'] ? $this->contract['customer_id'] : $this->contract['contact_id']),'source_id'=>$this->contract['source_id'],'type_id'=>$this->contract['type_id'],'customer'=> $this->contract['customer_name'],'contract_id'=>$in['contract_id'],'currency_type'=>$this->contract['currency_type'],'languages'=>$this->contract['email_language'],'acc_manager_id'=>$this->contract['acc_manager_id'],'is_recurring'=>true,'identity_id'=>$this->contract['identity_id'], 'segment_id'=>$this->contract['segment_id']);
		
		if($this->contract['segment_id']){
			$output['segment'] = $this->db->field("SELECT name FROM tblquote_segment WHERE id='".$this->contract['segment_id']."' ");
			$output['segment_id']= $this->contract['segment_id'];
		}
		if($this->contract['source_id']){
			$output['source'] = $this->db->field("SELECT name FROM tblquote_source WHERE id='".$this->contract['source_id']."' ");
			$output['source_id']= $this->contract['source_id'];
		}
		if($this->contract['type_id']){
			$output['xtype'] = $this->db->field("SELECT name FROM tblquote_type WHERE id='".$this->contract['type_id']."' ");
			$output['type_id']= $this->contract['type_id'];
		}
		$output['source_dd']					= get_categorisation_source();	
		$output['type_dd']					= get_categorisation_type();
		$output['segment_dd']= get_categorisation_segment();

		$output['is_invoice']=in_array('4', perm::$allow_apps) ? true : false;
		$output['is_service']=in_array('13', perm::$allow_apps) ? true : false;

		//subscription
		global $apps;
		for ($i = 1; $i<=15; $i++){
		    if(in_array($i,perm::$allow_apps)){
		    	if($i==3){
		        	$is_3 = 1;
		       	}
		    	if($i==4){
		    		$is_4 = 1;
		    	}
		    	if($i==13){
		    		$is_13 = 1;
		    	}
		    	if($i==6){
		    		$is_6 = 1;
		    	}
		    }
		}

        $output['is_3']= $is_3 == 0 ? false : true;
        $output['is_4']= $is_4 == 0 ? false : true;
	    $output['is_13']= $is_13 == 0 ? false : true;
	    $output['is_6']= $is_6 == 0 ? false : true;

		$mail_setting_option = $this->db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
		$output['sendgrid_selected'] = false;
		if($mail_setting_option==3){
			$output['sendgrid_selected'] =true;
		}

		if($this->contract['postgreen_id']!=''){  	
	    	include_once('apps/contract/model/contract.php');
			$postg=new contract();
			$in['version_id']=$this->version_id; 
			$post_status=json_decode($postg->postContractData($in));
	    	$output['postg_status']	= $post_status->status;
	    }
		$post_active=$this->db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
		$output['post_active']= !$post_active || $post_active==0 ? false : true;
		$post_library=array();
		if($post_active){
		    $in['ret_res']=true;
		    $post_library=include_once('apps/misc/controller/post_library.php');
		}
    		$output['post_library']=$post_library;

		$this->out = $output;

	}

	public function get_recipients($in)
	{
		$q = strtolower($in["term"]);
		if($in['customer_id'] && !$in['contact_id']){

		}else{
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}



		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['customer_id']){
			$filter .= " AND customers.customer_id='".$in['customer_id']."'";
		}
		if($in['contact_id']){
			$filter .= " AND customer_contacts.contact_id='".$in['contact_id']."'";
		}

		$items = array();
		$items2 = array();
		if($in['customer_id'] && $in['contact_id']){
			/*$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ORDER BY lastname ")->getAll();*/
			$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$in['customer_id']."' ORDER BY lastname ")->getAll();
		}else{
			//$contacts='';
			$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$in['customer_id']."' ORDER BY lastname ")->getAll();
		}
		if($in['customer_id'] && !$in['contact_id']){
			$customer = $this->db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE 1=1 $filter ");
		}else{
			$customer = $this->db->query("SELECT customers.name as name,customers.c_email as email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ");
		}
		$customerEmail = $customer->f('email');
		if(!empty($customerEmail)){
			$items2 = array(
				    'id' => $customer->f('email'), 
				    'label' => $customer->f('name'),
				    'value' => $customer->f('name').' - '.$customer->f('email'),
			);
		}


		foreach ($contacts as $key => $value) {

			if($value['name']){
				$name = $value['firstname'].' '.$value['lastname'].' > '.$value['name'];
			}else{
				$name = $value['firstname'].' '.$value['lastname'];
			}
			if($in['only_contact_name']){
				$name = $value['firstname'].' '.$value['lastname'];
			}
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	else{
		  		$price_category = "1";
		  	}
		  	$contactEmail = $value['email'];
			if(!empty($contactEmail)){
				$items[] = array(
				    'id' => $value['email'], 
				    'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				    'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)).' - '.$value['email'],
				);
			}
		}
		$added = false;
		foreach ($items as $key => $value) {
			if($value['id'] == $items2['id']){
				$added = true;
			}
		}
		if(!$added){
			array_push($items, $items2);
		}
		// $items['name'].= $customer; 
		return $items;
	}

	public function get_pdfSettings($in)
	{
		$result = array( 'logo'=>array());
		$tblinvoice = $this->db->query("SELECT pdf_logo, pdf_layout, email_language, identity_id FROM contracts WHERE contract_id='".$in['contract_id']."'");
		$tblinvoice->next();

		$result['contract_id']=$in['contract_id'];
		$result['version_id']=$in['version_id'];
	    $set_logo = $tblinvoice->f('pdf_logo');
	    if(!$set_logo){
	        $set_logo = ACCOUNT_LOGO_CONTRACT;
	    }

		if(ACCOUNT_LOGO_CONTRACT == '') {
		    array_push($result['logo'], array(
	            'account_logo'=>'images/no-logo.png',
	            'default'=>'images/no-logo.png'
	        ));
		}
		else {
		    $patt = '{c_logo_img_}';
		    $search = strpos(ACCOUNT_LOGO_CONTRACT,'c_logo_img_');
		    if($search === false){
		        $patt = '{'.str_replace('upload/'.DATABASE_NAME.'/','',ACCOUNT_LOGO_CONTRACT).',c_logo_img_}';
		    }
		    $logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/'.$patt.'*',GLOB_BRACE);
		    foreach ($logos as $v) {
		        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
		        $size = getimagesize($logo);
		        $ratio = 250 / 77;
		        if($size[0]/$size[1] > $ratio ){
		            $attr = 'width="250"';
		        }else{
		            $attr = 'height="77"';
		        }
		        
		        array_push($result['logo'], array(
	                'account_logo'=>$logo,
	                'default'=>$set_logo,
	                'attr' => $attr
	            ));
		    }
		}

		if(!$tblinvoice->f('pdf_layout')) {
		  $selected=defined('ACCOUNT_CONTRACT_PDF_FORMAT') && ACCOUNT_CONTRACT_PDF_FORMAT ? ACCOUNT_CONTRACT_PDF_FORMAT : '1';
		}else{
		 $selected=$tblinvoice->f('pdf_layout');

		 if($tblinvoice->f('use_custom_template')==1){
		    $selected=$tblinvoice->f('pdf_layout')+5;
		 }

		}

		$lng = $tblinvoice->f('email_language');

		if(!$lng){ $lng = 1; }

		if(!$tblinvoice->f('identity_id')) {
		  $selected_id='0';
		}else{
		 $selected_id=$tblinvoice->f('identity_id');
		}
		$link_end = '&type=1';
		$result['pdf_link']= 'index.php?do=contract-print&contract_id='.$in['contract_id'].'&lid='.$tblinvoice->f('email_language').$link_end;
		$result['language_dd']          = build_language_admin_dd($lng);
		$result['language_id']          = $lng;
		$result['pdf_type_dd']          = build_pdf_type_dd($selected,false,false,false,true);
		$result['pdf_type_id']          = $selected;
		$result['multiple_identity']    = build_identity_dd($selected_id);
		$result['multiple_identity_id'] = $selected_id;
		$result['identity_id']          = $tblinvoice->f('identity_id');
		$result['do_next']              = 'contract-contract-contract-pdf_settings';
		$result['default_logo']         = $set_logo;
		$result['xget']         		= 'pdfSettings';		

		return $result;
	}

	public function get_author($in)
	{
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}
		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) 
										 ORDER BY users.first_name ASC");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>htmlspecialchars_decode(stripslashes($db_users->f('first_name').' '.$db_users->f('last_name'))) ) );
		}
		return $items;
	}

	public function get_accmanager($in)
	{
		$q = strtolower($in["term"]);		

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}

		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
											FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
											WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>htmlspecialchars_decode(stripslashes($db_users->f('first_name').' '.$db_users->f('last_name'))) ) );
		}
		return $items;
	}

	public function get_email_preview($in)
	{
		global $config;
		if(!$in['contract_id'])
		{
			return '';
		}
		$tblinvoice = $this->db->query("SELECT * FROM contracts WHERE contract_id='".$in['contract_id']."'  ");
		if(!$tblinvoice->next()){
			return '';			
		}
		$in['weblink_url'] = $this->external_id($in);
		$in['message'] = stripslashes($in['message']);
		
		$e_lang = $tblinvoice->f('email_language');
		if(!$e_lang || $e_lang > 4){
			$e_lang=1;
		}

		$text_array = array('1' => array('simple' => array('1' => 'Your offer', '2'=> 'Check your offer by clicking on the above link','3'=>"Following this link",'4'=>'Web Link'),
        								   'pay' => array('1' => 'INVOICE WEB LINK', '2'=> 'Check your offer by clicking on the above link.','3'=>"HERE")
        								   ),
        					'2' => array('simple' => array('1' => 'Votre Offre', '2'=> 'Visualisez votre offre en cliquant sur le lien ci-dessus','3'=>'Cliquez sur ce lien','4'=>'Lien web'),
        								   'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Vous pouvez tÃ©lÃ©charger votre facture ICI et la payer en ligne','3'=>'ICI')
        								   ),
        					'3' => array('simple' => array('1' => 'Offerte', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'Via deze link','4'=>'Weblink'),
        								   'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'HIER')
        								   ),
        					'4' => array('simple' => array('1' => 'ANGEBOT WIE WEB-LINK', '2'=> 'Sie kÃ¶nnen Ihr Angebot HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
        								   'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie kÃ¶nnen Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER')
        								   )
        );

		$contact = $this->db->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'  ");
		$title_cont = $this->db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'  ");
		$customer = $this->db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('customer_id')."'  ");

		$in['message']=str_replace('[!CONTACT_FIRST_NAME!]', "".$contact->f('firstname')."", $in['message']);
		$in['message']=str_replace('[!CONTACT_LAST_NAME!]', "".$contact->f('lastname')."", $in['message']);
		$in['message']=str_replace('[!SALUTATION!]', "".$title_cont."", $in['message']);
		// $in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_CONTRACT."\">",$in['message']);
		

		if($in['use_html']){
			if(defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1){
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['contract_id']."' AND `version_id`='".$in['version_id']."' AND `type`='c'  ");
				
		    	$extra = "<p style=\"background: #e6e6e6; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #e6e6e6; color:#868d91; border-radius:8px;\">";
	        	
	    		$extra .="<b style=\"color: #5199b7; text-align:center; font-size:32px;\"><a style=\"text-decoration:none; text-transform: lowercase; color:#6399c6;\" href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b><br />";
	        	
	    		$extra .=$text_array[$e_lang]['simple']['2']."<br /></p>";
				$in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_CONTRACT."\" alt=\"\">",$in['message']);
		    	// $in['message'] .=$extra;
		        $in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
		    	$in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $in['message']);
		    }else{
		        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
		        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
		    }
		    if(defined('DROPBOX') && DROPBOX != ''){
		        if(strpos($in['message'], '[!DROP_BOX!]')){
		            $in['message']=str_replace('[!DROP_BOX!]', '', $in['message']);
		            $id = $tblinvoice->f('customer_id');
		            $is_contact = 0;
		            if(!$id){
		                $id = $tblinvoice->f('contact_id');
		                $is_contact = 1;
		            }
		            $d = new drop('contracts', $id, $in['contract_id'],true,'',$is_contact, $tblinvoice->f('serial_number'));
		            $files = $d->getContent();
		            if(!empty($files->entries)){
		                $in['message'] .="<br><br>";
		                foreach ($files->entries as $key => $value) {
		                    $l = $d->getLink(urldecode($value->path_display));
		                    $link = $l;
		                    $file = $tblinvoice->f('serial_number').'/'.str_replace('/','_', $value->path_display);
		                    $in['message'].="<p><a href=".$link.">".$file."</a></p>";
		                }
		            }
		        }
		    }
		}else{
			if(defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1){
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['invoice_id']."' AND `type`='i'  ");
				
			   	$extra = "\n\n-----------------------------------";
			   	$extra .="\n".$text_array[$e_lang]['simple']['1'];
			   	$extra .="\n-----------------------------------";
			   	$extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
			   	
			   	$in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
		        
		        $in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $in['message']);
			}else{
		        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
		        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
		    }
		    if(defined('DROPBOX') && DROPBOX != ''){
		        if(strpos($in['message'], '[!DROP_BOX!]')){
		            $in['message']=str_replace('[!DROP_BOX!]', '', $in['message']);
		            $id = $tblinvoice->f('customer_id');
		            $is_contact = 0;
		            if(!$id){
		                $id = $tblinvoice->f('contact_id');
		                $is_contact = 1;
		            }
		            $d = new drop('contracts', $id, $in['contract_id'],true,'',$is_contact, $tblinvoice->f('serial_number'));
		            $files = $d->getContent();
		            if(!empty($files->entries)){
		                $in['message'] .="\n\n";
		                foreach ($files->entries as $key => $value) {
		                    $l = $d->getLink(urldecode($value->path_display));
		                    $link = $l;
		                    $file = $tblinvoice->f('serial_number').'/'.str_replace('/','_', $value->path_display);
		                    $in['message'].="<p><a href=\"".$link."\">".$file."</a></p>\n";
		                }
		            }
		        }
		    }
		}

		
		$result = array( 'e_message' => $in['message'] );
		
		return $result;
	}

	function external_id(&$in)
	{
		if(defined('USE_CONTRACT_WEB_LINK') && USE_CONTRACT_WEB_LINK == 1){
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :t AND version_id= :version_id ",['d'=>DATABASE_NAME,'item_id'=>$in['contract_id'],'t'=>'c','version_id'=>$in['version_id']]);
			if(!$exist_url){
				$url = generate_chars();
				while (!isUnique($url)) {
					$url = generate_chars();
				}
				$this->db_users->insert("INSERT INTO urls SET `url_code`= :url_code, `database`= :d, `item_id`= :item_id, `type`= :t, version_id=:version_id ",['url_code'=>$url,'d'=>DATABASE_NAME,'item_id'=>$in['contract_id'],'t'=>'c','version_id'=>$in['version_id']]);
				$exist_url = $url;
			}
			return $exist_url;
		}
	}

	public function get_installations(&$in){
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND name LIKE '".$q."%'";
		}
		$installation=$this->db->query("SELECT * FROM installations 
			WHERE customer_id='".$in['buyer_id']."' $filter ORDER BY name ASC");
		$items = array();
		while($installation->next()){
			array_push( $items, array('installation_id'=>$installation->f('id'),'name'=>stripslashes($installation->f('name')),'serial_number'=>$installation->f('serial_number')));
		}
		if($q){
	        array_push($items,array('installation_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	    }else{
	        array_push($items,array('installation_id'=>'99999999999','name'=>''));
	    }
		return $items;
	}

	public function get_deals(&$in){
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND subject LIKE '".$q."%'";
		}
		$deals=$this->db->query("SELECT * FROM tblopportunity 
			WHERE buyer_id='".$in['buyer_id']."' $filter ORDER BY subject ASC");
		$items = array();
		while($deals->next()){
			array_push( $items, array('deal_id'=>$deals->f('opportunity_id'),'name'=>stripslashes($deals->f('subject'))));
		}
		return $items;
	}

}

	$contract = new ContractEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $contract->output($contract->$fname($in));
	}

	$contract->getContract();
	$contract->output();

?>