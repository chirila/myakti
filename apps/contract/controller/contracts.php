<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    if(function_exists($fname)){
	    	json_out($fname($in));
	    }else{
	    	msg::error('Function does not exist','error');
	    	json_out($in);
	    }
	}

	$result=array('query'=>array(), 'nav'=>array());
	$l_r = ROW_PER_PAGE;
	$result['lr']=$l_r;
	$order_by_array=array('iii','customer_name','serial_number','start_date','end_date','subject','amount');

	$beginOfDay = strtotime("midnight", strtotime('now'));
	$endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$order_by =" ORDER BY iii ";
	$filter = ' 1=1 ';
	if($in['customer_id']){
		$filter .=" AND contracts.customer_id='".$in['customer_id']."' ";
	}
	if(!$in['archived'] || $in['archived']=='0'){
		$filter.= " AND contracts.active=1 ";
	}else{
		$filter.= " AND contracts.active=0 ";
	}
	$views .= " AND (contracts.closed='0' OR contracts.closed='1') ";

	if($in['view']==1){
		$views = " AND contracts.sent='0' ";
		$order_by =" ORDER BY iii DESC ";
	}
	if($in['view']==2){
		$views = " AND contracts.sent='1' && contracts.status_customer='0' ";
		$order_by =" ORDER BY iii DESC ";
	}
	if($in['view']==3){
		$views = " AND contracts.sent='1' && contracts.status_customer='1' ";
		$order_by =" ORDER BY iii DESC ";
	}
	if($in['view']==4){
		$views = " AND contracts.sent='1' && contracts.status_customer='2' ";
		$order_by =" ORDER BY iii DESC ";
	}
	if($in['view']==5){
		$views = " AND contracts.closed='1' ";
		$order_by =" ORDER BY iii DESC ";
	}

	$filter = $filter.$views;

	if(!empty($in['search'])){
		$filter .= " AND (contracts.customer_name LIKE '%".$in['search']."%' OR contracts.serial_number LIKE '%".$in['search']."%' OR contracts.subject like '%".$in['search']."%' )";
	}

	 $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

	if(!empty($in['order_by'])){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
				$order = " DESC ";
			}
			if($in['order_by']=='iii'){
				$in['order_by']="serial_number";
			}
			if($in['order_by']=='amount' ){
				$filter_limit =' ';
	    		$order_by ='';
		    }else{
		       $order_by =" ORDER BY ".$in['order_by']." ".$order;
		       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		        $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
		    }		
		}
	}
	$prefix_lenght=strlen(ACCOUNT_CONTRACT_START)+1;

	$result['nav'] =  $db->query("SELECT contracts.contract_id , CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii FROM contracts
						INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
						WHERE ".$filter." AND contracts_version.active='1'  GROUP BY contracts.contract_id ".$order_by)->getAll();
	$result['max_rows']=count($result['nav']);

	$contracts=$db->query("SELECT contracts.name, contracts.closed,contracts.contract_id,contracts.customer_name, contracts_version.start_date,contracts_version.end_date,contracts_version.no_end_date,contracts.close_reason,
			contracts.invoicing_type, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii, serial_number,contracts.active,contracts.currency_rate,contracts.subject,
			contracts.pdf_layout,contracts.pdf_logo,contracts.email_language,contracts.status_customer,
			contracts_version.version_id,contracts_version.discount,contracts_version.apply_discount,contracts_version.version_code,contracts_version.sent,contracts_version.postgreen_id FROM contracts
			INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id	
			WHERE ".$filter." AND contracts_version.active='1' GROUP BY contracts.contract_id ".$order_by.$filter_limit );
	while($contracts->move_next() ){
		$ref = "";
		if(ACCOUNT_CONTRACT_REF && $contracts->f('our_reference')){
			$ref = $contracts->f('our_reference');
		}
		$status='';
		if(!$contracts->f('sent')){
			$status=gm('Draft');
		}else{
			if($contracts->f('status_customer')==0){
				//$status=gm('Active');
				$status=gm('Sent');
			}else if($contracts->f('status_customer')==1){
				$status=gm('Rejected');
			}else if($contracts->f('status_customer')==2){
				$status=gm('Active');
			}else{
				$status=gm('Closed');
			}
		}
		if($contracts->f('closed')==1){
			$status=gm('Closed');
		}

		$amount = $db->field("SELECT SUM(amount) FROM contract_line WHERE contract_id='".$contracts->f('contract_id')."' AND version_id='".$contracts->f('version_id')."' AND show_block_total='0' AND use_recurring = 0 ");

		if($contracts->f('discount') && $contracts->f('apply_discount') > 1){
			$amount = $amount - ($amount*$contracts->f('discount')/100);
		}

		if($contracts->f('currency_rate')){
			$amount = $amount*return_value($contracts->f('currency_rate'));
		}

		$link_end = '&type=1';
		if($contracts->f('pdf_layout') && $contracts->f('use_custom_template')==0){
			$link_end='&type='.$contracts->f('pdf_layout').'&logo='.$contracts->f('pdf_logo');
		}elseif($contracts->f('pdf_layout') && $contracts->f('use_custom_template')==1){
			$link_end = '&custom_type='.$contracts->f('pdf_layout').'&logo='.$contracts->f('pdf_logo');
		}
		#if we are using a customer pdf template
		if(defined('USE_CUSTOME_CONTRACT_PDF') && USE_CUSTOME_CONTRACT_PDF == 1 && $contracts->f('pdf_layout') == 0){
			$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT.'&logo='.$contracts->f('pdf_logo');
			$in['use_custom'] = 1;
		}
		$lng = 1;
		if($contracts->f('email_language')){
			$lng = $contracts->f('email_language');
		}

		$c_version_nr = $db->field("SELECT count(version_id) FROM contracts_version WHERE contract_id='".$contracts->f('contract_id')."' ");
		$show_expand = ($c_version_nr >= 2) ? true : false;

		$item=array(
			'NAME'					=> $contracts->f('name'),
		  	'DELETE_LINK'			=> array('do'=>'contract-contracts-contract-delete','customer_id'=>$in['customer_id'],'contract_id'=>$contracts->f('contract_id')),
		  	'ARCHIVE_LINK'			=> array('do'=>'contract-contracts-contract-archive','customer_id'=>$in['customer_id'],'contract_id'=>$contracts->f('contract_id')),
		  	'ACTIVATE_LINK'			=>  array('do'=>'contract-contracts-contract-activate','customer_id'=>$in['customer_id'],'contract_id'=>$contracts->f('contract_id')),
		  	'CUSTONER'				=> $contracts->f('customer_name'),
		  	'CONTRACT_NUMBER'		=> $ref.$contracts->f('serial_number'),
		  	'CONTRACT_DATE'			=> $contracts->f('start_date') ? date(ACCOUNT_DATE_FORMAT,$contracts->f('start_date')) : '',
		  	'CONTRACT_END_DATE'		=> $contracts->f('end_date') ? date(ACCOUNT_DATE_FORMAT,$contracts->f('end_date')) : '',
		  	'status_customer'			=> $contracts->f('status_customer'),
		  	'active'				=> $contracts->f('active'),
		  	'sent'				=> $contracts->f('sent'),
		  	'status'				=> $status,
		  	'contract_id'			=> $contracts->f('contract_id'),
		  	'version_id'			=> $contracts->f('version_id'),
		  	'subject'				=> htmlspecialchars($contracts->f('subject')),
		  	'pdf_link'				=> 'index.php?do=contract-print&customer_id='.$in['customer_id'].'&contract_id='.$contracts->f('contract_id').'&version_id='.$contracts->f('version_id').'&lid='.$lng.$link_end,
		  	'amount'				=> place_currency(display_number($amount)),
		  	'amount_ord'			=> $amount,
		  	'version' 	    	=> $in['archived'] == 1 ? $db->field("SELECT version_code FROM contracts_version WHERE active=1 AND contract_id='".$contracts->f('contract_id')."' ") : $contracts->f('version_code'),
			'post_order'		    		=> $contracts->f('postgreen_id')!='' ? true : false,
			'show_expand'					=> $show_expand
		);
		array_push($result['query'], $item);
	}

	if($in['order_by']=='amount' ){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
	    }

	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	}

	$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	$result['post_active']= !$post_active || $post_active==0 ? false : true;
    $result['view_dd'] = array( array('id'=>'1', 'value'=>gm('Draft')), array('id'=>'2', 'value'=>gm('Sent')), array('id'=>'3', 'value'=>gm('Rejected')), array('id'=>'4', 'value'=>gm('Active')), array('id'=>'5', 'value'=>gm('Closed')), array('id'=>'-1', 'value'=>gm('Archived')) );
return json_out($result);

	function get_subList($in){
		$db = new sqldb();
		if(!isset($in['contract_id']) || !is_numeric($in['contract_id'])){
			return array();
		}
		$result = array( 'list' => array());

		$l_r = ROW_PER_PAGE;

		$order_by = " ORDER BY contracts_version.version_code ASC ";

		$contract = $db->query("SELECT contracts.name, contracts.closed,contracts.contract_id,contracts.customer_name, contracts_version.start_date,contracts_version.end_date,contracts_version.no_end_date,contracts.close_reason,
			contracts.invoicing_type,contracts.active,contracts.currency_rate,
			contracts.pdf_layout,contracts.pdf_logo,contracts.email_language,contracts.status_customer,
			contracts_version.version_id,contracts_version.discount,contracts_version.apply_discount,contracts_version.version_code,contracts_version.sent,contracts_version.postgreen_id FROM contracts
			INNER JOIN contracts_version ON contracts_version.contract_id=contracts.contract_id
		            WHERE contracts.contract_id='".$in['contract_id']."'
		            ".$order_by)->getAll();
		$row = count($contract);

		$i=0;
		foreach ($contract as $key => $value) {

			$amount = $db->field("SELECT SUM(amount) FROM contract_line WHERE contract_id='".$in['contract_id']."' AND version_id='".$value['version_id']."' AND show_block_total='0' AND use_recurring = 0 ");

			if($value['discount'] && $value['apply_discount'] > 1){
				$amount = $amount - ($amount*$value['discount']/100);
			}

			if($value['currency_rate']){
				$amount = $amount*return_value($value['currency_rate']);
			}

			$link_end = '&type='.ACCOUNT_CONTRACT_PDF_FORMAT;
			if($value['pdf_layout'] && $value['use_custom_template']==0){
				$link_end='&type='.$value['pdf_layout'].'&logo='.$value['pdf_logo'];
			}elseif($value['pdf_layout'] && $value['use_custom_template']==1){
				$link_end = '&custom_type='.$value['pdf_layout'].'&logo='.$value['pdf_logo'];
			}
			$lng = 1;
			if($value['email_language']){
				$lng = $value['email_language'];
			}

			$status='';
			if(!$value['sent']){
				$status=gm('Draft');
			}else{
				if($value['status_customer']==0){
					$status=gm('Active');
				}else if($value['status_customer']==1){
					$status=gm('Rejected');
				}else if($value['status_customer']==2){
					$status=gm('Accepted');
				}else{
					$status=gm('Closed');
				}
			}

			$item=array(
			  	'DELETE_LINK'			=> array('do'=>'contract-contracts-contract_version-delete','contract_id'=>$value['contract_id'],'xget'=>'subList','version_id'=>$value['version_id']),
			  	'CONTRACT_DATE'			=> $value['start_date'] ? date(ACCOUNT_DATE_FORMAT,$value['start_date']) : '',
			  	'CONTRACT_END_DATE'		=> $value['end_date'] ? date(ACCOUNT_DATE_FORMAT,$value['end_date']) : '',
			  	'status_customer'			=> $value['status_customer'],
			  	'active'				=> $value['active'],
			  	'sent'				=> $value['sent'],
			  	'status'				=> $status,
			  	'contract_id'			=> $value['contract_id'],
			  	'version_id'			=> $value['version_id'],
			  	'pdf_link'				=> 'index.php?do=contract-print&customer_id='.$in['customer_id'].'&contract_id='.$in['contract_id'].'&version_id='.$value['version_id'].'&lid='.$lng.$link_end,
			  	'amount'				=> place_currency(display_number($amount)),
			  	'version' 	    			=> $in['archived'] == 1 ? $db->field("SELECT version_code FROM contracts_version WHERE active=1 AND contract_id='".$in['contract_id']."' ") : $value['version_code'],
			  	'post_order'		    => $value['postgreen_id']!='' ? true : false,
			);

			array_push($result['list'], $item);
			$i++;
		}

		return $result;
	}
?>