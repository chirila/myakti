<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class ReportOrder extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}
		public function get_Data(){
			ini_set('memory_limit','2G');
			$in=$this->in;
			$result=array();

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(23,59,59,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(23,59,59,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(23,59,59,$month,$today,$year);
					}
					$result['period_txt']='';
					break;
			}
			$in['time_start']=$time_start;
			$in['time_end']=$time_end;
			$filter = ' AND 1=1';
			if($in['customer_id']){
				$filter .= ' AND pim_orders.customer_id ='.$in['customer_id'].' ';			
			}	
			if($in['author_id']){
				$filter .= ' AND pim_orders.author_id ='.$in['author_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND pim_orders.acc_manager_id ='.$in['manager_id'].' ';				
			}

			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}

			$big_margin=0;
			$where = " date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active='1' AND pim_orders.sent = '1' ";
			$this->db->query("SELECT pim_orders.acc_manager_id,pim_orders.margin,pim_orders.currency_rate,pim_orders.identity_id
				                 FROM pim_orders 
				                 INNER JOIN customers ON customers.customer_id=pim_orders.customer_id
				                 WHERE  ".$where." ".$filter." " );
			while ( $this->db->move_next()) {			
			  $currency_rate  = return_value($this->db->f('currency_rate'));
				if(!$currency_rate){
					$currency_rate = 1;		
				}
				$big_margin+=$this->db->f('margin')* $currency_rate;
			}

			$order = $this->db->query("SELECT pim_orders.acc_manager_id,sum(pim_orders.amount) AS amount,sum(pim_orders.margin) AS margin, count(pim_orders.order_id) AS total_orders
				                 FROM pim_orders
				                 INNER JOIN customers ON customers.customer_id=pim_orders.customer_id
				                 WHERE  ".$where." ".$filter." " );

			$order_article = $this->db->query("SELECT pim_orders.acc_manager_id,quantity,packing,sale_unit, purchase_price FROM pim_order_articles 
				                         INNER JOIN pim_orders ON pim_order_articles.order_id=pim_orders.order_id 
			                             INNER JOIN customers ON customers.customer_id=pim_orders.customer_id
				                         WHERE  ".$where." AND article_id!='0' ".$filter );

			$total_purchase_amount =0;
			while ($order_article->move_next()) {
					$packing  = $order_article->f('packing');
				if(!$packing){
					$packing = 1;		
				}
				$sale_unit  = $order_article->f('sale_unit');
				if(!$sale_unit){
					$sale_unit  = 1;		
				}
				$total_articles+=$order_article->f('quantity')*$packing/$sale_unit;
				$total_purchase_amount +=$order_article->f('purchase_price')*$order_article->f('quantity')*$packing/$sale_unit;
			}

			//$big_margin_percent=($big_margin/($order->f('amount')))*100;
			$big_margin_percent=0;
			if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				if($total_purchase_amount){
					$big_margin_percent=($big_margin/$total_purchase_amount)*100;
				}elseif($big_margin){
					$big_margin_percent=100;
				}
			}else{
				if($order->f('amount')){
					$big_margin_percent=($big_margin/$order->f('amount'))*100;
				}elseif($big_margin){
					$big_margin_percent=100;
				}
			}
			

			$result['BIG_TOTAL_A'] 			= place_currency(display_number($order->f('amount')));
			$result['total_amount']      	= $order->f('amount'); 
			$result['BIG_QUANTITY_A'] 		= $total_articles;
			$result['TOTAL_ORDERS'] 		= $order->f('total_orders');
			$result['BIG_MARGIN']			= place_currency(display_number($big_margin));
			$result['BIG_MARGIN_PERCENT']	= display_number($big_margin_percent);
			$result['delivered_amount']		= $this->db->field("SELECT sum(amount) FROM pim_orders  INNER JOIN customers ON customers.customer_id=pim_orders.customer_id WHERE  ".$where." ".$filter." AND rdy_invoice = '1' ") ? $this->db->field("SELECT sum(amount) FROM pim_orders  INNER JOIN customers ON customers.customer_id=pim_orders.customer_id WHERE  ".$where." ".$filter." AND rdy_invoice = '1' ") : 0;
			$result['ndelivered_amount']	= $this->db->field("SELECT sum(amount) FROM pim_orders  INNER JOIN customers ON customers.customer_id=pim_orders.customer_id WHERE  ".$where." ".$filter." AND rdy_invoice != '1' ") ? $this->db->field("SELECT sum(amount) FROM pim_orders  INNER JOIN customers ON customers.customer_id=pim_orders.customer_id WHERE  ".$where." ".$filter." AND rdy_invoice != '1' ") : 0;
			$result['EXPORT_HREF']= 'index.php?do=report-export_orders_detail_report&u_id='.$in['author_id'].'&customer_id='.$in['customer_id'].'&manager_id='.$in['manager_id'].'&time_start='.$time_start.'&time_end='.$time_end;	
			$result['time_start']=$time_start;
			$result['time_end']=$time_end;
			$result['customers'] = $this->get_cc($in);
			$result['authors']=$this->get_authors($in);
			$result['managers']=$this->get_authors($in);
			$result['customer_tab']=$this->get_customersTab($in);
			$result['chart_order']=array(				
					'label'			=> array(gm('Delivered'),gm('Not delivered')),
					'data' 			=> array(number_format($result['delivered_amount'],2,'.',''),number_format($result['ndelivered_amount'],2,'.','')),
					'data_show' 	=> array(get_currency_sign(display_number($result['delivered_amount'])),get_currency_sign(display_number($result['ndelivered_amount']))),
				);
			$result['chart_customer']=$this->get_customerChart($in);
			$result['chart_manager']=$this->get_managerChart($in);
			$result['adv_search']	= true;
			$result['customer_type']			= getCustomerType();
			$result['identities']				= build_identity_dd();

			$this->out=$result;
		}

		public function get_cc(&$in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" is_admin='0' AND customers.active=1 ";
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			}
			if($in['customer_id']){
				$filter .=" AND customers.customer_id='".$in['customer_id']."' ";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type,cat_id
				FROM customers
				
				WHERE $filter
				GROUP BY customers.customer_id			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array('list'=>array());
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);
				if($value['type']==0){
					$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
				}elseif($value['type']==1){
					$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
				}else{
					$symbol ='';
				}
				$address = $this->db->query("SELECT zip,city,address FROM customer_addresses
										WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");
				$result_line=array(
					"id"					=> $value['cust_id'],
					'symbol'				=> $symbol,
					"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					/*'zip'					=> $value['zip'] ? $value['zip'] : '',
					'city'					=> $value['city'] ? $value['city'] : '',
					"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],*/
					'zip'					=> $address->f('zip') ? $address->f('zip') : '',
					'city'					=> $address->f('city') ? $address->f('city') : '',
					"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
					"right"					=> $value['acc_manager_name'],
					"cat_id"				=> $value['cat_id']
				);
				array_push($result['list'],$result_line);
			}		
			return $result;
		}

		public function get_authors(&$in){
			$q = strtolower($in["term"]);

			$filter = '';

			if($q){
				$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
			}
			if($in['author_id']){
				$filter .=" AND users.user_id='".$in['author_id']."' ";
			}

			$data=$this->db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY last_name ");
			while($data->move_next()){
			  $users[$data->f('last_name').' '.$data->f('first_name')]=$data->f('user_id');
			}

			$result = array('list'=>array());
			if($users){
				foreach ($users as $key=>$value) {
					$result_line=array(
						'id'		=> $value,
						'value'		=> htmlspecialchars_decode(stripslashes($key))
					);
					array_push($result['list'], $result_line);
				}
			}
			return $result;
		}
		public function get_customersTab(&$in){
			$result=array('other_row'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$order_by_array = array('amount','margin');

			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_customer']) || (!is_numeric($in['offset_customer'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_customer']-1;
			}

			if($in['customer_id']){
				$filter.=" and pim_orders.customer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND pim_orders.author_id=".$in['author_id'];
			}
			if($in['manager_id']){
				$filter.=" AND pim_orders.acc_manager_id=".$in['manager_id'];
			}

			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == 'true'){
						$ord = " DESC ";
					}
					if($in['order_by'] == 'margin'){

					}else{
						$order_by =" ORDER BY ".$in['order_by']." ".$ord;
					}				
				}
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_orders = 0;
			$big_margin=0;
			$margin_arr=array();
			$this->db->query("SELECT  pim_orders.acc_manager_id,pim_orders.order_id,pim_orders.margin,pim_orders.currency_rate,pim_orders.customer_id FROM pim_orders 
			LEFT JOIN customers ON pim_orders.customer_id=customers.customer_id
			WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND pim_orders.sent = '1' $filter ");
			while ( $this->db->move_next()) {		
			  $currency_rate  = return_value($this->db->f('currency_rate'));
				if(!$currency_rate){
					$currency_rate = 1;		
				}
				$margin_arr[$this->db->f('customer_id')]+=$this->db->f('margin')* $currency_rate;
			}

			$order = $this->db->query("SELECT pim_orders.acc_manager_id,pim_orders.customer_id,pim_orders.order_id,sum(amount) AS amount, count(pim_orders.order_id) AS total_orders, customer_name, pim_orders.customer_id, name, field FROM pim_orders 
				LEFT JOIN customers ON pim_orders.customer_id=customers.customer_id
				WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND pim_orders.sent = '1'  $filter
					GROUP BY pim_orders.customer_id $order_by ");
			$max_rows=$this->db->records_count();
			$result['max_rows']=$max_rows;

			if($in['order_by']!='amount' && $in['order_by']!='margin'){
				$this->db->move_to($offset*$l_r);
			}
			while ($order->next() && $i<$l_r){
				$line_q=0;
				$total_purchase_amount = 0;
				$order_article = $this->db->query("SELECT quantity,sale_unit,packing, purchase_price FROM pim_order_articles INNER JOIN pim_orders ON pim_order_articles.order_id=pim_orders.order_id WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND article_id!='0' AND pim_orders.sent = '1' AND customer_id='".$order->f('customer_id')."' ");
			    while ($order_article->move_next()) {		    	
			    	if($order_article->f('sale_unit')){
			    		$sale_unit=$order_article->f('sale_unit');
			    	}else{
			    		$sale_unit=1;
			    	}
			    	if($order_article->f('packing')){
			    		$packing=$order_article->f('packing');
			    	}else{
			    		$packing=1;
			    	}
			    	
			    	$line_q+=$order_article->f('quantity')*$packing/$sale_unit;
			    	$total_purchase_amount +=$order_article->f('purchase_price') *$order_article->f('quantity')*$packing/$sale_unit;

			    }
			    //$total_margin_percent= ($margin_arr[$order->f('customer_id')]/($order->f('amount')))*100;	
			    $total_margin_percent=0;
			    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				    if($total_purchase_amount){
				    	$total_margin_percent= ($margin_arr[$order->f('customer_id')]/($total_purchase_amount))*100;				   	
				    }elseif($margin_arr[$order->f('customer_id')]){
				    	$total_margin_percent= 100;
				    }
				}else{
					if($order->f('amount')){
				    	$total_margin_percent= ($margin_arr[$order->f('customer_id')]/($order->f('amount')))*100;				   	
				    }elseif($margin_arr[$order->f('customer_id')]){
				    	$total_margin_percent= 100;
				    }
				}
				$other_row=array(
						'CUSTOMER_N'		=> $order->f('field') == 'customer_id' ? $order->f('name') : $order->f('customer_name'),
						'CUSTOMER_ID'		=> $order->f('customer_id'),
						'u_id'		=> $in['u_id'],

						'NAV'				=> $time_nav,
						'TOTAL_MARGIN'		=> place_currency(display_number($margin_arr[$order->f('customer_id')])),
						'TOTAL_MARGIN_PERCENT'=> display_number($total_margin_percent),
						'TOTAL_CLIENT'		=> place_currency(display_number($order->f('amount'))),
						'TOTAL_QUANTITY'	=> display_number($line_q),
						'ORDERS'			=> $order->f('total_orders'),
						'amount_ord'		=> $order->f('amount'),
						'margin_ord'		=> $margin_arr[$order->f('customer_id')]
				);		
				array_push($result['other_row'],$other_row);
				$big_margin += $order->f('margin');
				$big_total += $order->f('amount');
				$big_quantity += $order_article;
				$big_orders += $order->f('total_orders');
				if($in['order_by']!='amount' && $in['order_by']!='margin'){
					$i++;
				}
			}
			$big_margin_percent=($big_margin/($big_total))*100;
			$result['BIG_TOTAL']			= place_currency(display_number($big_total));
			$result['BIG_MARGIN']			= place_currency(display_number($big_margin));
			$result['BIG_MARGIN_PERCENT']	= display_number($big_margin_percent);
			$result['BIG_QUANTITY']			= display_number($big_quantity);
			$result['BIG_ORDERS']			= $big_orders;

			if($in['order_by']=='amount' || $in['order_by']=='margin'){

			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord, $offset, $l_r);    
			}

			return $result;
		}

		public function get_ordersTab(&$in){
			$result=array('other_row'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$order_by_array = array('amount','t_date','margin');

			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_order']) || (!is_numeric($in['offset_order'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_order']-1;
			}
			$order_by = " ORDER BY t_date DESC, iii DESC ";
			$filter= " AND pim_orders.active=1 AND pim_orders.sent = '1' ";

			if($in['customer_id']){
				$filter.=" and pim_orders.customer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND pim_orders.author_id=".$in['author_id'];
			}
			if($in['manager_id']){
				$filter.=" AND pim_orders.acc_manager_id=".$in['manager_id'];
			}

			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}

			if($in['delivered']){
				$filter.=" and pim_orders.rdy_invoice='1' ";
			}else{
				$filter.=" and pim_orders.rdy_invoice!='1' ";
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == 'true'){
						$ord = " DESC ";
					}
					if($in['order_by'] == 'margin'){

					}else{
						$order_by =" ORDER BY ".$in['order_by']." ".$ord;
					}			
				}
			}

			$this->db->query("SELECT pim_orders.acc_manager_id,pim_orders.*, serial_number as iii, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date, sum(pim_order_articles.quantity * pim_order_articles.purchase_price) as purchase_amount
						FROM pim_orders 
						LEFT JOIN pim_order_articles ON pim_orders.order_id = pim_order_articles.order_id
						LEFT JOIN customers ON customers.customer_id=pim_orders.customer_id
						WHERE date BETWEEN '".$time_start."' AND '".$time_end."' ".$filter."  GROUP BY pim_orders.order_id ".$order_by );

			$max_rows=$this->db->records_count();
			$result['max_rows']=$max_rows;

			if($in['order_by']!='amount' && $in['order_by'] != 'margin'){
				$this->db->move_to($offset*$l_r);
			}

			$j=0;
			$color = '';
			$total_amount = 0;
			$total_margin = 0;
			while($this->db->move_next() && $j<$l_r){
				$ref = '';
				if(ACCOUNT_ORDER_REF && $this->db->f('buyer_ref')){
					$ref = $this->db->f('buyer_ref');
				}	
			 $currency_rate  = return_value($this->db->f('currency_rate'));
				if(!$currency_rate){
					$currency_rate = 1;		
				}
				$margin_percent = 0;
				//console::log($this->db->f('purchase_amount'), $this->db->f('margin'));
				if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
					if($this->db->f('purchase_amount') && $this->db->f('purchase_amount')!='0.00000000'){
						$margin_percent = $this->db->f('margin')/$this->db->f('purchase_amount')*100;
					}elseif($this->db->f('margin')!='0.0000'){
						$margin_percent = 100;
					}
				}else{
					if($this->db->f('amount')){
						$margin_percent = $this->db->f('margin')/$this->db->f('amount')*100;
					}elseif($this->db->f('margin')!='0.0000'){
						$margin_percent = 100;
					}
				}
				$other_row=array(
					'id'                	=> $this->db->f('order_id'),
					'serial_number'     	=> $this->db->f('serial_number') ? $ref.$this->db->f('serial_number') : '',
					'created'           	=> date(ACCOUNT_DATE_FORMAT,$this->db->f('date')),
					'amount'				=> place_currency(display_number($this->db->f('amount'))),
					'margin'				=> place_currency(display_number($this->db->f('margin')*$currency_rate) ),
			        //'margin_percent'		=> display_number($this->db->f('margin_percent')),
			        'margin_percent'		=> display_number($margin_percent),
					'buyer_name'        	=> $this->db->f('customer_name'),
					'is_editable'			=> $this->db->f('rdy_invoice') ? false : true,
					'is_archived'       	=> $in['archived'] ? true : false,
					'status'				=> $this->db->f('rdy_invoice') ? ($this->db->f('rdy_invoice')==1 ? 'green' : 'orange') : ($this->db->f('sent') == 1 ? 'orange' : 'gray'),
					'amount_ord' 			=> $this->db->f('amount'),
					'margin_ord'			=> ($this->db->f('margin')*$currency_rate)
				);
				array_push($result['other_row'],$other_row);

				$total_amount += $this->db->f('amount');
				$total_margin += $this->db->f('margin');
				if($in['order_by']!='amount' && $in['order_by']!='margin'){
					$j++;
				}
			}

			$total_percent= ($total_margin/($total_amount))*100;
			$result['total_amount']		= place_currency(display_number($total_amount));
			$result['total_margin']		= place_currency(display_number($total_margin));
			$result['total_percent']	= display_number($total_percent);

			if($in['order_by']=='amount' || $in['order_by']=='margin'){

			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord, $offset, $l_r);    
			}

			return $result;
		}

		public function get_articlesTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount','margin');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_article']) || (!is_numeric($in['offset_article'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_article']-1;
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			if($in['customer_id']){
				$filter.=" and pim_orders.customer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND pim_orders.author_id=".$in['author_id'];
			}
			if($in['manager_id']){
				$filter.=" AND pim_orders.acc_manager_id=".$in['manager_id'];
			}

			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}

			$articles = $this->db->query("SELECT
							pim_order_articles.order_articles_id,
							pim_order_articles.order_id,
							pim_order_articles.purchase_price,
							pim_order_articles.article_id,
							pim_order_articles.quantity,
							pim_order_articles.price,            
							pim_orders.apply_discount,
							pim_orders.currency_rate,
							pim_orders.acc_manager_id,
							pim_order_articles.discount AS line_disc,
							pim_orders.discount AS global_disc,
							pim_order_articles.packing,
							pim_order_articles.sale_unit,
							pim_articles.internal_name
			FROM pim_order_articles
			INNER JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
			INNER JOIN pim_articles ON pim_order_articles.article_id = pim_articles.article_id
			LEFT JOIN customers ON customers.customer_id=pim_orders.customer_id
			WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active = '1' 
			AND pim_orders.sent = '1' ".$filter." ");

			$article_data = array();
			$big_total = 0;
			$big_quantity = 0;
			$big_purchase_amount = 0;
			$max_rows=$this->db->records_count();
			while ($articles->next()){
				$currency_rate  = return_value($articles->f('currency_rate'));
				if(!$currency_rate){
					$currency_rate = 1;		
				}
			    
				$article_data[$articles->f('article_id')]['internal_name'] = $articles->f('internal_name');

				$article_data[$articles->f('article_id')]['order_q'][$articles->f('order_id')] = 1;

				
				$article_data[$articles->f('article_id')]['article_q']+=$articles->f('quantity')*($articles->f('packing')/$articles->f('sale_unit'));

				switch ($articles->f('apply_discount')) {
					case '0'://no discount
						$amount = $articles->f('quantity') * $articles->f('price') * ($articles->f('packing')/$articles->f('sale_unit'));
						break;
					case '1'://line discount
						$amount = $articles->f('quantity') * $articles->f('price') * ( 1 - $articles->f('line_disc')/100 ) * ($articles->f('packing')/$articles->f('sale_unit'));
						break;
					case '2'://global discount
						$a = $articles->f('quantity') * $articles->f('price') * $articles->f('packing')/$articles->f('sale_unit');
						$b = 1 - $articles->f('global_disc')/100;
						$amount = $a * $b;
						break;
					case '3'://line and global discount
						$line_discount = $articles->f('quantity') * $articles->f('price') * ( 1 - $articles->f('line_disc')/100 ) * ($articles->f('packing')/$articles->f('sale_unit'));
						$amount = $line_discount * ( 1 - $articles->f('global_disc')/100 );
						break;
					default://no discount
						$amount = $articles->f('quantity') * $articles->f('price') * ( 1 - 0/100 ) * ($articles->f('packing')/$articles->f('sale_unit'));
						break;
				}

				if($articles->f('currency_rate')){
					$currency_rate = return_value($articles->f('currency_rate'));
				}
				$amount = $amount*$currency_rate;
				
				$article_data[$articles->f('article_id')]['margin']+=$currency_rate*($articles->f('price')-$articles->f('purchase_price'))*$articles->f('quantity')*($articles->f('packing')/$articles->f('sale_unit'));
				$article_data[$articles->f('article_id')]['purchase_amount']+=$currency_rate*($articles->f('purchase_price'))*$articles->f('quantity')*($articles->f('packing')/$articles->f('sale_unit'));
				
				$article_data[$articles->f('article_id')]['amount']+=$amount;
				$big_total+=$amount;
				$big_purchase_amount +=$article_data[$articles->f('article_id')]['purchase_amount'];
			}
			$result['max_rows']=count($article_data);
			$i = 0;
			
			$j=0;
			foreach ($article_data as $article_id => $art_data) {
				if($in['order_by']!='amount' && $in['order_by']!='margin'){
						if($j>=$offset*$l_r && $j <($offset*$l_r+$l_r)){
							$margin_percent=0;

							if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
								if($art_data['purchase_amount']){
									$margin_percent = $art_data['margin']/ ($art_data['purchase_amount'])*100;
								}elseif($art_data['margin']){
									$margin_percent=100;
								}
							}else{
								if($art_data['amount']){
									$margin_percent = $art_data['margin']/ ($art_data['amount'])*100;
								}elseif($art_data['margin']){
									$margin_percent=100;
								}
							}
							$last_order=$this->db->query("SELECT pim_order_articles.order_id ,pim_orders.serial_number
								                    FROM pim_order_articles
								                    INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
								                    WHERE pim_order_articles.article_id='".$article_id."'
								                    ORDER BY pim_order_articles.order_id DESC
								                    LIMIT 1");
							$other_row=array(
									'ARTCLE_N'			=> htmlspecialchars_decode($art_data['internal_name']),
									'ARTICLE_ID'		=> $article_id,
									'NAV'				=> $time_nav,
									'TOTAL_ARTICLE'		=> place_currency(display_number($art_data['amount'])),
									'TOTAL_QUANTITY'		=> display_number($art_data['article_q']),
									'TOTAL_MARGIN'			=> place_currency(display_number($art_data['margin'])),
									//'TOTAL_MARGIN_PERCENT'	=> display_number(($art_data['margin']/ ($art_data['amount']))*100 ),
									'TOTAL_MARGIN_PERCENT'	=> display_number($margin_percent),
									'TIME_START' 		=> $in['time_start'],
									'TIME_END'	 		=> $in['time_end'],
									'XPAG'			    => $in['pag'],
									'last_order'		=> $last_order->f('serial_number'),
									'order_id'        	=> $last_order->f('order_id'),
									'amount_ord'		=> $art_data['amount']		
							);
							array_push($result['other_row'], $other_row);
						    $big_margin += $art_data['margin']*$art_data['article_q'];
							$big_quantity += $art_data['article_q'];
						}
				}else{
					$margin_percent=0;

							if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
								if($art_data['purchase_amount']){
									$margin_percent = $art_data['margin']/ ($art_data['purchase_amount'])*100;
								}elseif($art_data['margin']){
									$margin_percent=100;
								}
							}else{
								if($art_data['amount']){
									$margin_percent = $art_data['margin']/ ($art_data['amount'])*100;
								}elseif($art_data['margin']){
									$margin_percent=100;
								}
							}
							$last_order=$this->db->query("SELECT pim_order_articles.order_id ,pim_orders.serial_number
								                    FROM pim_order_articles
								                    INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
								                    WHERE pim_order_articles.article_id='".$article_id."'
								                    ORDER BY pim_order_articles.order_id DESC
								                    LIMIT 1");
							$other_row=array(
									'ARTCLE_N'			=> htmlspecialchars_decode($art_data['internal_name']),
									'ARTICLE_ID'		=> $article_id,
									'NAV'				=> $time_nav,
									'TOTAL_ARTICLE'		=> place_currency(display_number($art_data['amount'])),
									'TOTAL_QUANTITY'		=> display_number($art_data['article_q']),
									'TOTAL_MARGIN'			=> place_currency(display_number($art_data['margin'])),
									//'TOTAL_MARGIN_PERCENT'	=> display_number(($art_data['margin']/ ($art_data['amount']))*100 ),
									'TOTAL_MARGIN_PERCENT'	=> display_number($margin_percent),
									'TIME_START' 		=> $in['time_start'],
									'TIME_END'	 		=> $in['time_end'],
									'XPAG'			    => $in['pag'],
									'last_order'		=> $last_order->f('serial_number'),
									'order_id'        	=> $last_order->f('order_id'),
									'amount_ord'		=> $art_data['amount'],
									'margin_ord'		=> $art_data['margin']	
							);
							array_push($result['other_row'], $other_row);
						    $big_margin += $art_data['margin']*$art_data['article_q'];
							$big_quantity += $art_data['article_q'];
				}
				$j++;
			}

			//$big_margin_percent=($big_margin/($big_total))*100;
			$big_margin_percent=($big_margin/($big_purchase_amount))*100;
			$result['BIG_TOTAL']			= place_currency(display_number($big_total));
			$result['BIG_MARGIN']			= place_currency(display_number($big_margin));
			$result['BIG_MARGIN_PERCENT']	= display_number($big_margin_percent);
			$result['BIG_QUANTITY']			= display_number($big_quantity);

			if($in['order_by']=='amount' || $in['order_by']=='margin'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order, $offset, $l_r);	    
			}

			return $result;
		}

		public function get_taxesTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter.=" and pim_orders.customer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND pim_orders.author_id=".$in['author_id'];
			}
			if($in['manager_id']){
				$filter.=" AND pim_orders.acc_manager_id=".$in['manager_id'];
			}
			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$tax_data = array();
			$taxes = $this->db->query("SELECT 
							pim_order_articles.order_articles_id,
							pim_order_articles.order_id,
							pim_order_articles.article_id,
							pim_order_articles.quantity, 
							pim_order_articles.price, 
							pim_orders.apply_discount,
							pim_order_articles.discount AS line_disc, 
							pim_orders.discount AS global_disc,
							pim_order_articles.packing, 
							pim_order_articles.sale_unit,
							pim_article_tax_type.name AS t_name
				FROM pim_order_articles 
				INNER JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id 
				INNER JOIN pim_article_tax ON pim_article_tax.tax_id = pim_order_articles.tax_id
				INNER JOIN pim_article_tax_type ON pim_article_tax_type.id = pim_article_tax.type_id 
				LEFT JOIN customers 
					ON pim_orders.customer_id = customers.customer_id
				WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active = '1' AND pim_orders.sent = '1' AND tax_for_article_id > '0' ".$filter." ");
			while ($taxes->next()) {
				$tax_data[$taxes->f('tax_id')]['tax_name'] = $taxes->f('t_name');

				$tax_data[$taxes->f('tax_id')]['order_q'][$taxes->f('order_id')] = 1;

				$tax_data[$taxes->f('tax_id')]['tax_q']+=$taxes->f('quantity');

				switch ($taxes->f('apply_discount')) {
					case '0'://no discount
						$amount = $taxes->f('quantity') * $taxes->f('price');
						break;
					case '1'://line discount
						$amount = $taxes->f('quantity') * $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 );
						break;
					case '2'://global discount
						$a = $taxes->f('quantity') * $taxes->f('price');
						$b = 1 - $taxes->f('global_disc')/100;			
						$amount = $a * $b;
						break;
					case '3'://line and global discount
						$line_discount = $taxes->f('quantity') * $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 );
						$amount = $line_discount * ( 1 - $taxes->f('global_disc')/100 );	
						break;		
					default://no discount
						$amount = $taxes->f('quantity') * $taxes->f('price');
						break;
				}
				$tax_data[$taxes->f('tax_id')]['amount']+=$amount;
			}
			foreach ($tax_data as $tax_id => $data) {
				
				$other_row=array(
						'ARTCLE_N'			=> $data['tax_name'],
						'ARTICLE_ID'		=> $tax_id,
						'NAV'				=> $time_nav,
						'TOTAL_ARTICLE'		=> place_currency(display_number($data['amount'])),
						'TOTAL_QUANTITY'	=> display_number($data['tax_q']),
						'ORDERS'			=> display_number($data['order_q']),
						'TIME_START' 		=> $in['time_start'],
						'TIME_END'	 		=> $in['time_end'],
						'amount_ord'		=> $data['amount'],
				);			
				array_push($result['other_row'], $other_row);
				$big_total += $data['amount'];
				$big_quantity += $data['tax_q'];
				$i++;				
			}

			$result['BIG_TOTAL']		= place_currency(display_number($big_total));
			$result['BIG_QUANTITY']		= display_number($big_quantity);

			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order);   
			}
			return $result;
		}

		public function get_managersTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount','margin');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_manager']) || (!is_numeric($in['offset_manager'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_manager']-1;
			}

			if($in['customer_id']){
				$filter.=" and pim_orders.customer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND pim_orders.author_id=".$in['author_id'];
			}
			if($in['manager_id']){
				$filter.=" AND pim_orders.acc_manager_id=".$in['manager_id'];
			}
			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}
	
			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_orders = 0;
			$big_margin=0;
			$margin_arr=array();
			$this->db->query("SELECT  pim_orders.acc_manager_name,pim_orders.acc_manager_id,pim_orders.order_id,pim_orders.margin,pim_orders.currency_rate,pim_orders.customer_id FROM pim_orders 
			LEFT JOIN customers ON pim_orders.customer_id=customers.customer_id
			WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND pim_orders.sent = '1'  $filter
			");
			while ( $this->db->move_next()) {
				
			  $currency_rate  = return_value($this->db->f('currency_rate'));
				if(!$currency_rate){
					$currency_rate = 1;		
				}
				$margin_arr[$this->db->f('acc_manager_id')]+=$this->db->f('margin')* $currency_rate;
			}

			$order = $this->db->query("SELECT pim_orders.acc_manager_name,pim_orders.acc_manager_id,pim_orders.customer_id,pim_orders.order_id,sum(amount) AS amount, count(pim_orders.order_id) AS total_orders, customer_name, pim_orders.customer_id, name, field FROM pim_orders 
			LEFT JOIN customers ON pim_orders.customer_id=customers.customer_id
			WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND pim_orders.sent = '1'  $filter
			GROUP BY pim_orders.acc_manager_id, field 
			$order_by
			");
			$max_rows=$this->db->records_count();
			$result['max_rows']=$max_rows;
			if($in['order_by']!='amount' && $in['order_by']!='margin'){
				$this->db->move_to($offset*$l_r);
			}
			while ($order->next() && $i<$l_r){
				$line_q=0;
				$total_purchase_amount =0;
				$order_article = $this->db->query("SELECT quantity,sale_unit,packing, purchase_price FROM pim_order_articles INNER JOIN pim_orders ON pim_order_articles.order_id=pim_orders.order_id WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND article_id!='0' AND pim_orders.sent = '1' AND customer_id='".$order->f('customer_id')."' ");
			    while ($order_article->move_next()) {		    	 
			    	if($order_article->f('sale_unit')){
			    		$sale_unit=$order_article->f('sale_unit');
			    	}else{
			    		$sale_unit=1;
			    	}
			    	if($order_article->f('packing')){
			    		$packing=$order_article->f('packing');
			    	}else{
			    		$packing=1;
			    	}
			    	
			    	$line_q+=$order_article->f('quantity')*$packing/$sale_unit;
			    	$total_purchase_amount += $order_article->f('purchase_price')*$order_article->f('quantity')*$packing/$sale_unit;
			    }

			    //$total_margin_percent= ($margin_arr[$order->f('acc_manager_id')]/($order->f('amount')))*100;
			    $total_margin_percent= 0;
			    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				    if($total_purchase_amount){
				    	$total_margin_percent= ($margin_arr[$order->f('acc_manager_id')]/$total_purchase_amount)*100;
				    }elseif($margin_arr[$order->f('acc_manager_id')]){
				    	$total_margin_percent=100;
				    }
				 }else{
				 	if($order->f('amount')){
				    	$total_margin_percent= ($margin_arr[$order->f('acc_manager_id')]/$order->f('amount'))*100;
				    }elseif($margin_arr[$order->f('acc_manager_id')]){
				    	$total_margin_percent=100;
				    }
				 }

				$other_row=array(
						'MANAGER_N'			=> get_user_name($order->f('acc_manager_id')),
						'u_id'				=> $in['u_id'],
						'NAV'				=> $time_nav,
						'TOTAL_MARGIN'		=> place_currency(display_number($margin_arr[$order->f('acc_manager_id')])),
						'MARGIN_PERCENT'	=> display_number($total_margin_percent),
						'TOTAL_MANAGER'		=> place_currency(display_number($order->f('amount'))),						
						'TIME_START' 		=> $in['time_start'],
						'TIME_END'	 		=> $in['time_end'],
						'amount_ord'		=> $order->f('amount'),
						'margin_ord'		=> $margin_arr[$order->f('acc_manager_id')]
				);		
				array_push($result['other_row'], $other_row);
				$big_margin += $order->f('margin');
				$big_total += $order->f('amount');
				$big_quantity += $order_article;
				$big_orders += $order->f('total_orders');
				if($in['order_by']!='amount'){
					$i++;
				}
			}

			$big_margin_percent=($big_margin/($big_total))*100;
			$result['BIG_TOTAL']			= place_currency(display_number($big_total));
			$result['BIG_MARGIN']			= place_currency(display_number($big_margin));
			$result['BIG_MARGIN_PERCENT']	= display_number($big_margin_percent);
			$result['BIG_QUANTITY']			= display_number($big_quantity);
			$result['BIG_ORDERS']			= $big_orders;

			if($in['order_by']=='amount' || $in['order_by']=='margin'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord, $offset, $l_r);   
			}
			return $result;
		}

		public function get_customerChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter='';
			if($in['customer_id']){
				$filter .= ' AND pim_orders.customer_id ='.$in['customer_id'].' ';			
			}	
			if($in['author_id']){
				$filter .= ' AND pim_orders.author_id ='.$in['author_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND pim_orders.acc_manager_id ='.$in['manager_id'].' ';				
			}
			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_orders = 0;
			$order = $this->db->query("SELECT pim_orders.acc_manager_id,sum(amount) AS amount, count(order_id) AS total_orders, customer_name, pim_orders.customer_id, name, field FROM pim_orders 
				LEFT JOIN customers ON pim_orders.customer_id=customers.customer_id
				WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND pim_orders.sent = '1'  $filter
				GROUP BY pim_orders.customer_id, field order by amount DESC limit 5");
			while ($order->next()) {
				array_push($result['label'], $order->f('field') == 'customer_id' ? $order->f('name') : $order->f('customer_name'));
				array_push($result['data'], number_format($order->f('amount'),2,'.',''));
				array_push($result['data_show'], get_currency_sign(display_number($order->f('amount'))));
			}

			return $result;
		}

		public function get_managerChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];
			$filter='';
			if($in['customer_id']){
				$filter .= ' AND pim_orders.customer_id ='.$in['customer_id'].' ';			
			}	
			if($in['author_id']){
				$filter .= ' AND pim_orders.author_id ='.$in['author_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND pim_orders.acc_manager_id ='.$in['manager_id'].' ';				
			}
			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND pim_orders.identity_id ='.$in['identity_id'].'  ';
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_orders = 0;
			$order = $this->db->query("SELECT sum(amount) AS amount, count(order_id) AS total_orders, customer_name, pim_orders.customer_id, name, field,pim_orders.acc_manager_id,pim_orders.acc_manager_name 
				   FROM pim_orders 
				LEFT JOIN customers ON pim_orders.customer_id=customers.customer_id
				WHERE date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active=1 AND pim_orders.sent = '1'  $filter
				GROUP BY pim_orders.acc_manager_id, field order by amount DESC limit 5");
			while ($order->next()) {
				
				array_push($result['label'], $order->f('field') == 'customer_id' ? $order->f('acc_manager_name') : $order->f('acc_manager_name'));
				array_push($result['data'], number_format($order->f('amount'),2,'.',''));
				array_push($result['data_show'], get_currency_sign(display_number($order->f('amount'))));
			}

			return $result;
		}

	}

	$cash_data = new ReportOrder($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_Data();
	$cash_data->output();
?>