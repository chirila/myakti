<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit','2G');
setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Orders detail report")
							 ->setSubject("Orders detail report")
							 ->setDescription("Articles report export")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Articles");*/
$time_start = $in['time_start'];
$time_end = $in['time_end'];
/*$filename ="orders_detail_report.xls";*/
$filename ="orders_detail_report.csv";

$filter = " pim_order_articles.article_id != 0 AND sent = '1' ";
$filter2 = " date BETWEEN '".$time_start."' AND '".$time_end."' AND pim_orders.active='1' ";

if(!$time_start && !$time_end){
	$filter2 = " AND 1=1 ";
}


if($in['customer_id']){
	$filter .= "  AND pim_orders.customer_id=".$in['customer_id']."";
}
if($in['u_id']){
 $filter .= ' AND pim_orders.author_id ='.$in['u_id'].' ';
}
if($in['manager_id']){
    $filter .= ' AND pim_orders.acc_manager_id ='.$in['manager_id'].' ';        
}
$orders =$db->query("SELECT
              pim_order_articles.order_id,
							pim_order_articles.article_id,
							pim_order_articles.article AS article_name,
							pim_order_articles.price,
							pim_order_articles.quantity,
              pim_order_articles.discount AS line_disc,
              pim_order_articles.packing,
              pim_order_articles.sale_unit,


							pim_orders.serial_number,
							pim_orders.customer_name,
              pim_orders.customer_id,
							pim_orders.date,
							pim_orders.rdy_invoice,
              pim_orders.currency_rate,
              pim_orders.discount AS global_disc,
              pim_orders.apply_discount

					    FROM	pim_order_articles
				      LEFT JOIN pim_orders
				      ON pim_order_articles.order_id = pim_orders.order_id

	            WHERE ".$filter2." AND ".$filter." ");


/*$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Date"))
			->setCellValue('B1', gm("Client"))
			->setCellValue('C1', gm("Serial Number"))
			->setCellValue('D1', gm("Article Name"))
			->setCellValue('E1', gm("Total Quantity"))
			->setCellValue('F1', gm("Total Value"))
			->setCellValue('G1', gm("Total Tax value"))
			->setCellValue('H1', gm("Delivered"));*/

$headers    = array(gm("Date"),
                    gm('Client'),
                    gm('Company id'),
                    gm("Serial Number"),
                    gm("Article Name"),
                    gm("Total Quantity"),
                    gm("Total Value"),
                    gm("Total Tax value"),
                    gm("Delivered")
      );

$i=0;
 $xlsRow = 2;
 $article_total_value = 0;
 $taxes_total_value = 0;
 $article_total_quantity = 0;
 $final_data=array();

while($orders->next()) {
  switch ($orders->f('apply_discount')) {
    case '0'://no discount

      $total_value = $orders->f('quantity') * $orders->f('price') * ($orders->f('packing')/$orders->f('sale_unit'));
      break;
    case '1'://line discount
      $total_value = $orders->f('quantity') * $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) * ($orders->f('packing')/$orders->f('sale_unit'));
      break;
    case '2'://global discount
      $a = $orders->f('quantity') * $orders->f('price') * $orders->f('packing')/$orders->f('sale_unit');
      $b = 1 - $orders->f('global_disc')/100;
      $total_value = $a * $b;
      break;
    case '3'://line and global discount
      $line_discount = $orders->f('quantity') * $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) * ($orders->f('packing')/$orders->f('sale_unit'));
      $total_value = $line_discount * ( 1 - $orders->f('global_disc')/100 );
      break;
    default://no discount
      $total_value = $orders->f('quantity') * $orders->f('price') * ( 1 - 0/100 ) * ($orders->f('packing')/$orders->f('sale_unit'));
      break;
  }
	$currency_rate = 1;
  if($orders->f('currency_rate')){
    $currency_rate = return_value($orders->f('currency_rate'));
  }
  $total_value = $total_value*$currency_rate;
  $article_total_value = $article_total_value+$total_value;

  $taxes_price = 0;

  $article_taxes_data = $db->query("SELECT * FROM pim_order_articles
                                    INNER JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
                                    WHERE pim_orders.order_id = '".$orders->f('order_id')."'
                                    AND tax_for_article_id = '".$orders->f('article_id')."'
                                    AND article_id = '0' AND sent = '1' ");
  while($article_taxes_data->next()){
    switch ($orders->f('apply_discount')) {
        case '0'://no discount

          $tax_total_value = $article_taxes_data->f('quantity') * $article_taxes_data->f('price') * ($article_taxes_data->f('packing')/$article_taxes_data->f('sale_unit'));
          break;
        case '1'://line discount
          $tax_total_value = $article_taxes_data->f('quantity') * $article_taxes_data->f('price') * ( 1 - $article_taxes_data->f('discount')/100 ) * ($article_taxes_data->f('packing')/$article_taxes_data->f('sale_unit'));
          break;
        case '2'://global discount
          $a = $article_taxes_data->f('quantity') * $article_taxes_data->f('price') * $article_taxes_data->f('packing')/$article_taxes_data->f('sale_unit');
          $b = 1 - $orders->f('global_disc')/100;
          $tax_total_value = $a * $b;
          break;
        case '3'://line and global discount
          $line_discount = $article_taxes_data->f('quantity') * $article_taxes_data->f('price') * ( 1 - $article_taxes_data->f('discount')/100 ) * ($article_taxes_data->f('packing')/$article_taxes_data->f('sale_unit'));
          $tax_total_value = $line_discount * ( 1 - $orders->f('global_disc')/100 );
          break;
        default://no discount
          $tax_total_value = $article_taxes_data->f('quantity') * $article_taxes_data->f('price') * ( 1 - 0/100 ) * ($article_taxes_data->f('packing')/$article_taxes_data->f('sale_unit'));
          break;
      }
      $currency_rate = 1;
      if($article_taxes_data->f('currency_rate')){
        $currency_rate = return_value($article_taxes_data->f('currency_rate'));
      }
      $taxes_price+=$tax_total_value*$currency_rate;
  }

  $taxes_total_value = $taxes_total_value+$taxes_price;

  $article_total_quantity += $orders->f('quantity');

	/*$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$orders->f('date')))
				->setCellValue('B'.$xlsRow, $orders->f('customer_name'))
				->setCellValue('C'.$xlsRow, $orders->f('serial_number'))
				->setCellValue('D'.$xlsRow, $orders->f('article_name'))
				->setCellValue('E'.$xlsRow, $orders->f('quantity'))
				->setCellValue('F'.$xlsRow, $total_value)
				->setCellValue('G'.$xlsRow, $taxes_price)
				->setCellValue('H'.$xlsRow, $orders->f('rdy_invoice') == 1 ? gm('Yes') : gm('No') );
  	$xlsRow++;*/
    $tmp_item=array(
            date(ACCOUNT_DATE_FORMAT,$orders->f('date')),
            $orders->f('customer_name'),
            $orders->f('customer_id'),
            $orders->f('serial_number'),
            stripslashes(preg_replace("/[\n\r]/"," ",$orders->f('article_name'))),
            display_number_exclude_thousand($orders->f('quantity')),
            display_number_exclude_thousand($total_value),
            display_number_exclude_thousand($taxes_price),
            $orders->f('rdy_invoice') == 1 ? gm('Yes') : gm('No') 
          );
    array_push($final_data,$tmp_item);

}

/*$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Total'))
				->setCellValue('B'.$xlsRow, '')
				->setCellValue('C'.$xlsRow, '')
				->setCellValue('D'.$xlsRow, '')
				->setCellValue('E'.$xlsRow, $article_total_quantity)
				->setCellValue('F'.$xlsRow, $article_total_value)
				->setCellValue('G'.$xlsRow, $taxes_total_value)
				->setCellValue('H'.$xlsRow, '');*/

/*$tmp_item=array(
            gm('Total'),
            '',
            '',
            '',
            '',
            $article_total_quantity,
            $article_total_value,
            $taxes_total_value,
            '' 
          );
array_push($final_data,$tmp_item);*/


doQueryLog();
   $from_location='upload/'.DATABASE_NAME.'/export_orders_detail_report_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();



$rows_format='F2:G'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');



$objPHPExcel->getActiveSheet()->setTitle('Orders detail report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');




$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>