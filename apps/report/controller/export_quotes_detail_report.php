<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit','2G');

global $database_config;

$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

  $db_users = new sqldb($db_config);
  $account_users=array();
  $account_users_data=$db_users->query("SELECT user_id,first_name,last_name FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();

  foreach($account_users_data as $key=>$value){
    $account_users[$value['user_id']]=htmlspecialchars_decode(stripslashes($value['first_name'].' '.$value['last_name']));
  }


setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Quotes detail report")
							 ->setSubject("Quotes detail report")
							 ->setDescription("Quotes report export")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Quotes");*/
$time_start = $in['time_start'];
$time_end = $in['time_end'];
/*$filename ="quotes_detail_report.xls";*/
$filename ="quotes_detail_report.csv";

$filter = " tblquote_line.article_id != 0 ";
$filter2 = " tblquote.quote_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblquote.f_archived='0' ";

if(!$time_start && !$time_end){
	$filter2 = " 1=1 ";
}


if($in['customer_id']){
	$filter .= "  AND tblquote.buyer_id=".$in['customer_id']."";
}
if($in['u_id']){
 $filter .= ' AND tblquote.author_id ='.$in['u_id'].' ';
}
if($in['manager_id']){
    $filter .= ' AND tblquote.acc_manager_id ='.$in['manager_id'].' ';        
}
if($in['source_id']){
  $filter .= ' AND tblquote.source_id ='.$in['source_id'].' ';
}
if($in['type_id']){
  $filter .= ' AND tblquote.type_id ='.$in['type_id'].' ';
}


/*  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);


$objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A1', gm("Quote Number"))
			->setCellValue('B1', gm("Date"))
			->setCellValue('C1', gm("Client"))
			->setCellValue('D1', gm("Subject"))
			->setCellValue('E1', gm("Author"))
			->setCellValue('F1', gm("Account Manager"))
			->setCellValue('G1', gm("Amount"))
			->setCellValue('H1', gm("Status"));*/

$headers    = array(gm('Quote Number'),
                    gm("Date"),
                    gm('Client'),
                    gm('Company id'),
                    gm("Subject"),
                    gm("Author"),
                    gm("Account Manager"),
                    gm("Amount"),
                    gm("Status")
      );

$quotes =$db->query("SELECT * FROM tblquote WHERE ".$filter2."  ");

$i=0;
 $xlsRow = 2;
 $big_total=0;
 $final_data=array();

while($quotes->next()) {
          $lines = $db->query("SELECT tblquote_line.amount AS a, tblquote.discount, tblquote.apply_discount, tblquote.status_customer, tblquote.acc_manager_id,
           tblquote_version.sent, tblquote_line.line_discount FROM tblquote_line 
                  INNER JOIN tblquote ON tblquote_line.quote_id=tblquote.id 
                  INNER JOIN tblquote_version ON tblquote_line.version_id=tblquote_version.version_id 
                  WHERE tblquote_version.active='1' AND tblquote.id='".$quotes->f('id')."' ");   
          $total=0;
        while($lines->next()){    
            $amount = 0;
            
            $global_disc = $lines->f('discount');
            $disc_line = $lines->f('line_discount');    
            if($lines->f('apply_discount') == 0 || $lines->f('apply_discount') == 2){
              $disc_line = 0;
            }           
            $amount += $lines->f('a') - $lines->f('a')*$disc_line/100;        
            if($lines->f('apply_discount') < 2) {
              $global_disc = 0;
            }   
            $amount = $amount - ($amount*$global_disc/100);     
            if($quotes->f('currency_rate')){
              $amount = $amount*return_value($quotes->f('currency_rate'));     
            }         
            $total += $amount;    
          
        }

              $big_total += $total;  

              $opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
        
              $status=$opts[$quotes->f('status_customer')];
       
              if($quotes->f('sent') == 1 && $quotes->f('status_customer') == 0){
                $status=$opts[5];
              }

      	/*$objPHPExcel->setActiveSheetIndex(0)
      	      ->setCellValue('A'.$xlsRow, $quotes->f('serial_number'))
      				->setCellValue('B'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$quotes->f('quote_date')))
      				->setCellValue('C'.$xlsRow, $quotes->f('buyer_name'))
      				->setCellValue('D'.$xlsRow, $quotes->f('subject'))
      				->setCellValue('E'.$xlsRow, get_user_name($quotes->f('created_by')))
      				->setCellValue('F'.$xlsRow, $quotes->f('acc_manager_name'))
      				->setCellValue('G'.$xlsRow, $total)
      				->setCellValue('H'.$xlsRow, $status);

        	$xlsRow++;*/
          $tmp_item=array(
            $quotes->f('serial_number'),
            date(ACCOUNT_DATE_FORMAT,$quotes->f('quote_date')),
            $quotes->f('buyer_name'),
            $quotes->f('buyer_id'),
            preg_replace("/[\n\r]/"," ",$quotes->f('subject')),
            $account_users[$quotes->f('created_by')],
            $quotes->f('acc_manager_name'),
            display_number_exclude_thousand($total),
            $status
          );
         array_push($final_data,$tmp_item);

      }
/*
$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Total'))
				->setCellValue('B'.$xlsRow, '')
				->setCellValue('C'.$xlsRow, '')
				->setCellValue('D'.$xlsRow, '')
				->setCellValue('E'.$xlsRow, '')
				->setCellValue('F'.$xlsRow, '')
				->setCellValue('G'.$xlsRow, $big_total)
				->setCellValue('H'.$xlsRow, '');*/

/*array_push($final_data,
    array(
       gm('Total'),
       '',
       '',
       '',
       '',
       '',
       '',
       $big_total,
       ''
    )
  );*/

doQueryLog();
   $from_location='upload/'.DATABASE_NAME.'/export_quotes_detail_report_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();




$rows_format='G'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');



$objPHPExcel->getActiveSheet()->setTitle('Quotes detail report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');




$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>