<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit','2G');
setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Articles detail report")
							 ->setSubject("Articles detail report")
							 ->setDescription("Articles report export")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Articles");

$objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);*/

//$filename ="articles_detail_report.xls";
$filename ="articles_detail_report.csv";

$time_start = $in['time_start'];
$time_end = $in['time_end'];
if(!$time_start && !$time_end){
	$filter2 = " AND 1=1 ";
}
if($in['customer_id']){
	$filter.=" and pim_orders.customer_id = ".$in['customer_id'];
}

if($in['article_category_id']){
    $filter.=" and pim_articles.article_category_id = ".$in['article_category_id'];
}

if($in['article_id']){
    $filter.=" and pim_articles.article_id = ".$in['article_id'];
}

/*$objPHPExcel->getActiveSheet(0)->getStyle('A1:H1')->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Article"))
			->setCellValue('B1', gm("Client"))
			->setCellValue('C1', gm("Quantity"))
			->setCellValue('D1', gm("Delivered"))
			->setCellValue('E1', gm("Not delivered"))
      ->setCellValue('F1', gm("Delivered Value"))
      ->setCellValue('G1', gm("Not Delivered Value"))
			->setCellValue('H1', gm("Total Value"));*/

$headers    = array(gm('Article'),
                    gm('Client'),
                    gm('Company id'),
                    gm("Quantity"),
                    gm("Delivered"),
                    gm("Not delivered"),
                    gm("Delivered Value"),
                    gm("Not Delivered Value"),
                    gm("Total Value")
      );

$i=0;
$xlsRow = 2;

$articles_data = $db->query("SELECT
  pim_orders.order_id, pim_orders.customer_id, pim_orders.contact_id, pim_orders.rdy_invoice,
  pim_orders.sent, pim_orders.use_package, pim_orders.use_sale_unit, pim_orders.apply_discount,
  pim_orders.currency_type, pim_orders.currency_rate, pim_orders.discount AS global_discount,

  pim_order_articles.article, pim_order_articles.article_id, pim_order_articles.quantity, pim_order_articles.price,
  pim_order_articles.delivered, pim_order_articles.tax_id, pim_order_articles.packing,
  pim_order_articles.sale_unit, pim_order_articles.discount,

  pim_articles.item_code AS order_item_code,

  customers.name AS order_customer_name

  FROM pim_orders

  LEFT JOIN pim_order_articles
  ON pim_orders.order_id = pim_order_articles.order_id

  LEFT JOIN pim_articles
  ON pim_order_articles.article_id = pim_articles.article_id

  LEFT JOIN customers
  ON pim_orders.customer_id = customers.customer_id

  WHERE date BETWEEN '".$time_start."' AND '".$time_end."' ".$filter." AND pim_orders.active = '1' AND pim_order_articles.article_id != '0'  AND sent ='1'
  ORDER BY order_item_code ASC, order_customer_name ASC");


$total_contacts = array();
$total_companies = array();
$total_contacts2 = array();
$total_companies2 = array();
$art = array();
$total_amount = 0;

while ($articles_data->next()) {
  $del_a = 0;
  $ndel_a = 0;

  $global_disc = $articles_data->f('global_discount');
  $use_package=$articles_data->f('use_package');
  $use_sale_unit=$articles_data->f('use_sale_unit');

  $packing  = $articles_data->f('packing');
  if(!$use_package){
    $packing = 1;
  }
  $sale_unit  = $articles_data->f('sale_unit');
  if(!$use_sale_unit){
    $sale_unit  = 1;
  }
  $discount_line = $articles_data->f('discount');
  if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
    $discount_line = 0;
  }
  if($articles_data->f('apply_discount') < 2){
    $global_disc = 0;
  }
  $q = $articles_data->f('quantity') * $packing / $sale_unit;
  $price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

  $line_total=$price_line * $q;

  if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
    $line_total = $line_total*return_value($articles_data->f('currency_rate'));
  }

  $total_amount+= $line_total - ($line_total*$global_disc/100);

  if($articles_data->f('customer_id')){
    if(!$total_companies[$articles_data->f('article_id')]){
      $total_companies[$articles_data->f('article_id')] = 0;
      $total_companies2[] = $articles_data->f('customer_id');
    }
  }elseif($articles_data->f('contact_id')){
    if(!$total_contacts[$articles_data->f('article_id')]){
      $total_contacts[$articles_data->f('article_id')] = 0;
      $total_contacts2[] = $articles_data->f('contact_id');
    }
  }


  if($articles_data->f('customer_id')){
    $art[$articles_data->f('article_id')]['customer'][$articles_data->f('customer_id')] += $line_total - ($line_total*$global_disc/100);
    $art[$articles_data->f('article_id')]['customerq'][$articles_data->f('customer_id')] += $articles_data->f('quantity');
  }else{
    $art[$articles_data->f('article_id')]['contact'][$articles_data->f('contact_id')] += $line_total - ($line_total*$global_disc/100);
    $art[$articles_data->f('article_id')]['contactq'][$articles_data->f('contact_id')] += $articles_data->f('quantity');
  }
  $art[$articles_data->f('article_id')]['article'] += $line_total - ($line_total*$global_disc/100);
  $art[$articles_data->f('article_id')]['q'] += $articles_data->f('quantity');

  if($articles_data->f('rdy_invoice')==0){

    $ndel_a = $articles_data->f('quantity');



  }elseif($articles_data->f('rdy_invoice')==1){

    $del_a = $articles_data->f('quantity');

    if($articles_data->f('customer_id')){
        $art[$articles_data->f('article_id')]['customerd'][$articles_data->f('customer_id')] += $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['del_val_cust'][$articles_data->f('customer_id')] += $line_total - ($line_total*$global_disc/100);
     }else{
        $art[$articles_data->f('article_id')]['contactd'][$articles_data->f('customer_id')] += $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['del_val_cont'][$articles_data->f('customer_id')] += $line_total - ($line_total*$global_disc/100);
     }

  }else{//$articles_data->f('rdy_invoice')==2

    $total_art_quantity = $articles_data->f('quantity');
    $del_a = $db->field("SELECT SUM(pim_orders_delivery.quantity)
    FROM pim_order_articles LEFT JOIN pim_orders_delivery
    ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
    WHERE pim_order_articles.article_id = '".$articles_data->f('article_id')."' AND pim_orders_delivery.order_id = '".$articles_data->f('order_id')."' ");
    $ndel_a = $total_art_quantity-$del_a;

    if($articles_data->f('customer_id')){
        $art[$articles_data->f('article_id')]['customerd'][$articles_data->f('customer_id')] += $del_a;
        $art[$articles_data->f('article_id')]['del_val_cust'][$articles_data->f('customer_id')] += ($line_total - ($line_total*$global_disc/100))/$q*($del_a* $packing / $sale_unit);
     }else{
        $art[$articles_data->f('article_id')]['contactd'][$articles_data->f('customer_id')] += $del_a;
        $art[$articles_data->f('article_id')]['del_val_cont'][$articles_data->f('customer_id')] += ($line_total - ($line_total*$global_disc/100))/$q*($del_a* $packing / $sale_unit);
     }

  }

  $art[$articles_data->f('article_id')]['delivered'] += $del_a;
  $art[$articles_data->f('article_id')]['ndelivered'] += $ndel_a;

}

$big_quantity = 0;
$big_customers = 0;
$total_del = 0;
$total_ndel = 0;
$total_del_val = 0;
$total_ndel_val = 0;
$final_data=array();
foreach ($art as $art_id => $art_data) {
  $total_contacts[$art_id] = count($art_data['customer']);
  $total_companies[$art_id] = count($art_data['contact']);
  $total_quantity = $art_data['q'];

  $big_quantity+=$total_quantity;
// console::log($total_companies[$art_id]);
  $customers = $total_companies[$art_id]+$total_contacts[$art_id];

  $big_customers+=$customers;

  $article_name = htmlspecialchars_decode($db->field("SELECT internal_name FROM pim_articles WHERE article_id = '".$art_id."' "));
  $article_code = $db->field("SELECT item_code FROM pim_articles WHERE article_id = '".$art_id."' ");

  $delivered_a  = $art_data['delivered'];
  $ndelivered_a   = $art_data['ndelivered'];
  $total_del+=$delivered_a;
  $total_ndel+=$ndelivered_a;
  /*$view->assign(array(
      'TIME_START'    => $in['time_start'],
      'TIME_END'      => $in['time_end'],
      'XPAG'        => $in['pag'],
      'CUSTOM'      => $in['custom'] ? '&custom=1' : '&custom=',
      'ARTICLE_N'     => $article_code.' ('.$article_name.')',
      'ITEM_CODE'     =>  $article_code,
      'TOTAL_QUANTITY'    => $total_quantity,
      'CUSTOMERS'     => $customers,
      'TOTAL_CLIENT'    => place_currency(display_number($art_data['article'])),
      'art_id'      => $art_id,
      'to_collaps'    => $in['customer_id'] ? '' : 'to_collaps',
      'colspan_c_id'    => $in['customer_id'] ? 'colspan="2"' : '',
      'not_c_id'      => $in['customer_id'] ? false : true,
      'DELIVERED_A'   => $delivered_a,
      'NDELIVERED_A'    => $ndelivered_a,
  ),'article_row');*/


  if($art_data['customer']){
    foreach ($art_data['customer'] as $key => $value) {
      $client_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$key."' ");
      $not_delivered = $art_data['customerq'][$key]-$art_data['customerd'][$key];
      $del_val_comp = $art_data['del_val_cust'][$key];
      $ndel_val_comp = $value-$del_val_comp;

      $total_del_val += $del_val_comp;
      $total_ndel_val += $ndel_val_comp;

      /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $article_code.' ('.$article_name.')')
        ->setCellValue('B'.$xlsRow, $client_name)
        ->setCellValue('C'.$xlsRow, $art_data['customerq'][$key])
        ->setCellValue('D'.$xlsRow, $art_data['customerd'][$key])
        ->setCellValue('E'.$xlsRow, $not_delivered)
        ->setCellValue('F'.$xlsRow, $del_val_comp)
        ->setCellValue('G'.$xlsRow, $ndel_val_comp)
        ->setCellValue('H'.$xlsRow, $value);
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
      $xlsRow++;*/

      $tmp_item=array(
            $article_code.' ('.$article_name.')',
            $client_name,
            $key,
            display_number_exclude_thousand($art_data['customerq'][$key]),
            display_number_exclude_thousand($art_data['customerd'][$key]),
            display_number_exclude_thousand($not_delivered),
            display_number_exclude_thousand($del_val_comp),
            display_number_exclude_thousand($ndel_val_comp),
            display_number_exclude_thousand($value)
          );
      array_push($final_data,$tmp_item);

    }
  }
  if($art_data['contact']){
    foreach ($art_data['contact'] as $key => $value) {
      $client_name = $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id = '".$key."' ");

      $del_val_cont = $art_data['del_val_cont'][$key];
      $ndel_val_cont = $value-$del_val_cont;

      $total_del_val += $del_val_cont;
      $total_ndel_val += $ndel_val_cont;

      /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $article_name)
        ->setCellValue('B'.$xlsRow, $client_name)
        ->setCellValue('C'.$xlsRow, $art_data['contactq'][$key])
        ->setCellValue('D'.$xlsRow, $art_data['contactd'][$key])
        ->setCellValue('E'.$xlsRow, $not_delivered)
        ->setCellValue('F'.$xlsRow, $del_val_cont)
        ->setCellValue('G'.$xlsRow, $ndel_val_cont)
        ->setCellValue('H'.$xlsRow, $value);
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
      $xlsRow++;*/
      $tmp_item=array(
            $article_name,
            $client_name,
            $key,
            display_number_exclude_thousand($art_data['contactq'][$key]),
            display_number_exclude_thousand($art_data['contactd'][$key]),
            display_number_exclude_thousand($not_delivered),
            display_number_exclude_thousand($del_val_cont),
            display_number_exclude_thousand($ndel_val_cont),
            display_number_exclude_thousand($value)
          );
      array_push($final_data,$tmp_item);

    }
  }

  /*$view->loop('article_row');*/
  $i++;
}
$total_customers = count(array_count_values($total_companies2))+count(array_count_values($total_contacts2));


/*$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':I'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Total'))
				->setCellValue('B'.$xlsRow, $total_customers)
				->setCellValue('C'.$xlsRow, $big_quantity)
				->setCellValue('D'.$xlsRow, $total_del)
				->setCellValue('E'.$xlsRow, $total_ndel)
        ->setCellValue('F'.$xlsRow, $total_del_val)
        ->setCellValue('G'.$xlsRow, $total_ndel_val)
				->setCellValue('H'.$xlsRow, $total_amount);*/

/*array_push($final_data,
    array(
       gm('Total'),
       $total_customers,
       '',
       display_number($big_quantity),
       display_number($total_del),
       display_number($total_ndel),
       display_number($total_del_val),
       display_number($total_ndel_val),
       display_number($total_amount)
    )
  );*/

doQueryLog();
   $from_location='upload/'.DATABASE_NAME.'/export_articles_detail_report_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();


// set column width to auto
/*foreach(range('A','H') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}*/
/*
$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$xlsRow)->applyFromArray($styleArray);

$styleArray = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_MEDIUM
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$xlsRow)->applyFromArray($styleArray);
*/
$rows_format='F1:H'.$xlsRow;

$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');

$objPHPExcel->getActiveSheet()->setTitle('Articles detail report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>