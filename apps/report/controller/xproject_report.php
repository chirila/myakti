<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class ReportProject extends Controller{
		var $ALLOW_ARTICLE_PACKING;
		var $ALLOW_ARTICLE_SALE_UNIT;
		var $is_manager;

		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);
			$this->ALLOW_ARTICLE_PACKING = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
			$this->ALLOW_ARTICLE_SALE_UNIT = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
			$this->is_manager = $this->db->field("SELECT MAX(manager) FROM project_user WHERE user_id='".$_SESSION['u_id']."'");	
		}
		public function get_Data(){
			$in=$this->in;
			$result=array();

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(0,0,0,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(0,0,0,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(0,0,0,$month,$today,$year);
					}
					$result['period_txt']='';
					break;
			}
			$in['time_start']=$time_start;
			$in['time_end']=$time_end;
			$filter = ' AND 1=1';
			$filterp = ' AND 1=1';
			if($in['customer_id']){
				$filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';	
				$filterp .= ' AND projects.customer_id ='.$in['customer_id'].' ';	
			}
			if($in['user_id']){
				$filter .= ' AND task_time.user_id ='.$in['user_id'].' ';
				$filterp .= ' AND project_user.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter .= ' AND task_time.project_id ='.$in['project_id'].' ';
				$filterp .= ' AND projects.project_id ='.$in['project_id'].' ';
			}else if($in['task_id']){
				$filter .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
				$in['filter'] = 1;
			}

			// get total hours
			$exp = $this->db->query("SELECT SUM(amount) FROM project_expenses INNER JOIN projects ON project_expenses.project_id=projects.project_id INNER JOIN project_user ON project_expenses.project_id=project_user.project_id WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filterp ");
			$this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days, SUM(projects.bunit) as bunits FROM task_time
				INNER JOIN projects ON task_time.project_id=projects.project_id
				 WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter  ");
			$this->db->move_next();
			if($this->db->f('total_h') == 0 && !$exp ){
				//
			}
			else{
				$result['HOURS_TRACKED']	= number_as_hour($this->db->f('total_h'));
				$result['is_bunit']			= $this->db->f('bunits') ? true : false;
				// get total billable hours
				$this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days FROM task_time 
					INNER JOIN projects ON task_time.project_id=projects.project_id
					WHERE task_time.billable='1' AND projects.invoice_method!=0 AND task_time.date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
				$this->db->move_next();
				$result['BILLABLE_H']  = number_as_hour($this->db->f('total_h'));
				$result['BILLABLE_H_NR']=str_replace(':','.',number_as_hour($this->db->f('total_h')));
				$billable_h_nr=$this->db->f('total_h');
				//get total unbillable hours
				$this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days FROM task_time 
					INNER JOIN projects ON task_time.project_id=projects.project_id
					WHERE (task_time.billable='0' OR projects.invoice_method=0) AND date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
				$this->db->move_next();
				$result['UNBILLABLE_H']=number_as_hour($this->db->f('total_h'));
				$result['UNBILLABLE_H_NR']=str_replace(':','.',number_as_hour($this->db->f('total_h')));
				$unbillable_h_nr=$this->db->f('total_h');

				// get total billable amount and uninvoiced amount
				$rates = array();
				/* further optimization */
				/* save the user rate */
				$user_rates = array();
				/* save the user rate */
				/* further optimization */
				$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate, tasks.closed
				 				FROM task_time
								INNER JOIN projects ON task_time.project_id=projects.project_id
								INNER JOIN tasks ON task_time.task_id = tasks.task_id AND tasks.billable='1'
								WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
				//INNER JOIN project_user ON projects.project_id=project_user.project_id
				//,project_user.p_h_rate
				while ($q->next()) {
					// $i++;
					$rate = 0;
					switch ($q->f('billable_type')){
						case '1':
						case '7':
							$rate = 0;
							if($q->f('project_status_rate') == 0){
								if($q->f('t_h_rate')){
									$rate = $q->f('t_h_rate');
								}
							}else{
								if($q->f('t_daily_rate')){
									$rate = $q->f('t_daily_rate');
								}
							}
							break;
						case '2':
							if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
								$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
							}else{
								if($q->f('project_status_rate') == 0){
									$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
								}else{
									$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
								}
								$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
							}
							break;
						case '3':
							$rate = $q->f('pr_h_rate');
							break;
						case '4':
							$rate = 0;
							break;
						case '5':
							$rate = $q->f('closed')==1 ? 1 : 0;
							break;
					}
					$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
				}

				$billable_amount = 0;
				$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
					INNER JOIN projects ON task_time.project_id=projects.project_id
					INNER JOIN tasks ON task_time.task_id=tasks.task_id
					WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND task_time.billable='1' AND projects.invoice_method!=0 GROUP BY task_time.task_id, task_time.user_id, task_time.project_id ");
				while ($a->next()) {
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					if($a->f('billable_type')==5 && $a->f('closed')==1){
						$billable_amount += $a->f('task_budget')*$rate;
					}else{
						$billable_amount += $a->f('total_h')*$rate;
					}	
				}

				$unbillable_amount = 0;
				$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
					INNER JOIN projects ON task_time.project_id=projects.project_id
					INNER JOIN tasks ON task_time.task_id=tasks.task_id
					WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND task_time.billable='1' AND task_time.billed='0' AND projects.invoice_method!=0 GROUP BY task_time.task_id, task_time.user_id, task_time.project_id ");
				while ($a->next()) {
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					if($a->f('billable_type')==5 && $a->f('closed')==1){
						$unbillable_amount += $a->f('task_budget')*$rate;
					}else{
						$unbillable_amount += $a->f('total_h')*$rate;
					}	
				}
				$articles_billable=0;
				$articles_invoice=0;
				$articles=$this->db->query("SELECT SUM(task_time.hours) AS total_h,project_articles.price,project_articles.quantity,project_articles.billed,projects.invoice_method,pim_articles.packing,pim_articles.sale_unit FROM project_articles
					INNER JOIN projects ON project_articles.project_id=projects.project_id
					LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
					INNER JOIN task_time ON projects.project_id=task_time.project_id
					WHERE task_time.date BETWEEN '".$time_start."' AND '".$time_end."' {$filter} GROUP BY project_articles.project_id,project_articles.article_id");
				while($articles->next()){
					if($articles->f('total_h')==0){
						continue;
					}
					$packing_art=($this->ALLOW_ARTICLE_PACKING && $articles->f('packing')>0) ? $articles->f('packing') : 1;
					$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $articles->f('sale_unit')>0) ? $articles->f('sale_unit') : 1;
					$articles_billable+=$articles->f('invoice_method')==1 ? ($articles->f('price')*$articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
					if($articles->f('billed')==0){
						$articles_invoice+=$articles->f('invoice_method')==1 ? ($articles->f('price')*$articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
					}
				}

				$billable_amount+=$articles_billable;
				$unbillable_amount+=$articles_invoice;
				$result['BILLABLE_A'] = place_currency(display_number($billable_amount));
				$result['UNINVOICE_A'] 	= place_currency(display_number($unbillable_amount));
			}
			$result['time_start']=$time_start;
			$result['time_end']=$time_end;
			$result['customers'] = $this->get_cc($in);
			$result['users'] = $this->get_users($in);
			$result['bunits'] = $this->get_bunits($in);
			$result['bunit_tab']=$this->get_bunitsTab($in);
			$result['customer_tab']=$this->get_customersTab($in);
			$result['chart_hours']=array(				
					'label'			=> array(gm('Billable'),gm('Unbillable')),
					'data' 			=> array(number_format($billable_h_nr,2,'.',''),number_format($unbillable_h_nr,2,'.','')),
					'data_show' 	=> array($result['BILLABLE_H'],$result['UNBILLABLE_H']),
				);
			$result['chart_customer']=$this->get_customerChart($in);
			$result['chart_user']=$this->get_userChart($in);

			$this->out=$result;
		}

		public function get_cc(&$in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" (is_admin='0' OR is_admin>0) ";
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			}
			if($in['customer_id']){
				$filter .=" AND customers.customer_id='".$in['customer_id']."' ";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type,cat_id
				FROM customers
				LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
				WHERE $filter
				GROUP BY customers.customer_id			
				ORDER BY name
				LIMIT 50")->getAll();

			$result = array('list'=>array());
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);
				if($value['type']==0){
					$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
				}elseif($value['type']==1){
					$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
				}else{
					$symbol ='';
				}
				$result_line=array(
					"id"					=> $value['cust_id'],
					'symbol'				=> $symbol,
					"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					'zip'					=> $value['zip'] ? $value['zip'] : '',
					'city'					=> $value['city'] ? $value['city'] : '',
					"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
					"right"					=> $value['acc_manager_name'],
					"cat_id"				=> $value['cat_id']
				);
				array_push($result['list'],$result_line);
			}		
			return $result;
		}

		public function get_users(&$in){
			$q = strtolower($in["term"]);

			$filter = '';

			if($q){
				$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
			}
			if($in['user_id']){
				$filter .=" AND users.user_id='".$in['user_id']."' ";
			}

			$data=$this->db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY last_name ");
			while($data->move_next()){
			  $users[$data->f('last_name').' '.$data->f('first_name')]=$data->f('user_id');
			}

			$result = array('list'=>array());
			if($users){
				foreach ($users as $key=>$value) {
					$result_line=array(
						'id'		=> $value,
						'value'		=> htmlspecialchars_decode(stripslashes($key))
					);
					array_push($result['list'], $result_line);
				}
			}
			return $result;
		}

		public function get_bunits(&$in){
			$q = strtolower($in["term"]);

			$filter = '';

			if($q){
				$filter .=" AND name LIKE '%".$q."%'";
			}
			if($in['bunit_id']){
				$filter .=" AND id='".$in['bunit_id']."' ";
			}

			$bunit=$this->db->query("SELECT * FROM project_bunit");
			while($bunit->move_next()){
			  $items[$bunit->f('name')]=$bunit->f('id');
			}

			$result = array('list'=>array());
			if($items){
				foreach ($items as $key=>$value) {
					$result_line=array(
						'id'		=> $value,
						'value'		=> strip_tags($key)
					);
					array_push($result['list'], $result_line);
				}
			}
			return $result;
		}

		public function get_bunitsTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter_m .= ' AND task_time.customer_id ='.$in['customer_id'].' ';		
			}
			if($in['user_id']){
				$filter_m .= ' AND task_time.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter_m .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter_m.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter_m .= ' AND task_time.project_id ='.$in['project_id'].' ';
			}else if($in['task_id']){
				$filter_m .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter_m .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$internal_rates=array();
				/* optimization */
				// get rates
			$rates = array();
				/* further optimization */
				/* save the user rate */
			$user_rates = array();
				/* save the user rate */
				/* further optimization */
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate, tasks.closed
				 				FROM task_time
								INNER JOIN projects ON task_time.project_id=projects.project_id
								INNER JOIN tasks ON task_time.task_id = tasks.task_id AND (tasks.billable='1' OR tasks.billable='0')
								WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
				//INNER JOIN project_user ON projects.project_id=project_user.project_id
				//,project_user.p_h_rate
			while ($q->next()) {
				// $i++;
				$rate = 0;
				$internal_rate=0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('project_status_rate') == 0){
							if($q->f('t_h_rate')){
								$rate = $q->f('t_h_rate');
							}
						}else{
							if($q->f('t_daily_rate')){
								$rate = $q->f('t_daily_rate');
							}
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
								$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							if($q->f('project_status_rate') == 0){
									$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}else{
								$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}	
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
					case '5':
						$rate = $q->f('closed')==1 ? 1 : 0;
						break;
				}
					
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
				$internal_rate=$this->db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
				$internal_rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $internal_rate;
			}
			$billable_amount = array();
			$billable_amount_day=array();
			$cost_internal=array();
			$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id,task_time.customer_id, task_time.project_status_rate, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
					INNER JOIN projects ON task_time.project_id=projects.project_id
					INNER JOIN tasks ON task_time.task_id=tasks.task_id
					WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND task_time.billable='1' AND projects.invoice_method!=0 GROUP BY task_time.task_id, task_time.user_id, task_time.project_id ");
			while ($a->next()) {
				if($a->f('project_status_rate') == 0){
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					if($a->f('billable_type')==5 && $a->f('closed')==1){
						$billable_amount[$a->f('customer_id')] += $a->f('task_budget')*$rate;
					}else{
						$billable_amount[$a->f('customer_id')] += $a->f('total_h')*$rate;	
					}		
				}else{
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					$billable_amount_day[$a->f('customer_id')] += $a->f('total_h')*$rate;
				}
			}
			/* optimization */
			$b = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id,task_time.customer_id, task_time.project_status_rate FROM task_time 
					INNER JOIN projects ON task_time.project_id=projects.project_id
					WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND billable='0' AND projects.invoice_method='0' GROUP BY task_time.task_id, task_time.user_id, task_time.project_id ");
			while ($b->next()) {
				$internal_rate=0;
				if($internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')]){
					$internal_rate=$internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')];
				}	
				$cost_internal[$b->f('customer_id')]+=$b->f('total_h')*$internal_rate;
			}

			$total_hours = 0;
			$total_b_hours = 0;
			$internal_hours=0;
			$total_days = 0;
			$total_b_days = 0;
			$internal_days=0;
			$total_amount = 0;
			$total_cost_internal=0;
			$total_amount_articles=0;
			$total_purchase_amount=0;
			$report = $this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days, project_bunit.name as bunit_name, projects.bunit, 
				SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =0 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable,
				SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =1 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable_days,
				SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =0 THEN task_time.hours ELSE 0 END ) AS internal_hours,
				SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =1 THEN task_time.hours ELSE 0 END ) AS internal_days
			FROM task_time
			INNER JOIN projects ON task_time.project_id=projects.project_id
			INNER JOIN project_bunit ON projects.bunit=project_bunit.id
			WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} GROUP BY project_bunit.id")->getAll();
			foreach ($report as $key => $value) {
				if($value['total_h'] == 0 && $value['total_days'] == 0){
					continue;
				}
				$bil_amount =0;
				$internal_cost=0;
				$purchase_amount=0;
				$article_amount=0;
				$total_hours +=	$value['total_h'];
				$total_b_hours += $value['billable'];
				$internal_hours+=$value['internal_hours'];
				$total_days +=	$value['total_days'];
				$total_b_days += $value['billable_days'];
				$internal_days+=$value['internal_days'];
				$customer_data=$this->db->query("SELECT DISTINCT task_time.customer_id AS c_id, task_time.project_status_rate FROM task_time 
					INNER JOIN projects ON task_time.project_id=projects.project_id AND task_time.customer_id=projects.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND projects.bunit='".$value['bunit']."' ");
				while($customer_data->next()){
					if($value['billable'] || $value['total_h'] || $value['billable_days'] || $value['total_days']){
					}
					if($customer_data->f('project_status_rate')==0){
						$bil_amount += $billable_amount[$customer_data->f('c_id')];
					}else{
						$bil_amount+=$billable_amount_day[$customer_data->f('c_id')];
					}
					if($value['internal_hours']){
						$internal_cost+=$cost_internal[$customer_data->f('c_id')];
					}
				}
				$total_cost_internal+=$internal_cost;
				$project_articles=$this->db->query("SELECT project_articles.price,project_articles.quantity,projects.invoice_method,pim_articles.packing,pim_articles.sale_unit FROM project_articles 
					INNER JOIN projects ON project_articles.project_id=projects.project_id
					LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND projects.bunit='".$value['bunit']."' GROUP BY project_articles.project_id,project_articles.article_id ");
				while($project_articles->next()){
					$packing_art=($this->ALLOW_ARTICLE_PACKING && $project_articles->f('packing')>0) ? $project_articles->f('packing') : 1;
					$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $project_articles->f('sale_unit')>0) ? $project_articles->f('sale_unit') : 1;
					$article_amount+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
					$bil_amount+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
				}
				$total_amount +=$bil_amount;
				$total_amount_articles+=$article_amount;
				$purchases=$this->db->query("SELECT project_purchase.unit_price,project_purchase.margin,projects.invoice_method FROM project_purchase 
					INNER JOIN projects ON project_purchase.project_id=projects.project_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND projects.bunit='".$value['bunit']."' AND project_purchase.billable='1' GROUP BY project_purchase.project_id,project_purchase.project_purchase_id");
				while($purchases->next()){
					$purchase_amount+=$purchases->f('invoice_method')==1 ? (($purchases->f('unit_price')*$purchases->f('margin')/100)+$purchases->f('unit_price')) : 0;
				}
				$total_purchase_amount+=$purchase_amount;
				$other_row=array(
						'BUNIT'				=> $value['bunit_name'],
						'BUNIT_ID'			=> $value['bunit'],
						'TOTAL_HOUR'		=> number_as_hour($value['total_h']),
						'TOTAL_DAY'			=> $value['total_days'],
						'BILLABLE_H2'		=> number_as_hour($value['billable']),
						'BILLABLE_H2_DAY'	=> $value['billable_days'],
						'BILLABLE_A'		=> place_currency(display_number($bil_amount)),
						'INTERNAL_COST'		=> place_currency(display_number($internal_cost)),
						'INTERNAL'			=> number_as_hour($value['internal_hours']),
						'INTERNAL_DAY'		=> $value['internal_days'],	
						'article_amount'	=> place_currency(display_number($article_amount)),
						'purchase_row'		=> place_currency(display_number($purchase_amount)),
						'amount_ord'		=> $bil_amount
				);
				array_push($result['other_row'], $other_row);
				$i++;
			}

			$total_h_w=0;
			$total_days_w=0;
			$billable_w=0;
			$billable_days_w=0;
			$bil_amount_w=0;
			$internal_cost_w=0;
			$internal_hours_w=0;
			$internal_days_w=0;
			$total_w_purchase=0;
			$total_w_article=0;
			$report2 = $this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days, task_time.customer_id AS c_id, task_time.project_status_rate,
				SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =0 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable,
				SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =1 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable_days,
				SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =0 THEN task_time.hours ELSE 0 END ) AS internal_hours,
				SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =1 THEN task_time.hours ELSE 0 END ) AS internal_days
			FROM task_time
			INNER JOIN projects ON task_time.project_id=projects.project_id
			WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND projects.bunit='0' GROUP BY task_time.project_id")->getAll();

			foreach ($report2 as $key => $value) {
				if($value['total_h'] == 0 && $value['total_days'] == 0){
					continue;
				}
				$bil_amount =0;
				$internal_cost=0;
				$purchase_amount_w=0;
				$article_amount_w=0;
				$total_hours +=	$value['total_h'];
				$total_b_hours += $value['billable'];
				$internal_hours+=$value['internal_hours'];
				$total_days +=	$value['total_days'];
				$total_b_days += $value['billable_days'];
				$internal_days+=$value['internal_days'];
				if($value['project_status_rate']==0){
					$bil_amount += $billable_amount[$value['c_id']];
				}else{
					$bil_amount+=$billable_amount_day[$value['c_id']];
				}
				if($value['internal_hours']){
					$internal_cost+=$cost_internal[$value['c_id']];
				}
				$total_cost_internal+=$internal_cost;
				$project_articles=$this->db->query("SELECT project_articles.price,project_articles.quantity,projects.invoice_method,pim_articles.packing,pim_articles.sale_unit FROM project_articles 
					INNER JOIN projects ON project_articles.project_id=projects.project_id 
					LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND projects.bunit='0' GROUP BY project_articles.project_id,project_articles.article_id ");
				while($project_articles->next()){
					$packing_art=($this->ALLOW_ARTICLE_PACKING && $project_articles->f('packing')>0) ? $project_articles->f('packing') : 1;
					$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $project_articles->f('sale_unit')>0) ? $project_articles->f('sale_unit') : 1;
					$article_amount_w+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
					$bil_amount+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
				}
				$total_amount +=$bil_amount;
				$total_amount_articles+=$article_amount_w;
				$total_w_article+=$article_amount_w;
				$purchases=$this->db->query("SELECT project_purchase.unit_price,project_purchase.margin,projects.invoice_method FROM project_purchase 
					INNER JOIN projects ON project_purchase.project_id=projects.project_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND projects.bunit='0' AND project_purchase.billable='1' GROUP BY project_purchase.project_id,project_purchase.project_purchase_id");
				while($purchases->next()){
					$purchase_amount_w+=$purchases->f('invoice_method')==1 ? (($purchases->f('unit_price')*$purchases->f('margin')/100)+$purchases->f('unit_price')) : 0;
				}
				$total_purchase_amount+=$purchase_amount_w;
				$total_w_purchase+=$purchase_amount_w;

				$total_h_w+=$value['total_h'];
				$total_days_w+=$value['total_days'];
				$billable_w+=$value['billable'];
				$billable_days_w+=$value['billable_days'];
				$bil_amount_w+=$bil_amount;
				$internal_cost_w+=$internal_cost;
				$internal_hours_w+=$value['internal_hours'];
				$internal_days_w+=$value['internal_days'];
			}

			$result['TOTAL_HOUR_2']				= number_as_hour($total_h_w);
			$result['TOTAL_DAY_2']				= $total_days_w;
			$result['BILLABLE_H2_2']			= number_as_hour($billable_w);
			$result['BILLABLE_H2_DAY_2']		= $billable_days_w;
			$result['BILLABLE_A_2']				= place_currency(display_number($bil_amount_w));
			$result['INTERNAL_COST_2']			= place_currency(display_number($internal_cost_w));
			$result['INTERNAL_2']				= number_as_hour($internal_hours_w);
			$result['INTERNAL_DAY_2']			= $internal_days_w;
			$result['article_amount_2']			= place_currency(display_number($total_w_article));
			$result['purchase_amount_2']		= place_currency(display_number($total_w_purchase));
			$result['TOTAL_HOURS']				= number_as_hour($total_hours);
			$result['TOTAL_BILLABLE_H2'] 		= number_as_hour($total_b_hours);
			$result['INTERNAL_HOURS']			= number_as_hour($internal_hours);
			$result['TOTAL_DAYS']				= $total_days;
			$result['TOTAL_BILLABLE_H2_DAYS'] 	= $total_b_days;
			$result['INTERNAL_DAYS']			= $internal_days;
			$result['TOTAL_BILLABLE_A']			= place_currency(display_number($total_amount));
			$result['INTERNAL_COSTS_TOTAL']		= place_currency(display_number($total_cost_internal));
			$result['ONLY_XBUNIT']				= count($report2[0])>0 && ($total_h_w>0 || $total_days_w>0)? true : false;
			$result['total_article_amount']		= place_currency(display_number($total_amount_articles));
			$result['total_purchase_amount']	= place_currency(display_number($total_purchase_amount));

			if($in['order_by']=='amount'){
			   $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order);	    
			}

			return $result;
		}

		public function get_customersTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter_m .= ' AND task_time.customer_id ='.$in['customer_id'].' ';	
			}
			if($in['user_id']){
				$filter_m .= ' AND task_time.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter_m .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter_m.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter_m .= ' AND task_time.project_id ='.$in['project_id'].' ';
			}else if($in['task_id']){
				$filter_m .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter_m .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$internal_rates=array();
			/* optimization */
			// get rates
			$rates = array();
			/* further optimization */
			/* save the user rate */
			$user_rates = array();
			/* save the user rate */
			/* further optimization */
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate, tasks.closed
			 				FROM task_time
							INNER JOIN projects ON task_time.project_id=projects.project_id
							INNER JOIN tasks ON task_time.task_id = tasks.task_id AND (tasks.billable='1' OR tasks.billable='0')
							WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
			//INNER JOIN project_user ON projects.project_id=project_user.project_id
			//,project_user.p_h_rate
			while ($q->next()) {
				// $i++;
				$rate = 0;
				$internal_rate=0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('project_status_rate') == 0){
							if($q->f('t_h_rate')){
								$rate = $q->f('t_h_rate');
							}
						}else{
							if($q->f('t_daily_rate')){
								$rate = $q->f('t_daily_rate');
							}
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
							$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							if($q->f('project_status_rate') == 0){
								$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}else{
								$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
					case '5':
						$rate = $q->f('closed')==1 ? 1 : 0;
						break;
				}
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
				$internal_rate=$this->db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
				$internal_rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $internal_rate;
			}
			$billable_amount = array();
			$billable_amount_day=array();
			$cost_internal=array();
			$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id,task_time.customer_id, task_time.project_status_rate, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				INNER JOIN tasks ON task_time.task_id=tasks.task_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND task_time.billable='1' AND projects.invoice_method!=0 GROUP BY task_id, user_id, project_id ");
			while ($a->next()) {
				if($a->f('project_status_rate') == 0){
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					if($a->f('billable_type')==5 && $a->f('closed')==1){
						$billable_amount[$a->f('customer_id')] += $a->f('task_budget')*$rate;
					}else{
						$billable_amount[$a->f('customer_id')] += $a->f('total_h')*$rate;
					}		
				}else{
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					$billable_amount_day[$a->f('customer_id')] += $a->f('total_h')*$rate;
				}	
			}
			/* optimization */

			$b = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id,task_time.customer_id, task_time.project_status_rate FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND billable='0' AND projects.invoice_method='0' GROUP BY task_id, user_id, project_id ");
			while ($b->next()) {
				$internal_rate=0;
				if($internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')]){
					$internal_rate = $internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')];
				}	
				$cost_internal[$b->f('customer_id')]+=$b->f('total_h')*$internal_rate;	
			}


			$total_hours = 0;
			$total_b_hours = 0;
			$internal_hours=0;
			$total_days = 0;
			$total_b_days = 0;
			$internal_days=0;
			$total_amount = 0;
			$total_cost_internal=0;
			$total_amount_articles=0;
			$total_purchase_amount=0;

			$report = $this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days, projects.company_name AS c_name, task_time.customer_id AS c_id, task_time.project_status_rate,
				SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =0 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable,
				SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =1 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable_days,
				SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =0 THEN task_time.hours ELSE 0 END ) AS internal_hours,
				SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =1 THEN task_time.hours ELSE 0 END ) AS internal_days
			FROM task_time
			INNER JOIN projects ON task_time.project_id=projects.project_id
			WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} GROUP BY c_id, c_name")->getAll();
			foreach ($report as $key => $value) {
				if($value['total_h'] == 0 && $value['total_days'] == 0){
					continue;
				}
				$bil_amount =0;
				$internal_cost=0;
				$purchase_amount=0;
				$article_amount=0;
				$total_hours +=	$value['total_h'];
				$total_b_hours += $value['billable'];
				$internal_hours+=$value['internal_hours'];
				$total_days +=	$value['total_days'];
				$total_b_days += $value['billable_days'];
				$internal_days+=$value['internal_days'];

				if($value['project_status_rate']==0){
					$bil_amount += $billable_amount[$value['c_id']];
				}else{
					$bil_amount+=$billable_amount_day[$value['c_id']];	
				}

				if($value['internal_hours']){
					$internal_cost+=$cost_internal[$value['c_id']];
				}
				$total_cost_internal+=$internal_cost;
				$project_articles=$this->db->query("SELECT project_articles.price,project_articles.quantity,projects.invoice_method,pim_articles.packing,pim_articles.sale_unit FROM project_articles 
					INNER JOIN projects ON project_articles.project_id=projects.project_id
					LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id  
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND task_time.customer_id='".$value['c_id']."' GROUP BY project_articles.project_id,project_articles.article_id ");
				while($project_articles->next()){
					$packing_art=($this->ALLOW_ARTICLE_PACKING && $project_articles->f('packing')>0) ? $project_articles->f('packing') : 1;
					$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $project_articles->f('sale_unit')>0) ? $project_articles->f('sale_unit') : 1;
					$article_amount+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
					$bil_amount+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
				}
				$total_amount +=$bil_amount;
				$total_amount_articles+=$article_amount;
				$purchases=$this->db->query("SELECT project_purchase.unit_price,project_purchase.margin,projects.invoice_method FROM project_purchase 
					INNER JOIN projects ON project_purchase.project_id=projects.project_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND task_time.customer_id='".$value['c_id']."' AND project_purchase.billable='1' GROUP BY project_purchase.project_id,project_purchase.project_purchase_id");
				while($purchases->next()){
					$purchase_amount+=$purchases->f('invoice_method')==1 ? (($purchases->f('unit_price')*$purchases->f('margin')/100)+$purchases->f('unit_price')) : 0;
				}
				$total_purchase_amount+=$purchase_amount;
				$other_row=array(
						'CUSTOMER_N'		=> $value['c_name'],
						'CUSTOMER_ID'		=> $value['c_id'],
						'TOTAL_HOUR'		=> number_as_hour($value['total_h']),
						'TOTAL_DAY'			=> $value['total_days'],
						'BILLABLE_H2'		=> number_as_hour($value['billable']),
						'BILLABLE_H2_DAY'		=> $value['billable_days'],
						'BILLABLE_A'		=> place_currency(display_number($bil_amount)),
						'INTERNAL'			=> number_as_hour($value['internal_hours']),
						'INTERNAL_COST'		=> place_currency(display_number($internal_cost)),
						'INTERNAL_DAY'		=> $value['internal_days'],
						'article_amount'	=> place_currency(display_number($article_amount)),
						'purchase_row'		=> place_currency(display_number($purchase_amount)),
						'amount_ord'		=> $bil_amount,
				);
				array_push($result['other_row'], $other_row);
				$i++;
			}

			$result['TOTAL_HOURS']				= number_as_hour($total_hours);
			$result['TOTAL_BILLABLE_H2'] 		= number_as_hour($total_b_hours);
			$result['INTERNAL_HOURS']			= number_as_hour($internal_hours);
			$result['TOTAL_DAYS']				= $total_days;
			$result['TOTAL_BILLABLE_H2_DAYS'] 	= $total_b_days;
			$result['INTERNAL_DAYS']			= $internal_days;
			$result['TOTAL_BILLABLE_A']			= place_currency(display_number($total_amount));
			$result['INTERNAL_COSTS_TOTAL']		= place_currency(display_number($total_cost_internal));
			$result['total_article_amount']		= place_currency(display_number($total_amount_articles));
			$result['total_purchase_amount']	= place_currency(display_number($total_purchase_amount));

			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order);   
			}
			return $result;
		}

		public function get_projectsTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter_m .= ' AND projects.customer_id ='.$in['customer_id'].' ';		
			}
			if($in['user_id']){
				$filter_m .= ' AND task_time.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter_m .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter_m.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter_m .= ' AND task_time.project_id ='.$in['project_id'].' ';
			}else if($in['task_id']){
				$filter_m .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter_m .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}
			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}

			$internal_rates=array();
			/* optimization */
			// get rates
			$rates = array();
			/* further optimization */
			/* save the user rate */
			$user_rates = array();
			/* save the user rate */
			/* further optimization */
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate, tasks.closed
			 				FROM task_time
							INNER JOIN projects ON task_time.project_id=projects.project_id
							INNER JOIN tasks ON task_time.task_id = tasks.task_id AND (tasks.billable='1' OR tasks.billable='0')
							WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
			//INNER JOIN project_user ON projects.project_id=project_user.project_id
			//,project_user.p_h_rate
			while ($q->next()) {
				// $i++;
				$rate = 0;
				$internal_rate=0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('project_status_rate') == 0){
							if($q->f('t_h_rate')){
								$rate = $q->f('t_h_rate');
							}
						}else{
							if($q->f('t_daily_rate')){
								$rate = $q->f('t_daily_rate');
							}
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
							$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							if($q->f('project_status_rate') == 0){
								$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}else{
								$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
					case '5':
						$rate = $q->f('closed')==1 ? 1 : 0;
						break;
				}
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
				$internal_rate=$this->db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
				$internal_rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $internal_rate;
			}
			$billable_amount = array();
			$billable_amount_day=array();
			$cost_internal=array();
			$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, task_time.project_status_rate, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				INNER JOIN tasks ON task_time.task_id=tasks.task_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND task_time.billable='1' AND projects.invoice_method!=0 GROUP BY task_id, user_id, project_id ");
			while ($a->next()) {
				if($a->f('project_status_rate') == 0){
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					if($a->f('billable_type')==5 && $a->f('closed')==1){
						$billable_amount[$a->f('project_id')] += $a->f('task_budget')*$rate;
					}else{
						$billable_amount[$a->f('project_id')] += $a->f('total_h')*$rate;
					}		
				}else{
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					$billable_amount_day[$a->f('project_id')] += $a->f('total_h')*$rate;
				}	
			}
			/* optimization */

			$b = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, task_time.project_status_rate FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND billable='0' AND projects.invoice_method='0' GROUP BY task_id, user_id, project_id ");
			while ($b->next()) {
				$internal_rate=0;
				if($internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')]){
					$internal_rate = $internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')];
				}	
				$cost_internal[$b->f('project_id')]+=$b->f('total_h')*$internal_rate;
			}


			// get info for every customer
			$data=$this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days, projects.name AS p_name, projects.company_name AS c_name, task_time.project_id AS p_id, task_time.customer_id AS c_id,
						SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =0 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable,
						SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =1 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable_days,
						SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =0 THEN task_time.hours ELSE 0 END ) AS internal_hours,
						SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =1 THEN task_time.hours ELSE 0 END ) AS internal_days
						FROM projects
						INNER JOIN task_time ON projects.project_id = task_time.project_id
						WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter $filter_m GROUP BY p_id ");
			$i;
			$total_hours = 0;
			$total_b_hours = 0;
			$internal_hours=0;
			$total_days = 0;
			$total_b_days = 0;
			$internal_days=0;
			$total_amount = 0;
			$total_cost_internal=0;
			$total_amount_articles=0;
			$total_purchase_amount=0;

			while ($data->move_next()) {

				if($data->f('total_h') == 0 && $data->f('total_days') == 0){
					continue;
				}
				$bil_amount =0;
				$internal_cost=0;
				$purchase_amount=0;
				$article_amount=0;
				$total_hours +=	$data->f('total_h');
				$total_b_hours += $data->f('billable');
				$internal_hours+=$data->f('internal_hours');
				$total_days +=	$data->f('total_days');
				$total_b_days += $data->f('billable_days');
				$internal_days+=$data->f('internal_days');
				if($data->f('billable') > 0 && $data->f('total_h') > 0){
					$bil_amount = $billable_amount[$data->f('p_id')];
					
				}
				if($data->f('billable_days') > 0 && $data->f('total_days') > 0){
					$bil_amount=$billable_amount_day[$data->f('p_id')];	
				}
				if($data->f('internal_hours')){
					$internal_cost+=$cost_internal[$data->f('p_id')];
				}

				$total_cost_internal+=$internal_cost;
				$project_articles=$this->db->query("SELECT project_articles.price,project_articles.quantity,projects.invoice_method,pim_articles.packing,pim_articles.sale_unit FROM project_articles 
					INNER JOIN projects ON project_articles.project_id=projects.project_id 
					LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter} {$filter_m} AND task_time.project_id='".$data->f('p_id')."' GROUP BY project_articles.project_id,project_articles.article_id ");
				while($project_articles->next()){
					$packing_art=($this->ALLOW_ARTICLE_PACKING && $project_articles->f('packing')>0) ? $project_articles->f('packing') : 1;
					$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $project_articles->f('sale_unit')>0) ? $project_articles->f('sale_unit') : 1;
					$article_amount+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
					$bil_amount+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
				}
				$total_amount +=$bil_amount;
				$total_amount_articles+=$article_amount;
				$purchases=$this->db->query("SELECT project_purchase.unit_price,project_purchase.margin,projects.invoice_method FROM project_purchase 
					INNER JOIN projects ON project_purchase.project_id=projects.project_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND task_time.project_id='".$data->f('p_id')."' AND project_purchase.billable='1' GROUP BY project_purchase.project_id,project_purchase.project_purchase_id");
				while($purchases->next()){
					$purchase_amount+=$purchases->f('invoice_method')==1 ? (($purchases->f('unit_price')*$purchases->f('margin')/100)+$purchases->f('unit_price')) : 0;
				}
				$total_purchase_amount+=$purchase_amount;

				$other_row=array(
						'PROJECT_N'			=> $data->f('p_name'),
						'CUSTOMER_N'		=> $data->f('c_name'),
						'BILLABLE_H2'		=> number_as_hour($data->f('billable')),
						'BILLABLE_H2_DAY'	=> $data->f('billable_days'),
						'BILLABLE_A'		=> place_currency(display_number($bil_amount)),
						'INTERNAL'			=> number_as_hour($data->f('internal_hours')),
						'INTERNAL_DAY'		=> $data->f('internal_days'),
						'INTERNAL_COST'		=> place_currency(display_number($internal_cost)),
						'CUSTOMER_ID'		=> $data->f('c_id'),
						'COLLSPAN'			=> $colspan,
						'WIDTH'				=> $width,
						'PROJECT_ID'		=> $data->f('p_id'),
						'U_ID'				=> $in['u_id'],
						'T_ID'				=> $in['task_id'],					
						'TOTAL_HOUR'		=> number_as_hour($data->f('total_h')),
						'TOTAL_DAY'			=> $data->f('total_days'),
						'article_amount'	=> place_currency(display_number($article_amount)),
						'purchase_row'		=> place_currency(display_number($purchase_amount)),
						'amount_ord'		=> $bil_amount,
				);
				array_push($result['other_row'], $other_row);
				$i++;
			}

			$result['TOTAL_HOURS']				= number_as_hour($total_hours);
			$result['TOTAL_BILLABLE_H2'] 		= number_as_hour($total_b_hours);
			$result['INTERNAL_HOURS']			= number_as_hour($internal_hours);
			$result['TOTAL_DAYS']				= $total_days;
			$result['TOTAL_BILLABLE_H2_DAYS'] 	= $total_b_days;
			$result['INTERNAL_DAYS']			= $internal_days;
			$result['TOTAL_BILLABLE_A']			= place_currency(display_number($total_amount));
			$result['INTERNAL_COSTS_TOTAL']		= place_currency(display_number($total_cost_internal));
			$result['total_article_amount']		= place_currency(display_number($total_amount_articles));
			$result['total_purchase_amount']	= place_currency(display_number($total_purchase_amount));

			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord);    
			}

			return $result;
		}

		public function get_tasksTab(&$in){
			$result=array('other_row'=>array(),'progress_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$hide = false;
			if($in['customer_id']){
				$filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';
				$hide = true;		
			}
			if($in['user_id']){
				$filter .= ' AND task_time.user_id ='.$in['user_id'].' ';
				$hide = true;	
			}
			if($in['bunit_id']){
				$filter .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter .= ' AND task_time.project_id ='.$in['project_id'].' ';
				$hide = true;
			}else if($in['task_id']){
				$filter .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}

			$internal_rates=array();
			/* optimization */
			// get rates
			$rates = array();
			/* further optimization */
			/* save the user rate */
			$user_rates = array();
			/* save the user rate */
			/* further optimization */
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate, tasks.closed
			 				FROM task_time
							INNER JOIN projects ON task_time.project_id=projects.project_id
							INNER JOIN tasks ON task_time.task_id = tasks.task_id AND (tasks.billable='1' OR tasks.billable='0')
							WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
			//INNER JOIN project_user ON projects.project_id=project_user.project_id
			//,project_user.p_h_rate
			while ($q->next()) {
				// $i++;
				$rate = 0;
				$internal_rate=0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('project_status_rate') == 0){
							if($q->f('t_h_rate')){
								$rate = $q->f('t_h_rate');
							}
						}else{
							if($q->f('t_daily_rate')){
								$rate = $q->f('t_daily_rate');
							}
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
							$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							if($q->f('project_status_rate') == 0){
								$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}else{
								$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
					case '5':
						$rate = $q->f('closed')==1 ? 1 : 0;
						break;
				}
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
				$internal_rate=$this->db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
				$internal_rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $internal_rate;
			}
			$billable_amount = array();
			$billable_amount_day=array();
			$cost_internal=array();
			$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, task_time.project_status_rate, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				INNER JOIN tasks ON task_time.task_id=tasks.task_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND task_time.billable='1' AND projects.invoice_method!=0 GROUP BY task_id, user_id, project_id ");
			while ($a->next()) {
				if($a->f('project_status_rate') == 0){
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					if($a->f('billable_type')==5 && $a->f('closed')==1){
						$billable_amount[$a->f('task_id')] += $a->f('task_budget')*$rate;
					}else{
						$billable_amount[$a->f('task_id')] += $a->f('total_h')*$rate;
					}			
				}else{
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					$billable_amount_day[$a->f('task_id')] += $a->f('total_h')*$rate;
				}
				
			}
			/* optimization */

			$b = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, task_time.project_status_rate FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND billable='0' AND projects.invoice_method='0' GROUP BY task_id, user_id, project_id ");
			while ($b->next()) {
				$internal_rate=0;
				if($internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')]){
					$internal_rate = $internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')];
				}	
				$cost_internal[$b->f('task_id')]+=$b->f('total_h')*$internal_rate;
			}

			// get info for every customer
			$data=$this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days, task_time.task_id, task_time.project_id AS p_id, task_time.customer_id AS c_id, tasks.task_name, projects.name AS p_name,
						SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =0 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable,
						SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =1 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable_days,
						SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =0 THEN task_time.hours ELSE 0 END ) AS internal_hours,
						SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =1 THEN task_time.hours ELSE 0 END ) AS internal_days
						FROM task_time
						INNER JOIN tasks ON task_time.task_id = tasks.task_id
						INNER JOIN projects ON tasks.project_id = projects.project_id
						WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' $filter
						GROUP BY task_time.task_id ");

			$i=0;
			$total_hours = 0;
			$total_b_hours = 0;
			$internal_hours=0;
			$total_days = 0;
			$total_b_days = 0;
			$internal_days=0;
			$total_amount = 0;
			$total_cost_internal=0;
			$colors = array('success', 'info', 'warning', 'danger');
			$status=array('item-status-green','item-status-blue','item-status-yellow','item-status-red');

			while ($data->move_next()) {
				if($i==4){
					$i=0;
				}
				//$rand = $i>10 ?  $i-((10*(substr($i,0,1)))+1) : $i;
				if($data->f('total_h') == 0 && $data->f('total_days') == 0){
					continue;
				}

				$bil_amount =0;
				$internal_cost=0;
				$total_hours +=	$data->f('total_h');
				$total_b_hours += $data->f('billable');
				$internal_hours+=$data->f('internal_hours');
				$total_days +=	$data->f('total_days');
				$total_b_days += $data->f('billable_days');
				$internal_days+=$data->f('internal_days');
				if($data->f('billable') > 0 && $data->f('total_h') > 0){
					$bil_amount = $billable_amount[$data->f('task_id')];
					
				}
				if($data->f('billable_days') > 0 && $data->f('total_days') > 0){
					$bil_amount=$billable_amount_day[$data->f('task_id')];	
				}
				if($data->f('internal_hours')){
					$internal_cost+=$cost_internal[$data->f('task_id')];
				}

				$total_cost_internal+=$internal_cost;
				$total_amount +=$bil_amount;

				$other_row=array(
						'PROJECT_N'						=> $data->f('p_name'),
						'TASK_N'						=> $data->f('task_name'),
						'TOTAL_HOUR'					=> number_as_hour($data->f('total_h')),
						'TOTAL_DAY'						=> $data->f('total_days'),
						'BILLABLE_H2'					=> number_as_hour($data->f('billable')),
						'BILLABLE_H2_DAY'				=> $data->f('billable_days'),
						'BILLABLE_A'					=> place_currency(display_number($bil_amount)),
						'INTERNAL'						=> number_as_hour($data->f('internal_hours')),
						'INTERNAL_COST'					=> place_currency(display_number($internal_cost)),
						'INTERNAL_DAY'					=> $data->f('internal_days'),
						'PROJECT_ID'					=> $data->f('p_id'),
						'HIDE'							=> $hide,
						'COLLSPAN'						=> $colspan,
						'WIDTH'							=> $width,
						'TASK_ID'						=> $data->f('task_id'),
						'CLICK'							=> $click,
						'CUSTOMER_ID'					=> $data->f('c_id'),
						'BG'							=> $bg,
						'U_ID'							=> $in['u_id'],						
						'status'						=> $status[$i],
						'amount_ord'					=> $bil_amount,
				);
				array_push($result['other_row'], $other_row);
				$i++;
			}

			$j=0;
			foreach($result['other_row'] as $key=>$value){
				if($j==4){
					$j=0;
				}
				$progress_row=array(
					'task_name'		=> $value['TASK_N'],
					'value'			=> (float)number_format(correct_val($value['TOTAL_HOUR'])*100/$total_hours,2,'.',''),
					'type'			=> $colors[$j],
				);
				array_push($result['progress_row'], $progress_row);
				if($progress_row['value']>0){
					$j++;	
				}
			}

			$result['HIDE']						= $hide;
			$result['TOTAL_HOURS']				= number_as_hour($total_hours);
			$result['TOTAL_BILLABLE_H2'] 		= number_as_hour($total_b_hours);
			$result['INTERNAL_HOURS']			= number_as_hour($internal_hours);
			$result['TOTAL_DAYS']				= $total_days;
			$result['TOTAL_BILLABLE_H2_DAYS'] 	= $total_b_days;
			$result['INTERNAL_DAYS']			= $internal_days;
			$result['TOTAL_BILLABLE_A']			= place_currency(display_number($total_amount));
			$result['INTERNAL_COSTS_TOTAL']		= place_currency(display_number($total_cost_internal));

			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord);   
			}

			return $result;
		}

		public function get_moreUserReport(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";
			if($in['task_id']){
				$columns = ' project_user.user_id AS u_id, project_user.user_name AS u_name, task_time.customer_id AS c_id, task_time.project_id AS p_id ';
				$table = ' project_user INNER JOIN task_time ON project_user.user_id = task_time.user_id AND project_user.project_id = task_time.project_id ';
				$filter = " AND task_time.task_id='".$in['task_id']."'  ";
			}
			if($in['user_id']){
				if($in['customer_id']){
					$columns = ' task_time.project_id AS p_id, task_time.customer_id AS c_id, projects.name AS t_name, task_time.task_id AS t_id,tasks.task_name AS ts_name ';
					$table = ' task_time INNER JOIN projects ON task_time.project_id = projects.project_id 
							INNER JOIN tasks ON task_time.task_id = tasks.task_id  ';
					$filter = " AND task_time.customer_id ='".$in['customer_id']."' AND task_time.user_id='".$in['user_id']."' ";
					$group = ' GROUP BY task_time.project_id ';
					if($in['project_id']){
						$filter .= " AND task_time.project_id ='".$in['project_id']."' ";	
					}
				}
				else{
					$columns = ' task_time.task_id AS t_id, task_time.customer_id AS c_id, tasks.task_name AS t_name, task_time.project_id AS p_id ';
					$table = ' task_time INNER JOIN tasks ON task_time.task_id = tasks.task_id ';
					$filter = " AND  task_time.user_id ='".$in['user_id']."' AND task_time.project_id ='".$in['project_id']."' ";
				}
			}
			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}
			$internal_rates=array();
			/* optimization */
			// get rates
			$rates = array();
			/* further optimization */
			/* save the user rate */
			$user_rates = array();
			/* save the user rate */
			/* further optimization */
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate, tasks.closed
			 				FROM task_time
							INNER JOIN projects ON task_time.project_id=projects.project_id
							INNER JOIN tasks ON task_time.task_id = tasks.task_id AND (tasks.billable='1' OR tasks.billable='0')
							WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
			//INNER JOIN project_user ON projects.project_id=project_user.project_id
			//,project_user.p_h_rate
			while ($q->next()) {
				// $i++;
				$rate = 0;
				$internal_rate=0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('project_status_rate') == 0){
							if($q->f('t_h_rate')){
								$rate = $q->f('t_h_rate');
							}
						}else{
							if($q->f('t_daily_rate')){
								$rate = $q->f('t_daily_rate');
							}
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
							$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							if($q->f('project_status_rate') == 0){
								$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}else{
								$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
					case '5':
						$rate = $q->f('closed')==1 ? 1 : 0;
						break;
				}
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
				$internal_rate=$this->db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
				$internal_rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $internal_rate;
			}

			$data=$this->db->query("SELECT DISTINCT $columns FROM $table WHERE $filter_date $filter  ");
			$i;
			while($data->move_next()){
				$amount = 0;
				$amount_internal=0;
				$task_id = $in['user_id'] ? $data->f('t_id') : $in['task_id'];
				$user_id = $in['user_id'] ? $in['user_id'] : $data->f('u_id');
				/*if($in['customer_id']){
					$filter2 = ' AND project_id='.$data->f('p_id');
					// $task_id = 0;
				}
				else{*/
					$filter2 = " AND task_time.task_id='".$task_id."' ";
				// }	
			  $data2=$this->db->query("SELECT SUM(CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END) AS hour, SUM(CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END) AS day, projects.invoice_method, task_time.project_status_rate, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
			  	INNER JOIN projects ON task_time.project_id=projects.project_id
			  	INNER JOIN tasks ON task_time.task_id=tasks.task_id
			  	WHERE $filter_date $filter2 AND user_id='".$user_id."'  ");
			  $data2->move_next();
			  if($data2->f('hour')==0 && $data2->f('day')==0){
				continue;
			  }
			  $total_h = $data2->f('hour');
			  $total_day=$data2->f('day');
			  $total_budget=$data2->f('task_budget');
			  if($data2->f('project_status_rate') ==0){
			  	if($data2->f('billable_type')==5 && $data2->f('closed')==1){
			  		$amount = $total_budget * $rates[$task_id][$user_id][$data->f('p_id')];
			  	}else{
			  		$amount = $total_h * $rates[$task_id][$user_id][$data->f('p_id')];
			  	}
			  }else{
			  	$amount = $total_day * $rates[$task_id][$user_id][$data->f('p_id')];
			  }
			  $amount_internal=0;
			  if($data2->f('invoice_method')==0){
			  	$amount =0;
			  	$amount_internal=$total_h * $internal_rates[$task_id][$user_id][$data->f('p_id')];
			  }
			  $data3=$this->db->query("SELECT SUM(CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END) AS houre, SUM(CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END) AS days FROM task_time 
			  	INNER JOIN projects ON task_time.project_id=projects.project_id
			  	WHERE $filter_date $filter2 AND user_id='".$user_id."' AND billable='1' AND projects.invoice_method!=0");
			  $data3->move_next();
			  if($data3->f('houre') == null && $data3->f('days') == null){
			  	$total_b_h = '0';
			  	$total_days='0';
			  }
			  else{
			  	$total_b_h = $data3->f('houre');
			  	$total_days=$data3->f('days');
			  }
			  
			  $other_row=array(
					'STAFF_N'			=> $in['user_id'] ? ($in['customer_id'] ? $data->f('t_name').' - '.$data->f('ts_name') : $data->f('t_name') ) : $data->f('u_name'),
					'TOTAL_HOUR'		=> number_as_hour($total_h),
					'TOTAL_DAY'			=> $total_day,
					'BILLABLE_H2'		=> number_as_hour($total_b_h),
					'BILLABLE_H2_DAY'	=> $total_days,
					'INTERNAL'			=> number_as_hour($this->db->field("SELECT SUM(CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =0 THEN task_time.hours ELSE 0 END) FROM task_time 
										INNER JOIN projects ON task_time.project_id = projects.project_id
										WHERE $filter_date $filter ")),
					'INTERNAL_DAY'		=> $this->db->field("SELECT SUM(CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =1 THEN task_time.hours ELSE 0 END) FROM task_time 
										INNER JOIN projects ON task_time.project_id = projects.project_id
										WHERE $filter_date $filter "),
					'BILLALBE_A'		=> place_currency(display_number($amount)),
					'INTERNAL_COST'		=> place_currency(display_number($amount_internal)),
					'USER_ID'			=> $in['user_id'] ? $in['user_id'] : $data->f('u_id'),
					'PROJECT_ID'		=> $data->f('p_id'),
					'TASK_ID'			=> $in['task_id'] ? $in['task_id'] : $data->f('t_id'),
					'CUSTOMER_ID'		=> $data->f('c_id'),
					'amount_ord'		=> $amount,

				);
			  	array_push($result['other_row'], $other_row);
				$i++;
			}
			if($in['order_by']=='amount'){
			    if($ord ==' ASC '){
				       $exo = array_sort($result['other_row'], $in['order_by'].'_ord', SORT_ASC);    
				    }else{
				       $exo = array_sort($result['other_row'], $in['order_by'].'_ord', SORT_DESC);
				    }
				$exo = array_slice( $exo, $offset*$l_r, $l_r);
				$result['other_row']=array();
				foreach ($exo as $key => $value) {
				    array_push($result['other_row'], $value);
				}    
			}
			return $result;
		}

		public function get_staffTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter_expense = ' 1=1 ';
			$hide = false;
			if($in['customer_id']){
				$filter .= ' AND projects.customer_id ='.$in['customer_id'].' ';
				$hide = true;		
			}
			if($in['user_id']){
				$filter_m .= ' AND task_time.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter .= ' AND projects.project_id ='.$in['project_id'].' ';
				$filter_expense = ' project_expenses.project_id='.$in['project_id'].' ';
				$hide = true;	
			}else if($in['task_id']){
				$filter .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}
			$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}

			$internal_rates=array();
			/* optimization */
			// get rates
			$rates = array();
			/* further optimization */
			/* save the user rate */
			$user_rates = array();
			/* save the user rate */
			/* further optimization */
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate, tasks.closed
			 				FROM task_time
							INNER JOIN projects ON task_time.project_id=projects.project_id
							INNER JOIN tasks ON task_time.task_id = tasks.task_id AND (tasks.billable='1' OR tasks.billable='0')
							WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
			//INNER JOIN project_user ON projects.project_id=project_user.project_id
			//,project_user.p_h_rate
			while ($q->next()) {
				// $i++;
				$rate = 0;
				$internal_rate=0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('project_status_rate') == 0){
							if($q->f('t_h_rate')){
								$rate = $q->f('t_h_rate');
							}
						}else{
							if($q->f('t_daily_rate')){
								$rate = $q->f('t_daily_rate');
							}
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
							$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							if($q->f('project_status_rate') == 0){
								$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}else{
								$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
					case '5':
						$rate = $q->f('closed')==1 ? 1 : 0;
						break;
				}
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
				$internal_rate=$this->db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
				$internal_rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $internal_rate;
			}

			$billable_amount = array();
			$billable_amount_day=array();
			$cost_internal=array();
			$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, task_time.project_status_rate, projects.billable_type, tasks.closed, tasks.task_budget FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				INNER JOIN tasks ON task_time.task_id=tasks.task_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' $filter $filter_m AND task_time.billable='1' AND projects.invoice_method!=0 GROUP BY task_id, user_id, project_id ");
			while ($a->next()) {
				if($a->f('project_status_rate') == 0){
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					if($a->f('billable_type')==5 && $a->f('closed')==1){
						$billable_amount[$a->f('user_id')] += $a->f('task_budget')*$rate;
					}else{
						$billable_amount[$a->f('user_id')] += $a->f('total_h')*$rate;
					}			
				}else{
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					$billable_amount_day[$a->f('user_id')] += $a->f('total_h')*$rate;
				}
				//$t_hours[$a->f('user_id')] += $a->f('total_h');
			}

			$b = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, task_time.project_status_rate FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' $filter $filter_m AND billable='0' AND projects.invoice_method='0' GROUP BY task_id, user_id, project_id ");
			while ($b->next()) {		
				$internal_rate=0;
				if($internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')]){
					$internal_rate = $internal_rates[$b->f('task_id')][$b->f('user_id')][$b->f('project_id')];
				}	
				$cost_internal[$b->f('user_id')]+=$b->f('total_h')*$internal_rate;
			}

			// get info for every customer

			$e = $this->db->query("SELECT DISTINCT project_user.user_id AS u_id
							FROM project_user
							INNER JOIN task_time ON project_user.user_id = task_time.user_id
							INNER JOIN projects ON task_time.project_id = projects.project_id
							WHERE $filter_date $filter $filter_m  ");
			$i;
			$total_hours = 0;
			$total_b_hours = 0;
			$internal_hours=0;
			$total_days = 0;
			$total_b_days = 0;
			$internal_days=0;
			$total_amount = 0;
			$total_amount_ex = 0;
			$total_cost_internal=0;
			while ($e->next()) {
				$data=$this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, 
					SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days,
					SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =0 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable,
					SUM( CASE WHEN task_time.billable =1 AND task_time.project_status_rate =1 AND projects.invoice_method =1 THEN task_time.hours ELSE 0 END ) AS billable_days,
					SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =0 THEN task_time.hours ELSE 0 END ) AS internal_hours,
					SUM( CASE WHEN projects.invoice_method =0 AND task_time.project_status_rate =1 THEN task_time.hours ELSE 0 END ) AS internal_days
					FROM task_time
					INNER JOIN projects ON task_time.project_id = projects.project_id
					WHERE $filter_date $filter $filter_m AND task_time.user_id='".$e->f('u_id')."' ");

				if($data->f('total_h') == 0 && $data->f('total_days') == 0){
					continue;
				}
				$bil_amount =0;
				$internal_cost=0;
				$total_hours +=	$data->f('total_h');
				$total_b_hours += $data->f('billable');
				$internal_hours+=$data->f('internal_hours');
				$total_days +=	$data->f('total_days');
				$total_b_days += $data->f('billable_days');
				$internal_days+=$data->f('internal_days');
				if(($data->f('billable') > 0 && $data->f('total_h') > 0) || ($data->f('billable_days') > 0 && $data->f('total_days') > 0)){
					$bil_amount += $billable_amount[$e->f('u_id')];
					$bil_amount+=$billable_amount_day[$e->f('u_id')];	
				}
				if($data->f('internal_hours')){
					$internal_cost+=$cost_internal[$e->f('u_id')];
				}
				
				$total_cost_internal+=$internal_cost;
				$total_amount +=$bil_amount;
				//if the username is changed than the list is fucked up because we will have 2 users with the same user_id
				// so we just extract the username separate
				$u_name = $this->db->field("SELECT user_name FROM project_user WHERE user_id = '".$e->f('u_id')."' ORDER BY project_user_id DESC LIMIT 1 ");

				$expenses_data = $this->db->query("SELECT amount , unit_price 
									FROM project_expenses 
									INNER JOIN expense ON project_expenses.expense_id = expense.expense_id 
									INNER JOIN projects ON project_expenses.project_id=projects.project_id
									WHERE $filter_date AND $filter_expense $filter_m AND project_expenses.user_id = '".$e->f('u_id')."' ");	
				$total_expense = 0;
				while($expenses_data->next()){
					$expenses_amount = $expenses_data->f('amount');
					if($expenses_data->f('unit_price')){
						$expenses_amount = $expenses_data->f('amount')*$expenses_data->f('unit_price');
					}	
					$total_expense+= $expenses_amount;		
				}
				$total_amount_ex +=$total_expense;

				$other_row=array(
						'CUSTOMER_N'		=> $u_name,
						'TOTAL_HOUR'		=> number_as_hour($data->f('total_h')),
						'TOTAL_DAY'			=> $data->f('total_days'),
						'BILLABLE_H2'		=> number_as_hour($data->f('billable')),
						'BILLABLE_H2_DAY'	=> $data->f('billable_days'),
						'BILLABLE_A'		=> place_currency(display_number($bil_amount)),
						'INTERNAL'			=> number_as_hour($data->f('internal_hours')),
						'INTERNAL_DAY'		=> $data->f('internal_days'),
						'INTERNAL_COST'		=> place_currency(display_number($internal_cost)),
						'CLICK'				=> $click,
						'USER_ID'			=> $e->f('u_id'),
						'C_ID'				=> $c_id,
						'P_ID'				=> $p_id,
						'P_ID_REL'			=> $p_id,
						'T_ID'				=> $t_id,
						'NAV'				=> $time_nav,
						'EXPENSES_AMOUNT'	=> place_currency(display_number($total_expense)),
						'TIME_START'		=> $time_start,
						'TIME_END'			=> $time_end,
						'period_id'			=> $in['period_id'],
						'amount_ord'		=> $bil_amount
				);
				array_push($result['other_row'], $other_row);
				$i++;
			}

			$result['HIDE']						= $hide;
			$result['TOTAL_HOURS']				= number_as_hour($total_hours);
			$result['TOTAL_BILLABLE_H2'] 		= number_as_hour($total_b_hours);
			$result['INTERNAL_HOURS']			= number_as_hour($internal_hours);
			$result['TOTAL_DAYS']				= $total_days;
			$result['TOTAL_BILLABLE_H2_DAYS'] 	= $total_b_days;
			$result['INTERNAL_DAYS']			= $internal_days;
			$result['TOTAL_BILLABLE_A']			= place_currency(display_number($total_amount));
			$result['TOTAL_EXPENSES']			= place_currency(display_number($total_amount_ex));	
			$result['INTERNAL_COSTS_TOTAL']		= place_currency(display_number($total_cost_internal));
			$result['PROJECT_ID']				= $project;

			if($in['order_by']=='amount'){
			   $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord);   
			}
			return $result;
		}

		public function get_productsTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter_m .= ' AND projects.customer_id ='.$in['customer_id'].' ';		
			}
			if($in['user_id']){
				$filter_m .= ' AND task_time.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter_m .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter_m.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter_m .= ' AND task_time.project_id ='.$in['project_id'].' ';
			}else if($in['task_id']){
				$filter_m .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter_m .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}
			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}

			$article_total_quant=0;
			$article_total_purchase=0;
			$article_total_price=0;
			$total_amount = 0;
			$project_articles=$this->db->query("SELECT project_articles.article_id,SUM(task_time.hours) AS total_h FROM project_articles 
				INNER JOIN projects ON project_articles.project_id=projects.project_id 
				INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
				WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} GROUP BY project_articles.article_id ");
			while($project_articles->next()){
				if($project_articles->f('total_h')==0){
					continue;
				}
				$bil_amount=0;
				$quantity=0;
				$article_name=$this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$project_articles->f('article_id')."'");
				$article_data=$this->db->query("SELECT project_articles.quantity,project_articles.price,SUM(task_time.hours) AS total_h,projects.invoice_method,pim_articles.packing,pim_articles.sale_unit FROM project_articles 
					INNER JOIN projects ON project_articles.project_id=projects.project_id 
					LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id 
					INNER JOIN task_time ON projects.project_id=task_time.project_id AND projects.customer_id=task_time.customer_id
					WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} AND project_articles.article_id='".$project_articles->f('article_id')."' GROUP BY projects.project_id,project_articles.article_id");
				
				while($article_data->next()){
					if($article_data->f('total_h')==0){
						continue;
					}
					$packing_art=($this->ALLOW_ARTICLE_PACKING && $article_data->f('packing')>0) ? $article_data->f('packing') : 1;
					$sale_unit_art=($this->ALLOW_ARTICLE_SALE_UNIT && $article_data->f('sale_unit')>0) ? $article_data->f('sale_unit') : 1;
					$quantity+=$article_data->f('quantity');
					$bil_amount+=$article_data->f('invoice_method')==1 ? ($article_data->f('price')*$article_data->f('quantity')*($packing_art/$sale_unit_art)) : 0;
				}
				$total_amount+=$bil_amount;
				$article_total_quant+=$quantity;
				$other_row=array(
					'name'				=> $article_name,
					'art_quantity'		=> display_number($quantity),
					'art_bill_amount'	=> place_currency(display_number($bil_amount)),
					'amount_ord'		=> $bil_amount
				);
				array_push($result['other_row'], $other_row);
				$i++;
			}

			$result['TOTAL_QUANTITY']	= display_number($article_total_quant);	
			$result['TOTAL_BILL_ART']	= place_currency(display_number($total_amount));
			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord);    
			}
			return $result;	
		}

		public function get_expensesTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter .= ' AND projects.customer_id ='.$in['customer_id'].' ';		
			}
			if($in['user_id']){
				$filter .= ' AND project_expenses.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}
			
			if($in['project_id']){
				$filter .= ' AND projects.project_id ='.$in['project_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}
			$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}

			$total_amount_p = 0;
			$total_internal=0;
			$expense = $this->db->query("SELECT SUM( amount ) AS amount , unit_price, projects.name AS p_name, expense.name AS e_name, projects.project_id AS p_id, project_expenses.expense_id, projects.invoice_method,projects.customer_id
				FROM project_expenses
				INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
				INNER JOIN projects ON project_expenses.project_id = projects.project_id WHERE $filter_date $filter
				GROUP BY project_expenses.project_id, project_expenses.expense_id ");
			while ($expense->next()) {
				$amount = $expense->f('amount');
				$internal=0;
				if($expense->f('unit_price')){
					$amount = $expense->f('amount')*$expense->f('unit_price');
					if($expense->f('invoice_method') == 0){
						$internal=$amount;
						$total_internal+=$internal;
					}
				}
				$total_amount_p +=$amount;
				$other_row=array(
					'TASK_N'			=> $expense->f('e_name'),
					'PROJECT_N'			=> $expense->f('p_name'),
					'BILLABLE_A'		=> place_currency(display_number($amount)),
					'INTERNAL'			=> place_currency(display_number($internal)),
					'PROJECT_ID'		=> $expense->f('p_id'),
					'TASK_ID'			=> $expense->f('expense_id'),
					'CUSTOMER_ID'		=> $expense->f('customer_id'),	
					'amount_ord'		=> $amount,	
				);
				array_push($result['other_row'], $other_row);
			}

			$result['TOTAL_BILLABLE_A']		= place_currency(display_number($total_amount_p));
			$result['TOTAL_INTERNAL']		= place_currency(display_number($total_internal));
			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord);    
			}
			return $result;
		}

		public function get_moreExpReport(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter .= ' AND projects.customer_id ='.$in['customer_id'].' ';		
			}
			if($in['user_id']){
				$filter .= ' AND project_user.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}
			if($in['project_id']){
				$filter .= ' AND projects.project_id ='.$in['project_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}
			$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";
			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}
			$expense_user_data = $this->db->query("	SELECT SUM(project_expenses.amount) AS amount, expense.unit_price, project_expenses.user_id, project_user.user_name, projects.invoice_method FROM project_expenses 
						LEFT JOIN project_user ON project_expenses.user_id = project_user.user_id AND project_expenses.project_id = project_user.project_id
						LEFT JOIN expense ON project_expenses.expense_id = expense.expense_id
						LEFT JOIN projects ON project_expenses.project_id = projects.project_id
						WHERE 	project_expenses.project_id = '".$in['project_id']."' 
						AND  		project_expenses.expense_id = '".$in['task_id']."'
						AND ".$filter_date.$filter." ");
			while($expense_user_data->next()){
				$amount = $expense_user_data->f('amount');
				$internal=0;
				if($expense_user_data->f('unit_price')){
					$amount = $expense_user_data->f('amount')*$expense_user_data->f('unit_price');
					if($expense_user_data->f('invoice_method') == 0){
						$internal=$amount;
					}
				}	
				$other_row=array(
					'USER_ID'			=> $expense_user_data->f('user_id'),
					'user_name'			=> $expense_user_data->f('user_name'),
					'amount'			=> place_currency(display_number($amount)),
					'amount_internal'	=> place_currency(display_number($internal)),	
					'amount_ord'		=> $amount,	
				);
				array_push($result['other_row'], $other_row);
			}
			
			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord);   
			}
			return $result;
		}

		public function get_ReportDetailed(){
			$in=$this->in;
			$result=array('time_row'=>array(),'p_hours_row'=>array());

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(0,0,0,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(0,0,0,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(0,0,0,$month,$today,$year);
					}
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
			}
			$filter_date = " AND task_time.date BETWEEN '".$time_start."' AND '".$time_end."' ";
			$filter = '1=1';
			$customer_name = gm('All customers');
			$project_name = gm('All projects');
			$task_name = gm('All tasks');
			$staff_name = gm('All staff');
			$bunit_name=gm('All business units');
			$filter2 = $filter_date;

			if($in['customer_id'] || $in['customer_id2']){
				$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
				$filter = " task_time.customer_id='".$cid2."' ";
				$project_name = gm('All projects');
				$task_name = gm('All tasks');
				$staff_name = gm('All staff');
				$filter_project = " AND customer_id='".$cid2."' ";
				if($in['project_id'] || $in['project_id2']){
					$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
					$filter_task = " AND project_id='".$pid2."' ";
					$filter_user = " AND project_id='".$pid2."' ";
					$filter_project = " AND project_id='".$pid2."' ";
					$filter_p = " AND task_time.project_id='".$pid2."' ";
					$project_name = $this->db->field("SELECT name FROM projects WHERE customer_id='".$cid2."' AND project_id='".$pid2."' ");
					if($in['task_id'] || $in['task_id2']){
						$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];
						$filter_task = " AND task_id='".$tid2."' ";
						$filter_t = " AND task_time.task_id='".$tid2."' ";
						$task_name = $this->db->field("SELECT task_name FROM tasks WHERE project_id='".$pid2."' AND task_id='".$tid2."' ");
					}
					if($in['user_id'] || $in['user_id2']){
						$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
						$filter_user = " AND user_id='".$uid2."' ";
						$filter_u = " AND task_time.user_id='".$uid2."' ";
						$staff_name = $this->db->field("SELECT user_name FROM project_user WHERE project_id='".$pid2."' AND user_id='".$uid2."' ");
					}
				}
				if($in['user_id'] || $in['user_id2']){
					$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
					$filter_user = " AND user_id='".$uid2."' ";
					$filter_u = " AND task_time.user_id='".$uid2."' ";
					$staff_name = $this->db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' LIMIT 1 ");
				}
				$customer_name = $this->db->field("SELECT name FROM customers WHERE customer_id='".$cid2."' ");
			}
			elseif ($in['user_id'] || $in['user_id2']){
				$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
				$filter_user = " AND user_id='".$uid2."' ";
				$filter = " task_time.user_id='".$uid2."' ";
				$staff_name = $this->db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' ");
				if($in['customer_id'] || $in['customer_id2']){
					$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
					$customer_name = $this->db->field("SELECT name FROM customers WHERE customer_id='".$cid2."' ");
					$filter .= " AND task_time.customer_id='".$cid2."' ";
				}
				if($in['project_id'] || $in['project_id2']){
					$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
					$filter_task = " AND project_id='".$pid2."' ";
					$filter_project = " AND project_id='".$pid2."' ";
					$filter_user = " AND project_id='".$pid2."' ";
					$filter_p = " AND task_time.project_id='".$pid2."' ";
					$customer_name = $db->field("SELECT projects.company_name FROM projects WHERE projects.project_id='".$pid2."' ");
					$project_name = $db->field("SELECT name FROM projects WHERE project_id='".$pid2."' ");
					$filter .= " AND task_time.project_id='".$pid2."' ";
				}
				if($in['task_id'] || $in['task_id2']){
					$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];
					$filter_task = " AND task_id='".$tid2."' ";
					$task_name = $db->field("SELECT task_name FROM tasks WHERE task_id='".$tid2."' ");
					$customer_name = $db->field("SELECT projects.company_name FROM projects INNER JOIN task_time on projects.project_id=task_time.project_id WHERE task_time.task_id='".$tid2."' ");
					$project_name = $db->field("SELECT name FROM projects INNER JOIN task_time ON projects.project_id=task_time.project_id WHERE task_id='".$tid2."' ");
					$filter .= " AND task_time.task_id='".$tid2."' ";
				}
			}
			elseif($in['project_id'] || $in['project_id2']){
				$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
				$filter_user = " AND project_id='".$pid2."' ";
				$filter_task = " AND project_id='".$pid2."' ";
				$filter_p = " AND task_time.project_id='".$pid2."' ";
				$filter_project = " AND project_id='".$pid2."' ";
				$project_name = $this->db->field("SELECT name FROM projects WHERE project_id='".$pid2."' ");
			}
			elseif ($in['task_id'] || $in['task_id2']){
				$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];
				$filter_task = " AND task_id='".$tid2."' ";
				$filter_t = " AND task_time.task_id='".$tid2."' ";
				$task_name = $this->db->field("SELECT task_name FROM tasks WHERE task_id='".$tid2."' ");
			}
			elseif ($in['user_id'] || $in['user_id2']){
				$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
				$filter_user = " AND user_id='".$uid2."' ";
				$filter = " task_time.user_id='".$uid2."' ";
				$staff_name = $this->db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' ");
			}else if($in['bunit_id'] || $in['bunit_id2']){
				$bid2=$in['bunit_id2'] ? $in['bunit_id2'] : $in['bunit_id'];
				//$filter_user = " AND user_id='".$in['u_id']."' ";
				$filter_b= " AND projects.bunit='".$bid2."' ";
				$bunit_name=$this->db->field("SELECT name FROM project_bunit WHERE id='".$bid2."' ");
			}

			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}

			/* optimization */
			$data = array();
			$limit = 5000;	# suggested number 5000
			$loffset = 0;
			$exec_loop = false;
			do {
				$k = 0;
				$exec_loop = false;
				$query = $this->db->query("SELECT task_time.user_id, task_time.task_id, task_time.task_time_id, task_time.hours, task_time.project_status_rate, task_time.notes, task_time.date, task_time.billed, task_time.billable, projects.name AS p_name, projects.company_name AS c_name, projects.invoice_method , servicing_support_sheet.notes as notes2
									FROM task_time
									INNER JOIN projects ON task_time.project_id = projects.project_id
									LEFT JOIN servicing_support_sheet ON task_time.task_time_id = servicing_support_sheet.task_time_id AND task_time.task_id = servicing_support_sheet.task_id AND task_time.project_id = servicing_support_sheet.project_id 
									WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b
									ORDER BY c_name limit ".($loffset*$limit).", ".$limit."  ");
				// task_time.customer_id,
				 // task_time.project_id,
				while ($query->next()) {
					$data[$query->f('date')][$query->f('task_time_id')] = array(
						'user_id'			=> $query->f('user_id'),
						'c_name'			=> $query->f('c_name'),
						'p_name'			=> $query->f('p_name'),
						'task_id'			=> $query->f('task_id'),
						'hours'				=> $query->f('hours'),
						'notes'				=> $query->f('notes'),
						'notes2'			=> $query->f('notes2'),
						'billed'			=> $query->f('billed'),
						'billable'			=> $query->f('billable'),
						'invoice_method'	=> $query->f('invoice_method'),
						'status_rate' 		=> $query->f('project_status_rate'));
					$k++;
				}
				$query = null;
				if($k==$limit){
					$exec_loop = true;
				}else{
					break;
				}
				$loffset++;
			} while($exec_loop == true);

			$user_name = array();
			$u = $this->db->query("SELECT user_id, user_name FROM project_user WHERE 1=1 $filter_user ");
			while ($u->next()) {
				$user_name[$u->f('user_id')] = $u->f('user_name');
			}
			$u = null;
			$tasks = array();
			$t = $this->db->query("SELECT task_id, task_name FROM tasks WHERE 1=1 $filter_task ");
			while ($t->next()) {
				$tasks[$t->f('task_id')] = $t->f('task_name');
			}
			$t = null;
			//zhis iz where all zhe magic iz
			$customer_h = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b ");
			$customer_h_i = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND billable='1' AND billed='1' ");
			$customer_h_ub = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND (task_time.billable='0' OR projects.invoice_method='0') ");
			$customer_h_b = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND billed='0' AND (billable='1' && projects.invoice_method!='0') ");
			$customer_h_int=$this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND projects.invoice_method='0' ");
			$customer_d = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b ");
			$customer_d_i = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND billable='1' AND billed='1' ");
			$customer_d_ub = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND (task_time.billable='0' OR projects.invoice_method='0') ");
			$customer_d_b = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND billed='0' AND (billable='1' && projects.invoice_method!='0') ");
			$customer_d_int=$this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND projects.invoice_method='0' ");
			$customer_days = $this->db->query("SELECT DISTINCT task_time.date FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND hours!='0' ORDER BY date ASC ");
			// $days = array();

			$total_day =0;
			$total_days =0;
			while ($customer_days->next()) {
				$time = $customer_days->f('date');
				$day_hours = 0;
				$day_days = 0;
				$detaile_time_row=array();
				foreach ($data[$time] as $key => $value) {

					if(!$value['hours']){
						continue;
					}
					$temp_detaile_time_row=array(
						'CUSTOMERS'					=> $value['c_name'],
						'PROJECT'					=> $value['p_name'],
						'TASK'						=> $tasks[$value['task_id']],
						'USER'						=> $user_name[$value['user_id']],
						'HIDE_UNBILLED'				=> $value['billed'] == 1 ? '' : 'hide',
						'COMMENT'					=> $value['notes']? nl2br($value['notes']) : html_entity_decode(strip_tags($value['notes2']))
					);
					$lock = $value['billed'] == 1 ? "<span class='glyphicon glyphicon-lock' title=".gm('Entry has been invoiced and locked')."></span>" : "";
					if(($value['invoice_method']==1) && ($value['billable']==1)){
						if($value['status_rate']==0){
							$temp_detaile_time_row['HOURSB']= $lock."<span>".number_as_hour($value['hours'])."</span>";
							$temp_detaile_time_row['HOURS']= '-';
							$temp_detaile_time_row['INTERNAL_HOURS']= '-';
							$temp_detaile_time_row['DAYSB']= '-';
							$temp_detaile_time_row['DAYS']= '-';
							$temp_detaile_time_row['INTERNAL_DAYS']= '-';
							$total_day_h += $value['hours'];
							$total_day += $value['hours'];
							$day_hours += $value['hours'];
						}else{
							$temp_detaile_time_row['DAYSB'] = $lock."<span>".$value['hours']."</span>";
							$temp_detaile_time_row['DAYS']= '-';
							$temp_detaile_time_row['INTERNAL_DAYS']= '-';
							$temp_detaile_time_row['HOURSB'] = '-';
							$temp_detaile_time_row['HOURS']= '-';
							$temp_detaile_time_row['INTERNAL_HOURS']= '-';
							$total_day_d += $value['hours'];
							$total_days+= $value['hours'];
							$day_days += $value['hours'];
						}			
					}else if($value['invoice_method']==0){
						if($value['status_rate']==0){
							$temp_detaile_time_row['HOURSB']= '-';
							$temp_detaile_time_row['HOURS']	= $lock."<span>".number_as_hour($value['hours'])."</span>";
							$temp_detaile_time_row['INTERNAL_HOURS']= $lock."<span>".number_as_hour($value['hours'])."</span>";
							$temp_detaile_time_row['DAYSB']= '-';
							$temp_detaile_time_row['DAYS']= '-';
							$temp_detaile_time_row['INTERNAL_DAYS']= '-';
							$total_day_h_int += $value['hours'];
							$total_day_h_u += $value['hours'];
							$total_day += $value['hours'];
							$day_hours += $value['hours'];
						}else{
							$temp_detaile_time_row['DAYSB'] = '-';
							$temp_detaile_time_row['DAYS']	= $lock."<span>".$value['hours']."</span>";
							$temp_detaile_time_row['INTERNAL_DAYS']	= $lock."<span>".$value['hours']."</span>";
							$temp_detaile_time_row['HOURSB'] = '-';
							$temp_detaile_time_row['HOURS']	= '-';
							$temp_detaile_time_row['INTERNAL_HOURS']= '-';
							$total_day_d_int += $value['hours'];
							$total_day_d_u += $value['hours'];
							$total_days+= $value['hours'];
							$day_days += $value['hours'];
						}			
					}else{
						if($value['status_rate']==0){
							$temp_detaile_time_row['HOURS']= $lock."<span>".number_as_hour($value['hours'])."</span>";
							$temp_detaile_time_row['HOURSB']= '-';
							$temp_detaile_time_row['INTERNAL_HOURS']= '-';
							$temp_detaile_time_row['DAYS'] = '-';
							$temp_detaile_time_row['DAYSB']	= '-';
							$temp_detaile_time_row['INTERNAL_DAYS']	= '-';
							$total_day_h_u += $value['hours'];
							$total_day += $value['hours'];
							$day_hours += $value['hours'];
						}else{
							$temp_detaile_time_row['DAYS']= $lock."<span>".$value['hours']."</span>";
							$temp_detaile_time_row['DAYSB']	= '-';
							$temp_detaile_time_row['INTERNAL_DAYS']	= '-';
							$temp_detaile_time_row['HOURS'] = '-';
							$temp_detaile_time_row['HOURSB']= '-';
							$temp_detaile_time_row['INTERNAL_HOURS']= '-';
							$total_day_d_u += $value['hours'];
							$total_days+= $value['hours'];
							$day_days += $value['hours'];
						}		
					}	
					array_push($detaile_time_row, $temp_detaile_time_row);
				}
				$time_row=array(
					'DATE' 				=> date(ACCOUNT_DATE_FORMAT,$time),
					'TOTAL_C_HOURS'		=> number_as_hour($day_hours),
					'TOTAL_C_DAYS'		=> $day_days,
					'detaile_time_row'	=> $detaile_time_row,
				);
				array_push($result['time_row'], $time_row);
				$data[$time] = null;
			}
			$customer_days = null;

			$link = '';
			foreach ($in as $key => $value){
				$link .= '&'.$key.'='.$value;
			}
			$link = rtrim($link,'&');

			$result['TOTAL_HOURS'] 		= number_as_hour($customer_h);
			$result['TOTAL_HOURS_I'] 	= number_as_hour($customer_h_i);
			$result['TOTAL_HOURS_UB'] 	= number_as_hour($customer_h_ub);
			$result['TOTAL_HOURS_B'] 	= number_as_hour($customer_h_b);
			$result['TOTAL_HOURS_INT']	= number_as_hour($customer_h_int);
			$result['TOTAL_DAYS'] 		= $customer_d ? $customer_d : 0;
			$result['TOTAL_DAYS_I'] 	= $customer_d_i ? $customer_d_i : 0;
			$result['TOTAL_DAYS_UB'] 	= $customer_d_ub ? $customer_d_ub : 0;
			$result['TOTAL_DAYS_B'] 	= $customer_d_b ? $customer_d_b : 0;
			$result['TOTAL_DAYS_INT']	= $customer_d_int ? $customer_d_int : 0;
			$result['CUSTOMER']			= $customer_name;
			$result['PROJECTS']			= $project_name;
			$result['TASKS']			= $task_name;
			$result['STAFFS']			= $staff_name;
			$result['BUNITS']			= $bunit_name;
			$result['TOTAL']			= number_as_hour($total_day);
			$result['TOTAL_FIN_DAYS']	= $total_days;
			$result['TOTAL_H']			= number_as_hour($total_day_h);
			$result['TOTAL_H_U']		= number_as_hour($total_day_h_u);
			$result['TOTAL_H_INT']		= number_as_hour($total_day_h_int);
			$result['TOTAL_D']			= $total_day_d ? $total_day_d : 0;
			$result['TOTAL_D_U']		= $total_day_d_u ? $total_day_d_u : 0;
			$result['TOTAL_D_INT']		= $total_day_d_int ? $total_day_d_int : 0;
			$result['SOME_ID']			= $some_id;
			$result['EXPORT_HREF']		= 'index.php?do=report-export_detail_report&time_start='.$time_start.'&time_end='.$time_end.str_replace('do=report-xproject_report&','', $link);
		    $result['pdf_link_project_detail']	= 'index.php?do=report-reports_print&time_start='.$time_start.'&time_end='.$time_end.str_replace('do=report-xproject_report&','', $link);

		    if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter_m = " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}

			$filter = ' AND 1=1 ';

			if($in['customer_id'] || $in['customer_id2']){
				$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
				$filter .=' AND projects.customer_id='.$cid2.' ';
			}
			if($in['project_id'] || $in['project_id2']){
				$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
				$filter .=' AND projects.project_id='.$pid2.' ';
			}
			if($in['user_id'] || $in['user_id2']){
				$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
				$filter .=' AND project_user.user_id='.$uid2.' ';
				$filter_u = ' AND project_user.user_id='.$uid2.' ';
			}
			if($in['bunit_id'] || $in['bunit_id2']){
				$bid2=$in['bunit_id2'] ? $in['bunit_id2'] : $in['bunit_id'];
				$filter .= " AND projects.bunit='".$bid2."' ";
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}
			$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";

			$persons = array();
			$person = $this->db->query("SELECT user_name, user_id FROM project_user INNER JOIN projects ON project_user.project_id=projects.project_id WHERE 1=1 $filter");
			while($person->next()) {
				$persons[$person->f('user_id')] = $person->f('user_name');
			}

			$total_amount_p = 0;
			$total_amount_unbill = 0;
			$total_amount_bill = 0;

			$expense = $this->db->query("SELECT amount ,project_expenses.user_id, unit_price, projects.name AS p_name, expense.name AS e_name, expense.unit AS e_unit,
								projects.project_id AS p_id, project_expenses.expense_id, project_expenses.date AS exp_date,
								project_expenses.billable, projects.active, project_expences.expense_id AS billable_exp_id,project_expenses.note AS e_notes, projects.invoice_method
				FROM project_expenses
				INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
				INNER JOIN projects ON project_expenses.project_id = projects.project_id
				INNER JOIN project_user ON ( project_expenses.user_id = project_user.user_id AND project_expenses.project_id = project_user.project_id )
				LEFT JOIN project_expences ON ( project_expenses.expense_id = project_expences.expense_id AND project_expenses.project_id = project_expences.project_id )
				WHERE $filter_date $filter
				ORDER BY p_name, exp_date,e_name");

			while ($expense->next()) {
				$amount = $expense->f('amount');
				if($expense->f('unit_price')){
					$amount = $expense->f('amount')*$expense->f('unit_price');
				}
				$total_amount_p +=$amount;

				$quantity = display_number($expense->f('amount')).' ('.$expense->f('e_unit').')';
				if(!$expense->f('e_unit')){
					$quantity = display_number($expense->f('amount'));
				}
				if($expense->f('unit_price')){
					$unit_price = display_number($expense->f('unit_price'));
				}else{
					$unit_price = ' - ';
				}
				if(!$expense->f('e_unit') && !$expense->f('unit_price')){
					$quantity = ' - ';
					$unit_price = display_number($expense->f('amount'));
				}
				$p_hours_row=array(
					'EXPENSE_DATE'		=> date(ACCOUNT_DATE_FORMAT,$expense->f('exp_date')),
					'TASK_N'			=> $expense->f('e_name'),
					'PROJECT_N'			=> $expense->f('p_name'),
					// 'BILLABLE_A'		=> place_currency(display_number($amount)),
					'PERSON'			=> $persons[$expense->f('user_id')],
					'NAV'				=> $time_nav,
					'PROJECT_ID'		=> $expense->f('p_id'),
					'TASK_ID'			=> $expense->f('expense_id'),
					'QUANTITY'			=> $quantity,
					'NOTE'				=> $expense->f('e_notes'),
					'UNIT_PRICE'		=> $unit_price,
				);

				if($expense->f('active')==2){
					if(($expense->f('invoice_method')==1) && ($expense->f('billable')==1)){
						//for billable
						$total_amount_bill +=$amount;
						$p_hours_row['EXP_UNBILLABLE']		= '';
						$p_hours_row['EXP_BILLABLE']		= display_number($amount);
						$p_hours_row['EXP_INTERNAL']		= '';
					}else if($expense->f('invoice_method')==0){
						$total_amount_unbill +=$amount;
						$total_amount_int +=$amount;
						$p_hours_row['EXP_UNBILLABLE']		= display_number($amount);
						$p_hours_row['EXP_BILLABLE']		= '';
						$p_hours_row['EXP_INTERNAL']		= display_number($amount);
					}else{
						//for unbillable
						$total_amount_unbill +=$amount;
						$p_hours_row['EXP_UNBILLABLE']		= display_number($amount);
						$p_hours_row['EXP_BILLABLE']		= '';
						$p_hours_row['EXP_INTERNAL']		= '';
					}
				}else{//$expense->f('active')==1
					if(($expense->f('invoice_method')==1) && ($expense->f('billable_exp_id')==1)){
						//for billable
						$total_amount_bill +=$amount;
						$p_hours_row['EXP_UNBILLABLE']		= '';
						$p_hours_row['EXP_BILLABLE']		= display_number($amount);
						$p_hours_row['EXP_INTERNAL']		= '';
					}else if($expense->f('invoice_method')==0){
						$total_amount_unbill +=$amount;
						$total_amount_int +=$amount;
						$p_hours_row['EXP_UNBILLABLE']		= display_number($amount);
						$p_hours_row['EXP_BILLABLE']		= '';
						$p_hours_row['EXP_INTERNAL']		= display_number($amount);
					}else{
						//for unbillable
						$total_amount_unbill +=$amount;
						$p_hours_row['EXP_UNBILLABLE']		= display_number($amount);
						$p_hours_row['EXP_BILLABLE']		= '';
						$p_hours_row['EXP_INTERNAL']		= '';
					}
				}
				array_push($result['p_hours_row'], $p_hours_row);
			}
			$expense=null;
			$result['EXP_TOTAL_AMOUNT']		= display_number($total_amount_p);
			$result['EXP_TOTAL_UNBILL_AMOUNT']	= display_number($total_amount_unbill);
			$result['EXP_TOTAL_BILL_AMOUNT']		= display_number($total_amount_bill);
			$result['EXP_TOTAL_INTERNAL_AMOUNT']	= display_number($total_amount_int);
			$result['pdf_link']				= 'index.php?do=report-expenses_report_print&time_start='.$time_start.'&time_end='.$time_end.str_replace('do=report-xproject_report&','', $link);

			$this->out=$result;
		}

		public function get_customerChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter = ' AND 1=1';
			if($in['customer_id']){
				$filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';	
			}
			if($in['user_id']){
				$filter .= ' AND task_time.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}			
			if($in['project_id']){
				$filter .= ' AND task_time.project_id ='.$in['project_id'].' ';
			}
			if($in['task_id']){
				$filter .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
			}

			$rates = array();	
			$user_rates = array();	
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate
			 				FROM task_time
							INNER JOIN projects ON task_time.project_id=projects.project_id
							INNER JOIN tasks ON task_time.task_id = tasks.task_id AND tasks.billable='1' WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");	
			while ($q->next()) {
				// $i++;
				$rate = 0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('project_status_rate') == 0){
							if($q->f('t_h_rate')){
								$rate = $q->f('t_h_rate');
							}
						}else{
							if($q->f('t_daily_rate')){
								$rate = $q->f('t_daily_rate');
							}
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
							$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							if($q->f('project_status_rate') == 0){
								$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}else{
								$rate = $this->db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							}
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
				}
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
			}
			$billable_amount = array();
			$billable_amount_day=array();
			$a = $this->db->query("SELECT SUM(task_time.hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id, task_time.customer_id FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' AND billable='1' AND projects.invoice_method!=0 GROUP BY task_time.task_id, task_time.user_id, task_time.project_id ");
			while ($a->next()) {	
				if($a->f('project_status_rate') == 0){
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					$billable_amount[$a->f('customer_id')] += $a->f('total_h')*$rate;
				}else{
					$rate = 0;
					if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
						$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
					}
					$billable_amount_day[$a->f('customer_id')] += $a->f('total_h')*$rate;
				}
				
			}
			/* optimization */

			$total_hours = 0;
			$total_b_hours = 0;
			$total_amount = 0;
			$i = 0;
			$report = $this->db->query("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h, SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days, task_time.project_status_rate, projects.company_name AS c_name, task_time.customer_id AS c_id,
				SUM( CASE WHEN task_time.billable =1 THEN task_time.hours ELSE 0 END ) AS billable
			FROM task_time
			INNER JOIN projects ON task_time.project_id=projects.project_id
			WHERE task_time.`date` BETWEEN '{$time_start}' AND '{$time_end}' {$filter_m} {$filter} GROUP BY c_id, c_name")->getAll();
			foreach ($report as $key => $value) {
				if($value['total_h'] == 0 && $value['total_days'] == 0){
					continue;
				}

				array_push($result['label'], $value['c_name']);
				array_push($result['data'], $value['project_status_rate'] ==0 ? number_format($value['total_h'],2,'.','') : number_format($value['total_days'],2,'.',''));
				array_push($result['data_show'], $value['project_status_rate'] ==0 ? number_as_hour($value['total_h']) : $value['total_days']);	
			}

			return $result;
		}

		public function get_userChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter = ' AND 1=1';
			if($in['customer_id']){
				$filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';	
			}
			if($in['user_id']){
				$filter .= ' AND task_time.user_id ='.$in['user_id'].' ';	
			}
			if($in['bunit_id']){
				$filter .= ' AND projects.bunit ='.$in['bunit_id'].' ';
			}
			if($in['internal']=='true'){
				$filter.=" AND projects.invoice_method='0' ";
			}			
			if($in['project_id']){
				$filter .= ' AND task_time.project_id ='.$in['project_id'].' ';
			}
			if($in['task_id']){
				$filter .= ' AND task_time.task_id ='.$in['task_id'].' ';
			}
			if($this->is_manager == '1' && $_SESSION['access_level'] != 1){
				$filter .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
				$in['filter'] = 1;
			}
			$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";

			/* optimization */
			// get rates
			$rates = array();
			/* further optimization */
			/* save the user rate */
			$user_rates = array();
			/* save the user rate */
			/* further optimization */
			$q = $this->db->query("SELECT DISTINCT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, projects.billable_type, projects.pr_h_rate, tasks.t_h_rate
			 				FROM task_time
							INNER JOIN projects ON task_time.project_id=projects.project_id
							INNER JOIN tasks ON task_time.task_id = tasks.task_id AND tasks.billable='1'
							WHERE date BETWEEN '".$time_start."' AND '".$time_end."' $filter ");
			while ($q->next()) {
				$rate = 0;
				switch ($q->f('billable_type')){
					case '1':
					case '7':
						$rate = 0;
						if($q->f('t_h_rate')){
							$rate = $q->f('t_h_rate');
						}
						break;
					case '2':
						if(array_key_exists($q->f('user_id').'-'.$q->f('project_id'), $user_rates)){
							$rate = $user_rates[$q->f('user_id').'-'.$q->f('project_id')];
						}else{
							$rate = $this->db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
							$user_rates[$q->f('user_id').'-'.$q->f('project_id')] = $rate;
						}
						break;
					case '3':
						$rate = $q->f('pr_h_rate');
						break;
					case '4':
						$rate = 0;
						break;
				}
				$rates[$q->f('task_id')][$q->f('user_id')][$q->f('project_id')] = $rate;
			}

			$billable_amount = array();
			$t_hours = array();
			$ts_hours = array();
			$a = $this->db->query("SELECT SUM(hours) AS total_h, task_time.task_id, task_time.user_id, task_time.project_id FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' $filter AND billable='1' GROUP BY task_time.task_id, task_time.user_id, task_time.project_id ");
			while ($a->next()) {
				$rate = 0;
				if($rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')]){
					$rate = $rates[$a->f('task_id')][$a->f('user_id')][$a->f('project_id')];
				}
				$billable_amount[$a->f('user_id')] += $a->f('total_h')*$rate;
				$t_hours[$a->f('user_id')] += $a->f('total_h');
			}
			$a = $this->db->query("SELECT SUM(hours) AS total_h, task_time.user_id FROM task_time 
				INNER JOIN projects ON task_time.project_id=projects.project_id
				WHERE `date` BETWEEN '".$time_start."' AND '".$time_end."' $filter GROUP BY user_id ");
			while ($a->next()) {
				$ts_hours[$a->f('user_id')] += $a->f('total_h');
			}
			$e = $this->db->query("SELECT DISTINCT project_user.user_id AS u_id
							FROM project_user
							INNER JOIN task_time ON project_user.user_id = task_time.user_id AND project_user.project_id=task_time.project_id
							INNER JOIN projects ON task_time.project_id=projects.project_id
							WHERE $filter_date $filter $filter_m  ");
			$i=1;
			$total_hours = 0;
			$total_b_hours = 0;
			$total_amount = 0;
			$total_amount_ex = 0;
			while ($e->next()) {
				$total_h = $ts_hours[$e->f('u_id')];
				$total_h_b = $t_hours[$e->f('u_id')];
				if($total_h==0){
						continue;
				}
				$u_name = $this->db->field("SELECT user_name FROM project_user WHERE user_id = '".$e->f('u_id')."' ORDER BY project_user_id DESC LIMIT 1 ");

				array_push($result['label'], $u_name);
				array_push($result['data'], $total_h);
				array_push($result['data_show'], number_as_hour($total_h));
			}

			return $result;
		}

	}

	$cash_data = new ReportProject($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_Data();
	$cash_data->output();
?>