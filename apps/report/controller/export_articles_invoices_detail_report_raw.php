<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

  ini_set('memory_limit','786M');
 
  global $database_config;

    $db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

  $db_users = new sqldb($db_config);
  $account_users=array();
  $account_users_data=$db_users->query("SELECT user_id,first_name,last_name FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();

  foreach($account_users_data as $key=>$value){
    $account_users[$value['user_id']]=htmlspecialchars_decode(stripslashes($value['first_name'].' '.$value['last_name']));
  }


setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

//$filename ="articles_detail_report_raw.xls";
$filename ="articles_detail_report_raw.csv";

$time_start = $in['time_start'];
$time_end = $in['time_end'];
if(!$time_start && !$time_end){
  $filter2 = " AND 1=1 ";
}
if($in['customer_id']){
    $filter.=" and tblinvoice.buyer_id = ".$in['customer_id'];
}
if($in['m_id']){
  $filter .= ' AND tblinvoice.acc_manager_id ='.$in['m_id'].' ';
}
if($in['article_category_id']){
    $filter.=" and pim_articles.article_category_id = ".$in['article_category_id'];
}
if($in['u_id']){
    $filter.=" AND pim_orders.author_id=".$in['u_id'] ;
}
if($in['article_id']){
    $filter.=" and pim_articles.article_id = ".$in['article_id'];
}
if($in['supplier_id']){
    $filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';          
}

$headers = array(gm('Code'),
                    gm('Internal Name'),
                    gm("Customer"),
                    gm('Company id'),
                    gm("Country"),
                    gm("Supplier"),
                    gm("Family"),
                    gm("Account manager name"),
                    gm("Invoice date"),
                    gm("Customers"),
                    gm("Quantity"),
                    gm("Margin"),
                    gm("Margin %"),
                    gm("Main Country"),
                    gm('Type'),
                    gm("Total Value")        
      );

$i=0;
$xlsRow = 2;

$articles_data = $db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.discount as global_discount, tblinvoice.paid, tblinvoice.discount_line_gen, tblinvoice_line.discount, tblinvoice.acc_manager_id, tblinvoice.invoice_date,
                tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice.currency_rate,tblinvoice.currency_type,tblinvoice.buyer_country_id,
                tblinvoice_line.quantity, tblinvoice_line.price,tblinvoice_line.purchase_price, pim_articles.article_id, 
                pim_articles.item_code AS invoice_item_code,
                customers.name AS invoice_buyer_name,
                pim_articles.supplier_name, pim_articles.supplier_id,country.name as country_name,pim_articles.internal_name,pim_article_categories.name AS cat_name,
                CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) as invoice_contact_name, tblinvoice.type
                FROM tblinvoice_line
                LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
                LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
                LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
                LEFT JOIN country ON tblinvoice.buyer_country_id=country.country_id
                LEFT JOIN pim_article_categories ON pim_articles.article_category_id=pim_article_categories.id
                LEFT JOIN customer_contacts ON tblinvoice.contact_id=customer_contacts.contact_id
                WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
                tblinvoice_line.article_id != '0' ".$filter."
                ORDER BY invoice_item_code ASC, invoice_buyer_name ASC");

$total_contacts = array();
$total_companies = array();
$total_contacts2 = array();
$total_companies2 = array();
$art = array();
$total_amount = 0;
$big_margin = 0;
$big_margin_percent=0;

while ($articles_data->next()) {
    $del_a = 0;
    $ndel_a = 0;
    $acc_manager_name='';
    //$acc_manager_name =get_user_name($articles_data->f('acc_manager_id'));
    if(array_key_exists($articles_data->f('acc_manager_id'), $account_users)){
      $acc_manager_name=$account_users[$articles_data->f('acc_manager_id')];
    }

    $currency_rate  = return_value($articles_data->f('currency_rate'));
    if(!$currency_rate){
        $currency_rate = 1;     
    }
    $global_disc = $articles_data->f('global_discount');

    $discount_line = $articles_data->f('discount');
    if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
        $discount_line = 0;
    }
    if($articles_data->f('apply_discount') < 2){
        $global_disc = 0;
    }
    $q = $articles_data->f('quantity') ;
    
    $price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);
    $line_total=$price_line * $q;   

    if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
        $line_total = $line_total*return_value($articles_data->f('currency_rate'));
    }   

    $total_amount_purchase+= $articles_data->f('purchase_price') * $currency_rate* $articles_data->f('quantity') ;

    if($articles_data->f('buyer_id')){
        if(!$total_companies[$articles_data->f('article_id')]){
            $total_companies[$articles_data->f('article_id')] = 0;
        }       
    }elseif($articles_data->f('contact_id')){
        if(!$total_contacts[$articles_data->f('article_id')]){
            $total_contacts[$articles_data->f('article_id')] = 0;
        }       
    }

    if($articles_data->f('buyer_id')){
        $art[$articles_data->f('article_id')]['customer'][$articles_data->f('buyer_id')] += $line_total - ($line_total*$global_disc/100);
        $art[$articles_data->f('article_id')]['customerq'][$articles_data->f('buyer_id')] += $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['customer_margin'][$articles_data->f('buyer_id')]+= $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
        $art[$articles_data->f('article_id')]['customer_purchase_amount'][$articles_data->f('buyer_id')] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
         //$art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('buyer_id')] = get_country_name($articles_data->f('buyer_country_id'));
        $art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('buyer_id')] = addslashes($articles_data->f('country_name'));
         $art[$articles_data->f('article_id')]['invoice_date'][$articles_data->f('buyer_id')] = date(ACCOUNT_DATE_FORMAT,$articles_data->f('invoice_date'));
         $art[$articles_data->f('article_id')]['account_manager_name'][$articles_data->f('buyer_id')] = $acc_manager_name;
         $art[$articles_data->f('article_id')]['customer_name'][$articles_data->f('buyer_id')] = $articles_data->f('invoice_buyer_name');
         $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('buyer_id')]['invoice_id'][] = $articles_data->f('id');
         $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('buyer_id')]['invoice_date'][] = date(ACCOUNT_DATE_FORMAT,$articles_data->f('invoice_date'));
         $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('buyer_id')]['customer'][] = $line_total - ($line_total*$global_disc/100);
         $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('buyer_id')]['quantity'][] = $articles_data->f('quantity');
         $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('buyer_id')]['customer_margin'][] = $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
         $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('buyer_id')]['customer_purchase_amount'][] = $articles_data->f('purchase_price') * $articles_data->f('quantity');
         $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('buyer_id')]['invoice_type'][]=$articles_data->f('type')=='2' || $articles_data->f('type')=='4' ? gm('Credit Invoice') : gm('Invoice');
    }else{
        $art[$articles_data->f('article_id')]['contact'][$articles_data->f('contact_id')] += $line_total - ($line_total*$global_disc/100);
        $art[$articles_data->f('article_id')]['contactq'][$articles_data->f('contact_id')] += $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['contact_margin'][$articles_data->f('contact_id')] += $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
        $art[$articles_data->f('article_id')]['contact_purchase_amount'][$articles_data->f('contact_id')] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
        //$art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('contact_id')] = get_country_name($articles_data->f('buyer_country_id'));
        $art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('contact_id')] = addslashes($articles_data->f('country_name'));
        $art[$articles_data->f('article_id')]['invoice_date'][$articles_data->f('contact_id')] = date(ACCOUNT_DATE_FORMAT,$articles_data->f('invoice_date'));
        $art[$articles_data->f('article_id')]['account_manager_name'][$articles_data->f('contact_id')] =  $acc_manager_name;
        $art[$articles_data->f('article_id')]['contact_name'][$articles_data->f('contact_id')] = $articles_data->f('invoice_contact_name');
        $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('contact_id')]['invoice_id'] = $articles_data->f('id');
        $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('contact_id')]['invoice_date'] = date(ACCOUNT_DATE_FORMAT,$articles_data->f('invoice_date'));
        $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('contact_id')]['contact'] = $line_total - ($line_total*$global_disc/100);    
        $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('contact_id')]['quantity'][] = $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('contact_id')]['contact_margin'][] = $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
        $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('contact_id')]['contact_purchase_amount'][] = $articles_data->f('purchase_price') * $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['invoices'][$articles_data->f('contact_id')]['invoice_type'][] = $articles_data->f('type')=='2' || $articles_data->f('type')=='4' ? gm('Credit Invoice') : gm('Invoice');
    }
    $art[$articles_data->f('article_id')]['article'] += $line_total - ($line_total*$global_disc/100);
    $art[$articles_data->f('article_id')]['q'] += $articles_data->f('quantity') ;
    $art[$articles_data->f('article_id')]['margin'] += $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
    $art[$articles_data->f('article_id')]['currency_rate']= $currency_rate;
    $art[$articles_data->f('article_id')]['supplier_name'] = $articles_data->f('supplier_name');
    $art[$articles_data->f('article_id')]['invoice_buyer_name'] = $articles_data->f('invoice_buyer_name');
   
    $art[$articles_data->f('article_id')]['purchase_amount'] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
    $art[$articles_data->f('article_id')]['internal_name'] = $articles_data->f('internal_name');
    $art[$articles_data->f('article_id')]['item_code'] = $articles_data->f('invoice_item_code');
    $art[$articles_data->f('article_id')]['category_name'] = $articles_data->f('cat_name') ? $articles_data->f('cat_name') :'';
}

$big_quantity = 0;
$big_customers = 0;
$total_del = 0;
$total_ndel = 0;
$i = 0;
$j=0;
$total_margin=0;
$total_margin_percent=0;
$final_data=array();
foreach ($art as $art_id => $art_data) {
    $total_contacts[$art_id] = count($art_data['customer']);
    $total_companies[$art_id] = count($art_data['contact']);
    $total_quantity = $art_data['q'];
    
    $big_quantity+=$total_quantity;
    $customers = $total_companies[$art_id]+$total_contacts[$art_id];

    $big_customers+=$customers;
    $total_amount+= $art_data['article'];
    $article_name = htmlspecialchars_decode($art_data['internal_name']);
    $article_code = $art_data['item_code'];
    $article_fam = $art_data['category_name'];
    $article_supplier = $art_data['supplier_name'];
    $invoice_buyer_name = $art_data['invoice_buyer_name'];
    $invoice_buyer_country = $art_data['invoice_buyer_country'];
    $account_manager_name = $art_data['acc_manager_name'];
    $delivered_a    = $art_data['delivered'];
    $ndelivered_a   = $art_data['ndelivered'];
    $total_del+=$delivered_a;
    $total_ndel+=$ndelivered_a;
    $total_margin=$art_data['margin'];
    $total_margin_percent=0;

    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
      if($total_amount_purchase){
         $total_margin_percent=($art_data['margin']/$total_amount_purchase)*100;
      }elseif($art_data['margin']){
         $total_margin_percent=100;
      }
    }else{
          if($art_data['article']){
             $total_margin_percent=($art_data['margin']/$art_data['article'])*100;
          }elseif($art_data['margin']){
             $total_margin_percent=100;
          }
    }

  if($art_data['customer']){
    foreach ($art_data['customer'] as $key => $value) {
        foreach($art_data['invoices'][$key]['invoice_id'] as $key1=>$value1){
          $customer_total_margin_percent=0;
          if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
            if($art_data['invoices'][$key]['customer_purchase_amount'][$key1]){
              $customer_total_margin_percent = $art_data['invoices'][$key]['customer_margin'][$key1]/$art_data['invoices'][$key]['customer_purchase_amount'][$key1]*100;
            }elseif($art_data['invoices'][$key]['customer_margin'][$key1]){
              $customer_total_margin_percent =100;
            }
          }else{
             if($art_data['invoices'][$key]['customer'][$key1]){
                $customer_total_margin_percent = $art_data['invoices'][$key]['customer_margin'][$key1]/$art_data['invoices'][$key]['customer'][$key1]*100;
              }elseif($art_data['invoices'][$key]['customer_margin'][$key1]){
                $customer_total_margin_percent =100;
              }
          }
          $tmp_item=array(
            $article_code,
            $article_name,
            $art_data['customer_name'][$key],
            $key,
            $art_data['invoice_buyer_country'][$key],
            $article_supplier,
            $article_fam,
            $art_data['account_manager_name'][$key],
            $art_data['invoices'][$key]['invoice_date'][$key1],
            $customers,
            display_number_exclude_thousand($art_data['invoices'][$key]['quantity'][$key1]),
            display_number_exclude_thousand($art_data['invoices'][$key]['customer_margin'][$key1]),
            display_number_exclude_thousand($customer_total_margin_percent),
            $art_data['invoice_buyer_country'][$key],
            $art_data['invoices'][$key]['invoice_type'][$key1],
            display_number_exclude_thousand($art_data['invoices'][$key]['customer'][$key1])
          );
          array_push($final_data,$tmp_item);
        }
    }
  }


  if($art_data['contact']){
    foreach ($art_data['contact'] as $key => $value) {
        foreach($art_data['invoices'][$key]['invoice_id'] as $key1=>$value1){
            $contact_total_margin_percent =0;
           if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
              if($art_data['invoices'][$key]['contact_purchase_amount'][$key1]){
                $contact_total_margin_percent = $art_data['invoices'][$key]['contact_margin'][$key1]/$art_data['invoices'][$key]['contact_purchase_amount'][$key1]*100;
              }elseif($art_data['invoices'][$key]['contact_margin'][$key1]){
                $contact_total_margin_percent =100;
              }
            }else{
               if($art_data['invoices'][$key]['contact'][$key1]){
                  $contact_total_margin_percent = $art_data['invoices'][$key]['contact_margin'][$key1]/$art_data['invoices'][$key]['contact'][$key1]*100;
                }elseif($art_data['invoices'][$key]['contact_margin'][$key1]){
                  $contact_total_margin_percent =100;
                }
            }
            $tmp_item=array(
                $article_code,
                $article_name,
                $art_data['contact_name'][$key],
                $key,
                $art_data['invoice_buyer_country'][$key],
                $article_supplier,
                $article_fam,
                $art_data['account_manager_name'][$key],
                $art_data['invoices'][$key]['invoice_date'][$key1],
                $customers,
                display_number_exclude_thousand($art_data['invoices'][$key]['quantity'][$key1]),
                display_number_exclude_thousand($art_data['invoices'][$key]['contact_margin'][$key1]),
                display_number_exclude_thousand($contact_total_margin_percent),
                '',
                $art_data['invoices'][$key]['invoice_type'][$key1],
                display_number_exclude_thousand($art_data['invoices'][$key]['contact'][$key1])
            );
            array_push($final_data,$tmp_item);
        }
    }
  }
  $i++;
}
 
$total_customers = count(array_count_values($total_companies))+count(array_count_values($total_contacts));

/*array_push($final_data,
    array(
       gm('Total'),
       '',
       '',
       '',
       '',
       '',
       '',
       '',
       '',
       $big_quantity,
       '',
       '',
       '',
       '',
       $total_amount
    )
  );*/


  doQueryLog();
   $from_location='upload/'.DATABASE_NAME.'/export_articles_invoices_detail_report_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();

  $from_location='upload/'.DATABASE_NAME.'/export_articles_invoices_detail_report_'.time().'.csv';
    $fp = fopen($from_location, 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    $objReader = PHPExcel_IOFactory::createReader('CSV');

    $objPHPExcel = $objReader->load($from_location);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    unlink($from_location);
    $objWriter->save("php://output");
    doQueryLog();
    exit();

?>