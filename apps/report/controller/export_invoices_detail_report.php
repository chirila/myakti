<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
setcookie('Akti-Export','6',time()+3600,'/');
ini_set('memory_limit','1G');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Orders detail report")
               ->setSubject("Orders detail report")
               ->setDescription("Articles report export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Articles");*/

//for horus, the export contains more data
$db_horus = false;
 if (DATABASE_NAME == '74997702_76b4_759b_c140a8d1b73b' || DATABASE_NAME == 'salesassist_22' || DATABASE_NAME == 'SalesAssist_11') {
  $db_horus = true;

/*$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);*/


} else{
  /*$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
  $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
  $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);*/


}

$time_start = $in['time_start'];
$time_end = $in['time_end'];
/*$filename ="invoice_detail_report.xls";*/
$filename ="invoice_detail_report.csv";

//$filter = "   tblinvoice.sent = '1' ";
$filter = "   tblinvoice.sent != '0' ";
$filter2 = " tblinvoice.invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.f_archived='0' AND tblinvoice.buyer_id !=0  AND (tblinvoice.type=0 OR tblinvoice.type=2 OR tblinvoice.type=3  OR tblinvoice.type=4 ) ";

if(!$time_start && !$time_end){
  $filter2 = " AND 1=1 ";
}

if($in['m_id']){
  $filter .= "  AND tblinvoice.acc_manager_id=".$in['m_id']."";
}

if($in['customer_id']){
  $filter .= "  AND tblinvoice.buyer_id=".$in['customer_id']."";
}

if($in['c_type']){
    $in['c_type_name'] = $db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
    $filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
}
if(!empty($in['identity_id']) || $in['identity_id']==='0'){
    $filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
 }
if($in['source_id']){
    $filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
}
if($in['type_id']){
    $filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
}

 if ($db_horus){

  $orders =$db->query("SELECT
              tblinvoice_line.invoice_id,
              tblinvoice_line.article_id,
              tblinvoice_line.name AS article_name,
              tblinvoice_line.price,
              tblinvoice_line.vat,
              tblinvoice_line.quantity,
              tblinvoice_line.discount AS line_disc,
              tblinvoice_line.item_code,
              tblinvoice_line.content,
              tblinvoice_line.sort_order,
              tblinvoice.serial_number,
              tblinvoice.buyer_name,
              tblinvoice.invoice_date,
              tblinvoice.type,
              tblinvoice.currency_rate,
              tblinvoice.discount AS global_disc,
              tblinvoice.apply_discount,
              pim_articles.origin_number,
              pim_articles.weight

              FROM  tblinvoice_line
              LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
              LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
              LEFT JOIN pim_articles ON tblinvoice_line.article_id = pim_articles.article_id
              WHERE ".$filter2." AND ".$filter." AND (tblinvoice_line.article_id!='' ) ORDER BY tblinvoice_line.invoice_id, tblinvoice_line.sort_order");



    /*$objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A1', gm("Invoice Number"))
          ->setCellValue('B1', gm("Date"))
          ->setCellValue('C1', gm("Code douanier"))
          ->setCellValue('D1', gm("Name"))     
          ->setCellValue('E1', gm("Item Code"))
          ->setCellValue('F1', gm("Weight"))
          ->setCellValue('G1', gm("TAXCODE"))
          ->setCellValue('H1', gm("Quantity"))
          ->setCellValue('I1', gm("TAXVALUE"))
          ->setCellValue('J1', gm("Unit price"))  
          ->setCellValue('K1', gm("Total Value"))        
          ->setCellValue('L1', gm("VAT"))              
          ->setCellValue('M1', gm("VAT Credit Invoice"))
          ->setCellValue('N1', gm("Total Credit Invoice"));*/
     $headers    = array(gm("Invoice Number"),
                    gm("Date"),
                    gm('Code douanier'),
                    gm("Name"),
                    gm("Item Code"),
                    gm("Weight"),
                    gm("TAXCODE"),
                    gm("Quantity"),
                    gm("TAXVALUE"),
                    gm("Unit price"),
                    gm("Total Value"),
                    gm("VAT"),
                    gm("VAT Credit Invoice"),
                    gm("Total Credit Invoice")
      );

 }else{

  $orders =$db->query("SELECT
              tblinvoice_line.invoice_id,
              tblinvoice_line.article_id,
              tblinvoice_line.name AS article_name,
              tblinvoice_line.price,
              tblinvoice_line.vat,
              tblinvoice_line.quantity,
              tblinvoice_line.discount AS line_disc,
              tblinvoice_line.item_code,
              tblinvoice.serial_number,
              tblinvoice.acc_manager_name,
              tblinvoice.our_ref,
              tblinvoice.buyer_name,
              tblinvoice.buyer_id,
              tblinvoice.invoice_date,
              tblinvoice.type,
              tblinvoice.currency_rate,
              tblinvoice.discount AS global_disc,
              tblinvoice.apply_discount

              FROM  tblinvoice_line
              LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
              LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id

              WHERE ".$filter2." AND ".$filter." ");


/*  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Date"))
      ->setCellValue('B1', gm("Client"))
      ->setCellValue('C1', gm("Serial Number"))
      ->setCellValue('D1', gm("Account Manager"))
      ->setCellValue('E1', gm("Account Reference"))
      ->setCellValue('F1', gm("Item Code"))
      ->setCellValue('G1', gm("Name"))
      ->setCellValue('H1', gm("Total Quantity"))
      ->setCellValue('I1', gm("VAT"))
      ->setCellValue('J1', gm("Total Value"))
    
      ->setCellValue('K1', gm("VAT Credit Invoice"))
      ->setCellValue('L1', gm("Total Credit Invoice"));*/

       $headers    = array(gm("Date"),
                    gm("Client"),
                    gm("Company id"),
                    gm("Serial Number"),
                    gm("Account Manager"),
                    gm("Account Reference"),
                    gm("Item Code"),
                    gm("Name"),
                    gm("Total Quantity"),
                    gm("VAT"),
                    gm("Total Value"),
                    gm("VAT Credit Invoice"),
                    gm("Total Credit Invoice")
      );


 }

$i=0;
 $xlsRow = 2;
 $article_total_value = 0;
 $article_total_value_c = 0; 
 $taxes_total_value = 0;
 $article_total_quantity = 0;
 $article_tax_total_value = '';
 $final_data=array();

while($orders->next()) {
  $total_value=0;
  $total_value_c=0;
  $next_is_article=0;
  switch ($orders->f('apply_discount')) {
    case '0'://no discount

      $total_value = $orders->f('quantity') * $orders->f('price');
      $unit_price = $orders->f('price');
      break;
    case '1'://line discount
      $total_value = $orders->f('quantity') * $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) ;
      $unit_price = $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) ;
      break;
    case '2'://global discount
      $a = $orders->f('quantity') * $orders->f('price') ;
      $b = 1 - $orders->f('global_disc')/100;
      $total_value = $a * $b;
      $unit_price = $orders->f('price') * ( 1 - $orders->f('global_disc')/100 ) ;
      break;
    case '3'://line and global discount
      $line_discount = $orders->f('quantity') * $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) ;
      $total_value = $line_discount * ( 1 - $orders->f('global_disc')/100 );
      $unit_price =  ($orders->f('price') * ( 1 - $orders->f('line_disc')/100 )) * ( 1 - $orders->f('global_disc')/100 );
      break;
    default://no discount
      $total_value = $orders->f('quantity') * $orders->f('price') * ( 1 - 0/100 ) ;
      $unit_price = $orders->f('price') * ( 1 - 0/100 ) ;
      break;
  }
  $currency_rate = 1;
  if($orders->f('currency_rate')){
    $currency_rate = return_value($orders->f('currency_rate'));
  }
  $total_value=round($total_value,2);
  $unit_price = round($unit_price,2);
  $total_value = $total_value*$currency_rate;
  $unit_price = $unit_price*$currency_rate;

  if($orders->f('type') == 2 || $orders->f('type') == 4){
     $total_value_c=$total_value;
     $total_value=0;
    $article_total_value_c = $article_total_value_c+$total_value_c;
    $c++;
  }else{
    $article_total_value = $article_total_value+$total_value;
    $total_value_c=0;
    $n++;
  }
 
  
  $article_total_quantity += $orders->f('quantity');

  if ($db_horus) { 
      if ($orders->f('content')=='0'){//daca nu e linie text
        
       if ($orders->f('article_id')){ //daca nu e articol, taxa nu se afiseaza

                  //verificam daca articolul are taxe pe liniile urmatoare
                  $article_sort_order = $orders->f('sort_order');
                  //$next_is_article=0;
                  $tax_list=''; $taxes_value =0;
                  $i=1;
                  
                  while ($next_is_article!=1){
                   
                      $next_sort_order =$article_sort_order+$i;
                        
                      $taxes = $db->query("SELECT tblinvoice_line.*, tblinvoice.* FROM tblinvoice_line
                                          LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id 
                                          WHERE invoice_id='".$orders->f('invoice_id')."' AND tblinvoice_line.sort_order ='".$next_sort_order."' AND 
                                          tblinvoice_line.content ='0' AND (tblinvoice_line.article_id =  '0' OR ISNULL( tblinvoice_line.article_id )) ");
                 
 /* echo '<br>';     
 echo "SELECT tblinvoice_line.*, tblinvoice.* FROM tblinvoice_line
                                          LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id 
                                          WHERE invoice_id='".$orders->f('invoice_id')."' AND tblinvoice_line.sort_order ='".$next_sort_order."' AND 
                                          tblinvoice_line.content ='0' AND (tblinvoice_line.article_id =  '0' OR ISNULL( tblinvoice_line.article_id )) ";
             */        
                      //verificam daca pe urmatoarea linie exista taxa sau articol
                      
                      if ($taxes->move_next()){

                          $tax_list .= $taxes->f('item_code').', ';


                            switch ($taxes->f('apply_discount')) {
                                    case '0'://no discount

                                        $tax_total_value_q = $taxes->f('quantity') * $taxes->f('price');
                                        $tax_total_value = $taxes->f('price');
                                      break;
                                    case '1'://line discount
                                       $tax_total_value_q = $taxes->f('quantity') * $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                       $tax_total_value = $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                      break;
                                    case '2'://global discount
                                      $a_q = $taxes->f('quantity') * $taxes->f('price') ;
                                      $a = $taxes->f('price') ;
                                      $b = 1 - $taxes->f('global_disc')/100;
                                      $tax_total_value = $a * $b;
                                      break;
                                    case '3'://line and global discount
                                      $tax_line_discount_q = $taxes->f('quantity') * $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                      $tax_line_discount = $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                      $tax_total_value = $tax_line_discount * ( 1 - $taxes->f('global_disc')/100 );
                                      break;
                                    default://no discount
                                      $tax_total_value_q = $taxes->f('quantity') * $taxes->f('price') * ( 1 - 0/100 ) ;
                                      $tax_total_value = $taxes->f('price') * ( 1 - 0/100 ) ;
                                      break;
                                  }
                                  $currency_rate = 1;
                                  if($taxes->f('currency_rate')){
                                    $currency_rate = return_value($taxes->f('currency_rate'));
                                  }
                                  $tax_total_value=round($tax_total_value,2);
                                  $tax_total_value = $tax_total_value*$currency_rate;

                                  $tax_total_value_q=round($tax_total_value_q,2);
                                  $tax_total_value_q = $tax_total_value_q*$currency_rate;

                                  if($taxes->f('type') == 2){
                                     $tax_total_value_c=$tax_total_value;
                                     $tax_total_value=0;
                                    $article_tax_total_value_c = $article_tax_total_value_c+$tax_total_value_c;
                                  }else{
                                    $article_tax_total_value = $article_tax_total_value+$tax_total_value;
                                    $tax_total_value_c=0;
                                  }
                   
                                  $taxes_value += $tax_total_value;
                                  $article_total_value += $tax_total_value_q;


                         $i++;

                          }else{
                            $next_is_article=1;                             
                            //pe urmatoarea linie este articol

                          }
                         

                  }

              /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $orders->f('serial_number'))
              ->setCellValue('B'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')))
              ->setCellValue('C'.$xlsRow, $orders->f('origin_number'))
              ->setCellValue('D'.$xlsRow, $orders->f('article_name'))
              ->setCellValue('E'.$xlsRow, $orders->f('item_code'))
              ->setCellValue('F'.$xlsRow, $orders->f('weight')*$orders->f('quantity'))
              ->setCellValue('G'.$xlsRow, $tax_list)
              ->setCellValue('H'.$xlsRow, $orders->f('quantity'))
              ->setCellValue('I'.$xlsRow, $taxes_value)
              ->setCellValue('J'.$xlsRow, $unit_price)
              ->setCellValue('K'.$xlsRow, ($total_value + $orders->f('quantity')*$taxes_value))
              ->setCellValue('L'.$xlsRow, ($orders->f('vat')*$total_value/100) )     
              ->setCellValue('M'.$xlsRow, ($orders->f('vat')*$total_value_c/100))
              ->setCellValue('N'.$xlsRow, $total_value_c);
                 
             // ->setCellValue('H'.$xlsRow, $orders->f('type') == 2 ? gm('Yes') : gm('No') );
               $xlsRow++;*/
               $tmp_item=array($orders->f('serial_number'),
                    date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')),
                    $orders->f('origin_number'),
                    stripslashes(preg_replace("/[\n\r]/"," ",$orders->f('article_name'))),
                    $orders->f('item_code'),
                    display_number_exclude_thousand($orders->f('weight')*$orders->f('quantity')),
                    $tax_list,
                    display_number_exclude_thousand($orders->f('quantity')),
                    display_number_exclude_thousand($taxes_value),
                    display_number_exclude_thousand($unit_price),
                    display_number_exclude_thousand($total_value + $orders->f('quantity')*$taxes_value),
                    display_number_exclude_thousand($orders->f('vat')*$total_value/100) ,
                    display_number_exclude_thousand($orders->f('vat')*$total_value_c/100),
                    display_number_exclude_thousand($total_value_c)
                  );
            array_push($final_data,$tmp_item);
               }
   

    }else{ //daca e linie doar se afiseaza

      /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $orders->f('serial_number'))
              ->setCellValue('B'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')))
              ->setCellValue('C'.$xlsRow, $orders->f('origin_number'))
              ->setCellValue('D'.$xlsRow, $orders->f('article_name'))
              ->setCellValue('E'.$xlsRow, $orders->f('item_code'))
              ->setCellValue('F'.$xlsRow, $orders->f('weight')*$orders->f('quantity'))
              ->setCellValue('G'.$xlsRow, $tax_list)
              ->setCellValue('H'.$xlsRow, $orders->f('quantity'))
              ->setCellValue('I'.$xlsRow, $taxes_value)
              ->setCellValue('J'.$xlsRow, $unit_price)
              ->setCellValue('K'.$xlsRow, ($total_value + $orders->f('quantity')*$taxes_value))
              ->setCellValue('L'.$xlsRow, ($orders->f('vat')*$total_value/100) )     
              ->setCellValue('M'.$xlsRow, ($orders->f('vat')*$total_value_c/100))
              ->setCellValue('N'.$xlsRow, $total_value_c);
                 
             // ->setCellValue('H'.$xlsRow, $orders->f('type') == 2 ? gm('Yes') : gm('No') );
               $xlsRow++;*/

        $tmp_item=array($orders->f('serial_number'),
                    date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')),
                    $orders->f('origin_number'),
                    $orders->f('article_name'),
                    $orders->f('item_code'),
                    display_number_exclude_thousand($orders->f('weight')*$orders->f('quantity')),
                    $tax_list,
                    display_number_exclude_thousand($orders->f('quantity')),
                    display_number_exclude_thousand($taxes_value),
                    display_number_exclude_thousand($unit_price),
                    display_number_exclude_thousand($total_value + $orders->f('quantity')*$taxes_value),
                    display_number_exclude_thousand($orders->f('vat')*$total_value/100) ,
                    display_number_exclude_thousand($orders->f('vat')*$total_value_c/100),
                    display_number_exclude_thousand($total_value_c)
                  );
            array_push($final_data,$tmp_item);
     

    }  


      
    

  

  }else{ //in not horus

       /* $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')))
        ->setCellValue('B'.$xlsRow, $orders->f('buyer_name'))
        ->setCellValue('C'.$xlsRow, $orders->f('serial_number'))
        ->setCellValue('D'.$xlsRow, stripslashes($orders->f('acc_manager_name')))
        ->setCellValue('E'.$xlsRow, $orders->f('our_ref'))
        ->setCellValueExplicit('F'.$xlsRow, $orders->f('item_code'), PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValueExplicit('G'.$xlsRow, $orders->f('article_name'), PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValue('H'.$xlsRow, $orders->f('quantity'))
        ->setCellValue('I'.$xlsRow, ($orders->f('vat')*$total_value/100) )
        ->setCellValue('J'.$xlsRow, $total_value)
        ->setCellValue('K'.$xlsRow, ($orders->f('vat')*$total_value_c/100))
        ->setCellValue('L'.$xlsRow, $total_value_c);
        
       // ->setCellValue('H'.$xlsRow, $orders->f('type') == 2 ? gm('Yes') : gm('No') );
 $xlsRow++; */ 

      $tmp_item=array(date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')),
                    $orders->f('buyer_name'),
                    $orders->f('buyer_id'),
                    $orders->f('serial_number'),
                    stripslashes($orders->f('acc_manager_name')),
                    $orders->f('our_ref'),
                    $orders->f('item_code'),
                    $orders->f('article_name'),
                    display_number_exclude_thousand($orders->f('quantity')),
                    display_number_exclude_thousand($orders->f('vat')*$total_value/100),
                    display_number_exclude_thousand($total_value),
                    display_number_exclude_thousand($orders->f('vat')*$total_value_c/100) ,
                    display_number_exclude_thousand($total_value_c)
                  );
       array_push($final_data,$tmp_item);

  }

    


} //end while orders

if ($db_horus){
    /* $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$xlsRow, gm('Total'))
        ->setCellValue('B'.$xlsRow, '')
        ->setCellValue('C'.$xlsRow, '')
        ->setCellValue('D'.$xlsRow, '')
        ->setCellValue('E'.$xlsRow, '')
        ->setCellValue('F'.$xlsRow, '')
        ->setCellValue('G'.$xlsRow, '')
        ->setCellValue('H'.$xlsRow, $article_total_quantity)
        ->setCellValue('I'.$xlsRow, '')
        ->setCellValue('J'.$xlsRow, '')
        ->setCellValue('K'.$xlsRow, $article_total_value)
        ->setCellValue('L'.$xlsRow, '')
        ->setCellValue('M'.$xlsRow, '')
        ->setCellValue('N'.$xlsRow, $article_total_value_c);*/

  /*      $tmp_item=array(
            gm('Total'),
            '',
            '',
            '',
            '',
            '',
            '',
            $article_total_quantity,
            '',
            '',
            $article_total_value,
            '',
            '',
            $article_total_value_c,

          );
        array_push($final_data,$tmp_item);*/
        
        
        
        
        //->setCellValue('N'.$xlsRow, number_format($article_tax_total_value,2));

}else{
  /*$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$xlsRow, gm('Total'))
        ->setCellValue('B'.$xlsRow, '')
        ->setCellValue('C'.$xlsRow, '')
        ->setCellValue('D'.$xlsRow, '')
        ->setCellValue('E'.$xlsRow, '')
        ->setCellValue('F'.$xlsRow, '')
        ->setCellValue('G'.$xlsRow, '')
        ->setCellValue('H'.$xlsRow, $article_total_quantity)
        ->setCellValue('I'.$xlsRow, '')
        ->setCellValue('J'.$xlsRow, $article_total_value)
        ->setCellValue('K'.$xlsRow, '')
        ->setCellValue('L'.$xlsRow, $article_total_value_c);*/
 /*   $tmp_item=array(
            gm('Total'),
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            $article_total_quantity,
            '',
            $article_total_value,
            '',
            $article_total_value_c,

          );
   array_push($final_data,$tmp_item);*/
}

doQueryLog();
   $from_location='upload/'.DATABASE_NAME.'/export_invoices_detail_report_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();



if ($db_horus){
  $rows_format='F2:N'.$xlsRow;
  $objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');

}else{
  $rows_format='G2:K'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');
}

$objPHPExcel->getActiveSheet()->setTitle('Invoice detail report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// set headers
doQueryLog();
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>