<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

  ini_set('memory_limit','786M');
 
  global $database_config;

    $db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

  $db_users = new sqldb($db_config);
  $account_users=array();
  $account_users_data=$db_users->query("SELECT user_id,first_name,last_name FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();

  foreach($account_users_data as $key=>$value){
    $account_users[$value['user_id']]=htmlspecialchars_decode(stripslashes($value['first_name'].' '.$value['last_name']));
  }


setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Articles detail report")
               ->setSubject("Articles detail report")
               ->setDescription("Articles report export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Articles");

$objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);*/

//$filename ="articles_detail_report.xls";
$filename ="articles_detail_report.csv";

$time_start = $in['time_start'];
$time_end = $in['time_end'];
if(!$time_start && !$time_end){
  $filter2 = " AND 1=1 ";
}
if($in['customer_id']){
    $filter.=" and tblinvoice.buyer_id = ".$in['customer_id'];
}
if($in['m_id']){
  $filter .= ' AND tblinvoice.acc_manager_id ='.$in['m_id'].' ';
}
if($in['article_category_id']){
    $filter.=" and pim_articles.article_category_id = ".$in['article_category_id'];
}
if($in['u_id']){
    $filter.=" AND pim_orders.author_id=".$in['u_id'] ;
}
if($in['article_id']){
    $filter.=" and pim_articles.article_id = ".$in['article_id'];
}
if($in['supplier_id']){
    $filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';          
}

$headers = array(gm('Code'),
                    gm('Internal Name'),
                    gm("Customer"),
                    gm('Company id'),
                    gm("Country"),
                    gm("Supplier"),
                    gm("Family"),
                    gm("Account manager name"),
                    gm("Customers"),
                    gm("Quantity"),
                    gm("Margin"),
                    gm("Margin %"),
                    gm("Main Country"),
                    gm("Total Value")
      );

/*$objPHPExcel->getActiveSheet(0)->getStyle('A1:K1')->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Article"))
            ->setCellValue('B1', gm("Customer"))
            ->setCellValue('C1', gm("Country"))
      ->setCellValue('D1', gm("Supplier"))
      ->setCellValue('E1', gm("Family"))
      ->setCellValue('F1', gm("Account manager name"))
      ->setCellValue('G1', gm("Invoice date"))
      ->setCellValue('H1', gm("Customers"))
      ->setCellValue('I1', gm("Quantity"))
      ->setCellValue('J1', gm("Margin"))
      ->setCellValue('K1', gm("Main Country"))
      ->setCellValue('L1', gm("Total Value"));*/

$i=0;
$xlsRow = 2;

$articles_data = $db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.discount as global_discount, tblinvoice.paid, tblinvoice.discount_line_gen, tblinvoice_line.discount, tblinvoice.acc_manager_id, tblinvoice.invoice_date,
                tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice.currency_rate,tblinvoice.currency_type,tblinvoice.buyer_country_id,
                tblinvoice_line.quantity, tblinvoice_line.price,tblinvoice_line.purchase_price, pim_articles.article_id, 
                pim_articles.item_code AS invoice_item_code,
                customers.name AS invoice_buyer_name,
                pim_articles.supplier_name, pim_articles.supplier_id,country.name as country_name,pim_articles.internal_name,pim_article_categories.name AS cat_name,
                CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) as invoice_contact_name
                FROM tblinvoice_line
                LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
                LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
                LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
                LEFT JOIN country ON tblinvoice.buyer_country_id=country.country_id
                LEFT JOIN pim_article_categories ON pim_articles.article_category_id=pim_article_categories.id
                LEFT JOIN customer_contacts ON tblinvoice.contact_id=customer_contacts.contact_id
                WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
                 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
                ORDER BY invoice_item_code ASC, invoice_buyer_name ASC");

$total_contacts = array();
$total_companies = array();
$total_contacts2 = array();
$total_companies2 = array();
$art = array();
$total_amount = 0;
$big_margin = 0;
$big_margin_percent=0;

while ($articles_data->next()) {
    $del_a = 0;
    $ndel_a = 0;
    $acc_manager_name='';
    //$acc_manager_name =get_user_name($articles_data->f('acc_manager_id'));
    if(array_key_exists($articles_data->f('acc_manager_id'), $account_users)){
      $acc_manager_name=$account_users[$articles_data->f('acc_manager_id')];
    }

    $currency_rate  = return_value($articles_data->f('currency_rate'));
    if(!$currency_rate){
        $currency_rate = 1;     
    }
    $global_disc = $articles_data->f('global_discount');

    $discount_line = $articles_data->f('discount');
    if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
        $discount_line = 0;
    }
    if($articles_data->f('apply_discount') < 2){
        $global_disc = 0;
    }
    $q = $articles_data->f('quantity') ;
    $price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

    $line_total=$price_line * $q;   

    if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
        $line_total = $line_total*return_value($articles_data->f('currency_rate'));
    }   

    $total_amount_purchase+= $articles_data->f('purchase_price') * $currency_rate* $articles_data->f('quantity') ;

    if($articles_data->f('buyer_id')){
        if(!$total_companies[$articles_data->f('article_id')]){
            $total_companies[$articles_data->f('article_id')] = 0;
        }       
    }elseif($articles_data->f('contact_id')){
        if(!$total_contacts[$articles_data->f('article_id')]){
            $total_contacts[$articles_data->f('article_id')] = 0;
        }       
    }

    if($articles_data->f('buyer_id')){
        $art[$articles_data->f('article_id')]['customer'][$articles_data->f('buyer_id')] += $line_total - ($line_total*$global_disc/100);
        $art[$articles_data->f('article_id')]['customerq'][$articles_data->f('buyer_id')] += $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['customer_margin'][$articles_data->f('buyer_id')]+= $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
        $art[$articles_data->f('article_id')]['customer_purchase_amount'][$articles_data->f('buyer_id')] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
         //$art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('buyer_id')] = get_country_name($articles_data->f('buyer_country_id'));
        $art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('buyer_id')] = addslashes($articles_data->f('country_name'));
         $art[$articles_data->f('article_id')]['invoice_date'][$articles_data->f('buyer_id')] = date(ACCOUNT_DATE_FORMAT,$articles_data->f('invoice_date'));
         $art[$articles_data->f('article_id')]['account_manager_name'][$articles_data->f('buyer_id')] = $acc_manager_name;
         $art[$articles_data->f('article_id')]['customer_name'][$articles_data->f('buyer_id')] = $articles_data->f('invoice_buyer_name');
    }else{
        $art[$articles_data->f('article_id')]['contact'][$articles_data->f('contact_id')] += $line_total - ($line_total*$global_disc/100);
        $art[$articles_data->f('article_id')]['contactq'][$articles_data->f('contact_id')] += $articles_data->f('quantity');
        $art[$articles_data->f('article_id')]['contact_margin'][$articles_data->f('contact_id')] += $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
        $art[$articles_data->f('article_id')]['contact_purchase_amount'][$articles_data->f('contact_id')] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
        //$art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('contact_id')] = get_country_name($articles_data->f('buyer_country_id'));
        $art[$articles_data->f('article_id')]['invoice_buyer_country'][$articles_data->f('contact_id')] = addslashes($articles_data->f('country_name'));
        $art[$articles_data->f('article_id')]['invoice_date'][$articles_data->f('contact_id')] = date(ACCOUNT_DATE_FORMAT,$articles_data->f('invoice_date'));
        $art[$articles_data->f('article_id')]['account_manager_name'][$articles_data->f('contact_id')] =  $acc_manager_name;
        $art[$articles_data->f('article_id')]['contact_name'][$articles_data->f('contact_id')] = $articles_data->f('invoice_contact_name');
    }
    $art[$articles_data->f('article_id')]['article'] += $line_total - ($line_total*$global_disc/100);
    $art[$articles_data->f('article_id')]['q'] += $articles_data->f('quantity') ;
    $art[$articles_data->f('article_id')]['margin'] += $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
    $art[$articles_data->f('article_id')]['currency_rate']= $currency_rate;
    $art[$articles_data->f('article_id')]['supplier_name'] = $articles_data->f('supplier_name');
    $art[$articles_data->f('article_id')]['invoice_buyer_name'] = $articles_data->f('invoice_buyer_name');
   
    //$art[$articles_data->f('article_id')]['acc_manager_name'] = $acc_manager_name;
    //$art[$articles_data->f('article_id')]['invoice_date'] =  date(ACCOUNT_DATE_FORMAT,$articles_data->f('invoice_date'));
    $art[$articles_data->f('article_id')]['purchase_amount'] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
    $art[$articles_data->f('article_id')]['internal_name'] = $articles_data->f('internal_name');
    $art[$articles_data->f('article_id')]['item_code'] = $articles_data->f('invoice_item_code');
    $art[$articles_data->f('article_id')]['category_name'] = $articles_data->f('cat_name') ? $articles_data->f('cat_name') :'';
}

$big_quantity = 0;
$big_customers = 0;
$total_del = 0;
$total_ndel = 0;
$i = 0;
$j=0;
$total_margin=0;
$total_margin_percent=0;
$final_data=array();
foreach ($art as $art_id => $art_data) {
    $total_contacts[$art_id] = count($art_data['customer']);
    $total_companies[$art_id] = count($art_data['contact']);
    $total_quantity = $art_data['q'];
    
    $big_quantity+=$total_quantity;
    $customers = $total_companies[$art_id]+$total_contacts[$art_id];

    $big_customers+=$customers;
    $total_amount+= $art_data['article'];
    $article_name = htmlspecialchars_decode($art_data['internal_name']);
    $article_code = $art_data['item_code'];
    $article_fam = $art_data['category_name'];
    $article_supplier = $art_data['supplier_name'];
    $invoice_buyer_name = $art_data['invoice_buyer_name'];
    $invoice_buyer_country = $art_data['invoice_buyer_country'];
    $account_manager_name = $art_data['acc_manager_name'];
    $delivered_a    = $art_data['delivered'];
    $ndelivered_a   = $art_data['ndelivered'];
    $total_del+=$delivered_a;
    $total_ndel+=$ndelivered_a;
    $total_margin=$art_data['margin'];
    $total_margin_percent=0;

    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
      if($total_amount_purchase){
         $total_margin_percent=($art_data['margin']/$total_amount_purchase)*100;
      }elseif($art_data['margin']){
         $total_margin_percent=100;
      }
    }else{
          if($art_data['article']){
             $total_margin_percent=($art_data['margin']/$art_data['article'])*100;
          }elseif($art_data['margin']){
             $total_margin_percent=100;
          }
    }
   //var_dump($total_margin_percent);
   // $invoice_date =$art_data['invoice_date'];
    /*$article_row=array(         
            'TIME_START'        => $in['time_start'],
            'TIME_END'          => $in['time_end'],
            'XPAG'              => $in['pag'],
            'CUSTOM'            => $in['custom'] ? '&custom=1' : '&custom=',
            'ARTICLE_N'         => $article_code.' ('.$article_name.')',
            'TOTAL_QUANTITY'    => $total_quantity,
            'FAMILY'            => $article_fam,
            'SUPPLIER'          => $article_supplier,
            'CUSTOMER'          => $invoice_buyer_name,
            'COUNTRY'           => $invoice_buyer_country,
            'ACC_MANAGER'       => $account_manager_name,
            'CUSTOMERS'         => $customers,
            'TOTAL_CLIENT'      => place_currency(display_number($art_data['article'])),
            'TOTAL_MARGIN'      => place_currency(display_number($total_margin)),
            'TOTAL_MARGIN_PERCENT'=> display_number($total_margin_percent),
            'art_id'            => $art_id,         
            'to_collaps'        => $in['customer_id'] ? '' : 'to_collaps',
            'colspan_c_id'      => $in['customer_id'] ? 'colspan="2"' : '',
            'not_c_id'          => $in['customer_id'] ? false : true,
            'DELIVERED_A'       => $delivered_a,
            'NDELIVERED_A'      => $ndelivered_a,
            'article_collaps'   => array(),
            'invoice_date'      => $invoice_date,  
    ); */ 


  if($art_data['customer']){
    foreach ($art_data['customer'] as $key => $value) {
      //$client_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$key."' ");
      //$main_address_buyer=get_country_name($db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$key."' "));
      $customer_total_margin_percent=0;
      if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
        if($art_data['customer_purchase_amount'][$key]){
          $customer_total_margin_percent = $art_data['customer_margin'][$key]/$art_data['customer_purchase_amount'][$key]*100;
        }elseif($art_data['customer_margin'][$key]){
          $customer_total_margin_percent =100;
        }
      }else{
         if($value){
            $customer_total_margin_percent = $art_data['customer_margin'][$key]/$value*100;
          }elseif($art_data['customer_margin'][$key]){
            $customer_total_margin_percent =100;
          }
      }
      /*$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$xlsRow, $article_code.' ('.$article_name.')')
        //->setCellValue('B'.$xlsRow, $client_name)
        ->setCellValue('B'.$xlsRow, $art_data['customer_name'][$key])
        ->setCellValue('C'.$xlsRow, $art_data['invoice_buyer_country'][$key])
        ->setCellValue('D'.$xlsRow, $article_supplier)
        ->setCellValue('E'.$xlsRow, $article_fam)
        ->setCellValue('F'.$xlsRow, $art_data['account_manager_name'][$key])
        ->setCellValue('G'.$xlsRow, $art_data['invoice_date'][$key])
        ->setCellValue('H'.$xlsRow, $customers)
        ->setCellValue('I'.$xlsRow, $art_data['customerq'][$key])
        ->setCellValue('J'.$xlsRow, display_number($art_data['customer_margin'][$key]).' ('.display_number($customer_total_margin_percent).' %)')
        //->setCellValue('K'.$xlsRow, $main_address_buyer)
        ->setCellValue('K'.$xlsRow, $art_data['invoice_buyer_country'][$key])
        ->setCellValue('L'.$xlsRow, $value);

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);*/
      $xlsRow++;

      $tmp_item=array(
        $article_code,
        $article_name,
        $art_data['customer_name'][$key],
        $key,
        $art_data['invoice_buyer_country'][$key],
        $article_supplier,
        $article_fam,
        $art_data['account_manager_name'][$key],
        $customers,
        $art_data['customerq'][$key],
        display_number_exclude_thousand($art_data['customer_margin'][$key]),
        display_number_exclude_thousand($customer_total_margin_percent),
        $art_data['invoice_buyer_country'][$key],
        $value
      );
      array_push($final_data,$tmp_item);

    }
  }


  if($art_data['contact']){
    foreach ($art_data['contact'] as $key => $value) {
      //$client_name = $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id = '".$key."' ");
      $contact_total_margin_percent =0;
       if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
          if($art_data['contact_purchase_amount'][$key]){
            $contact_total_margin_percent = $art_data['contact_margin'][$key]/$art_data['contact_purchase_amount'][$key]*100;
          }elseif($art_data['contact_margin'][$key]){
            $contact_total_margin_percent =100;
          }
        }else{
           if($value){
              $contact_total_margin_percent = $art_data['contact_margin'][$key]/$value*100;
            }elseif($art_data['contact_margin'][$key]){
              $contact_total_margin_percent =100;
            }
        }
      /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $article_name)
              //->setCellValue('B'.$xlsRow, $client_name)
              ->setCellValue('B'.$xlsRow, $art_data['contact_name'][$key])
              ->setCellValue('C'.$xlsRow, $art_data['invoice_buyer_country'][$key])
        ->setCellValue('D'.$xlsRow, $article_supplier)
        ->setCellValue('E'.$xlsRow, $article_fam)
        ->setCellValue('F'.$xlsRow, $art_data['account_manager_name'][$key])
        ->setCellValue('G'.$xlsRow, $art_data['invoice_date'][$key])
        ->setCellValue('H'.$xlsRow, $customers)
        ->setCellValue('I'.$xlsRow, $art_data['contactq'][$key])
        ->setCellValue('J'.$xlsRow, display_number($art_data['contact_margin'][$key]).' ('.display_number($contact_total_margin_percent).') %')
        ->setCellValue('K'.$xlsRow, '')
        ->setCellValue('L'.$xlsRow, $value);
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);*/
      $xlsRow++;

      $tmp_item=array(
          $article_code,
          $article_name,
          $art_data['contact_name'][$key],
          $key,
          $art_data['invoice_buyer_country'][$key],
          $article_supplier,
          $article_fam,
          $art_data['account_manager_name'][$key],
          $customers,
          $art_data['contactq'][$key],
          display_number_exclude_thousand($art_data['contact_margin'][$key]),
          display_number_exclude_thousand($contact_total_margin_percent),
          '',
          $value
      );
      array_push($final_data,$tmp_item);

    }
  }
  //$view->loop('article_row');
  $i++;
}
 
$total_customers = count(array_count_values($total_companies))+count(array_count_values($total_contacts));

array_push($final_data,
    array(
       gm('Total'),
       '',
       '',
       '',
       '',
       '',
       '',
       '',
       '',
       $big_quantity,
       '',
       '',
       '',
       $total_amount
    )
  );

    doQueryLog();
    $from_location='upload/'.DATABASE_NAME.'/export_articles_invoices_detail_report_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();



  $from_location='upload/'.DATABASE_NAME.'/export_articles_invoices_detail_report_'.time().'.csv';
    $fp = fopen($from_location, 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    $objReader = PHPExcel_IOFactory::createReader('CSV');

    $objPHPExcel = $objReader->load($from_location);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    unlink($from_location);
    $objWriter->save("php://output");
    doQueryLog();
    exit();

/*$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':L'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$xlsRow, gm('Total'))
        ->setCellValue('B'.$xlsRow, '')
        ->setCellValue('C'.$xlsRow, '')
        ->setCellValue('D'.$xlsRow, $total_customers)
        ->setCellValue('E'.$xlsRow, '')
        ->setCellValue('F'.$xlsRow, '')
        ->setCellValue('G'.$xlsRow, '')
        ->setCellValue('H'.$xlsRow, '')
        ->setCellValue('I'.$xlsRow, $big_quantity)
        ->setCellValue('J'.$xlsRow, '')
        ->setCellValue('K'.$xlsRow, '')
        ->setCellValue('L'.$xlsRow, $total_amount);*/


// set column width to auto
foreach(range('A','L') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}
/*
$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$xlsRow)->applyFromArray($styleArray);

$styleArray = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_MEDIUM
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$xlsRow)->applyFromArray($styleArray);
*/
$rows_format='K1:L'.$xlsRow;

$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');

$objPHPExcel->getActiveSheet()->setTitle('Articles detail report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>