<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');

$path = __DIR__.'/../view/';

$html='expenses_report_print_1.html';

$view_html = new at($path.$html);
$db = new sqldb();
$time_start = $in['time_start'];
$time_end = $in['time_end'];

$img = 'images/no-logo.png';

if($in['logo']){		
	$print_logo = $in['logo'];	
}else {
	$print_logo=ACCOUNT_LOGO_TIMESHEET;
}
if($print_logo){
	$img = $print_logo;
	$size = getimagesize($img);
	$ratio = 250 / 77;
	if($size[0]/$size[1] > $ratio ){
		$attr = 'width="250"';
	}else{
		$attr = 'height="77"';
	}
}

$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";
if(!$time_start && !$time_end){
	$filter_date = " 1=1 ";
	$timeframe = ' - ';
}else{
	$timeframe = date(ACCOUNT_DATE_FORMAT,$time_start)." - ".date(ACCOUNT_DATE_FORMAT,$time_end);
}

$filter = ' AND 1=1 ';

if($in['customer_id'] || $in['customer_id2']){
	$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
	$filter .=' AND projects.customer_id='.$cid2.' ';	
}
if($in['project_id'] || $in['project_id2']){
	$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
	$filter .=' AND projects.project_id='.$pid2.' ';		
}
if($in['user_id'] || $in['user_id2']){
	$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
	$filter .=' AND project_expenses.user_id='.$uid2.' ';
	$filter_u = ' AND project_user.user_id='.$uid2.' ';
}
if($in['bunit_id'] || $in['bunit_id2']){
	$bid2=$in['bunit_id2'] ? $in['bunit_id2'] : $in['bunit_id'];
	$filter .= ' AND projects.bunit='.$bid2.' ';
}
if($in['internal']=='true'){
	$filter.=" AND projects.invoice_method='0' ";
}

$persons = array();
$person = $db->query("SELECT user_name, user_id FROM project_user INNER JOIN projects ON project_user.project_id=projects.project_id WHERE 1=1 $filter_u");
while($person->next()) {
	$persons[$person->f('user_id')] = $person->f('user_name');
}

$total_amount_p = 0;
$total_amount_unbill = 0;
$total_amount_bill = 0;
$total_amount_internal=0;

$expense = $db->query("SELECT project_expenses.id AS p_ex_id,amount ,project_expenses.user_id, unit_price, projects.name AS p_name, expense.name AS e_name, expense.unit AS e_unit,
					projects.project_id AS p_id, project_expenses.expense_id, project_expenses.date AS exp_date,
					project_expenses.billable, projects.active, project_expences.expense_id AS billable_exp_id, project_expenses.picture, projects.invoice_method
FROM project_expenses
INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
INNER JOIN projects ON project_expenses.project_id = projects.project_id
LEFT JOIN project_user ON ( project_expenses.user_id = project_user.user_id AND project_expenses.project_id = project_user.project_id )
LEFT JOIN project_expences ON ( project_expenses.expense_id = project_expences.expense_id AND project_expenses.project_id = project_expences.project_id )
WHERE $filter_date $filter
ORDER BY p_name, exp_date,e_name");

$max_expenses = $expense->records_count();
$is_data = false;
if($max_expenses > 0){
	$is_data = true;
}
$expenses_data = array();
while ($expense->next()) {
	$amount = $expense->f('amount');
	if($expense->f('unit_price')){
		$amount = $expense->f('amount')*$expense->f('unit_price');
	}
	$total_amount_p +=$amount;	
	$quantity = display_number($expense->f('amount')).' ('.$expense->f('e_unit').')';
	if(!$expense->f('e_unit')){
		$quantity = display_number($expense->f('amount'));
	}
	if($expense->f('unit_price')){
		$unit_price = display_number($expense->f('unit_price'));
	}else{
		$unit_price = ' - ';
	}
	if(!$expense->f('e_unit') && !$expense->f('unit_price')){
		$quantity = ' - ';
		$unit_price = display_number($expense->f('amount'));
	}
	if($expense->f('active')==2){
		if(($expense->f('invoice_method')==1) && ($expense->f('billable')==1)){
			//for billable
			$total_amount_bill +=$amount;			
			$exp_unbillable = '&nbsp;';
			$exp_internal = '&nbsp;';
			$exp_billable = display_number($amount);
		}else if($expense->f('invoice_method')==0){
			$total_amount_internal +=$amount;	
			$total_amount_unbill +=$amount;
			$exp_unbillable= display_number($amount);
			$exp_internal= display_number($amount);
			$exp_billable= '&nbsp;';
		}else{
			//for unbillable
			$total_amount_unbill +=$amount;			
			$exp_unbillable = display_number($amount);
			$exp_billable = '&nbsp;';
			$exp_internal= '&nbsp;';
		}
	}else{//$expense->f('active')==1
		if(($expense->f('invoice_method')==1) && ($expense->f('billable_exp_id'))){
			//for billable
			$total_amount_bill +=$amount;			
			$exp_unbillable = '&nbsp;';
			$exp_internal = '&nbsp;';
			$exp_billable = display_number($amount);
		}else if($expense->f('invoice_method')==0){
			$total_amount_internal +=$amount;	
			$total_amount_unbill +=$amount;
			$exp_unbillable= display_number($amount);
			$exp_internal= display_number($amount);
			$exp_billable= '&nbsp;';
		}else{
			//for unbillable
			$total_amount_unbill +=$amount;			
			$exp_unbillable = display_number($amount);
			$exp_billable = '&nbsp;';
			$exp_internal= '&nbsp;';
		}
	}
	$expenses_data[$expense->f('p_ex_id')]['unit_price'] = $unit_price;
	$expenses_data[$expense->f('p_ex_id')]['quantity'] = $quantity;
	$expenses_data[$expense->f('p_ex_id')]['task_n'] = $expense->f('e_name');
	$expenses_data[$expense->f('p_ex_id')]['p_name'] = $expense->f('p_name');
	$expenses_data[$expense->f('p_ex_id')]['person'] = $persons[$expense->f('user_id')];
	$expenses_data[$expense->f('p_ex_id')]['expense_date'] = date(ACCOUNT_DATE_FORMAT,$expense->f('exp_date'));
	$expenses_data[$expense->f('p_ex_id')]['exp_unbillable'] = $exp_unbillable;
	$expenses_data[$expense->f('p_ex_id')]['exp_billable'] = $exp_billable;	
	$expenses_data[$expense->f('p_ex_id')]['exp_internal'] = $exp_internal;	
	$expenses_data[$expense->f('p_ex_id')]['picture'] = $expense->f('picture');
}
//general data
$customer_name = gm('All customers');
$project_name = gm('All projects');
$task_name = gm('All tasks');
$staff_name = gm('All staff');
$bunit_name=gm('All business units');

if($in['customer_id'] || $in['customer_id2']){	
	$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
	$project_name = gm('All projects');
	$task_name = gm('All tasks');
	$staff_name = gm('All staff');	
	if($in['project_id'] || $in['project_id2']){	
		$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];	
		$project_name = $db->field("SELECT name FROM projects WHERE customer_id='".$cid2."' AND project_id='".$pid2."' ");
		if($in['task_id'] || $in['task_id2']){	
			$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];		
			$task_name = $db->field("SELECT task_name FROM tasks WHERE project_id='".$pid2."' AND task_id='".$tid2."' ");
		}
		if($in['user_id'] || $in['user_id2']){	
			$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];		
			$staff_name = $db->field("SELECT user_name FROM project_user WHERE project_id='".$pid2."' AND user_id='".$uid2."' ");
		}
	}
	$customer_name = $db->field("SELECT name FROM customers WHERE customer_id='".$cid2."' ");
}elseif ($in['user_id'] || $in['user_id2']){
	$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];	
	$staff_name = $db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' ");
	if($in['customer_id'] || $in['customer_id2']){
		$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
		$customer_name = $db->field("SELECT name FROM customers WHERE customer_id='".$cid2."' ");
	}
	if($in['project_id'] || $in['project_id2']){
		$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];		
		$customer_name = $db->field("SELECT projects.company_name FROM projects WHERE projects.project_id='".$pid2."' ");
		$project_name = $db->field("SELECT name FROM projects WHERE project_id='".$pid2."' ");		
	}
	if($in['task_id'] || $in['task_id2']){	
		$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];	
		$task_name = $db->field("SELECT task_name FROM tasks WHERE task_id='".$tid2."' ");
		$customer_name = $db->field("SELECT projects.company_name FROM projects INNER JOIN task_time on projects.project_id=task_time.project_id WHERE task_time.task_id='".$tid2."' ");
		$project_name = $db->field("SELECT name FROM projects INNER JOIN task_time ON projects.project_id=task_time.project_id WHERE task_id='".$tid2."' ");		
	}
}elseif($in['project_id'] || $in['project_id2']){
	$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];	
	$project_name = $db->field("SELECT name FROM projects WHERE project_id='".$pid2."' ");
}elseif ($in['task_id'] || $in['task_id2']){	
	$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];
	$task_name = $db->field("SELECT task_name FROM tasks WHERE task_id='".$tid2."' ");
}elseif ($in['user_id'] || $in['user_id2']){
	$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];	
	$staff_name = $db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' ");
}elseif($in['bunit_id'] || $in['bunit_id2']){
	$bid2=$in['bunit_id2'] ? $in['bunit_id2'] : $in['bunit_id'];
	$bunit_name=$db->field("SELECT name FROM project_bunit WHERE id='".$bid2."' ");
}

// echo "<pre>";
// print_r($expenses_data);
// exit();

//formula de pus deoparte
$ceil = ceil($max_expenses/4);
for ($i=0; $i < $ceil; $i++) { 
	$k=0;
	$j=0;

	foreach ($expenses_data as $key => $value) {		
		if($k>=$i*4 && $k<($i+1)*4){
			$img="";
			if($value['picture']){	
				$attr = '';	
				$img = UPLOAD_PATH.DATABASE_NAME."/receipt/".$value['picture'];
				$size = getimagesize($img);
				$ratio = 320 / 320;
				if($size[0]/$size[1] > $ratio ){
					$attr = 'width="320"';
				}else{
					$attr = 'height="320"';
				}
			}			
			$view_html->assign(array(
				'attr_'.$j			=> 	$attr,
				'picture_'.$j 		=>	$img,
				'expense_date_'.$j 	=>	$value['expense_date'],
				'p_name_'.$j 		=>	$value['p_name'],
				'task_n_'.$j 		=>	$value['task_n'],
				'person_'.$j 		=>	$value['person'],
				'quantity_'.$j 		=>	$value['quantity'],
				'unit_price_'.$j 		=>	$value['unit_price'],
				'exp_unbillable_'.$j 	=>	$value['exp_unbillable'],
				'exp_billable_'.$j 	=>	$value['exp_billable'],
				'exp_internal_'.$j      =>    $value['exp_internal'], 

				'TIMEFRAME' 		=> 	$timeframe,
				'STAFFS'			=> 	$staff_name,
			),'p_hours_row');	
			$j++;
		}
		$k++;
	}	
	$view_html->assign(array(				
		'empty_space'		=> ($ceil-$i)==1 ? false : true,
	),'p_hours_row');	
	$view_html->loop('p_hours_row');		
}
// exit();
$view_html->assign(array(
	'attr'				=> $attr,
	'account_logo'        		=> $img,
	'COLLSPAN'				=> 5,
	'EXP_TOTAL_AMOUNT'		=> display_number($total_amount_p),
	'EXP_TOTAL_UNBILL_AMOUNT'	=> display_number($total_amount_unbill),
	'EXP_TOTAL_BILL_AMOUNT'		=> display_number($total_amount_bill),
	'EXP_TOTAL_AMOUNT_INTERNAL'	=> display_number($total_amount_internal),

	'TIMEFRAME' 			=> $timeframe,
	'CUSTOMER'				=> $customer_name,
	'PROJECTS'				=> $project_name,
	'TASKS'				=> $task_name,
	'STAFFS'				=> $staff_name,
));


return $view_html->fetch();