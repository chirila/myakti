<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class ReportCatalogue extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}
		public function get_Data(){
			ini_set('memory_limit','2G');
			$in=$this->in;
			$result=array();

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(23,59,59,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(23,59,59,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(23,59,59,$month,$today,$year);
						//$time_end = mktime(0,0,0,$month,$today,$year)+84600;
					}
					$result['period_txt']='';
					break;
			}
			$in['time_start']=$time_start;
			$in['time_end']=$time_end;
			$filter = ' AND 1=1';
			$filterp = ' AND 1=1';
			if(!$in['u_id']){
				$in['u_id'] = $in['user_id'];
			}
			if(!$in['m_id']){
				$in['m_id'] = $in['manager_id'];
			}
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}	
			if($in['article_category_id']){
				$filter .= ' AND pim_articles.article_category_id ='.$in['article_category_id'].' ';
			}
			if($in['article_id']){
				$filter .= ' AND pim_articles.article_id ='.$in['article_id'].' ';					
			}

			if($in['supplier_id']){
				$filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';					
			}

			$where = " invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ";

			$articles_data = $this->db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.paid, tblinvoice.discount as global_discount, tblinvoice.discount_line_gen, tblinvoice_line.discount, 
				tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice.currency_type, tblinvoice.currency_rate, tblinvoice_line.quantity, tblinvoice_line.price, tblinvoice_line.purchase_price, pim_articles.article_id
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
				WHERE ".$where." ".$filter );
			$total_articles = 0;
			$total_amount = 0;
			$total_amount_purchase =0;
			$total_contacts = array();
			$total_companies = array();
			$delivered_a = 0;
			$ndelivered_a = 0;
			$delivered_amount = 0;
			$ndelivered_amount = 0;
			$paid_amount = 0;
			$npaid_amount = 0;
			$line_d = 0;
			$amount_art = 0;
			$big_margin = 0;
			$big_margin_percent=0;
			while($articles_data->next()){
				
				$global_disc = $articles_data->f('global_discount');
				
				$del_a = 0;
				$ndel_a = 0;
			   
			    $currency_rate  = return_value($articles_data->f('currency_rate'));
				if(!$currency_rate){
					$currency_rate = 1;		
				}

				$total_articles+=$articles_data->f('quantity');

				$discount_line = $articles_data->f('discount');
				if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
					$discount_line = 0;
				}
				if($articles_data->f('apply_discount') < 2){
					$global_disc = 0;
				}
				$q = $articles_data->f('quantity');
				$price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

				$line_total=$price_line * $q;		
				if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
					$line_total = $line_total*return_value($articles_data->f('currency_rate'));
				}

				$total_amount+= $line_total - ($line_total*$global_disc/100);	
				$total_amount_purchase+= $articles_data->f('purchase_price') * $currency_rate* $articles_data->f('quantity') ;

				if($articles_data->f('buyer_id')){
					if(!in_array($articles_data->f('buyer_id'), $total_companies)){
						$total_companies[] = $articles_data->f('buyer_id');
					}
				}elseif($articles_data->f('contact_id')){
					if(!in_array($articles_data->f('contact_id'), $total_contacts)){
						$total_contacts[] = $articles_data->f('contact_id');
					}
				}



				if($articles_data->f('rdy_invoice')==0){
					$ndel_a = $articles_data->f('quantity');
					$del_a = 0;
					$delivered_amount += 0;
					$ndelivered_amount += $line_total - ($line_total*$global_disc/100);
				}elseif($articles_data->f('rdy_invoice')==1){
					$del_a = $articles_data->f('quantity');
					$ndel_a = 0;
					$delivered_amount+= $line_total - ($line_total*$global_disc/100);
					$ndelivered_amount+=0;
				}else{
					$total_art_quantity = $articles_data->f('quantity');		
					$del_a = $this->db->field("SELECT SUM(pim_orders_delivery.quantity)  
					FROM pim_order_articles LEFT JOIN pim_orders_delivery 
					ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id 
					WHERE pim_order_articles.article_id = '".$articles_data->f('article_id')."' AND pim_orders_delivery.order_id = '".$articles_data->f('order_id')."' ");
					$ndel_a = $total_art_quantity-$del_a;
					$delivered_amount+= ($line_total - ($line_total*$global_disc/100))/round($total_art_quantity)*round($del_a);
					$ndelivered_amount+= ($line_total - ($line_total*$global_disc/100))/round($total_art_quantity)*round($ndel_a);
				}
				//paid / not paid chart
				$line_d = $articles_data->f('discount');
				if($articles_data->f('apply_discount') ==0 || $articles_data->f('apply_discount') == 2){
					$line_d = 0;
				}
				$amount_art = $articles_data->f('quantity') *($articles_data->f('price') - ( $articles_data->f('price') * $line_d / 100 ) );
				$amount_art = round($amount_art,ARTICLE_PRICE_COMMA_DIGITS);
				
/*				if($articles_data->f('paid')==0){
					$paid_amount+=0;
					$npaid_amount+=$amount_art;
				}elseif($articles_data->f('paid')==1 ){
					$paid_amount+=$amount_art;
					$npaid_amount+=0;
				}*/
				



				$delivered_a+=$del_a;
				$ndelivered_a+=$ndel_a;
				$big_margin+=$currency_rate* $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
				
			}

			$paid_amounts=$this->db->query("SELECT sum(tblinvoice_payments.amount) as paid_amount , COUNT( DISTINCT tblinvoice_line.id ) as nr_lines, tblinvoice.buyer_id,tblinvoice.acc_manager_id
					                           FROM  tblinvoice_payments 
					                           INNER JOIN   tblinvoice on tblinvoice.id=tblinvoice_payments.invoice_id
					                           LEFT JOIN tblinvoice_line ON tblinvoice_line.invoice_id = tblinvoice.id
											   LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
					                           LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
					                           WHERE  ".$where." ".$filter."  AND tblinvoice_payments.credit_payment='0' GROUP BY tblinvoice.id");
			$paid_amount = $paid_amounts->f('paid_amount')/$paid_amounts->f('nr_lines');
			$npaid_amount = $total_amount - $paid_amount;

			//$big_margin_percent=($big_margin/($total_amount))*100;
			$big_margin_percent=0;
			if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				if($total_amount_purchase){
					$big_margin_percent=($big_margin/($total_amount_purchase))*100;
				}elseif($big_margin){
					$big_margin_percent=100;
				}
			}else{
				if($total_amount){
					$big_margin_percent=($big_margin/($total_amount))*100;
				}elseif($big_margin){
					$big_margin_percent=100;
				}
			}

			$total_customers = count($total_companies)+count($total_contacts);
			$supplier_db = false; //suppliers filter tab only for Nodia
			if(DATABASE_NAME=='c826e597_4104_fdb6_0d30334a2992' || DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11'){
				$supplier_db = true;
			}
			$result['BIG_TOTAL_A'] 			= place_currency(display_number($total_amount));
			$result['BIG_QUANTITY_C'] 		= $total_customers;
			$result['TOTAL_ARTICLES'] 		= $total_articles;
			$result['total_amount']      	= $total_amount; 
			//$result['delivered_amount']		= $delivered_amount;
			//$result['ndelivered_amount']	= $ndelivered_amount;
			$result['delivered_a']			= round($delivered_a);
			$result['ndelivered_a']			= round($ndelivered_a);
			$result['BIG_MARGIN']			= place_currency(display_number($big_margin));
			$result['BIG_MARGIN_PERCENT']	= display_number($big_margin_percent);
			$result['supplier_db']			= $supplier_db;
			$result['EXPORT_HREF']			= 'index.php?do=report-export_articles_invoices_detail_report&article_id='.$in['article_id'].'&article_category_id='.$in['article_category_id'].'&customer_id='.$in['customer_id'].'&time_start='.$time_start.'&time_end='.$time_end.'&m_id='.$in['manager_id'].'&supplier_id='.$in['supplier_id'];
			$result['EXPORT_HREF_RAW']			= 'index.php?do=report-export_articles_invoices_detail_report_raw&article_id='.$in['article_id'].'&article_category_id='.$in['article_category_id'].'&customer_id='.$in['customer_id'].'&time_start='.$time_start.'&time_end='.$time_end.'&m_id='.$in['manager_id'].'&supplier_id='.$in['supplier_id'];
			$result['time_start']=$time_start;
			$result['time_end']=$time_end;
			$result['customers'] = $this->get_cc($in);
			$result['families']  = $this->get_families($in);
			$result['articles']  = $this->get_articles_list($in);
			$result['article_tab']=$this->get_articlesTab($in);
			$result['managers']= $this->get_managers($in);
			$result['chart_amount']=array(				
					'label'			=> array(gm('Paid'),gm('Not paid')),
					'data' 			=> array(number_format($paid_amount,2,'.',''),number_format($npaid_amount,2,'.','')),
					'data_show' 	=> array(get_currency_sign(display_number($paid_amount)),get_currency_sign(display_number($npaid_amount))),
				);
			$result['chart_article']=$this->get_articleChart($in);
			$result['chart_family']=$this->get_familyChart($in);
			$result['suppliers'] = $this->get_suppliers_list($in);

			$this->out=$result;
		}

		public function get_cc(&$in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" customers.is_admin='0' AND customers.active=1 ";
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			}
			if($in['customer_id']){
				$filter .=" AND customers.customer_id='".$in['customer_id']."' ";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type,cat_id
				FROM customers
				
				WHERE $filter
				GROUP BY customers.customer_id			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array('list'=>array());
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);
				if($value['type']==0){
					$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
				}elseif($value['type']==1){
					$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
				}else{
					$symbol ='';
				}

				$address = $this->db->query("SELECT zip,city,address FROM customer_addresses
										WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");

				$result_line=array(
					"id"					=> $value['cust_id'],
					'symbol'				=> $symbol,
					"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					/*'zip'					=> $value['zip'] ? $value['zip'] : '',
					'city'					=> $value['city'] ? $value['city'] : '',
					"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],*/
					'zip'					=> $address->f('zip') ? $address->f('zip') : '',
					'city'					=> $address->f('city') ? $address->f('city') : '',
					"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
					"right"					=> $value['acc_manager_name'],
					"cat_id"				=> $value['cat_id']
				);
				array_push($result['list'],$result_line);
			}		
			return $result;
		}

		public function get_managers(&$in){
			$q = strtolower($in["term"]);

			$filter = '';

			if($q){
				$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
			}
			if($in['manager_id']){
				$filter .=" AND users.user_id='".$in['manager_id']."' ";
			}

			$data=$this->db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY last_name ");
			while($data->move_next()){
			  $users[$data->f('last_name').' '.$data->f('first_name')]=$data->f('user_id');
			}

			$result = array('list'=>array());
			if($users){
				foreach ($users as $key=>$value) {
					$result_line=array(
						'id'		=> $value,
						'value'		=> htmlspecialchars_decode(stripslashes($key))
					);
					array_push($result['list'], $result_line);
				}
			}
			return $result;
		}

		public function get_suppliers_list(&$in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" is_admin='0' AND customers.active=1 AND customers.is_supplier='1'";
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			}
			if($in['supplier_id']){
				$filter .=" AND customers.customer_id='".$in['supplier_id']."' ";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type,cat_id
				FROM customers
				LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
				WHERE $filter
				GROUP BY customers.customer_id			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array('list'=>array());
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);
				if($value['type']==0){
					$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
				}elseif($value['type']==1){
					$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
				}else{
					$symbol ='';
				}
				$result_line=array(
					"id"					=> $value['cust_id'],
					'symbol'				=> $symbol,
					"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					'zip'					=> $value['zip'] ? $value['zip'] : '',
					'city'					=> $value['city'] ? $value['city'] : '',
					"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
					"right"					=> $value['acc_manager_name'],
					"cat_id"				=> $value['cat_id']
				);
				array_push($result['list'],$result_line);
			}		
			return $result;
		}



		public function get_families(&$in){

			$q = strtolower($in["term"]);
			$filter= " 1=1 ";
			if($q){
				$filter .= " AND name LIKE '%".addslashes($q)."%' ";
			}
			if($in['article_category_id']){
				$filter .=" AND pim_article_categories.id='".$in['article_category_id']."' ";
			} 

			$categories=$this->db->query("SELECT * FROM pim_article_categories WHERE ".$filter." ORDER BY name ASC LIMIT 5");
			
			$result=array('list'=>array());
			while($categories->move_next()){
				$families = array(
					'name'  	=> $categories->f('name'),
					'id'		=> $categories->f('id'),
					'value'		=> $categories->f('name'),
				);			  
				array_push($result['list'], $families);
			}
			return $result;
		}

		public function get_articles_list(&$in)
		{
			$def_lang = DEFAULT_LANG_ID;
			if($in['lang_id']){
				$def_lang= $in['lang_id'];
			}
			//if custom language is selected we show default lang data
			if($def_lang>4){
				$def_lang = DEFAULT_LANG_ID;
			}
			
			switch ($def_lang) {
				case '1':
					$text = gm('Name');
					break;
				case '2':
					$text = gm('Name fr');
					break;
				case '3':
					$text = gm('Name du');
					break;
				default:
					$text = gm('Name');
					break;
			}

			$cat_id = $in['cat_id'];
			if(!$in['from_address_id']) {
				$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
									   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
									   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id 
									   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';
				$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
							pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
							pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
							pim_article_prices.price, pim_articles.internal_name,
							pim_article_brands.name AS article_brand,
							pim_articles_lang.description AS description,
							pim_articles_lang.name2 AS item_name2,
							pim_articles_lang.name AS item_name,
							pim_articles_lang.lang_id,
							pim_articles.vat_id,
							pim_article_categories.name AS family,
							pim_articles.supplier_reference,
							pim_articles.is_service,
							pim_articles.price AS unit_price,
							pim_articles.block_discount';

			}else{
				$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
				                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
								   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
								   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';

				$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
							pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
							pim_articles.sale_unit,pim_articles.packing,
							pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

							pim_articles_lang.description AS description,
							pim_articles_lang.name2 AS item_name2,
							pim_articles_lang.name AS item_name,
							pim_articles_lang.lang_id,
							pim_articles.vat_id,
							pim_article_categories.name AS family,
							pim_articles.supplier_reference,
							pim_articles.is_service,
							pim_articles.price AS unit_price,
							pim_articles.block_discount';
			}

			$filter.=" 1=1 ";

			//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

			if ($in['search'])
			{
				$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
				// $arguments.="&search=".$in['search'];
			}
			if ($in['hide_article_ids'])
			{
				$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
				// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
			}
			if ($in['show_stock'])
			{
				$filter.=" AND pim_articles.hide_stock=0";
				// $arguments.="&show_stock=".$in['show_stock'];
			}
			if ($in['from_customer_id'])
			{
				$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
				// $arguments.="&from_customer_id=".$in['from_customer_id'];
			}
			if ($in['from_address_id'])
			{
				$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
				// $arguments.="&from_address_id=".$in['from_address_id'];
			}
			if($in['article_id'] && !is_array($in['article_id'])){
				$filter.=" AND  pim_articles.article_id=".$in['article_id'];
			}
			if($in['artadjust']){
				$in['artadjust']=rtrim($in['artadjust'],",");
				$filter.=" AND pim_articles.article_id IN('".$in['artadjust']."') ";
			}

			$articles= array( 'lines' => array());
			// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

			$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

			/*$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");*/

			$time = time();

			$j=0;
			while($article->next()){
				/*$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
				
				if($in['customer_id']){
					$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['customer_id']."'");
					if($vat_regime==2){
						$vat=0;
					}
				}

				$values = $article->next_array();
				$tags = array_map(function($field){
					return '/\[\!'.strtoupper($field).'\!\]/';
				},array_keys($values));

				$label = preg_replace($tags, $values, $fieldFormat);

				if($article->f('price_type')==1){

				    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

			        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

			       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
			            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

			        }else{
			       	   	$price_value=$price_value_custom_fam;

			         	 //we have to apply to the base price the category spec
			    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
			    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
			    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

			    	    if($cat_price_type==2){
			                $article_base_price=get_article_calc_price($article->f('article_id'),3);
			            }else{
			                $article_base_price=get_article_calc_price($article->f('article_id'),1);
			            }

			       		switch ($cat_type) {
							case 1:                  //discount
								if($price_value_type==1){  // %
									$price = $article_base_price - $price_value * $article_base_price / 100;
								}else{ //fix
									$price = $article_base_price - $price_value;
								}
								break;
							case 2:                 //profit margin
								if($price_value_type==1){  // %
									$price = $article_base_price + $price_value * $article_base_price / 100;
								}else{ //fix
									$price =$article_base_price + $price_value;
								}
								break;
						}
			        }

				    if(!$price || $article->f('block_discount')==1 ){
			        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
			        }
			    }else{
			    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
			        if(!$price || $article->f('block_discount')==1 ){
			        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
			        }
			    }

			    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
			  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

			    $start= mktime(0, 0, 0);
			    $end= mktime(23, 59, 59);
			    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
			    if($promo_price->move_next()){
			    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

			        }else{
			            $price=$promo_price->f('price');
			            $base_price = $price;
			        }
			    }
			 	if($in['customer_id']){
			 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
			  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['customer_id']."'");
			    	if($customer_custom_article_price->move_next()){

			            $price = $customer_custom_article_price->f('price');

			            $base_price = $price;
			       	}
			       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
			   	}

				$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");*/

				$linie = array(
				  	'article_id'				=> $article->f('article_id'),
				  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
				  	'name'						=> htmlspecialchars_decode($article->f('internal_name')),
				  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
				    /*'stock'						=> $article->f('stock'),
				    'stock2'					=> remove_zero_decimals($article->f('stock')),
				    'supplier_reference'		=> $article->f('supplier_reference'),
				    'family'					=> $article->f('family') ? $article->f('family') : '',
				    'quantity'		    		=> 1,
				    'pending_articles'  		=> intval($pending_articles),
				    'threshold_value'   		=> $article->f('article_threshold_value'),
				  	'sale_unit'					=> $article->f('sale_unit'),
				  	'percent'           		=> $vat_percent,
					'percent_x'         		=> display_number($vat_percent),
				    'packing'					=> remove_zero_decimals($article->f('packing')),
				  	'code'		  	    		=> $article->f('item_code'),
					'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
					'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
					'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
					'purchase_price'			=> $purchase_price,
					'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
					'quoteformat'    			=> html_entity_decode(gfn($label)), 
					'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),
					'show_stock'				=> $article->f('hide_stock') ? false:true,
					'hide_stock'				=> $article->f('hide_stock'),
					'is_service'				=> $article->f('is_service'),*/
				);
				array_push($articles['lines'], $linie);
			  	
			}

			$articles['customer_id'] 		= $in['customer_id'];
			$articles['lang_id'] 			= $in['lang_id'];
			$articles['cat_id'] 			= $in['cat_id'];
			$articles['txt_name']			= $text;
			$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

			return $articles;
		}

		public function get_articlesTab(&$in){
			$result=array('article_row'=>array());
			$order_by_array = array('amount','quantity');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter.=" and tblinvoice.buyer_id = ".$in['customer_id'];
			}
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['article_category_id']){
				$filter.=" and pim_articles.article_category_id = ".$in['article_category_id'];
			}
			/*if($in['u_id']){
				$filter.=" AND pim_orders.author_id=".$in['u_id'] ;
			}*/
			if($in['article_id']){
				$filter.=" and pim_articles.article_id = ".$in['article_id'];
			}
			if($in['supplier_id']){
				$filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';					
			}

			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_article']) || (!is_numeric($in['offset_article'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_article']-1;
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}


			$this->db->query("SELECT tblinvoice_line.id
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
			
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' "
				.$filter."
				GROUP BY tblinvoice_line.article_id ");

			$result['max_rows']=$this->db->records_count();

			$i = 0;

			$articles_data = $this->db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.discount as global_discount, tblinvoice.paid, tblinvoice.discount_line_gen, tblinvoice_line.discount, 
				tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice.currency_rate,tblinvoice.currency_type,tblinvoice_line.quantity, tblinvoice_line.price,tblinvoice_line.purchase_price, pim_articles.article_id, 
				pim_articles.item_code AS invoice_item_code,pim_articles.internal_name,
				customers.name AS invoice_buyer_name,pim_article_categories.name AS cat_name,
				pim_articles.supplier_name, pim_articles.supplier_id
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
				LEFT JOIN pim_article_categories ON pim_articles.article_category_id=pim_article_categories.id
				LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
				ORDER BY invoice_item_code ASC, invoice_buyer_name ASC ");
			$total_contacts = array();
			$total_companies = array();
			$art = array();
			$total_amount = 0;
			$big_margin = 0;
			$big_margin_percent=0;

			while ($articles_data->next()) { 
				$del_a = 0;
				$ndel_a = 0;
				
				$currency_rate  = return_value($articles_data->f('currency_rate'));
				if(!$currency_rate){
					$currency_rate = 1;		
				}
				$global_disc = $articles_data->f('global_discount');
				
				$discount_line = $articles_data->f('discount');
				if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
					$discount_line = 0;
				}
				if($articles_data->f('apply_discount') < 2){
					$global_disc = 0;
				}
				$q = $articles_data->f('quantity') ;
				$price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

				$line_total=$price_line * $q;	

				if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
					$line_total = $line_total*return_value($articles_data->f('currency_rate'));
				}	

				
				if($articles_data->f('buyer_id')){
					if(!$total_companies[$articles_data->f('article_id')]){
						$total_companies[$articles_data->f('article_id')] = 0;
					}		
				}elseif($articles_data->f('contact_id')){
					if(!$total_contacts[$articles_data->f('article_id')]){
						$total_contacts[$articles_data->f('article_id')] = 0;
					}		
				}
				
				if($articles_data->f('buyer_id')){
					$art[$articles_data->f('article_id')]['customer'][$articles_data->f('buyer_id')] += $line_total - ($line_total*$global_disc/100);
					$art[$articles_data->f('article_id')]['customerq'][$articles_data->f('buyer_id')] += $articles_data->f('quantity');
				    $art[$articles_data->f('article_id')]['customer_margin'][$articles_data->f('buyer_id')]+= $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
				    $art[$articles_data->f('article_id')]['customer_purchase_amount'][$articles_data->f('buyer_id')] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
				    $art[$articles_data->f('article_id')]['customer_name'][$articles_data->f('buyer_id')] = $articles_data->f('invoice_buyer_name');
				}else{
					$art[$articles_data->f('article_id')]['contact'][$articles_data->f('contact_id')] += $line_total - ($line_total*$global_disc/100);
					$art[$articles_data->f('article_id')]['contactq'][$articles_data->f('contact_id')] += $articles_data->f('quantity');
				    $art[$articles_data->f('article_id')]['contact_margin'][$articles_data->f('contact_id')] += $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
				    $art[$articles_data->f('article_id')]['contact_purchase_amount'][$articles_data->f('contact_id')] += $articles_data->f('purchase_price') * $articles_data->f('quantity');
				}
				$art[$articles_data->f('article_id')]['article'] += $line_total - ($line_total*$global_disc/100);
				$art[$articles_data->f('article_id')]['q'] += $articles_data->f('quantity') ;
				$art[$articles_data->f('article_id')]['margin'] += $articles_data->f('quantity')*(($articles_data->f('price')-$articles_data->f('price')*$discount_line/100)-$articles_data->f('purchase_price'));
			    $art[$articles_data->f('article_id')]['currency_rate']= $currency_rate;
			    $art[$articles_data->f('article_id')]['purchase_amount'] += $articles_data->f('purchase_price') * $articles_data->f('quantity');

/*				if($articles_data->f('rdy_invoice')==0){
					$ndel_a = $articles_data->f('quantity');		
				}elseif($articles_data->f('rdy_invoice')==1){
					$del_a = $articles_data->f('quantity');		
				}else{//$articles_data->f('rdy_invoice')==2	
					$total_art_quantity = $articles_data->f('quantity');	
					$del_a = $this->db->field("SELECT SUM(pim_orders_delivery.quantity)  
					FROM pim_order_articles LEFT JOIN pim_orders_delivery 
					ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id 
					WHERE pim_order_articles.article_id = '".$articles_data->f('article_id')."' AND pim_orders_delivery.order_id = '".$articles_data->f('order_id')."' ");
					$ndel_a = $total_art_quantity-$del_a;		
				}*/

			/*	$art[$articles_data->f('article_id')]['delivered'] += $del_a;
				$art[$articles_data->f('article_id')]['ndelivered'] += $ndel_a;*/
				$art[$articles_data->f('article_id')]['supplier_name'] = $articles_data->f('supplier_name');
				$art[$articles_data->f('article_id')]['internal_name'] = $articles_data->f('internal_name');
				$art[$articles_data->f('article_id')]['item_code'] = $articles_data->f('invoice_item_code');
			  	$art[$articles_data->f('article_id')]['category_name'] = $articles_data->f('cat_name') ? $articles_data->f('cat_name') :'';
			}

			$big_quantity = 0;
			$big_customers = 0;
			$total_del = 0;
			$total_ndel = 0;
			$i = 0;
			$j=0;
			$total_margin=0;
			$total_margin_percent=0;
			
			foreach ($art as $art_id => $art_data ) {
				if($in['order_by']!='amount' && $in['order_by']!='quantity'){
						if($j>=$offset*$l_r && $j <($offset*$l_r+$l_r)){				 			  
							$total_contacts[$art_id] = count($art_data['customer']);
							$total_companies[$art_id] = count($art_data['contact']);
							$total_quantity = $art_data['q'];
						    
							$big_quantity+=$total_quantity;
							$customers = $total_companies[$art_id]+$total_contacts[$art_id];

							$big_customers+=$customers;
						    $total_amount+= $art_data['article'];
							$article_name = htmlspecialchars_decode($art_data['internal_name']);
							$article_code = $art_data['item_code'];
							$article_fam = $art_data['category_name'];
							$article_supplier = $art_data['supplier_name'];
							$delivered_a 	= $art_data['delivered'];
							$ndelivered_a 	= $art_data['ndelivered'];
							$total_del+=$delivered_a;
							$total_ndel+=$ndelivered_a;
						    $total_margin=$art_data['margin'];
						    //$total_margin_percent=($art_data['margin']/$art_data['article'])*100;
						    $total_margin_percent=0;
						    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
							    if($art_data['purchase_amount']){
							    	$total_margin_percent=($art_data['margin']/$art_data['purchase_amount'])*100;
							    }elseif($art_data['margin']){
							    	$total_margin_percent=100;
							    }
							}else{
								if($art_data['article']){
							    	$total_margin_percent=($art_data['margin']/$art_data['article'])*100;
							    }elseif($art_data['margin']){
							    	$total_margin_percent=100;
							    }
							}

							$article_row=array(			
									'TIME_START' 		=> $in['time_start'],
									'TIME_END'	 		=> $in['time_end'],
									'XPAG'				=> $in['pag'],
									'CUSTOM'			=> $in['custom'] ? '&custom=1' : '&custom=',
									'ARTICLE_N'			=> $article_code.' ('.$article_name.')',
									'TOTAL_QUANTITY'	=> $total_quantity,
									'FAMILY'			=> $article_fam,
									'SUPPLIER'			=> $article_supplier,
									'CUSTOMERS'			=> $customers,
									'TOTAL_CLIENT'		=> place_currency(display_number($art_data['article'])),
									'TOTAL_MARGIN'		=> place_currency(display_number($total_margin)),
									'TOTAL_MARGIN_PERCENT'=> display_number($total_margin_percent),
									'art_id'			=> $art_id,			
									'to_collaps'		=> $in['customer_id'] ? '' : 'to_collaps',
									'colspan_c_id'		=> $in['customer_id'] ? 'colspan="2"' : '',
									'not_c_id'			=> $in['customer_id'] ? false : true,
									'DELIVERED_A'		=> $delivered_a,
									'NDELIVERED_A'		=> $ndelivered_a,
									'article_collaps'	=> array(),
									'amount_ord'		=> $art_data['article'],
									'quantity_ord'		=> $art_data['q'],
							);		
										
							if($art_data['customer']){
								foreach ($art_data['customer'] as $key => $value) {
									//$client_name = $this->db->field("SELECT name FROM customers WHERE customer_id = '".$key."' ");
									$client_name = $art_data['customer_name'][$key];
									$customer_total_margin_percent =0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($art_data['customer_purchase_amount'][$key]){
											$customer_total_margin_percent = $art_data['customer_margin'][$key]/$art_data['customer_purchase_amount'][$key]*100;
										}elseif($art_data['customer_margin'][$key]){
											$customer_total_margin_percent =100;
										}
									}else{
										if($value){
											$customer_total_margin_percent = $art_data['customer_margin'][$key]/$value*100;
										}elseif($art_data['customer_margin'][$key]){
											$customer_total_margin_percent =100;
										}
									}	
									$temp_article_collaps=array(
										'CUSTOMER_N'				=> $client_name,
										'CUSTOMER_QUANTITY'			=> $art_data['customerq'][$key],
										'TOTAL_CUSTOMER'			=> place_currency(display_number($value)),
										'art_id'					=> $art_id,
										'NAV'						=> $time_nav,
										'CUSTOMER_ID'				=> $key,
										'CUSTOMER_TOTAL_MARGIN'		=> place_currency(display_number($art_data['customer_margin'][$key])),
									    //'CUSTOMER_TOTAL_MARGIN_PERCENT'=> display_number($art_data['customer_margin'][$key]/$value*100),
									    'CUSTOMER_TOTAL_MARGIN_PERCENT'=> display_number($customer_total_margin_percent),
										'XPAG'						=> $in['pag'],				
										'KEEP_PAGE'					=> $in['keep_page'],
									);
									array_push($article_row['article_collaps'], $temp_article_collaps);
								}
							}
							if($art_data['contact']){
								foreach ($art_data['contact'] as $key => $value) {
									$client_name = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id = '".$key."' ");
									$contact_total_margin_percent =0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($art_data['contact_purchase_amount'][$key]){
											$contact_total_margin_percent = $art_data['contact_margin'][$key]/$art_data['contact_purchase_amount'][$key]*100;
										}elseif($art_data['contact_margin'][$key]){
											$contact_total_margin_percent =100;
										}
									}else{
										if($value){
											$contact_total_margin_percent = $art_data['contact_margin'][$key]/$value*100;
										}elseif($art_data['contact_margin'][$key]){
											$contact_total_margin_percent =100;
										}
									}
									$temp_article_contact=array(
										'CUSTOMER_N'		=> $client_name,
										'CUSTOMER_QUANTITY'	=> $art_data['contactq'][$key],
										'TOTAL_CUSTOMER'		=> place_currency(display_number($value)),
										'art_id'			=> $art_id,
										'NAV'				=> $time_nav,
										'CONTACT_ID'		=> $key,
										'XPAG'			=> $in['pag'],
										'KEEP_PAGE'			=> $in['keep_page'],
										'CUSTOMER_TOTAL_MARGIN'		=>place_currency(display_number($art_data['contact_margin'][$key])),
									    'CUSTOMER_TOTAL_MARGIN_PERCENT'=> display_number($contact_total_margin_percent),

									);
									array_push($article_row['article_collaps'], $temp_article_contact);	
								}
							}
							array_push($result['article_row'], $article_row);
						}//end if $j
					}else{
						$total_contacts[$art_id] = count($art_data['customer']);
							$total_companies[$art_id] = count($art_data['contact']);
							$total_quantity = $art_data['q'];
						    
							$big_quantity+=$total_quantity;
							$customers = $total_companies[$art_id]+$total_contacts[$art_id];

							$big_customers+=$customers;
						    $total_amount+= $art_data['article'];
							$article_name = htmlspecialchars_decode($art_data['internal_name']);
							$article_code = $art_data['item_code'];
							$article_fam = $art_data['category_name'];
							$article_supplier = $art_data['supplier_name'];
							$delivered_a 	= $art_data['delivered'];
							$ndelivered_a 	= $art_data['ndelivered'];
							$total_del+=$delivered_a;
							$total_ndel+=$ndelivered_a;
						    $total_margin=$art_data['margin'];
						    //$total_margin_percent=($art_data['margin']/$art_data['article'])*100;
						    $total_margin_percent=0;
						    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
							    if($art_data['purchase_amount']){
							    	$total_margin_percent=($art_data['margin']/$art_data['purchase_amount'])*100;
							    }elseif($art_data['margin']){
							    	$total_margin_percent=100;
							    }
							}else{
								if($art_data['article']){
							    	$total_margin_percent=($art_data['margin']/$art_data['article'])*100;
							    }elseif($art_data['margin']){
							    	$total_margin_percent=100;
							    }
							}

							$article_row=array(			
									'TIME_START' 		=> $in['time_start'],
									'TIME_END'	 		=> $in['time_end'],
									'XPAG'				=> $in['pag'],
									'CUSTOM'			=> $in['custom'] ? '&custom=1' : '&custom=',
									'ARTICLE_N'			=> $article_code.' ('.$article_name.')',
									'TOTAL_QUANTITY'	=> $total_quantity,
									'FAMILY'			=> $article_fam,
									'SUPPLIER'			=> $article_supplier,
									'CUSTOMERS'			=> $customers,
									'TOTAL_CLIENT'		=> place_currency(display_number($art_data['article'])),
									'TOTAL_MARGIN'		=> place_currency(display_number($total_margin)),
									'TOTAL_MARGIN_PERCENT'=> display_number($total_margin_percent),
									'art_id'			=> $art_id,			
									'to_collaps'		=> $in['customer_id'] ? '' : 'to_collaps',
									'colspan_c_id'		=> $in['customer_id'] ? 'colspan="2"' : '',
									'not_c_id'			=> $in['customer_id'] ? false : true,
									'DELIVERED_A'		=> $delivered_a,
									'NDELIVERED_A'		=> $ndelivered_a,
									'article_collaps'	=> array(),
									'amount_ord'		=> $art_data['article'],
									'quantity_ord'		=> $art_data['q'],
							);		
										
							if($art_data['customer']){
								foreach ($art_data['customer'] as $key => $value) {
									//$client_name = $this->db->field("SELECT name FROM customers WHERE customer_id = '".$key."' ");
									$client_name = $art_data['customer_name'][$key];
									$customer_total_margin_percent =0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($art_data['customer_purchase_amount'][$key]){
											$customer_total_margin_percent = $art_data['customer_margin'][$key]/$art_data['customer_purchase_amount'][$key]*100;
										}elseif($art_data['customer_margin'][$key]){
											$customer_total_margin_percent =100;
										}
									}else{
										if($value){
											$customer_total_margin_percent = $art_data['customer_margin'][$key]/$value*100;
										}elseif($art_data['customer_margin'][$key]){
											$customer_total_margin_percent =100;
										}
									}	
									$temp_article_collaps=array(
										'CUSTOMER_N'				=> $client_name,
										'CUSTOMER_QUANTITY'			=> $art_data['customerq'][$key],
										'TOTAL_CUSTOMER'			=> place_currency(display_number($value)),
										'art_id'					=> $art_id,
										'NAV'						=> $time_nav,
										'CUSTOMER_ID'				=> $key,
										'CUSTOMER_TOTAL_MARGIN'		=> place_currency(display_number($art_data['customer_margin'][$key])),
									    //'CUSTOMER_TOTAL_MARGIN_PERCENT'=> display_number($art_data['customer_margin'][$key]/$value*100),
									    'CUSTOMER_TOTAL_MARGIN_PERCENT'=> display_number($customer_total_margin_percent),
										'XPAG'						=> $in['pag'],				
										'KEEP_PAGE'					=> $in['keep_page'],
									);
									array_push($article_row['article_collaps'], $temp_article_collaps);
								}
							}
							if($art_data['contact']){
								foreach ($art_data['contact'] as $key => $value) {
									$client_name = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id = '".$key."' ");
									$contact_total_margin_percent =0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($art_data['contact_purchase_amount'][$key]){
											$contact_total_margin_percent = $art_data['contact_margin'][$key]/$art_data['contact_purchase_amount'][$key]*100;
										}elseif($art_data['contact_margin'][$key]){
											$contact_total_margin_percent =100;
										}
									}else{
										if($value){
											$contact_total_margin_percent = $art_data['contact_margin'][$key]/$value*100;
										}elseif($art_data['contact_margin'][$key]){
											$contact_total_margin_percent =100;
										}
									}
									$temp_article_contact=array(
										'CUSTOMER_N'		=> $client_name,
										'CUSTOMER_QUANTITY'	=> $art_data['contactq'][$key],
										'TOTAL_CUSTOMER'		=> place_currency(display_number($value)),
										'art_id'			=> $art_id,
										'NAV'				=> $time_nav,
										'CONTACT_ID'		=> $key,
										'XPAG'			=> $in['pag'],
										'KEEP_PAGE'			=> $in['keep_page'],
										'CUSTOMER_TOTAL_MARGIN'		=>place_currency(display_number($art_data['contact_margin'][$key])),
									    'CUSTOMER_TOTAL_MARGIN_PERCENT'=> display_number($contact_total_margin_percent),

									);
									array_push($article_row['article_collaps'], $temp_article_contact);	
								}
							}
							array_push($result['article_row'], $article_row);
					}//end else
				$j++;
			}//end foreach

			$result['BIG_TOTAL']			= place_currency(display_number($total_amount));
			$result['BIG_QUANTITY']			= $big_quantity;
			$result['BIG_CUSTOMERS']		= $big_customers;
			/*$result['colspan_c_id']			= $in['customer_id'] ? 'colspan="2"' : '';
			$result['not_c_id']				= $in['customer_id'] ? false : true;
			$result['is_c_id']				= $in['customer_id'] ? true : false;
			$result['TOTAL_DELIVERED']		= $total_del;
			$result['TOTAL_NDELIVERED']		= $total_ndel;*/

			if($in['order_by']=='amount' || $in['order_by']=='quantity'){
				$result['article_row']=order_array_by_column($result['article_row'],$in['order_by'],$order, $offset,$l_r);				    
			}

			return $result;
		}

		public function get_customersTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];
			$filter = ' AND 1=1';

			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}	
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['article_category_id']){
				$filter .= ' AND pim_articles.article_category_id ='.$in['article_category_id'].' ';
			}
			if($in['article_id']){
				$filter .= ' AND pim_articles.article_id ='.$in['article_id'].' ';					
			}
			if($in['supplier_id']){
				$filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';					
			}

			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_customer']) || (!is_numeric($in['offset_customer'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_customer']-1;
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$this->db->query("SELECT tblinvoice_line.id
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
				LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
				GROUP BY tblinvoice.buyer_id ");

			$result['max_rows']=$this->db->records_count();

			$articles_data = $this->db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.paid, tblinvoice.discount as global_discount, tblinvoice.discount_line_gen, tblinvoice_line.discount,
			 tblinvoice.buyer_id,customers.name AS invoice_buyer_name, tblinvoice.contact_id, tblinvoice.currency_rate,tblinvoice.currency_type, tblinvoice_line.quantity, tblinvoice_line.price,tblinvoice_line.purchase_price,  pim_articles.article_id
			 	FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
				LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
				ORDER BY invoice_buyer_name ASC ");

			$total_contacts = array();
			$total_companies = array();
			$customers = array();
			$total_amount = 0;

			while ($articles_data->next()) {

				$global_disc = $articles_data->f('global_discount');
				
				$discount_line = $articles_data->f('discount');
				if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
					$discount_line = 0;
				}
				if($articles_data->f('apply_discount') < 2){
					$global_disc = 0;
				}
				$q = $articles_data->f('quantity');
				$price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

				$line_total=$price_line * $q;	

				if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
					$line_total = $line_total*return_value($articles_data->f('currency_rate'));
				}	
			
				if($articles_data->f('buyer_id')){
					$customers['customer'][$articles_data->f('buyer_id')] += $line_total - ($line_total*$global_disc/100);	
					$customers['customer_art'][$articles_data->f('buyer_id')][$articles_data->f('article_id')] += $line_total - ($line_total*$global_disc/100);
					$customers['customer_s_art_q'][$articles_data->f('buyer_id')][$articles_data->f('article_id')] += $q;
					$customers['customer_art_q'][$articles_data->f('buyer_id')] += $articles_data->f('quantity');
					$customers['customer_margin'][$articles_data->f('buyer_id')] += $articles_data->f('quantity')*($articles_data->f('price')-$articles_data->f('purchase_price'));
					$customers['customer_purchase_amount'][$articles_data->f('buyer_id')] += $articles_data->f('quantity')*$articles_data->f('purchase_price');
				}elseif($articles_data->f('contact_id')){
					$customers['contact'][$articles_data->f('contact_id')] += $line_total - ($line_total*$global_disc/100);	
					$customers['contact_art'][$articles_data->f('contact_id')][$articles_data->f('article_id')] += $line_total - ($line_total*$global_disc/100);	
					$customers['contact_s_art_q'][$articles_data->f('contact_id')][$articles_data->f('article_id')] += $q;
					$customers['contact_art_q'][$articles_data->f('contact_id')] += $articles_data->f('quantity');
					$customers['contact_margin'][$articles_data->f('contact_id')] += $articles_data->f('quantity')*($articles_data->f('price')-$articles_data->f('purchase_price'));
					$customers['contact_purchase_amount'][$articles_data->f('contact_id')] += $articles_data->f('quantity')*$articles_data->f('purchase_price');

				}		
			}
			$i = 0;
			$total_articles = 0;
			foreach ($customers as $key => $value) {
				if($key=='customer' || $key=='contact'){
					foreach ($value as $cust_id => $amount) {
						if($in['order_by']!='amount' ){
							if($j>=$offset*$l_r && $j <($offset*$l_r+$l_r)){				
								if($key=='customer'){
									$client_name = $this->db->field("SELECT name FROM customers WHERE customer_id = '".$cust_id."' ");
									$articles_quantity = $customers['customer_art_q'][$cust_id];
									$total_margin = $customers['customer_margin'][$cust_id];
									//$total_margin_percent=($customers['customer_margin'][$cust_id]/$amount)*100;
									$total_margin_percent=0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($customers['customer_purchase_amount'][$cust_id]){
											$total_margin_percent=($customers['customer_margin'][$cust_id]/$customers['customer_purchase_amount'][$cust_id])*100;
										}elseif($customers['customer_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}else{
										if($amount){
											$total_margin_percent=($customers['customer_margin'][$cust_id]/$amount)*100;
										}elseif($customers['customer_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}

								}else{
									$client_name = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id = '".$cust_id."' ");
									$articles_quantity = $customers['contact_art_q'][$cust_id];
									$total_margin = $customers['contact_margin'][$cust_id];
									//$total_margin_percent=($customers['contact_margin'][$cust_id]/$amount)*100;
									$total_margin_percent=0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($customers['contact_purchase_amount'][$cust_id]){
											$total_margin_percent=($customers['contact_margin'][$cust_id]/$customers['contact_purchase_amount'][$cust_id])*100;
										}elseif($customers['contact_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}else{
										if($amount){
											$total_margin_percent=($customers['contact_margin'][$cust_id]/$amount)*100;
										}elseif($customers['contact_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}
								}	
								$total_articles+=$articles_quantity;
								$total_amount+= $amount;

								$other_row=array(			
									'TIME_START' 		=> $in['time_start'],
									'TIME_END'	 		=> $in['time_end'],
									'XPAG'				=> $in['pag'],
									'CUSTOM'			=> $in['custom'] ? '&custom=1' : '&custom=',
									'CUSTOMER_N'		=> $client_name,				
									'ARTICLES'			=> $articles_quantity,
									'TOTAL_CLIENT'		=> place_currency(display_number($amount)),
									'cust_id'			=> $cust_id,
									'NAV'				=> $time_nav,
									'CUSTOMER_ID'		=> $cust_id,
									'TOTAL_MARGIN'		=> place_currency(display_number($total_margin)),
								    'TOTAL_MARGIN_PERCENT'=> display_number($total_margin_percent),
								    'other_collaps'		=> array(),
								    'amount_ord'		=> $amount,
								);	

								if($key=='customer'){
									foreach ($customers['customer_art'][$cust_id] as $article_id => $art_val) {
										$article_name = $this->db->field("SELECT internal_name FROM pim_articles WHERE article_id = '".$article_id."' ");
										$total_art_q = $customers['customer_s_art_q'][$cust_id][$article_id];
										$total_margin = $customers['customer_margin'][$cust_id][$article_id];
										$other_collaps_temp=array(
											'ARTICLE_N'			=> $article_name,
											'TOTAL_ART_Q'		=> $total_art_q,
											'TOTAL_ART_AMOUNT'	=> place_currency(display_number($art_val)),
											'cust_id'			=> $cust_id,
										);
										array_push($other_row['other_collaps'], $other_collaps_temp);	
									}
								}else{
									foreach ($customers['contact_art'][$cust_id] as $article_id => $art_val) {
										$article_name = $this->db->field("SELECT internal_name FROM pim_articles WHERE article_id = '".$article_id."' ");
										$total_art_q = $customers['contact_s_art_q'][$cust_id][$article_id];
											$total_margin = $customers['contact_margin'][$cust_id][$article_id];
										$other_collaps_temp=array(
											'ARTICLE_N'			=> $article_name,
											'TOTAL_ART_Q'		=> $total_art_q,
											'TOTAL_ART_AMOUNT'	=> place_currency(display_number($art_val)),
											'cust_id'			=> $cust_id,
										);
										array_push($other_row['other_collaps'], $other_collaps_temp);	
									}
								}
								array_push($result['other_row'], $other_row);
							}
						}else{
							if($key=='customer'){
									$client_name = $this->db->field("SELECT name FROM customers WHERE customer_id = '".$cust_id."' ");
									$articles_quantity = $customers['customer_art_q'][$cust_id];
									$total_margin = $customers['customer_margin'][$cust_id];
									//$total_margin_percent=($customers['customer_margin'][$cust_id]/$amount)*100;
									$total_margin_percent=0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($customers['customer_purchase_amount'][$cust_id]){
											$total_margin_percent=($customers['customer_margin'][$cust_id]/$customers['customer_purchase_amount'][$cust_id])*100;
										}elseif($customers['customer_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}else{
										if($amount){
											$total_margin_percent=($customers['customer_margin'][$cust_id]/$amount)*100;
										}elseif($customers['customer_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}

								}else{
									$client_name = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id = '".$cust_id."' ");
									$articles_quantity = $customers['contact_art_q'][$cust_id];
									$total_margin = $customers['contact_margin'][$cust_id];
									//$total_margin_percent=($customers['contact_margin'][$cust_id]/$amount)*100;
									$total_margin_percent=0;
									if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
										if($customers['contact_purchase_amount'][$cust_id]){
											$total_margin_percent=($customers['contact_margin'][$cust_id]/$customers['contact_purchase_amount'][$cust_id])*100;
										}elseif($customers['contact_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}else{
										if($amount){
											$total_margin_percent=($customers['contact_margin'][$cust_id]/$amount)*100;
										}elseif($customers['contact_margin'][$cust_id]){
											$total_margin_percent=100;
										}
									}
								}	
								$total_articles+=$articles_quantity;
								$total_amount+= $amount;

								$other_row=array(			
									'TIME_START' 		=> $in['time_start'],
									'TIME_END'	 		=> $in['time_end'],
									'XPAG'				=> $in['pag'],
									'CUSTOM'			=> $in['custom'] ? '&custom=1' : '&custom=',
									'CUSTOMER_N'		=> $client_name,				
									'ARTICLES'			=> $articles_quantity,
									'TOTAL_CLIENT'		=> place_currency(display_number($amount)),
									'cust_id'			=> $cust_id,
									'NAV'				=> $time_nav,
									'CUSTOMER_ID'		=> $cust_id,
									'TOTAL_MARGIN'		=> place_currency(display_number($total_margin)),
								    'TOTAL_MARGIN_PERCENT'=> display_number($total_margin_percent),
								    'other_collaps'		=> array(),
								    'amount_ord'		=> $amount,
								);	

								if($key=='customer'){
									foreach ($customers['customer_art'][$cust_id] as $article_id => $art_val) {
										$article_name = $this->db->field("SELECT internal_name FROM pim_articles WHERE article_id = '".$article_id."' ");
										$total_art_q = $customers['customer_s_art_q'][$cust_id][$article_id];
										$total_margin = $customers['customer_margin'][$cust_id][$article_id];
										$other_collaps_temp=array(
											'ARTICLE_N'			=> $article_name,
											'TOTAL_ART_Q'		=> $total_art_q,
											'TOTAL_ART_AMOUNT'	=> place_currency(display_number($art_val)),
											'cust_id'			=> $cust_id,
										);
										array_push($other_row['other_collaps'], $other_collaps_temp);	
									}
								}else{
									foreach ($customers['contact_art'][$cust_id] as $article_id => $art_val) {
										$article_name = $this->db->field("SELECT internal_name FROM pim_articles WHERE article_id = '".$article_id."' ");
										$total_art_q = $customers['contact_s_art_q'][$cust_id][$article_id];
											$total_margin = $customers['contact_margin'][$cust_id][$article_id];
										$other_collaps_temp=array(
											'ARTICLE_N'			=> $article_name,
											'TOTAL_ART_Q'		=> $total_art_q,
											'TOTAL_ART_AMOUNT'	=> place_currency(display_number($art_val)),
											'cust_id'			=> $cust_id,
										);
										array_push($other_row['other_collaps'], $other_collaps_temp);	
									}
								}
								array_push($result['other_row'], $other_row);
						}
						$j++;
					}					
				}	
			}

			if($in['order_by']=='amount'){
				$result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order, $offset,$l_r);    
			}
			return $result;
		}

		public function get_familiesTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];
			$filter = ' AND 1=1';

			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}	
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['article_category_id']){
				$filter .= ' AND pim_articles.article_category_id ='.$in['article_category_id'].' ';
			}
			if($in['article_id']){
				$filter .= ' AND pim_articles.article_id ='.$in['article_id'].' ';					
			}
			if($in['supplier_id']){
				$filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';					
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$articles_data = $this->db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.paid, tblinvoice.discount as global_discount, tblinvoice.discount_line_gen, tblinvoice_line.discount,
			 tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice.currency_rate,tblinvoice.currency_type, tblinvoice_line.quantity, tblinvoice_line.price,tblinvoice_line.purchase_price,  pim_articles.article_id,
				pim_articles.item_code AS invoice_item_code,pim_articles.article_category_id,
				customers.name AS invoice_buyer_name
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
				LEFT JOIN pim_article_categories ON pim_articles.article_category_id=pim_article_categories.id
				LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
				ORDER BY invoice_item_code ASC, invoice_buyer_name ASC ");

			$total_contacts = array();
			$total_companies = array();
			$art = array();
			$art2 = array();
			$art3 = array();
			$art4=array();
			$total_amount = 0;

			while ($articles_data->next()){
				$del_a = 0;
				$ndel_a = 0;

				$global_disc = $articles_data->f('global_discount');
				
				$discount_line = $articles_data->f('discount');
				if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
					$discount_line = 0;
				}
				if($articles_data->f('apply_discount') < 2){
					$global_disc = 0;
				}
				$q = $articles_data->f('quantity') ;
				$price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

				$line_total=$price_line * $q;	

				if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
					$line_total = $line_total*return_value($articles_data->f('currency_rate'));
				}	

				$total_amount+= $line_total - ($line_total*$global_disc/100);
		
				$art[$articles_data->f('article_id')] += $line_total - ($line_total*$global_disc/100);
				
				$art2[$articles_data->f('article_id')] += $articles_data->f('quantity')*($articles_data->f('price')-$articles_data->f('purchase_price'));	
				$art3[$articles_data->f('article_id')] += $articles_data->f('quantity')*$articles_data->f('purchase_price');
				$art4[$articles_data->f('article_id')]=$articles_data->f('article_category_id');
			}
			$fam=array();
			$fam2=array();
			$fam3=array();
			foreach ($art as $art_id => $val) {
			    /*$family_id=$this->db->field("SELECT pim_articles.article_category_id 
			    	                  FROM pim_articles 
			    	                   INNER JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			    	                   WHERE pim_articles.article_id = '".$art_id."' and pim_articles.article_category_id!=0");*/
			    if($art4[$art_id]){
			       $fam[$art4[$art_id]]+=$val;
			    }
			}
			foreach ($art2 as $art_id => $margin) {
			    /*$family_id=$this->db->field("SELECT pim_articles.article_category_id 
			    	                  FROM pim_articles 
			    	                   INNER JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			    	                   WHERE pim_articles.article_id = '".$art_id."' and pim_articles.article_category_id!=0");*/
			    if($art4[$art_id]){
			       $fam2[$art4[$art_id]]+=$margin;
			    }
			}
			foreach ($art3 as $art_id => $purchase_amount) {
			    /*$family_id=$this->db->field("SELECT pim_articles.article_category_id 
			    	                  FROM pim_articles 
			    	                   INNER JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			    	                   WHERE pim_articles.article_id = '".$art_id."' and pim_articles.article_category_id!=0");*/
			    if($art4[$art_id]){
			       $fam3[$art4[$art_id]]+=$purchase_amount;
			    }
			}
			$j=0;
			$total_amount=0;
			$total_margin=0;
			foreach ($fam as $fam_id => $val) {
			    $total_margin=$fam2[$fam_id];
			    $total_purchase_amount=$fam3[$fam_id];
			    $total_margin_percent=0;
			    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				    if($total_purchase_amount){
				    	$total_margin_percent=($total_margin/$total_purchase_amount)*100;
				    }elseif($total_margin){
				    	$total_margin_percent=100;
				    }
				}else{
					if($val){
				    	$total_margin_percent=($total_margin/$val)*100;
				    }elseif($total_margin){
				    	$total_margin_percent=100;
				    }
				}
				$fam_name = $this->db->field("SELECT name FROM pim_article_categories WHERE id = '".$fam_id."' ");
				$other_row=array(	'FAM_NAME'=>$fam_name,
				                        'FAM_VAL'	=> place_currency(display_number($val)),
				                        'TOTAL_MARGIN'     => place_currency(display_number($total_margin)),
						              //'TOTAL_MARGIN_PERCENT'     => display_number(($total_margin/$val)*100),		
										'TOTAL_MARGIN_PERCENT'     => display_number($total_margin_percent),
										'amount_ord'				=>$val,	
						);	
			    $total_amount+=$val;
				array_push($result['other_row'], $other_row);
			    $j++;
			}
			$result['BIG_TOTAL']= place_currency(display_number($total_amount));

			if($in['order_by']=='amount'){
				$result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order);			    
			}
			return $result;
		}

		public function get_suppliersTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];
			$filter = ' AND 1=1';

			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}	
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['article_category_id']){
				$filter .= ' AND pim_articles.article_category_id ='.$in['article_category_id'].' ';
			}
			if($in['article_id']){
				$filter .= ' AND pim_articles.article_id ='.$in['article_id'].' ';					
			}
			if($in['supplier_id']){
				$filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';					
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$articles_data = $this->db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.paid, tblinvoice.discount as global_discount, tblinvoice.discount_line_gen, tblinvoice_line.discount,
			 tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice.currency_rate,tblinvoice.currency_type, tblinvoice_line.quantity, tblinvoice_line.price,tblinvoice_line.purchase_price,  pim_articles.article_id,
				pim_articles.item_code AS invoice_item_code,
				customers.name AS invoice_buyer_name,
				pim_articles.supplier_name, pim_articles.supplier_id
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
				LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
				ORDER BY pim_articles.supplier_id DESC ");

			$art = array();
			$art2 = array(); 
			$art3 = array(); //number of articles
			$art4 = array();
			$art5=array();
			$total_amount = 0;

			while ($articles_data->next()){
				$del_a = 0;
				$ndel_a = 0;

				$global_disc = $articles_data->f('global_discount');
				
				$discount_line = $articles_data->f('discount');
				if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
					$discount_line = 0;
				}
				if($articles_data->f('apply_discount') < 2){
					$global_disc = 0;
				}
				$q = $articles_data->f('quantity') ;
				$price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

				$line_total=$price_line * $q;	

				if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
					$line_total = $line_total*return_value($articles_data->f('currency_rate'));
				}	

				$total_amount+= $line_total - ($line_total*$global_disc/100);
				
				$art[$articles_data->f('article_id')] += $line_total - ($line_total*$global_disc/100);
				
				$art2[$articles_data->f('article_id')] += $articles_data->f('quantity')*($articles_data->f('price')-$articles_data->f('purchase_price'));
				
				$art3[$articles_data->f('article_id')] += $q;

				$art4[$articles_data->f('article_id')] += $articles_data->f('quantity')*$articles_data->f('purchase_price');
				$art5[$articles_data->f('article_id')] = $articles_data->f('supplier_id');
			 	
			}
			$suppliers=array();	 //total values	
			$suppliers2=array(); //margin
			$suppliers3=array(); //number of articles
			$suppliers4=array();

			foreach ($art as $art_id => $val) {
			    /*$supplier_id=$this->db->field("SELECT pim_articles.supplier_id   	                  
			    	                   FROM pim_articles     	                   
			    	                   WHERE pim_articles.article_id = '".$art_id."'"); */
			    if($art5[$art_id]){
			    	$suppliers[$art5[$art_id]]+=$val;
			    }  
			       
			}

			foreach ($art2 as $art_id => $margin) {
			    /*$supplier_id=$this->db->field("SELECT pim_articles.supplier_id     	                  
			    	                   FROM pim_articles 
			    	                   WHERE pim_articles.article_id = '".$art_id."'");*/
			   	if($art5[$art_id]){
			       $suppliers2[$art5[$art_id]]+=$margin;
			   	}
			 }

			foreach ($art3 as $art_id => $quantity) {
			    /*$supplier_id=$this->db->field("SELECT pim_articles.supplier_id     	                  
			    	                   FROM pim_articles 
			    	                   WHERE pim_articles.article_id = '".$art_id."'");*/
			    if($art5[$art_id]){
			       $suppliers3[$art5[$art_id]]+=$quantity;
			    }
			}

			foreach ($art4 as $art_id => $purchase_amount) {
			    /*$supplier_id=$this->db->field("SELECT pim_articles.supplier_id     	                  
			    	                   FROM pim_articles 
			    	                   WHERE pim_articles.article_id = '".$art_id."'");*/
			    	if($art5[$art_id]){
			    		$suppliers4[$art5[$art_id]]+= $purchase_amount;
			    	}		       
			}

			$j=0;
			$total_amount=0;
			$total_margin=0;
			$total_purchase_amount =0;
			foreach ($suppliers as $supplier_id => $val) {			
			    $total_margin = $suppliers2[$supplier_id];
			    $quantity = $suppliers3[$supplier_id];
			    $total_purchase_amount = $suppliers4[$supplier_id];

			    $total_margin_percent=0;
			    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				    if($total_purchase_amount){
				    	$total_margin_percent=($total_margin/$total_purchase_amount)*100;
				    }elseif($total_margin){
				    	$total_margin_percent=100;
				    }
				}else{
					if($val){
				    	$total_margin_percent=($total_margin/$val)*100;
				    }elseif($total_margin){
				    	$total_margin_percent=100;
				    }
				}
				$supplier_name = $this->db->field("SELECT supplier_name FROM pim_articles WHERE supplier_id = '".$supplier_id."'");
				$other_row=array(	'SUPPLIER_NAME'				=>$supplier_name? $supplier_name : gm('NO SUPPLIER'),
				                        'SUPPLIER_VAL'				=> place_currency(display_number($val)),
				                        'NUMBER_ARTICLES'			=> $quantity,
				                        'TOTAL_MARGIN'     			=> place_currency(display_number($total_margin)),
						              	//'TOTAL_MARGIN_PERCENT'      => display_number(($total_margin/$val)*100),	
						              	'TOTAL_MARGIN_PERCENT'      => display_number($total_margin_percent),	
						              	'amount_ord'				=> $val,
							
						);	
			    $total_amount+=$val;
				array_push($result['other_row'], $other_row);
			    $j++;
			}

			$result['BIG_TOTAL']= place_currency(display_number($total_amount));	
			if($in['order_by']=='amount'){
				$result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order);    
			}				
			return $result;
		}

		public function get_articleChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter='';
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}	
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['article_category_id']){
				$filter .= ' AND pim_articles.article_category_id ='.$in['article_category_id'].' ';
			}
			if($in['article_id']){
				$filter .= ' AND pim_articles.article_id ='.$in['article_id'].' ';					
			}
			if($in['supplier_id']){
				$filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';					
			}

			$i = 0;

			$articles_data = $this->db->query("SELECT tblinvoice.id, tblinvoice.apply_discount, tblinvoice.paid, tblinvoice.discount as global_discount, tblinvoice.discount_line_gen, tblinvoice_line.discount,
			 tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice.currency_rate,tblinvoice.currency_type, tblinvoice_line.quantity, tblinvoice_line.price,tblinvoice_line.purchase_price,  pim_articles.article_id,pim_articles.internal_name,
				pim_articles.item_code AS invoice_item_code,
				customers.name AS invoice_buyer_name,
				pim_articles.supplier_name, pim_articles.supplier_id
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id 
				LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
				ORDER BY invoice_item_code ASC, invoice_buyer_name ASC "
				);
			$total_contacts = array();
			$total_companies = array();
			$art = array();
			$art1=array();
			$art2=array();
			$total_amount = 0;
			while ($articles_data->next()){
				$del_a = 0;
				$ndel_a = 0;

				$global_disc = $articles_data->f('global_discount');
	
				$discount_line = $articles_data->f('discount');

				if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
					$discount_line = 0;
				}
				if($articles_data->f('apply_discount') < 2){
					$global_disc = 0;
				}
				$q = $articles_data->f('quantity') ;
				$price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);
				$line_total=$price_line * $q;	

				if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
					$line_total = $line_total*return_value($articles_data->f('currency_rate'));
				}	
				
				$total_amount+= $line_total - ($line_total*$global_disc/100);

				if($articles_data->f('customer_id')){
					if(!$total_companies[$articles_data->f('article_id')]){
						$total_companies[$articles_data->f('article_id')] = 0;
					}		
				}elseif($articles_data->f('contact_id')){
					if(!$total_contacts[$articles_data->f('article_id')]){
						$total_contacts[$articles_data->f('article_id')] = 0;
					}		
				}
					
				$art[$articles_data->f('article_id')] += $line_total - ($line_total*$global_disc/100);
				$art1[$articles_data->f('article_id')] =$articles_data->f('internal_name');
				$art2[$articles_data->f('article_id')] =$articles_data->f('invoice_item_code');
			}
			asort($art);
			$art= array_reverse($art,true);
			$art=array_slice($art,0,5, true);	

			foreach ($art as $art_id => $val) {			
				$article_name = $art1[$art_id];
				$article_code = $art2[$art_id];
			   	array_push($result['label'], $article_code.' ('.$article_name.')');
				array_push($result['data'], number_format($val,2,'.',''));
				array_push($result['data_show'], get_currency_sign(display_number($val)));
			}

			return $result;
		}

		public function get_familyChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter='';
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}	
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['article_category_id']){
				$filter .= ' AND pim_articles.article_category_id ='.$in['article_category_id'].' ';
			}
			if($in['article_id']){
				$filter .= ' AND pim_articles.article_id ='.$in['article_id'].' ';					
			}
			if($in['supplier_id']){
				$filter .= ' AND pim_articles.supplier_id ='.$in['supplier_id'].' ';					
			}

			$i = 0;

			$articles_data = $this->db->query("SELECT tblinvoice.id,tblinvoice.apply_discount, tblinvoice.paid,tblinvoice.discount as global_discount, tblinvoice.discount_line_gen, tblinvoice.currency_type,
			 tblinvoice_line.discount, tblinvoice.buyer_id, tblinvoice.contact_id, tblinvoice_line.quantity, tblinvoice_line.price, pim_articles.article_id,
				pim_articles.item_code AS invoice_item_code,pim_articles.article_category_id,
				customers.name AS invoice_buyer_name,
				pim_articles.supplier_name, pim_articles.supplier_id
				FROM tblinvoice_line
				LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id
				LEFT JOIN pim_articles ON pim_articles.article_id = tblinvoice_line.article_id
				LEFT JOIN pim_article_categories ON pim_articles.article_category_id=pim_article_categories.id 
				LEFT JOIN customers ON tblinvoice.buyer_id = customers.customer_id
				WHERE  invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND
				 (tblinvoice.type='0' || tblinvoice.type='3' )  AND tblinvoice_line.article_id != '0' ".$filter."
				ORDER BY invoice_item_code ASC, invoice_buyer_name ASC "
				);
			$total_contacts = array();
			$total_companies = array();
			$art = array();
			$art1=array();
			$total_amount = 0;
			while ($articles_data->next()){
				$del_a = 0;
				$ndel_a = 0;

				$global_disc = $articles_data->f('global_discount');
				
				$discount_line = $articles_data->f('discount');
				if($articles_data->f('apply_discount') == 0 || $articles_data->f('apply_discount') == 2){
					$discount_line = 0;
				}
				if($articles_data->f('apply_discount') < 2){
					$global_disc = 0;
				}
				$q = $articles_data->f('quantity') ;
				$price_line = $articles_data->f('price') - ($articles_data->f('price') * $discount_line /100);

				$line_total=$price_line * $q;	

				if($articles_data->f('currency_type') != ACCOUNT_CURRENCY_TYPE){
					$line_total = $line_total*return_value($articles_data->f('currency_rate'));
				}	

				$total_amount+= $line_total - ($line_total*$global_disc/100);		
				$art[$articles_data->f('article_id')] += $line_total - ($line_total*$global_disc/100);
				$art1[$articles_data->f('article_id')] =$articles_data->f('article_category_id');	
			}
			$fam=array();
			foreach ($art as $art_id => $val) {
			    /*$family_id=$this->db->field("SELECT pim_articles.article_category_id 		    	       FROM pim_articles 
			    	        INNER JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			    	        WHERE pim_articles.article_id = '".$art_id."' and pim_articles.article_category_id!=0");*/
			    if($art1[$art_id]){
			       $fam[$art1[$art_id]]+=$val;
			    }
			}
			asort($fam);
			$fam= array_reverse($fam,true);
			$fam=array_slice($fam,0,5, true);

			foreach ($fam as $fam_id => $val) {		
				$fam_name = $this->db->field("SELECT name FROM pim_article_categories WHERE id = '".$fam_id."' ");
				
				array_push($result['label'], $fam_name);
				array_push($result['data'], number_format($val,2,'.',''));
				array_push($result['data_show'], get_currency_sign(display_number($val)));
			}
			//console::log($result);

			return $result;
		}

	}

	$cash_data = new ReportCatalogue($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_Data();
	$cash_data->output();
?>
