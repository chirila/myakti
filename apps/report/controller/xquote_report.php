<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class ReportQuote extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}
		public function get_Data(){
			ini_set('memory_limit','2G');
			$in=$this->in;
			$result=array();

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(23,59,59,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(23,59,59,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(23,59,59,$month,$today,$year);
					}
					$result['period_txt']='';
					break;
			}
			$in['time_start']=$time_start;
			$in['time_end']=$time_end;
			$filter = ' AND 1=1';
			if($in['customer_id']){
				$filter .= ' AND buyer_id ='.$in['customer_id'].' ';			
			}
			if($in['author_id']){
				$filter .= ' AND tblquote.created_by ='.$in['author_id'].' ';
			}
			if($in['source_id']){
				$filter .= ' AND tblquote.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblquote.type_id ='.$in['type_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND tblquote.acc_manager_id ='.$in['manager_id'].' ';				
			}

			$where = " quote_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblquote.f_archived='0' ";
			$quote_count = $this->db->query("SELECT count(id) AS total_orders FROM tblquote WHERE  ".$where." ".$filter );
			$quote = $this->db->query("SELECT currency_rate,id FROM tblquote WHERE ".$where." ".$filter);
			$big_total 	= 0;
			$z_amount_d = 0; 		
			$z_amount_n = 0; 		
			$z_amount_s = 0;
            $z_amount_e = 0;
            while ($quote->next()) {
				$lines = $this->db->query("SELECT tblquote_line.amount AS a, tblquote.discount, tblquote.apply_discount, tblquote.status_customer, tblquote.acc_manager_id, tblquote_version.sent, tblquote_version.version_status_customer, tblquote_line.line_discount FROM tblquote_line 
									INNER JOIN tblquote ON tblquote_line.quote_id=tblquote.id 
									INNER JOIN tblquote_version ON tblquote_line.version_id=tblquote_version.version_id 
									WHERE tblquote_version.active='1' AND tblquote_line.show_block_total='0' AND tblquote.id='".$quote->f('id')."' ");		
				while($lines->next()){		
					$amount = 0;
					$amount_d = 0;
					$amount_n = 0;
					$amount_s = 0;
					$amount_e = 0;
					$global_disc = $lines->f('discount');
					$disc_line = $lines->f('line_discount');		
					if($lines->f('apply_discount') == 0 || $lines->f('apply_discount') == 2){
						$disc_line = 0;
					}	
					$amount += $lines->f('a');						
					//$amount += $lines->f('a') - $lines->f('a')*$disc_line/100;				
					if($lines->f('apply_discount') < 2) {
						$global_disc = 0;
					}		
					$amount = $amount - ($amount*$global_disc/100);			
					if($quote->f('currency_rate')){
						$amount = $amount*return_value($quote->f('currency_rate'));			
					}					
					$big_total += $amount;		
					if(  $lines->f('version_status_customer')=='1'){
						$z_amount_n += $amount;	
					}
					if( $lines->f('version_status_customer')>'1'){
						/*if($amount){
							console::log($amount);
						}*/
						$z_amount_d += $amount;
					}			
					if($lines->f('status_customer')==0 && $lines->f('sent')==1 && $lines->f('version_status_customer') == 0){
						$z_amount_s += $amount;
					}
					if($lines->f('status_customer')==0 && $lines->f('sent')==0){
						$z_amount_e += $amount;
					}		
				}
			}

			$result['BIG_TOTAL_A'] 			= place_currency(display_number($big_total));
			$result['BIG_QUANTITY_A'] 		= round($order_article);
			$result['TOTAL_ORDERS'] 		= $quote_count->f('total_orders');
			$result['delivered_amount']		= place_currency(display_number($z_amount_d));
			$result['sent_amount']			= place_currency(display_number($z_amount_s));
			$result['draft_amount']			= place_currency(display_number($z_amount_e));
			$result['ndelivered_amount']	= place_currency(display_number($z_amount_n));
			/*$result['delivered_o']			= $this->db->field("SELECT count(id) FROM tblquote WHERE  ".$where." ".$filter." AND status_customer > '1' ");
			$result['ndelivered_o']			= $this->db->field("SELECT count(id) FROM tblquote WHERE  ".$where." ".$filter." AND status_customer='1' ");*/
			$result['delivered_o']			= $this->db->field("SELECT count(id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id WHERE  ".$where." ".$filter." AND  tblquote_version.version_status_customer >'1' AND tblquote_version.active='1' ");
			$result['ndelivered_o']			= $this->db->field("SELECT count(id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id WHERE  ".$where." ".$filter." AND  tblquote_version.version_status_customer = '1' AND tblquote_version.active='1'  ");
			$result['sent']					= $this->db->field("SELECT count(id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id WHERE  ".$where." ".$filter." AND  status_customer='0' AND tblquote_version.sent='1' AND tblquote_version.active='1' ");
			$result['draft']				= $this->db->field("SELECT count(id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id WHERE  ".$where." ".$filter." AND  status_customer='0' AND tblquote_version.sent='0' AND tblquote_version.active='1' ");
			$result['delivered_art']		= $this->db->field("SELECT count(tblquote_line.id) FROM tblquote_line INNER JOIN tblquote ON tblquote_line.quote_id=tblquote.id WHERE ".$where." ".$filter." AND article_id='0' ");
			$result['ndelivered_art']		= $this->db->field("SELECT count(tblquote_line.id) FROM tblquote_line INNER JOIN tblquote ON tblquote_line.quote_id=tblquote.id WHERE ".$where." ".$filter." AND article_id!='0' ");
			$result['EXPORT_HREF']			= 'index.php?do=report-export_quotes_detail_report&customer_id='.$in['customer_id'].'&manager_id='.$in['manager_id'].'&u_id='.$in['author_id'].'&source_id='.$in['source_id'].'&type_id='.$in['type_id'].'&time_start='.$time_start.'&time_end='.$time_end;

			$result['time_start']=$time_start;
			$result['time_end']=$time_end;
			$result['customers'] = $this->get_cc($in);
			$result['authors'] = $this->get_authors($in);
			$result['managers']=$this->get_authors($in);
			$result['customer_tab']=$this->get_customersTab($in);
			$result['source_dd']= get_categorisation_source();
        	$result['type_dd']= get_categorisation_type();
        	$result['adv_search']=(!empty($result['source_dd']) || !empty($result['type_dd'])) ? true : false;
			$this->out=$result;
		}

		public function get_cc(&$in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" is_admin='0' AND customers.active=1 ";
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			}
			if($in['customer_id']){
				$filter .=" AND customers.customer_id='".$in['customer_id']."' ";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type,cat_id
				FROM customers
				
				WHERE $filter
				GROUP BY customers.customer_id			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array('list'=>array());
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);
				if($value['type']==0){
					$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
				}elseif($value['type']==1){
					$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
				}else{
					$symbol ='';
				}
				$address = $this->db->query("SELECT zip,city,address FROM customer_addresses
										WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");
				
				$result_line=array(
					"id"					=> $value['cust_id'],
					'symbol'				=> $symbol,
					"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					/*'zip'					=> $value['zip'] ? $value['zip'] : '',
					'city'					=> $value['city'] ? $value['city'] : '',
					"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],*/
					'zip'					=> $address->f('zip') ? $address->f('zip') : '',
					'city'					=> $address->f('city') ? $address->f('city') : '',
					"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
					"right"					=> $value['acc_manager_name'],
					"cat_id"				=> $value['cat_id']
				);
				array_push($result['list'],$result_line);
			}		
			return $result;
		}

		public function get_authors(&$in){
			$q = strtolower($in["term"]);

			$filter = '';

			if($q){
				$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
			}
			if($in['author_id']){
				$filter .=" AND users.user_id='".$in['author_id']."' ";
			}

			$data=$this->db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY last_name ");
			while($data->move_next()){
			  $users[$data->f('last_name').' '.$data->f('first_name')]=$data->f('user_id');
			}

			$result = array('list'=>array());
			if($users){
				foreach ($users as $key=>$value) {
					$result_line=array(
						'id'		=> $value,
						'value'		=> htmlspecialchars_decode(stripslashes($key))
					);
					array_push($result['list'], $result_line);
				}
			}
			return $result;
		}

		public function get_customersTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter.=" and tblquote.buyer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND tblquote.created_by=".$in['author_id'];
			}
			if($in['source_id']){
				$filter .= ' AND tblquote.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblquote.type_id ='.$in['type_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND tblquote.acc_manager_id ='.$in['manager_id'].' ';				
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$ord = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $ord = " DESC ";
					}
			    }
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_orders = 0;
			// $z_amount = 0;
			$order = $this->db->query("SELECT tblquote.buyer_name as b_name,tblquote.buyer_id, tblquote.contact_id, count(DISTINCT tblquote.id) AS total_orders,
			SUM( Case When tblquote.currency_rate='' Then tblquote_line.amount ELSE tblquote_line.amount*(REPLACE( tblquote.currency_rate, ',', '.' )) End  ) as amount
			FROM tblquote
			LEFT JOIN tblquote_version ON tblquote.id = tblquote_version.quote_id
			LEFT JOIN tblquote_line ON tblquote.id = tblquote_line.quote_id AND tblquote_line.version_id=tblquote_version.version_id
			WHERE tblquote.quote_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblquote.f_archived='0' AND tblquote_version.active=1 {$filter} GROUP BY tblquote.buyer_name ORDER BY b_name");
			while ($order->next()) {
				$filter2 = " AND tblquote.buyer_id = '".$order->f('buyer_id')."' ";
				if(!$order->f('buyer_id')){
					$filter2 = " AND tblquote.contact_id = '".$order->f('contact_id')."' ";
				}
				$line = $this->db->query("SELECT tblquote.buyer_name AS b_name, tblquote.buyer_id, tblquote.contact_id, tblquote.apply_discount, tblquote.discount, tblquote.id, tblquote_line.amount, tblquote.currency_rate, tblquote_line.line_discount
						FROM tblquote
						LEFT JOIN tblquote_version ON tblquote.id = tblquote_version.quote_id
						LEFT JOIN tblquote_line ON tblquote.id = tblquote_line.quote_id
						AND tblquote_line.version_id = tblquote_version.version_id
						WHERE tblquote.quote_date
						BETWEEN '".$time_start."' AND '".$time_end."'
						AND tblquote.f_archived = '0'
						AND tblquote_version.active =1 {$filter} {$filter2}
						ORDER BY b_name");

					$amount = 0;
				while($line->next()){
					$global_disc = $line->f('discount');
					if($line->f('apply_discount') < 2) {
						$global_disc = 0;
					}
                    $amount = $amount + ($line->f('amount') - $line->f('amount')*$global_disc/100);
                    if($line->f('currency_rate')){
						$amount = $amount*return_value($line->f('currency_rate'));
					}
					// $z_amount += $amount;
				}

                $big_total += $amount;
				$other_row=array(
					'CUSTOMER_N'		=> $order->f('b_name'),
					'customer_id'		=> $order->f('buyer_id'),
					'NAV'				=> $time_nav,
					'TOTAL_CLIENT'		=> place_currency(display_number($amount)),
					'ORDERS'			=> $order->f('total_orders'),
					'TIME_START' 		=> $in['time_start'],
					'TIME_END'	 		=> $in['time_end'],
					'is_customer'		=> $order->f('buyer_id') ? true : false,
					'amount_ord'		=> $amount,
				);
				array_push($result['other_row'], $other_row);
				$big_orders += $order->f('total_orders');
				$i++;
			}

			$result['BIG_TOTAL']		= place_currency(display_number($big_total));
			$result['BIG_ORDERS']		= $big_orders;
			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$ord);   
			}
			return $result;
		}

		public function get_authorsTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter.=" and tblquote.buyer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND tblquote.created_by=".$in['author_id'];
			}
			if($in['source_id']){
				$filter .= ' AND tblquote.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblquote.type_id ='.$in['type_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND tblquote.acc_manager_id ='.$in['manager_id'].' ';				
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_orders = 0;
			$order = $this->db->query("SELECT tblquote.created_by, count(DISTINCT tblquote.id) AS total_orders,
			SUM( Case When tblquote.currency_rate='' Then tblquote_line.amount ELSE tblquote_line.amount*(REPLACE(tblquote.currency_rate,',','.')) End  ) as amount
			FROM tblquote 
			LEFT JOIN tblquote_version ON tblquote.id = tblquote_version.quote_id
			LEFT JOIN tblquote_line ON tblquote.id = tblquote_line.quote_id AND tblquote_line.version_id=tblquote_version.version_id
			WHERE tblquote.quote_date BETWEEN '{$time_start}' AND '{$time_end}' {$filter} AND tblquote.f_archived='0' AND tblquote_version.active=1 GROUP BY tblquote.created_by ");
			while ($order->next()) {
				$z_amount = 0;
				$line =$this->db->query("SELECT tblquote.apply_discount, tblquote.discount, tblquote.id, tblquote_line.amount, tblquote.currency_rate, tblquote_line.line_discount
						FROM tblquote
						LEFT JOIN tblquote_version ON tblquote.id = tblquote_version.quote_id
						LEFT JOIN tblquote_line ON tblquote.id = tblquote_line.quote_id
						AND tblquote_line.version_id = tblquote_version.version_id
						WHERE tblquote.quote_date
						BETWEEN '".$time_start."' AND '".$time_end."'
						AND tblquote.f_archived = '0'
						AND tblquote.created_by = '".$order->f('created_by')."'
						AND tblquote_version.active =1 {$filter} ");
				while($line->next()){
					$amount = 0;
                    $global_disc = $line->f('discount');
                    if($line->f('apply_discount') < 2) {
                        $global_disc = 0;
                    }
                    $amount = $amount + ($line->f('amount') - $line->f('amount')*$global_disc/100);
                    if($line->f('currency_rate')){
						$amount = $amount*return_value($line->f('currency_rate'));
					}			
					$big_total += $amount;
					$z_amount += $amount; 			
				}
				$other_row=array(
					'CUSTOMER_N'		=> get_user_name($order->f('created_by')),
					'CUSTOMER_ID'		=> 'created_by='.$order->f('created_by'),
					'NAV'				=> $time_nav,
					'TOTAL_CLIENT'		=> place_currency(display_number($z_amount)),
					'ORDERS'			=> $order->f('total_orders'),
					'TIME_START' 		=> $in['time_start'],
					'TIME_END'	 		=> $in['time_end'],
					'author_id'			=> $order->f('created_by'),
					'amount_ord' 		=> $z_amount
				);
				array_push($result['other_row'],$other_row);		
				$big_orders += $order->f('total_orders');
				$i++;
			}
			$result['BIG_TOTAL']		= place_currency(display_number($big_total));
			$result['BIG_ORDERS']		= $big_orders;
			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order);   
			}
			return $result;
		}

		public function get_quotesTab(&$in){
			$result=array('order_row'=>array());

			$order_by_array=array('serial_number','quote_date','buyer_name', 'amount');

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_quote']) || (!is_numeric($in['offset_quote'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_quote']-1;
			}

			$order_by = " ORDER BY quote_date DESC ";
			$filter= " AND tblquote.f_archived='0' AND active=1 ";

			if($in['customer_id']){
				$filter.=" and tblquote.buyer_id = ".$in['customer_id'];
			}
			if($in['author_id']){
				$filter.=" AND tblquote.created_by=".$in['author_id'];
			}

			switch($in['quote_tab']){
				case '1':
					$filter.=" AND status_customer=0 AND tblquote_version.sent=0 ";
					break;
				case '2':
					$filter.=" AND status_customer=0 AND tblquote_version.sent=1 ";
					break;
				case '3':
					//$filter.=" AND status_customer>1 ";
					$filter .=" AND tblquote_version.version_status_customer >'1' AND tblquote_version.active='1' ";
					break;
				default:
					//$filter.=" AND status_customer=1 ";
					$filter .=" AND tblquote_version.version_status_customer ='1' AND tblquote_version.active='1' ";
			}
			if($in['source_id']){
				$filter .= ' AND tblquote.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblquote.type_id ='.$in['type_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND tblquote.acc_manager_id ='.$in['manager_id'].' ';				
			}

			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
				}
			}

			$q = $this->db->query("SELECT tblquote.*,tblquote_version.*,tblquote_stage.name as stage_name, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date, tblquote.sent as sent_q
			        FROM tblquote 
			        INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
			        LEFT JOIN tblquote_stage ON tblquote_stage.id = tblquote.stage_id 
					WHERE quote_date BETWEEN '".$time_start."' AND '".$time_end."' ".$filter.$order_by );

			$max_rows=$q->records_count();
			$result['max_rows']=$max_rows;

			if($in['order_by']!='amount'){
				$q->move_to($offset*$l_r);
			}

			$j=0;
			$total_amount = 0;
			while($q->move_next() && $j<$l_r){
				$ref = '';
				if(ACCOUNT_ORDER_REF && $q->f('buyer_ref')){
					$ref = $q->f('buyer_ref');
				}
				$amount =0;
				$global_disc = $q->f('discount');
				$line = $this->db->query("SELECT amount, line_discount FROM tblquote_line WHERE quote_id='".$q->f('id')."' AND version_id='".$q->f('version_id')."' ");
				/*while ($line->next()) {
					$disc_line = $line->f('line_discount');
					if($q->f('apply_discount') == 0 || $q->f('apply_discount') == 2){
						$disc_line = 0;
					}
					$amount += $line->f('amount') - $line->f('amount')*$disc_line/100;
				}
				if($q->f('apply_discount') < 2) {
					$global_disc = 0;
				}	
				$amount = $amount - ($amount*$global_disc/100);
				if($q->f('currency_rate')){
					$amount = $amount*return_value($q->f('currency_rate'));
				}*/	
				$amount = $this->db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$q->f('id')."' AND version_id='".$q->f('version_id')."' AND show_block_total='0' ");

				if($q->f('discount') && $q->f('apply_discount') > 1){
					$amount = $amount - ($amount*$q->f('discount')/100);
				}

				if($q->f('currency_rate')){
					$amount = $amount*return_value($q->f('currency_rate'));
				}
				$was_sent = $this->db->field("SELECT count(version_id) FROM tblquote_version WHERE sent='1' AND quote_id='".$q->f('id')."' ");
				$order_row=array(
					'id'                	=> $q->f('id'),
					'serial_number'     	=> $q->f('serial_number') ? $ref.$q->f('serial_number') : '',
					'created'           	=> date(ACCOUNT_DATE_FORMAT,$q->f('quote_date')),
					'amount'				=> place_currency(display_number($amount)),
					'buyer_name'        	=> $q->f('buyer_name'),
					'is_archived'       	=> $in['archived'] ? true : false,
					'status'				=> $q->f('version_status_customer'),
					'amount_ord'			=> $amount? $amount : 0,
				);
				$opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
				$field = 'version_status_customer';
				$order_row['status_customer']=$opts[$q->f($field)];
				if($q->f('sent') == 1 && $q->f('version_status_customer') == 0){
					$order_row['status_customer']=$opts[5];
					$order_row['status']='5';
				}
				array_push($result['order_row'],$order_row);
				if($in['order_by']!='amount'){
					$j++;
				}
				$total_amount += $amount;
			}

			$result['total_amount']=place_currency(display_number($total_amount));

			if($in['order_by']=='amount'){

			    $result['order_row']=order_array_by_column($result['order_row'],$in['order_by'],$order, $offset, $l_r);    
			}
			return $result;
		}

		public function get_Digest(){
			$in=$this->in;
			$result=array('detaile_row'=>array(),'row'=>array());

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(0,0,0,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(0,0,0,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(0,0,0,$month,$today,$year);
					}
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
			}
			$filter = " AND 1=1 ";
			if($in['customer_id']){
				$filter .= " AND tblquote.buyer_id ='".$in['customer_id']."' ";			
			}
			if($in['author_id']){
				$filter .= " AND tblquote.created_by ='".$in['author_id']."' ";
			}
			if($in['source_id']){
				$filter .= ' AND tblquote.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblquote.type_id ='.$in['type_id'].' ';
			}
			if($in['manager_id']){
				$filter .= ' AND tblquote.acc_manager_id ='.$in['manager_id'].' ';				
			}
			$is_data=true;
			$where = " quote_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblquote.f_archived='0' ".$filter;
			$quote = $this->db->query("SELECT DISTINCT created_by FROM tblquote WHERE ".$where." ")->getAll();
			foreach ($quote as $key => $value) {
				$filter2 = " AND tblquote.created_by ='".$value['created_by']."' ".$filter;
				$detaile_row=array(
					'user'			=> get_user_name($value['created_by']),
					'accepted'		=> $this->db->field("SELECT count(id) FROM tblquote WHERE  ".$where." ".$filter2." AND status_customer > '1' "),
					'lost'			=> $this->db->field("SELECT count(id) FROM tblquote WHERE  ".$where." ".$filter2." AND status_customer='1' "),
					'sent'			=> $this->db->field("SELECT count(id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id WHERE  ".$where." ".$filter2." AND status_customer='0' AND tblquote_version.sent='1' AND tblquote_version.active='1' "),
					'draft'			=> $this->db->field("SELECT count(id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id WHERE  ".$where." ".$filter2." AND status_customer='0' AND tblquote_version.sent='0' AND tblquote_version.active='1' "),
					'lost_reason'	=> 'some reason'
				);
				array_push($result['detaile_row'],$detaile_row);
			}
			if(count($quote) == 0){
				$is_data = false;
			}
			$reason = $this->db->query("SELECT * FROM  `tblquote_lost_reason` ")->getAll();
			$k=0;
			foreach ($reason as $key => $value) {
				$filter_r = " AND lost_id ='".$value['id']."' ";
				$row=array(
					'DATE'			=> $value['name'],
					'detaile_rows'	=>array(),
				);
				$q_lost = $this->db->query("SELECT count(id) AS sn, created_by FROM tblquote WHERE  ".$where." ".$filter_r." AND status_customer='1' GROUP BY created_by ")->getAll();
				if(count($q_lost) == 0){
					continue;
				}
				foreach ($q_lost as $k => $val) {
					$detaile_rows=array(
						'userl'		=> get_user_name($val['created_by']),
						'quote_sn'	=> $val['sn'],				
					);
					array_push($row['detaile_rows'],$detaile_rows);
					$k++;
				}
				array_push($result['row'],$row);
			}	
			
			$result['is_data']		= $is_data;
			$result['q_lost']		= $k ? true : false;
			$this->out=$result;
		}

	}
	$cash_data = new ReportQuote($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_Data();
	$cash_data->output();
?>