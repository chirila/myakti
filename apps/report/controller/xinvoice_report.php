<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class ReportInvoice extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}
		public function get_Data(){
			ini_set('memory_limit','2G');
			$in=$this->in;
			$result=array();

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);
			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(23,59,59,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(23,59,59,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(23,59,59,$month,$today,$year);
					}
					$result['period_txt']='';
					break;
				case '5':
					//monthly - first invoice
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
			}
			$in['time_start']=$time_start;
			$in['time_end']=$time_end;
			$filter = ' AND 1=1';
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}
			if($in['manager_id']){
				//console::log($in['manager_id']);
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['c_type']){
				$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
				$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
				$filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
			}
			if($in['source_id']){
				$filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
			}

			$where = " invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='0' || tblinvoice.type='3' ) ";

			$invoice = $this->db->query("SELECT tblinvoice.acc_manager_id,tblinvoice.currency_rate,sum(tblinvoice.amount) AS amount,sum(tblinvoice.amount_vat) AS amount_vat, count(tblinvoice.id) AS total_invoices
				                 FROM tblinvoice 
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                 WHERE  ".$where." ".$filter." " );
			$article_amount = $this->db->query("SELECT tblinvoice.acc_manager_id,sum(tblinvoice_line.amount) AS amount,tblinvoice_line.article_id,tblinvoice.buyer_id,tblinvoice.invoice_date,tblinvoice.sent,tblinvoice.f_archived,tblinvoice.type
				                 FROM tblinvoice_line 
				                 INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                 WHERE  ".$where." ".$filter."  AND tblinvoice_line.article_id!=0" );

			$credited_amount=$this->db->field("SELECT COALESCE(SUM(ROUND(ABS(tblinvoice_payments.amount),2)),0) FROM tblinvoice_payments 
				INNER JOIN tblinvoice on tblinvoice.id=tblinvoice_payments.invoice_id
				LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				WHERE ".$where." ".$filter."  AND tblinvoice_payments.credit_payment='1'");
			$credited_invoices_str="";
			$credited_invoices=$this->db->query("SELECT DISTINCT(tblinvoice.id) FROM tblinvoice_payments 
				INNER JOIN tblinvoice on tblinvoice.id=tblinvoice_payments.invoice_id
				LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				WHERE  ".$where." ".$filter."  AND tblinvoice_payments.credit_payment='1'")->getAll();
			foreach($credited_invoices as $k=>$v){
				$credited_invoices_str.="'".$v['id']."',";	
			}
			$credited_invoices_str=rtrim($credited_invoices_str,",");
			$filter_credited="";
			if($credited_invoices_str){
				$filter_credited.=" AND tblinvoice_payments.invoice_id NOT IN (".$credited_invoices_str.") ";
			}

			$paid_amount=$this->db->field("SELECT COALESCE(SUM(ROUND(tblinvoice_payments.amount,2)),0) ,tblinvoice.buyer_id,tblinvoice.acc_manager_id
					                           FROM  tblinvoice_payments 
					                           INNER JOIN   tblinvoice on tblinvoice.id=tblinvoice_payments.invoice_id
					                           LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
					                           WHERE  ".$where." ".$filter."  AND tblinvoice_payments.credit_payment='0' ".$filter_credited." ");

			$npaid_amount=$this->db->field("SELECT COALESCE(SUM(tblinvoice.amount_vat),0) ,tblinvoice.buyer_id,tblinvoice.acc_manager_id
					                           FROM tblinvoice 
					                           LEFT JOIN tblinvoice_payments on tblinvoice.id=tblinvoice_payments.invoice_id
					                           LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
					                           WHERE  ".$where." ".$filter." AND tblinvoice.paid!='1' AND tblinvoice_payments.invoice_id IS NULL");

			$data=$this->db->query("SELECT tblinvoice.currency_rate,tblinvoice.margin,tblinvoice.acc_manager_id, SUM( tblinvoice_line.purchase_price * tblinvoice_line.quantity ) as purchase_amount
				                 FROM tblinvoice 
				                 LEFT JOIN tblinvoice_line ON tblinvoice.id = tblinvoice_line.invoice_id
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                 WHERE  ".$where." ".$filter." GROUP BY tblinvoice.id " );
			$big_margin =0;
			$big_margin_percent=0;
			$purchase_amount=0;
			while ($data->move_next()) { 
			    if($data->f('currency_rate')){
			        $big_margin+=$data->f('margin')*return_value($data->f('currency_rate')) ;
			        $purchase_amount+=$data->f('purchase_amount')*return_value($data->f('currency_rate')) ;
			    }else{
			        $big_margin+=$data->f('margin') ;
			        $purchase_amount+=$data->f('purchase_amount');
			    }
			         
			}
			$result['is_profi']= USE_PROFI ? true : false;
			$result['EXPORT_HREF']= 'index.php?do=report-export_invoices_detail_report&customer_id='.$in['customer_id'].'&time_start='.$time_start.'&time_end='.$time_end.'&m_id='.$in['manager_id'].'&identity_id='.$in['identity_id'].'&c_type='.$in['c_type'].'&source_id='.$in['source_id'].'&type_id='.$in['type_id'];
			$result['adv_search']	= true;

			$where_c = " invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='2' || tblinvoice.type='4') ";

			$invoice_c = $this->db->query("SELECT tblinvoice.acc_manager_id,tblinvoice.currency_rate,sum(tblinvoice.amount) AS amount,sum(tblinvoice.amount_vat) AS amount_vat, count(tblinvoice.id) AS total_invoices, sum(abs(tblinvoice.amount)) AS amount_abs, sum(abs(tblinvoice.amount_vat)) AS amount_vat_abs
				                 FROM tblinvoice 
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                 WHERE  ".$where_c." ".$filter." " );
			$data1=$this->db->query("SELECT tblinvoice.acc_manager_id,tblinvoice.currency_rate,tblinvoice.margin, SUM( tblinvoice_line.purchase_price * tblinvoice_line.quantity ) as purchase_amount_c
				                 FROM tblinvoice 
				                 LEFT JOIN tblinvoice_line ON tblinvoice.id = tblinvoice_line.invoice_id
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                 WHERE  ".$where_c." ".$filter." GROUP BY tblinvoice.id " );
			$big_c_margin =0;
			$big_c_margin_percent=0;
			$purchase_amount_c =0;
			while ($data1->move_next()) { 
			    if($data1->f('currency_rate')){
			         $big_c_margin+=$data1->f('margin')*return_value($data1->f('currency_rate')) ;
			         $purchase_amount_c+=$data1->f('purchase_amount_c')*return_value($data1->f('currency_rate')) ;
			    }else{
			         $big_c_margin+=$data1->f('margin') ;
			         $purchase_amount_c+=$data1->f('purchase_amount_c');
			    }
			         
			}

			$big_c_margin_percent=0;
			if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
			    if($purchase_amount_c){
					$big_c_margin_percent=$big_c_margin/$purchase_amount_c *100;
				}else{
					if($big_c_margin){
						$big_c_margin_percent=100;
					}	
				}
		     }else{
		         if($invoice_c->f('amount')){
					$big_c_margin_percent=$big_c_margin/$invoice_c->f('amount') *100;
				}else{
					if($big_c_margin){
						$big_c_margin_percent=100;
					}	
				}
		     }
		
			
			$negative = 1;
		    if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
		      $negative = -1;
		    }   

			//$result['BIG_TOTAL_C'] 		        = place_currency(display_number($invoice_c->f('amount')));
			$result['BIG_TOTAL_C'] 		        = place_currency(display_number($negative*$invoice_c->f('amount_abs')));
			//$result['BIG_VAT_C'] 		        = place_currency(display_number($invoice_c->f('amount_vat')-$invoice_c->f('amount')));
			//$result['BIG_VAT_C'] 		        = place_currency(display_number($invoice_c->f('amount_vat')-abs($invoice_c->f('amount'))));
			$result['BIG_VAT_C'] 		        = place_currency(display_number($negative*($invoice_c->f('amount_vat_abs')-$invoice_c->f('amount_abs'))));
			$result['BIG_C_MARGIN']		        = place_currency(display_number($big_c_margin));
			$result['BIG_C_MARGIN_PERCENT']		= display_number($big_c_margin_percent);
			$profit=$invoice->f('amount')-$big_margin;
			//$big_margin=$big_margin-$big_c_margin;
			$big_margin=$big_margin-abs($big_c_margin);
			//$big_margin_percent=$big_margin/($invoice->f('amount')-$invoice_c->f('amount')) *100;
			//$big_margin_percent=$big_margin/($invoice->f('amount')-abs($invoice_c->f('amount'))) *100; 
			$big_margin_percent=0; 
			if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				if($purchase_amount-abs($purchase_amount_c)){
					$big_margin_percent=$big_margin/($purchase_amount-abs($purchase_amount_c)) *100;  
				}else{
					if($big_margin){
						$big_margin_percent=100;
					}
				}
			}else{
				if($invoice->f('amount')-abs($invoice_c->f('amount'))){
					$big_margin_percent=$big_margin/($invoice->f('amount')-abs($invoice_c->f('amount'))) *100;  
				}else{
					if($big_margin){
						$big_margin_percent=100;
					}
				}
			}			
			//$result['BIG_TOTAL_A'] 			= place_currency(display_number($invoice->f('amount')-$invoice_c->f('amount')));
			//$result['BIG_TOTAL_A'] 			= place_currency(display_number($invoice->f('amount')-abs($invoice_c->f('amount'))));
			$result['BIG_TOTAL_A'] 			= place_currency(display_number($invoice->f('amount')));
			//$result['BIG_VAT_A'] 			= place_currency(display_number($invoice->f('amount_vat')-$invoice->f('amount')-abs($invoice_c->f('amount'))));
			$result['BIG_VAT_A'] 			= place_currency(display_number($invoice->f('amount_vat')-$invoice->f('amount')));
			//$result['total_amount']      	= $invoice->f('amount')-$invoice_c->f('amount');
			$result['total_amount']       = $invoice->f('amount')-abs($invoice_c->f('amount')); 			
			$result['TOTAL_INVOICES'] 		= $invoice->f('total_invoices');
			$result['paid_amount']			= $paid_amount;	
			$result['credited_amount']		= $credited_amount;			
			//$result['npaid_amount']	     	= $invoice->f('amount')-$invoice_c->f('amount')-$paid_amount;
			//$result['npaid_amount']	     	= $invoice->f('amount_vat')-$invoice_c->f('amount_vat')-$paid_amount;
			//$result['npaid_amount']       = $invoice->f('amount')-abs($invoice_c->f('amount'))-$paid_amount;
			//$result['npaid_amount']       = $invoice->f('amount')-$paid_amount;
			//$result['npaid_amount']       = $invoice->f('amount_vat')-$paid_amount >0? $invoice->f('amount_vat')-$paid_amount :0;
			$result['npaid_amount']       = $npaid_amount;
			$result['BIG_MARGIN']		 	= place_currency(display_number($big_margin));
			$result['BIG_MARGIN_PERCENT']	= display_number($big_margin_percent);
			$result['is_credit']			= $invoice_c->f('amount') ? true : false;
			$result['time_start']=$time_start;
			$result['time_end']=$time_end;
			$result['customers'] = $this->get_cc($in);
			$result['managers']= $this->get_managers($in);
			$result['customer_tab']=$this->get_customersTab($in);
			$result['chart_invoice']=array(				
					'label'			=> array(gm('Paid'),gm('Unpaid'),gm('Credited')),
					'data' 			=> array(number_format($result['paid_amount'],2,'.',''),number_format($result['npaid_amount'],2,'.',''),number_format($result['credited_amount'],2,'.','')),
					'data_show' 	=> array(get_currency_sign(display_number($result['paid_amount'])),get_currency_sign(display_number($result['npaid_amount'])),get_currency_sign(display_number($result['credited_amount']))),
				);
			$result['chart_customer']=$this->get_customerChart($in);
			$result['chart_manager']=$this->get_managerChart($in);

			if($in['period_id']=='5'){ // the results are from the customer tab
				$result['BIG_TOTAL_A'] 			= $result['customer_tab']['BIG_TOTAL'];
				$result['BIG_VAT_A'] 			= place_currency(display_number($result['customer_tab']['BIG_VAT']));			
				$result['TOTAL_INVOICES'] 		= $result['customer_tab']['BIG_INVOICES']? $result['customer_tab']['BIG_INVOICES']:0;
				$result['BIG_TOTAL_C'] 		        = place_currency(display_number($result['customer_tab']['BIG_TOTAL_C']));
				$result['BIG_VAT_C'] 		        = place_currency(display_number($result['customer_tab']['BIG_VAT_C']));
				$result['BIG_MARGIN']		 	= place_currency(display_number($result['customer_tab']['BIG_MARGIN']));
				$result['BIG_MARGIN_PERCENT']	= display_number($result['customer_tab']['BIG_MARGIN_PERCENT']);
				$result['chart_invoice']=array(				
					'label'			=> array(gm('Paid'),gm('Unpaid')),
					'data' 			=> array(number_format($result['customer_tab']['paid_amount'],2,'.',''),number_format($result['customer_tab']['npaid_amount'],2,'.','')),
					'data_show' 	=> array(get_currency_sign(display_number($result['customer_tab']['paid_amount'])),get_currency_sign(display_number($result['customer_tab']['npaid_amount']))),
				);
				$result['chart_customer']=$result['customer_tab']['customer_chart'];
				$result['manager_tab']=$this->get_managersTab($in);
				$result['chart_manager']=$result['manager_tab']['manager_chart'];
			}

			$result['customer_type']			= getCustomerType();
			$result['identities']				= build_identity_dd();
			$result['source_dd']= get_categorisation_source();
        	$result['type_dd']= get_categorisation_type();
			$this->out=$result;
		}

		public function get_cc(&$in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" is_admin='0' AND customers.active=1 ";
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			}
			if($in['customer_id']){
				$filter .=" AND customers.customer_id='".$in['customer_id']."' ";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type,cat_id
				FROM customers
				
				WHERE $filter
				GROUP BY customers.customer_id			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array('list'=>array());
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);
				if($value['type']==0){
					$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
				}elseif($value['type']==1){
					$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
				}else{
					$symbol ='';
				}

				$address = $this->db->query("SELECT zip,city,address FROM customer_addresses
										WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");
				$result_line=array(
					"id"					=> $value['cust_id'],
					'symbol'				=> $symbol,
					"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					/*'zip'					=> $value['zip'] ? $value['zip'] : '',
					'city'					=> $value['city'] ? $value['city'] : '',
					"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],*/
					'zip'					=> $address->f('zip') ? $address->f('zip') : '',
					'city'					=> $address->f('city') ? $address->f('city') : '',
					"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
					"right"					=> $value['acc_manager_name'],
					"cat_id"				=> $value['cat_id']
				);
				array_push($result['list'],$result_line);
			}		
			return $result;
		}

		public function get_managers(&$in){
			$q = strtolower($in["term"]);

			$filter = '';

			if($q){
				$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
			}
			if($in['manager_id']){
				$filter .=" AND users.user_id='".$in['manager_id']."' ";
			}

			$data=$this->db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY last_name ");
			while($data->move_next()){
			  $users[$data->f('last_name').' '.$data->f('first_name')]=$data->f('user_id');
			}

			$result = array('list'=>array());
			if($users){
				foreach ($users as $key=>$value) {
					$result_line=array(
						'id'		=> $value,
						'value'		=> htmlspecialchars_decode(stripslashes($key))
					);
					array_push($result['list'], $result_line);
				}
			}
			return $result;
		}

		public function get_customersTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['c_type']){
					$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
					$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
					$filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
			}
			if($in['source_id']){
				$filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
			}
			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_customer']) || (!is_numeric($in['offset_customer'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_customer']-1;
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_invoice = 0;

			$invoice = $this->db->query("SELECT tblinvoice.acc_manager_id,tblinvoice.currency_rate,sum(amount) AS amount,sum(margin) AS total_margin, count(id) AS total_invoice, tblinvoice.contact_id,buyer_name, tblinvoice.buyer_id, tblinvoice.invoice_date, tblinvoice.id FROM tblinvoice 
				LEFT JOIN customers ON tblinvoice.buyer_id=customers.customer_id
				WHERE invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.buyer_id!='0'  AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='0' || tblinvoice.type='3')  $filter
				GROUP BY tblinvoice.buyer_id,tblinvoice.contact_id ");

			$max_rows=$invoice->records_count();
			$result['max_rows']=$max_rows;
			if($in['order_by']!='amount'){
				$invoice->move_to($offset*$l_r);
			}

			while ($invoice->next() && $i<$l_r){
				$article_amount = $this->db->query("SELECT tblinvoice.acc_manager_id,sum(tblinvoice_line.amount) AS amount,sum(tblinvoice_line.purchase_price * tblinvoice_line.quantity) AS purchase_amount,tblinvoice_line.article_id,tblinvoice.buyer_id,tblinvoice.invoice_date,tblinvoice.sent,tblinvoice.f_archived,tblinvoice.type
				                 FROM tblinvoice_line 
				                 INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                WHERE  tblinvoice.buyer_id= '".$invoice->f('buyer_id')."' AND tblinvoice_line.article_id!=0 and invoice_date BETWEEN '".$time_start."' AND '".$time_end."'  AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='0' || tblinvoice.type='3')  $filter ");
				$article_amount_c = $this->db->query("SELECT tblinvoice.acc_manager_id,sum(tblinvoice_line.amount) AS amount,sum(tblinvoice_line.purchase_price * tblinvoice_line.quantity) AS purchase_amount,tblinvoice_line.article_id,tblinvoice.buyer_id,tblinvoice.invoice_date,tblinvoice.sent,tblinvoice.f_archived,tblinvoice.type
				                 FROM tblinvoice_line 
				                 INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                WHERE  tblinvoice.buyer_id= '".$invoice->f('buyer_id')."' AND tblinvoice_line.article_id!=0 and invoice_date BETWEEN '".$time_start."' AND '".$time_end."'  AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='2' || tblinvoice.type='4')  $filter ");
				$big_margin=0;
			    $big_margin_percent=0;

				if($invoice->f('currency_rate')){
				    $big_margin = $invoice->f('total_margin')-$article_amount_c->f('amount') * return_value($invoice->f('currency_rate'));
				    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				    	if($article_amount->f('purchase_amount')-$article_amount_c->f('purchase_amount')){
				    		$big_margin_percent=$big_margin*100/ ($article_amount->f('purchase_amount')-$article_amount_c->f('purchase_amount')) *return_value($invoice->f('currency_rate')) ;
					    }elseif($big_margin){
					    	$big_margin_percent=100;
					    }
				    }else{
				    	if($invoice->f('amount')){
					    	$big_margin_percent=$big_margin*100/ ($invoice->f('amount')) *return_value($invoice->f('currency_rate')) ;
					    }elseif($big_margin){
					    	$big_margin_percent=100;
					    }
				    }
				    			    
				}else{
					 $big_margin = $invoice->f('total_margin')-$article_amount_c->f('amount');
					 if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				    	if($article_amount->f('purchase_amount')-$article_amount_c->f('purchase_amount')){
				    		$big_margin_percent=$big_margin*100/ ($article_amount->f('purchase_amount')-$article_amount_c->f('purchase_amount'));
					    }elseif($big_margin){
					    	$big_margin_percent=100;
					    }
					 }else{
					    	if($invoice->f('amount')){
						    	$big_margin_percent=$big_margin*100/ ($invoice->f('amount')) ;
						    }elseif($big_margin){
						    	$big_margin_percent=100;
						    }
					   }

				}	

				$other_row=array(
						'CUSTOMER_N'		=> ($invoice->f('contact_id') && $invoice->f('buyer_id')) ?$invoice->f('buyer_name').' > '.$this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$invoice->f('contact_id')."'"):$invoice->f('buyer_name'),
						'CUSTOMER_ID'		=> $invoice->f('buyer_id'),
						'margin'		    => place_currency(display_number($big_margin)),
			            'margin_percent'    => display_number($big_margin_percent),
						'TOTAL_CLIENT'		=> place_currency(display_number($invoice->f('amount'))),				
						'INVOICES'			=> $invoice->f('total_invoice'),
						'amount_ord'		=> $invoice->f('amount'),
				);		

				if($in['period_id']=='5' ){
					 $big_margin =0;
					 $big_margin_percent=0;
					 $big_margin_total=0;
					$first_invoice = $this->db->query("SELECT tblinvoice.*, SUM( tblinvoice_line.purchase_price * tblinvoice_line.quantity ) as purchase_amount, customers.name FROM tblinvoice 
											LEFT JOIN tblinvoice_line ON tblinvoice.id = tblinvoice_line.invoice_id
						 					LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
											WHERE tblinvoice.sent !=0 AND  tblinvoice.buyer_id ='".$invoice->f('buyer_id')."' AND tblinvoice.f_archived =0 AND tblinvoice.invoice_date !=0 AND (tblinvoice.type='0' || tblinvoice.type='3')
											GROUP BY tblinvoice.id
											ORDER BY tblinvoice.id ASC LIMIT 1");

				    if($first_invoice->f('id')==$invoice->f('id')){
				   		if($first_invoice->f('currency_rate')){
				   			$currency_rate = return_value($first_invoice->f('currency_rate'));
				   		}else{
				   			$currency_rate = 1;
				   		}
						    $big_margin = $first_invoice->f('margin') * $currency_rate;
						    //$big_margin_percent=$big_margin*100/ $first_invoice->f('amount') *$currency_rate ;
						     if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
						     	if($first_invoice->f('purchase_amount')){
							      $big_margin_percent=$big_margin*100/ $first_invoice->f('purchase_amount') *$currency_rate ;
							    }elseif($big_margin){
							    	$big_margin_percent=100;
							    }
							 }else{
							 	if($first_invoice->f('amount')){
							      $big_margin_percent=$big_margin*100/ $first_invoice->f('amount') *$currency_rate;
							    }elseif($big_margin){
							    	$big_margin_percent=100;
							    }
							 }
						
				   		$other_row=array(
							'CUSTOMER_N'		=> $first_invoice->f('buyer_name'),
							'CUSTOMER_ID'		=> $first_invoice->f('buyer_id'),
							'margin'		    => place_currency(display_number($big_margin)),
				            'margin_percent'    => display_number($big_margin_percent),
							'TOTAL_CLIENT'		=> place_currency(display_number($first_invoice->f('amount'))),	
							'total_client_value'		=> $first_invoice->f('amount'),				
							'INVOICES'			=> '1',
							'amount_ord'		=> $first_invoice->f('amount'),
						);
						array_push($result['other_row'], $other_row);
						$paid_amount = $this->db->field("SELECT SUM( tblinvoice_payments.amount ) FROM tblinvoice_payments
									INNER JOIN tblinvoice ON tblinvoice.id = tblinvoice_payments.invoice_id
									LEFT JOIN customers ON customers.customer_id = tblinvoice.buyer_id
									WHERE tblinvoice.id ='".$first_invoice->f('id')."'");

						$paid_amount_total += $paid_amount;

						$big_total += $first_invoice->f('amount');
						$big_total_vat += $first_invoice->f('amount_vat');
						$big_invoices++;
						$big_margin_total += $big_margin;
						$i++;


					}

				}else{
					array_push($result['other_row'], $other_row);
					$big_total += $invoice->f('amount');
					$big_quantity += $invoice_article;
					$big_invoices += $invoice->f('total_invoices');
					if($in['order_by']!='amount'){
						$i++;
					}
				}	
				
			}

			$result['BIG_TOTAL']	= place_currency(display_number($big_total));				
			$result['BIG_INVOICES']	= $big_invoices;

			if($in['period_id']=='5' ){
				$result['BIG_TOTAL_VAT']	= $big_total_vat;
				$result['BIG_VAT']	= $big_total_vat-$big_total;	
				$result['BIG_MARGIN']	= $big_margin_total;
				$result['BIG_MARGIN_PERCENT']	= $big_margin_total && $big_total ? ($big_margin_total*100)/$big_total : 0;
				$result['BIG_TOTAL_C']	= 0;
				$result['BIG_VAT_C']	= 0;
				$result['max_rows']=$i;
				$result['paid_amount']=$paid_amount_total;
				$result['npaid_amount']=$big_total-$paid_amount_total;

				$arr=array();
				$arr = $result['other_row'];
				usort($arr, create_function('$a,$b', 'return $b["total_client_value"] - $a["total_client_value"];'));
				$arr = array_slice($arr, 0, 5, true);
				$result['customer_chart']=array('label'=>array(),'data'=>array(),'data_show'=>array());
				foreach($arr as $k=>$v){
					array_push($result['customer_chart']['label'], $v['CUSTOMER_N']);
					array_push($result['customer_chart']['data'], number_format($v['total_client_value'],2,'.',''));
					array_push($result['customer_chart']['data_show'], get_currency_sign(display_number($v['total_client_value'])));
				}

				
			}

			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order, $offset, $l_r);    
			}
	
			return $result;
		}

		public function get_invoicesTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter = '';
			$order_by = " ORDER BY t_date DESC";
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}
			if($in['manager_id']){
				if($in['invoice_tab']=='1'){
					$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
				}else{
					//$filter .= ' AND  FIND_IN_SET( '.$in['m_id'].', customers.user_id )  ';	
					$filter .= ' AND  FIND_IN_SET( '.$in['manager_id'].', customers.user_id )  ';	
				}		
			}
			if($in['c_type']){
					$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
					$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
					$filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
			}
			if($in['source_id']){
				$filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
			}
			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_invoice']) || (!is_numeric($in['offset_invoice'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_invoice']-1;
			}
			switch($in['invoice_tab']){
				case '1':
					$filter.=" AND tblinvoice.sent!='0' AND  tblinvoice.f_archived='0'  AND (tblinvoice.type='0' || tblinvoice.type='3') ";
					break;
				case '2':
					$filter.=" AND tblinvoice.sent!='0' AND  tblinvoice.f_archived='0'  AND (tblinvoice.type='2' || tblinvoice.type='4') ";
					break;
			}
			if(!empty($in['order_by'])){
				if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
				}
			}

			$invoices=$this->db->query("SELECT tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, sum(tblinvoice_line.purchase_price * tblinvoice_line.quantity) as purchase_amount
						FROM tblinvoice 
						LEFT JOIN tblinvoice_line ON tblinvoice.id = tblinvoice_line.invoice_id
						LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
						WHERE invoice_date BETWEEN '".$time_start."' AND '".$time_end."' ".$filter." AND tblinvoice.buyer_id !=0 GROUP BY tblinvoice.id ".$order_by );

			$max_rows=$invoices->records_count();
			$result['max_rows']=$max_rows;

			if($in['order_by']!='amount'){
				$invoices->move_to($offset*$l_r);
			}
			$j=0;
			$color = '';
			$total_amount = 0;
			$total_margin = 0;
			while($invoices->move_next() && $j<$l_r){
				$status='';
				$status_title='';
				$type='';
				$type_title='';
				$color='';
				switch ($invoices->f('type')){
					case '0':
						$type = 'regular_invoice';
						$type_title = gm('Regular invoice');
						break;
					case '1':
						$type = 'pro-forma_invoice';
						$type_title = gm('Proforma invoice');
						break;
					case '2':
						$type = 'credit_invoice';
						$type_title = gm('Credit Invoice');
						break;
					case '3':
						$type = 'regular_invoice';
						$type_title = gm('Regular invoice');
						break;
					case '4':
						$type = 'credit_invoice';
						$type_title = gm('Credit Invoice');
						break;
				}

				switch ($invoices->f('status')){
					case '0':
						if($invoices->f('paid') == '2' ){
							$status='partial';
							$status_title= gm('Partially Paid');
							$color = '';
						}
						if($invoices->f('due_date') < time()){
							$status = 'late';
							$status_title= gm('Late');
							$color = 'late';

						}
						break;
					case '1':
						if($invoices->f('type') != '2' && $invoices->f('type') != '4' ){
							$color = '';
							$status='paid';
							$status_title= gm('Paid');
						}
						break;
				}
				if($invoices->f('sent') == '0' && $invoices->f('type')!='2' && $invoices->f('type') != '4'){
					$type_title = gm('Draft');
					$nr_status=1;
			    }else if($invoices->f('status')=='1'){
					$type_title = gm('Paid');
					$nr_status=3;
			    }else if($invoices->f('sent')!='0' && $invoices->f('not_paid')=='0'){
					$type_title = gm('Final');
					$nr_status=2;
			    }

				if($invoices->f('currency_rate')){
					$currency_rate=$invoices->f('currency_rate');
				}else{
					$currency_rate=1;
				}

				$late = false;
			    if($invoices->f('not_paid') == '0' && $invoices->f('status') == '0' && $invoices->f('sent') == '1' && $invoices->f('due_date') < time()){
				$late = true;
			    }

			    $margin_percent=0;
			    if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
				    if($invoices->f('purchase_amount')){
				    	 $margin_percent = $invoices->f('margin')/$invoices->f('purchase_amount') *100;
				    }elseif($invoices->f('margin')!='0.0000'){
				    	 $margin_percent = 100;
				    }
				}else{
					if($invoices->f('amount')){
				    	 $margin_percent = $invoices->f('margin')/$invoices->f('amount') *100;
				    }elseif($invoices->f('margin')!='0.0000'){
				    	 $margin_percent = 100;
				    }
				}
				$other_row=array(
					'id'                	=> $invoices->f('id'),
					'serial_number'     	=> $invoices->f('serial_number') ? $ref.$invoices->f('serial_number') : '',
					'created'           	=> date(ACCOUNT_DATE_FORMAT,$invoices->f('invoice_date')),
					'amount'				=> place_currency(display_number($invoices->f('amount'))),
					'vat'					=> place_currency(display_number($invoices->f('amount_vat')-$invoices->f('amount'))),
					'margin'				=> place_currency(display_number($invoices->f('margin')*return_value($currency_rate))),
			        //'margin_percent'		=> display_number($invoices->f('margin_percent')),		
			        'margin_percent'		=> display_number($margin_percent),			
					'buyer_name'        	=> $invoices->f('buyer_name'),
					'type'					=> $type,
					'type_title'			=> $type_title,
					'status'				=> $status,
					'status_title'			=> $status_title,
					'invoice_property'		=> $color,	
					'nr_status'				=> $nr_status,	
					'late'					=> $late,
					'amount_ord'			=> $invoices->f('amount'),	
				);

				if($in['period_id']=='5' ){ // only the first invoice ever
					$first_invoice = $this->db->query("SELECT  tblinvoice.*,  SUM( tblinvoice_line.purchase_price * tblinvoice_line.quantity ) as purchase_amount FROM tblinvoice 
						LEFT JOIN tblinvoice_line ON tblinvoice.id = tblinvoice_line.invoice_id
						WHERE sent !=0 AND  buyer_id ='".$invoices->f('buyer_id')."' AND tblinvoice.f_archived =0 AND invoice_date !=0 AND (type ='0' OR type ='3') 
						GROUP BY tblinvoice.id
						ORDER BY id ASC LIMIT 1");

				    if($first_invoice->f('id')==$invoices->f('id')){
						$status='';
						$status_title='';
						$type='';
						$type_title='';
						$color='';
						switch ($first_invoice->f('type')){
							case '0':
								$type = 'regular_invoice';
								$type_title = gm('Regular invoice');
								break;
							case '1':
								$type = 'pro-forma_invoice';
								$type_title = gm('Proforma invoice');
								break;
							case '2':
								$type = 'credit_invoice';
								$type_title = gm('Credit Invoice');
								break;
							case '3':
								$type = 'regular_invoice';
								$type_title = gm('Regular invoice');
								break;
							case '4':
								$type = 'credit_invoice';
								$type_title = gm('Credit Invoice');
								break;
						}

						switch ($first_invoice->f('status')){
							case '0':
								if($first_invoice->f('paid') == '2' ){
									$status='partial';
									$status_title= gm('Partially Paid');
									$color = '';
								}
								if($first_invoice->f('due_date') < time()){
									$status = 'late';
									$status_title= gm('Late');
									$color = 'late';

								}
								break;
							case '1':
								if($first_invoice->f('type') != '2' && $first_invoice->f('type') != '4'){
									$color = '';
									$status='paid';
									$status_title= gm('Paid');
								}
								break;
						}
						if($first_invoice->f('sent') == '0' && $first_invoice->f('type')!='2' && $first_invoice->f('type') != '4'){
							$type_title = gm('Draft');
							$nr_status=1;
					    }else if($first_invoice->f('status')=='1'){
							$type_title = gm('Paid');
							$nr_status=3;
					    }else if($first_invoice->f('sent')!='0' && $first_invoice->f('not_paid')=='0'){
							$type_title = gm('Final');
							$nr_status=2;
					    }

						$late = false;
					    if($first_invoice->f('not_paid') == '0' && $first_invoice->f('status') == '0' && $first_invoice->f('sent') == '1' && $first_invoice->f('due_date') < time()){
						$late = true;
					    }

				    	if($first_invoice->f('currency_rate')){
							$currency_rate=$first_invoice->f('currency_rate');
						}else{
							$currency_rate=1;
						}

						$big_margin = $first_invoice->f('margin') * return_value($currency_rate);
						//$big_margin_percent=$big_margin*100/ $first_invoice->f('amount') *return_value($currency_rate) ;

						$big_margin_percent=0;
						if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
							if($first_invoice->f('purchase_amount')){
								$big_margin_percent=$big_margin*100/ $first_invoice->f('purchase_amount') *return_value($currency_rate); 	
							}elseif($big_margin){
								$big_margin_percent=100;
							}
						}else{
							if($first_invoice->f('amount')){
								$big_margin_percent=$big_margin*100/ $first_invoice->f('amount') *return_value($currency_rate); 	
							}elseif($big_margin){
								$big_margin_percent=100;
							}
						}

						$other_row=array(
							'id'                	=> $first_invoice->f('id'),
							'serial_number'     	=> $first_invoice->f('serial_number'),
							'created'           	=> date(ACCOUNT_DATE_FORMAT,$first_invoice->f('invoice_date')),
							'amount'				=> place_currency(display_number($first_invoice->f('amount'))),
							'vat'					=> place_currency(display_number($first_invoice->f('amount_vat')-$first_invoice->f('amount'))),
							'margin'				=> place_currency(display_number($first_invoice->f('margin')*return_value($currency_rate))),
					        //'margin_percent'		=> display_number($first_invoice->f('margin_percent')),	
					        'margin_percent'		=> display_number($big_margin_percent),					
							'buyer_name'        	=> $first_invoice->f('buyer_name'),
							'type'					=> $type,
							'type_title'			=> $type_title,
							'status'				=> $status,
							'status_title'			=> $status_title,
							'invoice_property'		=> $color,	
							'nr_status'				=> $nr_status,	
							'late'					=> $late,
							'amount_ord'			=> $first_invoice->f('amount'),	
						);
						array_push($result['other_row'], $other_row);
						$total_amount += $first_invoice->f('amount');
						if($in['order_by']!='amount'){
							$j++;
						}
					}
				}else{
					array_push($result['other_row'], $other_row);
					if($in['order_by']!='amount'){
						$j++;
					}
					$total_amount += $invoices->f('amount');
				}
				
			}
			if($in['period_id']=='5' ){
				$result['max_rows']=$j;
			}
			$result['total_amount']= place_currency(display_number($total_amount));
			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order, $offset, $l_r);   
			}
			return $result;
		}

		public function get_managersTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('amount');
			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter = '';
			$order_by = " ORDER BY t_date DESC";
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}
			if($in['manager_id']){
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['c_type']){
					$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
					$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
					$filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
			}
			if($in['source_id']){
				$filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
			}
			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_manager']) || (!is_numeric($in['offset_manager'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_manager']-1;
			}

			if(!empty($in['order_by'])){
			    if(in_array($in['order_by'], $order_by_array)){
					$order = " ASC ";
					if($in['desc'] == '1' || $in['desc']=='true'){
					    $order = " DESC ";
					}
			    }
			}

			$i = 0;
			$big_total = 0;
			$big_quantity = 0;
			$big_invoice = 0;

			$invoice = $this->db->query("SELECT tblinvoice.acc_manager_name,tblinvoice.acc_manager_id,tblinvoice.currency_rate,sum(amount) AS amount,sum(margin) AS total_margin, count(id) AS total_invoice, tblinvoice.contact_id,buyer_name, tblinvoice.buyer_id, tblinvoice.id FROM tblinvoice 
				LEFT JOIN customers ON tblinvoice.buyer_id=customers.customer_id
				WHERE invoice_date BETWEEN '".$time_start."' AND '".$time_end."'  AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='0' OR tblinvoice.type='3')  $filter
				GROUP BY tblinvoice.acc_manager_id ");

			$max_rows=$invoice->records_count();
			$result['max_rows']=$max_rows;
			if($in['order_by']!='amount'){
				$invoice->move_to($offset*$l_r);
			}

			while ($invoice->next() && $i<$l_r){
				$article_amount = $this->db->query("SELECT tblinvoice.acc_manager_id,sum(tblinvoice_line.amount) AS amount,sum(tblinvoice_line.purchase_price * tblinvoice_line.quantity) AS purchase_amount, tblinvoice_line.article_id,tblinvoice.buyer_id,tblinvoice.invoice_date,tblinvoice.sent,tblinvoice.f_archived,tblinvoice.type
				                 FROM tblinvoice_line 
				                 INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                WHERE  tblinvoice.buyer_id= '".$invoice->f('buyer_id')."' AND tblinvoice_line.article_id!=0 and invoice_date BETWEEN '".$time_start."' AND '".$time_end."'  AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='0' OR tblinvoice.type='3')  $filter ");
				$article_amount_c = $this->db->query("SELECT tblinvoice.acc_manager_id,sum(tblinvoice_line.amount) AS amount ,sum(tblinvoice_line.purchase_price * tblinvoice_line.quantity) AS purchase_amount, tblinvoice_line.article_id,tblinvoice.buyer_id,tblinvoice.invoice_date,tblinvoice.sent,tblinvoice.f_archived,tblinvoice.type
				                 FROM tblinvoice_line 
				                 INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
				                 LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
				                WHERE  tblinvoice.buyer_id= '".$invoice->f('buyer_id')."' AND tblinvoice_line.article_id!=0 and invoice_date BETWEEN '".$time_start."' AND '".$time_end."'  AND tblinvoice.sent!='0' AND tblinvoice.f_archived='0'  AND (tblinvoice.type='2' OR tblinvoice.type='4')  $filter ");
			    $big_margin=0;
			    $big_margin_percent=0;
				if($invoice->f('currency_rate')){
					$currency_rate = return_value($invoice->f('currency_rate'));
				 }else{
				   	$currency_rate = 1;
				  }
				   			
				 $big_margin = $invoice->f('total_margin')-$article_amount_c->f('amount') * $currency_rate;
					if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
					    if($article_amount->f('purchase_amount')-$article_amount_c->f('purchase_amount')){
					    	$big_margin_percent=$big_margin*100/($article_amount->f('purchase_amount')-$article_amount_c->f('purchase_amount')) *$currency_rate ;
					    }elseif($big_margin){
					    	$big_margin_percent=100;
					    }
					 }else{
					 	
					    if($article_amount->f('amount')-$article_amount_c->f('amount')){
					    	$big_margin_percent=$big_margin*100/($article_amount->f('amount')-$article_amount_c->f('amount')) *$currency_rate ;
					    }elseif($big_margin){
					    	$big_margin_percent=100;
					    }
					 }
				   // $big_margin_percent = (1-(($invoice->f('amount')-$big_margin)/$invoice->f('amount')))*100;
				

				$other_row=array(
						'MANAGER_N'			=> $invoice->f('acc_manager_name')? $invoice->f('acc_manager_name') : ( $invoice->f('acc_manager_id')==0? gm('No account manager'):get_user_name($invoice->f('acc_manager_id')) ),
						'MANAGER_ID'		=> $invoice->f('acc_manager_id'),
						'CUSTOMER_ID'		=> $invoice->f('buyer_id'),
						'margin'		    => place_currency(display_number($big_margin)),
			            'margin_percent'    => display_number($big_margin_percent),
						'TOTAL_MANAGER'		=> place_currency(display_number($invoice->f('amount'))),					
						'INVOICES'			=> $invoice->f('total_invoice'),
						'amount_ord'		=> $invoice->f('amount'),			
				);	

				if($in['period_id']=='5' ){
					$first_invoice = $this->db->query("SELECT  tblinvoice.*, sum(tblinvoice_line.purchase_price * tblinvoice_line.quantity) AS purchase_amount FROM tblinvoice 
						LEFT JOIN tblinvoice_line ON tblinvoice.id = tblinvoice_line.invoice_id 
						WHERE sent !=0 AND  buyer_id ='".$invoice->f('buyer_id')."' AND tblinvoice.f_archived =0 AND invoice_date !=0 AND (type ='0' OR type='3')
						GROUP BY tblinvoice.id
						ORDER BY tblinvoice.id ASC LIMIT 1");

					$big_margin = 0;
					$big_margin_percent =0;
				    if($first_invoice->f('id')==$invoice->f('id')){
				   		if($first_invoice->f('currency_rate')){
				   			$currency_rate = return_value($first_invoice->f('currency_rate'));
				   		}else{
				   			$currency_rate = 1;
				   		}
				   		$big_margin = $first_invoice->f('margin') * $currency_rate;
				   		if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
						    if($first_invoice->f('purchase_amount')){
						    	$big_margin_percent=$big_margin*100/ $first_invoice->f('purchase_amount') *$currency_rate ;
						    }elseif($big_margin){
						    	$big_margin_percent=100;
						    }
						}else{
							if($first_invoice->f('amount')){
						    	$big_margin_percent=$big_margin*100/ $first_invoice->f('amount') *$currency_rate ;
						    }elseif($big_margin){
						    	$big_margin_percent=100;
						    }
						}
				   		$other_row=array(
				   			'MANAGER_N'			=> $first_invoice->f('acc_manager_name'),
							'CUSTOMER_ID'		=> $first_invoice->f('buyer_id'),
							'margin'		    => place_currency(display_number($big_margin)),
				            'margin_percent'    => display_number($big_margin_percent),
							'TOTAL_MANAGER'		=> place_currency(display_number($first_invoice->f('amount'))),
							'total_manager_value'		=> $first_invoice->f('amount'),				
							'INVOICES'			=> '1',
							'amount_ord'		=> $first_invoice->f('amount'),
						);
						array_push($result['other_row'], $other_row);
						$big_total += $first_invoice->f('amount');
						$big_total_vat += $first_invoice->f('amount_vat');
						$big_invoices++;
						$big_margin_total += $big_margin;
						if($in['order_by']!='amount'){
							$i++;
						}	
					}
				}else{
					array_push($result['other_row'], $other_row);
					$big_total += $invoice->f('amount');
					$big_quantity += $invoice_article;
					$big_invoices += $invoice->f('total_invoices');	
					if($in['order_by']!='amount'){	   
						$i++;
					}
				}	

			}

			$result['BIG_TOTAL']		= place_currency(display_number($big_total));			
			$result['BIG_INVOICES']		= $big_invoices;
			if($in['period_id']=='5' ){
				$result['max_rows']=$i;
				$arr=array();
				$arr = $result['other_row'];
				usort($arr, create_function('$a,$b', 'return $b["total_manager_value"] - $a["total_manager_value"];'));
				$arr = array_slice($arr, 0, 5, true);
				$result['manager_chart']=array('label'=>array(),'data'=>array(),'data_show'=>array());
				foreach($arr as $k=>$v){
					array_push($result['manager_chart']['label'], $v['MANAGER_N']);
					array_push($result['manager_chart']['data'], number_format($v['total_manager_value'],2,'.',''));
					array_push($result['manager_chart']['data_show'], get_currency_sign(display_number($v['total_manager_value'])));
				}
			}

			if($in['order_by']=='amount'){
			    $result['other_row']=order_array_by_column($result['other_row'],$in['order_by'],$order, $offset, $l_r);  
			}

			return $result;
		}

		public function get_customerChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter='';
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';
			}
			if($in['manager_id']){
				//$filter .= ' AND tblinvoice.acc_manager_id ='.$in['m_id'].' ';
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['c_type']){
					$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
					$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
					$filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
			}
			if($in['source_id']){
				$filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
			}

			$invoice = $this->db->query("SELECT sum(amount) AS amount, count(id) AS total_invoices, buyer_name, tblinvoice.buyer_id FROM tblinvoice 
				LEFT JOIN customers ON tblinvoice.buyer_id=customers.customer_id
				WHERE invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.buyer_id !='0' AND tblinvoice.sent!='0'  AND tblinvoice.f_archived='0' AND (tblinvoice.type='0' OR tblinvoice.type='3') $filter
				GROUP BY tblinvoice.buyer_id order by amount DESC limit 5");
			while ($invoice->next()) {
				array_push($result['label'], $invoice->f('buyer_name'));
				array_push($result['data'], number_format($invoice->f('amount'),2,'.',''));
				array_push($result['data_show'], get_currency_sign(display_number($invoice->f('amount'))));
			}

			return $result;
		}

		public function get_managerChart(&$in){
			$result=array('label'=>array(),'data'=>array(),'data_show'=>array());

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter='';
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';
			}
			if($in['manager_id']){
				//$filter .= ' AND tblinvoice.acc_manager_id ='.$in['m_id'].' ';
				$filter .= ' AND tblinvoice.acc_manager_id ='.$in['manager_id'].' ';
			}
			if($in['c_type']){
					$in['c_type_name'] = $this->db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
					$filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
			}
			if(!empty($in['identity_id']) || $in['identity_id']==='0'){
					$filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
			}
			if($in['source_id']){
				$filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
			}
			if($in['type_id']){
				$filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
			}

			$invoice = $this->db->query("SELECT tblinvoice.acc_manager_id,sum(amount) AS amount, count(id) AS total_invoices, tblinvoice.acc_manager_name , tblinvoice.buyer_id FROM tblinvoice 
				LEFT JOIN customers ON tblinvoice.buyer_id=customers.customer_id
				WHERE invoice_date BETWEEN '".$time_start."' AND '".$time_end."' AND tblinvoice.sent!='0'  AND tblinvoice.f_archived='0' AND (tblinvoice.type='0' OR tblinvoice.type='3') $filter
				GROUP BY tblinvoice.acc_manager_id  order by amount DESC limit 5");
			while ($invoice->next()) {

				array_push($result['label'], $invoice->f('acc_manager_name'));
				array_push($result['data'], number_format($invoice->f('amount'),2,'.',''));
				array_push($result['data_show'], get_currency_sign(display_number($invoice->f('amount'))));
			}

			return $result;
		}

	}

	$cash_data = new ReportInvoice($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_Data();
	$cash_data->output();
?>