<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class ReportInvoice extends Controller{
		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}
		public function get_Data(){
			ini_set('memory_limit','2G');
			$in=$this->in;
			$result=array();

			if($in['start_date']){
				$result['start_date']=strtotime($in['start_date'])*1000;
				$now			= strtotime($in['start_date']);
			}else{
				$result['start_date']=time()*1000;
				$now 				= time();
			}
			if(date('I',$now)=='1'){
				$now+=3600;
			}
			$today_of_week 		= date("N", $now);
			$today  			= date('d', $now);
			$month        		= date('n', $now);
			$year         		= date('Y', $now);

			switch ($in['period_id']) {
				case '0':
					//weekly
					$time_start = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
					$time_end = $time_start + 604799;
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end-3600);
					break;
				case '1':
					//daily
					$time_start = mktime(0,0,0,$month,$today,$year);
					$time_end   = mktime(23,59,59,$month,$today,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '2':
					//monthly
					$time_start = mktime(0,0,0,$month,1,$year);
					$time_end   = mktime(23,59,59,$month+1,0,$year);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '3':
					//yearly
					$time_start = mktime(0,0,0,1,1,$year);
					$time_end   = mktime(23,59,59,1,0,$year+1);
					$result['period_txt']=date(ACCOUNT_DATE_FORMAT,$time_start).' - '.date(ACCOUNT_DATE_FORMAT,$time_end);
					break;
				case '4':
					//custom
					if($in['from'] && !$in['to']){
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$time_end = mktime(23,59,59,date('n'),date('d'),date('Y'));
					}else if($in['to'] && !$in['from']){
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_start=946684800;
						$time_end = mktime(23,59,59,$month,$today,$year);
					}else{
						$today  			= date('d',strtotime($in['from']));
						$month        		= date('n', strtotime($in['from']));
						$year         		= date('Y', strtotime($in['from']));
						$time_start = mktime(0,0,0,$month,$today,$year);
						$today  			= date('d',strtotime($in['to']));
						$month        		= date('n', strtotime($in['to']));
						$year         		= date('Y', strtotime($in['to']));
						$time_end = mktime(23,59,59,$month,$today,$year);
					}
					$result['period_txt']='';
					break;
			}
			$in['time_start']=$time_start;
			$in['time_end']=$time_end;
			$filter = ' AND 1=1';
			$result['is_profi']= USE_PROFI ? true : false;
			$result['EXPORT_HREF']= 'index.php?do=report-export_client_history_report&customer_id='.$in['customer_id'].'&time_start='.$time_start.'&time_end='.$time_end.'&m_id='.$in['manager_id'].'&search='.$in['search'];
			$result['adv_search']	= true;

			
			$result['time_start']=$time_start;
			$result['time_end']=$time_end;
			$result['customers'] = $this->get_cc($in);

			$result['invoice_tab']=$this->get_invoicesTab($in);
			
			$result['customer_type']			= getCustomerType();
			$result['identities']				= build_identity_dd();
			$this->out=$result;
		}

		public function get_cc(&$in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" is_admin='0' AND customers.active=1 ";
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			}
			if($in['customer_id']){
				$filter .=" AND customers.customer_id='".$in['customer_id']."' ";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type,cat_id
				FROM customers
				
				WHERE $filter
				GROUP BY customers.customer_id			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array('list'=>array());
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);
				if($value['type']==0){
					$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
				}elseif($value['type']==1){
					$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
				}else{
					$symbol ='';
				}
				$address = $this->db->query("SELECT zip,city,address FROM customer_addresses
										WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");
				$result_line=array(
					"id"					=> $value['cust_id'],
					'symbol'				=> $symbol,
					"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					/*'zip'					=> $value['zip'] ? $value['zip'] : '',
					'city'					=> $value['city'] ? $value['city'] : '',
					"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],*/
					'zip'					=> $address->f('zip') ? $address->f('zip') : '',
					'city'					=> $address->f('city') ? $address->f('city') : '',
					"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
					"right"					=> $value['acc_manager_name'],
					"cat_id"				=> $value['cat_id']
				);
				array_push($result['list'],$result_line);
			}		
			return $result;
		}

			

		public function get_invoicesTab(&$in){
			$result=array('other_row'=>array());
			$order_by_array = array('cname');
			$other_row_all = array();
			if($in['customer_id']){
				$filter .= ' AND tblinvoice.buyer_id ='.$in['customer_id'].' ';			
			}

			$time_start = $in['time_start'];
			$time_end = $in['time_end'];

			$filter .= " AND (tblinvoice_line.tax_id ='0' AND tblinvoice_line.tax_for_article_id ='0') AND tblinvoice_line.content !='1' ";
			$q = strtolower($in["search"]);
					
			if($q){
				$filter .=" AND (tblinvoice_line.name LIKE '%".addslashes($q)."%' OR customers.name LIKE '%".addslashes($q)."%')";
			}/*else{
				return $result;
			}*/

			$order_by = " ORDER BY t_date DESC";
			
			
			
			$l_r =ROW_PER_PAGE;
			$result['lr']=$l_r;

			if((!$in['offset_invoice']) || (!is_numeric($in['offset_invoice'])))
			{
			    $offset=0;
			}
			else
			{
			    $offset=$in['offset_invoice']-1;
			}
/*			switch($in['invoice_tab']){
				case '1':
					$filter.=" AND tblinvoice.sent!='0' AND  tblinvoice.f_archived='0'  AND tblinvoice.type='0' ";
					break;
				case '2':
					$filter.=" AND tblinvoice.sent!='0' AND  tblinvoice.f_archived='0'  AND tblinvoice.type='2' ";
					break;
			}*/

			$filter.=" AND  tblinvoice.f_archived='0' ";

			if(!empty($in['order_by'])){
				
					$order = " ASC ";
					if($in['desc'] == 'true'){
						$order = " DESC ";
					}
				if(in_array($in['order_by'], $order_by_array)){
					$order_by =" ORDER BY ".$in['order_by']." ".$order;
				}
			}
			
			/*$invoices=$this->db->query("SELECT  tblinvoice_line.article_id,tblinvoice_line.price,tblinvoice_line.quantity,tblinvoice_line.item_code, tblinvoice_line.name,tblinvoice_line.discount as line_disc, 
												tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, customers.name AS cname
						FROM tblinvoice_line
						INNER JOIN tblinvoice ON tblinvoice_line.invoice_id=tblinvoice.id
						LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
						WHERE invoice_date BETWEEN '".$time_start."' AND '".$time_end."' ".$filter."  ".$order_by );*/
			$invoices=$this->db->query("SELECT  tblinvoice_line.article_id,tblinvoice_line.price,tblinvoice_line.quantity,tblinvoice_line.item_code, tblinvoice_line.name,tblinvoice_line.discount as line_disc, 
												tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, customers.name AS cname
						FROM tblinvoice_line
						INNER JOIN tblinvoice ON tblinvoice_line.invoice_id=tblinvoice.id
						LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
						WHERE invoice_date BETWEEN '".$time_start."' AND '".$time_end."' ".$filter."  ".$order_by  );
			

			$max_rows=$invoices->records_count();
			$result['max_rows']=$max_rows;
			//$invoices->move_to($offset*$l_r);
			//$j=0;
			$color = '';
			$total_amount = 0;
			$total_margin = 0;

			$all_prices_ar=array();
			$all_prices=$this->db->query("SELECT  article_id, price FROM pim_article_prices						
						WHERE base_price = '1' ")->getAll();
			  foreach($all_prices as $key=>$value){
			    $all_prices_ar[$value['article_id']]=$value['price'];
			  }

			//while($invoices->move_next() && $j<$l_r){
			while($invoices->move_next()){
				$status='';
				$status_title='';
				$type='';
				$type_title='';
				$color='';
				switch ($invoices->f('type')){
					case '0':
						$type = 'regular_invoice';
						$type_title = gm('Regular invoice');
						break;
					case '1':
						$type = 'pro-forma_invoice';
						$type_title = gm('Proforma invoice');
						break;
					case '2':
						$type = 'credit_invoice';
						$type_title = gm('Credit Invoice');
						break;
				}

				switch ($invoices->f('status')){
					case '0':
						if($invoices->f('paid') == '2' ){
							$status='partial';
							$status_title= gm('Partially Paid');
							$color = '';
						}
						if($invoices->f('due_date') < time()){
							$status = 'late';
							$status_title= gm('Late');
							$color = 'late';

						}
						break;
					case '1':
						if($invoices->f('type') != '2' ){
							$color = '';
							$status='paid';
							$status_title= gm('Paid');
						}
						break;
				}
				if($invoices->f('sent') == '0' && $invoices->f('type')!='2'){
					$type_title = gm('Draft');
					$nr_status=1;
			    }else if($invoices->f('status')=='1'){
					$type_title = gm('Paid');
					$nr_status=3;
			    }else if($invoices->f('sent')!='0' && $invoices->f('not_paid')=='0'){
					$type_title = gm('Final');
					$nr_status=2;
			    }

				if($invoices->f('currency_rate')){
					$currency_rate=$invoices->f('currency_rate');
				}else{
					$currency_rate=1;
				}

				$late = false;
			    if($invoices->f('not_paid') == '0' && $invoices->f('status') == '0' && $invoices->f('sent') == '1' && $invoices->f('due_date') < time()){
				$late = true;
			    }

			    $discount_line = $invoices->f('line_disc');
				if($invoices->f('discount') == 0 || $invoices->f('discount') == 2){
					$discount_line = 0;
				}
				//$line_total= $invoices->f('quantity') * ($invoices->f('price') -$invoices->f('price') * $discount_line / 100) *return_value($currency_rate);
				$line_total= ($invoices->f('price') -$invoices->f('price') * $discount_line / 100) *return_value($currency_rate);
				$base_price=0;
				if($invoices->f('article_id')){
					/*$base_price =$this->db->field("SELECT  price FROM pim_article_prices						
						WHERE article_id ='".$invoices->f('article_id')."' AND base_price = '1' ");*/
						$base_price = $all_prices_ar[$invoices->f('article_id')];
				}
				//$base_line= $base_price * $invoices->f('quantity') *return_value($currency_rate);
				$base_line= $base_price * return_value($currency_rate);
				$other_row=array(
					'id'                	=> $invoices->f('id'),
					'serial_number'     	=> $invoices->f('serial_number') ? $ref.$invoices->f('serial_number') : '',
					'created'           	=> date(ACCOUNT_DATE_FORMAT,$invoices->f('invoice_date')),
					'amount'				=> place_currency(display_number($invoices->f('amount'))),
					'vat'					=> place_currency(display_number($invoices->f('amount_vat')-$invoices->f('amount'))),
					'margin'				=> place_currency(display_number($invoices->f('margin')*return_value($currency_rate))),
			        'margin_percent'		=> display_number($invoices->f('margin_percent')),				
					'description'           => $invoices->f('name'),
					'item_code'             => $invoices->f('item_code'),
					'buyer_name'        	=> $invoices->f('cname'),
					'quantity'           	=> $invoices->f('quantity'),
					'line_price'           	=> place_currency(display_number($line_total)),
					'base_price'           	=> place_currency(display_number($base_line)),
					'difference'           	=> place_currency(display_number($line_total-$base_line)),
					'type'					=> $type,
					'type_title'			=> $type_title,
					'status'				=> $status,
					'status_title'			=> $status_title,
					'invoice_property'		=> $color,	
					'nr_status'				=> $nr_status,	
					'late'					=> $late,
					'difference_ord'		=> ($line_total-$base_line),
				);
				array_push($other_row_all, $other_row);
				//$j++;
				$total_amount += $invoices->f('amount');

			}

			$result['total_amount']= place_currency(display_number($total_amount));

			if($in['order_by']=='difference'){

			    if($order ==' ASC '){
			       $exo = array_sort($other_row_all, $in['order_by'].'_ord', SORT_ASC);    
			    }else{
			       $exo = array_sort($other_row_all, $in['order_by'].'_ord', SORT_DESC);
			    }

			    $exo = array_slice( $exo, $offset*$l_r, $l_r);
			    //$result['other_row']=array();
			       foreach ($exo as $key => $value) {
			           array_push($result['other_row'], $value);
			       }
			}else{
				//$result['other_row']=$other_row_all;
				$result['other_row']=array_slice( $other_row_all, $offset*$l_r, $l_r);
			}

			return $result;
		}

	
	}

	$cash_data = new ReportInvoice($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_Data();
	$cash_data->output();
?>