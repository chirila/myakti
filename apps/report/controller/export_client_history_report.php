<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
setcookie('Akti-Export','6',time()+3600,'/');
ini_set('memory_limit','1G');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet(0)->getStyle('A1:I1')->getFont()->setBold(true);
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Orders detail report")
               ->setSubject("Orders detail report")
               ->setDescription("Articles report export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Articles");
$objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);*/

$time_start = $in['time_start'];
$time_end = $in['time_end'];
/*$filename ="client_history_report.xls";*/
$filename ="client_history_report.csv";

if(!$time_start && !$time_end){
  $filter2 = " AND 1=1 ";
}

if($in['m_id']){
  $filter .= "  AND tblinvoice.acc_manager_id=".$in['m_id']." ";
}

if($in['customer_id']){
  $filter .= "  AND tblinvoice.buyer_id=".$in['customer_id']." ";
}

if($in['c_type']){
    $in['c_type_name'] = $db->field("SELECT name FROM customer_type WHERE  id = '".$in['c_type']."'" );
    $filter .= " AND customers.c_type_name LIKE '%".$in['c_type_name']."%'  ";
}
if(!empty($in['identity_id']) || $in['identity_id']==='0'){
    $filter .= ' AND tblinvoice.identity_id ='.$in['identity_id'].'  ';
 }
if($in['source_id']){
    $filter .= ' AND tblinvoice.source_id ='.$in['source_id'].' ';
}
if($in['type_id']){
    $filter .= ' AND tblinvoice.type_id ='.$in['type_id'].' ';
}
if($in['search']){
  $filter .= " AND tblinvoice_line.name LIKE '%".$in['search']."%' ";
}
$filter .= " AND (tblinvoice_line.tax_id ='0' AND tblinvoice_line.tax_for_article_id ='0') AND tblinvoice_line.content !='1' AND tblinvoice.f_archived='0' ";
$order_by = " ORDER BY t_date DESC";

$orders = $db->query("SELECT  tblinvoice_line.article_id,tblinvoice_line.price,tblinvoice_line.quantity,tblinvoice_line.item_code, tblinvoice_line.name,tblinvoice_line.discount as line_disc, 
                        tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, customers.name AS cname
            FROM tblinvoice_line
            INNER JOIN tblinvoice ON tblinvoice_line.invoice_id=tblinvoice.id
            LEFT JOIN customers ON customers.customer_id=tblinvoice.buyer_id
            WHERE tblinvoice.invoice_date BETWEEN '".$time_start."' AND '".$time_end."' ".$filter." ".$order_by);

/*$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Invoice ID"))
      ->setCellValue('B1', gm("Customer Name"))
      ->setCellValue('C1', gm("Invoice Date"))

      ->setCellValue('D1', gm("Article Code"))
      ->setCellValue('E1', gm("Line Description"))
      ->setCellValue('F1', gm("Line Price"))
      ->setCellValue('G1', gm("Base Price"))

      ->setCellValue('H1', gm("Difference"))
      ->setCellValue('I1', gm("Quantity"));*/
$headers    = array(gm("Invoice ID"),
                    gm('Customer Name'),
                    gm('Company id'),
                    gm("Invoice Date"),
                    
                    gm("Article Code"),
                    gm("Line Description"),
                    gm("Line Price"),
                    gm("Base Price"),
                    gm("Difference"),
                    gm("Quantity")
      );

 $i=0;
 $xlsRow = 2;
 $article_total_value = 0;
 $article_total_value_c = 0;
 $taxes_total_value = 0;
 $article_total_quantity = 0;
 $article_tax_total_value = '';
 $final_data=array();

while($orders->next()) {
  $total_value=0;
  $next_is_article=0;
  switch ($orders->f('apply_discount')) {
    case '0'://no discount

      $total_value = $orders->f('quantity') * $orders->f('price');
      $unit_price = $orders->f('price');
      break;
    case '1'://line discount
      $total_value = $orders->f('quantity') * $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) ;
      $unit_price = $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) ;
      break;
    case '2'://global discount
      $a = $orders->f('quantity') * $orders->f('price') ;
      $b = 1 - $orders->f('global_disc')/100;
      $total_value = $a * $b;
      $unit_price = $orders->f('price') * ( 1 - $orders->f('global_disc')/100 ) ;
      break;
    case '3'://line and global discount
      $line_discount = $orders->f('quantity') * $orders->f('price') * ( 1 - $orders->f('line_disc')/100 ) ;
      $total_value = $line_discount * ( 1 - $orders->f('global_disc')/100 );
      $unit_price =  ($orders->f('price') * ( 1 - $orders->f('line_disc')/100 )) * ( 1 - $orders->f('global_disc')/100 );
      break;
    default://no discount
      $total_value = $orders->f('quantity') * $orders->f('price') * ( 1 - 0/100 ) ;
      $unit_price = $orders->f('price') * ( 1 - 0/100 ) ;
      break;
  }
  $currency_rate = 1;
  if($orders->f('currency_rate')){
    $currency_rate = return_value($orders->f('currency_rate'));
  }
  $total_value=round($total_value,2);
  $unit_price = round($unit_price,2);
  $total_value = $total_value*$currency_rate;
  $unit_price = $unit_price*$currency_rate;

  if($orders->f('type') == 2){
     $total_value_c=$total_value;
     $total_value=0;
    $article_total_value_c = $article_total_value_c+$total_value_c;
  }else{
    $article_total_value = $article_total_value+$total_value;
    $total_value_c=0;
  }
 
  
  $article_total_quantity += $orders->f('quantity');

  $discount_line = $orders->f('line_disc');
  if($orders->f('discount') == 0 || $orders->f('discount') == 2){
    $discount_line = 0;
  }
  //$line_total= $orders->f('quantity') * ($orders->f('price') -$orders->f('price') * $discount_line / 100) *return_value($currency_rate);
  //$line_total= $orders->f('quantity') * ($orders->f('price') -$orders->f('price') * $discount_line / 100) *$currency_rate;
  $line_total= ($orders->f('price') -$orders->f('price') * $discount_line / 100) *$currency_rate;
  $base_price=0;
  if($orders->f('article_id')){
    $base_price =$db->field("SELECT  price FROM pim_article_prices            
      WHERE article_id ='".$orders->f('article_id')."' AND base_price = '1' ");
  }
  if($orders->f('currency_rate')){
          $currency_rate=$orders->f('currency_rate');
        }else{
          $currency_rate=1;
        }
  //$base_line= $base_price * $orders->f('quantity') *return_value($currency_rate);
  $base_line= $base_price * return_value($currency_rate);

  if ($db_horus) { 
      if ($orders->f('content')=='0'){//daca nu e linie text
        
       if ($orders->f('article_id')){ //daca nu e articol, taxa nu se afiseaza

                  //verificam daca articolul are taxe pe liniile urmatoare
                  $article_sort_order = $orders->f('sort_order');
                  //$next_is_article=0;
                  $tax_list=''; $taxes_value =0;
                  $i=1;
                  
                  while ($next_is_article!=1){
                   
                      $next_sort_order =$article_sort_order+$i;
                        
                      $taxes = $db->query("SELECT tblinvoice_line.*, tblinvoice.* FROM tblinvoice_line
                                          LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id 
                                          WHERE invoice_id='".$orders->f('invoice_id')."' AND tblinvoice_line.sort_order ='".$next_sort_order."' AND 
                                          tblinvoice_line.content ='0' AND (tblinvoice_line.article_id =  '0' OR ISNULL( tblinvoice_line.article_id )) ");
                 
 /* echo '<br>';     
 echo "SELECT tblinvoice_line.*, tblinvoice.* FROM tblinvoice_line
                                          LEFT JOIN tblinvoice ON tblinvoice_line.invoice_id = tblinvoice.id 
                                          WHERE invoice_id='".$orders->f('invoice_id')."' AND tblinvoice_line.sort_order ='".$next_sort_order."' AND 
                                          tblinvoice_line.content ='0' AND (tblinvoice_line.article_id =  '0' OR ISNULL( tblinvoice_line.article_id )) ";
             */        
                      //verificam daca pe urmatoarea linie exista taxa sau articol
                      
                      if ($taxes->move_next()){

                          $tax_list .= $taxes->f('item_code').', ';


                            switch ($taxes->f('apply_discount')) {
                                    case '0'://no discount

                                        $tax_total_value_q = $taxes->f('quantity') * $taxes->f('price');
                                        $tax_total_value = $taxes->f('price');
                                      break;
                                    case '1'://line discount
                                       $tax_total_value_q = $taxes->f('quantity') * $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                       $tax_total_value = $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                      break;
                                    case '2'://global discount
                                      $a_q = $taxes->f('quantity') * $taxes->f('price') ;
                                      $a = $taxes->f('price') ;
                                      $b = 1 - $taxes->f('global_disc')/100;
                                      $tax_total_value = $a * $b;
                                      break;
                                    case '3'://line and global discount
                                      $tax_line_discount_q = $taxes->f('quantity') * $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                      $tax_line_discount = $taxes->f('price') * ( 1 - $taxes->f('line_disc')/100 ) ;
                                      $tax_total_value = $tax_line_discount * ( 1 - $taxes->f('global_disc')/100 );
                                      break;
                                    default://no discount
                                      $tax_total_value_q = $taxes->f('quantity') * $taxes->f('price') * ( 1 - 0/100 ) ;
                                      $tax_total_value = $taxes->f('price') * ( 1 - 0/100 ) ;
                                      break;
                                  }
                                  $currency_rate = 1;
                                  if($taxes->f('currency_rate')){
                                    $currency_rate = return_value($taxes->f('currency_rate'));
                                  }
                                  $tax_total_value=round($tax_total_value,2);
                                  $tax_total_value = $tax_total_value*$currency_rate;

                                  $tax_total_value_q=round($tax_total_value_q,2);
                                  $tax_total_value_q = $tax_total_value_q*$currency_rate;

                                  if($taxes->f('type') == 2){
                                     $tax_total_value_c=$tax_total_value;
                                     $tax_total_value=0;
                                    $article_tax_total_value_c = $article_tax_total_value_c+$tax_total_value_c;
                                  }else{
                                    $article_tax_total_value = $article_tax_total_value+$tax_total_value;
                                    $tax_total_value_c=0;
                                  }
                   
                                  $taxes_value += $tax_total_value;
                                  $article_total_value += $tax_total_value_q;


                         $i++;

                          }else{
                            $next_is_article=1;                             
                            //pe urmatoarea linie este articol

                          }
              
                  }

             /* $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $orders->f('serial_number'))
              ->setCellValue('B'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')))
              ->setCellValue('C'.$xlsRow, $orders->f('origin_number'))
              ->setCellValue('D'.$xlsRow, $orders->f('article_name'))
              ->setCellValue('E'.$xlsRow, $orders->f('item_code'))
              ->setCellValue('F'.$xlsRow, $orders->f('weight')*$orders->f('quantity'))
              ->setCellValue('G'.$xlsRow, $tax_list)
              ->setCellValue('H'.$xlsRow, $orders->f('quantity'))
              ->setCellValue('I'.$xlsRow, $taxes_value)
              ->setCellValue('J'.$xlsRow, $unit_price)
              ->setCellValue('K'.$xlsRow, ($total_value + $orders->f('quantity')*$taxes_value))
              ->setCellValue('L'.$xlsRow, ($orders->f('vat')*$total_value/100) );    
               $xlsRow++;*/

                $tmp_item=array($orders->f('serial_number'),
                    date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')),
                    $orders->f('origin_number'),
                    stripslashes(preg_replace("/[\n\r]/"," ",$orders->f('article_name'))),
                    $orders->f('item_code'),
                    $orders->f('weight')*$orders->f('quantity'),
                    $tax_list,
                    $orders->f('quantity'),
                    $taxes_value,
                    $unit_price,
                    ($total_value + $orders->f('quantity')*$taxes_value),
                    ($orders->f('vat')*$total_value/100) 
                  );
            array_push($final_data,$tmp_item);
       }
   

    }else{ //daca e linie doar se afiseaza

      /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $orders->f('serial_number'))
              ->setCellValue('B'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')))
              ->setCellValue('C'.$xlsRow, $orders->f('origin_number'))
              ->setCellValue('D'.$xlsRow, $orders->f('article_name'))
              ->setCellValue('E'.$xlsRow, $orders->f('item_code'))
              ->setCellValue('F'.$xlsRow, $orders->f('weight')*$orders->f('quantity'))
              ->setCellValue('G'.$xlsRow, $tax_list)
              ->setCellValue('H'.$xlsRow, $orders->f('quantity'))
              ->setCellValue('I'.$xlsRow, $taxes_value)
              ->setCellValue('J'.$xlsRow, $unit_price)
              ->setCellValue('K'.$xlsRow, ($total_value + $orders->f('quantity')*$taxes_value))
              ->setCellValue('L'.$xlsRow, ($orders->f('vat')*$total_value/100) )     
              ->setCellValue('M'.$xlsRow, ($orders->f('vat')*$total_value_c/100))
              ->setCellValue('N'.$xlsRow, $total_value_c);
                 
             // ->setCellValue('H'.$xlsRow, $orders->f('type') == 2 ? gm('Yes') : gm('No') );
               $xlsRow++;*/

        $tmp_item=array($orders->f('serial_number'),
                    date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')),
                    $orders->f('origin_number'),
                    $orders->f('article_name'),
                    $orders->f('item_code'),
                    $orders->f('weight')*$orders->f('quantity'),
                    $tax_list,
                    $orders->f('quantity'),
                    $taxes_value,
                    $unit_price,
                    ($total_value + $orders->f('quantity')*$taxes_value),
                    ($orders->f('vat')*$total_value/100) ,
                    ($orders->f('vat')*$total_value_c/100),
                    $total_value_c
                  );
            array_push($final_data,$tmp_item);

    }  

  }else{ //if not horus
    if($in['time_start']&&$in['time_end']){
      /*$objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $orders->f('serial_number'))
        ->setCellValue('B'.$xlsRow, $orders->f('buyer_name'))
        ->setCellValue('C'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')))
        ->setCellValue('D'.$xlsRow, $orders->f('item_code'))
        ->setCellValue('E'.$xlsRow, $orders->f('name'))
        ->setCellValue('F'.$xlsRow, display_number($line_total))
        ->setCellValue('G'.$xlsRow, display_number($base_line))
        ->setCellValue('H'.$xlsRow, display_number($line_total-$base_line))
        ->setCellValue('I'.$xlsRow, $orders->f('quantity'));
    
      $xlsRow++; */
      $tmp_item=array(
            $orders->f('serial_number'),
            $orders->f('buyer_name'),
            $orders->f('buyer_id'),
            date(ACCOUNT_DATE_FORMAT,$orders->f('invoice_date')),
            $orders->f('item_code'),
            $orders->f('name'),
            number_format($line_total,2,'.',''),
            number_format($base_line,2,'.',''),
            number_format($line_total-$base_line,2,'.',''),
            number_format($orders->f('quantity'),2,'.','')
            
          );
      array_push($final_data,$tmp_item);
    }

  }

} //end while orders


doQueryLog();
   $from_location='upload/'.DATABASE_NAME.'/export_client_history_report_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();




if ($db_horus){
  $rows_format='F2:N'.$xlsRow;
  $objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');

}else{
  $rows_format='G2:J'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');
}



$objPHPExcel->getActiveSheet()->setTitle('Invoice detail report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// set headers
doQueryLog();
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>