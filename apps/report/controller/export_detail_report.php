<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
setcookie('Akti-Export','6',time()+3600,'/');
ini_set('memory_limit','1G');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Customers report")
							 ->setSubject("Customers report")
							 ->setDescription("Customers export")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Customers");
$time_start = $in['time_start'];
$time_end = $in['time_end'];

$filename ="detail_report.xls";

$filter = '1=1';
$filter_p = ' ';
$filter_e = ' ';
$filter_t = ' ';
$filter_u = ' ';
$filter_b=' ';
$filter_user =' ';
$filter_task = ' ';
$customer_name = 'All customers';
$project_name = 'All projects';
$task_name = 'All tasks';
$staff_name = 'All staff';
$bunit_name= 'All business units';
$filter2 = " AND task_time.date BETWEEN '".$time_start."' AND '".$time_end."' ";

if($in['customer_id'] || $in['customer_id2']){
	$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
	$filter = " task_time.customer_id='".$cid2."' ";
	$project_name = 'All projects';
	$task_name = 'All tasks';
	$staff_name = 'All staff';
	if($in['project_id'] || $in['project_id2']){
		$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
		$filter_p = " AND task_time.project_id='".$pid2."' ";
		$filter_e = " AND projects.project_id='".$pid2."' ";
		$filter_user = " AND project_id='".$pid2."' ";
		$filter_task = " AND project_id='".$pid2."' ";
		$project_name = $db->field("SELECT name FROM projects WHERE customer_id='".$cid2."' AND project_id='".$pid2."' ");
		if($in['task_id'] || $in['task_id2']){
			$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];
			$filter_task = " AND task_id='".$tid2."' ";
			$filter_t = " AND task_time.task_id='".$tid2."' ";
			$task_name = $db->field("SELECT task_name FROM tasks WHERE project_id='".$pid2."' AND task_id='".$tid2."' ");
		}
		if($in['user_id'] || $in['user_id2']){
			$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
			$filter_u = " AND task_time.user_id='".$uid2."' ";
			$filter_user = " AND user_id='".$uid2."' ";
			$staff_name = $db->field("SELECT user_name FROM project_user WHERE project_id='".$pid2."' AND user_id='".$uid2."' ");
		}
	}
	if($in['user_id'] || $in['user_id2']){
		$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
		$filter_user = " AND user_id='".$uid2."' ";
		$filter_u = " AND task_time.user_id='".$uid2."' ";
		$staff_name = $db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' LIMIT 1 ");
	}
	$customer_name = $db->field("SELECT name FROM customers WHERE customer_id='".$cid2."' ");
}
elseif ($in['user_id'] || $in['user_id2']){
	$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
	$filter_user = " AND user_id='".$uid2."' ";
	$filter = " task_time.user_id='".$uid2."' ";
	$staff_name = $db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' ");
	if($in['customer_id'] || $in['customer_id2']){
		$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
		$customer_name = $db->field("SELECT name FROM customers WHERE customer_id='".$cid2."' ");
		$filter .= " AND task_time.customer_id='".$cid2."' ";
	}
	if($in['project_id'] || $in['project_id2']){
		$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
		$filter_task = " AND project_id='".$pid2."' ";
		$filter_user = " AND project_id='".$pid2."' ";
		$customer_name = $db->field("SELECT  projects.company_name FROM projects WHERE projects.project_id='".$pid2."' ");
		$project_name = $db->field("SELECT name FROM projects WHERE project_id='".$pid2."' ");
		$filter .= " AND task_time.project_id='".$pid2."' ";
	}
	if($in['task_id'] || $in['task_id2']){
		$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];
		$filter_task = " AND task_id='".$tid2."' ";
		$task_name = $db->field("SELECT task_name FROM tasks WHERE task_id='".$tid2."' ");
		$customer_name = $db->field("SELECT projects.company_name FROM projects INNER JOIN task_time on projects.project_id=task_time.project_id WHERE task_time.task_id='".$tid2."' ");
		$project_name = $db->field("SELECT name FROM projects INNER JOIN task_time ON projects.project_id=task_time.project_id WHERE task_id='".$tid2."' ");
		$filter .= " AND task_time.task_id='".$tid2."' ";
	}
}
elseif($in['project_id'] || $in['project_id2']){
	$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
	$filter_task = " AND project_id='".$pid2."' ";
	$filter_user = " AND project_id='".$pid2."' ";
	$filter_p = " AND task_time.project_id='".$pid2."' ";
	$filter_e = " AND projects.project_id='".$pid2."' ";
	$project_name = $db->field("SELECT name FROM projects WHERE project_id='".$pid2."' ");
}
elseif ($in['task_id'] || $in['task_id2']){
	$tid2=$in['task_id2'] ? $in['task_id2'] : $in['task_id'];
	$filter_task = " AND task_id='".$tid2."' ";
	$filter_t = " AND task_time.task_id='".$tid2."' ";
	$task_name = $db->field("SELECT task_name FROM tasks WHERE task_id='".$tid2."' ");
}
elseif ($in['user_id'] || $in['user_id2']){
	$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
	$filter_user = " AND user_id='".$uid2."' ";
	$filter = " task_time.user_id='".$uid2."' ";
	$staff_name = $db->field("SELECT user_name FROM project_user WHERE user_id='".$uid2."' ");
}else if($in['bunit_id'] || $in['bunit_id2']){
	$bid2=$in['bunit_id2'] ? $in['bunit_id2'] : $in['bunit_id'];
	$filter_b= " AND projects.bunit='".$bid2."' ";
	$bunit_name=$db->field("SELECT name FROM project_bunit WHERE id='".$bid2."' ");	
}
if($in['internal']=='true'){
	$filter.=" AND projects.invoice_method='0' ";
}
/* optimization */
$data = array();
$limit = 5000;	# suggested number 5000
$loffset = 0;
$exec_loop = false;
do {
	$k = 0;
	$exec_loop = false;
	$query = $db->query("SELECT task_time.user_id, task_time.task_id, task_time.task_time_id, task_time.hours, task_time.notes, task_time.date, task_time.project_status_rate,
						task_time.billed, task_time.billable, projects.name AS p_name, projects.serial_number AS p_serial_number,projects.company_name AS c_name, projects.invoice_method, project_bunit.name as bunit_name,
						project_user.user_name as p_manager,projects.project_id, servicing_support_sheet.notes as notes2
						FROM task_time
						INNER JOIN projects ON task_time.project_id = projects.project_id
						LEFT JOIN servicing_support_sheet ON task_time.task_time_id = servicing_support_sheet.task_time_id AND task_time.task_id = servicing_support_sheet.task_id AND task_time.project_id = servicing_support_sheet.project_id 
						LEFT JOIN project_user ON project_user.project_id = projects.project_id AND project_user.manager=1
						LEFT JOIN project_bunit ON projects.bunit=project_bunit.id
						WHERE $filter $filter2 $filter_p $filter_t $filter_u
						ORDER BY c_name limit ".($loffset*$limit).", ".$limit."  ");
	// task_time.customer_id,
	 // task_time.project_id,
	while ($query->next()) {
		$data[$query->f('date')][$query->f('task_time_id')] = array(
			'project_id'			=> $query->f('project_id'),
			'user_id'				=> $query->f('user_id'),
			'c_name'				=> $query->f('c_name'),
			'p_name'				=> $query->f('p_name'),
			'p_manager'				=> $query->f('p_manager'),
			'p_serial_number'		=> $query->f('p_serial_number'),
			'task_id'				=> $query->f('task_id'),
			'hours'					=> $query->f('hours'),
			'notes'					=> $query->f('notes'),
			'notes2'				=> $query->f('notes2'),
			'bunit'					=> $query->f('bunit_name'),
			'billed'				=> $query->f('billed'),
			'billable'				=> $query->f('billable'),
			'invoice_method'		=> $query->f('invoice_method'),
			'status_rate' 			=> $query->f('project_status_rate'));
		$k++;
	}
	$query = null;
	if($k==$limit){
		$exec_loop = true;
	}else{
		break;
	}
	$loffset++;
} while($exec_loop == true);

$user_name = array();
$u = $db->query("SELECT user_id, user_name FROM project_user WHERE 1=1 $filter_user ");
while ($u->next()) {
	$user_name[$u->f('user_id')] = get_user_name($u->f('user_id'));
}
$u = null;
$manager_name = array();
$u = $db->query("SELECT project_id, user_name, user_id FROM project_user where manager=1");
while ($u->next()) {
	if(!$manager_name[$u->f('project_id')]){
	    $manager_name[$u->f('project_id')] = get_user_name($u->f('user_id'));
    }else{
    	  $manager_name[$u->f('project_id')] = get_user_name($u->f('user_id')).','.$manager_name[$u->f('project_id')];
    }
}


$u = null;
$tasks = array();
$t = $db->query("SELECT task_id, task_name FROM tasks WHERE 1=1 $filter_task ");
while ($t->next()) {
	$tasks[$t->f('task_id')] = $t->f('task_name');
}
$t = null;

//zhis iz where all zhe magic iz
$customer_h = $db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
	INNER JOIN projects ON task_time.project_id = projects.project_id
	WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b");
$customer_h_ub = $db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
	INNER JOIN projects ON task_time.project_id = projects.project_id
	WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND billable='1' AND billed='0' ");
$customer_h_int=$db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
	INNER JOIN projects ON task_time.project_id=projects.project_id
	WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND projects.invoice_method='0' ");
$customer_d = $db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
	INNER JOIN projects ON task_time.project_id = projects.project_id
	WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b");
$customer_d_ub = $db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
	INNER JOIN projects ON task_time.project_id = projects.project_id
	WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND billable='1' AND billed='0' ");
$customer_d_int=$db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
	INNER JOIN projects ON task_time.project_id=projects.project_id
	WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND projects.invoice_method='0' ");
$customer_days = $db->query("SELECT DISTINCT task_time.date FROM task_time 
	INNER JOIN projects ON task_time.project_id = projects.project_id
	WHERE $filter $filter2 $filter_p $filter_t $filter_u $filter_b AND hours!='0' ORDER BY date ASC ");

$objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


$objPHPExcel->getActiveSheet(0)->getStyle('A1:P1')->getFont()->setBold(true);
/*$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Date"))
			->setCellValue('B1', gm("Client"))
			->setCellValue('C1', gm("Project"))
			->setCellValue('D1', gm("Task"))
			->setCellValue('E1', gm("Person"))
			->setCellValue('F1', gm("Business Units"))
			->setCellValue('G1', gm("Comment"))
			->setCellValue('H1', gm("Internal Hours"))
			->setCellValue('I1', gm("Internal Days"))
			->setCellValue('J1', gm("Unbillable Hours"))
			->setCellValue('K1', gm("Unbillable Days"))
			->setCellValue('L1', gm("Billable Hours"))
			->setCellValue('M1', gm("Billable Days"));
*/
	
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Date"))
            ->setCellValue('B1', gm("internal"))
			->setCellValue('C1', gm("Client"))
			->setCellValue('D1', gm("Project number"))
			->setCellValue('E1', gm("Project"))
			->setCellValue('F1', gm("Project Manager"))
			->setCellValue('G1', gm("Task"))
			->setCellValue('H1', gm("Person"))
			->setCellValue('I1', gm("Business Units"))
			->setCellValue('J1', gm("Comment"))
			->setCellValue('K1', gm("Internal Hours"))
			->setCellValue('L1', gm("Internal Days"))
			->setCellValue('M1', gm("Unbillable Hours"))
			->setCellValue('N1', gm("Unbillable Days"))
			->setCellValue('O1', gm("Billable Hours"))
			->setCellValue('P1', gm("Billable Days"));
			

foreach (range('A1', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
        $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
    } 
$i=0;
$xlsRow = 2;
//$total_day_h=0;
//$total_day_h_u = 0;
//$total_day_h_int=0;
$total_day = 0;
$total_days = 0;
$days = array();
while($customer_days->next()) {

	$time = $customer_days->f('date');
	$day_hours = 0;
	$day_days = 0;
	foreach ($data[$time] as $key => $value){
		if(!$value['hours']){
			continue;
		}
		$notes = $value['notes']? $value['notes'] : html_entity_decode(strip_tags($value['notes2']));
		$objPHPExcel->setActiveSheetIndex(0)
	    ->setCellValue('A'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$time))
	    ->setCellValue('B'.$xlsRow, $value['invoice_method']==0 ?gm('yes'):gm('no'))
			->setCellValue('C'.$xlsRow, $value['c_name'])
			->setCellValue('D'.$xlsRow, $value['p_serial_number'])
			->setCellValue('E'.$xlsRow, $value['p_name'])
			->setCellValue('F'.$xlsRow, $manager_name[$value['project_id']])
			->setCellValue('G'.$xlsRow, $tasks[$value['task_id']])
			->setCellValue('H'.$xlsRow, $user_name[$value['user_id']])
			->setCellValue('I'.$xlsRow, $value['bunit'])
			->setCellValue('J'.$xlsRow, $notes);
		if(($value['invoice_method']==1) && ($value['billable']==1)){
			if($value['status_rate']==0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$xlsRow, '')
		  			->setCellValue('L'.$xlsRow, '')
		  			->setCellValue('M'.$xlsRow, '')
		  			->setCellValue('N'.$xlsRow, '')
		  			->setCellValue('O'.$xlsRow, number_as_hour($value['hours']))
		  			->setCellValue('P'.$xlsRow, '');
				$total_day_h += $value['hours'];
				$total_day += $value['hours'];
				$day_hours += $value['hours'];
			}else{
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$xlsRow, '')
		  			->setCellValue('L'.$xlsRow, '')
		  			->setCellValue('M'.$xlsRow, '')
		  			->setCellValue('N'.$xlsRow, '')
		  			->setCellValue('O'.$xlsRow, '')
		  			->setCellValue('P'.$xlsRow, $value['hours']);
		  		$total_day_d += $value['hours'];
				$total_days+= $value['hours'];
				$day_days += $value['hours'];
			}
			
		}else if($value['invoice_method']==0){
			if($value['status_rate']==0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$xlsRow,number_as_hour($value['hours']))
					->setCellValue('L'.$xlsRow, '')
			  		->setCellValue('M'.$xlsRow,number_as_hour($value['hours']))
			  		->setCellValue('N'.$xlsRow, '')
			  		->setCellValue('O'.$xlsRow, '')
		  			->setCellValue('P'.$xlsRow, '');
				$total_day_h_int += $value['hours'];
				$total_day_h_u += $value['hours'];
				$total_day += $value['hours'];
				$day_hours += $value['hours'];
			}else{
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$xlsRow, '')
					->setCellValue('L'.$xlsRow,$value['hours'])
					->setCellValue('M'.$xlsRow, '')
			  		->setCellValue('N'.$xlsRow,$value['hours'])
			  		->setCellValue('O'.$xlsRow, '')
		  			->setCellValue('P'.$xlsRow, '');
			  	$total_day_d_int += $value['hours'];
				$total_day_d_u += $value['hours'];
				$total_days+= $value['hours'];
				$day_days += $value['hours'];
			}		
		}else{
			if($value['status_rate']==0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$xlsRow, '')
					->setCellValue('L'.$xlsRow, '')
					->setCellValue('M'.$xlsRow,number_as_hour($value['hours']))
					->setCellValue('N'.$xlsRow, '')
			  		->setCellValue('O'.$xlsRow, '')
			  		->setCellValue('P'.$xlsRow, '');
				$total_day_h_u += $value['hours'];
				$total_day += $value['hours'];
				$day_hours += $value['hours'];
			}else{
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$xlsRow, '')
					->setCellValue('L'.$xlsRow, '')
					->setCellValue('M'.$xlsRow, '')
					->setCellValue('N'.$xlsRow,$value['hours'])
			  		->setCellValue('O'.$xlsRow, '')
			  		->setCellValue('P'.$xlsRow, '');
			  	$total_day_d_u += $value['hours'];
				$total_days+= $value['hours'];
				$day_days += $value['hours'];
			}			
		}
		$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
  	$xlsRow++;
	}
}
$total_day_d_int=$total_day_d_int?$total_day_d_int:0;
$total_day_d_u=$total_day_d_u?$total_day_d_u:0;
$total_day_d=$total_day_d?$total_day_d:0;
$objPHPExcel->getActiveSheet(0)->mergeCells('A'.$xlsRow.':J'.$xlsRow);
$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':P'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Subtotal'))
				->setCellValue('K'.$xlsRow, number_as_hour($total_day_h_int))
				->setCellValue('L'.$xlsRow, $total_day_d_int)
				->setCellValue('M'.$xlsRow, number_as_hour($total_day_h_u))
				->setCellValue('N'.$xlsRow, $total_day_d_u)
				->setCellValue('O'.$xlsRow, number_as_hour($total_day_h))
				->setCellValue('P'.$xlsRow, $total_day_d);
				$xlsRow++;

$total_days=$total_days?$total_days:0;
$objPHPExcel->getActiveSheet(0)->mergeCells('A'.$xlsRow.':O'.$xlsRow);
$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':P'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Total'))
				->setCellValue('P'.$xlsRow, number_as_hour($total_day).' '.gm('Hours').'/'.$total_days.' '.gm('Days'));


$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:P'.$xlsRow)->applyFromArray($styleArray);

$styleArray = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_MEDIUM
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:P'.$xlsRow)->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('Customers report');

$time_start = $in['time_start'];
$time_end = $in['time_end'];
$is_manager = $db->field("SELECT MAX(manager) FROM project_user WHERE user_id='".$_SESSION['u_id']."'");
if($is_manager == '1' && $_SESSION['access_level'] != 1){
	$filter_m .= " AND  projects.project_id in (SELECT project_id FROM project_user WHERE user_id='".$_SESSION['u_id']."' )";
}
$filter_date = " date BETWEEN '".$time_start."' AND '".$time_end."' ";

$filter = ' AND 1=1 ';

if($in['customer_id'] || $in['customer_id2']){
	$cid2=$in['customer_id2'] ? $in['customer_id2'] : $in['customer_id'];
	$filter .=' AND projects.customer_id='.$cid2.' ';	
}
if($in['project_id'] || $in['project_id2']){
	$pid2=$in['project_id2'] ? $in['project_id2'] : $in['project_id'];
	$filter_p .=' AND projects.project_id='.$pid2.' ';
}
if($in['user_id'] || $in['user_id2']){
	$uid2=$in['user_id2'] ? $in['user_id2'] : $in['user_id'];
	$filter .=' AND project_expenses.user_id='.$uid2.' ';
	$filter_z =' AND project_user.user_id='.$uid2.' ';
	// $filter_u = ' AND projects.user_id='.$in['u_id'].' ';
}
if($in['bunit_id'] || $in['bunit_id2']){
	$bid2=$in['bunit_id2'] ? $in['bunit_id2'] : $in['bunit_id'];
	$filter .=' AND projects.bunit='.$bid2.' ';
}
if($in['internal']=='true'){
	$filter.=" AND projects.invoice_method='0' ";
}

$persons = array();
$person = $db->query("SELECT user_name, user_id FROM project_user INNER JOIN projects ON project_user.project_id=projects.project_id WHERE 1=1 $filter_z");
while($person->next()) {
	$persons[$person->f('user_id')] = $person->f('user_name');
}

$total_amount_p = 0;
$total_amount_unbill = 0;
$total_amount_bill = 0;
$total_amount_internal=0;

$expense = $db->query("SELECT amount ,user_id, unit_price, projects.name AS p_name, expense.name AS e_name, expense.unit AS e_unit,
					projects.project_id AS p_id, project_expenses.expense_id, project_expenses.date AS exp_date,
					project_expenses.billable, projects.active, project_expences.expense_id AS billable_exp_id, projects.invoice_method
FROM project_expenses
INNER JOIN expense ON project_expenses.expense_id = expense.expense_id
INNER JOIN projects ON project_expenses.project_id = projects.project_id
LEFT JOIN project_expences ON ( project_expenses.expense_id = project_expences.expense_id AND project_expenses.project_id = project_expences.project_id )
WHERE $filter_date $filter $filter_e
ORDER BY p_name, exp_date,e_name");

$xlsRow = $xlsRow+3;

$sec_tbl_f_row = $xlsRow;

$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':I'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$xlsRow, gm('Date'))
				->setCellValue('B'.$xlsRow, gm('Project Name'))
				->setCellValue('C'.$xlsRow, gm('Expense Name'))
				->setCellValue('D'.$xlsRow, gm('Person'))
				->setCellValue('E'.$xlsRow, gm('Quantity'))
				->setCellValue('F'.$xlsRow, gm('Unit Price'))
				->setCellValue('G'.$xlsRow, gm('Internal').' '.gm('Amount'))
				->setCellValue('H'.$xlsRow, gm('Unbillable').' '.gm('Amount'))
				->setCellValue('I'.$xlsRow, gm('Billable').' '.gm('Amount'));
$xlsRow++;


while ($expense->next()) {
	$amount = $expense->f('amount');
	if($expense->f('unit_price')){
		$amount = $expense->f('amount')*$expense->f('unit_price');
	}
	$total_amount_p +=$amount;

	$quantity = number_format($expense->f('amount'),2,'.','').' ('.$expense->f('e_unit').')';
	if(!$expense->f('e_unit')){
		$quantity = number_format($expense->f('amount'),2,'.','');
	}
	if($expense->f('unit_price')){
		$unit_price = number_format($expense->f('unit_price'),2,'.','');
	}else{
		$unit_price = ' - ';
	}
	if(!$expense->f('e_unit') && !$expense->f('unit_price')){
		$quantity = ' - ';
		$unit_price = number_format($expense->f('amount'),2,'.','');
	}

	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$xlsRow, date(ACCOUNT_DATE_FORMAT,$expense->f('exp_date')))
				->setCellValue('B'.$xlsRow, $expense->f('p_name'))
				->setCellValue('C'.$xlsRow, $expense->f('e_name'))
				->setCellValue('D'.$xlsRow, $persons[$expense->f('user_id')])
				->setCellValue('E'.$xlsRow, $quantity)
				->setCellValue('F'.$xlsRow, $unit_price);

	if($expense->f('active')==2){
		if(($expense->f('invoice_method')==1) && ($expense->f('billable')==1)){
			//for billable
			$total_amount_bill +=$amount;
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$xlsRow, '')
				->setCellValue('H'.$xlsRow, '')
				->setCellValue('I'.$xlsRow, number_format($amount,2,'.',''));
		}else if($expense->f('invoice_method')==0){
			$total_amount_unbill +=$amount;
			$total_amount_internal+=$amount;
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$xlsRow, number_format($amount,2,'.',''))
				->setCellValue('H'.$xlsRow, number_format($amount,2,'.',''))
				->setCellValue('I'.$xlsRow, '');
		}else{
			//for unbillable
			$total_amount_unbill +=$amount;
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$xlsRow, '')
				->setCellValue('H'.$xlsRow, number_format($amount,2,'.',''))
				->setCellValue('I'.$xlsRow, '');
		}
	}else{//$expense->f('active')==1
		if(($expense->f('invoice_method')==1) && ($expense->f('billable_exp_id'))){
			//for billable
			$total_amount_bill +=$amount;
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$xlsRow, '')
				->setCellValue('H'.$xlsRow, '')
				->setCellValue('I'.$xlsRow, number_format($amount,2,'.',''));
		}else if($expense->f('invoice_method')==0){
			$total_amount_unbill +=$amount;
			$total_amount_internal+=$amount;
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$xlsRow, number_format($amount,2,'.',''))
				->setCellValue('H'.$xlsRow, number_format($amount,2,'.',''))
				->setCellValue('I'.$xlsRow, '');
		}else{
			//for unbillable
			$total_amount_unbill +=$amount;
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$xlsRow, '')
				->setCellValue('H'.$xlsRow, number_format($amount,2,'.',''))
				->setCellValue('I'.$xlsRow, '');
		}
	}
	$xlsRow++;
}

//subtotal
$objPHPExcel->getActiveSheet(0)->mergeCells('A'.$xlsRow.':F'.$xlsRow);
$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':I'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Subtotal'))
	      		->setCellValue('G'.$xlsRow, number_format($total_amount_internal,2,'.',''))
				->setCellValue('H'.$xlsRow, number_format($total_amount_unbill,2,'.',''))
				->setCellValue('I'.$xlsRow, number_format($total_amount_bill,2,'.',''));
				$xlsRow++;

//total
$objPHPExcel->getActiveSheet(0)->mergeCells('A'.$xlsRow.':H'.$xlsRow);
$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':I'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Total'))
				->setCellValue('I'.$xlsRow, number_format($total_amount_p,2,'.',''));

// set column width to auto
foreach(range('A','I') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);

$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A'.$sec_tbl_f_row.':I'.$xlsRow)->applyFromArray($styleArray);

$styleArray = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_MEDIUM
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A'.$sec_tbl_f_row.':I'.$xlsRow)->applyFromArray($styleArray);

$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>