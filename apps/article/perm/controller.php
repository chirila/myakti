<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps, $user_credentials;

perm::controller('all', 'admin',in_array($apps['article'], $user_credentials));
perm::controller('all', 'user',in_array($apps['article'], $user_credentials));

// if(!in_array($apps['article'], $p_access)){
// 	perm::controller(array('settings', 'article', 'aservices','template','import_articles','add'), array('admin','user'),in_array('3', $p_access));
// 	// perm::controller(array('article','template'), 'admin',in_array('3', $p_access));
// 	// perm::controller(array('settings','template'), 'admin',in_array('3', $p_access));

// }

if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
	perm::controller(array('settings'), 'user', false);
}
if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::controller('all', 'admin',false);
	perm::controller('all', 'user',false);
}
