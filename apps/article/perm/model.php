<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps,$user_credentials;
perm::model('all','all', 'admin',in_array($apps['article'], $user_credentials));
perm::model('all','all', 'user',in_array($apps['article'], $user_credentials));
// perm::model('service','all', 'user',in_array('3', $p_access));


if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::model('all','all', 'admin',false);
	perm::model('all','all', 'user',false);
}
