<?php
/**
 * Article class
 *
 * @author      Arkweb SRL
 * @link        https://go.salesassist.eu
 * @copyright   [description]
 * @package 	article
 */
class article
{
	var $pag = 'article';
	var $field_n = 'article_id';

	/**
	 * [article description]
	 * @return [type] [description]
	 */
	function article()
	{
		$this->db = new sqldb;
		$this->db2 = new sqldb;
		$this->db3 = new sqldb;
		$this->db4 = new sqldb;
		$this->db_import = new sqldb;
		$this->db_users = new sqldb;
		
	}

	/**
	 * Add article
	 * @param array $in
	 * @return boolean
	 */
	function add(&$in)
	{
		$in['failure'] = false;
		if(!$in['product_id']){ //is direct save no product involved
			if(!$this->add_validate($in)){
				
				$in['failure'] = true;
				return false;
			}
		}else{
			$in['name']         	= 	$in['article_name'];
			$in['item_code']    	= 	$in['article_number'];
			$in['internal_name']	=	$in['article_internal_name'];
			if(!$this->add_to_product_validate($in)){
				$in['failure'] = true;
				return false;
			}

		}
		$NEXT_AUTOMATIC_ARTICLE_BARCODE = $this->db->field("SELECT value FROM settings WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE'");
		if($in['ean_code'] && $NEXT_AUTOMATIC_ARTICLE_BARCODE){			
			$v=new validation($in);
			$v->field('ean_code','Ean Code','integer:unique[pim_articles.ean_code]');
			if(!$v->run()){
				return false;
			}
		}

		$query_sync=array();
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['sale_unit']=1;
		}
        if($in['article_threshold_value']!=ARTICLE_THRESHOLD_VALUE){
        	$custom_threshold_value=1;
        }else{
        	$custom_threshold_value=0;
        }
        $custom_maxim_threshold_value=$in['article_maximum_stock_treshold'] ? $in['article_maximum_stock_treshold'] : $in['max_stock'];
        	if(!$in['exclude_serial_no']){
        		$in['exclude_serial_no']=0;
        	}
        	if(!$in['exclude_batch_no']){
        		$in['exclude_batch_no']=0;
        	}
		if(!$in['supplier_id']){
           $in['supplier_name']='';
		}
		$in['price_type'] = PRICE_TYPE;
		if($in['price_type']==1){
            $in['use_percent_default']=0;
		}
		if(!$in['packing']){
			$in['packing'] = 1;
		}
		if(!$in['sale_unit']){
			$in['sale_unit'] = 1;
		}

		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
															item_code  							= '".$in['item_code']."',
															ean_code  							= '".$in['ean_code']."',
															hide_stock  						= '".$in['hide_stock']."',
															use_percent_default  				= '".$in['use_percent_default']."',
															supplier_reference      = '".$in['supplier_reference']."',
															vat_id									= '".$in['vat_id']."',
															article_brand_id			= '".$in['article_brand_id']."',
															ledger_account_id			= '".$in['ledger_account_id']."',
															article_category_id			= '".$in['article_category_id']."',
															origin_number   			= '".$in['origin_number']."',
															stock        	    		= '".$in['stock']."',
															max_stock        	    		= '".$custom_maxim_threshold_value."',
															sale_unit        			= '".$in['sale_unit']."',
															internal_name       		= '".$in['internal_name']."',
															packing          			= '".return_value2($in['packing'])."',
															price_type          		= '".$in['price_type']."',
															article_threshold_value    	= '".$in['article_threshold_value']."',
															weight    					= '".$in['weight']."',
															show_img_q								= '".$in['show_img_q']."',
															supplier_id								= '".$in['supplier_id']."',
															supplier_name							= '".$in['supplier_name']."',
															aac_price			        = '".return_value($in['aac_price'])."',
															
															custom_threshold_value    	= '".$custom_threshold_value."',
															created_at='".time()."' ");
		//update next automatic ean code
		if($in['ean_code'] && $NEXT_AUTOMATIC_ARTICLE_BARCODE){
			$this->db->query("UPDATE settings SET value='".($NEXT_AUTOMATIC_ARTICLE_BARCODE+1)."' WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE' ");
		}

		$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
		
       $this->db->query("INSERT INTO dispatch_stock SET  article_id= '".$in['article_id']."',
				                                              stock= '".return_value2($in['stock'])."',
				                                              address_id='".$main_address_id."',
				                                              main_address=1

				");
		/*start for  stock movements*/
		if($in['stock'] && $in['hide_stock']!=1){
        	$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
           //we add the stock in the main location
			

        	$this->db->insert("INSERT INTO stock_movements SET
        													date 						=	'".time()."',
        													created_by 					=	'".$_SESSION['u_id']."',
        													article_id 					=	'".$in['article_id']."',
        													article_name       			= 	'".$in['internal_name']."',
        													item_code  					= 	'".$in['item_code']."',
        													movement_type				=	'1',
        													quantity 					= 	'".$in['stock']."',
        													stock 						=	'0',
        													new_stock 					= 	'".$in['stock']."'
        	");
        }
        /*end for  stock movements*/
		$query_sync[0]="INSERT INTO pim_articles SET
																article_id      				='".$in['article_id']."',
																item_code  							= '".$in['item_code']."',
																ean_code  							= '".$in['ean_code']."',
																hide_stock  							= '".$in['hide_stock']."',
																supplier_reference      = '".$in['supplier_reference']."',
																vat_id									= '".$in['vat_id']."',
																article_brand_id				= '".$in['article_brand_id']."',
																ledger_account_id			= '".$in['ledger_account_id']."',
																origin_number   			 	= '".$in['origin_number']."',
																article_category_id			= '".$in['article_category_id']."',
																stock        	   	      = '".$in['stock']."',
																internal_name       		= '".$in['internal_name']."',
																sale_unit        				= '".$in['sale_unit']."',
																packing         			 	= '".$in['packing']."' ,
                                price_type          		= '".$in['price_type']."' ,
                                article_threshold_value = '".$in['article_threshold_value']."',
                                weight    							= '".$in['weight']."',
                                show_img_q							= '".$in['show_img_q']."',
                                supplier_id								= '".$in['supplier_id']."',
								supplier_name							= '".$in['supplier_name']."',
								aac_price			        = '".return_value($in['aac_price'])."',
                                custom_threshold_value  = '".$custom_threshold_value."' ";
		Sync::start(ark::$model.'-'.ark::$method);
		$active_languages = $this -> db -> query("SELECT * FROM pim_lang GROUP BY lang_id");
		while ($active_languages -> next()) {
			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '" . $in['article_id'] . "',
		                                                                         lang_id                = '" . $active_languages -> f('lang_id') . "',
		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."' ");
		}

		$i=0;
		//***Get Attributes for articles
		$this->db->query("SELECT pim_product_attribute.* ,pim_product_attribute_category.category_id
	        		   FROM pim_product_attribute
	        		   INNER JOIN pim_product_attribute_category ON pim_product_attribute_category.product_attribute_id=pim_product_attribute.product_attribute_id
	        	       WHERE pim_product_attribute.apply_to=2 AND pim_product_attribute_category.category_id='".$in['category_id']."'
	        		   ORDER BY pim_product_attribute.sort_order ASC ,pim_product_attribute.name ASC ");
		while($this->db->move_next())
		{
			$this->attribute[$i]['product_attribute_id']=$this->db->f('product_attribute_id');
			$this->attribute[$i]['name']=$this->db->f('name');
			$this->attribute[$i]['field_name']=$this->db->f('field_name');
			$this->attribute[$i]['type']=$this->db->f('type');
			$this->attribute[$i]['items_per_line_4']=$this->db->f('items_per_line_4');
			$this->attribute[$i]['sort_order']=$this->db->f('sort_order');
			$this->attribute[$i]['searchable']=$this->db->f('searchable');
			$this->attribute[$i]['mandatory']=$this->db->f('mandatory');
			$this->attribute[$i]['filterable']=$this->db->f('filterable');
			$i++;
		}

		$this->db->query("DELETE FROM pim_product_attribute_value WHERE article_id='".$in['article_id']."'");

		if(is_array($this->attribute) && count($this->attribute) > 0){
			foreach($this->attribute as $key => $attribute_array){
				insert_product_attribute_value($in['article_id'], $attribute_array, $in,2);
			}
		}
		//insert the base price
		$this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".return_value($in['purchase_price'])."'
						");


     if($in['price_type']==1){
			$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
				//insert the default price
                 if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }
				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) - ($prices->f('price_value') * return_value($base_price)/ 100);
							}else{ //fix
								$total_price = return_value($base_price) - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) + ($prices->f('price_value') * return_value($base_price) / 100);
							}else{ //fix
								$total_price = return_value($base_price) + $prices->f('price_value');
							}

					break;
				}

					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");



			}
		}else{   //quantity discounts

			$prices = $this->db->query("SELECT * FROM article_price_volume_discount");
				while ($prices->next()){
                     //var price=((percent*base_price)/100)+base_price;
                     
                     $total_price    = return_value($in['price']) - (($prices->f('percent')*return_value($in['price']))/100);

                     // $total_price    = (($prices->f('percent')*return_value($in['price']))/100)+return_value($in['price']);

		             $vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		             $total_with_vat = $total_price+($total_price*$vat_value/100);

                      $this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");


				}


		}

		foreach ($in['tax_id'] as $tax_id=>$is_tax){
			$this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$tax_id."'

				");
		}

		if($in['product_id']){
			$this -> db -> query("INSERT INTO pim_product_article (
	                									product_id,
	                									article_id,
	                									block_discount
	                									)
	                									values (
	                									'" . $in['product_id'] . "',
	                									'" . $in['article_id'] . "',
	                									'" . $in['block_discount_article'] . "'


	                									)
	                			");
		}


		Sync::end($in['article_id'],$query_sync);
		if($in['product_id']){
	        $in['article_name']         = '';
			$in['article_number']    	= '';
			$in['price']    			= '';
			$in['origin_number']    	= '';
			$in['ean_code']    			= '';
			$in['article_internal_name']= '';
			$in['stock']    = '';

		}


		$in['view_add_new_article_box']=0;
		msg::success(gm("Article succefully added."),'success');
		insert_message_log($this->pag,'{l}Article added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['article_id'],false,$_SESSION['u_id']);

		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$dbu = new sqldb($db_config);

		/*$show_info=$dbu->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
															AND name		= 'article-article_show_info'	");*/
		$show_info=$dbu->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
															AND name		= :name	",
														['user_id'=>$_SESSION['u_id'],
														 'name'=>'article-article_show_info']
													);															
		if(!$show_info->move_next()) {
			/*$dbu->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
													name 	= 'article-article_show_info',
													value 	= '1' ");*/
			$dbu->insert("INSERT INTO user_meta SET 	user_id = :user_id,
													name 	= :name,
													value 	= :value ",
												['user_id' => $_SESSION['u_id'],
												 'name'    => 'article-article_show_info',
												 'value'   => '1']
											);													
		} else {
			/*$dbu->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
														  AND name 	 	= 'article-article_show_info' ");*/
			$dbu->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
														  AND name 	 	= :name ",
													['value'=>'1',
													 'user_id'=>$_SESSION['u_id'],
													 'name'=>'article-article_show_info']
												);														  
		}
		update_articles($in['article_id'],DATABASE_NAME);

		$count = $this->db->field("SELECT COUNT(article_id) FROM pim_articles ");
		if($count == 1){
			doManageLog('Created the first article.','',array( array("property"=>'first_module_usage',"value"=>'Article') ));
		}

		return true;
	}

	/**
	 * Validate add article
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in)
	{

		$v = new validation($in);

		if (ark::$method == 'update') {

			$option = ".( article_id != '" . $in['article_id'] . "')";
		} else {
			$option = "";
		}
		//$v -> f('name', 'Name', 'required:unique[pim_articles_lang.name' . $option_lang . ']');
		//  $v -> f('name', 'Name', 'required');
		//$v -> f('category_id', 'Category', 'required');
		$v -> f('item_code', 'Item Code', 'required:unique[pim_articles.item_code' . $option . ']');
		$v -> f('internal_name', 'Internal name', 'required');
		//$v -> f('vat_id', 'Vat Id', 'required[pim_vats.vat_id]');
		//$v -> f('price', 'Price', 'required:numeric');
		//$v -> f('total_price', 'Total Price', 'required:numeric');
   /*
		if(ALLOW_STOCK){
			$v -> f('stock', 'Stock', 'numeric');
            }
		if(ALLOW_ARTICLE_PACKING){
			if(ALLOW_ARTICLE_SALE_UNIT){
				$v -> f('sale_unit', 'Sale Unit', 'required:integer:greater_than[1]');
			}
			$v -> f('packing', 'Packing', 'required:greater_than[1]');
		}
	*/	
		return $v -> run();
	}

	/**
	 * Update_main_details_validate
	 * @param  array $in
	 * @return boolean
	 */
	function update_main_details_validate(&$in)
	{

		$v = new validation($in);



		$option = ".( article_id != '" . $in['article_id'] . "')";

        $v -> f('article_number', 'Item Code', 'required:unique[pim_articles.item_code' . $option . ']');
        $v -> f('article_internal_name', 'Internal name', 'required:unique[pim_articles.internal_name' . $option . ']');
		$v -> f('price', 'Price', 'required:numeric');
			if(ALLOW_STOCK){
			$v -> f('stock', 'Stock', 'numeric');
            }

		if(ALLOW_ARTICLE_PACKING){
			if(ALLOW_ARTICLE_SALE_UNIT){
				$in['sale_unit']=1;

			}
			$in['packing']=1;

		}


	   $in['vat_id'] = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");

	   $vat= $this->db->field("SELECT value FROM vats WHERE vat_id='".$in['vat_id']."'");

	   $in['total_price']= display_number(return_value($in['price'])+(return_value($in['price'])* ($vat/100)));



		return $v -> run();
	}

	/**
	 * add_to_product_validate description
	 * @param array $in
	 */
	function add_to_product_validate(&$in)
	{

		$v = new validation($in);

		if (ark::$method == 'update') {

			$option = ".( article_id != '" . $in['article_id'] . "')";
		} else {
			$option = "";
		}
        $v -> f('article_number', 'Item Code', 'required:unique[pim_articles.item_code' . $option . ']');
        $v -> f('article_internal_name', 'Internal name', 'required:unique[pim_articles.internal_name' . $option . ']');
		$v -> f('price', 'Price', 'required:numeric');

		if(ALLOW_ARTICLE_PACKING){
			if(ALLOW_ARTICLE_SALE_UNIT){
				$in['sale_unit']=1;

			}
			$in['packing']=1;

		}
			$in['vat_id'] = $this->db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");

			$in['total_price']= display_number(return_value($in['price'])+(return_value($in['price'])* (ACCOUNT_VAT/100)));



		return $v -> run();
	}

	/**
	 * Update article
	 * @param  array $in
	 * @return boolean
	 */
	function update(&$in)
	{

		if(!$this->update_validate($in)){
			return false;
		}
		$NEXT_AUTOMATIC_ARTICLE_BARCODE = $this->db->field("SELECT value FROM settings WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE'");
		if($in['ean_code'] && $NEXT_AUTOMATIC_ARTICLE_BARCODE){			
			$v=new validation($in);
			$v->field('ean_code','Ean Code','integer:unique[pim_articles.ean_code' .".( article_id != '" . $in['article_id'] . "')".']');
			if(!$v->run()){
				return false;
			}
		}
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['sale_unit']=1;
		}
		if(CAN_SYNC){

		  Sync::start(ark::$model.'-'.ark::$method);
		  $commands = array();
		}

		$article_vat = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
        $price_type_old=$this->db->field("SELECT pim_articles.price_type FROM pim_articles  WHERE article_id='".$in['article_id']."'");
        /*if($price_type_old==1 || ($price_type_old==2 && $in['price_type']==1) ){
           $this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");

		}*/
		if($price_type_old==1){
        	if($in['price_type']==1){
        		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");
        	}else if($in['price_type']==2){
        		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1' AND price_category_id>'0'");
        	}
		}else if($price_type_old==2){
			if($in['price_type']==1){
        		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");
        	}
		}
		if($price_type_old==1 && $in['price_type']==2){
			$this->db->query("DELETE FROM pim_article_price_category_custom WHERE article_id='".$in['article_id']."'");
		}

		if($in['article_threshold_value']!=ARTICLE_THRESHOLD_VALUE){
        	$custom_threshold_value=1;
        }else{
        	$custom_threshold_value=0;
        }


        if(!$in['supplier_id']){
           $in['supplier_name']='';
		}
		if($in['price_type']==1){
            $in['use_percent_default']=0;
		}
       	$old_price_type=$this->db->field("SELECT pim_articles.price_type  FROM pim_articles WHERE article_id='".$in['article_id']."' ");
       	
       	if(!$in['packing']){
			$in['packing'] = 1;
		}
		if(!$in['sale_unit']){
			$in['sale_unit'] = 1;
		}

		$this->db->query("UPDATE pim_articles SET
									item_code  			= '".$in['item_code']."',
									ean_code  			= '".$in['ean_code']."',
									hide_stock  							= '".$in['hide_stock']."',
										use_percent_default  				= '".$in['use_percent_default']."',
									vat_id				= '".$in['vat_id']."',
									origin_number       = '".$in['origin_number']."',
									supplier_reference          = '".$in['supplier_reference']."',
									article_brand_id    = '".$in['article_brand_id']."',
									ledger_account_id			= '".$in['ledger_account_id']."',
									article_category_id = '".$in['article_category_id']."',
									stock				= '".$in['stock']."',
									max_stock        	    		= '".$in['max_stock']."',
									sale_unit           = '".$in['sale_unit']."',
									internal_name       = '".$in['internal_name']."',
									packing             = '".return_value2($in['packing'])."',
									price_type          = '".$in['price_type']."',
									article_threshold_value    = '".$in['article_threshold_value']."',
									weight    			= '".return_value($in['weight'])."',
									show_img_q								= '".$in['show_img_q']."',
									supplier_id								= '".$in['supplier_id']."',
								    supplier_name							= '".$in['supplier_name']."',
								    aac_price			        = '".return_value($in['aac_price'])."',
									custom_threshold_value    = '".$custom_threshold_value."',
									updated_at='".time()."'

						  WHERE article_id='".$in['article_id']."' ");

         $this->db->query("UPDATE stock_movements SET
 									item_code  			= '".$in['item_code']."',
                                    article_name 	  			= '".$in['internal_name']."'

									WHERE article_id='".$in['article_id']."' ");


	/*	$this -> db -> query("UPDATE pim_articles_lang 	SET

		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."'
		                                                WHERE item_id= '" . $in['article_id'] . "' AND lang_id='" . $in['lang_id'] . "'
		                                                   ");*/
		//array_push($commands,"UPDATE pim_articles SET custom_threshold_value    = '".$custom_threshold_value."',article_threshold_value= '".$in['article_threshold_value']."'   WHERE article_id='".$in['article_id']."'");



		$i=0;
		//***Get Attributes for articles
		$this->db->query("SELECT pim_product_attribute.* ,pim_product_attribute_category.category_id
	        		   FROM pim_product_attribute
	        		   INNER JOIN pim_product_attribute_category ON pim_product_attribute_category.product_attribute_id=pim_product_attribute.product_attribute_id
	        	       WHERE pim_product_attribute.apply_to=2 AND pim_product_attribute_category.category_id='".$in['category_id']."'
	        		   ORDER BY pim_product_attribute.sort_order ASC ,pim_product_attribute.name ASC
	        		       ");
		while($this->db->move_next())
		{
			$this->attribute[$i]['product_attribute_id']=$this->db->f('product_attribute_id');
			$this->attribute[$i]['name']=$this->db->f('name');
			$this->attribute[$i]['field_name']=$this->db->f('field_name');
			$this->attribute[$i]['type']=$this->db->f('type');
			$this->attribute[$i]['items_per_line_4']=$this->db->f('items_per_line_4');
			$this->attribute[$i]['sort_order']=$this->db->f('sort_order');
			$this->attribute[$i]['searchable']=$this->db->f('searchable');
			$this->attribute[$i]['mandatory']=$this->db->f('mandatory');
			$this->attribute[$i]['filterable']=$this->db->f('filterable');
			$i++;
		}

		$this->db->query("delete from pim_product_attribute_value where article_id='".$in['article_id']."'");

		if(is_array($this->attribute) && count($this->attribute) > 0){
			foreach($this->attribute as $key => $attribute_array){
				insert_product_attribute_value($in['article_id'], $attribute_array, $in,2);
			}
		}
		//update base price
		$this->db->query("SELECT * FROM pim_article_prices WHERE base_price=1   AND  article_id='".$in['article_id']."'");
		if($this->db->f('article_id')) {
		$this->db2->query("UPDATE pim_article_prices SET

						price				= '".return_value($in['price'])."',
						total_price			= '".return_value($in['total_price'])."',
						purchase_price 		= '".return_value($in['purchase_price'])."'
						WHERE
						base_price=1   AND  article_id='".$in['article_id']."'
				");
            if (display_number($this->db->f('price'),ARTICLE_PRICE_COMMA_DIGITS) != display_number(return_value($in['price']),ARTICLE_PRICE_COMMA_DIGITS)) {
                insert_message_log('article', '{l}Base price was changed from{endl} '.display_number($this->db->f('price'),ARTICLE_PRICE_COMMA_DIGITS). ' to ' .display_number(return_value($in['price']),ARTICLE_PRICE_COMMA_DIGITS). ' by ' .get_user_name($_SESSION['u_id']), 'article_id', $in['article_id'], false, $_SESSION['u_id']);
            }
            if (display_number($this->db->f('purchase_price'),ARTICLE_PRICE_COMMA_DIGITS) != display_number(return_value($in['purchase_price']),ARTICLE_PRICE_COMMA_DIGITS)) {
                insert_message_log('article', '{l}Purchase price was changed from{endl} '.display_number($this->db->f('purchase_price'),ARTICLE_PRICE_COMMA_DIGITS). ' to ' .display_number(return_value($in['purchase_price']),ARTICLE_PRICE_COMMA_DIGITS). ' by ' .get_user_name($_SESSION['u_id']), 'article_id', $in['article_id'], false, $_SESSION['u_id']);
            }
        }else{
	 	$this->db2->query("INSERT INTO pim_article_prices SET

						price				= '".return_value($in['price'])."',
						total_price			= '".return_value($in['total_price'])."',
						purchase_price 		= '".return_value($in['purchase_price'])."',
						base_price=1,
						article_id='".$in['article_id']."'

				");
	 }



        if($in['price_type']==1){

		$prices = $this->db->query("SELECT * FROM pim_article_price_category");
		while ($prices->next()){
               if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }

            $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
            	              WHERE article_id='".$in['article_id']."' AND category_id='".$prices->f('category_id')."'");
            if($this->db2->move_next()){

            	 if($this->db2->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }

			switch ($this->db2->f('type')) {
				case 1:                  //discount
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) - round(($this->db2->f('price_value') * return_value( $base_price) / 100),2);

				}else{ //fix
					$total_price = return_value($base_price) - $this->db2->f('price_value');
				}

				break;
				case 2:                 //markup margin
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value( $base_price) +  round(($this->db2->f('price_value') * return_value( $base_price) / 100),2);
				}else{ //fix
					$total_price = return_value( $base_price) + $this->db2->f('price_value');
				}

				break;
				case 3:                 //profit margin
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value( $base_price) / (1- round($this->db2->f('price_value') / 100,2));
				}else{ //fix
					//
				}
				break;
				case 4:                 //manually set price

					$total_price =$this->db2->f('price_value');
				
              	break;
					}



              }else{

                      switch ($prices->f('type')) {
				case 1:                  //discount
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value( $base_price) - round(($prices->f('price_value') * return_value( $base_price) / 100),2);

				}else{ //fix
					$total_price = return_value( $base_price) - $prices->f('price_value');
				}

				break;
				case 2:                 //markup margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) +  round(($prices->f('price_value') * return_value($base_price) / 100),2);
				}else{ //fix
					$total_price = return_value($base_price) + $prices->f('price_value');
				}
				break;
				case 3:                 //profit margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) / (1- round($prices->f('price_value') / 100,2));
				}else{ //fix
					//
				}
				break;
				case 4:                 //manually set price
					continue;
				break;
              }
          }

            $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$in['article_id']."',
						price_category_id	= '".$prices->f('category_id')."',
						price				= '".$total_price."',
						total_price			= '".$total_with_vat."'
				");



		}
	}else{  //qunatity order price

	      $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
          $this->db->query("SELECT * FROM  pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price=0");
        while ($this->db->move_next()) {
        	   $total_price = $this->db->f('price');
        	   $total_with_vat = $total_price+($total_price*$vat_value/100);
        	   $this->db2->query("UPDATE pim_article_prices SET

											price           = '".$total_price."',
											total_price     = '".$total_with_vat."'
						  WHERE article_price_id='".$this->db->f('article_price_id')."'

							");
        }

        update_q_price($in['article_id'],$in['price'],$in['vat_id']);
        
        if($old_price_type==1){
             $prices = $this->db->query("SELECT * FROM article_price_volume_discount");
				while ($prices->next()){
                     //var price=((percent*base_price)/100)+base_price;
                     $total_price    = return_value($in['price']) - (($prices->f('percent')*return_value($in['price']))/100);

		             $vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		             $total_with_vat = $total_price+($total_price*$vat_value/100);

                      $this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");


				}



        }

	}
		
	if(CAN_SYNC){
		 $packet = array(
			   'command' => base64_encode(serialize($commands)),
			   'command_id' => 1
		   );
       	  Sync::send(WEB_URL, $packet);

		Sync::end($in['article_id']);
    }

		
		
        msg::success(gm("Article succefully updated."),'success');
        insert_message_log($this->pag,'{l}Article updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);

		update_articles($in['article_id'],DATABASE_NAME);

		return true;
	}


	/**
	 * Update main details for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_main_details(&$in)
	{


		if(!$this->update_main_details_validate($in)){

			return false;
		}


		Sync::start(ark::$model.'-'.ark::$method);
		$article_vat = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");

		$this->db->query("UPDATE pim_articles SET
									item_code  			= '".$in['article_number']."',
									internal_name 	    = '".$in['article_internal_name']."',
									ean_code  			= '".$in['ean_code']."',
									supplier_reference          = '".$in['supplier_reference']."',
									stock  			= '".$in['stock']."',
									origin_number       = '".$in['origin_number']."',
									updated_at='".time()."'

						  WHERE article_id='".$in['article_id']."'");




		// $this->db->query("UPDATE pim_articles_lang SET
		// 						 name  			    = '".$in['article_name']."'

		// 				  WHERE item_id='".$in['article_id']."' ");


		//update base price
		$this->db->query("UPDATE pim_article_prices SET

						price				= '".return_value($in['price'])."',
						total_price			= '".return_value($in['total_price'])."'
						WHERE
						base_price=1   AND  article_id='".$in['article_id']."'
				");




		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' and base_price!=1");
		$prices = $this->db->query("SELECT * FROM pim_article_price_category ");
		while ($prices->next()){

			switch ($prices->f('type')) {
				case 1:                  //discount
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($in['price']) - ($prices->f('price_value') * return_value($in['price']) / 100);
				}else{ //fix
					$total_price = return_value($in['price']) - $prices->f('price_value');
				}

				break;
				case 2:                 //profit margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($in['price']) + ($prices->f('price_value') * return_value($in['price']) / 100);
				}else{ //fix
					$total_price = return_value($in['price']) + $prices->f('price_value');
				}

				break;

			}
			$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$in['article_id']."',
						price_category_id	= '".$prices->f('category_id')."',
						price				= '".$total_price."',
						total_price			= '".$total_with_vat."'
				");



		}


		Sync::end($in['article_id']);
		$in['view_add_new_article_box']=0;

		if($in['product_id']){
	         $in['article_id']    = '';
			$in['article_name']    = '';
			$in['article_number']  = '';
			$in['price']           = '';
			$in['origin_number']   = '';
			$in['ean_code']        = '';
			$in['supplier_reference']        = '';

			$in['stock']        = '';

		}
		update_articles($in['article_id'],DATABASE_NAME);
		 insert_message_log($this->pag,'{l}Article updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);

		msg::$success = "Article succefully updated.";
		return true;
	}



	/**
	 * Update validate
	 * @param  array $in
	 * @return boolean
	 */
	function update_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');

		return $this -> add_validate($in);

	}

	/**
	 * Return main details for articles
	 * @param  array $in
	 * @return array
	 */
	function get_main_details(&$in)
	{

		$this->db->query("SELECT pim_articles.* , pim_article_prices.price, pim_article_prices.price_category_id, pim_articles_lang.name,pim_articles_lang.lang_id
		                  FROM  pim_articles
		                  LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id
		                  INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$_SESSION['lang_id']."'
		                  WHERE pim_articles.article_id='".$in['article_id']."'  AND pim_article_prices.price_category_id=0
		                  ");

		$this->db->move_next();

		$in['article_number'] 			= 	$this->db->f('item_code');
		$in['article_name'] 			= 	$this->db->f('name');
		$in['article_internal_name'] 	= 	$this->db->f('internal_name');
		$in['origin_number'] 			= 	$this->db->f('origin_number');
		$in['ean_code'] 				= 	$this->db->f('ean_code');
		$in['supplier_reference'] 				= 	$this->db->f('supplier_reference');

		$in['stock'] 	= 	$this->db->f('stock');
		$in['price'] 					= 	display_number($this->db->f('price'));





		return true;
	}


	/**
	 * Update stocks for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_stock(&$in)
	{
		

		if(!$this->update_stock_validate($in)){

			return false;
		}

		/*start for  stock movements*/
		$article_data = $this->db->query("SELECT internal_name, item_code, stock FROM pim_articles WHERE article_id = '".$in['article_id']."' ");
		$article_data->next();
		/*end for  stock movements*/


			if($in['inc']){
				$this->db->query("UPDATE pim_articles SET
									 stock  			=  stock +'".return_value2($in['stock'])."'
							  WHERE article_id='".$in['article_id']."'");
	          

				/*start for  stock movements*/
				$new_stock = $article_data->f('stock') + return_value2($in['stock']);
				$this->db->insert("INSERT INTO stock_movements SET
	        													date 						=	'".time()."',
	        													created_by 					=	'".$_SESSION['u_id']."',
	        													article_id 					=	'".$in['article_id']."',
	        													article_name       			= 	'".addslashes($article_data->f('internal_name'))."',
	        													item_code  					= 	'".addslashes($article_data->f('item_code'))."',
	        													movement_type				=	'2',
	        													quantity 					= 	'".return_value2($in['stock'])."',
	        													stock 						=	'".$article_data->f('stock')."',
	        													new_stock 					= 	'".$new_stock."'
	        	");
	        	/*end for  stock movements*/

			}

	        if($in['dec']){
				$this->db->query("UPDATE pim_articles SET
									 stock  			=  stock - '".return_value2($in['stock'])."'
							  WHERE article_id='".$in['article_id']."'");


	             

				/*start for  stock movements*/
				$new_stock = $article_data->f('stock') - return_value2($in['stock']);
				$this->db->insert("INSERT INTO stock_movements SET
	        													date 						=	'".time()."',
	        													created_by 					=	'".$_SESSION['u_id']."',
	        													article_id 					=	'".$in['article_id']."',
	        													article_name       			= 	'".addslashes($article_data->f('internal_name'))."',
	        													item_code  					= 	'".addslashes($article_data->f('item_code'))."',
	        													movement_type				=	'2',
	        													quantity 					= 	'".return_value2($in['stock'])."',
	        													stock 						=	'".$article_data->f('stock')."',
	        													new_stock 					= 	'".$new_stock."'
	        	");
	        	/*end for  stock movements*/
			}

			if($in['method_1']){

				

				$this->db->query("UPDATE pim_articles SET
									 stock  			=  '".return_value2($in['stock'])."'
							  WHERE article_id='".$in['article_id']."'");


	             

				/*start for  stock movements*/
				$new_stock = return_value2($in['stock']);
				$this->db->insert("INSERT INTO stock_movements SET
	        													date 						=	'".time()."',
	        													created_by 					=	'".$_SESSION['u_id']."',
	        													article_id 					=	'".$in['article_id']."',
	        													article_name       			= 	'".addslashes($article_data->f('internal_name'))."',
	        													item_code  					= 	'".addslashes($article_data->f('item_code'))."',
	        													movement_type				=	'2',
	        													quantity 					= 	'".return_value2($in['stock'])."',
	        													stock 						=	'".$article_data->f('stock')."',
	        													new_stock 					= 	'".$new_stock."'
	        	");
	        	/*end for  stock movements*/
			}


        $in['new_stock']=$this->db->field("SELECT pim_articles.stock FROM  pim_articles WHERE article_id='".$in['article_id']."'");

        $in['new_stock'] = remove_zero_decimals($in['new_stock']);

		
       

		//Sync::end($in['article_id']);
         insert_message_log($this->pag,'{l}Stock updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Stock succefully updated."),'success');

		return true;
	}

	/**
	 * Validate data
	 * @param  array $in
	 * @return boolean
	 */
	function update_stock_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');
		return $v -> run();

	}


	/**
	 * Import articles from Excel sheets
	 * @param  array $in
	 * @return boolean
	 */
	function import(&$in){
		global $_FILES;
        $is_xml=false;
		$header_name = array(
			"ITEM CODE",
			"INTERNAL NAME",
			"NAME (EN)",
			"NAME (FR)",
			"NAME (DU)",
			"NAME (GE)",
			"STOCK",
			"ORIGIN NUMBER",
			"EAN CODE",
			"FAMILY",
			"BRAND",
			"PACKING",
			"SALE UNIT",
			"BASE PRICE",
			"PURCHASE PRICE",
			"VAT PERCENT",
			"DESCRIPTION (EN)",
			"DESCRIPTION (FR)",
			"DESCRIPTION (DU)",
			"DESCRIPTION (GE)",
			"BLOCKED",
			"SUPPLIER",
			"SUPPLIER REFERENCE"
		);
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 0);
		if($in['file_type']==3){ #import with .xml file
            $is_xml=true;
           if($in['create_temp_data']){

            $this->db->query("TRUNCATE TABLE import_article_data");

				if(!$this->upload_file($in))
				{
					$in['tab']=0;
					return false;
				}
				$orig_filename=$_FILES['filename']['name'];




				$filename='import_article.xml';
			   //$fp = fopen (INSTALLPATH.UPLOAD_PATH."import_article.csv","r");

				 $xml = simplexml_load_file(INSTALLPATH.UPLOAD_PATH."import_article.xml");


$xml_i=0;
                foreach ($xml->{'article-data'} as $article) {


                 /*      echo $article->{'codeart.'}.'<br>';
                       echo $article->Qtéboîte.'<br>';
                       echo $article->Unitédevente.'<br>';
                       echo $article->Prixdevente1.'<br>';
                       echo $article->{'Coded-art.fourn.'};*/
                      if($article->blocked==1){
                      	$front_active=0;
                      }else{
                      	$front_active=1;
                      }

                          if($article->art_code){
                          $supplier_id=$this->db->field("SELECT customer_id FROM customers WHERE name='".addcslashes($article->supplier,"'")."'");
                          if($supplier_id){
                          	 $supplier_name=addcslashes($article->supplier,"'");
                          }else{
                          	 $supplier_name='';
                          }

                          $this->db->query("INSERT INTO  import_article_data SET

							`item_code` 		='".addcslashes($article->art_code,"\\'")."',
							`internal_name` 	='".addcslashes($article->art_code,"\\'")."',
							`name_en` 			='".addcslashes($article->art_code,"\\'")."',
							`name_fr` 			='".addcslashes($article->art_code,"\\'")."',
							`name_du` 			='".addcslashes($article->art_code,"\\'")."',
							`name_ge` 			='".addcslashes($article->art_code,"\\'")."',
							`sale_unit` 		='".intval($article->sale_unit)."',
							`packing` 			='".format_number_import($article->packing)."',
							`supplier_reference` 			='".addcslashes($article->supplier_reference,"\\'")."',
							`supplier_name` 			='".$supplier_name."',
                            `supplier_id` 			='".$supplier_id."',
							`front_active` 			='".$front_active."',
							`base_price` 		='".format_number_import($article->unit_price)."',
							`purchase_price` 		='".format_number_import($article->purchase_price)."'



						");

                      }

$xml_i++;

                  }

                  $in['count']=$xml_i;

           }


            $count=$in['count'];

		}


		elseif($in['file_type']==1){ #import with .xls file
			if($in['create_temp_data']){


				$this->db->query("TRUNCATE TABLE import_article_data");

				if(!$this->upload_file($in))
				{
					$in['tab']=0;
					return false;
				}
				$orig_filename=$_FILES['filename']['name'];



				$filename='import_article.xls';

				require_once '../libraries/PHPExcel.php';

				$inputFileName = INSTALLPATH.UPLOAD_PATH.$filename;

				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load($inputFileName);
				$objPHPExcel->setActiveSheetIndex(0);
				$objWorksheet = $objPHPExcel->getActiveSheet();
				$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
				$highestColumn = $objWorksheet->getHighestColumn();
				$i=0;
				$x=0;
				$sdsd = 0;
				$nr_rows=0;
				$nr_cols=0;
				$blabla = 0;
				$headers = array();
				$values = array();
				$final_arr = array();
				$row_data = array();
				// $end_time = microtime(true);
				$count = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
				$count_time = microtime(true);
				// $total_count_time = $count_time-$begin_time;
				// $total_time = $end_time-$begin_time;
				// echo 'total_time='.$total_time;
				// echo "<br>";
				// echo 'total_count_time='.$total_count_time;

				foreach ($objWorksheet->getRowIterator() as $row) {
					if($i>$highestRow){
						break;
					}
					$cellIterator = $row->getCellIterator();
					$cellIterator->setIterateOnlyExistingCells(false);
					if($i!=0){
						$nr_rows++;
					}
					$k=0;
					foreach ($cellIterator as $cell) {
						$val = $cell->getValue();
						if($i==0){
							if($val){
								array_push($headers, $val);
								$nr_cols++;
							}else{
								break;
							}
						}else{
							if($k!=$nr_cols){
								$values[$k] = $cell->getValue();
								$k++;
							}else{
								break;
							}
						}
					}
					if($i!=0){
						for ($j=0;$j<$k;$j++){
							$final_arr[$headers[$j]][$x] = $values[$j];

							//create an array containing the row
							$row_data[$j] = $values[$j];
						}
											//we have to format the base price and vat

                      if($row_data[20]==1){
                      	$front_active=0;
                      }else{
                      	$front_active=1;
                      }
                          $supplier_id=$this->db->field("SELECT customer_id FROM customers WHERE name='".addcslashes($row_data[21],"\\'")."'");
						    if($supplier_id){
                          	 $supplier_name=addcslashes($row_data[21],"\\'");
                          }else{
                          	 $supplier_name='';
                          }

						//insert data from array in temportary table
						$this->db->query("INSERT INTO  import_article_data SET
							`item_code` 		='".addcslashes(trim($row_data[0]),"\\'")."',
							`internal_name` 	='".addcslashes($row_data[1],"\\'")."',
							`name_en` 			='".addcslashes($row_data[2],"\\'")."',
							`name_fr` 			='".addcslashes($row_data[3],"\\'")."',
							`name_du` 			='".addcslashes($row_data[4],"\\'")."',
							`name_ge` 			='".addcslashes($row_data[5],"\\'")."',
							`stock` 			='".$row_data[6]."',
							`origin_number` 	='".addcslashes($row_data[7],"\\'")."',
							`ean_code` 			='".addcslashes($row_data[8],"\\'")."',
							`family` 			='".addcslashes($row_data[9],"\\'")."',
							`brand` 			='".addcslashes($row_data[10],"\\'")."',
							`packing` 			='".format_number_import($row_data[11])."',
							`sale_unit` 		='".intval($row_data[12])."',
							`base_price` 		='".format_number_import($row_data[13])."',
							`purchase_price` 	='".format_number_import($row_data[14])."',
							`vat_percent` 		='".format_number_import($row_data[15])."',
							`description_en` 	='".addcslashes($row_data[16],"\\'")."',
							`description_fr` 	='".addcslashes($row_data[17],"\\'")."',
							`description_du` 	='".addcslashes($row_data[18],"\\'")."',
							`description_ge` 	='".addcslashes($row_data[19],"\\'")."',
							`supplier_name` 	='".$supplier_name."',
							`supplier_id` 	='".$supplier_id."',
							`front_active`    	='".$front_active."',
							`supplier_reference` 	='".addcslashes($row_data[22],"\\'")."'
						");

						$x++;
					}
					$i++;
				}
				$heads_count = 0;
				foreach ($header_name as $key => $head) {
					if($final_arr[$head]){
						$heads_count ++;
					}
				}
				if($heads_count!=23){
					msg::$error = gm("The column names are different from the sample file. Please edit your file to match the sample file.");
					echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=5";
          </script>';
					$in['tab']=0;
		         	return false;
				}
				$count;
				$uploaded = 0;
				$width_percent = 100*$uploaded/$count;
				$width_percent_info = round($width_percent,2).'%';
				$width_percent = $width_percent.'%';
				$info_uploaded_articles = gm('Please wait while articles are read from the file');
				echo '
						<table style="width:auto" align="center">
							<tr>
								<td style="text-align:center;">
									<div id="loading_msg" style="font-size: 14px;font-weight: bold;color: #3D4C5C;font-family: Arial,Helvetica,sans-serif,Tahoma;background: white; padding: 7px 15px 7px 15px;z-index: 600; width:100px;margin:0px 185px 0px; 185px;">'.gm("Import").'...</div>
									<p style="font-size:11px;font-family: Arial,Helvetica,sans-serif,Tahoma;">'.$info_uploaded_articles.'</p>
									<div style="width:500px;z-index: 21;">
					      				<div style="height:40px;display:none;background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #B2B2B2;border-radius: 3px 3px 3px 3px;display: inline-block;margin-bottom: 10px;width:100%">
					        				<div style="width:'.$width_percent.'; display: block;background: none repeat scroll 0 0 #477894;border-radius: 2px 2px 2px 2px;height: 40px; float:left;" id="restore_bar"></div>
					      				</div>
				    				</div>

								</td>
							</tr>
						</table>

				<script language="javascript">

				location.replace("index.php?do=article-import_articles-article-import&debug=1&file_type='.$in['file_type'].'&tab=1&filename='.$orig_filename.'&create_temp_data=0&count='.$count.'&uploaded='.$uploaded.'");

				</script>
				';
				exit();
			}

		}else{ #import with .csv file
			if($in['create_temp_data']){

				$this->db->query("TRUNCATE TABLE import_article_data");

				ini_set("auto_detect_line_endings", true);
				//upload file
				if(!$in['offset']){
					$in['offset']=0;
				}
				if( $in['offset']==0){
					if(!$this->upload_file($in))
					{
						$in['tab']=0;
						return false;
					}
					$orig_filename=$_FILES['filename']['name'];
				}else{
					$orig_filename=$in['filename'];
				}
				$date=strtotime('now');

				$fieldlist = array(
				'ITEM_CODE'	    => 0,
				'INTERNAL_NAME'	=> 1,
				'NAME_EN'		=> 2,
				'NAME_FR'		=> 3,
				'NAME_DU'		=> 4,
				'NAME_GE'		=> 5,
				'STOCK'			=> 6,
				'ORIGIN_NUMBER'	=> 7,
				'EAN_CODE'      => 8,
				'FAMILY'		=> 9,
				'BRAND'			=> 10,
				'PACKING'		=> 11,
				'SALE_UNIT'		=> 12,
				"BASE_PRICE"    => 13,
				"PURCHASE_PRICE"=> 14,
				'VAT_VALUE'     => 15,
				'DESCRIPTION_EN'=> 16,
				'DESCRIPTION_FR'=> 17,
				'DESCRIPTION_DU'=> 18,
				'DESCRIPTION_GE'=> 19,
				'BLOCKED' => 20,
				'SUPPLIER' => 21,
				'SUPPLIER_REFERENCE' => 22

				);

				$row = 0;
				$fp = fopen (INSTALLPATH.UPLOAD_PATH."import_article.csv","r");
				$count = count( file( INSTALLPATH.UPLOAD_PATH."import_article.csv" ));

				$added_art_array = array();
				$updated_art_array = array();

				$heads_count = 0;

				while ($data = fgetcsv ($fp, 10000, ';','"','\r')){
					if(($row==0)){	// $row = 0	is the head of the table
						foreach ($header_name as $key => $head) {

							foreach ($data as $key2 => $head2) {
								if($head == $head2){
									$heads_count++;
								}
							}
						}




						if($heads_count!=23){
							msg::$error = gm("The column names are different from the sample file. Please edit your file to match the sample file.");
							echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=5";
          </script>';
							$in['tab']=0;
		     				return false;
						}
						foreach ($header_name as $key => $head) {
							if($data[$key]!=$head){
								msg::$error = gm("The columns order is different from the sample file. Please edit your file to match the sample file.");
								echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=6";
          </script>';
								$in['tab']=0;
		     					return false;
							}
						}
						$row++;
						continue;
					}


                      if($data[$fieldlist['BLOCKED']]==1){
                      	$front_active=0;
                      }else{
                      	$front_active=1;
                      }
                      $supplier_id=$this->db->field("SELECT customer_id FROM customers WHERE name='".addcslashes($data[$fieldlist['SUPPLIER']],"'")."'");
                        if($supplier_id){
                          	 $supplier_name=addcslashes($data[$fieldlist['SUPPLIER']],"'");
                          }else{
                          	 $supplier_name='';
                          }
					$this->db->query("INSERT INTO  import_article_data SET
						`item_code` 		='".addcslashes(trim($data[$fieldlist['ITEM_CODE']]),"\\'")."',
						`internal_name` 	='".addcslashes($data[$fieldlist['INTERNAL_NAME']],"\\'")."',
						`name_en` 			='".addcslashes($data[$fieldlist['NAME_EN']],"\\'")."',
						`name_fr` 			='".addcslashes($data[$fieldlist['NAME_FR']],"\\'")."',
						`name_du` 			='".addcslashes($data[$fieldlist['NAME_DU']],"\\'")."',
						`name_ge` 			='".addcslashes($data[$fieldlist['NAME_GE']],"\\'")."',
						`stock` 			='".$data[$fieldlist['STOCK']]."',
						`origin_number` 	='".addcslashes($data[$fieldlist['ORIGIN_NUMBER']],"\\'")."',
						`ean_code` 			='".addcslashes($data[$fieldlist['EAN_CODE']],"\\'")."',
						`family` 			='".addcslashes($data[$fieldlist['FAMILY']],"\\'")."',
						`brand` 			='".addcslashes($data[$fieldlist['BRAND']],"\\'")."',
						`packing` 			='".format_number_import($data[$fieldlist['PACKING']])."',
						`sale_unit` 		='".intval($data[$fieldlist['SALE_UNIT']])."',
						`base_price` 		='".format_number_import($data[$fieldlist['BASE_PRICE']])."',
						`purchase_price` 	='".format_number_import($data[$fieldlist['PURCHASE_PRICE']])."',
						`vat_percent` 		='".format_number_import($data[$fieldlist['VAT_VALUE']])."',
						`description_en` 	='".addcslashes($data[$fieldlist['DESCRIPTION_EN']],"\\'")."',
						`description_fr` 	='".addcslashes($data[$fieldlist['DESCRIPTION_FR']],"\\'")."',
						`description_du` 	='".addcslashes($data[$fieldlist['DESCRIPTION_DU']],"\\'")."',
						`description_ge` 	='".addcslashes($data[$fieldlist['DESCRIPTION_GE']],"\\'")."' ,
						`front_active` 	    ='".$front_active."',
						`supplier_name` 	='".$supplier_name."' ,
						`supplier_id` 	    ='".$supplier_id."',
						`supplier_reference` 	='".addcslashes($data[$fieldlist['SUPPLIER_REFERENCE']],"\\'")."'
					");
				}
				$count;
				$uploaded = 0;
				$width_percent = 100*$uploaded/$count;
				$width_percent_info = round($width_percent,2).'%';
				$width_percent = $width_percent.'%';
				$info_uploaded_articles = gm('Please wait while articles are read from the file');
				echo '
						<table style="width:auto" align="center">
							<tr>
								<td style="text-align:center;">
									<div id="loading_msg" style="font-size: 14px;font-weight: bold;color: #3D4C5C;font-family: Arial,Helvetica,sans-serif,Tahoma;background: white; padding: 7px 15px 7px 15px;z-index: 600; width:100px;margin:0px 85px 0px; 185px;">'.gm("Import").'...</div>
									<p style="font-size:11px;font-family: Arial,Helvetica,sans-serif,Tahoma;">'.$info_uploaded_articles.'</p>
									<div style="width:500px;z-index: 21;">
					      				<div style="height:40px;display:none;background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #B2B2B2;border-radius: 3px 3px 3px 3px;display: inline-block;margin-bottom: 10px;width:100%">
					        				<div style="width:'.$width_percent.'; display: block;background: none repeat scroll 0 0 #477894;border-radius: 2px 2px 2px 2px;height: 40px; float:left;" id="restore_bar"></div>
					      				</div>
				    				</div>

								</td>
							</tr>
						</table>

				<script language="javascript">

				location.replace("index.php?do=article-import_articles-article-import&debug=1&file_type='.$in['file_type'].'&tab=1&filename='.$orig_filename.'&create_temp_data=0&count='.$count.'&uploaded='.$uploaded.'");

				</script>
				';
				exit();
			}
		}

		//bellow he have the same code for .xls and .csv import

		$added_art_array = array();
		$updated_art_array = array();
		if(!$xml){
		   $orig_filename=$in['filename'];
	     }
      	$row = 1;
      	$rows = 25;

      	//make an array with all vat values to use in every row
      	$all_vats_array=array();
      	$all_vats_data = $this->db->query('SELECT * FROM vats');
      	while($all_vats_data->next()){
      		$all_vats_array[$all_vats_data->f('vat_id')] = $all_vats_data->f('value');
      	}

      	//make an array with all families to use in every row
      	$all_family_array=array();
      	$all_family_data = $this->db->query('SELECT * FROM pim_article_categories');
      	while($all_family_data->next()){
      		$all_family_array[$all_family_data->f('id')] = $all_family_data->f('name');
      	}

      	//make an array with all brands to use in every row
      	$all_brand_array=array();
      	$all_brand_data = $this->db->query('SELECT * FROM pim_article_brands');
      	while($all_brand_data->next()){
      		$all_brand_array[$all_brand_data->f('id')] = $all_brand_data->f('name');
      	}

      	//make an array with all active langs to use in every row
      	$all_languages_array = array();
      	$all_languages_data = $this->db->query("SELECT lang_id FROM pim_lang GROUP BY lang_id");
      	while($all_languages_data->next()){
      		$all_languages_array[$all_languages_data->f('lang_id')] = $all_languages_data->f('lang_id');
      	}

      	//make an array with existing price categories to ue in every row
      	$all_price_categ_array = array();
      	$all_price_categ_data = $this->db->query("SELECT type, price_value, price_value_type, category_id,price_type FROM pim_article_price_category");
      	$m = 1;
      	while($all_price_categ_data->next()){
      		$all_price_categ_array[$m]['category_id'] 		= $all_price_categ_data->f('category_id');
      		$all_price_categ_array[$m]['type'] 				= $all_price_categ_data->f('type');
      		$all_price_categ_array[$m]['price_value'] 		= $all_price_categ_data->f('price_value');
      		$all_price_categ_array[$m]['price_value_type'] 	= $all_price_categ_data->f('price_value_type');
      		$all_price_categ_array[$m]['price_type'] 	= $all_price_categ_data->f('price_type');
      		$m++;
      	}

      	$temp_art_data = $this->db->query("SELECT * FROM import_article_data LIMIT {$rows}");

      	while($temp_art_data->next() && $row<=$rows ){
      		if($temp_art_data->f('item_code')){

      			#vat
      			$vat_percent = $temp_art_data->f('vat_percent');
	            if(!$vat_percent){
					$vat_percent=0;
				}
			    $vat_id = 0;
			    $vat_id = array_search($vat_percent, $all_vats_array); // if value exists returns the key
			    if(!$vat_id){
					$vat_id = $this->db->insert("INSERT INTO vats SET value='".$vat_percent."'");
					$all_vats_array[$vat_id] = $vat_percent;
				}

				#family
				$family_temp = addcslashes($temp_art_data->f('family'),"'");
				if(!$family_temp){
					$family_temp='0';
				}
			    $article_categories_id=0;
			    $article_categories_id = array_search($family_temp, $all_family_array); // if value exists returns the key


			    if(!$article_categories_id){
		    		if($family_temp){
		    			$sort_order = $this->db->field("SELECT COUNT(id) FROM pim_article_categories");
		    			$sort_order++;
		    			$article_categories_id = $this->db->insert("INSERT INTO pim_article_categories SET name='".$family_temp."', sort_order='".$sort_order."' ");
		    			$all_family_array[$article_categories_id] = $family_temp;
		    		}
				}

				#brand
				$brand_temp = addcslashes($temp_art_data->f('brand'),"'");
				if(!$brand_temp){
					$brand_temp='0';
				}
			    $article_brand_id = 0;
			    $article_brand_id = array_search($brand_temp, $all_brand_array); // if value exists returns the key
			    if(!$article_brand_id){
		    		if($brand_temp){
		    			$sort_order = $this->db->field("SELECT COUNT(id) FROM pim_article_brands");
		    			$sort_order++;
						$article_brand_id = $this->db->insert("INSERT INTO pim_article_brands SET name='".$brand_temp."', sort_order='".$sort_order."' ");
						$all_brand_array[$article_brand_id] = $brand_temp;
					}
				}

				#stock
				if(ALLOW_STOCK){

		          		// $stock_packing = addcslashes($temp_art_data->f('packing'),"'");
		          		$stock_packing = 1;
		          		if(!ALLOW_ARTICLE_PACKING){
						$stock_packing = 1;
					}
					// $stock_sale_unit = addcslashes($temp_art_data->f('sale_unit'),"'");
					$stock_sale_unit = 1;
			          		if(!ALLOW_ARTICLE_SALE_UNIT){
						$stock_sale_unit = 1;
					}

						$stock = " stock='".$temp_art_data->f('stock')*$stock_packing/$stock_sale_unit."', ";
				}else{
						$stock = " ";
				}

				#item_code
				$this->db->query("SELECT pim_articles.item_code,pim_articles.article_id FROM pim_articles WHERE item_code='".addcslashes(utf8_encode($temp_art_data->f('item_code')),"\\'")."'");

				if($this->db->move_next()){ #update
					$mod='update';
					$article_id = $this->db->field("SELECT article_id FROM pim_articles WHERE item_code='".addcslashes($temp_art_data->f('item_code'),"\\'" )."'");
					
                   

					$price_type = $this->db->field("SELECT price_type FROM pim_articles WHERE item_code='".addcslashes($temp_art_data->f('item_code'),"\\'" )."'");
					$in['article_id']=$article_id;
				}
				else{ #add
					$mod='add';
				}

				if($mod=='add'){
                    if($is_xml){
                       $xml_vat_id = $this->db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");
		              	$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
						item_code             = 	'".addcslashes($temp_art_data->f('item_code'),"\\'")."',
						vat_id                = 	'".$xml_vat_id."',
						internal_name         = 	'".addcslashes($temp_art_data->f('internal_name'),"\\'")."',
						price_type 			  = 	'".PRICE_TYPE."',
						`sale_unit` 		  =  '".addcslashes($temp_art_data->f('sale_unit'),"\\'")."',
						`packing` 			  =  '".addcslashes($temp_art_data->f('packing'),"\\'")."',
						front_active             =  '".addcslashes($temp_art_data->f('front_active'),"\\'")."',
						`supplier_reference`  =  '".addcslashes($temp_art_data->f('supplier_reference'),"\\'")."',
						`supplier_name`  =  '".addcslashes($temp_art_data->f('supplier_name'),"\\'")."',
						`supplier_id`  =  '".addcslashes($temp_art_data->f('supplier_id'),"\\'")."'

					");


                    } else{
					//add product item
					$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
						item_code             = 	'".addcslashes($temp_art_data->f('item_code'),"\\'")."',
						ean_code              = 	'".addcslashes($temp_art_data->f('ean_code'),"\\'")."',
						vat_id                = 	'".$vat_id."',
						article_category_id   = 	'".$article_categories_id."',
						article_brand_id   	  = 	'".$article_brand_id."',
						packing         	  = 	'".addcslashes($temp_art_data->f('packing'),"\\'")."',
						sale_unit         	  = 	'".addcslashes($temp_art_data->f('sale_unit'),"\\'")."',
						front_active             =  '".addcslashes($temp_art_data->f('front_active'),"\\'")."',
						origin_number         = 	'".addcslashes($temp_art_data->f('origin_number'),"\\'")."',
						".$stock."
						internal_name         = 	'".addcslashes($temp_art_data->f('internal_name'),"\\'")."',
						price_type 			  = 	'".PRICE_TYPE."',
						`supplier_name`  =  '".addcslashes($temp_art_data->f('supplier_name'),"\\'")."',
						`supplier_id`  =  '".addcslashes($temp_art_data->f('supplier_id'),"\\'")."',
						supplier_reference              = 	'".addcslashes($temp_art_data->f('supplier_reference'),"\\'")."'

					");
				   }

					/*start for  stock movements*/
					if(ALLOW_STOCK && $temp_art_data->f('item_code')){
			        	if(!$is_xml){
			        		$main_address_id= $this->db->field("SELECT  dispatch_stock_address.address_id
			        	   	                                     FROM   dispatch_stock_address
			        	   	                                    WHERE dispatch_stock_address.is_default=1
			        	   	                               ");
			        	$this->db->insert("INSERT INTO dispatch_stock SET stock='".($temp_art_data->f('stock')*$stock_packing/$stock_sale_unit)."' ,article_id='".$in['article_id']."' , main_address='1',address_id='".$main_address_id."'");

			        	$this->db->insert(" INSERT INTO stock_movements SET
			        													date 						=	'".time()."',
			        													created_by 					=	'".$_SESSION['u_id']."',
			        													article_id 					=	'".$in['article_id']."',
			        													article_name       			= 	'".addcslashes($temp_art_data->f('internal_name'),"\\'")."',
			        													item_code  					= 	'".addcslashes($temp_art_data->f('item_code'),"\\'")."',
			        													movement_type				=	'1',
			        													quantity 					= 	'".addcslashes($temp_art_data->f('stock'),"\\'")*$stock_packing/$stock_sale_unit."',
			        													stock 						=	'0',
			        													new_stock 					= 	'".$temp_art_data->f('stock')*$stock_packing/$stock_sale_unit."'
			        	");
			        	}
			        }
			        /*end for  stock movements*/

					$added_art_array[] = $in['article_id'];
					$in['added']++;

				}else{

					/*start for  stock movements*/
					if(ALLOW_STOCK && $temp_art_data->f('stock')){
						$old_stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id = '".$article_id."' ");
					}
					/*end for  stock movements*/


                    if($is_xml){
                    	$this->db->query("UPDATE pim_articles SET
                              item_code             = 	'".addcslashes($temp_art_data->f('item_code'),"'")."',
						     `sale_unit` 		  =    '".addcslashes($temp_art_data->f('sale_unit'),"'")."',
						     `packing` 			  =   '".addcslashes($temp_art_data->f('packing'),"'")."',
						     `supplier_reference`  =  '".addcslashes($temp_art_data->f('supplier_reference'),"'")."' ,
						     front_active             =  '".addcslashes($temp_art_data->f('front_active'),"'")."',
						     	`supplier_name`  =  '".addcslashes($temp_art_data->f('supplier_name'),"'")."',
						     `supplier_id`  =  '".addcslashes($temp_art_data->f('supplier_id'),"'")."'




						WHERE article_id='".$article_id."' ");

                    } else{
					
					$this->db->query("UPDATE pim_articles SET

						item_code             = 	'".addcslashes($temp_art_data->f('item_code'),"\\'")."',
						ean_code              = 	'".addcslashes($temp_art_data->f('ean_code'),"\\'")."',
						vat_id                = 	'".$vat_id."',
						article_category_id   = 	'".$article_categories_id."',
						article_brand_id   	  = 	'".$article_brand_id."',
						  `sale_unit` 		  =    '".addcslashes($temp_art_data->f('sale_unit'),"\\'")."',
						     `packing` 			  =   '".addcslashes($temp_art_data->f('packing'),"\\'")."',
						origin_number         = 	'".addcslashes($temp_art_data->f('origin_number'),"\\'")."',
						front_active             =  '".addcslashes($temp_art_data->f('front_active'),"\\'")."',
						".$stock."
						internal_name         = 	'".addcslashes($temp_art_data->f('internal_name'),"\\'")."',
						 	`supplier_name`  =  '".addcslashes($temp_art_data->f('supplier_name'),"\\'")."',
						     `supplier_id`  =  '".addcslashes($temp_art_data->f('supplier_id'),"\\'")."',
							`supplier_reference`  =  '".addcslashes($temp_art_data->f('supplier_reference'),"\\'")."'

						WHERE article_id='".$article_id."' ");

				     }

					/*start for  stock movements*/
					if(ALLOW_STOCK && $temp_art_data->f('stock')){

			        	if(!$is_xml){
			        		$main_address_id= $this->db->field("SELECT  dispatch_stock_address.address_id
			        	   	                                     FROM   dispatch_stock_address
			        	   	                                    WHERE dispatch_stock_address.is_default=1
			        	   	                               ");

			        	   $this->db->query("SELECT dispatch_stock.stock ,dispatch_stock.address_id
			        	   	                                FROM  dispatch_stock
			        	   	                                INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id and dispatch_stock_address.is_default=1
			        	   	                                WHERE dispatch_stock.article_id='".$in['article_id']."'");

                        if($this->db->move_next()){

                                $stock_in_main=$this->db->f('stock');


                                 $stock_diff=($temp_art_data->f('stock')*$stock_packing/$stock_sale_unit)-$old_stock;

                               if($stock_diff>0 ){
                                    $this->db2->query("UPDATE dispatch_stock SET stock='".($stock_in_main+$stock_diff)."' WHERE dispatch_stock.article_id='".$in['article_id']."' and address_id='".$main_address_id."'");
                               }elseif($stock_in_main>= abs($stock_diff) ) {
                                    $this->db2->query("UPDATE dispatch_stock SET stock='".($stock_in_main-abs($stock_diff))."' WHERE dispatch_stock.article_id='".$in['article_id']."' and address_id='".$main_address_id."'");
                               }else{
                        	        $this->db2->query("UPDATE dispatch_stock SET stock='".($temp_art_data->f('stock')*$stock_packing/$stock_sale_unit)."' WHERE dispatch_stock.article_id='".$in['article_id']."' and address_id='".$main_address_id."'");
                                    $this->db2->query("UPDATE dispatch_stock SET stock='0' WHERE dispatch_stock.article_id='".$in['article_id']."' and address_id!='".$main_address_id."'");
                                  }
			        	 }

			        	$this->db->insert("INSERT INTO stock_movements SET
			        													date 						=	'".time()."',
			        													created_by 					=	'".$_SESSION['u_id']."',
			        													article_id 					=	'".$in['article_id']."',
			        													article_name       			= 	'".addcslashes($temp_art_data->f('internal_name'),"'")."',
			        													item_code  					= 	'".addcslashes($temp_art_data->f('item_code'),"'")."',
			        													movement_type				=	'2',
			        													quantity 					= 	'".$temp_art_data->f('stock')*$stock_packing/$stock_sale_unit."',
			        													stock 						=	'".$old_stock."',
			        													new_stock 					= 	'".$temp_art_data->f('stock')*$stock_packing/$stock_sale_unit."'
			        	");
			        	}
			        }
			        /*end for  stock movements*/

					$updated_art_array[] = $article_id;
					$in['updated']++;
                    if(!$is_xml){
					   $this->db->query("DELETE FROM pim_articles_lang WHERE item_id= '".$article_id."'");
				     }

				}
				//insert name and description in all active languages
				if(($is_xml && $mod=='add') || !$is_xml) {



				  foreach ($all_languages_array as $key => $lang_id) {
					switch ($lang_id){
						case 1:
							$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
									item_id        = '" .$in['article_id']. "',
									lang_id        = '" .$lang_id. "',
									name           = '".addcslashes($temp_art_data->f('name_en'),"\\'")."',
									description    = '".addcslashes($temp_art_data->f('description_en'),"\\'")."'
							");
						break;
						case 2:
							$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
									item_id        = '" .$in['article_id']. "',
									lang_id        = '" .$lang_id. "',
									name           = '".addcslashes($temp_art_data->f('name_fr'),"\\'")."',
									description    = '".addcslashes($temp_art_data->f('description_fr'),"\\'")."'
							");
						break;
						case 3:
							$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
									item_id        = '" .$in['article_id']. "',
									lang_id        = '" .$lang_id. "',
									name           = '".addcslashes($temp_art_data->f('name_du'),"\\'")."',
									description    = '".addcslashes($temp_art_data->f('description_du'),"\\'")."'
							");
						break;
						case 4:
							$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
									item_id        = '" .$in['article_id']. "',
									lang_id        = '" .$lang_id. "',
									name           = '".addcslashes($temp_art_data->f('name_ge'),"\\'")."',
									description    = '".addcslashes($temp_art_data->f('description_ge'),"\\'")."'
							");
						break;
					}
				  }
			   }


				$b_price = $temp_art_data->f('base_price');




				$v_price = $temp_art_data->f('vat_percent');
				$purchase_price = $temp_art_data->f('purchase_price');


				if(is_numeric($temp_art_data->f('base_price'))){
					$b_price = $temp_art_data->f('base_price');
				}
				if(is_numeric($temp_art_data->f('vat_percent'))){
					$v_price = $temp_art_data->f('vat_percent');
				}

                if($is_xml && $mod=='add'){
				   $v_price = ACCOUNT_VAT;
			     }elseif($is_xml){
				   $xml_vat_id= $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
				   $v_price = $this->db->field("SELECT value FROM vats WHERE vat_id='".$xml_vat_id."'");
			     }

                   $total_price=$b_price +($b_price* $v_price/100) ;

				if($mod=='add'){
                    if($is_xml){


                     $this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$in['article_id']."',
						price_category_id	= '0',
						price			    = '".$b_price."',
						total_price		    = '".$total_price."',
						base_price	        = '1',
						default_price		= '0',
						purchase_price 		= '".$purchase_price."'

					");

                  }else{

                  	//insert the base price
					$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$in['article_id']."',
						price_category_id	= '0',
						price			    = '".$b_price."',
						total_price		    = '".$total_price."',
						base_price	        = '1',
						default_price		= '0',
						purchase_price 		= '".$purchase_price."'
					");
				   }

				}else{
                   if(WAC){
                 $this->db->query("UPDATE pim_article_prices SET
						price				= '".$b_price."',
						total_price			= '".$total_price."'
						
						WHERE
						base_price=1   AND  article_id='".$article_id."'
					");
                   }else{
					//update base price
					$this->db->query("UPDATE pim_article_prices SET
						price				= '".$b_price."',
						total_price			= '".$total_price."',
						purchase_price 		= '".$purchase_price."'
						WHERE
						base_price=1   AND  article_id='".$article_id."'
					");
				  }

				}



				if($article_id){
					if($price_type==1){
				   		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$article_id."' and base_price!=1 AND price_category_id!=0");
					}else{
                            if($is_xml){

                                update_q_price($article_id,display_number($b_price),$xml_vat_id);
                            }else{
					 	      update_q_price($article_id,display_number($b_price),$vat_id);
					         }
					}
				}elseif(PRICE_TYPE==2){
                    $prices = $this->db3->query("SELECT * FROM article_price_volume_discount");
				      while ($prices->next()) {

                     $total_price    = (($prices->f('percent')*$b_price)/100)+$b_price;


		             $total_with_vat = $total_price+($total_price*$v_price/100);

                      $this->db4->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");


			    	}

				}



		      	foreach ($all_price_categ_array as $key => $price_categ_row) {
		      		// echo $b_price;
		      		// echo "<br>";
                    $this->db3->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
            	              WHERE article_id='".$article_id."' AND category_id='".$price_categ_row['category_id']."'");
                     if($this->db3->move_next()) {
                     	   $price_categ_row['price_type']= $this->db3->f('price_type');
                     	   $price_categ_row['type']= $this->db3->f('type');
                            $price_categ_row['price_value_type']= $this->db3->f('price_value_type');
                            $price_categ_row['price_value']= $this->db3->f('price_value');
                     }

                     if($price_categ_row['price_type']==2){
                             $b_price= $purchase_price;
                     }else{
                     	$b_price= $temp_art_data->f('base_price');
                     }


		      		switch ($price_categ_row['type']){
						case 1:                  //discount
						if($price_categ_row['price_value_type']==1){  // %
							// echo "1 - %"; echo "<br>";
							// print_r($price_categ_row['price_value']); echo "<br>";
							$total_price = $b_price - ($price_categ_row['price_value'] * $b_price/ 100);
						}else{ //fix
							$total_price = $b_price - $price_categ_row['price_value'];
							// echo "1 - fix"; echo "<br>";
							// print_r($price_categ_row['price_value']); echo "<br>";
						}
						break;

						case 2:                 //profit margin
						if($price_categ_row['price_value_type']==1){  // %
							$total_price = $b_price + ($price_categ_row['price_value'] * $b_price / 100);
							// echo "2 - %"; echo "<br>";
							// print_r($price_categ_row['price_value']); echo "<br>";
						}else{ //fix
							$total_price = $b_price + $price_categ_row['price_value'];
							// echo "2 - fix"; echo "<br>";
							// print_r($price_categ_row['price_value']); echo "<br>";
						}
						break;

					}

					$vat_value   = $v_price;
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db2->query("DELETE FROM pim_article_prices WHERE article_id='".$article_id."' and base_price!=1 AND price_category_id=0 AND from_q=0");
                    $this->db2->query("SELECT pim_article_prices.*  FROM pim_article_prices WHERE article_id='".$article_id."' and base_price!=1 AND price_category_id=0 AND from_q!=0");
					// print_r($total_price);
					// echo "<br>";
					// print_r($total_with_vat);
					// echo "<br>";
					// if(!$this->db2->move_next()){
						if($mod=='add'){
							if(PRICE_TYPE==1){
							// echo "a";
							// echo "<br>";
								$this->db->query("INSERT INTO pim_article_prices SET
								article_id			= '".$in['article_id']."',
								price_category_id	= '".$price_categ_row['category_id']."',
								price				= '".$total_price."',
								total_price			= '".$total_with_vat."'
								");
							}
						}else{
							// echo "b";
							// echo "<br>";
							if($price_type==1){
								$this->db->query("INSERT INTO pim_article_prices SET
								article_id			= '".$in['article_id']."',
								price_category_id	= '".$price_categ_row['category_id']."',
								price				= '".$total_price."',
								total_price			= '".$total_with_vat."'
								");
							}
						}
					// }
					// echo "<br>";
					// echo "<br>";
		      	}

		      	// exit();
      		} #end if item_code
      		update_articles($in['article_id'],DATABASE_NAME);
      		$delete_temp_row = $this->db->query("DELETE FROM import_article_data WHERE data_id = '".$temp_art_data->f('data_id')."' ");
      		$uploaded = $row;
      		$row++;
      	} #end while

      	$article_added=$in['added'];
		$article_updated=$in['updated'];
		//$user_info = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_info = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		$user_info->next();

		if($row>$rows){

			if(!$in['export_time']){
				$in['export_time'] = strtotime('now');
				$this->db->query("INSERT INTO pim_article_import_log SET
						   date			    = '".$in['export_time']."',
						   filename              = '".$orig_filename."',
						   article_added         = '".$article_added."',
						   article_updated       = '".$article_updated."',
						   article_id_added      = '".serialize($added_art_array)."',
						   article_id_updated    = '".serialize($updated_art_array)."',
						   username='".$user_info->f('first_name')." ".$user_info->f('last_name')."'");
				
			}else{
				$articles_added = $this->db->field("SELECT article_id_added FROM pim_article_import_log WHERE date='".$in['export_time']."' ");
				$articles_updated = $this->db->field("SELECT article_id_updated FROM pim_article_import_log WHERE date='".$in['export_time']."' ");
				$added_ser_articles= unserialize($articles_added);
				$updated_ser_articles= unserialize($articles_updated);
				$all_added_articles = array_merge($added_ser_articles,$added_art_array);
				$all_updated_articles = array_merge($updated_ser_articles,$updated_art_array);
				$this->db->query("UPDATE pim_article_import_log SET
						   filename              = '".$orig_filename."',
						   article_added         = '".$article_added."',
						   article_updated       = '".$article_updated."',
						   article_id_added      = '".serialize($all_added_articles)."',
						   article_id_updated    = '".serialize($all_updated_articles)."',
						   username 			 ='".$user_info->f('first_name')." ".$user_info->f('last_name')."'
						   WHERE date			 = '".$in['export_time']."'
				");
			}

			$count=$in['count'];

			$uploaded = $in['uploaded']+$uploaded;


			$width_percent = 100*$uploaded/$count;

			$width_percent_info = round($width_percent,2).'%';
			$width_percent = $width_percent.'%';

			$info_uploaded_articles = $uploaded.' '.gm('articles uploaded').' ('.$width_percent_info.')';
			echo '
					<table style="width:auto" align="center">
						<tr>
							<td style="text-align:center;">
								<div id="loading_msg" style="font-size: 14px;font-family: Arial,Helvetica,sans-serif,Tahoma;font-weight: bold;color: #3D4C5C;background: white; padding: 7px 15px 7px 15px;z-index: 600; width:100px;margin:0px 185px 0px; 185px;">'.gm("Import").'...</div>
								<p style="font-size:11px;font-family: Arial,Helvetica,sans-serif,Tahoma;">'.$info_uploaded_articles.'</p>
								<div style="width:500px;z-index: 21;">
				      				<div style="height:40px;display:none;background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #B2B2B2;border-radius: 3px 3px 3px 3px;display: inline-block;margin-bottom: 10px;width:100%">
				        				<div style="width:'.$width_percent.'; display: block;background: none repeat scroll 0 0 #477894;border-radius: 2px 2px 2px 2px;height: 40px; float:left;" id="restore_bar"></div>
				      				</div>
			    				</div>

							</td>
						</tr>
					</table>

			<script language="javascript">

			location.replace("index.php?do=article-import_articles-article-import&debug=1&export_time='.$in['export_time'].'&file_type='.$in['file_type'].'&tab=1&offset='.$in['offset'].'&added='.$in['added'].'&updated='.$in['updated'].'&filename='.$orig_filename.'&create_temp_data=0&count='.$count.'&uploaded='.$uploaded.'");

			</script>
			';
			exit();
		}else{
			if(!$in['export_time']){
				$in['export_time'] = strtotime('now');
				$this->db->query("INSERT INTO pim_article_import_log SET
						   date			    = '".$in['export_time']."',
						   filename              = '".$orig_filename."',
						   article_added         = '".$article_added."',
						   article_updated       = '".$article_updated."',
						   article_id_added      = '".serialize($added_art_array)."',
						   article_id_updated    = '".serialize($updated_art_array)."'
						   username='".$user_info->f('first_name')." ".$user_info->f('last_name')."'");
			}else{
				$articles_added = $this->db->field("SELECT article_id_added FROM pim_article_import_log WHERE date='".$in['export_time']."' ");
				$articles_updated = $this->db->field("SELECT article_id_updated FROM pim_article_import_log WHERE date='".$in['export_time']."' ");
				$added_ser_articles= unserialize($articles_added);
				$updated_ser_articles= unserialize($articles_updated);
				$all_added_articles = array_merge($added_ser_articles,$added_art_array);
				$all_updated_articles = array_merge($updated_ser_articles,$updated_art_array);
				$this->db->query("UPDATE pim_article_import_log SET
						   filename              = '".$orig_filename."',
						   article_added         = '".$article_added."',
						   article_updated       = '".$article_updated."',
						   article_id_added      = '".serialize($all_added_articles)."',
						   article_id_updated    = '".serialize($all_updated_articles)."',
						   username 			 = '".$user_info->f('first_name')." ".$user_info->f('last_name')."'
						   WHERE date			 = '".$in['export_time']."'
				");


			}
            if($in['file_type']==3){
				@unlink(INSTALLPATH.UPLOAD_PATH."import_article.xml");
			}
			elseif($in['file_type']==1){
				@unlink(INSTALLPATH.UPLOAD_PATH."import_article.xls");
			}else{
				fclose($fp);
				@unlink(INSTALLPATH.UPLOAD_PATH."import_article.csv");
			}
		}


		if(CAN_SYNC){
			//sync articles on front
			global $database_config;
			global $config;

			//$config['front_address']='192.168.3.2';

	    	if($database_config['mysql']['database'] == '144ef87d_69c4_3d05_7bbf383e7711' ){
	    		$config['webshop_address']='5.9.104.242';
	    	}

			//create .sql file with tables to create
			$dump_database_article_beckup=shell_exec("mysqldump 	-h ".$database_config['mysql']['hostname']." -u ".$database_config['mysql']['username']." -p".$database_config['mysql']['password']." ".$database_config['mysql']['database']." vats pim_article_categories pim_article_brands pim_articles stock_movements pim_articles_lang pim_article_prices > ../art_imp_beckup_".$database_config['mysql']['database'].".sql 2>&1;");
			//run .sql file
			$replicate_user =shell_exec("mysql -h ".$config['webshop_address']." -u ".$config['webshop_user']." -p".$config['webshop_password']." ".$database_config['mysql']['database']." < ../art_imp_beckup_".$database_config['mysql']['database'].".sql 2>&1;");
			//delete .sql file
			$remove_user_file=shell_exec("rm ../art_imp_beckup_".$database_config['mysql']['database'].".sql 2>&1;");
		}



         	echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&succ=1";
          </script>';

		msg::$success = "Data succefully imported.";
        return true;
	}

	/**
	 * Upload files
	 * @param  array $in
	 * @return boolean
	 */
	function upload_file(&$in)
	{
		global $_FILES;
		if($in['file_type']==3){
			$allowed['.xml']=1;

			$f_name="import_article.xml";
		}elseif($in['file_type']==1){
			$allowed['.xls']=1;
			$allowed['.xlsx']=1;
			$f_name="import_article.xls";
		}else{
			$allowed['.csv']=1;
			$f_name="import_article.csv";
		}


		$f_ext=substr($_FILES['filename']['name'],strpos($_FILES['filename']['name'],"."));
		if(!$allowed[strtolower($f_ext)])
		{
			if($in['file_type']==3){
				msg::$error = "Only xml files are accepted.";
					echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=7";
          </script>';
			}elseif($in['file_type']==1){
				msg::$error = "Only xls and xlsx files are accepted.";
					echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=1";
          </script>';
			}else{
				msg::$error = "Only csv files are accepted.";
					echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=2";
          </script>';
			}
			return false;
		}



		$f_out=INSTALLPATH.UPLOAD_PATH.$f_name;

		if(!$_FILES['filename']['tmp_name'])
		{
			$in['error'].="Please upload a file"."<br>";
				echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=3";
          </script>';
			return false;
		}

		if(FALSE === move_uploaded_file($_FILES['filename']['tmp_name'],$f_out))
		{
			$in['error'].="Unable to upload the file.  Move operation failed."."<!-- Check file permissions -->";
				echo '<script>
          (window.parent||window ).location = "index.php?do=article-import_articles&err=4";
          </script>';
			return false;
		}

		@chmod($f_out, 0777);
		$in['error'].="File Uploaded";
		return true;
	}



	/**
	 * Duplicate articles
	 * @param  array $in
	 * @return boolean
	 */
	function duplicate(&$in)
	{

		if(!$this->add_validate($in)){

			return false;
		}

		$NEXT_AUTOMATIC_ARTICLE_BARCODE = $this->db->field("SELECT value FROM settings WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE'");
		if($in['ean_code'] && $NEXT_AUTOMATIC_ARTICLE_BARCODE){			
			$v=new validation($in);
			$v->field('ean_code','Ean Code','integer:unique[pim_articles.ean_code]');
			if(!$v->run()){
				return false;
			}
		}

		$query_sync=array();
		$article_vat = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['duplicate_article_id']."'");





		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET			item_code  							= '".$in['item_code']."',
																ean_code  							= '".$in['ean_code']."',
																hide_stock  							= '".$in['hide_stock']."',
																	use_percent_default  				= '".$in['use_percent_default']."',
																supplier_reference      = '".$in['supplier_reference']."',
																vat_id									= '".$in['vat_id']."',
																article_brand_id			= '".$in['article_brand_id']."',
																article_category_id			= '".$in['article_category_id']."',
																origin_number   			= '".$in['origin_number']."',
																stock        	    		= '".$in['stock']."',
																sale_unit        			= '".$in['sale_unit']."',
																internal_name       		= '".$in['internal_name']."',
																packing          			= '".return_value2($in['packing'])."',
																price_type          		= '".$in['price_type']."',
																article_threshold_value    	= '".$in['article_threshold_value']."',
																weight    					= '".$in['weight']."',
																show_img_q								= '".$in['show_img_q']."',
																supplier_id								= '".$in['supplier_id']."',
																supplier_name							= '".$in['supplier_name']."',
																custom_threshold_value    	= '".$custom_threshold_value."',
																ledger_account_id			='".$in['ledger_account_id']."'

																");
		//update next automatic ean code
		if($in['ean_code'] && $NEXT_AUTOMATIC_ARTICLE_BARCODE){
			$this->db->query("UPDATE settings SET value='".($NEXT_AUTOMATIC_ARTICLE_BARCODE+1)."' WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE' ");
		}
		
		/*start for  stock movements*/
		if($in['stock']){
        	$this->db->insert("INSERT INTO stock_movements SET
        													date 						=	'".time()."',
        													created_by 					=	'".$_SESSION['u_id']."',
        													article_id 					=	'".$in['article_id']."',
        													article_name       			= 	'".$in['internal_name']."',
        													item_code  					= 	'".$in['item_code']."',
        													movement_type				=	'1',
        													quantity 					= 	'".$in['stock']."',
        													stock 						=	'0',
        													new_stock 					= 	'".$in['stock']."'
        	");
        }
        /*end for  stock movements*/

		$query_sync[0]="INSERT INTO pim_articles SET
																item_code  				= 	'".$in['item_code']."',
																ean_code  				= 	'".$in['ean_code']."',
																vat_id					= 	'".$in['vat_id']."',
																article_brand_id		= 	'".$in['article_brand_id']."',
																article_category_id		= 	'".$in['article_category_id']."',
																description		 		= 	'".$in['description']."',
																internal_name    		= 	'".$in['internal_name']."',
																origin_number    		= 	'".$in['origin_number']."',
																article_threshold_value = 	'".$in['article_threshold_value']."',
																stock        	    	= 	'".$in['stock']."',
																weight        	    	= 	'".$in['weight']."',
																price_type          	= 	'".$in['price_type']."'

																";

		Sync::start(ark::$model.'-'.ark::$method);


			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '" . $in['article_id'] . "',
		                                                                         lang_id                = '" . $in['lang_id'] . "',
		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."' ");

		    //duplicate translation
		    $pim_articles_lang = $this -> db -> query("SELECT * FROM pim_articles_lang where item_id='".$in['duplicate_article_id']."' AND lang_id!='".$in['lang_id']."'");
		   while ($pim_articles_lang -> next()) {
			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '".$in['article_id']."',
		                                                                         lang_id                = '".$pim_articles_lang-> f('lang_id')."',
		                                                                         name                   = '".addcslashes($pim_articles_lang-> f('name'),"'\\")."',
		                                                                         name2                  = '".addcslashes($pim_articles_lang-> f('name2'),"'\\")."',
		                                                                         description			= '".addcslashes($pim_articles_lang-> f('description'),"'")."'
		                                                                         	");
		}



		$i=0;
		//***Get Attributes for articles
		$this->db->query("SELECT pim_product_attribute.* ,pim_product_attribute_category.category_id
	        		   FROM pim_product_attribute
	        		   INNER JOIN pim_product_attribute_category ON pim_product_attribute_category.product_attribute_id=pim_product_attribute.product_attribute_id
	        	       WHERE pim_product_attribute.apply_to=2 AND pim_product_attribute_category.category_id='".$in['category_id']."'
	        		   ORDER BY pim_product_attribute.sort_order ASC ,pim_product_attribute.name ASC
	        		       ");
		while($this->db->move_next())
		{
			$this->attribute[$i]['product_attribute_id']=$this->db->f('product_attribute_id');
			$this->attribute[$i]['name']=$this->db->f('name');
			$this->attribute[$i]['field_name']=$this->db->f('field_name');
			$this->attribute[$i]['type']=$this->db->f('type');
			$this->attribute[$i]['items_per_line_4']=$this->db->f('items_per_line_4');
			$this->attribute[$i]['sort_order']=$this->db->f('sort_order');
			$this->attribute[$i]['searchable']=$this->db->f('searchable');
			$this->attribute[$i]['mandatory']=$this->db->f('mandatory');
			$this->attribute[$i]['filterable']=$this->db->f('filterable');
			$i++;
		}

		$this->db->query("delete from pim_product_attribute_value where article_id='".$in['article_id']."'");

		if(is_array($this->attribute) && count($this->attribute) > 0){
			foreach($this->attribute as $key => $attribute_array){
				insert_product_attribute_value($in['article_id'], $attribute_array, $in,2);
			}
		}

$this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".return_value($in['purchase_price'])."'
						");


    if($in['price_type']==1){
$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
				//insert the default price
                 if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }
				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) - ($prices->f('price_value') * return_value($base_price)/ 100);
							}else{ //fix
								$total_price = return_value($base_price) - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) + ($prices->f('price_value') * return_value($base_price) / 100);
							}else{ //fix
								$total_price = return_value($base_price) + $prices->f('price_value');
							}

					break;
				}

					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");



			}


		//duplicate_data('pim_article_prices','article_id',$in['duplicate_article_id'],$in['article_id'],'article_price_id', return_value($in['price']), return_value($in['total_price']));
      }else{
        /* $this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0'
						");*/
      }

      //	duplicate_data('pim_product_attribute_value','product_id',$in['duplicate_product_id'],$in['product_id']);

		foreach ($in['tax_id'] as $tax_id=>$is_tax){
			$this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$tax_id."'

				");
		}

		if($in['parent_variant_id']){
			$this->db->query("INSERT INTO pim_article_variants SET
								parent_article_id = '".$in['parent_variant_id']."',
								article_id	= '".$in['article_id']."',
								type_id	= '".$in['variant_type_id']."'

				");
		}


		Sync::end($in['duplicate_article_id'],$query_sync);
		unset($in['duplicate_article_id']);
		update_articles($in['article_id'],DATABASE_NAME);
		msg::success(gm("Article succefully duplicate"),'success');
		return true;
	}

	/**
	 * Add price for articles to quantities
	 * @param  array $in
	 * @return boolean
	 */
	function quantity_price(&$in)
	{

		if(!$this->quantity_price_validate($in)){

			return false;
		}
		

		$total_price    = return_value($in['price_q']);

		$vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		$total_with_vat = $total_price+($total_price*$vat_value/100);

	    $this->db->query("INSERT INTO pim_article_prices SET
											article_id		= '".$in['article_id']."',
											from_q			= '".return_value($in['from_q'])."',
											to_q			= '".return_value($in['to_q'])."',
											price           = '".$total_price."',
											total_price     = '".$total_with_vat."',
											percent   = '".return_value($in['percent'])."'

							");

		insert_message_log($this->pag,'{l}Price updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Price succefully updated"),'success');
        update_articles($in['article_id'],DATABASE_NAME);
		
		return true;
	}



    /**
     * Update price for articles to quantities
     * @param  array $in
     * @return boolean
     */
	function quantity_price_update(&$in)
	{
		if(!$this->quantity_price_update_validate($in)){

			return false;
		}
		

		$total_price    = return_value($in['price_q']);

		$vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		$total_with_vat = $total_price+($total_price*$vat_value/100);

	    $this->db->query("UPDATE pim_article_prices SET
											from_q			= '".return_value($in['from_q'])."',
											to_q			= '".return_value($in['to_q'])."',
											price           = '".$total_price."',
											total_price     = '".$total_with_vat."',
											percent         = '".return_value($in['percent'])."'
						  WHERE article_price_id='".$in['article_price_id']."'

							");

	
	
		msg::success(gm("Price succefully updated."),'success');
		update_articles($in['article_id'],DATABASE_NAME);
		insert_message_log($this->pag,'{l}Price updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		return true;
	}



	/**
	 * Delete article price to quantities
	 * @param  array $in
	 * @return boolean
	 */
	function delete_quantity_price(&$in)
	{
		if(!$this->delete_quantity_price_validate($in)){
			return false;
		}
		

		$this->db->query("DELETE FROM pim_article_prices WHERE article_price_id='".$in['article_price_id']."'");

		msg::success("Price deleted.",'success');
	
		return true;
	}


	/**
	 * Return article price based for quantities
	 * @param  array $in
	 * @return float
	 */
	function get_article_quantity_price(&$in)
	{

		$price_type=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
        $is_block_discount=$this->db->query("SELECT pim_product_article.product_id,pim_product_article.article_id,pim_product_article.block_discount
                               FROM pim_product_article
                               WHERE pim_product_article.article_id='".$in['article_id']."' AND pim_product_article.block_discount='1'");

        if($is_block_discount->move_next()){
	    		$price=$in['price'];
		}else{
			if($price_type==1){
		   		$price=$in['price'];
	    	}else{
	    		$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices
	    		                     WHERE pim_article_prices.from_q<='".$in['quantity']."'
	    		                     AND  pim_article_prices.article_id='".$in['article_id']."'
	                                 ORDER BY pim_article_prices.from_q DESC
	                                 LIMIT 1
	    		                     ");

		        if(!$price){
		        	$price=$in['price'];
		        }
	    	}
		}
	    $in['new_price']=$price;
	    $in['debug']=true;
		return true;
	}

	/**
	 * Validate date for add qunatity price
	 * @param  array $in
	 * @return boolean
	 */
	function quantity_price_validate(&$in)
	{

		$v = new validation($in);

		// biggest to quantity
		$biggest_to_quantity=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT pim_article_prices.from_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   //$in['error']=gm('To Qty value should be bigger than From Qty');
		   msg::error(gm('To Qty value should be bigger than From Qty'),'error');
		   return false;
		}
        if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['from_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1");
         if($this->db->move_next()){
         	 //$in['error']=gm('Invalid Interval');
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['to_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1");
         if($this->db->move_next()){
         	 //$in['error']=gm('Invalid Interval');
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE  pim_article_prices.from_q < '".return_value($in['to_q'])."'  AND    pim_article_prices.to_q < '".return_value($in['to_q'])."'
         	               AND pim_article_prices.to_q > '".return_value($in['from_q'])."'
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1");
         if($this->db->move_next()){
         	 //$in['error']=gm('Invalid Interval');
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.from_q < '".return_value($in['to_q'])."' AND pim_article_prices.from_q > '0' AND base_price!=1  AND  pim_article_prices.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	// $in['error']=gm('Invalid Interval');
            	  msg::error(gm('Invalid Interval'),'error');
		         return false;
            }



        }else{
           //return_value($in['from_q']) >

            $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND  pim_article_prices.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	 //$in['error']=gm('Invalid Interval');
            	  msg::error(gm('Invalid Interval'),'error');
		         return false;
            }

        }
        if(return_value($in['price_q']) <= 0 && !empty($in['price_q'])){
        	// $in['price_error']=gm('This field should be bigger than 0');
        	$v -> f('price_q', 'Price', 'empty_condition','This field should be bigger than 0');
        }




	

		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]:numeric');
		$v -> f('to_q', 'Max Qty', 'numeric:greater_than[1]');
		$v -> f('price_q', 'Price', 'required:numeric');

		//$v -> f('to_q', 'Max Qty', 'required:greater_than[1]');

        return $v -> run();
    }

   /**
    * Validate update quantity price
    * @param  array $in
    * @return boolena
    */
	function quantity_price_update_validate(&$in)
	{
		$v = new validation($in);
		$biggest_to_quantity=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND article_price_id!='".$in['article_price_id']."' ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT pim_article_prices.from_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."'  AND article_price_id!='".$in['article_price_id']."' ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		    msg::error(gm('To Qty value should be bigger than From Qty'),'error');
		   return false;
		}
        if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['from_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND article_price_id!='".$in['article_price_id']."'");
         if($this->db->move_next()){
         	  msg::error(gm('Invalid Interval'),'error');

		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['to_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND article_price_id!='".$in['article_price_id']."'");
         if($this->db->move_next()){
         	   msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE  pim_article_prices.from_q < '".return_value($in['to_q'])."'  AND    pim_article_prices.to_q < '".return_value($in['to_q'])."'
         	               AND pim_article_prices.to_q > '".return_value($in['from_q'])."'
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND article_price_id!='".$in['article_price_id']."'");
         if($this->db->move_next()){
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

          $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.from_q < '".return_value($in['to_q'])."' AND base_price!=1  AND  pim_article_prices.to_q=0  AND article_price_id!='".$in['article_price_id']."' LIMIT 1");
            if($this->db->move_next()){
            	  msg::error(gm('Invalid Interval'),'error');
		         return false;
            }



        }else{
           //return_value($in['from_q']) >

            $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND  pim_article_prices.to_q=0  AND article_price_id!='".$in['article_price_id']."' LIMIT 1");
            if($this->db->move_next()){
            	   msg::error(gm('Invalid Interval'),'error');
		         return false;
            }

        }

		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]');

        return $v -> run();
    }



   	/**
   	 * Delete quantity price for article
   	 * @param  array $in
   	 * @return boolean
   	 */
	function delete_quantity_price_validate(&$in)
	{
		$is_ok = true;
		if(!$in['article_price_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		if(!$in['article_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		else{
			$this->db->query("SELECT article_id FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND article_price_id='".$in['article_price_id']."'");
			if(!$this->db->move_next()){
				$in['error'] = "Invalid ID.";
				return false;
			}
		}

		return $is_ok;
	}

    /**
     * Update price list for article
     * @param  array $in
     * @return boolean
     */
	function update_price_list(&$in)
	{
		if(!$this->update_price_list_validate($in)){
           return false;
		}

		

	    $in['total_price']=return_value($in['price'])+(return_value($in['price'])* get_article_vat($in['article_id'])/100);
        $in['price_type']=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
		$in['vat_id']=$this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
        $in['purchase_price'] =$this->db->field("SELECT pim_article_prices.purchase_price FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price='1'");
		if($in['price_type']==1){
           $this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");
        }

		//update base price
		$this->db->query("UPDATE pim_article_prices SET
						price				= '".return_value($in['price'])."',
						total_price			= '".$in['total_price']."'
						WHERE
						base_price=1   AND  article_id='".$in['article_id']."'
				");

        if($in['price_type']==1){

		$prices = $this->db->query("SELECT * FROM pim_article_price_category");
		while ($prices->next()){
               if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }


            $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
            	              WHERE article_id='".$in['article_id']."' AND category_id='".$prices->f('category_id')."'");
            if($this->db2->move_next()){
                    if($this->db2->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }


			switch ($this->db2->f('type')) {
				case 1:                  //discount
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) - round(($this->db2->f('price_value') * return_value($base_price) / 100),2);

				}else{ //fix
					$total_price = return_value($base_price) - $this->db2->f('price_value');
				}

				break;
				case 2:                 //markup margin
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) +  round(($this->db2->f('price_value') * return_value($base_price) / 100),2);
				}else{ //fix
					$total_price = return_value($base_price) + $this->db2->f('price_value');
				}

				break;

				case 3:                 //profit margin
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) /  round(1-($this->db2->f('price_value') / 100),2);
				}else{ //fix
					//$total_price = return_value($base_price) + $this->db2->f('price_value');
				}

				break;
					}



              }else{

                      switch ($prices->f('type')) {
				case 1:                  //discount
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) - round(($prices->f('price_value') * return_value($base_price) / 100),2);

				}else{ //fix
					$total_price = return_value($base_price) - $prices->f('price_value');
				}

				break;
				case 2:                 //markup margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) +  round(($prices->f('price_value') * return_value($base_price) / 100),2);
				}else{ //fix
					$total_price = return_value($base_price) + $prices->f('price_value');
				}

				break;
				case 3:                 //profit margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) /  round(1-($prices->f('price_value') / 100),2);
				}else{ //fix
					//$total_price = return_value($base_price) + $prices->f('price_value');
				}

				break;
              }
          }

            $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$in['article_id']."',
						price_category_id	= '".$prices->f('category_id')."',
						price				= '".$total_price."',
						total_price			= '".$total_with_vat."'
				");



		}
	}else{
		//qunatity order price
	    $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
        $this->db->query("SELECT * FROM  pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price=0");
        while ($this->db->move_next()) {
        	   $total_price = $this->db->f('price');
        	   $total_with_vat = $total_price+($total_price*$vat_value/100);
        	   $this->db2->query("UPDATE pim_article_prices SET
											price           = '".$total_price."',
											total_price     = '".$total_with_vat."'
						  WHERE article_price_id='".$this->db->f('article_price_id')."'

							");
        }
          update_q_price($in['article_id'],$in['price'],$in['vat_id']);

	}


      
		update_articles($in['article_id'],DATABASE_NAME);
		msg::success (gm("Price succefully updated."),'success');
		return true;
	}

	/**
	 * validate update price list data
	 * @param  array $in
	 * @return boolean
	 */
	function update_price_list_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');
		$v->field('price','Price','required:numeric');
		return $v -> run();

	}

	/**
	 * Update price for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_price(&$in)
	{
		if(!$this->update_price_validate($in)){
           return false;
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' and base_price!=1");
		$prices = $this->db->query("SELECT * FROM pim_article_price_category ");

		while ($prices->next()){

			$article_price = $in['si_price'][$prices->f('category_id')];
			$vat = $in['si_vat_value'] * $article_price/100;

			$this->db->query("INSERT INTO pim_article_prices SET
											article_id			= '".$in['article_id']."',
											price_category_id	= '".$prices->f('category_id')."',
											price			    = '".$in['si_price'][$prices->f('category_id')]."',
											total_price			= '".return_value($article_price+$vat)."'
							");
        }

        Sync::end($in['article_id']);
		update_articles($in['article_id'],DATABASE_NAME);
		 insert_message_log($this->pag,'{l}Price updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::$success = "Price succefully updated.";
		return true;
	}

	/**
	 * Validate update price data
	 * @param  array $in
	 * @return boolean
	 */
	function update_price_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');

		foreach ($in['si_price'] AS $price_category_id => $price){
			$v->field('si_price','Price','required:numeric');

		}

		return $v -> run();

	}

	/**
	 * Update default price for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_default_prices(&$in)
	{
		$in['failure'] = false;
		if(!$this->update_default_prices_validate($in)){
			$in['failure'] = true;
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);

		foreach($in['base_price'] as $article_id => $new_price){

			$article_price  = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article_id."' AND base_price=1 ");
			$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_id."'");
			$vat_value      = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
			$new_price_with_vat = return_value($new_price)+(return_value($new_price)*$vat_value/100);

			// updating buy prices only if default price or vat percent has changed



			$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$article_id."' AND base_price!=1");


			$this->db->query("UPDATE pim_article_prices SET

						price				= '".return_value($new_price)."',
						total_price			= '".$new_price_with_vat."'
						WHERE
						base_price=1   AND  article_id='".$article_id."'
				");


			$prices = $this->db->query("SELECT * FROM pim_article_price_category");
			while ($prices->next()){
				//insert the default price

				switch ($prices->f('type')) {
					case 1:                  //discount
					if($prices->f('price_value_type')==1){  // %
						$total_price = return_value($new_price) - ($prices->f('price_value') * return_value($new_price) / 100);
					}else{ //fix
						$total_price = return_value($new_price) - $prices->f('price_value');
					}

					break;
					case 2:                 //profit margin
					if($prices->f('price_value_type')==1){  // %
						$total_price = return_value($new_price) + ($prices->f('price_value') * return_value($new_price) / 100);
					}else{ //fix
						$total_price = return_value($new_price) + $prices->f('price_value');
					}

					break;

				}

				$total_with_vat = $total_price+($total_price*$vat_value/100);

				$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$article_id."',
						price_category_id	= '".$prices->f('category_id')."',
						price				= '".$total_price."',
						total_price			= '".$total_with_vat."'
				");

			}
			update_articles($article_id,DATABASE_NAME);
		}
		Sync::end($in['article_id']);
		 insert_message_log($this->pag,'{l}Price updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::$success = "Price succesfully updated.";
		return true;
	}

	/**
	 * Validate update default price data
	 * @param  array $in
	 * @return boolean
	 */
	function update_default_prices_validate(&$in)
	{
		$v = new validation($in);

		foreach ($in['base_price'] AS $price_category_id => $price){
			$v->field('base_price','Price','required:numeric');

		}

		return $v -> run();

	}

	/**
	 * Delete articles
	 * @param  array $in
	 * @return boolean
	 */
	function delete(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
//                global $cfg;

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' ");
		$this->db->query("DELETE FROM pim_product_attribute_value WHERE article_id='".$in['article_id']."'");
		$this->db->query("DELETE FROM pim_product_article WHERE  article_id='" . $in['article_id'] . "'");
		$this->db->query("DELETE FROM pim_articles_lang WHERE  item_id='" . $in['article_id'] . "'");
		$this->db->query("DELETE FROM pim_articles_taxes where article_id='".$in['article_id']."'");
		$this->db->query("DELETE FROM article_threshold_dispatch where article_id='".$in['article_id']."'");
		$this->db->query("DELETE FROM url_mask where item_id='".$in['article_id']."' and controller='article'");
		$this->db->query("DELETE FROM pim_articles_combined WHERE article_id='". $in['article_id'] ."'");
		$c = $this->db->query("SELECT file_name,upload_amazon FROM pim_article_photo where parent_id='".$in['article_id']."'");
		ark::loadLibraries(array('aws'));
    $a = new awsWrap(DATABASE_NAME);
		while ($c->next()) {
			if($c->f('upload_amazon')==1){
				$item = $c->f('file_name');
  			$link = $this->db->field("SELECT item FROM s3_links WHERE link='".$item."' ");
				$a->deleteItem($link);
			}else{
				@unlink( INSTALLPATH.'upload/'.DATABASE_NAME.'/pim_article_photo/'.$c->f("file_name"));
			}
		}
		$this->db->query("DELETE FROM pim_article_photo where parent_id='".$in['article_id']."'");

		msg::$success = "Article deleted.";
		Sync::end($in['article_id']);
		return true;
	}

	/**
	 * archive article function
	 *
	 * @param  array $in
	 * @return boolean
	 */
	function archive(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE pim_articles SET active='0', updated_at='".time()."' WHERE article_id='".$in['article_id']."' ");
		insert_message_log($this->pag,'{l}Article archived by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::$success = "Article archived.";
		Sync::end($in['article_id']);
		return true;
	}

	/**
	 * activate article function
	 *
	 * @param  array $in
	 * @return boolean
	 */
	function activate(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE pim_articles SET active='1', updated_at='".time()."' WHERE article_id='".$in['article_id']."' ");
		insert_message_log($this->pag,'{l}Article activated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::$success = "Article activated.";
		Sync::end($in['article_id']);
		return true;
	}

	/**
	 * Validate delete data
	 * @param  array $in
	 * @return boolean
	 */
	function delete_validate(&$in)
	{
		$is_ok = true;
		if(!$in['article_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		else{
			$this->db->query("SELECT article_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
			if(!$this->db->move_next()){
				$in['error'] = "Invalid ID.";
				return false;
			}
		}

		return $is_ok;
	}

	/**
	 * Delete import log
	 * @param  array $in
	 * @return boolean
	 */
	function delete_import(&$in)
	{
		if(!$this->delete_import_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM pim_article_import_log WHERE article_import_log_id='".$in['article_import_log_id']."' ");

		msg::$success = "Import Log deleted.";

		return true;
	}

	/**
	 * Validate delete import data
	 * @param  array $in
	 * @return boolean
	 */
	function delete_import_validate(&$in)
	{
		$is_ok = true;
		if(!$in['article_import_log_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		else{
			$this->db->query("SELECT article_import_log_id FROM pim_article_import_log WHERE article_import_log_id='".$in['article_import_log_id']."'");
			if(!$this->db->move_next()){
				$in['error'] = "Invalid ID.";
				return false;
			}
		}

		return $is_ok;
	}

	/**
	 * Update article reference from excel file
	 * @param  array $in
	 * @return boolean
	 */
	function update_article_nr(&$in)
	{

		global $_FILES;
		ini_set("auto_detect_line_endings", true);
		//upload file


		if(!$this->upload_file($in))
		{
			return false;
		}
		$filename=$_FILES['filename']['name'];

		$date=strtotime('now');

		$fieldlist = array(
		'ARTICLE_NR'	    => 0,
		'NEW_ARTICLE_NR'	        => 1

		);

		$i=0;
		$fp = fopen (INSTALLPATH.UPLOAD_PATH."import_csv.csv","r");


		while ($data = fgetcsv ($fp, 10000, ";",'\r')){

			if(($row == 0 )) // 1 if not count
			{
				$row++;
				continue;
			}


			$this->db->query("SELECT pim_articles.item_code,pim_articles.article_id FROM pim_articles WHERE item_code='".addcslashes($data[$fieldlist['ARTICLE_NR']],"'")."'");
			if($this->db->move_next()){
				$this->db2->query("UPDATE pim_articles set item_code='".addcslashes($data[$fieldlist['NEW_ARTICLE_NR']],"'")."', old_item_code='".$this->db->f('item_code')."' WHERE article_id='".$this->db->f('article_id')."' ");
			}

			$row++;

		}



		fclose($fp);
		@unlink(INSTALLPATH.UPLOAD_PATH."import_csv.csv");


		msg::$success = "Data succesfully imported.";

		return true;
	}

	/**
	 * Set article to be show on site
	 * @param  array $in
	 * @return boolean
	 */
	function show_front(&$in)
	{
		$this->db->query("UPDATE pim_articles SET show_front='1' WHERE article_id='".$in['article_id']."'");
	}
	function hide_front(&$in)
	{
		$this->db->query("UPDATE pim_articles SET show_front='0' WHERE article_id='".$in['article_id']."'");
	}


	/**
	 * Update lang filed for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_lang_fields(&$in)
	{

		if(!$this->update_lang_fields_validate($in)){
			return false;
		}

		foreach($in['translation'] as $item=>$data){
			   $this->db-> query("SELECT   item_id FROM  pim_articles_lang	WHERE item_id= '" . $in['article_id'] . "' AND lang_id='".$data['lang_id']."'");
			   if( $this->db->next()){
			   	    $this->db2-> query("UPDATE  pim_articles_lang 	SET  ".$in['txt']."= '".$data[$in['txt']]."' WHERE item_id= '" . $in['article_id'] . "' AND lang_id='".$data['lang_id']."'");
			   }else{
			   	  $this->db2-> query("INSERT INTO   pim_articles_lang 	SET  ".$in['txt']."= '".$data[$in['txt']]."' , item_id= '" . $in['article_id'] . "' , lang_id='".$data['lang_id']."'");
			   }

                
		}



		 insert_message_log($this->pag,'{l}Article language fields updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Article language fields successfully updated."),'success');
		json_out($in);
		return true;
	}

	/**
	 * Validate update lang field data
	 * @param  array $in
	 * @return boolean
	 */
	function update_lang_fields_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');

		return $v -> run();

	}

	/**
	 * Edit select fields
	 * @param  array $in
	 * @return boolean
	 */
	function select_edit(&$in){

		$query_sync=array();
		$i=0;
		foreach ($in['name_select'] as $key => $value){
			if(!empty($value)){
			$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$in['id_select'][$key]."' ");
				if(!$id){
					$inserted_id=$this->db->insert("INSERT INTO ".$in['table']." SET name='".addslashes($value)."', sort_order='".$key."' ");
					$query_sync[$i]="INSERT INTO ".$in['table']." SET id='".$inserted_id."',name='".addslashes($value)."', sort_order='".$key."'";
					$i++;
				}
			}
		}

		Sync::start(ark::$model.'-'.ark::$method);

        foreach ($in['name_select'] as $key=>$value){
			if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$in['id_select'][$key]."' ");
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value)."', sort_order='".$key."' WHERE id='".$id."' ");

				}
			}
		}

		Sync::end(0,$query_sync);
		return true;
	}

	/**
	 * Delete data from select dropdowns
	 * @param  array $in
	 * @return boolean
	 */
	function delete_select(&$in){
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM ".$in['table']." WHERE id='".$in['id']."' ");
		Sync::end();
		return true;
	}



	/**
	 * Get language value
	 * @param  array $in
	 * @return boolean
	 */
	function get_lang_value(&$in) {
		$in['code']=get_language_code($in['lang_id']);

		if(!$in['lang_id'] || !$in['item_id']){
			return true;
		}

		$in['value']=$this->db->field("SELECT ".$in['field']."
		                           FROM ".$in['tbl']."
		                           WHERE lang_id=".$in['lang_id']." and item_id=".$in['item_id']."
		                  ");

		return true;

	}

	function upload_photo1($in){
    $response=array();
    if (!empty($_FILES)) {
       $this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");

      $tempFile = $_FILES['file']['tmp_name'][0];
      $ext = pathinfo($_FILES['file']['name'][0],PATHINFO_EXTENSION);
      $in['name'] = 'tmp_file_'.time().'.'.$ext;
      
      $targetPath = INSTALLPATH.'upload';

      
      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
      // Validate the file type
      $fileTypes = array('jpg','jpeg','gif'); // File extensions
      $fileParts = pathinfo($_FILES['file']['name'][0]);
      mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

      	if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE){
            $image = new SimpleImage();
            $image->load($_FILES['file']['tmp_name'][0]);
            $image->scale(MAX_IMAGE_SIZE,$size);
            $image->save($targetFile);
        }else{
            move_uploaded_file($tempFile,$targetFile);             
        }
        $in['tmp_file_name'] = $in['name'];
        $response['name'] =  $this->saveTmpFileToAmazon($in);
        $response['success'] = 'success';
       echo json_encode($response);
        // ob_clean();
      } else {
        $response['error'] = gm('Invalid file type.');
       echo json_encode($response);
      }
      exit();
    }
  }


	function upload_photo($in){
    $response=array();
    if (!empty($_FILES)) {
    	$size=$_FILES['Filedata']['size'];
    	if(!defined('MAX_IMAGE_SIZE') &&  $size>512000){
  			$response['error'] = gm('Size exceeds 500kb');
  			$response['filename'] = $_FILES['Filedata']['name'];
			echo json_encode($response);
			exit();
  		}
       $this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);
      $in['name'] = 'tmp_file_'.time().'.'.$ext;
      
      $targetPath = INSTALLPATH.'upload';

      
      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
      // Validate the file type
      $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
      	if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE){
            $image = new SimpleImage();
            $image->load($_FILES['Filedata']['tmp_name']);
            $image->scale(MAX_IMAGE_SIZE,$size);
            $image->save($targetFile);
        }else{
            move_uploaded_file($tempFile,$targetFile);             
        }
        $in['tmp_file_name'] = $in['name'];
        $response['name'] =  $this->saveTmpFileToAmazon($in);
        $response['success'] = 'success';
       echo json_encode($response);
        // ob_clean();
      } else {
        $response['error'] = gm('Invalid file type.');
       echo json_encode($response);
      }
      exit();
    }
  }

    /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function saveTmpFileToAmazon(&$in)
  {
    
    $in['up_folder']='../upload/'.DATABASE_NAME.'/pim_article_photo';
    $parentId=$in['article_id'];
    if($in['tmp_file_name']){
      global $config;
      ark::loadLibraries(array('aws'));
      $a = new awsWrap(DATABASE_NAME);
      $img_id = $this->db->insert("INSERT INTO  pim_article_photo SET parent_id='".$in['article_id']."',folder='".$in['up_folder']."', upload_amazon=1");
     
      $artPath = INSTALLPATH.'upload/'.$in['tmp_file_name'];
     

    
      $ext = pathinfo($artPath,PATHINFO_EXTENSION);
      
       $artFile = 'article/article_'.$parentId."_".$img_id.".".$ext;
    
      $a->uploadFile($artPath,$artFile);
      unlink($artPath);
     
     
      
      
       $link =  $a->getLink($config['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext);
       $this->db->query("UPDATE pim_article_photo SET file_name='".$link."',amazon_path='".$config['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext."' WHERE photo_id='".$img_id."' AND parent_id ='".$parentId."' ");
       $this->db->query("UPDATE pim_articles SET article_photo='".$link."' WHERE article_id ='".$parentId."' ");
				
       $in['artPhoto']=$link;
   
      return $link;
   }
 }
   function remove_photo(&$in)
  {
    global $config;
    $info = $this->db->query("SELECT * FROM pim_article_photo WHERE parent_id='".$in['article_id']."' ");

      ark::loadLibraries(array('aws'));
      $aws = new awsWrap(DATABASE_NAME);
      $link =  $aws->doesObjectExist($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
      if($link){
        $aws->deleteItem($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
      }
      $this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");
      $this->db->query("UPDATE pim_articles SET article_photo='' WHERE article_id ='".$in['article_id']."' ");
     msg::success(('File deleted'),'success');
     
    
    return true;
  }


	
/*function set_dispatch_stock(&$in)
	{
        $in['failure'] = false;
		if(!$this->set_dispatch_stock_validate($in)){
			 $in['failure'] = true;
			return false;
		}
         
          $stock_total=0;
         foreach($in['query'] as $item=>$data){
               $this->db->query("DELETE FROM dispatch_stock WHERE address_id='".$data['address_id']."' AND article_id='".$in['article_id']."' and main_address=1");
               
                $this->db->insert("INSERT INTO dispatch_stock SET
	        												  article_id 		          = 	'".$in['article_id']."',
	        												  address_id 		          = 	'".$data['address_id']."',
	        												  main_address                =     '1',
	        												  stock 		              = 	'".return_value($data['stock'])."'

	        	                             ");

                        $stock_total=$stock_total+return_value($data['stock']);

            }

             $stock_total=$stock_total+return_value2($in['extern_dispatch']);

                 	      $article_data = $this->db->query("SELECT internal_name, item_code, stock FROM pim_articles WHERE article_id = '".$in['article_id']."' ");
		                  $article_data->next();

                 	      $in['movement_id']=$this->db->insert("INSERT INTO stock_movements SET
	        													date 						=	'".time()."',
	        													created_by 					=	'".$_SESSION['u_id']."',
	        													article_id 					=	'".$in['article_id']."',
	        													article_name       			= 	'".addslashes($article_data->f('internal_name'))."',
	        													item_code  					= 	'".addslashes($article_data->f('item_code'))."',
	        													movement_type				=	'2',
	        													quantity 					= 	'0',
	        													stock 						=	'".$article_data->f('stock')."',
	        													new_stock 					= 	'0'
	        	           ");

                 	     $this->db->query("UPDATE  pim_articles set stock='".$stock_total."' WHERE article_id='".$in['article_id']."'") ;
                          $in['new_stock']=$this->db->field("SELECT pim_articles.stock FROM  pim_articles WHERE article_id='".$in['article_id']."'");

                         $this->db->query("update stock_movements SET

	        													quantity 					= 	'".($in['new_stock']-$article_data->f('stock'))."',

	        													new_stock 					= 	'".$in['new_stock']."'
	        									 		WHERE movement_id='".$in['movement_id']."'
	        	           ");


        $in['new_stock'] = remove_zero_decimals($in['new_stock']);


                 

 insert_message_log($this->pag,'{l}Stock set by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
   msg::success(gm("Stock succefully set."),'success');
   if($in['from_stock_tab']){
   		return true;
   }else{
   	  	json_out($in);
   }
 
	

	}*/

	function set_dispatch_stock(&$in)
	{
        $in['failure'] = false;
		if(!$this->set_dispatch_stock_validate($in)){
			 $in['failure'] = true;
			return false;
		}

		$stock_has_changed_error = false;

		foreach($in['query'] as $item=>$data){
			if($data['require_reason_stock_modif']=='1'){
				if(return_value($data['old_stock']) != return_value($data['stock'])){
					if(empty($data['edit_reason_stock_id']) || ($data['edit_reason_stock_id'] == 'undefined')){
						$stock_has_changed_error = true;	
						msg::error(('Edit Reason is mandatory'),'edit_reason_stock_id_'.$item);	
					}
				}
			}
		}

		if($stock_has_changed_error){
			json_out($in);
		}
         
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);

		$user_id = $_SESSION['u_id'];
		$user_name = addslashes($db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = '".$user_id."'"));

         $stock_total=0;
         $locations=array();

         foreach($in['query'] as $item=>$data){
               $this->db->query("DELETE FROM dispatch_stock WHERE address_id='".$data['address_id']."' AND article_id='".$in['article_id']."' and main_address=1");
               
                $this->db->insert("INSERT INTO dispatch_stock SET
	        												  article_id 		          = 	'".$in['article_id']."',
	        												  address_id 		          = 	'".$data['address_id']."',
	        												  main_address                =     '1',
	        												  stock 		              = 	'".return_value($data['stock'])."'

	        	                             ");

                        $stock_total=$stock_total+return_value($data['stock']);

              	if(return_value($data['old_stock']) != return_value($data['stock'])){
              		$edit_reason_name = $this->db->field("SELECT name FROM pim_stock_edit_reasons WHERE id='".$data['edit_reason_stock_id']."'");
              		if($data['require_reason_stock_modif']=='1'){
	              		$this->db->insert("INSERT INTO stock_edit_reasons SET
										  article_id = 	'".$in['article_id']."',
										  address_id = 	'".$data['address_id']."',
										  user_id = '".$user_id."',
										  username = '".$user_name."',
										  edit_reason_id = '".$data['edit_reason_stock_id']."',
										  edit_reason = 	'".addslashes($edit_reason_name)."',
										  date = '".time()."'

	                     ");
	              	}
              		$locations['0-'.$data['address_id']]=return_value($data['stock']);
              	}
              	
            }

             $stock_total=$stock_total+return_value2($in['extern_dispatch']);

                 	      $article_data = $this->db->query("SELECT internal_name, item_code, stock FROM pim_articles WHERE article_id = '".$in['article_id']."' ");
		                  $article_data->next();

                 	      $in['movement_id']=$this->db->insert("INSERT INTO stock_movements SET
	        													date 						=	'".time()."',
	        													created_by 					=	'".$_SESSION['u_id']."',
	        													article_id 					=	'".$in['article_id']."',
	        													article_name       			= 	'".addslashes($article_data->f('internal_name'))."',
	        													item_code  					= 	'".addslashes($article_data->f('item_code'))."',
	        													movement_type				=	'2',
	        													quantity 					= 	'0',
	        													stock 						=	'".$article_data->f('stock')."',
	        													new_stock 					= 	'0',
	        													location_info				= 	'".serialize($locations)."'
	        	           ");

                 	     $this->db->query("UPDATE  pim_articles set stock='".$stock_total."' WHERE article_id='".$in['article_id']."'") ;
                          $in['new_stock']=$this->db->field("SELECT pim_articles.stock FROM  pim_articles WHERE article_id='".$in['article_id']."'");

                         $this->db->query("update stock_movements SET

	        													quantity 					= 	'".($in['new_stock']-$article_data->f('stock'))."',

	        													new_stock 					= 	'".$in['new_stock']."'
	        									 		WHERE movement_id='".$in['movement_id']."'
	        	           ");


        $in['new_stock'] = remove_zero_decimals($in['new_stock']);


                 

 insert_message_log($this->pag,'{l}Stock set by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
   msg::success(gm("Stock succefully set."),'success');
   if($in['from_stock_tab']){
   		return true;
   }else{
   	  	json_out($in);
   }
 
	

	}

    function set_batch_dispatch_stock(&$in)
	{

        $in['failure'] = false;
		if(!$this->set_batch_dispatch_stock_validate($in)){
			 $in['failure'] = true;
			return false;
		}
         
		$article_id = $this->db->field("SELECT article_id FROM batches WHERE id = '".$in['batch_id']."' ");

         $stock_total=0;
         $locations=array();

         foreach($in['query'] as $item=>$data){

              $this->db->query("DELETE FROM batches_dispatch_stock WHERE address_id='".$data['address_id']."' AND batch_id='".$in['batch_id']."' ");

                $this->db->insert("INSERT INTO batches_dispatch_stock SET
	        												  batch_id 		          	  = 	'".$in['batch_id']."',
	        												  address_id 		          = 	'".$data['address_id']."',
	        												  quantity 		              = 	'".return_value($data['stock'])."',
	        												  article_id 				  =  	'".$article_id."'

	        	                             ");
                 $stock_address=0;
 
                $stock_address = $this->db->field("SELECT sum(quantity) FROM batches_dispatch_stock WHERE address_id='".$data['address_id']."' AND article_id='".$article_id."' "); 
  
                 $this->db->query("DELETE FROM dispatch_stock WHERE address_id='".$data['address_id']."' AND article_id='".$article_id."' and main_address=1");
               
                $this->db->insert("INSERT INTO dispatch_stock SET
	        												  article_id 		          = 	'".$article_id."',
	        												  address_id 		          = 	'".$data['address_id']."',
	        												  main_address                =     '1',
	        												  stock 		              = 	'".$stock_address."'

	        	                             ");
	        	 
	        	

                $stock_total=$stock_total+return_value($data['stock']);
             	if(return_value($data['old_stock']) != return_value($data['stock'])){
              		
              		$locations['0-'.$data['address_id']]=return_value($data['stock']);
              	}
            }


           

		 insert_message_log($this->pag,'{l}Batch stock set by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$article_id,false,$_SESSION['u_id']);
		  msg::success(gm("Stock succefully set."),'success');
		return true;
		  json_out($in);

	}


	/**
	 * set_threshold_value
	 *
	 * @return bool
	 * @author me
	 **/
	function set_threshold_value(&$in)
	{
       
       foreach($in['query'] as $item=>$data){
              $this->db->query("DELETE FROM article_threshold_dispatch WHERE customer_id='".$data['customer_id']."' AND article_id='".$in['article_id']."' AND address_id='".$data['address_id']."'");
              $this->db->insert("INSERT INTO article_threshold_dispatch SET
	        												  article_id 		          = 	'".$in['article_id']."',
	        												  address_id 		          = 	'".$data['address_id']."',
	        												  customer_id 		          = 	'".$data['customer_id']."',
	        												  value 		              = 	'".return_value($data['threshold_value'])."'



	        	                             ");

       }

		
        msg::success(gm("Threshold value succefully set."),'success');
        json_out($in);
		return true;

	}



	/**
	 * Backup tables for articles
	 * @param  array $in
	 * @return boolean
	 */
	/*function backup_data(&$in)
	{
		$this->db->query("show tables");
		$table_names=array(
			'0'			=>	'vats',
			'1'			=>	'pim_article_categories',
			'2'			=>	'pim_article_brands',
			'3'			=>	'pim_articles',
			'4'			=>	'stock_movements',
			'5'			=>	'pim_articles_lang',
			'6'			=>	'pim_article_prices',
			'7'			=>	'serial_numbers',
			'8'			=>	'serial_number_status',
			'9'			=>	'batches',
			'10'			=>	'batches_from_orders',
			'11'			=>	'batch_number_status'
		);
		while ($this->db->move_next()){
			if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'beckup_art_')!==FALSE)
			{
				$this->db2->query("DROP TABLE IF EXISTS ".$this->db->f("Tables_in_".DATABASE_NAME));
			}
		}
		for($i=0;$i<count($table_names);$i++){
	    	$this->db->query("CREATE table IF NOT EXISTS beckup_art_".$table_names[$i]." like `".DATABASE_NAME."`.".$table_names[$i].";");
	    	$this->db->query("TRUNCATE beckup_art_".$table_names[$i]);
	    	$this->db->query("INSERT beckup_art_".$table_names[$i]." select * from `".DATABASE_NAME."`.".$table_names[$i].";");
    	}
    	$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ART_DATA_BACKEDUP' ");

    	$time = time();
    	$this->db->query("UPDATE settings SET value={$time} WHERE constant_name='ART_DATA_BACKEDUP_TIMESTAMP' ");

    	$backup_date = date(ACCOUNT_DATE_FORMAT,$time);
		$backup_h_m = date('H:i',$time);
    	$in['backup_date'] = $backup_date.' , '.$backup_h_m;

    	if(CAN_SYNC){
    		//create backup .sql file
	    	global $database_config;
			global $config;

			//$config['front_address']='192.168.3.2';

	    	if($database_config['mysql']['database'] == '144ef87d_69c4_3d05_7bbf383e7711' ){
	    		$config['webshop_address']='5.9.104.242';
	    	}

			$dump_database_article_beckup=shell_exec("mysqldump 	-h ".$database_config['mysql']['hostname']." -u ".$database_config['mysql']['username']." -p".$database_config['mysql']['password']." ".$database_config['mysql']['database']." vats pim_article_categories pim_article_brands pim_articles stock_movements pim_articles_lang pim_article_prices > ../article_beckup_".$database_config['mysql']['database'].".sql 2>&1;");
    	}


    	msg::$success = gm('Data successfully saved');
    	return true;
	}
*/
	/**
	 * Restire articles
	 * @param  array $in
	 * @return boolean
	 */
	function get_backup_tables(&$in)
	{
		if(!$in['no_backup']){
			if($in['remove']){
				$columns = $this->db->query("SHOW COLUMNS FROM ".$in['table_name'])->getAll();
				$cols_array = array();
				$cols_info = array();
				foreach ($columns as $key => $value) {
					array_push($cols_array, $value['Field']);
					array_push($cols_info,$value);
				}
				$is_table = $this->db->query("SELECT COUNT(*) as table_name FROM information_schema.tables WHERE table_schema = '".DATABASE_NAME."' AND table_name = 'beckup_art_".$in['table_name']."'");
				$is_table->next();
				if($is_table->f('table_name'))
				{
					$backup_columns = $this->db->query("SHOW COLUMNS FROM beckup_art_".$in['table_name'])->getAll();
					$backup_cols_array = array();
					foreach ($backup_columns as $key => $value) {
						array_push($backup_cols_array, $value['Field']);
					}
					$res = array_diff($cols_array,$backup_cols_array);
					if($res)
					{
						foreach ($res as $key => $value) {
							$query = "ALTER TABLE beckup_art_".$in['table_name']." ADD `".$cols_info[$key]['Field']."` ".$cols_info[$key]['Type'];
							if($cols_info[$key]['Null']=='NO')
							{
								$query .= " NOT NULL";
							}else
							{
								$query .= " NULL";
							}
							if($cols_info[$key]['Default'])
							{
								$query .= " DEFAULT '".$cols_info[$key]['Default']."'";
							}
							// echo $query."<br>";
							console::log("Database: ".DATABASE_NAME."\n\tTable: ".$in['table_name']."<br>\t".$query);
							$this->db->query($query);
						}
					}
				}



				$this->db->query("TRUNCATE ".$in['table_name']);
				$this->db->query("INSERT ".$in['table_name']." SELECT * FROM beckup_art_".$in['table_name']."");
				$this->db->query("DROP TABLE beckup_art_".$in['table_name']."");



			}
			$this->db->query("show tables");
			$table_names=array();
			while ($this->db->move_next()){
				if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'beckup_art') !== FALSE )
				{
					$table_names[]=str_replace('beckup_art_','',$this->db->f("Tables_in_".DATABASE_NAME));
				}
			}
			if($table_names){
				$in['table_names'] = $table_names;
			}
			$in['table_nr'] = count($table_names);
		}else{
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ART_DATA_BACKEDUP' ");

			if(CAN_SYNC){
			//sync articles on front
			global $database_config;
			global $config;

		//	$config['front_address']='192.168.3.2';

	    	if($database_config['mysql']['database'] == '144ef87d_69c4_3d05_7bbf383e7711' ){
	    		$config['webshop_address']='5.9.104.242';
	    	}
			$replicate_user =shell_exec("mysql -h ".$config['webshop_address']." -u ".$config['webshop_user']." -p".$config['webshop_password']." ".$database_config['mysql']['database']." < ../article_beckup_".$database_config['mysql']['database'].".sql 2>&1;");
			//delete .sql file
			$remove_user_file=shell_exec("rm ../article_beckup_".$database_config['mysql']['database'].".sql 2>&1;");
			}


			msg::$success = gm('Data successfully restored');
		}
    	return true;
	}


/**
	 * validate delivery
	 *
	 * @return bool
	 * @author me
	 **/
	function set_dispatch_stock_validate(&$in)
	{
		$v = new validation($in);
		$v->field('article_id', 'ID', 'required:exist[pim_articles.article_id]', "Invalid ID");


			return $v->run();

	}

	function set_batch_dispatch_stock_validate(&$in)
	{
		$v = new validation($in);
		$v->field('batch_id', 'ID', 'required:exist[batches.id]', "Invalid ID");


			return $v->run();

	}


	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author me
	 **/
	function set_threshold_value_validate(&$in)
	{
		$v = new validation($in);
		$v->field('article_id', 'ID', 'required:exist[pim_articles.article_id]', "Invalid ID");
        return $v->run();

	}


	/****************************************************************
	* function activate_checkbox(&$in)                              *
	****************************************************************/
	function block_discount(&$in)
	{

		if(!$in['article_id']){
			
			msg::success(gm("Invalid ID"),'error');
			return false;
		}

		
		$table = $in['table'];
		$field_name = $in['name'];
		$id_name = $in['id_name'];
		$id_val = $in['article_id'];
		$activate_checkbox = $this->db->query("UPDATE $table SET $field_name = '1' WHERE $id_name = $id_val ");

		 insert_message_log($this->pag,'{l}Discount blocked by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Article successfully updated."),'success');
		return true;
	}

	function unblock_discount(&$in)
	{

		if(!$in['article_id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}

		
		$table = $in['table'];
		$field_name = $in['name'];
		$id_name = $in['id_name'];
		$id_val = $in['article_id'];
		$activate_checkbox = $this->db->query("UPDATE $table SET $field_name = '0' WHERE $id_name = $id_val ");

		insert_message_log($this->pag,'{l}Discount unblocked by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Article successfully updated."),'success');
		return true;
	}

	function add_serial_number(&$in)
	{
		if(!$this->add_serial_number_validate($in)){
			return false;
		}

		if(!$in['article_id'] || !trim($in['serial_number'])){
			msg::success(gm("Invalid ID"));
			return false;
		}

		$now = time();

		$serial_numbers = explode('<br />',nl2br($in['serial_number']));
		$added==0;
		foreach ($serial_numbers as $key => $serial_number) {
			if(trim($serial_number)){
				$serial_number_exist = $this->db->field("SELECT serial_number FROM serial_numbers WHERE article_id = '".$in['article_id']."' AND serial_number = '".trim($serial_number)."' ");
				if($serial_number_exist){
					$existing_s_n .= $serial_number_exist.'<br />';
				}else{
					$main_address_id = $this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default = '1' ");
					$status_details_1 = 'Added manually';
					$this->db->query("INSERT INTO serial_numbers
								SET 	serial_number 		='".trim($serial_number)."' ,
									article_id 			='".$in['article_id']."',
									date_in 			='".$now."',
									status_id 			='1',
									status_details_1 		='".$status_details_1."',
									address_id 			='".$main_address_id."'  ");
					$added++;
				}

			}
		}

		//$serial_number_nr = $this->db->field("SELECT count(serial_number) FROM serial_numbers WHERE article_id = '".$in['article_id']."' and status_id='1'");

		$allow_article_packing = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$allow_article_sale_unit = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
	
		$article_data = $this->db->query("SELECT stock, packing, sale_unit, hide_stock FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$article_data->next();
		$current_in_stock = $article_data->f("stock");

		$stock_packing = $article_data->f("packing");
		if(!$allow_article_packing){
			$stock_packing = 1;
		}
		$stock_sale_unit = $article_data->f("sale_unit");
		if(!$allow_article_sale_unit){
			$stock_sale_unit = 1;
		}
        
        //$new_stock=$serial_number_nr;
         $new_stock=$current_in_stock + $added*$stock_packing/$stock_sale_unit;

		$this->db->query("UPDATE pim_articles set stock='".$new_stock."' WHERE article_id='".$in['article_id']."'");
		

		$main_address_id=$this->db->field("SELECT address_id FROM  dispatch_stock_address  WHERE is_default='1'");
		if($main_address_id){
			$this->db->query("UPDATE dispatch_stock set stock='".$new_stock."' WHERE article_id='".$in['article_id']."' 
				and address_id='".$main_address_id."'");
		}

		
		if($existing_s_n){
			msg::success(gm("The following serial numbers already exist").':<br />'.rtrim($existing_s_n,'<br />'),'notice');
		}
		msg::success(gm("Changes saved"),'success');
		return true;
	}
	function add_serial_number_validate(&$in){
		$v = new validation($in);

		if (ark::$method == 'update_serial_number') {
			$option = ".id != '".$in['id']."' ";
		}else {
			$option = ".article_id = '".$in['article_id']."'";
		}
		//var_dump('required:unique[serial_numbers.serial_number' . $option . ']');exit();
		$v -> f('serial_number', 'Serial Number', 'required:unique[serial_numbers.serial_number' . $option . ']');

		return $v -> run();
	}

	function delete_serial_number(&$in)
	{
		

		if(!$in['article_id'] || !$in['id']){
			msg::success(gm("Invalid ID"));
			return false;
		}
		$old_address_id=$this->db->field("SELECT address_id FROM serial_numbers WHERE id = '".$in['id']."'");
		$stock_old_address = $this->db->field("SELECT stock FROM  dispatch_stock WHERE address_id='".$old_address_id."' AND article_id='".$in['article_id']."'");

		$this->db->query("DELETE FROM serial_numbers WHERE id='".$in['id']."' ");

/*		$current_in_stock=$this->db->field("SELECT stock FROM pim_articles  WHERE article_id='".$in['article_id']."'");
        $serial_number_nr = $this->db->field("SELECT count(serial_number) FROM serial_numbers WHERE article_id = '".$in['article_id']."' and status_id='1' ");
        $new_stock=$serial_number_nr;
        //$new_stock = $current_in_stock-1;*/

        $allow_article_packing = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$allow_article_sale_unit = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
	
		$article_data = $this->db->query("SELECT stock, packing, sale_unit, hide_stock FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$article_data->next();
		$current_in_stock = $article_data->f("stock");

		$stock_packing = $article_data->f("packing");
		if(!$allow_article_packing){
			$stock_packing = 1;
		}
		$stock_sale_unit = $article_data->f("sale_unit");
		if(!$allow_article_sale_unit){
			$stock_sale_unit = 1;
		}
        
        $new_stock=$current_in_stock - $stock_packing/$stock_sale_unit;
        
		$this->db->query("UPDATE pim_articles set stock='".$new_stock."' WHERE article_id='".$in['article_id']."'");
/*		$main_address_id=$this->db->field("SELECT address_id FROM  dispatch_stock_address  WHERE is_default='1'");
		if($main_address_id){
			$this->db->query("UPDATE dispatch_stock set stock='".$new_stock."' WHERE article_id='".$in['article_id']."' 
				and address_id='".$main_address_id."'");
		}*/
		if($old_address_id){
			$stock_old_address = $stock_old_address - $stock_packing/$stock_sale_unit;
			$this->db->query("UPDATE dispatch_stock set stock='".$stock_old_address."' WHERE article_id='".$in['article_id']."' 
				and address_id='".$old_address_id."'");
		}
		msg::success(gm('Serial number deleted.'),'success');
		return true;
	}

	function save_article_tax(&$in)
	{
		if(!$in['article_id']){
			msg::$error = gm("Invalid ID");
			return false;
		}
	
		$this->db->query("DELETE FROM pim_articles_taxes WHERE article_id='".$in['article_id']."'");
		
		foreach($in['query'] as $item=>$data){
			
          if($data['tax']){
			  $this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$data['tax_id']."'

				");
		     }
		}
		
		//Insert log message for services
		$articleService = $this->db->query('SELECT article_id,is_service FROM pim_articles WHERE article_id ="'.$in['article_id'].'"')->getAll();
		$articleService = $articleService[0];
		if($articleService['is_service']){
			insert_message_log('aservice','{l}Service taxes updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);	
		} else {
			insert_message_log($this->pag,'{l}Article taxes updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);			
		}
		//End Insert log message for services

		 msg::success(gm("Changes saved."),'success');
		 json_out($in);
		return true;
	}

	function hide_stock(&$in){
		if(!$in['article_id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}
		
		$this->db->query("UPDATE pim_articles SET hide_stock = '".$in['hide_stock']."' WHERE article_id = '".$in['article_id']."' ");

		
		insert_message_log($this->pag,'{l}Article stock hided by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Changes saved"),'success');
		return true;
	}

	function use_serial_no(&$in){
		if(!$in['article_id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}
		if(!ALLOW_STOCK && $in['use_serial_no']==1){
			msg::success(gm("You have to activate Stock"),'success');
			return false;
		}
		$in['block_serial_numbers'] = 0;
		$art_no_used = $this->db->field("SELECT COUNT(id) FROM serial_numbers WHERE article_id = '".$in['article_id']."' ");
		if($art_no_used > 0 && $in['use_serial_no']==0){
			if(ALLOW_STOCK){
				msg::success(gm("This article uses serial_numbers"),'error');
				$in['block_serial_numbers'] = 1;
				return false;
			}

		}
		$this->db->query("UPDATE pim_articles SET use_serial_no = '".$in['use_serial_no']."' WHERE article_id = '".$in['article_id']."' ");

		if($art_no_used > 0 && $in['use_serial_no']==1){
			$in['block_serial_numbers'] = 1;
		}
		//we set stock to 0
		if($in['use_serial_no']==1){
			$this->db->query("UPDATE pim_articles SET stock = '0' WHERE article_id = '".$in['article_id']."' ");
			$this->db->query("UPDATE dispatch_stock SET stock = '0' WHERE article_id = '".$in['article_id']."' ");
			
		}

		//insert_message_log($this->pag,'{l}Article updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Changes saved"),'success');
		return true;
	}

	function update_serial_number(&$in)
	{
		if(!trim($in['serial_number']) || !$in['id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}
		if(!$this->add_serial_number_validate($in)){
			msg::success(gm("Serial Number already exist"),'error');
			
			return false;
		}
		$in['date_in2'] = strtotime($in['date_in']);
		$in['date_out2'] = strtotime($in['date_out']);
		
		$old_status_id=$this->db->field("SELECT status_id FROM serial_numbers WHERE id = '".$in['id']."'");
		$old_address_id=$this->db->field("SELECT address_id FROM serial_numbers WHERE id = '".$in['id']."'");

		$this->db->query("UPDATE serial_numbers
					SET 	serial_number 	= '".$in['serial_number']."',
						date_in 		= '".$in['date_in2']."',
						date_out 		= '".$in['date_out2']."',
						status_id		= '".$in['status_id']."',
						status_details_1 	= '".$in['status_details_1']."',
						status_details_2 	= '".$in['status_details_2']."',
						status_details_3 	= '".$in['status_details_3']."',
						address_id 			= '".$in['address_id']."'
					WHERE id = '".$in['id']."'");
		$in['article_id']=$this->db->field("SELECT article_id FROM serial_numbers WHERE id = '".$in['id']."'");

	/*	$serial_number_nr = $this->db->field("SELECT count(serial_number) FROM serial_numbers WHERE article_id = '".$in['article_id']."' and status_id='1' ");
        $new_stock=$serial_number_nr; //add sales and packaging * $stock_packing/$stock_sale_unit*/

     
	    $allow_article_packing = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$allow_article_sale_unit = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
	
		$article_data = $this->db->query("SELECT stock, packing, sale_unit, hide_stock FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$article_data->next();
		//$current_in_stock = $article_data->f("stock");
		$current_in_stock = $this->db->field("SELECT count(serial_number) FROM serial_numbers WHERE article_id = '".$in['article_id']."' and status_id='1' ");


		$stock_packing = $article_data->f("packing");
		if(!$allow_article_packing){
			$stock_packing = 1;
		}
		$stock_sale_unit = $article_data->f("sale_unit");
		if(!$allow_article_sale_unit){
			$stock_sale_unit = 1;
		}

		$current_in_stock = $current_in_stock * $stock_packing/$stock_sale_unit;

		if($old_address_id!=$in['address_id'] ){
			$stock_old_address = 0;
			$stock_new_address = 0;
			$stock_old_address = $this->db->field("SELECT stock FROM  dispatch_stock WHERE address_id='".$old_address_id."' AND article_id='".$in['article_id']."'");
			$stock_new_address = $this->db->field("SELECT stock FROM  dispatch_stock WHERE address_id='".$in['address_id']."' AND article_id='".$in['article_id']."'");
			$if_stock_new_address = $this->db->field("SELECT dispatch_stock_id FROM  dispatch_stock WHERE address_id='".$in['address_id']."' AND article_id='".$in['article_id']."'");
			$stock_new_address= $stock_new_address+ $stock_packing/$stock_sale_unit;
			$stock_old_address= $stock_old_address-$stock_packing/$stock_sale_unit;;
			if($if_stock_new_address){
				$this->db->query("UPDATE dispatch_stock set stock='".$stock_new_address."' WHERE article_id='".$in['article_id']."' 
									and address_id='".$in['address_id']."'");
			}else{

				$this->db->query("INSERT INTO dispatch_stock SET
								  stock='".$stock_new_address."',
								 article_id='".$in['article_id']."',
								 address_id='".$in['address_id']."'	");					
			}
			$this->db->query("UPDATE dispatch_stock set stock='".$stock_old_address."' WHERE article_id='".$in['article_id']."' 
									and address_id='".$old_address_id."'");
		}   

		if($old_status_id!=$in['status_id'] ){

			$new_stock=$current_in_stock;
		    $this->db->query("UPDATE pim_articles set stock='".$new_stock."' WHERE article_id='".$in['article_id']."'");
			/*$main_address_id=$this->db->field("SELECT address_id FROM  dispatch_stock_address  WHERE is_default='1'");
			if($main_address_id){
				$this->db->query("UPDATE dispatch_stock set stock='".$new_stock."' WHERE article_id='".$in['article_id']."' 
									and address_id='".$main_address_id."'");
			}*/

			/*if($in['status_id']=='2' && $old_status_id !='3'){       
		        $new_stock=$current_in_stock - $stock_packing/$stock_sale_unit;
		        $this->db->query("UPDATE pim_articles set stock='".$new_stock."' WHERE article_id='".$in['article_id']."'");
				$main_address_id=$this->db->field("SELECT address_id FROM  dispatch_stock_address  WHERE is_default='1'");
					if($main_address_id){
							$this->db->query("UPDATE dispatch_stock set stock='".$new_stock."' WHERE article_id='".$in['article_id']."' 
									and address_id='".$main_address_id."'");
						}
		    }elseif($in['status_id']=='1'  && $old_status_id !='3' ){
		    	$new_stock=$current_in_stock + $stock_packing/$stock_sale_unit;
		    	$this->db->query("UPDATE pim_articles set stock='".$new_stock."' WHERE article_id='".$in['article_id']."'");
				$main_address_id=$this->db->field("SELECT address_id FROM  dispatch_stock_address  WHERE is_default='1'");
					if($main_address_id){
							$this->db->query("UPDATE dispatch_stock set stock='".$new_stock."' WHERE article_id='".$in['article_id']."' 
									and address_id='".$main_address_id."'");
						}
		    }*/

		    if($in['status_id']=='2' && $old_status_id !='3'){   
  				$current_in_stock_addr = $this->db->field("SELECT stock FROM  dispatch_stock WHERE address_id='".$in['address_id']."' AND article_id='".$in['article_id']."'");  
		        $new_stock=$current_in_stock_addr - $stock_packing/$stock_sale_unit;
				$this->db->query("UPDATE dispatch_stock set stock='".$new_stock."' WHERE article_id='".$in['article_id']."' 
									and address_id='".$in['address_id']."'");

		    }elseif($in['status_id']=='1'  && $old_status_id !='3' ){
		    	$current_in_stock_addr = $this->db->field("SELECT stock FROM  dispatch_stock WHERE address_id='".$in['address_id']."' AND article_id='".$in['article_id']."'");  
		    	$new_stock=$current_in_stock_addr + $stock_packing/$stock_sale_unit;
				$this->db->query("UPDATE dispatch_stock set stock='".$new_stock."' WHERE article_id='".$in['article_id']."' 
									and address_id='".$in['address_id']."'");

		    }
		 		 
		}     
	

		msg::success(gm("Changes saved"),'success');
		return true;
	}

/*	function show_art_serial_nr_del(&$in){
		if(!ALLOW_STOCK && $in['show_art_s_n_order_pdf']==1){
			msg::$error = gm("You have to activate Stock");
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['show_art_s_n_order_pdf']."' WHERE constant_name = 'SHOW_ART_S_N_ORDER_PDF' ");
		msg::$success = gm('Changes saved');
		return true;
	}

	function show_art_serial_nr_inv(&$in){
		if(!ALLOW_STOCK && $in['show_art_s_n_invoice_pdf']==1){
			msg::$error = gm("You have to activate Stock");
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['show_art_s_n_invoice_pdf']."' WHERE constant_name = 'SHOW_ART_S_N_INVOICE_PDF' ");
		msg::$success = gm('Changes saved');
		return true;
	}*/

	function add_batch(&$in)
	{

		if(!$this->add_batch_validate($in)){
			return false;
		}

		/*printvar($in);
		exit();*/
			$now = time(); 
		$status_details_1 = gm('Added manually');
		$article_data = $this->db->query("SELECT stock, internal_name, item_code FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$stock= $article_data->f('stock');
		$exist_batch=$this->db->field("SELECT count(*) FROM batches WHERE article_id='".$in['article_id']."' ");
		//if there is no batch, we reset the article stock and the user will add the batches and the coresponding stock manually (for that case when the option "use batches" was not checked from the begining)
		if($exist_batch==0 && $stock>0){
			$this->db->query("UPDATE pim_articles SET stock=0 WHERE article_id='".$in['article_id']."' ");
		}
        $in['date_exp2'] = strtotime($in['date_exp']);
		$in['batch_id']=$this->db->insert("INSERT INTO batches SET batch_number 		= '".$in['batch_number']."',
									article_id 			= '".$in['article_id']."',
									quantity 			= '".return_value($in['quantity'])."',
									in_stock 			= '".return_value($in['quantity'])."',
									status_id			= '1',
									status_details_1 		= '". $status_details_1 ."',
									date_in 			= '".$now."',
									date_exp 			='".$in['date_exp2']."' ");
		
		$new_stock = $stock + return_value($in['quantity']);

		$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id']."'");

		//we update the stock in the main location

		$main_address_id = $this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");

		$this->db->query("UPDATE dispatch_stock SET stock= '".$new_stock."'
				                                      WHERE  article_id='".$in['article_id']."' and address_id='".$main_address_id."'" );
		// we add stock for batches at main location

		$this->db->insert("INSERT INTO batches_dispatch_stock SET batch_id 		= '".$in['batch_id']."',
									quantity 			= '".return_value($in['quantity'])."',
									address_id 			= '".$main_address_id."',
									article_id 			= '".$in['article_id']."' ");

        $this->db->insert("INSERT INTO stock_movements SET
        													date 						=	'".time()."',
        													created_by 					=	'".$_SESSION['u_id']."',
        													article_id 					=	'".$in['article_id']."',
        													article_name       			= 	'".$article_data->f('internal_name')."',
        													item_code  					= 	'".$article_data->f('item_code')."',
        													movement_type				=	'2',
        													quantity 					= 	'".return_value($in['quantity'])."',
        													stock 						=	'".$stock."',
        													new_stock 					= 	'".$new_stock."'
        													");

		
		// insert_message_log($this->pag,'{l}Article updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Changes saved"),'success');
		return true;
	}

	function add_batch_validate(&$in){

		if(!$in['article_id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}
		if(!$in['batch_number'] || !$in['quantity']){
			msg::success(gm("Fields cannot be empty"),'error');
			
			return false;
		}
		

		if (ark::$method == 'update_batch_number') {
			$in['article_id']=$this->db->field("SELECT article_id FROM batches WHERE id = '".$in['id']."'");
			if(!$in['id']){
			    msg::success(gm("Invalid ID"),'error');
				return false;
			}
			/*$batch_number = $this->db->field("SELECT batch_number FROM batches
									WHERE
									article_id != '".$in['article_id']."'
									AND
									id != '".$in['id']."'
									AND
									batch_number = '".$in['batch_number']."' ");

			if($batch_number==$in['batch_number'] && UNIQUE_BATCH_NR==1 ){
				
				msg::success(gm("Batch number already exists"),'error');
				return false;
			}*/

		}else {
					$batch_number = $this->db->field("SELECT batch_number FROM batches
											WHERE
											article_id = '".$in['article_id']."'
											AND
											batch_number = '".$in['batch_number']."' ");
					if($batch_number == $in['batch_number'] && UNIQUE_BATCH_NR==1){
						
						msg::success(gm("Batch number already exists"),'error');
						return false;
					}

		}
		if(!is_numeric(return_value($in['quantity']))){
			msg::success(gm("Article quantity field must be numeric"),'error');
			
			return false;
		}
		return true;
	}

	function update_batch_number(&$in)
	{
		if(!trim($in['batch_number']) || !$in['id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}
		$in['article_id']=$this->db->field("SELECT article_id FROM batches WHERE id = '".$in['id']."'");
		if(!$this->add_batch_validate($in)){
			return false;
		}
		
		$batch_stock = $this->db->field("SELECT in_stock FROM batches WHERE id = '".$in['id']."'");
        

        $in['date_in2'] = strtotime($in['date_in']);
		$in['date_out2'] = strtotime($in['date_out']);
		$in['date_exp2'] = strtotime($in['date_exp']);

		$in['in_stock'] = return_value($in['in_stock']);
		if($in['locations'] && is_array($in['locations'])){
			if(count($in['locations'])>1){
				$in['in_stock']=0;
				foreach($in['locations'] as $location){
					$in['in_stock']+=return_value($location['stock']);
				}
			}
		}
		
		$in['quantity'] = return_value($in['quantity']);

		$this->db->query("UPDATE batches
					SET 	batch_number 	= '".$in['batch_number']."',
						date_in 		= '".$in['date_in2']."',
						date_out 		= '".$in['date_out2']."',
						date_exp 			='".$in['date_exp2']."', 
						status_id		= '".$in['status_id']."',
						in_stock 		= '".$in['in_stock']."',
						quantity 		= '".$in['quantity']."',
						status_details_1 	= '".$in['status_details_1']."',
						status_details_2 	= '".$in['status_details_2']."',
						status_details_3 	= '".$in['status_details_3']."'
					WHERE id = '".$in['id']."'");

		$article_data = $this->db->query("SELECT stock, internal_name, item_code FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$stock= $article_data->f('stock');
		$new_stock = $this->db->field("SELECT SUM(in_stock) FROM batches WHERE article_id='".$in['article_id']."' ");

		if ($batch_stock>$in['in_stock']) {
			$new_batch_stock = $batch_stock - $in['in_stock'];
			$greater =1;
			//$new_stock = $stock - $new_batch_stock;
			//$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id']."'");
		} else {
			$new_batch_stock = $in['in_stock'] -$batch_stock;
			$greater =0;
			//$new_stock = $stock + $new_batch_stock;
			//$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id']."'");
		}
		$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id']."'");

		if($in['locations'] && is_array($in['locations']) && count($in['locations'])>1){
			foreach($in['locations'] as $location){
				$stock_batch_exists=$this->db->field("SELECT dispatch_stock_id FROM batches_dispatch_stock WHERE  article_id='".$in['article_id']."' and address_id='".$location['address_id']."' and batch_id='".$in['id']."'");
				if($stock_batch_exists){
					$this->db->query("UPDATE batches_dispatch_stock SET quantity= '".return_value($location['stock'])."'
					WHERE  article_id='".$in['article_id']."' and address_id='".$location['address_id']."' and batch_id='".$in['id']."' " );
				}else{
					$this->db->query("INSERT INTO batches_dispatch_stock SET quantity= '".return_value($location['stock'])."', article_id='".$in['article_id']."', address_id='".$location['address_id']."', batch_id='".$in['id']."' " );
				}	

				$address_stock = $this->db->field("SELECT COALESCE(SUM(quantity),0) FROM batches_dispatch_stock WHERE address_id='".$location['address_id']."' AND article_id='".$in['article_id']."' "); 

				$dispatch_stock_exists=$this->db->field("SELECT dispatch_stock_id FROM dispatch_stock WHERE article_id='".$in['article_id']."' and address_id='".$location['address_id']."' ");
				if($dispatch_stock_exists){
					$this->db->query("UPDATE dispatch_stock SET stock= '".$address_stock."'
					WHERE article_id='".$in['article_id']."' and address_id='".$location['address_id']."'" );
				}else{
					$this->db->query("INSERT INTO dispatch_stock SET stock= '".$address_stock."',article_id='".$in['article_id']."',address_id='".$location['address_id']."'" );
				}		
			}
		}else{
			$main_address_id = $this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
			$main_address_stock = $this->db->field("SELECT stock FROM dispatch_stock WHERE article_id='".$in['article_id']."' and address_id='".$main_address_id."'");
			$main_address_stock_batch = $this->db->field("SELECT quantity FROM batches_dispatch_stock WHERE article_id='".$in['article_id']."' and address_id='".$main_address_id."' and batch_id='".$in['id']."'");

			if($greater){
				$main_address_stock = $main_address_stock - $new_batch_stock;
				$main_address_stock_batch = $main_address_stock_batch - $new_batch_stock;
			}else{
				$main_address_stock = $main_address_stock + $new_batch_stock;
				$main_address_stock_batch = $main_address_stock_batch + $new_batch_stock;
			}

			$this->db->query("UPDATE dispatch_stock SET stock= '".$main_address_stock."'
				WHERE  article_id='".$in['article_id']."' and address_id='".$main_address_id."'" );

			$this->db->query("UPDATE batches_dispatch_stock SET quantity= '".$main_address_stock_batch."'
				WHERE  article_id='".$in['article_id']."' and address_id='".$main_address_id."' and batch_id='".$in['id']."' " );
		}

        $this->db->insert("INSERT INTO stock_movements SET
        													date 						=	'".time()."',
        													created_by 					=	'".$_SESSION['u_id']."',
        													article_id 					=	'".$in['article_id']."',
        													article_name       			= 	'".$article_data->f('internal_name')."',
        													item_code  					= 	'".$article_data->f('item_code')."',
        													movement_type				=	'2',
        													quantity 					= 	'".$new_batch_stock."',
        													stock 						=	'".$stock."',
        													new_stock 					= 	'".$new_stock."'
        													");

		
		
		msg::$success = gm("Changes saved");
		return true;
	}

	function use_combined(&$in){
		if(!$in['article_id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}


		$in['block_combined'] = 0;
		$art_no_used = $this->db->field("SELECT COUNT(pim_articles_combined_id) FROM  pim_articles_combined WHERE parent_article_id = '".$in['article_id']."' ");
		if($art_no_used > 0 && $in['use_combined']==0){
			//msg::$error = gm("This article uses batches");
			$in['block_combined'] = 1;
			return false;
		}
		$this->db->query("UPDATE pim_articles SET use_combined = '".$in['use_combined']."' WHERE article_id = '".$in['article_id']."' ");
		if($art_no_used > 0 && $in['use_combined']==1){
			$in['block_combined'] = 1;
		}
		msg::success(gm("Changes saved"),'success');
		return true;
	}
	function use_batch_no(&$in){
		if(!$in['article_id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}

		if(!ALLOW_STOCK && $in['use_batch_no']==1){
			msg::success(gm("You have to activate Stock"),'error');
			return false;
		}

		$in['block_batches'] = 0;
		$art_no_used = $this->db->field("SELECT COUNT(id) FROM batches WHERE article_id = '".$in['article_id']."' ");
		if($art_no_used > 0 && $in['use_batch_no']==0){
			msg::success(gm("This article uses batches"),'error');
			$in['block_batches'] = 1;
			return false;
		}
		$article = $this->db->query("SELECT internal_name, item_code, stock FROM pim_articles WHERE article_id = '".$in['article_id']."' ");
		
		$this->db->query("UPDATE pim_articles SET use_batch_no = '".$in['use_batch_no']."', stock='0' WHERE article_id = '".$in['article_id']."' ");
		$this->db->query("UPDATE dispatch_stock SET stock= '0' WHERE  article_id='".$in['article_id']."' ");
		$this->db->insert(" INSERT INTO stock_movements SET
			        													date 						=	'".time()."',
			        													created_by 					=	'".$_SESSION['u_id']."',
			        													article_id 					=	'".$in['article_id']."',
			        													article_name       			= 	'".$article->f('internal_name')."',
			        													item_code  					= 	'".$article->f('item_code')."',
			        													movement_type				=	'2',
			        													quantity 					= 	'0',
			        													stock 						=	'".$article->f('stock')."',
			        													new_stock 					= 	'0'
			        							");
		if($art_no_used > 0 && $in['use_batch_no']==1){
			$in['block_batches'] = 1;
		}
		//insert_message_log($this->pag,'{l}Article updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);
		msg::success(gm("Changes saved"),'success');
		return true;
	}


	function update_default_settings(&$in)
	{
		//$in['elem_name'] must be the name of the constant_name from table settings

		
		msg::success ( gm('Changes have been saved.'),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addRelatedArticle(&$in)
	{
		if(!$this->addRelatedArticle_validate($in)){
			return false;
		}
		$this->db->query("INSERT INTO pim_articles_related SET parent_article_id='".$in['parent_article_id']."', article_id='".$in['value']."' ");
		msg::$success = gm('Changes have been saved.');
		return true;
	}
/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addCombinedArticle(&$in)
	{
		if(!$this->addCombinedArticle_validate($in)){
			return false;
		}
		$is_ok = true;
		$exist = $this->db->field("SELECT pim_articles_combined_id FROM pim_articles_combined WHERE parent_article_id='".$in['parent_article_id']."' AND article_id='".$in['value']."' ");
		if($exist){
			msg::error(gm("Invalid ID"),'error');
			$is_ok = false;
		}else{
			$new_sort_order = 0;
			$last_sort_order = $this->db->field("SELECT sort_order FROM pim_articles_combined WHERE parent_article_id='".$in['parent_article_id']."' ORDER BY `sort_order` DESC");
			$new_sort_order = $last_sort_order +1;
			$this->db->query("INSERT INTO pim_articles_combined SET parent_article_id='".$in['parent_article_id']."', article_id='".$in['value']."', quantity='".$in['quantity']."', visible ='1',  sort_order ='".$new_sort_order."'");
			msg::success(gm('Changes have been saved.'),'success');
			$is_ok = true;
		}

		
		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addRelatedArticle_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('value', 'value Code', 'required');
		$v -> f('parent_article_id', 'parent_article_id', 'required');
		$is_ok = $v -> run();
		if($is_ok === true){
			$exist = $this->db->field("SELECT id FROM pim_articles_related WHERE parent_article_id='".$in['parent_article_id']."' AND article_id='".$in['value']."' ");
			if($exist){
				msg::$error = gm("Invalid ID");
				$is_ok = false;
			}
		}
		return $is_ok;
	}
/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addCombinedArticle_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('value', 'value Code', 'required');
		$v -> f('parent_article_id', 'parent_article_id', 'required');
		$is_ok = $v -> run();
		if($is_ok === true){
			$exist = $this->db->field("SELECT pim_articles_combined_id FROM pim_articles_combined WHERE parent_article_id='".$in['parent_article_id']."' AND article_id='".$in['value']."' ");
			if($exist){
				msg::success(gm("Invalid ID"),'error');
				$is_ok = false;
			}
		}
		return $is_ok;
	}
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteCombinedArticle(&$in)
	{
		if(!$this->deleteCombinedArticle_validate($in)){
			return false;
		}
		$this->db->query("DELETE FROM pim_articles_combined WHERE pim_articles_combined_id='".$in['pim_articles_combined_id']."' ");
		msg::success(gm('Changes have been saved.'),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteCombinedArticle_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('pim_articles_combined_id', 'id', 'required');
		return $v -> run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteRelatedArticle(&$in)
	{
		if(!$this->deleteRelatedArticle_validate($in)){
			return false;
		}
		$this->db->query("DELETE FROM pim_articles_related WHERE id='".$in['id']."' ");
		msg::$success = gm('Changes have been saved.');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteRelatedArticle_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('id', 'id', 'required');
		return $v -> run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function saveAddToPurchase(&$in)
	{
		foreach ($in['ids'] as $key) {
			preg_match('/[0-9]+/', $key['name'],$match);
			if($match){
				// array_push($_SESSION['add_to_pdf'], array($match[0] => 1));
				if($in['val']==1){
					$_SESSION['add_to_purchase'][$match[0]]= $in['val'];
				}else{
					unset($_SESSION['add_to_purchase'][$match[0]]);
				}
			}

		}
		return true;
	}

	function update_combined_order(&$in)
	{   
		if(!$this->update_combined_order_validate($in)){
			return false;
		}
		
		foreach ($in['order_arr'] as $key => $value) {
			if($value){
				$this->db->query("UPDATE pim_articles_combined SET sort_order='".$key."' where parent_article_id='".$in['parent_article_id']."' and  article_id='".$value."' ");
			}
		}
		
		msg::success(gm('Changes have been saved.'),'success');
		return true;
	}

		function update_combined_order_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('parent_article_id', 'Article Id', 'required:exist[pim_articles_combined.parent_article_id]');

		return $v -> run();
      }

    function update_combined_quantity(&$in)
	{
		if(!$this->update_combined_order_validate($in)){
			return false;
		}
		
		$this->db->query("UPDATE pim_articles_combined SET quantity='".return_value2($in['update_value'])."' where parent_article_id='".$in['parent_article_id']."' and  article_id='".$in['article_id']."' ");

		$in['new_quantity']=$this->db->field("SELECT pim_articles_combined.quantity FROM  pim_articles_combined WHERE parent_article_id='".$in['parent_article_id']."'  and article_id='".$in['article_id']."'");
		$in['new_quantity'] = remove_zero_decimals($in['new_quantity']);
           
        $combined= $this->db->query(" SELECT pim_articles_combined.* , pim_articles.stock FROM pim_articles_combined 
                            JOIN pim_articles WHERE pim_articles_combined.article_id = pim_articles.article_id
                            AND pim_articles_combined.parent_article_id ='".$in['parent_article_id']."' AND pim_articles.active='1'"); 
        $in['new_potential_stock']= $combined->f('stock')/$combined->f('quantity');
        while ($combined->next()){
    				$in['new_potential_stock']=min($in['new_potential_stock'], floor($combined->f('stock')/$combined->f('quantity')));
		}
        

		msg::success(gm('Changes have been saved.'),'success');


		return true;
	}
	/**
	 * Validate data
	 * @param  array $in
	 * @return boolean
	 */
	function update_combined_quantity_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles_combined.article_id]');
		$v -> f('parent_article_id', 'Article Id', 'required:exist[pim_articles_combined.parent_article_id]');
		// $v -> f('update_value', 'Stock', 'required:numeric');

		return $v -> run();
      }

      function set_article_visibility(&$in)
	{
		if(!$this->set_article_visibility_validate($in)){
			return false;
		}
		
		$this->db->query("UPDATE pim_articles_combined SET visible='".$in['visible']."' where parent_article_id='".$in['parent_article_id']."' and  article_id='".$in['article_id']."' ");

		msg::success(gm('Changes have been saved.'),'success');


		return true;
	}
	/**
	 * Validate data
	 * @param  array $in
	 * @return boolean
	 */
	function set_article_visibility_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles_combined.article_id]');
		$v -> f('parent_article_id', 'Article Id', 'required:exist[pim_articles_combined.parent_article_id]');

		return $v -> run();
      }

      function compose_combined(&$in)
	{
		if(!$this->compose_combined_validate($in)){
			return false;
		}
		$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
		$old_stock=$this->db->field("SELECT stock FROM pim_articles WHERE  article_id='".$in['article_id']."' ");
		$old_stock_main=$this->db->field("SELECT stock FROM dispatch_stock WHERE  article_id='".$in['article_id']."' and address_id='".$main_address_id."'");
		$this->db->query("UPDATE pim_articles SET stock='".(return_value2($in['compose'])+ $old_stock)."' WHERE article_id='".$in['article_id']."' ");



           //we update the stock in the main location
			$this->db->query("UPDATE dispatch_stock SET   stock= '".(return_value2($in['compose'])+$old_stock_main)."'
				                                      WHERE  article_id='".$in['article_id']."' and address_id='".$main_address_id."'"
                                                         );

        	$this->db->insert("INSERT INTO stock_movements SET
        													date 						=	'".time()."',
        													created_by 					=	'".$_SESSION['u_id']."',
        													article_id 					=	'".$in['article_id']."',
        													article_name       			= 	'".$in['internal_name']."',
        													item_code  					= 	'".$in['item_code']."',
        													movement_type				=	'11',
        													quantity 					= 	'".return_value2($in['compose'])."',
        													stock 						=	'".$old_stock."',
        													new_stock 					= 	'".(return_value2($in['compose']) + $old_stock)."'
        													");
           $in['new_stock']=return_value2($in['compose']) + $old_stock;

		//now we have to update the stock for the parts
        $this->db->query("SELECT pim_articles_combined.* FROM pim_articles_combined WHERE parent_article_id='".$in['article_id']."'");
         while ($this->db->move_next()) {
         	     $internal_name=$this->db2->field("SELECT internal_name FROM pim_articles WHERE  article_id='".$this->db->f('article_id')."' ");
         	     $item_code=$this->db2->field("SELECT item_code FROM pim_articles WHERE  article_id='".$this->db->f('article_id')."' ");
         	     $old_stock=$this->db2->field("SELECT stock FROM pim_articles WHERE  article_id='".$this->db->f('article_id')."' ");
		         $old_stock_main=$this->db2->field("SELECT stock FROM dispatch_stock WHERE   article_id='".$this->db->f('article_id')."'  and address_id='".$main_address_id."'");

                 $this->db2->query("UPDATE pim_articles SET stock='".( $old_stock- return_value2($in['compose'])*$this->db->f('quantity'))."' WHERE  article_id='".$this->db->f('article_id')."'  ");

                $this->db2->query("UPDATE dispatch_stock SET   stock= '".($old_stock_main-return_value2($in['compose'])*$this->db->f('quantity'))."'
				                                      WHERE  article_id='".$this->db->f('article_id')."' and address_id='".$main_address_id."'"
                                                         );
                $this->db2->insert("INSERT INTO stock_movements SET
        													date 						=	'".time()."',
        													created_by 					=	'".$_SESSION['u_id']."',
        													article_id 					=	'".$in['article_id']."',
        													article_name       			= 	'".$internal_name."',
        													item_code  					= 	'".$item_code."',
        													movement_type				=	'12',
        													quantity 					= 	'".(return_value2($in['compose'])*$this->db->f('quantity'))."',
        													stock 						=	'".$old_stock."',
        													new_stock 					= 	'".($old_stock - return_value2($in['compose'])*$this->db->f('quantity') )."'
        													");
         }

		msg::success(gm('Changes have been saved.'),'success');
		  //json_out($in);
		return true;
	}

	function compose_combined_validate(&$in)
	{
		if(!$in['compose']){
				msg::success(gm("Please fill in new items field"),'error');
				return false;
		}
      $this->db->query("SELECT pim_articles_combined.* FROM pim_articles_combined WHERE parent_article_id='".$in['article_id']."'");
         while ($this->db->move_next()) {

         	     $old_stock=$this->db2->field("SELECT stock FROM pim_articles WHERE  article_id='".$this->db->f('article_id')."' ");
		        // $old_stock_main=$this->db2->field("SELECT stock FROM dispatch_stock WHERE   article_id='".$this->db->f('article_id')."'  and address_id='".$main_address_id."'");


                if(($old_stock - return_value2($in['compose'])*$this->db->f('quantity') )<0){
                     	//msg::$error.= gm("Stock will be negative for some parts");
                     	msg::success(gm("Stock will be negative for some parts"),'error');
                     	return false;
                }


         }

		return true;

      }

  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function singleImgUpload(&$in)
  {
  	// console::log($in);
  	global $cfg;
  	$response = array();
  	if (!empty($_FILES)) {
  		$tempFile = $_FILES['Filedata']['tmp_name'];
  		$fName = $_FILES['Filedata']['name'];
  		$ext = pathinfo($fName, PATHINFO_EXTENSION);
  		$item_code = substr($fName,0,strrpos($fName, "."));
  		$parentId = $this->db->field("SELECT article_id FROM pim_articles WHERE item_code='".$item_code."' ");
  		if(!$parentId){
  			$response['error'] = "Article not found - ".$item_code;
				echo json_encode($response);
				return;
  		}else{
  			$param = array('parent_id'=>$parentId);
  			$this->delete_art_photos($param);
  		}
  		$targetPath = INSTALLPATH.UPLOAD_PATH . '/'.DATABASE_NAME.'/pim_article_photo/';
  		$img_id = $this->db->insert("INSERT INTO pim_article_photo SET parent_id='".$parentId."', upload_amazon=1");

  		$imgName = "img_".$parentId."_".$img_id.'.'.$ext;
			$targetFile =  str_replace('//','/',$targetPath) . $imgName;
			$this->db->query("UPDATE pim_article_photo SET file_name='".$imgName."' WHERE photo_id='".$img_id."' AND parent_id='".$parentId."' ");

			if(move_uploaded_file($tempFile,$targetFile) == FALSE)
			{
				$response['error'] = "File canot be moved - ".$targetPath;
				echo json_encode($response);
			}else {
				$size = getimagesize($targetFile);
				$width = $size[0];
				$height = $size[1];

        ark::loadLibraries(array('aws'));
        $a = new awsWrap(DATABASE_NAME);
        $pdfPath = $targetFile;//upload server

        $pdfFile = 'article/article_'.$parentId."_".$img_id.".".$ext;

        $a->uploadFile($pdfPath,$pdfFile);//upload amazon
        unlink($targetFile);//delete server
        $link =  $a->getLink($cfg['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext);
        $response['success'] = 'Uploaded - '.$item_code;

        $this->db->query("UPDATE pim_article_photo SET file_name='".$link."',amazon_path='".$cfg['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext."' WHERE photo_id='".$img_id."' AND parent_id='".$parentId."' ");
				echo json_encode($response);
			}
  	}

		return;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function delete_art_photos($in)
  {
  	$this->db->query("SELECT * FROM pim_article_photo WHERE  parent_id='".$in['parent_id']."'");
  	while ($this->db->next()) {
  		if($this->db->f('upload_amazon')==1){
    		$item = $this->db->f('file_name');
    		$link = $this->db->field("SELECT item FROM s3_links WHERE link='".$item."' ");
    		ark::loadLibraries(array('aws'));
        $a = new awsWrap(DATABASE_NAME);
        $a->deleteItem($link);
    	}else{
    		unlink($_SERVER['DOCUMENT_ROOT'].$this->db->f('folder')."/".$this->db->f('file_name'));
    	}
    	$this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['parent_id']."' AND photo_id='".$this->db->f('photo_id')."' ");
    	$commands = array();
    	array_push($commands, "DELETE FROM pim_article_photo WHERE parent_id='".$in['parent_id']."' AND photo_id='".$this->db->f('photo_id')."' ");
        // Sync::end();
        $packet = array(
					'command' => base64_encode(serialize($commands)),
					'command_id' => 1
				);
				Sync::send(WEB_URL, $packet);
  	}
  	return true;
  }


  	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function setFileDefault(&$in)
	{
		$this->db->query("UPDATE `attached_files` SET `default`='".$in['val']."' WHERE `file_id`='".$in['file_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteFile(&$in)
	{
		if(unlink($in['path'].$in['name']) === true){
			msg::$success = gm('File deleted');
		}
		rmdir($in['path']); //remove folder if it's empty
		$this->db->query("DELETE FROM `attached_files` WHERE `file_id`='".$in['file_id']."' ");
		return true;
	}

	function uploadify2(&$in)
	{
		$response = array();
		if (!empty($_FILES)) {
			if($in['folder']){
				global $database_config;
				$database_2 = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $in['folder'],
				);
				$db_upload = new sqldb($database_2);
			}
			$tempFile = $_FILES['Filedata']['tmp_name'];

			$in['name'] = $_FILES['Filedata']['name'];
			$targetPath = $in['path'];
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

			mkdir(str_replace('//','/',$targetPath), 0775, true);
			/*if(file_exists($targetFile)){
				echo 'File';
				return false;
			}*/
			move_uploaded_file($tempFile,$targetFile);
			$file_id = $db_upload->insert("INSERT INTO attached_files SET `path` = 'upload/".$in['folder']."/batches/".$in['batch_id']."/', name = '".$in['name']."', type='4',item_id='".$in['batch_id']."'");
			$response = array();
			$response['name'] = $in['name'];
			$response['path'] = $in['path'];
			$response['file_id'] = $file_id;
			echo json_encode($response);
		}
	}
	function article_settings(&$in){


		if(!$in['ALLOW_ARTICLE_PACKING']){
		   $in['ALLOW_ARTICLE_PACKING']=0;
		   $in['ALLOW_QUOTE_PACKING']=0;
		   $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}
		if(!$in['ALLOW_ARTICLE_SALE_UNIT'] || $in['ALLOW_ARTICLE_PACKING'] == 0 ){
		    $in['ALLOW_ARTICLE_SALE_UNIT']=0;
		}
		if(!$in['ALLOW_ARTICLE_SALE_UNIT']){
		    $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}
		if(!$in['ALLOW_QUOTE_PACKING']){
		   $in['ALLOW_QUOTE_PACKING']=0;
		   $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}

		if(!$in['ALLOW_QUOTE_SALE_UNIT'] || $in['ALLOW_QUOTE_PACKING'] == 0 ){
		    $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}


		$SHOW_TOTAL_MARGIN_QUOTE=$this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_TOTAL_MARGIN_QUOTE' ");
		if(is_null($SHOW_TOTAL_MARGIN_QUOTE)){
			$this->db->query("INSERT INTO settings SET constant_name='SHOW_TOTAL_MARGIN_QUOTE',value='".$in['SHOW_TOTAL_MARGIN_QUOTE']."'  ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['SHOW_TOTAL_MARGIN_QUOTE']."' WHERE constant_name='SHOW_TOTAL_MARGIN_QUOTE' ");
		}

		$this->db->query("UPDATE settings SET value='".$in['ALLOW_ARTICLE_PACKING']."' WHERE constant_name='ALLOW_ARTICLE_PACKING'");
		$this->db->query("UPDATE settings SET value='".$in['ALLOW_ARTICLE_SALE_UNIT']."' WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT'");

		$SHOW_TOTAL_MARGIN_ORDER=$this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_TOTAL_MARGIN_ORDER' ");
		if(is_null($SHOW_TOTAL_MARGIN_ORDER)){
			$this->db->query("INSERT INTO settings SET constant_name='SHOW_TOTAL_MARGIN_ORDER',value='".$in['SHOW_TOTAL_MARGIN_ORDER']."'  ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['SHOW_TOTAL_MARGIN_ORDER']."' WHERE constant_name='SHOW_TOTAL_MARGIN_ORDER' ");
		}

		$ALLOW_ARTICLE_VARIANTS=$this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_VARIANTS' ");
		if(is_null($ALLOW_ARTICLE_VARIANTS)){
			$this->db->query("INSERT INTO settings SET constant_name='ALLOW_ARTICLE_VARIANTS',value='".$in['ALLOW_ARTICLE_VARIANTS']."'  ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['ALLOW_ARTICLE_VARIANTS']."' WHERE constant_name='ALLOW_ARTICLE_VARIANTS' ");
		}

		$SHOW_ARTICLES_PDF=$this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_ARTICLES_PDF' ");
		if(is_null($SHOW_ARTICLES_PDF)){
			$this->db->query("INSERT INTO settings SET constant_name='SHOW_ARTICLES_PDF',value='".$in['SHOW_ARTICLES_PDF']."'  ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['SHOW_ARTICLES_PDF']."' WHERE constant_name='SHOW_ARTICLES_PDF' ");
		}

	  $this->db->query("UPDATE settings SET value='".$in['ALLOW_QUOTE_PACKING']."' WHERE constant_name='ALLOW_QUOTE_PACKING'");
		$this->db->query("UPDATE settings SET value='".$in['ALLOW_QUOTE_SALE_UNIT']."' WHERE constant_name='ALLOW_QUOTE_SALE_UNIT'");

/*		if($in['ARTICLE_PRICE_COMMA_DIGITS']==2 || $in['ARTICLE_PRICE_COMMA_DIGITS']==3 || $in['ARTICLE_PRICE_COMMA_DIGITS']==4){
			$this->db->query("UPDATE settings SET value='".$in['ARTICLE_PRICE_COMMA_DIGITS']."' WHERE constant_name='ARTICLE_PRICE_COMMA_DIGITS'");
		}
		$in['ARTICLE_PRICE_COMMA_DIGITS'] = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ARTICLE_PRICE_COMMA_DIGITS' ");*/

   if($in['WAC']==1 && !WAC){
      $this->db->query("UPDATE settings SET value='".strtotime('now')."' WHERE constant_name='WAC_ACT_DATE'");
       $this->db->query("SELECT  pim_articles.article_id FROM pim_articles");
       while ($this->db->move_next()) {
            generate_purchase_price($this->db->f('article_id'),1);
       }

    }
    $this->db->query("UPDATE settings SET value='".$in['WAC']."' WHERE constant_name='WAC'");
    $this->db->query("UPDATE settings SET value='".$in['AAC']."' WHERE constant_name='AAC'");
    $this->db->query("UPDATE settings SET value='".$in['AUTO_AAC']."' WHERE constant_name='AUTO_AAC'");
    if(!$in['AAC']){
       $this->db->query("UPDATE settings SET value='0' WHERE constant_name='AUTO_AAC'");
    }
    if($in['AUTO_AAC']==1 && !AUTO_AAC){
         $this->db->query("SELECT  pim_articles.article_id FROM pim_articles");
          while ($this->db->move_next()) {
              generate_aac_price($this->db->f('article_id'),1);
          }
    }


/*    if ($in['config']==1)
		{
			$this->db->query("UPDATE setup SET value=1 WHERE name='article_settings_ready'");
			$article_settings_ready = $this->db->field("SELECT value FROM setup WHERE name='article_settings_ready'");
			$article_price_ready = $this->db->field("SELECT value FROM setup WHERE name='article_price_ready'");
			$article_taxes_ready = $this->db->field("SELECT value FROM setup WHERE name='article_taxes_ready'");
			echo $article_price_ready."-".$article_settings_ready."-".$article_taxes_ready;
			if ($article_settings_ready==1 && $article_price_ready==1 && $article_taxes_ready==1)
			{
				$this->db->query("UPDATE setup SET value=1 WHERE name='articles_ready'");
				header("Location: index.php?do=home-account_configure");
			}
		}*/

		msg::success ( gm('Article settings has been successfully updated.'),'success');
		return true;

	}
	function use_fam_price(&$in) {

   	    	$this->db->query("UPDATE settings SET value='".$in['PRICE_TYPE']."' WHERE constant_name='PRICE_TYPE'");
  			$this->db->query("UPDATE settings SET value='".$in['USE_FAM_CUSTOM_PRICE']."' WHERE constant_name='USE_FAM_CUSTOM_PRICE'");
			msg::success ( gm("Price settings has been successfully updated"),'success');
			return true;
	}
	function add_price_category(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_price_category_validate($in)){
			$in['failure'] = true;
			return false;
		}
		
	
		
		$in['category_id'] = $this->db->insert("INSERT INTO pim_article_price_category SET name='".$in['name']."',
		                                                                                   type='".$in['category_type_id']."',
		                                                                                   price_type='".$in['price_type_id']."' ,
		                                                                                   price_value='".return_value($in['price_value'])."',
		                                                                                   price_value_type='".$in['price_value_type_id']."' 
		                                       ");
       
		
		

		

		  $article_prices = $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices WHERE base_price=1");
		

		while ($article_prices->next()){
             if($in['price_type_id']==2){
               $article_base_price=$article_prices->f('purchase_price');
             }else{
               $article_base_price=$article_prices->f('price');
             }
			//insert the article prices
			switch ($in['category_type_id']) {
				case 1:                  //discount
				if($in['price_value_type_id']==1){  // %
					$total_price = $article_base_price - (return_value($in['price_value']) * $article_base_price / 100);
				}else{ //fix
					$total_price = $article_base_price - $in['price_value'];
				}

				break;
				case 2:                 //profit margin
				if($in['price_value_type_id']==1){  // %
					$total_price = $article_base_price + (return_value($in['price_value']) * $article_base_price / 100);
				}else{ //fix
					$total_price =$article_base_price + return_value($in['price_value']);
				}

				break;

			}
			$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_prices->f('article_id')."'");
			$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			
		    $this->db->query("INSERT INTO pim_article_prices SET
				article_id			= '".$article_prices->f('article_id')."',
				price_category_id	= '".$in['category_id']."',
				price				= '".$total_price."',
				total_price			= '".$total_with_vat."'
				");

		}
		
 		
		msg::success ( "Article Price Category added.",'success');

		return true;
	}
	function add_price_category_validate(&$in){

		$v = new validation($in);
		if (ark::$method == 'update') {
			$option = ".(category_id != '".$in['category_id']."')";
		} else {
			$option = "";
		}

		$v->field('name','Name','required:unique[pim_article_price_category.name'.$option.']');
		$v->field('price_type_id','Type','required');
		//$v->field('price_value','Price Value','required:numeric');
		$v->field('price_value_type_id','Price Value Type','required');

		return $v->run();

	}
	function delete_price_category(&$in)	{
		
		if(!$this->delete_price_category_validate($in)){
			
			return false;
		}
		$this->db->query("DELETE FROM pim_article_price_category WHERE category_id='".$in['id']."' ");
		$this->db->query("DELETE FROM fam_custom_price WHERE category_id='".$in['id']."' ");
		$this->db->query("DELETE FROM pim_article_prices
					      WHERE price_category_id='".$in['id']."'");
		msg::success ( "Article Price Category deleted.",'success');
		
		return true;
	}
	function delete_price_category_validate(&$in){
		$v=new validation($in);
		$v->f('id','Category Id','required:exist[pim_article_price_category.category_id]');

		return $v->run();

	}
	function update_fam_price_list(&$in)
	{  
		if(!$this->update_fam_price_list_validate($in)){
           return false;
		}
		$this->db->query("DELETE FROM  fam_custom_price WHERE fam_id = '".$in['fam_id']."' AND category_id = '".$in['category_id']."'");
        $this->db->query("INSERT INTO fam_custom_price SET fam_id 		= '".$in['fam_id']."',
									category_id 			= '".$in['category_id']."',
									value 			= '".return_value($in['fam_value'])."'");		
   

		msg::success ( gm('Changes saved'),'success');
		return true;
	}

	/**
	 * validate update price list data
	 * @param  array $in
	 * @return boolean
	 */
	function update_fam_price_list_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('fam_id', 'fam_id', 'required');
	
		return $v -> run();

	}

	function update_category(&$in)
	{   
		$in['failure'] = false;
		if(!$this->update_category_validate($in)){
			$in['failure'] = true;
			return false;
		}

		$this->db->query("SELECT pim_article_price_category.* FROM pim_article_price_category WHERE category_id='".$in['category_id']."' ");
		$this->db->move_next();
		$price_value = $this->db->f('price_value');
		$type = $this->db->f('type');
		$price_type = $this->db->f('price_type');
		$price_value_type = $this->db->f('price_value_type');

		$this->db->query("UPDATE pim_article_price_category SET     name='".$in['name']."',
		                                                            type='".$in['type']."',
		                                                            price_type='".$in['price_type_id']."',
		                                                            price_value='".return_value($in['price_value'])."',
		                                                            price_value_type='".$in['price_value_type_id']."'
		                  WHERE category_id='".$in['category_id']."'");

       $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE category_id='".$in['category_id']."'");
   			while($this->db2->move_next()){
           	$article_ids.=$this->db2->f('article_id').',';
   		}
        $article_ids=rtrim($article_ids,',');
        $article_ids_array=explode(',',$article_ids);

		if(($in['type'] != $type) || ($in['price_type_id'] != $price_type) || (return_value($in['price_value']) != $price_value) || ($in['price_value_type_id'] != $price_value_type)) {
		
         
           if(!$article_ids){   
			 $this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' and base_price!=1");
			}
			else{
			 $this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' AND article_id NOT IN (".$article_ids.") and base_price!=1");	
			}
			
			$article_prices = $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices WHERE base_price=1");

			while ($article_prices->next()){
                     if($in['price_type_id']==2){
                       $article_base_price=$article_prices->f('purchase_price');
                     }else{
                       $article_base_price=$article_prices->f('price');
                      }

				//insert the article prices
				switch ($in['type']) {
					case 1:                  //discount
					if($in['price_value_type_id']==1){  // %
						$total_price =$article_base_price - (return_value($in['price_value']) * $article_base_price / 100);
					}else{ //fix
						$total_price = $article_base_price - return_value($in['price_value']);
					}

					break;
					case 2:                 //profit margin 
					if($in['price_value_type_id']==1){  // %
						$total_price = $article_base_price + (return_value($in['price_value']) * $article_base_price / 100);
					}else{ //fix
						$total_price =$article_base_price + return_value($in['price_value']);
					}

					break;

					case 3:                 //profit margin
						if($in['price_value_type_id']==1){  // %
							$total_price = $article_base_price / (1- return_value($in['price_value']) / 100);
						}else{ //fix
							

							//$total_price =$article_base_price + return_value($in['price_value']);
						}
		              break;
		             case 4:                //manually set price

						$total_price =return_value($in['price_value']);
						
		              break;

				}
				$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_prices->f('article_id')."'");
				$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
				$total_with_vat = $total_price+($total_price*$vat_value/100);
				
            if(!in_array($article_prices->f('article_id'),  $article_ids_array)){
				$this->db->query("INSERT INTO pim_article_prices SET
				article_id			= '".$article_prices->f('article_id')."',
				price_category_id	= '".$in['category_id']."',
				price				= '".$total_price."',
				total_price			= '".$total_with_vat."'
				");
              }
            
			}

		}
		msg::success ( "Article Price Category edited.",'success');
		return true;
	}
	function update_category_validate(&$in){
		$v=new validation($in);
		$v->f('category_id','Category Id','required:exist[pim_article_price_category.category_id]');
		if(!$v->run()){
			return false;
		}else{
			return $v -> run();
		}
	}
	function add_volume(&$in)
	{		
		$in['failure'] = false;
		if(!$this->add_volume_validate($in)){
			$in['failure'] = true;
			return false;
		}

		
		$in['id'] = $this->db->insert("INSERT INTO  article_price_volume_discount SET `from_q`='".return_value($in['from_q'])."',
		                                                                                   `to_q`='".return_value($in['to_q'])."',
		                                                                                   percent='".return_value($in['percent'])."' 
		                                                                                 
		                                       ");
       
	
		
		msg::success ( gm("Volume Discount added."),'success');

		return true;
	}
	function add_volume_validate(&$in){

		$v = new validation($in);
		if (ark::$method == 'update_volume') {
			$option = ".(id != '".$in['id']."')";
		} else {
			$option = "";
		}
          // biggest to quantity
		$biggest_to_quantity=$this->db->field("SELECT  article_price_volume_discount.to_q FROM  article_price_volume_discount  ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT article_price_volume_discount.from_q FROM article_price_volume_discount  ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   $in['price_error']=gm('To Qty value should be bigger than From Qty');
		   msg::error(gm('To Qty value should be bigger than From Qty'),'error');
		   return false;
		}
		 if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['from_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
         	 msg::error(gm('To Qty value should be bigger than From Qty3'),'error');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['to_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
         	 msg::error(gm('To Qty value should be bigger than From Qty2'),'error');
		     return false;
          }
           	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND    article_price_volume_discount.to_q < '".return_value($in['to_q'])."'
         	               AND article_price_volume_discount.to_q > '".return_value($in['from_q'])."'
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
         	 msg::error(gm('To Qty value should be bigger than From Qty1'),'error');
		     return false;
          }

        $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."' and  article_price_volume_discount.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
            	 msg::error(gm('To Qty value should be bigger than From Qty4'),'error');
		         return false;
            }



        }else{

            $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE    article_price_volume_discount.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	msg::error(gm('To Qty value should be bigger than From Qty5'),'error');
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }

        }



	
		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]:numeric');
		$v -> f('to_q', 'Max Qty', 'numeric:greater_than[1]');
		$v->field('percent','Percent','required');

		return $v->run();

	}
	function update_volume(&$in)
	{   
		$in['failure'] = false;
		if(!$this->update_volume_validate($in)){
			$in['failure'] = true;
			return false;
		}
		

		$this->db->query("UPDATE  article_price_volume_discount SET  `from_q`='".return_value($in['from_q'])."',
		                                                             to_q='".return_value($in['to_q'])."',
		                                                             percent='".return_value($in['percent'])."'
		                                                           
		                  WHERE id='".$in['id']."'");

      
		msg::success ( gm("Volume Discount edited."),'success');
		return true;
	}
	function update_volume_validate(&$in){
		$v=new validation($in);
		$v->f('id',' Id','required:exist[article_price_volume_discount.id]');
			
		$biggest_to_quantity=$this->db->field("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  id!='".$in['id']."' ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT article_price_volume_discount.from_q FROM article_price_volume_discount WHERE  id!='".$in['id']."' ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   $in['price_error']=gm('To Qty value should be bigger than From Qty');
		   return false;
		}
        if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['from_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['to_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	                 AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND    article_price_volume_discount.to_q < '".return_value($in['to_q'])."'
         	               AND article_price_volume_discount.to_q > '".return_value($in['from_q'])."'
         	                AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

          $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND  article_price_volume_discount.to_q=0  AND id!='".$in['id']."' LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }



        }else{
           //return_value($in['from_q']) >

            $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE    article_price_volume_discount.to_q=0  AND id!='".$in['id']."' LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }

        }

		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]');

		return $v->run();
	}
	function delete_volume(&$in)	{
		
		if(!$this->delete_volume_validate($in)){
			
			return false;
		}
		$this->db->query("DELETE FROM article_price_volume_discount WHERE id='".$in['id']."' ");
		
		msg::success ( gm("Article Volume Discount deleted."),'success');
		
		return true;
	}
	function delete_volume_validate(&$in){
		$v=new validation($in);
		$v->f('id',' Id','required:exist[article_price_volume_discount.id]');

		return $v->run();

	}
	function add_tax(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_tax_validate($in)){
            $in['failure'] = true;
			return false;
		}
		
		$in['tax_id'] = $this->db->insert("INSERT INTO pim_article_tax SET
																		
																code  		    = '".$in['code']."',
																type_id  		= '".$in['type_id']."',
																amount			= '".return_value($in['amount'])."',
																description		= '".$in['description']."',
																vat_id			= '".$in['tax_vat_id']."',
																apply_to		= '".$in['apply_to']."' 																	
																");

		$query_sync[0]="INSERT INTO pim_article_tax SET
																tax_id          =  '".$in['tax_id']."',
																code  		    = '".$in['code']."',
																type_id  		= '".$in['type_id']."',
																amount			= '".return_value($in['amount'])."',
																description		= '".$in['description']."',
																vat_id			= '".$in['tax_vat_id']."',
																apply_to		= '".$in['apply_to']."'  																	
																";
		msg::success ( "Tax succefully added.",'success');
		return true;
	}

	/**
	 * Validae add data
	 * @param array $in
	 * @return boolean
	 */
	function add_tax_validate(&$in)
	{

		$v = new validation($in);

		if (ark::$method == 'update') {

			$option = ".( tax_id != '" . $in['tax_id'] . "')";
		} else {
			$option = "";
		}

	    $v -> f('code', 'Code', 'required:unique[pim_article_tax.code' . $option . ']');
		
		$v -> f('amount', 'Amount', 'required:numeric');
		
		return $v -> run();
	}

	function update_tax(&$in)
	{
		 $in['failure'] = false;
		if(!$this->update_tax_validate($in)){
            $in['failure'] = true;
			return false;
		}
		
		$this->db->query("UPDATE pim_article_tax SET
							
													code  		    = '".$in['code']."',
													type_id  		= '".$in['type_id']."',
													amount			= '".return_value($in['amount'])."',
													description		= '".$in['description']."',
													vat_id			= '".$in['tax_vat_id']."',
													apply_to		= '".$in['apply_to']."'  
													
						  WHERE tax_id='".$in['tax_id']."' ");

		msg::success ( "Tax succefully updated.",'success');
		return true;
	}

	/**
	 * Update validate data
	 * @param  array $in
	 * @return boolean
	 */
	function update_tax_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('tax_id', 'Tax Id', 'required:exist[pim_article_tax.tax_id]');

		return $v->run();

	}
	function delete_tax(&$in)
	{

		if(!$this->delete_tax_validate($in)){
			return false;
		}
		$this->db->query("DELETE FROM pim_article_tax WHERE tax_id='".$in['tax_id']."' ");
	    $this->db->query("DELETE FROM pim_articles_taxes WHERE tax_id='".$in['tax_id']."' ");
		msg::success ( "Tax deleted.",'success');
		return true;
	}

	/**
	 * Validate delete data
	 * @param  array $in
	 * @return boolean
	 */
	function delete_tax_validate(&$in)
	{
		
		$v=new validation($in);
		$v->f('tax_id','Tax Id','required:exist[pim_article_tax.tax_id]');
		$this->db->query("SELECT article_id FROM pim_articles_taxes WHERE tax_id='".$in['tax_id']."'");
		if($this->db->move_next()){
			// $v->f('tax_id','Tax Id','empty_condition','There are articles define for these tax.Please update first that articles.');
		
		}

		return $v->run();

	}
/*	function delete_select(&$in){
		$this->db->query("DELETE FROM pim_article_tax_type WHERE id='".$in['id']."' ");
		msg::success ( "Tax deleted.",'success');
		return true;
	}*/

		function add_article_tax(&$in)
	{
		if(!$in['article_id'] || !$in['tax_id'] ){
			msg::$error = gm("Invalid ID");
			return false;
		}
	
		$this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$in['tax_id']."'

				");

		//Insert log message for services
		$articleService = $this->db->query('SELECT article_id,is_service FROM pim_articles WHERE article_id ="'.$in['article_id'].'"')->getAll();
		$articleService = $articleService[0];
		if($articleService['is_service']){
			insert_message_log('aservice','{l}Service taxes updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);	
		} else {
			insert_message_log($this->pag,'{l}Article taxes updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$in['article_id'],false,$_SESSION['u_id']);			
		}
		//End Insert log message for services
	
		 msg::success(gm("Changes saved."),'success');
		/* json_out($in);*/
		return true;
	}


		function remove_article_tax(&$in)
	{
		if(!$in['article_id'] || !$in['tax_id'] ){
			msg::$error = gm("Invalid ID");
			return false;
		}
	
		$this->db->query("DELETE from pim_articles_taxes WHERE
								  article_id	= '".$in['article_id']."' AND
								  tax_id	= '".$in['tax_id']."'

				");
			
		 msg::success(gm("Changes saved."),'success');
		 /*json_out($in);*/
		return true;
	}

	function get_supplier_ref(&$in)
	{
		if(!$in['article_id'] || !$in['supplier_id'] ){
			msg::$error = gm("Invalid ID");
			return false;
		}
	
		$in['supplier_reference'] = $this->db->field("SELECT supplier_ref from pim_article_references WHERE
								  article_id	= '".$in['article_id']."' AND
								  supplier_id	= '".$in['supplier_id']."'");
				
		$in['purchasing_price'] = $this->db->field("SELECT purchasing_price from pim_article_references WHERE
								  article_id	= '".$in['article_id']."' AND
								  supplier_id	= '".$in['supplier_id']."'");
		$in['purchasing_price'] = display_number_var_dec($in['purchasing_price']);

		 msg::success(gm("Changes saved."),'success');
		 json_out($in);
		return true;
	}

	function set_supplier_ref(&$in)
	{
		if(!$in['article_id'] || !$in['supplier_id'] ){
			msg::$error = gm("Invalid ID");
			return false;
		}

		$val = $this->db->field("SELECT id from pim_article_references WHERE
								  article_id	= '".$in['article_id']."' AND
								  supplier_id	= '".$in['supplier_id']."'");
		if($val){
			$this->db->query("UPDATE pim_article_references SET 
									supplier_ref ='".$in['supplier_reference']."'
									WHERE id	= '".$val."'");
			if($in['purchasing_price']){
				$this->db->query("UPDATE pim_article_references SET 
									purchasing_price ='".return_value($in['purchasing_price'])."' 
									WHERE  id	= '".$val."' ");
			}
		}else{
			$this->db->query("INSERT INTO pim_article_references SET article_id = '".$in['article_id']."',
																	supplier_id = '".$in['supplier_id']."',
																	supplier_ref = '".$in['supplier_reference']."',
																	purchasing_price ='".return_value($in['purchasing_price'])."' ");
		}
		
			
		 msg::success(gm("Changes saved."),'success');
		 /*json_out($in);*/
		return true;
	}

	function remove_article_reference(&$in)
	{
		if(!$in['article_id'] || !$in['supplier_id'] ){
			msg::$error = gm("Invalid ID");
			return false;
		}
	
		$this->db->query("DELETE from pim_article_references WHERE
								  article_id	= '".$in['article_id']."' AND
								  supplier_id	= '".$in['supplier_id']."'

				");
			
		 msg::success(gm("Changes saved."),'success');
		 /*json_out($in);*/
		return true;
	}



	function show_art_serial_nr_del(&$in){
		if(!ALLOW_STOCK && $in['SHOW_ART_S_N_ORDER_PDF']==1){
			msg::error ( gm("You have to activate Stock"),'error');
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['SHOW_ART_S_N_ORDER_PDF']."' WHERE constant_name = 'SHOW_ART_S_N_ORDER_PDF' ");
		msg::success ( gm('Changes saved'),'success');
		return true;
	}
	function show_art_serial_nr_inv(&$in){
		if(!ALLOW_STOCK && $in['SHOW_ART_S_N_INVOICE_PDF']==1){
			msg::error ( gm("You have to activate Stock"),'error');
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['SHOW_ART_S_N_INVOICE_PDF']."' WHERE constant_name = 'SHOW_ART_S_N_INVOICE_PDF' ");
		msg::success ( gm('Changes saved'),'success');
		return true;
	}
	function show_art_batch_nr_del(&$in){
		if(!ALLOW_STOCK && $in['SHOW_ART_B_N_ORDER_PDF']==1){
			msg::error ( gm("You have to activate Stock"),'error');
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['SHOW_ART_B_N_ORDER_PDF']."' WHERE constant_name = 'SHOW_ART_B_N_ORDER_PDF' ");
		msg::success ( gm('Changes saved'),'success');
		return true;
	}

	function show_art_batch_nr_inv(&$in){
		if(!ALLOW_STOCK && $in['SHOW_ART_B_N_INVOICE_PDF']==1){
			msg::error ( gm("You have to activate Stock"),'error');
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['SHOW_ART_B_N_INVOICE_PDF']."' WHERE constant_name = 'SHOW_ART_B_N_INVOICE_PDF' ");
		msg::success ( gm('Changes saved'),'success');
		return true;
	}

	function unique_batch_nr(&$in){
		if(!ALLOW_STOCK && $in['UNIQUE_BATCH_NR']==1){
			msg::error ( gm("You have to activate Stock"),'error');
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['UNIQUE_BATCH_NR']."' WHERE constant_name = 'UNIQUE_BATCH_NR' ");
		msg::success ( gm('Changes saved'),'success');
		return true;
	}

	function show_art_batch_ed_del(&$in){
		if(!ALLOW_STOCK && $in['SHOW_ART_B_ED_ORDER_PDF']==1){
			msg::error ( gm("You have to activate Stock"),'error');
			return false;
		}
		$this->db->query("UPDATE settings SET value= '".$in['SHOW_ART_B_ED_ORDER_PDF']."' WHERE constant_name = 'SHOW_ART_B_ED_ORDER_PDF' ");
		msg::success ( gm('Changes saved'),'success');
		return true;
	}
	function saveAddToArticle(&$in)
	{

  	if($in['check_all']){

  		if($in['val'] == 1){

  			foreach ($in['item'] as $key => $value) {
			    $_SESSION['add_to_article'][$value]= $in['val'];
	  		}
  		}else{
  			foreach ($in['item'] as $key => $value) {
			    unset($_SESSION['add_to_article'][$value]);
	  		}
		  }
  	}else{
	    if($in['val'] == 1){
	    	$_SESSION['add_to_article'][$in['id']]= $in['val'];
	    }else{
	    	unset($_SESSION['add_to_article'][$in['id']]);
	    }
	  }
	  	json_out($_SESSION['add_to_article']);
    return true;
  }

    function saveAddToArchived(&$in)
  {
    if($in['all']){
      if($in['value'] == 1){
        foreach ($in['item'] as $key => $value) {
          $_SESSION['add_to_archive'][$value]= $in['value'];
        }
      }else{
        foreach ($in['item'] as $key => $value) {
          unset($_SESSION['add_to_archive'][$value]);
        }
      }
    }else{
      if($in['value'] == 1){
        $_SESSION['add_to_archive'][$in['item']]= $in['value'];
      }else{
        unset($_SESSION['add_to_archive'][$in['item']]);
      }
    }
      $all_pages_selected=false;
      $minimum_selected=false;
      if($_SESSION['add_to_archive'] && count($_SESSION['add_to_archive'])){
          if($_SESSION['tmp_add_to_archive'] && count($_SESSION['tmp_add_to_archive']) == count($_SESSION['add_to_archive'])){
              $all_pages_selected=true;
          }else{
              $minimum_selected=true;
          }
      }
      $in['all_pages_selected']=$all_pages_selected;
      $in['minimum_selected']=$minimum_selected;
      json_out($in);
  }

    function saveAddToArchivedAll(&$in)
    {
        $all_pages_selected = false;
        $minimum_selected = false;

        if ($in['value']) {
            unset($_SESSION['add_to_archive']);
            if ($_SESSION['tmp_add_to_archive'] && count($_SESSION['tmp_add_to_archive'])) {
                $_SESSION['add_to_archive'] = $_SESSION['tmp_add_to_archive'];
                $all_pages_selected = true;
            }
        } else {
            unset($_SESSION['add_to_archive']);
        }
        $in['all_pages_selected'] = $all_pages_selected;
        $in['minimum_selected'] = $minimum_selected;
        json_out($in);
    }

  function archiveBulkArticles(&$in){
    if(empty($_SESSION['add_to_archive'])){
      msg::error(gm('No articles selected'),'error');
      json_out($in);
    }

    $add_to_archive=$this->db->query("SELECT article_id FROM pim_articles WHERE active='1' AND article_id IN (".implode(',',array_keys($_SESSION['add_to_archive'])).")")->getValues('article_id');

    if(empty($add_to_archive)){
      msg::error(gm('No articles selected'),'error');
      json_out($in);
    }

    foreach($add_to_archive as $value){
        $is_active = $this->db->field("SELECT active FROM pim_articles WHERE article_id = '". $value ."'");
        if ($is_active) {
            $this->db->query("UPDATE pim_articles SET active='0', updated_at='" . time() . "' WHERE article_id='" . $value . "' ");
            insert_message_log($this->pag, '{l}Article archived by{endl} ' . get_user_name($_SESSION['u_id']), 'article_id', $value, false, $_SESSION['u_id']);
        }
        unset($_SESSION['add_to_archive'][$value]);
    }

    msg::success(gm('Articles successfully archived'),'success');

    return true;
  }

    function restoreBulkArticles(&$in){
        if(empty($_SESSION['add_to_archive'])){
            msg::error(gm('No articles selected'),'error');
            json_out($in);
        }

        $add_to_archive=$this->db->query("SELECT article_id FROM pim_articles WHERE active='0' AND article_id IN (".implode(',',array_keys($_SESSION['add_to_archive'])).")")->getValues('article_id');

        if(empty($add_to_archive)){
	      msg::error(gm('No articles selected'),'error');
	      json_out($in);
	    }

        foreach($add_to_archive as $value){
            $is_active = $this->db->field("SELECT active FROM pim_articles WHERE article_id = '". $value ."'");
            if (!$is_active) {
                $this->db->query("UPDATE pim_articles SET active='1', updated_at='" . time() . "' WHERE article_id='" . $value . "' ");
                insert_message_log($this->pag, '{l}Article activated by{endl} ' . get_user_name($_SESSION['u_id']), 'article_id', $value, false, $_SESSION['u_id']);
            }
            unset($_SESSION['add_to_archive'][$value]);
        }

        msg::success(gm('Articles successfully restored'),'success');

        return true;
    }

  function apply_ledger_to_articles(&$in)
  {

	 if($_SESSION['add_to_article']){
	  foreach ($_SESSION['add_to_article'] as $key => $value) {
	    
	      $this->db->query("SELECT *
	                    FROM  pim_article_categories
	                    WHERE id ='".$key."' 
	                    ");
	if($this->db->move_next()){

	   /*$ledger_account_id=$this->db2->field("SELECT   pim_article_ledger_accounts.id FROM   pim_article_ledger_accounts WHERE name='".$this->db->f('ledger_val')."'");*/
	   $this->db2->query("UPDATE pim_articles SET ledger_account_id='".$this->db->f('ledger_account_id')."' WHERE article_category_id='".$this->db->f('id')."'");
	}
	  }
	}
	unset($_SESSION['add_to_article']);
    msg::success ( gm("Accounts successfully set."),'success');
    return true;
  }
  function add_fam(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_fam_validate($in)){
            $in['failure'] = true;
			return false;
		}
		$this->db->insert("UPDATE pim_article_categories SET ledger_account_id='".$in['ledger_val']."' WHERE id='".$in['fam_id']."'");
/*		$this->db->insert("DELETE FROM fam_ledger WHERE fam_id      = '".$in['fam_id']."'");
		
		$in['fam_ledger_id'] = $this->db->insert("INSERT INTO fam_ledger SET
																fam_id      = '".$in['fam_id']."',
																ledger_val      = '".$in['ledger_val']."'	
																
															
																								
																");
	  $this->db->query("SELECT * FROM pim_article_ledger_accounts WHERE name      = '".$in['ledger_val']."'");

	  if(!$this->db->move_next()){
	    $this->db2->query("INSERT INTO pim_article_ledger_accounts SET
																
																name      = '".$in['ledger_val']."'	
																
															
																								
																");
	}*/

		msg::success ( gm("Field updated."),'success');
		return true;
	}

	/**
	 * Validae add data
	 * @param array $in
	 * @return boolean
	 */
	function add_fam_validate(&$in)
	{

		$v = new validation($in);

		

	  $v -> f('ledger_val','fg','required');
	  $v -> f('fam_id', 'fam_id', 'required:numeric');
		
		return $v -> run();
	}


	function upgrade(&$in)
	{
		
		$this->db->query("UPDATE settings SET value=1  WHERE constant_name='ADV_PRODUCT'");

		msg::success(gm("Account upgraded"),'success');
		return true;
	}

	function deleteDropboxFile(&$in)
	{
		
			$d = new drop('articles',null,$in['id'],false,'',null,null, null, true);
			$e = $d->deleteF(stripslashes($in['path']));
			if(is_array($e)){
				msg::success(gm('File deleted'),'success');
			}else{
				msg::success(gm($e),'error');
				
			}
			return true;

		}
		/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function uploadDropboxFile(&$in)
	{
		$response=array();
		$in['drop_folder']='articles';
		//$in['folder']='articles';

		if(!$in['drop_folder']){
			msg::success('Invalid Id','error');
			$response['error'] = 'Invalid Id';
			return false;
		}
		
		if($_FILES['Filedata']['error'] == 0){
			$d = new drop($in['drop_folder'],null,$in['id'],true,$in['folder'],null,null, null, true);
				$e = $d->upload($_FILES['Filedata']['name'],$_FILES['Filedata']['tmp_name'],$_FILES['Filedata']['size']);
				
				// echo json_encode($in);
				$response['success'] = 'success';
				echo json_encode($response);
				exit();

			
		}
		$response['error'] = 'unknown error';
		echo json_encode($response);
		exit();
	}
	function backup_data(&$in)
	{
		set_time_limit(0);
	    ini_set('memory_limit', '-1');   
	    $folder=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog';
	    $folder_exist=file_get_contents($folder);

	    if($folder_exist==false){
	    	@mkdir($folder,0755,true);
	    }
	    $array_exclude = array('pim_article_import');
		$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup1.xml';
		$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup2.xml';
		$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup3.xml';
		$file_fill1 = file_get_contents($file1);
		$file_fill2 = file_get_contents($file2);
		$file_fill3 = file_get_contents($file3);
		if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
			if($in['file_id']!=0){
					ini_set('memory_limit', '-1');
					$this->db->query("show tables");
					$table_names=array();
					while ($this->db->move_next()){
						if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'pim_article')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'pim_articles')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'dispatch_stock')!==FALSE)
						{
							if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
								$table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
							}				
						}				
					}		
					$xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');	

				    for($i=0;$i<count($table_names);$i++){
				    	/*$cus = $xml->addChild('customers-data');*/
			            	 $tbl=$xml->addChild($table_names[$i],''); 
			                 //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
			                $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
			                $this->db->query("SELECT * FROM  ".$table_names[$i]."");
			                while($this->db->move_next()){
			                 	    $chese=$tbl->addChild('line','');  
			                 	    foreach ($cols as $key => $value) {
			                 	   		$chese->addChild($value['Field'], htmlspecialchars($this->db->f($value['Field']))) ;
			                 	   	}
			                 	    /*$this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."");
						            while ($this->db2->move_next()) {
			                 	  	  $chese->addChild($this->db2->f('Field'), $this->db->f($this->db2->f('Field'))) ;
			 	               	  	}*/
			                }
			    	}
			    	$time=time();
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup'.$in['file_id'].'.xml');
		    		$this->db->query("UPDATE backup_data_article SET backup_time='".$time."' WHERE backup_id=".$in['file_id']."");
		    	msg::success ( gm('Data successfully saved'),'success');
		    	$in['step']=2;
		    	return true;
			}else{
				msg::error ( gm('Select a file'),'error');
	    		return false;
			}
		}else{
			if($in['file_id']!=0){
				ini_set('memory_limit', '-1');
				$this->db->query("show tables");
				$table_names=array();
				while ($this->db->move_next()){
					if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'pim_article')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'pim_articles')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'dispatch_stock')!==FALSE)
					{
						if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
							$table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
						}					
					}				
				}		
				
				$xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');	
			    for($i=0;$i<count($table_names);$i++){
			    	/*$cus = $xml->addChild('article-data');*/
		            	 $tbl=$xml->addChild($table_names[$i],''); 
		                 //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
		                $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
		                $this->db->query("SELECT * FROM  ".$table_names[$i]."");
		                while($this->db->move_next()){
		                 	    $chese=$tbl->addChild('line',''); 
		                 	   	foreach ($cols as $key => $value) {
		                 	   		$chese->addChild($value['Field'], htmlspecialchars($this->db->f($value['Field']))) ;
		                 	   	}
					            /*while ($this->db2->move_next()) {
		                 	  	  $chese->addChild($this->db2->f('Field'), $this->db->f($this->db2->f('Field'))) ;
		 	               	  	}*/
		                }
		    	}
		    	$time=time();
		    	if($file_fill1==false){
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup1.xml');
		    		$this->db->query("UPDATE backup_data_article SET backup_time='".$time."' WHERE backup_id='1'");
		    	}elseif($file_fill1==true&&$file_fill2==false){
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup2.xml');
		    		$this->db->query("UPDATE backup_data_article SET backup_time='".$time."' WHERE backup_id='2'");
		    	}elseif ($file_fill2==true&&$file_fill3==false) {
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup3.xml');
		    		$this->db->query("UPDATE backup_data_article SET backup_time='".$time."' WHERE backup_id='3'");
		    	}
		    	msg::success ( gm('Data successfully saved'),'success');
		    	$in['step']=2;
		    	return true;
			}else{
				msg::error ( gm('Select a file'),'error');
	    		return false;
			}
		}
    	/*msg::success ( gm('Data successfully saved'),'success');*/
/*    	$in['step']=2;
    	return true;*/
	}
	function uploadify(&$in)
	{
		$response=array();
		// Define a destination
		$targetFolder = 'upload/'.DATABASE_NAME.'/catalog/'; // Relative to the root
		@mkdir($targetFolder,0755,true);
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

			// Validate the file type
			$fileTypes = array('xml','xls','xlsx'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			if (in_array($fileParts['extension'],$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
				// $response['success'] = $_FILES['Filedata']['name'];
				msg::success($_FILES['Filedata']['name'],'success');
				$in['xls_name'] = $_FILES['Filedata']['name'];

				json_out($in);
				// $this->import_company($in);
				/*echo json_encode($response);*/
			} else {
				// $response['error'] = 'Invalid file type.';
				msg::error( 'Invalid file type.','error');
				// echo json_encode($response);
			}

		}
	}
	function import_article(&$in){

		ini_set('memory_limit', '2048M');
		ini_set('max_execution_time', 0);
		$in['timestamp'] = time();
		// $in['debug'] = true;
		global $_FILES;
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		require_once 'libraries/PHPExcel.php';		
		$inputFileName = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/catalog/".$in['xls_name'];
		
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		//$objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
		$highestColumn = $objWorksheet->getHighestColumn();
		$i=0;
		$x=0;
		$z=1;
		$sdsd = 0;
		$nr_rows=0;
		$nr_cols=0;
		$blabla = 0;
		$headers = array();
		$values = array();
		$final_arr = array();
		// $arrayy = array();
		// print_r($objWorksheet->getRowIterator());
		foreach ($objWorksheet->getRowIterator() as $row) {
			if($i>$highestRow)
			{
				break;
			}
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			/*if($i!=0)
			{
				$nr_rows++;
			}*/
			$k=0;
			$row_array = array();
			foreach ($cellIterator as $cell) {
				$val = $cell->getValue();
				array_push($row_array, $val);
				if($i==0)
				{	if($val)
					{
						$val = str_replace("'", " ", $val);
						array_push($headers, $val);
						$nr_cols++;
					}else{
						break;
					}
				}else
				{
					if($k!=$nr_cols)
					{
						$values[$k] = $cell->getValue();
						$k++;
					}else
					{
						break;
					}
				}
			}
			if($i!=0)
			{
				if($i < 5){
					for ($j=0;$j<$k;$j++)
					{
						$final_arr[$headers[$j]][$x] = $values[$j];
					}
				}
				$x++;
				$t = 0;
				$nr_rows++;
			}else{
				$t = 1;
			}
			// array_push($arrayy, $row_array);
			$i++;
			// console::memory(addslashes(serialize($row_array)),'line');
			$this->db->query("INSERT INTO pim_article_import SET data='".addslashes(serialize($row_array))."', timestamp='".$in['timestamp']."', line='".$z."',type='".$t."' ");
			$j++;
			$z++;
		}

		$in['file_content']=$final_arr;
		$dark = str_replace(array("\r"), "", $in['file_content']);
		foreach ($dark as $header => $values) {
				$i=0;
				$in['match_field_id'] = '0';
				$in['match_field'] = '0';
/*				$header = trim($header);
				$header_select = str_replace(array("\r\n","\n","\r"," "), '_', $header);*/
				$header_select = $header;
				/*$header_select1 = str_replace(array("\r\n","\n","\r"," "), '.', $header);*/
				$in['match_field_company'] = build_match_field_dd_article($in[$header_select]);
				/*$in['match_field_company1'] = build_match_field_dd_article_language($in[$header_select]);*/
				/*$in['match_field_contacts'] = build_match_field_dd_contacts($in[$header_select]);*/
				
				$in['names_id'][]=$header_select;
		}
		$final_arr = null;
		$in['total_rows']=$nr_rows;
		$in['step']=3;
		$in['changefield']=0;
		$in['class3']='text-primary';
		@unlink(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/catalog/".$in['xls_name']);
		$stock_location_dd=build_dispach_location_dd();
		if(count($stock_location_dd)>1){
			$in['stock_location_dd']=$stock_location_dd;
			$in['stock_location_id']='0-'.$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
		}
		//header("Location: index.php?do=company-save_imports");
		// var_dump($in['file_content']); exit();
		
		json_out($in);
		return true;
	}
	function import_article_prices(&$in){

		ini_set('memory_limit', '2048M');
		ini_set('max_execution_time', 0);
		$in['timestamp'] = time();
		global $_FILES;
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		require_once 'libraries/PHPExcel.php';		
		$inputFileName = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/catalog/".$in['xls_name'];
		
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
		$highestColumn = $objWorksheet->getHighestColumn();
		$i=0;
		$x=0;
		$z=1;
		$sdsd = 0;
		$nr_rows=0;
		$nr_cols=0;
		$blabla = 0;
		$headers = array();
		$values = array();
		$final_arr = array();
		foreach ($objWorksheet->getRowIterator() as $row) {
			if($i>$highestRow)
			{
				break;
			}
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			$k=0;
			$row_array = array();
			foreach ($cellIterator as $cell) {
				$val = $cell->getValue();
				array_push($row_array, $val);
				if($i==0)
				{	if($val)
					{
						$val = str_replace("'", " ", $val);
						array_push($headers, $val);
						$nr_cols++;
					}else{
						break;
					}
				}else
				{
					if($k!=$nr_cols)
					{
						$values[$k] = $cell->getValue();
						$k++;
					}else
					{
						break;
					}
				}
			}
			if($i!=0)
			{
				if($i < 5){
					for ($j=0;$j<$k;$j++)
					{
						$final_arr[$headers[$j]][$x] = $values[$j];
					}
				}
				$x++;
				$t = 0;
				$nr_rows++;
			}else{
				$t = 1;
			}
			$i++;
			$this->db->query("INSERT INTO pim_article_import SET data='".addslashes(serialize($row_array))."', timestamp='".$in['timestamp']."', line='".$z."',type='".$t."' ");
			$j++;
			$z++;
		}

		$in['file_content']=$final_arr;
		$dark = str_replace(array("\r"), "", $in['file_content']);
		foreach ($dark as $header => $values) {
				$i=0;
				$in['match_field_id'] = '0';
				$in['match_field'] = '0';
				$header_select = $header;			
				$in['match_field_company'] = build_match_field_dd_article_prices();		
				$in['names_id'][]=$header_select;
		}
		$final_arr = null;
		$in['total_rows']=$nr_rows;
		$in['step']=3;
		$in['changefield']=0;
		$in['class3']='text-primary';
		@unlink(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/catalog/".$in['xls_name']);		
		json_out($in);
	}
	function save_imports(&$in)
	{
		$file_content = array();
		$tmp_header = array();
		$q = $this->db->query("SELECT * FROM pim_article_import WHERE `timestamp`='".$in['timestamp']."' ORDER BY line ASC ");
		while ($q->next()) {
			$line = unserialize($q->f('data'));
			foreach ($line as $key => $value) {
				if($q->f('type') == 1 ){
					$file_content[$value] = array();
					$tmp_header[$key] = $value;
				}else{
					array_push($file_content[$tmp_header[$key]],$value);					
				}
			}
		}

		$insert_values = "";
		$values_info = array();

		$j=0;
		$nr_headers=0;
		$headers_ids= array();
		$inserted_cust = 0;
		$inserted_cust_arr = array();
		$update_cust = 0;
		$update_cust_arr = array();
		$inserted_contacts = 0;
		$inserted_contacts_arr = array();
		$inserted_price_arr = array();
		$inserted_price = 0;
		$update_cont = 0;
		$update_cont_arr = array();
		$filename = $in['xls_name'];
		$vat_value = $this->db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");

		for ($k=$in['k'];$k<$in['total_rows'];$k++)
		{ 

			$no_action_customer = true;
			$no_action_contact = true;
			$no_action_contact_addr = false;// set true to import the address only if there is a country id
			$no_action_cust_addr = false;
			$has_first_name = false;
			$has_last_name = false;
			$first_name="";
			$last_name="";
			$begin_query = "";
			$end_query = "";
			$cmon_id ="";
			$begin_contact_query = "INSERT INTO ";
			$insert_cont = true;
			$search_by_id=false;
			unset($values_info);

	/*			echo "<pre>";
			print_r($file_content);
			echo "</pre>";
			exit();*/


			foreach ($file_content as $headers => $value) {

/*				$headers = trim($headers);
				$headers = str_replace(array("\r\n","\n","\r"," "), '_', $headers);*/
				/*$headers = explode(' ', $headers);
				$headers = implode('_', $headers);*/
				$headers = str_replace("'",' ',$headers);

				$value[$k] = addslashes($value[$k]);


				if($in[$headers])
				{

					if($k==0)
					{	
						if(in_array($in[$headers], $headers_ids))
						{
							
							msg::error ( gm("You cannot select the same field twice"),'error');
							$in['error'] = 'error';
							json_out($in);
							//ark::$controller = 'save_imports';
							return false;
						}else
						{
							array_push($headers_ids, $in[$headers]);
						}
					}

					$nr_headers++;
					$labels_info = $this->db_import->query("SELECT field_name, tabel_name, is_custom_dd, type, custom_field FROM import_labels_articles WHERE import_label_id='".$in[$headers]."'");
					
					$labels_info->next();
					if($labels_info->f('type')=='article')
					{

						$tabel_name = 'pim_articles';
						
					}
					
					if($labels_info->f('custom_field')==0)
					{
						
						if(trim($value[$k]) || trim($value[$k]) == 0 )
						{
							if($labels_info->f('field_name')=='item_code'){

								$values_info[$j]['field_name'] = $labels_info->f('field_name');
								$values_info[$j]['tabel_name'] = $tabel_name;
								$values_info[$j]['value'] = $value[$k];
							
							}elseif($labels_info->f('field_name')=='internal_name'){

									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = htmlentities($value[$k]);

							}elseif($labels_info->f('field_name')=='origin_number'){

									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
						
							}elseif($labels_info->f('field_name')=='base_price'){

									$values_info[$j]['field_name'] = 'price';
									$values_info[$j]['tabel_name'] = 'pim_article_prices';
									$values_info[$j]['value'] = $value[$k];
									$j++;
									$values_info[$j]['field_name'] = 'price';
									$values_info[$j]['tabel_name'] = 'pim_articles';
									$values_info[$j]['value'] = $value[$k];
									$j++;
						
							}elseif($labels_info->f('field_name')=='purchase_price'){

									$values_info[$j]['field_name'] = 'purchase_price';
									$values_info[$j]['tabel_name'] = 'pim_article_prices';
									$values_info[$j]['value'] = $value[$k];
						
							}elseif($labels_info->f('field_name')=='ean_code')
							{
								
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
								
							}elseif($labels_info->f('field_name')=='supplier_name')
							{
								if(trim($value[$k])){
									$supplier = $this->db->field("SELECT customer_id FROM customers WHERE name LIKE '$value[$k]%'");
									$supplier_name = $this->db->field("SELECT name FROM customers WHERE name LIKE '$value[$k]%'");
									if(!$supplier){
										$supplier_new = $this->db->insert("INSERT INTO customers SET name='".addslashes($value[$k])."', active='1', is_supplier='1'");
										$values_info[$j]['field_name'] = 'supplier_id';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $supplier_new;
										$j++;
										$values_info[$j]['field_name'] = 'supplier_name';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = addslashes($value[$k]);
									}else{
									$update_supplier = $this->db->query("UPDATE customers SET is_supplier='1' WHERE customer_id='".$supplier."'");
										$values_info[$j]['field_name'] = 'supplier_id';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $supplier;
										$j++;
										$values_info[$j]['field_name'] = 'supplier_name';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = addslashes($supplier_name);
									}
								}
								
							}elseif($labels_info->f('field_name')=='supplier_reference')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='block_discount')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='weight')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='packing')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='sale_unit')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='stock')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='vat_id')//vat_id
							{
								$vat_value = $this->db->field("SELECT vat_id FROM vats WHERE value='".$value[$k]."'");
								if(!$vat_value){
									$vat = $this->db->insert("INSERT INTO vats SET value='".$value[$k]."'");
									$values_info[$j]['field_name'] = 'vat_id';
									$values_info[$j]['tabel_name'] = 'pim_articles';
									$values_info[$j]['value'] = $vat;
								}else{
									$values_info[$j]['field_name'] = 'vat_id';
									$values_info[$j]['tabel_name'] = 'pim_articles';
									$values_info[$j]['value'] = $vat_value;
								}
									
							}elseif($labels_info->f('field_name')=='family_name')
							{
								if(trim($value[$k])){
									$exist_family = $this->db->field("SELECT id FROM pim_article_categories WHERE name LIKE '$value[$k]%'");
									if(!$exist_family){
										$family_new = $this->db->insert("INSERT INTO pim_article_categories SET name='".$value[$k]."'");
										$values_info[$j]['field_name'] = 'article_category_id';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $family_new;
										$j++;
										$values_info[$j]['field_name'] = 'article_category';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $value[$k];
									}else{
										$values_info[$j]['field_name'] = 'article_category_id';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $exist_family;
										$j++;
										$values_info[$j]['field_name'] = 'article_category';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $value[$k];
									}
								}
/*
								$family = $this->db->field("SELECT id FROM pim_article_categories WHERE id='".$value[$k]."'");

									$values_info[$j]['field_name'] = 'article_category_id';
									$values_info[$j]['tabel_name'] = 'pim_articles';
									$values_info[$j]['value'] = $family;*/
							}
							elseif($labels_info->f('field_name')=='name')
							{ 
								if(trim($value[$k])){
									$brand = $this->db->field("SELECT id FROM pim_article_brands WHERE name LIKE '$value[$k]%'");
									if(!$brand){
										$brand_value = $this->db->insert("INSERT INTO pim_article_brands SET name='".$value[$k]."'");
										$values_info[$j]['field_name'] = 'article_brand_id';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $brand_value;
									}else{
										$values_info[$j]['field_name'] = 'article_brand_id';
										$values_info[$j]['tabel_name'] = 'pim_articles';
										$values_info[$j]['value'] = $brand;
									}
								}
							}elseif($labels_info->f('field_name')=='name1' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='name2' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = $labels_info->f('field_name');
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='description' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = $labels_info->f('field_name');
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
							}
							elseif($labels_info->f('field_name')=='name1EN' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name3';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$english_name1=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='name2EN' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name4';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$english_name2=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='descriptionEN' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'description2';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$english_description=addslashes($value[$k]);
							}
							elseif($labels_info->f('field_name')=='name1FR' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name5';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$french_name1=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='name2FR' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name6';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$french_name2=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='descriptionFR' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'description3';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$french_description=addslashes($value[$k]);
							}
							elseif($labels_info->f('field_name')=='name1NL' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name7';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$dutch_name1=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='name2NL' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name8';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$dutch_name2=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='descriptionNL' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'description4';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$dutch_description=addslashes($value[$k]);
							}
							elseif($labels_info->f('field_name')=='name1DE' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name9';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$german_name1=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='name2DE' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'name10';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$german_name2=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='descriptionDE' && $labels_info->f('tabel_name')=='pim_articles_lang')
							{
								$values_info[$j]['field_name'] = 'description5';
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = addslashes($value[$k]);
								$german_description=addslashes($value[$k]);
							}elseif($labels_info->f('field_name')=='hide_stock')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='use_batch_no' || $labels_info->f('field_name')=='use_serial_no')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='max_stock')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
									$values_info[$j]['value'] = $value[$k];
							}elseif($labels_info->f('field_name')=='article_threshold_value')
							{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
									$values_info[$j]['value'] = $value[$k];
							}
						}

						if($labels_info->f('tabel_name')=='pim_articles' && $labels_info->f('field_name')=='article_id' && trim($value[$k])!='')
							{
								$search_by_id=true;
								$article_id = $this->db->field("SELECT article_id FROM pim_articles WHERE article_id='".$value[$k]."'");
								if(!$article_id)
								{
									$insert_cust = true;
									$begin_query = "INSERT INTO ";

								}else
								{
									$begin_query = "UPDATE ";
									$insert_cust = false;
									$end_query = " WHERE article_id='".$article_id."'";

								}
								//$customer_id = 1;
								$no_action_customer = false;
						}

						if($labels_info->f('tabel_name')=='pim_articles' && $labels_info->f('field_name')=='item_code' && trim($value[$k])!='')
							{
								if($search_by_id){
									continue;
								}
								$article_id = $this->db->field("SELECT article_id FROM pim_articles WHERE item_code='".$value[$k]."'");
								if(!$article_id)
								{
									$insert_cust = true;
									$begin_query = "INSERT INTO ";

								}else
								{
									$begin_query = "UPDATE ";
									$insert_cust = false;
									$end_query = " WHERE article_id='".$article_id."'";

								}
								//$customer_id = 1;
								$no_action_customer = false;
						}

						if($labels_info->f('tabel_name')=='pim_article_prices' && $labels_info->f('field_name')=='base_price' && trim($value[$k])!='')
						{
									$insert_price = true;
									$begin_query_price = "INSERT INTO ";

						}
						if($labels_info->f('tabel_name')=='pim_articles_lang' && $labels_info->f('field_name')=='name1' && trim($value[$k])!='')
							{
								$article_lang_id = $this->db->field("SELECT article_lang_id FROM pim_articles_lang WHERE name='".$value[$k]."'");
								if($article_id)
								{

									$begin_query_lang = "UPDATE ";
									$insert_cont = false;
									$end_query = " WHERE article_lang_id='".$article_lang_id."'";
								}else
								{
									
									$insert_cont = true;
									$begin_query_lang = "UPDATE ";
								}
								//$customer_id = 1;
								$no_action_customer = false;
						}
					}
				}
				$j++;
			}
			if($nr_headers==0)
			{
				msg::error ( gm("No field selected"),'error');
				//ark::$controller = 'save_imports';
				$in['step']=3;
				return false;
			}
			unset($insert_values);

		/*		echo "<pre>";
			print_r($values_info);
			echo "</pre>";*/
/*			var_dump($values_info);
			exit();*/
			$is_stock=0;
			$is_use_serial_no=0;
			$is_use_batch_no=0;
			$use_serial_no=false;
			$custom_threshold_value=0;
			foreach ($values_info as $key => $val) {
				if($val['value'] || $val['value'] == '0'){
					if($val['tabel_name']=='pim_articles')
					{
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
						if($val['field_name']=='stock'){
							$is_stock=1;
						}
						if($val['field_name']=='use_serial_no'){
							$is_use_serial_no=1;
							if($val['value']){
								$use_serial_no=true;
							}						
						}
						if($val['field_name']=='article_threshold_value' && $val['value'] != ARTICLE_THRESHOLD_VALUE){
					        $custom_threshold_value=1;
						}
					}
					if($val['tabel_name']=='pim_articles_lang' && $no_action_cust_addr==false)
					{
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
					}
					if($val['tabel_name']=='pim_article_prices')
					{
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
					}
				}
			}
			/*krsort($insert_values);*/
			/*echo "<pre>";
			print_r($insert_values);
			echo "</pre>";*/
			/*exit();*/

			foreach ($insert_values as $db_name => $query_string) {//db_name = numele tabelei;
				if ($db_name=='pim_articles')
				{
					if($insert_cust==true)
					{
						if($in['service']!=1){
							$in['service']=0;
						}
						if($begin_query && $db_name && $query_string){
							$article_id = $this->db->insert($begin_query.$db_name." SET ".$query_string." is_service='".$in['service']."',custom_threshold_value='".$custom_threshold_value."',created_at='".time()."' ");
							for($z=0;$z<5;$z++){
								$lang_id = $this->db->insert("INSERT INTO pim_articles_lang SET item_id='".$article_id."', lang_id='".$z."' ");
							}

							if(ALLOW_STOCK && !$is_stock && ($is_use_serial_no || $is_use_batch_no)){
								if($use_serial_no || $is_use_batch_no){
									$this->db->query("UPDATE pim_articles SET stock='0' WHERE article_id='".$article_id."' ");
									$this->db->query("UPDATE dispatch_stock SET stock= '0' WHERE article_id='".$article_id."'");
								}
								if($is_use_batch_no){
									$article=$this->db->query("SELECT * FROM pim_articles WHERE article_id='".$article_id."'");
									$this->db->query("INSERT INTO stock_movements SET `date`='".time()."',`created_by`='".$_SESSION['u_id']."',article_id='".$article_id."',article_name='".addslashes($article->f('internal_name'))."',item_code= 	'".addslashes($article->f('item_code'))."',movement_type = '2', quantity = '0', stock = '0.00', new_stock = '0' ");
								}						
							}

							if(ALLOW_STOCK && $is_stock){
								if($in['stock_location_id']){
									$location_add_array=explode("-", $in['stock_location_id']);
									if(count($location_add_array)==2){
										$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE address_id='".$location_add_array[1]."' ");
									}else{
										$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
									}
								}else{
									$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
								}							
								$article=$this->db->query("SELECT * FROM pim_articles WHERE article_id='".$article_id."'");
	       						$this->db->query("INSERT INTO dispatch_stock SET  article_id= '".$article_id."',
					                                              stock= '".$article->f('stock')."',
					                                              address_id='".$main_address_id."',
					                                              main_address=1 ");
	       						$this->db->insert(" INSERT INTO stock_movements SET
				        													date 						=	'".time()."',
				        													created_by 					=	'".$_SESSION['u_id']."',
				        													article_id 					=	'".$article_id."',
				        													article_name       			= 	'".addslashes($article->f('internal_name'))."',
				        													item_code  					= 	'".addslashes($article->f('item_code'))."',
				        													movement_type				=	'1',
				        													quantity 					= 	'".$article->f('stock')."',
				        													stock 						=	'0',
				        													new_stock 					= 	'".$article->f('stock')."'
				        							");
							}

							$inserted_cust_arr[$inserted_cust] = $article_id;
							$inserted_cust++;
						}
					}else
					{	
						if(ALLOW_STOCK && $is_stock){
							$old_stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id = '".$article_id."' ");
						}
						 if($begin_query && $db_name && $query_string){
						 	$this->db->query($begin_query.$db_name." SET ".$query_string." custom_threshold_value='".$custom_threshold_value."',updated_at='".time()."' ".$end_query);
						
						if(ALLOW_STOCK && !$is_stock && ($is_use_serial_no || $is_use_batch_no)){
							if($use_serial_no || $is_use_batch_no){
								$this->db->query("UPDATE pim_articles SET stock='0' WHERE article_id='".$article_id."' ");
								$this->db->query("UPDATE dispatch_stock SET stock= '0' WHERE article_id='".$article_id."'");
							}
							if($is_use_batch_no){
								$article=$this->db->query("SELECT * FROM pim_articles WHERE article_id='".$article_id."'");
								$this->db->query("INSERT INTO stock_movements SET `date`='".time()."',`created_by`='".$_SESSION['u_id']."',article_id='".$article_id."',article_name='".addslashes($article->f('internal_name'))."',item_code= 	'".addslashes($article->f('item_code'))."',movement_type = '2', quantity = '0', stock = '0.00', new_stock = '0' ");
							}						
						}

						if(ALLOW_STOCK && $is_stock){
							if($in['stock_location_id']){
								$location_add_array=explode("-", $in['stock_location_id']);
								if(count($location_add_array)==2){
									$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE address_id='".$location_add_array[1]."' ");
								}else{
									$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
								}
							}else{
								$main_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1");
							}
							$article=$this->db->query("SELECT * FROM pim_articles WHERE article_id='".$article_id."'");

       						$dispatch = $this->db->query("SELECT dispatch_stock.stock ,dispatch_stock.address_id
			        	   	                                FROM  dispatch_stock
			        	   	                                INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
			        	   	                                WHERE dispatch_stock.article_id='".$article_id."' AND dispatch_stock.address_id='".$main_address_id."' ");

       						if($dispatch->move_next()){

                                $stock_in_main=$dispatch->f('stock');

                                $stock_diff=$article->f('stock')-$old_stock;

                               if($stock_diff>0 ){
                                    $this->db->query("UPDATE dispatch_stock SET stock='".($stock_in_main+$stock_diff)."' WHERE dispatch_stock.article_id='".$article_id."' and address_id='".$main_address_id."'");
                               }elseif($stock_in_main>= abs($stock_diff) ) {
                                    $this->db->query("UPDATE dispatch_stock SET stock='".($stock_in_main-abs($stock_diff))."' WHERE dispatch_stock.article_id='".$article_id."' and address_id='".$main_address_id."'");
                               }else{
                        	        $this->db->query("UPDATE dispatch_stock SET stock='".$article->f('stock')."' WHERE dispatch_stock.article_id='".$article_id."' and address_id='".$main_address_id."'");
                                    $this->db->query("UPDATE dispatch_stock SET stock='0' WHERE dispatch_stock.article_id='".$article_id."' and address_id!='".$main_address_id."'");
                                  }
			        	 	}else{
			        	 		$this->db->query("INSERT INTO dispatch_stock SET stock='".$article->f('stock')."',dispatch_stock.article_id='".$article_id."',address_id='".$main_address_id."',main_address='1'");
			        	 		$this->db->query("UPDATE dispatch_stock SET stock='0' WHERE dispatch_stock.article_id='".$article_id."' and address_id!='".$main_address_id."'");
			        	 	}

       						$this->db->insert(" INSERT INTO stock_movements SET
			        													date 						=	'".time()."',
			        													created_by 					=	'".$_SESSION['u_id']."',
			        													article_id 					=	'".$article_id."',
			        													article_name       			= 	'".addslashes($article->f('internal_name'))."',
			        													item_code  					= 	'".addslashes($article->f('item_code'))."',
			        													movement_type				=	'2',
			        													quantity 					= 	'".$article->f('stock')."',
			        													stock 						=	'".$old_stock."',
			        													new_stock 					= 	'".$article->f('stock')."'
			        							");
						}

						$update_cust_arr[$update_cust] = $article_id;
						$update_cust++;
					  }
						//echo $begin_query.$db_name." SET ".$query_string.$end_query."<br>";
					}

				}

				if ($db_name=='pim_articles_lang')
				{
						$article_lang_id = $this->db->field("SELECT article_lang_id FROM pim_articles_lang WHERE name='".$value[$k]."'");
						if($article_lang_id)
						{

							$begin_contact_query = "UPDATE ";
							$insert_cont = true;
							$end_contact_query = " WHERE article_lang_id='".$article_lang_id."'";							

						}else{
							$begin_contact_query = "UPDATE ";
							$insert_cont = true;
						}
						/*if($in['language_multi']){*/
							if($french_name1){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name='".$french_name1."' , from_adveo=0  WHERE lang_id='2' AND item_id='".$article_id."' ");
							}
							if($french_name2){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name2='".$french_name2."' , from_adveo=0  WHERE lang_id='2' AND item_id='".$article_id."' ");
							}
							if($french_description){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET description='".$french_description."' , from_adveo=0  WHERE lang_id='2' AND item_id='".$article_id."' ");
							}
							if($english_name1){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name='".$english_name1."' , from_adveo=0  WHERE lang_id='1' AND item_id='".$article_id."' ");
							}
							if($english_name2){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name2='".$english_name2."' , from_adveo=0  WHERE lang_id='1' AND item_id='".$article_id."' ");
							}
							if($english_description){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET description='".$english_description."' , from_adveo=0  WHERE lang_id='1' AND item_id='".$article_id."' ");
							}
							if($dutch_name1){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name='".$dutch_name1."' , from_adveo=0  WHERE lang_id='3' AND item_id='".$article_id."' ");
							}
							if($dutch_name2){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name2='".$dutch_name2."' , from_adveo=0  WHERE lang_id='3' AND item_id='".$article_id."' ");
							}
							if($dutch_description){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET description='".$dutch_description."' , from_adveo=0  WHERE lang_id='3' AND item_id='".$article_id."' ");
							}
							if($german_name1){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name='".$german_name1."' , from_adveo=0  WHERE lang_id='4' AND item_id='".$article_id."' ");
							}
							if($german_name2){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET name2='".$german_name2."' , from_adveo=0  WHERE lang_id='4' AND item_id='".$article_id."' ");
							}
							if($german_description){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET description='".$german_description."' , from_adveo=0  WHERE lang_id='4' AND item_id='".$article_id."' ");
							}
/*							$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET ".$query_string." from_adveo=0  WHERE lang_id='3' AND item_id='".$article_id."' ");
							$inserted_contacts_arr[$inserted_contacts] = $contact_id;
							$inserted_contacts++;*/
						/*}*//*else{
							if($_SESSION['l']=='en'){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET ".$query_string." from_adveo=0  WHERE lang_id='1' AND item_id='".$article_id."' ");
							}elseif($_SESSION['l']=='fr'){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET ".$query_string." from_adveo=0  WHERE lang_id='2' AND item_id='".$article_id."' ");
							}elseif($_SESSION['l']=='nl'){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET ".$query_string." from_adveo=0  WHERE lang_id='3' AND item_id='".$article_id."' ");
							}elseif($_SESSION['l']=='de'){
								$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET ".$query_string." from_adveo=0  WHERE lang_id='4' AND item_id='".$article_id."' ");
							}
						}*/
/*							$article_lang_id = $this->db->query($begin_contact_query.$db_name." SET ".$query_string." from_adveo=0  WHERE lang_id='3' AND item_id='".$article_id."' ");
							$inserted_contacts_arr[$inserted_contacts] = $contact_id;
							$inserted_contacts++;*/
							$inserted_contacts_arr[$inserted_contacts] = $article_lang_id;
							$inserted_contacts++;

				}


				if ($db_name=='pim_article_prices')
				{
					if($insert_price==true)
					{
						$pimArticleId = $article_id;

						//Update article price if price is selected first
						if(isset($insert_values['pim_articles']) && isset($insert_values['pim_article_prices'])){
							if($article_id){
								$price_val = '';
								$explode = explode(',', $query_string);
								foreach ($explode as $val) {
									if ( preg_match("~\bprice\b~",$val) ){		
									  $price_val = $val;
									} 
								}

								if($price_val){
									$this->db->query("UPDATE pim_articles SET ".$price_val."WHERE article_id='".$article_id."'");
								}					
							}
						}

						//Delete Article Prices - only the base price
						$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$pimArticleId."' AND base_price='1'");
						
						$vats = $this->db->query("SELECT vat_id,price FROM pim_articles WHERE article_id='".$pimArticleId."'");

						$vat_value = $this->db->field("SELECT value FROM vats WHERE vat_id='".$vats->f('vat_id')."'");

						if($vat_value){
							$total_price = (($vats->f('price')*$vat_value)/100)+$vats->f('price');
						}else{
							$total_price = 0;
						}
						
						 $pim_article_id = $this->db->insert($begin_query_price.$db_name." SET total_price='".$total_price."', ".$query_string." article_id='".$article_id."', base_price='1' ");
						 $inserted_price_arr[$inserted_price] = $pim_article_id;
						 $inserted_price++;

						 //Insert price categories
						 $price_type = $this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$pimArticleId."'");
						 if(!$price_type){
						 	$price_type=1;
						 }
						 $articlePricesInfo = $this->db->query("SELECT * FROM pim_article_prices WHERE article_id = '".$pimArticleId."' AND base_price = '1'")->getALL();
						 $articlePricesInfo = $articlePricesInfo[0];

						 if($price_type==1){

						 	 $this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$pimArticleId."' AND base_price!='1'");
							 $prices = $this->db->query("SELECT * FROM pim_article_price_category");
							 
							 
								while ($prices->next()){
									//insert the default price
					                 if($prices->f('price_type')==2){
			                      		 $base_price = $articlePricesInfo['purchase_price'];
					                 }else{
				                         $base_price = $articlePricesInfo['price'];
					                 }

									switch ($prices->f('type')){
										case 1:                  //discount
												if($prices->f('price_value_type')==1){  // %
													$total_price = $base_price - ($prices->f('price_value') * $base_price/ 100);
												}else{ //fix
													$total_price = $base_price - $prices->f('price_value');
												}
										break;
										case 2:                 //profit margin
												if($prices->f('price_value_type')==1){  // %
													$total_price = $base_price + ($prices->f('price_value') * $base_price / 100);
												}else{ //fix
													$total_price = $base_price + $prices->f('price_value');
												}
										break;
									}
									
									$vat_value_prices   = $this->db->field("SELECT value from vats WHERE vat_id='".$vats->f('vat_id')."'");
									$total_with_vat = $total_price+($total_price*$vat_value_prices/100);

									//Insert Article Prices
									$this->db->query("INSERT INTO pim_article_prices SET
											article_id			= '".$articlePricesInfo['article_id']."',
											price_category_id	= '".$prices->f('category_id')."',
											price				= '".$total_price."',
											total_price			= '".$total_with_vat."'
										");
								}
						}else{
							//volume discount price categories
							$prices_vol= $this->db->query("SELECT * FROM  pim_article_prices WHERE article_id='".$articlePricesInfo['article_id']."' AND base_price=0 ");
							if ($prices_vol->records_count()){
								while ($prices_vol->move_next()) {
						               $total_price=$articlePricesInfo['price'] - ($articlePricesInfo['price']*$prices_vol->f('percent')/100);
						               $total_with_vat = $total_price+($total_price*$vat_value/100);
						               $db2->query("UPDATE pim_article_prices SET
						                                            price           = '".$total_price."',
						                                            total_price     = '".$total_with_vat."'
						                          WHERE article_price_id='".$prices_vol->f('article_price_id')."'

						                            ");
						        }
							}else{
								$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$pimArticleId."' AND base_price!='1'");
						        $prices_default = $this->db->query("SELECT * FROM article_price_volume_discount");
								while ($prices_default->next()){
		                              $total_price    = $articlePricesInfo['price'] - (($prices_default->f('percent')*$articlePricesInfo['price'])/100);
						             $total_with_vat = $total_price+($total_price*$vat_value/100);

				                      $this->db->query("INSERT INTO pim_article_prices SET
											article_id			= '".$articlePricesInfo['article_id']."',
											price_category_id	= '0',
											price				= '".$total_price."',
											total_price			= '".$total_with_vat."',
											from_q              = '".$prices_default->f('from_q')."',
											to_q                = '".$prices_default->f('to_q')."',
											percent             = '".$prices_default->f('percent')."'
										");
								}

							}	
						 //End Insert price categories	
						}



					}/*else
					{
						$this->db->query($begin_query.$db_name." SET ".$query_string.' use_serial_no=0 '.$end_query);
						$update_cust_arr[$update_cust] = $article_id;
						$update_cust++;
					}*/
				}
			}
		}

/*		if($in['k'] == 0){
		//header("Location: index.php?do=company-import_accounts&succ='Import completed successfully'");
		$user_info = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_info->next();
		$this->db->query("INSERT INTO pim_article_import_log SET
							`date`='".$in['timestamp']."', filename='".$filename."',
							article_added='".$inserted_cust."',
							article_updated='".$update_cust."'");
		}*/
		//$user_info = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		//$user_info->next();

		if($in['k'] == 0){
		//header("Location: index.php?do=company-import_accounts&succ='Import completed successfully'");
		
		$this->db->query("INSERT INTO pim_article_import_log SET
							`date`='".$in['timestamp']."', filename='".$filename."',
							username='".addslashes(get_user_name($_SESSION['u_id']))."',
							article_added='".$inserted_cust."',
							article_updated='".$update_cust."',
							article_id_added='".serialize($inserted_cust_arr)."',
							article_id_updated='".serialize($update_cust_arr)."'");
		}else{
			$a = $this->db->query("SELECT * FROM pim_article_import_log WHERE `date` = '".$in['timestamp']."' ");
			$ca = unserialize($a->f('article_id_added'));
			foreach ($ca as $key => $value) {
				array_push($inserted_cust_arr,$value);
			}
			$cu = unserialize($a->f('article_id_updated'));
			foreach ($cu as $key => $value) {
				array_push($update_cust_arr,$value);
			}
			$this->db->query("UPDATE pim_article_import_log SET
							article_added=article_added+'".$inserted_cust."',
							article_updated=article_updated+'".$update_cust."',
							article_id_added='".serialize($inserted_cust_arr)."',
							article_id_updated='".serialize($update_cust_arr)."',
							username='".addslashes(get_user_name($_SESSION['u_id']))."'
							WHERE `date`='".$in['timestamp']."' ");
		}
		$in['done'] = '1';
		$in['class3']='text-primary';
		$in['step']=3;
		msg::success ( gm('Import completed successfully'),'success');
		return true;
	}
	function save_imports_prices($in){
		$file_content = array();
		$tmp_header = array();
		$q = $this->db->query("SELECT * FROM pim_article_import WHERE `timestamp`='".$in['timestamp']."' ORDER BY line ASC ");
		while ($q->next()) {
			$line = unserialize($q->f('data'));
			foreach ($line as $key => $value) {
				if($q->f('type') == 1 ){
					$file_content[$value] = array();
					$tmp_header[$key] = $value;
				}else{
					array_push($file_content[$tmp_header[$key]],$value);					
				}
			}
		}

		$values_info = array();

		$j=0;
		$headers_ids= array();
		$update_cust = 0;
		$update_cust_arr = array();
		$filename = $in['xls_name'];

		for ($k=$in['k'];$k<$in['total_rows'];$k++)
		{
			foreach ($file_content as $headers => $value) {
				$headers = str_replace("'",' ',$headers);

				$value[$k] = addslashes($value[$k]);
				if($in[$headers])
				{
					if($k==0)
					{	
						if(in_array($in[$headers], $headers_ids))
						{					
							msg::error ( gm("You cannot select the same field twice"),'error');
							$in['error'] = 'error';
							json_out($in);
						}else
						{
							array_push($headers_ids, $in[$headers]);
						}
					}

					if(strpos($in[$headers],'main_') !== false){
						$main_label_id=str_replace("main_","",$in[$headers]);
						$labels_info = $this->db_import->query("SELECT field_name, tabel_name, is_custom_dd, type, custom_field FROM import_labels_articles WHERE import_label_id='".$main_label_id."'");
						$labels_info->next();
						if(trim($value[$k]) || trim($value[$k]) == 0 )
						{
							if($labels_info->f('field_name')=='item_code'){
								$article_id=$this->db->field("SELECT article_id FROM pim_articles WHERE item_code='".trim($value[$k])."' ");
								if($article_id){
									$values_info[$k]['article_id']=$article_id;	
								}
							}
						}				
					}
					if(strpos($in[$headers],'price_') !== false){
						$price_cat_id=str_replace("price_","",$in[$headers]);
						$category_data=$this->db->query("SELECT * FROM pim_article_price_category WHERE category_id='".$price_cat_id."' ")->getAll();
						if(!empty($category_data) && array_key_exists('article_id', $values_info[$k]) && is_numeric(trim($value[$k]))){
							if(array_key_exists('categories', $values_info[$k]) === false){
								$values_info[$k]['categories']=array();
							}
							if(array_key_exists($price_cat_id, $values_info[$k]['categories']) === false){
								$values_info[$k]['categories'][$price_cat_id]=array();
							}
							$values_info[$k]['categories'][$price_cat_id]['category_type']='4';
							$values_info[$k]['categories'][$price_cat_id]['price_value']=trim($value[$k]);	
							$values_info[$k]['categories'][$price_cat_id]['price_value_type']='2';	
						}
					}
				}
				$j++;
			}			
		}

		if(!empty($values_info)){
			foreach($values_info as $data){
				if(array_key_exists('article_id', $data) && array_key_exists('categories', $data)){
					foreach($data['categories'] as $category_id => $category_data){
						$price_type=$this->db->field("SELECT price_type FROM pim_article_price_category_custom WHERE category_id='".$category_id."' AND article_id='".$data['article_id']."'");
						if(!$price_type){
							$price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$category_id."'");
						}
						$this->db->query("DELETE FROM pim_article_price_category_custom WHERE category_id='".$category_id."' and article_id='".$data['article_id']."'");
						$this->db->query("INSERT INTO pim_article_price_category_custom SET   category_id='".$category_id."',
						   article_id='".$data['article_id']."',
							type='".$category_data['category_type']."',
							price_type='".$price_type."',
							price_value='".$category_data['price_value']."',
							price_value_type='".$category_data['price_value_type']."' ");

						$this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$category_id."' AND article_id='".$data['article_id']."' and base_price!=1");

						$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$data['article_id']."'");
						$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
						$total_with_vat = $category_data['price_value']+($category_data['price_value']*$vat_value/100);
						
						$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$data['article_id']."',
						price_category_id	= '".$category_id."',
						price				= '".$category_data['price_value']."',
						total_price			= '".$total_with_vat."'
						");

						//Insert log message
						$articleService = $this->db->query('SELECT is_service FROM pim_articles WHERE article_id ="'.$data['article_id'].'"')->getAll();
						$articleService = $articleService[0];
						if($articleService['is_service']){
							insert_message_log('aservice','{l}Service Price Category updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$data['article_id'],false,$_SESSION['u_id']);	
						} else {
							insert_message_log('article','{l}Article Price Category updated by{endl} '.get_user_name($_SESSION['u_id']),'article_id',$data['article_id'],false,$_SESSION['u_id']);			
						}
						$update_cust_arr[$update_cust] = $data['article_id'];				
					}
					$update_cust++;
				}			
			}
		}	

		if($in['k'] == 0){	
			$this->db->query("INSERT INTO pim_article_import_log SET
							`date`='".$in['timestamp']."', filename='".$filename."',
							username='".addslashes(get_user_name($_SESSION['u_id']))."',
							article_added='0',
							article_updated='".$update_cust."',
							article_id_added='',
							article_id_updated='".serialize($update_cust_arr)."'");
		}else{
			$a = $this->db->query("SELECT * FROM pim_article_import_log WHERE `date` = '".$in['timestamp']."' ");
			$cu = unserialize($a->f('article_id_updated'));
			foreach ($cu as $key => $value) {
				array_push($update_cust_arr,$value);
			}
			$this->db->query("UPDATE pim_article_import_log SET
							article_added='0',
							article_updated=article_updated+'".$update_cust."',
							article_id_added='',
							article_id_updated='".serialize($update_cust_arr)."',
							username='".addslashes(get_user_name($_SESSION['u_id']))."'
							WHERE `date`='".$in['timestamp']."' ");
		}
		$in['done'] = '1';
		$in['class3']='text-primary';
		$in['step']=3;
		msg::success ( gm('Import completed successfully'),'success');
		json_out($in);
	}
	function erase_data(&$in)
	{
		$array_table = array();
		$tables_row = array();
		$column_row = array();
		$i=0;
		$tables = $this->db->query("SHOW TABLES WHERE Tables_in_".DATABASE_NAME." LIKE 'pim_article%'")->getAll();
		foreach ($tables as $query => $table) {
			array_push($array_table, $table);
				foreach ($table as $querys => $tab) {
					array_push($tables_row, $tab);
				}
			$i++;
		}
		for($z=0;$z<$i;$z++){
			$this->db->query("TRUNCATE TABLE ".$tables_row[$z]."");
		}
		$array_table1 = array();
		$tables_row1 = array();
		$column_row1 = array();
		$x=0;
		$tables1 = $this->db->query("SHOW TABLES WHERE Tables_in_".DATABASE_NAME." LIKE 'dispatch_stock%'")->getAll();
		foreach ($tables1 as $query1 => $table1) {
			array_push($array_table1, $table1);
				foreach ($table1 as $querys1 => $tab1) {
					array_push($tables_row1, $tab1);
				}
			$x++;
		}
		for($y=0;$y<$x;$y++){
			$this->db->query("TRUNCATE TABLE ".$tables_row1[$y]."");
		}

		$array_table2 = array();
		$tables_row2 = array();
		$column_row2 = array();
		$x=0;
		$tables2 = $this->db->query("SHOW TABLES WHERE Tables_in_".DATABASE_NAME." LIKE 'beckup_art_pim_article%'")->getAll();
		foreach ($tables2 as $query2 => $table2) {
			array_push($array_table2, $table2);
				foreach ($table2 as $querys2 => $tab2) {
					array_push($tables_row2, $tab2);
				}
			$x++;
		}
		for($y=0;$y<$x;$y++){
			$this->db->query("TRUNCATE TABLE ".$tables_row2[$y]."");
		}

		$array_table3 = array();
		$tables_row3 = array();
		$column_row3 = array();
		$x=0;
		$tables3 = $this->db->query("SHOW TABLES WHERE Tables_in_".DATABASE_NAME." LIKE 'batches_dispatch_stoc%'")->getAll();
		foreach ($tables3 as $query3 => $table3) {
			array_push($array_table3, $table3);
				foreach ($table3 as $querys3 => $tab3) {
					array_push($tables_row3, $tab3);
				}
			$x++;
		}

		for($y=0;$y<$x;$y++){
			$this->db->query("TRUNCATE TABLE ".$tables_row3[$y]."");
		}
		msg::success ( gm("The data has been erased."),'success');
		return true;
	}
	function restore_data(&$in)
	{
		
		if($in['backup_id']!=0){
			global $config;

			//make temporary backup
			$array_exclude = array('pim_article_import');
			$this->db->query("show tables");
			$table_names=array();
			while ($this->db->move_next()){
				if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'pim_article')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'pim_articles')!==FALSE || strpos($this->db->f("Tables_in_".DATABASE_NAME), 'dispatch_stock')!==FALSE)
				{
					if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
						$table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
					}				
				}				
			}
			$tables='';
			foreach($table_names as $key=>$value){
				$tables.=$value.' ';
			}
			$dir=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME;
			$file_name='tmp_backup_article_'.$_SESSION['u_id'].'.sql';
			shell_exec('mysqldump --user='.$config['db_user'].' --password='.$config['db_pass'].' --host='.$config['db_host'].' '.DATABASE_NAME.' '.$tables.' > '.$dir.'/'.$file_name);
			//end temporary backup

			$this->erase_data($in);	
			set_time_limit(0);
		    ini_set('memory_limit', '-1');   
			$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup'.$in['backup_id'].'.xml';

			clearstatcache();
			//check if original backup is empty
			if(!filesize($file1)) {
				//restore temporary backup
				$this->restore_temporary_backup($dir.'/'.$file_name);		
				msg::error ( gm('Restore failed'),'error');
				return false;
			}

			//check if original backup is ok
			$use_xml_errors = libxml_use_internal_errors(true);
			$xmlDoc = simplexml_load_file($file1);
			if($xmlDoc === false){
				//restore temporary backup
				$this->restore_temporary_backup($dir.'/'.$file_name);	
				msg::error ( gm('Restore failed'),'error');
			}else{
				$i=0;
				$backup_error=false;
				$this->db->exitOnError=false;
				$this->db->query("TRUNCATE TABLE dispatch_stock_address");
				foreach ($xmlDoc as $key => $value) {
					foreach ($value as $k => $v) {
						$add = false;
						$sql   = "INSERT INTO {$key} SET ";
						foreach ($v as $field => $val) {
							$add=true;
							$sql .= " `".$field."`='".addslashes($val)."',";
						}
						$sql = rtrim($sql,',');
						if($add){
				  			$sql_res=$this->db->insert($sql);
				  			if($sql_res === false){
				  				$backup_error=true;
				  				break 2;
				  			}
						}
					}
					$i++;
				}
				$this->db->exitOnError=true;
				if($backup_error){
					//restore temporary backup
					$this->restore_temporary_backup($dir.'/'.$file_name);	
					msg::error ( gm('Restore failed'),'error');
				}else{
					msg::success ( gm('Restore completed successfully'),'success');
					unlink($dir.'/'.$file_name);
				}
			}

			libxml_clear_errors();
			libxml_use_internal_errors($use_xml_errors);	
	
			return true;
		}else{
			msg::error ( gm('Select a file'),'error');
	    	return false;
		}
		
	}

	function restore_temporary_backup($file){
		$this->erase_data(array());
		shell_exec('mysql --user='.$config['db_user'].' --password='.$config['db_pass'].' --host='.$config['db_host'].' '.DATABASE_NAME.' < '.$file);
		//delete temporary backup
		unlink($file);	
	}

	function addCategory_validate(&$in){
		$v=new validation($in);
		$v->field('name','name','required');
		return $v->run();
	}

	function addCategory(&$in){
		if(!$this->addCategory_validate($in)){
			json_out($in);
			return false;
		}
		$this->db->query("INSERT INTO pim_article_categories SET name='".addslashes($in['name'])."' ");
		msg::success(gm('Category added'),'success');
		return true;
	}

	function editCategory(&$in){
		$v=new validation($in);
		$v->field("id",'Id','required:exist[pim_article_categories.id]');
		$v->field('name','name','required');
		if(isset($in['ledger_account_id']) && $in['ledger_account_id']>0){
			$v->field("ledger_account_id",'Id','required:exist[pim_article_ledger_accounts.id]');
		}
		if(!$v->run()){
			json_out($in);
		}
		$this->db->query("UPDATE pim_article_categories SET name='".addslashes($in['name'])."',ledger_account_id='".$in['ledger_account_id']."' WHERE id='".$in['id']."' ");
		if(isset($in['ledger_account_id']) && $in['ledger_account_id']>0 && $in['apply_ledger'] && $in['apply_ledger'] == '1'){
			$this->db->query("UPDATE pim_articles SET ledger_account_id='".$in['ledger_account_id']."' WHERE article_category_id='".$in['id']."'");
		}
		msg::success(gm('Category updated'),'success');
		return true;
	}

	function uploadCategoryPhoto($in){
		$response=array();
	    if (!empty($_FILES)) {
	    	$size=$_FILES['Filedata']['size'];
	    	if(!defined('MAX_IMAGE_SIZE') && $size>256000){
              	$response['error'] = gm('Size exceeds 200kb');
              	$response['filename'] = $_FILES['Filedata']['name'];
            	echo json_encode($response);
            	exit();
          	}
	       $this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");

	      $tempFile = $_FILES['Filedata']['tmp_name'];
	      $ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);
	      $in['name'] = 'tmp_file_'.time().'.'.$ext;
	      
	      $targetPath = INSTALLPATH.'upload';

	      
	      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
	      // Validate the file type
	      $fileTypes = array('jpg','jpeg','gif'); // File extensions
	      $fileParts = pathinfo($_FILES['Filedata']['name']);
	      mkdir(str_replace('//','/',$targetPath), 0775, true);
	      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
	      	if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE){
	            $image = new SimpleImage();
	            $image->load($_FILES['Filedata']['tmp_name']);
	            $image->scale(MAX_IMAGE_SIZE,$size);
	            $image->save($targetFile);
	        }else{
	            move_uploaded_file($tempFile,$targetFile);             
	        }
	        $in['tmp_file_name'] = $in['name'];
	        $this->saveCategoryToAmazon($in);
	        $response['success'] = gm('Photo uploaded');
	        $response['filename'] = $_FILES['Filedata']['name'];
	       	echo json_encode($response);
	        // ob_clean();
	      } else {
	        $response['error'] = gm('Invalid file type.');
	        $response['filename'] = $_FILES['Filedata']['name'];
	       	echo json_encode($response);
	      }
	      exit();
	    }
	}

	  /**
	   * undocumented function
	   *
	   * @return void
	   * @author
	   **/
	  function saveCategoryToAmazon(&$in){
	    
	    $in['up_folder']='../upload/'.DATABASE_NAME.'/pim_article_photo';
	    $parentId=$in['category_id'];
	    if($in['tmp_file_name']){
	      global $config;
	      ark::loadLibraries(array('aws'));
	      $a = new awsWrap(DATABASE_NAME);
	      $this->db->query("UPDATE pim_article_categories SET folder='".$in['up_folder']."', upload_amazon=1 WHERE id='".$in['category_id']."' ");
	     
	      $artPath = INSTALLPATH.'upload/'.$in['tmp_file_name'];
	    
	      $ext = pathinfo($artPath,PATHINFO_EXTENSION);
	      
	      $artFile = 'category/category_'.$parentId.".".$ext;
	    
	      $a->uploadFile($artPath,$artFile);
	      unlink($artPath);     
	      
	      $link =  $a->getLink($config['awsBucket'].DATABASE_NAME.'/category/category_'.$parentId.".".$ext);
	      if($link){
	      	$this->db->query("UPDATE pim_article_categories SET file_name='".$link."',amazon_path='".$config['awsBucket'].DATABASE_NAME.'/category/category_'.$parentId.".".$ext."' WHERE id='".$in['category_id']."' ");
	      }   				
	   }
	}

   	function removeCategoryPhoto(&$in){
	    global $config;
	    $info = $this->db->query("SELECT * FROM pim_article_categories WHERE id='".$in['category_id']."' ");
	    ark::loadLibraries(array('aws'));
	    $aws = new awsWrap(DATABASE_NAME);
	    $link =  $aws->doesObjectExist($info->f('amazon_path'));
	    if($link){
	        $aws->deleteItem($info->f('amazon_path'));
	        $this->db->query("UPDATE pim_article_categories 
	    	SET folder='',
	    		upload_amazon='0',
	    		file_name='',
	    		amazon_path=''
	    	WHERE id='".$in['category_id']."'");
	    	msg::success(gm('Photo removed'),'success');
	    	return true; 
	    }else{
	    	msg::error(gm('An error occured'),'error');
	    	json_out($in);
	    }
	}

	function deleteCategory(&$in){
		$arts=$this->db->field("SELECT COUNT(article_id) FROM pim_articles WHERE article_category_id='".$in['category_id']."' ");
		if($arts){
			msg::error(gm('Category attached to articles'),'error');
			json_out($in);
		}else{
			$info = $this->db->query("SELECT * FROM pim_article_categories WHERE id='".$in['category_id']."' ");
			if($info->f('upload_amazon')){
				ark::loadLibraries(array('aws'));
			    $aws = new awsWrap(DATABASE_NAME);
			    $link =  $aws->doesObjectExist($info->f('amazon_path'));
			    if($link){
			        $aws->deleteItem($info->f('amazon_path'));
			    }
			}
			$this->db->query("DELETE FROM pim_article_categories WHERE id='".$in['category_id']."' ");
			msg::success(gm('Category deleted'),'success');
			return true;
		}
	}

	function editLedger(&$in){
		if($in['id']=='tmp'){
			$last_order=$this->db->field("SELECT sort_order FROM pim_article_ledger_accounts ORDER BY sort_order DESC LIMIT 1");
			$this->db->query("INSERT INTO pim_article_ledger_accounts SET name='".addslashes($in['name'])."', description='".addslashes($in['description'])."', sort_order='".($last_order+1)."' ");
		}else{
			$this->db->query("UPDATE pim_article_ledger_accounts SET name='".addslashes($in['name'])."',description='".addslashes($in['description'])."' WHERE id='".$in['id']."' ");
		}
		msg::success(gm('Changes done'),'success');
		return true;
	}

	function deleteLedger(&$in){
		$this->db->query("DELETE FROM pim_article_ledger_accounts WHERE id='".$in['id']."' ");
		msg::success(gm('Changes done'),'success');
		return true;
	}

	function saveLedgers(&$in){
		$this->db->query("UPDATE pim_article_ledger_accounts SET sort_order='0' ");
		$i=0;
		foreach($in['list'] as $key=>$value){
			$this->db->query("UPDATE pim_article_ledger_accounts SET sort_order='".$i."' WHERE id='".$value['id']."' ");
			$i++;
		}
		msg::success(gm('Changes done'),'success');
		json_out($in);
	}

	function delete_custom_price(&$in){
		if($in['customer_id'] && $in['article_id']){
			$this->db->query("DELETE FROM customer_custom_article_price WHERE customer_id='".$in['customer_id']."' AND  article_id='".$in['article_id']."' ");
	    	msg::success ( gm("Changes have been saved."),'success');
	   		return true;
		}
		return false;
	  }
	function update_custom_price(&$in)
	{
		
		if(!$this->update_custom_price_validate($in)){
           return false;
		}

	    $article_details =   $this->db->query("SELECT item_code, internal_name FROM pim_articles WHERE article_id='".$in['article_id']."' ");
	    $article_number  = 	 $article_details->f('item_code').' - '.$article_details->f('internal_name');
  
        $custom_price =   $this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");

        if($custom_price->next()){
        	$this->db->query("UPDATE customer_custom_article_price SET
						price				= '".$in['custom_price']."', 
						article_number 		= '".$article_number."'
						WHERE customer_id='".$in['customer_id']."'   AND  article_id='".$in['article_id']."'
				");
        }else{
        	$this->db->query("INSERT INTO customer_custom_article_price SET
						price				= '".$in['custom_price']."',
						article_number 		='".$article_number."',
						customer_id 		='".$in['customer_id']."',
						article_id 			='".$in['article_id']."'
				");
        }
       
		msg::success (gm("Price succefully updated."),'success');
		return true;
	}

	function update_custom_price_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');
		$v -> f('customer_id', 'Customer Id', 'required:exist[customers.customer_id]');
		$v->field('custom_price','Price','required:numeric');
		return $v -> run();

	}

	function set_default_price_category(&$in)
	{
		$this->db->query("UPDATE pim_article_price_category SET
							default_category = '0'	
					      WHERE default_category='1'");
		$this->db->query("UPDATE pim_article_price_category SET
							default_category = '1'	
					      WHERE category_id='".$in['id']."'");
		msg::success ( "Article Price Category updated.",'success');
		
		return true;
	}

	  function tryAddSC(&$in){
    if(!$this->tryAddSCValid($in)){ 
        json_out($in);
        return false; 
    }

    global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
	$db_users = new sqldb($database_users);

    $in['article_id'] = $in['item_id'];
    //$name_user= $db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
    $name_user= $db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    //Set account default vat on customer creation
    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

    if(empty($vat_regime_id)){
      $vat_regime_id = 1;
    }
    if($in['add_customer']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
  $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $in['sc_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['name'])."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            user_id = '".$_SESSION['u_id']."',
                                            acc_manager_name = '".addslashes($name_user)."',
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            type          		 = '0',
                                            is_supplier          = '1',
                                            zip_name  = '".$in['zip']."'");
      
       $in['second_address_id']= $this->db->insert("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['sc_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        
       $in['supplier_name']= $in['name'];
      // include_once('../apps/company/admin/model/customer.php');
        //$in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
  $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['sc_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
  $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['sc_id']."' ");
        }

        /*$show_info=$db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
		$show_info=$db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
  /*$db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
	$db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                        ['user_id' => $_SESSION['u_id'],
                         'name'    => 'company-customers_show_info',
                         'value'   => '1']
                    );                            
        } else {
  /*$db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
	$db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",
                            ['value'=>'1',
                             'user_id'=>$_SESSION['u_id'],
                             'name'=>'company-customers_show_info']
                        );                                
        }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
  doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }

        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          /*$this->db->query("UPDATE pim_p_orders SET 
            second_customer_id='".$in['sc_id']."', 
            second_address_id='".$in['second_address_id']."',
           
            WHERE p_order_id='".$in['item_id']."' ");     */    
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
    if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $in['sc_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['lastname'])."',
                                            firstname = '".addslashes($in['firstname'])."',
                                            user_id = '".$_SESSION['u_id']."',
                                            acc_manager_name = '".addslashes($name_user)."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            type = 1,
                                            is_supplier          = '1',
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."'");
      
        $in['second_address_id']= $this->db->insert("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['sc_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");

         $in['supplier_name']= $in['firstname'].' '.$in['lastname'];

        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['sc_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['sc_id']."' ");
        }

        /*$show_info=$db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
		$show_info=$db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
			$db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                        ['user_id' => $_SESSION['u_id'],
                         'name'    => 'company-customers_show_info',
                         'value'   => '1']
                    );                            
        } else {
          /*$db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
			$db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          /*$this->db->query("UPDATE pim_p_orders SET 
            second_customer_id='".$in['sc_id']."', 
            second_address_id='".$in['second_address_id']."',
           
            WHERE p_order_id='".$in['item_id']."' ");    */     
        }
         $in['supplier_id']=$in['sc_id'];
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['sc_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
      
    return true;
    }

    /**
    * undocumented function
    *
    * @return void
    * @author PM
    **/
    function tryAddSCValid(&$in)
    {
    if($in['add_customer']){
        $v = new validation($in);
        $v->field('name', 'name', 'required:unique[customers.name]');
        $v->field('country_id', 'country_id', 'required');
        return $v->run();  
    }
    if($in['add_contact']){
        $v = new validation($in);
        $v->field('firstname', 'firstname', 'required');
        $v->field('lastname', 'lastname', 'required');
        $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
        $v->field('country_id', 'country_id', 'required');
        return $v->run();  
    }
    return true;
    }

    function check_vies_vat_number(&$in){
    $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

    if(!$in['value'] || $in['value']==''){      
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }
    $value=trim($in['value']," ");
    $value=str_replace(" ","",$value);
    $value=str_replace(".","",$value);
    $value=strtoupper($value);

    $vat_numeric=is_numeric(substr($value,0,2));

    if(!in_array(substr($value,0,2), $eu_countries)){
        $value='BE'.$value;
    }
    if(in_array(substr($value,0,2), $eu_countries)){
        $search   = array(" ", ".");
        $vat = str_replace($search, "", $value);
        $_GET['a'] = substr($vat,0,2);
        $_GET['b'] = substr($vat,2);
        $_GET['c'] = '1';
        $dd = include('valid_vat.php');
    }else{
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }
    if(isset($response) && $response == 'invalid'){
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }else if(isset($response) && $response == 'error'){
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }else if(isset($response) && $response == 'valid'){
        $full_address=explode("\n",$result->address);
        switch($result->countryCode){
  case "RO":
      $in['comp_address']=$full_address[1];
      $in['comp_city']=$full_address[0];
      $in['comp_zip']=" ";
      break;
  case "NL":
      $zip=explode(" ",$full_address[2],2);
      $in['comp_address']=$full_address[1];
      $in['comp_zip']=$zip[0];
      $in['comp_city']=$zip[1];
      break;
  default:
      $zip=explode(" ",$full_address[1],2);
      $in['comp_address']=$full_address[0];
      $in['comp_zip']=$zip[0];
      $in['comp_city']=$zip[1];
      break;
        }

        $in['comp_name']=$result->name;

        $in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
        $in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
        $in['full_details']=$result;
        $in['vies_ok']=1;
        if($in['customer_id']){
 				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
        }
        if(ark::$method == 'check_vies_vat_number'){
  msg::success ( gm('VAT Number is valid'),'success');
  json_out($in);
        }
        return true;
    }else{
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }
    if(ark::$method == 'check_vies_vat_number'){
        json_out($in);
    }
    }

    function setArticleHasVariants(&$in){
    	$in['hide_stock']=$in['has_variants'] ? true : false;
    	$this->db->query("UPDATE pim_articles SET has_variants='".$in['has_variants']."',hide_stock='".$in['hide_stock']."' WHERE article_id='".$in['article_id']."' ");
    	msg::success (gm('Success'),'success');
    	json_out($in);
    }

    function setArticleVariantType(&$in){
    	$this->db->query("UPDATE pim_article_variants SET type_id='".$in['type_id']."' WHERE id='".$in['id']."' ");
    	json_out($in);
    }

    function deleteArticleVariant(&$in){
    	$this->db->query("DELETE FROM pim_article_variants WHERE id='".$in['id']."' ");
    	msg::success(gm("Changes have been saved."),'success');
    	json_out($in);
    }

    function saveBarcodes(&$in){
    	if($in['next_auto_barcode'] && !is_numeric($in['next_auto_barcode'])){
    		$v=new validation($in);
    		$v->field('next_auto_barcode','Barcode','integer');
    		if(!$v->run()){
    			json_out($in);
    		}		
    	}
    	$check_variable=$this->db->field("SELECT value FROM settings WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE'");
    	if($in['next_auto_barcode'] && !is_null($check_variable) && $in['next_auto_barcode']<$check_variable){
    		msg::error(gm('Value must be at least the same with the current one or greater'),'next_auto_barcode');
    		json_out($in);
    	}
    	if(is_null($check_variable)){
    		$this->db->query("INSERT INTO settings SET constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE',value='".$in['next_auto_barcode']."' ");
    	}else{
    		$this->db->query("UPDATE settings SET value='".$in['next_auto_barcode']."' WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE' ");
    	}
    	msg::success(gm("Changes have been saved."),'success');
    	json_out($in);
    }

}//end class
?>