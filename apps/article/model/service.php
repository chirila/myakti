<?php
/**
 * Article class
 *
 * @author      Arkweb SRL
 * @link        https://go.salesassist.eu
 * @copyright   [description]
 * @package 	article
 */
class service
{
	var $pag = 'aservice';
	var $field_n = 'article_id';

	/**
	 * [article description]
	 * @return [type] [description]
	 */
	function service()
	{
		$this->db = new sqldb;
		$this->db2 = new sqldb;
		$this->db3 = new sqldb;
		$this->db4 = new sqldb;
	}

    function saveAddToArchived(&$in)
    {
        if($in['all']){
            if($in['value'] == 1){
                foreach ($in['item'] as $key => $value) {
                    $_SESSION['add_service_to_archive'][$value]= $in['value'];
                }
            }else{
                foreach ($in['item'] as $key => $value) {
                    unset($_SESSION['add_service_to_archive'][$value]);
                }
            }
        }else{
            if($in['value'] == 1){
                $_SESSION['add_service_to_archive'][$in['item']]= $in['value'];
            }else{
                unset($_SESSION['add_service_to_archive'][$in['item']]);
            }
        }
        $all_pages_selected=false;
        $minimum_selected=false;
        if($_SESSION['add_service_to_archive'] && count($_SESSION['add_service_to_archive'])){
            if($_SESSION['tmp_add_service_to_archive'] && count($_SESSION['tmp_add_service_to_archive']) == count($_SESSION['add_service_to_archive'])){
                $all_pages_selected=true;
            }else{
                $minimum_selected=true;
            }
        }
        $in['all_pages_selected']=$all_pages_selected;
        $in['minimum_selected']=$minimum_selected;

        json_out($in);
    }

    function saveAddToArchivedAll(&$in)
    {

        $all_pages_selected = false;
        $minimum_selected = false;
        if ($in['value']) {
            unset($_SESSION['add_service_to_archive']);
            if ($_SESSION['tmp_add_service_to_archive'] && count($_SESSION['tmp_add_service_to_archive'])) {
                $_SESSION['add_service_to_archive'] = $_SESSION['tmp_add_service_to_archive'];
                $all_pages_selected = true;
            }
        } else {
            unset($_SESSION['add_service_to_archive']);
        }
        $in['all_pages_selected'] = $all_pages_selected;
        $in['minimum_selected'] = $minimum_selected;

        json_out($in);
    }

    /**
     * Add article
     * @param array $in
     * @return boolean
     */
	function add(&$in)
	{
		$in['failure'] = false;
		if(!$in['product_id']){ //is direct save no product involved
			if(!$this->add_validate($in)){
				
				$in['failure'] = true;
				return false;
			}
		}else{
			$in['name']         	= 	$in['article_name'];
			$in['item_code']    	= 	$in['article_number'];
			$in['internal_name']	=	$in['article_internal_name'];
			if(!$this->add_to_product_validate($in)){
				$in['failure'] = true;
				return false;
			}

		}

		$query_sync=array();
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['sale_unit']=1;
		}
        if($in['article_threshold_value']!=ARTICLE_THRESHOLD_VALUE){
        	$custom_threshold_value=1;
        }else{
        	$custom_threshold_value=0;
        }

        	if(!$in['exclude_serial_no']){
        		$in['exclude_serial_no']=0;
        	}
        	if(!$in['exclude_batch_no']){
        		$in['exclude_batch_no']=0;
        	}
		if(!$in['supplier_id']){
           $in['supplier_name']='';
		}
		$in['price_type'] = PRICE_TYPE;
		if($in['price_type']==1){
            $in['use_percent_default']=0;
		}
		if(!$in['packing']){
			$in['packing'] = 1;
		}
		if(!$in['sale_unit']){
			$in['sale_unit'] = 1;
		}

		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
															item_code  							= '".$in['item_code']."',
															ean_code  							= '".$in['ean_code']."',
															
															use_percent_default  				= '".$in['use_percent_default']."',
															supplier_reference      = '".$in['supplier_reference']."',
															vat_id									= '".$in['vat_id']."',
															article_brand_id			= '".$in['article_brand_id']."',
															ledger_account_id			= '".$in['ledger_account_id']."',
															article_category_id			= '".$in['article_category_id']."',
															origin_number   			= '".$in['origin_number']."',
														
															sale_unit        			= '".$in['sale_unit']."',
															internal_name       		= '".$in['internal_name']."',
															packing          			= '".return_value2($in['packing'])."',
															price_type          		= '".$in['price_type']."',
														
															weight    					= '".$in['weight']."',
															show_img_q								= '".$in['show_img_q']."',
															supplier_id								= '".$in['supplier_id']."',
															supplier_name							= '".$in['supplier_name']."',
															aac_price			        = '".return_value($in['aac_price'])."',
															is_service			        = '1',
															billable			        = '".$in['billable']."',
															created_at='".time()."'
															
															");
		
       $this->db->query("INSERT INTO dispatch_stock SET  article_id= '".$in['article_id']."',
				                                              stock= '".return_value2($in['stock'])."',
				                                              address_id='".$main_address_id."',
				                                              main_address=1

				");

        
		$active_languages = $this -> db -> query("SELECT * FROM pim_lang GROUP BY lang_id");
		while ($active_languages -> next()) {
			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '" . $in['article_id'] . "',
		                                                                         lang_id                = '" . $active_languages -> f('lang_id') . "',
		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."' ");
		}

		

		
		//insert the base price
		$this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".return_value($in['purchase_price'])."'
						");


     if($in['price_type']==1){
			$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
				//insert the default price
                 if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }
				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) - ($prices->f('price_value') * return_value($base_price)/ 100);
							}else{ //fix
								$total_price = return_value($base_price) - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) + ($prices->f('price_value') * return_value($base_price) / 100);
							}else{ //fix
								$total_price = return_value($base_price) + $prices->f('price_value');
							}

					break;
				}

					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");



			}
		}else{   //quantity discounts

			$prices = $this->db->query("SELECT * FROM article_price_volume_discount");
				while ($prices->next()){
                     //var price=((percent*base_price)/100)+base_price;
                     $total_price    = (($prices->f('percent')*return_value($in['price']))/100)+return_value($in['price']);

		             $vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		             $total_with_vat = $total_price+($total_price*$vat_value/100);

                      $this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");


				}


		}

		foreach ($in['tax_id'] as $tax_id=>$is_tax){
			$this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$tax_id."'

				");
		}

	


		
		if($in['product_id']){
	        $in['article_name']         = '';
			$in['article_number']    	= '';
			$in['price']    			= '';
			$in['origin_number']    	= '';
			$in['ean_code']    			= '';
			$in['article_internal_name']= '';
			$in['stock']    = '';

		}


		$in['view_add_new_article_box']=0;
		msg::success(gm("Service succefully added."),'success');
		insert_message_log($this->pag,'{l}Service added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['article_id'],false,$_SESSION['u_id']);

		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$dbu = new sqldb($db_config);

		/*$show_info=$dbu->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
															AND name		= 'article-article_show_info'	");*/
		$show_info=$dbu->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
															AND name		= :name	",
														['user_id' 	=> $_SESSION['u_id'],
														 'name'		=> 'article-article_show_info']
													);															
		if(!$show_info->move_next()) {
			/*$dbu->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
													name 	= 'article-article_show_info',
													value 	= '1' ");*/
			$dbu->insert("INSERT INTO user_meta SET 	user_id = :user_id,
													name 	= :name,
													value 	= :value ",
												['user_id' => $_SESSION['u_id'],
												 'name'    => 'article-article_show_info',
												 'value'   => '1']
											);													
		} else {
			/*$dbu->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
														  AND name 	 	= 'article-article_show_info' ");*/
			$dbu->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
														  AND name 	 	= :name ",
														['value'=>1,
														 'user_id'=>$_SESSION['u_id'],
														 'name'=>'article-article_show_info']
													);														  
		}
		update_articles($in['article_id'],DATABASE_NAME);

		$count = $this->db->field("SELECT COUNT(article_id) FROM pim_articles ");
		if($count == 1){
			doManageLog('Created the first services.','',array( array("property"=>'first_module_usage',"value"=>'Article') ));
		}

		return true;
	}

	/**
	 * Validate add article
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in)
	{

		$v = new validation($in);

		if (ark::$method == 'update') {

			$option = ".( article_id != '" . $in['article_id'] . "' AND active='1')";
		} else {
			$option = "";
		}
		//$v -> f('name', 'Name', 'required:unique[pim_articles_lang.name' . $option_lang . ']');
		//  $v -> f('name', 'Name', 'required');
		//$v -> f('category_id', 'Category', 'required');
		$v -> f('item_code', 'Item Code', 'required:unique[pim_articles.item_code' . $option . ']');
		$v -> f('internal_name', 'Internal name', 'required');
		//$v -> f('vat_id', 'Vat Id', 'required[pim_vats.vat_id]');
		//$v -> f('price', 'Price', 'required:numeric');
		//$v -> f('total_price', 'Total Price', 'required:numeric');
   /*
		if(ALLOW_STOCK){
			$v -> f('stock', 'Stock', 'numeric');
            }
		if(ALLOW_ARTICLE_PACKING){
			if(ALLOW_ARTICLE_SALE_UNIT){
				$v -> f('sale_unit', 'Sale Unit', 'required:integer:greater_than[1]');
			}
			$v -> f('packing', 'Packing', 'required:greater_than[1]');
		}
	*/	
		return $v -> run();
	}

	/**
	 * Update_main_details_validate
	 * @param  array $in
	 * @return boolean
	 */
	function update_main_details_validate(&$in)
	{

		$v = new validation($in);



		$option = ".( article_id != '" . $in['article_id'] . "')";

        $v -> f('article_number', 'Item Code', 'required:unique[pim_articles.item_code' . $option . ']');
        $v -> f('article_internal_name', 'Internal name', 'required:unique[pim_articles.internal_name' . $option . ']');
		$v -> f('price', 'Price', 'required:numeric');
			if(ALLOW_STOCK){
			$v -> f('stock', 'Stock', 'numeric');
            }

		if(ALLOW_ARTICLE_PACKING){
			if(ALLOW_ARTICLE_SALE_UNIT){
				$in['sale_unit']=1;

			}
			$in['packing']=1;

		}


	   $in['vat_id'] = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");

	   $vat= $this->db->field("SELECT value FROM vats WHERE vat_id='".$in['vat_id']."'");

	   $in['total_price']= display_number(return_value($in['price'])+(return_value($in['price'])* ($vat/100)));



		return $v -> run();
	}

	

	/**
	 * Update article
	 * @param  array $in
	 * @return boolean
	 */
	function update(&$in)
	{
	
		if(!$this->update_validate($in)){
			return false;
		}
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['sale_unit']=1;
		}
		

		$article_vat = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
        $price_type_old=$this->db->field("SELECT pim_articles.price_type FROM pim_articles  WHERE article_id='".$in['article_id']."'");
        if($price_type_old==1 || ($price_type_old==2 && $in['price_type']==1) ){
           $this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");

		}
		if($price_type_old==1 && $in['price_type']==2){
			$this->db->query("DELETE FROM pim_article_price_category_custom WHERE article_id='".$in['article_id']."'");
		}


        if(!$in['supplier_id']){
           $in['supplier_name']='';
		}
		if($in['price_type']==1){
            $in['use_percent_default']=0;
		}
       	$old_price_type=$this->db->field("SELECT pim_articles.price_type  FROM pim_articles WHERE article_id='".$in['article_id']."' ");
       	
       	if(!$in['packing']){
			$in['packing'] = 1;
		}
		if(!$in['sale_unit']){
			$in['sale_unit'] = 1;
		}

		$this->db->query("UPDATE pim_articles SET
									item_code  			= '".$in['item_code']."',
									ean_code  			= '".$in['ean_code']."',
									
										use_percent_default  				= '".$in['use_percent_default']."',
									vat_id				= '".$in['vat_id']."',
									origin_number       = '".$in['origin_number']."',
									supplier_reference          = '".$in['supplier_reference']."',
									article_brand_id    = '".$in['article_brand_id']."',
									ledger_account_id			= '".$in['ledger_account_id']."',
									article_category_id = '".$in['article_category_id']."',
									
									sale_unit           = '".$in['sale_unit']."',
									internal_name       = '".$in['internal_name']."',
									packing             = '".return_value2($in['packing'])."',
									price_type          = '".$in['price_type']."',
									article_threshold_value    = '".$in['article_threshold_value']."',
									weight    			= '".return_value($in['weight'])."',
									show_img_q								= '".$in['show_img_q']."',
									supplier_id								= '".$in['supplier_id']."',
								    supplier_name							= '".$in['supplier_name']."',
								    aac_price			        = '".return_value($in['aac_price'])."',
									billable			        = '".$in['billable']."',
									updated_at='".time()."'

						  WHERE article_id='".$in['article_id']."' ");

        


		$this -> db -> query("UPDATE pim_articles_lang 	SET

		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."'
		                                                WHERE item_id= '" . $in['article_id'] . "' AND lang_id='" . $in['lang_id'] . "'
		                                                   ");
		//array_push($commands,"UPDATE pim_articles SET custom_threshold_value    = '".$custom_threshold_value."',article_threshold_value= '".$in['article_threshold_value']."'   WHERE article_id='".$in['article_id']."'");



		
		//update base price
		$this->db->query("SELECT * FROM pim_article_prices WHERE base_price=1   AND  article_id='".$in['article_id']."'");
		if($this->db->f('article_id')) {
		$this->db2->query("UPDATE pim_article_prices SET

						price				= '".return_value($in['price'])."',
						total_price			= '".return_value($in['total_price'])."',
						purchase_price 		= '".return_value($in['purchase_price'])."'
						WHERE
						base_price=1   AND  article_id='".$in['article_id']."'
				");
	 }else{
	 	$this->db2->query("INSERT INTO pim_article_prices SET

						price				= '".return_value($in['price'])."',
						total_price			= '".return_value($in['total_price'])."',
						purchase_price 		= '".return_value($in['purchase_price'])."',
						base_price=1,
						article_id='".$in['article_id']."'

				");
	 }



        if($in['price_type']==1){

		$prices = $this->db->query("SELECT * FROM pim_article_price_category");
		while ($prices->next()){
               if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }

            $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
            	              WHERE article_id='".$in['article_id']."' AND category_id='".$prices->f('category_id')."'");
            if($this->db2->move_next()){

            	 if($this->db2->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }

			switch ($this->db2->f('type')) {
				case 1:                  //discount
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) - round(($this->db2->f('price_value') * return_value( $base_price) / 100),2);

				}else{ //fix
					$total_price = return_value($base_price) - $this->db2->f('price_value');
				}

				break;
				case 2:                 //profit margin
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value( $base_price) +  round(($this->db2->f('price_value') * return_value( $base_price) / 100),2);
				}else{ //fix
					$total_price = return_value( $base_price) + $this->db2->f('price_value');
				}

				break;
					}



              }else{

                      switch ($prices->f('type')) {
				case 1:                  //discount
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value( $base_price) - round(($prices->f('price_value') * return_value( $base_price) / 100),2);

				}else{ //fix
					$total_price = return_value( $base_price) - $prices->f('price_value');
				}

				break;
				case 2:                 //profit margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) +  round(($prices->f('price_value') * return_value($base_price) / 100),2);
				}else{ //fix
					$total_price = return_value($base_price) + $prices->f('price_value');
				}

				break;
              }
          }

            $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$in['article_id']."',
						price_category_id	= '".$prices->f('category_id')."',
						price				= '".$total_price."',
						total_price			= '".$total_with_vat."'
				");



		}
	}else{  //qunatity order price

	      $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
          $this->db->query("SELECT * FROM  pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price=0");
        while ($this->db->move_next()) {
        	   $total_price = $this->db->f('price');
        	   $total_with_vat = $total_price+($total_price*$vat_value/100);
        	   $this->db2->query("UPDATE pim_article_prices SET

											price           = '".$total_price."',
											total_price     = '".$total_with_vat."'
						  WHERE article_price_id='".$this->db->f('article_price_id')."'

							");
        }

        update_q_price($in['article_id'],$in['price'],$in['vat_id']);
        
        if($old_price_type==1){
             $prices = $this->db->query("SELECT * FROM article_price_volume_discount");
				while ($prices->next()){
                     //var price=((percent*base_price)/100)+base_price;
                     $total_price    = (($prices->f('percent')*return_value($in['price']))/100)+return_value($in['price']);

		             $vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		             $total_with_vat = $total_price+($total_price*$vat_value/100);

                      $this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."',
							from_q              = '".$prices->f('from_q')."',
							to_q                = '".$prices->f('to_q')."',
							percent             = '".$prices->f('percent')."'
						");


				}



        }

	}
				
        msg::success(gm("Service succefully updated."),'success');

        insert_message_log($this->pag,'{l}Service updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['article_id'],false,$_SESSION['u_id']);

		update_articles($in['article_id'],DATABASE_NAME);

		return true;
	}


	



	/**
	 * Update validate
	 * @param  array $in
	 * @return boolean
	 */
	function update_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');

		return $this -> add_validate($in);

	}

	

	





	/**
	 * Duplicate articles
	 * @param  array $in
	 * @return boolean
	 */
	function duplicate(&$in)
	{

		if(!$this->add_validate($in)){

			return false;
		}
		$query_sync=array();
		$article_vat = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['duplicate_article_id']."'");





		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET			item_code  							= '".$in['item_code']."',
																ean_code  							= '".$in['ean_code']."',
																hide_stock  							= '".$in['hide_stock']."',
																	use_percent_default  				= '".$in['use_percent_default']."',
																supplier_reference      = '".$in['supplier_reference']."',
																vat_id									= '".$in['vat_id']."',
																article_brand_id			= '".$in['article_brand_id']."',
																article_category_id			= '".$in['article_category_id']."',
																origin_number   			= '".$in['origin_number']."',
																stock        	    		= '".$in['stock']."',
																sale_unit        			= '".$in['sale_unit']."',
																internal_name       		= '".$in['internal_name']."',
																packing          			= '".return_value2($in['packing'])."',
																price_type          		= '".$in['price_type']."',
																article_threshold_value    	= '".$in['article_threshold_value']."',
																weight    					= '".$in['weight']."',
																show_img_q								= '".$in['show_img_q']."',
																supplier_id								= '".$in['supplier_id']."',
																supplier_name							= '".$in['supplier_name']."',
																custom_threshold_value    	= '".$custom_threshold_value."'


																");
	
       


			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '" . $in['article_id'] . "',
		                                                                         lang_id                = '" . $in['lang_id'] . "',
		                                                                         name                   = '" . $in['name'] . "',
		                                                                         name2                  = '" . $in['name2'] . "',
		                                                                         description			= '" . $in['description']."' ");

		    //duplicate translation
		    $pim_articles_lang = $this -> db -> query("SELECT * FROM pim_articles_lang where item_id='".$in['duplicate_article_id']."' AND lang_id!='".$in['lang_id']."'");
		   while ($pim_articles_lang -> next()) {
			$in['article_lang_id'] = $this -> db -> insert("INSERT INTO pim_articles_lang SET
		                                                                         item_id                = '".$in['article_id']."',
		                                                                         lang_id                = '".$pim_articles_lang-> f('lang_id')."',
		                                                                         name                   = '".addcslashes($pim_articles_lang-> f('name'),"'\\")."',
		                                                                         name2                  = '".addcslashes($pim_articles_lang-> f('name2'),"'\\")."',
		                                                                         description			= '".addcslashes($pim_articles_lang-> f('description'),"'")."'
		                                                                         	");
		}



		

$this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0',
										purchase_price 		= '".return_value($in['purchase_price'])."'
						");


    if($in['price_type']==1){
$prices = $this->db->query("SELECT * FROM pim_article_price_category");
				while ($prices->next()){
				//insert the default price
                 if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }
				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) - ($prices->f('price_value') * return_value($base_price)/ 100);
							}else{ //fix
								$total_price = return_value($base_price) - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) + ($prices->f('price_value') * return_value($base_price) / 100);
							}else{ //fix
								$total_price = return_value($base_price) + $prices->f('price_value');
							}

					break;
				}

					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");



			}


		//duplicate_data('pim_article_prices','article_id',$in['duplicate_article_id'],$in['article_id'],'article_price_id', return_value($in['price']), return_value($in['total_price']));
      }else{
        /* $this->db->query("INSERT INTO pim_article_prices SET
										article_id			= '".$in['article_id']."',
										price_category_id	= '0',
										price			    = '".return_value($in['price'])."',
										total_price		    = '".return_value($in['total_price'])."',
										base_price	        = '1',
										default_price		= '0'
						");*/
      }

      //	duplicate_data('pim_product_attribute_value','product_id',$in['duplicate_product_id'],$in['product_id']);

		foreach ($in['tax_id'] as $tax_id=>$is_tax){
			$this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$tax_id."'

				");
		}


		
		unset($in['duplicate_article_id']);
		update_articles($in['article_id'],DATABASE_NAME);
		msg::$success = "Services succefully duplicate.";
		return true;
	}

	/**
	 * Add price for articles to quantities
	 * @param  array $in
	 * @return boolean
	 */
	function quantity_price(&$in)
	{

		if(!$this->quantity_price_validate($in)){

			return false;
		}
		

		$total_price    = return_value($in['price_q']);

		$vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		$total_with_vat = $total_price+($total_price*$vat_value/100);

	    $this->db->query("INSERT INTO pim_article_prices SET
											article_id		= '".$in['article_id']."',
											from_q			= '".return_value($in['from_q'])."',
											to_q			= '".return_value($in['to_q'])."',
											price           = '".$total_price."',
											total_price     = '".$total_with_vat."',
											percent   = '".return_value($in['percent'])."'

							");

		
		msg::success(gm("Price succefully updated"),'success');
        update_articles($in['article_id'],DATABASE_NAME);
		
		return true;
	}



    /**
     * Update price for articles to quantities
     * @param  array $in
     * @return boolean
     */
	function quantity_price_update(&$in)
	{
		if(!$this->quantity_price_update_validate($in)){

			return false;
		}
		

		$total_price    = return_value($in['price_q']);

		$vat_value   	= $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
		$total_with_vat = $total_price+($total_price*$vat_value/100);

	    $this->db->query("UPDATE pim_article_prices SET
											from_q			= '".return_value($in['from_q'])."',
											to_q			= '".return_value($in['to_q'])."',
											price           = '".$total_price."',
											total_price     = '".$total_with_vat."',
											percent         = '".return_value($in['percent'])."'
						  WHERE article_price_id='".$in['article_price_id']."'

							");

	
	
		msg::success(gm("Price succefully updated."),'success');
		update_articles($in['article_id'],DATABASE_NAME);
		return true;
	}



	/**
	 * Delete article price to quantities
	 * @param  array $in
	 * @return boolean
	 */
	function delete_quantity_price(&$in)
	{
		if(!$this->delete_quantity_price_validate($in)){
			return false;
		}
		

		$this->db->query("DELETE FROM pim_article_prices WHERE article_price_id='".$in['article_price_id']."'");

		msg::success("Price deleted.",'success');
	
		return true;
	}


	/**
	 * Return article price based for quantities
	 * @param  array $in
	 * @return float
	 */
	function get_article_quantity_price(&$in)
	{

		$price_type=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
        $is_block_discount=$this->db->query("SELECT pim_product_article.product_id,pim_product_article.article_id,pim_product_article.block_discount
                               FROM pim_product_article
                               WHERE pim_product_article.article_id='".$in['article_id']."' AND pim_product_article.block_discount='1'");

        if($is_block_discount->move_next()){
	    		$price=$in['price'];
		}else{
			if($price_type==1){
		   		$price=$in['price'];
	    	}else{
	    		$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices
	    		                     WHERE pim_article_prices.from_q<='".$in['quantity']."'
	    		                     AND  pim_article_prices.article_id='".$in['article_id']."'
	                                 ORDER BY pim_article_prices.from_q DESC
	                                 LIMIT 1
	    		                     ");

		        if(!$price){
		        	$price=$in['price'];
		        }
	    	}
		}
	    $in['new_price']=$price;
	    $in['debug']=true;
		return true;
	}

	/**
	 * Validate date for add qunatity price
	 * @param  array $in
	 * @return boolean
	 */
	function quantity_price_validate(&$in)
	{

		$v = new validation($in);

		// biggest to quantity
		$biggest_to_quantity=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT pim_article_prices.from_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   //$in['error']=gm('To Qty value should be bigger than From Qty');
		   msg::error(gm('To Qty value should be bigger than From Qty'),'error');
		   return false;
		}
        if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['from_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1");
         if($this->db->move_next()){
         	 //$in['error']=gm('Invalid Interval');
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['to_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1");
         if($this->db->move_next()){
         	 //$in['error']=gm('Invalid Interval');
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE  pim_article_prices.from_q < '".return_value($in['to_q'])."'  AND    pim_article_prices.to_q < '".return_value($in['to_q'])."'
         	               AND pim_article_prices.to_q > '".return_value($in['from_q'])."'
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1");
         if($this->db->move_next()){
         	 //$in['error']=gm('Invalid Interval');
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.from_q < '".return_value($in['to_q'])."' AND base_price!=1  AND  pim_article_prices.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	// $in['error']=gm('Invalid Interval');
            	  msg::error(gm('Invalid Interval'),'error');
		         return false;
            }



        }else{
           //return_value($in['from_q']) >

            $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND  pim_article_prices.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	 //$in['error']=gm('Invalid Interval');
            	  msg::error(gm('Invalid Interval'),'error');
		         return false;
            }

        }
        if(return_value($in['price_q']) <= 0 && !empty($in['price_q'])){
        	// $in['price_error']=gm('This field should be bigger than 0');
        	$v -> f('price_q', 'Price', 'empty_condition','This field should be bigger than 0');
        }




	

		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]:numeric');
		$v -> f('to_q', 'Max Qty', 'numeric:greater_than[1]');
		$v -> f('price_q', 'Price', 'required:numeric');

		//$v -> f('to_q', 'Max Qty', 'required:greater_than[1]');

        return $v -> run();
    }

   /**
    * Validate update quantity price
    * @param  array $in
    * @return boolena
    */
	function quantity_price_update_validate(&$in)
	{
		$v = new validation($in);
		$biggest_to_quantity=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND article_price_id!='".$in['article_price_id']."' ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT pim_article_prices.from_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."'  AND article_price_id!='".$in['article_price_id']."' ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		    msg::error(gm('To Qty value should be bigger than From Qty'),'error');
		   return false;
		}
        if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['from_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND article_price_id!='".$in['article_price_id']."'");
         if($this->db->move_next()){
         	  msg::error(gm('Invalid Interval'),'error');

		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE '".return_value($in['to_q'])."' BETWEEN pim_article_prices.from_q AND pim_article_prices.to_q
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND article_price_id!='".$in['article_price_id']."'");
         if($this->db->move_next()){
         	   msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

        	//check if we allready have an interval for these value
         $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices
         	               WHERE  pim_article_prices.from_q < '".return_value($in['to_q'])."'  AND    pim_article_prices.to_q < '".return_value($in['to_q'])."'
         	               AND pim_article_prices.to_q > '".return_value($in['from_q'])."'
         	               AND pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND article_price_id!='".$in['article_price_id']."'");
         if($this->db->move_next()){
         	  msg::error(gm('Invalid Interval'),'error');
		     return false;
          }

          $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.from_q < '".return_value($in['to_q'])."' AND base_price!=1  AND  pim_article_prices.to_q=0  AND article_price_id!='".$in['article_price_id']."' LIMIT 1");
            if($this->db->move_next()){
            	  msg::error(gm('Invalid Interval'),'error');
		         return false;
            }



        }else{
           //return_value($in['from_q']) >

            $this->db->query("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND base_price!=1  AND  pim_article_prices.to_q=0  AND article_price_id!='".$in['article_price_id']."' LIMIT 1");
            if($this->db->move_next()){
            	   msg::error(gm('Invalid Interval'),'error');
		         return false;
            }

        }

		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]');

        return $v -> run();
    }



   	/**
   	 * Delete quantity price for article
   	 * @param  array $in
   	 * @return boolean
   	 */
	function delete_quantity_price_validate(&$in)
	{
		$is_ok = true;
		if(!$in['article_price_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		if(!$in['article_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		else{
			$this->db->query("SELECT article_id FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND article_price_id='".$in['article_price_id']."'");
			if(!$this->db->move_next()){
				$in['error'] = "Invalid ID.";
				return false;
			}
		}

		return $is_ok;
	}

    /**
     * Update price list for article
     * @param  array $in
     * @return boolean
     */
	function update_price_list(&$in)
	{
		if(!$this->update_price_list_validate($in)){
           return false;
		}

		

	    $in['total_price']=return_value($in['price'])+(return_value($in['price'])* get_article_vat($in['article_id'])/100);
        $in['price_type']=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
		$in['vat_id']=$this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
        $in['purchase_price'] =$this->db->field("SELECT pim_article_prices.purchase_price FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price='1'");
		if($in['price_type']==1){
           $this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price!='1'");
        }

		//update base price
		$this->db->query("UPDATE pim_article_prices SET
						price				= '".return_value($in['price'])."',
						total_price			= '".$in['total_price']."'
						WHERE
						base_price=1   AND  article_id='".$in['article_id']."'
				");

        if($in['price_type']==1){

		$prices = $this->db->query("SELECT * FROM pim_article_price_category");
		while ($prices->next()){
               if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }


            $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom
            	              WHERE article_id='".$in['article_id']."' AND category_id='".$prices->f('category_id')."'");
            if($this->db2->move_next()){
                    if($this->db2->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }


			switch ($this->db2->f('type')) {
				case 1:                  //discount
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) - round(($this->db2->f('price_value') * return_value($base_price) / 100),2);

				}else{ //fix
					$total_price = return_value($base_price) - $this->db2->f('price_value');
				}

				break;
				case 2:                 //markup margin
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) +  round(($this->db2->f('price_value') * return_value($base_price) / 100),2);
				}else{ //fix
					$total_price = return_value($base_price) + $this->db2->f('price_value');
				}

				break;

				case 3:                 //profit margin
				if($this->db2->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) /  round(1-($this->db2->f('price_value') / 100),2);
				}else{ //fix
					//$total_price = return_value($base_price) + $this->db2->f('price_value');
				}

				break;
					}



              }else{

                      switch ($prices->f('type')) {
				case 1:                  //discount
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) - round(($prices->f('price_value') * return_value($base_price) / 100),2);

				}else{ //fix
					$total_price = return_value($base_price) - $prices->f('price_value');
				}

				break;
				case 2:                 //markup margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) +  round(($prices->f('price_value') * return_value($base_price) / 100),2);
				}else{ //fix
					$total_price = return_value($base_price) + $prices->f('price_value');
				}

				break;
				case 3:                 //profit margin
				if($prices->f('price_value_type')==1){  // %
					$total_price = return_value($base_price) /  round(1-($prices->f('price_value') / 100),2);
				}else{ //fix
					//$total_price = return_value($base_price) + $prices->f('price_value');
				}

				break;
              }
          }

            $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$in['article_id']."',
						price_category_id	= '".$prices->f('category_id')."',
						price				= '".$total_price."',
						total_price			= '".$total_with_vat."'
				");



		}
	}else{
		//qunatity order price
	    $vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$in['vat_id']."'");
        $this->db->query("SELECT * FROM  pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price=0");
        while ($this->db->move_next()) {
        	   $total_price = $this->db->f('price');
        	   $total_with_vat = $total_price+($total_price*$vat_value/100);
        	   $this->db2->query("UPDATE pim_article_prices SET
											price           = '".$total_price."',
											total_price     = '".$total_with_vat."'
						  WHERE article_price_id='".$this->db->f('article_price_id')."'

							");
        }
          update_q_price($in['article_id'],$in['price'],$in['vat_id']);

	}


      
		update_articles($in['article_id'],DATABASE_NAME);
		msg::success (gm("Price succefully updated."),'success');
		return true;
	}

	/**
	 * validate update price list data
	 * @param  array $in
	 * @return boolean
	 */
	function update_price_list_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');
		$v->field('price','Price','required:numeric');
		return $v -> run();

	}

	/**
	 * Update price for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_price(&$in)
	{
		if(!$this->update_price_validate($in)){
           return false;
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' and base_price!=1");
		$prices = $this->db->query("SELECT * FROM pim_article_price_category ");

		while ($prices->next()){

			$article_price = $in['si_price'][$prices->f('category_id')];
			$vat = $in['si_vat_value'] * $article_price/100;

			$this->db->query("INSERT INTO pim_article_prices SET
											article_id			= '".$in['article_id']."',
											price_category_id	= '".$prices->f('category_id')."',
											price			    = '".$in['si_price'][$prices->f('category_id')]."',
											total_price			= '".return_value($article_price+$vat)."'
							");
        }

        Sync::end($in['article_id']);
		update_articles($in['article_id'],DATABASE_NAME);
		msg::$success = "Price succefully updated.";
		return true;
	}

	/**
	 * Validate update price data
	 * @param  array $in
	 * @return boolean
	 */
	function update_price_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');

		foreach ($in['si_price'] AS $price_category_id => $price){
			$v->field('si_price','Price','required:numeric');

		}

		return $v -> run();

	}

	/**
	 * Update default price for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_default_prices(&$in)
	{
		$in['failure'] = false;
		if(!$this->update_default_prices_validate($in)){
			$in['failure'] = true;
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);

		foreach($in['base_price'] as $article_id => $new_price){

			$article_price  = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article_id."' AND base_price=1 ");
			$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_id."'");
			$vat_value      = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
			$new_price_with_vat = return_value($new_price)+(return_value($new_price)*$vat_value/100);

			// updating buy prices only if default price or vat percent has changed



			$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$article_id."' AND base_price!=1");


			$this->db->query("UPDATE pim_article_prices SET

						price				= '".return_value($new_price)."',
						total_price			= '".$new_price_with_vat."'
						WHERE
						base_price=1   AND  article_id='".$article_id."'
				");


			$prices = $this->db->query("SELECT * FROM pim_article_price_category");
			while ($prices->next()){
				//insert the default price

				switch ($prices->f('type')) {
					case 1:                  //discount
					if($prices->f('price_value_type')==1){  // %
						$total_price = return_value($new_price) - ($prices->f('price_value') * return_value($new_price) / 100);
					}else{ //fix
						$total_price = return_value($new_price) - $prices->f('price_value');
					}

					break;
					case 2:                 //profit margin
					if($prices->f('price_value_type')==1){  // %
						$total_price = return_value($new_price) + ($prices->f('price_value') * return_value($new_price) / 100);
					}else{ //fix
						$total_price = return_value($new_price) + $prices->f('price_value');
					}

					break;

				}

				$total_with_vat = $total_price+($total_price*$vat_value/100);

				$this->db->query("INSERT INTO pim_article_prices SET
						article_id			= '".$article_id."',
						price_category_id	= '".$prices->f('category_id')."',
						price				= '".$total_price."',
						total_price			= '".$total_with_vat."'
				");

			}
			update_articles($article_id,DATABASE_NAME);
		}
		Sync::end($in['article_id']);
		msg::$success = "Price succesfully updated.";
		return true;
	}

	/**
	 * Validate update default price data
	 * @param  array $in
	 * @return boolean
	 */
	function update_default_prices_validate(&$in)
	{
		$v = new validation($in);

		foreach ($in['base_price'] AS $price_category_id => $price){
			$v->field('base_price','Price','required:numeric');

		}

		return $v -> run();

	}

	/**
	 * Delete articles
	 * @param  array $in
	 * @return boolean
	 */
	function delete(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
//                global $cfg;

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$this->db->query("DELETE FROM pim_article_prices WHERE article_id='".$in['article_id']."' ");
		$this->db->query("DELETE FROM pim_product_attribute_value WHERE article_id='".$in['article_id']."'");
		$this->db->query("DELETE FROM pim_product_article WHERE  article_id='" . $in['article_id'] . "'");
		$this->db->query("DELETE FROM pim_articles_lang WHERE  item_id='" . $in['article_id'] . "'");
		$this->db->query("DELETE FROM pim_articles_taxes where article_id='".$in['article_id']."'");
		$this->db->query("DELETE FROM article_threshold_dispatch where article_id='".$in['article_id']."'");
		$this->db->query("DELETE FROM url_mask where item_id='".$in['article_id']."' and controller='article'");
		$c = $this->db->query("SELECT file_name,upload_amazon FROM pim_article_photo where parent_id='".$in['article_id']."'");
		ark::loadLibraries(array('aws'));
    $a = new awsWrap(DATABASE_NAME);
		while ($c->next()) {
			if($c->f('upload_amazon')==1){
				$item = $c->f('file_name');
  			$link = $this->db->field("SELECT item FROM s3_links WHERE link='".$item."' ");
				$a->deleteItem($link);
			}else{
				@unlink( INSTALLPATH.'upload/'.DATABASE_NAME.'/pim_article_photo/'.$c->f("file_name"));
			}
		}
		$this->db->query("DELETE FROM pim_article_photo where parent_id='".$in['article_id']."'");

		msg::$success = "Article deleted.";
		Sync::end($in['article_id']);
		return true;
	}

	/**
	 * archive article function
	 *
	 * @param  array $in
	 * @return boolean
	 */
	function archive(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE pim_articles SET active='0' WHERE article_id='".$in['article_id']."' ");
		msg::$success = "Article archived.";
		Sync::end($in['article_id']);
		return true;
	}

	/**
	 * activate article function
	 *
	 * @param  array $in
	 * @return boolean
	 */
	function activate(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE pim_articles SET active='1' WHERE article_id='".$in['article_id']."' ");
		msg::$success = "Article activated.";
		Sync::end($in['article_id']);
		return true;
	}

	/**
	 * Validate delete data
	 * @param  array $in
	 * @return boolean
	 */
	function delete_validate(&$in)
	{
		$is_ok = true;
		if(!$in['article_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		else{
			$this->db->query("SELECT article_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
			if(!$this->db->move_next()){
				$in['error'] = "Invalid ID.";
				return false;
			}
		}

		return $is_ok;
	}

	/**
	 * Delete import log
	 * @param  array $in
	 * @return boolean
	 */
	function delete_import(&$in)
	{
		if(!$this->delete_import_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM pim_article_import_log WHERE article_import_log_id='".$in['article_import_log_id']."' ");

		msg::$success = "Import Log deleted.";

		return true;
	}

	

	



	/**
	 * Update lang filed for articles
	 * @param  array $in
	 * @return boolean
	 */
	function update_lang_fields(&$in)
	{

		if(!$this->update_lang_fields_validate($in)){
			return false;
		}

			foreach($in['translation'] as $item=>$data){
			   $this->db-> query("SELECT   item_id FROM  pim_articles_lang	WHERE item_id= '" . $in['article_id'] . "' AND lang_id='".$data['lang_id']."'");
			   if( $this->db->next()){
			   	    $this->db2-> query("UPDATE  pim_articles_lang 	SET  ".$in['txt']."= '".$data[$in['txt']]."' WHERE item_id= '" . $in['article_id'] . "' AND lang_id='".$data['lang_id']."'");
			   }else{
			   	  $this->db2-> query("INSERT INTO   pim_articles_lang 	SET  ".$in['txt']."= '".$data[$in['txt']]."' , item_id= '" . $in['article_id'] . "' , lang_id='".$data['lang_id']."'");
			   }

                
		}



		
		msg::success(gm("Services language fields successfully updated."),'success');
		json_out($in);
		return true;
	}

	/**
	 * Validate update lang field data
	 * @param  array $in
	 * @return boolean
	 */
	function update_lang_fields_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');

		return $v -> run();

	}

	/**
	 * Edit select fields
	 * @param  array $in
	 * @return boolean
	 */
	function select_edit(&$in){

		$query_sync=array();
		$i=0;
		foreach ($in['name_select'] as $key => $value){
			if(!empty($value)){
			$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$in['id_select'][$key]."' ");
				if(!$id){
					$inserted_id=$this->db->insert("INSERT INTO ".$in['table']." SET name='".addslashes($value)."', sort_order='".$key."' ");
					$query_sync[$i]="INSERT INTO ".$in['table']." SET id='".$inserted_id."',name='".addslashes($value)."', sort_order='".$key."'";
					$i++;
				}
			}
		}

		

        foreach ($in['name_select'] as $key=>$value){
			if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$in['id_select'][$key]."' ");
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value)."', sort_order='".$key."' WHERE id='".$id."' ");

				}
			}
		}

		
		return true;
	}

	/**
	 * Delete data from select dropdowns
	 * @param  array $in
	 * @return boolean
	 */
	function delete_select(&$in){
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM ".$in['table']." WHERE id='".$in['id']."' ");
		Sync::end();
		return true;
	}



	/**
	 * Get language value
	 * @param  array $in
	 * @return boolean
	 */
	function get_lang_value(&$in) {
		$in['code']=get_language_code($in['lang_id']);

		if(!$in['lang_id'] || !$in['item_id']){
			return true;
		}

		$in['value']=$this->db->field("SELECT ".$in['field']."
		                           FROM ".$in['tbl']."
		                           WHERE lang_id=".$in['lang_id']." and item_id=".$in['item_id']."
		                  ");

		return true;

	}


	function upload_photo($in){
    $response=array();
    if (!empty($_FILES)) {
       $this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);
      $in['name'] = 'tmp_file_'.time().'.'.$ext;
      
      $targetPath = INSTALLPATH.'upload';

      
      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
      // Validate the file type
      $fileTypes = array('jpg','jpeg','gif'); // File extensions
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      mkdir(str_replace('//','/',$targetPath), 0775, true);
      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
        move_uploaded_file($tempFile,$targetFile);
        $in['tmp_file_name'] = $in['name'];
        $response['name'] =  $this->saveTmpFileToAmazon($in);
        $response['success'] = 'success';
       echo json_encode($response);
        // ob_clean();
      } else {
        $response['error'] = gm('Invalid file type.');
       echo json_encode($response);
      }
      exit();
    }
  }

    /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function saveTmpFileToAmazon(&$in)
  {
    
    $in['up_folder']='../upload/'.DATABASE_NAME.'/pim_article_photo';
    $parentId=$in['article_id'];
    if($in['tmp_file_name']){
      global $config;
      ark::loadLibraries(array('aws'));
      $a = new awsWrap(DATABASE_NAME);
      $img_id = $this->db->insert("INSERT INTO  pim_article_photo SET parent_id='".$in['article_id']."',folder='".$in['up_folder']."', upload_amazon=1");
     
      $artPath = INSTALLPATH.'upload/'.$in['tmp_file_name'];
     

    
      $ext = pathinfo($artPath,PATHINFO_EXTENSION);
      
       $artFile = 'article/article_'.$parentId."_".$img_id.".".$ext;
    
      $a->uploadFile($artPath,$artFile);
      unlink($artPath);
     
     
      
      
       $link =  $a->getLink($config['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext);
       $this->db->query("UPDATE pim_article_photo SET file_name='".$link."',amazon_path='".$config['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext."' WHERE photo_id='".$img_id."' AND parent_id ='".$parentId."' ");
				
       $in['artPhoto']=$link;
   
      return $link;
   }
 }
   function remove_photo(&$in)
  {
    global $config;
    $info = $this->db->query("SELECT * FROM pim_article_photo WHERE parent_id='".$in['article_id']."' ");

      ark::loadLibraries(array('aws'));
      $aws = new awsWrap(DATABASE_NAME);
      $link =  $aws->doesObjectExist($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
      if($link){
        $aws->deleteItem($config['awsBucket'].DATABASE_NAME.'/'.$info->f('file_name'));
      }
      $this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");
     msg::success(('File deleted'),'success');
     
    
    return true;
  }








	/****************************************************************
	* function activate_checkbox(&$in)                              *
	****************************************************************/
	function block_discount(&$in)
	{

		if(!$in['article_id']){
			
			msg::success(gm("Invalid ID"),'error');
			return false;
		}

		
		$table = $in['table'];
		$field_name = $in['name'];
		$id_name = $in['id_name'];
		$id_val = $in['article_id'];
		$activate_checkbox = $this->db->query("UPDATE $table SET $field_name = '1' WHERE $id_name = $id_val ");

		
		msg::success(gm("Service successfully updated."),'success');
		return true;
	}

	function unblock_discount(&$in)
	{

		if(!$in['article_id']){
			msg::success(gm("Invalid ID"),'error');
			return false;
		}

		
		$table = $in['table'];
		$field_name = $in['name'];
		$id_name = $in['id_name'];
		$id_val = $in['article_id'];
		$activate_checkbox = $this->db->query("UPDATE $table SET $field_name = '0' WHERE $id_name = $id_val ");

		
		msg::success(gm("Service successfully updated."),'success');
		return true;
	}

	

	function save_article_tax(&$in)
	{
		if(!$in['article_id']){
			msg::$error = gm("Invalid ID");
			return false;
		}
	
		$this->db->query("DELETE FROM pim_articles_taxes WHERE article_id='".$in['article_id']."'");
		
		foreach($in['query'] as $item=>$data){
			
          if($data['tax']){
			  $this->db->query("INSERT INTO pim_articles_taxes SET
								  article_id	= '".$in['article_id']."',
								  tax_id	= '".$data['tax_id']."'

				");
		     }
		}
		
		 msg::success(gm("Changes saved."),'success');
		 json_out($in);
		return true;
	}

	
	








  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function singleImgUpload(&$in)
  {
  	// console::log($in);
  	global $cfg;
  	$response = array();
  	if (!empty($_FILES)) {
  		$tempFile = $_FILES['Filedata']['tmp_name'];
  		$fName = $_FILES['Filedata']['name'];
  		$ext = pathinfo($fName, PATHINFO_EXTENSION);
  		$item_code = substr($fName,0,strrpos($fName, "."));
  		$parentId = $this->db->field("SELECT article_id FROM pim_articles WHERE item_code='".$item_code."' ");
  		if(!$parentId){
  			$response['error'] = "Article not found - ".$item_code;
				echo json_encode($response);
				return;
  		}else{
  			$param = array('parent_id'=>$parentId);
  			$this->delete_art_photos($param);
  		}
  		$targetPath = INSTALLPATH.UPLOAD_PATH . '/'.DATABASE_NAME.'/pim_article_photo/';
  		$img_id = $this->db->insert("INSERT INTO pim_article_photo SET parent_id='".$parentId."', upload_amazon=1");

  		$imgName = "img_".$parentId."_".$img_id.'.'.$ext;
			$targetFile =  str_replace('//','/',$targetPath) . $imgName;
			$this->db->query("UPDATE pim_article_photo SET file_name='".$imgName."' WHERE photo_id='".$img_id."' AND parent_id='".$parentId."' ");

			if(move_uploaded_file($tempFile,$targetFile) == FALSE)
			{
				$response['error'] = "File canot be moved - ".$targetPath;
				echo json_encode($response);
			}else {
				$size = getimagesize($targetFile);
				$width = $size[0];
				$height = $size[1];

        ark::loadLibraries(array('aws'));
        $a = new awsWrap(DATABASE_NAME);
        $pdfPath = $targetFile;//upload server

        $pdfFile = 'article/article_'.$parentId."_".$img_id.".".$ext;

        $a->uploadFile($pdfPath,$pdfFile);//upload amazon
        unlink($targetFile);//delete server
        $link =  $a->getLink($cfg['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext);
        $response['success'] = 'Uploaded - '.$item_code;

        $this->db->query("UPDATE pim_article_photo SET file_name='".$link."',amazon_path='".$cfg['awsBucket'].DATABASE_NAME.'/article/article_'.$parentId."_".$img_id.".".$ext."' WHERE photo_id='".$img_id."' AND parent_id='".$parentId."' ");
				echo json_encode($response);
			}
  	}

		return;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function delete_art_photos($in)
  {
  	$this->db->query("SELECT * FROM pim_article_photo WHERE  parent_id='".$in['parent_id']."'");
  	while ($this->db->next()) {
  		if($this->db->f('upload_amazon')==1){
    		$item = $this->db->f('file_name');
    		$link = $this->db->field("SELECT item FROM s3_links WHERE link='".$item."' ");
    		ark::loadLibraries(array('aws'));
        $a = new awsWrap(DATABASE_NAME);
        $a->deleteItem($link);
    	}else{
    		unlink($_SERVER['DOCUMENT_ROOT'].$this->db->f('folder')."/".$this->db->f('file_name'));
    	}
    	$this->db->query("DELETE FROM pim_article_photo WHERE parent_id='".$in['parent_id']."' AND photo_id='".$this->db->f('photo_id')."' ");
    	$commands = array();
    	array_push($commands, "DELETE FROM pim_article_photo WHERE parent_id='".$in['parent_id']."' AND photo_id='".$this->db->f('photo_id')."' ");
        // Sync::end();
        $packet = array(
					'command' => base64_encode(serialize($commands)),
					'command_id' => 1
				);
				Sync::send(WEB_URL, $packet);
  	}
  	return true;
  }


  	
	function article_settings(&$in){


		if(!$in['ALLOW_ARTICLE_PACKING']){
		   $in['ALLOW_ARTICLE_PACKING']=0;
		   $in['ALLOW_QUOTE_PACKING']=0;
		   $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}
		if(!$in['ALLOW_ARTICLE_SALE_UNIT'] || $in['ALLOW_ARTICLE_PACKING'] == 0 ){
		    $in['ALLOW_ARTICLE_SALE_UNIT']=0;
		}
		if(!$in['ALLOW_ARTICLE_SALE_UNIT']){
		    $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}
		if(!$in['ALLOW_QUOTE_PACKING']){
		   $in['ALLOW_QUOTE_PACKING']=0;
		   $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}

		if(!$in['ALLOW_QUOTE_SALE_UNIT'] || $in['ALLOW_QUOTE_PACKING'] == 0 ){
		    $in['ALLOW_QUOTE_SALE_UNIT']=0;
		}
		$this->db->query("UPDATE settings SET value='".$in['SHOW_TOTAL_MARGIN_QUOTE']."' WHERE constant_name='SHOW_TOTAL_MARGIN_QUOTE' ");
		$this->db->query("UPDATE settings SET value='".$in['ALLOW_ARTICLE_PACKING']."' WHERE constant_name='ALLOW_ARTICLE_PACKING'");
		$this->db->query("UPDATE settings SET value='".$in['ALLOW_ARTICLE_SALE_UNIT']."' WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT'");


	  $this->db->query("UPDATE settings SET value='".$in['ALLOW_QUOTE_PACKING']."' WHERE constant_name='ALLOW_QUOTE_PACKING'");
		$this->db->query("UPDATE settings SET value='".$in['ALLOW_QUOTE_SALE_UNIT']."' WHERE constant_name='ALLOW_QUOTE_SALE_UNIT'");

/*		if($in['ARTICLE_PRICE_COMMA_DIGITS']==2 || $in['ARTICLE_PRICE_COMMA_DIGITS']==3 || $in['ARTICLE_PRICE_COMMA_DIGITS']==4){
			$this->db->query("UPDATE settings SET value='".$in['ARTICLE_PRICE_COMMA_DIGITS']."' WHERE constant_name='ARTICLE_PRICE_COMMA_DIGITS'");
		}
		$in['ARTICLE_PRICE_COMMA_DIGITS'] = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ARTICLE_PRICE_COMMA_DIGITS' ");*/

   if($in['WAC']==1 && !WAC){
      $this->db->query("UPDATE settings SET value='".strtotime('now')."' WHERE constant_name='WAC_ACT_DATE'");
       $this->db->query("SELECT  pim_articles.article_id FROM pim_articles");
       while ($this->db->move_next()) {
            generate_purchase_price($this->db->f('article_id'),1);
       }

    }
    $this->db->query("UPDATE settings SET value='".$in['WAC']."' WHERE constant_name='WAC'");
    $this->db->query("UPDATE settings SET value='".$in['AAC']."' WHERE constant_name='AAC'");
    $this->db->query("UPDATE settings SET value='".$in['AUTO_AAC']."' WHERE constant_name='AUTO_AAC'");
    if(!$in['AAC']){
       $this->db->query("UPDATE settings SET value='0' WHERE constant_name='AUTO_AAC'");
    }
    if($in['AUTO_AAC']==1 && !AUTO_AAC){
         $this->db->query("SELECT  pim_articles.article_id FROM pim_articles");
          while ($this->db->move_next()) {
              generate_aac_price($this->db->f('article_id'),1);
          }
    }



		msg::success ( gm('Article settings has been successfully updated.'),'success');
		return true;

	}
	
	function add_price_category(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_price_category_validate($in)){
			$in['failure'] = true;
			return false;
		}
		
		$query_sync=array();
		
		$in['category_id'] = $this->db->insert("INSERT INTO pim_article_price_category SET name='".$in['name']."',
		                                                                                   type='".$in['category_type_id']."',
		                                                                                   price_type='".$in['price_type_id']."' ,
		                                                                                   price_value='".return_value($in['price_value'])."',
		                                                                                   price_value_type='".$in['price_value_type_id']."' 
		                                       ");
       
		$query_sync[0]="INSERT INTO pim_article_price_category SET                         
		                                                                                   category_id='".$in['category_id']."', 
		                                                                                   name='".$in['name']."',
		                                                                                   type='".$in['category_type_id']."',
		                                                                                   price_type='".$in['price_type_id']."',
		                                                                                   price_value='".return_value($in['price_value'])."',
		                                                                                   price_value_type='".$in['price_value_type_id']."' 
		                                       ";
		

		

		  $article_prices = $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices WHERE base_price=1");
		

		while ($article_prices->next()){
             if($in['price_type_id']==2){
               $article_base_price=$article_prices->f('purchase_price');
             }else{
               $article_base_price=$article_prices->f('price');
             }
			//insert the article prices
			switch ($in['category_type_id']) {
				case 1:                  //discount
				if($in['price_value_type_id']==1){  // %
					$total_price = $article_base_price - (return_value($in['price_value']) * $article_base_price / 100);
				}else{ //fix
					$total_price = $article_base_price - $in['price_value'];
				}

				break;
				case 2:                 //profit margin
				if($in['price_value_type_id']==1){  // %
					$total_price = $article_base_price + (return_value($in['price_value']) * $article_base_price / 100);
				}else{ //fix
					$total_price =$article_base_price + return_value($in['price_value']);
				}

				break;

			}
			$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_prices->f('article_id')."'");
			$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			
		    $this->db->query("INSERT INTO pim_article_prices SET
				article_id			= '".$article_prices->f('article_id')."',
				price_category_id	= '".$in['category_id']."',
				price				= '".$total_price."',
				total_price			= '".$total_with_vat."'
				");

		}
		
 		
		msg::success ( "Article Price Category added.",'success');

		return true;
	}
	function add_price_category_validate(&$in){

		$v = new validation($in);
		if (ark::$method == 'update') {
			$option = ".(category_id != '".$in['category_id']."')";
		} else {
			$option = "";
		}

		$v->field('name','Name','required:unique[pim_article_price_category.name'.$option.']');
		$v->field('price_type_id','Type','required');
		//$v->field('price_value','Price Value','required:numeric');
		$v->field('price_value_type_id','Price Value Type','required');

		return $v->run();

	}
	function delete_price_category(&$in)	{
		
		if(!$this->delete_price_category_validate($in)){
			
			return false;
		}
		$this->db->query("DELETE FROM pim_article_price_category WHERE category_id='".$in['id']."' ");
		$this->db->query("DELETE FROM fam_custom_price WHERE category_id='".$in['id']."' ");
		$this->db->query("DELETE FROM pim_article_prices
					      WHERE price_category_id='".$in['id']."'");
		msg::success ( "Article Price Category deleted.",'success');
		
		return true;
	}
	function delete_price_category_validate(&$in){
		$v=new validation($in);
		$v->f('id','Category Id','required:exist[pim_article_price_category.category_id]');

		return $v->run();

	}
	function update_fam_price_list(&$in)
	{  
		if(!$this->update_fam_price_list_validate($in)){
           return false;
		}
		$this->db->query("DELETE FROM  fam_custom_price WHERE fam_id = '".$in['fam_id']."' AND category_id = '".$in['category_id']."'");
        $this->db->query("INSERT INTO fam_custom_price SET fam_id 		= '".$in['fam_id']."',
									category_id 			= '".$in['category_id']."',
									value 			= '".return_value($in['fam_value'])."'");		
   

		msg::success ( gm('Changes saved'),'success');
		return true;
	}

	/**
	 * validate update price list data
	 * @param  array $in
	 * @return boolean
	 */
	function update_fam_price_list_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('fam_id', 'fam_id', 'required');
	
		return $v -> run();

	}

	function update_category(&$in)
	{   
		$in['failure'] = false;
		if(!$this->update_category_validate($in)){
			$in['failure'] = true;
			return false;
		}

		$this->db->query("SELECT pim_article_price_category.* FROM pim_article_price_category WHERE category_id='".$in['category_id']."' ");
		$this->db->move_next();
		$price_value = $this->db->f('price_value');
		$type = $this->db->f('type');
		$price_type = $this->db->f('price_type');
		$price_value_type = $this->db->f('price_value_type');

		$this->db->query("UPDATE pim_article_price_category SET     name='".$in['name']."',
		                                                            type='".$in['type']."',
		                                                            price_type='".$in['price_type_id']."',
		                                                            price_value='".return_value($in['price_value'])."',
		                                                            price_value_type='".$in['price_value_type_id']."'
		                  WHERE category_id='".$in['category_id']."'");

       $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE category_id='".$in['category_id']."'");
   			while($this->db2->move_next()){
           	$article_ids.=$this->db2->f('article_id').',';
   		}
        $article_ids=rtrim($article_ids,',');
        $article_ids_array=explode(',',$article_ids);

		if(($in['type'] != $type) || ($in['price_type_id'] != $price_type) || (return_value($in['price_value']) != $price_value) || ($in['price_value_type_id'] != $price_value_type)) {
		
         
           if(!$article_ids){   
			 $this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' and base_price!=1");
			}
			else{
			 $this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' AND article_id NOT IN (".$article_ids.") and base_price!=1");	
			}
			
			$article_prices = $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices WHERE base_price=1");

			while ($article_prices->next()){
                     if($in['price_type']==2){
                       $article_base_price=$article_prices->f('purchase_price');
                     }else{
                       $article_base_price=$article_prices->f('price');
                      }

				//insert the article prices
				switch ($in['type']) {
					case 1:                  //discount
					if($in['price_value_type_id']==1){  // %
						$total_price =$article_base_price - (return_value($in['price_value']) * $article_base_price / 100);
					}else{ //fix
						$total_price = $article_base_price - return_value($in['price_value']);
					}

					break;
					case 2:                 //profit margin
					if($in['price_value_type_id']==1){  // %
						$total_price = $article_base_price + (return_value($in['price_value']) * $article_base_price / 100);
					}else{ //fix
						$total_price =$article_base_price + return_value($in['price_value']);
					}

					break;

				}
				$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_prices->f('article_id')."'");
				$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
				$total_with_vat = $total_price+($total_price*$vat_value/100);
				
            if(!in_array($article_prices->f('article_id'),  $article_ids_array)){
				$this->db->query("INSERT INTO pim_article_prices SET
				article_id			= '".$article_prices->f('article_id')."',
				price_category_id	= '".$in['category_id']."',
				price				= '".$total_price."',
				total_price			= '".$total_with_vat."'
				");
              }
            
			}

		}
		msg::success ( "Service Price Category edited.",'success');
		return true;
	}
	function update_category_validate(&$in){
		$v=new validation($in);
		$v->f('category_id','Category Id','required:exist[pim_article_price_category.category_id]');
		if(!$v->run()){
			return false;
		}else{
			return $v -> run();
		}
	}
	function add_volume(&$in)
	{		
		$in['failure'] = false;
		if(!$this->add_volume_validate($in)){
			$in['failure'] = true;
			return false;
		}

		
		$in['id'] = $this->db->insert("INSERT INTO  article_price_volume_discount SET `from_q`='".$in['from_q']."',
		                                                                                   `to_q`='".$in['to_q']."',
		                                                                                   percent='".return_value($in['percent'])."' 
		                                                                                 
		                                       ");
       
		$query_sync[0]="INSERT INTO article_price_volume_discount SET                         
		                                                                                  `from_q`='".$in['from_q']."',
		                                                                                   `to_q`='".$in['to_q']."',
		                                                                                   percent='".return_value($in['percent'])."' 
		                                       ";
		
		msg::success ( gm("Volume Discount added."),'success');

		return true;
	}
	function add_volume_validate(&$in){

		$v = new validation($in);
		if (ark::$method == 'update_volume') {
			$option = ".(id != '".$in['id']."')";
		} else {
			$option = "";
		}
          // biggest to quantity
		$biggest_to_quantity=$this->db->field("SELECT  article_price_volume_discount.to_q FROM  article_price_volume_discount  ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT article_price_volume_discount.from_q FROM article_price_volume_discount  ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   $in['price_error']=gm('To Qty value should be bigger than From Qty');
		   msg::error(gm('To Qty value should be bigger than From Qty'),'error');
		   return false;
		}
		 if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['from_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
         	 msg::error(gm('To Qty value should be bigger than From Qty3'),'error');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['to_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
         	 msg::error(gm('To Qty value should be bigger than From Qty2'),'error');
		     return false;
          }
           	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND    article_price_volume_discount.to_q < '".return_value($in['to_q'])."'
         	               AND article_price_volume_discount.to_q > '".return_value($in['from_q'])."'
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
         	 msg::error(gm('To Qty value should be bigger than From Qty1'),'error');
		     return false;
          }

        $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."' and  article_price_volume_discount.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
            	 msg::error(gm('To Qty value should be bigger than From Qty4'),'error');
		         return false;
            }



        }else{

            $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE    article_price_volume_discount.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	msg::error(gm('To Qty value should be bigger than From Qty5'),'error');
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }

        }



	
		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]:numeric');
		$v -> f('to_q', 'Max Qty', 'numeric:greater_than[1]');
		$v->field('percent','Percent','required');

		return $v->run();

	}
	function update_volume(&$in)
	{   
		$in['failure'] = false;
		if(!$this->update_volume_validate($in)){
			$in['failure'] = true;
			return false;
		}
		

		$this->db->query("UPDATE  article_price_volume_discount SET  `from_q`='".$in['from_q']."',
		                                                             to_q='".$in['to_q']."',
		                                                             percent='".return_value($in['percent'])."'
		                                                           
		                  WHERE id='".$in['id']."'");

      
		msg::success ( gm("Volume Discount edited."),'success');
		return true;
	}
	function update_volume_validate(&$in){
		$v=new validation($in);
		$v->f('id',' Id','required:exist[article_price_volume_discount.id]');
			
		$biggest_to_quantity=$this->db->field("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  id!='".$in['id']."' ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT article_price_volume_discount.from_q FROM article_price_volume_discount WHERE  id!='".$in['id']."' ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   $in['price_error']=gm('To Qty value should be bigger than From Qty');
		   return false;
		}
        if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['from_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['to_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	                 AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND    article_price_volume_discount.to_q < '".return_value($in['to_q'])."'
         	               AND article_price_volume_discount.to_q > '".return_value($in['from_q'])."'
         	                AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

          $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND  article_price_volume_discount.to_q=0  AND id!='".$in['id']."' LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }



        }else{
           //return_value($in['from_q']) >

            $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE    article_price_volume_discount.to_q=0  AND id!='".$in['id']."' LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }

        }

		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]');

		return $v->run();
	}
	function delete_volume(&$in)	{
		
		if(!$this->delete_volume_validate($in)){
			
			return false;
		}
		$this->db->query("DELETE FROM article_price_volume_discount WHERE id='".$in['id']."' ");
		
		msg::success ( gm("Article Volume Discount deleted."),'success');
		
		return true;
	}
	function delete_volume_validate(&$in){
		$v=new validation($in);
		$v->f('id',' Id','required:exist[article_price_volume_discount.id]');

		return $v->run();

	}
	function add_tax(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_tax_validate($in)){
            $in['failure'] = true;
			return false;
		}
		
		$in['tax_id'] = $this->db->insert("INSERT INTO pim_article_tax SET
																		
																code  		    = '".$in['code']."',
																type_id  		= '".$in['type_id']."',
																amount			= '".return_value($in['amount'])."',
																description		= '".$in['description']."'																
																");

		msg::success ( "Tax succefully added.",'success');
		return true;
	}

	/**
	 * Validae add data
	 * @param array $in
	 * @return boolean
	 */
	function add_tax_validate(&$in)
	{

		$v = new validation($in);

		if (ark::$method == 'update') {

			$option = ".( tax_id != '" . $in['tax_id'] . "')";
		} else {
			$option = "";
		}

	    $v -> f('code', 'Code', 'required:unique[pim_article_tax.code' . $option . ']');
		
		$v -> f('amount', 'Amount', 'required:numeric');
		
		return $v -> run();
	}

	function update_tax(&$in)
	{
		 $in['failure'] = false;
		if(!$this->update_tax_validate($in)){
            $in['failure'] = true;
			return false;
		}
		
		$this->db->query("UPDATE pim_article_tax SET
							
													code  		    = '".$in['code']."',
													type_id  		= '".$in['type_id']."',
													amount			= '".return_value($in['amount'])."',
													description		= '".$in['description']."'
													
						  WHERE tax_id='".$in['tax_id']."' ");

		msg::success ( "Tax succefully updated.",'success');
		return true;
	}

	/**
	 * Update validate data
	 * @param  array $in
	 * @return boolean
	 */
	function update_tax_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('tax_id', 'Tax Id', 'required:exist[pim_article_tax.tax_id]');

		return $v->run();

	}
	function delete_tax(&$in)
	{

		if(!$this->delete_tax_validate($in)){
			return false;
		}
		$this->db->query("DELETE FROM pim_article_tax WHERE tax_id='".$in['tax_id']."' ");
	    $this->db->query("DELETE FROM pim_articles_taxes WHERE tax_id='".$in['tax_id']."' ");
		msg::success ( "Tax deleted.",'success');
		return true;
	}

	/**
	 * Validate delete data
	 * @param  array $in
	 * @return boolean
	 */
	function delete_tax_validate(&$in)
	{
		
		$v=new validation($in);
		$v->f('tax_id','Tax Id','required:exist[pim_article_tax.tax_id]');
		$this->db->query("SELECT article_id FROM pim_articles_taxes WHERE tax_id='".$in['tax_id']."'");
		if($this->db->move_next()){
			// $v->f('tax_id','Tax Id','empty_condition','There are articles define for these tax.Please update first that articles.');
		
		}

		return $v->run();

	}



	function saveAddToArticle(&$in)
	{

  	if($in['check_all']){

  		if($in['val'] == 1){

  			foreach ($in['item'] as $key => $value) {
			    $_SESSION['add_to_article'][$value]= $in['val'];
	  		}
  		}else{
  			foreach ($in['item'] as $key => $value) {
			    unset($_SESSION['add_to_article'][$value]);
	  		}
		  }
  	}else{
	    if($in['val'] == 1){
	    	$_SESSION['add_to_article'][$in['id']]= $in['val'];
	    }else{
	    	unset($_SESSION['add_to_article'][$in['id']]);
	    }
	  }
	  	json_out($_SESSION['add_to_article']);
    return true;
  }
  function apply_ledger_to_articles(&$in)
  {
	 if($_SESSION['add_to_article']){
	  foreach ($_SESSION['add_to_article'] as $key => $value) {
	    
	      $this->db->query("SELECT *
	                    FROM  fam_ledger
	                    WHERE fam_id ='".$key."' 
	                    ");
	if($this->db->move_next()){
	   $ledger_account_id=$this->db2->field("SELECT   pim_article_ledger_accounts.id FROM   pim_article_ledger_accounts WHERE name='".$this->db->f('ledger_val')."'");
	   $this->db2->query("UPDATE pim_articles SET ledger_account_id='".$ledger_account_id."' WHERE article_category_id='".$this->db->f('fam_id')."'");
	}
	  }
	}
	unset($_SESSION['add_to_article']);
    msg::success ( gm("Accounts successfully set."),'success');
    return true;
  }
  function add_fam(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_fam_validate($in)){
            $in['failure'] = true;
			return false;
		}
		$this->db->insert("DELETE FROM fam_ledger WHERE fam_id      = '".$in['fam_id']."'");
		
		$in['fam_ledger_id'] = $this->db->insert("INSERT INTO fam_ledger SET
																fam_id      = '".$in['fam_id']."',
																ledger_val      = '".$in['ledger_val']."'	
																
															
																								
																");
	  $this->db->query("SELECT * FROM pim_article_ledger_accounts WHERE name      = '".$in['ledger_val']."'");

	  if(!$this->db->move_next()){
	    $this->db2->query("INSERT INTO pim_article_ledger_accounts SET
																
																name      = '".$in['ledger_val']."'	
																
															
																								
																");
	}

		msg::success ( gm("Field updated."),'success');
		return true;
	}

	/**
	 * Validae add data
	 * @param array $in
	 * @return boolean
	 */
	function add_fam_validate(&$in)
	{

		$v = new validation($in);

		

	  $v -> f('ledger_val','fg','required');
	  $v -> f('fam_id', 'fam_id', 'required:numeric');
		
		return $v -> run();
	}


	function upgrade(&$in)
	{
		
		$this->db->query("UPDATE settings SET value=1  WHERE constant_name='ADV_PRODUCT'");

		msg::success(gm("Account upgraded"),'success');
		return true;
	}

    function archiveBulkServices(&$in)
    {
        if(empty($_SESSION['add_service_to_archive'])){
            msg::error(gm('No services selected'),'error');
            json_out($in);
        }

        $add_service_to_archive=$this->db->query("SELECT article_id FROM pim_articles WHERE is_service='1' AND active='1' AND article_id IN (".implode(',',array_keys($_SESSION['add_service_to_archive'])).")")->getValues('article_id');

        if(empty($add_service_to_archive)){
	      msg::error(gm('No services selected'),'error');
	      json_out($in);
	    }

        foreach($add_service_to_archive as $value){
            $is_active = $this->db->field("SELECT active FROM pim_articles WHERE article_id = '". $value ."'");
            if ($is_active) {
                $this->db->query("UPDATE pim_articles SET active='0', updated_at='" . time() . "' WHERE article_id='" . $value . "' ");
                insert_message_log($this->pag, '{l}Service archived by{endl} ' . get_user_name($_SESSION['u_id']), 'article_id', $value, false, $_SESSION['u_id']);
            }
            unset($_SESSION['add_service_to_archive'][$value]);
        }
        msg::success(gm('Services successfully archived'),'success');

        return true;
    }

    function restoreBulkServices(&$in)
    {
        if(empty($_SESSION['add_service_to_archive'])){
            msg::error(gm('No articles selected'),'error');
            json_out($in);
        }

        $add_service_to_archive=$this->db->query("SELECT article_id FROM pim_articles WHERE is_service='1' AND active='0' AND article_id IN (".implode(',',array_keys($_SESSION['add_service_to_archive'])).")")->getValues('article_id');

        if(empty($add_service_to_archive)){
	      msg::error(gm('No services selected'),'error');
	      json_out($in);
	    }

        foreach($add_service_to_archive as $value){
            $is_active = $this->db->field("SELECT active FROM pim_articles WHERE article_id = '". $value ."'");
            if (!$is_active) {
                $this->db->query("UPDATE pim_articles SET active='1', updated_at='" . time() . "' WHERE article_id='" . $value . "' ");
                insert_message_log($this->pag, '{l}Service activated by{endl} ' . get_user_name($_SESSION['u_id']), 'article_id', $value, false, $_SESSION['u_id']);
            }
            unset($_SESSION['add_service_to_archive'][$value]);
        }
        msg::success(gm('Services successfully restored'),'success');

        return true;
    }


}//end class
?>