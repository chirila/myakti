<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');

setcookie('Akti-Export','6',time()+3600,'/');
//require_once 'libraries/PHPExcel.php';
ark::loadLibraries(array('PHPExcel'));
$filename ="article_export.csv";

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Articles report")
               ->setSubject("Articles report")
               ->setDescription("Articles export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Articles");*/
$db = new sqldb();
$db2 = new sqldb();
$db3 = new sqldb();

$filter = '1=1';
$filter_attach = '';
$filter.= " AND pim_articles.is_service='0' ";

if(!$in['archived']){
  $filter.= " AND pim_articles.active='1' ";
 

}else{
  $filter.= " AND pim_articles.active='0' ";
 
}


if(!empty($in['search'])){
  $filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
 
}
if($in['attach_to_product']){
   if($in['attach_to_product']==1){
    $filter_attach.= " INNER JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
   }else{
    $filter_attach.= " LEFT JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
    $filter.=" AND pim_product_article.article_id is null";
   }    
  
}
if($in['is_picture']){
   if($in['is_picture']==1){
    $filter_attach.= " INNER JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
   }else{
    $filter_attach.= " LEFT JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
    $filter.=" AND pim_article_photo.parent_id is null";
   }    
  $arguments.="&is_picture=".$in['is_picture'];
}

if($in['article_category_id']){ 
  $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";  
  
}
if($in['first'] && $in['first'] == '[0-9]'){
  $filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
  
} elseif ($in['first']){
  $filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
  
}

if(!empty($in['supplier_reference'])){
  $filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
}

if($in['supplier_id'] && !empty($in['supplier_id'])){
    $filter .=" AND pim_articles.supplier_id='".$in['supplier_id']."' ";
}

$headers = array('ARTICLE ID',
                    'ITEM CODE',                
                    'INTERNAL NAME',
                    "NAME (EN)",
                    "NAME (FR)",
                    "NAME (DU)",
                    "NAME (GE)",
                    "STOCK",
                    "ORIGIN CODE",
                    "EAN CODE",
                    "CATEGORY",
                    "BRAND",
                    "PACKING",
                    "SALE UNIT",
                    "BASE PRICE",
                    "PURCHASE PRICE",
                    "VAT PERCENT",
                    "DESCRIPTION (EN)",
                    "DESCRIPTION (FR)",
                    "DESCRIPTION (DU)",
                    "DESCRIPTION (GE)",
                    //"BLOCKED",
                    "SUPPLIER",
                    "SUPPLIER REFERENCE",
                    //"ADDITIONAL PRICE",
                    "LEDGER ACCOUNT"
      );

$price_categories=$db->query("SELECT * FROM pim_article_price_category ORDER BY name ASC")->getAll();
foreach($price_categories as $category){
    $headers[]=$category['name'];
}

//articles ids data
$articles_ids_str="";

$articles_ids_data=$db->query("SELECT DISTINCT(pim_articles.article_id) FROM pim_articles $filter_attach WHERE $filter")->getAll();

foreach($articles_ids_data as $k=>$v){
  $articles_ids_str.="'".$v['article_id']."',";
}
$articles_ids_str=rtrim($articles_ids_str,",");

$articles_ids_data=null;
unset($articles_ids_data);
//end articles ids data

//lang_data data
$lang_data_ar=array();

$lang_data=$db->query("SELECT * FROM pim_articles_lang WHERE item_id IN (".$articles_ids_str.") ")->getAll();

foreach($lang_data as $k=>$v){
  if(array_key_exists($v['item_id'],$lang_data_ar) === false){
    $lang_data_ar[$v['item_id']]=array();
  }
  if(array_key_exists($v['lang_id'],$lang_data_ar[$v['item_id']]) === false){
    $lang_data_ar[$v['item_id']][$v['lang_id']]=array();
  }
  $lang_data_ar[$v['item_id']][$v['lang_id']]['name']=$v['name'];
  $lang_data_ar[$v['item_id']][$v['lang_id']]['description']=$v['description'];
}

$lang_data=null;
unset($lang_data);
//end lang_data data

//vats data
$vats_data_ar=array();

$vats_data=$db->query("SELECT * FROM vats")->getAll();
foreach($vats_data as $k=>$v){
  $vats_data_ar[$v['vat_id']]=$v['value'];
}

$vats_data=null;
unset($vats_data);
//end vats data

//articles prices data
$articles_prices_ar=array();

$articles_prices_data=$db->query("SELECT * FROM pim_article_prices WHERE base_price='1' AND article_id IN (".$articles_ids_str.") GROUP BY article_id")->getAll();
foreach($articles_prices_data as $k=>$v){
  if(array_key_exists($v['article_id'], $articles_prices_ar) === false){
    $articles_prices_ar[$v['article_id']]=array();
  }
  $articles_prices_ar[$v['article_id']]['price']=$v['price'];
  $articles_prices_ar[$v['article_id']]['purchase_price']=$v['purchase_price'];
}
$articles_prices_data=null;
unset($articles_prices_data);
//end article prices data

//article categories data
$article_categories_data_ar=array();

$article_categories_data=$db->query("SELECT * FROM pim_article_categories")->getAll();
foreach($article_categories_data as $k=>$v){
  $article_categories_data_ar[$v['id']]=$v['name'];
}

$article_categories_data=null;
unset($article_categories_data);
//end article categories data

//article brands data
$article_brands_data_ar=array();

$article_brands_data=$db->query("SELECT * FROM pim_article_brands")->getAll();
foreach($article_brands_data as $k=>$v){
  $article_brands_data_ar[$v['id']]=$v['name'];
}

$article_brands_data=null;
unset($article_brands_data);
//end article brands data

//article ledger data
$article_ledger_data_ar=array();

$article_ledger_data=$db->query("SELECT * FROM pim_article_ledger_accounts")->getAll();
foreach($article_ledger_data as $k=>$v){
  $article_ledger_data_ar[$v['id']]=$v['name'];
}

$article_ledger_data=null;
unset($article_ledger_data);
//end article ledger data

//article prices list data
$prices_list_data_ar=array();

$prices_list_data=$db->query("SELECT pim_article_price_category.*,pim_article_prices.price,pim_article_prices.total_price,pim_article_prices.article_id
        FROM pim_article_price_category 
        RIGHT JOIN pim_article_prices ON pim_article_price_category.category_id=pim_article_prices.price_category_id
        WHERE pim_article_prices.article_id IN (".$articles_ids_str.") AND base_price!=1 ORDER BY pim_article_price_category.name ASC")->getAll();

foreach($prices_list_data as $k=>$v){
  if(!$v['category_id']){
    continue;
  }
  if(array_key_exists($v['article_id'], $prices_list_data_ar) === false){
    $prices_list_data_ar[$v['article_id']]=array();
  }
  $prices_list_data_ar[$v['article_id']][]=$v;
}

$prices_list_data=null;
unset($prices_list_data);
//end article prices list data

//article custom category data
$article_custom_cat_data_ar=array();

$article_custom_cat_data=$db->query("SELECT * FROM pim_article_price_category_custom")->getAll();
foreach($article_custom_cat_data as $k=>$v){
  if(array_key_exists($v['article_id'].'_'.$v['category_id'], $article_custom_cat_data_ar) === false){
    $article_custom_cat_data_ar[$v['article_id'].'_'.$v['category_id']]=array();
  }
  $article_custom_cat_data_ar[$v['article_id'].'_'.$v['category_id']]=$v;
}

$article_custom_cat_data=null;
unset($article_custom_cat_data);
//end article custom category data

//fam custom price data
$fam_cust_data_ar=array();

$fam_cust_data=$db->query("SELECT value FROM fam_custom_price")->getAll();
foreach($fam_cust_data as $k=>$v){
  $fam_cust_data_ar[$v['fam_id'].'_'.$v['category_id']]=$v['value'];
}
$fam_cust_data=null;
unset($fam_cust_data);
//end fam custom price data

$info = $db->query("SELECT pim_articles.supplier_name,pim_articles.hide_stock,pim_articles.article_id,pim_articles.front_active,pim_articles.origin_number,pim_articles.ean_code,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,
                     pim_articles.vat_id, pim_articles.article_category_id, pim_articles.article_brand_id, pim_articles.supplier_reference, pim_articles.aac_price,  
                         pim_articles.internal_name,
                         pim_articles.packing,pim_articles.sale_unit,
                         pim_articles.ledger_account_id
            FROM pim_articles
      $filter_attach
      WHERE $filter 
      GROUP BY pim_articles.article_id
      ORDER BY  pim_articles.item_code 
      ");

  $final_data=array();

   $xlsRow=2;
while ($info->next())
{

    $tmp_prices=[];
    /*$prices_list=$db->query("SELECT pim_article_price_category.*,pim_article_prices.price,pim_article_prices.total_price
        FROM pim_article_price_category 
        RIGHT JOIN pim_article_prices ON pim_article_price_category.category_id=pim_article_prices.price_category_id
        WHERE pim_article_prices.article_id = '".$info->f('article_id')."' AND base_price!=1 ORDER BY pim_article_price_category.name ASC");*/
    //while ($prices_list->move_next()) {
    if(array_key_exists($info->f('article_id'), $prices_list_data_ar)){
      foreach($prices_list_data_ar[$info->f('article_id')] as $value){
          /*if(!$prices_list->f('category_id')){
              continue;
          }*/        
          //$db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE category_id='".$prices_list->f('category_id')."' AND article_id='".$info->f('article_id')."'");
          //if($db2->move_next()){
          if(array_key_exists($info->f('article_id').'_'.$value['category_id'], $article_custom_cat_data_ar)){
              $is_custom=true;
              $type = $article_custom_cat_data_ar[$info->f('article_id').'_'.$value['category_id']]['type']; //$db2->f('type')
              $price_value = $article_custom_cat_data_ar[$info->f('article_id').'_'.$value['category_id']]['price_value']; //$db2->f('price_value');
              $price_value_type = $article_custom_cat_data_ar[$info->f('article_id').'_'.$value['category_id']]['price_value_type']; //$db2->f('price_value_type');
          }else{
              $is_custom=false;
              $type = $value['type']; //$prices_list->f('type');            
              //$price_value_custom_fam=$db3->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$info->f('article_category_id')."' AND category_id='".$prices_list->f('category_id')."'");
              //if($price_value_custom_fam==NULL){
              if(array_key_exists($info->f('article_category_id').'_'.$value['category_id'], $fam_cust_data_ar) === false || (array_key_exists($info->f('article_category_id').'_'.$value['category_id'], $fam_cust_data_ar) && $fam_cust_data_ar[$info->f('article_category_id').'_'.$value['category_id']] == NULL)){
                  $price_value = $value['price_value'];//$prices_list->f('price_value');
                  $custom_fam=0;
              }else{
                  $price_value = $fam_cust_data_ar[$info->f('article_category_id').'_'.$value['category_id']];//$price_value_custom_fam;
                  $custom_fam=1;
              }
              $price_value_type = $value['price_value_type'];//$prices_list->f('price_value_type');
          }
          if($custom_fam==0){
              $price = $value['price'];//$prices_list->f('price');
          }else{
              //we have to apply to the base price the category spec
              $cat_price_type=$value['price_type'];//$db3->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$prices_list->f('category_id')."'");
              $cat_type=$value['type'];//$db3->field("SELECT type FROM pim_article_price_category WHERE category_id='".$prices_list->f('category_id')."'");
              $price_value_type=$value['price_value_type'];//$db3->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$prices_list->f('category_id')."'");
              if($cat_price_type==2){
                  $article_base_price=$articles_prices_ar[$info->f('article_id')]['purchase_price'];
                  //$article_base_price=get_article_calc_price($info->f('article_id'),3);
              }else{
                  $article_base_price=$articles_prices_ar[$info->f('article_id')]['price'];
                  //$article_base_price=get_article_calc_price($info->f('article_id'),1);
              }
              switch ($cat_type) {
                  case 1:                  //discount
                      if($price_value_type==1){  // %
                          $price = $article_base_price - $price_value * $article_base_price / 100;
                      }else{ //fix
                          $price = $article_base_price - $price_value;
                      }
                      break;
                  case 2:                 //profit margin
                      if($price_value_type==1){  // %
                          $price = $article_base_price + $price_value * $article_base_price / 100;
                      }else{ //fix
                          $price =$article_base_price + $price_value;
                      }
                      break;
              }
          }
          $price_row = array(
              'price'                => $price,
          );
          array_push($tmp_prices, $price_row);
      }
    }

    $name_lang_en="";
    $name_lang_fr="";
    $name_lang_du="";
    $name_lang_de="";
    $description_lang_en="";
    $description_lang_fr="";
    $description_lang_du="";
    $description_lang_de="";

    if(array_key_exists($info->f('article_id'), $lang_data_ar)){
      if(array_key_exists(1, $lang_data_ar[$info->f('article_id')]) && array_key_exists('name', $lang_data_ar[$info->f('article_id')][1])){
        $name_lang_en=$lang_data_ar[$info->f('article_id')][1]['name'];
      }
      if(array_key_exists(2, $lang_data_ar[$info->f('article_id')]) && array_key_exists('name', $lang_data_ar[$info->f('article_id')][2])){
        $name_lang_fr=$lang_data_ar[$info->f('article_id')][2]['name'];
      }
      if(array_key_exists(3, $lang_data_ar[$info->f('article_id')]) && array_key_exists('name', $lang_data_ar[$info->f('article_id')][3])){
        $name_lang_du=$lang_data_ar[$info->f('article_id')][3]['name'];
      }
      if(array_key_exists(4, $lang_data_ar[$info->f('article_id')]) && array_key_exists('name', $lang_data_ar[$info->f('article_id')][4])){
        $name_lang_de=$lang_data_ar[$info->f('article_id')][4]['name'];
      }
      if(array_key_exists(1, $lang_data_ar[$info->f('article_id')]) && array_key_exists('description', $lang_data_ar[$info->f('article_id')][1])){
        $description_lang_en=$lang_data_ar[$info->f('article_id')][1]['description'];
      }
      if(array_key_exists(2, $lang_data_ar[$info->f('article_id')]) && array_key_exists('description', $lang_data_ar[$info->f('article_id')][2])){
        $description_lang_fr=$lang_data_ar[$info->f('article_id')][2]['description'];
      }
      if(array_key_exists(3, $lang_data_ar[$info->f('article_id')]) && array_key_exists('description', $lang_data_ar[$info->f('article_id')][3])){
        $description_lang_du=$lang_data_ar[$info->f('article_id')][3]['description'];
      }
      if(array_key_exists(4, $lang_data_ar[$info->f('article_id')]) && array_key_exists('description', $lang_data_ar[$info->f('article_id')][4])){
        $description_lang_de=$lang_data_ar[$info->f('article_id')][4]['description'];
      }
    }

       $tmp_item=array(
        $info->f('article_id'),
        $info->f('item_code'),
        html_entity_decode(preg_replace("/[\n\r]/","",stripslashes($info->f('internal_name')))),
        preg_replace("/[\n\r]/","",stripslashes($name_lang_en)),
        preg_replace("/[\n\r]/","",stripslashes($name_lang_fr)),
        preg_replace("/[\n\r]/","",stripslashes($name_lang_du)),
        preg_replace("/[\n\r]/","",stripslashes($name_lang_de)),
        ($info->f('hide_stock')?gm('No Stock'):display_number_exclude_thousand($info->f('stock'))),
        preg_replace("/[\n\r]/","",$info->f('origin_number')).'',
        preg_replace("/[\n\r]/","",$info->f('ean_code')).'',
        (array_key_exists($info->f('article_category_id'), $article_categories_data_ar) ? preg_replace("/[\n\r]/","",$article_categories_data_ar[$info->f('article_category_id')]) : ""),
        (array_key_exists($info->f('article_brand_id'), $article_brands_data_ar) ? $article_brands_data_ar[$info->f('article_brand_id')] : ""),
        remove_zero_decimals($info->f('packing')),
        $info->f('sale_unit'),
        (array_key_exists($info->f('article_id'), $articles_prices_ar) ? display_number_exclude_thousand($articles_prices_ar[$info->f('article_id')]['price']) : 0.00),
        (array_key_exists($info->f('article_id'), $articles_prices_ar) ? display_number_exclude_thousand($articles_prices_ar[$info->f('article_id')]['purchase_price']) : 0.00),
        (array_key_exists($info->f('vat_id'), $vats_data_ar) ? $vats_data_ar[$info->f('vat_id')] : ""),
        preg_replace("/[\n\r]/","",stripslashes($description_lang_en)),
        preg_replace("/[\n\r]/","",stripslashes($description_lang_fr)),
        preg_replace("/[\n\r]/","",stripslashes($description_lang_du)),
        preg_replace("/[\n\r]/","",stripslashes($description_lang_de)),
        //($info->f('front_active')==1 ? 0 : 1),
        preg_replace("/[\n\r]/","",stripslashes($info->f('supplier_name'))),
        $info->f('supplier_reference'),
        //(defined('AAC') && AAC=='1' ? $info->f('aac_price') : 0),
        (array_key_exists($info->f('ledger_account_id'), $article_ledger_data_ar) ? $article_ledger_data_ar[$info->f('ledger_account_id')] : "")
      );

      foreach($tmp_prices as $price_list_row){
          $tmp_item[]=$price_list_row['price'];
      }
      array_push($final_data,$tmp_item); 

  
  $xlsRow++;
}

//free memory
$articles_ids_str="";
unset($articles_ids_str);
$lang_data_ar=null;
unset($lang_data_ar);
$vats_data_ar=null;
unset($vats_data_ar);
$articles_prices_ar=null;
unset($articles_prices_ar);
$article_categories_data_ar=null;
unset($article_categories_data_ar);
$article_brands_data_ar=null;
unset($article_brands_data_ar);
$article_ledger_data_ar=null;
unset($article_ledger_data_ar);
$prices_list_data_ar=null;
unset($prices_list_data_ar);
$article_custom_cat_data_ar=null;
unset($article_custom_cat_data_ar);
$fam_cust_data_ar=null;
unset($fam_cust_data_ar);
//end free memory

if(!file_exists('upload/'.DATABASE_NAME)){
  if(!mkdir('upload/'.DATABASE_NAME)) {
    die('Failed');exit;
  }
}


header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$from_location='upload/'.DATABASE_NAME.'/article_export_'.$_SESSION['u_id'].'_'.time().'.csv';
$fp = fopen("php://output", 'w');

fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
unlink($from_location);
exit();



define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',
  
  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('../upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name."); 
}
// file size in bytes
$fsize = filesize($file_path); 
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type."); 
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);  
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
setcookie('Akti-Export','9',time()+3600,'/');
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>