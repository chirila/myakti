<?php
$db2=new sqldb();
$is_data=false;

$result = array('batch'=>array());

if($in['id']){
	$batch_number_data = $db->query("SELECT * FROM batches WHERE id = '".$in['id']."'");
	if($batch_number_data->move_next()){
		$status_id=1;

		$result=array(
			'batch_number'								=> $batch_number_data->f('batch_number'),
			'id'									   => $in['id'],
			'status_id'								    => $batch_number_data->f('status_id'),
			'date_in'                                   => $batch_number_data->f('date_in')?$batch_number_data->f('date_in')*1000:'',
			'date_out'                                  => $batch_number_data->f('date_out')?$batch_number_data->f('date_out')*1000:'',
			'date_exp'                                  => $batch_number_data->f('date_exp')?$batch_number_data->f('date_exp')*1000:'',
			'article_id'								=> $in['article_id'],
			'status_dd'					=> build_batch_nr_status_dd($batch_number_data->f('status_id')),
			
			'status_details_1'							=> $batch_number_data->f("status_details_1") != 'Added manually'? $batch_number_data->f("status_details_1"):gm($batch_number_data->f("status_details_1")),
			'status_details_2'							=> $batch_number_data->f('status_details_2'),
			'status_details_3'							=> $batch_number_data->f('status_details_3'),
			'quantity'									=> display_number($batch_number_data->f('quantity')),
			'in_stock'									=> display_number($batch_number_data->f('in_stock')),
			'locations'			=> array()
		);
		 array_push($result['batch'], $result);


		$locations=$db->query("SELECT  dispatch_stock_address.* ,SUM( batches_dispatch_stock.quantity ) AS quantity,batches_dispatch_stock.batch_id
            FROM  dispatch_stock_address 
            LEFT JOIN batches_dispatch_stock ON batches_dispatch_stock.address_id=dispatch_stock_address.address_id 
            AND batches_dispatch_stock.batch_id='".$in['id']."' 
            GROUP BY address_id
            ORDER BY dispatch_stock_address.naming ASC");

		while($locations->move_next()) {
		    $item=array(
                'naming'      => stripslashes($locations->f('naming')),
                'address_id'  => $locations->f('address_id'),
                'stock'  => display_number($locations->f('quantity'))
		    ); 
		    array_push($result['locations'], $item);
		}
	}
}






$result['id']=$in['id'];







json_out($result);