<?php
$db2=new sqldb();
$is_data=false;
$o=0;
$p=0;
$i=0;
$po=0;
$result = array('query_order'=>array(),'query_project'=>array(),'query_intervention'=>array(),'query_po_order'=>array());
//orders
/*$orders = $db->query("SELECT COUNT( pim_orders.order_id ) as nr_orders, pim_orders.*,SUM(pim_order_articles.quantity) as total_order,SUM(pim_orders_delivery.quantity) as total_deliver,pim_orders_delivery.order_articles_id
                      FROM  pim_orders 
                      INNER JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id
                      LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id 
                      WHERE pim_order_articles.article_id='".$in['article_id']."' AND pim_orders.sent=1
                      GROUP BY pim_orders.order_id
                      ");*/
$stock_packing =  $db->field("SELECT pim_articles.packing FROM pim_articles WHERE pim_articles.article_id =  '".$in['article_id']."'");
$stock_sale_unit =  $db->field("SELECT pim_articles.sale_unit FROM pim_articles WHERE pim_articles.article_id =  '".$in['article_id']."'");
if(!ALLOW_ARTICLE_PACKING){
    $stock_packing = 1;
 }
if(!ALLOW_ARTICLE_SALE_UNIT){
    $stock_sale_unit = 1;
}

$orders = $db->query("SELECT pim_order_articles.order_articles_id, SUM( pim_order_articles.quantity ) as total_order ,COUNT(distinct pim_orders_delivery.delivery_id) as nr_del , pim_order_articles.article_id, SUM( pim_orders_delivery.quantity ) as total_deliver , SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_orders_delivery.delivery_id, pim_order_articles.order_id, pim_orders.*, pim_order_articles.sale_unit, pim_order_articles.packing
                        FROM pim_order_articles
                        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
                        LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
                        LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id 
                        WHERE pim_order_articles.article_id =  '".$in['article_id']."'
                        AND pim_orders.sent =1
                        AND pim_orders.active =1
                        GROUP BY  pim_orders.order_id
                      ");

while($orders->move_next()) {
    $total_deliver=0;
     $qty_order=0;
     $nr_del = $orders->f('nr_del');
     if($nr_del==0) {
          $nr_del = 1;
          //continue;
        }

     $stock_packing_ord = $orders->f("packing");
        if(!$stock_packing_ord){
          $stock_packing_ord = $stock_packing;
        }
        if(!ALLOW_ARTICLE_PACKING){
           $stock_packing_ord = 1;
         }
         
        $stock_sale_unit_ord = $orders->f("sale_unit");
        if(!$stock_sale_unit_ord){
          $stock_sale_unit_ord = $stock_sale_unit;
        }
        if(!ALLOW_ARTICLE_SALE_UNIT){
           $stock_sale_unit_ord = 1;
        } 

      $total_del=0;
        if(ORDER_DELIVERY_STEPS == '2'){
            $total_del = $orders->f('total_deliver2');
         }else{
            $total_del = $orders->f('total_deliver');
        }
   
    if($orders->f('total_order')/$nr_del < $total_del){
            $total_deliver = ($orders->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
        }else{
            if($total_del){
              $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord ;
            }
        }
     $qty_order = ($orders->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver; 
   
     $qty_diff=0;
    /*$diff = $db->query("SELECT COUNT( pim_orders.order_id ) as nr_orders, SUM( pim_order_articles.quantity ) AS total_order, SUM( pim_orders_delivery.quantity ) AS total_deliver
                        FROM pim_orders
                        INNER JOIN pim_order_articles ON pim_orders.order_id = pim_order_articles.order_id
                        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
                        WHERE pim_order_articles.article_id =  '".$in['article_id']."' AND pim_orders.order_id = '".$orders->f('order_id')."'
                        AND pim_orders.sent =1
                        AND pim_orders.rdy_invoice =  '1'
                       ");*/

  $diff = $db->query("SELECT pim_order_articles.order_articles_id, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del , pim_order_articles.article_id, SUM( pim_orders_delivery.quantity ) as total_deliver , SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_orders_delivery.delivery_id, pim_order_articles.order_id, pim_order_articles.sale_unit, pim_order_articles.packing
                        FROM pim_order_articles
                        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
                        LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
                        LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id 
                        WHERE pim_order_articles.article_id =  '".$in['article_id']."' AND pim_orders.order_id = '".$orders->f('order_id')."'
                        AND pim_orders.sent =1
                        AND pim_orders.rdy_invoice =  '1'
                        AND pim_orders.active =1
                       ");

    $total_deliver=0;
    

    while($diff->move_next()) {
       $nr_del = $diff->f('nr_del');
      if($nr_del==0) {
          $nr_del = 1;
        }

      $stock_packing_ord = $diff->f("packing");
        if(!$stock_packing_ord){
          $stock_packing_ord = $stock_packing;
        }
        if(!ALLOW_ARTICLE_PACKING){
           $stock_packing_ord = 1;
         }
         
        $stock_sale_unit_ord = $diff->f("sale_unit");
        if(!$stock_sale_unit_ord){
          $stock_sale_unit_ord = $stock_sale_unit;
        }
        if(!ALLOW_ARTICLE_SALE_UNIT){
           $stock_sale_unit_ord = 1;
        } 

      $total_del=0;
        if(ORDER_DELIVERY_STEPS == '2'){
            $total_del = $diff->f('total_deliver2');
         }else{
            $total_del = $diff->f('total_deliver');
        } 

        if($diff->f('total_order')/$nr_del < $total_del){
            $total_deliver = ($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
        }else{
            $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord ;
        }

     $qty_diff +=(($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver); // if we have an order delivered partially, but marked as delivered

    }


   if( $qty_order - $qty_diff!=0){
    $item_order=array(
                     'id'                    => $orders->f('order_id'),  
                     'serial_number'         => $orders->f('serial_number'),
                     'customer'              => $orders->f('customer_name'),
                     'created'               => date(ACCOUNT_DATE_FORMAT,$orders->f('date')),
                     'delivery_date'         => $orders->f('del_date')?date(ACCOUNT_DATE_FORMAT,$orders->f('del_date')):'',
                     'quantity'              => display_number($qty_order - $qty_diff)
                   
      );   
       array_push($result['query_order'], $item_order);
       $o++;
     }
}
//projects
$projects = $db->query("SELECT projects.*, SUM(project_articles.quantity) as q
                            FROM   projects
                            INNER JOIN project_articles  ON  projects.project_id=project_articles.project_id
                            WHERE project_articles.article_id='".$in['article_id']."'
                            AND projects.stage!=0 AND project_articles.delivered=0
                            GROUP BY projects.project_id");
while($projects->move_next()) {
    $item_project=array(
                     'project_id'       => $projects->f('project_id'),
                     'serial_number'    => $projects->f('serial_number'),
                     'customer'         => $projects->f('company_name'),
                     'start_date'       => date(ACCOUNT_DATE_FORMAT,$projects->f('start_date')),
                     'end_date'         => $projects->f('end_date')?date(ACCOUNT_DATE_FORMAT,$projects->f('end_date')):'',
                     'quantity'         => display_number($projects->f('q')*$stock_packing/$stock_sale_unit)
                   
      );   
       array_push($result['query_project'], $item_project);
       $p++;
}
//interventions
$interventions = $db->query("SELECT servicing_support.* , SUM( servicing_support_articles.quantity) as q
                            FROM   servicing_support 
                            INNER JOIN  servicing_support_articles ON  servicing_support.service_id=servicing_support_articles.service_id
                            WHERE servicing_support_articles.article_id='".$in['article_id']."'
                            AND servicing_support_articles.article_delivered=0 AND servicing_support.active=1 AND servicing_support.status<2 
                            GROUP BY servicing_support.service_id");
while($interventions->move_next()) {
    $item_intervention=array(
                     'service_id'         => $interventions->f('service_id'),
                     'serial_number'      => $interventions->f('serial_number'),
                     'customer'           => $interventions->f('customer_name'),
                    
                     'planeddate'         => $interventions->f('planeddate')?date(ACCOUNT_DATE_FORMAT,$interventions->f('planeddate')):'',
                     'quantity'           => display_number($interventions->f('q')*$stock_packing/$stock_sale_unit)
                   
      );   
       array_push($result['query_intervention'], $item_intervention);
       $i++;
}

//po_orders
/*$po_orders = $db->query("SELECT COUNT(pim_p_orders.p_order_id) as nr_orders, COUNT(pim_p_orders_delivery.delivery_id) as nr_del, pim_p_orders.*,SUM(pim_p_order_articles.quantity) as total_order,SUM(pim_p_orders_delivery.quantity) as total_deliver,pim_p_orders_delivery.order_articles_id
                      FROM  pim_p_orders 
                      INNER JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
                      LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id 
                      WHERE pim_p_order_articles.article_id='".$in['article_id']."' AND pim_p_orders.rdy_invoice!=0
                      GROUP BY pim_p_orders.p_order_id");*/

if(P_ORDER_DELIVERY_STEPS == '2'){
        $filter_po2 = " AND pim_p_order_deliveries.delivery_done ='1'  ";
    }

$po_orders = $db->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order ,COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity ) as total_deliver ,  SUM(CASE WHEN pim_p_order_deliveries.delivery_done = '1' THEN pim_p_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_p_orders_delivery.delivery_id, pim_p_order_articles.p_order_id, pim_p_orders.*, pim_p_order_articles.sale_unit,pim_p_order_articles.packing
                                FROM pim_p_order_articles
                                LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                                LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
                                LEFT JOIN pim_p_order_deliveries ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id 
                      WHERE pim_p_order_articles.article_id='".$in['article_id']."' AND pim_p_orders.rdy_invoice!=0
                      GROUP BY  pim_p_orders.p_order_id");


while($po_orders->move_next()) {
    //purchase orders haven't used packing and sale unit from the start
    /*$stock_packing_ord_p = $po_orders->f("packing");
        if(!$stock_packing_ord_p){*/
          $stock_packing_ord_p = $stock_packing;
      //  }
        if(!ALLOW_ARTICLE_PACKING){
           $stock_packing_ord_p = 1;
         }
         
     /*   $stock_sale_unit_ord_p = $po_orders->f("sale_unit");
        if(!$stock_sale_unit_ord_p){*/
          $stock_sale_unit_ord_p = $stock_sale_unit;
      //  }
        if(!ALLOW_ARTICLE_SALE_UNIT){
           $stock_sale_unit_ord_p = 1;
        } 

     $total_deliver=0;
     $qty_order_p=0;

      $total_del=0;
        if(P_ORDER_DELIVERY_STEPS == '2'){
            $total_del = $po_orders->f('total_deliver2');
         }else{
            $total_del = $po_orders->f('total_deliver');
          }

   if($po_orders->f('nr_del')){
      if($po_orders->f('total_order')/$po_orders->f('nr_del') < $total_del ){
            $total_deliver = ($po_orders->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$po_orders->f('nr_del');          
        }else{
            $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;
        }
        $qty_order_p = ($po_orders->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$po_orders->f('nr_del') - $total_deliver; 
   }else{
      $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;
      $qty_order_p = $po_orders->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p - $total_deliver; 
   }
    
     

      $qty_diff_p=0;
   /*   $diff_p = $db->query("SELECT COUNT( pim_p_orders.p_order_id) as nr_orders,COUNT(pim_p_orders_delivery.delivery_id) as nr_del, SUM( pim_p_order_articles.quantity ) AS total_order, SUM( pim_p_orders_delivery.quantity ) AS total_deliver
                          FROM pim_p_orders
                          INNER JOIN pim_p_order_articles ON pim_p_orders.p_order_id = pim_p_order_articles.p_order_id
                          LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                          WHERE pim_p_order_articles.article_id =  '".$in['article_id']."'
                          AND pim_p_orders.sent =1
                          AND pim_p_orders.rdy_invoice =  '1'
                          AND pim_p_orders.p_order_id ='".$po_orders->f('p_order_id')."'
                          GROUP BY pim_p_orders.p_order_id");*/
       $diff_p = $db->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order , COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity ) as total_deliver ,  SUM(CASE WHEN pim_p_order_deliveries.delivery_done = '1' THEN pim_p_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_p_orders_delivery.delivery_id, pim_p_orders_delivery.p_order_id, pim_p_order_articles.sale_unit,pim_p_order_articles.packing
                                FROM pim_p_order_articles
                                LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                                LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
                                LEFT JOIN pim_p_order_deliveries ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id 
                          WHERE pim_p_order_articles.article_id =  '".$in['article_id']."'
                          AND pim_p_orders.sent =1
                          AND pim_p_orders.rdy_invoice =  '1'
                          AND pim_p_orders.p_order_id ='".$po_orders->f('p_order_id')."' ".$filter_po2."
                          GROUP BY  pim_p_orders.p_order_id");
     $total_deliver=0;
    while($diff_p->move_next()) {

        /*$stock_packing_ord_p = $diff_p->f("packing");
        if(!$stock_packing_ord_p){*/
          $stock_packing_ord_p = $stock_packing;
       // }
        if(!ALLOW_ARTICLE_PACKING){
           $stock_packing_ord_p = 1;
         }
         
       /* $stock_sale_unit_ord_p = $diff_p->f("sale_unit");
        if(!$stock_sale_unit_ord_p){*/
          $stock_sale_unit_ord_p = $stock_sale_unit;
        //}
        if(!ALLOW_ARTICLE_SALE_UNIT){
           $stock_sale_unit_ord_p = 1;
        } 

         $total_del=0;
        if(P_ORDER_DELIVERY_STEPS == '2'){
            $total_del = $diff_p->f('total_deliver2');
         }else{
            $total_del = $diff_p->f('total_deliver');
          }

        if($diff_p->f('nr_del')){
            if($diff_p->f('total_order')/$diff_p->f('nr_del') < $total_del){
                $total_deliver = ($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$diff_p->f('nr_del');
            }else{
                $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;
            }
            $qty_diff_p +=(($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$diff_p->f('nr_del') - $total_deliver); // if we have an order delivered partially, but marked as delivered
        }else{
            $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;
            $qty_diff_p +=($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p - $total_deliver);
        }

      }
//console::log($qty_order_p,$qty_diff_p);
     if( $qty_order_p-$qty_diff_p!=0){
      $item_po_order=array(
                      'id'                   => $po_orders->f('p_order_id'),
                     'serial_number'         => $po_orders->f('serial_number'),
                     'customer'              => $po_orders->f('customer_name'),
                     'created'               => date(ACCOUNT_DATE_FORMAT,$po_orders->f('date')),
                     'delivery_date'         => $po_orders->f('del_date')?date(ACCOUNT_DATE_FORMAT,$po_orders->f('del_date')):'',
                     'quantity'              =>  $qty_order_p-$qty_diff_p
                   
      );   
       array_push($result['query_po_order'], $item_po_order);
       $po++;
     }
}




$articles=$db->query("SELECT pim_articles.item_code,pim_articles.internal_name FROM pim_articles WHERE article_id='".$in['article_id']."'");




if($o>0){
  $is_order_data=1;
}
if($p>0){
  $is_project_data=1;
}
if($i>0){
  $is_intervention_data=1;
}
if($po>0){
  $is_po_order_data=1;
}
$result['is_order_data']=$is_order_data;
$result['is_project_data']=$is_project_data;
$result['is_intervention_data']=$is_intervention_data;
$result['is_po_order_data']=$is_po_order_data;

$result['item_code']=$articles->f('item_code');
$result['internal_name']=$articles->f('internal_name');

$result['article_id']=$in['article_id'];
$result['account_number_format']=ACCOUNT_NUMBER_FORMAT;
$result['nr_decimals']=ARTICLE_PRICE_COMMA_DIGITS;
$result['style']=ACCOUNT_NUMBER_FORMAT;



json_out($result);