<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');

setcookie('Akti-Export','6',time()+3600,'/');
ark::loadLibraries(array('PHPExcel'));
$filename ="service_export.csv";

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Services report")
							 ->setSubject("Services report")
							 ->setDescription("Services export")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Services");*/
$db = new sqldb();

$filter = '1=1';
$filter_attach = '';
$filter.= " AND pim_articles.is_service='1' ";

if(!$in['archived']){
  $filter.= " AND pim_articles.active='1' ";
 

}else{
  $filter.= " AND pim_articles.active='0' ";
 
}


if(!empty($in['search'])){
  $filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
 
}




$info = $db->query("SELECT pim_articles.supplier_name,pim_articles.hide_stock,pim_articles.article_id,pim_articles.front_active,pim_articles.origin_number,pim_articles.ean_code,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,
			               pim_articles.vat_id, pim_articles.article_category_id, pim_articles.article_brand_id, pim_articles.internal_name,  pim_articles.d_price,  pim_articles.h_price,  pim_article_prices.price,  pim_article_prices.purchase_price
		                   
      FROM pim_articles	
      LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'	
			WHERE $filter 
			GROUP BY pim_articles.article_id
			ORDER BY  pim_articles.item_code 
			");


/*	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', "SERVICE CODE")
            ->setCellValue('B1',"SERVICE NAME")
            ->setCellValue('C1',"UNIT PRICE")
            ->setCellValue('D1',"PURCHASE PRICE")
            ->setCellValue('E1',"VAT %")
            ->setCellValue('F1',"CATEGORY");	*/	
    $headers = array('SERVICE CODE',
                    'SERVICE NAME',                
                    'UNIT PRICE',
                    "PURCHASE PRICE",
                    "VAT %",
                    "CATEGORY"
      );

$final_data=array();

 $xlsRow=2;

while ($info->next())
{

  $vat = $db->field("SELECT value FROM vats WHERE vat_id='".$info->f('vat_id')."'");
	$article_category = $db->field("SELECT name  FROM pim_article_categories WHERE id='".$info->f('article_category_id')."'");
 
	/* $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A'.$xlsRow, ' '.$info->f('item_code'))
      ->setCellValue('B'.$xlsRow, preg_replace("/[\n\r]/","",$info->f('internal_name')))
      ->setCellValue('C'.$xlsRow, display_number($info->f('price')))
      ->setCellValue('D'.$xlsRow, display_number($info->f('purchase_price')))
      ->setCellValue('E'.$xlsRow, display_number($vat))
      ->setCellValue('F'.$xlsRow, $article_category);	*/	
		
     $tmp_item=array(
        $info->f('item_code'),
        preg_replace("/[\n\r]/","",$info->f('internal_name')),
        display_number_exclude_thousand($info->f('price')),
        display_number_exclude_thousand($info->f('purchase_price')),
        display_number_exclude_thousand($vat),
        $article_category
     );		


  array_push($final_data,$tmp_item); 
	
	$xlsRow++;
}




if(!file_exists('upload/'.DATABASE_NAME)){
  if(!mkdir('upload/'.DATABASE_NAME)) {
    die('Failed');exit;
  }
}


header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$from_location='upload/'.DATABASE_NAME.'/service_export_'.$_SESSION['u_id'].'_'.time().'.csv';
$fp = fopen("php://output", 'w');

fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
unlink($from_location);
exit();



$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

$decimals='0';
$format_code = '0.';
for ($q=0; $q < ARTICLE_PRICE_COMMA_DIGITS; $q++) { 
  $format_code= $format_code.'0';
}

$rows_format='N2:O'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode($format_code);

$rows_format='P2:P'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');
    
$objPHPExcel->getActiveSheet()->setTitle('Articles export');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
$objWriter->setDelimiter(';');
// $objWriter->setEnclosure('"');
$objWriter->save('../upload/'.$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',
  
  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('../upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name."); 
}
// file size in bytes
$fsize = filesize($file_path); 
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type."); 
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);  
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
setcookie('Akti-Export','9',time()+3600,'/');
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>