<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '2000M');

setcookie('Akti-Export','6',time()+3600,'/');
//require_once 'libraries/PHPExcel.php';
ark::loadLibraries(array('PHPExcel'));
$filename ="article_export.xls";

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Articles report")
               ->setSubject("Articles report")
               ->setDescription("Articles export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Articles");*/
$db = new sqldb();
$db2 = new sqldb();
$db3 = new sqldb();

$filter = '1=1';
$filter_attach = '';
$filter.= " AND pim_articles.is_service='0' ";

if(!$in['archived']){
  $filter.= " AND pim_articles.active='1' ";
 

}else{
  $filter.= " AND pim_articles.active='0' ";
 
}


if(!empty($in['search'])){
  $filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
 
}
if($in['attach_to_product']){
   if($in['attach_to_product']==1){
    $filter_attach.= " INNER JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
   }else{
    $filter_attach.= " LEFT JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
    $filter.=" AND pim_product_article.article_id is null";
   }    
  
}
if($in['is_picture']){
   if($in['is_picture']==1){
    $filter_attach.= " INNER JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
   }else{
    $filter_attach.= " LEFT JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
    $filter.=" AND pim_article_photo.parent_id is null";
   }    
  $arguments.="&is_picture=".$in['is_picture'];
}

if($in['article_category_id']){ 
  $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";  
  
}
if($in['first'] && $in['first'] == '[0-9]'){
  $filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
  
} elseif ($in['first']){
  $filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
  
}
if($in['show_stock']){ 
  $filter.=" AND pim_articles.hide_stock='0' ";  
  
}
if(!empty($in['supplier_reference'])){
  $filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
}

if($in['supplier_id'] && !empty($in['supplier_id'])){
    $filter .=" AND pim_articles.supplier_id='".$in['supplier_id']."' ";
}

$headers = array('ARTICLE ID',
                    'ITEM CODE',                
                    'INTERNAL NAME',
                    "NAME (EN)",
                    "NAME (FR)",
                    "NAME (DU)",
                    "NAME (GE)",
                    "STOCK",
                    "ORIGIN CODE",
                    "EAN CODE",
                    "CATEGORY",
                    "BRAND",
                    "PACKING",
                    "SALE UNIT",
                    "BASE PRICE",
                    "PURCHASE PRICE",
                    "VAT PERCENT",
                    "DESCRIPTION (EN)",
                    "DESCRIPTION (FR)",
                    "DESCRIPTION (DU)",
                    "DESCRIPTION (GE)",
                    //"BLOCKED",
                    "SUPPLIER",
                    "SUPPLIER REFERENCE",
                    //"ADDITIONAL PRICE",
                    "LEDGER ACCOUNT"
      );

$price_categories=$db->query("SELECT * FROM pim_article_price_category ORDER BY name ASC")->getAll();
foreach($price_categories as $category){
  $headers[]=$category['name'];
}

$info = $db->query("SELECT pim_articles.supplier_name,pim_articles.hide_stock,pim_articles.article_id,pim_articles.front_active,pim_articles.origin_number,pim_articles.ean_code,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,
                     pim_articles.vat_id, pim_articles.article_category_id, pim_articles.article_brand_id, pim_articles.supplier_reference, pim_articles.aac_price,  
                         pim_article_prices.base_price,pim_articles.internal_name,pim_article_prices.price,pim_article_prices.purchase_price,pim_articles.packing,pim_articles.sale_unit,
                    lang_name_en.name AS name_lang_en,lang_name_fr.name AS name_lang_fr,lang_name_du.name AS name_lang_du,lang_name_de.name AS name_lang_de,
                    lang_name_en.description AS description_lang_en,lang_name_fr.description AS description_lang_fr,lang_name_du.description AS description_lang_du,lang_name_de.description AS description_lang_de,vats.value as vat_name,pim_article_categories.name as cat_name,pim_article_brands.name as brand_name,
                    pim_article_ledger_accounts.name AS ledger_account_name
            FROM pim_articles
            LEFT JOIN pim_articles_lang AS lang_name_en ON lang_name_en.item_id=pim_articles.article_id AND lang_name_en.lang_id='1'
            LEFT JOIN pim_articles_lang AS lang_name_fr ON lang_name_fr.item_id=pim_articles.article_id AND lang_name_fr.lang_id='2'
            LEFT JOIN pim_articles_lang AS lang_name_du ON lang_name_du.item_id=pim_articles.article_id AND lang_name_du.lang_id='3'
            LEFT JOIN pim_articles_lang AS lang_name_de ON lang_name_de.item_id=pim_articles.article_id AND lang_name_de.lang_id='4'
            LEFT JOIN vats ON pim_articles.vat_id=vats.vat_id
            LEFT JOIN pim_article_categories ON pim_articles.article_category_id=pim_article_categories.id  
            LEFT JOIN pim_article_brands ON pim_articles.article_brand_id=pim_article_brands.id
      $filter_attach
      LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price=1
      LEFT JOIN pim_article_ledger_accounts ON pim_articles.ledger_account_id=pim_article_ledger_accounts.id
      WHERE $filter 
      GROUP BY pim_articles.article_id
      ORDER BY  pim_articles.item_code 
      ");

/*$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', "ITEM CODE")
      ->setCellValue('B1',"INTERNAL NAME")
      ->setCellValue('C1',"NAME (EN)")
      ->setCellValue('D1',"NAME (FR)")
      ->setCellValue('E1',"NAME (DU)")
      ->setCellValue('F1',"NAME (GE)")
      
      ->setCellValue('G1',"STOCK")
      ->setCellValue('H1',"ORIGIN NUMBER")
      ->setCellValue('I1',"EAN CODE")
      ->setCellValue('J1',"FAMILY")
      ->setCellValue('K1',"BRAND")
      ->setCellValue('L1',"PACKING")
      ->setCellValue('M1',"SALE UNIT")
      ->setCellValue('N1',"BASE PRICE")
      ->setCellValue('O1',"PURCHASE PRICE")
      
      ->setCellValue('P1',"VAT PERCENT")
      ->setCellValue('Q1',"DESCRIPTION (EN)")
      ->setCellValue('R1',"DESCRIPTION (FR)")
      ->setCellValue('S1',"DESCRIPTION (DU)")
      ->setCellValue('T1',"DESCRIPTION (GE)")
      ->setCellValue('U1',"BLOCKED")
      ->setCellValue('V1',"SUPPLIER")
      ->setCellValue('W1',"SUPPLIER REFERENCE")
      ->setCellValue('X1',"ADDITIONAL PRICE");*/
      
$final_data=array();

$xlsRow=2;
while ($info->next())
{
    /*if($info->f('front_active')==1){
        $blocked=0;
    }else{
        $blocked=1;
    }

  $info_en=$db->query("SELECT name,description FROM  pim_articles_lang WHERE item_id='".$info->f('article_id')."' AND lang_id=1");
   
   $info_fr=$db->query("SELECT name,description FROM  pim_articles_lang WHERE item_id='".$info->f('article_id')."' AND lang_id=2");
   $info_du=$db->query("SELECT name,description FROM  pim_articles_lang WHERE item_id='".$info->f('article_id')."' AND lang_id=3");
   $info_ge=$db->query("SELECT name,description FROM  pim_articles_lang WHERE item_id='".$info->f('article_id')."' AND lang_id=4");
   $vat=$db->field("SELECT value from vats WHERE vat_id='".$info->f('vat_id')."'"); 
   $article_category = $db->field("SELECT name  FROM pim_article_categories WHERE id='".$info->f('article_category_id')."'");
   $article_brand = $db->field("SELECT name  FROM pim_article_brands WHERE id='".$info->f('article_brand_id')."'");  

    if (defined('AAC') && AAC==1) {
      $aac_price = $info->f('aac_price');
    }else {
      $aac_price =0;
    }

$objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A'.$xlsRow, ''.$info->f('item_code'))
      ->setCellValue('B'.$xlsRow, html_entity_decode($info->f('internal_name')))
      ->setCellValue('C'.$xlsRow, $info_en->f('name'))
      ->setCellValue('D'.$xlsRow, $info_fr->f('name'))
      ->setCellValue('E'.$xlsRow, $info_du->f('name'))
      ->setCellValue('F'.$xlsRow, $info_ge->f('name'))
      
      ->setCellValue('G'.$xlsRow, $info->f('hide_stock')?gm('No Stock'):$info->f('stock'))
      ->setCellValue('H'.$xlsRow, $info->f('origin_number').'')
      ->setCellValue('I'.$xlsRow, $info->f('ean_code').'')
      ->setCellValue('J'.$xlsRow, $article_category)
      ->setCellValue('K'.$xlsRow, $article_brand)
      ->setCellValue('L'.$xlsRow, remove_zero_decimals($info->f('packing')))
      ->setCellValue('M'.$xlsRow, $info->f('sale_unit'))
      ->setCellValue('N'.$xlsRow, $info->f('price'))
      ->setCellValue('O'.$xlsRow, $info->f('purchase_price'))
      
      ->setCellValue('P'.$xlsRow, $vat)      
      ->setCellValue('Q'.$xlsRow, $info_en->f('description'))
      ->setCellValue('R'.$xlsRow, $info_fr->f('description'))
      ->setCellValue('S'.$xlsRow, $info_du->f('description'))
      ->setCellValue('T'.$xlsRow, $info_ge->f('description'))
      ->setCellValue('U'.$xlsRow, $blocked)
       ->setCellValue('V'.$xlsRow, $info->f('supplier_name'))
       ->setCellValue('W'.$xlsRow, $info->f('supplier_reference'))
       ->setCellValue('X'.$xlsRow, $aac_price);*/

       $tmp_prices=[];
      $prices_list=$db->query("SELECT pim_article_price_category.*,pim_article_prices.price,pim_article_prices.total_price
        FROM pim_article_price_category 
        RIGHT JOIN pim_article_prices ON pim_article_price_category.category_id=pim_article_prices.price_category_id
        WHERE pim_article_prices.article_id = '".$info->f('article_id')."' AND base_price!=1 ORDER BY pim_article_price_category.name ASC");

      while ($prices_list->move_next()) {
        if(!$prices_list->f('category_id')){
          continue;
        }
        $db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE category_id='".$prices_list->f('category_id')."' AND article_id='".$info->f('article_id')."'");
        if($db2->move_next()){
            $is_custom=true;
                    
            $type = $db2->f('type'); 
            $price_value = $db2->f('price_value'); 
            $price_value_type = $db2->f('price_value_type');       
        }else{
            $is_custom=false;

            $type = $prices_list->f('type'); 
         
            $price_value_custom_fam=$db3->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$info->f('article_category_id')."' AND category_id='".$prices_list->f('category_id')."'");
            if($price_value_custom_fam==NULL){
                $price_value = $prices_list->f('price_value'); 
                $custom_fam=0;
             }else{
                $price_value = $price_value_custom_fam; 
                $custom_fam=1;
             }

            $price_value_type = $prices_list->f('price_value_type');   
        }
        if($custom_fam==0){
          $price = $prices_list->f('price');  
        }else{
          //we have to apply to the base price the category spec
           $cat_price_type=$db3->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$prices_list->f('category_id')."'");
           $cat_type=$db3->field("SELECT type FROM pim_article_price_category WHERE category_id='".$prices_list->f('category_id')."'");
           $price_value_type=$db3->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$prices_list->f('category_id')."'");
           
           if($cat_price_type==2){
               $article_base_price=get_article_calc_price($info->f('article_id'),3);
             }else{
               $article_base_price=get_article_calc_price($info->f('article_id'),1);
             }

            switch ($cat_type) {
            case 1:                  //discount
              if($price_value_type==1){  // %
                $price = $article_base_price - $price_value * $article_base_price / 100;
              }else{ //fix
                $price = $article_base_price - $price_value;
              }
              break;
            case 2:                 //profit margin
              if($price_value_type==1){  // %
                $price = $article_base_price + $price_value * $article_base_price / 100;
              }else{ //fix
                $price =$article_base_price + $price_value;
              }
              break;
          }
        }
        $price_row = array(
          'price'                => $price,
        );
        array_push($tmp_prices, $price_row);
     }

      $tmp_item=array(
        $info->f('article_id'),
        $info->f('item_code'),
        html_entity_decode(preg_replace("/[\n\r]/","",$info->f('internal_name'))),
        preg_replace("/[\n\r]/","",$info->f('name_lang_en')),
        preg_replace("/[\n\r]/","",$info->f('name_lang_fr')),
        preg_replace("/[\n\r]/","",$info->f('name_lang_du')),
        preg_replace("/[\n\r]/","",$info->f('name_lang_de')),
        ($info->f('hide_stock')?gm('No Stock'):$info->f('stock')),
        preg_replace("/[\n\r]/","",$info->f('origin_number')).'',
        preg_replace("/[\n\r]/","",$info->f('ean_code')).'',
        preg_replace("/[\n\r]/","",$info->f('cat_name')),
        $info->f('brand_name'),
        remove_zero_decimals($info->f('packing')),
        $info->f('sale_unit'),
        $info->f('price'),
        $info->f('purchase_price'),
        $info->f('vat_name'),
        preg_replace("/[\n\r]/","",$info->f('description_lang_en')),
        preg_replace("/[\n\r]/","",$info->f('description_lang_fr')),
        preg_replace("/[\n\r]/","",$info->f('description_lang_du')),
        preg_replace("/[\n\r]/","",$info->f('description_lang_de')),
        //($info->f('front_active')==1 ? 0 : 1),
        $info->f('supplier_name'),
        $info->f('supplier_reference'),
        //(defined('AAC') && AAC=='1' ? $info->f('aac_price') : 0),
        ($info->f('ledger_account_name') ? $info->f('ledger_account_name') : '')
      );
      foreach($tmp_prices as $price_list_row){
        $tmp_item[]=$price_list_row['price'];
      }
      array_push($final_data,$tmp_item); 
  $xlsRow++;
}
if(!file_exists('upload/'.DATABASE_NAME)){
  if(!mkdir('upload/'.DATABASE_NAME)) {
    die('Failed');exit;
  }
}

$from_location='upload/'.DATABASE_NAME.'/article_export_'.$_SESSION['u_id'].'_'.time().'.csv';
$fp = fopen($from_location, 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
$objReader = PHPExcel_IOFactory::createReader('CSV');

$objPHPExcel = $objReader->load($from_location);

$rows_format='A1:K'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

// set column width to auto
foreach(range('A','B') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}
foreach(range('G','K') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

unlink($from_location);
$objWriter->save("php://output");
doQueryLog();
exit();


$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);


$decimals='0';
$format_code = '0.';
for ($q=0; $q < ARTICLE_PRICE_COMMA_DIGITS; $q++) { 
  $format_code= $format_code.'0';
}

$rows_format='N2:P'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode($format_code);

$rows_format='Q2:Q'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');

$objPHPExcel->getActiveSheet()->setTitle('Articles export');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');                                                                               
$objWriter->save('../upload/'.$filename);


define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',
  
  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('../upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name."); 
}
// file size in bytes
$fsize = filesize($file_path); 
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type."); 
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);  
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
setcookie('Akti-Export','9',time()+3600,'/');
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>