<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');

setcookie('Akti-Export','6',time()+3600,'/');
$filename ="article_custom_price_export.csv";

$db = new sqldb();

$filter = '1=1';
$filter_attach = '';
$filter.= " AND pim_articles.is_service='0' ";

if(!$in['archived']){
  $filter.= " AND pim_articles.active='1' ";
 

}else{
  $filter.= " AND pim_articles.active='0' ";
 
}

if(!empty($in['search'])){
  $filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
 
}

if($in['article_category_id']){ 
  $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";  
  
}
if($in['first'] && $in['first'] == '[0-9]'){
  $filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
  
} elseif ($in['first']){
  $filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
  
}

if(!empty($in['supplier_reference'])){
  $filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
}

if($in['supplier_id'] && !empty($in['supplier_id'])){
    $filter .=" AND pim_articles.supplier_id='".$in['supplier_id']."' ";
}

$headers = array('ITEM CODE',                
                    'INTERNAL NAME',
                    "BASE PRICE",
                    "PURCHASE PRICE",
                    "CUSTOMER NAME",
                    "CUSTOM PRICE"
      );

$info = $db->query("SELECT customer_custom_article_price.*, pim_articles.price AS p_price,pim_articles.item_code,pim_articles.internal_name,pim_article_prices.purchase_price,customers.name AS customer_name FROM customer_custom_article_price
                  LEFT JOIN pim_articles ON pim_articles.article_id=customer_custom_article_price.article_id
                  LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price=1
                  INNER JOIN customers ON customers.customer_id=customer_custom_article_price.customer_id
                  WHERE $filter AND customers.is_admin='0' AND customers.front_register='0' AND customers.active='1' ORDER BY pim_articles.item_code ");

  $final_data=array();

while ($info->next())
{
       $tmp_item=array(
        $info->f('item_code'),
        html_entity_decode(preg_replace("/[\n\r]/","",stripslashes($info->f('internal_name')))),
        display_number_exclude_thousand($info->f('p_price')),
        display_number_exclude_thousand($info->f('purchase_price')),
        $info->f('customer_name'),
        display_number_exclude_thousand($info->f('price'))
      );
      array_push($final_data,$tmp_item); 
}

if(!file_exists('upload/'.DATABASE_NAME)){
  if(!mkdir('upload/'.DATABASE_NAME)) {
    die('Failed');exit;
  }
}


header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$from_location='upload/'.DATABASE_NAME.'/article_custom_price_export_'.$_SESSION['u_id'].'_'.time().'.csv';
$fp = fopen("php://output", 'w');

fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
unlink($from_location);
exit();

?>