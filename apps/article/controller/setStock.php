<?php
$db2=new sqldb();
$is_data=false;
$i==0;
$result = array('query'=>array());

$stock_reasons_data = $db->query("SELECT * FROM stock_edit_reasons WHERE article_id = '".$in['article_id']."' ORDER BY id DESC")->getAll();

$stock_edit_reasons = array();
$require_reason_stock_modif = $db->field("SELECT value FROM settings WHERE constant_name='REQUIRE_REASON_MANUAL_STOCK_MODIF'");


foreach ($stock_reasons_data as $key=>$val) {
    $stock_edit_reasons[$val['address_id']][$key] = array('id' => $val['id'],
                                              'article_id' => $val['article_id'],
                                              'address_id' => $val['address_id'],
                                              'user_id' => $val['user_id'],
                                              'username' => stripslashes($val['username']),
                                              'edit_reason' => stripslashes($val['edit_reason']),
                                              'date'  => date(ACCOUNT_DATE_FORMAT,  $val['date'])
                                              );
}

$db->query("SELECT  dispatch_stock_address.* ,dispatch_stock.stock,dispatch_stock.article_id
            FROM  dispatch_stock_address 
            LEFT JOIN dispatch_stock ON dispatch_stock.address_id=dispatch_stock_address.address_id 
            AND dispatch_stock.article_id='".$in['article_id']."' and dispatch_stock.main_address=1
            ORDER BY dispatch_stock_address.naming ASC");

while($db->move_next()) {
    $item=array(
                     'address' => stripslashes($db->f('address')),
                     'zip'     => $db->f('zip'),
                     'city'    => $db->f('city'),
                     'country'     => get_country_name($db->f('country_id')),
                     'naming'      => $db->f('naming'),
                     'address_id'  => $db->f('address_id'),
                     'stock'  => remove_zero_decimals($db->f('stock')),
                     'old_stock' => remove_zero_decimals($db->f('stock')),
                     'edit_reason_stock_id' => undefined,
                     'require_reason_stock_modif' =>$require_reason_stock_modif == '1' ? true : false,
      ); 

      $stock_address_edit_reason = array();

      foreach ($stock_edit_reasons as $key => $value) {
          if($key == $db->f('address_id')){
             $item['edit_reasons'] = $value; 
          }
      }

       array_push($result['query'], $item);
       $i++;
}

$extern_dispatch=$db->field("SELECT sum(stock) FROM dispatch_stock WHERE article_id='".$in['article_id']."' and customer_id!=0");

if($i>0){
  $is_data=1;
}
$result['is_data']=$is_data;
$result['extern_dispatch']=remove_zero_decimals($extern_dispatch);
$result['article_id']=$in['article_id'];
$result['account_number_format']=ACCOUNT_NUMBER_FORMAT;
$result['nr_decimals']=ARTICLE_PRICE_COMMA_DIGITS;
$result['style']=ACCOUNT_NUMBER_FORMAT;
$result['from_stock_tab']=$in['from_stock_tab']?true:false;
$result['view_mode'] = $in['view_mode'] ? true : false;
$result['edit_reason_stock_dd'] = build_stock_edit_reason_dd();

json_out($result);