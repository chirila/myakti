<?php
$db2=new sqldb();

$result = array('info'=>array());

if(!$in['category_id']){
   
    $result['page_title']='Add Article Price Category';
    $result['do_next']='article-article_price_category-article_price_category-add';
    $result['price_type_dd']=build_price_type($in['price_type']);
    $result['category_type_dd']=build_price_category_type($in['type']);
    $result['price_value_type_dd']=build_price_value_type_list($in['price_value_type']);
    $result['price_value']=display_number($in['price_value']);
    $result['is_regular']=true;
    $result['edit_mode']=false;
    
   
  
} else {
  if(!$in['article_id']){
     $result['page_title']='Edit Article Price Category';
     $result['do_next']='article-article_price_category-article_price_category-update';
    
     
  
    $db->query("SELECT pim_article_price_category.* FROM pim_article_price_category WHERE pim_article_price_category.category_id='".$in['category_id']."'  ");
    $db->move_next(); 
  
   
    $result['name']=$db->gf('name');
    $result['price_value']=display_number($db->gf('price_value'));

  
    $result['price_type']=$db->gf('price_type'); 
    $result['price_type_dd']=build_price_type($db->gf('price_type'));
    $result['category_type_dd']=build_price_category_type($db->gf('type') );
    $result['price_value_type_dd']=build_price_value_type_list($db->gf('price_value_type'));
    $result['is_regular']=true;
   
    $use_fam_custom_price = $db->query("SELECT * FROM settings WHERE constant_name='USE_FAM_CUSTOM_PRICE'");
        $use_fam_custom_price->next();
      $result['edit_mode']=true;
      $result['use_fam_custom_price']=$use_fam_custom_price->f('value');
       
   
   }else{ //custom price for article
        $page_title = '{l}Custom Price Category{endl}';
        $view->assign('do_next','article-article_price_category-article_price_category-update_for_article');
        $db->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE pim_article_price_category_custom.category_id='".$in['category_id']."' AND article_id='".$in['article_id']."'");
    if($db->move_next()){
     
      $result['price_type']=$db->gf('price_type'); 
      $result['price_value_type']=$db->gf('price_value_type');
      $result['category_type']=$db->gf('type');
      $result['price_value']=display_number($db->gf('price_value'));
      
      $result['price_type_dd']=build_price_type($db->gf('price_type'));
      $result['price_value_type_dd']=build_price_value_type_list($db->gf('price_value_type'));
      $result['category_type_dd']=build_price_category_type($db->gf('type'));

    }else{  
        $db->query("SELECT pim_article_price_category.* FROM pim_article_price_category WHERE pim_article_price_category.category_id='".$in['category_id']."'");
       $db->move_next(); 
      

      $result['price_type']=$db->gf('price_type'); 
      $result['price_value_type']=$db->gf('price_value_type');
      $result['category_type']=$db->gf('type');
      $result['price_value']=display_number($db->gf('price_value'));
      
      $result['price_type_dd']=build_price_type($db->gf('price_type'));
      $result['price_value_type_dd']=build_price_value_type_list($db->gf('price_value_type'));
      $result['category_type_dd']=build_price_category_type($db->gf('type'));


    
    
    }
     $result['is_regular']=false;
  


   }


    
}



$result['category_id']=$in['category_id'];
$result['article_id']=$in['article_id'];
//$result['price_categories']=$in['price_categories'];

$result['account_number_format']=ACCOUNT_NUMBER_FORMAT;
$result['nr_decimals']=ARTICLE_PRICE_COMMA_DIGITS;
$result['style']=ACCOUNT_NUMBER_FORMAT;



json_out($result);