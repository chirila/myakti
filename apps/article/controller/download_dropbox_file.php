<?php

/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$db = new sqldb();

if($in['is_batch']){
	if(!$in['path'] || !$in['for'] || !$in['file']){
			exit();
		}
		$d = new drop($in['for'],null,$in['id'],true,'',null,null,null, true);
}else{
		if((!$in['customer_id'] && !$in['article_id']) || !$in['path'] || !$in['for'] || !$in['file']){
			exit();
		}
		if($in['customer_id']){
		  $d = new drop($in['for'],$in['customer_id'],$in['id'],true,'',$in['isConcact'],$in['serial_number']);
        }
        if($in['article_id']){
		  $d = new drop($in['for'],$in['article_id'],$in['id'],true,'',$in['isConcact'],$in['serial_number']);
        }
}

$outFile = false;

$path = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/dropbox_files/';
if(!file_exists($path)){
	mkdir($path,0775,true);
}

$f = fopen($path.$in['file'], 'w+b');

// Download the file
$file = $d->getFile($in['path'], $f);
fclose($f);

doQueryLog();

header("Content-Disposition: attachment; filename=\"".$in['file']."\"");
header("Content-type: application/".$in['mime_type']);

// Dump the output
echo file_get_contents($path.$in['file']);
unlink($path.$in['file']);
exit();
?>
