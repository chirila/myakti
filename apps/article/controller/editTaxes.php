<?php

if($in['tax_id'] == 'tmp'){
    $do_next ='article-setTaxes-article-add_tax';
    $is_add = true;
    $page_title = gm('Add tax');
    $tax_type_dd = get_article_tax_type_dd();
    $vat_dd = build_vat_dd();
    $apply_to_dd = build_article_tax_application_type_dd();
}else{
    $do_next ='article-setTaxes-article-update_tax';
    $is_add = false;
    $page_title = gm('Edit tax');
    $i==0;
    
    $db->query("SELECT pim_article_tax.*, vats.value, pim_article_tax_type.name
            FROM pim_article_tax        
            LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
            LEFT JOIN vats ON vats.vat_id = pim_article_tax.vat_id
            WHERE pim_article_tax.tax_id='".$in['tax_id']."'
           ");
    
    while ($db->move_next()) {
        $tax_type_dd = get_article_tax_type_dd($db->f('tax_id'));
        $vat_dd = build_vat_dd($db->f('vat_id'));
        $apply_to_dd = build_article_tax_application_type_dd($db->f('apply_to')+1);

       $result['item'] =array(  
                     'tax_id'=> $db->f('tax_id'),
                     'code'  => $db->f('code'),
                     'amount'=>display_number($db->f('amount')),
                     'description' => $db->f('description'),
                     'tax'   => $in['tax_id'][$db->f('tax_id')] == 1?true :false,
                     'type'  => $db->f('name'),
                     'type_id'  => $db->f('type_id'),
                     'tax_vat'=> display_number($db->f('value')).' %',
                     'tax_vat_id'=> $db->f('vat_id'),
                     'apply_to_id'=> $db->f('apply_to')+1,

                     
                 );
        $i++;
    }
   
}

$result['apply_to_dd']= $apply_to_dd;
$result['tax_type_dd']= $tax_type_dd;
$result['vat_dd']= $vat_dd;
$result['do_next']=$do_next;
$result['is_add']=$is_add;
$result['tax_id']=$in['tax_id'];
$result['style']=ACCOUNT_NUMBER_FORMAT;
$result['page_title']=$page_title;

json_out($result);