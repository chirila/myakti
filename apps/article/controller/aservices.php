<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if (isset($_SESSION['tmp_add_service_to_archive'])) {
    unset($_SESSION['tmp_add_service_to_archive']);
}
if($in['reset_list']){
    if(isset($_SESSION['add_service_to_archive'])){
        unset($_SESSION['add_service_to_archive']);
    }
}

$db = new sqldb();
$db2 = new sqldb();

$l_r =ROW_PER_PAGE;
$order_by = " ORDER BY  pim_articles.item_code  ";

$order_by_array = array('item_code','internal_name','article_category','price','customer_name','block_discount');

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

//FILTER LIST
$selected =array();
$filter = '1=1';
$filter_attach = '';
$filter.= " AND pim_articles.is_service='1' ";

if(!$in['archived']){
	$filter.= " AND pim_articles.active='1' ";
}else{
	$filter.= " AND pim_articles.active='0' ";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}


if(!empty($in['search'])){
	$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_articles.origin_number LIKE '%".$in['search']."%') ";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['name'])){
	$filter .= " AND ( pim_articles.supplier_name LIKE '%".$in['name']."%') ";
	$arguments_s.="&name=".$in['name'];
}
if(!empty($in['supplier_reference'])){
	$filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
	$arguments_s.="&supplier_reference=".$in['supplier_reference'];
}

# this parts is obsolete
if($in['attach_to_product']){
	 if($in['attach_to_product']==1){
	 	$filter_attach.= " INNER JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
	 }else{
	 	$filter_attach.= " LEFT JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
	 	$filter.=" AND pim_product_article.article_id is null";
	 }
	$arguments.="&attach_to_product=".$in['attach_to_product'];
}
# end
if($in['is_picture']){
	 if($in['is_picture']==1){
	 	// $filter_attach.= " INNER JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
	 	$filter.=" AND pim_articles.parent_id <> 0";
	 }else{
	 	// $filter_attach.= " LEFT JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
	 	$filter.=" AND pim_articles.parent_id = 0";
	 }
	$arguments.="&is_picture=".$in['is_picture'];
}

if($in['article_category_id']){
	$filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
	$arguments.="&article_category_id=".$in['article_category_id'];
}
if($in['first'] && $in['first'] == '[0-9]'){
	$filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
	$arguments.="&first=".$in['first'];
} elseif ($in['first']){
	$filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
	$arguments.="&first=".$in['first'];
}

if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc']=='1' || $in['desc']=='true'){
			$order = " DESC ";
		}
		if($in['order_by']=='customer_name'){
			$in['order_by']='name';
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		/*$view_list->assign(array(
			'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
		));*/
	}
}


$arguments = $arguments.$arguments_s;
if($in['order_by']=='name'){

}elseif($in['name']){

}else{
$internal_name = $db->query("SELECT DISTINCT SUBSTR( internal_name, 1, 1 ) AS internal_name
            FROM pim_articles
			WHERE $filter
		     ".$order_by."
			")->getAll();
}
foreach ($internal_name as $key => $value) {
	array_push($selected, $value['internal_name']);
}
$internal_name =null;
/*if($in['order_by']=='name'){

}elseif($in['name']){

}else{*/
	$e = $db->query("SELECT pim_articles.article_id FROM pim_articles
			WHERE $filter ".$order_by)->getAll();
//}
$result = array('query'=>array(),'max_rows'=>'', 'nav'=>array());

$result['nav']= $e;
$max_rows=count($e);
$_SESSION['articles_id']=array();
foreach ($e as $key => $value) {
	array_push($_SESSION['articles_id'],$value['article_id']);
}

$nav = array();
if(!$_SESSION['tmp_add_service_to_archive'] || ($_SESSION['tmp_add_service_to_archive'] && empty($_SESSION['tmp_add_service_to_archive']))) {
    foreach ($e as $key => $value) {
        foreach ($value as $key2 => $value2) {
            $_SESSION['tmp_add_service_to_archive'][$value2] = 1;
        array_push($nav, (object)['article_id'=> $value2 ]);
        }
    }
}else{
    foreach($e as $key => $value) {
        foreach($value as $key2 => $value2) {
            array_push($nav, (object)['article_id'=> $value2 ]);
        }
    }
}
$result['nav'] =  $nav;

$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_service_to_archive']){
    if($max_rows && count($_SESSION['add_service_to_archive']) == $max_rows){
        $all_pages_selected=true;
    }else if(count($_SESSION['add_service_to_archive'])){
        $minimum_selected=true;
    }
}

$e = null;
$result['max_rows']= $max_rows;
$result['all_pages_selected'] = $all_pages_selected;
$result['minimum_selected'] = $minimum_selected;

if(!isset($in['lang_id'])){
  $in['lang_id'] = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1' GROUP BY lang_id ");
}
if($in['lang_id']){
	$in['lang_id']=1;
}
$articles = $db->query("SELECT pim_articles.article_id,pim_articles.hide_stock,pim_articles.block_discount,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,customers.name,
			                   pim_articles.internal_name,pim_articles.price,pim_articles.article_category,
			                   vats.value AS vat_value ,pim_articles_lang.description AS description,pim_articles.ean_code,pim_article_categories.name AS article_category_name

            FROM pim_articles
            LEFT JOIN customers ON pim_articles.supplier_id = customers.customer_id
            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
			WHERE $filter
		   ".$order_by."
			LIMIT ".$offset*$l_r.",".$l_r."
			");

$j=0;
while($db->move_next()){
	     if(!$db->f('article'))

    

     
	$item =array(
				'vat_value'		  	=> $db->f('vat_value'),
				'description'       =>  $db->f('description'),
				'ean_code'      =>  $db->f('ean_code'),
				'category_name' =>  $db->f('article_category_name'),
				'name'		  		=> $db->f('internal_name'),
				'customer_name'		=> $db->f('name'),
				'article_category'	=> $db->f('article_category'),
				'article_id'  		=> $db->f('article_id'),
				'stock'  			=> remove_zero_decimals($db->f('stock')),
				'show_stock'  		=> $db->f('hide_stock') ? false:true,
			    'block_discount'    => $db->f('block_discount')? true:false,
				'base_price'  		=> display_number_var_dec($db->f('price')),
				// 'vat_value'	  => $db->f('vat_value'),
				'check_show_front'	=> $db->f('show_front')==1? 'checked="checked"':'',
				'code'		  		=> $db->f('item_code'),
				'duplicate_link'	=> $in['archived'] ? 'index.php?do=article-articles_list-article-activate&article_id='.$db->f('article_id').$arguments : 'index.php?do=article-article&duplicate_article_id='.$db->f('article_id'),
				//'edit_link'	  		=> 'index.php?do=article-article&article_id='.$db->f('article_id'),
				//'delete_link' 		=> $in['archived'] ? 'index.php?do=article-articles_list-article-delete&article_id='.$db->f('article_id').$arguments : 'index.php?do=article-articles_list-article-archive&article_id='.$db->f('article_id').$arguments,
				'title'				=> $in['archived'] ? gm('Delete') : gm('Archive'),
				'confirm'			=> $in['archived'] ? gm('Delete this article?') : gm('Archive this article?'),
				'd_confirm'			=> $in['archived'] ? gm('Activate this article?') : '',
				'd_title'			=> $in['archived'] ? gm('Activate') : gm('Duplicate'),
				'd_class'			=> $in['archived'] ? 'art_del undo' : 'matching_company',
				'd_img'				=> $in['archived'] ? 'undo' : 'duplicate',
				'allow_stock'         			=> ALLOW_STOCK == '0' ? false:true,
				'stock_multiple_locations'      => STOCK_MULTIPLE_LOCATIONS,
				//'stock_multiple_locations'=> false,
				'confirm'			=>    gm('Confirm'),
				'ok'				=>    gm('Ok'),
				'cancel'			=> 	  gm('Cancel'),
				'adv_product'         			=> ADV_PRODUCT == '0' ? false:true,
				'is_archived'      	=> $in['archived'] ? true: false,
				'archive_link' 		 	=> array('do'=>'article-aservices-service-archive', 'article_id'=>$db->f('article_id')),
				'undo_link' 		 	=> array('do'=>'article-aservices-service-activate', 'article_id'=>$db->f('article_id')),
				'delete_link' 		 	=> array('do'=>'article-aservices-service-delete', 'article_id'=>$db->f('article_id')),
				'edit_link' 		 	=> array('do'=>'article-add', 'article_id'=>$db->f('article_id'),'form_type'=>'service'),
                'check_add_to_product'  => $_SESSION['add_service_to_archive'][$db->f('article_id')] == 1 ? true : false,
                'id'                    => $db->f('article_id'),
    );
     if($db->f('stock') <= 0){
     	   $item['stock_status']='text-danger';
          
     }elseif($db->f('stock') > 0  && $db->f('stock') <= $db->f('article_threshold_value') ){
     	   $item['stock_status']='text-warning';
     }else{
     	   $item['stock_status']='text-success';

     }



    array_push($result['query'], $item);
	$j++;
}


$result['allow_stock']=ALLOW_STOCK == '0'?false:true;
if(ALLOW_STOCK && ADV_PRODUCT ){
	$result['code_width']='col-sm-2  col-md-2';
	$result['fam_width']='col-sm-2  col-md-2';
	$result['cust_width']='col-sm-2  col-md-2';
}else if(!ALLOW_STOCK && ADV_PRODUCT){
	$result['code_width']='col-sm-2  col-md-2';
	$result['fam_width']='col-sm-2  col-md-2';
	$result['cust_width']='col-sm-2  col-md-2';
}else{
	$result['code_width']='col-sm-3  col-md-3';
	$result['fam_width']='col-sm-3  col-md-3';
	$result['cust_width']='col-sm-2  col-md-2';
}
$result['adv_product']=ADV_PRODUCT == '0' ? false:true;
if($result['adv_product']) {
	$result['code_class']='expand_row';
}



//hide import and export functionality if user is not admin on module

global $database_config;

$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users= new sqldb($database_users);

//$is_p_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' ");
$is_p_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'project_admin']);
//$is_art_ord_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id = '".$_SESSION['u_id']."' AND name = 'admin_6' ");
$is_art_ord_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id = :user_id AND name = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_6']);
$is_module_admin = false;
if($is_p_admin == '1' || $is_art_ord_admin == 'order'){
	$is_module_admin = true;
}
$result['is_module_admin']=$is_module_admin;
$_SESSION['filters'] = $arguments.$arguments_o;

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$result['families']=array();
while($db->move_next()){
	$families = array(
		'name'  => $db->f('name'),
		'id'=> $db->f('id')
	);
	  
	array_push($result['families'], $families);
}
$result['lr'] = $l_r;

json_out($result);