<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}
/*if (isset($_SESSION['tmp_add_to_archive'])) {
    unset($_SESSION['tmp_add_to_archive']);
}*/
if($in['reset_list']){
    if(isset($_SESSION['tmp_add_to_archive'])){
        unset($_SESSION['tmp_add_to_archive']);
    }
    if(isset($_SESSION['add_to_archive'])){
        unset($_SESSION['add_to_archive']);
    }
}

$db = new sqldb();
$db2 = new sqldb();

$result = array('query'=>array(), 'nav'=>array());

$l_r =ROW_PER_PAGE;
$order_by = " ORDER BY  pim_articles.item_code  ";

$order_by_array = array('item_code','internal_name','article_category','price','total_price','customer_name','stock','stock_ant','name_lang_en','name_lang_fr','name_lang_du','stock_pack','ant_stock_pack','article_brand','name_lang2_en','name_lang2_fr','name_lang2_du');

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

//FILTER LIST
$selected =array();
$filter = '1=1';
$filter_attach = '';
$filter.= " AND pim_articles.is_service='0' ";

$result['allow_stock'] = ALLOW_STOCK == '0' ? false:true;
$result['adv_product']=ADV_PRODUCT == '0' ? false:true;
$result['use_packaging']=ALLOW_ARTICLE_PACKING == '1' ? true:false;

$dynamic_join="";
$dynamic_cols="";
$result['columns']=array();
$cols_default=default_columns_dd(array('list'=>'articles'));
$cols_order_dd=default_columns_order_dd(array('list'=>'articles'));
$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='articles' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();

$lang_name_en_done=false;
$lang_name_fr_done=false;
$lang_name_du_done=false;

if(!count($cols_selected)){
  $i=1;
    $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'articles'));
    foreach($cols_default as $key=>$value){
    if(($key=='stock' || $key=='ant_stock') && (!$result['adv_product'] || !$result['allow_stock'])){
        continue;
    }
    if(($key=='stock_pack' || $key=='ant_stock_pack') && !$result['use_packaging']){
        continue;
    }
    if($key=='name_lang_en' || $key=='name_lang2_en'){
      if(!$lang_name_en_done){
        $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_en ON lang_name_en.item_id=pim_articles.article_id AND lang_name_en.lang_id='1' ";
        $dynamic_cols.=",lang_name_en.name AS name_lang_en,lang_name_en.name2 AS name_lang2_en";
        $lang_name_en_done=true;
      }     
    }else if($key=='name_lang_fr' || $key=='name_lang2_fr'){
      if(!$lang_name_fr_done){
        $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_fr ON lang_name_fr.item_id=pim_articles.article_id AND lang_name_fr.lang_id='2' ";
        $dynamic_cols.=",lang_name_fr.name AS name_lang_fr,lang_name_fr.name2 AS name_lang2_fr";
        $lang_name_fr_done=true;
      }     
    }else if($key=='name_lang_du' || $key=='name_lang2_du'){
      if(!$lang_name_du_done){
        $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_du ON lang_name_du.item_id=pim_articles.article_id AND lang_name_du.lang_id='3' ";
        $dynamic_cols.=",lang_name_du.name AS name_lang_du,lang_name_du.name2 AS name_lang2_du";
        $lang_name_du_done=true;
      }    
    }
    $tmp_item=array(
      'id'        => $i,
      'column_name'   => $key,
      'name'        => $value,
      'order_by'      => $cols_order_dd[$key]
    );
    if ($default_selected_columns_dd[$key]) {
        array_push($result['columns'],$tmp_item);
    }
    $i++;
  }
}else{
  foreach($cols_selected as $key=>$value){
    if(($value['column_name']=='stock' || $value['column_name']=='ant_stock') && (!$result['adv_product'] || !$result['allow_stock'])){
        continue;
    }
    if(($value['column_name']=='stock_pack' || $value['column_name']=='ant_stock_pack') && !$result['use_packaging']){
        continue;
    }
    if($value['column_name']=='name_lang_en' || $value['column_name']=='name_lang2_en'){
      if(!$lang_name_en_done){
        $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_en ON lang_name_en.item_id=pim_articles.article_id AND lang_name_en.lang_id='1' ";
        $dynamic_cols.=",lang_name_en.name AS name_lang_en,lang_name_en.name2 AS name_lang2_en";
        $lang_name_en_done=true;
      }     
    }else if($value['column_name']=='name_lang_fr' || $value['column_name']=='name_lang2_fr'){
      if(!$lang_name_fr_done){
        $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_fr ON lang_name_fr.item_id=pim_articles.article_id AND lang_name_fr.lang_id='2' ";
        $dynamic_cols.=",lang_name_fr.name AS name_lang_fr,lang_name_fr.name2 AS name_lang2_fr";
        $lang_name_fr_done=true;
      }     
    }else if($value['column_name']=='name_lang_du' || $value['column_name']=='name_lang2_du'){
      if(!$lang_name_du_done){
        $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_du ON lang_name_du.item_id=pim_articles.article_id AND lang_name_du.lang_id='3' ";
        $dynamic_cols.=",lang_name_du.name AS name_lang_du,lang_name_du.name2 AS name_lang2_du";
        $lang_name_du_done=true;
      }    
    }
    $tmp_item=array(
      'id'        => $value['id'],
      'column_name'   => $value['column_name'],
      'name'        => $cols_default[$value['column_name']],
      'order_by'      => $cols_order_dd[$value['column_name']]
    );
    array_push($result['columns'],$tmp_item);
  }
}

if($in['archived']==0){
    $filter.= " AND pim_articles.active='1' ";
}else{
    $filter.= " AND pim_articles.active='0' ";
    $arguments.="&archived=".$in['archived'];
    $arguments_a = "&archived=".$in['archived'];
}

if(!empty($in['search'])){
    $quotation_mark = false;
    $pos = strpos($in['search'], '"');
    if($pos){
      $quotation_mark = true;
    }
    if($quotation_mark){
      $in['search'] = str_replace('\\"','',$in['search']);
      $filter .= " AND ( pim_articles.item_code LIKE '".rtrim(ltrim($in['search']))."%'  OR pim_articles.internal_name LIKE '".rtrim(ltrim($in['search']))."%' OR pim_articles.origin_number LIKE '".rtrim(ltrim($in['search']))."%' ) ";
    }else{
      $search_words = explode(" ", $in['search']);
      $query_string ='';
      $query_string2 ='';
      $all_tcl=1;
      $tcl=0;
      $len = count($search_words);

      for ($s = 0; $s < $len; $s++) {
          if ($search_words[$s]) {
              $query_string8 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.internal_name ";
              $query_string9 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.item_code ";
              $query_string10 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.origin_number ";

              if(strlen($search_words[$s])>3){
                $all_tcl=0;
              }else{
                $tcl=1;
              }
  /*            if($len>1 && $s>0){
                    $query_string4 .= "+".$search_words[$s]." "; 
              }else{*/
                $query_string4 .= "+".$search_words[$s]."* "; 
                $query_string5 .= "+".$search_words[$s]." "; 
                if($s==0){
                  $query_string6 .= "+".$search_words[$s]." "; 
                }else{
                  $query_string6 .= "+".$search_words[$s]."* ";
                }
              
             /* }*/
          }
      }
      $query_string7 =SUBSTR($query_string4, 0, STRLEN($query_string4) - 2);

      if($all_tcl || $tcl ){
          $size = $len - 1;

          if($size){
              $perm = range(0, $size);
              $j = 0;

              do { 
                   foreach ($perm as $i) { $perms[$j][] = $search_words[$i]; }
              } while ($perm = pc_next_permutation($perm, $size) and ++$j);

              foreach ($perms as $p) {
                  $query_string1 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.item_code ";
                  $query_string2 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.internal_name ";
                  $query_string3 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.origin_number ";
              }

              $query_string1 = SUBSTR($query_string1, 0, STRLEN($query_string1) - 27);
              $query_string2 = SUBSTR($query_string2, 0, STRLEN($query_string2) - 31);
              $query_string3 = SUBSTR($query_string3, 0, STRLEN($query_string3) - 31);

          }else{
                  $query_string1 .= "LIKE '%" . $search_words[0] . "%' ";
                  $query_string2 .= "LIKE '%" . $search_words[0] . "%' ";
                  $query_string3 .= "LIKE '%" . $search_words[0]. "%' ";
          }

          $query_string8 =SUBSTR($query_string8, 0, STRLEN($query_string8) - 31);
          $query_string9 =SUBSTR($query_string9, 0, STRLEN($query_string9) - 27);
          $query_string10 =SUBSTR($query_string10, 0, STRLEN($query_string10) - 31);
          
      }

      $query_string = SUBSTR($query_string, 0, STRLEN($query_string) - 31);
          
      //var_dump($filter);exit();
      if ($len>1){
        if($all_tcl || $tcl){ 
          if($tcl){
            $filter .= " AND ( pim_articles.item_code ".$query_string9."  OR pim_articles.internal_name ".$query_string8." OR pim_articles.origin_number ".$query_string10." ) ";
          }else{
            $filter .= " AND ( pim_articles.item_code ".$query_string1."  OR pim_articles.internal_name ".$query_string2." OR pim_articles.origin_number ".$query_string3." ) ";
          }
          
        }else{
          $filter .= " AND (match(pim_articles.internal_name) against ('".$query_string4."' in boolean mode)
                      OR match(pim_articles.internal_name) against ('".$query_string5."' in boolean mode)
                      OR match(pim_articles.internal_name) against ('".$query_string6."' in boolean mode)   
                      OR match(pim_articles.internal_name) against ('".$query_string7."' in boolean mode)    
                      OR match(pim_articles.item_code) against ('".$query_string4."' in boolean mode)
                      OR match(pim_articles.origin_number) against ('".$query_string4."' in boolean mode)            
                             )";
        }
         
      }else{
          $filter .= " AND (pim_articles.internal_name LIKE '%".rtrim(ltrim($in['search']))."%'
                        OR pim_articles.item_code LIKE '%".rtrim(ltrim($in['search']))."%'
                        OR pim_articles.origin_number like '%" . rtrim(ltrim($in['search'])) . "%' )"; 
      }

      $arguments_s.="&search=".$in['search'];
    }
}
if(!empty($in['name'])){
    $filter .= " AND ( customers.name LIKE '%".$in['name']."%') ";
    $arguments_s.="&name=".$in['name'];
}
if(!empty($in['supplier_reference'])){
    $filter .= " AND ( pim_articles.supplier_reference LIKE '%".$in['supplier_reference']."%') ";
    $arguments_s.="&supplier_reference=".$in['supplier_reference'];
}
if($in['supplier_id'] && !empty($in['supplier_id'])){
    $filter .=" AND pim_articles.supplier_id='".$in['supplier_id']."'";
}
if($in['commercial_name'] && !empty($in['commercial_name'])){
  if(!$lang_name_en_done){
    $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_en ON lang_name_en.item_id=pim_articles.article_id AND lang_name_en.lang_id='1' ";
  }
  if(!$lang_name_fr_done){
    $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_fr ON lang_name_fr.item_id=pim_articles.article_id AND lang_name_fr.lang_id='2' ";
  }
  if(!$lang_name_du_done){
    $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_du ON lang_name_du.item_id=pim_articles.article_id AND lang_name_du.lang_id='3' ";
  }
  $filter .=" AND (lang_name_en.name LIKE '%".$in['commercial_name']."%' OR lang_name_fr.name LIKE '%".$in['commercial_name']."%' OR lang_name_du.name LIKE '%".$in['commercial_name']."%') ";
}
if($in['commercial_name2'] && !empty($in['commercial_name2'])){
  if(!$lang_name_en_done){
    $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_en ON lang_name_en.item_id=pim_articles.article_id AND lang_name_en.lang_id='1' ";
  }
  if(!$lang_name_fr_done){
    $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_fr ON lang_name_fr.item_id=pim_articles.article_id AND lang_name_fr.lang_id='2' ";
  }
  if(!$lang_name_du_done){
    $dynamic_join.=" LEFT JOIN pim_articles_lang AS lang_name_du ON lang_name_du.item_id=pim_articles.article_id AND lang_name_du.lang_id='3' ";
  }
  $filter .=" AND (lang_name_en.name2 LIKE '%".$in['commercial_name2']."%' OR lang_name_fr.name2 LIKE '%".$in['commercial_name2']."%' OR lang_name_du.name2 LIKE '%".$in['commercial_name2']."%') ";
}
if($in['brand_id'] && !empty($in['brand_id'])){
    $filter .=" AND pim_articles.article_brand_id='".$in['brand_id']."'";
}
if(!empty($in['ean_code'])){
    $filter .= " AND ( pim_articles.ean_code LIKE '%".$in['ean_code']."%') ";
}

# this parts is obsolete
if($in['attach_to_product']){
     if($in['attach_to_product']==1){
        $filter_attach.= " INNER JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
     }else{
        $filter_attach.= " LEFT JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
        $filter.=" AND pim_product_article.article_id is null";
     }
    $arguments.="&attach_to_product=".$in['attach_to_product'];
}
# end
if($in['is_picture']){
     if($in['is_picture']==1){
        // $filter_attach.= " INNER JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
        $filter.=" AND pim_articles.parent_id <> 0";
     }else{
        // $filter_attach.= " LEFT JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
        $filter.=" AND pim_articles.parent_id = 0";
     }
    $arguments.="&is_picture=".$in['is_picture'];
}

if($in['article_category_id'] || $in['article_category_id'] != '' ){
  if($in['article_category_id']==0){
    $filter.=" AND pim_articles.article_category_id=''";
  }else{
    $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
  }
    $arguments.="&article_category_id=".$in['article_category_id'];
}
if($in['first'] && $in['first'] == '[0-9]'){
    $filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
    $arguments.="&first=".$in['first'];
} elseif ($in['first']){
    $filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
    $arguments.="&first=".$in['first'];
}

if($in['order_by']){
    if(in_array($in['order_by'], $order_by_array)){
    $order = " ASC ";
    if($in['desc'] == '1' || $in['desc']=='true'){
        $order = " DESC ";
    }
    if($in['order_by']=='customer_name'){
        $in['order_by']='name';
    }
    if($in['order_by']=='stock_ant' || $in['order_by']=='total_price' || $in['order_by']=='stock_pack' || $in['order_by']=='ant_stock_pack'){

    }else{
       $order_by =" ORDER BY ".$in['order_by']." ".$order;
       $arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
    }
    /*$view_list->assign(array(
        'on_'.$in['order_by']   => $in['desc'] ? 'on_asc' : 'on_desc',
    ));*/
    }
}

if($in['order_by']=='stock_ant' || $in['order_by']=='total_price' || $in['order_by']=='stock_pack' || $in['order_by']=='ant_stock_pack'){
    $filter_limit =' ';
    $order_by ='';
}else{
     $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
}


$arguments = $arguments.$arguments_s;
if($in['order_by']=='name'){

}elseif($in['name']){

}else{
/*$internal_name = $db->query("SELECT DISTINCT SUBSTR( internal_name, 1, 1 ) AS internal_name
            FROM pim_articles
        WHERE $filter
         ".$order_by."
        ")->getAll();*/
}
/*foreach ($internal_name as $key => $value) {
    array_push($selected, $value['internal_name']);
}*/
$internal_name =null;
/*if($in['order_by']=='name'){

}elseif($in['name']){

}else{*/
$e = $db->query("SELECT pim_articles.article_id
            FROM pim_articles
            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
            ".$dynamic_join."
        WHERE $filter ".$order_by);
//}

$max_rows=$e->records_count();

$_SESSION['articles_id']=array();
/*
foreach ($e as $key => $value) {
    array_push($_SESSION['articles_id'],$value['article_id']);
}

//$nav = array();
*/
if(!$_SESSION['tmp_add_to_archive'] || ($_SESSION['tmp_add_to_archive'] && empty($_SESSION['tmp_add_to_archive']))){
    while($e->move_next()){
        array_push($_SESSION['articles_id'],$e->f('article_id'));
        $_SESSION['tmp_add_to_archive'][$e->f('article_id')]=1;
        //array_push($result['nav'], $e->f('article_id'));
    }
}/*else{
    while($e->next()){
        array_push($result['nav'], $e->f('article_id'));
    }
}*/
//$result['nav'] =  $nav;

$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_to_archive']){
    if($max_rows && count($_SESSION['add_to_archive']) == $max_rows){
        $all_pages_selected=true;
    }else if(count($_SESSION['add_to_archive'])){
        $minimum_selected=true;
    }
}

$e = null;

$result['max_rows']=$max_rows;
$result['all_pages_selected'] = $all_pages_selected;
$result['minimum_selected'] = $minimum_selected;


if(!isset($in['lang_id'])){
  $in['lang_id'] = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1' GROUP BY lang_id ");
}
if(!$in['lang_id']){
    $in['lang_id']=3;
}

/*$articles = $db->query("SELECT pim_articles.use_serial_no,pim_articles.use_batch_no,pim_articles.article_id,pim_articles.hide_stock,pim_articles.block_discount,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,customers.name,

                           pim_articles.internal_name,pim_articles.price,pim_articles.article_category,
                           vats.value AS vat_value ,pim_articles_lang.description AS description,pim_articles.ean_code,pim_article_categories.name AS article_category_name

            FROM pim_articles
            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
        WHERE $filter
       ".$order_by."
        LIMIT ".$offset*$l_r.",".$l_r."
        ");*/
$articles = $db->query("SELECT pim_article_variants.parent_article_id, pim_articles.use_serial_no,pim_articles.use_batch_no,pim_articles.use_combined,pim_articles.article_id,pim_articles.hide_stock,pim_articles.block_discount,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,customers.name,pim_articles.internal_name,pim_articles.price,pim_articles.article_category,pim_articles.sale_unit,pim_articles.packing,pim_articles.vat_id,
 pim_articles.ean_code,pim_article_categories.name AS article_category_name,pim_article_brands.name AS article_brand,pim_articles.has_variants".$dynamic_cols."
            FROM pim_articles
            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id AND customers.active='1'
            LEFT JOIN pim_article_variants ON pim_articles.article_id = pim_article_variants.article_id
            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
            LEFT JOIN pim_article_brands ON pim_articles.article_brand_id=pim_article_brands.id
            ".$dynamic_join."
        WHERE $filter
       ".$order_by.$filter_limit);

if($in['order_by']=='stock_ant' || $in['order_by']=='total_price' || $in['order_by']=='stock_pack' || $in['order_by']=='ant_stock_pack'){
    $filter_limit =' ';
    $order_by ='';
}else{
     $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
}


$all_vats_ar=array();
  $all_vats=$db2->query("SELECT vat_id, value FROM vats")->getAll();
  foreach($all_vats as $key=>$value){
    $all_vats_ar[$value['vat_id']]=$value['value'];
  }


$j=0;
$filter_po = ' ';
$filter_po2 = ' ';
 $filter_po = ' LEFT JOIN pim_p_order_deliveries ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id ';
       // $filter_po2 = " AND pim_p_order_deliveries.delivery_done ='1'  ";
if(P_ORDER_DELIVERY_STEPS == '2'){
/*        $filter_po = ' INNER JOIN pim_p_order_deliveries ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id ';*/
        $filter_po2 = " AND pim_p_order_deliveries.delivery_done ='1'  ";
    }

$filter_o = ' ';
$filter_o2 = ' ';
 $filter_o = ' LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id ';
      //  $filter_o2 = " AND pim_order_deliveries.delivery_done ='1'"; 
if(ORDER_DELIVERY_STEPS == '2'){
  /*      $filter_o = ' INNER JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id ';
        $filter_o2 = " AND pim_order_deliveries.delivery_done ='1'  ";*/
    }

$calculate_ant_stock = false;
if(column_selected('ant_stock', $result['columns']) || column_selected('ant_stock_pack', $result['columns']) ){
    $calculate_ant_stock = true;
}

while($db->move_next()){
         //if(!$db->f('article'))
   
          $ant_stock=0;
          $ant_stock_from_suppliers=0;
          $reserved_stock=0;
          $q_delivered=0;

          $stock_packing = $db->f("packing");
            
           if(!ALLOW_ARTICLE_PACKING || !(float)$stock_packing){
               $stock_packing = 1;
           }

           $stock_sale_unit = $db->f("sale_unit");
         
           if(!ALLOW_ARTICLE_SALE_UNIT || !(float)$stock_sale_unit){
               $stock_sale_unit = 1;
           } 

           if($calculate_ant_stock && !$articles->f('use_combined')){
          //items on purchase
          /*    $items_order=$db2->field("SELECT SUM(pim_p_order_articles.quantity) 
                                    FROM  pim_p_order_articles 
                                    INNER JOIN pim_p_orders ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
                                    WHERE pim_p_order_articles.article_id='".$db->f('article_id')."'
                                    AND pim_p_orders.rdy_invoice!=0");

              $items_received=$db2->field("SELECT SUM(pim_p_orders_delivery.quantity) 
                                       FROM   pim_p_orders_delivery 
                                       INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
                                       $filter_po
                                       WHERE pim_p_order_articles.article_id='".$db->f('article_id')."' ".$filter_po2);*/
              $qty_orders_p=0;
          /*    $qty_ord_p = $db2->query("SELECT COUNT(pim_p_orders.p_order_id) as nr_orders, COUNT(pim_p_orders_delivery.delivery_id) as nr_del, SUM( pim_p_order_articles.quantity ) AS total_order, SUM( pim_p_orders_delivery.quantity ) AS total_deliver
                                  FROM pim_p_orders
                                  INNER JOIN pim_p_order_articles ON pim_p_orders.p_order_id = pim_p_order_articles.p_order_id
                                  LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                                  LEFT JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                                  $filter_po
                                  WHERE pim_p_order_articles.article_id =  '".$db->f('article_id')."'
                                   AND pim_p_orders.rdy_invoice!=0 ".$filter_po2." 
             
                                  GROUP BY pim_p_orders.p_order_id, pim_p_orders_delivery.delivery_id");*/

                $qty_ord_p = $db2->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order, COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_p_order_deliveries.delivery_done = '1' THEN pim_p_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_p_orders_delivery.delivery_id, pim_p_order_articles.p_order_id, pim_p_order_articles.sale_unit, pim_p_order_articles.packing
                                          FROM pim_p_order_articles
                                          LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                                          LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
                                          $filter_po
                                          WHERE pim_p_order_articles.article_id =  '".$db->f('article_id')."'
                                           AND pim_p_orders.rdy_invoice!=0 
             
                                        GROUP BY pim_p_orders.p_order_id");

              $total_deliver=0;
              while($qty_ord_p->move_next()) {
                    //purchase orders haven't used packing and sale unit from the start
                    /*$stock_packing_ord_p = $qty_ord_p->f("packing");
                    if(!$stock_packing_ord_p){*/
                      $stock_packing_ord_p = $db->f("packing");
                    //}
                    if(!ALLOW_ARTICLE_PACKING || !(float)$stock_packing_ord_p){
                       $stock_packing_ord_p = 1;
                     }

                    /*$stock_sale_unit_ord_p = $qty_ord_p->f("sale_unit");
                    if(!$stock_sale_unit_ord_p){*/
                      $stock_sale_unit_ord_p = $db->f("sale_unit");
                    /*}*/
                    if(!ALLOW_ARTICLE_SALE_UNIT || !(float)$stock_sale_unit_ord_p){
                       $stock_sale_unit_ord_p = 1;
                    }  

                  $total_del=0;
                  if(P_ORDER_DELIVERY_STEPS == '2'){
                      $total_del = $qty_ord_p->f('total_deliver2');
                   }else{
                      $total_del = $qty_ord_p->f('total_deliver');
                    }
                  if($qty_ord_p->f('nr_del')){
                        if($qty_ord_p->f('total_order')/$qty_ord_p->f('nr_del') < $total_del){
                            $total_deliver = ($qty_ord_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$qty_ord_p->f('nr_del');
                        }else{
                            $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;                
                        }

                        $qty_orders_p +=(($qty_ord_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$qty_ord_p->f('nr_del') - $total_deliver); 
                    }else{
                        $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;

                        $qty_orders_p +=($qty_ord_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p - $total_deliver); 
                    }
                  
              }


              $qty_diff_p=0;
          /*    $diff_p = $db2->query("SELECT COUNT(pim_p_orders.p_order_id) as nr_orders,COUNT(pim_p_orders_delivery.delivery_id) as nr_del, SUM( pim_p_order_articles.quantity ) AS total_order, SUM( pim_p_orders_delivery.quantity ) AS total_deliver
                                  FROM pim_p_orders
                                  INNER JOIN pim_p_order_articles ON pim_p_orders.p_order_id = pim_p_order_articles.p_order_id
                                  LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                                  WHERE pim_p_order_articles.article_id =  '".$db->f('article_id')."'
                                  AND pim_p_orders.sent =1
                                  AND pim_p_orders.rdy_invoice =  '1'
                                  GROUP BY pim_p_orders.p_order_id");*/

              $diff_p = $db2->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order ,COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_p_order_deliveries.delivery_done = '1' THEN pim_p_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_p_orders_delivery.delivery_id, pim_p_order_articles.p_order_id,pim_p_order_articles.sale_unit,pim_p_order_articles.packing
                                          FROM pim_p_order_articles
                                          LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
                                          LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
                                          $filter_po
                                          WHERE pim_p_order_articles.article_id =  '".$db->f('article_id')."'
                                          AND pim_p_orders.sent =1
                                          AND pim_p_orders.rdy_invoice =  '1' ".$filter_po2."

                                          GROUP BY pim_p_orders.p_order_id");
             
              $total_deliver=0;
              while($diff_p->move_next()) {

                  /* $stock_packing_ord_p = $diff_p->f("packing");
                  if(!$stock_packing_ord_p){*/
                    $stock_packing_ord_p = $db->f("packing");
                  //}
                  if(!ALLOW_ARTICLE_PACKING || !(float)$stock_packing_ord_p ){
                     $stock_packing_ord_p = 1;
                   }
                   
                  /*$stock_sale_unit_ord_p = $diff_p->f("sale_unit");
                  if(!$stock_sale_unit_ord_p){*/
                    $stock_sale_unit_ord_p = $db->f("sale_unit");
                  //}
                  if(!ALLOW_ARTICLE_SALE_UNIT || !(float)$stock_sale_unit_ord_p){
                     $stock_sale_unit_ord_p = 1;
                  } 

                  $total_del=0;
                  if(P_ORDER_DELIVERY_STEPS == '2'){
                      $total_del = $diff_p->f('total_deliver2');
                   }else{
                      $total_del = $diff_p->f('total_deliver');
                    }
                 if($diff_p->f('nr_del')){
                      if($diff_p->f('total_order')/$diff_p->f('nr_del') < $total_del){
                          $total_deliver = ($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$diff_p->f('nr_del');
                      }else{

                          $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;

                      }
                      $qty_diff_p +=(($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$diff_p->f('nr_del') - $total_deliver); 
                 }else{

                      $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;

                      $qty_diff_p +=($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p - $total_deliver); 
                 }
            
              }



          //items on orders   
          /*     $items_on_order=$db2->field("SELECT SUM(pim_order_articles.quantity) 
                                    FROM  pim_order_articles 
                                    INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
                                    WHERE pim_order_articles.article_id='".$db->f('article_id')."'
                                    AND pim_orders.sent=1");

              $items_on_received=$db2->field("SELECT SUM(pim_orders_delivery.quantity) 
                                       FROM   pim_orders_delivery 
                                       INNER JOIN pim_order_articles ON pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
                                       WHERE pim_order_articles.article_id='".$db->f('article_id')."'");*/
              $qty_orders=0;
             /* $qty_ord = $db2->query("SELECT COUNT( pim_orders.order_id ) as nr_orders, SUM( pim_order_articles.quantity ) AS total_order, SUM( pim_orders_delivery.quantity ) AS total_deliver
                                  FROM pim_orders
                                  INNER JOIN pim_order_articles ON pim_orders.order_id = pim_order_articles.order_id
                                  LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
                                  WHERE pim_order_articles.article_id =  '".$db->f('article_id')."'
                                  AND pim_orders.sent =1
                                  GROUP BY pim_orders.order_id");*/
              $qty_ord = $db2->query("SELECT pim_order_articles.order_articles_id, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del , pim_order_articles.article_id, SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_orders_delivery.delivery_id, pim_order_articles.order_id, pim_order_articles.sale_unit, pim_order_articles.packing
                                  FROM pim_order_articles
                                  LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
                                  LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
                                  $filter_o
                                  WHERE pim_order_articles.article_id =  '".$db->f('article_id')."'
                                  AND pim_orders.sent =1 ".$filter_o2." 
                                  AND pim_orders.active =1
                                  GROUP BY pim_orders.order_id");



              $total_deliver=0;
              while($qty_ord->move_next()) {
                  $nr_del = $qty_ord->f('nr_del');
                  if($nr_del==0) {
                    $nr_del = 1;
                    //continue;
                  }

                  $total_del=0;

                  $stock_packing_ord = $qty_ord->f("packing");
                  if(!$stock_packing_ord){
                    $stock_packing_ord = $db->f("packing");
                  }
                  if(!ALLOW_ARTICLE_PACKING || !(float)$stock_packing_ord){
                     $stock_packing_ord = 1;
                   }
                   
                  $stock_sale_unit_ord = $qty_ord->f("sale_unit");
                  if(!$stock_sale_unit_ord){
                    $stock_sale_unit_ord = $db->f("sale_unit");
                  }
                  if(!ALLOW_ARTICLE_SALE_UNIT || !(float)$stock_sale_unit_ord){
                     $stock_sale_unit_ord = 1;
                  } 

                  if(ORDER_DELIVERY_STEPS == '2'){
                      $total_del = $qty_ord->f('total_deliver2');
                   }else{
                      $total_del = $qty_ord->f('total_deliver');
                  }

                  if($qty_ord->f('total_order')/$nr_del < $total_del){

                      $total_deliver = ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
                  }else{
                      $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord;
                  }

                  $qty_orders +=( ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver );  
              }

              $qty_diff=0;
              /*$diff = $db2->query("SELECT COUNT( pim_orders.order_id ) as nr_orders, SUM( pim_order_articles.quantity ) AS total_order, SUM( pim_orders_delivery.quantity ) AS total_deliver
                                  FROM pim_orders
                                  INNER JOIN pim_order_articles ON pim_orders.order_id = pim_order_articles.order_id
                                  LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
                                  WHERE pim_order_articles.article_id =  '".$db->f('article_id')."'
                                  AND pim_orders.sent =1
                                  AND pim_orders.rdy_invoice =  '1'
                                  GROUP BY pim_orders.order_id");*/

              $diff = $db2->query("SELECT pim_order_articles.order_articles_id, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del, pim_order_articles.article_id,SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_orders_delivery.delivery_id, pim_orders_delivery.order_id,pim_order_articles.sale_unit, pim_order_articles.packing
                                  FROM pim_order_articles
                                  LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
                                  LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
                                  $filter_o
                                  WHERE pim_order_articles.article_id =  '".$db->f('article_id')."'
                                  AND pim_orders.sent =1
                                  AND pim_orders.rdy_invoice =  '1' ".$filter_o2."
                                  AND pim_orders.active =1
                                  GROUP BY pim_orders.order_id");

              $total_deliver=0;
              while($diff->move_next()) {

                $nr_del = $diff->f('nr_del');
                if($nr_del==0) {
                    $nr_del = 1;
                  }

                  $stock_packing_ord = $diff->f("packing");
                  if(!$stock_packing_ord){
                    $stock_packing_ord = $db->f("packing");
                  }
                  if(!ALLOW_ARTICLE_PACKING || !(float)$stock_packing_ord){
                     $stock_packing_ord = 1;
                   }
                   
                  $stock_sale_unit_ord = $diff->f("sale_unit");
                  if(!$stock_sale_unit_ord){
                    $stock_sale_unit_ord = $db->f("sale_unit");
                  }
                  if(!ALLOW_ARTICLE_SALE_UNIT || !(float)$stock_sale_unit_ord){
                     $stock_sale_unit_ord = 1;
                  } 

                  if(ORDER_DELIVERY_STEPS == '2'){
                      $total_del = $diff->f('total_deliver2');
                   }else{
                      $total_del = $diff->f('total_deliver');
                  }
                  if($diff->f('total_order')/$nr_del < $total_del){
                      $total_deliver =($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
                  }else{
                      $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord ;
                  }
               $qty_diff +=(($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver); // if we have an order delivered partially, but marked as delivered
               
              }




          //items on project
               $items_on_project=$db2->field("SELECT SUM(project_articles.quantity) 
                                    FROM  project_articles 
                                    INNER JOIN  projects ON  projects.project_id=project_articles.project_id
                                    WHERE project_articles.article_id='".$db->f('article_id')."'
                                    AND projects.stage!=0 AND project_articles.delivered=0");

          //items on intervetions
               $items_on_intervetion=$db2->field("SELECT SUM( servicing_support_articles.quantity) 
                                    FROM   servicing_support_articles 
                                    INNER JOIN  servicing_support ON  servicing_support.service_id=servicing_support_articles.service_id
                                    WHERE servicing_support_articles.article_id='".$db->f('article_id')."'
                                    AND servicing_support_articles.article_delivered=0 AND servicing_support.active=1 AND servicing_support.status<2");
        

          /*$qty_orders = $qty_orders *$stock_packing_ord/$stock_sale_unit_ord;
          $qty_diff = $qty_diff*$stock_packing_ord/$stock_sale_unit_ord;
          $qty_orders_p= $qty_orders_p*$stock_packing_ord_p/$stock_sale_unit_ord_p;
          $qty_diff_p= $qty_diff_p*$stock_packing_ord_p/$stock_sale_unit_ord_p;*/
          $items_on_project= $items_on_project* $stock_packing/$stock_sale_unit;
          $items_on_intervetion =$items_on_intervetion *$stock_packing/$stock_sale_unit;

          /*console::log($db->f('stock'),$qty_orders, $qty_diff ,$items_on_project,$items_on_intervetion ,$qty_orders_p , $qty_diff_p);*/
          $ant_stock=$db->f('stock')-($qty_orders- $qty_diff +$items_on_project+$items_on_intervetion ) + ($qty_orders_p - $qty_diff_p);
          $ant_stock_from_suppliers =$qty_orders_p- $qty_diff_p;
        } //end if calculate anticipated stock
        else if($articles->f('use_combined')){
            if($articles->f('quantity')){
              $ant_stock= $articles->f('stock')/$articles->f('quantity');
            }
            $potential_stock=$db2->query("SELECT pim_articles.stock,pim_articles_combined.quantity
              FROM pim_articles
              INNER JOIN pim_articles_combined ON pim_articles_combined.article_id=pim_articles.article_id
              WHERE parent_article_id='".$articles->f('article_id')."'  ORDER BY pim_articles_combined.`sort_order` ASC ");
            while($potential_stock->move_next()){
                $ant_stock=min($ant_stock, floor($potential_stock->f('stock')/$potential_stock->f('quantity')));
            }
        }

     $reserved_stock=$db2->field("SELECT SUM( service_reservation.quantity ) AS reserved_stock
            FROM service_reservation            
            WHERE service_reservation.a_id =  '".$db->f('article_id')."' ");
     $q_delivered=$db2->field("SELECT SUM(quantity) FROM service_delivery WHERE a_id='".$db->f('article_id')."' AND from_reserved='1'");

    if (!empty($articles->f('parent_article_id'))) {
         $main_code = $db2->field("SELECT pim_articles.item_code FROM pim_articles WHERE pim_articles.article_id='" . $articles->f('parent_article_id') . "'");
    } else {
        $main_code = '-';
    }

    if(abs($ant_stock) == INF || abs($ant_stock) == NAN){
      $ant_stock = 0;
    }

    $item =array(
        'use_batch_no'      => $db->f('use_batch_no')? true:false,
        'use_serial_no'     => $db->f('use_serial_no')? true:false,
        'use_combined'      => $db->f('use_combined')? true:false,
        //'vat_value'         => $db->f('vat_value'),
        'vat_value'         => $all_vats_ar[$db->f('vat_id')],
        'description'       =>  $db->f('description'),
        'ean_code'      =>  $db->f('ean_code'),
        'category_name' =>  $db->f('article_category_name'),
        'name'              => htmlspecialchars_decode($db->f('internal_name')),
        'customer_name'     => $db->f('name'),
        //'article_category'  => $db->f('article_category'),
        'article_category'  => $db->f('article_category_name'),
        'brand'             => $db->f('article_brand'),
        'article_id'        => $db->f('article_id'),
        'id'                => $db->f('article_id'),
        'ant_stock'         => remove_zero_decimals_dn(display_number($ant_stock)),
        'stock_ant_ord'     => $ant_stock,

        'ant_stock_from_suppliers'         => remove_zero_decimals_dn(display_number($ant_stock_from_suppliers)),
        'stock'             => remove_zero_decimals_dn(display_number($db->f('stock'))), 
        'reserved_stock'    => $reserved_stock-$q_delivered>0? remove_zero_decimals_dn(display_number($reserved_stock-$q_delivered)) :0,

        'show_stock'        => $db->f('hide_stock') ? false:true,
        'block_discount'    => $db->f('block_discount')? true:false,
        'base_price'        => display_number_var_dec($db->f('price')),
        'total_price'        => place_currency(display_number_var_dec($db->f('price')+ $db->f('price')*$all_vats_ar[$db->f('vat_id')]/100)),
        'total_price_ord'    => $db->f('price')+ $db->f('price')*$all_vats_ar[$db->f('vat_id')]/100,

        'check_show_front'  => $db->f('show_front')==1? 'checked="checked"':'',
        'code'              => $db->f('item_code'),
        'main_code'         => $main_code,
        'duplicate_link'    => $in['archived'] ? 'index.php?do=article-articles_list-article-activate&article_id='.$db->f('article_id').$arguments : 'index.php?do=article-article&duplicate_article_id='.$db->f('article_id'),
        //'edit_link'           => 'index.php?do=article-article&article_id='.$db->f('article_id'),
        //'delete_link'         => $in['archived'] ? 'index.php?do=article-articles_list-article-delete&article_id='.$db->f('article_id').$arguments : 'index.php?do=article-articles_list-article-archive&article_id='.$db->f('article_id').$arguments,
        'title'             => $in['archived'] ? gm('Delete') : gm('Archive'),
        'confirm'           => $in['archived'] ? gm('Delete this article?') : gm('Archive this article?'),
        'd_confirm'         => $in['archived'] ? gm('Activate this article?') : '',
        'd_title'           => $in['archived'] ? gm('Activate') : gm('Duplicate'),
        'd_class'           => $in['archived'] ? 'art_del undo' : 'matching_company',
        'd_img'             => $in['archived'] ? 'undo' : 'duplicate',
        'allow_stock'                   => ALLOW_STOCK == '0' ? false:true,
        'stock_multiple_locations'         =>true,
        //'stock_multiple_locations'      => STOCK_MULTIPLE_LOCATIONS== '0' ? false:true,
        //'stock_multiple_locations'=> false,
        'adv_product'                   => ADV_PRODUCT == '0' ? false:true,
        'is_archived'       => $in['archived'] ? true: false,
        'confirm'           =>    gm('Confirm'),
        'ok'                =>    gm('Ok'),
        'cancel'            =>    gm('Cancel'),
        'archive_link'          => array('do'=>'article-articles-article-archive', 'article_id'=>$db->f('article_id')),
        'undo_link'             => array('do'=>'article-articles-article-activate', 'article_id'=>$db->f('article_id')),
        'delete_link'           => array('do'=>'article-articles-article-delete', 'article_id'=>$db->f('article_id')),
        'edit_link'             => array('do'=>'article-add', 'article_id'=>$db->f('article_id')),
        'has_variants'          => $db->f('has_variants') && defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1? true:false,
        'check_add_to_product'  => $_SESSION['add_to_archive'][$db->f('article_id')] == 1 ? true : false,
        'name_lang_en'  => stripslashes($db->f('name_lang_en')),
        'name_lang_fr'  => stripslashes($db->f('name_lang_fr')),
        'name_lang_du'  => stripslashes($db->f('name_lang_du')),
        'stock_pack'    => remove_zero_decimals_dn(display_number($db->f('stock')/$stock_packing)),
        'ant_stock_pack'=> remove_zero_decimals_dn(display_number($ant_stock/$stock_packing)),
        'stock_pack_ord'     => $db->f('stock')/$stock_packing,
        'ant_stock_pack_ord'=> $ant_stock/$stock_packing,
        'name_lang2_en'  => stripslashes($db->f('name_lang2_en')),
        'name_lang2_fr'  => stripslashes($db->f('name_lang2_fr')),
        'name_lang2_du'  => stripslashes($db->f('name_lang2_du')),
        );
     if($ant_stock<=0){
           $item['ant_stock_status']='text-danger';
          
     }elseif($ant_stock > 0  && $ant_stock <= $db->f('article_threshold_value')){
           $item['ant_stock_status']='text-warning';
     }else{
           $item['ant_stock_status']='text-success';

     }

     if($db->f('stock') <= 0){
           $item['stock_status']='text-danger';
          
     }elseif($db->f('stock') > 0  && $db->f('stock') <= $db->f('article_threshold_value') ){
           $item['stock_status']='text-warning';
     }else{
           $item['stock_status']='text-success';

     }


    array_push($result['query'], $item);
    $j++;
}

if(ALLOW_STOCK && ADV_PRODUCT ){
    $result['code_width']='col-sm-2  col-md-2';
    $result['fam_width']='col-sm-1  col-md-1';
    $result['cust_width']='col-sm-2  col-md-2';
}else if(!ALLOW_STOCK && ADV_PRODUCT){
    $result['code_width']='col-sm-2  col-md-2';
    $result['fam_width']='col-sm-1  col-md-1';
    $result['cust_width']='col-sm-2  col-md-2';
}else{
    $result['code_width']='col-sm-3  col-md-3';
    $result['fam_width']='col-sm-3  col-md-3';
    $result['cust_width']='col-sm-2  col-md-2';
}

if($result['adv_product']) {
    $result['code_class']='expand_row';
}
$result['allow_variants']=defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1? true:false;  


//hide import and export functionality if user is not admin on module

global $database_config;

$database_users = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
);
$db_users= new sqldb($database_users);

//$is_p_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' ");
$is_p_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'project_admin']);
//$is_art_ord_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id = '".$_SESSION['u_id']."' AND name = 'admin_6' ");
$is_art_ord_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id = :user_id AND name = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_6']);
$is_module_admin = false;
if($is_p_admin == '1' || $is_art_ord_admin == 'order'){
    $is_module_admin = true;
}
$result['is_module_admin']=$is_module_admin;
$_SESSION['filters'] = $arguments.$arguments_o;

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$result['families']=array();
while($db->move_next()){
    $families = array(
    'name'  => $db->f('name'),
    'id'=> $db->f('id')
    );
      
    array_push($result['families'], $families);
}
$families_unspecified=array(
  'name' => 'Unspecified',
  'id'  => '0'
);
array_push($result['families'], $families_unspecified);
$result['lr'] = $l_r;

$result['view_disabled'] = defined('NO_ACCESS_VIEW_ARTICLE') && NO_ACCESS_VIEW_ARTICLE =='1'? '1': '0';
$result['order_by'] = $in['order_by'];
$result['desc'] = $in['desc'];

if($in['order_by']=='stock_ant' || $in['order_by']=='total_price' || $in['order_by']=='stock_pack' || $in['order_by']=='ant_stock_pack'){

    if($order ==' ASC '){
       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
    }else{
       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
    }

    $exo = array_slice( $exo, $offset*$l_r, $l_r);
    $result['query']=array();
       foreach ($exo as $key => $value) {
           array_push($result['query'], $value);
       }
}
$result['suppliers']=get_cc($in,true,false);
$result['brands']=get_brands($in,true,false);

json_out($result);

function get_cc($in,$showin=true,$exit=true){
  $db= new sqldb();
  $q = strtolower($in["term"]);
  global $database_config;
  $database_users = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
  );
  
  $db_user = new sqldb($database_users);
  $filter =" is_admin='0' AND customers.active=1 AND is_supplier=1 ";
  if($q){
    $filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
  }
  if($in['supplier_id']){
    $filter .=" AND customers.customer_id='".$in['supplier_id']."'";
  }

  $admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
  if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
    $filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
  }
  $db= new sqldb();

  $cust = $db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
      FROM customers
      LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
      WHERE $filter
      GROUP BY customers.customer_id    
      ORDER BY name
      LIMIT 10")->getAll();

  $result = array();
  foreach ($cust as $key => $value) {
    $cname = trim($value['name']);
    $result[]=array(
        "id"          => $value['cust_id'],
        "label"         => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
        "name"         => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
    );
    
  }
  return json_out($result, $showin,$exit);

}
function get_brands($in,$showin=true,$exit=true){
  $db= new sqldb();
  $q = strtolower($in["term"]);

  $filter =" 1=1 ";
  if($q){
    $filter .=" AND name LIKE '%".addslashes($q)."%'";
  }
  if($in['brand_id']){
    $filter .=" AND id='".$in['brand_id']."'";
  }

  $db= new sqldb();
  $brands = $db->query("SELECT id,name FROM pim_article_brands
      WHERE $filter   
      ORDER BY name
      LIMIT 10")->getAll();

  $result = array();
  foreach ($brands as $key => $value) {
    $cname = trim($value['name']);
    $result[]=array(
        "id"          => $value['id'],
        "label"         => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
        "name"         => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
        "value" => $cname,
    );
    
  }
  return json_out($result, $showin,$exit);

}

