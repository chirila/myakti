<?php

$article = array();

// $article['article_brand'] 			= build_article_brand_dd($in['article_brand_id']);
$article['ledger_account'] 	= build_article_ledger_account_dd($in['ledger_account_id']);
$article['article_category'] 		= build_article_category_dd($in['article_category_id']);
$article['supplier_name']				= $in['supplier_name'];
$article['supplier_id']				= $in['sc_id'];

$article['supplier']				= get_supplier($in,true,false);
$article['is_admin']				= $_SESSION['access_level'] == 1 ? true : false;

$article['article_brand'] 		= build_article_brand_dd($in['article_brand_id']);
$article['article_ledger_account'] 	= build_article_ledger_account_dd($in['ledger_account_id']);
$article['allow_article_packing'] 		= ALLOW_ARTICLE_PACKING ?true:false;
$article['allow_article_sale_unit'] 	= ALLOW_ARTICLE_SALE_UNIT?true:false;
$article['allow_stock'] 		= ALLOW_STOCK ?true:false;
$article['adv_product'] 	= ADV_PRODUCT?true:false;
$article['vat_dd']					= build_vat_dd('');
$article['vat_id']	                = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");
$article['price_type']	            = $db->field("SELECT value FROM settings WHERE constant_name='PRICE_TYPE'");
$article['lang_code']               = $db->field("SELECT code FROM pim_lang WHERE sort_order='1'");
$article['is_aac']               = $db->field("SELECT value FROM settings WHERE constant_name='AAC'");
$article['is_auto_aac']               = $db->field("SELECT value FROM settings WHERE constant_name='AUTO_AAC'");
$article['article_threshold_value'] 		= ARTICLE_THRESHOLD_VALUE;
// we check if there is value
$article_maximum_threshold_value=$db->field("SELECT value FROM settings WHERE constant_name='ARTICLE_MAXIMUM_THRESHOLD_VALUE'");
if($article_maximum_threshold_value) {
    $article['article_maximum_stock_treshold'] = $article_maximum_threshold_value;
}
$article['ARTICLE_PRICE_COMMA_DIGITS']		= $db->field("SELECT value FROM settings WHERE constant_name = 'ARTICLE_PRICE_COMMA_DIGITS' ");
$article['country_dd']				= build_country_list();
$article['main_country_id']			= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';

$article['tab_class']='disabled';
$article['allow_variants']=defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1? true:false;
if(!$in['form_type']){
	$NEXT_AUTOMATIC_ARTICLE_BARCODE = $db->field("SELECT value FROM settings WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE'");
	if($NEXT_AUTOMATIC_ARTICLE_BARCODE){
		$article['ean_code']=$NEXT_AUTOMATIC_ARTICLE_BARCODE;
	}	
}

json_out($article);

function get_supplier(&$in,$showin=true,$exit=true){
	$in['supplier'] = 1;
	$in['from_addArticle_page'] = true;
	//$in['noAdd'] = 'false';
	$in['noAdd'] = 'true';
	$in['term']	= $in['supplier_name'];
	// return false;
	return ark::run('article-accounts');
}

?>