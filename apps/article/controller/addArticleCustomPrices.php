<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$l_r =ROW_PER_PAGE;
$result = array('lines'=>array(),'lr'=>$l_r);

$order_by = " ORDER BY customers.name ";

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$filter=" 1=1 ";
if(!empty($in['search'])){
	$filter.= " AND ( name LIKE '%".$in['search']."%') ";
}
$result['max_rows']= (int)$db->field("SELECT count(customer_id) FROM customers WHERE $filter AND is_admin='0' AND front_register='0' AND active='1' ");

$custom_prices = $db->query("SELECT customer_custom_article_price.*, customers.name,customers.customer_id,acc_manager_name, c_type_name, is_supplier,is_customer FROM customers 
							LEFT JOIN customer_custom_article_price ON customer_custom_article_price.customer_id=customers.customer_id AND customer_custom_article_price.article_id='".$in['article_id']."'	
							WHERE customers.is_admin='0' AND customers.front_register='0' AND customers.active='1' AND ".$filter.$order_by." LIMIT ".$offset*$l_r.",".$l_r);
while($custom_prices->move_next()){
	
	if($custom_prices->f('is_supplier')==1 && $custom_prices->f('is_customer')==1) {
		$class='akti-icon akti-icon-16 o_akti_custopplier';
		$class_name = gm('Customer').' & '.gm('Supplier');
	}elseif($custom_prices->f('is_customer')==0 && $custom_prices->f('is_supplier')==0){
		$class='';
		$class_name='';
	}elseif($custom_prices->f('is_supplier')==1 && $custom_prices->f('is_customer')==0){
		$class='akti-icon akti-icon-16 o_akti_delivery';
		$class_name = gm('Supplier');
	}elseif($custom_prices->f('is_customer')==1 && $custom_prices->f('is_supplier')==0){
		$class='akti-icon akti-icon-16 o_akti_customer';
		$class_name = gm('Customer');
	}

	$linie = array(
		'customer_id'				=> $custom_prices->f('customer_id'),
		'customer_name'				=> $custom_prices->f('name'),
		'custom_price'				=> display_number($custom_prices->f('price')),
		'type'						=> $class,
		'type_name'					=> $class_name,
		'type2'						=> $custom_prices->f('type')==1 ? 'users' : 'building',
		'type2_name'				=> $custom_prices->f('type')==1 ? gm('Individual') : gm('Company'),
		);
	array_push($result['lines'], $linie);
}

json_out($result);


?>


