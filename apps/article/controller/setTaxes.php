<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$l_r =10;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$filter=" 1=1 ";
if ($in['search'])
{
    $filter.=" AND pim_article_tax.code LIKE '%" .addslashes($in['search']). "%' ";
}

$is_data=false;
$i=0;
$result = array('lr'=>$l_r,'query'=>array());

$result['max_rows']= (int)$db->field("SELECT count( pim_article_tax.tax_id) FROM pim_article_tax WHERE $filter ");

$q =$db->query("SELECT pim_article_tax.*,vats.value, pim_articles_taxes.article_id,pim_articles_taxes.tax_id AS active_tax, pim_article_tax_type.name
            FROM pim_article_tax
            LEFT JOIN pim_articles_taxes on pim_articles_taxes.tax_id=pim_article_tax.tax_id AND pim_articles_taxes.article_id='".$in['article_id']."'
            LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
            LEFT JOIN vats ON vats.vat_id = pim_article_tax.vat_id WHERE $filter LIMIT ".$offset*$l_r.",".$l_r);

while ($q->move_next()) {
    if($q->f('active_tax')){
       $in['tax_id'][$q->f('tax_id')] = 1;
    }

    $db->query("SELECT * FROM pim_articles_taxes WHERE article_id='".$in['article_id']."' AND tax_id='".$q->f('tax_id')."'");
        if($db->move_next()){
            $applied_tax = 1;
        }else{
            $applied_tax = 0;
        }

    
    $item =array(  
                 'tax_id'=> $q->f('tax_id'),
                 'taxe_code'  => $q->f('code'),
                 'tax_value'=>place_currency(display_number($q->f('amount'))),
                 'description' => $q->f('description'),
                 'tax'   => $in['tax_id'][$q->f('tax_id')] == 1?true :false,
                 'tax_type'  => $q->f('name'),
                 'tax_vat'=> display_number($q->f('value')).' %',
                 'tax_vat_id'=> $q->f('vat_id'),
                 'applied_tax'=> $applied_tax,
                 'apply_to'  =>get_article_tax_application_type($q->f('apply_to')),
                 
             );
   

 
     array_push($result['query'], $item);
    //$view->loop('dispatch_row');
    $i++;
}



if($i>0){
  $is_data=1;
}
$result['is_data']=$is_data;
$result['article_id']=$in['article_id'];
$result['style']=ACCOUNT_NUMBER_FORMAT;




json_out($result);