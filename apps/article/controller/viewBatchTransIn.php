<?php
$db2=new sqldb();
$is_data=false;
$i==0;
$result = array('query'=>array());

if ($in['id']){
	$batches = $db->query("SELECT batches.batch_number, batches.date_exp, batches.article_id FROM batches WHERE batches.id= '".$in['id']."'");
	if($batches->next()){
		$transactions = $db->query("SELECT batches.* FROM batches						 
							  WHERE batches.batch_number= '".addslashes($batches->f("batch_number"))."' AND batches.date_exp= '".$batches->f("date_exp")."' AND batches.article_id= '".$batches->f("article_id")."'");
		$article_id = $batches->f("article_id");
	}
	

	while($transactions->next()){

		$is_po_delivery= false;

		if($transactions->f('p_delivery_id')){
			
			$po_delivery = $db->query("SELECT pim_p_orders.p_order_id,pim_p_orders.customer_name, pim_p_orders.serial_number, pim_p_orders.email_language, pim_p_order_deliveries.delivery_id FROM pim_p_order_deliveries 
				INNER JOIN pim_p_orders ON pim_p_order_deliveries.p_order_id = pim_p_orders.p_order_id 
				LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id = pim_p_order_articles.p_order_id 
				WHERE pim_p_order_deliveries.delivery_id= '".$transactions->f('p_delivery_id')."' AND pim_p_order_articles.article_id = '".$transactions->f('article_id')."'");
			$is_po_delivery= $po_delivery->f('delivery_id')? true : false;
			$email_language = $po_delivery->f('email_language');
			$pack = $po_delivery->f('packing');
			$su = $po_delivery->f('sale_unit');
		}

		if(!$su){
			$su=1;
		}
		if(!$pack){
			$pack=1;
		}

		$packing = $pack/$su;
		$quantity = $transactions->f('quantity')< $packing ? $transactions->f('quantity')* $packing :  $transactions->f('quantity');
		$in_stock = $transactions->f('in_stock')< $packing ? $transactions->f('in_stock')* $packing :  $transactions->f('in_stock');


		if(!$email_language){
			$email_language = 1;
		}

		$status_id = $transactions->f('status_id') ;
		$status_details = 'status_details_'.$status_id;
	  $item=array(
            'client'				=> $is_po_delivery? $po_delivery->f('customer_name'):$transactions->f('status_details_1'),
			'transaction_date'		=> date(ACCOUNT_DATE_FORMAT,$transactions->f('date_in')),
            'quantity_value'		=> $quantity,
			'quantity'				=> display_number($quantity),
			'in_stock_value'		=> $in_stock,
			'in_stock'				=> display_number($in_stock),
			/*'document_link' 		=> $is_po_delivery?'index.php?do=po_order-p_order_print&p_order_id='.$po_delivery->f('order_id').'&lid='.$email_language.'&type='.ACCOUNT_ORDER_DELIVERY_PDF_FORMAT.'&delivery_id='.$transactions->f('p_delivery_id') : $transactions->f('status_details_1'),*/
			'document'				=> $is_po_delivery? $po_delivery->f('serial_number'):$transactions->f('status_details_1'),	
			'is_po_delivery' 		=> $is_po_delivery,
			'p_order_id'			=> $is_po_delivery? $po_delivery->f('p_order_id'):'',
			'batch_id' 				=> $transactions->f('id'),
			//'show_edit'				=> $in_stock>0? true : false,
			'show_edit'				=> true ,

      );   
       array_push($result['query'], $item);    
		
		$i++;

	}
}

if($i>0){
  $is_data=1;
}
$result['is_data']=$is_data;
$result['id']=$in['id'];
$result['article_id']=$article_id;

json_out($result);