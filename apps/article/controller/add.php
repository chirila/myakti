<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
//echo generate_purchase_price($in['article_id']);

global $config;
$db2 = new sqldb();
$db3 = new sqldb();

$o=array();

if(!isset($in['tab'])){
	$in['tab']=0;
}


if(!isset($in['lang_id'])){
	$lang = $db->query("SELECT lang_id,code,default_lang FROM pim_lang WHERE sort_order='1' GROUP BY lang_id");

}else{
	$lang = $db->query("SELECT lang_id,code,default_lang FROM pim_lang WHERE lang_id='".$in['lang_id']."' GROUP BY lang_id");
}

$in['lang_id']= $lang->f('lang_id');
$in['code']   = $lang->f('code');

$o['language_dd']=build_language_ecom_dd($in['lang_id']);
$o['code']=strtoupper($in['code']);

$o['adv_product']=ADV_PRODUCT ? true:false;

if(!$in['price']){
	$in['price']=display_number_var_dec(0);
	$in['total_price']=display_number_var_dec(0);
}
if(!$in['aac_price']){
	$in['aac_price']=display_number_var_dec(0);
	
}
if(!$in['h_price']){
	$in['h_price']=display_number_var_dec(0);
}
if(!$in['d_price']){
	$in['d_price']=display_number_var_dec(0);
}

if(!$in['article_id'] && !$in['duplicate_article_id'] ){
	$page_title = gm('Add article');

	if(!isset($in['price_type'])){
		$in['price_type']=PRICE_TYPE;
	}
	if(!isset($in['vat_id'])){
		$in['vat_id'] = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");
		$in['vat_value']=ACCOUNT_VAT;
	}
	if(!$in['sale_unit']){
	   $in['sale_unit']=1;
	}
	if(!$in['packing']){
	   $in['packing']=1;
	}
    if(!$in['article_threshold_value']){
    	$in['article_threshold_value']=ARTICLE_THRESHOLD_VALUE;
    }

    $o['item_exists']= true;
    $o['do_next']='article-article-article-add';
    $o['vat_dd']=build_vat_dd($in['vat_id']);
    $o['vat_value']=$in['vat_value'];
    //$o["price_type_".$in['price_type']."_check"]=build_category_list($in['category_id'],0,1);
    $o['use_percent_default_check']=$in['use_percent_default']?"checked='check'":'';
    $o['article_brand']= build_article_brand_dd($in['article_brand_id']);
    $o['article_ledger_account']=build_article_ledger_account_dd($in['ledger_account_id']); 
    $o['article_category']=build_article_category_dd($in['article_category_id']);
    $o['edit_mode']=0;
    $o['unit_price']=0;
    $o['add_mode']=true;
    $o['check_show_on_q']=$in['show_img_q'] ? 'CHECKED' : '';
    $o['check_hide_stock']=$in['hide_stock'] ? 'CHECKED' : '';
    $o['supplier_name']=$in['supplier_name'];
    $o['is_taxes']=false;
    $o['check_billable']=$in['billable'] ? 'CHECKED' : '';
    $o['billable']= $in['billable'] == 1 ? true : false;
    $o['tab_class']='disabled';
    $o['allow_variants']=defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1? true:false;

} elseif($in['article_id'] && !$in['duplicate_article_id']) { //edit


	$o['tab_class']='';
	$o['do_next']='article-article-article-update';
	
	$article=$db->query("SELECT pim_articles.*, pim_articles.price AS unit_price,vats.value AS vat_value,pim_articles_lang.name,pim_articles_lang.name2,pim_articles_lang.description,
	                            pim_article_prices.default_price, pim_article_prices.price, pim_article_prices.total_price, pim_article_prices.purchase_price
				         FROM pim_articles
				         LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
						 LEFT JOIN vats ON pim_articles.vat_id=vats.vat_id
						 LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'
				         WHERE pim_articles.article_id='".$in['article_id']."' ");

    $multiple_suppliers = $db2->field("SELECT count(*) FROM pim_article_references WHERE article_id='". $in['article_id'] ."'");

	$o['item_exists']= true;
	console::log($article->f('article_id'));
	if(!$article->f('article_id')){
		msg::error('Article does not exist','error');
		$in['item_exists']= false;
	    json_out($in);
	}


	$page_title = gm('Edit Article').' '. $article->gf('internal_name') ;

	$in['internal_name']	= 	gfn($article->gf('internal_name'));
  
	$in['item_code']   		= 	gfn($article->gf('item_code'));
	
	
	$in['purchase_price']   = 	display_number_var_dec($article->f('purchase_price'));
	$in['total_price'] 		= 	display_number_var_dec($article->f('total_price'));
	
	
	$in['category_id'] 		= 	$article->gf('category_id');
	
	$in['supplier_reference'] 	= 	$article->gf('supplier_reference');
		
	$in['price_type']       =   $article->gf('price_type');
	
	//$in['supplier_id']   		= 	$article->f('supplier_id');
	//$in['supplier_name']   		= 	$article->f('supplier_name');

	$in['article_category_id'] = $article->gf('article_category_id');	
	$in['ledger_account_id'] = $article->gf('ledger_account_id');	
	
		// $o['category_dd']     = 	build_category_list($db->gf('category_id'),0,1);
	     $o['use_batch_no']		=   $article->gf('use_batch_no')? true:false;
	     $o['use_serial_no']		=   $article->gf('use_serial_no')? true:false;
	     $o['article_id']       = 	$article->gf('article_id'); 
	     $o['lang_id']        	= 	$article->gf('lang_id');
		 $o['name']        		= 	gfn($article->gf('name'));
	     $o['description'] 		= 	gfn($article->gf('description'));
	     $o['name2']       		= 	gfn($article->gf('name2'));
		 $o['is_admin']			=   $_SESSION['access_level'] == 1 ? true : false;
		 $o['internal_name']    = 	htmlspecialchars_decode($article->gf('internal_name'));
		 $o['unit_price']    	= 	display_number_var_dec($article->f('unit_price'));
		 $o['item_code']        = 	$article->f('item_code');
		 $o['h_price']       		= 	display_number_var_dec($article->f('h_price'));
		 $o['d_price']       		= 	display_number_var_dec($article->f('d_price'));
		 $o['check_billable']   = $article->gf('billable') ? 'CHECKED' : '';
		 $o['billable']         = $article->gf('billable') == 1 ? true : false;
		 $o['price']       		= 	display_number_var_dec($article->f('price'));
		 $o['aac_price']        = display_number_var_dec($article->f('aac_price'));
		 $o['purchase_price']   = 	display_number_var_dec($article->f('purchase_price'));
		 $o['total_price']   = 	display_number_var_dec($article->f('total_price'));
		 $o['total_price_currency']   = 	place_currency(display_number_var_dec($article->f('total_price')));
		 $o['supplier_reference'] 	= 	$article->gf('supplier_reference');
		 $o['article_category'] = $article->gf('article_category_id');
		 $o['article_category_id'] = $article->f('article_category_id');
		 $o['supplier_id']   		= 	$article->f('supplier_id') ? (string)$article->f('supplier_id') : '';	 
		 $o['ledger_account_id']   		= 	$article->f('ledger_account_id');
		 $o['article_brand_id']   		= 	$article->f('article_brand_id') ? (string)$article->f('article_brand_id') : '';
	     //$o['supplier_name']   		= 	(string)$article->f('supplier_name');	
	     $o['stock']	   		= 	remove_zero_decimals($article->f('stock'));
	     $o['article_threshold_value']       =   $article->f('article_threshold_value');
	     $o['max_stock']	   		= 	remove_zero_decimals($article->f('max_stock'));
	     $o['weight']   		= 	 display_number_var_dec($article->f('weight'));
         $o['sale_unit']     	= 	$article->f('sale_unit')?$article->f('sale_unit'):'';
	     $o['packing']       	= 	$article->f('packing')? remove_zero_decimals($article->f('packing')):'';
         $o['origin_number'] 	= 	$article->f('origin_number');
         $o['ean_code']    		= 	$article->f('ean_code');
		 $o['vat_dd']      = 	build_vat_dd($article->gf('vat_id'));
		 //$o['supplier'] 		= 	build_supplier_dd($article->gf('supplier_id'));
		 $o['supplier_list'] 		= 	get_suppliers_list($in,true,false);
		 $in['supplier_name1']=$article->f('supplier_name');
		 $o['supplier'] 		= 	get_suppliers_list($in,true,false);
		 $o['account']			=	build_customer_dd();
		 $o['article_brand'] 		= 	build_article_brand_dd($article->gf('article_brand_id'));
		 $o['article_ledger_account'] = build_article_ledger_account_dd($article->gf('ledger_account_id'));
		 $o['article_category'] 	=	build_article_category_dd($article->gf('article_category_id'));
		 $o['vat_value'] 			= 	$article->gf('vat_value');
		 $o['price_type_'.$in['price_type'].'_check'] 		= "checked='check'";
		 $o['use_percent_default_check'] 	=$article->gf('use_percent_default')?"checked='check'":'';
		 $o['edit_mode']     		= 	1;
		 $o['add_mode'] 			=  false;
		 $o['vat_id'] 				=	$article->gf('vat_id');
		 $o['active_tabs']          = '{selected:'.$in['tab'].'}';
		 $o['check_show_on_q']      = $article->gf('show_img_q') ? 'CHECKED' : '';
		 $o['check_hide_stock'] 	= $article->gf('hide_stock') ? 'CHECKED' : '';
		 $o['no_batches'] 		    = $article->gf('use_batch_no')? false:true;	
		 $o['no_serial'] 		    = $article->gf('use_serial_no')? false:true;	
		 $o['price_type'] 				=	$article->gf('price_type');
		 //$o['artPhoto']=   $db->field("SELECT file_name FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");
		 //$o['artPhoto']=  $article->f('article_photo');
		 $o['default_image'] ='images/akti-picture.png';
		 $artPhoto=get_article_photo($article->gf('article_id'));

		 if($artPhoto){
	         $o['view_upload'] = false;
	         $o['exist_image'] = true;
	     }else{
	      	 $o['view_upload'] = true;
	      	 $o['exist_image'] = false;
	     }

		 $o['artPhoto']= $artPhoto ? $artPhoto : $o['default_image'];
		 $o['has_variants']=$article->f('has_variants')? true:false;	
		 $o['allow_variants']=defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1? true:false;	
		 $o['is_archived']=$article->f('active') == 0 ? true:false;

		 $variantData=$db->query("SELECT pim_articles.article_id,pim_article_variants.id,pim_articles.item_code FROM pim_article_variants 
		 	LEFT JOIN pim_articles ON pim_article_variants.parent_article_id=pim_articles.article_id
		 	WHERE pim_article_variants.article_id='".$in['article_id']."' LIMIT 1")->getAll();

		 $o['variantsDisabled']=!empty($variantData) ? true : false;
		 $o['variantsDisabledMessage']= !empty($variantData) ? gm('This product is a variant of product:'): '';
		 $o['variantsDisabledMessageCode']= !empty($variantData) ? $variantData[0]['item_code']: '';
		 $o['variantsDisabledMessageID']= !empty($variantData) ? $variantData[0]['article_id']: '';
		 $o['variant_types_dd']=build_article_variant_types_dd();
		 $o['multiple_suppliers'] = $multiple_suppliers;


      $in['hide_stock']=$article->gf('hide_stock');
     if($in['hide_stock']){
	$o['hide_stock']=true;

}else{
	$o['hide_stock']=false;
	
}


    if($article->f('price_type')==1){
	     $o['price_type_1'] 	= true;
	     $o['price_type_2'] 	= false;
	 
	  }else{
	  	 $o['price_type_1'] 	= false;
	     $o['price_type_2'] 	= true;
     
	  }
	/*$db->query("SELECT pim_article_tax.*,vats.value,pim_articles_taxes.article_id,pim_articles_taxes.tax_id AS active_tax, pim_article_tax_type.name
            FROM pim_article_tax
            INNER JOIN pim_articles_taxes on pim_articles_taxes.tax_id=pim_article_tax.tax_id AND pim_articles_taxes.article_id='".$in['article_id']."'
            LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
            LEFT JOIN vats ON vats.vat_id = pim_article_tax.vat_id
           ");

	$j=0;*/

//$o['taxes']=array();
$o['taxes'] = get_articleTaxes($in);
	/*while($db->move_next()){
		$taxe_row=array(
                        'taxe_code' =>$db->f('code'),
                        'tax_id'    =>$db->f('tax_id'),
                        'tax_value' =>place_currency(display_number($db->f('amount'))),
                        'tax_type'  =>$db->f('name'),
                        'tax_vat'	=>display_number($db->f('value')).' %',
			);
		  
		 
		  
		
           array_push($o['taxes'], $taxe_row);
		$j++;
	}
	 $o['is_taxes']=$j;*/
	 $o['is_taxes']=count($o['taxes']);

if( $o['price_type']==1){
	$prices = get_articlePrice($in);
    $o['price_categories']=$prices['price_categories'];
    $o['si_vat_value']=$prices['si_vat_value'];
}else{
	$prices = get_articlePriceVol($in);
   

    $o['price_categories_vol']=$prices['price_categories_vol'];
   
}
$drop_info = array('drop_folder' => 'articles', 'item_id' => $in['article_id'],'document_files'=>'1');
$o['drop_info']						= $drop_info;



} else{ //duplicate
	$view->assign('do_next','article-article-article-duplicate');
	$article=$db->query("SELECT pim_articles.*,vats.value AS vat_value,pim_articles_lang.name,pim_articles_lang.name2,pim_articles_lang.description,
	                            pim_article_prices.default_price, pim_article_prices.price, pim_article_prices.total_price, pim_article_prices.purchase_price
				         FROM pim_articles
				         LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
						 LEFT JOIN vats ON pim_articles.vat_id=vats.vat_id
						 LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'
				         WHERE pim_articles.article_id='".$in['duplicate_article_id']."'  ");
	$article->next();

	$page_title = gm('Duplicate Article').' '. $article->gf('internal_name') ;

	// $in['item_code']   = $article->gf('item_code');
	$o['item_exists']= true;
	$in['name2']       = $article->gf('name2');
	$in['name']       = $article->gf('name');
	$in['weight']   = $article->gf('weight');
	//$in['ean_code']    = $article->gf('ean_code');
	$in['price']       = display_number_var_dec($article->f('price'));
	$in['aac_price']   = display_number_var_dec($article->f('aac_price'));
	$in['total_price'] = display_number_var_dec($article->f('total_price'));
	$in['description'] = $article->gf('description');
	$in['stock']	   = remove_zero_decimals($article->gf('stock'));
	$in['category_id'] = $article->gf('category_id');
//	$in['origin_number'] = $article->gf('origin_number');
	$in['sale_unit']     = $article->gf('sale_unit')?$article->gf('sale_unit'):'';
	$in['packing']       =$article->gf('packing')?$article->gf('packing'):'';
    $in['price_type'] = $article->f('price_type');
    $in['article_threshold_value']       =   $article->f('article_threshold_value');
    $in['vat_id']       =   $article->f('vat_id');
     $in['article_category_id']       =  $db->gf('article_category_id');
     $in['ledger_account_id']       =  $db->gf('ledger_account_id');
	
	//$o['category_dd']= build_category_list($db->gf('category_id'),0,1);
    $o['duplicate_article_id']       				= 	$in['duplicate_article_id'];
    $o['price']       								= 	display_number_var_dec($article->f('price'));
    $o['purchase_price']   							= 	display_number_var_dec($article->f('purchase_price'));
    $o['total_price']   							= 	display_number_var_dec($article->f('total_price'));
    $o['weight']   									= 	 display_number_var_dec($article->f('weight'));
    $o['packing']       							= 	$article->f('packing')? remove_zero_decimals($article->f('packing')):'';

    $NEXT_AUTOMATIC_ARTICLE_BARCODE = $db2->field("SELECT value FROM settings WHERE constant_name='NEXT_AUTOMATIC_ARTICLE_BARCODE'");
    if(!$article->f('is_service') && $NEXT_AUTOMATIC_ARTICLE_BARCODE){
    	$o['ean_code']								=$NEXT_AUTOMATIC_ARTICLE_BARCODE;
    }else{
    	$o['ean_code']    							= 	$article->f('ean_code');
    }
    $o['origin_number'] 							= 	$article->f('origin_number');
    $o['supplier_id']   							= 	$article->f('supplier_id') ? (string)$article->f('supplier_id') : '';	
    $in['supplier_name1']							=	$article->f('supplier_name');
	$o['supplier'] 									= 	get_suppliers_list($in,true,false);
    $o['article_category_id'] 						= 	$article->f('article_category_id');
    $o['article_brand_id']   						= 	$article->f('article_brand_id') ? (string)$article->f('article_brand_id') : '';
    $o['supplier_reference'] 						= 	$article->gf('supplier_reference');
    $o['sale_unit']     							= 	$article->f('sale_unit')?$article->f('sale_unit'):'';
    $o['is_admin']									=   $_SESSION['access_level'] == 1 ? true : false;
	$o['vat_dd']									= build_vat_dd($db->gf('vat_id'));
	$o['vat_value']									= $db->gf('vat_value');
	$o['article_brand']								= build_article_brand_dd($db->gf('article_brand_id'));
	$o['ledger_account']							= build_article_ledger_account_dd($db->gf('ledger_account_id'));
	$o['article_category']							= build_article_category_dd($db->gf('article_category_id'));
	$o['price_type_'.$in['price_type'].'_check']	= "checked='check'";
	$o['use_percent_default_check']					= $db->gf('use_percent_default')?"checked='check'":'';
	$o['add_mode']									= true;
	//$o['edit_mode']= build_category_list($db->gf('category_id'),0,1);
	$o['disabled_tabs']								= '{disabled:[1]}';
	$o['active_tabs']								= '{selected:'.$in['tab'].'}';
	$o['check_show_on_q']							= $article->gf('show_img_q') ? 'CHECKED' : '';
	$o['check_hide_stock']							= $article->gf('hide_stock') ? 'CHECKED' : '';
	$o['internal_name']								= $article->gf('internal_name');
	$o['item_code']									= $article->gf('item_code');
	$o['vat_id']									= $article->f('vat_id');
	$o['hide_stock']								= $article->gf('hide_stock') ? true : false;
	$o['ledger_account_id'] 						= $article->gf('ledger_account_id');	
	$o['price_type'] 								= $article->f('price_type');

	$variant=$db->query("SELECT pim_article_variants.id,pim_article_variants.parent_article_id
				         FROM pim_article_variants			         
				         WHERE pim_article_variants.article_id='".$in['duplicate_article_id']."'  ");

	$variant->next();

	$o['variant_type_id'] 							= $variant->f('type_id');	
	$o['parent_variant_id'] 						= $variant->f('parent_article_id');	

	$db->query("SELECT pim_article_tax.*,pim_articles_taxes.article_id,pim_articles_taxes.tax_id AS active_tax
            FROM pim_article_tax
            LEFT JOIN pim_articles_taxes on pim_articles_taxes.tax_id=pim_article_tax.tax_id  AND pim_articles_taxes.article_id='".$in['duplicate_article_id']."'
           ");

	$j=0;
	$in['tax_id']=array();
	while($db->move_next()){
		if($db->f('active_tax')){
		   $in['tax_id'][$db->f('tax_id')] = 1;
		}
		$o['taxe_code']=$db->f('code');
		$o['tax_id']=$db->f('tax_id');
		$o['tax_value']=display_number($db->f('amount'));
		$o['check_tax']=$in['tax_id'][$db->f('tax_id')] == 1?'checked="checked"' : '';

		/*$view->assign(array(
		'taxe_code'	  => $db->f('code'),
		'tax_id'	  => $db->f('tax_id'),
		'tax_value'   => display_number($db->f('amount')),
		'check_tax'   => $in['tax_id'][$db->f('tax_id')] == 1?'checked="checked"' : '',
		),"taxes_row" );*/

		//$view->loop('taxes_row');
		$j++;
	}
	$o['is_taxes']=$j;
	
}



if($in['product_id']){
	$o['is_product']=1;
	$o['product_link']="index.php?do=pim-product&product_id=".$in['product_id'];
	

}else{
	$o['is_product']=0;
	
}
switch ($in['lang_id'])
	{
		case 1:$o['translate_cls']='translate_en';
		case 2:$o['translate_cls']='translate_fr'; 
		case 3:$o['translate_cls']='translate_nl'; 
		case 4:$o['translate_cls']='translate_de'; 
		// case 5:$view->assign('translate_cls','translate_au');break;
	}

$allow_stock = $db->query("SELECT * FROM settings WHERE constant_name='ALLOW_STOCK'");
$allow_stock->next();

$stock_multiple_locations = $db->query("SELECT * FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
$stock_multiple_locations->next();


if ($allow_stock->f('value')==1)
{  
	$o['allow_stock']=true;

}else{
	$o['allow_stock']=false;

}


if ($stock_multiple_locations->f('value')== 1)
{
	$o['stock_multiple_locations']=true;

}else{
	$o['stock_multiple_locations']=false;

}
if($in['hide_stock']){
	$o['show_stock']=false;

}else{
	$o['show_stock']=true;
	
}



$current_index = array_search($in['article_id'], $_SESSION['articles_id']);

$next = $current_index + 1;
$prev = $current_index - 1;


// $o['prev_link']	  		= 'index.php?do=article-article&article_id='. $_SESSION['articles_id'][$prev];
// $o['next_link']  		= 'index.php?do=article-article&article_id='. $_SESSION['articles_id'][$next];
$o['prev']      		= $_SESSION['articles_id'][$prev];
$o['next']      		= $_SESSION['articles_id'][$next];
$o['allow_article_packing']         = ALLOW_ARTICLE_PACKING;
$o['allow_article_sale_unit']       = ALLOW_ARTICLE_SALE_UNIT;
$o['page_title']	                = $page_title;
$o['account_number_format']         = ACCOUNT_NUMBER_FORMAT;
$o['nr_decimals']					= ARTICLE_PRICE_COMMA_DIGITS;



if(WAC && $in['article_id']  ){
	$o['is_wac']= true;
 }else{
	$o['is_wac']= false;
}
if(AAC){
	$o['is_aac']= true;

}else{
		$o['is_aac']= false;
}
if(AUTO_AAC){
	$o['is_auto_aac']= true;

}else{
	$o['is_auto_aac']= false;
	
}

$o['lang_code']               = $db->field("SELECT code FROM pim_lang WHERE sort_order='1'");


json_out($o);
function get_articlePriceVol($in){
$db = new sqldb();
$db2 = new sqldb();
$result = array('price_categories_vol'=>array(),
		);
$db->query("SELECT pim_article_prices.*
			FROM pim_article_prices 
			WHERE pim_article_prices.article_id = '".$in['article_id']."' AND pim_article_prices.base_price!=1 AND pim_article_prices.price_category_id='0'
			ORDER BY pim_article_prices.from_q");
$i=0;
while ($db->move_next()){

	$price = $db->f('price');
	$vat   =$db->f('total_price')- $db->f('price');
	$total_price = $db->f('total_price') ;
	$vat_percent=(($db->f('total_price')*100)/$db->f('price')) -100;

	$price_row = array(
	'min_qty'	    	=> $db->f('from_q'),
    'max_qty'	    	=> return_value($db->f('to_q'))!=0? $db->f('to_q') : '8',
	'si_price'			=> display_number_var_dec($price),
	'si_percent'		=> display_number($db->f('percent')),
	'si_vat'			=> display_number($vat),
	'si_vat_percent'    => display_number($vat_percent),
	'si_total_price'	=> display_number_var_dec($total_price),
	'article_price_id'  => $db->f('article_price_id'),
	'article_id'  => $in['article_id'],
	);
	array_push($result['price_categories_vol'], $price_row);
	$i++;
}


if($i==0){
	$result['is_prices_vol']=false;
}else{
	$result['is_prices_vol']=true;
}

   return $result;
}

 
function get_articlePrice($in){
$db = new sqldb();
$db2 = new sqldb();
$db3 = new sqldb();
$result = array('price_categories'=>array(),
		'si_vat_value'=>'');

$db->query("SELECT vats.value,vats.vat_id
			FROM vats
			INNER JOIN pim_articles ON pim_articles.vat_id = vats.vat_id
			WHERE pim_articles.article_id = '".$in['article_id']."' AND pim_articles.active='1' ");
$db->move_next();
$article_vat_value = $db->f('value');
$result['si_vat_value']=$article_vat_value;

$db->query("SELECT pim_article_price_category.*,pim_article_prices.price,pim_article_prices.total_price
			FROM pim_article_price_category 
			RIGHT JOIN pim_article_prices ON pim_article_price_category.category_id=pim_article_prices.price_category_id
			WHERE pim_article_prices.article_id = '".$in['article_id']."' AND base_price!=1
			ORDER BY pim_article_price_category.category_id");


while ($db->move_next()) {
	  $db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE category_id='".$db->f('category_id')."' AND article_id='".$in['article_id']."'");
	if($db2->move_next()){
        $is_custom=true;
       
        
        $type = $db2->f('type'); 
        $price_value = $db2->f('price_value'); 
        $price_value_type = $db2->f('price_value_type');       
    }else{
        $is_custom=false;

        $type = $db->f('type'); 
     
        $price_value_custom_fam=$db3->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$in['article_category_id']."' AND category_id='".$db->f('category_id')."'");
        if($price_value_custom_fam==NULL){
            $price_value = $db->f('price_value'); 
            $custom_fam=0;
         }else{
         	  $price_value = $price_value_custom_fam; 
         	  $custom_fam=1;
         }

        $price_value_type = $db->f('price_value_type');   
	}
   if($custom_fam==0){
	    $price = $db->f('price');  
	    $vat   = $article_vat_value * $price / 100;
	    $total_price = $db->f('total_price') ;	
    }else{
    	//we have to apply to the base price the category spec
    	 $cat_price_type=$db3->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$db->f('category_id')."'");
    	 $cat_type=$db3->field("SELECT type FROM pim_article_price_category WHERE category_id='".$db->f('category_id')."'");
    	 $price_value_type=$db3->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$db->f('category_id')."'");
    	 
    	 if($cat_price_type==2){
               $article_base_price=get_article_calc_price($in['article_id'],3);
             }else{
               $article_base_price=get_article_calc_price($in['article_id'],1);
             }

       		switch ($cat_type) {
				case 1:                  //discount
				if($price_value_type==1){  // %
					$price = $article_base_price - $price_value * $article_base_price / 100;
				}else{ //fix
					$price = $article_base_price - $price_value;
				}

				break;

				case 2:                 //profit margin
				if($price_value_type==1){  // %
					$price = $article_base_price + $price_value * $article_base_price / 100;
				}else{ //fix
					$price =$article_base_price + $price_value;
				}

				break;

			}
    	

	    //$price = $db->f('price');  
	    $vat   = $article_vat_value * $price / 100;
	    $total_price = $vat+$price ;	



    }


	$price_row = array(
	'is_custom'	             => $is_custom, 
	'si_price_cat_name'	            => $db->f('name'),
	'si_type'	        			=> get_price_category_type($type),
	'si_price_value'				=> display_number_var_dec($price_value),
	'si_price_value_type'			=> get_price_value_type($price_value_type),
	'si_sup_cat_id'		    		=> $db->f('category_id'),
	'si_price'			    		=> display_number_var_dec($price),
	'si_vat'			    		=> display_number($vat),
	'si_total_price'				=> display_number_var_dec($total_price),
	'price_cat_id'               => 'price_cat_'.$in['article_id'].'_'.$db->f('category_id'),
	'article_id'                 => $in['article_id'],
	'article_category_id'        => $in['article_category_id'],
	     );
	

	
	 array_push($result['price_categories'], $price_row);
     }
	 return $result;

  }

function get_articleDispatch($in){
$db = new sqldb();
$db2 = new sqldb();
$result = array('customer_stocks'=>array(),
		);
$is_data2=false;
$i=0;
//first we select own locations
/*$db->query("SELECT dispatch_stock.*,dispatch_stock_address.*
            FROM   dispatch_stock
            INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='1'
	        ");*/
$db->query("SELECT dispatch_stock.*,dispatch_stock_address.*
            FROM   dispatch_stock
            INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.customer_id='0'
	        ");
$i=0;
while ($db->move_next()) {

   
    $data_row=array(
		             'customer_name' => $db->f('naming'),
		             'current_quantity'      => remove_zero_decimals($db->f('stock')),
                     'address'=>  $db->f('address'),
                     'city'=>  $db->f('city'),
                     'zip'=> $db->f('zip'),
                     'country'=> get_country_name($db->f('country_id'))
                    ); 

		        
     array_push($result['customer_stocks'], $data_row);

 

   
    $i++;
}
//then customer locations
$db->query("SELECT dispatch_stock.*, customer_addresses.*,customers.name
            FROM   dispatch_stock
            INNER JOIN customers ON customers.customer_id=dispatch_stock.customer_id
            LEFT JOIN  customer_addresses ON  dispatch_stock.address_id=customer_addresses.address_id AND dispatch_stock.customer_id=customer_addresses.customer_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='0'
            ");

while ($db->move_next()) {

   
    $data_row_c=array(
                     'customer_name' => $db->f('name'),
                     'current_quantity'      => remove_zero_decimals($db->f('stock')),
                     'address'=> $db->f('address'),
                     'city'=> $db->f('city'),
                     'zip'=> $db->f('zip'),
                     'country'=> get_country_name($db->f('country_id'))
                     );
                     

    array_push($result['customer_stocks'], $data_row_c);
    $i++;
}



   return $result;
}

function get_articleTranslation($in)
{
    $db = new sqldb();
    $db2 = new sqldb();
    $result = array('translation'=>array(),
            );
    $internal_name=$db->field("SELECT internal_name FROM pim_articles where article_id='".$in['article_id']."'");
    $active_langs = $db->query("SELECT language, lang_id, default_lang,code FROM pim_lang WHERE active = '1' GROUP BY lang_id ORDER BY sort_order ASC");
    $i=1;
    $article_langs = array();
    while($active_langs->next()){
        $article_data = $db->query(" SELECT * FROM pim_articles_lang WHERE item_id ='".$in['article_id']."' AND lang_id='".$active_langs->f('lang_id')."' ");

        while($article_data->next()){
             $data_row_l=array(
                'lang_id'			=>	$active_langs->f('lang_id'),
                'lang_name'			=>	gm($active_langs->f('language')),
                'lang_img'          =>	'images/geo/'.$active_langs->f('code').'.png',
                'internal_name'		=>	$internal_name,
                'name'				=>	$article_data->f('name'),
                'name2'				=>	$article_data->f('name2'),
                'description'		=>	$article_data->f('description'),
                'block_data'        => $active_langs->f('lang_id')==$default_lang ?'readonly="readonly" class="block_edit" ' :''
            );

            // this is required since AKTI-4802 - dummy data was present for article_id = 1 when creating a new account
            // this part of code is for accounts older than 11/09/2021
             if ($in['article_id'] == 1) {
                 if (empty($result['translation'])) {
                     $article_langs[] = $data_row_l['lang_id'];
                     array_push($result['translation'], $data_row_l);
                 } else {
                     if(!in_array($data_row_l['lang_id'], $article_langs)) {
                         $article_langs[] = $data_row_l['lang_id'];
                         array_push($result['translation'], $data_row_l);
                     }
                 }
             } else { //finish filter for article_id = 1
                 array_push($result['translation'], $data_row_l);
             }
        }
    }
    return $result;
}

function get_articleBatch($in){
$db = new sqldb();
$db2 = new sqldb();
$result = array('batch'=>array(
		                          'use_batch_no'=>false,
		                          'items'=>array())
);
if($in['parent_article_id']){
	$in['article_id']=$in['parent_article_id'];
}
$db->query("SELECT pim_articles.use_batch_no FROM pim_articles WHERE article_id = '".$in['article_id']."'");
if($db->f('use_batch_no')){
	$result['batch']['use_batch_no']=true;
  }
$art_no_used = $db->field("SELECT COUNT(id) FROM batches WHERE article_id = '".$in['article_id']."' ");


$result['batch']['disable_use_batch_no']=($result['batch']['use_batch_no'] == 1 && $art_no_used > 0) ? true : false;

$filter = " article_id='".$in['article_id']."' ";
$having_filter="";

if($in['status_id']){
	switch($in['status_id']){
		case '1':
			$having_filter=" HAVING status_id='3' "; 				
			break;
		case '2':
			$having_filter=" HAVING in_stock>'0' "; 
			break;
		case '3':
			$having_filter=" HAVING status_id!='3' AND in_stock='0' "; 
			break;
	}
}

$db->query("SELECT batches.*,batch_number_status.name, SUM( quantity ) AS quantity, SUM( in_stock ) AS in_stock, max(status_id) as status_id, count(batches.id) as nr_ids
			FROM batches
			LEFT JOIN batch_number_status
			ON batches.status_id = batch_number_status.id
			WHERE ".$filter." GROUP BY batch_number, date_exp ".$having_filter." ");
$j=0;
$p_order_id=0;
while($db->move_next()){
	//$in_details = gm($db->f('status_details_1'));	
	$in_details = ($db->f("status_details_1") != 'Added manually')? $db->f("status_details_1"):gm($db->f("status_details_1"));
	if($db->f('p_delivery_id')){
		$p_order_serial_number = $db->f('status_details_1');
		$p_order_id = $db2->field("SELECT p_order_id FROM pim_p_order_deliveries WHERE delivery_id = '".$db->f('p_delivery_id')."' ");
		//$in_details = '<a href="index.php?do=order-p_order&p_order_id='.$p_order_id.'" target="_blank" >'.$p_order_serial_number.'</a>';	
		$in_details = 	$db2->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$p_order_id."' ");
	}
	$out_details = $db->f('status_details_2');
	if($db->f('delivery_id')){
		$order_serial_number = $db->f('status_details_2');
		$order_id = $db2->field("SELECT order_id FROM pim_order_deliveries WHERE delivery_id = '".$db->f('delivery_id')."' ");
		$out_details = '<a href="index.php?do=order-order&order_id='.$order_id.'" target="_blank" >'.$order_serial_number.'</a>';		
	}
	$drop_info = array('drop_folder' => 'articles', 'item_id' =>  $db->f('id'), 'is_batch'=>1);
	$list_ids='';
	$multiple_ids=false;
	if($db->f('nr_ids')>1){
		$multiple_ids=true;
		$ids = $db2->query("SELECT id from batches WHERE batch_number='".$db->f('batch_number')."' AND date_exp ='".$db->f('date_exp')."'");
		while($db2->move_next()){
			$list_ids .= $ids->f('id').';';
		}

	}
	$in_stock= ($db->f('in_stock')>0)? true: false;
	
	$data_row=array(
		'batch_number'		=> $db->f('batch_number'),
		'id'				=> $db->f('id'),
		'multiple_ids'		=> $multiple_ids,
		'list_ids'			=> $list_ids,
		//'status_name'		=> $db->f('name'),
		'status_name'		=> $db->f('status_id')=='3'? gm('Not available') : ($db->f('in_stock')>0? gm('In stock'): gm('Sold') ),
		//'status_id'		    => $db->f('status_id'),
		'status_id'			=> $db->f('status_id')=='3'? '3':( $db->f('in_stock')>0? '1': '2'),
		'status'			=> ($db->f('in_stock')>0) ? 'green' : ($db->f('status_id') == 2 ? 'red' : 'orange' ),

		'date_in'			=> $db->f('date_in') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_in')),
		'date_out'			=> $db->f('date_out') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_out')),		
		'date_exp'			=> $db->f('date_exp') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_exp')),		
		'in_details'		=> $in_details,
		'p_order_id'        => $p_order_id,
		'p_delivery_id'    =>$db->f('p_delivery_id'),
		'out_details'		=> $out_details,		
		'quantity'			=> display_number($db->f('quantity')),
		'in_stock'			=> display_number($db->f('in_stock')),
		'show_edit'			=> $in_stock? true : false,
		'drop_info'			=> htmlentities(json_encode($drop_info)),
		'delete_link' 		=> array('do'=>'article-article-article-delete_serial_number', 'article_id'=>$db->f('article_id'),'id'=>$db->f('id')),
	);

     array_push($result['batch']['items'], $data_row);
}



   return $result;
 }



function get_articleSerial($in){
$db = new sqldb();
$db2 = new sqldb();
$result = array('serial'=>array(
		                          'use_serial_no'=>false,
		                          'items'=>array())
);
if($in['parent_article_id']){
	$in['article_id']=$in['parent_article_id'];
}
$db->query("SELECT pim_articles.use_serial_no,stock FROM pim_articles WHERE article_id = '".$in['article_id']."'");
if($db->f('use_serial_no')){
	$result['serial']['use_serial_no']=true;
  }

$result['serial']['new_stock']=$db->f('stock');

$art_no_used = $db->field("SELECT COUNT(id) FROM serial_numbers WHERE article_id = '".$in['article_id']."' ");


$result['serial']['disable_use_serial_no']=($result['serial']['use_serial_no'] == 1 && $art_no_used > 0) ? true : false;





$filter = " article_id='".$in['article_id']."' ";

$db->query("SELECT serial_numbers.*,serial_number_status.name
			FROM serial_numbers
			LEFT JOIN serial_number_status
			ON serial_numbers.status_id = serial_number_status.id
			WHERE ".$filter." ");
while($db->move_next()){
	
	//$in_details = gm($db->f('status_details_1'));	
	$in_details = ($db->f("status_details_1") != 'Added manually')? $db->f("status_details_1"):gm($db->f("status_details_1"));
	$supplier_name ='';
	if($db->f('p_delivery_id')){
		$p_order_serial_number = $db->f('status_details_1');
		$p_order_id = $db2->field("SELECT p_order_id FROM pim_p_order_deliveries WHERE delivery_id = '".$db->f('p_delivery_id')."' ");
		//$in_details = '<a href="index.php?do=order-p_order&p_order_id='.$p_order_id.'" target="_blank" >'.$p_order_serial_number.'</a>';
		$in_details = 	$db2->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$p_order_id."' ");	
		$supplier_name = $db2->field("SELECT customer_name FROM pim_p_orders WHERE p_order_id = '".$p_order_id."' ");	
	}
	$out_details = $db->f('status_details_2');
	$out_serial_number = $db->f('status_details_2');
	$customer_name ='';
	if($db->f('delivery_id')){
		$order_id = $db2->field("SELECT order_id FROM pim_order_deliveries WHERE delivery_id = '".$db->f('delivery_id')."' ");
		$out_details = '<a href="index.php?do=order-order&order_id='.$order_id.'" target="_blank" >'.$out_serial_number.'</a>';	
		$customer_name = $db2->field("SELECT customer_name FROM pim_orders WHERE order_id = '".$order_id."' ");	
	}
	if($db->f('project_id')){
		$out_details = '<a href="index.php?do=project-project&project_id='.$db->f('project_id').'" target="_blank" >'.$db->f('status_details_2').'</a>';
	}
	if($db->f('service_id')){
		$out_details = '<a href="index.php?do=maintenance-services&service_id='.$db->f('service_id').'" target="_blank" >'.$db->f('status_details_2').'</a>';
	}
	$stock_location='';
	if($db->f('address_id')){
		$stock_location= $db2->field("SELECT naming FROM dispatch_stock_address WHERE address_id = '".$db->f('address_id')."' ");
	}
	 $data_row=array(
		'serial_number'		=> $db->f('serial_number'),
		'id'		=> $db->f('id'),
		'status_name'		=> gm($db->f('name')),
		'status_id'         => $db->f('status_id'), 
		'status'			=> $db->f('status_id') == 1 ? 'green' : ($db->f('status_id') == 2 ? 'red' : 'orange' ),
		'date_in'			=> $db->f('date_in') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_in')),
		'date_out'			=> $db->f('date_out') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_out')),		
		'in_details'		=> $in_details,
		'p_delivery_id'     => $db->f('p_delivery_id'),
		'out_details'		=> $out_serial_number,
		'p_order_id'		=> $p_order_id,
		'order_id'		    => $order_id,
		'stock_location'	=> $stock_location,
		'customer_name'		=> $customer_name,
		'supplier_name' 	=> $supplier_name
	  );

     array_push($result['serial']['items'], $data_row);

     
	
}
/* if($in['search']){
     		$filterBy = $in['search'];
			$new = array_filter($result['serial']['items'], function ($var) use ($filterBy) {
			    return (strpos($var['customer_name'], $filterBy) || strpos($var['serial_number'] , $filterBy) || strpos($var['in_details'], $filterBy) || strpos($var['out_details'], $filterBy) );
			});
			$result['serial']['items'] =$new;
     }*/


   return $result;
 }


function get_articleCombine($in){
$db = new sqldb();
$db2 = new sqldb();
$result = array('composed'=>array(
		                          'use_combined'=>false,
		                          'articles'=>array())
);
if($in['parent_article_id']){
	$in['article_id']=$in['parent_article_id'];
}

$db->query("SELECT pim_articles.use_combined FROM pim_articles WHERE article_id = '".$in['article_id']."'");
if($db->f('use_combined')){
	$result['composed']['use_combined']=true;
  }

   $result['composed']['articles_list']			=  get_articles_list($in);
   

   $filter = " parent_article_id='".$in['article_id']."'  ORDER BY pim_articles_combined.`sort_order` ASC";
    $db->query("SELECT pim_articles.article_threshold_value,pim_articles.stock,pim_articles.hide_stock,pim_articles.article_id,pim_articles.item_code,pim_articles.internal_name,pim_articles_combined.parent_article_id,pim_articles_combined.pim_articles_combined_id,pim_articles_combined.quantity,pim_articles_combined.visible,pim_articles_combined.sort_order
         FROM pim_articles
			INNER JOIN pim_articles_combined ON pim_articles_combined.article_id=pim_articles.article_id
			WHERE $filter
		  
			");

	$stock = 0;
    $quantity =0;
    $stock = $db->f('stock');
    $quantity = $db->f('quantity');
	if($quantity){
		$potential_stock= $stock/$quantity;
	}else{
		$potential_stock= 0;
	}

  while($db->move_next()){
    $potential_stock=min($potential_stock, floor($db->f('stock')/$db->f('quantity')));
    $data_row=array(
                'name'		  				=> $db->f('internal_name'),
				'article_id'  				=> $db->f('article_id'),
				'parent_article_id'  		=> $db->f('parent_article_id'),
				'quantity'  				=> remove_zero_decimals($db->f('quantity')),
				'stock'  					=> remove_zero_decimals($db->f('stock')),
				'show_stock'  				=> $db->f('hide_stock') ? false:true,
				'pim_articles_combined_id'  => $db->f('pim_articles_combined_id'),
				'code'		  				=> $db->f('item_code'),
				'allow_stock'         		=> ALLOW_STOCK,
				'stock_multiple_locations'	=> STOCK_MULTIPLE_LOCATIONS,
				'visible'		  			=> $db->f('visible')? true:false,
				'text_visible'				=> $db->f('visible')? gm("Visible on PDF. Click to change it to 'Not visible on PDF'"):gm("Not visible on PDF. Click to change it to 'Visible on PDF'"),
				'sort_order'		  		=> $db->f('sort_order'),
                     );

     if($db->f('stock') <= 0){
     	   $data_row['stock_status']='text-danger';
          
     }elseif($db->f('stock') > 0  && $db->f('stock') <= $db->f('article_threshold_value') ){
     	   $data_row['stock_status']='text-warning';
     }else{
     	   $data_row['stock_status']='text-success';

     }
                     

    array_push($result['composed']['articles'], $data_row);
   }
   $result['composed']['potential_stock']=$potential_stock;

   return $result;
 }

 function get_supplierRef($in){

$db = new sqldb();
$db2 = new sqldb();

$l_r = ROW_PER_PAGE;

if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$result = array('supplierRef'=>array( 'references'=>array()) );


$result['article_id'] = $in['article_id'];

$result['cc'] = get_cc($in,true,false);
$filter = " pim_articles.article_id='".$in['article_id']."' ";

$article_base_price = $db->field("SELECT price FROM pim_articles WHERE pim_articles.article_id='".$in['article_id']."' ");
$result['supplierRef']['base_price'] = display_number_var_dec($article_base_price);

/*$q = strtolower($in["term"]);
if($q){
	$filter .=" AND customers.name LIKE '%".addslashes($q)."%' ";
}*/
$max_rows = $db->field("SELECT count(pim_articles.article_id)
			FROM pim_article_references
			LEFT JOIN pim_articles ON pim_article_references.article_id=pim_articles.article_id
			LEFT JOIN customers ON customers.customer_id = pim_article_references.supplier_id
			WHERE $filter ");

$db->query("SELECT pim_articles.article_id,pim_article_references.supplier_id, pim_article_references.supplier_ref, customers.name as supplier_name, pim_article_references.purchasing_price
         FROM pim_article_references
			LEFT JOIN pim_articles ON pim_article_references.article_id=pim_articles.article_id
			LEFT JOIN customers ON customers.customer_id = pim_article_references.supplier_id
			WHERE $filter ORDER BY customers.name LIMIT ".$offset*$l_r.",".$l_r
		  
			);

  while($db->move_next()){

    $data_row=array(
                'name'		  		=> $db->f('supplier_name'),
				'supplier_id'  		=> $db->f('supplier_id'),
				'article_id'  		=> $db->f('article_id'),
				'supplier_ref'  		=> $db->f('supplier_ref'),
				'purchasing_price'	=> display_number_var_dec($db->f('purchasing_price')),
  );

                   

    array_push($result['supplierRef']['references'], $data_row);
   }

 $result['max_rows'] = $max_rows;
 $result['lr']		= $l_r;

   return $result;
 }

function get_articles_list($in)
	{
		
		$db = new sqldb();
        $db2 = new sqldb();
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id ';
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id';

			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount';
		}

		$filter.=" 1=1 ";
        $filter= "pim_articles.is_service = '0'";
		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

		$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 	
		  		$customer_custom_article_price=$db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       
		   	}

			$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	'name'						=> $article->f('internal_name'),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)), 
				'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		array_push($articles['lines']);

		return $articles;
	}

	function get_articleTaxes($in){
	$db = new sqldb();
	$result = array();

	$db->query("SELECT pim_article_tax.*,vats.value,pim_articles_taxes.article_id,pim_articles_taxes.tax_id AS active_tax, pim_article_tax_type.name
            FROM pim_article_tax
            INNER JOIN pim_articles_taxes on pim_articles_taxes.tax_id=pim_article_tax.tax_id AND pim_articles_taxes.article_id='".$in['article_id']."'
            LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
            LEFT JOIN vats ON vats.vat_id = pim_article_tax.vat_id
           ");

	while($db->move_next()){
		$taxe_row=array(
                        'taxe_code' =>$db->f('code'),
                        'tax_id'    =>$db->f('tax_id'),
                        'tax_value' =>place_currency(display_number($db->f('amount'))),
                        'tax_type'  =>$db->f('name'),
                        'tax_vat'	=>display_number($db->f('value')).' %',
                        'apply_to'  =>get_article_tax_application_type($db->f('apply_to')),
			);
	  
           array_push($result, $taxe_row);
	}
	
	return $result;
	}

	function get_allTaxes($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$result = array('taxes'=>array());
	$db->query("SELECT pim_article_tax.*,vats.value, pim_article_tax_type.name
            FROM pim_article_tax
            LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
            LEFT JOIN vats ON vats.vat_id = pim_article_tax.vat_id
           ");

	while($db->move_next()){
		if ($in['article_id']){
			$db2->query("SELECT * FROM pim_articles_taxes WHERE article_id='".$in['article_id']."' AND tax_id='".$db->f('tax_id')."'");
		        if($db2->move_next()){
		            $applied_tax = 1;
		        }else{
		            $applied_tax = 0;
		        }
		}
		$taxe_row=array(
                        'taxe_code' =>$db->f('code'),
                        'tax_id'    =>$db->f('tax_id'),
                        'tax_value' =>place_currency(display_number($db->f('amount'))),
                        'tax_type'  =>$db->f('name'),
                        'tax_vat'	=>display_number($db->f('value')).' %',
                        'applied_tax'=> $applied_tax,
                        'apply_to'  =>get_article_tax_application_type($db->f('apply_to')),
			);
	  
           array_push($result['taxes'], $taxe_row);
	}
	
	return $result;
	}

	function get_articleCustom($in){
	if (!$in['article_id']){
		msg::error(gm('Invalid ID'),'error');
			return json_out($in);
	}
	$db = new sqldb();
	$result = array('custom'=>array(
		                          'use_custom_no'=>false,
		                          'items'=>array(),
		                          'cc'=>get_cc($in,true,false))
	);

	$l_r =ROW_PER_PAGE;
		$order_by = " ORDER BY  customers.name  ";
		

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		if(!empty($in['search'])){
	   		$filter = " AND ( name LIKE '%".$in['search']."%') ";
	 	}

	
		$custom_prices = $db->query("SELECT customer_custom_article_price.*, pim_articles.price AS p_price,customers.name,acc_manager_name, c_type_name, is_supplier,is_customer FROM customer_custom_article_price
									LEFT JOIN pim_articles ON pim_articles.article_id=customer_custom_article_price.article_id
									INNER JOIN customers ON customers.customer_id=customer_custom_article_price.customer_id
									WHERE customers.is_admin='0' AND customers.front_register='0' AND customers.active='1' 
									AND customer_custom_article_price.article_id='".$in['article_id']."' $filter");
		while($custom_prices->move_next()){
			
			if($custom_prices->f('is_supplier')==1 && $custom_prices->f('is_customer')==1) {
				$class='akti-icon akti-icon-16 o_akti_custopplier';
				$class_name = gm('Customer').' & '.gm('Supplier');
			}elseif($custom_prices->f('is_customer')==0 && $custom_prices->f('is_supplier')==0){
				$class='';
				$class_name='';
			}elseif($custom_prices->f('is_supplier')==1 && $custom_prices->f('is_customer')==0){
				$class='akti-icon akti-icon-16 o_akti_delivery';
				$class_name = gm('Supplier');
			}elseif($custom_prices->f('is_customer')==1 && $custom_prices->f('is_supplier')==0){
				$class='akti-icon akti-icon-16 o_akti_customer';
				$class_name = gm('Customer');
			}
		

			$linie = array(
				'name'						=> $custom_prices->f('name'),
				'acc_manager_name'			=> $custom_prices->f('acc_manager_name'),
				'type_rel'					=> $custom_prices->f('c_type_name'),
				'base_price'				=> display_number($custom_prices->f('p_price')),
				'custom_price'				=> display_number($custom_prices->f('price')),
				'type'						=> $class,
				'type_name'					=> $class_name,
				'type2'						=> $custom_prices->f('type')==1 ? 'users' : 'building',
				'type2_name'				=> $custom_prices->f('type')==1 ? gm('Individual') : gm('Company'),
				'confirm'					=> gm('Confirm'),
				'ok'						=> gm('Ok'),
				'cancel'					=> gm('Cancel'),
			
				'delete_link'				=> array('do'=>'article-add-article-delete_custom_price', 'customer_id'=>$custom_prices->f('customer_id'),  'article_id'=> $in['article_id'] ),
				);
			array_push($result['custom']['items'], $linie);
		}
	
	return $result;
	}

	function get_cc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	
	$db_user = new sqldb($database_users);
	$filter =" AND is_admin='0' ";

if($q){
	$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
}

if($is_admin){
	$filter =" AND is_admin='".$is_admin."' ";
}
if($in['supplier']){
	$filter .= " AND customers.is_supplier='1'";
}

if($in['current_id']){
	$filter .= " AND customers.customer_id !='".$in['current_id']."'";
}
//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
$db= new sqldb();

$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 $filter GROUP BY customers.customer_id ORDER BY customers.name limit 5 ")->getAll();

$result = array();
foreach ($cust as $key => $value) {
	$cname = trim($value['name']);

	$result[]=array(
		"id"					=> $value['customer_id'],
		"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
		"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
		"ref" 					=> $value['our_reference'],
		"currency_id"			=> $value['currency_id'],
		"lang_id" 				=> $value['internal_language'],
		"identity_id" 			=> $value['identity_id'],
		'contact_name'			=> $value['acc_manager_name'],
		'country'				=> $value['country_name']
	);

}
	if($q){
		array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	}else{
		array_push($result,array('id'=>'99999999999','value'=>''));
	}
	array_push($result,array('id'=>'99999999999','value'=>''));
	
	return json_out($result, $showin,$exit);

}

function get_suppliers_list($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" AND is_admin='0' AND customers.is_supplier='1' ";
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
	}
	if($in['supplier_name1']){
		$supplier_name1=preg_split('/[\s,]+/', $in['supplier_name1'], 3);
		if(count($supplier_name1)>2){
			$in['supplier_name1']=implode(" ",array_slice($supplier_name1,0,2));
		}
		$filter .=" AND customers.name LIKE '%".addslashes(htmlspecialchars_decode($in['supplier_name1']))."%'";
	}
	if($in['current_id']){
		$filter .= " AND customers.customer_id !='".$in['current_id']."'";
	}
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	
	$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
						acc_manager_name, customers.country_name
						FROM customers 
						WHERE customers.active=1 $filter GROUP BY customers.customer_id ORDER BY customers.name limit 5 ")->getAll();

	$result = array();
	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);

		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 				=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id" 			=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);

	}

	if($q){
		array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	}else{
		array_push($result,array('id'=>'99999999999','value'=>''));
	}
	return json_out($result, $showin,$exit);
}
function get_articleVariants(&$in){
    $db= new sqldb();
    $articles = $db->query("SELECT pim_articles.article_id,pim_articles.internal_name,pim_articles.item_code,pim_articles.stock,pim_articles.price,pim_article_variants.id,pim_article_variants.type_id,pim_articles.article_threshold_value
        FROM pim_article_variants
        INNER JOIN pim_articles ON pim_article_variants.article_id=pim_articles.article_id        
        WHERE pim_article_variants.parent_article_id='".$in['article_id']."' ")->getAll();

    $result = array('articleVariants'=>array('list'=>array()));
    $result['articleVariants']['show_stock'] = (ADV_PRODUCT=='1' && ALLOW_STOCK == '1') ? true : false;
    foreach ($articles as $key => $value) {
    	if($value['stock'] <= 0){
           $stock_status='text-danger';        
	     }elseif($value['stock'] > 0  && $value['stock'] <= $value['article_threshold_value']){
	           $stock_status='text-warning';
	     }else{
	           $stock_status='text-success';
	     }
        $item=array(
        	"id"					=> $value['id'], 
            "article_id"           => $value['article_id'],
            'name'                  => htmlspecialchars_decode($value['internal_name']),
            'code'                  =>    $value['item_code'],
            'stock'             => remove_zero_decimals($value['stock']),
            'stock_status'		=> $stock_status,
            'base_price'        => display_number_var_dec($value['price']),
            'type_id'			=> $value['type_id'] ? $value['type_id'] : undefined, 
        );
        array_push($result['articleVariants']['list'],$item);
    }
    return $result;
}

