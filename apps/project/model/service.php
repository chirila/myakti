<?php
/**
 * Service class
 *
 * @author      Arkweb SRL
 * @link        https://app.akti.com/
 * @copyright   [description]
 * @package 	Service
 */
class service
{
	var $pag = 'service';
	var $field_n = 'article_id';

	/**
	 * [article description]
	 * @return [type] [description]
	 */
	function service()
	{
		$this->db = new sqldb;
		$this->db2 = new sqldb;
	
	}

	/**
	 * Add service
	 * @param array $in
	 * @return boolean
	 */
	function add(&$in)
	{
	    if(!$this->add_validate($in)){
			return false;
		}
		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET
															
															internal_name       		= '".$in['internal_name']."',
															item_code  					= '".$in['item_code']."',
															article_category_id       	= '".$in['article_category_id']."',
															d_price			        	= '".return_value($in['d_price'])."',
															h_price			        	= '".return_value($in['h_price'])."',
															ledger_account_id			= '".$in['ledger_account_id']."',
															billable			        = '".$in['billable']."',
															is_service			        = '2' ");
		
       	//update_articles($in['article_id'],DATABASE_NAME);
		msg::success(gm("Service succesfully added."),'success');
        return true;
	}
		/**
	 * update service
	 * @param array $in
	 * @return boolean
	 */
	function update(&$in)
	{
	    if(!$this->update_validate($in)){
			return false;
		}
		$this->db->query("UPDATE pim_articles SET
															
															internal_name       		= '".$in['internal_name']."',
															item_code  					= '".$in['item_code']."',
															article_category_id       	= '".$in['article_category_id']."',
															d_price			        	= '".return_value($in['d_price'])."',
															h_price			        	= '".return_value($in['h_price'])."',
															ledger_account_id			= '".$in['ledger_account_id']."',
															billable			        = '".$in['billable']."',
															is_service			        = '2' 
						 WHERE article_id=	'".$in['article_id'] ."'");
		
        //update_articles($in['article_id'],DATABASE_NAME);
		msg::success(gm("Service succesfully updated."),'success');
        return true;
	}
	/**
	 * Validate add article
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in)
	{

		$v = new validation($in);
		
		if (ark::$method == 'update') {

			$option = ".( article_id != '" . $in['article_id'] . "')";
		} else {
			$option = "";
		}
		
		$v -> f('item_code', 'Item Code', 'required:unique[pim_articles.item_code' . $option . ']');

		$v -> f('internal_name', 'Internal name', 'required');
	
		return $v -> run();
	}
		/**
	 * Update validate
	 * @param  array $in
	 * @return boolean
	 */
	function update_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('article_id', 'Article Id', 'required:exist[pim_articles.article_id]');

		return $this -> add_validate($in);

	}
	/**
	 * activate article function
	 *
	 * @param  array $in
	 * @return boolean
	 */
	function activate(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE pim_articles SET active='1' WHERE article_id='".$in['article_id']."' ");
		msg::$success = "Service activated.";
		Sync::end($in['article_id']);
		return true;
	}

	/**
	 * Validate delete data
	 * @param  array $in
	 * @return boolean
	 */
	function delete_validate(&$in)
	{
		$is_ok = true;
		if(!$in['article_id']){
			$in['error'] = "Invalid ID.";
			return false;
		}
		else{
			$this->db->query("SELECT article_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
			if(!$this->db->move_next()){
				$in['error'] = "Invalid ID.";
				return false;
			}
		}

		return $is_ok;
	}
	/**
	 * archive article function
	 *
	 * @param  array $in
	 * @return boolean
	 */
	function archive(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
		
		$this->db->query("UPDATE pim_articles SET active='0' WHERE article_id='".$in['article_id']."' ");
		msg::$success = "Article archived.";
		
		return true;
	}
		/**
	 * Delete articles
	 * @param  array $in
	 * @return boolean
	 */
	function delete(&$in)
	{
		if(!$this->delete_validate($in)){
			return false;
		}
//                global $cfg;

		
		$this->db->query("DELETE FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		
		
   

	
		return true;
	}


}	
