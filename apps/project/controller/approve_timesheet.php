<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

	if(!is_numeric($in['start'])){
		$now 				= time();
		$today_of_week 		= date("N", $now);
		$month        		= date('n');
		$year         		= date('Y');
		$week_start 		= mktime(0,0,0,$month,(date('j')-$today_of_week+1),$year);
		$week_end 			= $week_start + 604799;
	} else {
		$week_start 		= $in['start'];
		$week_end 			= $in['start'] + 604799;
	}

	$result=array(
		'SELECT_DAY'		=> build_days_of_the_week($week_start,$week_start),
		'user_id'			=> $in['user_id'],
		'week_start'		=> (string)$week_start,
		'week_end'			=> (string)($week_end-86399)
	);


return json_out($result);
?>