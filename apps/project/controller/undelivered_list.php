<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$result=array('list'=>array());
if(!isset($in['project_id']) || !is_numeric($in['project_id'])){
	return json_out($result);
}
$is_data = false;

$customer_name 	= $in['customer_name'];
$serial_number 	= $in['serial_number'];
$project_id 		= $in['project_id'];
$delivery_status 	= $in['delivery_status'];

$undelivered_articles = $db->query("SELECT * FROM project_articles WHERE project_id='".$in['project_id']."' AND delivered='0' ORDER BY a_id DESC");

while ($undelivered_articles->move_next()) {
	$j++;
	$list=array(
		'serial_number'					=> $in['serial_number'],
		'customer_name'					=> $in['customer_name'],
		'project_id'					=> $in['project_id'],
		'article_name'					=> $undelivered_articles->f('name'),
		'quantity'						=> $undelivered_articles->f('quantity'),
		'price'						=> place_currency(display_number($undelivered_articles->f('price'))),
		'edit_link'						=> 'index.php?do=project-project&project_id='.$undelivered_articles->f('project_id'),

	);
	array_push($result['list'], $list);
}
if($j>0){
	$is_data = true;
}


	$result['is_data']	= $is_data;

return json_out($result);