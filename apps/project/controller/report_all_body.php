<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
session_start();

$db =  new sqldb();

$db2=new sqldb();
$db3 = new sqldb();
$view_html = new at(ark::$viewpath.'report_all_body.html');
	
	$project_man='';
	$man_array=array();
	foreach ($value as $k => $val) {
		$c_name = $val['name'];
		if(!$val['quantity'] || !$val['unit_price']){
			continue;
		}
		if(!array_key_exists($val['project_id'],$man_array)){
			$man_array[$val['project_id']]=array();
		}
		$data2[$key] = $inc;
		$inc++;
		$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$val['project_id']."' AND manager='1' ");
		while($proj_name->next()){
			if(!in_array($proj_name->f('user_id'),$man_array[$val['project_id']])){
				$project_man.=get_user_name($proj_name->f('user_id')).',';
				$man_array[$val['project_id']][]=$proj_name->f('user_id');
			}
		}
		$view_html->assign(array(
			'row_nr'				=> $i+1,
			'row_description'		=> $val['description'],
			'row_unitmeasure'		=> 'MT',
			'row_quantity'			=> display_number($val['quantity']),
			'row_unit_price'		=> place_currency(display_number($val['unit_price'])),
			'row_amount'			=> place_currency(display_number($val['quantity']*$val['unit_price'])),
		),'invoice_row');

		$i++;		
	 	$view_html->loop('invoice_row','customers');
		$total += $val['quantity']*$val['unit_price'];
	}

		if($expenses_array[$key]){
			$is_expense = true;
		}else{
			$is_expense = false;
		}
		$expense_total = 0;
		foreach ($expenses_array[$key] as $key2 => $expenses) {
			$expense_total += $expenses['expense_amount'];
			$view_html->assign(array(
				'expense_description' 	=> $expenses['project_name'].': '.( $expenses['expense_name'] ? $expenses['expense_name'] : gm('Ad Hoc Expense') ),
				'expense_amount' 		=> place_currency(display_number($expenses['expense_amount'])),
			),'expenses');
			$view_html->loop('expenses','customers');
		}

		//customer purchases loop
		if($purchases_array[$key]){
			$is_purchase = true;
		}else{
			$is_purchase = false;
		}
		$purchase_total = 0;
          
		foreach ($purchases_array[$key] as $key4 => $purchases) {
			$purchase_total += $purchases['purchase_amount'];
			$view_html->assign(array(
				'purchase_description' 	=> $purchases['project_name'].': '.$purchases['purchase_description'],
				'purchase_amount' 	=> place_currency(display_number($purchases['purchase_amount'])),
			),'purchases');
			$view_html->loop('purchases','customers');
		}

		//articles amount loop
		$articles_total=0;
		if($articles_array[$key]){
			$is_articles=true;
		}else{
			$is_articles=false;
		}
		foreach ($articles_array[$key] as $key5 => $articles) {
			$articles_total += $articles['amount'];
			$view_html->assign(array(
				'description' 	=> $articles['project_name'].': '.$articles['name'],
				'quantity'		=> display_number($articles['quantity']),
				'price'		=> place_currency(display_number($articles['price'])),
				'amount' 		=> place_currency(display_number($articles['amount'])),
			),'articles');
			$view_html->loop('articles','customers');
		}

/*if($c_name == null){
	$c_name = $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$key."' ");		
}*/

$view_html->assign(array(
	'account_logo'        => $upload_path.'../img/no-logo.png',
	'billing_address_txt' => get_label_txt('billing_address',$in['lid']),
	'invoice_note_txt'    => get_label_txt('invoice_note',$in['lid']),
	'invoice_txt'         => get_label_txt('invoice',$in['lid']),
	'date_txt'            => get_label_txt('date',$in['lid']),
	'customer_txt'        => get_label_txt('customer',$in['lid']),
	'item_txt'            => get_label_txt('item',$in['lid']),
	'unitmeasure_txt'     => get_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => get_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => get_label_txt('unit_price',$in['lid']),
	'amount_txt'          => get_label_txt('amount',$in['lid']),
	'subtotal_txt'        => get_label_txt('subtotal',$in['lid']),
	'discount_txt'        => get_label_txt('discount',$in['lid']),
	'vat_txt'             => get_label_txt('vat',$in['lid']),
	'payments_txt'        => get_label_txt('payments',$in['lid']),
	'amount_due_txt'      => get_label_txt('amount_due',$in['lid']),
	'notes_txt'           => get_label_txt('notes',$in['lid']),
	'notes2_txt'          => get_label_txt('invoice_note2',$in['lid']),
	'duedate_txt'		  => get_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => get_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => get_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => get_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => get_label_txt('iban',$in['lid']),
	'phone_txt'			  => get_label_txt('phone',$in['lid']),
	'fax_txt'			  => get_label_txt('fax',$in['lid']),
	'url_txt'			  => get_label_txt('url',$in['lid']),
	'email_txt'			  => get_label_txt('email',$in['lid']),
	'our_ref_txt'		  => get_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => get_label_txt('your_ref',$in['lid']),
	'vat_number_txt'      => get_label_txt('vat_number',$in['lid']),
	'payment_inst_txt'	  => get_label_txt('pay_instructions',$in['lid']),
	
	'seller_name'         	=> $c_name,
	'proj_man'			=> rtrim($project_man,","),
	'total'		      => place_currency(display_number($total)),
	'articles_total'		=> place_currency(display_number($articles_total)),
	'expenses_total'		=> place_currency(display_number($expense_total)),
	'purchases_total'		=> place_currency(display_number($purchase_total)),
	'is_article'		=> $is_articles,
	'is_expense' 		=> $is_expense,
	'is_purchase' 		=> $is_purchase,
	
),'customers');

$view_html->loop('customers');

return $view_html->fetch();
?>