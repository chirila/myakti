<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$special_template = array(4,5); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Timesheet');
$pdf->SetSubject('Timesheet');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//                   15                  15                 15

$pdf->SetMargins(10, 10, 10);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

// $pdf->SetPrintfooter(false);

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);
$pdf->AddPage();

$html = include('expenses_report_print_body.php');
// echo "<pre>";print_r($in);exit();
if($in['print']){
	die($html);
}
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();
if($in['do']=='project-expenses_report_print'){
	doQueryLog();
   $pdf->Output('expenses.pdf','I');
}else{
   $pdf->Output(__DIR__.'/../../../expenses_report.pdf', 'F');
}
?>