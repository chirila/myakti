<?php if(!defined('BASEPATH')) exit('No direct script access allowed');



$db = new sqldb();
$db2 = new sqldb();

$l_r=ROW_PER_PAGE;
$order_by = " ORDER BY pim_articles.internal_name  ";

$order_by_array = array('internal_name','article_category','h_price','d_price');

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

//FILTER LIST
$selected =array();
$filter = '1=1';
$filter_attach = '';
$filter.= " AND pim_articles.is_service='2' ";

if(!$in['archived']){
	$filter.= " AND pim_articles.active='1' ";
}else{
	$filter.= " AND pim_articles.active='0' ";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}


if(!empty($in['search'])){
	$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
	$arguments_s.="&search=".$in['search'];
}



if($in['article_category_id']){
	$filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
	$arguments.="&article_category_id=".$in['article_category_id'];
}


if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		
	}
}


$arguments = $arguments.$arguments_s;



$e = $db->query("SELECT pim_articles.article_id
            FROM pim_articles
			WHERE $filter
		     ".$order_by."
			")->getAll();
$max_rows=count($e);


$result = array('query'=>array(),'max_rows'=>$max_rows);

$articles = $db->query("SELECT pim_articles.article_id,pim_articles.hide_stock,pim_articles.block_discount,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,
			                   pim_articles.internal_name,pim_articles.d_price,pim_articles.h_price,pim_articles.article_category
            FROM pim_articles
			WHERE $filter
		   ".$order_by."
			LIMIT ".$offset*$l_r.",".$l_r."
			");

$j=0;
while($articles->move_next()){
	     if(!$articles->f('article'))

    

     
	$item =array(
				'name'		  		=> $articles->f('internal_name'),
				'code'		  		=> $articles->f('item_code'),
				'article_category'		=> $articles->f('article_category'),
				'article_id'  			=> $articles->f('article_id'),
				
				'd_price'  				=> display_number_var_dec($articles->f('d_price')),
				'h_price'  				=> display_number_var_dec($articles->f('h_price')),
				
				
				
				'is_archived'      		=> $in['archived'] ? true: false,
				'archive_link' 		 	=> array('do'=>'project-pservices-service-archive', 'article_id'=>$articles->f('article_id')),
				'undo_link' 		 	=> array('do'=>'project-pservices-service-activate', 'article_id'=>$articles->f('article_id')),
				'delete_link' 		 	=> array('do'=>'project-pservices-service-delete', 'article_id'=>$articles->f('article_id')),			

			);
    



    array_push($result['query'], $item);
	$j++;
}





//hide import and export functionality if user is not admin on module

global $database_config;

$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users= new sqldb($database_users);

//$is_p_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' ");
$is_p_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'project_admin']);
//$is_art_ord_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id = '".$_SESSION['u_id']."' AND name = 'admin_6' ");
$is_art_ord_admin = $db_users->field("SELECT value FROM user_meta WHERE user_id = :user_id AND name = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_6']);
$is_module_admin = false;
if($is_p_admin == '1' || $is_art_ord_admin == 'order'){
	$is_module_admin = true;
}
$result['is_module_admin']=$is_module_admin;
$_SESSION['filters'] = $arguments.$arguments_o;

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$result['families']=array();
while($db->move_next()){
	$families = array(
			'name'  => $db->f('name'),
			'id'=> $db->f('id')
			
		);
	  
	  array_push($result['families'], $families);
}
$result['lr'] = $l_r;

json_out($result);