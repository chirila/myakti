<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

	$result=array();

	$above_PM=false;
	if($_SESSION['group'] == 'admin'){
		$above_PM=true;
	}else{
		//$perm_proj_module=$db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$perm_proj_module=$db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$perm_array=explode(";",$perm_proj_module);
		if(in_array('3',$perm_array)){
			//$perm_mod_admin=$db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_3' AND value='project' ");
			$perm_mod_admin=$db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name AND value= :value ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_3','value'=>'project']);
			if($perm_mod_admin){
				$above_PM=true;
			}else{
				//$perm_proj_admin=$db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' AND value='1' ");
				$perm_proj_admin=$db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name AND value= :value ",['user_id'=>$_SESSION['u_id'],'name'=>'project_admin','value'=>'1']);
				if($perm_proj_admin){
					$above_PM=true;
				}
			}
		}
	}

	$result['cost_rights']	= $above_PM == false ? false : true;

	//$articles_denied=$db_users->field("SELECT plan FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$articles_denied=$db_users->field("SELECT plan FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

	$ALLOW_ARTICLE_PACKING = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
	$ALLOW_ARTICLE_SALE_UNIT = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

// $db = new sqldb();

$article_forecast=0;
$article_actual=0;
$article_costs=0;
$article_not_billed=0;
$billable_amount_articles=0;
$article_total_quant=0;
$article_total_purchase=0;
$article_total_price=0;
$articles_nr=0;
$result['article_row']=array();
$project_articles=$db->query("SELECT project_articles.*,projects.invoice_method,pim_articles.packing,pim_articles.sale_unit FROM project_articles 
	INNER JOIN projects ON project_articles.project_id=projects.project_id 
	LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
	WHERE project_articles.project_id='".$in['project_id']."' ");
	while($project_articles->next()){
		$packing_art=($ALLOW_ARTICLE_PACKING && $project_articles->f('packing')>0) ? $project_articles->f('packing') : 1;
		$sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $project_articles->f('sale_unit')>0) ? $project_articles->f('sale_unit') : 1;
		$art_line=array(
			'ARTICLE_N'			=> $project_articles->f('name'),
			'art_quantity'		=> display_number($project_articles->f('quantity')),
			'art_purchase_price'	=> display_number($project_articles->f('purchase_price')),
			'art_sell_price'		=> display_number($project_articles->f('price')),
			'art_bill_amount'		=> $project_articles->f('invoice_method')==1 ? place_currency(display_number($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art))) : place_currency(display_number(0)),
		);
		array_push($result['article_row'], $art_line);
		$billable_amount_articles+= $project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
		$article_forecast+=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
		$art_total_billing=$project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
		$proj_delivered_art=$db->query("SELECT quantity,invoiced FROM service_delivery WHERE project_id='".$in['project_id']."' AND a_id='".$project_articles->f('article_id')."' ");
		while($proj_delivered_art->next()){
			if($proj_delivered_art->f('invoiced')){
				$article_actual+=($project_articles->f('price')*$proj_delivered_art->f('quantity')*($packing_art/$sale_unit_art));
				$art_total_billing-=($project_articles->f('price')*$proj_delivered_art->f('quantity')*($packing_art/$sale_unit_art));
			}	
		}
		$article_not_billed+=$art_total_billing;
		/*if($project_articles->f('billed')==1){
			$article_actual+=($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art));
		}else{
			$article_not_billed+= $project_articles->f('invoice_method')==1 ? ($project_articles->f('price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art)) : 0;
		}*/
		$article_costs+=($project_articles->f('purchase_price')*$project_articles->f('quantity')*($packing_art/$sale_unit_art));
		$article_total_quant+=$project_articles->f('quantity');
		$article_total_purchase+=$project_articles->f('purchase_price');
		$article_total_price+=$project_articles->f('price');
		$articles_nr++;
	}

$project_name = $db->query("SELECT projects.*, projects.company_name AS c_name, projects.customer_id as c_id, projects.status_rate
														FROM projects
														WHERE project_id='".$in['project_id']."' AND projects.active='1' ");
$project_name->next();
$budget_t = 0;
$rate = 0;
$currency_show = false;
switch ($project_name->f('budget_type')){
	case '2':
		$budget_t = $project_name->f('t_pr_hour');
		break;
	case '3':
		$budget_t = $project_name->f('t_pr_fees');
		if( $project_name->f('billable_type') == '1'){
			$rate_value = $db->query("SELECT t_h_rate, t_daily_rate, task_id FROM tasks WHERE project_id='".$project_name->f('project_id')."' AND billable='1' AND task_id IN (SELECT DISTINCT task_id FROM task_time WHERE project_id='".$project_name->f('project_id')."' AND hours !=0 )");
			while ($rate_value->next()) {
				$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE task_id='".$rate_value->f('task_id')."' ");
				if($project_name->f('status_rate')==0){
					$rate += ($rate_value->f('t_h_rate') * $task_total_h) ;
				}else{
					$rate += ($rate_value->f('t_daily_rate') * $task_total_h) ;
				}	
			}
		}
		if($project_name->f('billable_type') == '2'){
			$rate_value = $db->query("SELECT p_h_rate, p_daily_rate, user_id FROM project_user WHERE project_id='".$project_name->f('project_id')."' ");
			while($rate_value->next()){
				$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE task_id in (SELECT DISTINCT task_id FROM task_time WHERE project_id='".$project_name->f('project_id')."' AND user_id='".$rate_value->f('user_id')."' AND hours !=0 ) ");
				if($project_name->f('status_rate')==0){
					$rate += ( $rate_value->f('p_h_rate') * $task_total_h );
				}else{
					$rate += ( $rate_value->f('p_daily_rate') * $task_total_h );
				}	
			}
		}
		if( $project_name->f('billable_type') == '3'){
				$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$project_name->f('project_id')."' ");
				$rate = ( $project_name->f('pr_h_rate') * $task_total_h );
		}
		if( $project_name->f('billable_type') == '4') {
				$rate = 0;
		}
		$currency_show = true;
		break;
	case '4':
		$budget = $db->query("SELECT t_hours FROM tasks WHERE project_id='".$project_name->f('project_id')."' ");
		while ($budget->next()) {
			$budget_t += $budget->f('t_hours');
		}
		break;
	case '5':
		$budget = $db->query("SELECT p_hours FROM project_user WHERE project_id='".$project_name->f('project_id')."' ");
		while ($budget->next()) {
			$budget_t += $budget->f('p_hours');
		}
		break;
}

$spent_h = $db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$project_name->f('project_id')."' ");
$spent = $currency_show == true ? $rate : $spent_h;
$purchases = $db->field("SELECT SUM(amount) FROM project_purchase WHERE project_id='".$project_name->f('project_id')."' ");
if($currency_show == true){$spent += $purchases;}
$left = $budget_t - $spent;
$procent_spent = $budget_t ? (($spent/$budget_t)*100) : 0;
$procent_left = $budget_t ? (($left/$budget_t)*100) : 0;
if($left < 0){
	$procent_left = 0;
}

$b = projectHourReport($project_name);
$costs=($b['user_cost']+$b['exp_cost']+$b['total_purchases_cost']+$article_costs);
$teoreticalAmount = $project_name->f('invoice_method')==1 ? ($b['teoreticalA']+$b['exp_can_bill']+$b['teo_puchases_in']+$article_forecast) : 0;
$invoicedAmount = ($b['invoicedA']+$b['exp_bill']+$b['puchases_in']+$article_actual);


	$result['BUDGET']				= $currency_show == true ? place_currency(display_number($budget_t)): ($project_name->f('status_rate')==0 ? gm('hours').' '.number_as_hour($budget_t) : gm('Days').' '.$budget_t);
	$result['SPENT']				= $currency_show == true ? place_currency(display_number($spent)) : ($project_name->f('status_rate')==0 ? gm('hours').' '.number_as_hour($spent) : gm('Days').' '.$spent);
	$result['BUDGET_LEFT']			= $currency_show == true ? place_currency(display_number($left)) : ($project_name->f('status_rate')==0 ? gm('hours').' '.number_as_hour($left) : gm('Days').' '.$left);
	$result['PROCENT']			= display_number($procent_left);
	$result['PROCENT_SPENT']		= number_format($procent_spent,2);
	$result['total_purchases_cost']	= place_currency(display_number($b['total_purchases_cost']));
	$result['teo_puchases_in']		= place_currency(display_number($b['teo_puchases_in']));
	$result['puchases_in']			= place_currency(display_number($b['puchases_in']));
	$result['exp_can_bill']			= place_currency(display_number($b['exp_can_bill']));
	$result['exp_bill']			= place_currency(display_number($b['exp_bill']));
	$result['exp_cost']			= place_currency(display_number($b['exp_cost']));
	$result['user_cost']			= $project_name->f('status_rate')==0 ? place_currency(display_number($b['user_cost'])) : '&nbsp;';
	$result['user_t']				= place_currency(display_number($b['teoreticalA']));
	$result['user_i']				= place_currency(display_number($b['invoicedA']));
	$result['total_purchases_cost1']	= $b['total_purchases_cost'] ? $b['total_purchases_cost'] : '0';
	$result['teo_puchases_in1']		= $b['teo_puchases_in'] ? $b['teo_puchases_in'] : '0';
	$result['puchases_in1']			= $b['puchases_in'] ? $b['puchases_in'] : '0';
	$result['exp_can_bill1']		= $b['exp_can_bill'];
	$result['exp_bill1']			= $b['exp_bill'];
	$result['exp_cost1']			= $b['exp_cost'];
	$result['user_cost1']			= $b['user_cost'];
	$result['user_t1']			= $project_name->f('invoice_method')==1 ? $b['teoreticalA'] : 0;
	$result['user_i1']			= $b['invoicedA'];
	$result['teoretic_p']			= place_currency(display_number($teoreticalAmount-$costs));
	//'invoice_p'				= place_currency(display_number($invoicedAmount-$costs)),
	$result['invoice_p']			= $invoicedAmount ? place_currency(display_number($invoicedAmount-$costs)) : '-';
	//'teoretic_pr'			= $costs ? display_number(($teoreticalAmount-$costs)/$costs*100) : '100',
	$result['teoretic_pr']			= $costs ? display_number(($teoreticalAmount-$costs)/$teoreticalAmount*100) : '100';
	//'invoice_pr'			= $costs ? display_number(($invoicedAmount-$costs)/$costs*100) : '100',
	$result['invoice_pr']			= ($costs && $invoicedAmount) ? display_number(($invoicedAmount-$costs)/$invoicedAmount*100).' %' : '-';
	$result['hour_day']			= $project_name->f('status_rate')==0 ? gm('Hours Tracked') : gm('Days Tracked');
	$result['hour_day_billable']		= $project_name->f('status_rate')==0 ? gm('Billable Hours') : gm('Billable Days');
	$result['hours_fixed_budget']		= $project_name->f('status_rate')==0 ? gm('Hours') : gm('Days');
	$result['budget_hours']			= $project_name->f('status_rate')==0 ? gm('Budget (hours)') : gm('Budget (days)');
	$result['balance_spent_left']		= $project_name->f('status_rate')==0 ? gm('Balance spent / left(hours)') : gm('Balance spent / left(days)');

	$result['PAGE_TITLE']			= gm('Project').' '.$project_name->f('name').' '.gm('report');
	$result['STYLE']    			= ACCOUNT_NUMBER_FORMAT;


### reports
$filter = " AND task_time.project_id ='".$in['project_id']."' ";


	$result['HIDE_C']				= false;
	$result['HIDE_P']				= false;
	$result['GLOBS']				= 'project_id='.$in['project_id'];


	$result['HOURS_TRACKED']= $project_name->f('status_rate')==0 ? number_as_hour($spent_h) : ($spent_h ? $spent_h : '0');
// get total billable hours
$db->query("SELECT SUM(hours) AS total_h FROM task_time WHERE billable='1' $filter ");
$db->move_next();
if($project_name->f('invoice_method')==1){
	$result['BILLABLE_H']= $project_name->f('status_rate')==0 ? number_as_hour($db->f('total_h')) : ($db->f('total_h') ? $db->f('total_h') : '0');
}else{
	$result['BILLABLE_H'] = $project_name->f('status_rate')==0 ? number_as_hour(0) : 0;
}
//get total not billable hours
$db->query("SELECT SUM(hours) AS total_h FROM task_time 
		INNER JOIN projects ON task_time.project_id = projects.project_id
		WHERE (billable='0' OR projects.invoice_method = '0') $filter ");
$db->move_next();
	$result['UNBILLABLE_H']= $project_name->f('status_rate')==0 ? number_as_hour($db->f('total_h')) : ($db->f('total_h') ? $db->f('total_h'): '0');
//get total submitted hours.
$db->query("SELECT SUM(hours) AS total_h FROM task_time WHERE submited='1' $filter ");
$db->move_next();
	$result['SUBMIT_H']= $project_name->f('status_rate')==0 ? number_as_hour($db->f('total_h')) : ($db->f('total_h') ? $db->f('total_h') : '0');
//get total not submitted hours
$db->query("SELECT SUM(hours) AS total_h FROM task_time WHERE submited='0' $filter ");
$db->move_next();
	$result['NOT_SUBMIT_H']= $project_name->f('status_rate')==0 ? number_as_hour($db->f('total_h')) : ($db->f('total_h') ? $db->f('total_h') : '0');

// get total billabble amount and uninvoiced amount
$billable_amount = 0;

$q = $db->query("SELECT task_time.task_id, task_time.project_id, task_time.user_id, task_time.customer_id, task_time.project_status_rate, tasks.billable,projects.billable_type, projects.pr_h_rate, tasks.t_h_rate, tasks.t_daily_rate
						FROM task_time
						INNER JOIN tasks ON task_time.task_id=tasks.task_id
						INNER JOIN projects ON task_time.project_id=projects.project_id
						WHERE 1=1 $filter AND tasks.billable='1' GROUP BY task_time.task_id ");
while($q->next()){
	switch ($q->f('billable_type')){
		case '1':
		case '7':
			if($q->f('project_status_rate') == 0){
				if(!$q->f('t_h_rate')){
					$rate = 0;
				}
				else{
					$rate = $q->f('t_h_rate');
				}
			}else{
				if(!$q->f('t_daily_rate')){
					$rate = 0;
				}
				else{
					$rate = $q->f('t_daily_rate');
				}
			}			
			break;
		case '2':
			if($q->f('project_status_rate') == 0){
				$rate = $db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
			}else{
				$rate = $db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$q->f('user_id')."' AND project_id='".$q->f('project_id')."' ");
			}		
			break;
		case '3':
			$rate = $q->f('pr_h_rate');
			break;
		case '4':
			$rate = 0;
			break;
		case '5':
			break;
	}

	$total_h = $db->query("SELECT SUM(hours) AS total_h FROM task_time WHERE task_id='".$q->f('task_id')."' AND project_id='".$q->f('project_id')."' ");
	$total_h->next();
	$billable_amount += ($total_h->f('total_h'))*$rate;
	$total_hu = $db->query("SELECT SUM(hours) AS total_h FROM task_time WHERE task_id='".$q->f('task_id')."' AND project_id='".$q->f('project_id')."' AND billed='0' ");
	$total_hu->next();
	$unbillable_amount += ($total_hu->f('total_h'))*$rate;

}
$closed_task = $db->field("SELECT sum(task_budget) FROM tasks WHERE project_id='".$in['project_id']."' AND closed!='1' AND billable='1' ");
$billable_amount += $closed_task;
$closed_task_un = $db->field("SELECT sum(task_budget) FROM tasks WHERE project_id='".$in['project_id']."' AND closed='1' AND billable='1' ");
$unbillable_amount += $closed_task_un;

if($project_name->f('billable_type') == 6){

	$billable_amount = $b['teoreticalA'];

}

$billable_amount+=$billable_amount_articles;
$unbillable_amount+=$article_not_billed;

	$result['purchase_row']=array();
	$total_purchase_price=0;
	$total_purchase_sell_price=0;
	$TOTAL_P_BILLABLE=0;
	$purchas = $db->query("SELECT project_purchase.*,projects.invoice_method,customers.name FROM project_purchase 
		INNER JOIN projects ON project_purchase.project_id=projects.project_id
		LEFT JOIN customers ON project_purchase.supplier_id=customers.customer_id
		WHERE project_purchase.project_id='".$in['project_id']."' ");
	while($purchas->next()){
		$margin_price = $purchas->f('invoice_method') ? ($purchas->f('unit_price')*$purchas->f('margin')/100) + $purchas->f('unit_price') : 0;
		if($purchas->f('billable')){		
			$uninv_purchase=$purchas->f('invoice_method') ? (!$purchas->f('invoiced') ? ($purchas->f('unit_price')*$purchas->f('margin')/100) + $purchas->f('unit_price') : 0) : 0;
			$billable_amount+= $margin_price;
			$unbillable_amount+=$uninv_purchase;
			$TOTAL_P_BILLABLE+=$margin_price;
		}	
		$total_purchase_price+=$purchas->f('unit_price');
		$total_purchase_sell_price+=$margin_price;
		$temp_p=array(
			'name'			=> $purchas->f('description'),
			'supplier'			=> $purchas->f('name'),
			'price'			=> place_currency(display_number($purchas->f('unit_price'))),
			'selling_price'		=> place_currency(display_number($margin_price)),
			'billable_price'	=> $purchas->f('billable') ? place_currency(display_number($margin_price)) : place_currency(display_number(0)),
		);
		array_push($result['purchase_row'],$temp_p);
	}

	$result['BILLABLE_A'] 			= $project_name->f('invoice_method')==1 ? place_currency(display_number($billable_amount)) : place_currency(display_number(0));
	$result['UNINVOICE_A'] 			= $project_name->f('invoice_method')==1 ? place_currency(display_number($unbillable_amount)) : place_currency(display_number(0));
	$result['article_forecast']		= $article_forecast;
	$result['article_actual']		= $article_actual;
	$result['article_costs']		= $article_costs;
	$result['article_min']			= ($articles_nr==0 || $articles_denied==2 || $articles_denied==5) ? false : true;
	$result['article_allowed']		= ($articles_denied==2 || $articles_denied==5) ? false : true;
	$result['TOTAL_QUANTITY']		= display_number($article_total_quant);
	$result['TOTAL_PURCHASE_PRICE'] 	= display_number($article_total_purchase);
	$result['TOTAL_SELL_PRICE']		= display_number($article_total_price);
	$result['TOTAL_BILL_ART']		= place_currency(display_number($article_forecast));
	$result['type']				= $project_name->f('status_rate')==0 ? build_budget_type_list($project_name->f('budget_type'),'',false) : build_budget_type_list_daily($project_name->f('budget_type'),'',false);
	$result["TOTAL_P_UNIT_PRICE"]		= place_currency(display_number($total_purchase_price));
	$result["TOTAL_P_SELLING_PRICE"]	= place_currency(display_number($total_purchase_sell_price));
	$result['TOTAL_P_BILLABLE']=place_currency(display_number($TOTAL_P_BILLABLE));

$user_tasks = array();
$something = $db->query("SELECT task_time.user_id, task_time.task_id,project_user.user_name, tasks.task_name
												 FROM task_time
												 INNER JOIN tasks ON task_time.task_id=tasks.task_id
												 INNER JOIN project_user ON task_time.user_id=project_user.user_id
												 WHERE task_time.project_id ='".$in['project_id']."' GROUP BY task_time.user_id , task_time.task_id ");
while ($something->next()) {
	$user_tasks[$something->f('user_id')][$something->f('task_id')] = array($something->f('user_name'),$something->f('task_name'));
}
foreach ($user_tasks as $key => $val){
	foreach ($val as $k => $v){
		$h = $db->field("SELECT SUM(hours) as t_h FROM task_time WHERE user_id='".$key."' AND task_id='".$k."' AND project_id='".$in['project_id']."' ");
		array_push($user_tasks[$key][$k],$h );
		$billable_amount = get_billable_amount($project_name->f('c_id'),0, 0,$in['project_id'],$k,$key);
		array_push($user_tasks[$key][$k],$billable_amount );
	}
}

$t_h = 0;
$t_a = 0;
$i= 0;
//console::log($user_tasks);
$result['u_hours_row']=array();
foreach ($user_tasks as $key => $val) {
	$temp_r=array();
	foreach ($val as $k => $v){
		// if($v[2]){
			$x_row=array(
				'USER_ID'		=> $key,
				'STAFF_N'		=> $v[1],
				'TOTAL_HOUR_2'	=> $project_name->f('status_rate')==0 ? number_as_hour($v[2]) : $v[2],
				'BILLALBE_A2'	=> $project_name->f('invoice_method')==1 ?  place_currency(display_number($v[3])) : place_currency(display_number(0)),
				'hide_ud'		=> $project_name->f('budget_type') == 5 ? true : false,
			);
			array_push($temp_r, $x_row);
			$t_h += $v[2];
			$t_a += $v[3];
		// }
		$name = $v[0];
	}
	$user_b =$db->field("SELECT p_hours FROM `project_user` WHERE user_id='".$key."' AND project_id='".$in['project_id']."' ");
	$u_row=array(
		'TOTAL_HOUR' 	=> $project_name->f('status_rate')==0 ? number_as_hour($t_h) : $t_h,
		'BILLALBE_A' 	=> $project_name->f('invoice_method')==1 ? place_currency(display_number($t_a)) : place_currency(display_number(0)),
		'CUSTOMER_N' 	=> $name,
		'budget_user'	=> $project_name->f('status_rate')==0 ? number_as_hour($user_b) : $user_b,
		'budget_user_l'	=> $project_name->f('status_rate')==0 ? number_as_hour($user_b-$t_h) : $user_b-$t_h,
		'b_u'			=> $user_b,
		'b_u_l'		=> $t_h,
		'bu_over_spent'	=> $t_h>$user_b ? true : false,
	  	'bu_width'		=> ($user_b == 0 || $t_h == 0)? 0 : ($user_b >= $t_h ? ceil($t_h*100/$user_b) : ceil($user_b*100/$t_h) ),
	  	'bu_width_red'	=> ($t_h != 0 && $t_h>$user_b )? 100-ceil($user_b*100/$t_h) : 0,
		'hide_ud'		=> $project_name->f('budget_type') == 5 ? true : false,
		'x_hours_row'	=> $temp_r,
	);
	if($t_h != 0){
		array_push($result['u_hours_row'], $u_row);
	}
	$t_h = 0;
	$t_a = 0;
	$i++;
}
	$result['HIDE_INFO']	= true;
if($i == 0){
	$result['HIDE_INFO']	= false;
}
### task table
$result['task_row']=array();
if($project_name->f('billable_type') && ($project_name->f('billable_type') == 5 || $project_name->f('budget_type') == 4 )){
	$user_task_array = array();

	$tasks = $db->query("SELECT tasks.*, projects.status_rate FROM tasks
		INNER JOIN projects ON tasks.project_id=projects.project_id
		WHERE tasks.project_id='".$project_name->f('project_id')."' ");
	$rate_value = $db->query("SELECT p_h_rate, user_id FROM project_user WHERE project_id='".$project_name->f('project_id')."' ");
	while($rate_value->next()){
		$user_task_array[$rate_value->f('user_id')]	 = $rate_value->f('p_h_rate');
	}

	$budget_amount = 0;
	$budget_amount_spent = 0;
	$budget_amount_left = 0;
	$budget_hours = 0;
	$budget_hours_amount = 0;
	$budget_hours_left = 0;
	$j=0;
	$allowed_budget = array(2,4);
	while ($tasks->next()) {
		$j++;
		$rate = 0;
		$rate_h = 0;
		foreach ($user_task_array as $key => $value) {
			$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE task_id='".$tasks->f('task_id')."' AND project_id='".$in['project_id']."' AND user_id='".$key."' AND hours !=0 ");
			$rate += ( $value * $task_total_h );
			$rate_h += $task_total_h;
		}

		$t_row=array(
			'TASK_NAME'			=> $tasks->f('task_name'),
			'TASK_BUDGET'		=> place_currency(display_number($tasks->f('task_budget'))),
			'RATE'			=> place_currency(display_number($rate))." / ".place_currency(display_number($tasks->f('task_budget')-$rate)),
			'RATE_HOURS'		=> $tasks->f('status_rate')==0 ? number_as_hour($rate_h)." / ".number_as_hour($tasks->f('t_hours')-$rate_h) : $rate_h." / ".$tasks->f('t_hours')-$rate_h,
			'TASK_HOURS'		=> $tasks->f('status_rate')==0 ? number_as_hour($tasks->f('t_hours')) : $tasks->f('t_hours'),
			'T_B'				=> $tasks->f('task_budget'),
			'T_S'				=> $rate,
			'b_over_spent'		=> $rate>$tasks->f('task_budget') ? true : false,
	  		'b_width'			=> ($tasks->f('task_budget') ==0 || $rate ==0)? 0 : ($tasks->f('task_budget') >= $rate ? ceil($rate*100/$tasks->f('task_budget')) : ceil($tasks->f('task_budget')*100/$rate) ),
	  		'b_width_red'		=> ($rate !=0 && $rate>$tasks->f('task_budget') )? 100-ceil($tasks->f('task_budget')*100/$rate) : 0,
			'T_B_H'			=> $tasks->f('t_hours'),
			'T_S_H'			=> $rate_h,
			'h_over_spent'		=> $rate_h>$tasks->f('t_hours') ? true : false,
	  		'h_width'			=> ($tasks->f('t_hours') ==0 || $rate_h ==0)? 0 : ($tasks->f('t_hours') >= $rate_h ? ceil($rate_h*100/$tasks->f('t_hours')) : ceil($tasks->f('t_hours')*100/$rate_h) ),
	  		'h_width_red'		=> ($rate_h != 0 && $rate_h>$tasks->f('t_hours') ) ? 100-ceil($tasks->f('t_hours')*100/$rate_h) : 0,
			"LAST"			=> in_array($project_name->f('budget_type'), $allowed_budget) ? '' : 'last',
			"HIDE_THIS"			=> in_array($project_name->f('budget_type'), $allowed_budget) ? true : false,
			'hide_these'		=> $project_name->f('budget_type') == 4 && $project_name->f('billable_type') != 5 ? false : true,
			"p"				=> $project_name->f('budget_type') == 4 ? false : true,
			"p_h"				=> $project_name->f('budget_type') == 4 ? true : false,
		);
		$budget_amount += $tasks->f('task_budget');
		$budget_amount_spent += $rate;
		$budget_amount_left += $tasks->f('task_budget')-$rate;
		$budget_hours += $tasks->f('t_hours');
		$budget_hours_amount += $rate_h;
		$budget_hours_left += $tasks->f('t_hours')-$rate_h;
		array_push($result['task_row'], $t_row);
	}

	$result['TOTAL_AMOUNT']			= place_currency(display_number($budget_amount));
	$result['TOTAL_AMOUNT_SPENT']		= place_currency(display_number($budget_amount_spent))." / ".place_currency(display_number($budget_amount_left));
	$result['TOTAL_HOURS']			= $project_name->f('status_rate')==0 ? number_as_hour($budget_hours) : $budget_hours;
	$result['TOTAL_HOURS_SPENT']		= $project_name->f('status_rate')==0 ? number_as_hour($budget_hours_amount)." / ".number_as_hour($budget_hours_left) : $budget_hours_amount." / ".$budget_hours_left;
	$result["LAST"]				= in_array($project_name->f('budget_type'), $allowed_budget) ? '' : 'last';
	$result["HIDE_THIS"]			= in_array($project_name->f('budget_type'), $allowed_budget) ? true : false;
	$result['hide_these']			= $project_name->f('budget_type') == 4 && $project_name->f('billable_type') != 5 ? false : true;
	$result['HIDE_TASK_REP']            = true;
	$t_width=2;
	if(!$result['hide_these']){
		$t_width=$t_width+5;
	}
	if(!$result['HIDE_THIS']){
		$t_width=$t_width+5;
	}
	$result['t_width']			= $t_width;

}else{ $j = 0; }
if($j==0){
	$result['HIDE_TASK_REP']= false;
}

if($project_name->f('budget_type') == 1){
	$result['HIDE_TASK_REP']=false;
}
	$result['hide_ud']		= true;
if($project_name->f('budget_type') != 5){
	$result['hide_ud']		= false;
	$result['colspan']		= 'colspan="3"';

}
	$bar_labels=array($result['hours_fixed_budget'].'/'.gm('fixed budget'),gm('Expenses'),gm('Purchases'));
	if($result['article_allowed']){
		array_push($bar_labels, gm('Articles'));
	}
	$bar_data=array(array(),array(),array());
	if($result['article_allowed']){
		$bar_data[0][0]=$result['user_t1'];
		$bar_data[0][1]=$result['exp_can_bill1'];
		$bar_data[0][2]=$result['teo_puchases_in1'];
		$bar_data[0][3]=$result['article_forecast'];
		$bar_data[1][0]=$result['user_i1'];
		$bar_data[1][1]=$result['exp_bill1'];
		$bar_data[1][2]=$result['puchases_in1'];
		$bar_data[1][3]=$result['article_actual'];
		if($result['cost_rights']){
			$bar_data[2][0]=$result['user_cost1'];
			$bar_data[2][1]=$result['exp_cost1'];
			$bar_data[2][2]=$result['total_purchases_cost1'];
			$bar_data[2][3]=$result['article_costs'];
		}else{
			$bar_data[2][0]='';
			$bar_data[2][1]=$result['exp_cost1'];
			$bar_data[2][2]=$result['total_purchases_cost1'];
		}
	}else{
		$bar_data[0][0]=$result['user_t1'];
		$bar_data[0][1]=$result['exp_can_bill1'];
		$bar_data[0][2]=$result['teo_puchases_in1'];
		$bar_data[1][0]=$result['user_i1'];
		$bar_data[1][1]=$result['exp_bill1'];
		$bar_data[1][2]=$result['puchases_in1'];
		if($result['cost_rights']){
			$bar_data[2][0]=$result['user_cost1'];
			$bar_data[2][1]=$result['exp_cost1'];
			$bar_data[2][2]=$result['total_purchases_cost1'];
		}else{
			$bar_data[2][0]='';
			$bar_data[2][1]=$result['exp_cost1'];
			$bar_data[2][2]=$result['total_purchases_cost1'];
		}
	}
	$budget_left=100-$result['PROCENT_SPENT'] <= 0 ? 0 : 100-$result['PROCENT_SPENT'];

	$result['WEEK_NR']		= date('W');
	$result['YEAR']			= date('Y');
	$result['MONTH']			= date('n');
	$result['TIME_START']		= $time_start;
	$result['TIME_END']		= $time_end;
	$result['KEEP_PAGE']		= $in['keep_page'];
	$result['FILTER']			= $in['filter'];
	$result['PICK_DATE_FORMAT']  	= pick_date_format();
	$result['PROJECT_ID']		= $in['project_id'];
	$result['budget_data']		= array($result['PROCENT_SPENT'],$budget_left);
	$result['bar_labels']		= $bar_labels;
	$result['bar_data']		= $bar_data;

return json_out($result);
?>