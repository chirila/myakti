<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 
if(!defined('BASEPATH')) exit('No direct script access allowed');

	if($in['status_rate']==0){
		$project_data=$db->field("SELECT project_user.p_hourly_cost FROM project_user WHERE project_user_id='".$in['project_user_id']."' ");
	}else{	
		$project_data=$db->field("SELECT project_user.p_daily_cost FROM project_user WHERE project_user_id='".$in['project_user_id']."' ");
	}

	$result=array(
		'cost_name' 		=> $in['status_rate']==0 ? gm('Hourly Cost') : gm('Daily Cost'),
		'user_cost'			=> display_number($project_data),
		'project_user_id'		=> $in['project_user_id'],
		'status_rate'		=> $in['status_rate']
	);

return json_out($result);