<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');

global $database_config;
$result=array('query'=>array());
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);
$is_pagination = false;

$l_r = ROW_PER_PAGE;
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$filter = '1=1';
if(!empty($in['search'])){
	$filter .= " AND (users.first_name LIKE '%".$in['search']."%' OR users.last_name LIKE '%".$in['search']."%')  ";

}
//$filter .= " AND users.active = '1' ";
$filter .= " AND users.active<>1000 ";

/*$db_users->query("SELECT users.*, user_meta.value, user_meta.name FROM users LEFT JOIN user_meta ON users.user_id=user_meta.user_id AND user_meta.name='active' WHERE ".$filter." AND database_name='".DATABASE_NAME."' GROUP BY users.user_id  ORDER BY last_name ");*/
$db_users->query("SELECT users.*, user_meta.value, user_meta.name FROM users LEFT JOIN user_meta ON users.user_id=user_meta.user_id AND user_meta.name='active' WHERE ".$filter." AND user_meta.value IS NULL AND database_name='".DATABASE_NAME."' GROUP BY users.user_id  ORDER BY last_name ");

$max_rows=$db_users->records_count();
$result['max_rows']=$max_rows;
//$db_users->move_to($offset*$l_r);
$start = $offset*$l_r;
//$start = 0;
$k=0;
while ($db_users->move_next()) {
	$all_rows[$k] = array(
		'name11' 				=> htmlspecialchars($db_users->f('last_name')).' - '.htmlspecialchars($db_users->f('first_name')),
		'name22' 				=> htmlspecialchars($db_users->f('first_name'))." ".htmlspecialchars($db_users->f('last_name')),
		'h_rate'				=> $in['status_rate'] > 0 ? $db_users->f('daily_rate') : $db_users->f('h_rate'),
		'user_id' 				=> $db_users->f('user_id'),
		'active'				=> $db_users->f('active'),
		'h_cost'				=> $in['status_rate'] > 0 ? $db_users->f('daily_cost') : $db_users->f('h_cost'),
	);
	$k++;
}

for ($k=$start; $k<$max_rows; $k++){
	$query=array(
		'NAME'					=> $all_rows[$k]['name11'],
		'NAME2'					=> $all_rows[$k]['name22'],
		'H_RATE'					=> $all_rows[$k]['h_rate'],
		'ACTION' 			    		=> $all_rows[$k]['user_id'],
		'user_id' 			  		=> $all_rows[$k]['user_id'],
		'user_name' 				=> $all_rows[$k]['name22'],
		'p_h_rate'        			=> $all_rows[$k]['h_rate'],
		'p_h_cost'        			=> $all_rows[$k]['h_cost'],
		'checked'					=> $all_rows[$k]['user_id'] == $_SESSION['u_id'] ? 'CHECKED' : '',
	);
	array_push($result['query'], $query);
}

	$result['is_data']				= $k ? true : false;
	$result['lr'] 					= $l_r;
	$result['project_id']				= $in['project_id'];

return json_out($result);