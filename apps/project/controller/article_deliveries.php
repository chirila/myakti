<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array('list'=>array());

$l_r = ROW_PER_PAGE;
//$l_r = 10;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$deliveries = $db->query("SELECT * FROM service_deliveries WHERE project_id='".$in['project_id']."' ORDER BY delivery_id ASC");

$max_rows=$deliveries->records_count();
$result['max_rows']=$max_rows;
$deliveries->move_to($offset*$l_r);
$j=0;

while($deliveries->move_next() && $j<$l_r){
	$query=array(
		'delivery_id'		=> $deliveries->f('delivery_id'),
		'date'			=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('date')),
		'address'			=> nl2br($deliveries->f('delivery_address')),
		'contact'			=> $db->field("SELECT CONCAT_WS(' ',lastname,firstname) FROM customer_contacts WHERE contact_id='".$deliveries->f('contact_id')."' "),
	);
	array_push($result['list'], $query);
	$j++;
}	

	$stage=$db->field("SELECT stage FROM projects WHERE project_id='".$in['project_id']."' ");
	$result['project_id']			= $in['project_id'];
	$result['not_closed']			= $stage!=2 ? true : false;

return json_out($result);