<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '2000M');

setcookie('Akti-Export','6',time()+3600,'/');
ark::loadLibraries(array('PHPExcel'));
$filename ="export_to_deliver_projects.xls";

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("To Deliver Projects")
							 ->setSubject("To Deliver Projects")
							 ->setDescription("To Deliver Projects")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Projects");
$db = new sqldb();

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);

$filter = '1=1';
$order_by =" ORDER BY serial_number DESC ";


if(!empty($in['search'])){
 $filter .= " AND (projects.name LIKE '%".$in['search']."%' OR company_name LIKE '%".$in['search']."%' OR projects.serial_number LIKE '%".$in['search']."%' 
 	OR project_articles.name LIKE '%".$in['search']."%')";
}

$filter .= " AND project_articles.delivered='0' AND projects.active=1 AND projects.stage<2";
if($_SESSION['access_level'] !='1'){
	$filter .=" AND project_user.user_id='".$_SESSION['u_id']."' AND project_user.active='1' ";
} 

if($in['order_by']){
	$order = " ASC ";
	if($in['desc']){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	//$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
}

$articles = $db->query("SELECT project_articles.name AS article_name, project_articles.quantity,project_articles.price, projects.*, pim_articles.item_code 
						FROM projects
						INNER JOIN project_articles ON projects.project_id=project_articles.project_id
						LEFT JOIN pim_articles ON pim_articles.article_id=project_articles.article_id
						WHERE $filter ".$order_by);

$objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


$objPHPExcel->getActiveSheet(0)->getStyle('A1:H1')->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Serial Number"))
			->setCellValue('B1', gm("Project Name"))
			->setCellValue('C1', gm("Start Date"))
			->setCellValue('D1', gm("Client"))
			->setCellValue('E1', gm("Article Code"))
			->setCellValue('F1', gm("Article"))
			->setCellValue('G1', gm("Quantity"))
			->setCellValue('H1', gm("Price"));

   $xlsRow=2;
while ($articles->next())
{
   $start_date=$articles->f('start_date')? date(ACCOUNT_DATE_FORMAT,$articles->f('start_date')) : '';
   $article_price = display_number($articles->f('price'));
	$objPHPExcel->setActiveSheetIndex(0)	
				->setCellValue('A'.$xlsRow, ' '.$articles->f('serial_number'))
     			->setCellValue('B'.$xlsRow, ' '.$articles->f('name'))
      			->setCellValue('C'.$xlsRow, ' '.$start_date)
      			->setCellValue('D'.$xlsRow, ' '.$articles->f('company_name'))
      			->setCellValue('E'.$xlsRow, ' '.$articles->f('item_code'))
      			->setCellValue('F'.$xlsRow, ' '.$articles->f('article_name'))   
      			->setCellValue('G'.$xlsRow, ' '.$articles->f('quantity'))
      			->setCellValue('H'.$xlsRow, ' '.display_number($articles->f('price')));
	
	$xlsRow++;
}
$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);


$decimals='0';
$format_code = '0.';
for ($q=0; $q < ARTICLE_PRICE_COMMA_DIGITS; $q++) { 
  $format_code= $format_code.'0';
}

$rows_format='N2:O'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode($format_code);

$rows_format='P2:P'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');

$objPHPExcel->getActiveSheet()->setTitle('To deliver projects');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');                                                                               
$objWriter->save('upload/'.$filename);





define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',
  
  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name."); 
}
// file size in bytes
$fsize = filesize($file_path); 
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type."); 
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);  
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
setcookie('Akti-Export','9',time()+3600,'/');
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}

?>