<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 
$result=array();
$db = new sqldb();

$project_name = $db->query("SELECT projects.*,  projects.company_name AS c_name, projects.customer_id as c_id, projects.status_rate
														FROM projects 
														WHERE project_id='".$in['project_id']."' AND projects.active='1' ");
$project_name->next();
# new reports
//$filter = ' 1=1 ';
$filter = 'task_time.project_id='.$in['project_id'];

$customer_days = $db->query("select distinct date from task_time where ".$filter." and hours!='0' order by date asc ");

$days = array();
while ($customer_days->next()) {
	array_push($days,$customer_days->f('date'));
}
$result['time_row']=array();
foreach ($days as $time){
	$detailed_time_row=array();
	$day_hours = $db->query("SELECT hours, tasks.task_name as t_name, project_user.user_name as u_name, date, notes, task_time.billed, task_time.billable, task_time.project_status_rate,task_time.task_time_id,task_time.user_id,task_time.task_id FROM task_time
													 INNER JOIN tasks ON task_time.task_id=tasks.task_id
													 INNER JOIN project_user ON task_time.user_id=project_user.user_id 
													 WHERE $filter AND date='".$time."' GROUP BY task_time.user_id, task_time.task_id ");
	$day_h = 0;
	while ($day_hours->next()) {		
		if($day_hours->f('hours')){
			$temp_array=array();
			$temp_array['CUSTOMERS']		= $project_name->f('c_name');
			$temp_array['PROJECT']			= $project_name->f('name');
			$temp_array['TASK']			= $day_hours->f('t_name');
			$temp_array['USER']			= $day_hours->f('u_name');
			$temp_array['HIDE_UNBILLED']		= $day_hours->f('billed') == 1 ? true : false;

			$day_notes="";
			$task_time_subdata=$db->query("SELECT notes FROM servicing_support_sheet WHERE project_id='".$in['project_id']."' AND task_id='".$day_hours->f('task_id')."' AND task_time_id='".$day_hours->f('task_time_id')."' AND user_id='".$day_hours->f('user_id')."' ORDER BY id ASC ");
			while($task_time_subdata->next()){
				$day_notes.=$task_time_subdata->f('notes');
			}

			//$temp_array['COMMENT']			= $day_hours->f('notes');
			$temp_array['COMMENT']			= $day_notes;
							
			if($day_hours->f('billable') && $project_name->f('invoice_method')){
				$temp_array['HOURSB'] 		= $day_hours->f('project_status_rate')==0 ? number_as_hour($day_hours->f('hours')) : $day_hours->f('hours');
				$temp_array['HOURS']		= '';
				$temp_array['LOCKED_SB']	= $day_hours->f('billed') == 1 ? true : false;
				$temp_array['LOCKED_B']		= false;
				$total_day_h += $day_hours->f('hours');
			}else{
				$temp_array['HOURS'] 		= $day_hours->f('project_status_rate')==0 ? number_as_hour($day_hours->f('hours')) : $day_hours->f('hours');
				$temp_array['HOURSB']		= '';
				$temp_array['LOCKED_SB']	= false;
				$temp_array['LOCKED_B']		= $day_hours->f('billed') == 1 ? true : false;
				$total_day_h_u += $day_hours->f('hours');
			}
			array_push($detailed_time_row, $temp_array);
			$total_day += $day_hours->f('hours');
			$day_h += $day_hours->f('hours');
		}
	}
	if($day_h){
		$t_row=array(
			'DATE' 			=> date(ACCOUNT_DATE_FORMAT,$time),
			'TOTAL_C_HOURS'		=> $project_name->f('status_rate')==0 ? number_as_hour($day_h) : $day_h,
			'detailed_time_row'	=> $detailed_time_row
		);
		array_push($result['time_row'], $t_row);
	}
}

$link = '';
foreach ($in as $key => $value){
	$link .= '&'.$key.'='.$value;
}
$link = ltrim($link,'&');

	$result['TOTAL']			= $project_name->f('status_rate')==0 ? number_as_hour($total_day) : $total_day;
	$result['TOTAL_H']		= $project_name->f('status_rate')==0 ? number_as_hour($total_day_h) : $total_day_h;
	$result['TOTAL_H_U']		= $project_name->f('status_rate')==0 ? number_as_hour($total_day_h_u) : $total_day_h_u;
	$result['EXPORT_HREF']		= 'index.php?do=project-export_detail_report&'.str_replace('do=project-project_report_detail&','', $link);
	$result['PAGE_TITLE']		= gm('Project').' '.$project_name->f('name').' '.gm('Detailed Report');
	$result['PROJECT_ID']		= $in['project_id'];
	$result['hour_day']		= $project_name->f('status_rate')==0 ? gm('Hours') : gm('Days');



return json_out($result);
?>