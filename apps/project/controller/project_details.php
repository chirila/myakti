<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

if(!defined('BASEPATH')) exit('No direct script access allowed');

$result=array('tasks'=>array());
$is_data = false;
if($in['budget']){
	$is_data = true;	
}
$result['HIDE_TASK_REP']=true;
//for the second table
$project_name = $db->query("SELECT projects.*, projects.company_name AS c_name, projects.customer_id as c_id
														FROM projects
														WHERE project_id='".$in['project_id']."' AND projects.active='1' ");
$project_name->next();


### task table
if($project_name->f('billable_type') && ($project_name->f('billable_type') == 5 || $project_name->f('budget_type') == 4 )){
	$user_task_array = array();

	$tasks = $db->query("SELECT * FROM tasks WHERE project_id='".$project_name->f('project_id')."' ");
	$rate_value = $db->query("SELECT p_h_rate, user_id FROM project_user WHERE project_id='".$project_name->f('project_id')."' ");
	while($rate_value->next()){
		$user_task_array[$rate_value->f('user_id')]	 = $rate_value->f('p_h_rate');
	}

	$budget_amount = 0;
	$budget_amount_spent = 0;
	$budget_amount_left = 0;
	$budget_hours = 0;
	$budget_hours_amount = 0;
	$budget_hours_left = 0;
	$j=0;
	$allowed_budget = array(2,4);
	while ($tasks->next()) {
		$j++;
		$rate = 0;
		$rate_h = 0;
		foreach ($user_task_array as $key => $value) {
			$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE task_id='".$tasks->f('task_id')."' AND project_id='".$in['project_id']."' AND user_id='".$key."' AND hours !=0 ");
			$rate += ( $value * $task_total_h );
			$rate_h += $task_total_h;
		}

		$task_row=array(
			'TASK_NAME'			=> $tasks->f('task_name'),
			'TASK_BUDGET'		=> place_currency(display_number($tasks->f('task_budget'))),
			'RATE'			=> place_currency(display_number($rate))." / ".place_currency(display_number($tasks->f('task_budget')-$rate)),
			'RATE_HOURS'		=> number_as_hour($rate_h)." / ".number_as_hour($tasks->f('t_hours')-$rate_h),
			'TASK_HOURS'		=> number_as_hour($tasks->f('t_hours')),
			'T_B'				=> $tasks->f('task_budget'),
			'T_S'				=> $rate,
			'T_B_H'			=> $tasks->f('t_hours'),
			'T_S_H'			=> $rate_h,
			"LAST"			=> in_array($project_name->f('budget_type'), $allowed_budget) ? '' : 'last',
			"HIDE_THIS"			=> in_array($project_name->f('budget_type'), $allowed_budget) ? true : false,
			'hide_these'		=> $project_name->f('budget_type') == 4 && $project_name->f('billable_type') != 5 ? false : true,
			"p"				=> $project_name->f('budget_type') == 4 ? false : true,
			"p_h"				=> $project_name->f('budget_type') == 4 ? true : false,			
			'over_spent_t'		=> $rate>$tasks->f('task_budget') ? true : false,
	  		'width_t'			=> ($tasks->f('task_budget') ==0 || $rate ==0)? 0 : ($tasks->f('task_budget') >= $rate ? ceil($rate*100/$tasks->f('task_budget')) : ceil($tasks->f('task_budget')*100/$rate) ),
	  		'width_red_t'		=> ($rate !=0 && $rate>$tasks->f('task_budget') )? 100-ceil($tasks->f('task_budget')*100/$rate) : 0,
	  		'over_spent_h'		=> $rate_h>$tasks->f('t_hours') ? true : false,
	  		'width_h'			=> ($tasks->f('t_hours') ==0 || $rate_h ==0)? 0 : ( $tasks->f('t_hours') >= $rate_h ? ceil($rate_h*100/$tasks->f('t_hours')) : ceil($tasks->f('t_hours')*100/$rate_h) ),
	  		'width_red_h'		=> ($rate_h !=0 && $rate_h>$tasks->f('t_hours') )? 100-ceil($tasks->f('t_hours')*100/$rate_h) : 0,
		);
		$budget_amount += $tasks->f('task_budget');
		$budget_amount_spent += $rate;
		$budget_amount_left += $tasks->f('task_budget')-$rate;
		$budget_hours += $tasks->f('t_hours');
		$budget_hours_amount += $rate_h;
		$budget_hours_left += $tasks->f('t_hours')-$rate_h;

		array_push($result['tasks'],$task_row);
	}
	
		$result['TOTAL_AMOUNT']				= place_currency(display_number($budget_amount));
		$result['TOTAL_AMOUNT_SPENT']			= place_currency(display_number($budget_amount_spent))." / ".place_currency(display_number($budget_amount_left));
		$result['TOTAL_HOURS']				= number_as_hour($budget_hours);
		$result['TOTAL_HOURS_SPENT']			= number_as_hour($budget_hours_amount)." / ".number_as_hour($budget_hours_left);
		$result["LAST"]					= in_array($project_name->f('budget_type'), $allowed_budget) ? '' : 'last';
		$result["HIDE_THIS"]				= in_array($project_name->f('budget_type'), $allowed_budget) ? true : false;
		$result['hide_these']				= $project_name->f('budget_type') == 4 && $project_name->f('billable_type') != 5 ? false : true;
	
}else{ $j = 0; }
if($j==0){
	$result['HIDE_TASK_REP']		= false;
}

if($project_name->f('budget_type') == 1){
	$result['HIDE_TASK_REP']		= false;
}


	$result['is_data']			= $is_data;
	$result['BUDGET']				= $in['budget'];
	$result['SPENT']				= $in['spent'];
	$result['BUDGET_LEFT']			= $in['budget_left'];
	$result['PROCENT']			= $in['procent'];


json_out($result);
?>