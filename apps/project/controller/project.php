<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class ProjectEdit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getProject(){
		if($this->in['project_id'] == 'tmp'){ //add
			$this->getAddProject();
		}elseif ($this->in['template']) { # create quote template
			$this->getTemplateProject();
		}else{ # edit
			$this->getEditProject();
		}
	}

	public function getAddProject(){
		$in = $this->in;

		$next_function = 'project-project-project-updateCustomerData';
		$output=array('project_type_te_hourly'=>array(),'project_type_te_daily'=>array(),'project_type_fixed'=>array());
		$output['item_exists']			= true;
		$proj_type_te=array(1=>gm('task hourly rate'),2=>gm('person hourly rate'),3=>gm('project hourly rate'));
		$proj_type_te_daily=array('1' => gm('Task daily rate (each task can have its own daily rate)'),'2' => gm('Person daily rate (each user can have its own daily rate)'),'3' => gm('Project daily rate (all the tasks for this project have the same daily rate)'));
		$proj_type_val_te=array(1=>1,2=>2,3=>3);
		$proj_type_fixed=array(1=>gm('budget per task'),2=>gm('retainer (monthly fixed amount)'));
		$proj_type_val_fixed=array(1=>5,2=>6);
		$bunit=$this->db->field("SELECT count(id) FROM project_bunit");

		foreach($proj_type_te as $key=>$value){
			$p_hourly=array(
				'id'	=> $proj_type_val_te[$key],
				'name'	=> $value
			);
			array_push($output['project_type_te_hourly'], $p_hourly);
		}
		foreach($proj_type_te_daily as $key=>$value){
			$p_daily=array(
				'id'	=> $proj_type_val_te[$key],
				'name'	=> $value
			);
			array_push($output['project_type_te_daily'], $p_daily);
		}
		foreach($proj_type_fixed as $key=>$value){
			$p_fixed=array(
				'id'	=> $proj_type_val_fixed[$key],
				'name'	=> $value
			);
			array_push($output['project_type_fixed'], $p_fixed);
		}

		$details = array('table'	 => !$in['is_contact'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field' 	 => !$in['is_contact'] ? 'customer_id' : 'contact_id',
					 'value'	 => !$in['is_contact'] ? $in['buyer_id'] : $in['contact_id'],					 
					 'filter'	 => !$in['is_contact'] ? ' AND is_primary =1' : '',
					);

		$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");
	
		if($in['deal_id']){
			$output['deal_id']=$in['deal_id'];
			$deal_data=$this->db->query("SELECT * FROM tblopportunity WHERE opportunity_id='".$in['deal_id']."' ");
			if($deal_data->next()){
				if(!$in['buyer_id'] && $deal_data->f('buyer_id')){
					$in['buyer_id']=$deal_data->f('buyer_id');
					$output['add_customer']=false;
				}
				if(!$in['contact_id'] && $deal_data->f('contact_id')){
					$in['contact_id']=$deal_data->f('contact_id');
				}
				$in['name']=$deal_data->f('subject');
				$in['main_address_id']=$deal_data->f('main_address_id');
				$in['delivery_address_id']=$deal_data->f('same_address');
				$in['identity_id']=$deal_data->f('identity_id');
				$in['source_id']=$deal_data->f('source_id');
				$in['type_id']=$deal_data->f('type_id');
				$in['segment_id']=$deal_data->f('segment_id');
			}		
		}

		if($in['buyer_id'] ){
			$buyer_info = $this->db->query("SELECT customers.payment_term,customers.payment_term_type, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name,
					customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.invoice_note2, customers.internal_language,
					customers.attention_of_invoice,customers.line_discount, customers.apply_fix_disc, customers.apply_line_disc, customers.name AS company_name, customers.vat_regime_id,customers.identity_id
				  	FROM customers
				  	LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
				  	WHERE customers.customer_id = '".$in['buyer_id']."' ");
			$buyer_info->next();
			$name = $buyer_info->f('company_name').' '.$buyer_info->f('l_name');

			$text = gm('Company Name').':';
			$c_email = $buyer_info->f('c_email');
			$c_fax = $buyer_details->f('comp_fax');
			$c_phone = $buyer_details->f('comp_phone');
					
			$buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			$main_address_id			= $buyer_details->f('address_id');
			$sameAddress = false;
			if($buyer_details->f('is_primary')==1){
				$sameAddress = true;
			}
			$next_function='project-project-project-add';
			$in['vat_regime_id']=$buyer_info->f('vat_regime_id');
			if(!$in['vat_regime_id']){
				$vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

			    if(empty($vat_regime_id)){
			      $vat_regime_id = 1;
			    }
				$in['vat_regime_id'] = $vat_regime_id;
			}

			$output['deals']				= $this->get_deals($in);
		}else{
			$buyer_info = $this->db->query("SELECT phone, cell, email, CONCAT_WS(' ',firstname, lastname) as name FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
			$buyer_info->next();
			$c_email = $buyer_info->f('email');
			$c_fax = '';
			$c_phone = $buyer_info->f('phone');
			$text =gm('Name').':';
			$name = $buyer_info->f('name');
			if($in['contact_id']){
				$next_function='project-project-project-add';
			}
		}

		$free_field = $buyer_details->f('address').'
		'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
		'.get_country_name($buyer_details->f('country_id'));
		$name = stripslashes($name);

		$b_country_id=$buyer_details->f('country_id');
		$b_city=$buyer_details->f('city');
		$b_zip=$buyer_details->f('zip');
		$b_address=$buyer_details->f('address');
	      if($in['main_address_id'] && $in['main_address_id']!=$main_address_id){
			$main_address_id=$in['main_address_id'];
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
				$b_country_id=$buyer_addr->f('country_id');
				$b_city=$buyer_addr->f('city');
				$b_zip=$buyer_addr->f('zip');
				$b_address=$buyer_addr->f('address');
		}
		if($in['delivery_address_id']){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
			$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
		}
		
		$output['pick_date_format']		= pick_date_format();
		$output['country_dd']			= $in['country_id']? build_country_list($in['country_id']):build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['main_country_id']		= $in['country_id'] ? $in['country_id'] : ACCOUNT_BILLING_COUNTRY_ID;
		$output['language_dd']			= build_language_dd();
		$output['gender_dd']			= gender_dd();
		$output['title_dd']				= build_contact_title_type_dd(0);
		$output['title_id']				= '0';
		$output['gender_id']			= '0';
		$output['language_id']			= '0';
		$output['style']  			= ACCOUNT_NUMBER_FORMAT;
		$output['page_title']			= gm('Add project');
		$output['SEARCH']				= $in['search']? '&search='.$in['search'] : '';
		$output['FIRST']				= $in['first']? '&first='.$in['first']:'';
		$output['language_dd3'] 		= isset($in['languages']) ? build_language_dd_new($in['languages']) : build_language_dd_new(0);
		$output['START_DATE']			= time();
		$output['HIDE_BUNIT']			= $bunit > 0 ? true : false;
		$output['customer']			= 'customer';
		$output['contact']			= 'contact';
		$output['disable_next']			= $in['quote_id'] ? '' : 'disable_next';
		$output['main_address_id']		= $main_address_id;
		$output['sameAddress']			= $sameAddress;
		$output['buyer_id']             = $in['buyer_id'];
		$output['customer_id']			= $in['buyer_id'];
		$output['contact_id']           = $in['contact_id'];
		$output['buyer_name']           = $name;
		$output['is_contact']			= $in['is_contact'];
		$output['field']				= $in['buyer_id'] ? 'customer_id' : 'contact_id';
		$output['free_field']			= $in['customer_address'] ? $in['customer_address'] : $free_field;
		$output['free_field_txt']		= $in['customer_address'] ? nl2br($in['customer_address']) : nl2br($free_field);
		$output['delivery_address_id']	= $in['delivery_address_id'];
		$output['start_date']			= $in['start_date'] ? strtotime($in['start_date'])*1000 : time()*1000;
		$output['end_date']				= $in['end_date'] ? strtotime($in['end_date'])*1000 : '';
		$output['budget_type']			= $in['budget_type'] ? $in['budget_type'] : '1';
		$output['budget_dd_daily']		= build_budget_type_list_daily(0);
		$output['budget_dd_hourly']		= build_budget_type_list(0);
		$output['t_pr_hour']			= $in['t_pr_hour'];
		$output['send_email_alerts']	= $in['send_email_alerts'];
		$output['alert_percent']		= $in['alert_percent'];
		$output['invoice_method']		= $in['invoice_method'];
		$output['internal_value']		= $in['internal_value'];


		if($in['contact_id']){
			$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
		}

		$quote_task=false;
		$quote_art=false;
		if($in['quote_id']){
			$quote_inf=$this->db->query("SELECT * FROM tblquote WHERE id='".$in['quote_id']."' ");
			$output['quote_id']			= $in['quote_id'];
			$output['quote_hide']			= $in['quote_id'] ? false : true;
			$output['customer']			= '';
			$output['contact']			= '';
			$output['customer_name']		= urldecode($in['customer']);
			$output['customer_readonly']		= true;
			$output['currency_type']		= $in['currency_type'];
			$output['segment_id']			= $in['segment_id'];
			$output['source_id']			= $in['source_id'];
			$output['type_id']			= $in['type_id'];
			$in['vat_regime_id']=$quote_inf->f('vat_regime_id');
			$in['name']					= $quote_inf->f('subject');
			$output['main_address_id']		= $quote_inf->f('main_address_id');
			$output['sameAddress']			=!$quote_inf->f('delivery_address_id') ? '1' : '';
			$output['delivery_address_id']	= $quote_inf->f('delivery_address_id');
			if(!$quote_inf->f('delivery_address_id')){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$quote_inf->f('main_address_id')."' ");
			}else{
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$quote_inf->f('delivery_address_id')."' ");
			}
			$free_field = $buyer_addr->f('address').'
			'.$buyer_addr->f('zip').' '.$buyer_addr->f('city').'
			'.get_country_name($buyer_addr->f('country_id'));
			$output['free_field']			= $free_field;
			$output['free_field_txt']		= nl2br($free_field);

			if($in['quote_group_data'] && !empty($in['quote_group_data'])){
				$output['quote_group_data']=$in['quote_group_data'];
				$quote_task=true;
			}else{
				$groups = $this->db->query("SELECT DISTINCT tblquote_line.group_id,tblquote_group.title FROM tblquote_line
						INNER JOIN tblquote_version
						ON tblquote_line.version_id = tblquote_version.version_id
						INNER JOIN tblquote_group ON tblquote_line.group_id = tblquote_group.group_id
						LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
						WHERE tblquote_line.quote_id='".$in['quote_id']."'
						AND tblquote_line.content_type='1'
						AND tblquote_version.active='1'
						AND (tblquote_line.article_id='0' OR (tblquote_line.article_id>'0' AND pim_articles.is_service='1'))
						AND tblquote_line.is_tax='0'
						ORDER BY group_id ASC ");
				$i=1;
				$output['quote_group_data']=array();
				while($groups->next()){
					$quote_task=true;
					$data_line = $this->db->query("SELECT tblquote_line.* FROM tblquote_line
								INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
								LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
								WHERE tblquote_line.quote_id='".$in['quote_id']."'
								AND tblquote_line.content_type='1'
								AND tblquote_line.line_type<'3'
								AND tblquote_version.active='1'
								AND (tblquote_line.article_id='0' OR (tblquote_line.article_id>'0' AND pim_articles.is_service='1'))
								AND tblquote_line.is_tax='0'
								AND tblquote_line.group_id = '".$groups->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;

					$temp_data_line=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$d_line=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
						);
						array_push($temp_data_line, $d_line);
						$total+=$amount;
					}

					$quote_group_data=array(
						'group_title'		=> $groups->f('title') ? $groups->f('title') : gm('Group').' '.$i,
						'nr_crt'			=> $i,
						'group_id'			=> $groups->f('group_id'),
						'checked'		=> false,
						'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
						'data_line'			=> $temp_data_line
					);
					if($total>0){
						array_push($output['quote_group_data'], $quote_group_data);
						$i++;
					}
				}			
			}
			if($in['quote_group_articles'] && !empty($in['quote_group_articles'])){
				$output['quote_group_articles']=$in['quote_group_articles'];
				$quote_art=true;
			}else{
				$articles = $this->db->query("SELECT DISTINCT tblquote_line.group_id,tblquote_group.title FROM tblquote_line
						INNER JOIN tblquote_version
						ON tblquote_line.version_id = tblquote_version.version_id
						INNER JOIN tblquote_group ON tblquote_line.group_id = tblquote_group.group_id
						LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
						WHERE tblquote_line.quote_id='".$in['quote_id']."'
						AND tblquote_line.content_type='1'
						AND tblquote_version.active='1'
						AND (tblquote_line.article_id>'0' AND pim_articles.is_service='0' AND tblquote_line.is_tax='0')
						ORDER BY group_id ASC ");
				$j=1;
				$output['quote_group_articles']=array();
				while($articles->next()){
					$quote_art=true;
					$data_line = $this->db->query("SELECT tblquote_line.* FROM tblquote_line
								INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
								LEFT JOIN pim_articles ON tblquote_line.article_id=pim_articles.article_id
								WHERE tblquote_line.quote_id='".$in['quote_id']."'
								AND tblquote_line.content_type='1'
								AND tblquote_line.line_type<'3'
								AND tblquote_version.active='1'
								AND (tblquote_line.article_id>'0' AND pim_articles.is_service='0' AND tblquote_line.is_tax='0')
								AND tblquote_line.group_id = '".$articles->f('group_id')."'
								ORDER BY id ASC ");
					$total = 0;
					$temp_articles=array();
					while($data_line->next()){
						$amount = $data_line->f('amount');
						if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
								$amount = $amount*return_value($currency_rate);
						}
						$art=array(
							'item'			=> $data_line->f('name'),
							'quantity'			=> display_number($data_line->f('quantity')),
							'unit_price'		=> display_number_var_dec($data_line->f('price')),
							'amount'			=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
						);
						array_push($temp_articles, $art);
						$total+=$amount;
					}
					$quote_group_articles=array(
							'group_title'		=> $articles->f('title') ? $articles->f('title') : gm('Group').' '.$j,
							'nr_crt'			=> $j,
							'group_a_id'		=> $articles->f('group_id'),
							'checked'		=> false,
							'total'			=> place_currency(display_number($total),get_commission_type_list($currency_type)),
							'articles'			=> $temp_articles
					);
					if($total>0){
						array_push($output['quote_group_articles'], $quote_group_articles);
						$j++;
					}
				}		
			}		
		}

		$bunit_count=$this->db->field("SELECT COUNT(id) FROM project_bunit");

		$quote_subject = $this->db->field("SELECT subject FROM tblquote WHERE id='".$in['quote_id']."' ");

		$output['quote_task']		= $quote_task ? true : false;
		$output['quote_art']		= $quote_art ? true : false;
		$output['quote_task_e']		= $quote_task;
		$output['quote_art_e']		= $quote_art;
		$output['do_next']		= $next_function;
		$output['cc']			= $this->get_cc($in);
		$output['contacts']		= $this->get_contacts($in);
		$output['addresses']		= $this->get_addresses($in);
		$output['users_list']			= $this->get_users_list($in);
		$output['bunits']			= $this->get_bunits_list($in);
		$output['main_comp_info']	= getCompanyInfo($in);
		$output['BUNIT_HIDE']		= $bunit_count > 0 ? true : false;
		$output['project_id']		= $in['project_id'];
		$output['add_customer']		= false;
		$output['edit_access']		= true;
		//$output['serial_number']	= $in['serial_number'] ? $in['serial_number'] : generate_project_number(DATABASE_NAME);
		$output['name']			= $in['name'];
		$output['ACCOUNT_CURRENCY'] = get_currency(ACCOUNT_CURRENCY_TYPE);
		$output['project_type']=$in['project_type'];
		$output['project_rate']=$in['project_rate'];
		$output['billable_type']=$in['billable_type'];
		$output['task_rate']=$in['task_rate'];
		$output['vat_regim_dd']			= build_vat_regime_dd();
		$output['vat_regime_id']=$in['vat_regime_id'];
		$output['identity_id']=$in['identity_id'];
		$output['source_dd']= get_categorisation_source();
		$output['type_dd']= get_categorisation_type();
		$output['source_id']=$in['source_id'];
		$output['type_id']=$in['type_id'];
		$output['segment_id']=$in['segment_id'];
		$output['segment_dd']= get_categorisation_segment();

		$this->out = $output;
	}

	public function getTemplateProject(){
		//
	}

	public function getEditProject(){
		$in = $this->in;
		$project_exists = $this->db->field("SELECT project_id FROM projects WHERE  project_id='".$in['project_id']."'  ");
		if(!$project_exists){
			msg::error('Project does not exist','error');
			$in['item_exists']= false;
		    json_out($in);
		}

		$output=array();
		$output['item_exists']			= true;
		$b_type = array('1' => gm('task hourly rate'),'2' => gm('person hourly rate'),'3' => gm('project hourly rate'),'5' => gm('budget per task'),'6' => gm('retainer (monthly fixed amount)'),'7'=>gm('Functions (based on the function of your employees)'));
		$b_type2 = array('1' => 'Task hourly rate','2' => 'Person hourly rate','3' => 'Project hourly rate','5' => 'Budget per task','6' => 'Tetainer','7'=>'Functions');

		$b_type_1 = array('1' => gm('Task daily rate (each task can have its own daily rate)'),'2' => gm('Person daily rate (each user can have its own daily rate)'),'3' => gm('Project daily rate (all the tasks for this project have the same daily rate)'),'5' => gm('budget per task'),'6' => gm('retainer (monthly fixed amount)'),'7'=>gm('Functions (based on the function of your employees)'));
		$b_type_2 = array('1' => 'Task daily rate','2' => 'Person daily rate','3' => 'Project daily rate','5' => 'Budget per task','6' => 'Tetainer','7'=>'Functions');

		/*$tips = $page_tip->getTip('project-project');*/
		$project_finished = true;

		$projects_hours = $this->db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$in['project_id']."' AND billable='1' and billed='0' and approved='1'");

		$next_function = 'project-project-project-update';
		$bunit_count=$this->db->field("SELECT COUNT(id) FROM project_bunit");
		$project = $this->db->query("SELECT projects.*, customers.customer_id, customers.is_admin AS is_admin, project_bunit.name as bunit_name
		            FROM   projects
		            INNER  JOIN customers ON customers.customer_id=projects.customer_id
		            LEFT JOIN project_bunit ON projects.bunit=project_bunit.id
		            WHERE  project_id='".$in['project_id']."'  ");
		
		if(!$project->next()){
			$project = $this->db->query("SELECT projects.* FROM projects WHERE  project_id='".$in['project_id']."'  ");
			$project->next();
		}

		$hide_b_type = array(5,6,7);

		$in['step'] = $project->f('step');

		$in['billable_type'] = $project->f('billable_type');
		$output['hide_collaps']			= false;
		$output['hide_add_task']		= $project->f('billable_type') == 7 ? false : true;
		$output['hide_change_b_type']		= in_array($project->f('billable_type'), $hide_b_type) ? false : true;
		
		$project_name=$project->f('name');
		$ref = '';
		if(ACCOUNT_PROJECT_REF && $project->f('our_reference')){
			$ref = $project->f('our_reference').ACCOUNT_PROJECT_DEL;
		}
		$billable_dd = $project->f('status_rate') > 0 ? build_budget_type_list_daily($project->f('budget_type')) : build_budget_type_list($project->f('budget_type'));

		if($project->f('billable_type') == 5){
			$billable_dd = build_budget_type_list($project->f('budget_type'),3);
		}
		$hide_invoice = '';
		$invoice_link = 'buyer_id='.$project->f('customer_id').'&customer_name='.urlencode($project->f('company_name')).'&base_type=1&project_id='.$in['project_id'].'&currency_type='.$project->f('currency_type').'&billable_type='.$project->f('billable_type');
		$base_type = 1;
		if(!$projects_hours && $project->f('billable_type') == '6'){
			$hide_invoice = false;
		}
		if($project->f('billable_type') == '4'){
			$invoice_link = '';
			$hide_invoice = false;
		}
		if($project->f('billable_type') == '5'){
			$hide_invoice = false;
			$tasks = $this->db->query("SELECT task_id FROM tasks WHERE project_id='".$in['project_id']."' AND billable='1' AND closed='1' ");
			if($tasks->next()){
				$hide_invoice = '';
			}
			$invoice_link = 'buyer_id='.$project->f('customer_id').'&customer_name='.urlencode($project->f('company_name')).'&base_type=4&project_id='.$in['project_id'].'&currency_type='.$project->f('currency_type').'&billable_type='.$project->f('billable_type');
		}

		$purchas = $this->db->query("SELECT project_purchase_id FROM project_purchase WHERE project_id='".$in['project_id']."' AND delivered='1' AND billable='1' AND invoiced='0' ");
		if($purchas->next() && !empty($hide_invoice)){
			$hide_invoice = '';
			if(!$invoice_link){
				$invoice_link = 'project_id='.$in['project_id'].'&buyer_id='.$project->f('customer_id').'&currency_type='.$project->f('currency_type').'&billable_type='.$project->f('billable_type');
			}
		}
		$in['is_contact'] = $project->f('is_contact');
		if(!$in['is_contact']){
			$in['data_val'] = 1;
		}

		$details = array('table' 				=> !$project->f('is_contact') ? 'customer_addresses' : 'customer_contact_address' ,
							 'field'		=> !$project->f('is_contact') ? 'customer_id' : 'contact_id',
							 'value'		=> !$project->f('is_contact') ? $project->f('customer_id') : $project->f('contact_id'),
							 'name'		=> $project->f('buyer_name'),
							 'filter'		=> !$project->f('is_contact') ? ' AND is_primary =1' : '' );

		$buyer_details = $this->db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ".$details['filter']." ");

		if($in['is_contact']){
			$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
		}else{
			$free_field = $buyer_details->f('address').'
			'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
			'.get_country_name($buyer_details->f('country_id'));
		}

		if(!$in['buyer_id'] && !$project->f('is_contact')){
			$in['buyer_id']=$project->f('customer_id');
		}else if(!$in['contact_id'] && $project->f('is_contact')){
			$in['contact_id']=$project->f('customer_id');
		}	

		$output['retainer_budget'] = display_number($project->f('retainer_budget'));

		$lead_source=$this->db->field("SELECT name FROM tblquote_source WHERE id='".$project->f('source_id')."'");

		$budget_follow = array('1' => gm('No budget'),'2' => gm('Total project hours'),'3' => gm('Total project fees'),'4' => gm('Hours per task'),'5' => gm('Hours per person'));
		$budget_follow_daily = array('1' => gm('No budget'),'2' => gm('Total project days'),'3' => gm('Total project fees'),'4' => gm('Days per task'),'5' => gm('Days per staff'));
		$budget_follow_value=$project->f('status_rate') > 0 ? $budget_follow_daily[$project->f('budget_type')] : $budget_follow[$project->f('budget_type')];
		if($project->gf('budget_type') == '2'){
			$budget_follow_value=$project->f('status_rate') > 0 ? $budget_follow_daily[$project->f('budget_type')].': '.$project->f('t_pr_hour').' '.gm('Days') : $budget_follow[$project->f('budget_type')].': '.$project->f('t_pr_hour').' '.gm('Hours');
		}else if($project->gf('budget_type') == '3'){
			$budget_follow_value=$project->f('status_rate') > 0 ? $budget_follow_daily[$project->f('budget_type')].': '.$project->f('t_pr_fees').' &euro;' : $budget_follow[$project->f('budget_type')].': '.$project->f('t_pr_fees').' &euro;';
		}else{
			$budget_follow_value=$project->f('status_rate') > 0 ? $budget_follow_daily[$project->f('budget_type')] : $budget_follow[$project->f('budget_type')];
		}
		$in['cat_id']= $project->f('is_contact') ==0 ? $this->db->field('SELECT cat_id FROM customers WHERE customer_id ='.$project->f('customer_id')) : 0;
		$quote_downpayment=$this->db->field("SELECT tblinvoice.id FROM tblinvoice 
						INNER JOIN projects ON tblinvoice.quote_id=projects.quote_id
						WHERE projects.project_id='".$in['project_id']."' AND tblinvoice.downpayment_inv='1' ");
		if($project->f('customer_id') && !$project->f('is_contact')){
			$buyer_credit_limit_info=$this->db->query("SELECT credit_limit,currency_id FROM customers WHERE customer_id='".$project->f('customer_id')."' ");
			if($buyer_credit_limit_info->f('credit_limit')){
				$invoices_customer_due=customer_credit_limit($project->f('customer_id'));
				if($invoices_customer_due && $invoices_customer_due>$buyer_credit_limit_info->f('credit_limit')){
					$buyer_currency_id=$buyer_credit_limit_info->f('currency_id') ? $buyer_credit_limit_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
					$output['customer_credit_limit']=place_currency(display_number($buyer_credit_limit_info->f('credit_limit')),get_commission_type_list($buyer_currency_id));
				}
			}	
		}


		$output['main_address_id']			= $project->f('main_address_id');
		$output['name']			       	= $project->f('name');
		$output['assign_text']				= $in['is_contact'] ? gm("Assign to a company") : gm("Assign to a person");
		$output['company_txt']				= $in['is_contact'] ? gm('Person') : gm('Company');
		$output['CUSTOMER']	           		= $project->f('is_contact') == 0 ? $project->f('company_name') : '';
		$output['CONTACT']	           		= $project->f('is_contact') == 1 ? $project->f('company_name') : '';
		$output['buyer_name']				= $project->f('company_name');
		$output['is_contact']				= $in['is_contact'];
		$output['buyer_id']				= $in['is_contact'] == 0 ? $project->f('customer_id') : 0;
		$output['contact_id']				= $project->f('contact_id');
		$output['contact_name']				= $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$project->f('contact_id')."' ");	
		$output['free_field']				= $project->f('customer_address') ? $project->f('customer_address') : $free_field;
		$output['free_field_txt']			= trim($project->f('customer_address')) ? nl2br($project->f('customer_address')) : nl2br($free_field );
		$output['sameAddress']				= !$project->f('same_address') ? true : false;
		$output['delivery_address_id']		= $project->f('same_address');
		$output['cc']					= $this->get_cc($in);
		$output['contacts']				= $this->get_contacts($in);
		$output['addresses']				= $this->get_addresses($in);
		$output['field']					= !$in['is_contact'] ? 'customer_id' : 'contact_id';	
		//$output['main_comp_info']			= $this->getCompanyInfo();
		$output['CUSTOMER2']	           		= $in['is_contact'] ? $project->f('company_name')." > " : $project->f('company_name');
		$output['CUSTOMER_URL']	          		= urlencode($project->f('company_name'));
		$output['CUSTOMER_ID']	       		= $project->f('customer_id');
		$output['CODE']			       	= $project->f('code');
		$output['NOT_BILLABLE_CHECKED'] 		= $project->f('invoice_method') == 0 ? true : false;
		$output['BILLABLE_CHECKED']	   		= $project->f('invoice_method') == 1 ? true : false;
		$output['DISABLED_BILLABLE2']   		= $project->f('invoice_method') == 0 ? true : false;
		$output['invoice_method']			= $project->f('invoice_method');
		$output['ACCOUNT_CURRENCY']     		= get_currency(ACCOUNT_CURRENCY_TYPE);
		$output['pr_h_rate']		       	= display_number($project->f('pr_h_rate'));
		$output['BUDGET_TYPE_DD']      		= $billable_dd;
		$output['budget_type']				= $project->f('budget_type');
		$output['budget_type_name']			= $project->f('status_rate') > 0 ? build_budget_type_list_daily($project->f('budget_type'),'',false) : build_budget_type_list($project->f('budget_type'),'',false);
		$output['t_pr_hour']            		= $project->f('t_pr_hour');
		$output['t_pr_fees']            		= $project->f('t_pr_fees');
		$output['SHOW_BUDGET_TO_ALL_CHECKED'] 	= $project->f('show_budget_to_all') == 1 ? true : false;
		$output['send_email_alerts'] 			= $project->f('send_email_alerts') == 1 ? true : false;
		$output['alert_percent']	           	= $project->f('alert_percent');
		$output['DISABLED_ALERT_PERCENT']   	= $project->f('send_email_alerts') == 1 ? false : true;
		$output['notes']			     		= $project->f('notes');
		$output['NOTES_TXT']				= $project->f('notes');
		$output['VIEW_BUGET_OPTIONS']        	= $project->f('budget_type')   == 1 ? false : true;
		$output['PR_H_RATE_VIEW']            	= $project->f('billable_type') == 3 ? true : false;
		$output['VIEW_TOTAL_TASK_BUDGET']    	= $project->f('billable_type') == 1 ? true : false;
		$output['VIEW_T_HOURS']              	= $project->f('budget_type')   == 4 ? true : false;
		$output['VIEW_TOTAL_USER_BUDGET']    	= $project->f('billable_type') == 2 ? true : false;
		$output['VIEW_U_HOURS']             	= $project->f('budget_type') == 5 ? true : false;
		$output['CLOSE_P']				= '';
		$output['HIDE_TOOLS']				= $hide_invoice=='hide' && $_SESSION['acc_type']=='free'? false : true;
		$output['CLOSE_CHECKED']   			= $project->f('closed') == 1 ? true : false;
		$output['add_expenses']				= $project->f('add_expense') == 1 ? true : false;
		$output['JS']					= ($project->f('add_expense') == 1) && ($project->f('invoice_method') == 1) ? "$('.c_expenses').show();" : '';
		$output['start_date']				= $project->f('start_date') > 0 ? $project->f('start_date')*1000 : '';
		$output['end_date']				= $project->f('end_date') ? $project->f('end_date')*1000 : '';
		$output['END_D']					= $project->f('end_date') ? $project->f('end_date')*1000 : '';
		$output['BUNIT']					= $bunit_count > 0 ? $project->f('bunit_name') : '';
		$output['BUNIT_HIDE']				= $bunit_count > 0 ? true : false;
		$output['BUNIT_OPEN']				= $bunit_count > 0 ? 'open' : '';
		$output['bunit_id']				= $project->f('bunit');
		$output['bunits']					= $this->get_bunits_list($in);
		$output['checked_internal']			= $project->f('invoice_method') > 0 ? false : true;
		$output['internal_value']			= $project->f('project_type')=='3' ? true : false;
		$output['PROJECT_NUMBER']			= $project->f('serial_number') ? $project->f('serial_number') : generate_project_number(DATABASE_NAME);
		$output['serial_number']			= $project->f('serial_number') ? $project->f('serial_number') : $ref.generate_project_number(DATABASE_NAME);
		$output['REF']					= $ref ? " var ref= '".$ref."';":' var ref;';		
		$output['CLOSE_TXT']				= $project->f('closed') == 1 ? gm("Open project") : gm("Close project");
		$output['CLOSE_ID']				= $project->f('closed') == 1 ? "open_p" : "close_p";
		$output['HIDE_NOT']				= $project->f('invoice_method') == 1 ? true : false;
		$output['HIDE_YES']				= $project->f('invoice_method') == 1 ? false : ($project->f('is_admin') == 1 ? false : true);
		$output['BIL_TYPE']				= $project->f('billable_type');
		$output['B_TYPE']					= $project->f('budget_type');
		$output['MANAGER']				= serialize($in['manager']);
		$output['BILLABLE_T']				= serialize($in['billable']);
		$output['HIDE_INT']				= $project->f('is_admin') == 1  ? false : true;
		$output['HIDE_INT_N']				= $project->f('is_admin') == 1  ? true : false;
		$output['IS_ADMIN']				= $project->f('is_admin');
		$output['READONLY']				= true;
		$output['INVOICE_LINK']				= $invoice_link;
		$output['HIDE_INVOICE']				= $hide_invoice;
		$output['BASE_TYPE']				= $base_type;
		$output['our_reference']			= $in['our_reference'] ? $in['our_reference'] : $ref;
		$output['currency_type']			= $project->f('currency_type');
		$output['quote_id']				= $project->f('quote_id');
		$output['buyer_or_contact']			= $in['is_contact'] ? 'contact_id' : 'buyer_id';
		$output['buyer_or_contact2']			= $in['is_contact'] ? 'contact_name' : 'customer_name';
		$output['quote_link']				= 'index.php?do=quote-nquote-quote-project_to_quote&'.($in['is_contact'] ? 'contact_id' : 'buyer_id').'='.$project->f('customer_id').'&'.($in['is_contact'] ? 's_customer_id' : 's_buyer_id' ).'='.urlencode($project->f('company_name')).'&project_id='.$in['project_id'].'&currency_type='.$project->f('currency_type');
		$output['retainer_hide']			= $project->f('billable_type') == 6 ? true : false;
		$output['project_finished']			= $project_finished;
		$output['bill_type_name']			= $project->f('status_rate') > 0 ? ($project->f('billable_type')==3 ? $b_type_1[$project->f('billable_type')].': '.display_number($project->f('pr_h_rate')).' '.get_currency(ACCOUNT_CURRENCY_TYPE) : $b_type_1[$project->f('billable_type')]) : ($project->f('billable_type')==3 ? $b_type[$project->f('billable_type')].': '.display_number($project->f('pr_h_rate')).' '.get_currency(ACCOUNT_CURRENCY_TYPE) : $b_type[$project->f('billable_type')]);
		$output['HIDE_ADD']		 		= $in['step'] ? false : true;
		$output['source_id']				= $project->f('source_id');
		$output['lead_source']				= $this->db->field("SELECT name FROM tblquote_source WHERE id='".$project->f('source_id')."' ");
		$output['SOURCE_DD']				= build_data_dd('tblquote_source',$project->f('source_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['lead_source']				= $lead_source ? $lead_source : '';
		$output['budget_follow_value']		= $budget_follow_value;
		$output['a_links_hourly']			= $project->f('status_rate') == 0 ? 'project_rate_selected' : 'a_links';
		$output['a_links_daily']			= $project->f('status_rate') == 0 ? 'a_links' : 'project_rate_selected';
		$output['project_rate_status']		= $project->f('status_rate');
		$output['project_not_fixed']			= ($project->f('billable_type') != 5 && $project->f('billable_type') != 6) ? true : false;
		$output['checked_hourly']			= $project->f('status_rate') == 0 ? true : false;
		$output['checked_daily']			= $project->f('status_rate') == 1 ? true : false;
		$output['DRAFT_HIDE']				= $project->f('stage') != 0 ? false : true;
		$output['stage_status']				= $project->f('stage');
		$output['DRAFT_SELECTED']			= $project->f('stage') == 0 ? true : false;
		$output['ACTIVE_SELECTED']			= $project->f('stage') == 1 ? true : false;
		$output['CLOSED_SELECTED']			= $project->f('stage') == 2 ? true : false;
		$output['start_done']				= $project->f('start_done');
		$output['ACTIVE_HIDE']				= $project->f('stage') != 1 ? false : true;
		$output['CLOSED_HIDE']				= $project->f('stage') != 2 ? false : true;
		$output['INVOICES_HIDE']			= $project->f('stage') != 0 ? true : false;
		$output['ACTIVE_INV_HIDE']			= ($project->f('stage') == 1 || $project->f('stage') == 2) ? true : false;
		$output['BILLABLE_SIX']				= $project->f('billable_type') == 6 ? true : false;
		$output['billable_number']			= $project->f('billable_type');
		$output['billable_type']			= $project->f('billable_type');
		$output['task_rate']				= $project->f('status_rate') > 0 ? gm('Days'): gm('Hours');
		$output['double_fixed_hide']			= ($project->f('billable_type')==5 || $project->f('billable_type')==6 || $project->f('invoice_method')==0) ? false : true;
		$output['hide_internal']			= $project->f('invoice_method')==0 ? false : true;
		$output['approve_hours']			= $project->f('status_rate') == 0 ? gm('Approve hours') : gm('Approve days');
		$output['all_hours']				= $project->f('status_rate') == 0 ? gm('All hours') : gm('All days');
		$output['undo_all_approved']			= $project->f('status_rate') == 0 ? gm('Undo all approved hours from') : gm('Undo all approved days from');
		$output['cat_id']					= $in['cat_id'];
		$output['closed_project']			= $project->f('stage')==2 ? false : true;
		$output['quote_downpayment']			= $quote_downpayment ? true : false;
		$output['downpayment']				= $quote_downpayment;
		$output['downpayment_draft_hide']		= ($project->f('stage')==0 && $quote_downpayment) ? false : true;
		$output['project_rate']				= $project->f('invoice_method');
		$output['project_type']				= !$project->f('project_type') ? (!$project->f('invoice_method') ? '3' : ($project->f('billable_type')>=5 ? '2' : '1')) : $project->f('project_type');
		$output['vat_regim_dd']					= build_vat_regime_dd();
		$output['vat_regime_id']				= $project->f('vat_regime_id') ? $project->f('vat_regime_id') : '';
		$in['identity_id']=$project->f('identity_id');
		$output['main_comp_info']   = getCompanyInfo($in);
        	$output['identity_id']=$in['identity_id'];
       	$output['source_dd']= get_categorisation_source();
        	$output['type_dd']= get_categorisation_type();
        	$output['source_id']=$project->f('source_id');
        	$output['type_id']=$project->f('type_id');
        	$output['segment_dd']= get_categorisation_segment();
		$output['segment_id']=$project->f('segment_id');
		$output['deals']= $this->get_deals($in);
		if($project->f('trace_id')){
			$linked_doc=$this->db->field("SELECT tracking_line.origin_id FROM tracking_line
				INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
					WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$project->f('trace_id')."' ");
			if($linked_doc){
				$output['deal_id']=$linked_doc;	
			}
		}

		$in['currency_type'] = $project->f('currency_type');
		if($_SESSION['acc_type']=='free')
		{
			$output['HIDE_WHEN_FREE'] 		= false;
		}else
		{
			$output['HIDE_WHEN_FREE'] 		= true;
		}

		$proj_type_te=array(1=>gm('task hourly rate'),2=>gm('person hourly rate'),3=>gm('project hourly rate'),4=>gm('Functions (based on the function of your employees)'));
		$proj_type_te_1=array(1=>gm('Task daily rate (each task can have its own daily rate)'),2=>gm('Person daily rate (each user can have its own daily rate)'),3=>gm('Project daily rate (all the tasks for this project have the same daily rate)'),4=>gm('Functions (based on the function of your employees)'));
		$proj_type_val_te=array(1=>1,2=>2,3=>3,4=>7);
		$proj_type_fixed=array(1=>gm('budget per task'),2=>gm('retainer (monthly fixed amount)'));
		$proj_type_val_fixed=array(1=>5,2=>6);

		$output['bill_type']=array();
		if(array_search($project->f('billable_type'), $proj_type_val_te) > 0){
			if($project->f('status_rate') > 0){
				foreach ($proj_type_te_1 as $key => $value) {
					$bill_type=array(
						'id' 				=> $proj_type_val_te[$key],
						'name'			=> $value,
						'label'			=> $b_type_2[$proj_type_val_te[$key]],
						'bill_checked'		=> $proj_type_val_te[$key] == $project->f('billable_type') ? true : false,
						'disabled' 			=> $project->f('invoice_method') == 0 ? true : false,
						'readonly'			=> in_array($project->f('billable_type'), $hide_b_type) ? true : false,
					);
					if($key==4){
						$bill_type['HIDE_BILLABLE'] 	= false;
					}
					if(in_array($project->f('billable_type'), $hide_b_type) && $proj_type_val_te[$key] != $project->f('billable_type')){
						$bill_type['disabled'] 	= true;
					}elseif (!in_array($project->f('billable_type'), $hide_b_type) && in_array($proj_type_val_te[$key], $hide_b_type)) {
						$bill_type['disabled'] 	= true;
					}
					array_push($output['bill_type'], $bill_type);
				}
			}else{
				foreach ($proj_type_te as $key => $value) {
					$bill_type=array(
						'id' 				=> $proj_type_val_te[$key],
						'name'			=> $value,
						'label'			=> $b_type2[$proj_type_val_te[$key]],
						'bill_checked'		=> $proj_type_val_te[$key] == $project->f('billable_type') ? true : false,
						'disabled' 			=> $project->f('invoice_method') == 0 ? true : false,
						'readonly'			=> in_array($project->f('billable_type'), $hide_b_type) ? true : false,
					);
					if($key==4){
						$bill_type['HIDE_BILLABLE'] 	= false;
					}
					if(in_array($project->f('billable_type'), $hide_b_type) && $proj_type_val_te[$key] != $project->f('billable_type')){
						$bill_type['disabled'] = true;
					}elseif (!in_array($project->f('billable_type'), $hide_b_type) && in_array($proj_type_val_te[$key], $hide_b_type)) {
						$bill_type['disabled'] = true;
					}
					array_push($output['bill_type'], $bill_type);
				}
			}
		}else{
			foreach ($proj_type_fixed as $key => $value) {
				$bill_type=array(
					'id' 				=> $proj_type_val_fixed[$key],
					'name'			=> $value,
					'label'			=> $b_type2[$proj_type_val_fixed[$key]],
					'bill_checked'		=> $proj_type_val_fixed[$key] == $project->f('billable_type') ? true : false,
					'disabled' 			=> $project->f('invoice_method') == 0 ? true : false,
					'readonly'			=> in_array($project->f('billable_type'), $hide_b_type) ? true : false,
				);
				if(in_array($project->f('billable_type'), $hide_b_type) || $proj_type_val_fixed[$key] != $project->f('billable_type')){
					$bill_type['disabled'] = true;					
				}elseif (!in_array($project->f('billable_type'), $hide_b_type) && in_array($proj_type_val_fixed[$key], $hide_b_type)) {
					$bill_type['disabled'] = true;
				}
				array_push($output['bill_type'], $bill_type);
			}
		}

		$is_contact = 0;
		if($project->f('is_contact')) {
			$is_contact = 1;
		}

		$drop_info = array('drop_folder' => 'projects', 'customer_id' => $project->f('customer_id'), 'item_id' => $in['project_id'], 'isConcact' => $is_contact, 'serial_number' => $project->f('serial_number'));

		$output['is_customer']		= true;
		$output['is_drop']		= defined('DROPBOX') && (DROPBOX != '') ? true : false;
		$output['is_data']		= $is_data;
		//$output['drop_info']		= htmlentities(json_encode($drop_info));
		$output['drop_info']		= $drop_info;

		$output['expenses']=array();
		$expenses=$this->db->query("SELECT expense.*,project_expences.project_id FROM expense LEFT JOIN project_expences ON expense.expense_id=project_expences.expense_id AND project_expences.project_id='".$in['project_id']."'  ");
		$expenses_view='';
		$i=0;
		while ($expenses->move_next()){
			$expenses_view.=$expenses->f('project_id') ? $expenses->f('name').',' : '';
			$expense_line=array(
			  	'EXPENSE'			    	=> $expenses->f('name'),
				'EXPENSE_ID'			=> $expenses->f('expense_id'),
				'EXP_CHECKED'			=> $expenses->f('project_id') ? true : false,
			);
			array_push($output['expenses'], $expense_line);
			$i++;
		}
		$currency_selected=$this->db->query("SELECT code,value FROM currency WHERE id='".$project->f('currency_type')."'");
		$article_locations=$this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
		$article_stock_warning=$this->db->field("SELECT value FROM settings WHERE constant_name='SHOW_STOCK_WARNING'");

		$output['country_dd']			= build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		$output['main_country_id']		= ACCOUNT_BILLING_COUNTRY_ID;
		$output['do_next']			= $next_function;
		$output['SEARCH']				= $in['search']? '&search='.$in['search'] : '';
		$output['FIRST']				= $in['first']? '&first='.$in['first']:'';
		$output['project_id']   		= $in['project_id'];
		$output['STYLE']				= ACCOUNT_NUMBER_FORMAT;
		$output['PICK_DATE_FORMAT']		= pick_date_format();
		$output['is_admin']			= $_SESSION['group'] == 'admin' ? true : in_array('project', $_SESSION['admin_sett']);
		$output['page_tip']			= $tips;
		$output['HIDE_CANCEL_PREVIOUS']	= $in['step'] ? true : false;			
		$output['CURRENCY_TYPE_LIST']		= build_currency_list($in['currency_type']);
		$output['currency_selected']		= $currency_selected->f('code').' '.$currency_selected->f('value');
		$output['expenses_view']		= $project->f('add_expense') == 1 ? trim($expenses_view,',') : '';
		$output['article_loc']			= $article_locations==1? true : false;
		$output['article_stock_warn']		= $article_stock_warning==1 ? true : false;
		$output['tasks']				= $this->get_tasks($in);		
		$output['users']				= $this->get_users($in);
		$output['users_list']			= $this->get_users_list($in);
		$output['articles']			= $this->get_articles($in);
		$output['articles_list']		= $this->get_articles_list($in);
		$output['purchases']			= $this->get_purchases($in);
		$in['only_service']			= true;
		$output['tasks_list']			= $this->get_articles_list($in);

		$select_weeks = build_timesheet_week_from_project($in['project_id']);

		$output['weeks']=array();
		foreach ($select_weeks as $key => $value) {
			$week_line=array(
				'name'		=> date(ACCOUNT_DATE_FORMAT,$value[0]).' - '.date(ACCOUNT_DATE_FORMAT,$value[1]),
				'value'		=> $value[0].'-'.$value[1],
			);
			array_push($output['weeks'], $week_line);
		}

		if($_SESSION['group'] == 'admin'){
			$output['edit_access']		= true;
			$output['above_PM']		= true;
		}else{
			$perm_proj_module=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$perm_array=explode(";",$perm_proj_module);
			if(in_array('3',$perm_array)){
				$perm_mod_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_3' AND value='project' ");
				if($perm_mod_admin){
					$output['edit_access']		= true;
					$output['above_PM']		= true;
				}else{
					$perm_proj_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' AND value='1' ");
					if($perm_proj_admin){
						$output['edit_access']		= true;
						$output['above_PM']		= true;
					}else{
						$perm_proj_man=$this->db->field("SELECT manager FROM project_user WHERE user_id='".$_SESSION['u_id']."' AND project_id='".$in['project_id']."' AND manager='1' ");
						if($perm_proj_man){
							$output['edit_access']		= true;
						}
					}
				}
			}
		}
		$articles_denied=$this->db_users->field("SELECT plan FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$output['articles_allowed']	= ($articles_denied==2 || $articles_denied==5) ? false : true;

		$invoice_nr_row = array();
    	$linked_downpayment_invoice=false;
    	$inv_nr = $this->db->query("SELECT tblinvoice.id,tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.due_date, tblinvoice.paid,tblinvoice.f_archived,tbldownpayments.id as downpayent_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id = tracking.trace_id
						INNER JOIN tblinvoice ON tracking.target_id = tblinvoice.id
						LEFT JOIN tbldownpayments ON tblinvoice.id = tbldownpayments.invoice_id AND tbldownpayments.project_id='".$in['project_id']."'
						WHERE tracking_line.origin_id = '".$in['project_id']."' AND tracking_line.origin_type ='3' AND (tracking.target_type ='1' OR tracking.target_type ='7') ")->getAll();

    	foreach($inv_nr as $key=>$value){
			$type_title='';
		    switch ($value['type']){
			case '0':
			    $type_title = gm('Regular invoice');
			    break;
			case '1':
			    $type_title = gm('Proforma invoice');
			    break;
			case '2':
			    $type_title = gm('Credit Invoice');
			    break;
		    }	    

		    if($value['sent'] == '0' && $value['type']!='2'){
				$type_title = gm('Draft');
		    }else if($value['status']=='1'){
				$type_title = $value['paid']=='2' ? gm('Partially Paid') : gm('Paid');
		    }else if($value['sent']!='0' && $value['not_paid']=='0'){
				$type_title = $value['status']=='0' && $value['due_date']< time() ? gm('Late') : gm('Final');
		    } else if($value['sent']!='0' && $value['not_paid']=='1'){
				$type_title = gm('No Payment');
		    }

		    $downp_txt = "";
		    if($value['downpayent_id']){
		    	$downp_txt = gm('Downpayment');
		    	$linked_downpayment_invoice=true;
		    }

	    	array_push($invoice_nr_row, array( 'serial' => ($value['f_archived']=='1' ? '': $value['serial_number']), 'id'=> $value['id'], 'status'=> ($value['f_archived']=='1' ? gm('Archived') : $type_title),'downp_txt'=>$downp_txt));
	    
		}
		$output['downpayment_made']=$linked_downpayment_invoice ? true : false;


		$this->out = $output;
	}

	function get_bunits_list(&$in){
		$q = strtolower($in["term"]);
		$bunit=$this->db->query("SELECT * FROM project_bunit");
		while($bunit->move_next()){
			$items[$bunit->f('name')]=$bunit->f('id');
		}
		$result = array();

		if($items){
			foreach ($items as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "name" => htmlentities($key)));
					}
				}else{
					array_push($result, array("id"=>$value, "name" => htmlentities($key)));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "name" => 'No matches'));
		}

		return $result;
	}

	public function get_cc($in)
	{
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		if($in['internal']=='true'){
			$filter=" is_admin>0 ";
		}
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		if($in['buyer_id']){
			$filter .=" AND customers.customer_id='".$in['buyer_id']."' ";
		}
		// UNION 
		// 	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
		// 	FROM customer_contacts
		// 	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
		// 	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
		// 	WHERE $filter_contact
		if($in['internal'] == 'true'){
			$cust = $this->db->query("SELECT customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name,our_reference,currency_id,internal_language
			FROM customers
			WHERE $filter			
			ORDER BY name
			LIMIT 50")->getAll();
		}else{
			$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 50")->getAll();
		}
		
		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
			$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				/*"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_contacts($in) {
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				/*'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), */
				'right' 				=> '', 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				/*'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),*/
				'bottom' 				=> '',
			);

		}

		$added = false;
		if(count($result)==9){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
					customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				//'right'					=> $value['country_name'],
				'right'					=> '',
				'title'					=> $value['position_n'],
				//'bottom'				=> $value['position_n'],
				'bottom'				=> '',
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in)
	{
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 9");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 9");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		"symbol"				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==9){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	/*'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
				  	'right'				=> '',
				  	'bottom'			=> '',
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

	private function getCompanyInfo(){
		$array = array(			
				'name'		=> ACCOUNT_COMPANY,
				'address'		=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
				'zip'			=> ACCOUNT_DELIVERY_ZIP,
				'city'		=> ACCOUNT_DELIVERY_CITY,
				'country'		=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
				'phone'		=> ACCOUNT_PHONE,
				'fax'			=> ACCOUNT_FAX,
				'email'		=> ACCOUNT_EMAIL,
				'url'			=> ACCOUNT_URL,
				'logo'		=> '../'.ACCOUNT_LOGO_QUOTE,			
		);		
		return $array;
	}

	public function get_tasks(&$in){

		$edit_access=false;
		if($_SESSION['group'] == 'admin'){
			$edit_access		= true;
		}else{
			$perm_proj_module=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$perm_array=explode(";",$perm_proj_module);
			if(in_array('3',$perm_array)){
				$perm_mod_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_3' AND value='project' ");
				if($perm_mod_admin){
					$edit_access		= true;
				}else{
					$perm_proj_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' AND value='1' ");
					if($perm_proj_admin){
						$edit_access		= true;
					}else{
						$perm_proj_man=$this->db->field("SELECT manager FROM project_user WHERE user_id='".$_SESSION['u_id']."' AND project_id='".$in['project_id']."' AND manager='1' ");
						if($perm_proj_man){
							$edit_access		= true;
						}
					}
				}
			}
		}

		$output=array('tasks'=>array());
		$i=0;
		$project_data=$this->db->query("SELECT status_rate,stage,billable_type,budget_type,invoice_method FROM projects WHERE project_id='".$in['project_id']."' ");

		$task = $this->db->query("SELECT tasks.*,projects.billable_type,projects.budget_type, projects.currency_type,projects.invoice_method
           			 FROM tasks
            		INNER JOIN projects ON projects.project_id=tasks.project_id
            		WHERE tasks.project_id='".$in['project_id']."' ORDER BY sort_order ASC, task_id ASC");
		$is_data = false;
		$total_invoice = 0;
		$currency = get_commission_type_list($task->f('currency_type'));

		if($in['billable']){
			$in['billable'] = unserialize($in['billable']);
		}
		$hide_billable=0;
		$task_closed = 0;
		$task_bill = 0;
		$t_hours=0;
		$t_h_rate=0;
		$daily_h_rate=0;
		$task_width=$project_data->f('billable_type') ==6 ? 6 : 2;
		while ($task->move_next()){
			$hide_billable=$task->f('invoice_method');
			$g = '';
			if($task->f('task_group_id')){
				$g = $this->db->field("SELECT name FROM task_groups WHERE task_group_id='".$task->f('task_group_id')."' ");
			}
			$task_line=array(
				'task_name'        		=> $task->f('task_name'),
				'tasks_group'			=> $g,
				'REL'              		=> $task->f('task_id'),
				'BILLABLE_CHECKED' 		=> ($in['billable'] && in_array($task->f('task_id'),$in['billable']) || $task->f('billable')) ? true : false,
				't_h_rate'         		=> display_number($task->f('t_h_rate')),
				'daily_h_rate'			=> display_number($task->f('t_daily_rate')),
				't_hours'          		=> display_number($task->f('t_hours')),
				'task_budget'      		=> display_number($task->f('task_budget')),
				'task_budget_c'			=> $task->f('closed') ? true : false,		
				'DISABLED'				=> $task->f('closed') == 2 ? true : false,		
				'HIDE'				=> $task->f('closed') == 2 ? true : false,								
				'currency'				=> $currency,
				'start_date'			=> $task->f('start_date') ? $task->f('start_date')*1000 : '',
				'end_date'				=> $task->f('end_date') ? $task->f('end_date')*1000 : '',
				'sort_order'				=> $task->f('sort_order'),
			);
			$t_hours+=$task->f('t_hours');
			$t_h_rate+=$task->f('t_h_rate');
			$daily_h_rate+=$task->f('t_daily_rate');
			$total_invoice += $task->f('task_budget');
			array_push($output['tasks'], $task_line);
			$i++;
			if($task->f('closed')){
				$task_closed++;
			}
			if($in['billable'] && in_array($task->f('task_id'),$in['billable']) || $task->f('billable')){
				$task_bill++;
			}
			$is_data = true;
		}

		if($project_data->f('billable_type') !=5 && $project_data->f('billable_type') !=6){
			if($project_data->f('billable_type')!=1 && $project_data->f('billable_type')!=7){
				$task_width=$task_width+2;	
			}
			if(!$project_data->f('invoice_method') || $project_data->f('billable_type')==6){
				$task_width++;
			}
		}	
		if($project_data->f('budget_type')!=4){
			$task_width++;
		}
		if($project_data->f('stage')==2 || !$edit_access){
			$task_width++;
		}
	
		$output['is_data']			= $is_data;
		$output['total_invoice']		= place_currency(display_number($total_invoice));
		$output['page_tip']			= $tips;
		$output['TASKS_CLOSED_CHECKED']	= $i == $task_closed ? true : false;
		$output['BILLABLE_CHECKEDs']		= $i == $task_bill ? true : false;
		$output['task_rate']			= $project_data->f('status_rate') > 0 ? gm('Budget').' ('.gm('Days').')' : gm('Budget').' ('.gm('Hours').')';		
		$output['total_task_hours']		= display_number($t_hours);
		$output['total_task_budget']		= $project_data->f('status_rate')>0 ? display_number($daily_h_rate) : display_number($t_h_rate);
		$output['t_width']			= $task_width;
	return $output;
	}

	public function get_tasks_list(&$in){
		$q = strtolower($in["term"]);
		$filter = " is_service='2' AND active=1 ";
		if($q != ''){
			$filter .=" AND internal_name LIKE '%".addslashes($q)."%'";
		}

		$tasks=$this->db->query("SELECT internal_name,article_id FROM pim_articles WHERE ".$filter." ORDER BY internal_name ASC");
		while($tasks->move_next()){
		  	$accounts[trim($tasks->f('internal_name'))]=$tasks->f('article_id');
		}
		$result = array();
		if($accounts){
			foreach ($accounts as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "name" => strip_tags($key)));
					}
				}else{
					array_push($result, array("id"=>$value, "name" => strip_tags($key)));
				}
				if (count($result) > 50)
				break;
			}			
		}
		return $result;
	}

	public function get_users(&$in){

		$edit_access=false;
		$above_PM=false;
		if($_SESSION['group'] == 'admin'){
			$edit_access	= true;
			$above_PM		= true;
		}else{
			$perm_proj_module=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$perm_array=explode(";",$perm_proj_module);
			if(in_array('3',$perm_array)){
				$perm_mod_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_3' AND value='project' ");
				if($perm_mod_admin){
					$edit_access	= true;
					$above_PM		= true;
				}else{
					$perm_proj_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' AND value='1' ");
					if($perm_proj_admin){
						$edit_access	= true;
						$above_PM		= true;
					}else{
						$perm_proj_man=$this->db->field("SELECT manager FROM project_user WHERE user_id='".$_SESSION['u_id']."' AND project_id='".$in['project_id']."' AND manager='1' ");
						if($perm_proj_man){
							$edit_access	= true;
						}
					}
				}
			}
		}

		$output=array('users'=>array());
		
		$i=0;
		$users=$this->db->query("SELECT project_user.*,projects.billable_type,projects.budget_type 
            	FROM project_user
            	INNER JOIN projects ON projects.project_id=project_user.project_id
            	WHERE project_user.project_id='".$in['project_id']."' AND project_user.active='1' ");

		if($in['manager'] && is_string($in['manager'])){
			$in['manager'] = unserialize($in['manager']);
		}
		$i=0;
		$p_hours=0;
		$p_h_rate=0;
		$daily_p_rate=0;
		$t_width=4;
		
		while ($users->move_next()){
			$not_deleted=$this->db_users->field("SELECT users.user_id FROM users WHERE user_id ='".$users->f('user_id')."' ");
			$user_line=array(
				'user_name'            	=> $not_deleted? get_user_name($users->f('user_id')) : htmlspecialchars($users->f('user_name')),
				//'user_name'            	=> get_user_name($users->f('user_id')),
				'REL'              	=> $users->f('project_user_id'),
				'MANAGER_CHECKED'  	=> ($in['manager'] && in_array($users->f('project_user_id'),$in['manager']) || $users->f('manager')) ? true :false,
				'p_h_rate'         	=> display_number($users->f('p_h_rate')),
				'p_hours'          	=> display_number($users->f('p_hours')),			  	
				'p_daily_rate'		=> display_number($users->f('p_daily_rate'))
				);	
			
			$p_hours+=$users->f('p_hours');
			$p_h_rate+=$users->f('p_h_rate');
			$daily_p_rate+=$users->f('p_daily_rate');
			array_push($output['users'], $user_line);
			$i++;
		}

		$project_data=$this->db->query("SELECT billable_type,budget_type,status_rate,stage FROM projects WHERE project_id='".$in['project_id']."' ");

		if($project_data->f('budget_type')!=5){
			$t_width=$t_width+2;		
		}
		if($project_data->f('billable_type')!=5 && $project_data->f('billable_type') !=2){
			$t_width=$t_width+2;
		}
		if($project_data->f('stage')==2 || !$above_PM){
			$t_width++;
		}
		if($project_data->f('stage')==2 || !$edit_access){
			$t_width++;
		}	

		$is_data = true;
		if($i == 0){
			$is_data = false;	
		}
		
		$output['PROJECT_ID'] 			= $in['project_id'];
		$output['is_data']			= $is_data;		
		$output['user_rate']			= $project_data->f('status_rate') > 0 ? gm('Budget').' ('.gm('Days').')' : gm('Budget').' ('.gm('Hours').')';		
		$output['total_user_hours']		= display_number($p_hours);
		$output['total_user_budget']		= $project_data->f('status_rate')>0 ? display_number($daily_p_rate) : display_number($p_h_rate);
		$output['t_width']			= $t_width;

		return $output;
	}

	public function get_users_list(&$in){

		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
			$filter .=" AND CONCAT_WS(' ',first_name, last_name) LIKE '%".$q."%'";
		}

		$data=$this->db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");
		while($data->move_next()){
		  $users[htmlspecialchars_decode(stripslashes($data->f('first_name').' '.$data->f('last_name')))]=$data->f('user_id');
		}

		$result = array();
		if($users){
			foreach ($users as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "name" => strip_tags($key)));
					}
				}else{
					array_push($result, array("id"=>$value, "name" => strip_tags($key)));
				}
				if (count($result) > 2000)
				break;
			}
		}
		return $result;
	}

	public function get_articles(&$in){
		$edit_access=false;
		$above_PM=false;
		if($_SESSION['group'] == 'admin'){
			$edit_access	= true;
			$above_PM		= true;
		}else{
			$perm_proj_module=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$perm_array=explode(";",$perm_proj_module);
			if(in_array('3',$perm_array)){
				$perm_mod_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_3' AND value='project' ");
				if($perm_mod_admin){
					$edit_access	= true;
					$above_PM		= true;
				}else{
					$perm_proj_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' AND value='1' ");
					if($perm_proj_admin){
						$edit_access	= true;
						$above_PM		= true;
					}else{
						$perm_proj_man=$this->db->field("SELECT manager FROM project_user WHERE user_id='".$_SESSION['u_id']."' AND project_id='".$in['project_id']."' AND manager='1' ");
						if($perm_proj_man){
							$edit_access	= true;
						}
					}
				}
			}
		}
		$output=array('articles'=>array());

		$project_data=$this->db->query("SELECT status_rate,stage FROM projects WHERE project_id='".$in['project_id']."' ");

		$article=0;
		$t_width=3;
		$articles = $this->db->query("SELECT * FROM project_articles WHERE project_id='".$in['project_id']."' AND article_id!='0' ORDER BY name ASC ");
		while($articles->next()){
			$art_data=$this->db->query("SELECT hide_stock,item_code,stock FROM pim_articles WHERE article_id='".$articles->f('article_id')."' ");
			if($art_data->f('stock') <= 0){
		     	   $stock_status='text-danger';          
		      }else if($art_data->f('stock') > 0  && $art_data->f('stock') <= $art_data->f('article_threshold_value') ){
		     	   $stock_status='text-warning';
		      }else{
		     	   $stock_status='text-success';
		      }
		      $billed=$this->db->field("SELECT SUM(invoiced) FROM service_delivery WHERE project_id='".$in['project_id']."' AND a_id='".$articles->f('article_id')."' ");
		      $q_delivered=$this->db->field("SELECT SUM(quantity) FROM service_delivery WHERE project_id='".$in['project_id']."' AND a_id='".$articles->f('article_id')."' ");
			$article_line=array(
				'name' 			=> $articles->f('name'),
				'id'				=> $articles->f('a_id'),
				'article_id'		=> $articles->f('article_id'),
				'billed'			=> $billed ? true : false,
				'quantity'			=> display_number($articles->f('quantity')),
				'to_deliver'		=> display_number($articles->f('to_deliver')),
				'delivered'			=> display_number($articles->f('delivered')),
				'to_be_delivered'		=> display_number($articles->f('quantity')-$q_delivered),
				'price'			=> display_number($articles->f('price')),
				'margin'			=> display_number($articles->f('margin')),
				'sell_price'		=> display_number($articles->f('price')*$articles->f('margin')/100+$articles->f('price')),
				'delivered_checked' 	=> $articles->f('delivered')==1 ? true : false,
				'closed_project'		=> $closed_project,
				'PROJECT_INACTIVE'	=> $project_data->f('stage')!=1 ? true : false,
				'hstock'			=> $art_data->f('hide_stock') ? '1' : '0', 
				'code'			=> $art_data->f('item_code'),
				'stock'			=> display_number($art_data->f('stock')),
				'stock_status'		=> $stock_status,
			);
			array_push($output['articles'], $article_line);
			$article++;
		}

		if($project_data->f('stage')==2 || !$above_PM){
			$t_width++;
		}
		if($project_data->f('stage')==2 || !$edit_access){
			$t_width++;
		}
		if(!defined('ALLOW_STOCK') || (defined('ALLOW_STOCK') && ALLOW_STOCK!='1')){
			$t_width++;
		}

		$is_data = true;
		if($article == 0){
			$is_data = false;	
		}

		$output['t_width']			= $t_width;
		$output['is_data']			= $is_data;
		$output['allow_stock']		= defined('ALLOW_STOCK') && ALLOW_STOCK=='1' ? true : false;
		$output['minus_stock']		= $in['minus_stock']==1 ? true : false;
		$output['stock']			= $in['minus_stock']==1 ? $in['stock'] : '';

		return $output;
	}

	public function get_articles_list($in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id ';
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id';

			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount,
						pim_articles.supplier_reference';
		}

		$filter.=" 1=1 ";
		if($in['only_service']){
			$filter.=" AND pim_articles.is_service='1' ";
		}else{
			$filter.=" AND pim_articles.is_service='0' ";
		}
		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id']){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
				if($vat_regime==2){
					$vat=0;
				}
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	'name'						=> htmlspecialchars_decode($article->f('internal_name')),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'ALLOW_STOCK'				=> ALLOW_STOCK==1 ? true : false,
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)), 
				'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
				'supplier_reference'	   => $article->f('supplier_reference'),
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		if(in_array(12,explode(';', $this->db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']])))){
			array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));
		}

		return $articles;
	}

	public function get_purchases(&$in){
		$edit_access=false;
		if($_SESSION['group'] == 'admin'){
			$edit_access	= true;
		}else{
			$perm_proj_module=$this->db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			$perm_array=explode(";",$perm_proj_module);
			if(in_array('3',$perm_array)){
				$perm_mod_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_3' AND value='project' ");
				if($perm_mod_admin){
					$edit_access	= true;
				}else{
					$perm_proj_admin=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='project_admin' AND value='1' ");
					if($perm_proj_admin){
						$edit_access	= true;
					}else{
						$perm_proj_man=$this->db->field("SELECT manager FROM project_user WHERE user_id='".$_SESSION['u_id']."' AND project_id='".$in['project_id']."' AND manager='1' ");
						if($perm_proj_man){
							$edit_access	= true;
						}
					}
				}
			}
		}
		$output=array('purchases'=>array());

		$project_data=$this->db->query("SELECT status_rate,stage FROM projects WHERE project_id='".$in['project_id']."' ");

		$lines = $this->db->query("SELECT project_purchase.*,customers.name
					FROM project_purchase
					LEFT JOIN customers ON project_purchase.supplier_id=customers.customer_id
					WHERE project_purchase.project_id='".$in['project_id']."' ");
		$i = 0;
		$t_width=1;
		while ($lines->next()) {
			$margin = ($lines->f('unit_price')*$lines->f('margin')/100)+$lines->f('unit_price');
			$show_delete_button = true;	
			if($lines->f('purchase_service_id') > 0){
				$status = $this->db->field("SELECT status FROM servicing_support_articles 
					LEFT JOIN servicing_support ON servicing_support.service_id = servicing_support_articles.service_id
					WHERE servicing_support_articles.a_id = '".$lines->f('purchase_service_id')."' ");				
				if($status != '2'){
					$show_delete_button = true;
				}else{
					$show_delete_button = false;
				}
			}
			$p_line=array(
				'SUPPLIER'					=> $lines->f('name'),
				'PURCHASE'					=> $lines->f('description'),
				'COLOR'					=> $lines->f('note') ? 'yellowish' : '',
				'billed'					=> $lines->f('invoiced') ? true : false,
				'billable'				=> $lines->f('billable') ? true : false,
				'DISABLE'					=> $lines->f('invoiced') ? true : false,
				'HIDE_DEL'					=> $lines->f('invoiced') ? false : true,
				'unit_price'				=> display_number($lines->f('unit_price')),
				'sell_price'				=> display_number($margin),
				'PROJECT_ID'				=> $in['project_id'],
				'margin'					=> display_number($lines->f('margin')),
				'PURCHASE_ID'				=> $lines->f('project_purchase_id'),
				'DELETE_LINK'				=> 'index.php?do=project-project_purchases-project-delete_purchase&project_purchase_id='.$lines->f('project_purchase_id').'&project_id='.$in['project_id'],
				'show_delete_button' 			=> $show_delete_button,
				'edit_access'				=> $edit_access,
				'closed_project'				=> $closed_project,
				'delete_row'				=> $delete_row == false ? false : true,
				'purchase_new'				=> $edit_access == '' ? 'purchase_new' : '',
				'timesheet_coment'			=> $edit_access == '' ? 'timesheet_coment' : '',
				'art_edit'					=> $lines->f('invoiced')==1 ? '' : 'art_edit',	
				'delivered'					=> $lines->f('delivered')==1 ? true : false,
			);
			array_push($output['purchases'], $p_line);
			$i++;
		}

		$is_data = true;
		if($i == 0){
			$is_data = false;	
		}

		if($project_data->f('stage')==2 || !$edit_access){
			$t_width++;
		}

		$output['is_data']		= $is_data;
		$output['t_width']		= $t_width;

		return $output;
	}

	public function get_deals(&$in){
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND subject LIKE '".$q."%'";
		}
		$deals=$this->db->query("SELECT * FROM tblopportunity 
			WHERE buyer_id='".$in['buyer_id']."' $filter ORDER BY subject ASC");
		$items = array();
		while($deals->next()){
			array_push( $items, array('deal_id'=>$deals->f('opportunity_id'),'name'=>stripslashes($deals->f('subject'))));
		}
		return $items;
	}

}


	$project = new ProjectEdit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $project->output($project->$fname($in));
	}

	$project->getProject();
	$project->output();

?>