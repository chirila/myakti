<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

	$project_article=$db->query("SELECT * FROM project_articles WHERE a_id='".$in['article_id']."' ");
	$billed=$db->field("SELECT SUM(invoiced) FROM service_delivery WHERE project_id='".$project_article->f('project_id')."' AND a_id='".$project_article->f('article_id')."' ");

	$result=array(
		'article_margin' 			=> display_number($project_article->f('margin_per')),
		'article_purchase_price'	=> display_number($project_article->f('purchase_price')),
		'article_sell_price'		=> display_number($project_article->f('price')),
		'article_name'			=> $project_article->f('name'),
		'REL'					=> $in['article_id'],
		'invoiced'				=> $billed ? true : false,
		'art_edit'				=> $billed ? '' : 'art_edit'
	);

return json_out($result);