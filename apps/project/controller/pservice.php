<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $config;
$o=array();

if(!isset($in['tab'])){
	$in['tab']=0;
}


if(!isset($in['lang_id'])){
	$lang = $db->query("SELECT lang_id,code,default_lang FROM pim_lang WHERE sort_order='1' GROUP BY lang_id");

}else{
	$lang = $db->query("SELECT lang_id,code,default_lang FROM pim_lang WHERE lang_id='".$in['lang_id']."' GROUP BY lang_id");
}

$in['lang_id']= $lang->f('lang_id');
$in['code']   = $lang->f('code');

$o['language_dd']=build_language_ecom_dd($in['lang_id']);
$o['code']=strtoupper($in['code']);


if(!$in['price']){
	$in['price']=display_number_var_dec(0);
	$in['total_price']=display_number_var_dec(0);
}
if(!$in['aac_price']){
	$in['aac_price']=display_number_var_dec(0);
	
}
if(!$in['h_price']){
	$in['h_price']=display_number_var_dec(0);
}
if(!$in['d_price']){
	$in['d_price']=display_number_var_dec(0);
}

if($in['article_id']=='tmp' && !$in['duplicate_article_id'] ){
	$page_title = gm('Add service');

	if(!isset($in['price_type'])){
		$in['price_type']=PRICE_TYPE;
	}
	if(!isset($in['vat_id'])){
		$in['vat_id'] = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");
		$in['vat_value']=ACCOUNT_VAT;
	}
	if(!$in['sale_unit']){
	   $in['sale_unit']=1;
	}
	if(!$in['packing']){
	   $in['packing']=1;
	}
    if(!$in['article_threshold_value']){
    	$in['article_threshold_value']=ARTICLE_THRESHOLD_VALUE;
    }

    $o['do_next']='project-pservice-service-add';
    $o['vat_dd']=build_vat_dd($in['vat_id']);
    $o['vat_value']=$in['vat_value'];
    //$o["price_type_".$in['price_type']."_check"]=build_category_list($in['category_id'],0,1);
    $o['use_percent_default_check']=$in['use_percent_default']?"checked='check'":'';
    $o['article_brand']= build_article_brand_dd($in['article_brand_id']);
    $o['article_ledger_account']=build_article_ledger_account_dd($in['ledger_account_id']); 
    $o['article_category']=build_article_category_dd($in['article_category_id']);
    $o['edit_mode']=0;
    $o['add_mode']=true;
    $o['check_show_on_q']=$in['show_img_q'] ? 'CHECKED' : '';
    $o['check_hide_stock']=$in['hide_stock'] ? 'CHECKED' : '';
    $o['supplier_name']=$in['supplier_name'];
    $o['is_taxes']=false;
    $o['check_billable']=$in['billable'] ? 'CHECKED' : '';
    $o['billable']= $in['billable'] == 1 ? true : false;
    $o['h_price']       		= 	display_number_var_dec(0);
    $o['d_price']       		= 	display_number_var_dec(0);
    

} elseif($in['article_id']!='tmp' && !$in['duplicate_article_id']) { //edit


	
	$o['do_next']='project-pservice-service-update';
	
	$article=$db->query("SELECT pim_articles.*,vats.value AS vat_value,pim_articles_lang.name,pim_articles_lang.name2,pim_articles_lang.description,
	                            pim_article_prices.default_price, pim_article_prices.price, pim_article_prices.total_price, pim_article_prices.purchase_price
				         FROM pim_articles
				         LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
						 LEFT JOIN vats ON pim_articles.vat_id=vats.vat_id
						 LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'
				         WHERE pim_articles.article_id='".$in['article_id']."' ");

	$page_title = gm('Edit Article').' '. $article->gf('internal_name') ;
	
	$in['internal_name']	= 	gfn($article->gf('internal_name'));
    $in['name']        		= 	gfn($article->gf('name'));
	$in['name2']       		= 	gfn($article->gf('name2'));
	$in['item_code']   		= 	gfn($article->gf('item_code'));
	$in['ean_code']    		= 	gfn($article->gf('ean_code'));
	$in['price']       		= 	display_number_var_dec($article->f('price'));
	$in['aac_price']       	= 	display_number_var_dec($article->f('aac_price'));
	$in['purchase_price']   = 	display_number_var_dec($article->f('purchase_price'));
	$in['total_price'] 		= 	display_number_var_dec($article->f('total_price'));
	$in['description'] 		= 	gfn($article->gf('description'));
	$in['stock']	   		= 	remove_zero_decimals($article->gf('stock'));
	$in['category_id'] 		= 	$article->gf('category_id');
	$in['origin_number'] 	= 	$article->gf('origin_number');
	$in['supplier_reference'] 	= 	$article->gf('supplier_reference');
	$in['sale_unit']     	= 	$article->gf('sale_unit')?$article->gf('sale_unit'):'';
	$in['packing']       	= 	$article->gf('packing')? remove_zero_decimals($article->f('packing')):'';	
	$in['price_type']       =   $article->gf('price_type');
	$in['article_threshold_value']       =   $article->f('article_threshold_value');
	$in['max_stock']	   		= 	remove_zero_decimals($article->gf('max_stock'));
	$in['weight']   		= 	$in['weight'] ? $article->gf('weight') : display_number($article->f('weight'));
	//$in['supplier_id']   		= 	$article->f('supplier_id');
	//$in['supplier_name']   		= 	$article->f('supplier_name');

	$in['article_category_id'] = $article->gf('article_category_id');	
	
	
		// $o['category_dd']     = 	build_category_list($db->gf('category_id'),0,1);
		 $o['internal_name']    = 	gfn($article->gf('internal_name'));
		 $o['item_code']        = 	gfn($article->gf('item_code'));
		 $o['h_price']       		= 	display_number_var_dec($article->f('h_price'));
		 $o['d_price']       		= 	display_number_var_dec($article->f('d_price'));
		 $o['check_billable']   = $article->gf('billable') ? 'CHECKED' : '';
		 $o['billable']         = $article->gf('billable') == 1 ? true : false;
		 $o['price']       		= 	display_number_var_dec($article->f('price'));
		 $o['purchase_price']   = 	display_number_var_dec($article->f('purchase_price'));
		 $o['supplier_reference'] 	= 	$article->gf('supplier_reference');
		 $o['article_category'] = $article->gf('article_category_id');
		 $o['article_category_id'] = $article->f('article_category_id');
		 $o['supplier_id']   		= 	(string)$article->f('supplier_id');		 
		 $o['ledger_account_id']   		= 	$article->f('ledger_account_id') ? (string)$article->f('ledger_account_id') : '';
	     //$o['supplier_name']   		= 	(string)$article->f('supplier_name');	
       

		 $o['vat_dd']      = 	build_vat_dd($db->gf('vat_id'));
		 $o['article_brand'] 		= 	build_article_brand_dd($db->f('article_brand_id'));
		 $o['article_ledger_account'] = build_article_ledger_account_dd($db->f('ledger_account_id'));
		 $o['article_category'] 	=	build_article_category_dd($db->f('article_category_id'));
		 $o['vat_value'] 			= 	$db->gf('vat_value');
		 $o['price_type_'.$in['price_type'].'_check'] 		= "checked='check'";
		 $o['use_percent_default_check'] 	=$db->gf('use_percent_default')?"checked='check'":'';
		 $o['edit_mode']     		= 	1;
		 $o['add_mode'] 			=  false;
		 $o['vat_id'] 				=	$db->f('vat_id');
		 $o['active_tabs']  = '{selected:'.$in['tab'].'}';
		 $o['check_show_on_q'] = $article->gf('show_img_q') ? 'CHECKED' : '';
		 $o['check_hide_stock'] 	= $article->gf('hide_stock') ? 'CHECKED' : '';
		 $o['no_batches'] 		= $article->gf('use_batch_no')? false:true;	
		 $o['article_id']			= $in['article_id'];
		
	

      $in['hide_stock']=$article->gf('hide_stock');

    if($article->f('price_type')==1){
	     $o['price_type_1'] 	= true;
	     $o['price_type_2'] 	= false;
	 
	  }else{
	  	 $o['price_type_1'] 	= false;
	     $o['price_type_2'] 	= true;
     
	  }
	$db->query("SELECT pim_article_tax.*,pim_articles_taxes.article_id,pim_articles_taxes.tax_id AS active_tax
            FROM pim_article_tax
            INNER JOIN pim_articles_taxes on pim_articles_taxes.tax_id=pim_article_tax.tax_id AND pim_articles_taxes.article_id='".$in['article_id']."'
           ");

	$j=0;

	while($db->move_next()){
		if($db->f('active_tax')){
		   $in['tax_id'][$db->f('tax_id')] = 1;
		}
		  $o['taxe_code']=$db->f('code');
		  $o['tax_id']=$db->f('tax_id');
		   $o['tax_value']=display_number($db->f('amount'));
		   $o['tax_id']=$in['tax_id'][$db->f('tax_id')] == 1?'checked="checked"' : ''; 

		/*$view->assign(array(
		'taxe_code'	  => $db->f('code'),
		'tax_id'	  => $db->f('tax_id'),
		'tax_value'   => display_number($db->f('amount')),
		'check_tax'   => $in['tax_id'][$db->f('tax_id')] == 1?'checked="checked"' : '',
		),"taxes_row" );*/

		//$view->loop('taxes_row');
		$j++;
	}
	 $o['is_taxes']=$j;



}else{ //duplicate
	$view->assign('do_next','project-pservice-service-duplicate');
	$article=$db->query("SELECT pim_articles.*,vats.value AS vat_value,pim_articles_lang.name,pim_articles_lang.name2,pim_articles_lang.description,
	                            pim_article_prices.default_price, pim_article_prices.price, pim_article_prices.total_price
				         FROM pim_articles
				         LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
						 LEFT JOIN vats ON pim_articles.vat_id=vats.vat_id
						 LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'
				         WHERE pim_articles.article_id='".$in['duplicate_article_id']."'  ");
	$article->next();

	$page_title = gm('Duplicate Article').' '. $article->gf('internal_name') ;

	// $in['item_code']   = $article->gf('item_code');
	$in['name2']       = $article->gf('name2');
	$in['name']       = $article->gf('name');
	$in['weight']   = $article->gf('weight');
	//$in['ean_code']    = $article->gf('ean_code');
	$in['price']       = display_number_var_dec($article->f('price'));
	$in['aac_price']   = display_number_var_dec($article->f('aac_price'));
	$in['total_price'] = display_number_var_dec($article->f('total_price'));
	$in['description'] = $article->gf('description');
	$in['stock']	   = remove_zero_decimals($article->gf('stock'));
	$in['category_id'] = $article->gf('category_id');
//	$in['origin_number'] = $article->gf('origin_number');
	$in['sale_unit']     = $article->gf('sale_unit')?$article->gf('sale_unit'):'';
	$in['packing']       =$article->gf('packing')?$article->gf('packing'):'';
    $in['price_type'] = $article->gf('price_type');
    $in['article_threshold_value']       =   $article->f('article_threshold_value');
    $in['vat_id']       =   $article->f('vat_id');
     $in['article_category_id']       =  $db->gf('article_category_id');

	
	//$o['category_dd']= build_category_list($db->gf('category_id'),0,1);
	$o['vat_dd']= build_vat_dd($db->gf('vat_id'));
	$o['vat_value']= $db->gf('vat_value');
	$o['article_brand']= build_article_brand_dd($db->gf('article_brand_id'));
	$o['article_ledger_account']= build_article_ledger_account_dd($db->gf('ledger_account_id'));
	$o['article_category']= build_article_category_dd($db->gf('article_category_id'));
	$o['price_type_'.$in['price_type'].'_check']= "checked='check'";
	$o['use_percent_default_check']= $db->gf('use_percent_default')?"checked='check'":'';
	$o['add_mode']= true;
	//$o['edit_mode']= build_category_list($db->gf('category_id'),0,1);
	$o['disabled_tabs']= '{disabled:[1]}';
	$o['active_tabs']= '{selected:'.$in['tab'].'}';
	$o['check_show_on_q']= $article->gf('show_img_q') ? 'CHECKED' : '';
	$o['check_hide_stock']= $article->gf('hide_stock') ? 'CHECKED' : '';





	$db->query("SELECT pim_article_tax.*,pim_articles_taxes.article_id,pim_articles_taxes.tax_id AS active_tax
            FROM pim_article_tax
            LEFT JOIN pim_articles_taxes on pim_articles_taxes.tax_id=pim_article_tax.tax_id  AND pim_articles_taxes.article_id='".$in['duplicate_article_id']."'
           ");

	$j=0;
	$in['tax_id']=array();
	while($db->move_next()){
		if($db->f('active_tax')){
		   $in['tax_id'][$db->f('tax_id')] = 1;
		}
		$o['taxe_code']=$db->f('code');
		$o['tax_id']=$db->f('tax_id');
		$o['tax_value']=display_number($db->f('amount'));
		$o['check_tax']=$in['tax_id'][$db->f('tax_id')] == 1?'checked="checked"' : '';

		/*$view->assign(array(
		'taxe_code'	  => $db->f('code'),
		'tax_id'	  => $db->f('tax_id'),
		'tax_value'   => display_number($db->f('amount')),
		'check_tax'   => $in['tax_id'][$db->f('tax_id')] == 1?'checked="checked"' : '',
		),"taxes_row" );*/

		//$view->loop('taxes_row');
		$j++;
	}
	$o['is_taxes']=$j;
	
}



if($in['product_id']){
	$o['is_product']=1;
	$o['product_link']="index.php?do=pim-product&product_id=".$in['product_id'];
	

}else{
	$o['is_product']=0;
	
}
switch ($in['lang_id'])
	{
		case 1:$o['translate_cls']='translate_en';
		case 2:$o['translate_cls']='translate_fr'; 
		case 3:$o['translate_cls']='translate_nl'; 
		case 4:$o['translate_cls']='translate_de'; 
		// case 5:$view->assign('translate_cls','translate_au');break;
	}

$allow_stock = $db->query("SELECT * FROM settings WHERE constant_name='ALLOW_STOCK'");
$allow_stock->next();

$stock_multiple_locations = $db->query("SELECT * FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
$stock_multiple_locations->next();


if ($allow_stock->f('value')==1)
{  
	$o['allow_stock']=true;

}else{
	$o['allow_stock']=false;

}


if ($stock_multiple_locations->f('value')== 1)
{
	$o['stock_multiple_locations']=true;

}else{
	$o['stock_multiple_locations']=false;

}
if($in['hide_stock']){
	$o['show_stock']=false;

}else{
	$o['show_stock']=true;
	
}



$current_index = array_search($in['article_id'], $_SESSION['articles_id']);
$next = $current_index + 1;
$prev = $current_index - 1;


// $o['prev_link']	  		= 'index.php?do=article-article&article_id='. $_SESSION['articles_id'][$prev];
// $o['next_link']  		= 'index.php?do=article-article&article_id='. $_SESSION['articles_id'][$next];
$o['prev']      		= $_SESSION['articles_id'][$prev];
$o['next']      		= $_SESSION['articles_id'][$next];
$o['allow_article_packing']         = ALLOW_ARTICLE_PACKING;
$o['allow_article_sale_unit']       = ALLOW_ARTICLE_SALE_UNIT;
$o['page_title']	                = $page_title;
$o['account_number_format']         = ACCOUNT_NUMBER_FORMAT;
$o['nr_decimals']					= ARTICLE_PRICE_COMMA_DIGITS;



if(WAC && $in['article_id']  ){
	$o['is_wac']= true;
 }else{
	$o['is_wac']= false;
}
if(AAC){
	$o['is_aac']= true;

}else{
		$o['is_aac']= false;
}
if(AUTO_AAC){
	$o['is_auto_aac']= true;

}else{
	$o['is_auto_aac']= false;
	
}



json_out($o);