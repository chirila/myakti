<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
session_start();

$db =  new sqldb();

$db2=new sqldb();
$db3 = new sqldb();

$view_html = new at(ark::$viewpath.'report_all_body.html');

		if($data2){
			$alone_expenses = array_diff_key($expenses_array, $data2);
		}else{
			$alone_expenses = $expenses_array;
		}
		foreach ($alone_expenses as $cust_id => $value) {
			if(!in_array($cust_id, $customers_involved)){
				$customers_involved[] = $cust_id;
			}
		}

		foreach ($alone_expenses as $key => $value) {
			$project_man='';
			$man_array=array();
			$alone_expense_total = 0;
			foreach ($expenses_array[$key] as $key2 => $expenses) {
				if(!array_key_exists($expenses['project_id'],$man_array)){
					$man_array[$expenses['project_id']]=array();
				}
				$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$expenses['project_id']."' AND manager='1' ");
				while($proj_name->next()){
					if(!in_array($proj_name->f('user_id'),$man_array[$expenses['project_id']])){
						$project_man.=get_user_name($proj_name->f('user_id')).',';
						$man_array[$expenses['project_id']][]=$proj_name->f('user_id');
					}
				}
				$is_alone_expense = true;
				$alone_expense_total += $expenses['expense_amount'];
				$view_html->assign(array(
					'alone_expense_description' 	=> $expenses['project_name'].': '.( $expenses['expense_name'] ? $expenses['expense_name'] : gm('Ad Hoc Expense') ),
					'alone_expense_amount' 		=> place_currency(display_number($expenses['expense_amount'])),
					'customer' 				=> $expenses['company_name'],
				),'alone_expenses');
				$view_html->loop('alone_expenses','alone_customers');
				$alone_customer_name = $expenses['company_name'];
			}
			$view_html->assign(array(
				'alone_customer_name' 		=> $alone_customer_name,
				'alone_expense_total' 	=> place_currency(display_number($alone_expense_total)),
				'man_name'				=> rtrim($project_man,","),
			),'alone_customers');
			$view_html->loop('alone_customers');
		}

		//purchases from projects that does not have billable tasks
		if($data2){
			$alone_purchases = array_diff_key($purchases_array, $data2);
		}else{
			$alone_purchases = $purchases_array;
		}
		foreach ($alone_purchases as $cust_id => $value) {
			if(!in_array($cust_id, $customers_involved)){
				$customers_involved[] = $cust_id;
			}
		}
		
		foreach ($alone_purchases as $key => $value) {
			$project_man='';
			$man_array=array();
			$alone_purchase_total = 0;
			foreach ($purchases_array[$key] as $key2 => $purchases) {
				if(!array_key_exists($purchases['project_id'],$man_array)){
					$man_array[$purchases['project_id']]=array();
				}
				$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$purchases['project_id']."' AND manager='1' ");
				while($proj_name->next()){
					if(!in_array($proj_name->f('user_id'),$man_array[$purchases['project_id']])){
						$project_man.=get_user_name($proj_name->f('user_id')).',';
						$man_array[$purchases['project_id']][]=$proj_name->f('user_id');
					}
				}
				$is_alone_purchase = true;
				$alone_purchase_total += $purchases['purchase_amount'];
				$view_html->assign(array(
					'alone_purchase_description' 		=> $purchases['project_name'].': '.$purchases['purchase_description'],
					'alone_purchase_amount' 		=> place_currency(display_number($purchases['purchase_amount'])),
					'customer' 					=> $purchases['company_name'],
				),'alone_purchases');
				$view_html->loop('alone_purchases','alone_customers2');
				$alone_customer_name = $purchases['company_name'];
			}
			$view_html->assign(array(
				'alone_customer_name' 		=> $alone_customer_name,
				'alone_purchase_total' 		=> place_currency(display_number($alone_purchase_total)),
				'man_name'					=> rtrim($project_man,","),
			),'alone_customers2');
			$view_html->loop('alone_customers2');
		}

		if($data2){
			$alone_articles = array_diff_key($articles_array, $data2);
		}else{
			$alone_articles = $articles_array;
		}
		foreach ($alone_articles as $cust_id => $value) {
			if(!in_array($cust_id, $customers_involved)){
				$customers_involved[] = $cust_id;
			}
		}
		
		foreach ($alone_articles as $key => $value) {
			$project_man='';
			$man_array=array();
			$alone_article_total = 0;
			foreach ($articles_array[$key] as $key3 => $articles) {
				if(!array_key_exists($articles['project_id'],$man_array)){
					$man_array[$articles['project_id']]=array();
				}
				$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$articles['project_id']."' AND manager='1' ");
				while($proj_name->next()){
					if(!in_array($proj_name->f('user_id'),$man_array[$articles['project_id']])){
						$project_man.=get_user_name($proj_name->f('user_id')).',';
						$man_array[$articles['project_id']][]=$proj_name->f('user_id');
					}
				}
				$is_alone_article = true;
				$alone_article_total += $articles['amount'];
				$view_html->assign(array(
					'alone_article_description' 		=> $articles['project_name'].': '.$articles['name'],
					'alone_article_quantity' 		=> display_number($articles['quantity']),
					'alone_article_price' 			=> place_currency(display_number($articles['price'])),
					'alone_article_amount' 			=> place_currency(display_number($articles['amount'])),
					'customer' 					=> $articles['company_name'],
				),'alone_articles');
				$view_html->loop('alone_articles','alone_customers3');
				$alone_customer_name = $articles['company_name'];
			}
			$view_html->assign(array(
				'alone_customer_name' 		=> $alone_customer_name,
				'alone_article_total' 		=> place_currency(display_number($alone_article_total)),
				'man_name'				=> rtrim($project_man,","),
			),'alone_customers3');
			$view_html->loop('alone_customers3');
		}

		$view_html->assign(array(
			'is_alone_expense' 	=> $is_alone_expense,
			'is_alone_purchase' 	=> $is_alone_purchase,
			'is_alone_article' 	=> $is_alone_article,
		));

return $view_html->fetch();
?>