<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $database_config;
$result=array('query'=>array());
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

	$archive_access=false;
	if($_SESSION['group'] == 'admin'){
		$archive_access=true;
	}else{
		//$perm_proj_module=$db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$perm_proj_module=$db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$perm_array=explode(";",$perm_proj_module);
		if(in_array('3',$perm_array)){
			//$perm_mod_admin=$db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_3' AND value='project' ");
			$perm_mod_admin=$db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name AND value= :value ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_3','value'=>'project']);
			if($perm_mod_admin){
				$archive_access=true;
			}
		}
	}
	$result['archive_access_top'] = $archive_access == false ? false : true;

$l_r = ROW_PER_PAGE;
$order_by_array=array('serial_number','start_date','end_date','name','company_name');

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

if ((!$in['view']) || (!is_numeric($in['view']))){
    $in['view'] = 1;
}

$order_by =" ORDER BY serial_number DESC ";
$filter = ' 1=1 ';
if($in['customer_id']){
	$filter .=" AND customer_id='".$in['customer_id']."' ";
}
//$order_by = " ORDER BY projects.start_date ASC ";
if(!$in['archived'] || $in['archived']=='0'){
	$filter.= " AND projects.active=1 ";
}else{
	$filter.= " AND projects.active=0 ";
}
$views .= " ";
if($in['view']=='2'){
	$views = " AND (projects.closed='1' OR projects.stage='2') ";
	$order_by =" ORDER BY serial_number DESC ";
}else if ($in['view'] == 1){
	$views .= " AND projects.closed='0' ";
	$filter.=" AND projects.stage<2 ";
} else if ($in['view'] == '') {
    $views .= "";
    $filter.= "";
}
$filter = $filter.$views;
//FILTER LIST

if(!empty($in['search'])){
	$filter .= " AND (projects.name LIKE '%".$in['search']."%' OR company_name LIKE '%".$in['search']."%' OR projects.serial_number LIKE '%".$in['search']."%' )";
}

if(!empty($in['order_by'])){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == '1' || $in['desc']=='true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	}
}

if($_SESSION['access_level'] !='1'){
	$filter .=" AND project_user.user_id='".$_SESSION['u_id']."' AND project_user.active='1' ";
}

$arguments = $arguments.$arguments_s;

$result['nav'] =  $db->query("SELECT projects.project_id FROM projects
						LEFT JOIN project_user ON projects.project_id=project_user.project_id
						WHERE ".$filter." GROUP BY project_id ".$order_by )->getAll();;
$result['max_rows']=count($result['nav']);
$prefix_lenght=strlen(ACCOUNT_PROJECT_START)+1;

$projects=$db->query("SELECT name, projects.t_pr_fees, projects.t_pr_hour, projects.billable_type, projects.budget_type, projects.project_id, projects.stage, company_name, start_date, end_date, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii, serial_number,projects.active FROM projects			
			LEFT JOIN project_user ON projects.project_id=project_user.project_id
			WHERE ".$filter." GROUP BY project_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r );

while($projects->move_next() && $i<$l_r){
	$ref = "";
	if(ACCOUNT_PROJECT_REF && $db->f('our_reference')){
		$ref = $projects->f('our_reference');
	}

	//progress bar
	$budget_t = 0;
	$rate = 0;
	$currency_show = false;

	switch ($projects->f('budget_type')) {
		case '2':
			$budget = $projects->f('t_pr_hour');
			$budget_t = $budget;
		case '3':
			$budget = $projects->f('t_pr_fees');
			$budget_t = $budget;
			if( $projects->f('billable_type') == '1'){
				$rate_value = $db->query("SELECT t_h_rate, task_id FROM tasks WHERE project_id='".$projects->f('project_id')."' AND billable='1' AND task_id IN (SELECT DISTINCT task_id FROM task_time WHERE project_id='".$projects->f('project_id')."' AND hours !=0 )");
				while ($rate_value->next()) {
					$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE task_id='".$rate_value->f('task_id')."' ");
					$rate += ($rate_value->f('t_h_rate') * $task_total_h) ;
				}
			}

			if($projects->f('billable_type') == '2'){
				$rate_value = $db->query("SELECT p_h_rate, user_id FROM project_user WHERE project_id='".$projects->f('project_id')."' ");
				while($rate_value->next()){
					$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$projects->f('project_id')."' AND user_id='".$rate_value->f('user_id')."' ");
					$rate += ( $rate_value->f('p_h_rate') * $task_total_h );
				}
			}

			if( $projects->f('billable_type') == '3'){
				$task_total_h = $db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$projects->f('project_id')."' ");
				$rate = ( $projects->f('pr_h_rate') * $task_total_h );
			}
			if( $projects->f('billable_type') == '4') {
				$rate = 0;
			}

			$currency_show = true;
		break;		
		case '4':
			$budget = $db->query("SELECT t_hours FROM tasks WHERE project_id='".$projects->f('project_id')."' ");
			while ($budget->next()) {
				$budget_t += $budget->f('t_hours');
			}
		break;
		case '5':
			$budget = $db->query("SELECT p_hours FROM project_user WHERE project_id='".$projects->f('project_id')."' ");
			while ($budget->next()) {
				$budget_t += $budget->f('p_hours');
			}
		break;

	}
	
	$show_progress_bar = true;
	if($budget_t == 0){
		// continue;
		$show_progress_bar = false;
	}	

	$spent = $db->field("SELECT SUM(hours) FROM task_time WHERE project_id='".$projects->f('project_id')."' ");
	$spent = $currency_show == true ? $rate : $spent;

	$purchases = $db->field("SELECT SUM(amount) FROM project_purchase WHERE project_id='".$projects->f('project_id')."' ");
	if($currency_show == true){$spent += $purchases;}
	$left = $budget_t - $spent;
	if($left == 0 || $budget_t == 0){
		$procent_left = 0;
	}else{
		$procent_left = ($left/$budget_t)*100;
	}
	switch ($projects->f('stage')) {
		case '1':
			$status = gm('Active');
			$img = 'green';
			break;
		case '2':
			$status = gm('Closed');
			break;
		default:
			$status = gm('Draft');
			$img = 'gray';
			break;
	}

	$query=array(
		'NAME'			=> $projects->f('name'),
		'EDIT_LINK'			=> 'index.php?do=project-project&project_id='.$db->f('project_id'),
	  	'DELETE_LINK'		=> array('do'=>'project-projects-project-delete','customer_id'=>$in['customer_id'],'project_id'=>$projects->f('project_id')),
	  	'ARCHIVE_LINK'		=> array('do'=>'project-projects-project-archive','customer_id'=>$in['customer_id'],'project_id'=>$projects->f('project_id')),
	  	'ACTIVATE_LINK'		=> array('do'=>'project-projects-project-activate','customer_id'=>$in['customer_id'],'project_id'=>$projects->f('project_id')), 
	  	'CUSTOMER'			=> $projects->f('company_name'),
	  	'PROJECT_NUMBER'		=> $ref.$projects->f('serial_number'),
	  	'PROJECT_DATE'		=> $projects->f('start_date') ? date(ACCOUNT_DATE_FORMAT,$projects->f('start_date')) : '&nbsp;',
	  	'END_DATE'			=> $projects->f('end_date') ? date(ACCOUNT_DATE_FORMAT,$projects->f('end_date')) : '&nbsp;',

	  	'BUDGET'			=> $currency_show == true ? place_currency(display_number($budget_t)) : gm('hours').' '.number_as_hour($budget_t),
	  	'SPENT'			=> $currency_show == true ? place_currency(display_number($spent)) : gm('hours').' '.number_as_hour($spent),
	  	'BUDGET_LEFT'		=> $currency_show == true ? place_currency(display_number($left)) : gm('hours ').' '.number_as_hour($left) ,
	  	'PROCENT'			=> display_number($procent_left),
	  	'over_spent'		=> $spent>$budget_t ? true : false,
	  	'width'				=>  ($budget_t==0 || $spent==0)? 0 : ($budget_t >= $spent ? ceil($spent*100/$budget_t) : ceil($budget_t*100/$spent) ),
	  	'width_red'			=>  ($spent!=0 && $spent>$budget_t) ? 100-ceil($budget_t*100/$spent) : 0,
	  	'show_progress_bar'	=> $show_progress_bar,
	  	'project_id'		=> $projects->f('project_id'),
	  	'status_customer'		=> $img,
	  	'status_title'		=> $status,
	  	'stage'			=> $projects->f('stage'),
	  	'closed_project'		=> $in['view'] ? false :true,
	  	'archive_access'		=> $archive_access == false ? false : true,
	  	'project_type'		=> $projects->f('billable_type')<5 ? 'time_p' : 'fixed_p',
	  	'project_title'		=> $projects->f('billable_type')<5 ? gm('Time & Expenses') : gm('Fixed price'),
	  	'confirm'			=> gm('Confirm'),
		'ok'				=> gm('Ok'),
		'cancel'			=> gm('Cancel'),
	);
	array_push($result['query'], $query);
	$i++;
}

	$result['lr'] = $l_r;
	$result['STYLE']=ACCOUNT_NUMBER_FORMAT;

return json_out($result);