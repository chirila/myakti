<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$special_template = array(4,5); # for the specials templates
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

//$filter = " task_time.billable='1' AND task_time.billed='0' ";
$filter = " task_time.billable='1' AND task_time.billed='0' AND service = '0' AND billable_type !=  '4' AND billable_type !='0' AND projects.active>'0' AND projects.stage>'0' ";
$approved_filter = " task_time.billable='1' AND task_time.billed='0' AND task_time.approved='1' AND service = '0' AND projects.active>'0' AND projects.stage>'0' ";
$filter_expenses .= " AND is_service='0' AND projects.active>'0' AND projects.stage>'0' AND project_expenses.billed !=  '1' ";
$filter_purchases .= " AND project_purchase.billable = '1' AND projects.active>'0' AND projects.stage>'0' ";
$filter_articles .= " AND projects.active>'0' AND projects.stage>'0' ";
$filter_task=" ";

if($in['from'] && $in['to']){
	$filter .= " AND date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
	$approved_filter .= " AND date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
	$filter_expenses .= " AND project_expenses.date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
}

if($in['customer_id']){
	$filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';
	$approved_filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';
	$filter_expenses .= " AND projects.customer_id =".$in["customer_id"]." ";
	$filter_purchases .= " AND projects.customer_id =".$in["customer_id"]." ";
	$filter_articles .= " AND projects.customer_id =".$in["customer_id"]." ";
	$filter_task.=' AND projects.customer_id ='.$in['customer_id'].' ';
}

$ALLOW_ARTICLE_PACKING = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
$ALLOW_ARTICLE_SALE_UNIT = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

$total_expenses_services = 0;
$expenses_services_data = $db->query("SELECT project_expenses.*,
					projects.customer_id, projects.name, projects.company_name,
					expense.unit_price, expense.name AS expense_name,
					project_expences.expense_id AS expense_id_2,
					task_time.approved,task_time.billable AS task_billable,task_time.billed AS task_billed

					FROM project_expenses
					LEFT JOIN projects ON project_expenses.project_id = projects.project_id
					LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                              LEFT JOIN expense ON project_expenses.expense_id = expense.expense_id
                              LEFT JOIN task_time ON project_expenses.project_id = task_time.project_id AND project_expenses.date = task_time.date AND project_expenses.user_id=task_time.user_id
					WHERE 1 =1
					AND projects.billable_type !=  '4'
					AND projects.invoice_method>0
					$filter_expenses ");
while($expenses_services_data->next()){
	if( ( $expenses_services_data->f('expense_id_2') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 ) ){
		if(!$expenses_services_data->f('unit_price')){
			$unit_price = 1;
		}else{
			$unit_price = $expenses_services_data->f('unit_price');
		}

		$total_expenses_services+=$unit_price*$expenses_services_data->f('amount');
		$expense_amount = $unit_price*$expenses_services_data->f('amount');

		if( ( $expenses_services_data->f('expense_id_2') && $expenses_services_data->f('approved') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 && $expenses_services_data->f('approved') ) ){
			$total_expenses_services_approved+=$unit_price*$expenses_services_data->f('amount');
			// echo $expenses_services_data->f('id');
	  //  		echo "<br>";
		}
		if($in['show_approved']){
				if( ( $expenses_services_data->f('expense_id_2') && $expenses_services_data->f('approved') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 && $expenses_services_data->f('approved') ) ){
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_name'] = $expenses_services_data->f('expense_name');
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_amount'] = $expense_amount;
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['company_name'] = $expenses_services_data->f('company_name');
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_name'] = $expenses_services_data->f('name');
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_id'] = $expenses_services_data->f('project_id');
				if(!in_array($expenses_services_data->f('customer_id'), $customers_involved)){
					$customers_involved[] = $expenses_services_data->f('customer_id');
				}
			}
		}else{
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_name'] = $expenses_services_data->f('expense_name');
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_amount'] = $expense_amount;
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['company_name'] = $expenses_services_data->f('company_name');
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_name'] = $expenses_services_data->f('name');
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_id'] = $expenses_services_data->f('project_id');
			if(!in_array($expenses_services_data->f('customer_id'), $customers_involved)){
				$customers_involved[] = $expenses_services_data->f('customer_id');
			}
		}

	}
}
//get total purchases
$total_purchases = 0;
$total_purchases_data = $db->query("SELECT project_purchase.project_purchase_id, project_purchase.description, project_purchase.quantity, project_purchase.unit_price, project_purchase.margin, project_purchase.delivered, projects.project_id, projects.name,projects.company_name,projects.customer_id
						FROM project_purchase
						LEFT JOIN projects ON project_purchase.project_id = projects.project_id
						WHERE 1 =1
						AND projects.billable_type !=  '4'
						AND projects.invoice_method>0
						AND billable =  '1'
						AND invoiced =  '0'
						$filter_purchases	");
while($total_purchases_data->next()){
	$purchase_quantity = $total_purchases_data->f('quantity');
	if(!$total_purchases_data->f('quantity')){
			$purchase_quantity = 1;
	}
	$total_purchases+=($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));

	if($total_purchases_data->f('delivered')){
		$total_purchases_approved+=($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
	}
	if($in['show_approved']){
		if($total_purchases_data->f('delivered')){
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_description'] = $total_purchases_data->f('description');
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['company_name'] = $total_purchases_data->f('company_name');
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_amount'] = ($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_name'] = $total_purchases_data->f('name');
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_id'] = $total_purchases_data->f('project_id');
			if(!in_array($total_purchases_data->f('customer_id'), $customers_involved)){
				$customers_involved[] = $total_purchases_data->f('customer_id');
			}
		}
	}else{
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_description'] = $total_purchases_data->f('description');
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['company_name'] = $total_purchases_data->f('company_name');
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_amount'] = ($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_name'] = $total_purchases_data->f('name');
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_id'] = $total_purchases_data->f('project_id');
		if(!in_array($total_purchases_data->f('customer_id'), $customers_involved)){
			$customers_involved[] = $total_purchases_data->f('customer_id');
		}
	}

}
$total_articles=0;
$total_articles_approved=0;
$articles_data=$db->query("SELECT project_articles.*,projects.invoice_method,projects.customer_id,projects.name as project_name,projects.project_id,projects.company_name,pim_articles.packing,pim_articles.sale_unit FROM project_articles
				LEFT JOIN projects ON project_articles.project_id=projects.project_id
				LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
				WHERE project_articles.billed='0'
				AND projects.invoice_method>0
				 $filter_articles ");
	while($articles_data->next()){
		$packing_art=($ALLOW_ARTICLE_PACKING && $articles_data->f('packing')>0) ? $articles_data->f('packing') : 1;
		$sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $articles_data->f('sale_unit')>0) ? $articles_data->f('sale_unit') : 1;
		$total_articles+=($articles_data->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
		$project_del_articles=$db->query("SELECT * FROM service_delivery WHERE project_id='".$articles_data->f('project_id')."' AND a_id='".$articles_data->f('article_id')."' ");
		$total_art_q_del=0;
		$total_art_q_app=0;
		while($project_del_articles->next()){
			if($project_del_articles->f('invoiced')){
				$total_articles-=(($packing_art/$sale_unit_art)*$articles_data->f('price')*$project_del_articles->f('quantity'));
				$total_art_q_del+=$project_del_articles->f('quantity');
			}else{
				$total_articles_approved+=($project_del_articles->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
				$total_art_q_app+=$project_del_articles->f('quantity');
			}
		}
		/*if($articles_data->f('delivered')==1){
			$total_articles_approved+=($articles_data->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
		}*/
		if($in['show_approved']){
			//if($articles_data->f('delivered')){
			if($total_art_q_app){
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['name']=$articles_data->f('name');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['quantity']=$total_art_q_app;
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['price']=$articles_data->f('price');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['amount']=($total_art_q_app*($packing_art/$sale_unit_art)*$articles_data->f('price'));
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['company_name']=$articles_data->f('company_name');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_name']=$articles_data->f('project_name');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_id']=$articles_data->f('project_id');
				if(!in_array($articles_data->f('customer_id'), $customers_involved)){
					$customers_involved[] = $articles_data->f('customer_id');
				}
			}
		}else{
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['name']=$articles_data->f('name');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['quantity']=($articles_data->f('quantity')-$total_art_q_del);
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['price']=$articles_data->f('price');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['amount']=(($articles_data->f('quantity')-$total_art_q_del)*($packing_art/$sale_unit_art)*$articles_data->f('price'));
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['company_name']=$articles_data->f('company_name');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_name']=$articles_data->f('project_name');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_id']=$articles_data->f('project_id');
			if(!in_array($articles_data->f('customer_id'), $customers_involved)){
				$customers_involved[] = $articles_data->f('customer_id');
			}
		}

	}
if($in['show_approved']){
	$filter2 = " AND task_time.approved='1' ";
}
$data = billableAmountToInvoice($filter,$filter_task);
if($in['show_approved']){
	$data = billableAmountToInvoice($filter.$filter2,$filter_task);
}
$inc = 1;
$data2=array();
foreach ($data['details'] as $key => $value) {
	$total = 0;
	$html = include('report_all_body.php');
	if($total == 0){
		continue;
	}
	$pdf->AddPage();
	$pdf->writeHTML($html, true, false, true, false, '');
}
$var_once=1;
for($a=0;$a<$var_once;$a++) {
	$is_alone_expense = false;
	$is_alone_purchase = false;
	$is_alone_article = false;
	$html = include('report_all_body_2.php');
	if(!$is_alone_expense && !$is_alone_purchase && !$is_alone_article){
		continue;
	}
	$pdf->AddPage();
	$pdf->writeHTML($html, true, false, true, false, '');
}

$pdf->lastPage();
if($in['do']=='project-report_all_pdf'){
	doQueryLog();
   // $pdf->Output($serial_number.'.pdf','I');
	$pdf->Output('Pdf Report.pdf','I');
}else{
   $pdf->Output(__DIR__.'/../../../project_pdf.pdf', 'F');
}
?>