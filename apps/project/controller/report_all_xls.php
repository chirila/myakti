<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

$filename ="invoicing_report.xls";
header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-type: application/vnd.ms-excel; charset=utf-8");

$filter = " task_time.billable='1' AND task_time.billed='0' AND service = '0' AND billable_type !=  '4' AND billable_type !='0' AND projects.active>'0' AND projects.stage>'0' ";
$approved_filter = " task_time.billable='1' AND task_time.billed='0' AND task_time.approved='1' AND service = '0' AND projects.active>'0' AND projects.stage>'0' ";
$filter_expenses .= " AND is_service='0' AND projects.active>'0' AND projects.stage>'0' AND project_expenses.billed !=  '1' ";
$filter_purchases .= " AND project_purchase.billable = '1' AND projects.active>'0' AND projects.stage>'0' ";
$filter_articles .= " AND projects.active>'0' AND projects.stage>'0' ";
$filter_task=" ";

if($in['from'] && $in['to']){
	$filter .= " AND date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
	$approved_filter .= " AND date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
	$filter_expenses .= " AND project_expenses.date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
}

if($in['customer_id']){
	$filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';
	$approved_filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';
	$filter_expenses .= " AND projects.customer_id =".$in["customer_id"]." ";
	$filter_purchases .= " AND projects.customer_id =".$in["customer_id"]." ";
	$filter_articles .= " AND projects.customer_id =".$in["customer_id"]." ";
	$filter_task.=' AND projects.customer_id ='.$in['customer_id'].' ';
}

$ALLOW_ARTICLE_PACKING = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
$ALLOW_ARTICLE_SALE_UNIT = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

$total_expenses_services = 0;
$expenses_services_data = $db->query("SELECT project_expenses.*,
					projects.customer_id, projects.name, projects.company_name,
					expense.unit_price, expense.name AS expense_name,
					project_expences.expense_id AS expense_id_2,
					task_time.approved,task_time.billable AS task_billable,task_time.billed AS task_billed

					FROM project_expenses
					LEFT JOIN projects ON project_expenses.project_id = projects.project_id
					LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                              LEFT JOIN expense ON project_expenses.expense_id = expense.expense_id
                              LEFT JOIN task_time ON project_expenses.project_id = task_time.project_id AND project_expenses.date = task_time.date AND project_expenses.user_id=task_time.user_id
					WHERE 1 =1
					AND projects.billable_type !=  '4'
					AND projects.invoice_method>0
					$filter_expenses ");
while($expenses_services_data->next()){
	if( ( $expenses_services_data->f('expense_id_2') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 ) ){
		if(!$expenses_services_data->f('unit_price')){
			$unit_price = 1;
		}else{
			$unit_price = $expenses_services_data->f('unit_price');
		}

		$total_expenses_services+=$unit_price*$expenses_services_data->f('amount');
		$expense_amount = $unit_price*$expenses_services_data->f('amount');

		if( ( $expenses_services_data->f('expense_id_2') && $expenses_services_data->f('approved') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 && $expenses_services_data->f('approved') ) ){
			$total_expenses_services_approved+=$unit_price*$expenses_services_data->f('amount');
			// echo $expenses_services_data->f('id');
	  //  		echo "<br>";
		}
		if($in['show_approved']){
			if( ( $expenses_services_data->f('expense_id_2') && $expenses_services_data->f('approved') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 && $expenses_services_data->f('approved') ) ){
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_name'] = $expenses_services_data->f('expense_name');
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_amount'] = $expense_amount;
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['company_name'] = $expenses_services_data->f('company_name');
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_name'] = $expenses_services_data->f('name');
				$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_id'] = $expenses_services_data->f('project_id');
				if(!in_array($expenses_services_data->f('customer_id'), $customers_involved)){
					$customers_involved[] = $expenses_services_data->f('customer_id');
				}
			}
		}else{
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_name'] = $expenses_services_data->f('expense_name');
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_amount'] = $expense_amount;
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['company_name'] = $expenses_services_data->f('company_name');
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_name'] = $expenses_services_data->f('name');
			$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_id'] = $expenses_services_data->f('project_id');
			if(!in_array($expenses_services_data->f('customer_id'), $customers_involved)){
				$customers_involved[] = $expenses_services_data->f('customer_id');
			}
		}

	}
}
//get total purchases
$total_purchases = 0;
$total_purchases_data = $db->query("SELECT project_purchase.project_purchase_id, project_purchase.description, project_purchase.quantity, project_purchase.unit_price, project_purchase.margin, project_purchase.delivered, projects.project_id, projects.name,projects.company_name,projects.customer_id
						FROM project_purchase
						LEFT JOIN projects ON project_purchase.project_id = projects.project_id
						WHERE 1 =1
						AND projects.billable_type !=  '4'
						AND projects.invoice_method>0
						AND billable =  '1'
						AND invoiced =  '0'
						$filter_purchases	");
while($total_purchases_data->next()){
	$purchase_quantity = $total_purchases_data->f('quantity');
	if(!$total_purchases_data->f('quantity')){
			$purchase_quantity = 1;
	}
	$total_purchases+=($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));

	if($total_purchases_data->f('delivered')){
		$total_purchases_approved+=($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
	}
	if($in['show_approved']){
		if($total_purchases_data->f('delivered')){
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_description'] = $total_purchases_data->f('description');
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['company_name'] = $total_purchases_data->f('company_name');
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_amount'] = ($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_name'] = $total_purchases_data->f('name');
			$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_id'] = $total_purchases_data->f('project_id');
			if(!in_array($total_purchases_data->f('customer_id'), $customers_involved)){
				$customers_involved[] = $total_purchases_data->f('customer_id');
			}
		}
	}else{
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_description'] = $total_purchases_data->f('description');
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['company_name'] = $total_purchases_data->f('company_name');
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_amount'] = ($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_name'] = $total_purchases_data->f('name');
		$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_id'] = $total_purchases_data->f('project_id');
		if(!in_array($total_purchases_data->f('customer_id'), $customers_involved)){
			$customers_involved[] = $total_purchases_data->f('customer_id');
		}
	}

}
$total_articles=0;
$total_articles_approved=0;
$articles_data=$db->query("SELECT project_articles.*,projects.invoice_method,projects.customer_id,projects.name as project_name,projects.project_id,projects.company_name,pim_articles.packing,pim_articles.sale_unit FROM project_articles
				LEFT JOIN projects ON project_articles.project_id=projects.project_id
				LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
				WHERE project_articles.billed='0'
				AND projects.invoice_method>0
				 $filter_articles ");
	while($articles_data->next()){
		$packing_art=($ALLOW_ARTICLE_PACKING && $articles_data->f('packing')>0) ? $articles_data->f('packing') : 1;
		$sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $articles_data->f('sale_unit')>0) ? $articles_data->f('sale_unit') : 1;
		$total_articles+=($articles_data->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
		$project_del_articles=$db->query("SELECT * FROM service_delivery WHERE project_id='".$articles_data->f('project_id')."' AND a_id='".$articles_data->f('article_id')."' ");
		$total_art_q_del=0;
		$total_art_q_app=0;
		while($project_del_articles->next()){
			if($project_del_articles->f('invoiced')){
				$total_articles-=(($packing_art/$sale_unit_art)*$articles_data->f('price')*$project_del_articles->f('quantity'));
				$total_art_q_del+=$project_del_articles->f('quantity');
			}else{
				$total_articles_approved+=($project_del_articles->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
				$total_art_q_app+=$project_del_articles->f('quantity');
			}
		}
		/*if($articles_data->f('delivered')==1){
			$total_articles_approved+=($articles_data->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
		}*/
		if($in['show_approved']){
			//if($articles_data->f('delivered')){
			if($total_art_q_app){
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['name']=$articles_data->f('name');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['quantity']=$total_art_q_app;
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['price']=$articles_data->f('price');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['amount']=($total_art_q_app*($packing_art/$sale_unit_art)*$articles_data->f('price'));
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['company_name']=$articles_data->f('company_name');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_name']=$articles_data->f('project_name');
				$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_id']=$articles_data->f('project_id');
				if(!in_array($articles_data->f('customer_id'), $customers_involved)){
					$customers_involved[] = $articles_data->f('customer_id');
				}
			}
		}else{
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['name']=$articles_data->f('name');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['quantity']=($articles_data->f('quantity')-$total_art_q_del);
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['price']=$articles_data->f('price');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['amount']=(($articles_data->f('quantity')-$total_art_q_del)*($packing_art/$sale_unit_art)*$articles_data->f('price'));
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['company_name']=$articles_data->f('company_name');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_name']=$articles_data->f('project_name');
			$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_id']=$articles_data->f('project_id');
			if(!in_array($articles_data->f('customer_id'), $customers_involved)){
				$customers_involved[] = $articles_data->f('customer_id');
			}
		}

	}
if($in['show_approved']){
	$filter2 = " AND task_time.approved='1' ";
}
$data = billableAmountToInvoice($filter,$filter_task);
if($in['show_approved']){
	$data = billableAmountToInvoice($filter.$filter2,$filter_task);
}
$inc = 1;
$data2=array();
$output='';
//var_dump($data['details']);
foreach ($data['details'] as $key => $value) {
	$total = 0;
	$table_task = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
				<tr>
					<th>&nbsp;</th>
				</tr>
			  </table>
			  <table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			   <caption>
			      	<p bgcolor=\"#EEEEEE\">'".gm('Customer Name')."': [CUSTOMER_NAME]</p>
			      </caption>
			    <thead>
			      <tr>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Tasks')."'</th>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Project Manager')."'</th>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Quantity')."'</th>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Unit Price')."'</th>
			        	<th bgcolor=\"#EEEEEE\">'".gm('Amount')."'</th>
			      </tr>
			    </thead>
			    <tbody>";
	
		foreach ($value as $k => $val) {
			$project_man_task='';
			$c_name = $val['name'];
			if(!$val['quantity'] || !$val['unit_price']){
				continue;
			}
			$data2[$key] = $inc;
			$inc++;
			$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$val['project_id']."' AND manager='1' ");
			while($proj_name->next()){
				$project_man_task.=utf8_decode(get_user_name($proj_name->f('user_id'))).',';
			}
			
			$table_task .="<tr>      
				      <td bgcolor=\"#EEEEEE\">'".$val['description']."'</td>
				      <td bgcolor=\"#EEEEEE\">'".rtrim($project_man_task,",")."'</td>      
				      <td bgcolor=\"#EEEEEE\">'".display_number($val['quantity'])."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($val['unit_price']))."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($val['quantity']*$val['unit_price']))."'</td>
				    </tr>";
			
			$i++;		
			$total += $val['quantity']*$val['unit_price'];
		}
		$table_task.="</tbody></table>";
		$table_task.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    	<tr>
			      	<td colspan=\"5\" bgcolor=\"#EEEEEE\">'".gm('Total')."' : '".place_currency(display_number($total))."'</td>
			      </tr>
			    </table>";
		$table_task = str_replace('[CUSTOMER_NAME]', rtrim($c_name,","), $table_task);

		if($total == 0){
			continue;
			$table_task = '';
		}
		$output .= $table_task;

		//articles amount loop
		$table_article="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    <thead>
			      <tr>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Articles')."'</th>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Project Manager')."'</th>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Quantity')."'</th>
			      	<th bgcolor=\"#EEEEEE\">'".gm('Unit Price')."'</th>
			        <th bgcolor=\"#EEEEEE\">'".gm('Amount')."'</th>
			      </tr>
			    </thead>
			    <tbody>";
		$articles_total=0;
		foreach ($articles_array[$key] as $key5 => $articles) {
			$project_man_art='';
			$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$articles['project_id']."' AND manager='1' ");
			while($proj_name->next()){
				$project_man_art.=utf8_decode(get_user_name($proj_name->f('user_id'))).',';
			}
			$articles_total += $articles['amount'];
			$table_article .="<tr>      
				      <td bgcolor=\"#EEEEEE\">'".$articles['project_name']."' : '".$articles['name'].".</td> 
				      <td bgcolor=\"#EEEEEE\">'".rtrim($project_man_art,",")."'</td>     
				      <td bgcolor=\"#EEEEEE\">'".display_number($articles['quantity'])."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($articles['price']))."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($articles['amount']))."'</td>
				    </tr>";
		}
		$table_article.="</tbody></table>";
		$table_article.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    	<tr>
			      	<td colspan=\"5\" bgcolor=\"#EEEEEE\">'".gm('Total')."' : '".place_currency(display_number($articles_total))."'</td>
			      </tr>
			    </table>";
		if(!$articles_array[$key]){
			$table_article='';
		}
		$output .=$table_article;

		$table_expense="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    <thead>
			      <tr>
			         <th bgcolor=\"#EEEEEE\">'".gm('Expenses')."'</th>
			         <th bgcolor=\"#EEEEEE\">'".gm('Project Manager')."'</th>
			        <th bgcolor=\"#EEEEEE\">'".gm('Amount')."'</th>
			      </tr>
			    </thead>
			    <tbody>";
		$expense_total = 0;
		foreach ($expenses_array[$key] as $key2 => $expenses) {
			$project_man_exp='';
			$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$expenses['project_id']."' AND manager='1' ");
			while($proj_name->next()){
				$project_man_exp.=utf8_decode(get_user_name($proj_name->f('user_id'))).',';
			}
			$expense_total += $expenses['expense_amount'];
			$table_expense .="<tr>      
				      <td bgcolor=\"#EEEEEE\">'".$expenses['project_name']."' : '".( $expenses['expense_name'] ? $expenses['expense_name'] : gm('Ad Hoc Expense') ).".</td>      
				      <td bgcolor=\"#EEEEEE\">'".rtrim($project_man_exp,",")."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($expenses['expense_amount']))."'</td>
				    </tr>";
		}
		$table_expense.="</tbody></table>";
		$table_expense.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    	<tr>
			      	<td colspan=\"3\" bgcolor=\"#EEEEEE\">'".gm('Total')."' : '".place_currency(display_number($expense_total))."'</td>
			      </tr>
			    </table>";
		if(!$expenses_array[$key]){
			$table_expense='';
		}
		$output .=$table_expense;

		//customer purchases loop
		$table_purchase="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    <thead>
			      <tr>
			         <th bgcolor=\"#EEEEEE\">'".gm('Purchases')."'</th>
			         <th bgcolor=\"#EEEEEE\">'".gm('Project Manager')."'</th>
			        <th bgcolor=\"#EEEEEE\">'".gm('Amount')."'</th>
			      </tr>
			    </thead>
			    <tbody>";
		$purchase_total = 0;
		foreach ($purchases_array[$key] as $key4 => $purchases) {
			$project_man_prc='';
			$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$purchases['project_id']."' AND manager='1' ");
			while($proj_name->next()){
				$project_man_prc.=utf8_decode(get_user_name($proj_name->f('user_id'))).',';
			}
			$purchase_total += $purchases['purchase_amount'];
			$table_purchase .="<tr>      
				      <td bgcolor=\"#EEEEEE\">'".$purchases['project_name']."' : '".$purchases['purchase_description'].".</td>      
				      <td bgcolor=\"#EEEEEE\">'".rtrim($project_man_prc,",")."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($purchases['purchase_amount']))."'</td>
				    </tr>";
		}
		$table_purchase.="</tbody></table>";
		$table_purchase.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    	<tr>
			      	<td colspan=\"3\" bgcolor=\"#EEEEEE\">'".gm('Total')."' : '".place_currency(display_number($purchase_total))."'</td>
			      </tr>
			    </table>";
		if(!$purchases_array[$key]){
			$table_purchase='';
		}
		$output .=$table_purchase;




	
}
echo $output;

$output_alone_article="";

		if($data2){
			$alone_articles = array_diff_key($articles_array, $data2);
		}else{
			$alone_articles = $articles_array;
		}
		foreach ($alone_articles as $cust_id => $value) {
			if(!in_array($cust_id, $customers_involved)){
				$customers_involved[] = $cust_id;
			}
		}
		$is_alone_article = false;
		foreach ($alone_articles as $key => $value) {
			$alone_article="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
						<tr>
							<th>&nbsp;</th>
						</tr>
					  </table>
					  <table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
					  	<caption>
					      	<p bgcolor=\"#EEEEEE\">'".gm('Customer Name')."': [CUSTOMER_NAME_ART]</p>
					      </caption>
					    <thead>
					      <tr>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Articles')."'</th>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Project Manager')."'</th>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Quantity')."'</th>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Unit Price')."'</th>
					        <th bgcolor=\"#EEEEEE\">'".gm('Amount')."'</th>
					      </tr>
					    </thead>
					    <tbody>";
			$alone_article_total = 0;
			$alone_customer_name_art='';
			foreach ($articles_array[$key] as $key3 => $articles) {
				$project_man_art='';
				$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$articles['project_id']."' AND manager='1' ");
				while($proj_name->next()){
					$project_man_art.=utf8_decode(get_user_name($proj_name->f('user_id'))).',';
				}
				$is_alone_article = true;
				$alone_article_total += $articles['amount'];
				$alone_article .="<tr>      
				      <td bgcolor=\"#EEEEEE\">'".$articles['project_name']."' : '".$articles['name'].".</td> 
				      <td bgcolor=\"#EEEEEE\">'".rtrim($project_man_art,",")."'</td>    
				      <td bgcolor=\"#EEEEEE\">'".display_number($articles['quantity'])."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($articles['price']))."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($articles['amount']))."'</td>
				    </tr>";
				$alone_customer_name_art = $articles['company_name'];
			}
			$alone_article.="</tbody></table>";
			$alone_article.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    	<tr>
			      	<td colspan=\"5\" bgcolor=\"#EEEEEE\">'".gm('Total')."' : '".place_currency(display_number($alone_article_total))."'</td>
			      </tr>
			    </table>";
			$alone_article = str_replace('[CUSTOMER_NAME_ART]', rtrim($alone_customer_name_art,","), $alone_article);
			if(!$is_alone_article){
				$alone_article='';
			}
			$output_alone_article.=$alone_article;
		}
		

echo $output_alone_article;

$output_alone_expense="";

		if($data2){
			$alone_expenses = array_diff_key($expenses_array, $data2);
		}else{
			$alone_expenses = $expenses_array;
		}
		foreach ($alone_expenses as $cust_id => $value) {
			if(!in_array($cust_id, $customers_involved)){
				$customers_involved[] = $cust_id;
			}
		}
		$is_alone_expense = false;
		foreach ($alone_expenses as $key => $value) {
			$alone_expense="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
						<tr>
							<th>&nbsp;</th>
						</tr>
					  </table>
					  <table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
					  	<caption>
					      	<p bgcolor=\"#EEEEEE\">'".gm('Customer Name')."': [CUSTOMER_NAME_EXP]</p>
					      </caption>
					    <thead>
					      <tr>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Expenses')."'</th>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Project Manager')."'</th>
					        	<th bgcolor=\"#EEEEEE\">'".gm('Amount')."'</th>
					      </tr>
					    </thead>
					    <tbody>";		
			$alone_customer_name_exp='';
			$alone_expense_total = 0;
			foreach ($expenses_array[$key] as $key2 => $expenses) {
				$project_man_exp='';
				$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$expenses['project_id']."' AND manager='1' ");
				while($proj_name->next()){
					$project_man_exp.=utf8_decode(get_user_name($proj_name->f('user_id'))).',';
				}
				$is_alone_expense = true;
				$alone_expense_total += $expenses['expense_amount'];
				$alone_expense .="<tr>      
				      <td bgcolor=\"#EEEEEE\">'".$expenses['project_name']."' : '".( $expenses['expense_name'] ? $expenses['expense_name'] : gm('Ad Hoc Expense') ).".</td>      
				      <td bgcolor=\"#EEEEEE\">'".rtrim($project_man_exp,",")."'</td>  
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($expenses['expense_amount']))."'</td>
				    </tr>";
				$alone_customer_name_exp = $expenses['company_name'];
			}
			$alone_expense.="</tbody></table>";
			$alone_expense.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    	<tr>
			      	<td colspan=\"3\" bgcolor=\"#EEEEEE\">'".gm('Total')."' : '".place_currency(display_number($alone_expense_total))."'</td>
			      </tr>
			    </table>";
			$alone_expense = str_replace('[CUSTOMER_NAME_EXP]', rtrim($alone_customer_name_exp,","), $alone_expense);
			if(!$is_alone_expense){
				$alone_expense='';
			}
			$output_alone_expense.=$alone_expense;
		}

echo $output_alone_expense;

$output_alone_purchase="";

		if($data2){
			$alone_purchases = array_diff_key($purchases_array, $data2);
		}else{
			$alone_purchases = $purchases_array;
		}
		foreach ($alone_purchases as $cust_id => $value) {
			if(!in_array($cust_id, $customers_involved)){
				$customers_involved[] = $cust_id;
			}
		}
		$is_alone_purchase = false;
		foreach ($alone_purchases as $key => $value) {
			$alone_purchase="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
						<tr>
							<th>&nbsp;</th>
						</tr>
					  </table>
					  <table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
					  	<caption>
					      	<p bgcolor=\"#EEEEEE\">'".gm('Customer Name')."': [CUSTOMER_NAME_PRC]</p>
					      </caption>
					    <thead>
					      <tr>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Purchases')."'</th>
					      	<th bgcolor=\"#EEEEEE\">'".gm('Project Manager')."'</th>
					        	<th bgcolor=\"#EEEEEE\">'".gm('Amount')."'</th>
					      </tr>
					    </thead>
					    <tbody>";			
			$alone_customer_name_prc='';
			$alone_purchase_total = 0;
			foreach ($purchases_array[$key] as $key2 => $purchases) {
				$project_man_prc='';
				$proj_name=$db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$purchases['project_id']."' AND manager='1' ");
				while($proj_name->next()){
					$project_man_prc.=utf8_decode(get_user_name($proj_name->f('user_id'))).',';
				}
				$is_alone_purchase = true;
				$alone_purchase_total += $purchases['purchase_amount'];
				$alone_purchase .="<tr>      
				      <td bgcolor=\"#EEEEEE\">'".$purchases['project_name']."' : '".$purchases['purchase_description'].".</td>      
				      <td bgcolor=\"#EEEEEE\">'".rtrim($project_man_prc,",")."'</td>
				      <td bgcolor=\"#EEEEEE\">'".place_currency(display_number($purchases['purchase_amount']))."'</td>
				    </tr>";
				$alone_customer_name_prc = $purchases['company_name'];
			}
			$alone_purchase.="</tbody></table>";
			$alone_purchase.="<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" >
			    	<tr>
			      	<td colspan=\"3\" bgcolor=\"#EEEEEE\">'".gm('Total')."' : '".place_currency(display_number($alone_purchase_total))."'</td>
			      </tr>
			    </table>";
			$alone_purchase = str_replace('[CUSTOMER_NAME_PRC]', rtrim($alone_customer_name_prc,","), $alone_purchase);
			if(!$is_alone_purchase){
				$alone_purchase='';
			}
			$output_alone_purchase.=$alone_purchase;
		}

echo $output_alone_purchase;
doQueryLog();
exit();

?>