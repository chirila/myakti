<?php
/************************************************************************
* @Author: MFeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProjectPurchase extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getData(){
		$in = $this->in;
		
		$in['description'] = $in['purchase_name'];
		if($in['project_purchase_id']){
			$purchase = $this->db->query("SELECT *, customers.name FROM project_purchase
															LEFT JOIN customers ON project_purchase.supplier_id=customers.customer_id
															WHERE project_purchase_id='".$in['project_purchase_id']."' ");
			$purchase->next();

			$in['description'] = $purchase->f('description');
			$in['quantity'] = $purchase->f('quantity');
			$in['unit_price'] = $purchase->f('unit_price');
			$in['margin'] = $purchase->f('margin');
			$in['ids'] = $in['project_purchase_id'];
			$disabled = false;
			if($purchase->f('invoiced')){
				$disabled = true;
			}
		}

		$result=array(
			'project_purchase_id'			=> $in['project_purchase_id'] ? $in['project_purchase_id'] : '',
			'ACT'						=> $in['project_purchase_id'] ? 'project-project-project-update_purchase' : 'project-project-project-add_purchase',
			//'SUPPLIER_ID'				=> $in['project_purchase_id'] ? $purchase->f('supplier_id') : $in['supplier_id'],
			'supplier'					=> $in['project_purchase_id'] ? $purchase->f('supplier_id') : $in['supplier'],
			'purchase_date'				=> $in['project_purchase_id'] ? $purchase->f('purchase_date') : $in['quote_ts'],
			'INVOICE_DATE'				=> $in['project_purchase_id'] ? ($purchase->f('purchase_date') ? date(ACCOUNT_DATE_FORMAT,$purchase->f('purchase_date')) : '') : ($in['purchase_date'] ? date(ACCOUNT_DATE_FORMAT,$in['purchase_date']) : ''),
			'billable'					=> $in['project_purchase_id'] ? ($purchase->f('billable') ? true : false) : false,
			'delivered'					=> $in['project_purchase_id'] ? ($purchase->f('delivered') ? true : false) : false,
			'project_id'				=> $in['project_id'],
			'DISABLE'					=> $disabled ? true : false,
		);

		$sell_price = ($in['unit_price']*$in['margin']/100)+$in['unit_price'];
		$line_total = $in['unit_price']*$in['quantity'];

		$result['description'] 		= $in['description'];
		$result['quantity']		= display_number($in['quantity']);
		$result['unit_price']		= display_number($in['unit_price']);
		$result['margin']			= display_number($in['margin']);
		$result['sell_price']		= display_number($sell_price);
		$result['LINE_TOTAL']		= display_number($line_total);
		$result['TR_ID']			= $in['ids'] ? $in['ids'] : '1';
		$result['STYLE']     		= ACCOUNT_NUMBER_FORMAT;
		$result['PICK_DATE_FORMAT']	= pick_date_format();
		$result['HIDE_CURRENCY2']	= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
		$result['HIDE_CURRENCY1']	= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
		$result['title']			= $in['project_purchase_id'] ? gm('Edit Purchase') : gm('Add Purchase');
		$result['suppliers']		= $this->get_suppliers($in);

		$this->out = $result;
	}

	public function get_suppliers(&$in){
		$q = strtolower($in["term"]);
		$filter =" active=1 AND is_admin='0' AND is_supplier='1' ";

		if($q){
			$filter .=" AND name LIKE '%".addslashes($q)."%'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}

		$suppliers=$this->db->query("SELECT customers.name,customers.customer_id FROM customers
						LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id WHERE $filter ORDER BY name");

		while($suppliers->move_next()){
		  	$accounts[trim($suppliers->f('name'))]=$suppliers->f('customer_id');
		}
		$result = array();

		if($accounts){
			foreach ($accounts as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "name"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key)));
					}
				}else{
					array_push($result, array("id"=>$value, "name"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key)));
				}
				if (count($result) > 50)
				break;
			}			
		}
		return $result;
	}

}

	$purchase_obj = new ProjectPurchase($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $purchase_obj->output($purchase_obj->$fname($in));
	}

	$purchase_obj->getData();
	$purchase_obj->output();

?>