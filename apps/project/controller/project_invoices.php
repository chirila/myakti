<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result=array('list'=>array());
$project_serial = $db->field("SELECT serial_number FROM projects WHERE project_id='".$in['project_id']."' ");
$l_r = ROW_PER_PAGE;
//$l_r = 10;

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$arguments_o='';
$arguments_s='';
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$arguments='';
$filter = 'WHERE `our_ref` LIKE  \'%'.$project_serial.'%\' ';
$filter_link = 'block';
$order_by = " ORDER BY t_date DESC, iii DESC ";

if(!empty($in['filter'])){
	$arguments.="&filter=".$in['filter'];
}

if(!empty($in['search'])){
	$filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}

$arguments = $arguments.$arguments_s;
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;

$tblinvoice = $db->query("SELECT tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date
			FROM tblinvoice ".$filter.$order_by );

$max_rows=$tblinvoice->records_count();
$result['max_rows']=$max_rows;
$tblinvoice->move_to($offset*$l_r);
$j=0;

while($tblinvoice->move_next() && $j<$l_r){
	$query=array(
		'info_link'     		=> 'index.php?do=invoice-invoice&invoice_id='.$tblinvoice->f('id'),
		'edit_link'     		=> 'index.php?do=invoice-ninvoice&invoice_id='.$tblinvoice->f('id'),
		'is_edit'			=> (isset($in['archived']) && $in['archived'] == 1) ? false : ($tblinvoice->f('sent') == 1 ? false : true),
		'amount'			=> place_currency(display_number($tblinvoice->f('amount'))),
		'amount_vat'		=> place_currency(display_number($tblinvoice->f('amount_vat'))),
		'id'                	=> $tblinvoice->f('id'),
		's_number'		     	=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
		'created'           	=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
		'seller_name'       	=> $tblinvoice->f('seller_name'),
		'buyer_name'        	=> $tblinvoice->f('buyer_name'),
		'invoice_id'		=> $tblinvoice->f('id'),
	);
	array_push($result['list'], $query);
	$j++;
}

$project_serial = $db->field("SELECT serial_number FROM projects WHERE project_id='".$in['project_id']."' ");

	$result['is_data']			= $in['project_id'] ? ( $j ? true : false ) : false;
	$result['view_go']			= $_SESSION['access_level'] == 1 ? true : false;
	$result['serial_nr']			= $project_serial;

return json_out($result);