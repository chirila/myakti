<?php if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class TimesheetExpense extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function get_Expenses(){
		$in = $this->in;

		$result=array();
		$filter = '1=1';

		if(!$in['user_id'] )
		{
			$in['user_id'] = $_SESSION['u_id'];
		}
		if($in['start']){
			$result['start']=$in['start'];
			$now	= $in['start'];
		}else{
			$now 				= time();
		}
		$today_of_week 		= date("N", $now);
		$month        		= date('n', $now);
		$year         		= date('Y', $now);
		$week_start 		= mktime(0,0,0,$month,(date('j', $now)-$today_of_week+1),$year);
		$week_end 			= $week_start + 604799;

		$result['week_txt'] = date(ACCOUNT_DATE_FORMAT,$week_start).' - '.date(ACCOUNT_DATE_FORMAT,$week_end);
		$result['ACTIVE_WEEK']='activated';
		$result['day_row']=array();
		$expense = $this->db->query("SELECT project_expenses.*, projects.company_name AS c_name, projects.name AS p_name, expense.name AS e_name,expense.unit_price, expense.unit, projects.customer_id as customer_id, projects.active
							   FROM project_expenses
							   INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
							   INNER JOIN projects ON project_expenses.project_id=projects.project_id
							   WHERE project_expenses.user_id='".$in['user_id']."' AND project_expenses.date BETWEEN '".$week_start."' AND '".$week_end."' ORDER BY id");
		$q=0;

		while ($expense->next()) {
			$amount = place_currency(display_number($expense->f('amount')));
			if($expense->f('unit_price')){
				$amount = place_currency(display_number(($expense->f('amount') * $expense->f('unit_price'))))." (".$expense->f('amount')." ".$expense->f('unit').")";
			}

			$temp_row=array(
				'DATE'   					=> date(ACCOUNT_DATE_FORMAT,$expense->f('date')),
				'PROJECT_CUSTOMER_NAME'			=> $expense->f('c_name'),
				'PROJECT_LIST_NAME'			=> $expense->f('p_name'),
				'PROJECR_EXPENSE'				=> $expense->f('e_name'),
				'CUSTOMER_ID1'				=> $expense->f('customer_id'),
				'AMOUNT'					=> $amount,
				'AMOUNT_TXT'				=> $expense->f('amount'),
				'NOTES'					=> split_lines($expense->f('note')) ? split_lines($expense->f('note')):'&nbsp;',
				'E_ID'					=> $expense->f('id'),
				'PROJECT_IDE1'				=> $expense->f('project_id'),
				'DATEE'					=> $expense->f('date'),
				'EXP_ID'					=> $expense->f('expense_id'),
				'VIEW_IMG'					=> $expense->f('picture') == "" ? false : true,
				'IMG_HREF'					=> UPLOAD_PATH.DATABASE_NAME."/receipt/".$expense->f('picture'),
				'VIEW_L'					=> $expense->f('billed') == 1 ? false : ($in['hide_s'] == 1 ? false : true),
				'HIDE_EDIT'					=> $in['is_ajax'] == 1 ? true : false,
			);
			array_push($result['day_row'], $temp_row);
			$q++;
		}

		$result['history_rowe']=array();
		$i=0;
		$activity = $this->db->query("SELECT * FROM timesheet_log WHERE user_id='".$in['user_id']."' AND start_date='".$week_start."' limit 5 ");
		while($activity->next()){
			$temp_h=array(
				'HISTORY_DATE'			=> $activity->f('date'),
				'HISTORY_DESCRIPTION'		=> $activity->f('message'),
			);
			array_push($result['history_rowe'], $temp_h);
			$i++;
		}
		if($i==0){
			$result['HIDE_LOG']=false;
		}else {
			$result['HIDE_LOG']= $_SESSION['access_level'] == 1 ? true : false;
		}

		if(!$in['tab']){
			$in['tab'] = 2;
		}
		$user_dd = build_user_dd_timesheet($in['user_id'], $_SESSION['access_level']);


		$result['PAGE_TITLE']	    	= gm('Expenses for').' <span id="username">'.$user_dd[0].$user_dd[1][0].($user_dd[1][1] > 1 ? '<b id="change_user2" class="'.($in['is_ajax'] == 1 ? 'hide': '').'" >&nbsp;</b>' : '').'</span>';
		$result['user_dd']			= $user_dd;
		$result['user_id']           		= $in['user_id'];
		$result['PAG']               		= $in['is_ajax'] == 1 ? 'timesheets' : $in['pag'];
		$result['ADD_ROW_BOX']       		= $in['no_hide'] ? true : false;
		$result['PROJECT_ERROR_ID']  		= $in['project_id'];
		$result['PROJECT_IDE']  		= $in['project_id'];
		$result['CUSTOMER_IDE']  		= $in['customer_id'];
		$result['EXPENSES_ID']  		= $in['expense_id'];
		$result['PROJECT_NAMEE']  		= $in['project_name'];
		$result['EXPENSE_NAME']  		= $in['expense_name'];
		$result['AMOUNT']  			= $in['amount'];
		$result['NEXT_FUNCTION']     		= 'project-timesheet-time-add_expense';
		$result['START']             		= $week_start;
		$result['week_s']        		= $week_start;
		$result['week_e']          		= $week_end;
		$result['HIDE_AJAX']         		= $in['is_ajax'] == 1 ? false : true;
		$result['DISPLAY_SUBMIT']		= $in['hide_s']==1 ? false : true;
		$result['IS_AJAX']           		= $in['is_ajax'] == 1 ? true : false;
		$result['hide_s']				= $in['hide_s'] == 1 ? false : true;
		$result['STYLE']             		= ACCOUNT_NUMBER_FORMAT;
		$result['tab']				= $in['tab'];
		$result['AMOUNT']				= $in['amount'];
		$result['DATABASE']			= DATABASE_NAME;
		$result['NO_HIDE']			= $in['no_hide'];
		$result['TODAY_nr']		= $today_of_week;
		$result['is_manager']			= $is_manager;
		$result['are_expenses']			= $q>0? true : false;
		$result['allowadhoc']			= USE_ADHOC_TMS == '1'? true:false;

		$this->out = $result;
	}

	public function get_ExpenseData(&$in){
		$result=array();
		$in['exp']=1;
		if($in['id']){
			$exp_data=$this->db->query("SELECT project_expenses.*,projects.customer_id FROM  project_expenses
				INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
				INNER JOIN projects ON project_expenses.project_id=projects.project_id
				WHERE project_expenses.id='".$in['id']."' ");
			$result['project_name']=$in['type']=='1' ? $exp_data->f('customer_id') : $exp_data->f('project_id');
			$result['project_id']=$in['type']=='1' ? 0 : $exp_data->f('project_id');
			$result['customer_id']=$in['type']=='1' ? $exp_data->f('customer_id') : 0;
			$result['expense_id']=$exp_data->f('expense_id');
			$result['note']=$exp_data->f('note');
			$result['amount']=$exp_data->f('amount');
			$result['date']=$exp_data->f('date');
			$result['billable']=$exp_data->f('billable') ? true : false;
			$result['do_next']='misc--time-update_expense';
			$result['id']=$in['id'];
		}else{
			$result['do_next']='misc--time-add_expense';
		}
		$result['type']=$in['type'];
		$result['user_id']=$in['user_id'];
		$result['SELECT_DAY']=$in['SELECT_DAY'];
		$result['TODAY_nr']= $in['TODAY_nr'];
		$result['companies_list']=$this->get_companies($in);
		$result['projects_list']=$this->get_projects($in);
		$result['categories_list']=$this->get_categories($in);
		return $result;
	}

	public function get_companies(&$in){

		$q = strtolower($in["term"]);
		$filter ="active=1 AND is_admin='0' ";

		if($q){
			$filter .=" AND name LIKE '%".addslashes($q)."%'";
		}

		if($is_admin){
			$filter =" is_admin='".$is_admin."' ";
		}

		if($in['current_id']){
			$filter .= " AND customer_id !='".$in['current_id']."'";
		}

		$this->db->query("SELECT name,customer_id,active,our_reference,currency_id,internal_language FROM customers WHERE $filter ORDER BY name ");

		while($this->db->move_next()){
		  	$accounts[trim($this->db->f('name'))]=$this->db->f('customer_id');
		  	$ref[trim($this->db->f('name'))]=$this->db->f('our_reference');
		  	$currency[trim($this->db->f('name'))]=$this->db->f('currency_id');
		  	$lang[trim($this->db -> f('name'))] = $this->db->f('internal_language');
		}

		$result = array();
		if($accounts){
			foreach ($accounts as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key), "name" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)), "ref" => $ref[$key], "currency_id" => $currency[$key], "lang_id" => $lang[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$key), "name" => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($key)), "ref" => $ref[$key], "currency_id" => $currency[$key], "lang_id" => $lang[$key]));
				}
				if (count($result) > 50)
				break;
			}
		}
		return $result;
	}

	public function get_projects(&$in){

		$q = strtolower($in["term"]);

		$filter ='';
		if($in['user_id']){
			$filter .=" AND project_user.user_id='".$in['user_id']."'";
			if($_SESSION['group'] != 'admin' && $in['user_id'] != $_SESSION['u_id']){
				$is_manager = $this->db->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");
				if(!empty($is_manager)){
					$filter .=" AND projects.project_id IN ( SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ) ";
				}
			}
		}
		if($q){
			$filter .=" AND (projects.name LIKE '%".$q."%' OR projects.company_name LIKE '%".$q."%') ";
		}

		if($in['exp'] == 1){
			$filter .= " AND add_expense='1' ";
		}
		$this->db->query("SELECT projects.project_id,projects.name AS p_name,projects.active, projects.customer_id, projects.company_name AS c_name
		            FROM projects
		            INNER JOIN project_user ON project_user.project_id=projects.project_id
		            WHERE projects.active=1 AND projects.closed='0' AND project_user.active='1' AND projects.stage='1' $filter ");
		while($this->db->move_next()){
		  $projects[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('project_id');
		  $customer[stripslashes($this->db->f('c_name'))." > ".$this->db->f('p_name')] = $this->db->f('customer_id');
		}

		$result = array();
		if($projects){
			foreach ($projects as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "c_id" => $customer[$key]));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "c_id" => $customer[$key]));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "name" => 'No matches'));
		}
		return $result;
	}

	public function get_categories(&$in){

		$q = strtolower($_GET["q"]);

		$filter = ' 1=1 ';

		if($q){
			$filter =" name LIKE '%".$q."%'";
		}

		$prj_bill =array();
		if($in['project_id']){
			$this->db->query("SELECT * FROM project_expences WHERE project_id = '".$in['project_id']."' ");
			while ($this->db->next()) {
				array_push($prj_bill, $this->db->f('expense_id'));
			}
		}

		$this->db->query("SELECT * FROM expense WHERE $filter");

		while($this->db->move_next()){
		  $expense[$this->db->f('name')]= $this->db->f('expense_id');
		  $unit_price[$this->db->f('name')] = $this->db->f('unit_price');
		  $unit[$this->db->f('name')] = $this->db->f('unit');
		  $billable = '';
		  if(in_array($this->db->f('expense_id'), $prj_bill)){
		  	$billable = 1;
		  }
		  $billable_expense[$this->db->f('name')] = $billable;
		}

		$result = array();
		if($expense){
			foreach ($expense as $key=>$value) {
				if($q){
					if (strpos(strtolower($key), $q) !== false) {
						array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "unit_price" => $unit_price[$key], "unitt" => $unit[$key], "billable_expense" => $billable_expense[$key] ));
					}
				}else{
					array_push($result, array("id"=>$value, "label"=>$key, "name" => strip_tags($key), "unit_price" => $unit_price[$key], "unitt" => $unit[$key], "billable_expense" => $billable_expense[$key] ));
				}
				if (count($result) > 2000)
				break;
			}
		}else {
			array_push($result, array("id"=>'0', "label"=>'No matches', "name" => 'No matches'));
		}
		return $result;
	}

}

	$timesheet_exp = new TimesheetExpense($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $timesheet_exp->output($timesheet_exp->$fname($in));
	}

	$timesheet_exp->get_Expenses();
	$timesheet_exp->output();

?>