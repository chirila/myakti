<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$result=array('query'=>array());
$l_r = ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$order_by =" ORDER BY serial_number DESC ";
$filter = ' 1=1 ';
if($in['customer_id']){
	$filter .=" AND customer_id='".$in['customer_id']."' ";
}

if(!$in['archived'] || $in['archived']=='0'){
	$filter.= " AND projects.active=1 ";
}else{
	$filter.= " AND projects.active=0 ";
}


//FILTER LIST

if(!empty($in['search'])){
	$filter .= " AND (projects.name LIKE '%".$in['search']."%' OR company_name LIKE '%".$in['search']."%' OR projects.serial_number LIKE '%".$in['search']."%' OR project_articles.name LIKE '%".$in['search']."%')";
}

if($in['first'] && $in['first'] == '[0-9]'){
	$filter.=" AND SUBSTRING(LOWER(projects.name),1,1) BETWEEN '0' AND '9'";
	$arguments.="&first=".$in['first'];
} elseif ($in['first']){
	$filter .= " AND projects.name LIKE '".$in['first']."%'  ";
	$arguments.="&first=".$in['first'];
}
if(!empty($in['order_by'])){
	$order = " ASC ";
	if($in['desc'] == '1' || $in['desc']== 'true'){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
}
$selected =array();

if($_SESSION['access_level'] !='1'){
	$filter .=" AND project_user.user_id='".$_SESSION['u_id']."' AND project_user.active='1' ";
}

/*$db->query("SELECT DISTINCT first_letter FROM projects
			LEFT JOIN project_user ON projects.project_id=project_user.project_id
			WHERE ".$filter." ORDER BY name");
while($db->move_next()){
	array_push($selected, $db->f('first_letter'));
}*/
$arguments = $arguments.$arguments_s;

$max_rows =  $db->field("SELECT count(DISTINCT projects.project_id) FROM projects
						LEFT JOIN project_user ON projects.project_id=project_user.project_id
						INNER JOIN project_articles ON projects.project_id=project_articles.project_id
						WHERE ".$filter ."AND project_articles.delivered='0'");
$result['max_rows']=$max_rows;
$prefix_lenght=strlen(ACCOUNT_PROJECT_START)+1;

$db->query("SELECT projects.name, projects.project_id, projects.stage, company_name, start_date, end_date, serial_number,projects.active FROM projects			
			LEFT JOIN project_user ON projects.project_id=project_user.project_id
			INNER JOIN project_articles ON projects.project_id=project_articles.project_id
			WHERE ".$filter." AND project_articles.delivered='0' GROUP BY project_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r );
global $config;

$now = time();

while($db->move_next() && $j<$l_r){
	$ref = "";
	if(ACCOUNT_PROJECT_REF && $db->f('our_reference')){
		$ref = $db->f('our_reference');
	}


	switch ($db->f('stage')) {
		case '1':
			$status = gm('Active');
			$img = 'green';
			break;
		case '2':
			$status = gm('Closed');
			break;
		default:
			$status = gm('Draft');
			$img = 'gray';
			break;
	}

	$query=array(
		'NAME'				=> $db->f('name'),
		'EDIT_LINK'			=> 'index.php?do=project-project&project_id='.$db->f('project_id'),
	  	'CUSTOMER'			=> $db->f('company_name'),
	  	'PROJECT_NUMBER'	=> $ref.$db->f('serial_number'),
	  	'PROJECT_DATE'		=> $db->f('start_date') ? date(ACCOUNT_DATE_FORMAT,$db->f('start_date')) : '&nbsp;',
	  	'END_DATE'			=> $db->f('end_date') ? date(ACCOUNT_DATE_FORMAT,$db->f('end_date')) : '&nbsp;',
	  	'project_id'		=> $db->f('project_id'),
	  	'stage'		=> $db->f('stage'),
	  	'status_title'		=> $status,
	);
	array_push($result['query'], $query);
	$j++;
}

$default_lang_id = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
$default_pdf_type = ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;

	$result['lr'] = $l_r;
	$result['lid']=$default_lang_id;
	$result['pdf_type']=$default_pdf_type;
	$result['export_link']="index.php?do=project-export_to_deliver_projects&search=".$in['search'];


return json_out($result);