<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

	$project_data=$db->query("SELECT status_rate,is_contact,company_name,customer_id FROM projects WHERE project_id='".$in['project_id']."'");

	$result=array(
		'VIEW_PURCHASE_OPTIONS' 	=> false,
		'VIEW_TASK_OPTIONS' 		=> false,
		'VIEW_ARTICLE_OPTIONS'		=> false,
		'HIDE_BASE_TYPE1'			=> false,
		'base_type'				=> $in['base_type'],
		'currency_type'			=> $in['currency_type'],
		'not_6'				=> $in['billable_type'] == 6 ? false : true ,
		'first_choice'			=> $project_data->f('status_rate')==0 ? gm('All approved + un-invoiced billable hours and billable expenses') : gm('All approved + un-invoiced billable days and billable expenses'),
		'second_choice'			=> $project_data->f('status_rate')==0 ? gm('Approved  + un-invoiced billable hours and billable expenses from') : gm('Approved  + un-invoiced billable days and billable expenses from'),
		'billable_h'			=> $project_data->f('status_rate')==0 ? gm('Billable Hours') : gm('Billable Days'),
		'hours_type'			=> '0',
		'custom_hours'			=> '1',
		'projects_id'			=> array($in['project_id'])
	);

	if($project_data->f('is_contact')){
		$result['contact_id']		= $project_data->f('customer_id');
	}else{
		$result['buyer_id']		= $project_data->f('customer_id');
	}

	if($project_data->f('is_contact')){
		$result['contact_name']		= $project_data->f('company_name')." > ";
	}else{
		$result['customer_name']	= $project_data->f('company_name');
	}

	if($in['base_type'] == 1 && $in['buyer_id']){
		$result['custom_hours_dd'] 		= build_custom_hours_list();
		$result['HIDE_BASE_TYPE1']		= true;
	}

	if($in['base_type'] == 4 && $in['buyer_id']){
		$result['tasks_id'] 			= build_task_list($in['project_id'],$in['buyer_id']);
		$result['VIEW_TASK_OPTIONS']		= true;
	}

	$purchase = $db->field("SELECT project_purchase_id FROM project_purchase WHERE project_id='".$in['project_id']."' AND delivered='1' AND billable='1' AND invoiced='0' ");
	if($purchase){
		$result['VIEW_PURCHASE_OPTIONS'] 	= true;	
		$result['project_purchases_id'] 	= build_purchase_list($in['project_id'],$in['buyer_id']);
	}

	$article = $db->field("SELECT service_delivery.a_id FROM service_delivery 
		INNER JOIN projects ON service_delivery.project_id=projects.project_id
		WHERE service_delivery.project_id='".$in['project_id']."' AND projects.invoice_method>0 AND service_delivery.invoiced='0' GROUP BY service_delivery.a_id");
	if($article){
		$result['VIEW_ARTICLE_OPTIONS'] 	= true;	
		$result['project_a_id'] 		= build_article_list($in['project_id'],$in['buyer_id']);
	}

return json_out($result);
?>