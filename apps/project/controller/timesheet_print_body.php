<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
$path = __DIR__.'/../view/';
global $mode;
$path2 = '';


$db = new sqldb();
$db2=new sqldb();
$db3 = new sqldb();
$db5 = new sqldb();

$path = ark::$viewpath;

if($in['type']){
	$html='reports_print_'.$in['type'].'.html';
}elseif ($in['custom_type']) {
	$path = INSTALLPATH.'upload/'.DATABASE_NAME.'/custom_pdf/';
	$html='custom_project_print-'.$in['custom_type'].'.html';
}elseif ($in['body']){
	$html='customizable_print_'.$in['layout'].'.html';
}elseif($in['body_id']){
	$html='customizable_body_print_'.$in['body_id'].'.html';
}elseif($in['header']){
	$html='customizable_print_header_'.$in['layout'].'.html';
}elseif($in['layout'] || $in['footer_id']){
	$html='customizable_print_footer_'.$in['layout'].'.html';
}else{
	$html='reports_print_1.html';
}

// echo $path.$html;exit();
// $html='invoice_print_6.html';
$view_html = new at($path.$html);

if($in['type']){
	$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_HEADER_PDF_FORMAT' ");
	$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_BODY_PDF_FORMAT' ");
	$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_FOOTER_PDF_FORMAT' ");
	$in['header_id']=$def_header;
	$in['footer_id']=$def_footer;

		$view_html->assign(array(
		'head'			  => true,
		'foot'			  => true,
		));
}

$labels_query = $db->query("SELECT * FROM label_language_time WHERE label_language_id='".$in['lid']."'")->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
	$labels[$key] = $value;
	$j++;
}
$labels_query_custom = $db->query("SELECT * FROM label_language_time WHERE lang_code='".$in['lid']."'")->getAll();
if(is_array($labels_query_custom['0'])){
	foreach ($labels_query_custom['0'] as $key => $value) {
		$labels[$key] = $value;
		$j++;
	}
}

if($in['lid']<=4){
	$labels_quotes = $db->query("SELECT * FROM label_language_time WHERE label_language_id='".$in['lid']."'")->getAll();
}else{
	$labels_quotes = $db->query("SELECT * FROM label_language_time WHERE lang_code='".$in['lid']."'")->getAll();
}
$labels_q = array();
foreach ($labels_quotes['0'] as $key => $value) {
	$labels_q[$key] = $value;
	$j++;
}




$img = 'images/no-logo.png';

if($in['logo']){
	$print_logo = $path2.$in['logo'];
}else {
	$print_logo=$path2.ACCOUNT_LOGO_TIMESHEET;
}
if($print_logo){
	$img = $print_logo;
	$size = getimagesize($img);
	$ratio = 250 / 77;
	if($size[0]/$size[1] > $ratio ){
		$attr = 'width="250"';
	}else{
		$attr = 'height="77"';
	}
}

if($in['t_preview']){
if($in['header']){
		$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='1' AND layout='".$in['layout']."' ");
		if($data_exist1){
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
		}else{
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='0' AND layout='".$in['layout']."' ");
		}
		$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
		if($data_exist2){
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
		}else{
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
		}

	}elseif($in['header_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}elseif($in['footer_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}


//header1
	$needle = "[label]";
	$needle_end = "[/label]";
	$positions = array();
	$raw_data=array();
	$final_data=array();
	$initPos = 0;
	while (($initPos = strpos($content1, $needle, $initPos))!== false) {
	 $lastPos=strpos($content1, $needle_end, $initPos);
	    $positions[$initPos + strlen($needle)] = $lastPos;
	    $initPos = $initPos + strlen($needle);
	}

	foreach ($positions as $key=>$value) {	 
	    $raw_data[]=substr($content1,$key,$value-$key);
	}
	foreach($raw_data as $value){
	 if($labels[$value]){
			$final_data[$value]=$labels[$value];		
		}else{
	 		$final_data[$value]=$labels_q[$value];
		}
	}
	foreach($final_data as $key=>$value){
	 	$content1=str_replace("[label]".$key."[/label]",$value,$content1);
	}

//header1

//footer1
	$needle2 = "[label]";
	$needle_end2 = "[/label]";
	$positions2 = array();
	$raw_data2=array();
	$final_data2=array();
	$initPos2 = 0;
	while (($initPos2 = strpos($content2, $needle2, $initPos2))!== false) {
	 $lastPos2=strpos($content2, $needle_end2, $initPos2);
	    $positions2[$initPos2 + strlen($needle2)] = $lastPos2;
	    $initPos2 = $initPos2 + strlen($needle2);
	}

	foreach ($positions2 as $key=>$value) {	 
	    $raw_data2[]=substr($content2,$key,$value-$key);
	}
	foreach($raw_data2 as $value){
		if($labels[$value]){
			$final_data2[$value]=$labels[$value];		
		}else{
	 		$final_data2[$value]=$labels_q[$value];
		}
	}
	
	foreach($final_data2 as $key=>$value){
	 	$content2=str_replace("[label]".$key."[/label]",$value,$content2);
	}
//footer1w



	$pdf->setQuotePageLabel(gettime_label_txt('page',$in['lid']));
	$all_hours = 480;
	$time_start = 1420434000; # 05/01/2015
	$time_end = 1420952400; # 11/01/2015
	$customer_name = '[CUSTOMER NAME]';
	$user_name = '[CONSULTANT NAME]';



	$view_html->assign(array(
		'account_logo'        		=> $img,
		'attr'				=> $attr,
		'all_hours'				=> number_as_hour($all_hours),
		'timeframe'				=> date(ACCOUNT_DATE_FORMAT,$time_start)." - ".date(ACCOUNT_DATE_FORMAT,$time_end),
		'timeframe_txt'			=> gettime_label_txt('timeframe',$in['lid']),
		'hours_txt'				=> gettime_label_txt('hours',$in['lid']),
		'days_txt'				=> gettime_label_txt('days',$in['lid']),
		'total_txt'				=> gettime_label_txt('total',$in['lid']),
		'client_txt'			=> gettime_label_txt('client',$in['lid']),
		'project_txt'			=> gettime_label_txt('project',$in['lid']),
		'task_txt'				=> gettime_label_txt('task',$in['lid']),
		'person_txt'			=> gettime_label_txt('person',$in['lid']),
		'customer_txt'			=> gettime_label_txt('customer',$in['lid']),
		'consultant_txt'			=> gettime_label_txt('consultant',$in['lid']),
		'comment_txt'			=> gettime_label_txt('comment',$in['lid']),
		'customer_name'			=> $customer_name,
		'consultant_name'			=> $user_name,
		'is_customer'			=> $is_customer,
		'consultant'			=> $consultant,
		'project_name'			=> '[PROJECT NAME]',
		'is_client'				=> true,
		'is_project'			=> true,
		'is_task'				=> true,
		'width_cp'				=> '30%',
		'width'				=> '30%',
		'consultant'			=> true,
	));

	for ($i=0; $i < 3; $i++) {

		for($j=1;$j<5;$j++){
			$view_html->assign(array(
				'client'		=> '[COMPANY NAME]',
				'project'		=> '[PROJECT NAME]',
				'task'		=> '[TASK NAME]',
				'hours'			=> number_as_hour(40),
				'comment'		=> nl2br('[Notes]'),
				'person'		=> '[CONSULTANT NAME]',
				'colspan_comm'	=> 4,
			),'project_row');
			$view_html->loop('project_row','day_row');
		}

		$view_html->assign(array(
			'date'		=> date("d/m/Y",$time_start+($i*(60*60*24))),
			'total_hours'	=> number_as_hour(160),
			'colspan'		=> 3,
		),'day_row');
		$view_html->loop('day_row');
	}

}else{
if($in['header']){
		$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='1' AND layout='".$in['layout']."' ");
		if($data_exist1){
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
		}else{
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='0' AND layout='".$in['layout']."' ");
		}
		$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
		if($data_exist2){
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
		}else{
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
		}

	}elseif($in['header_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}elseif($in['footer_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='project' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*
			exit();*/
	}
	// var_dump($content1);
//header1
	$needle = "[label]";
	$needle_end = "[/label]";
	$positions = array();
	$raw_data=array();
	$final_data=array();
	$initPos = 0;
	while (($initPos = strpos($content1, $needle, $initPos))!== false) {
	 $lastPos=strpos($content1, $needle_end, $initPos);
	    $positions[$initPos + strlen($needle)] = $lastPos;
	    $initPos = $initPos + strlen($needle);
	}

	foreach ($positions as $key=>$value) {	 
	    $raw_data[]=substr($content1,$key,$value-$key);
	}
	foreach($raw_data as $value){
	 	if($labels[$value]){
			$final_data[$value]=$labels[$value];		
		}else{
	 		$final_data[$value]=$labels_q[$value];
		}
	}
	
	foreach($final_data as $key=>$value){
	 	$content1=str_replace("[label]".$key."[/label]",$value,$content1);
	}

//header1

//footer1
	$needle2 = "[label]";
	$needle_end2 = "[/label]";
	$positions2 = array();
	$raw_data2=array();
	$final_data2=array();
	$initPos2 = 0;
	while (($initPos2 = strpos($content2, $needle2, $initPos2))!== false) {
	 $lastPos2=strpos($content2, $needle_end2, $initPos2);
	    $positions2[$initPos2 + strlen($needle2)] = $lastPos2;
	    $initPos2 = $initPos2 + strlen($needle2);
	}

	foreach ($positions2 as $key=>$value) {	 
	    $raw_data2[]=substr($content2,$key,$value-$key);
	}

	foreach($raw_data2 as $value){
	 	if($labels[$value]){
			$final_data2[$value]=$labels[$value];		
		}else{
	 		$final_data2[$value]=$labels_q[$value];
		}
	}

	foreach($final_data2 as $key=>$value){
	 	$content2=str_replace("[label]".$key."[/label]",$value,$content2);
	}

//footer1w




	$time_start = $in['time_start'];
	$time_end = $in['time_end'];

	$filter = " AND task_time.user_id='".$in['user_id']."' ";
	$is_customer = false;
	$consultant = true;
	$default_name = 'timesheet_print';
	$join_timesheet_only = ' ';
	$filter_timesheet_only = ' ';
	if($in['customer_id'])
	{
		$join_timesheet_only = ' INNER JOIN projects ON task_time.project_id=projects.project_id ';
		$filter_timesheet_only = " AND projects.customer_id='".$in['customer_id']."' ";
		$customer_name = $db->field("SELECT company_name FROM projects WHERE customer_id='".$in['customer_id']."'");
		$is_customer = true;
	}
	if($in['project_id']){
		$filter = " AND project_id='".$in['project_id']."' AND approved='1' AND billable=1  ";
		$customer_name = $db->field("SELECT company_name FROM projects WHERE project_id='".$in['project_id']."'");
		$is_customer = true;
		$consultant = false;
		$default_name .="_inv";
	}

	if($in['timesheet_only']){
		$join_timesheet_only = " LEFT JOIN project_user
						ON task_time.project_id = project_user.project_id
						AND task_time.user_id = project_user.user_id ";
		$filter_timesheet_only = " AND project_user.manager =  '1' ";
	}

	if($in['id']){
		$filter_invoice=" AND invoice_id='".$in['id']."' ";
		$task_time_test = $db->field("SELECT COUNT(task_time_id)
	            FROM task_time
	            ".$join_timesheet_only."
	            WHERE date >= '".$time_start."' AND date < '".$time_end."' AND hours!='0' ".$filter_invoice.$filter.$filter_timesheet_only."
	            ORDER BY task_time.date
			   ");
		if($task_time_test>0){
			$filter.= " AND invoice_id='".$in['id']."' ";
		}
	}
	$task_time = $db->query("SELECT task_time.*
	            FROM task_time
	            ".$join_timesheet_only."
	            WHERE date >= '".$time_start."' AND date < '".$time_end."' AND hours!='0' ".$filter.$filter_timesheet_only."
	            ORDER BY task_time.date
			   ");

	$all_hours = 0;
	$all_days=0;
	$comment_active = false;
	$nr_cols = $db->field("SELECT COUNT(default_name) FROM default_data WHERE default_name='".$default_name."' AND active='1' AND value!='comment' and value!='person'");
	// $nr_cols = $nr_cols;
	$active_cols = $db->query("SELECT * FROM default_data WHERE default_name='".$default_name."'");
	while ($active_cols->next()) {
		if($active_cols->f('active')=='1')
		{
			$view_html->assign(array(
				'is_'.$active_cols->f('value')		=> true,
			));
			if($active_cols->f('value')=='comment')
			{
				$comment_active = true;
			}
		}else
		{
			$view_html->assign(array(
				'is_'.$active_cols->f('value')		=> false,
			));
		}
	}
	switch ($nr_cols) {
		case '1':
			$view_html->assign(array(
				'width_cp'		=> '84%',
				'width'			=> '84%',
			));
			break;
		case '2':
			$view_html->assign(array(
				'width_cp'		=> '42%',
				'width'			=> '42%',
			));
			break;
		case '3':
			$view_html->assign(array(
				'width_cp'		=> '28%',
				'width'			=> '28%',
			));
			break;
		case '4':
			$view_html->assign(array(
				'width_cp'		=> '21%',
				'width'			=> '21%',
			));
			break;
		default:
			$view_html->assign(array(
				'width_cp'		=> '84%',
				'width'			=> '84%',
				'is_nada'		=> true,
			));
			$nr_cols++;
			break;
	}

	while($task_time->next())
	{
		//$all_hours = $all_hours + $task_time->f('hours');
		$total_hours = $db->field("SELECT SUM(hours) FROM task_time ".$join_timesheet_only." WHERE `date`='".$task_time->f('date')."' ".$filter.$filter_timesheet_only." AND project_status_rate='0' ");
		$total_days = $db->field("SELECT SUM(hours) FROM task_time ".$join_timesheet_only." WHERE `date`='".$task_time->f('date')."' ".$filter.$filter_timesheet_only." AND project_status_rate='1' ");
		if($task_time->f('project_status_rate') == 0){
			$all_hours +=$task_time->f('hours');
		}else{
			$all_days +=$task_time->f('hours');
		}
			
		$project_info = $db->query("SELECT projects.*, tasks.*
									FROM projects
									INNER JOIN tasks ON projects.project_id=tasks.project_id
									WHERE projects.project_id = '".$task_time->f('project_id')."' AND tasks.task_id='".$task_time->f('task_id')."' ");
		$user_name = $db->field("SELECT user_name FROM project_user WHERE user_id='".$task_time->f('user_id')."'");
		if($project_info->next())
		{
			$view_html->assign(array(
				'client'		=> $project_info->f('company_name'),
				'project'		=> $project_info->f('name'),
				'task'			=> $project_info->f('task_name'),
				'hours'			=> $task_time->f('project_status_rate') == 0 ? number_as_hour($task_time->f('hours')) : '',
				'day'				=> $task_time->f('project_status_rate') > 0 ? $task_time->f('hours') : '',
				'comment'		=> nl2br($task_time->f('notes')),
				'person'		=> $user_name,
				'colspan_comm'	=> $nr_cols+1,
			),'project_row');
			if($comment_active==true)
			{
				if(trim($task_time->f('notes'))!='')
				{
					$view_html->assign('is_comment_line',true,'project_row');
				}else
				{
					$view_html->assign('is_comment_line',false,'project_row');
				}
			}
			$view_html->loop('project_row','day_row');
		}
		if($last_date!=$task_time->f('date'))
		{
			$view_html->assign(array(
				'date'			=> date("d/m/Y",$task_time->f('date')),
				'total_hours'	=> number_as_hour($total_hours),
				'total_days'	=> $total_days,
				'colspan'		=> $nr_cols,
			),'day_row');
			$last_date = $task_time->f('date');
		}else
		{
			$view_html->assign(array(
				'hide_date'			=> 'hide',
			),'day_row');
		}
		$view_html->loop('day_row');

	}

	$pdf->setQuotePageLabel(gettime_label_txt('page',$in['lid']));

	$view_html->assign(array(
		'account_logo'        		=> $img,
		'attr'				=> $attr,
		'all_hours'				=> number_as_hour($all_hours),
		'all_days'				=> $all_days,
		'timeframe'				=> date(ACCOUNT_DATE_FORMAT,$time_start)." - ".date(ACCOUNT_DATE_FORMAT,$time_end),
		'timeframe_txt'		=> gettime_label_txt('timeframe',$in['lid']),
		'hours_txt'				=> gettime_label_txt('hours',$in['lid']),
		'days_txt'				=> gettime_label_txt('days',$in['lid']),
		'total_txt'				=> gettime_label_txt('total',$in['lid']),
		'client_txt'			=> gettime_label_txt('client',$in['lid']),
		'project_txt'			=> gettime_label_txt('project',$in['lid']),
		'task_txt'				=> gettime_label_txt('task',$in['lid']),
		'person_txt'			=> gettime_label_txt('person',$in['lid']),
		'customer_txt'		=> gettime_label_txt('customer',$in['lid']),
		'consultant_txt'	=> gettime_label_txt('consultant',$in['lid']),
		'comment_txt'			=> gettime_label_txt('comment',$in['lid']),
		'customer_name'		=> $customer_name,
		'consultant_name'	=> $user_name,
		'is_customer'			=> $is_customer,
		'consultant'			=> $consultant,
		'project_name'		=> $in['project_id'] ? $db->field("SELECT name FROM projects WHERE project_id='{$in['project_id']}' ") : '',
	));
}
$vars=$view_html->return_vars();
	$needle1 = "[!";
	$needle_end1 = "!]";
	$positions1 = array();
	$raw_data1=array();
	$final_data1=array();
	$initPos1 = 0;
	// var_dump($content1);	
	while (($initPos1 = strpos($content1, $needle1, $initPos1))!== false) {
	 	$lastPos1=strpos($content1, $needle_end1, $initPos1);
	    $positions1[$initPos1 + strlen($needle1)] = $lastPos1;
	    $initPos1 = $initPos1 + strlen($needle1);
	}

	foreach ($positions1 as $key=>$value) {	 
	    $raw_data1[]=substr($content1,$key,$value-$key);
	}

	foreach($raw_data1 as $value){
 		$final_data1[$value]=$vars->$value;
	}
// var_dump($final_data1);
	foreach($final_data1 as $key=>$value){
		if($key=='ACCOUNT_LOGO'){
			$value=$img;
		}
/*		if(!$value){
			$value = '-';
		}*/
			
	 	$content1=str_replace("[!".$key."!]",$value,$content1);
	 	// if(!$value){
	 	// 	$content1=preg_replace('/\[if\:'.$key.'\!\](.*)\[end:'.$key.'\!\]/', '', $content1);
	 	// }
	}
//header1

//footer1
	$vars3=$view_html->return_vars();
	$needle3 = "[!";
	$needle_end3 = "!]";
	$positions3 = array();
	$raw_data3=array();
	$final_data3=array();
	$initPos3 = 0;
	while (($initPos3 = strpos($content2, $needle3, $initPos3))!== false) {
	 	$lastPos3=strpos($content2, $needle_end3, $initPos3);
	    $positions3[$initPos3 + strlen($needle3)] = $lastPos3;
	    $initPos3 = $initPos3 + strlen($needle3);
	}

	foreach ($positions3 as $key=>$value) {	 
	    $raw_data3[]=substr($content2,$key,$value-$key);
	}

	foreach($raw_data3 as $value){
 		$final_data3[$value]=$vars->$value;
	}
// var_dump($final_data3);
	foreach($final_data3 as $key=>$value){
		if($key=='ACCOUNT_LOGO'){
			$value=$img;
		}
/*		if(!$value){
			$value = '-';
		}*/
		
	 	$content2=str_replace("[!".$key."!]",$value,$content2);
	}

//footer1
	if($in['header_id']){
		$pdf->setQuotePageLabel('salesassist_2');
		$pdf->Custom_footer($content2);
	}elseif($in['footer_id']){
		$pdf->setQuotePageLabel('salesassist_2');
		$pdf->Custom_footer($content2);

	}

	$view_html->assign(array(
		'HEADER'			  => $content1,
	));


return $view_html->fetch();