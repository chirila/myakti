<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class ProjectToBeInvoiced extends Controller{
	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getData(){
		session_write_close();
		ini_set('memory_limit','4G');
		$in=$this->in;

		$output=array();
		$customers_involved = array();
		$filter = " task_time.billable='1' AND task_time.billed='0' AND service = '0' AND billable_type !=  '4' AND billable_type !='0' AND projects.active>'0' AND projects.stage>'0' ";
		$approved_filter = " task_time.billable='1' AND task_time.billed='0' AND task_time.approved='1' AND service = '0' AND projects.active>'0' AND projects.stage>'0' ";
		$filter_expenses = " AND is_service='0' AND projects.active>'0' AND projects.stage>'0' AND project_expenses.billed !=  '1' ";
		$filter_purchases = " AND project_purchase.billable = '1' AND projects.active>'0' AND projects.stage>'0' ";
		$filter_articles = " AND projects.active>'0' AND projects.stage>'0' ";
		$filter_task=" ";

		$output['periods']=array(
			array('id'=>'1', 'value'=> gm('All Periods')),
			array('id'=>'2', 'value'=> gm('This month')),
			array('id'=>'3', 'value'=> gm('Last month'))
		);
		$now = time();
		$today_of_week  = date("N", $now);
		$month        	= date('n');
		$year         	= date('Y');
		if($in['period_id']){
			$output['period_id']=$in['period_id'];
			switch ($in['period_id']) {
				case '1':
					//all
					$in['from'] = '';
					$in['to'] = '';
					break;
				case '2':
					//this month
					$week_start = mktime(0,0,0,$month,1,$year);
					$week_end   = mktime(23,59,59,$month+1,0,$year);
					$in['from'] = $week_start;
					$in['to'] = $week_end;
					break;
				case '3':
					//last month
					$week_start = mktime(0,0,0,$month-1,1,$year);
					$week_end   = mktime(23,59,59,$month,0,$year);
					$in['from'] = $week_start;
					$in['to'] = $week_end;
					break;
			}
		}else{
			$output['period_id']='1';
			$in['from'] = '';
			$in['to'] = '';
			$in['period_id']=1;
		}
		
		$output['FROM']	= $in['from'];
		$output['TO']	= $in['to'];

		if($in['from'] && $in['to']){
			$filter .= " AND date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
			$approved_filter .= " AND date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
			$filter_expenses .= " AND project_expenses.date BETWEEN '".$in['from']."' AND '".$in['to']."' ";
		}
		if($in['customer_id']>'0'){
			$this->db->field("SELECT name FROM customers WHERE customer_id='".$in['customer_id']."' ");
			$filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';
			$approved_filter .= ' AND task_time.customer_id ='.$in['customer_id'].' ';
			$filter_expenses .= " AND projects.customer_id =".$in["customer_id"]." ";
			$filter_purchases .= " AND projects.customer_id =".$in["customer_id"]." ";
			$filter_articles .= " AND projects.customer_id =".$in["customer_id"]." ";
			$filter_task.=' AND projects.customer_id ='.$in['customer_id'].' ';
			$output['customer_id']	= $in['customer_id'];
		}else{
			$output['customer_id']	= '0';
		}

		$ALLOW_ARTICLE_PACKING = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$ALLOW_ARTICLE_SALE_UNIT = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

		$is_data = false;
		// get total hours
		$total = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) AS total_h FROM task_time LEFT JOIN projects
					ON task_time.project_id = projects.project_id
				 	LEFT JOIN tasks
					ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
				 	WHERE $filter AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_h_rate !='0') OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_h_rate!='0') ) ");

		$total_days = $this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) AS total_days FROM task_time LEFT JOIN projects
					ON task_time.project_id = projects.project_id
				 	LEFT JOIN tasks
					ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
				 	WHERE $filter  AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_daily_rate !='0') OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_daily_rate!='0') ) ");

		//get total expenses and services
		//services are part of a project and can be also billable
		$total_expenses_services = 0;
		$expenses_services_data = $this->db->query("SELECT project_expenses.*,
							projects.customer_id, projects.name, projects.company_name,
							expense.unit_price, expense.name AS expense_name,
							project_expences.expense_id AS expense_id_2,
							task_time.approved,task_time.billable AS task_billable,task_time.billed AS task_billed
							FROM project_expenses
							LEFT JOIN projects ON project_expenses.project_id = projects.project_id
							LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
		                              LEFT JOIN expense ON project_expenses.expense_id = expense.expense_id
		                              LEFT JOIN task_time ON project_expenses.project_id = task_time.project_id AND project_expenses.date = task_time.date AND project_expenses.user_id=task_time.user_id
							WHERE 1 =1
							AND projects.billable_type !=  '4'
							AND projects.invoice_method>0
							$filter_expenses ");
		while($expenses_services_data->next()){
			if( ( $expenses_services_data->f('expense_id_2') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 ) ){
				if(!$expenses_services_data->f('unit_price')){
					$unit_price = 1;
				}else{
					$unit_price = $expenses_services_data->f('unit_price');
				}

				$total_expenses_services+=$unit_price*$expenses_services_data->f('amount');
				$expense_amount = $unit_price*$expenses_services_data->f('amount');

				if( ( $expenses_services_data->f('expense_id_2') && $expenses_services_data->f('approved') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 && $expenses_services_data->f('approved') ) ){
					$total_expenses_services_approved+=$unit_price*$expenses_services_data->f('amount');
				}

				if($in['show_approved']){
					if( ( $expenses_services_data->f('expense_id_2') && $expenses_services_data->f('approved') ) || ( !$expenses_services_data->f('expense_id_2') && $expenses_services_data->f('billable')==1 && $expenses_services_data->f('approved') ) ){
						$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_name'] = $expenses_services_data->f('expense_name');
						$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_amount'] = $expense_amount;
						$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['company_name'] = $expenses_services_data->f('company_name');
						$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_name'] = $expenses_services_data->f('name');
						$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_id'] = $expenses_services_data->f('project_id');
						if(!in_array($expenses_services_data->f('customer_id'), $customers_involved)){
							$customers_involved[] = $expenses_services_data->f('customer_id');
						}
					}
				}else{

					$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_name'] = $expenses_services_data->f('expense_name');
					$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['expense_amount'] = $expense_amount;
					$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['company_name'] = $expenses_services_data->f('company_name');
					$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_name'] = $expenses_services_data->f('name');
					$expenses_array[$expenses_services_data->f('customer_id')][$expenses_services_data->f('id')]['project_id'] = $expenses_services_data->f('project_id');
					if(!in_array($expenses_services_data->f('customer_id'), $customers_involved)){
						$customers_involved[] = $expenses_services_data->f('customer_id');
					}

				}
			}
		}
		//get total purchases
		$total_purchases = 0;
		$total_purchases_data = $this->db->query("SELECT project_purchase.project_purchase_id, project_purchase.description, project_purchase.quantity, project_purchase.unit_price, project_purchase.margin, project_purchase.delivered, projects.project_id, projects.name,projects.company_name,projects.customer_id
								FROM project_purchase
								LEFT JOIN projects ON project_purchase.project_id = projects.project_id
								WHERE 1 =1
								AND projects.billable_type !=  '4'
								AND projects.invoice_method>0
								AND billable =  '1'
								AND invoiced =  '0'
								$filter_purchases	");
		while($total_purchases_data->next()){
			$purchase_quantity = $total_purchases_data->f('quantity');
			if(!$total_purchases_data->f('quantity')){
					$purchase_quantity = 1;
			}
			$total_purchases+=($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));

			if($total_purchases_data->f('delivered')){
				$total_purchases_approved+=($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
			}

			if($in['show_approved']){
				if($total_purchases_data->f('delivered')){
					$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_description'] = $total_purchases_data->f('description');
					$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['company_name'] = $total_purchases_data->f('company_name');
					$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_amount'] += ($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
					$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_name'] = $total_purchases_data->f('name');
					$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_id'] = $total_purchases_data->f('project_id');
					if(!in_array($total_purchases_data->f('customer_id'), $customers_involved)){
						$customers_involved[] = $total_purchases_data->f('customer_id');
					}
				}
			}else{
				$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_description'] = $total_purchases_data->f('description');
				$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['company_name'] = $total_purchases_data->f('company_name');
				$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['purchase_amount'] += ($purchase_quantity*$total_purchases_data->f('unit_price')) + ($purchase_quantity*$total_purchases_data->f('unit_price')/100*$total_purchases_data->f('margin'));
				$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_name'] = $total_purchases_data->f('name');
				$purchases_array[$total_purchases_data->f('customer_id')][$total_purchases_data->f('project_purchase_id')]['project_id'] = $total_purchases_data->f('project_id');
				if(!in_array($total_purchases_data->f('customer_id'), $customers_involved)){
					$customers_involved[] = $total_purchases_data->f('customer_id');
				}
			}
		}
		$total_articles=0;
		$total_articles_approved=0;
		$articles_data=$this->db->query("SELECT project_articles.*,projects.invoice_method,projects.customer_id,projects.name as project_name,projects.project_id,projects.company_name,pim_articles.packing,pim_articles.sale_unit FROM project_articles
						LEFT JOIN projects ON project_articles.project_id=projects.project_id
						LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id
						WHERE project_articles.billed='0'
						AND projects.invoice_method>0
						 $filter_articles ");
			while($articles_data->next()){
				$packing_art=($ALLOW_ARTICLE_PACKING && $articles_data->f('packing')>0) ? $articles_data->f('packing') : 1;
				$sale_unit_art=($ALLOW_ARTICLE_SALE_UNIT && $articles_data->f('sale_unit')>0) ? $articles_data->f('sale_unit') : 1;
				$total_articles+=($articles_data->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
				$project_del_articles=$this->db->query("SELECT * FROM service_delivery WHERE project_id='".$articles_data->f('project_id')."' AND a_id='".$articles_data->f('article_id')."' ");
				$total_art_q_del=0;
				$total_art_q_app=0;
				while($project_del_articles->next()){
					if($project_del_articles->f('invoiced')){
						$total_articles-=(($packing_art/$sale_unit_art)*$articles_data->f('price')*$project_del_articles->f('quantity'));
						$total_art_q_del+=$project_del_articles->f('quantity');
					}else{
						$total_articles_approved+=($project_del_articles->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
						$total_art_q_app+=$project_del_articles->f('quantity');
					}
				}
				/*$total_articles+=($articles_data->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));			
				if($articles_data->f('delivered')==1){
					$total_articles_approved+=($articles_data->f('quantity')*($packing_art/$sale_unit_art)*$articles_data->f('price'));
				}*/
				if($in['show_approved']){
					//if($articles_data->f('delivered')){
					if($total_art_q_app){
						$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['name']=$articles_data->f('name');
						$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['quantity']=$total_art_q_app;
						$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['price']=$articles_data->f('price');
						$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['amount']=($total_art_q_app*($packing_art/$sale_unit_art)*$articles_data->f('price'));
						$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['company_name']=$articles_data->f('company_name');
						$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_name']=$articles_data->f('project_name');
						$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_id']=$articles_data->f('project_id');
						if(!in_array($articles_data->f('customer_id'), $customers_involved)){
							$customers_involved[] = $articles_data->f('customer_id');
						}
					}
				}else{
					$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['name']=$articles_data->f('name');
					$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['quantity']=($articles_data->f('quantity')-$total_art_q_del);
					$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['price']=$articles_data->f('price');
					$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['amount']=(($articles_data->f('quantity')-$total_art_q_del)*($packing_art/$sale_unit_art)*$articles_data->f('price'));
					$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['company_name']=$articles_data->f('company_name');
					$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_name']=$articles_data->f('project_name');
					$articles_array[$articles_data->f('customer_id')][$articles_data->f('a_id')]['project_id']=$articles_data->f('project_id');
					if(!in_array($articles_data->f('customer_id'), $customers_involved)){
						$customers_involved[] = $articles_data->f('customer_id');
					}
				}
			}
			
		$total_in_hours = $total;
		$total_in_days = $total_days;
		$total+=$total_expenses_services+$total_purchases+$total_articles;

		if($in['show_approved']){
			$filter2 = " AND task_time.approved='1' ";
		}

		$output['customers']=array();
		$output['alone_customers']=array();
		$output['alone_customers2']=array();
		$output['alone_customers3']=array();
		$output['HIDE_ALL']=true;
		if(empty($total) && empty($total_days)){
			$output['HIDE_ALL']=false;
		}
		else{
			$is_data = true;
			$data = billableAmountToInvoice($filter,$filter_task);
			$data_billable = billableAmountToInvoice($filter,$filter_task);
			$approved_data = 	billableAmountToInvoice($approved_filter,$filter_task);
			$data_billable['total']+=$total_expenses_services+$total_purchases+$total_articles;
			if($in['show_approved']){
				$data = billableAmountToInvoice($filter.$filter2,$filter_task);
			}

			$approved_data['total']+=$total_purchases_approved+$total_expenses_services_approved+$total_articles_approved;

			$output['HOURS_TRACKED'] 	= number_as_hour($total_in_hours);
			$output['DAYS_TRACKED']		= display_number($total_in_days);
			$output['BILLABLE_H']		= number_as_hour($this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
					LEFT JOIN projects ON task_time.project_id = projects.project_id
					LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
				 	WHERE $filter AND submited='1' AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_h_rate !='0') OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_h_rate!='0') ) "));
			$output['BILLABLE_D']	= display_number($this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
					LEFT JOIN projects ON task_time.project_id = projects.project_id
					LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
				 	WHERE $filter AND submited='1' AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_daily_rate !='0') OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_daily_rate!='0') ) "));
			$output['UNBILLABLE_H']	= number_as_hour($this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=0 THEN task_time.hours ELSE 0 END ) FROM task_time 
					LEFT JOIN projects ON task_time.project_id = projects.project_id
					LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
				 	WHERE $filter AND approved='1' AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_h_rate !='0') OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_h_rate!='0') ) "));
			$output['UNBILLABLE_D']	= display_number($this->db->field("SELECT SUM( CASE WHEN task_time.project_status_rate=1 THEN task_time.hours ELSE 0 END ) FROM task_time 
					LEFT JOIN projects ON task_time.project_id = projects.project_id
					LEFT JOIN tasks ON (task_time.project_id = tasks.project_id AND task_time.task_id = tasks.task_id)
					LEFT JOIN project_user ON (projects.project_id=project_user.project_id AND task_time.user_id=project_user.user_id)
				 	WHERE $filter AND approved='1' AND projects.invoice_method>0 AND ( ( (billable_type = '1' or billable_type = '7') AND t_daily_rate !='0') OR (billable_type = '3' AND pr_h_rate!='0') OR (billable_type = '2' AND p_daily_rate!='0') ) "));
			$output['BILLABLE_A']	= place_currency(display_number($data_billable['total']));
			$output['APPROVED_A']	= place_currency(display_number($approved_data['total']));

			$inc = 1;

			foreach ($data['details'] as $key => $value) {
				$total = 0;

				$lines=array();
				$exps=array();
				$srvs=array();
				$prcs=array();
				$arts=array();

				//customer expenses loop
				$project_man='';
				$man_array=array();
				foreach ($value as $k => $val) {
					//if(!$val['quantity'] || !$val['unit_price'] || ($in['show_approved'] && !$val['task_approved'] && !$is_expense) )

					if(!$val['quantity'] || !$val['unit_price'] ){
						// console::log('continue');
						continue;
					}

					if(!in_array($key, $customers_involved)){
						$customers_involved[] = $key;
					}
					if(!array_key_exists($val['project_id'],$man_array)){
						$man_array[$val['project_id']]=array();
					}
					$data2[$key] = $inc;
					$inc++;
					$c_name = $val['name'];		
					$proj_name=$this->db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$val['project_id']."' AND manager='1' ");
					while($proj_name->next()){
						if(!in_array($proj_name->f('user_id'),$man_array[$val['project_id']])){
							$project_man.=get_user_name($proj_name->f('user_id')).',';
							$man_array[$val['project_id']][]=$proj_name->f('user_id');
						}
					}
					$temp_line=array(
						'project_id'	=> $val['project_id'],
						'project_name'	=> $val['project_name'],
						'task_name'		=> $val['task_name'],
						'quantity'		=> ($in['show_approved'] && $val['task_approved']) ? display_number($val['quantity_approved']) : display_number($val['quantity']),
						'price'			=> place_currency(display_number($val['unit_price'])),
						'line_total'	=> ($in['show_approved'] && $val['task_approved']) ? place_currency(display_number($val['quantity_approved']*$val['unit_price'])) : place_currency(display_number($val['quantity']*$val['unit_price'])),
					);
					//if($in['show_approved'] && $val['task_approved']){
					if($in['show_approved']){
						$total += ($val['quantity_approved']*$val['unit_price']);
					}else{
						$total += ($val['quantity']*$val['unit_price']);
					}
					//if($in['show_approved'] && !$val['task_approved']){
					if($in['show_approved'] && $total==0){
						# do nothign
						$output['is_task']=false;
					}else{
						$output['is_task']=true;
						array_push($lines, $temp_line);
						//$view->loop('lines','customers');
					}
					
				}

				if($total == 0){
					// console::log('continue2');
					continue;
				}
				/*if($c_name == null){
					$c_name = $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$key."' ");
				}*/

				if($expenses_array[$key]){
					$is_expense = true;
				}else{
					$is_expense = false;
				}
				$expense_total = 0;
				foreach ($expenses_array[$key] as $key2 => $expenses) {
					$expense_total += $expenses['expense_amount'];
					$temp_exp=array(
						'project_id'		=> $expenses['project_id'],
						'project_name'		=> $expenses['project_name'],
						'expense_name'		=> $expenses['expense_name'] ? $expenses['expense_name'] : gm('Ad Hoc Expense'),
						'expense_amount' 		=> place_currency(display_number($expenses['expense_amount'])),
					);
					array_push($exps, $temp_exp);
					//$view->loop('expenses','customers');
				}

				//customer services loop
				if($services_array[$key]){
					$is_service = true;
				}else{
					$is_service = false;
				}
				$service_total = 0;

				foreach ($services_array[$key] as $key3 => $services) {
					$service_total += $services['expense_amount'];
					$temp_serv=array(
						'project_id'		=> $services['project_id'],
						'project_name'		=> $services['project_name'],
						'service_name'		=> $services['service_name'],
						'service_amount' 		=> place_currency(display_number($services['expense_amount'])),
					);
					array_push($srvs, $temp_serv);
					//$view->loop('services','customers');
				}

				//customer purchases loop
				if($purchases_array[$key]){
					$is_purchase = true;
				}else{
					$is_purchase = false;
				}
				$purchase_total = 0;
				foreach ($purchases_array[$key] as $key4 => $purchases) {
					$purchase_total += $purchases['purchase_amount'];
					$temp_prc=array(
						'project_id'	=> $purchases['project_id'],
						'project_name'	=> $purchases['project_name'],
						'purchase_description'	=> $purchases['purchase_description'],
						'purchase_amount' 	=> place_currency(display_number($purchases['purchase_amount'])),
					);
					array_push($prcs, $temp_prc);
					//$view->loop('purchases','customers');
				}

				//articles amount loop
				$articles_total=0;
				if($articles_array[$key]){
					$is_articles=true;
				}else{
					$is_articles=false;
				}

				foreach ($articles_array[$key] as $key5 => $articles) {
					$articles_total += $articles['amount'];
					$temp_art=array(
						'project_id'	=> $articles['project_id'],
						'project_name'	=> $articles['project_name'],
						'name'		=> $articles['name'],
						'quantity'		=> display_number($articles['quantity']),
						'price'		=> place_currency(display_number($articles['price'])),
						'amount' 		=> place_currency(display_number($articles['amount'])),
					);
					array_push($arts, $temp_art);
					//$view->loop('articles','customers');
				}

				$temp_cust=array(
					'c_name' 					=> $c_name,
					'proj_man'					=> rtrim($project_man,","),
					'total'					=> place_currency(display_number($total)),
					'is_expense' 				=> $is_expense,
					'expense_total' 				=> place_currency(display_number($expense_total)),
					'is_service' 				=> $is_service,
					'service_total' 				=> place_currency(display_number($service_total)),
					'is_purchase' 				=> $is_purchase,
					'purchase_total' 				=> place_currency(display_number($purchase_total)),
					'hide_if_service_or_purchase' 	=> ($is_service || $is_purchase) ? false : true,
					'hide_if_purchase' 			=> $is_purchase ? false : true,
					'is_article'				=> $is_articles,
					'articles_total'				=> place_currency(display_number($articles_total)),
					'lines'					=> $lines,
					'expenses'					=> $exps,
					'services'					=> $srvs,
					'purchases'					=> $prcs,
					'articles'					=> $arts
				);
				array_push($output['customers'], $temp_cust);
				//$view->loop('customers');
			}

			//expenses from projects that does not have billable tasks
			// console::log($expenses_array);
			if($data2){
				$alone_expenses = array_diff_key($expenses_array, $data2);
			}else{
				$alone_expenses = $expenses_array;
			}
			foreach ($alone_expenses as $cust_id => $value) {
				if(!in_array($cust_id, $customers_involved)){
					$customers_involved[] = $cust_id;
				}
			}
			$is_alone_expense = false;
			//console::log($alone_expenses);

			foreach ($alone_expenses as $key => $value) {

				$al_exp=array();
				$project_man='';
				$man_array=array();
				$alone_expense_total = 0;
				foreach ($expenses_array[$key] as $key2 => $expenses) {
					if(!array_key_exists($expenses['project_id'],$man_array)){
						$man_array[$expenses['project_id']]=array();
					}
					$proj_name=$this->db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$expenses['project_id']."' AND manager='1' ");
					while($proj_name->next()){
						if(!in_array($proj_name->f('user_id'),$man_array[$expenses['project_id']])){
							$project_man.=get_user_name($proj_name->f('user_id')).',';
							$man_array[$expenses['project_id']][]=$proj_name->f('user_id');
						}
					}
					$is_alone_expense = true;
					$alone_expense_total += $expenses['expense_amount'];
					$temp_all_exp=array(
						'project_id'		=> $expenses['project_id'],
						'project_name'		=> $expenses['project_name'],
						'expense_name'		=> $expenses['expense_name'] ? $expenses['expense_name'] : gm('Ad Hoc Expense'),
						'alone_expense_amount' 		=> place_currency(display_number($expenses['expense_amount'])),
						'customer' 				=> $expenses['company_name'],				
					);
					array_push($al_exp, $temp_all_exp);
					$alone_customer_name = $expenses['company_name'];
				}
				$temp_al_cust=array(
					'alone_customer_name' 		=> $alone_customer_name,
					'alone_expense_total' 		=> place_currency(display_number($alone_expense_total)),
					'man_name'				=> rtrim($project_man,","),
					'alone_expenses'			=> $al_exp
				);
				array_push($output['alone_customers'], $temp_al_cust);
			}
			
			$output['is_alone_expense'] 	= $is_alone_expense;

		// print_r($purchases_array);
			//purchases from projects that does not have billable tasks
			if($data2){
				$alone_purchases = array_diff_key($purchases_array, $data2);
			}else{
				$alone_purchases = $purchases_array;
			}
			foreach ($alone_purchases as $cust_id => $value) {
				if(!in_array($cust_id, $customers_involved)){
					$customers_involved[] = $cust_id;
				}
			}
			$is_alone_purchase = false;
			foreach ($alone_purchases as $key => $value) {

				$al_prc=array();
				$project_man='';
				$man_array=array();
				$alone_purchase_total = 0;
				foreach ($purchases_array[$key] as $key2 => $purchases) {
					if(!array_key_exists($purchases['project_id'],$man_array)){
						$man_array[$purchases['project_id']]=array();
					}
					$proj_name=$this->db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$purchases['project_id']."' AND manager='1' ");
					while($proj_name->next()){
						if(!in_array($proj_name->f('user_id'),$man_array[$purchases['project_id']])){
							$project_man.=get_user_name($proj_name->f('user_id')).',';
							$man_array[$purchases['project_id']][]=$proj_name->f('user_id');
						}
					}
					$is_alone_purchase = true;
					$alone_purchase_total += $purchases['purchase_amount'];
					$temp_al_prc=array(
						'project_id'		=> $purchases['project_id'],
						'project_name'		=> $purchases['project_name'],
						'purchase_description'	=> $purchases['purchase_description'],
						'alone_purchase_amount' 		=> place_currency(display_number($purchases['purchase_amount'])),
						'customer' 					=> $purchases['company_name'],				
					);
					array_push($al_prc, $temp_al_prc);
					$alone_customer_name = $purchases['company_name'];
				}
				$temp_al_cust2=array(
					'alone_customer_name' 		=> $alone_customer_name,
					'alone_purchase_total' 		=> place_currency(display_number($alone_purchase_total)),
					'man_name'				=> rtrim($project_man,","),
					'alone_purchases'			=> $al_prc	
				);
				array_push($output['alone_customers2'], $temp_al_cust2);
			}			
			$output['is_alone_purchase'] 	= $is_alone_purchase;

			if($data2){
				$alone_articles = array_diff_key($articles_array, $data2);
			}else{
				$alone_articles = $articles_array;
			}
			foreach ($alone_articles as $cust_id => $value) {
				if(!in_array($cust_id, $customers_involved)){
					$customers_involved[] = $cust_id;
				}
			}

			$is_alone_article = false;
			foreach ($alone_articles as $key => $value) {

				$al_art=array();
				$project_man='';
				$man_array=array();
				$alone_article_total = 0;
				foreach ($articles_array[$key] as $key3 => $articles) {
					if(!array_key_exists($articles['project_id'],$man_array)){
						$man_array[$articles['project_id']]=array();
					}
					$proj_name=$this->db->query("SELECT user_name,user_id FROM project_user WHERE project_id='".$articles['project_id']."' AND manager='1' ");
					while($proj_name->next()){
						if(!in_array($proj_name->f('user_id'),$man_array[$articles['project_id']])){
							$project_man.=get_user_name($proj_name->f('user_id')).',';
							$man_array[$articles['project_id']][]=$proj_name->f('user_id');
						}
					}
					$is_alone_article = true;
					$alone_article_total += $articles['amount'];
					$temp_al_art=array(
						'project_id'		=> $articles['project_id'],
						'project_name'		=> $articles['project_name'],
						'name'			=> $articles['name'],
						'alone_article_quantity' 		=> display_number($articles['quantity']),
						'alone_article_price' 			=> place_currency(display_number($articles['price'])),
						'alone_article_amount' 			=> place_currency(display_number($articles['amount'])),
						'customer' 					=> $articles['company_name'],				
					);
					array_push($al_art, $temp_al_art);
					$alone_customer_name = $articles['company_name'];
				}
				$temp_al_cust3=array(
					'alone_customer_name' 		=> $alone_customer_name,
					'alone_article_total' 		=> place_currency(display_number($alone_article_total)),
					'man_name'				=> rtrim($project_man,","),
					'alone_articles'			=> $al_art
				);
				array_push($output['alone_customers3'], $temp_al_cust3);
			}
			$output['is_alone_article'] 	= $is_alone_article;
		}
		$involved_customers = '';
		foreach ($customers_involved as $key => $value) {
			$involved_customers.=$value.',';
		}
		$involved_customers = rtrim($involved_customers,',');
		if(!$involved_customers){
			$involved_customers = 'empty';
		}
		$in['customers_involved'] 		= $involved_customers;
		$output['is_data'] 			= $is_data;
		$output['PICK_DATE_FORMAT']		=  pick_date_format();
		$output['from_ts']			= $in['from'] ? date(ACCOUNT_DATE_FORMAT,$in['from']) : '';
		$output['to_ts']				= $in['to'] ? date(ACCOUNT_DATE_FORMAT,$in['to']) : '';
		$output['lid']				= $this->db->field("SELECT lang_id FROM pim_lang WHERE default_lang='1' ");
		$output['show_approved'] 		= $in['show_approved'];
		$output['customers_list']		= $this->get_customers($in);
		$output['pdf_link']			='index.php?do=project-report_all_pdf&lid='.$output['lid'].'&from='.$in['from'].'&to='.$in['to'].'&customer_id='.$output['customer_id'].'&show_approved='.$in['show_approved'];
		$output['excel_link']			='index.php?do=project-report_all_xls&lid='.$output['lid'].'&from='.$in['from'].'&to='.$in['to'].'&customer_id='.$output['customer_id'].'&show_approved='.$in['show_approved'];

		$this->out = $output;
	}

	function get_customers(&$in){
		$q = strtolower($in["term"]);

		$filter =" active=1 AND is_admin='0' ";
		if($in['internal']){
			$filter =" is_admin='0' OR is_admin>0 ";
		}
		if($in['internal_only']==1){
			$filter =" is_admin>0 ";
		}
		if(!$in['search_by'] || $in['search_by']=='name' ){
			$filter .=" AND name LIKE '%".$q."%'";
		}else{
			$filter .=" AND company_number LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_id !='".$in['current_id']."'";
		}
		if($in['supplier']){
			$filter .= " AND supplier ='1'";
		}
		if($in['customers_involved']){
			if($in['customers_involved']=='empty'){// ca sa nu scoata nici un customer
				$filter .= " AND 1=2 ";
			}else{
				$filter .= " AND customer_id IN (".$in['customers_involved'].")";
			}	
		}

		$this->db->query("SELECT name,customer_id,company_number,cat_id as price_category_id  FROM customers WHERE $filter ORDER BY name ");
		while($this->db->move_next()){
			if(!$in['view_company_number']){
			   $items[$this->db->f('name')]=$this->db->f('customer_id');
			}else{
			   $items[$this->db->f('name').' - '.$this->db->f('company_number')]=$this->db->f('customer_id');
			   $price_category[$this->db->f('name').' - '.$this->db->f('company_number')]=$this->db->f('price_category_id');
			}
		}
		$result = array();
		foreach ($items as $key=>$value) {
			$price_category_id=$price_category[$key];
			if($q){
				if (strpos(strtolower($key), $q) !== false) {
					array_push($result, array("id"=>$value, "label"=>$key, "value"=> strip_tags($key), "name" => strip_tags($key),"price_category_id"=>$price_category_id));
				}
			}else{
				array_push($result, array("id"=>$value, "label"=>$key, "value"=> strip_tags($key), "name" => strip_tags($key),"price_category_id"=>$price_category_id));
			}
		}
		return $result;
	}

}

	$project_invoice = new ProjectToBeInvoiced($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $project_invoice->output($project_invoice->$fname($in));
	}

	$project_invoice->getData();
	$project_invoice->output();

?>