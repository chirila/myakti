<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');
$in['stock_location_ids'] ='';
if($_SESSION['add_to_export']){
    foreach ($_SESSION['add_to_export'] as $key => $value) {
        $in['stock_location_ids'] .= $key.',';
    }
}
if(isset($_SESSION['tmp_add_to_export'])){
    unset($_SESSION['tmp_add_to_export']);
}
if(isset($_SESSION['add_to_export'])){
    unset($_SESSION['add_to_export']);
}
$in['stock_location_ids'] = rtrim($in['stock_location_ids'],',');
if(!$in['stock_location_ids']){
    $in['stock_location_ids']= '0';
}
$headers = array("DEPOT/CUSTOMER",
    "CODE PRODUCT",
    "INTERNAL NAME",
    "CATEGORY",
    "STOCK",
    "MINIMAL THRESHOLD"
);
$db = new sqldb();
$filename="export_stock_on_location.csv";
$final_data=array();
$stock_location_data=$db->query("SELECT dispatch_stock_address.naming,pim_articles.item_code,pim_articles.internal_name,pim_articles.article_category,dispatch_stock.stock,pim_articles.article_threshold_value FROM dispatch_stock 
                            LEFT JOIN dispatch_stock_address ON dispatch_stock.address_id=dispatch_stock_address.address_id 
                            LEFT JOIN pim_articles ON dispatch_stock.article_id=pim_articles.article_id 
                            WHERE dispatch_stock.address_id IN (".$in['stock_location_ids'].") ORDER BY dispatch_stock.address_id");

while($stock_location_data->next()){
    $tmp_line=array(
        stripslashes($stock_location_data->f('naming')),
        stripslashes($stock_location_data->f('item_code')),
        stripslashes($stock_location_data->f('internal_name')),
        stripslashes($stock_location_data->f('article_category')),
        display_number_exclude_thousand($stock_location_data->f('stock')),
        display_number_exclude_thousand($stock_location_data->f('article_threshold_value'))
    );
    array_push($final_data,$tmp_line);
}
 header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');

	//$from_location=INSTALLPATH.'upload/'.DATABASE_NAME.'/tmp_export_purchase_orders.csv';
    $fp = fopen("php://output", 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    doQueryLog();
    exit();
    foreach(range('A','W') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
    }

    $objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
    $rows_format='A1:A'.$xlsRow;
    $objPHPExcel->getActiveSheet()->getStyle($rows_format)
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    $objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('upload/'.$filename);





    define('ALLOWED_REFERRER', '');
    define('BASE_DIR','upload/');
    define('LOG_DOWNLOADS',false);
    define('LOG_FILE','downloads.log');
    $allowed_ext = array (

        // archives
        'zip' => 'application/zip',

        // documents
        'pdf' => 'application/pdf',
        'doc' => 'application/msword',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'csv' => 'text/csv',

        // executables
        //'exe' => 'application/octet-stream',

        // images
        'gif' => 'image/gif',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',

        // audio
        'mp3' => 'audio/mpeg',
        'wav' => 'audio/x-wav',

        // video
        'mpeg' => 'video/mpeg',
        'mpg' => 'video/mpeg',
        'mpe' => 'video/mpeg',
        'mov' => 'video/quicktime',
        'avi' => 'video/x-msvideo'
    );
    ####################################################################
    ###  DO NOT CHANGE BELOW
    ####################################################################

    // If hotlinking not allowed then make hackers think there are some server problems
    if (ALLOWED_REFERRER !== ''
        && (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
    ) {
        die("Internal server error. Please contact system administrator.");
    }
    $fname = basename($filename);
    function find_file ($dirname, $fname, &$file_path) {
        $dir = opendir($dirname);
        while ($file = readdir($dir)) {
            if (empty($file_path) && $file != '.' && $file != '..') {
                if (is_dir($dirname.'/'.$file)) {
                    find_file($dirname.'/'.$file, $fname, $file_path);
                }
                else {
                    if (file_exists($dirname.'/'.$fname)) {
                        $file_path = $dirname.'/'.$fname;
                        return;
                    }
                }
            }
        }

    } // find_file
    // get full file path (including subfolders)
    $file_path = '';
    find_file('upload', $fname, $file_path);
    if (!is_file($file_path)) {
        die("File does not exist. Make sure you specified correct file name.");
    }
    // file size in bytes
    $fsize = filesize($file_path);
    // file extension
    $fext = strtolower(substr(strrchr($fname,"."),1));
    // check if allowed extension
    if (!array_key_exists($fext, $allowed_ext)) {
        die("Not allowed file type.");
    }
    // get mime type
    if ($allowed_ext[$fext] == '') {
        $mtype = '';
        // mime type is not set, get from server settings
        if (function_exists('mime_content_type')) {
            $mtype = mime_content_type($file_path);
        }
        else if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME); // return mime type
            $mtype = finfo_file($finfo, $file_path);
            finfo_close($finfo);
        }
        if ($mtype == '') {
            $mtype = "application/force-download";
        }
    }
    else {
        // get mime type defined by admin
        $mtype = $allowed_ext[$fext];
    }
    doQueryLog();
    // set headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Type: $mtype");
    header("Content-Disposition: attachment; filename=\"$fname\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $fsize);
    // download
    //@readfile($file_path);
    $file = @fopen($file_path,"rb");
    if ($file) {
        while(!feof($file)) {
            print(fread($file, 1024*8));
            flush();
            if (connection_status()!=0) {
                @fclose($file);

                die();
            }
        }
        @fclose($file);
    }