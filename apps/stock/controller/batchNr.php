<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$is_data = false;
$batch = array('b_n_row'=>array());

if(($in['stock_disp_id'] && $in['stock_disp_articles_id']) && ($in['count_quantity_nr'] > 0)){
	$article_id = $db->field("SELECT article_id FROM pim_stock_disp_articles WHERE stock_disp_articles_id = '".$in['stock_disp_articles_id']."' ");

	$disp_info=$db->query("SELECT pim_stock_disp.* FROM pim_stock_disp WHERE pim_stock_disp.stock_disp_id='".$in['stock_disp_id']."'");

	
	$batch_number_data = $db->query("SELECT * FROM batches WHERE article_id = '".$article_id."'  AND status_id = '1' AND in_stock > '0' ORDER BY in_stock ASC ");

	/*var_dump($disp_info->f('from_customer_id'),$disp_info->f('to_customer_id'),$disp_info->f('from_type'),$disp_info->f('to_type'),$disp_info->f('from_address_id'),$disp_info->f('to_address_id') );exit();*/
	$i=0;

	$b_n_q_selected = 0;
	$b_n_q_selected_p = 0;
	$rest = $in['count_quantity_nr'];
	$rest_p = 0;

	$selected_quantity = 0;
	$selected_quantity_p = 0;
	$selected_batches = 0;
	$selected_batches_p = 0;

	$from_customer_id =$disp_info->f('from_customer_id');
	$to_customer_id =$disp_info->f('to_customer_id');
	$from_type=$disp_info->f('from_type');
	$to_type =$disp_info->f('to_type');
	$from_address_id=$disp_info->f('from_address_id');
	$to_address_id=$disp_info->f('to_address_id');


	while($batch_number_data->next()){
		if($from_type =='1' && $from_address_id){
			$in_stock_from_address =  $db->field("SELECT quantity FROM batches_dispatch_stock WHERE address_id='".$from_address_id."' AND article_id='".$article_id."' and batch_id='".$batch_number_data->f('id')."'"); 
		}elseif($from_type =='2' && $from_customer_id){
			$in_stock_from_address =  $db->field("SELECT quantity FROM batches_dispatch_stock WHERE customer_id='".$from_customer_id."' AND article_id='".$article_id."' and batch_id='".$batch_number_data->f('id')."'"); 
		}else{
			$in_stock_from_address = $batch_number_data->f('in_stock');
		}
		

		if( $rest > $in_stock_from_address ){
			$b_n_q_selected = $in_stock_from_address;
			$rest = $rest - $in_stock_from_address;
		}elseif($rest<=$in_stock_from_address && $rest != 0 ){
			$b_n_q_selected = $rest;
			$rest = 0;
		}else{
			$b_n_q_selected = $rest;
			$rest = 0;
		}

		$selected_quantity += $b_n_q_selected;
		if($b_n_q_selected>0){
			$selected_batches++;
		}

		array_push($batch['b_n_row'],array(
			'stock_disp_articles_id'	=> $in['stock_disp_articles_id'],
			'article_id'				=> $article_id,
	
			'batch_no_checked'			=> $b_n_q_selected > 0 ? true : false,
			'batch_checked'				=> $b_n_q_selected > 0 ? 'active' : '',
			'batch_number'				=> $batch_number_data->f('batch_number'),
			'in_stock'					=> display_number($in_stock_from_address),
			'b_n_q_selected'			=> display_number($b_n_q_selected),
			'b_n_id'					=> $batch_number_data->f('id'),
			'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
			'is_error'					=> false,
		));
		$i++;
	}

	$batch['is_delivery_id']			= true;
	$batch['page_title']				= gm("Select batches");

	$batch['stock_disp_articles_id']			= $in['stock_disp_articles_id'];
	$batch['total_b_n']							= display_number($selected_quantity);
	$batch['selected_b_n']						= display_number($in['count_quantity_nr']);
	$batch['green']								= $selected_quantity == $in['count_quantity_nr'] ? 'green' : '';
	$batch['selected_batches']					= $selected_batches;

	if($in['delivery_id']){
		if($in['delivery_approved'] !=1){
			$batch['hide_if_delivery']		= false;
			$batch['column']							= '12';
			$batch['view_sn']						= 'view_sn';
			$batch['not_delivery_id']		= false;
			$sel_serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND delivery_id = '".$in['delivery_id']."' ORDER BY serial_number ASC ");
			while($sel_serial_numbers->next()){
				array_push($batch['sel_serial_nr_row'],array(
					'stock_disp_articles_id'			=> $in['stock_disp_articles_id'],
					'serial_nr_id'	=> $sel_serial_numbers->f('id'),
					'serial_number'	=> $sel_serial_numbers->f('serial_number'),
				));
			}
		}
	}

}
if($i>0){
	$is_data = true;
}

$batch['is_data']				= $is_data;

json_out($batch);
?>