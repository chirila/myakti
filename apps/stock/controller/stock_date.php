<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db = new sqldb();
$db2 = new sqldb();

$l_r = 30;
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$order_by = " ORDER BY  pim_articles.item_code  ";
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

$current_item_stock = 0;
$items_stock_at_date = 0;
$stock_value_at_date = 0;

$arguments = 'stock_type=4';


if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}


//FILTER LIST
$selected =array();
$filter = '1=1';
$filter.= " AND pim_articles.active='1' ";




if(!empty($in['search'])){
	$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_article_categories.name LIKE'%".$in['search']."%') ";
	$arguments_s.="&search=".$in['search'];
}

if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}

$join="";
$join_cols="";
if($in['start_date']) {
	
	$stop_date= time();
	//$filter.=" and tblinvoice.invoice_date BETWEEN '".$in['start_date']."' and '".$stop_date."' ";
	//$filter.=" and cast(pim_orders.date as signed) > '".$in['start_date']."'";
//list($d, $m, $y) = explode($spliter,  $in['start_date']);
	$start_date= $in['start_date'];

	$arguments.="&start_date=".$in['start_date'];
	//$view_list->assign('start_date',$in['start_date']);
	$join.="LEFT JOIN stock_movements AS stock_move ON stock_move.article_id=pim_articles.article_id AND
                stock_move.movement_id = (SELECT stock_movements.movement_id FROM stock_movements where stock_movements.date<'".$start_date."' and stock_movements.article_id = pim_articles.article_id ORDER BY stock_movements.date DESC LIMIT 1)";
    $join_cols.=",stock_move.new_stock";

}

$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

if(!empty($in['order_by'])){
	
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
			    $order = " DESC ";
			}
			if($in['order_by']=='stock_at_date' || $in['order_by']=='stock_value'){
				$order_by='';
				$filter_limit =' ';
			}else{

				$order_by =" ORDER BY ".$in['order_by']." ".$order;
				$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
				$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			}

	    
	}


$arguments = $arguments.$arguments_s;

/*$art = $db->query("SELECT pim_articles.internal_name,pim_articles.article_id, pim_articles.hide_stock, pim_articles.stock, pim_article_prices.purchase_price,
			stock_movements.new_stock,stock_movements.date
            FROM pim_articles
			INNER JOIN pim_articles_lang ON pim_articles.article_id=pim_articles_lang.item_id
			
			LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.price_category_id=0
			LEFT JOIN stock_movements ON  stock_movements.article_id=pim_articles.article_id AND 
				stock_movements.date = (SELECT MAX(stock_movements.date) FROM stock_movements where stock_movements.date<'".$start_date."' and stock_movements.article_id = pim_articles.article_id) 
			LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			WHERE $filter AND pim_articles.hide_stock=0 AND pim_articles_lang.lang_id={$_SESSION['lang_id']}  
			GROUP BY pim_articles.article_id
		   ".$order_by."			
			");*/
$art = $db->query("SELECT pim_articles.internal_name, pim_articles.item_code, pim_articles.article_id, pim_articles.hide_stock, pim_articles.stock, pim_articles.is_service, pim_article_prices.purchase_price".$join_cols."
            FROM pim_articles
			LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.price_category_id=0
			".$join." 
			LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			WHERE $filter AND pim_articles.hide_stock=0 AND pim_articles.is_service='0'
			GROUP BY pim_articles.article_id
		   ".$order_by);

$max_rows = $art->records_count();
$total_stock_value=0;
$total_stock =0;
$total_stock_at_date =0;
$stock_value1 = 0;
$stock_date=0;
$result = array('query'=>array(),'max_rows'=>$max_rows);
while($art->move_next()){
	 if($start_date){
		  	//$stock_at_date=$db2->field("SELECT new_stock FROM stock_movements WHERE article_id='".$art->f('article_id')."' AND `date`< '".$start_date."' ORDER BY date DESC  limit 1");    
	 		//$stock_at_date=$art->f('new_stock');
	 		$stock_at_date=is_null($art->f('new_stock')) ? $art->f('stock') :$art->f('new_stock');
		  }else{
		  	 $stock_at_date=$art->f('stock');
		  }

		  //$stock_value += $stock_at_date * $art->f('purchase_price');
		  $total_stock +=$art->f('stock');
		  $total_stock_at_date +=$stock_at_date;
		  $stock_value1 = $stock_at_date * $art->f('purchase_price');

		  if($stock_at_date>0){
		  	$stock_date +=$stock_value1; 
		  }
		  if($art->f('stock')>0){
		  	$total_stock_value += $art->f('stock')*$art->f('purchase_price');
		  }
	}

$articles = $db->query("SELECT pim_articles.article_id, pim_articles.hide_stock,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,			                   
			                   pim_articles.internal_name,pim_article_categories.name as article_category, pim_articles.price, pim_articles.is_service, pim_article_prices.purchase_price".$join_cols."
            FROM pim_articles
			INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id			
			
			LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.price_category_id=0
			LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			".$join."
			WHERE $filter AND pim_articles.hide_stock=0 AND pim_articles.is_service='0' AND pim_articles_lang.lang_id={$_SESSION['lang_id']} 
			GROUP BY pim_articles.article_id
		   ".$order_by.$filter_limit
			);

$j=0; 
while($db->move_next()){
     
//if we have a date we have to select the stock ad that date base on stock movemtns
 
  if($start_date){
  	 //$stock_at_date=$db2->field("SELECT new_stock FROM stock_movements WHERE article_id='".$db->f('article_id')."' AND `date`< '".$start_date."' ORDER BY date DESC  limit 1"); 
  	 $stock_at_date= is_null($db->f('new_stock')) ? $db->f('stock') :$db->f('new_stock');   
  }else{
  	 $stock_at_date=$db->f('stock');
  }
  
  $stock_value = $stock_at_date * $db->f('purchase_price');
  	
	$item=array(
				'name'		  		=> htmlspecialchars_decode($db->f('internal_name')),
				'article_category'	=> $db->f('article_category'),
				'article_id'  		=> $db->f('article_id'),
				'stock'  			=> remove_zero_decimals($db->f('stock')),
				'stock_at_date'  	=> remove_zero_decimals($stock_at_date),
			    'stock_value'		=> $stock_value>0 ? place_currency(display_number($stock_value)): place_currency(display_number(0)),
			    'stock_at_date_ord'  	=> $stock_at_date,
			    'stock_value_ord'		=> $stock_value>0 ? $stock_value: 0,
			
				'threshold'	  		=> $db->f('article_threshold_value'),
				'code'		  		=> $db->f('item_code'),
				'allow_stock'                => ALLOW_STOCK,
				'stock_multiple_locations'   => STOCK_MULTIPLE_LOCATIONS
         );


	 array_push($result['query'], $item);
	 
	$j++;
}

	$result['current_item_stock']   = display_number($total_stock);
	$result['items_stock_at_date']  = display_number($total_stock_at_date);
	$result['stock_value_at_date']  = display_number($stock_date);
	$result['current_value_of_stock']  = display_number($total_stock_value);

	$result['article_no']           = $max_rows;
	$result['total_stock_value']	= place_currency(display_number($total_stock_value));
	$result['horus_db']			   	= (DATABASE_NAME=='74997702_76b4_759b_c140a8d1b73b' || DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11')? true:false;
	$result['EXPORT_HREF']			= 'index.php?do=stock-export_articles_stock_on_date&search='.$in['search'].'&start_date='.$in['start_date'];
	$result['lr']           		= $l_r;

	$result['export_args']  = ltrim($arguments,'&');


	if($in['order_by']=='stock_value' ||  $in['order_by']=='stock_at_date'){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
	    }
	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	    
	}


json_out($result);