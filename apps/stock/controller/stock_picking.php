<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


global $config;
$l_r =ROW_PER_PAGE;
$db2 = new sqldb();


if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}



$filter = 'WHERE 1=1 ';
$filter_link = 'block';
$order_by = " ORDER BY planeddate DESC, iii DESC ";

/*if(!$in['archived']){
	$filter.= " AND pim_stock_disp.active=1";
}else{
	$filter.= " AND pim_stock_disp.active=0";
	$arguments.="&archived=".$in['archived'];
}*/


if($in['search']){
	$filter.=" and (servicing_support.customer_name like '%".$in['search']."%' OR servicing_support.serial_number like '%".$in['search']."%')";
}
if($in['location']){
	$filter.=" and (service_reservation.location_id = '".$in['location']."') ";
}
if($in['order_by']){
	$order = " ASC ";
	if($in['desc']== '1' || $in['desc']=='true'){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
}

/*if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
if($in['start_date'] && $in['stop_date']){
	$filter.=" and pim_stock_disp.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if($in['start_date']){
	$filter.=" and cast(pim_stock_disp.date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if($in['stop_date']){
	$filter.=" and cast(pim_stock_disp.date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}*/
$filter2 ='';
switch ($in['view']) {
		case '1':
			$filter2 .="  HAVING SUM(service_reservation.picked_quantity) =0 ";
			break;
		 case '2':
			$filter2 .="  HAVING (SUM(service_reservation.quantity - service_reservation.picked_quantity)>0 AND SUM(service_reservation.picked_quantity) >0) ";
			break;
	    case '3':
			$filter2 .="  HAVING SUM(service_reservation.quantity - service_reservation.picked_quantity)=0 ";
			break;	
		default:
			break;
	}	

$locations = build_picking_location_dd();

$db->query("SELECT service_reservation.id, service_reservation.service_id, service_reservation.reservation_id, service_reservation.a_id, service_reservation.location_id, serial_number as iii, planeddate, cast( FROM_UNIXTIME( `planeddate` ) AS DATE ) AS planned_date, SUM(service_reservation.quantity) as quantity,SUM(service_reservation.picked_quantity) as picked_quantity,servicing_support.customer_name, dispatch_stock_address.naming, (SUM(service_reservation.quantity)-SUM(service_reservation.picked_quantity)) as quantity_diff
			FROM service_reservation 
			LEFT JOIN servicing_support ON service_reservation.service_id = servicing_support.service_id 
			LEFT JOIN dispatch_stock_address ON service_reservation.location_id = dispatch_stock_address.address_id ".$filter."
			GROUP BY service_reservation.location_id,service_reservation.service_id  ".$filter2.$order_by );

$max_rows=$db->records_count();
$db->move_to($offset*$l_r);
$j=0;
$color = '';
global $config;
$result = array('query'=>array(),'max_rows'=>$max_rows,'locations'=>$locations);
while($db->move_next() && $j<$l_r){

	if($db->f('quantity')-$db->f('picked_quantity')>0 && $db->f('picked_quantity')>0){
		$status2 = gm('Partially picked');
		$status ='orange';
	}elseif($db->f('quantity')-$db->f('picked_quantity')==0){
		$status2 = gm('Picked');
		$status ='green';
	}else{
		$status2 = gm('To be picked');
		$status ='grey';
	}
		

	$item=array(	
		'id'                	=> $db->f('id'),
		'service_id'            => $db->f('service_id'),
		'location_id'           => $db->f('location_id'),
		'serial_number'     	=> $db->f('iii'),
		'planned_date'         	=> $db->f('planeddate')? date(ACCOUNT_DATE_FORMAT,$db->f('planeddate')): '',
		'customer_name'         => $db->f('customer_name'),
		'stock_location'		=> $db->f('naming'),
		'reserved_qty'     		=> $db->f('quantity'),
		'picked_qty'     		=> $db->f('picked_quantity'),
		'status2'				=> $status2,
		 'status'				=> $status,
		);
	
 array_push($result['query'], $item);

	$j++;
}

$result['lr']		= $l_r;

json_out($result);