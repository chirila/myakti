<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

if(!defined('BASEPATH')) exit('No direct script access allowed');

$result=array('articles'=>array());
$is_data = false;
$db2=new sqldb();
if($in['order_id']){
	$articles = $db->query("SELECT pim_order_articles.* ,SUM(pim_orders_delivery.quantity) as q_deliver
		                    FROM  pim_order_articles  
		                    LEFT JOIN  pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id 
		                    WHERE pim_order_articles.order_id='".$in['order_id']."'
		                    AND pim_order_articles.article_id!=0
		                    GROUP BY pim_order_articles.`order_articles_id`");
	while ( $articles->move_next()) {
		if($articles->f('quantity')-$articles->f('q_deliver')!=0){
		$article_row=array(
			'article_code'		=> $articles->f('article_code'),
			'article'			=> $articles->f('article'),
			'quantity'         => $articles->f('quantity')-$articles->f('q_deliver'),
			);
		
        array_push($result['articles'],$article_row); 
		$is_data=true;
	  }
	}
}

if($in['project_id']){
	$articles = $db->query("SELECT project_articles.* ,SUM(service_delivery.quantity) as q_deliver
		                    FROM  project_articles  
		                    LEFT JOIN service_delivery ON project_articles.article_id=service_delivery.a_id and  project_articles.project_id=service_delivery.project_id
		                    WHERE project_articles.project_id='".$in['project_id']."' AND project_articles.article_id!=0
		                    
		                    GROUP BY project_articles.`a_id`");
	while ( $articles->move_next()) {
	  if($articles->f('quantity')-$articles->f('q_deliver')!=0){
		$article_row=array(
			'article_code'		=> $db2->field("SELECT item_code FROM pim_articles WHERE article_id='".$articles->f('article_id')."'"),
			'article'			=> $articles->f('name'),
			'quantity'         => $articles->f('quantity')-$articles->f('q_deliver'),
			);
		
        array_push($result['articles'],$article_row); 
		$is_data=true;
	  }
	}
}

if($in['intervention_id']){
	$articles = $db->query("SELECT servicing_support_articles.* ,SUM(service_delivery.quantity) as q_deliver
		                    FROM  servicing_support_articles  
		                    LEFT JOIN service_delivery ON servicing_support_articles.article_id=service_delivery.a_id and  servicing_support_articles.service_id=service_delivery.service_id
                              WHERE     servicing_support_articles.service_id='".$in['intervention_id']."' AND servicing_support_articles.article_id!=0
		                    
		                    GROUP BY servicing_support_articles.`a_id`");
	while ( $articles->move_next()) {
	  if($articles->f('quantity')-$articles->f('q_deliver')!=0){
		$article_row=array(
			'article_code'		=> $db2->field("SELECT item_code FROM pim_articles WHERE article_id='".$articles->f('article_id')."'"),
			'article'			=> $articles->f('name'),
			'quantity'         => $articles->f('quantity')-$articles->f('q_deliver'),
			);
		
        array_push($result['articles'],$article_row); 
		$is_data=true;
	  }
	}
}



	$result['is_data']			= $is_data;
	


json_out($result);
?>