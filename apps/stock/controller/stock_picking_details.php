<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result = array('query'=>array());

$filter = "";

if(!($in['service_id'] || $in['location_id']) ){
	json_out($result);
}

$stock_picking_details = $db->query("SELECT service_reservation.*, serial_number as iii, planeddate, cast( FROM_UNIXTIME( `planeddate` ) AS DATE ) AS planned_date, servicing_support.customer_name, servicing_support.customer_id, dispatch_stock_address.naming, servicing_support_articles.name
			FROM service_reservation 
			LEFT JOIN servicing_support ON service_reservation.service_id = servicing_support.service_id 
			LEFT JOIN dispatch_stock_address ON service_reservation.location_id = dispatch_stock_address.address_id 
			LEFT JOIN servicing_support_articles ON service_reservation.a_id = servicing_support_articles.article_id 
			WHERE service_reservation.service_id='".$in['service_id']."' AND service_reservation.location_id='".$in['location_id']."' 
			GROUP BY service_reservation.id");

while($stock_picking_details->next()){
	$location = $stock_picking_details->f('naming'); 
	$item= array(
		'customer_name'			=> $stock_picking_details->f('customer_name'),
		'customer_id'			=> $stock_picking_details->f('customer_id'),
		'article_name'			=> $stock_picking_details->f('name'),
		'a_id'					=> $stock_picking_details->f('a_id'),
		'quantity'				=> display_number($stock_picking_details->f('quantity')),
		'picked_quantity'		=> display_number($stock_picking_details->f('picked_quantity')),
		'reservation_id'		=> $stock_picking_details->f('reservation_id'),
		
	);	
	array_push($result['query'], $item);
}

$result['service_id']=$in['service_id'];
$result['location_id']=$in['location_id'];
$result['page_title']=gm('Order picking').' - '.$location;
json_out($result);
?>