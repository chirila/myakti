<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}


$db = new sqldb();
$db2 = new sqldb();

$l_r = 30;
$order_by = " ORDER BY  pim_articles.item_code  ";
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

$arguments = 'stock_type=2';
$arguments_location = 'stock_type=5';

if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}




//FILTER LIST
$selected =array();
$filter = " pim_articles.active='1' ";
$filter_attach = '';

$filter_po2 = ' ';
$filter_po = ' LEFT JOIN pim_p_order_deliveries ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id ';
if(P_ORDER_DELIVERY_STEPS == '2'){
    $filter_po2 = " AND pim_p_order_deliveries.delivery_done ='1'  ";
}

if(!empty($in['search'])){
	$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
	$arguments_s.="&search=".$in['search'];
}

if($in['article_category_id']){	
	$filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";	 
	$arguments.="&article_category_id=".$in['article_category_id'];
}

$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	if(!empty($in['order_by'])){
	 
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
			    $order = " DESC ";
			}
			if($in['order_by']=='virtual_stock'){
				$order_by='';
				$filter_limit =' ';
			}else{
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
				$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
			}
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			$arguments_location.="&order_by=".$in['order_by']."&desc=".$in['desc'];

	   
	}

$arguments = $arguments.$arguments_s;

$arguments_location = $arguments_location .$arguments_s;

$db->query("SELECT pim_articles.internal_name,pim_articles.hide_stock
            FROM pim_articles
			INNER JOIN pim_articles_lang ON pim_articles.article_id=pim_articles_lang.item_id
			$filter_attach
			
			LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			WHERE $filter AND pim_articles.is_service='0' AND pim_articles.hide_stock=0 AND pim_articles_lang.lang_id={$_SESSION['lang_id']}  
			GROUP BY pim_articles.article_id
		   ".$order_by."			
			");

$max_rows=$db->records_count();
while($db->next()){
	if(!in_array(substr(strtoupper($db->f('internal_name')),0,1), $selected)){

	   array_push($selected, substr(strtoupper($db->f('internal_name')),0,1));
  }
}

$stock_value= $db->field("SELECT sum(pim_articles.stock * pim_article_prices.purchase_price)
                                FROM pim_articles
                                INNER JOIN pim_articles_lang ON pim_articles.article_id=pim_articles_lang.item_id
                                LEFT JOIN pim_article_prices ON pim_article_prices.article_id = pim_articles.article_id AND pim_article_prices.price_category_id=0
                                WHERE pim_articles.stock>0 AND pim_articles.active='1' AND pim_articles.hide_stock=0 AND pim_articles_lang.lang_id={$_SESSION['lang_id']}");

$articles = $db->query("SELECT pim_articles.article_id,pim_articles.hide_stock,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,			                   
			                   pim_articles.internal_name,pim_article_categories.name as article_category
            FROM pim_articles
			INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id			
			$filter_attach
			
			LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
			WHERE $filter AND pim_articles.is_service='0' AND pim_articles.hide_stock=0  AND pim_articles_lang.lang_id={$_SESSION['lang_id']} 
			GROUP BY pim_articles.article_id
		   ".$order_by.$filter_limit);



$j=0;
$result = array('query'=>array(),'max_rows'=>$max_rows);
while($db->move_next()){
    

    
    
    /*$items_order=$db2->field("SELECT SUM(quantity) FROM  pim_p_order_articles WHERE article_id='".$db->f('article_id')."'");
	$items_received=$db2->field("SELECT SUM(pim_p_orders_delivery.quantity) 
		                         FROM   pim_p_orders_delivery 
		                         INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
		                         WHERE pim_p_order_articles.article_id='".$db->f('article_id')."'");*/

    $qty_orders_p=0;
    $qty_ord_p = $db2->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order, COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_p_order_deliveries.delivery_done = '1' THEN pim_p_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_p_orders_delivery.delivery_id, pim_p_order_articles.p_order_id, pim_p_order_articles.sale_unit, pim_p_order_articles.packing
	      FROM pim_p_order_articles
	      LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
	      LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
	      $filter_po
	      WHERE pim_p_order_articles.article_id =  '".$db->f('article_id')."'
	       AND pim_p_orders.rdy_invoice!=0 

	    GROUP BY pim_p_orders.p_order_id");

	$total_deliver=0;
	while($qty_ord_p->move_next()) {
	    $stock_packing_ord_p = $db->f("packing");

	    if(!ALLOW_ARTICLE_PACKING || !(float)$stock_packing_ord_p){
	       $stock_packing_ord_p = 1;
	    }

	    $stock_sale_unit_ord_p = $db->f("sale_unit");

	    if(!ALLOW_ARTICLE_SALE_UNIT || !(float)$stock_sale_unit_ord_p){
	       $stock_sale_unit_ord_p = 1;
	    }  

	  	$total_del=0;
	  	if(P_ORDER_DELIVERY_STEPS == '2'){
	      $total_del = $qty_ord_p->f('total_deliver2');
	   	}else{
	      $total_del = $qty_ord_p->f('total_deliver');
	    }
	  	if($qty_ord_p->f('nr_del')){
	        if($qty_ord_p->f('total_order')/$qty_ord_p->f('nr_del') < $total_del){
	            $total_deliver = ($qty_ord_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$qty_ord_p->f('nr_del');
	        }else{
	            $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;                
	        }

	        $qty_orders_p +=(($qty_ord_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$qty_ord_p->f('nr_del') - $total_deliver); 
	    }else{
	        $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;

	        $qty_orders_p +=($qty_ord_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p - $total_deliver); 
	    }  
	}

	$qty_diff_p=0;
	$diff_p = $db2->query("SELECT pim_p_order_articles.order_articles_id, SUM( pim_p_order_articles.quantity ) as total_order ,COUNT(distinct pim_p_orders_delivery.delivery_id) as nr_del , pim_p_order_articles.article_id, SUM( pim_p_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_p_order_deliveries.delivery_done = '1' THEN pim_p_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_p_orders_delivery.delivery_id, pim_p_order_articles.p_order_id,pim_p_order_articles.sale_unit,pim_p_order_articles.packing
	      FROM pim_p_order_articles
	      LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id = pim_p_orders_delivery.order_articles_id
	      LEFT JOIN pim_p_orders ON pim_p_order_articles.p_order_id = pim_p_orders.p_order_id
	      $filter_po
	      WHERE pim_p_order_articles.article_id =  '".$db->f('article_id')."'
	      AND pim_p_orders.sent =1
	      AND pim_p_orders.rdy_invoice =  '1' ".$filter_po2."

	      GROUP BY pim_p_orders.p_order_id");
             
      $total_deliver=0;
      while($diff_p->move_next()) {
        $stock_packing_ord_p = $db->f("packing");
        if(!ALLOW_ARTICLE_PACKING || !(float)$stock_packing_ord_p ){
            $stock_packing_ord_p = 1;
        }
          
        $stock_sale_unit_ord_p = $db->f("sale_unit");
        if(!ALLOW_ARTICLE_SALE_UNIT || !(float)$stock_sale_unit_ord_p){
             $stock_sale_unit_ord_p = 1;
        } 

        $total_del=0;
        if(P_ORDER_DELIVERY_STEPS == '2'){
            $total_del = $diff_p->f('total_deliver2');
        }else{
            $total_del = $diff_p->f('total_deliver');
        }
        if($diff_p->f('nr_del')){
          	if($diff_p->f('total_order')/$diff_p->f('nr_del') < $total_del){
              $total_deliver = ($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$diff_p->f('nr_del');
          	}else{

              $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;

          	}
          	$qty_diff_p +=(($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p)/$diff_p->f('nr_del') - $total_deliver); 
        }else{
            $total_deliver = $total_del*$stock_packing_ord_p/$stock_sale_unit_ord_p;

            $qty_diff_p +=($diff_p->f('total_order')*$stock_packing_ord_p/$stock_sale_unit_ord_p - $total_deliver); 
        }
    }
	
	//$view_list->assign(array(
	$item=array(
				'name'		  		=> htmlspecialchars_decode($db->f('internal_name')),
				'article_category'	=> $db->f('article_category'),
				'article_id'  		=> $db->f('article_id'),
				'stock'  			=> remove_zero_decimals($db->f('stock')),
			    
			
				 'threshold'	  => $db->f('article_threshold_value'),
				 //'virtual_stock'	  => $items_order-$items_received,
				 'virtual_stock'	  => $qty_orders_p-$qty_diff_p,
				
				'code'		  		=> $db->f('item_code'),
				
				'title'				=> $in['archived'] ? 'Delete' : 'Archive',
				'confirm'			=> $in['archived'] ? 'Delete this article?' : 'Archive this article?',
				'd_confirm'			=> $in['archived'] ? 'Activate this article?' : '',
				'd_title'			=> $in['archived'] ? 'Activate' : 'Duplicate',
				'd_class'			=> $in['archived'] ? 'art_del' : '',
				'd_img'				=> $in['archived'] ? 'undo' : 'duplicate',
				'allow_stock'                =>  ALLOW_STOCK == 1 ? true : false,
				'stock_multiple_locations'   => STOCK_MULTIPLE_LOCATIONS==1? true : false

			//),'articles');
				);
    array_push($result['query'], $item);
	$j++;
}


if($max_rows > $l_r){
	$is_pagination=true;
}



	$result['allow_stock']   = ALLOW_STOCK == 1 ? true : false;
	$result['stock_multiple_locations']   = STOCK_MULTIPLE_LOCATIONS == 1 ? true : false;
	$result['export_args']  = ltrim($arguments,'&');
	$result['export_args_location']  = ltrim($arguments_location,'&');
	$result['article_no']   = $max_rows;
	$result['stock_value']  = place_currency(display_number($stock_value));
	$result['horus']	    = (DATABASE_NAME=='74997702_76b4_759b_c140a8d1b73b' || DATABASE_NAME=='a2f5b012_4714_b182_c9c27f9f30ad' || DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11') ? true : false;

	if($in['order_by']=='virtual_stock'){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'], SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by'], SORT_DESC);
	    }
	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	    
	}

$result['lr']		= $l_r;



json_out($result);

function get_depo_list($in){
	$db = new sqldb();
	if(!isset($in['article_id']) || !is_numeric($in['article_id'])){
		return array();
	}
	$result = array( 'list' => array());
	

	$l_r = ROW_PER_PAGE;

	

	$depo = $db->query("SELECT dispatch_stock.*,dispatch_stock_address.*
            FROM   dispatch_stock
            INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='1'")->getAll();
	//$row = count($depo);

	$i=0;
	foreach ($depo as $key => $value) {
	
		 // $view->assign(array(
           $item = array(          'customer_name' => $value['naming'],
                     'current_quantity'      => remove_zero_decimals($value['stock']),
                     'address'=> $value['address'],
                     'city'=> $value['city'],
                     'zip'=> $value['zip'],
                     'country'=> get_country_name($value['country_id'])
                     );
                     

                //  ),'customer_row');
	

		

		array_push($result['list'], $item);



		$i++;
	}

	$depo2 = $db->query("SELECT dispatch_stock.*, customer_addresses.*,customers.name
            FROM   dispatch_stock
            INNER JOIN customers ON customers.customer_id=dispatch_stock.customer_id
            LEFT JOIN  customer_addresses ON  dispatch_stock.address_id=customer_addresses.address_id AND dispatch_stock.customer_id=customer_addresses.customer_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='0'")->getAll();
	//$row = count($depo2);

foreach ($depo2 as $key => $value) {
	
		 // $view->assign(array(
           $item = array(          'customer_name' => $value['name'],
                     'current_quantity'      => remove_zero_decimals($value['stock']),
                     'address'=> $value['address'],
                     'city'=> $value['city'],
                     'zip'=> $value['zip'],
                     'country'=> get_country_name($value['country_id'])
                     );
                     

                //  ),'customer_row');
	

		

		array_push($result['list'], $item);



		$i++;
	}


	return $result;
}