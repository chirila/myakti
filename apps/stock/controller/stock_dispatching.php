<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


global $config;
$l_r =ROW_PER_PAGE;
$db2 = new sqldb();


if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}



$filter = 'WHERE 1=1 ';
$filter_link = 'block';
$order_by = " ORDER BY t_date DESC, iii DESC ";

if(!$in['archived']){
	$filter.= " AND pim_stock_disp.active=1";
}else{
	$filter.= " AND pim_stock_disp.active=0";
	$arguments.="&archived=".$in['archived'];
}

if($in['filter']){
	$arguments.="&filter=".$in['filter'];
}
if($in['search']){
	$filter.=" and (pim_stock_disp.serial_number like '%".$in['search']."%' OR pim_stock_disp.customer_name like '%".$in['search']."%')";
	$arguments.="&search=".$in['search'];
}
if($in['order_by']){
	$order = " ASC ";
	if($in['desc'] == '1' || $in['desc']=='true'){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
}

if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
if($in['start_date'] && $in['stop_date']){
	$filter.=" and pim_stock_disp.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if($in['start_date']){
	$filter.=" and cast(pim_stock_disp.date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if($in['stop_date']){
	$filter.=" and cast(pim_stock_disp.date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}

switch ($in['view']) {
		case '1':
			$filter.=" and pim_stock_disp.rdy_invoice = '0' and pim_stock_disp.sent='0' ";
			$arguments.="&view=1";
			break;
		 case '2':
			$filter.=" and pim_stock_disp.to_type = '1'";
			$arguments.="&view=2";
			break;
	    case '3':
			$filter.=" and pim_stock_disp.to_type = '2'";
			$arguments.="&view=3";
			break;	
		default:
			break;
	}	



$arguments = $arguments.$arguments_s;

$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
$db->query("SELECT pim_stock_disp.*, serial_number as iii, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date
			FROM pim_stock_disp ".$filter.$order_by );

$max_rows=$db->records_count();
$db->move_to($offset*$l_r);
$j=0;
$color = '';
global $config;
$result = array('query'=>array(),'max_rows'=>$max_rows);
while($db->move_next() && $j<$l_r){
	$ref = '';
	if(ACCOUNT_ORDER_REF && $db->f('buyer_ref')){
		$ref = $db->f('buyer_ref');

	}	

	
	

	//$view_list->assign(array(
	$item=array(	
		
		'delete_link'  		      => array('do'=>'stock-stock_dispatching-stock_dispatch-delete','stock_disp_id'=>$db->f('stock_disp_id')),
		'undo_link' 	 		  => array('do'=>'stock-stock_dispatching-stock_dispatch-activate','stock_disp_id'=>$db->f('stock_disp_id')),
		'archive_link'  		  => array('do'=>'stock-stock_dispatching-stock_dispatch-archive','stock_disp_id'=>$db->f('stock_disp_id')),
		'id'                	=> $db->f('stock_disp_id'),
		'serial_number'     	=> $db->f('serial_number') ? $ref.$db->f('serial_number') : '',
		'created'           	=> date(ACCOUNT_DATE_FORMAT,$db->f('date')),
		'to_naming'        	    => $db->f('to_naming'),
		'from_naming'        	=> $db->f('from_naming'),
		'is_editable'			=> $db->f('rdy_invoice') ? false : true,
		'is_archived'       	=> $in['archived'] ? true : false,
		'status2'				=> $db->f('rdy_invoice') ? ($db->f('rdy_invoice')==1 ? gm('Delivered') : gm('Confirmed')) : ($db->f('sent') == 1 ? gm('Confirmed') : gm('Draft')),

		 'status'				=> $db->f('rdy_invoice') ? ($db->f('rdy_invoice')==1 ? 'green' : 'orange') : ($db->f('sent') == 1 ? 'orange' : 'gray'),

		//'path'					=> $config['site_url'],
	
		'alt_for_delete_icon'	=> $in['archived'] == 1 ? 'Delete' : 'Archive',
		'is_delivered'			=> $db->f('rdy_invoice')==1 ? true : false
		);
	//),'stock_disp_row');
 array_push($result['query'], $item);
	
	//$view_list->loop('stock_disp_row');
	$j++;
}

$result['lr']		= $l_r;
$result['view_dd'] = array( array('id'=>'1', 'value'=>gm('Draft')), array('id'=>'2', 'value'=>gm('Own Company')), array('id'=>'3', 'value'=>gm('Other Company')), array('id'=>'-1', 'value'=>gm('Archived')));

json_out($result);