<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');
/*Stock Type
 	1 - Transaction Based
	2 - Article based 
	3 - Account / Location Based
	4 - Stock on Date
	5- Article based - on main location
End Stock Type
*/
$db = new sqldb();
$db2 = new sqldb();

$my_headers = '';
$final_result = '';

//Transaction Based
if($in['stock_type'] == 1){
	$filename = 'transaction_based.csv';
	$order_by = " ORDER BY t_date DESC, serial_number DESC ";
	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

	if(($in['ofs']) || (is_numeric($in['ofs'])))
	{
		$in['offset']=$in['ofs'];
	}

	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	//FILTER LIST
	$selected =array();
	$filter = " pim_articles.active='1' AND stock_movements.article_id !='0' ";
	$filter_attach = '';

	if(!empty($in['search'])){
	$filter .= " AND ( stock_movements.item_code LIKE '%".$in['search']."%'  OR stock_movements.article_name LIKE '%".$in['search']."%'  
		OR stock_movements.serial_number LIKE '%".$in['search']."%' OR pim_articles.origin_number LIKE '%".$in['search']."%' OR pim_articles.supplier_name LIKE '%".$in['search']."%'
		OR pim_article_brands.name  LIKE '%".$in['search']."%' ) ";
	}
	if($in['order_by']){
		$order = " ASC ";
		if($in['desc']){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
	}
	if($in['view']){
		$filter .= " AND movement_type='".$in['view']."'";
	}
	if(!empty($in['start_date_js'])){
		$in['start_date'] =strtotime($in['start_date_js']);
		$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
	}
	if(!empty($in['stop_date_js'])){
		$in['stop_date'] =strtotime($in['stop_date_js']);
		$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
	}

	if($in['start_date'] && $in['stop_date']){
		
		$start_date= $in['start_date'];
		$stop_date= $in['stop_date'];
		$filter.=" AND stock_movements.date BETWEEN '".$start_date."' AND '".$stop_date."' ";
		$filter_link = 'none';
	}
	else if($in['start_date']){
		$filter.=" and cast(stock_movements.date as signed) > ".$in['start_date']." ";
	}
	else if($in['stop_date']){
		$filter.=" and cast(stock_movements.date as signed) < ".$in['stop_date']." ";
	}

		
		if($in['location_id'] && is_array($in['location_id']) && !empty($in['location_id'])){
			$location_dd = get_location_dd();
			$location_str="";
			if(count($in['location_id'])==1 && $in['location_id'][0]=='0'){
				foreach($location_dd as $key=>$value){
					$location_str.="'".$value['id']."',";
				}
			}else{
				foreach($in['location_id'] as $key=>$value){
					$location_str.="'".$value."',";
				}		
			}
			$location_str=rtrim($location_str,",");

			$all_addresses_locations = $db->query("SELECT dispatch_stock.article_id,dispatch_stock.address_id,
				 										dispatch_stock_address.location_id
													FROM dispatch_stock	
													INNER JOIN dispatch_stock_address ON dispatch_stock_address.address_id = dispatch_stock.address_id
													WHERE dispatch_stock_address.location_id IN (".$location_str.") 
													")->getAll();
			
			$article_id_loc = '';
			
			foreach($all_addresses_locations as $loc){
				if(strpos($article_id_loc, $loc['article_id']) == false){
					$article_id_loc .= "'".$loc['article_id']."',";
				}
			}

			$article_id_loc = rtrim($article_id_loc,",");

			if(!empty($article_id_loc)){
				$filter.= " AND stock_movements.article_id IN (".$article_id_loc.") ";
			} else {
				$filter.= " AND stock_movements.article_id IN (0) ";	
			}
		}

		$edit_stock_reason_data = $db->query("SELECT * FROM (SELECT stock_edit_reasons.*,  dispatch_stock_address.naming FROM stock_edit_reasons LEFT JOIN dispatch_stock_address on stock_edit_reasons.address_id = dispatch_stock_address.address_id ORDER BY stock_edit_reasons.id DESC) as stock_edit_reasons GROUP BY stock_edit_reasons.article_id")->getAll();

		$edit_stock_reason = array();
		foreach ($edit_stock_reason_data as $key => $val) {
			$edit_stock_reason[$val['article_id']][] = array('id' => $val['id'],
	                                              'article_id' => $val['article_id'],
	                                              'address_id' => $val['address_id'],
	                                              'user_id' => $val['user_id'],
	                                              'username' => stripslashes($val['username']),
	                                              'edit_reason' => stripslashes($val['edit_reason']),
	                                              'naming' => stripslashes($val['naming']),
	                                              'date'  => date(ACCOUNT_DATE_FORMAT,  $val['date'])
	                                              );
			
		}

	//depot data
	$depot_ar=array();

	$depot_data=$db->query("SELECT * FROM dispatch_stock_address")->getAll();

	foreach($depot_data as $k=>$v){
	    $depot_data[$v['address_id']]=$v['naming'];
	}

	$depot_data=null;
	unset($depot_data);
	//end depot data

	if($in['depot_id'] && is_array($in['depot_id']) && !empty($in['depot_id'])){
			$depot_str=" AND (";

			foreach($in['depot_id'] as $key=>$value){
					$depot_str.=" (stock_movements.location_info LIKE  '%-".$value."\";d:%' AND stock_movements.location_info NOT LIKE  '%-".$value."\";d:0%')  OR ";
					$depot_str.=" (stock_movements.location_info LIKE  '%-".$value."\";s:4:\"%' AND stock_movements.location_info NOT LIKE  '%-".$value."\";s:4:\"0,00%')  OR ";
				}		
			
			$depot_str=rtrim($depot_str," OR");
			$depot_str.=") ";

			if($depot_str!=" AND () "){
				$filter.= $depot_str;
			} 
	}

	if(!empty($in['depot_id'])){
		foreach($in['depot_id'] as $loc_id){
			$arguments.="&depot_id[]=".$loc_id;
		}	
	} else {
		$arguments.="&depot_id=";
	}

	$records = $db->query("SELECT DISTINCT(stock_movements.movement_id) FROM stock_movements 
						LEFT join pim_articles ON stock_movements.article_id = pim_articles.article_id
						LEFT join pim_article_brands on pim_articles.article_brand_id = pim_article_brands.id
						WHERE $filter  ");
	$max_rows=$records->records_count();

	//movement ids data
	$movement_ids_str="";

	while($records->next()){
	  $movement_ids_str.="'".$records->f('movement_id')."',";
	}
	$movement_ids_str=rtrim($movement_ids_str,",");
	//end movement ids data

	//movement_data data
	$movement_data_ar=array();

	$movement_data=$db->query("SELECT movement_id,location_info FROM stock_movements WHERE movement_id IN (".$movement_ids_str.") ")->getAll();

	foreach($movement_data as $k=>$v){
	    $movement_data_ar[$v['movement_id']]=$v['location_info'];
	}

	$movement_data=null;
	unset($movement_data);
	//end movement_data data

/*	$articles = $db->query("SELECT stock_movements.*, date AS t_date 
		                    FROM stock_movements WHERE $filter  AND stock!=new_stock  ".$order_by);*/
	$articles = $db->query("SELECT stock_movements.*, date AS t_date, pim_articles.origin_number, pim_articles.supplier_name, pim_article_brands.name 
	                    FROM stock_movements 
	                    LEFT join pim_articles ON stock_movements.article_id = pim_articles.article_id
						LEFT join pim_article_brands on pim_articles.article_brand_id = pim_article_brands.id
	                    WHERE $filter  ".$order_by." ");

	$my_headers = array(gm("Date"),
	                    gm("Selling Ref"),
	                    gm("Purchase Ref."),
	                    gm("Article Name"),
	                    gm("Article Code"),
	                    gm("Action made"),
	                    gm("Updated by"),
	                    gm("Reason"),
	                    gm("Depot"),
	                    gm("Previous Stock"),
	                    gm("Actual Stock"),
	  );

	$movements_type = array("1"=>"Article added", 
							"2"=>"Article Updated", 
							"3"=>"Delivery added", 
							"4"=>"Delivery deleted", 
							"5"=>"Order deleted", 
							"6"=>"Entry added", 
							"7"=>"Entry deleted", 
							"8"=>"Purchase order deleted",
							"9"=>"Delivery edited",
							"11"=>"Compose New Main Article",
							"12"=>"Compose To Main Article",
							"13"=>"Return added",
							"14"=>"Return updated",
							"15"=>"Return deleted",
							"10"=>"Entry edited",
						/*	"11"=>"Project article delivered",
							"12"=>"Intervention article delivered",
							"13"=>"Ticket added"*/
							"16"=>"Project article delivered",
							"17"=>"Intervention article delivered",
							"18"=>"Ticket added",
							"19"=>"Intervention return added",
							"20"=>"Intervention RMA added",
							"21"=>"Intervention DOA added");

	$j=0;
	global $config;
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	while($articles->move_next()){	
		if($articles->f('stock') < $articles->f('new_stock')){
			$stock_status = 'stock_status_up';
		}elseif($articles->f('stock') > $articles->f('new_stock')){	
			$stock_status = 'stock_status_down';
		}else{
			$stock_status = '';
		}

		$locations=array();
		$depot_arr=array();
		/*$location_info = $db->field("SELECT location_info 
		                    FROM stock_movements WHERE movement_id ='".$articles->f('movement_id')."' ");*/
		if(array_key_exists($articles->f('movement_id'), $movement_data_ar)){
					
			$locations=unserialize($movement_data_ar[$articles->f('movement_id')]);
			foreach ($locations as $key => $value) {
				$info=explode("-", $key);
				if(return_value($value)!=0.00){
				      array_push($depot_arr,$info[1]);
				    }
				}
			$depot_list ='';
			foreach ($depot_arr as $key => $value) {
					//$depot_name = $db->field("SELECT naming FROM dispatch_stock_address WHERE address_id ='".$value."' ");
					if(array_key_exists($value, $depot_ar)){
						$depot_list .= $depot_ar[$value].',';
					}					
				}
			$depot_list=rtrim($depot_list,",");

		}
	


	  $item=array('date'  			=> date("Y-m-d H:i:s",$articles->f('date')),
				'selling_reference' => ($articles->f('order_id') || $articles->f('ticket_id'))?$articles->f('serial_number'):'',
				'purchase_reference'=> $articles->f('p_order_id')?$articles->f('serial_number'):'',
				'article_name'		=> htmlspecialchars_decode(stripslashes($articles->f('article_name'))),
				'item_code'		  	=> htmlspecialchars_decode(stripslashes($articles->f('item_code'))),
				'movement_type'		=> gm($movements_type[$articles->f('movement_type')]),
				'username'			=> get_user_name($articles->f('created_by')),
				'reason'			=> '',
				'depot'				=> $depot_list,
				'stock'  			=> remove_zero_decimals($articles->f('stock')),
				'new_stock' 		=> remove_zero_decimals($articles->f('new_stock')),
			);

	    $art_id = $articles->f('order_id');
  		$p_ord_id = $articles->f('p_order_id');
  		$serial_number = $articles->f('serial_number');
  		$delivery_id = 	$articles->f('delivery_id');
  		$project_id = $articles->f('project_id');
  		$service_id = $articles->f('service_id');
  		$ticket_id = $articles->f('ticket_id');
	  
	  		if(empty($art_id) && empty($p_ord_id)  && empty($serial_number) && empty($delivery_id) && empty($project_id) && empty($service_id) && empty($ticket_id) ){
	  			foreach ($edit_stock_reason as $key => $value) {
			  		if($key == $articles->f('article_id')){
			  			$item['username'] = $value[0]['username'];
			  			$item['reason'] = $value[0]['edit_reason'];
			  			$item['depot'] = $value[0]['naming'];
			  		}
		  		}	
	  		}
	  		
	    array_push($result['query'], $item);
		$j++;
	}

	$final_result = $result['query']; 

} else if($in['stock_type'] == 2){ //Article Based

	$my_headers = array(gm("Code"),
	                    gm("Internal Name"),
	                    gm("Article Category"),
	                    gm("Stock"),
	                    gm("Items Ordered"),
	                    gm("Threshold")
	  );

	$filename = 'article_based.csv';

	$order_by = " ORDER BY  pim_articles.item_code  ";
	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

	if(($in['ofs']) || (is_numeric($in['ofs'])))
	{
		$in['offset']=$in['ofs'];
	}

	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	//FILTER LIST
	$selected =array();
	$filter = " pim_articles.active='1' ";
	$filter_attach = '';

	if(!empty($in['search'])){
		$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
	}

	if($in['article_category_id']){	
		$filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
	}

	if($in['order_by']){
		$order = " ASC ";
		if($in['desc']){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
	}

	$db->query("SELECT pim_articles.internal_name,pim_articles.hide_stock
	            FROM pim_articles
				INNER JOIN pim_articles_lang ON pim_articles.article_id=pim_articles_lang.item_id
				$filter_attach
				WHERE $filter AND pim_articles.hide_stock=0 AND pim_articles_lang.lang_id={$_SESSION['lang_id']}  
				GROUP BY pim_articles.article_id		
				");

	$max_rows=$db->records_count();

	while($db->next()){
		if(!in_array(substr(strtoupper($db->f('internal_name')),0,1), $selected)){

		   array_push($selected, substr(strtoupper(gfns($db->f('internal_name'))),0,1));
	  }
	}

	$stock_value= $db->field("SELECT sum(pim_articles.stock * pim_article_prices.purchase_price)
	                                FROM pim_articles
	                                INNER JOIN pim_articles_lang ON pim_articles.article_id=pim_articles_lang.item_id
	                                LEFT JOIN pim_article_prices ON pim_article_prices.article_id = pim_articles.article_id AND pim_article_prices.price_category_id=0
	                                WHERE pim_articles.stock>0 AND pim_articles.active='1' AND pim_articles.hide_stock=0 AND pim_articles_lang.lang_id={$_SESSION['lang_id']}");

	$articles = $db->query("SELECT pim_articles.article_id,pim_articles.hide_stock,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,			                   
				                   pim_articles.internal_name,pim_article_categories.name as article_category, SUM(pim_p_order_articles.quantity) AS article_quantity, SUM(pim_p_orders_delivery.quantity) AS delivered_quantity
	            FROM pim_articles
				INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id			
				$filter_attach
				
				LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
				LEFT JOIN pim_p_order_articles ON pim_articles.article_id=pim_p_order_articles.article_id
				LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
				WHERE $filter AND pim_articles.hide_stock=0  AND pim_articles_lang.lang_id={$_SESSION['lang_id']} 
				GROUP BY pim_articles.article_id
			   ".$order_by)->getAll();

	$j=0;
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	while($db->move_next()){
	    //$items_order=$db2->field("SELECT SUM(quantity) FROM  pim_p_order_articles WHERE article_id='".$db->f('article_id')."'");
		/*$items_received=$db2->field("SELECT SUM(pim_p_orders_delivery.quantity) 
			                         FROM   pim_p_orders_delivery 
			                         INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
			                         WHERE pim_p_order_articles.article_id='".$db->f('article_id')."'");*/
		
		$item=array('code'		  		=> htmlspecialchars_decode(stripslashes($db->f('item_code'))),
					'name'		  		=> htmlspecialchars_decode(stripslashes($db->f('internal_name'))),
					'article_category'	=> htmlspecialchars_decode(stripslashes($db->f('article_category'))),
					'stock'  			=> remove_zero_decimals($db->f('stock')),
				    'virtual_stock'	  => $db->f('article_quantity')-$db->f('delivered_quantity'),
					 'threshold'	  => $db->f('article_threshold_value')
					);
	    array_push($result['query'], $item);
		$j++;
	}

	$final_result = $result['query'];	

} else if($in['stock_type'] == 3){ // Account / Location Based

	$my_headers = array(gm("Depot/Customer"),
	                    gm("Address"),
	                    gm("Stock value"),
	                    gm("In Stock")
	  );

	$filename = 'account_location_based.csv';

	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

	if(($in['ofs']) || (is_numeric($in['ofs'])))
	{
		$in['offset']=$in['ofs'];
	}

	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	$filter = " pim_articles.active='1' ";

	if ($in['search'])
	{
		$filter .= " AND customer_name LIKE '%".$in['search']."%' ";
	}


	$order_by = " ORDER BY pim_stock_disp.customer_address_id ";

	if($in['order_by']){
		$order = " ASC ";
		if($in['desc']){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		
	}

	//first we select own location
	$customers = $db->query("SELECT dispatch_stock.*,dispatch_stock_address.*,sum(dispatch_stock.stock) as stock_at_address, sum( dispatch_stock.stock * pim_article_prices.purchase_price ) AS stock_value 
	                         FROM dispatch_stock
	                         INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
	                         LEFT JOIN pim_article_prices ON pim_article_prices.article_id = dispatch_stock.article_id
									AND pim_article_prices.base_price = '1'
	                         WHERE   dispatch_stock.main_address='1' AND dispatch_stock.stock>'0' 
	                         GROUP BY dispatch_stock.address_id
		       
		                     ");


	$j=0;
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	while($customers->move_next()){
		$item=array('naming'        => htmlspecialchars_decode(stripslashes($customers->f('naming'))),
		             'address'       => htmlspecialchars_decode(stripslashes($customers->f('address')).' '.stripslashes($customers->f('zip')).' '.stripslashes($customers->f('city')).' '.stripslashes(get_country_name($customers->f('country_id')))),
		             'stock_value'	 => get_currency_sign(display_number($customers->f('stock_value'))),
		             'in_stock'      => remove_zero_decimals($customers->f('stock_at_address')),
	     );
		array_push($result['query'], $item);
		$j++;
	 
	}
	//then we other own location
	$customers = $db->query("SELECT dispatch_stock.*, customer_addresses.*,customers.name,sum(dispatch_stock.stock) as stock_at_address, sum( dispatch_stock.stock * pim_article_prices.purchase_price ) AS stock_value 
	            FROM   dispatch_stock
	            INNER JOIN customers ON customers.customer_id=dispatch_stock.customer_id
	            LEFT JOIN  customer_addresses ON  dispatch_stock.address_id=customer_addresses.address_id AND dispatch_stock.customer_id=customer_addresses.customer_id
	             LEFT JOIN pim_article_prices ON pim_article_prices.article_id = dispatch_stock.article_id
									AND pim_article_prices.base_price = '1'
	            WHERE   dispatch_stock.main_address='0' AND dispatch_stock.stock>'0'   GROUP BY dispatch_stock.address_id
		       
		                     ");

	$j=0;

	while($customers->move_next()){
		$item=array('naming'        => htmlspecialchars_decode(stripslashes($customers->f('naming'))),
		             'address'       => htmlspecialchars_decode(stripslashes($customers->f('address')).' '.stripslashes($customers->f('zip')).' '.stripslashes($customers->f('city')).' '.stripslashes(get_country_name($customers->f('country_id')))),
		             'stock_value'	 => get_currency_sign(display_number($customers->f('stock_value'))),
		             'in_stock'      => remove_zero_decimals($customers->f('stock_at_address')),
	        );

	array_push($result['query'], $item);

	$j++;
	 
	}

	$final_result = $result['query'];
} else if($in['stock_type'] == 4){ //

	$my_headers = array(gm("Code"),
	                    gm("Internal Name"),
	                    gm("Article Category"),
	                    gm("Current Stock"),
	                    gm("Stock at date"),
	                    gm("Stock value at date")
	  );

	$filename = 'stock_on_date.csv';

	$order_by = " ORDER BY  pim_articles.item_code  ";
	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

	if(($in['ofs']) || (is_numeric($in['ofs'])))
	{
		$in['offset']=$in['ofs'];
	}

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	//FILTER LIST
	$selected =array();
	$filter = '1=1';
	$filter.= " AND pim_articles.active='1' ";

	if(!empty($in['search'])){
		$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_article_categories.name LIKE'%".$in['search']."%') ";
		$arguments_s.="&search=".$in['search'];
	}

	if(!empty($in['start_date_js'])){
		$in['start_date'] =strtotime($in['start_date_js']);
		$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
	}

	if($in['start_date']) {
		$stop_date= time();
		$start_date= $in['start_date'];
	}

	if($in['order_by']){
		$order = " ASC ";
		if($in['desc']){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
	}

	$art = $db->query("SELECT pim_articles.internal_name,pim_articles.article_id, pim_articles.hide_stock, pim_articles.stock, pim_article_prices.purchase_price,
				stock_movements.new_stock,stock_movements.date
	            FROM pim_articles
				INNER JOIN pim_articles_lang ON pim_articles.article_id=pim_articles_lang.item_id
				
				LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.price_category_id=0
				LEFT JOIN stock_movements ON  stock_movements.article_id=pim_articles.article_id AND 
					stock_movements.date = (SELECT MAX(stock_movements.date) FROM stock_movements where stock_movements.date<'".$start_date."' and stock_movements.article_id = pim_articles.article_id) 
				LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
				WHERE $filter AND pim_articles.hide_stock=0 AND pim_articles_lang.lang_id={$_SESSION['lang_id']}  
				GROUP BY pim_articles.article_id
			   ".$order_by."			
				");



	$max_rows=$art->records_count();
	$total_stock_value=0;
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	while($art->move_next()){
		 if($start_date){   
		 		$stock_at_date=$art->f('new_stock');
			  }else{
			  	 $stock_at_date=$art->f('stock');
			  }
			  
			  $stock_value = $stock_at_date * $art->f('purchase_price');

			  if ($stock_value>0){
			  	$total_stock_value += $stock_value; 
			  }
		}

	$articles = $db->query("SELECT pim_articles.article_id,pim_articles.hide_stock,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,			                   
				                   pim_articles.internal_name,pim_article_categories.name as article_category, pim_articles.price, pim_article_prices.purchase_price
	            FROM pim_articles
				INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id			
				
				LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.price_category_id=0
				LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
				WHERE $filter AND pim_articles.hide_stock=0  AND pim_articles_lang.lang_id={$_SESSION['lang_id']} 
				GROUP BY pim_articles.article_id
			   ".$order_by."");

	$j=0; 
	while($db->move_next()){
	     
	//if we have a date we have to select the stock ad that date base on stock movemtns
	  if($start_date){
	  	 $stock_at_date=$db2->field("SELECT new_stock FROM stock_movements WHERE article_id='".$db->f('article_id')."' AND `date`< '".$start_date."' ORDER BY date DESC  limit 1");    
	  }else{
	  	 $stock_at_date=$db->f('stock');
	  }
	  
	  $stock_value = $stock_at_date * $db->f('purchase_price');
	  	
		$item=array('code'		  		=> stripslashes($db->f('item_code')),
					'name'		  		=> gfns($db->f('internal_name')),
					'article_category'	=> stripslashes($db->f('article_category')),
					'stock'  			=> remove_zero_decimals($db->f('stock')),
					'stock_at_date'  	=> remove_zero_decimals($stock_at_date),
				    'stock_value'		=> ($stock_value > 0) ? get_currency_sign(display_number($stock_value)): get_currency_sign(display_number(0))
	         );
		 array_push($result['query'], $item);			
		$j++;
	}

	$final_result = $result['query'];

}else if($in['stock_type'] == 5){ //Article Based per location

	$my_headers = array(gm("Code"),
	                    gm("Descrition"),
	                    gm("Total stock"),
	                    gm("Stock at Main Location")
	  );

	$filename = 'article_based_location.csv';

	$order_by = " ORDER BY  pim_articles.item_code  ";
	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

	if(($in['ofs']) || (is_numeric($in['ofs'])))
	{
		$in['offset']=$in['ofs'];
	}

	$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	//FILTER LIST
	$selected =array();
	$filter = " pim_articles.active='1' ";


	if(!empty($in['search'])){
		$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
	}

	if($in['order_by']){
		$order = " ASC ";
		if($in['desc']){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
	}

	$articles = $db->query("SELECT pim_articles.article_id,pim_articles.hide_stock, pim_articles.stock as total_stock, pim_articles.item_code, pim_articles.internal_name, dispatch_stock.stock as stock_location
	            FROM pim_articles		
				LEFT JOIN dispatch_stock ON pim_articles.article_id = dispatch_stock.article_id AND dispatch_stock.address_id = '1'
				WHERE $filter AND pim_articles.hide_stock=0  AND dispatch_stock.stock != '0'
				GROUP BY pim_articles.article_id
			   ".$order_by)->getAll();

	$max_rows=$db->records_count();
	$j=0;
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	while($db->move_next()){

		
		$item=array('code'		  		=> htmlspecialchars_decode(stripslashes($db->f('item_code'))),
					'name'		  		=> htmlspecialchars_decode(stripslashes($db->f('internal_name'))),
					'total_stock'  			=> remove_zero_decimals($db->f('total_stock')),
				    'stock_location'	 		 => remove_zero_decimals($db->f('stock_location'))
					);
	    array_push($result['query'], $item);
		$j++;
	}

	$final_result = $result['query'];	

}

doQueryLog();
header('Content-Type: application/excel; charset=UTF-8');
header("Content-Disposition: attachment; filename=\"$filename\"");
header('Pragma: no-cache');
header('Expires: 0');

$fp = fopen('php://output', 'w');

//Add headers
fputcsv($fp, $my_headers);

//Add Information
foreach ( $final_result as $line ) {
    fputcsv($fp, $line);
}


fclose($fp);
exit();



