<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1000M');
setcookie('Akti-Export','6',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

/*$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
							 ->setLastModifiedBy("Akti")
							 ->setTitle("Articles detail report")
							 ->setSubject("Articles detail report")
							 ->setDescription("Articles report export")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Articles");

$objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);*/

$filename ="articles_stock_on_date.csv";

$db2 = new sqldb();

$start_date = $in['start_date'];

if(!$start_date ){
	$filter2 = " AND 1=1 ";
}

/*$objPHPExcel->getActiveSheet(0)->getStyle('A1:J1')->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Item code"))
			->setCellValue('B1', gm("Internal name"))
      ->setCellValue('C1', gm('Commercial Name').' '.gm('English'))
      ->setCellValue('D1', gm('Commercial Name').' '.gm('French'))
      ->setCellValue('E1', gm('Commercial Name').' '.gm('Dutch'))
			->setCellValue('F1', gm("Categorie"))
			->setCellValue('G1', gm("Current stock"))
			->setCellValue('H1', gm("Stock at date"))
      ->setCellValue('I1', gm("Purchase price"))
      ->setCellValue('J1', gm("Stock value at date"));*/

$headers = array(gm("Item code"),                
                  gm("Internal name"),
                  gm('Commercial Name').' '.gm('English'),
                  gm('Commercial Name').' '.gm('French'),
                  gm('Commercial Name').' '.gm('Dutch'),
                  gm("Categorie"),
                  gm("Current stock"),
                  gm("Stock at date"),
                  gm("Purchase price"),
                  gm("Stock value at date")
      );

$i=0;
$xlsRow = 2;

$order_by = " ORDER BY  pim_articles.item_code  ";

//FILTER LIST
$selected =array();
$filter = '1=1';
$filter.= " AND pim_articles.active='1' ";

if(!empty($in['search'])){
  $filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_article_categories.name LIKE'%".$in['search']."%') ";
}

if(!empty($in['start_date_js'])){
  $in['start_date'] =strtotime($in['start_date_js']);
  $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}

$join="";
$join_cols="";
if($in['start_date']) {
  $stop_date= time();
  $start_date= $in['start_date'];
  $join.="LEFT JOIN stock_movements AS stock_move ON stock_move.article_id=pim_articles.article_id AND
                stock_move.movement_id = (SELECT stock_movements.movement_id FROM stock_movements where stock_movements.date<'".$start_date."' and stock_movements.article_id = pim_articles.article_id ORDER BY stock_movements.date DESC LIMIT 1)";
    $join_cols.=",stock_move.new_stock";
}

if($in['order_by']){
  $order = " ASC ";
  if($in['desc']){
    $order = " DESC ";
  }
  $order_by =" ORDER BY ".$in['order_by']." ".$order;
  
}

//articles ids data
$articles_ids_str="";

$articles_ids_data=$db->query("SELECT DISTINCT(pim_articles.article_id) FROM pim_articles LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id WHERE $filter")->getAll();

foreach($articles_ids_data as $k=>$v){
  $articles_ids_str.="'".$v['article_id']."',";
}
$articles_ids_str=rtrim($articles_ids_str,",");

$articles_ids_data=null;
unset($articles_ids_data);
//end articles ids data

//lang_data data
$lang_data_ar=array();

$lang_data=$db->query("SELECT * FROM pim_articles_lang WHERE item_id IN (".$articles_ids_str.") ")->getAll();

foreach($lang_data as $k=>$v){
  if(array_key_exists($v['item_id'],$lang_data_ar) === false){
    $lang_data_ar[$v['item_id']]=array();
  }
  if(array_key_exists($v['lang_id'],$lang_data_ar[$v['item_id']]) === false){
    $lang_data_ar[$v['item_id']][$v['lang_id']]=array();
  }
  $lang_data_ar[$v['item_id']][$v['lang_id']]['name']=$v['name'];
}

$lang_data=null;
unset($lang_data);
//end lang_data data

$articles = $db->query("SELECT pim_articles.article_id,pim_articles.hide_stock,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,                         
                         pim_articles.internal_name,pim_article_categories.name as article_category, pim_articles.price, pim_article_prices.purchase_price".$join_cols."
            FROM pim_articles
  LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.price_category_id=0
    ".$join." 
      LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
      WHERE $filter AND pim_articles.hide_stock=0  
      GROUP BY pim_articles.article_id
       ".$order_by." 
      ");

$j=0; 
$total_stock =0;
$total_stock_at_date =0;
$total_stock_value = 0; 
$result = array('query'=>array());


while($articles->move_next()){
     
//if we have a date we have to select the stock ad that date base on stock movemtns
 
  if($start_date){
     /*$stock_at_date=$db2->field("SELECT new_stock FROM stock_movements WHERE article_id='".$articles->f('article_id')."' AND `date`< '".$start_date."' ORDER BY date DESC  limit 1"); */   
     $stock_at_date=is_null($articles->f('new_stock')) ? $articles->f('stock') :$articles->f('new_stock');
  }else{
     $stock_at_date=$articles->f('stock');
  }
  
  $stock_value = $stock_at_date * $articles->f('purchase_price');

  $total_stock +=$articles->f('stock');
  $total_stock_at_date +=$stock_at_date;
  if ($stock_value>0){
        $total_stock_value += $stock_value; 
      }

  $item=array(
        'name'              => htmlspecialchars_decode($articles->f('internal_name')),
        'article_category'  => $articles->f('article_category'),
        'article_id'        => $articles->f('article_id'),
        'stock'             => remove_zero_decimals($articles->f('stock')),
        'stock_at_date'     => remove_zero_decimals($stock_at_date),
        'stock_value'       => $stock_value>0 ? $stock_value: 0,
        'threshold'         => $articles->f('article_threshold_value'),
        'code'              => $articles->f('item_code'),
        'purchase_price'             => display_number_exclude_thousand($articles->f('purchase_price')),
        'allow_stock'                => ALLOW_STOCK,
        'stock_multiple_locations'   => STOCK_MULTIPLE_LOCATIONS
         );

   array_push($result['query'], $item);     
  $j++;
}
$final_data=array();

if($result['query']){
    foreach ($result['query'] as $key => $value) {
      $comm_name_en="";
      $comm_name_fr="";
      $comm_name_nl="";

      if(array_key_exists($value['article_id'], $lang_data_ar)){
        if(array_key_exists(1, $lang_data_ar[$value['article_id']]) && array_key_exists('name', $lang_data_ar[$value['article_id']][1])){
          $comm_name_en=$lang_data_ar[$value['article_id']][1]['name'];
        }
        if(array_key_exists(2, $lang_data_ar[$value['article_id']]) && array_key_exists('name', $lang_data_ar[$value['article_id']][2])){
          $comm_name_fr=$lang_data_ar[$value['article_id']][2]['name'];
        }
        if(array_key_exists(3, $lang_data_ar[$value['article_id']]) && array_key_exists('name', $lang_data_ar[$value['article_id']][3])){
          $comm_name_nl=$lang_data_ar[$value['article_id']][3]['name'];
        }
      }

      $tmp_item=array(
        $value['code'],
        $value['name'],
        $comm_name_en,
        $comm_name_fr,
        $comm_name_nl,
        $value['article_category'],
        $value['stock'],
        $value['stock_at_date'],
        $value['purchase_price'],
        $value['stock_value']
      );
      array_push($final_data,$tmp_item); 
      /*$objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A'.$xlsRow, $value['code'])
                  ->setCellValue('B'.$xlsRow, $value['name'])
                  ->setCellValue('C'.$xlsRow, $comm_name_en)
                  ->setCellValue('D'.$xlsRow, $comm_name_fr)
                  ->setCellValue('E'.$xlsRow, $comm_name_nl)
                  ->setCellValue('F'.$xlsRow, $value['article_category'])
                  ->setCellValue('G'.$xlsRow, $value['stock'])
                  ->setCellValue('H'.$xlsRow, $value['stock_at_date'])
                  ->setCellValue('I'.$xlsRow, $value['purchase_price'])
                  ->setCellValue('J'.$xlsRow, $value['stock_value']);

      $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);*/
      $xlsRow++;

    }
  }

//free memory
$articles_ids_str="";
unset($articles_ids_str);
$lang_data_ar=null;
unset($lang_data_ar);

if(!file_exists('upload/'.DATABASE_NAME)){
  if(!mkdir('upload/'.DATABASE_NAME)) {
    die('Failed');exit;
  }
}

header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$from_location='upload/'.DATABASE_NAME.'/article_export_'.$_SESSION['u_id'].'_'.time().'.csv';

$fp = fopen("php://output", 'w');

fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
unlink($from_location);
exit();

$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet(0)->getStyle('A'.$xlsRow.':J'.$xlsRow)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
	      ->setCellValue('A'.$xlsRow, gm('Total'))
				->setCellValue('B'.$xlsRow, '')
        ->setCellValue('C'.$xlsRow, '')
        ->setCellValue('D'.$xlsRow, '')
        ->setCellValue('E'.$xlsRow, '')
				->setCellValue('F'.$xlsRow, '')
				->setCellValue('G'.$xlsRow, $total_stock)
				->setCellValue('H'.$xlsRow, $total_stock_at_date)
        ->setCellValue('I'.$xlsRow, '')
        ->setCellValue('J'.$xlsRow, $total_stock_value);


// set column width to auto
foreach(range('A','J') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}
/*
$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$xlsRow)->applyFromArray($styleArray);

$styleArray = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_MEDIUM
    )
  )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$xlsRow)->applyFromArray($styleArray);
*/
$rows_format='I1:J'.$xlsRow;

$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode('0.00');

$objPHPExcel->getActiveSheet()->setTitle('Articles detail report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','../upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Export','9',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();

?>