<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


$db = new sqldb();
$db2 = new sqldb();

$l_r = 20;
//$order_by = " ORDER BY t_date DESC, serial_number DESC ";
$order_by = " ORDER BY t_date DESC, movement_id DESC ";
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}


//FILTER LIST
$selected =array();
$filter = " stock_movements.article_id !='0' ";
$filter_attach = '';
$arguments = 'stock_type=1';

if(!empty($in['search'])){
	$filter .= " AND ( stock_movements.item_code LIKE '%".$in['search']."%'  OR stock_movements.article_name LIKE '%".$in['search']."%'  OR stock_movements.serial_number LIKE '%".$in['search']."%') ";
	$arguments_s.="&search=".$in['search'];
}
if($in['order_by']){
	$order = " ASC ";
	if($in['desc']){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
}
if($in['view']){
	$filter .= " AND movement_type='".$in['view']."'";
	$arguments.="&view=".$in['view'];
}
if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}

if($in['start_date'] && $in['stop_date']){
	
	$start_date= $in['start_date'];
	$stop_date= $in['stop_date'];
	$filter.=" AND stock_movements.date BETWEEN '".$start_date."' AND '".$stop_date."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];

	$filter_link = 'none';
}
else if($in['start_date']){
	//list($d, $m, $y) = explode($spliter,  $in['start_date']);
	//$start_date= mktime(0, 0, 0, $m, $d, $y);
	//$stop_date= mktime(23, 59, 59, $m, $d, $y);
	$filter.=" and cast(stock_movements.date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
	//$view_list->assign('start_date',$in['start_date']);
}
else if($in['stop_date']){
	//list($d, $m, $y) = explode($spliter,  $in['stop_date']);
	//$start_date= mktime(0, 0, 0, $m, $d, $y);
	//$stop_date= mktime(23, 59, 59, $m, $d, $y);
	$filter.=" and cast(stock_movements.date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	//$view_list->assign('stop_date',$in['stop_date']);
	//$filter_link = 'none';
}

$edit_stock_reason_data = $db->query("SELECT * FROM (SELECT * FROM stock_edit_reasons ORDER BY id DESC) as stock_edit_reasons GROUP BY stock_edit_reasons.article_id")->getAll();

	$edit_stock_reason = array();
	foreach ($edit_stock_reason_data as $key => $val) {
		$edit_stock_reason[$val['article_id']][] = array('id' => $val['id'],
                                              'article_id' => $val['article_id'],
                                              'address_id' => $val['address_id'],
                                              'user_id' => $val['user_id'],
                                              'username' => stripslashes($val['username']),
                                              'edit_reason' => stripslashes($val['edit_reason']),
                                              'date'  => date(ACCOUNT_DATE_FORMAT,  $val['date'])
                                              );
		
	}


$arguments = $arguments.$arguments_s;
$db->query("SELECT pim_articles.article_id, pim_articles.is_service, count(stock_movements.movement_id) AS max_rows, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date 
            FROM stock_movements
            LEFT JOIN pim_articles ON stock_movements.article_id = pim_articles.article_id
            WHERE $filter AND pim_articles.is_service = '0'");
$max_rows=$db->f('max_rows');

// $articles = $db->query("SELECT stock_movements.*, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date FROM stock_movements WHERE $filter   ".$order_by."	LIMIT ".$offset*$l_r.",".$l_r."	");
$articles = $db->query("SELECT stock_movements.*, pim_articles.article_id, pim_articles.is_service, date AS t_date 
	                    FROM stock_movements
                        LEFT JOIN pim_articles ON stock_movements.article_id = pim_articles.article_id
                        WHERE $filter  AND stock_movements.stock != stock_movements.new_stock AND pim_articles.is_service = '0'  ".$order_by."	LIMIT ".$offset*$l_r.",".$l_r."	");

$movements_type = array("1"=>"Article added", 
						"2"=>"Article Updated", 
						"3"=>"Delivery added", 
						"4"=>"Delivery deleted", 
						"5"=>"Order deleted", 
						"6"=>"Entry added", 
						"7"=>"Entry deleted", 
						"8"=>"Purchase order deleted",
						"9"=>"Delivery edited",
						"11"=>"Compose New Main Article",
						"12"=>"Compose To Main Article",
						"13"=>"Return added",
						"14"=>"Return updated",
						"15"=>"Return deleted",
						"10"=>"Entry edited",
					/*	"11"=>"Project article delivered",
						"12"=>"Intervention article delivered",
						"13"=>"Ticket added"*/
						"16"=>"Project article delivered",
						"17"=>"Intervention article delivered",
						"18"=>"Ticket added",
						"19"=>"Intervention return added",
						"20"=>"Intervention RMA added",
						"21"=>"Intervention DOA added",
						"22"=>"Stock Count");

$movements_img = array("1"=>"user_box-bg.png", 
						"2"=>"user_box-bg.png", 
						"3"=>"make_entry.png", 
						"4"=>"user_box-bg.png", 
						"5"=>"user_box-bg.png", 
						"6"=>"make_delivery.png", 
						"7"=>"user_box-bg.png", 
						"8"=>"user_box-bg.png", 
						"9"=>"user_box-bg.png", 
						"11"=>"user_box-bg.png", 
						"12"=>"user_box-bg.png", 
						"13"=>"make_delivery.png", 
						"14"=>"make_delivery.png", 
						"15"=>"user_box-bg.png", 
						"10"=>"user_box-bg.png",
                  		"16"=>"make_delivery.png",
                  		"17"=>"make_delivery.png",
                  		"18"=>"make_delivery.png",
                  		"19"=>"make_delivery.png",
                  		"20"=>"make_delivery.png",
                  		"21"=>"make_delivery.png",
                  		"22"=>"user_box-bg.png"
						);
$j=0;
global $config;
$result = array('query'=>array(),'max_rows'=>$max_rows);
while($articles->move_next()){	
	if($articles->f('stock') < $articles->f('new_stock')){
	$stock_status = 'stock_status_up';
}elseif($articles->f('stock') > $articles->f('new_stock')){	
	$stock_status = 'stock_status_down';
}else{
	$stock_status = '';
}
$line_price='';
$buyer_name='';
if($articles->f('order_id') || $articles->f('p_order_id') || $articles->f('project_id') || $articles->f('service_id')){
	if($articles->f('order_id')){
		$buyer_name=$db2->field("SELECT customer_name FROM pim_orders WHERE order_id='".$articles->f('order_id')."' ");
		$line_price=display_number($db2->field("SELECT price FROM pim_order_articles WHERE order_id='".$articles->f('order_id')."' AND article_id='".$articles->f('article_id')."' "));
	}else if($articles->f('p_order_id')){
		$buyer_name=$db2->field("SELECT customer_name FROM pim_p_orders WHERE p_order_id='".$articles->f('p_order_id')."' ");
		$line_price=display_number($db2->field("SELECT price FROM pim_p_order_articles WHERE p_order_id='".$articles->f('p_order_id')."' AND article_id='".$articles->f('article_id')."' "));
	}else if($articles->f('project_id')){
		$buyer_name=$db2->field("SELECT name FROM customers WHERE customer_id IN (SELECT customer_id FROM projects WHERE project_id='".$articles->f('project_id')."') ");
		$line_price=display_number($db2->field("SELECT price FROM project_articles WHERE project_id='".$articles->f('project_id')."' AND article_id='".$articles->f('article_id')."' "));
	}else if($articles->f('service_id')){
		$buyer_name=$db2->field("SELECT customer_name FROM servicing_support WHERE service_id='".$articles->f('service_id')."' ");
		$line_price=display_number($db2->field("SELECT price FROM servicing_support_articles WHERE service_id='".$articles->f('service_id')."' AND article_id='".$articles->f('article_id')."' "));
	}
}
  $item=array(
   // $view_list->assign(array(
				'article_name'		=> $articles->f('article_name'),				
				'article_id'  		=> $articles->f('article_id'),
				//'selling_reference'  => $articles->f('order_id')? '<a href="index.php?do=order-order&order_id='.$articles->f('order_id').'">'.$articles->f('serial_number').'</a>':'',
				'order_id' 			=> $articles->f('order_id'),
				'selling_reference' => ($articles->f('order_id') || $articles->f('ticket_id') || $articles->f('p_order_id')) ? $articles->f('serial_number') :'',
				'state'				=> $articles->f('order_id')? 'order.view': ($articles->f('ticket_id')? 'cashregister':''),
				//'purchase_reference' => $articles->f('p_order_id')?'<a href="index.php?do=order-p_order&p_order_id='.$articles->f('p_order_id').'">'.$articles->f('serial_number').'</a>':'',
				'p_order_id' 		=> $articles->f('p_order_id'),
				'purchase_reference'=> $articles->f('p_order_id')?$articles->f('serial_number'):'',

				'date'  			=> date("Y-m-d H:i:s",$articles->f('date')),
				'stock'  			=> remove_zero_decimals($articles->f('stock')),
				'new_stock' 		=> remove_zero_decimals($articles->f('new_stock')),
				'item_code'		  	=> $articles->f('item_code'),				
				'movement_type'		=> gm($movements_type[$articles->f('movement_type')]),
				'quantity'			=> remove_zero_decimals($articles->f('quantity')),
				'backorder_articles'=> $articles->f('backorder_articles') ? remove_zero_decimals($articles->f('backorder_articles')) : 0,
				'stock_status'		=> $stock_status,
				'type_img'          => $movements_img[$articles->f('movement_type')],
				'created_by'		=> get_user_name($articles->f('created_by')),
				'delivery_date'		=> $articles->f('delivery_date') ? date("Y-m-d",$articles->f('delivery_date')) : '',
				'buyer_name'		=> stripslashes($buyer_name),
				'line_price'		=> $line_price
				);
	
	  	$art_id = $articles->f('order_id');
  		$p_ord_id = $articles->f('p_order_id');
  		$serial_number = $articles->f('serial_number');
  		$delivery_id = 	$articles->f('delivery_id');
  		$project_id = $articles->f('project_id');
  		$service_id = $articles->f('service_id');
  		$ticket_id = $articles->f('ticket_id');
  		
  		if(empty($art_id) && empty($p_ord_id) && empty($serial_number) && empty($delivery_id) && empty($project_id) && empty($service_id) && empty($ticket_id) ){
  			foreach ($edit_stock_reason as $key => $value) {
		  		if($key == $articles->f('article_id')){
		  			$item['edit_reason'] = $value[0];
		  		}
	  		}	
  		}

    array_push($result['query'], $item);

			//),'articles');
   // $view_list->loop('articles');
	$j++;
}


$result['view_dd'] = array( array('id'=>'1', 'value'=>gm('Article added')), array('id'=>'2', 'value'=>gm('Article updated')), array('id'=>'3', 'value'=>gm('Delivery added')),
                            array('id'=>'4', 'value'=>gm('Delivery deleted')), array('id'=>'5', 'value'=>gm('Order deleted')), array('id'=>'6', 'value'=>gm('Entry added')),
                            array('id'=>'7', 'value'=>gm('Entry deleted')), array('id'=>'8', 'value'=>gm('Purchase order deleted')), array('id'=>'9', 'value'=>gm('Delivery edited')),
                            array('id'=>'10', 'value'=>gm('Entry edited')), array('id'=>'13', 'value'=>gm('Return added')));

$result['lr']		= $l_r;
$result['export_args'] = ltrim($arguments,'&');

$result['columns']=array();
$cols_default=default_columns_dd(array('list'=>'stock_transactions'));
$cols_order_dd=default_columns_order_dd(array('list'=>'stock_transactions'));
$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='stock_transactions' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
if(!count($cols_selected)){
	$i=1;
    $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'stock_transactions'));
    foreach($cols_default as $key=>$value){
		if($key=='buyer_name' || $key=='line_price'){
        	continue;
        }
		$tmp_item=array(
			'id'				=> $i,
			'column_name'		=> $key,
			'name'				=> $value,
			'order_by'			=> $cols_order_dd[$key]
		);
        if ($default_selected_columns_dd[$key]) {
            array_push($result['columns'],$tmp_item);
        }
		$i++;
	}
}else{
	foreach($cols_selected as $key=>$value){
		$tmp_item=array(
			'id'				=> $value['id'],
			'column_name'		=> $value['column_name'],
			'name'				=> $cols_default[$value['column_name']],
			'order_by'			=> $cols_order_dd[$value['column_name']]
		);
		array_push($result['columns'],$tmp_item);
	}
}

json_out($result);