<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db = new sqldb();
$db2 = new sqldb();
$db3 = new sqldb();
$db4 = new sqldb();

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}


//first we have to update the backorders for articles
$db2->query("UPDATE dispatch_stock SET backorders='0' ");  	
$db->query("SELECT pim_order_articles.order_articles_id,pim_order_articles.delivered,pim_order_articles.order_id,pim_order_articles.article_id,pim_order_articles.quantity as t_quantity
	        
	        FROM pim_order_articles  
	        WHERE pim_order_articles.delivered=0 AND pim_order_articles.article_id!='0'
            GROUP BY pim_order_articles.order_articles_id
	        ");
$order_articles=array();

while ($db->move_next()) {
	$already_delivered=$db2->field("SELECT sum(quantity) FROM pim_orders_delivery WHERE order_articles_id='".$db->f('order_articles_id')."'");
	
       if($already_delivered){
	     $order_articles[$db->f('order_articles_id')][$db->f('article_id')] =$db->f('t_quantity')- $already_delivered;
	}
    
}


foreach ($order_articles as $order_articles_id => $articles) {
	    foreach ($articles as $article_id => $quantity) {
	    	     $db2->query("UPDATE dispatch_stock SET backorders=(backorders+".$quantity.") WHERE article_id='".$article_id."'");  	
	  	     
	    }
}

$l_r = 30;
$order_by = " ORDER BY  pim_articles.item_code  ";
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}



//FILTER LIST
$selected =array();
$filter = '1=1';
$filter.=" AND (pim_articles.stock <= pim_articles.article_threshold_value OR dispatch_stock.backorders!='0.00')";
$filter .= " AND pim_articles.active=1 AND pim_articles.is_service =0 AND pim_articles.use_combined =0 ";
$filter_attach = '';




if(!empty($in['search'])){
	$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
	$arguments_s.="&search=".$in['search'];
}
if($in['attach_to_product']){
	 if($in['attach_to_product']==1){
	 	$filter_attach.= " INNER JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
	 }else{
	 	$filter_attach.= " LEFT JOIN pim_product_article ON pim_product_article.article_id=pim_articles.article_id ";
	 	$filter.=" AND pim_product_article.article_id is null";
	 }	 	
	$arguments.="&attach_to_product=".$in['attach_to_product'];
}
if($in['supplier_id']){	
	$filter.=" AND pim_articles.supplier_id='".$in['supplier_id']."' ";	 
	$arguments.="&supplier_id=".$in['supplier_id'];
}
if($in['article_category_id']){	
	$filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";	 
	$arguments.="&article_category_id=".$in['article_category_id'];
}

if($in['first'] && $in['first'] == '[0-9]'){
	$filter.=" AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
	$arguments.="&first=".$in['first'];
} elseif ($in['first']){
	$filter .= " AND pim_articles.internal_name LIKE '".$in['first']."%'  ";
	$arguments.="&first=".$in['first'];
}

if($in['order_by']){
	$order = " ASC ";
	if($in['desc']){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
}

$arguments = $arguments.$arguments_s;

$db->query("SELECT pim_articles.article_id
            FROM pim_articles
			
			$filter_attach
			LEFT JOIN  dispatch_stock ON dispatch_stock.article_id=pim_articles.article_id			
			
			WHERE $filter AND pim_articles.hide_stock=0 
			GROUP BY pim_articles.article_id	

			");

$max_rows=$db->records_count();
/*while($db->next()){
	if(!in_array(substr(strtoupper($db->f('internal_name')),0,1), $selected)){

	   array_push($selected, substr(strtoupper($db->f('internal_name')),0,1));
  }
}*/

/*$articles = $db->query("SELECT pim_articles.max_stock,pim_articles.supplier_name,pim_articles.supplier_id,pim_articles.article_category_id,pim_articles.sale_unit,pim_articles.packing,pim_articles.article_id,pim_articles.hide_stock,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,			                   
			                   pim_articles.internal_name,dispatch_stock.backorders
            FROM pim_articles
			INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id
			LEFT JOIN  dispatch_stock ON dispatch_stock.article_id=pim_articles.article_id				
			$filter_attach
			
			
			WHERE $filter AND pim_articles.hide_stock=0 AND pim_articles_lang.lang_id={$_SESSION['lang_id']} 
			GROUP BY pim_articles.article_id
		   ".$order_by."
			LIMIT ".$offset*$l_r.",".$l_r."
			");*/

$articles = $db->query("SELECT pim_articles.max_stock,pim_articles.supplier_name,pim_articles.supplier_id,pim_articles.article_category_id,pim_articles.sale_unit,pim_articles.packing,pim_articles.article_id,pim_articles.hide_stock,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,			                   
			                   pim_articles.internal_name,dispatch_stock.backorders
            FROM pim_articles
			LEFT JOIN  dispatch_stock ON dispatch_stock.article_id=pim_articles.article_id				
			$filter_attach			
			WHERE $filter AND pim_articles.hide_stock=0
			GROUP BY pim_articles.article_id
		   ".$order_by."
			LIMIT ".$offset*$l_r.",".$l_r."
			");

$j=0;
$result = array('query'=>array(),'max_rows'=>$max_rows,'supplier_exist'=>array(),'family_exist'=>array());
while($db->move_next()){
     
 $packing=$db->f('packing');
    if($packing='0.00'){
    	$packing=1;
    }
     

     
    $tresh=$db->f('max_stock')!='0.00' ? $db->f('max_stock'):$db->f('article_threshold_value');
   

    if($db->f('stock') < $tresh){
	    $suggested_purchase = $db->f('stock')- $tresh;
    }else{
    	$suggested_purchase=0;
    }
        $items_order=$db2->field("SELECT SUM(quantity) FROM  pim_p_orders 
        						LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id = pim_p_order_articles.p_order_id
        						 WHERE article_id='".$db->f('article_id')."' AND pim_p_orders.active =1");
	$items_received=$db2->field("SELECT SUM(pim_p_orders_delivery.quantity) 
		                         FROM   pim_p_orders_delivery 
		                         INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
		                         WHERE pim_p_order_articles.article_id='".$db->f('article_id')."' ");
	//$view_list->assign(array(
	$item=array(			'name'		  		=> $db->f('internal_name'),
				'supplier_name'	=> $db->f('supplier_name'),
				'article_id'  		=> $db->f('article_id'),
				'id'  				=> $db->f('article_id'),
				'stock'  			=> remove_zero_decimals($db->f('stock')),
			    
			   'virtual_stock'	  => $items_order-$items_received,
				 'threshold'	  => $db->f('article_threshold_value'),
				 'backorders'	           =>  remove_zero_decimals($db->f('backorders')),
				'suggested_purchase'	   =>  remove_zero_decimals(abs($suggested_purchase)*($packing/$db->f("sale_unit"))),
				'code'		  		=> $db->f('item_code'),
				
				'title'				=> $in['archived'] ? 'Delete' : 'Archive',
				'confirm'			=> $in['archived'] ? 'Delete this article?' : 'Archive this article?',
				'd_confirm'			=> $in['archived'] ? 'Activate this article?' : '',
				'd_title'			=> $in['archived'] ? 'Activate' : 'Duplicate',
				'd_class'			=> $in['archived'] ? 'art_del' : '',
				'd_img'				=> $in['archived'] ? 'undo' : 'duplicate',
				'allow_stock'                => ALLOW_STOCK,
				'stock_multiple_locations'   => STOCK_MULTIPLE_LOCATIONS,
               'check_add_to_purchase'=> $_SESSION['add_to_purchase'][$db->f('article_id')] == 1 ? 'CHECKED' : '',
               'check_add_to_product'	=> $_SESSION['add_to_purchase'][$db->f('article_id')] == 1 ? true : false,
  );
	 array_push($result['query'], $item);
	 $array_next=array();
		//	),'articles');
	$supplier_exist=$db3->query("SELECT name,customer_id FROM customers WHERE is_supplier='1' AND 1=1");
	while($supplier_exist->move_next()){
        $array_next[] = array('id'=>$supplier_exist->f('customer_id'),'value'=>$supplier_exist->f('name'));            
    }
	array_push($result['supplier_exist'], $array_next);

	$array_next1=array();
		//	),'articles');
	$family_exist=$db4->query("SELECT name, id FROM pim_article_categories WHERE 1=1");
	while($family_exist->move_next()){
        $array_next1[] = array('id'=>$family_exist->f('id'),'value'=>$family_exist->f('name'));            
    }
	array_push($result['family_exist'], $array_next1);
   // $view_list->loop('articles');
	$j++;
}

function get_depo_list($in){
	$db = new sqldb();
	if(!isset($in['article_id']) || !is_numeric($in['article_id'])){
		return array();
	}
	$result = array( 'list' => array());
	

	$l_r = ROW_PER_PAGE;

	

	$depo = $db->query("SELECT dispatch_stock.*,dispatch_stock_address.*
            FROM   dispatch_stock
            INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='1'")->getAll();
	//$row = count($depo);

	$i=0;
	foreach ($depo as $key => $value) {
	
		 // $view->assign(array(
           $item = array(          'customer_name' => $value['naming'],
                     'current_quantity'      => remove_zero_decimals($value['stock']),
                     'address'=> $value['address'],
                     'city'=> $value['city'],
                     'zip'=> $value['zip'],
                     'country'=> get_country_name($value['country_id'])
                     );
                     

                //  ),'customer_row');
	

		

		array_push($result['list'], $item);



		$i++;
	}

	$depo2 = $db->query("SELECT dispatch_stock.*, customer_addresses.*,customers.name
            FROM   dispatch_stock
            INNER JOIN customers ON customers.customer_id=dispatch_stock.customer_id
            LEFT JOIN  customer_addresses ON  dispatch_stock.address_id=customer_addresses.address_id AND dispatch_stock.customer_id=customer_addresses.customer_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='0'")->getAll();
	//$row = count($depo2);

foreach ($depo2 as $key => $value) {
	
		 // $view->assign(array(
           $item = array(          'customer_name' => $value['name'],
                     'current_quantity'      => remove_zero_decimals($value['stock']),
                     'address'=> $value['address'],
                     'city'=> $value['city'],
                     'zip'=> $value['zip'],
                     'country'=> get_country_name($value['country_id'])
                     );
                     

                //  ),'customer_row');
	

		

		array_push($result['list'], $item);



		$i++;
	}


	return $result;
}




$result['lr']		= $l_r;



json_out($result);