<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['reset_list']){
        if(isset($_SESSION['tmp_add_to_export'])){
            unset($_SESSION['tmp_add_to_export']);
        }
        if(isset($_SESSION['add_to_export'])){
            unset($_SESSION['add_to_export']);
       }
}
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}


$db = new sqldb();
$db_dep = new sqldb();
$db2 = new sqldb();
$db3 = new sqldb();
$l_r = 20;

$arguments = 'stock_type=3';
$order_by_array = array('naming','address','stock_value','in_stock');

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$filter = "1=1";

if ($in['search'])
{
	$filter .= " AND customer_name LIKE '%".$in['search']."%' ";
	$arguments_s.="&search=".$in['search'];
}


$order_by = " ORDER BY pim_stock_disp.customer_address_id ";
$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

	if(!empty($in['order_by'])){
	    if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
			    $order = " DESC ";
			}
			if($in['order_by']=='naming' || $in['order_by']=='address'|| $in['order_by']=='stock_value'|| $in['order_by']=='in_stock'){
				$order_by='';
				$filter_limit =' ';
			}else{
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
				$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
				$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			}

	    }
	}

$arguments = $arguments.$arguments_s;


//first we select own location
$customers = $db->query("SELECT pim_articles.is_service, dispatch_stock.*,dispatch_stock_address.*,sum(dispatch_stock.stock) as stock_at_address, sum( dispatch_stock.stock * pim_article_prices.purchase_price ) AS stock_value 
                         FROM dispatch_stock
                         INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
                         LEFT JOIN pim_article_prices ON pim_article_prices.article_id = dispatch_stock.article_id    
                         LEFT JOIN pim_articles on pim_articles.article_id=dispatch_stock.article_id
					     AND pim_article_prices.base_price = '1'
                         WHERE   dispatch_stock.main_address='1' AND dispatch_stock.stock>'0' AND pim_articles.is_service='0' AND pim_articles.active='1' AND pim_articles.has_variants='0'
                         GROUP BY dispatch_stock.address_id "
	       				.$filter_limit
	                     );

$max_rows = $db->query("SELECT pim_articles.is_service, dispatch_stock.*,dispatch_stock_address.*,sum(dispatch_stock.stock) as stock_at_address, sum( dispatch_stock.stock * pim_article_prices.purchase_price ) AS stock_value 
                         FROM dispatch_stock
                         INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
                         LEFT JOIN pim_article_prices ON pim_article_prices.article_id = dispatch_stock.article_id    
                         LEFT JOIN pim_articles on pim_articles.article_id=dispatch_stock.article_id
					     AND pim_article_prices.base_price = '1'
                         WHERE   dispatch_stock.main_address='1' AND dispatch_stock.stock>'0' AND pim_articles.is_service='0' AND pim_articles.active='1' AND pim_articles.has_variants='0'
                         GROUP BY dispatch_stock.address_id")->records_count();

$j=0;
$result = array('query'=>array(),'max_rows'=>$max_rows);
$result['max_rows'] = $max_rows;
if(!$_SESSION['tmp_add_to_export'] || ($_SESSION['tmp_add_to_export'] && empty($_SESSION['tmp_add_to_export']))) {
       for($i=1; $i<=$max_rows; $i++) {
               $_SESSION['tmp_add_to_export'][$i] = 1;
       }
}

$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_to_export']){
    if($max_rows && count($_SESSION['add_to_export']) == $max_rows){
        $all_pages_selected=true;
    }else if(count($_SESSION['add_to_export'])){
        $minimum_selected=true;
    }
}
$result['all_pages_selected'] = $all_pages_selected;
$result['minimum_selected'] = $minimum_selected;
while($customers->move_next()){
	$item=array(
	//$view_list->assign(array(
		             'naming'        =>  '<b>'.$customers->f('naming').'</b>',
		             'naming_ord'    =>  $customers->f('naming'),
		             'in_stock'      => remove_zero_decimals($customers->f('stock_at_address')),
		             'in_stock_ord'  => $customers->f('stock_at_address'),
                     'address'       => $customers->f('address').'<br/>'.$customers->f('zip').' '.$customers->f('city').'<br/>'.get_country_name($customers->f('country_id')),
                     'address_ord'   => $customers->f('address').'<br/>'.$customers->f('zip').' '.$customers->f('city').'<br/>'.get_country_name($customers->f('country_id')),
                     'customer_id'   => '0',
                     'address_id'    => $customers->f('address_id'),
                     'stock_value'	 => place_currency(display_number($customers->f('stock_value'))),
                     'stock_value_ord' => $customers->f('stock_value'),
                     'horus'		 => (DATABASE_NAME=='74997702_76b4_759b_c140a8d1b73b' || DATABASE_NAME=='a2f5b012_4714_b182_c9c27f9f30ad' || DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11') ? true : false,
                    'id'            => $customers->f('address_id'),
                    'check_add_to_product'  => $_SESSION['add_to_export'][$customers->f('address_id')] == 1 ? true : false,
     );
	array_push($result['query'], $item);
		        //  ),'customers');
    //$view_list->loop('customers');

	$j++;
 
}
//then we other own location
$customers = $db->query("SELECT pim_articles.is_service, dispatch_stock.*, customer_addresses.*,customers.name,sum(dispatch_stock.stock) as stock_at_address, sum( dispatch_stock.stock * pim_article_prices.purchase_price ) AS stock_value 
            FROM   dispatch_stock
            INNER JOIN customers ON customers.customer_id=dispatch_stock.customer_id
            LEFT JOIN  customer_addresses ON  dispatch_stock.address_id=customer_addresses.address_id AND dispatch_stock.customer_id=customer_addresses.customer_id
            LEFT JOIN pim_article_prices ON pim_article_prices.article_id = dispatch_stock.article_id
            LEFT JOIN pim_articles on pim_articles.article_id=dispatch_stock.article_id
            AND pim_article_prices.base_price = '1'
            WHERE   dispatch_stock.main_address='0' AND dispatch_stock.stock>'0' AND pim_articles.is_service='0' AND pim_articles.active='1' AND pim_articles.has_variants='0' GROUP BY dispatch_stock.address_id "
	       .$filter_limit
	                     );

$j=0;

while($customers->move_next()){
//	$view_list->assign(array(
	$item=array(
		             'naming'        =>    $customers->f('name'),
		             'naming_ord'    =>    $customers->f('name'),
		             'in_stock'      => remove_zero_decimals($customers->f('stock_at_address')),
		             'in_stock_ord'  => $customers->f('stock_at_address'),
                     'address'       => $customers->f('address').'<br/>'.$customers->f('zip').' '.$customers->f('city').'<br/>'.get_country_name($customers->f('country_id')),
                     'address_ord'   => $customers->f('address').'<br/>'.$customers->f('zip').' '.$customers->f('city').'<br/>'.get_country_name($customers->f('country_id')),
                     'customer_id'   => $customers->f('customer_id'),
                     'address_id'   => $customers->f('address_id'),
                     'stock_value'	 => place_currency(display_number($customers->f('stock_value'))),
                     'stock_value_ord'	 => $customers->f('stock_value'),
                     'horus'		 => (DATABASE_NAME=='74997702_76b4_759b_c140a8d1b73b' || DATABASE_NAME=='a2f5b012_4714_b182_c9c27f9f30ad' || DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11') ? true : false,
        );

array_push($result['query'], $item);
	//	          ),'customers');
 //   $view_list->loop('customers');
	$j++;
 
}



if($max_rows > $l_r){
	$is_pagination=true;
}

$result['article_no']   = $max_rows;
$result['horus']		= (DATABASE_NAME=='74997702_76b4_759b_c140a8d1b73b' || DATABASE_NAME=='a2f5b012_4714_b182_c9c27f9f30ad' || DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11') ? true : false;

$result['export_args']  = ltrim($arguments,'&');

$result['lr']		= $l_r;

if($in['order_by']=='naming' || $in['order_by']=='address'|| $in['order_by']=='stock_value'|| $in['order_by']=='in_stock'){

	    if($order ==' ASC '){
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
	    }else{
	       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
	    }
	    $exo = array_slice( $exo, $offset*$l_r, $l_r);
	    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
	    
	}
console::log($result);
json_out($result);

function get_stock_list($in){
	$db = new sqldb();
	if(!isset($in['address_id']) || !is_numeric($in['address_id'])){
		return array();
	}
	$result = array( 'list' => array());
	$l_r = ROW_PER_PAGE;
	$order_by = " ORDER BY  pim_articles.item_code  ";
	$filter = '1=1';
$filter.= " AND  dispatch_stock.customer_id='".$in['customer_id']."'";
$filter.= " AND  dispatch_stock.address_id='".$in['address_id']."'";

	if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}


  $db->query("SELECT  dispatch_stock.article_id,dispatch_stock.stock,pim_articles.is_service
            FROM  dispatch_stock
            LEFT JOIN pim_articles on pim_articles.article_id=dispatch_stock.article_id
            WHERE $filter AND dispatch_stock.stock!=0 AND pim_articles.is_service='0' AND pim_articles.active='1' AND pim_articles.has_variants='0'
			GROUP BY  dispatch_stock.article_id
          ");

$max_rows=$db->records_count();




 $db->query("SELECT  dispatch_stock.stock,dispatch_stock.article_id,dispatch_stock.stock,pim_articles.article_id,pim_articles.article_threshold_value,pim_articles.item_code,pim_articles.internal_name,pim_articles.is_service,pim_article_categories.name as article_category
            FROM  dispatch_stock
            LEFT JOIN pim_articles on pim_articles.article_id=dispatch_stock.article_id
            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
            WHERE $filter AND dispatch_stock.stock!=0 AND pim_articles.is_service='0' AND pim_articles.active='1' AND pim_articles.has_variants='0'
			GROUP BY  dispatch_stock.article_id
		   ".$order_by."
			LIMIT ".$offset*$l_r.",".$l_r."
			");

$j=0;
$qty=0;
while($db->move_next()){
   $item=array('name'		  		=> $db->f('internal_name'),
				'article_category'	=> $db->f('article_category'),
				'article_id'  		=> $db->f('article_id'),
				'stock'  			=> remove_zero_decimals($db->f('stock')),
				'threshold_value'   => $db->f('article_threshold_value') ,
				'code'		  		=> $db->f('item_code'),
				);
   
   array_push($result['list'], $item);


  $j++;
}

return $result;
}