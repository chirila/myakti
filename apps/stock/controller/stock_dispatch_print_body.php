<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['type']){
	$html='stock_dispatch_print_'.$in['type'].'.html';
}else{
	$html='stock_dispatch_print_1.html';
}
$db2=new sqldb();
$db3 = new sqldb();
$db4 = new sqldb();

$view = new at(ark::$viewpath.$html);
if(!$in['stock_disp_id']){ //sample
	//assign dummy data
$height = 251;
	$factur_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));	
	$view->assign(array(
	'account_logo'        => '../img/no-logo.png',
	'billing_address_txt' => geto_label_txt('billing_address',$in['lid']),
	'pick_up_address_txt' => geto_label_txt('pickup_address',$in['lid']),
	'stock_dispatching_note'      => geto_label_txt('stock_dispatching_note',$in['lid']),
	'stock_dispatching'           => geto_label_txt('stock_dispatching',$in['lid']),
	'invoice_txt'         => geto_label_txt('invoice',$in['lid']),
	'date_txt'            => geto_label_txt('date',$in['lid']),
	'customer_txt'        => geto_label_txt('customer',$in['lid']),
	'article_txt'         => geto_label_txt('article',$in['lid']),
	'unitmeasure_txt'     => geto_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => geto_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => geto_label_txt('unit_price',$in['lid']),
	'amount_txt'          => geto_label_txt('amount',$in['lid']),
	'subtotal_txt'        => geto_label_txt('subtotal',$in['lid']),
	'discount_txt'        => geto_label_txt('discount',$in['lid']),
	'vat_txt'             => geto_label_txt('vat',$in['lid']),
	'payments_txt'        => geto_label_txt('payments',$in['lid']),
	'amount_due_txt'      => geto_label_txt('amount_due',$in['lid']),
	'notes_txt'           => geto_label_txt('notes',$in['lid']),
	//	'duedate'			  => geto_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => geto_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => geto_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => geto_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => geto_label_txt('iban',$in['lid']),
	'our_ref_txt'		  => geto_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => geto_label_txt('your_ref',$in['lid']),
	'phone_txt'			  => geto_label_txt('phone',$in['lid']),
	'fax_txt'			  => geto_label_txt('fax',$in['lid']),
	'url_txt'			  => geto_label_txt('url',$in['lid']),
	'email_txt'			  => geto_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => geto_label_txt('vat_number',$in['lid']),
	'vat_number_txt' 	  => geto_label_txt('vat_number',$in['lid']),
	'shipping_price_txt'  => geto_label_txt('shipping_price',$in['lid']),
	
	'seller_name'         => '[Account Name]',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_CITY'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'seller_b_country'    => '[Billing Country]',
	'seller_b_state'      => '[Billing State]',
	'seller_b_city'       => '[Billing City]',
	'seller_b_zip'        => '[Billing Zip]',
	'seller_b_address'    => '[Billing Address]',
	'serial_number'       => '####',
	'order_date'          => $factur_date,
	'order_vat'           => '##',
	'order_vat_no'		  => '[Vat Number]',
	'buyer_name'       	  => '[Customer Name]',
	'buyer_country'    	  => '[Customer Country]<br>',
	'buyer_state'      	  => '[Customer State]',
	'buyer_city'       	  => '[Customer City]<br>',
	'buyer_zip'           => '[Customer ZIP]',
	'buyer_address'    	  => '[Customer Address]<br>',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_city'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'delivery_address'	  => '[Delivery Address]',
	'your_ref'       	  => '[Your Reference]',
	'our_ref' 			  => '[Our reference]',
	'notes'            	  => '[NOTES]',
	'bank'				  => '[Bank name]',
	'bic'				  => '[BIC]',
	'iban'				  => '[IBAN]',
	//'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'seller_b_fax'        => '[Account Fax]',
	'seller_b_email'      => '[Account Email]',
	'seller_b_phone'      => '[Account Phone]',
	'seller_b_url'        => '[Account URL]',
	
	'hide_f'			  => true,
	'hide_e'			  => true,
	'hide_p'			  => true,
	'hide_u'			  => true,
	'is_delivery'		  => true,
	'sh_discount'		  => 'hide',
	));
	$i=0;
	while ($i<3)
	{
		$shipping_price = 9;
		$vat_total = 5;
		$total = 63;
		$total_with_vat = 68+$shipping_price;		
		$view->assign(array(
			'article'  			    	=> 'Article Name',
			'quantity'      			=> display_number(2),
			'price_vat'      			=> display_number(21),
			'price'         			=> display_number(20),
			'percent'					=> display_number(5),
			'content'					=> true,
			'vat_value'					=> display_number(1),
		),'order_row');
		$view->assign('article_width','18','order_row');

		$view->loop('order_row');
		$i++;

	}
	$view->assign(array(
		'vat_total'				=> display_number($vat_total),
		'total'					=> display_number($total),
		'total_vat'				=> display_number($total_with_vat),
		'delivery'				=> true,
		'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
		'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
		'currency_type'			=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		'hide_default_total'	=> hide,
		'is_free_delivery'		=> true,
		'shipping_price'		=> display_number($shipping_price),
	));


}else{
	$db = new sqldb();
	$db->query("SELECT pim_stock_disp.* FROM pim_stock_disp
			
			WHERE pim_stock_disp.stock_disp_id='".$in['stock_disp_id']."'");
	$db->move_next();
	$serial_number = $db->f('serial_number');
   
	
	
	$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
	//	$due_date = date(ACCOUNT_DATE_FORMAT,  $db->f('due_date'));
	$delivery_date = date(ACCOUNT_DATE_FORMAT,  $db->f('del_date'));
	
	$img = '../images/no-logo.png';
	$attr = '';
	if($in['logo']) {
		$print_logo = $in['logo'];
	}else {
		$print_logo=ACCOUNT_LOGO_ORDER;
	}
	if($print_logo){
		$img = $print_logo;
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}
	$buyer_country = get_country_name($db->f('customer_country_id'));
	
	

	$notes = $db3->field("SELECT item_value FROM note_fields 
								   WHERE item_type = 'stock_disp_note' 
								   AND item_name = 'notes' 
								   AND active = '1' 
								   AND item_id = '".$in['stock_disp_id']."' ");	

	$view->assign(array(
	'account_logo'        => $img,
	'attr'				  => $attr,
	'billing_address_txt' => geto_label_txt('billing_address',$in['lid']),
	'pick_up_address_txt' => geto_label_txt('pickup_address',$in['lid']),
	'dispatch_note_txt'      => geto_label_txt('stock_dispatching_note',$in['lid']),
	'dispatch_txt'           => $in['delivery_id'] ? geto_label_txt('delivery_note',$in['lid']) : geto_label_txt('stock_dispatching',$in['lid']),

	'date_txt'            => geto_label_txt('date',$in['lid']),
	'customer_txt'        => geto_label_txt('customer',$in['lid']),
	'article_txt'         => geto_label_txt('article',$in['lid']),
	'unitmeasure_txt'     => geto_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => geto_label_txt('quantity',$in['lid']),
	
	
	
	'notes_txt'           => $notes ? geto_label_txt('notes',$in['lid']) : '',
	'duedate_txt'		  => geto_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => geto_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => geto_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => geto_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => geto_label_txt('iban',$in['lid']),
	
	
	'our_ref_txt'		  => geto_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => geto_label_txt('your_ref',$in['lid']),
	'phone_txt'			  => get_label_txt('phone',$in['lid']),
	'fax_txt'			  => get_label_txt('fax',$in['lid']),
	'url_txt'			  => get_label_txt('url',$in['lid']),
	'email_txt'			  => get_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => get_label_txt('vat_number',$in['lid']),
	'delivery_note_txt'	  => geto_label_txt('delivery_note',$in['lid']),
	'shipping_price_txt'  => geto_label_txt('shipping_price',$in['lid']),
	'article_code_txt'    => geto_label_txt('article_code',$in['lid']),
	'delivery_date_txt'   => geto_label_txt('delivery_date',$in['lid']),


	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',

	'serial_number'		  => $serial_number,
	'order_date'          => $factur_date,
	'buyer_name'       	  => $db->f('customer_name'),
	'buyer_country'    	  => $buyer_country? $buyer_country."<br/>":'',
	'buyer_state'      	  => $db->f('customer_state'),
	'buyer_city'       	  => $db->f('customer_city')? $db->f('customer_city')."<br/>":'',
	'buyer_zip'        	  => $db->f('customer_zip'),
	'buyer_address'    	  => $db->f('customer_address')? $db->f('customer_address')."<br/>":'',
	'buyer_email'		  => $db->f('customer_email')? $db->f('customer_email')."<br/>":'',
	'buyer_fax'           => $db->f('customer_fax')? $db->f('customer_fax')."<br/>":'',
	'buyer_phone'         => $db->f('customer_phone')? $db->f('customer_phone')."<br/>":'',
	'seller_name'         => $db->f('seller_name'),
	'seller_d_country'    => get_country_name($db->f('seller_d_country_id')),
	'seller_d_state'      => get_state_name($db->f('seller_d_state_id')),
	'seller_d_city'       => utf8_decode($db->f('seller_d_city')),
	'seller_d_zip'        => $db->f('seller_d_zip'),
	'seller_d_address'    => utf8_decode($db->f('seller_d_address')),
	'order_contact_name'  => $contact_title.$db->f('contact_name'),
	'order_buyer_country'   		=> get_country_name($db->f('customer_country_id')),
	'order_buyer_state'     		=> $db->f('customer_state'),
	'order_buyer_city'      		=> $db->f('customer_city'),
	'order_buyer_zip'       		=> $db->f('customer_zip'),
	'order_buyer_address'   		=> $db->f('customer_address'),
	'field_customer_id'   => $db->f('field')=='customer_id'?true:false,
	//	'INVOICE_BUYER_VAT'   => ACCOUNT_VAT_NUMBER,
	'bank'				  => ACCOUNT_BANK_NAME,
	'bic'				  => ACCOUNT_BIC_CODE,
	'iban'				  => ACCOUNT_IBAN,
	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	
	'notes'               =>  $notes ? nl2br($notes) : '',
	'view_notes'          => $notes ? '' : 'hide' ,
	
	'order_vat_no'		  => $db->f('seller_bwt_nr'),
	'your_ref'       	  => $db->f('your_ref'),
	'our_ref' 			  => $db->f('our_ref'),
	'hide_our_ref'		  => $db->f('our_ref')? '':'hide',
	'hide_your_ref'		  => $db->f('your_ref')? '':'hide',

	'seller_b_fax'        => ACCOUNT_FAX,
	'seller_b_email'      => ACCOUNT_EMAIL,
	'seller_b_phone'      => ACCOUNT_PHONE,
	'seller_b_url'        => ACCOUNT_URL,
	
	'hide_f'			  => defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? false : true,
	'hide_e'			  => defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? false : true,
	'hide_p'			  => defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? false : true,
	'hide_u'			  => defined('ACCOUNT_URL') && !ACCOUNT_URL ? false : true,
	'address_info'	      => nl2br($db->f('address_info')),
	'delivery_date'		  => $delivery_date,
	'is_delivery_date'	  => ($in['delivery_id'] && $delivery_date) ? false : true,
	'delivery_del_date'	  => $delivery_del_date,
	'is_delivery_del_date'=> ($in['delivery_id'] && $delivery_del_date) ? true : false,
	'to_naming'             => $db->f('to_naming'),
	'to_address'            => $db->f('to_address'),
	'to_zip'                => $db->f('to_zip'),
	'to_city'               => $db->f('to_city'),
	'to_country'            => $db->f('to_country'),

	));
	$height = 251;
	

$view->assign(array(
		'delivery_address'		=>nl2br($db->f('to_naming').'<br/>'.$db->f('to_address').'<br/>'.$db->f('to_zip').' '.$db->f('to_city').'<br/>'.$db->f('to_country')),
		'pick_up_address'		=> nl2br($db->f('from_naming').'<br/>'.$db->f('from_address').'<br/>'.$db->f('from_zip').' '.$db->f('from_city').'<br/>'.$db->f('from_country')),
		'is_delivery'			=>true
	));	

/*
 if($db->f('delivery_address')){
	$view->assign(array(
		'delivery_address'		=>nl2br($db->f('delivery_address')),
		'is_delivery'			=>true
	));	
}else{
	$view->assign(array(
		'delivery_address'=>'',
		'is_delivery'=>false
	));	
}*/

	$i=0;
		if($in['delivery_id']){

		$first_line_content_data = $db4->query("SELECT article, content FROM pim_stock_disp_articles WHERE stock_disp_id = '".$in['stock_disp_id']."' ORDER BY stock_disp_articles_id ASC LIMIT 1");
		$first_line_content_data->next();
		$is_first_content_line = false;
		if($first_line_content_data->f('content')=='1'){
			$is_first_content_line = true;
			$view->assign(array(
			'is_content_line2'			=> true,		
			'content_line2'				=> $first_line_content_data->f('article'),
			'is_first_content_line'		=> $is_first_content_line,
			));
			
		}		
		$db->query("SELECT pim_stock_disp_articles.*, pim_stock_disp_delivery.* FROM pim_stock_disp_articles 
					INNER JOIN pim_stock_disp_delivery ON pim_stock_disp_delivery.stock_disp_articles_id=pim_stock_disp_articles.stock_disp_articles_id 
					WHERE pim_stock_disp_delivery.stock_disp_id='".$in['stock_disp_id']."' AND pim_stock_disp_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");
		// echo "SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles 
		// 			INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id 
		// 			WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC";
		$content_line_data = $db4->query("SELECT stock_disp_articles_id, article FROM pim_stock_disp_articles WHERE stock_disp_id = '".$in['stock_disp_id']."' AND content = '1' ");
		$is_content_line = false;
		$content_line = array();
		while($content_line_data->next()){
			$content_line[$content_line_data->f('stock_disp_articles_id')] = $content_line_data->f('article');
			$is_content_line = true;
		}		
	}else{
        $db->query("SELECT pim_stock_disp_articles.* FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' ORDER BY sort_order ASC");
		$is_content_line = false;
	}
	while ($db->move_next())
	{
		$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));

		if($is_content_line == true){			
			foreach ($content_line as $key => $value) {
				$after_order_articles_id = $db->f('stock_disp_articles_id')+1;
				
				if($key == $after_order_articles_id){
					$is_spec_content_line = true;
					$spec_content_line = $value;
					goto found;
				}else{
					$is_spec_content_line = false;
				}
			}			
		}
		found:

		if ($in['delivery_id'] && $db->f('content')!=1  && (SHOW_ART_S_N_ORDER_PDF == 1)) {
            $art_serial_number_list = '';
            $use_serial_no = $db3->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '" . $db->f('article_id') . "' ");
            if ($use_serial_no) {
                $serial_number_data = $db3->query("SELECT serial_number FROM serial_numbers WHERE sd_delivery_id = '" . $in['delivery_id'] . "' AND article_id = '" . $db->f('article_id') . "' ");
                $k = 0;
                while ($serial_number_data->next()) {
                    $art_serial_number_list .= "<br/>" . $serial_number_data->f('serial_number');
                    $k++;
                }
                $art_serial_number_list = "<br/>" . htmlspecialchars(geto_label_txt('serial_numbers',$in['lid'])) . ': ' . $art_serial_number_list;
                $view->assign(array(
                    'is_serial_number' => $k > 0 ? true : false,
                    'art_serial_number_list' => $art_serial_number_list,
                        ), 'order_row');
            }
        }

        if ($in['delivery_id'] &&  $db->f('content')!=1  && (SHOW_ART_B_N_ORDER_PDF == 1)) {
            $art_batch_number_list = '';
            $use_batch_no = $db3->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '" . $db->f('article_id') . "' ");
            if ($use_batch_no) {
                $batch_number_data = $db3->query("SELECT DISTINCT batch_id,sum(quantity) as quantity FROM batches_from_orders WHERE sd_delivery_id = '" . $in['delivery_id'] . "' AND article_id = '" . $db->f('article_id') . "' AND sd_articles_id = '" . $db->f('stock_disp_articles_id') . "' GROUP BY  batch_id");
                $article_data = $db3->query("SELECT packing, sale_unit FROM pim_articles WHERE article_id = '" . $db->f('article_id') . "' ");
                $stock_packing = $article_data->f("packing");
                if(!$allow_article_packing){
                    $stock_packing = 1;
                }
                $stock_sale_unit = $article_data->f("sale_unit");
                if(!$allow_article_sale_unit){
                    $stock_sale_unit = 1;
                }

                $k = 0;
                while ($batch_number_data->next()) {
                    $add_location_name = '';
                    $batch = $db3->query("SELECT batch_number,date_exp FROM batches WHERE id = '" . $batch_number_data->f('batch_id') . "' ");
                    $art_batch_number_list .= "<br/>" .display_number($batch_number_data->f('quantity')/($stock_packing/$stock_sale_unit))."x ". $batch->f('batch_number').$add_location_name;
                    if (SHOW_ART_B_ED_ORDER_PDF == 1) {

                        $date_exp = $batch->f('date_exp') < 10000 ? ' - ' : date(ACCOUNT_DATE_FORMAT, $batch->f('date_exp'));
                        $art_batch_number_list .= "<br/>" . htmlspecialchars(geto_label_txt('batch_expiration_date',$in['lid'])).": " . $date_exp;
                    }
                    $k++;
                }
                $art_batch_number_list = "<br/>" . geto_label_txt('batches',$in['lid']) . ':' . $art_batch_number_list;
                $view->assign(array(
                    'is_batch_number' => $k > 0 ? true : false,
                    'art_batch_number_list' => $art_batch_number_list,
                        ), 'order_row');
            }
        }
				
		$view->assign(array(
		'article'  			    	=> $db->f('article'),
		//'quantity'      			=> display_number($db->f('quantity')),
		'quantity'      			=> (DATABASE_NAME == '03a0fb2c_c144_cda3_adda851c8e4b')? display_number($db->f('quantity'), $nr_dec_q):display_number($db->f('quantity')),
		'colspan'					=> $db->f('content') ? ' colspan="4" ' : '',
		'content'					=> $db->f('content') ? false : true,
		'content_class'				=> $db->f('content') ? ' last_nocenter ' : '',
		'is_article_code'			=> $db->f('article_code') ? true : false,
		'article_code'				=> $db->f('article_code'),
		'is_content_line'			=> ($is_content_line && $is_spec_content_line) ? true : false,		
		'content_line'				=> $spec_content_line,
		),'order_row');

		$view->loop('order_row');

		$view->assign(array('delivery_note'	=> nl2br($db->f('delivery_note'))));
		$delivery_note = $db->f('delivery_note');

		$i++;

	}
	if ($in['delivery_id']) {
    	$view->assign(array(
        'show_delivery_note' => $delivery_note ? true : false));

	} else {
	    $view->assign(array(
	        'show_delivery_note' => false));
	}
	
}

$is_table = true;
$is_data = false;

#for layout nr 4.
if($hide_all == 1){
	// $view->assign('hide_all','hide');
	$is_table = false;
	$is_data = true;
}else if($hide_all == 2){
	$is_table = true;
	$is_data = false;
	// $view->assign('hide_t','hide');
}

$view->assign(array(
	'is_table'=> $is_table,
	'is_data'=>$is_data,
));

return $view->fetch();