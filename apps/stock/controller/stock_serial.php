	<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


	$db = new sqldb();

	$l_r = 20;
	$order_by = " ORDER BY serial_numbers.id DESC ";

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
		$offset=0;
		$in['offset']=1;
	}
	else
	{
		$offset=$in['offset']-1;
	}

	//FILTER LIST
	$selected =array();
	$filter = " serial_numbers.article_id !='0' AND serial_numbers.serial_number != ' ' AND serial_numbers.active = '1' AND serial_numbers.status_id = '1' AND pim_articles.active = '1' ";
	$arguments = "serial_numbers.article_id !='0'";

	if(!empty($in['search'])){
		$filter .= " AND ( pim_articles.item_code LIKE '%".$in['search']."%'  OR pim_articles.internal_name LIKE '%".$in['search']."%'  OR serial_numbers.serial_number LIKE '%".$in['search']."%') ";
		$arguments_s.="&search=".$in['search'];
	}
	if($in['order_by']){
		$order = " ASC ";
		if($in['desc']){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		
	}
	if($in['product_id'])
	{
		$filter .= " AND pim_article_categories.id = ".$in['product_id']." ";
		$arguments.= "&product_name=".$in['product_id'];
	}

	$arguments = $arguments.$arguments_s;

	$db->query("SELECT count(serial_numbers.id) AS max_rows
		FROM serial_numbers
		LEFT JOIN pim_articles ON serial_numbers.article_id = pim_articles.article_id
		LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id
		WHERE $filter AND pim_articles.use_serial_no = '1'");
	$max_rows=$db->f('max_rows');

	$articles = $db->query("SELECT serial_numbers.serial_number, pim_articles.item_code, pim_articles.internal_name, pim_article_categories.name as product_name
							FROM serial_numbers
							LEFT JOIN pim_articles ON serial_numbers.article_id = pim_articles.article_id
							LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id
							WHERE $filter  ".$order_by." LIMIT ".$offset*$l_r.",".$l_r."");

	$j=0;
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	while($articles->move_next()){	

	$item=array(
		
					'serial_number'     	=> $articles->f('serial_number'),
					'item_code'		  		=> $articles->f('item_code'),				
					'article_name'			=> $articles->f('internal_name'),				
					'product_name' 			=> $articles->f('product_name'),	
					);
		
		array_push($result['query'], $item);
		$j++;
	}


	$result['categories_dd'] = getCategorieList();

	$result['lr']		= $l_r;
	$result['export_args'] = ltrim($arguments,'&');

	json_out($result);

	function getCategorieList()
	{
	
		$db = new sqldb();
		$db->query("SELECT id, name FROM pim_article_categories ORDER BY name ");
		$result=array();
		while($db->move_next()){
			$categories = array(
			'value'  => $db->f('name'),
			'id'=> $db->f('id')
			);
			  
			array_push($result, $categories);
		}
		return $result;
	}