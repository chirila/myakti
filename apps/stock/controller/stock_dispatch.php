<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db2 = new sqldb();
$db3 = new sqldb();

global $database_config;
$database = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
$dbu_users =  new sqldb($database);

if(!$in['stock_disp_id'])
{
	exit();
}
$page_title= gm("Stock Dispatching Info");

$is_inv_nr=false;


$db->query("SELECT  pim_stock_disp.* FROM  pim_stock_disp
			LEFT JOIN   pim_stock_disp_articles ON pim_stock_disp.stock_disp_id=pim_stock_disp_articles.stock_disp_id
			WHERE pim_stock_disp.stock_disp_id='".$in['stock_disp_id']."'");
if(!$db->move_next()){
	exit();
}


$in['lid'] = $db->f('email_language');

if(!$in['lid']){
	$in['lid'] = 1;
}
$customer_id = $db->f('customer_id');
$link_b_text = 'buyer_id';
$link_c_text = 'customer_name';
$to_customer_id=$db->f('to_customer_id');
$sent_date=$db->f('sent_date');
$from_customer_id = $db->f('from_customer_id');



$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
$factur_del_date = date(ACCOUNT_DATE_FORMAT,  $db->f('del_date'));
$currency = get_commission_type_list($db->f('currency_type'));

$serial_number = $db->f('serial_number');
$customer_name = $db->f('customer_name');






$default_lang = $db->f('email_language');
if(!$default_lang){
	$default_lang = 1;
}

$status='';
if($db->f('sent')==0){
	$status='0';
}else{
	if($db->f('invoiced')){
		if($db->f('rdy_invoice') == 1){
			$status='2';
		}else{
			$status='1';
		}
	}else{
		if($db->f('rdy_invoice')== 2){
			$status='1';
		}else if($db->f('rdy_invoice') == 1){
			$status='2';
		}else if($db->f('sent') == 1){
			$status='1';
		}
	}
}

$delivery_status = $db->f('rdy_invoice');
$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'stock_disp_note'
								   AND item_name = 'notes'
								   And active = '1'
								   AND item_id = '".$in['stock_disp_id']."' ");


$o = array(
	'default_lang'                                      =>$default_lang,
	'languages'					                        => $db->f('email_language'),

	'is_revert'						                    => $db->f('rdy_invoice') > 0  ? false : true,
	'link_c_text'										=> $link_c_text,
	'link_b_text'										=> $link_b_text,
	'factur_nr' 										=> $db->f('serial_number') ? $ref.$db->f('serial_number'): '',
	'factur_date' 									    => $factur_date,
	'is_sent'				        => $db->f('sent')==1 ? true : false,
	'selectedDraft'					=> $db->f('sent')==0 ? 'selected':'',
	'selectedSent'					=> $db->f('invoiced') ? ( $db->f('rdy_invoice') == 1 ? '' : 'selected') : ( $db->f('rdy_invoice')      == 2 ? 'selected' : ($db->f('rdy_invoice') == 1 ? '' : ($db->f('sent') == 1 ? 'selected' : '')  ) ),
	'selectedWon'					=> $db->f('invoiced') ? ( $db->f('rdy_invoice') == 1 ? 'selected' : '') : ( $db->f('rdy_invoice') == 2 ? '' : ($db->f('rdy_invoice') == 1 ? 'selected' : ($db->f('sent') == 1 ? '' : '' ) ) ),
	
	'status'						=> $status,
	'order_buyer_name'              => $db->f('field') == 'customer_id' ? $db->f('customer_name') : $db->f('customer_name')." > " ,
	
	'serial_number'			 				  => $db->f('serial_number'),

	'is_editable'							  => $db->f('rdy_invoice') > 0 ? false : true,

    'notes'	 	  		                      => $notes,
	'buyer_id'								  => $db->f('customer_id'),
	'p_order_id'							  => $in['p_order_id'],
	
	'your_ref'       						  => $db->f('your_ref'),
	'our_ref' 				  			      => $db->f('our_ref'),
	'is_delivered'							  => $delivery_status > 0 ? true : false ,
	'address_info'							  => nl2br($db->f('address_info')),
	'language_dd'	      					  => build_pdf_language_dd($in['lid'],1),
	'from_naming'           => $db->f('from_naming'),
	'from_address'          => $db->f('from_address'),
	'from_zip'              => $db->f('from_zip'),
	'from_city'             => $db->f('from_city'),
	'from_country'          => $db->f('from_country'),
	
	'to_naming'             => $db->f('to_naming'),
	'to_address'            => $db->f('to_address'),
	'to_zip'                => $db->f('to_zip'),
	'to_city'               => $db->f('to_city'),
	'to_country'            => $db->f('to_country'),

	'default_currency_val' => get_commission_type_list(ACCOUNT_CURRENCY_TYPE),
	'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
	'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
	'to_customer_id'		=> $to_customer_id,
	'is_active'				=> $db->f('active')=='1'? true:false,
	'is_archived'			=> $db->f('active')=='0'? true:false,
	
);

$nr_status=$db->f('rdy_invoice') ? ($db->f('rdy_invoice')==1 ? 'green' : 'orange') : ($db->f('sent') == 1 ? 'orange' : 'gray');

if($db->f('sent')==0 ){
    $type_title = gm('Draft');
}
if($db->f('invoiced')){
    if($db->f('rdy_invoice') == 1){
        $type_title = gm('Delivered');
    }else{
        $type_title = gm('Confirmed');
    }
}else{
    if($db->f('rdy_invoice') == 2){
        $type_title = gm('Confirmed');
    }else{
        if($db->f('rdy_invoice') == 1){
            $type_title = gm('Delivered');
        }else{
            if($db->f('sent') == 1){
                $type_title = gm('Confirmed');
            }else{
                $type_title = gm('Draft');
            }
        }
    }
}
if(!$db->f('active')){
	$type_title = gm('Archived');
	$nr_status='gray';
}
$o['type_title']=$type_title;
$o['nr_status']=$nr_status;

$view->assign("view_send","style='display:none;'");
$o['view_send']="style='display:none;'";

if($notes == ''){
	$o['hide_notes']="hide";

}

$i=0;
$fully_delivered = true;

$find_if_delivered=$db->query("SELECT  pim_stock_disp_articles.* FROM  pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND content='0' ORDER BY sort_order ASC");
while ($find_if_delivered->next()) {
	if(!$find_if_delivered->f('delivered')){
		$fully_delivered = false;
	}
	
}

	$db->query("SELECT pim_stock_disp_articles.* FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' ORDER BY sort_order ASC");
	
$o['lines']=array();
	while ($db->move_next())
	{
		$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));

		$line = array(
			//'article'  			    	=> nl2br($db->f('article')),
			'article'  			    	=> nl2br(stripslashes($db->f('article'))),
			//'quantity'      			=> display_number($db->f('quantity')),
           	'quantity'      			=> display_number($db->f('quantity'),$nr_dec_q),		
			'content'					=> $db->f('content') ? false : true,
			'content_class'				=> $db->f('content') ? ' last_nocenter ' : '',
			//'article_code'				=> $db->f('article_code'),
			'article_code'				=> stripslashes($db->f('article_code')),
			'is_article_code'			=> $db->f('article_code') ? true : false,
		);
       array_push($o['lines'], $line);

		//$view->loop('order_row');
		$i++;
	}



global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

if(!$in['include_pdf'])
{
	$in['include_pdf'] = 1;
}


global $cfg;
$message_data=get_sys_message('stock_disp_mess',$in['lid']);

$subject=$message_data['subject'];

$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
$subject=str_replace('[!SERIAL_NUMBER!]',$serial_number, $subject);
$subject=str_replace('[!DISPATCH_NOTE_DATE!]-',$factur_date, $subject);
$subject=str_replace('[!CUSTOMER!]',$customer_name, $subject);


$body=$message_data['text'];
$body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
$body=str_replace('[!SERIAL_NUMBER!]',$serial_number, $body);
$body=str_replace('[!DISPATCH_NOTE_DATE!]',$factur_date, $body);
$body=str_replace('[!CUSTOMER!]',$customer_name, $body);

    $o['include_pdf']  				 = $in['include_pdf'] ? true : false;
	$o['copy_checked']         = $in['copy'] ? 'checked="checked"' : '';
	$o['subject']              = $in['subject'] ? $in['subject'] : $subject;
	$o['e_message']            = $in['e_message'] ? $in['e_message'] : $body;
	$o['language_dd']	       = build_pdf_language_dd($in['lid'],1);
	$o['use_html']			   = $message_data['use_html'];


$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_p_orders.p_order_id,pim_p_orders.customer_id,pim_p_orders.serial_number
             FROM pim_p_orders
             INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_p_orders.customer_id
             WHERE pim_p_orders.p_order_id='".$in['p_order_id']."' AND customer_contacts.active ='1'");
$j = 0;


if($customer_id){
	
	$customer_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$customer_id."' ");
}
$jj=1;
$o['recipients']=array();
/*while ($tblinvoice_customer_contacts->move_next()){
	$in['recipients'][$tblinvoice_customer_contacts->f('contact_id')]=1;

	$recipient = array(
		'recipient_name'  			=> $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
		'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
		'recipient_id'    			=> $tblinvoice_customer_contacts->f('contact_id'),
		'recipients_checked'    	=> $in['recipients'][$tblinvoice_customer_contacts->f('contact_id')] ? 'checked="checked"' : '',

		'contact_email_data' 			=> $tblinvoice_customer_contacts->f('email'),
		'contact_id_data'				=> $tblinvoice_customer_contacts->f('contact_id'),
		'contact_name_data'			=> $customer_id ? $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname').' > '.$customer_name : $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
		
	);

	$j++;
	$jj++;
	array_push($o['recipients'], $recipient);
	
}*/
$o['recipients_ids']=array();
if($from_customer_id){
	$c_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$from_customer_id."' ");
	$invoice_email_type = $db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$from_customer_id."' ");

	$filter= " AND customers.customer_id='".$from_customer_id."'";

	$items = array();
	$items2 = array();
	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
				LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$from_customer_id."' ORDER BY lastname ")->getAll();

	$customer = $db->query("SELECT customers.name as name,customers.c_email as email
				FROM customers
				WHERE 1=1 $filter ");

	$items2 = array(
		    'id' => $customer->f('email'), 
		    'label' => $customer->f('name'),
		    'value' => $customer->f('name').' - '.$customer->f('email'),
	);
	if($invoice_email_type==1){
		$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
				LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$from_customer_id."'  ORDER BY lastname ")->getAll();
	}

	foreach ($contacts as $key => $value) {

		if($value['name']){
			$name = $value['firstname'].' '.$value['lastname'].' > '.$value['name'];
		}else{
			$name = $value['firstname'].' '.$value['lastname'];
		}
		$items[] = array(
		    'id' => $value['email'], 
		    'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
		    'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)).' - '.$value['email'],
		);
	}

	$customer_email = $db->field("SELECT c_email FROM customers WHERE customer_id='".$from_customer_id."'");
	if($customer_email){
		$cc_email = $customer_email;
	}

	if($cc_email){
		$o['recipients_ids'] = $cc_email;
	}

	$added = false;
	foreach ($items as $key => $value) {
		if($value['id'] == $items2['id']){
			$added = true;
		}
	}
	if(!$added){
		array_push($items, $items2);
	}
	if($invoice_email_type==1){
		array_push($items, $items2);
	}

	$o['recipients'] =$items;
}

if($j == 0){
	$o["hide_send_to"]="";
}
else{
	$o["hide_send_to"]="hide";
}

//sent box
$o["view_sent"]="style='display:none;'";




	


  	   
	

$l=0;

	  $o['pdf_del_type'] = $o['p_order_type']?$o['p_order_type']:ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
	  $o['generate_pdf_id']  	= '';
	  $o['hide_language']	   	= 'hide';
	  $o['pdf_link']          	= 'index.php?do=stock-stock_dispatch_print&stock_disp_id='.$in['stock_disp_id'].'&lid='.$in['lid'].'&type='.$o['pdf_del_type'];
	

$l=0;





$deliveries = $db->query("SELECT * FROM   pim_stock_disp_deliveries WHERE stock_disp_id='".$in['stock_disp_id']."' ORDER BY delivery_id DESC ");
$del_nr = $deliveries->records_count();
$j=0;
$o['deliveries']=array();
while ($deliveries->next()) {
$j++;
	$delivery = array(
		'delivey'					=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('date')),
		
		'delivery_link'				=> 'index.php?do=order-p_order_print&p_order_id='.$in['p_order_id'].'&lid='.$default_lang.'&type='.ACCOUNT_ORDER_DELIVERY_PDF_FORMAT.'&delivery_id='.$deliveries->f('delivery_id'),
		'del_delivery_link'			=> 'index.php?do=stock-stock_dispatch-stock_dispatch-delete_delivery&stock_disp_id='.$in['stock_disp_id'].'&del_nr='.$del_nr.'&delivery_id='.$deliveries->f('delivery_id'),
		'id'						=> $deliveries->f('delivery_id'),
		'active'					=> $j==1 && $delivery_status==1 ? 'active' : 'delivered',
		'del_title'					=> $delivery_status==1 ? gm('Received') : gm('Partially received'),
		'delete_delivery_title'		=> gm('Delete entry'),
		'delivery_id'				=> $deliveries->f('delivery_id'),
	);
	array_push($o['deliveries'], $delivery);
}
if($j && $delivery_status!=1){
	
          $o['are_delivery']=true ;
          $o['no_delivery']= false;
		
}else{
	      $o['are_delivery']=false ;
          $o['no_delivery']= true;
}
if($j){
	$o['no_delivery']= false;
}else{
	$o['no_delivery']= true;
}


if(!$in['s_date']){
	$in['s_date'] = time();
}


$is_extra = false;
if($customer_id){
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");

	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<h4 >'.gm('Bank Details').'</h4>
	<p><strong>'.gm('Name').': </strong> <span>'.$c_details->f('bank_name').'</span></p>
	<p><strong>'.gm('BIC Code').': </strong> <span>'.$c_details->f('bank_bic_code').'</span></p>
	<p><strong>'.gm('IBAN').': </strong> <span>'.$c_details->f('bank_iban').'</span></p>
	';
	}
	if($extra_info){
		$is_extra = true;
		$extra_info = '<div style="width: inherit;" >'.$extra_info.'</div>';
	}
}

// $customer_id = $db->field("SELECT customer_id from pim_p_orders WHERE p_order_id ='".$in['p_order_id']."' ");
$customer_data = $db->query("SELECT c_email, customer_id, name FROM customers WHERE customer_id = '".$customer_id."'");

//$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php';
if($customer_id){
	$customer_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$customer_id."' ");
	//$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php?customer_id='.$customer_id;
}
$id= $customer_id;
$is_contact = 0;



//$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);


    $o['s_date']							= $in['s_date'];
    $o['extra_info']						= $extra_info;
	$o['is_extra']							= $is_extra;
/*	$o['sent_date']				  	  		= date(ACCOUNT_DATE_FORMAT,$in['s_date']);*/
	$o['sent_date']				  	  		= $sent_date ? $sent_date*1000 : time()*1000;
	$o['hide_activity']			    		= $_SESSION['access_level'] == 1 ? '' : 'hide';
	$o['page_title']	    	    		= $page_title;
	$o['pick_date_format']      			= pick_date_format();
	$o['search']							= $in['search']? '&search='.$in['search'] : '';
	$o['view']								= $in['view']? '&view='.$in['view'] : '';
	
	$o['is_deliveredd']						= !$fully_delivered ? true : false;
	$o['customer_id']						=	$customer_data->f('customer_id');
	$o['c_email']							=	$customer_data->f('c_email');
	$o['is_c_email']						=	$customer_data->f('c_email') ? true : false;
	$o['is_customer']						= true;
	
	
	
	$o['user_email'] 						= $user_email;
	


json_out($o);
?>