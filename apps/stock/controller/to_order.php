<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db = new sqldb();
$db2 = new sqldb();



if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
$result = array('query_order'=>array(),'query_project'=>array(),'query_intervention'=>array());
$l_r = 30;


if(($in['ofs']) || (is_numeric($in['ofs'])))
{
  $in['offset']=$in['ofs'];
}


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
//orders
/*$orders = $db->query("SELECT pim_orders.*,SUM(pim_order_articles.quantity) as total_order,SUM(pim_orders_delivery.quantity) as total_deliver,pim_orders_delivery.order_articles_id,pim_order_articles.article_id 
                      FROM  pim_orders 
                      INNER JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id 
                      INNER JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id  AND pim_articles.supplier_id!=0
                      LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id 
                      WHERE  pim_orders.sent=1 AND pim_orders.rdy_invoice!='1' AND pim_orders.is_purchase=0
                      GROUP BY pim_orders.order_id");*/
  $orders = $db->query("SELECT pim_orders.*
                        FROM  pim_orders 
                         INNER JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id 
                        INNER JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id  AND pim_articles.supplier_id!=0
                        WHERE  pim_orders.sent=1 AND pim_orders.rdy_invoice!='1' AND pim_orders.is_purchase=0 AND pim_orders.active=1
                        GROUP BY pim_orders.order_id
                        ");     
while($orders->move_next()) {
   $total_order= $db2->field("SELECT SUM(pim_order_articles.quantity) FROM pim_order_articles WHERE order_id='".$orders->f('order_id')."' and article_id!=0");
    $total_deliver= $db2->field("SELECT SUM(pim_orders_delivery.quantity) FROM pim_orders_delivery WHERE order_id='".$orders->f('order_id')."'");
   
   if(($total_order-$total_deliver)!=0) {
    $item_order=array(
                     'id' => $orders->f('order_id'), 
                     'type' => 'orders',   
                     'serial_number' => $orders->f('serial_number'),
                     'customer' => $orders->f('customer_name'),
                     'created'               => date(ACCOUNT_DATE_FORMAT,$orders->f('date')),
                     'delivery_date'         => $orders->f('del_date')?date(ACCOUNT_DATE_FORMAT,$orders->f('del_date')):'',
                     'quantity' => $total_order-$total_deliver
                   
      );   
       array_push($result['query_order'], $item_order);
       $o++;
     }
}

//projects
/*$projects = $db->query("SELECT projects.*,project_articles.article_id,SUM(project_articles.quantity) as total_order,SUM(service_delivery.quantity) as total_deliver, service_delivery.a_id 
                      FROM  projects 
                      INNER JOIN project_articles ON projects.project_id=project_articles.project_id 
                      INNER JOIN pim_articles ON pim_articles.article_id=project_articles.article_id  AND pim_articles.supplier_id!=0
                      LEFT JOIN service_delivery ON project_articles.article_id=service_delivery.a_id and  project_articles.project_id=service_delivery.project_id
                      WHERE    projects.is_purchase=0
                      GROUP BY projects.project_id");*/
$projects = $db->query("SELECT projects.*,project_articles.article_id, service_delivery.a_id 
                      FROM  projects 
                      INNER JOIN project_articles ON projects.project_id=project_articles.project_id 
                      INNER JOIN pim_articles ON pim_articles.article_id=project_articles.article_id  AND pim_articles.supplier_id!=0
                      LEFT JOIN service_delivery ON project_articles.article_id=service_delivery.a_id and  project_articles.project_id=service_delivery.project_id
                      WHERE    projects.is_purchase=0
                      GROUP BY projects.project_id");

while($projects->move_next()) {

    $total_order= $db2->field("SELECT SUM(project_articles.quantity) FROM project_articles WHERE project_id='".$projects->f('project_id')."' and article_id!=0");
    $total_deliver= $db2->field("SELECT SUM(service_delivery.quantity) FROM service_delivery WHERE project_id='".$projects->f('project_id')."'");

   if($total_order-$total_deliver!=0){
    $item_project=array(
                     'id' => $projects->f('project_id'),  
                      'type' => 'projects',   
                     'serial_number' => $projects->f('serial_number'),
                     'customer' => $projects->f('company_name'),
                     'created'               => date(ACCOUNT_DATE_FORMAT,$projects->f('start_date')),
                     'delivery_date'         => $projects->f('del_date')?date(ACCOUNT_DATE_FORMAT,$projects->f('end_date ')):'',
                     'quantity' => $total_order-$total_deliver
                   
      );   
       array_push($result['query_project'], $item_project);
       $p++;
     }
}

//intervention
/*$interventions = $db->query("SELECT  servicing_support.*,servicing_support_articles.article_id,SUM(servicing_support_articles.quantity) as total_order,SUM(service_delivery.quantity) as total_deliver, service_delivery.a_id 
                      FROM  servicing_support 
                      INNER JOIN servicing_support_articles ON servicing_support.service_id=servicing_support_articles.service_id 
                      INNER JOIN pim_articles ON pim_articles.article_id=servicing_support_articles.article_id  AND pim_articles.supplier_id!=0
                      LEFT JOIN service_delivery ON servicing_support_articles.article_id=service_delivery.a_id and  servicing_support_articles.service_id=service_delivery.service_id
                      WHERE    servicing_support.is_purchase=0
                      GROUP BY servicing_support.service_id");*/
$interventions = $db->query("SELECT  servicing_support.*, service_delivery.a_id 
                      FROM  servicing_support 
                      INNER JOIN servicing_support_articles ON servicing_support.service_id=servicing_support_articles.service_id 
                      INNER JOIN pim_articles ON pim_articles.article_id=servicing_support_articles.article_id  AND pim_articles.supplier_id!=0
                      LEFT JOIN service_delivery ON servicing_support_articles.article_id=service_delivery.a_id and  servicing_support_articles.service_id=service_delivery.service_id
                      WHERE    servicing_support.is_purchase=0
                      GROUP BY servicing_support.service_id");

while($interventions->move_next()) {

    $total_order= $db2->field("SELECT SUM(servicing_support_articles.quantity) FROM servicing_support_articles WHERE service_id='".$interventions->f('service_id')."' and article_id!=0");
    $total_deliver= $db2->field("SELECT SUM(service_delivery.quantity) FROM service_delivery WHERE service_id='".$interventions->f('service_id')."'");

   if($total_order-$total_deliver!=0){
    $item_intervention=array(
                     'id' => $interventions->f('service_id'),  
                      'type' => 'interventions',   
                     'serial_number' => $interventions->f('serial_number'),
                     'customer' => $interventions->f('customer_name'),
                     'created'               => $interventions->f('planeddate')?date(ACCOUNT_DATE_FORMAT,$interventions->f('planeddate')):'',
                     
                     'quantity' => $total_order-$total_deliver
                   
      );   
       array_push($result['query_intervention'], $item_intervention);
       $i++;
     }
}


$result['number_o']		= $o;
$result['number_p']		= $p;
$result['number_i']		= $i;
$result['query_order'] = array_slice($result['query_order'], $offset*$l_r, $l_r);
$result['query_project'] = array_slice($result['query_project'], $offset*$l_r, $l_r);
$result['query_intervention'] = array_slice($result['query_intervention'], $offset*$l_r, $l_r);

$result['max_rows']   = max($o, $p, $i);

$result['lr']		= $l_r;
$result['show_settings']   = true;
$result['module_settings'] = array();

$easyinvoice= $db2->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
if($easyinvoice=='1'){
  $result['module_settings']['show_projects']      = false;
  $result['module_settings']['show_interventions']   = false;
}else{
  $result['module_settings']['show_projects']      = (in_array(19,perm::$allow_apps) && ($_SESSION['acc_type'] == 'service-free' || $_SESSION['acc_type']=='both' || $_SESSION['acc_type']=='services')) ? true : false;
  $result['module_settings']['show_interventions']   = in_array(13,perm::$allow_apps) ? true : false;
}
$result['module_settings']['not_easy_invoice_diy']=aktiUser::get('is_easy_invoice_diy') ? false : true;

json_out($result);

function get_create_po_articles(&$in,$showin=true,$exit=true){
  $db= new sqldb();
  $out=array('lines'=>array());

  $tmp_articles=[];
  if($in['order_ids'] && is_array($in['order_ids']) && !empty($in['order_ids'])){
    $order_ids="";
    foreach($in['order_ids'] as $order_id){
      $order_ids.="'".$order_id."',";
    }
    $order_ids=rtrim($order_ids,",");

    $articles = $db->query("SELECT pim_order_articles.* ,SUM(pim_orders_delivery.quantity) as q_deliver, pim_articles.item_code, pim_articles.internal_name,pim_articles.supplier_id, pim_articles_lang.name as article_name,pim_articles.supplier_reference
                        FROM  pim_order_articles  
                        LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
                        LEFT JOIN  pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
                        INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id 
                        WHERE pim_order_articles.order_id IN (".$order_ids.")
                        AND pim_order_articles.article_id!=0 AND pim_articles_lang.lang_id='".$_SESSION['lang_id']."'
                        GROUP BY pim_order_articles.`order_articles_id`");
    while ( $articles->move_next()) {
      if($articles->f('quantity')-$articles->f('q_deliver')!=0){
        if(array_key_exists($articles->f('article_id'), $tmp_articles) === false){
          $tmp_articles[$articles->f('article_id')]=array(
            'article_id'          => $articles->f('article_id'),
            'article_code'        => $articles->f('item_code'),
            'internal_name'       => $articles->f("internal_name"),
            'article_name'        => $articles->f("article_name"),
            'article'             => $articles->f('article'),
            'quantity'            => $articles->f('quantity')-$articles->f('q_deliver'),
            'supplier_id'         => $articles->f('supplier_id'),
            'supplier_reference'  => $articles->f('supplier_reference')
          );
        }else{
          $tmp_articles[$articles->f('article_id')]['quantity']+=$articles->f('quantity')-$articles->f('q_deliver');
        }     
      }
    }      
  }
  if($in['project_ids'] && is_array($in['project_ids']) && !empty($in['project_ids'])){
    $project_ids="";
    foreach($in['project_ids'] as $project_id){
      $project_ids.="'".$project_id."',";
    }
    $project_ids=rtrim($project_ids,",");

    $articles = $db->query("SELECT project_articles.* ,SUM(service_delivery.quantity) as q_deliver,pim_articles.item_code, pim_articles.internal_name,pim_articles.supplier_id, pim_articles_lang.name as article_name,pim_articles.supplier_reference
                          FROM  project_articles
                          LEFT JOIN pim_articles ON project_articles.article_id=pim_articles.article_id  
                          LEFT JOIN service_delivery ON project_articles.article_id=service_delivery.a_id and  project_articles.project_id=service_delivery.project_id
                          INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id
                          WHERE project_articles.project_id IN (".$project_ids.") AND project_articles.article_id!=0 AND pim_articles_lang.lang_id='".$_SESSION['lang_id']."'
                          GROUP BY project_articles.`a_id`");
    while ( $articles->move_next()) {
      if($articles->f('quantity')-$articles->f('q_deliver')!=0){
        if(array_key_exists($articles->f('article_id'), $tmp_articles) === false){
          $tmp_articles[$articles->f('article_id')]=array(
            'article_id'          => $articles->f('article_id'),
            'article_code'        => $articles->f('item_code'),
            'internal_name'       => $articles->f("internal_name"),
            'article_name'        => $articles->f("article_name"),
            'article'             => $articles->f('name'),
            'quantity'            => $articles->f('quantity')-$articles->f('q_deliver'),
            'supplier_id'         => $articles->f('supplier_id'),
            'supplier_reference'  => $articles->f('supplier_reference')
          );
        }else{
          $tmp_articles[$articles->f('article_id')]['quantity']+=$articles->f('quantity')-$articles->f('q_deliver');
        }
      }
    }
  }
  if($in['intervention_ids'] && is_array($in['intervention_ids']) && !empty($in['intervention_ids'])){
    $intervention_ids="";
    foreach($in['intervention_ids'] as $intervention_id){
      $intervention_ids.="'".$intervention_id."',";
    }
    $intervention_ids=rtrim($intervention_ids,",");
    $articles = $db->query("SELECT servicing_support_articles.* ,SUM(service_delivery.quantity) as q_deliver,pim_articles.item_code, pim_articles.internal_name,pim_articles.supplier_id, pim_articles_lang.name as article_name,pim_articles.supplier_reference
                        FROM  servicing_support_articles
                        LEFT JOIN pim_articles ON servicing_support_articles.article_id=pim_articles.article_id  
                        LEFT JOIN service_delivery ON servicing_support_articles.article_id=service_delivery.a_id and  servicing_support_articles.service_id=service_delivery.service_id
                        INNER JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id
                        WHERE servicing_support_articles.service_id IN (".$intervention_ids.") AND servicing_support_articles.article_id!=0 AND pim_articles_lang.lang_id='".$_SESSION['lang_id']."'
                        GROUP BY servicing_support_articles.`a_id`");
    while ( $articles->move_next()) {
      if($articles->f('quantity')-$articles->f('q_deliver')!=0){
        if(array_key_exists($articles->f('article_id'), $tmp_articles) === false){
          $tmp_articles[$articles->f('article_id')]=array(
            'article_id'          => $articles->f('article_id'),
            'article_code'        => $articles->f('item_code'),
            'internal_name'       => $articles->f("internal_name"),
            'article_name'        => $articles->f("article_name"),
            'article'             => $articles->f('name'),
            'quantity'            => $articles->f('quantity')-$articles->f('q_deliver'),
            'supplier_id'         => $articles->f('supplier_id'),
            'supplier_reference'  => $articles->f('supplier_reference')
          );
        }else{
          $tmp_articles[$articles->f('article_id')]['quantity']+=$articles->f('quantity')-$articles->f('q_deliver');
        }
      }
    }
  }
  if(!empty($tmp_articles)){
    foreach($tmp_articles as $article_id => $article_data){
      $final_data=$article_data;
      $in['supplier_id']=$final_data['supplier_id'];
      $final_data['supplier_dd']=get_cc($in,true,false);
      unset($in['supplier_id']);
      array_push($out['lines'],$final_data); 
    }
  }
  return $out;
}
function get_cc($in,$showin=true,$exit=true){
      
  $db = new sqldb();

  global $database_config;
  $database = array(
      'hostname' => $database_config['mysql']['hostname'],
      'username' => $database_config['mysql']['username'],
      'password' => $database_config['mysql']['password'],
      'database' => $database_config['user_db'],
      );
  $dbu_users =  new sqldb($database);

    $q = strtolower($in["term"]);
        
    $filter =" is_admin='0' AND customers.active=1 AND customers.is_supplier=1";

    if($q){
      $filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
    }
    if($in['supplier_id']){
      $filter .=" AND customers.customer_id='".$in['supplier_id']."' ";
    }

    $admin_licence = $dbu_users->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
    if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
      $filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
    }
    $cust = $db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
      FROM customers
      WHERE $filter     
      ORDER BY name
      LIMIT 5")->getAll();

    $result = array();
    foreach ($cust as $key => $value) {
      $cname = trim($value['name']);

      $result[]=array(
        "id"          => $value['customer_id'],
        "name"          => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
        "value"         => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
        "top"         => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
        "ref"           => $value['our_reference'],
        "currency_id"     => $value['currency_id'],
        "lang_id"         => $value['internal_language'],
        "identity_id"       => $value['identity_id'],
        'contact_name'      => $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
        'country'       => $value['country_name'] ? $value['country_name'] : '',
        'zip'         => $value['zip_name'] ? $value['zip_name'] : '',
        'city'          => $value['city_name'] ? $value['city_name'] : '',
        "bottom"        => $value['zip_name'].' '.$value['city'].' '.$value['country_name'],
        "right"         => $value['acc_manager_name']
      );
    }
    array_unshift($result,['id'=>'0','name'=>gm("Don't order"),'value'=>gm("Don't order")]);
    return json_out($result, $showin,$exit);
  }
