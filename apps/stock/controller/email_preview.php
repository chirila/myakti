<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $config;

if(defined('ACCOUNT_DATE_FORMAT') && ACCOUNT_DATE_FORMAT!=''){
    $date_format = ACCOUNT_DATE_FORMAT;
  }else{
    $date_format ='m/d/Y';
  }

$in['message'] = stripslashes($in['message']);
$order = $db->query("SELECT * FROM pim_stock_disp WHERE stock_disp_id='".$in['stock_disp_id']."'");
$contact = $db->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$order->f('contact_id')."'");
$title_cont = $db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
$customer = $db->field("SELECT name FROM customers WHERE customer_id='".$order->f('buyer_id')."'");
$identity_logo ='';
$logo = 'images/no-logo.png';
if($order->f('identity_id')){
	$identity_logo = $db->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$order->f('identity_id')."'");
}
if($identity_logo){
	$logo = $identity_logo;
}else{
	$print_logo = ACCOUNT_LOGO_ORDER;
	if ($print_logo) {
        $logo = $print_logo;
    }
}
$in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].$logo."\" alt=\"\">",$in['message']);
$in['message']=str_replace('[!CONTACT_FIRST_NAME!]',"".$contact->f('firstname')."",$in['message']);
$in['message']=str_replace('[!CONTACT_LAST_NAME!]',"".$contact->f('lastname')."",$in['message']);
$in['message']=str_replace('[!SALUTATION!]',"".$title_cont."",$in['message']);
$in['message']=str_replace('[!SERIAL_NUMBER!]',"".$order->f('serial_number')."",$in['message']);
$in['message']=str_replace('[!DATE!]',"".date($date_format, $order->f('date'))."",$in['message']);
$in['message']=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$in['message']);
$in['message']=str_replace('[!DUE_DATE!]',"".date($date_format, $order->f('del_date'))."",$in['message']);
$in['message']=str_replace('[!DELIVERY_DATE!]',"".date($date_format, $order->f('del_date'))."",$in['message']);
$in['message']=str_replace('[!YOUR_REFERENCE!]',"".$order->f('your_ref')."",$in['message']);

$result = array(
  'e_message' => ($in['send_email'] ? $in['message'] : nl2br($in['message']))
);
json_out($result);
?>