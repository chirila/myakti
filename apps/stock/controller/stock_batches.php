<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db = new sqldb();
$db2 = new sqldb();

if($in['reset_list']){
	if(isset($_SESSION['tmp_add_stock_batch'])){
		unset($_SESSION['tmp_add_stock_batch']);
	}
	if(isset($_SESSION['add_stock_batch'])){
		unset($_SESSION['add_stock_batch']);
	}	
}

$l_r = 30;
$order_by = " ORDER BY batches.batch_number ";
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$order_by_array = array('batch_number','internal_name','item_code','in_stock','date_exp');

$arguments = '';

if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

//FILTER LIST
$filter = " pim_articles.active='1' ";

if(!empty($in['search'])){
	$filter .= " AND (batches.batch_number LIKE '%".$in['search']."%' OR pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%') ";
	$arguments_s.="&search=".$in['search'];
}

$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
if(!empty($in['order_by']) && in_array($in['order_by'], $order_by_array)){ 
	$order = " ASC ";
	if($in['desc'] == '1' || $in['desc']=='true'){
	    $order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	$arguments_location.="&order_by=".$in['order_by']."&desc=".$in['desc'];   
}

$arguments = $arguments.$arguments_s;

$max_rows_data = $db->query("SELECT batches.id FROM batches
			INNER JOIN pim_articles ON batches.article_id=pim_articles.article_id
			WHERE $filter ");

$max_rows=$max_rows_data->records_count();

if(!$_SESSION['tmp_add_stock_batch'] || ($_SESSION['tmp_add_stock_batch'] && empty($_SESSION['tmp_add_stock_batch']))){
	while($max_rows_data->next()){	
		$_SESSION['tmp_add_stock_batch'][$max_rows_data->f('id')]=1;
	}
}

$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_stock_batch']){
	if($max_rows>0 && count($_SESSION['add_stock_batch']) == $max_rows){
		$all_pages_selected=true;
	}else if(count($_SESSION['add_stock_batch'])){
		$minimum_selected=true;
	}	
}

$batches = $db->query("SELECT batches.id,batches.batch_number,batches.date_exp,batches.in_stock,pim_articles.internal_name,pim_articles.item_code FROM batches
			INNER JOIN pim_articles ON batches.article_id=pim_articles.article_id
			WHERE $filter 
		   ".$order_by.$filter_limit);

$j=0;
$result = array('query'=>array(),'max_rows'=>$max_rows,'lr'=>$l_r,'all_pages_selected'=>$all_pages_selected,'minimum_selected'=>$minimum_selected);
while($batches->move_next()){
    
	$item=array(
		'id'				=> $batches->f('id'),
		'batch_number'		=> $batches->f('batch_number'),
		'name'		  		=> htmlspecialchars_decode($batches->f('internal_name')),
		'code'		  		=> $batches->f('item_code'),
		'in_stock'			=> display_number($batches->f('in_stock')),
		'date_exp'			=> $batches->f('date_exp') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$batches->f('date_exp')),
		'check_add_to_product'	=> $_SESSION['add_stock_batch'][$batches->f('id')] == 1 ? true : false,
	);
    array_push($result['query'], $item);
}

$result['export_args']  = ltrim($arguments,'&');

json_out($result);
