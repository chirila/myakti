<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

    $data = false;
	$o['lines'] 		= array();
	$i = 0;
	$j = 0;
	$k = 0;
	$line = $db->query("SELECT * FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND delivered='0' AND content='0' ORDER BY sort_order ASC ");
	while ($line->next()) {
		$delivered = 0;
		$left = 0;
		$delivered = $db->field("SELECT SUM(quantity) FROM pim_stock_disp_delivery WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$line->f('stock_disp_articles_id')."' ");
		$left = $line->f('quantity') - $delivered;

		$article_data = $db->query("SELECT * FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");

		$use_serial_no =$article_data->f('use_serial_no');
		$use_batch_no = $article_data->f('use_batch_no');

		if($use_serial_no==1){
			$j++;
		}

		if($use_batch_no==1){
			$k++;
		}

		$is_service = 0;
		$is_combined = 0;
		$is_service = $article_data->f('is_service'); 
		$is_combined = $article_data->f('use_combined'); 
		$hide_stock = $article_data->f('hide_stock'); 
		$stock = $article_data->f('stock'); 
		$left = $line->f('quantity') - $delivered;

		$linie=array(	'article'				=> $line->f('article'),
			'quantity'							=> display_number($left),
			'stock_disp_articles_id' 			=> $line->f('stock_disp_articles_id'),
			'use_serial_no_art'					=> $use_serial_no == 1 ? true : false,
			'use_batch_no_art'					=> $use_batch_no == 1 ? true : false,
			'code'								=> $line->f('article_code'),
			'is_error'								=> (!$is_combined && !$hide_stock && !$is_service  && $line->f('article_id') && ALLOW_STOCK) ? ($left > $article_data->f('stock') ? true : false) : false,
			);
		array_push($o['lines'],$linie);
		$i++;
	}
	if($i>0){
		$data = true;
	}
	$o['use_serial_no'] 	= $j > 0 && ALLOW_STOCK ? true : false;
	$o['use_batch_no'] 		= $k > 0 && ALLOW_STOCK ? true : false;

	$o['stock_disp_id']	   = $in['stock_disp_id'];
	$o['style']    = ACCOUNT_NUMBER_FORMAT;
	$o['is_data']	   = $data;


json_out($o);
?>