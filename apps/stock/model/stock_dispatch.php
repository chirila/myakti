<?php
class stock_dispatch
{
	var $db;
	var $db2;									# holds the database connection
	var $db_users;							# holds the database connection
	var $db_users2;
	var $pag = 'stock_disp';
	var $field_n = 'stock_disp_id';



	function stock_dispatch()
	{
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db = new sqldb();
		$this->db2 = new sqldb();
		$this->db_users = new sqldb($db_config);
		$this->db_users2 = new sqldb($db_config);
	}

	

	


	/****************************************************************
	* function add(&$in)                                          *
	****************************************************************/
	function add(&$in)
	{

       if(isset($in['date_h']) && !empty($in['date_h']) ){
			$in['date_h'] = strtotime($in['date_h']);
		}


		if(!$this->add_validate($in)){
			json_out($in);
			return false;
		}

		if ($in['rdy_invoice'])
		{
			$rdy_invoice = 1;
		}
		else
		{
			$rdy_invoice = 0;
		}

		if(!$in['notes']){
			$in['notes'] = $this->db->field("SELECT value FROM default_data WHERE type='stock_disp_note'");
        }

       $in['from_customer_id']=$in['buyer_id'];
       if($in['from_customer_id']==0){
       	$in['from_type']=1;
       	$in['from_customer_name']='';
       }else{
       	$in['from_type']=2;
       	$in['from_customer_name']=$in['customer_name'];
       }
        $in['from_address_id']=$in['delivery_address_id'];
        $in['from_address']=$in['customer_address'];
        $in['from_zip']=$in['customer_zip'];
        $in['from_city']=$in['customer_city'];
        $in['from_country']=get_country_name($in['customer_country_id']);
          
           $in['to_customer_id']=$in['to_buyer_id'];
           if($in['to_customer_id']==0){
       	$in['to_type']=1;
       	$in['to_customer_name']='';

       	$to_buyer_adr=$this->db->query("SELECT * FROM dispatch_stock_address WHERE customer_id = '0' and address_id='".$in['to_main_address_id']."'");

       	$in['to_address']=$to_buyer_adr->f('address');
        $in['to_zip']=$to_buyer_adr->f('zip');
		$in['to_city']=$to_buyer_adr->f('city');
		$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));
		$in['to_naming']=$to_buyer_adr->f('naming');

       }else{
       	$in['to_type']=2;
       	$in['to_customer_name']=$in['to_customer_name'];

       	$to_buyer_adr=$this->db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['to_customer_id']."' and address_id='".$in['to_main_address_id']."'");
       	$in['to_address']=$to_buyer_adr->f('address');
        $in['to_zip']=$to_buyer_adr->f('zip');
		$in['to_city']=$to_buyer_adr->f('city');
		$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));

       }
         
$in['to_address_id']=$in['to_main_address_id'];

		$in['stock_disp_id'] = $this->db->insert("INSERT INTO pim_stock_disp SET
															from_customer_id 			= '".$in['from_customer_id']."',
															from_type 				    = '".$in['from_type']."',
															from_address_id 		    = '".$in['from_address_id']."',
															from_customer_name 		    = '".$in['from_customer_name']."',

 															from_naming 			    = '".$in['from_naming']."',
															from_address				= '".$in['from_address']."',
															from_zip				    = '".$in['from_zip']."',
															from_city 		            = '".$in['from_city']."',
															from_country 		        = '".$in['from_country']."',


															to_customer_id 			    = '".$in['to_customer_id']."',
															to_type 				    = '".$in['to_type']."',
															to_address_id 		        = '".$in['to_address_id']."',
															to_customer_name 		    = '".$in['to_customer_name']."',

                                                            to_naming 			    = '".$in['to_naming']."',
															to_address				= '".addslashes($in['to_address'])."',
															to_zip				    = '".$in['to_zip']."',
															to_city 		        = '".addslashes($in['to_city'])."',
															to_country 		        = '".$in['to_country']."',


                                                            seller_name       			= '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                                                            seller_d_address     		= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
                                                			seller_d_zip       			= '".ACCOUNT_DELIVERY_ZIP."',
                                               			 	seller_d_city         		= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
                                                			seller_d_country_id   		= '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                                			seller_d_state_id     		= '".ACCOUNT_DELIVERY_STATE_ID."',
                                                			seller_bwt_nr         	    = '".ACCOUNT_VAT_NUMBER."',
                                                            delivery_id 			    = '".$in['delivery_address_id']."',
                                                            delivery_address 			= '".$in['delivery_address']."',
                                                			`date` 						= '".$in['date_h']."',
														    serial_number 				= '".$in['serial_number']."',
		                                                   	active						= '1',
		                                                    buyer_ref					= '".$in['buyer_ref']."',

		                                                    our_ref						= '".$in['our_ref']."',
		                                                    your_ref					= '".$in['your_ref']."',
		                                                    del_date					= '".$in['del_date']."',
		                                                    email_language				= '".$in['email_language']."'

		                                                    ");

		//add articles

		foreach ($in['tr_id'] as $nr => $row)
		{

			$in['stock_disp_articles_id'] = $this->db->insert("INSERT INTO pim_stock_disp_articles SET

																		 stock_disp_id = '".$in['stock_disp_id']."',
																		 article_id = '".$row['article_id']."',

																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".addslashes($row['article'])."',
																	     sort_order='".$nr."',
																	     article_code = '".$row['article_code']."',
																		 content = '".$row['content']."'
															");

				### for meyer app
				$in['meyer_lines_stock'][$nr]=$in['stock_disp_articles_id'];
				### for meyer app

        }

        // add multilanguage order notes in note_fields
		/*$lang_note_id = $in['langs'];
        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        foreach($in['translate_loop'] as $key => $value){
        		$in['notes_'.$value['lang_id']] = $value['notes'];
        }
        while($langs->next()){
        	if($lang_note_id == $langs->f('lang_id')){
        		$active_lang_note = 1;
        	}else{
        		$active_lang_note = 0;
        	}
        	$this->db->insert("INSERT INTO note_fields SET
        										item_id 				=	'".$in['stock_disp_id']."',
        										lang_id 				= 	'".$langs->f('lang_id')."',
        										active 					= 	'".$active_lang_note."',
        										item_type 				= 	'stock_disp_note',
        										item_name 				= 	'notes',
        										item_value				= 	'".$in['notes_'.$langs->f('lang_id')]."'
        	");
        }*/

        $this->db->insert("INSERT INTO note_fields SET
                        item_id           = '".$in['stock_disp_id']."',
                        lang_id           =   '".$in['email_language']."',
                        active            =   '1',
                        item_type         =   'stock_disp_note',
                        item_name         =   'notes',
                        item_value        =   '".$in['notes']."'
      	");

       // msg::$success = gm("Dispatch Note successfully added.");

        msg::success (gm("Dispatch Note successfully added"),'success');
		insert_message_log($this->pag,'{l}Dispatch Note successfully added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);

		

		// ark::run('order-stock_dispatching_info');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function add_meyer(&$in){
		$in['stock_disp_id'] = $this->db->field("SELECT stock_disp_id FROM pim_stock_disp WHERE adveo='1' ");
		if(!$in['stock_disp_id']){
			$this->add($in);
			$this->db->query("UPDATE pim_stock_disp SET adveo='1',active='123' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
			$in['sent_date'] = time();
			$this->mark_sent($in);
			return true;
		}else{
			$item = array();
			$s = $this->db->query("SELECT stock_disp_articles_id,article_id FROM `pim_stock_disp_articles` WHERE `stock_disp_id`='".$in['stock_disp_id']."' ");
			while($s->next()){
				$item[$s->f('article_id')] = $s->f('stock_disp_articles_id');
			}
			$s=null;
			foreach ($in['tr_id'] as $nr => $row)
			{
				// $in['stock_disp_articles_id'] = $this->db->field("SELECT stock_disp_articles_id FROM `pim_stock_disp_articles` WHERE `stock_disp_id`='".$in['stock_disp_id']."' AND article_id='".$in['article_id'][$nr]."' ");
				$in['stock_disp_articles_id'] = $item[$in['article_id'][$nr]];
				if(!array_key_exists($in['article_id'][$nr], $item)){
					$in['stock_disp_articles_id'] = $this->db->insert("INSERT INTO pim_stock_disp_articles SET
														 stock_disp_id = '".$in['stock_disp_id']."',
																		 article_id = '".$in['article_id'][$nr]."',
																		 quantity = '".return_value($in['quantity'][$nr])."',
																		 article = '".addslashes($in['article'][$nr])."',
																	     sort_order='".$nr."',
																	     article_code = '".$in['article_code'][$nr]."',
																		 content = '".$in['content'][$nr]."'
															");

					### for meyer app
					$in['meyer_lines_stock'][$nr]=$in['stock_disp_articles_id'];
					### for meyer app
				}else{
					$this->db->insert("UPDATE pim_stock_disp_articles SET
																		quantity = '".return_value($in['quantity'][$nr])."'
																		WHERE stock_disp_articles_id='".$in['stock_disp_articles_id']."' ");
					$in['meyer_lines_stock'][$nr]=$in['stock_disp_articles_id'];
				}

			}
			$item = null;
			return true;
		}
	}
	/****************************************************************
	* function add_validate(&$in)                                          *
	****************************************************************/
	function add_validate(&$in)
	{
		$v = new validation($in);
		$v->field('serial_number', 'Name', 'required');
		if (ark::$method == 'add') {
			$v->field('serial_number', 'Serial Number', 'unique[pim_stock_disp.serial_number]');
			$v->field('delivery_address_id','From Address','required');
			$v->field('to_main_address_id','To Address','required');
		}else{
			$v->field('serial_number', "Serial Number", "unique[pim_stock_disp.serial_number.( stock_disp_id!='".$in['stock_disp_id']."')]");
		}

		//$v->field('customer_name', "Company Name", "required");

		return $v->run();
	}

	/****************************************************************
	* function update(&$in)                                          *
	****************************************************************/
	function update(&$in)
	{

		if(!$this->update_validate($in)){
			json_out($in);
			return false;
		}
		
		if(isset($in['date_h']) && !empty($in['date_h']) ){
			$in['date_h'] = strtotime($in['date_h']);
		}

		if ($in['rdy_invoice']){
			$rdy_invoice = 1;
		}else{
			$rdy_invoice = 0;
		}

		$in['from_customer_id']=$in['buyer_id'];
	    if($in['from_customer_id']==0){
	       	$in['from_type']=1;
	       	$in['from_customer_name']='';     	

	       	$buyer_adr=$this->db->query("SELECT * FROM dispatch_stock_address WHERE customer_id = '0' and address_id='".$in['main_address_id']."'");

	       	$in['from_address']=$buyer_adr->f('address');
	        $in['from_zip']=$buyer_adr->f('zip');
			$in['from_city']=$buyer_adr->f('city');
			$in['from_country']=get_country_name($buyer_adr->f('country_id'));
			$in['from_naming']=$buyer_adr->f('naming');		       	

	    }else{
	    	$buyer_adr=$this->db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['main_address_id']."'");

        	$in['from_address']=$buyer_adr->f('address');
        	$in['from_zip']=$buyer_adr->f('zip');
        	$in['from_city']=$buyer_adr->f('city');
        	$in['from_country']=get_country_name($buyer_adr->f('country_id'));

        	$buyer_info = $this->db->query("SELECT customers.name FROM customers WHERE customer_id='".$in['from_customer_id']."' ");

        	$in['from_naming']=$buyer_info->f('name');
        	$in['from_type']=2;
        	$in['from_customer_name']=stripslashes($buyer_info->f('name'));

	    }
        $in['from_address_id']=$in['main_address_id'];       
	          
	    $in['to_customer_id']=$in['to_buyer_id'];
	    if($in['to_customer_id']==0){

	       	$in['to_type']=1;
	       	$in['to_customer_name']='';

	       	$to_buyer_adr=$this->db->query("SELECT * FROM dispatch_stock_address WHERE customer_id = '0' and address_id='".$in['to_main_address_id']."'");

	       	$in['to_address']=stripslashes($to_buyer_adr->f('address'));
	        $in['to_zip']=$to_buyer_adr->f('zip');
			$in['to_city']=stripslashes($to_buyer_adr->f('city'));
			$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));
			$in['to_naming']=stripslashes($to_buyer_adr->f('naming'));

	    }else{
	       	$in['to_type']=2;

	       	$to_buyer_adr=$this->db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['to_customer_id']."' and address_id='".$in['to_main_address_id']."'");
	       	$in['to_address']=stripslashes($to_buyer_adr->f('address'));
	        $in['to_zip']=$to_buyer_adr->f('zip');
			$in['to_city']=stripslashes($to_buyer_adr->f('city'));
			$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));

			$buyer_info = $this->db->query("SELECT customers.name FROM customers WHERE customer_id='".$in['to_customer_id']."' ");
			$in['to_naming']=stripslashes($buyer_info->f('name'));
			$in['to_customer_name']=stripslashes($buyer_info->f('name'));
	    }
	    $in['to_address_id']=$in['to_main_address_id'];

		$this->db->query("UPDATE pim_stock_disp SET
			from_customer_id      = '".$in['from_customer_id']."',
                  from_type             = '".$in['from_type']."',
                  from_address_id         = '".$in['from_address_id']."',
                  from_customer_name        = '".addslashes($in['from_customer_name'])."',
                  from_naming           = '".addslashes($in['from_naming'])."',
                  from_address        = '".addslashes($in['from_address'])."',
                  from_zip            = '".$in['from_zip']."',
                  from_city                 = '".addslashes($in['from_city'])."',
                  from_country            = '".$in['from_country']."',
                  to_customer_id          = '".$in['to_customer_id']."',
                  to_type             = '".$in['to_type']."',
                  to_address_id             = '".$in['to_address_id']."',
                  to_customer_name        = '".addslashes($in['to_customer_name'])."',
                  to_naming           = '".addslashes($in['to_naming'])."',
                  to_address        = '".addslashes($in['to_address'])."',
                  to_zip            = '".$in['to_zip']."',
                  to_city             = '".addslashes($in['to_city'])."',
                  to_country            = '".$in['to_country']."',
                    rdy_invoice 		= '".$rdy_invoice."',
                    buyer_ref 			= '".$in['buyer_ref']."',
                    seller_name       	= '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                    seller_d_address    = '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
        			seller_d_zip       	= '".ACCOUNT_DELIVERY_ZIP."',
       			 	seller_d_city       = '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
        			seller_d_country_id = '".ACCOUNT_DELIVERY_COUNTRY_ID."',
        			seller_d_state_id   = '".ACCOUNT_DELIVERY_STATE_ID."',
        			seller_bwt_nr       = '".ACCOUNT_VAT_NUMBER."',
				    `date` 				= '".$in['date_h']."',
                    serial_number 		= '".$in['serial_number']."',
                    delivery_id			= '".$in['delivery_address_id']."',
                    our_ref				= '".$in['our_ref']."',
                    your_ref			= '".$in['your_ref']."',
                    del_date			= '".$in['del_date']."',
                    delivery_address 	= '".$in['delivery_address']."'

                    WHERE stock_disp_id='".$in['stock_disp_id']."' ");


		$this->db->query("DELETE FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."'");

		foreach ($in['tr_id'] as $nr => $row)
		{

			/*$in['stock_disp_articles_id'] = $this->db->insert("INSERT INTO pim_stock_disp_articles SET

																		 stock_disp_id = '".$in['stock_disp_id']."',
																		 article_id = '".$in['article_id'][$nr]."',

																		 quantity = '".return_value($in['quantity'][$nr])."',
																		 article = '".addslashes($in['article'][$nr])."',
																	     sort_order='".$nr."',
																		 content = '".$in['content'][$nr]."',
																		 article_code = '".$in['article_code'][$nr]."'
															");*/
			$in['stock_disp_articles_id'] = $this->db->insert("INSERT INTO pim_stock_disp_articles SET

																		 stock_disp_id = '".$in['stock_disp_id']."',
																		 article_id = '".$row['article_id']."',

																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".addslashes($row['article'])."',
																	     sort_order='".$nr."',
																		 content = '".$row['content']."',
																		 article_code = '".$row['article_code']."'
															");															




		}

		$exist_note = $this->db->field("SELECT note_fields_id FROM note_fields WHERE active='1' AND item_type='stock_disp_note' AND item_name='notes' AND lang_id='".$in['email_language']."' AND item_id='".$in['stock_disp_id']."'");
		if($exist_note){
			//Update other entries as inactive
    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'0'
    								WHERE 	item_type 				= 	'stock_disp_note'
    								AND		item_name 				= 	'notes'
    								AND		item_id 				= 	'".$in['stock_disp_id']."'"
    								);

    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'1',
    										item_value				= 	'".$in['notes']."'
    								WHERE 	item_type 				= 	'stock_disp_note'
    								AND		item_name 				= 	'notes'
    								AND		lang_id 				= 	'".$in['email_language']."'
    								AND		item_id 				= 	'".$in['stock_disp_id']."'

    		");
    	}else{
    		//Update other entries as inactive
    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'0'
    								WHERE 	item_type 				= 	'stock_disp_note'
    								AND		item_name 				= 	'notes'
    								AND		item_id 				= 	'".$in['stock_disp_id']."'"
    								);

    		$this->db->insert("INSERT INTO note_fields SET
    										item_id 				=	'".$in['stock_disp_id']."',
    										lang_id 				= 	'".$in['email_language']."',
    										active 					= 	'1',
    										item_type 				= 	'stock_disp_note',
    										item_name 				= 	'notes',
    										item_value				= 	'".$in['notes']."'
    		");
    	}

		// update multilanguage order note in note_fields
		/*$lang_note_id = $in['langs'];
        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        foreach($in['translate_loop'] as $key => $value){
        	$in['notes_'.$value['lang_id']] = $value['notes'];
        }
        while($langs->next()){
        	if($lang_note_id == $langs->f('lang_id')){
        		$active_lang_note = 1;
        	}else{
        		$active_lang_note = 0;
        	}

        		$this->db->query("UPDATE note_fields SET
        										active 					= 	'".$active_lang_note."',
        										item_value				= 	'".$in['notes_'.$langs->f('lang_id')]."'
        								WHERE 	item_type 				= 	'stock_disp_note'
        								AND		item_name 				= 	'notes'
        								AND		lang_id 				= 	'".$langs->f('lang_id')."'
        								AND		item_id 				= 	'".$in['stock_disp_id']."'

        		");


        }*/



		msg::success(gm("Dispatch Note successfully updated"),'success');
		//ark::$controller = 'stock_dispatching';
		//ark::run('order-stock_dispatching');
		insert_message_log($this->pag,'{l}Dispatch Note successfully updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);
        //ark::$controller = 'stock_dispatching_info';
		return true;
	}

	/**
	 * Make the order able to be invoiced
	 * Only this orders will show when you invoice by order
	 *
	 * @param array $in
	 * @return true
	 */
	function ready_to_invoice(&$in){
		$is_order = $this->db->field("SELECT order_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		if(!$is_order){
			msg::$success = "Invalid Id";
			return false;
		}

		$this->db->query("UPDATE pim_orders SET rdy_invoice='1' WHERE order_id='".$in['order_id']."' ");
		msg::$success = "Order successfully edited";
		insert_message_log($this->pag,'{l}Order marked as ready for invoice by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;

		return true;
	}

	/**
	 * Revert an order
	 * making it unable to be invoiced
	 *
	 * @param array $in
	 * @return true
	 */
	function revert(&$in){
		$is_order = $this->db->field("SELECT order_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		if(!$is_order){
			msg::$success = "Invalid Id";
			return false;
		}

		$this->db->query("UPDATE pim_orders SET rdy_invoice='0', invoiced='0' WHERE order_id='".$in['order_id']."' ");
		msg::$success = "Order successfully edited";
		insert_message_log($this->pag,'{l}Order reverted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;

		return true;

	}

	/****************************************************************
	* function update_validate(&$in)                                          *
	****************************************************************/
	function update_validate(&$in)	{

		$v = new validation($in);
		$v->field('stock_disp_id', 'ID', 'required:exist[pim_stock_disp.stock_disp_id]', "Invalid Id");
		if(!$v->run()){
			return false;
		} else {
			return $this->add_validate($in);
		}
	}
	/****************************************************************
	* function delete(&$in)                                          *
	****************************************************************/
	function delete(&$in)
	{
		if(!$this->delete_stock_disp_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM pim_stock_disp WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		$this->db->query("DELETE FROM pim_stock_disp_articles  WHERE stock_disp_id='".$in['stock_disp_id']."'");


       $this->db->query("SELECT * FROM pim_stock_disp_delivery  WHERE stock_disp_id='".$in['stock_disp_id']."'");
       while ($this->db->move_next()) {
           $this->db2->query("DELETE FROM  dispatch_stock_movements  WHERE stock_disp_delivery_id='".$this->db->f('stock_disp_delivery_id')."'");
       }

		$this->db->query("DELETE FROM pim_stock_disp_delivery  WHERE stock_disp_id='".$in['stock_disp_id']."'");
		$this->db->query("DELETE FROM pim_stock_disp_deliveries  WHERE stock_disp_id='".$in['stock_disp_id']."'");
       	$this->db->query("DELETE FROM note_fields WHERE item_type = 'stock_disp_note' AND item_name = 'notes' AND item_id = '".$in['stock_disp_id']."'");

		msg::$success =gm("Dispatch Note has been deleted");
		insert_message_log($this->pag,'{l}Dispatch Note deleted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);
		return true;
	}
/****************************************************************
	* function set_main(&$in)                                          *
	****************************************************************/
	function set_main(&$in)
	{
		if(!$this->set_main_validate($in)){
			return false;
		}
        $this->db->query("UPDATE  dispatch_stock_address SET is_default='0' ");
      	$this->db->query("UPDATE  dispatch_stock_address SET is_default='1' WHERE address_id='".$in['address_id']."' ");


		msg::$success =gm("Main Depot has been set");

		return true;
	}



	/**
	 * Archiver
	 *
	 * @param unknown_type $in
	 * @return unknown
	 */
	function archive(&$in){
		if(!$this->delete_stock_disp_validate($in)){
			return false;
		}
		$this->db->query("UPDATE pim_stock_disp SET active='0' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
        msg::success(gm("Dispatch Note has been archived"),'success');

		insert_message_log($this->pag,'{l}Dispatch Note has been archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);
		return true;
	}

	/**
	 * Activate
	 *
	 * @param array $in
	 * @return true
	 */
	function activate(&$in){
		if(!$this->delete_stock_disp_validate($in)){
			return false;
		}

        $this->db->query("UPDATE pim_stock_disp SET active='1' WHERE stock_disp_id='".$in['stock_disp_id']."'");
        msg::success(gm("Dispatch Note has been activated"),'success');
       

		insert_message_log($this->pag,'{l}Dispatch Note has been activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);
		return true;
	}

	/****************************************************************
	* function delete_stock_disp_validate(&$in)                                          *
	****************************************************************/
	function delete_stock_disp_validate(&$in)
	{
		$v = new validation($in);
		$v->field('stock_disp_id', 'ID', 'required:exist[pim_stock_disp.stock_disp_id]', "Invalid Id");

		return $v->run();
	}
/****************************************************************
	* function set_main_validate(&$in)                                          *
	****************************************************************/
	function set_main_validate(&$in)
	{
		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[dispatch_stock_address.address_id]', "Invalid Id");

		return $v->run();
	}



	/**
	 * Syncronize orders
	 * update customer info
	 *
	 * @param array $in
	 * @return true
	 */
	function sync_order(&$in){
		$order = $this->db->query("SELECT customer_id,field FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		$order->next();

		$d = array('table' => $order->f('field') == 'customer_id' ? 'customer_addresses' : 'customer_contact_address' ,
		'field' => $order->f('field') == 'customer_id' ? 'customer_id' : 'contact_id',
		'value' => $order->f('customer_id') ,
		'filter' => $order->f('field') == 'customer_id' ? " AND billing='1' " : '',
		);

		$buyer_details = $this->db->query("SELECT * FROM ".$d['table']." WHERE ".$d['field']."='".$d['value']."' ".$d['filter']);
		$buyer_details->next();

		if($order->f('field') == 'customer_id'){
			$buyer_info = $this->db->query("SELECT customers.payment_term, customers.btw_nr, customers.c_email, customer_legal_type.name as l_name, customers.name as c_name,customers.our_reference
															FROM customers
															LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
															WHERE customers.customer_id = '".$d['value']."' ");
			$buyer_info->next();
			$name = $buyer_info->f('c_name').' '.$buyer_info->f('l_name');
			$c_email = $buyer_info->f('c_email');
			$c_fax = $buyer_details->f('comp_fax');
			$c_phone = $buyer_details->f('comp_phone');
			$ref = $buyer_info->f('our_reference');
		}else{
			$buyer_info = $this->db->query("SELECT phone, cell, email, firstname, lastname FROM customer_contacts WHERE ".$d['field']."='".$d['value']."' ");
			$buyer_info->next();
			$c_email = $buyer_info->f('email');
			$c_fax = '';
			$c_phone = $buyer_info->f('phone');
			$name = $buyer_info->f('firstname').' '.$buyer_info->f('lastname');
			$ref = '';
		}

		$update_order = $this->db->query("UPDATE pim_orders SET customer_name='".addslashes($name)."',
																								 customer_country_id='".addslashes($buyer_details->f('country_id'))."',
																								 customer_city='".addslashes($buyer_details->f('city'))."',
																								 customer_zip='".addslashes($buyer_details->f('zip'))."',
																								 customer_address='".addslashes($buyer_details->f('address'))."',
																								 customer_email='".addslashes($c_email)."',
																								 customer_phone='".addslashes($c_phone)."'
																						 WHERE order_id='".$in['order_id']."' ");
		msg::$success = "Order successfully updated.";
		insert_message_log($this->pag,'{l}Order successfully updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		return true;
	}

	/**
	 * make delivery
	 *
	 * @return bool
	 * @author me
	 **/
	function make_delivery(&$in)
	{
		if(!$this->make_delivery_validate($in)){
			return false;
		}
		
		$now = time();
		$disp_info=$this->db->query("SELECT pim_stock_disp.* FROM pim_stock_disp WHERE pim_stock_disp.stock_disp_id='".$in['stock_disp_id']."'");

		$del = $this->db->insert("INSERT INTO pim_stock_disp_deliveries SET stock_disp_id='".$in['stock_disp_id']."', date='".$now."' ");
		$this->db->query("UPDATE pim_stock_disp SET rdy_invoice='2' WHERE stock_disp_id='".$in['stock_disp_id']."' ");


		
		//foreach ($in['quantity'] as $key => $value) {
		foreach ($in['lines'] as $key => $value) {
			if(return_value($value['quantity']) != '0.00'){



				$q = return_value($value['quantity']);
				$line = $this->db->field("SELECT quantity FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$value['stock_disp_articles_id']."' AND content='0' ");
				$article_id=$this->db->field("SELECT article_id from pim_stock_disp_articles WHERE stock_disp_articles_id='".$value['stock_disp_articles_id']."' ");
				$delivered = $this->db->field("SELECT sum(quantity) FROM pim_stock_disp_delivery WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$value['stock_disp_articles_id']."' ");

				# update deliveries
				$this->db->query("INSERT INTO pim_stock_disp_delivery SET stock_disp_id='".$in['stock_disp_id']."' , stock_disp_articles_id='".$value['stock_disp_articles_id']."', quantity='".$q."', delivery_id='".$del."', delivery_note='".$in['delivery_note']."' ");

				$delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_stock_disp_delivery WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$value['stock_disp_articles_id']."' ");
				# delivery complete
				if($delivered_all > $line || $delivered_all == $line ){
					$this->db->query("UPDATE pim_stock_disp_articles SET delivered='1' WHERE stock_disp_articles_id='".$value['stock_disp_articles_id']."' ");
				}
                //select from adress stock info
                $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."'");
				//update dispach from stock
				$this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock-$q)."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."'");

				//select to adress stock info
                $to_address_current_stock=$this->db->query("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."'");
				//for to address we have to see if there are some stock if not add info
				if($to_address_current_stock->move_next()){
					//update dispach from stock
				$this->db->query("UPDATE dispatch_stock SET stock='".($to_address_current_stock->f('stock')+$q)."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."'");
				}else{
					if($disp_info->f('to_customer_id')){
					   $this->db->insert("INSERT INTO  dispatch_stock SET stock='".$q."' , article_id='".$article_id."' ,  address_id='".$disp_info->f('to_address_id')."',main_address=0 , customer_id='".$disp_info->f('to_customer_id')."'");
				      }else{
				      	$this->db->insert("INSERT INTO  dispatch_stock SET stock='".$q."' , article_id='".$article_id."' ,  address_id='".$disp_info->f('to_address_id')."',main_address=1 , customer_id='".$disp_info->f('to_customer_id')."'");
				      }
				}

				if($value['use_serial_no_art'] == 1){
						$status_details_2 = $disp_info->f('serial_number');
						// $selected_s_n = explode(',', $in['selected_s_n'][$key]);

						foreach ($value['selected_s_n'] as $key2 => $v) {
							$this->db->query("UPDATE serial_numbers
										SET 	sd_delivery_id 	= '".$del."',
												address_id = '".$disp_info->f('to_address_id')."'
										WHERE id = '".$v['serial_nr_id']."' ");
						}

					}

				if($value['use_batch_no_art'] == 1){
					
						foreach ($value['selected_b_n'] as $key2 => $v) {
							if($v['batch_no_checked']){
								$this->db->query("INSERT INTO batches_from_orders
										SET sd_articles_id='".$v['stock_disp_articles_id']."',
										  	sd_delivery_id='".$del."',
										  	article_id='".$v['article_id']."',
										  	batch_id ='".$v['b_n_id']."',
										  	quantity='".return_value($v['b_n_q_selected'])."'

									 ");

								if($disp_info->f('from_customer_id')=='0'){
									//select from adress stock info batches
					                $from_address_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."' and batch_id='".$v['b_n_id']."'");

					                if($from_address_current_stock_batch->move_next()){
									//update dispach batch from stock
										$this->db->query("UPDATE batches_dispatch_stock SET quantity='".($from_address_current_stock_batch->f('quantity')-return_value($v['b_n_q_selected']))."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."' and batch_id='".$v['b_n_id']."'");
									}else{
										$this->db->insert("INSERT INTO  batches_dispatch_stock SET quantity='".return_value($v['b_n_q_selected'])*(-1)."' , article_id='".$article_id."' ,  address_id='".$disp_info->f('from_address_id')."' , customer_id='".$disp_info->f('from_customer_id')."'  , batch_id='".$v['b_n_id']."'");
									}
								}

								if($disp_info->f('to_customer_id')=='0'){
									//select to adress stock info
					                $to_address_current_stock_batch=$this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."' and batch_id='".$v['b_n_id']."'");
									//for to address we have to see if there are some stock if not add info
									if($to_address_current_stock_batch->move_next()){
										//update dispach from stock
										$this->db->query("UPDATE batches_dispatch_stock SET quantity='".($to_address_current_stock_batch->f('quantity')+return_value($v['b_n_q_selected']))."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."' and batch_id='".$v['b_n_id']."'");
									}else{
										
										   $this->db->insert("INSERT INTO  batches_dispatch_stock SET quantity='".return_value($v['b_n_q_selected'])."' , article_id='".$article_id."' ,  address_id='".$disp_info->f('to_address_id')."' , customer_id='".$disp_info->f('to_customer_id')."'  , batch_id='".$v['b_n_id']."'");
									      
									}
								}
								 $in_stock_batch = $this->db->field("SELECT sum(quantity) FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  customer_id='0' and batch_id='".$v['b_n_id']."'");

								 $this->db->query("UPDATE batches SET in_stock='".$in_stock_batch."' WHERE article_id='".$article_id."' AND id='".$v['b_n_id']."'");
							}
							
						}//end foreach

					} // end if use batch

			}
		}


		$articles = $this->db->field("SELECT count(stock_disp_articles_id) FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND content='0' ");
		$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND content='0' ");

		if($articles == $articles_delivered){
			$this->db->query("UPDATE pim_stock_disp SET rdy_invoice='1' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		}
		insert_message_log($this->pag,'{l}Delivery made by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);
		return true;

	}


	function make_delivery_meyer(&$in){
		$delivery = $this->db->field("SELECT delivery_id FROM  `pim_stock_disp_deliveries` WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		if(!$delivery){
			$this->make_delivery($in);
			$this->db->query("UPDATE pim_stock_disp_articles SET delivered='1' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
			return true;
		}
		$arts = $this->db->query("SELECT pim_stock_disp_articles.stock_disp_articles_id,pim_stock_disp_articles.quantity
						FROM pim_stock_disp_articles
						LEFT JOIN pim_stock_disp_delivery on pim_stock_disp_articles.stock_disp_articles_id=pim_stock_disp_delivery.stock_disp_articles_id
						WHERE pim_stock_disp_delivery.stock_disp_articles_id is NULL AND pim_stock_disp_articles.stock_disp_id='".$in['stock_disp_id']."'");
		while ($arts->next()) {
				$this->db->query("INSERT INTO pim_stock_disp_delivery SET
									stock_disp_id='".$in['stock_disp_id']."' ,
									stock_disp_articles_id='".$arts->f('stock_disp_articles_id')."',
									quantity='".$arts->f('quantity')."',
									delivery_id='".$delivery."' ");

		}
		$this->db->query("UPDATE pim_stock_disp_delivery SET quantity=(SELECT pim_stock_disp_articles.quantity FROM pim_stock_disp_articles WHERE pim_stock_disp_articles.stock_disp_articles_id=pim_stock_disp_delivery.stock_disp_articles_id),delivered='1' WHERE stock_disp_id='".$in['stock_disp_id']."' ");

		/*$disp_delivery=array();
		$disp_deliverys = $this->db->query("SELECT stock_disp_delivery_id,stock_disp_articles_id FROM pim_stock_disp_delivery WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		while($disp_deliverys->next()) {
			$disp_delivery[$disp_deliverys->f('stock_disp_articles_id')] =  $disp_deliverys->f('stock_disp_delivery_id');
			// array_push($disp_delivery, $disp_deliverys->f('stock_disp_delivery_id'));
		}
		$disp_deliverys=null;
		foreach ($in['quantity'] as $key => $value) {
			$q = return_value($value);
			//$disp_delivery = $this->db->field("SELECT stock_disp_delivery_id FROM pim_stock_disp_delivery WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$key."' ");
			if(!array_key_exists($key, $disp_delivery)){
				$this->db->query("INSERT INTO pim_stock_disp_delivery SET stock_disp_id='".$in['stock_disp_id']."' , stock_disp_articles_id='".$key."', quantity='".$q."', delivery_id='".$delivery."' ");
			}else{
				$this->db->query("UPDATE pim_stock_disp_delivery SET quantity='".$q."' WHERE stock_disp_articles_id='".$key."' AND delivery_id='".$delivery."' AND stock_disp_delivery_id='".$disp_delivery."' ");
			}
		}
		$disp_delivery=null;*/
		// $this->db->query("UPDATE pim_stock_disp_articles SET delivered='1' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		return true;
	}

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author me
	 **/
	function make_delivery_validate(&$in)
	{
		$v = new validation($in);
		$v->field('stock_disp_id', 'ID', 'required:exist[pim_stock_disp.stock_disp_id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author me
	 **/
	function mark_as_delivered(&$in)
	{
		if(!$this->delete_stock_disp_validate($in)){
			return false;
		}
		/*if(ALLOW_STOCK){
				$info = $this->db->query("SELECT article_id, quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."'  AND content='0'  ");
				while ($info->next())
			{
				$stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id=".$info->f('article_id'));
				$stock = $stock - $info->f('quantity');
				$this->db->query("UPDATE pim_articles SET stock=".$stock." WHERE article_id=".$info->f('article_id'));
			}
				}*/

		$this->db->query("UPDATE pim_stock_disp_articles SET delivered='1' WHERE stock_disp_id='".$in['stock_disp_id']."' AND content='0' ");
		$this->db->query("UPDATE pim_stock_disp SET rdy_invoice='1' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
	}

/**
	 * make entry
	 *
	 * @return bool
	 * @author me
	 **/
	function make_entry(&$in)
	{
		if(!$this->make_entry_validate($in)){
			return false;
		}

			Sync::start(ark::$model.'-'.ark::$method);
		$now = time();
		$del = $this->db->insert("INSERT INTO pim_p_order_deliveries SET p_order_id='".$in['p_order_id']."', date='".$now."' ");
		$this->db->query("UPDATE pim_p_orders SET rdy_invoice='2' WHERE p_order_id='".$in['p_order_id']."' ");
		foreach ($in['quantity'] as $key => $value) {
			if(return_value($value) != '0.00'){
				if(ALLOW_STOCK){
				$article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
				$article_data = $this->db->query("SELECT item_code, internal_name, stock FROM pim_articles WHERE article_id='".$article_id."' ");
				$article_data->next();
				$stock = $article_data->f("stock");
				// $stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id='".$article_id."'");
				$new_stock = $stock + return_value($in['quantity'][$key]);
				$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
				/*start for  stock movements*/
				$backorder_articles = return_value($in['old_quantity'][$key]) - return_value($in['quantity'][$key]);
				$serial_number = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
	        	$this->db->insert("INSERT INTO stock_movements SET
	        													date 						=	'".$now."',
	        													created_by 					=	'".$_SESSION['u_id']."',
	        													p_order_id 					= 	'".$in['p_order_id']."',
	        													serial_number 				= 	'".$serial_number."',
	        													article_id 					=	'".$article_id."',
	        													article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
	        													item_code  					= 	'".addslashes($article_data->f("item_code"))."',
	        													movement_type				=	'6',
	        													quantity 					= 	'".return_value($in['quantity'][$key])."',
	        													stock 						=	'".$stock."',
	        													new_stock 					= 	'".$new_stock."',
	        													backorder_articles 			= 	'".$backorder_articles."',
	        													delivery_date 			='".$now."'
	        	");
		        /*end for  stock movements*/
				}

				# check to see if the value is bigger than what we have
				$q = return_value($value);
				$line = $this->db->field("SELECT quantity FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' AND content='0' ");
				$delivered = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' ");
				if( $q+$delivered > $line){
					//$q = $line - $delivered;
				}
				# update deliveries
				$this->db->query("INSERT INTO pim_p_orders_delivery SET p_order_id='".$in['p_order_id']."' , order_articles_id='".$key."', quantity='".$q."', delivery_id='".$del."', delivery_note='".$in['delivery_note']."' ");

				$delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' ");
				# delivery complete
				if($delivered_all > $line || $delivered_all == $line ){
					$this->db->query("UPDATE pim_p_order_articles SET delivered='1' WHERE order_articles_id='".$key."' ");
				}
			}
		}
        Sync::end();

		$articles = $this->db->field("SELECT count(order_articles_id) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' ");
		$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' ");

		if($articles == $articles_delivered){
			$this->db->query("UPDATE pim_p_orders SET rdy_invoice='1' WHERE p_order_id='".$in['p_order_id']."' ");
		}
		insert_message_log($this->pag,'{l}Entry made by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);


	}

	/**
	 * validate endtry
	 *
	 * @return bool
	 * @author me
	 **/
	function make_entry_validate(&$in)
	{
		$v = new validation($in);
		$v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
		$sum_of_delivery_items = 0;
		foreach ($in['quantity'] as $key => $value) {
			if(return_value($value)<0) {
				return false;
			}
			$sum_of_delivery_items += (return_value($value));
		}
		if($sum_of_delivery_items==0){
			return false;
		}else{
		return $v->run();
		}
	}

	

	

	function delete_delivery(&$in){
		if(!$this->delete_delivery_validate($in)){
			return false;
		}

		
        $disp_info=$this->db->query("SELECT pim_stock_disp.* FROM pim_stock_disp WHERE pim_stock_disp.stock_disp_id='".$in['stock_disp_id']."'");
        $articles = $this->db->query("SELECT * FROM pim_stock_disp_delivery WHERE stock_disp_id ='".$in['stock_disp_id']."' AND delivery_id='".$in['delivery_id']."' ");
		while($articles->next()){
			if(ALLOW_STOCK){
			    $q= $articles->f('quantity');

			     $article_id=$this->db->field("SELECT article_id from pim_stock_disp_articles WHERE stock_disp_articles_id='".$articles->f('stock_disp_articles_id')."' ");
			    //select from adress stock info
                $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."'");
				//update dispach from stock
				$this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock+$q)."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."'");

				//select to adress stock info
                $to_address_current_stock=$this->db->query("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."'");
				//for to address we have to see if there are some stock if not add info
				if($to_address_current_stock->move_next()){
					//update dispach from stock
					$this->db->query("UPDATE dispatch_stock SET stock='".($to_address_current_stock->f('stock')-$q)."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."'");
				}


			    $this->db->query("DELETE FROM pim_stock_disp_delivery WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$articles->f('stock_disp_articles_id')."' AND delivery_id='".$articles->f('delivery_id')."' ");
				$this->db->query("UPDATE pim_stock_disp_articles SET delivered = '0' WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$articles->f('stock_disp_articles_id')."' ");


				$this->db->field("UPDATE serial_numbers
						SET 	date_out 		= '0',
								sd_delivery_id 	= '0',
								status_details_2 	= '',
								status_id 		= 1,
								address_id = '".$disp_info->f('from_address_id')."'
						WHERE sd_delivery_id = '".$in['delivery_id']."' ");

				 $batches = $this->db->query("SELECT * FROM batches_from_orders WHERE sd_articles_id='".$articles->f('stock_disp_articles_id')."' AND sd_delivery_id='".$articles->f('delivery_id')."' ");
				while($batches->next()){

					$qty = $batches->f('quantity');

					if($disp_info->f('from_customer_id')=='0'){

						 $from_address_current_stock_batch = $this->db->field("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."' and batch_id='".$batches->f('batch_id')."'");
						//update dispach from stock
						$this->db->query("UPDATE batches_dispatch_stock SET quantity='".($from_address_current_stock_batch+$qty)."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('from_address_id')."' and customer_id='".$disp_info->f('from_customer_id')."'  and batch_id='".$batches->f('batch_id')."'");
					}

					if($disp_info->f('from_customer_id')=='0'){
						//select to adress stock info
		                $to_address_current_stock_batch=$this->db->query("SELECT  quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."'  and batch_id='".$batches->f('batch_id')."'");
						//for to address we have to see if there are some stock if not add info
						if($to_address_current_stock_batch->move_next()){
							//update dispach from stock
							$this->db->query("UPDATE batches_dispatch_stock SET quantity='".($to_address_current_stock_batch->f('quantity')-$qty)."' WHERE article_id='".$article_id."' AND  address_id='".$disp_info->f('to_address_id')."' and customer_id='".$disp_info->f('to_customer_id')."'  and batch_id='".$batches->f('batch_id')."'");
						}
					}

					 $in_stock_batch = $this->db->field("SELECT sum(quantity) FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  customer_id='0' and batch_id='".$batches->f('batch_id')."'");

					 $this->db->query("UPDATE batches SET in_stock='".$in_stock_batch."' WHERE article_id='".$article_id."' AND id='".$batches->f('batch_id')."'");
				}
				$this->db->query("DELETE FROM batches_from_orders WHERE sd_articles_id='".$articles->f('stock_disp_articles_id')."' AND sd_delivery_id='".$articles->f('delivery_id')."' ");

			}
		}


		$this->db->query("DELETE FROM pim_stock_disp_deliveries WHERE stock_disp_id='".$in['stock_disp_id']."' AND delivery_id='".$in['delivery_id']."' ");

		if($in['del_nr']==1){
			$this->db->query("UPDATE pim_stock_disp SET rdy_invoice='0' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		}else{
			$this->db->query("UPDATE pim_stock_disp SET rdy_invoice='2' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		}
		
		msg::success(gm('Delivery was succesfully deleted'),'success');

   }

	function delete_delivery_validate(&$in){

		$v = new validation($in);
		$v->field('stock_disp_id', 'ID', 'required:exist[pim_stock_disp.stock_disp_id]', "Invalid Id");
		$v->field('delivery_id', 'ID', 'required:exist[pim_stock_disp_deliveries.delivery_id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * Send order by email
	 *
	 * @return bool
	 * @author Mp
	 **/
	function generate_pdf(&$in)
	{
		include_once(__DIR__.'/../controller/stock_dispatch_print.php');
	}
	/**
	 * Send order by email
	 *
	 * @return bool
	 * @author Mp
	 **/
		function pdf_settings(&$in)
		{
		if(!($in['stock_disp_id']&&$in['pdf_layout']&&$in['logo']))
			{
				return false;
			}
		$this->db->query("UPDATE pim_stock_disp SET pdf_layout='".$in['pdf_layout']."' WHERE stock_disp_id = '".$in['stock_disp_id']."' ");
		//console::log($this->dbu->query("UPDATE tblinvoice SET pdf_layout='".$in['pdf_layout']."' WHERE invoice_id = '".$in['invoice_id']."' "));
		$this->db->query("UPDATE pim_stock_disp SET pdf_logo='".$in['logo']."' WHERE stock_disp_id = '".$in['stock_disp_id']."' ");
		//console::log($this->dbu->query($this->dbu->query("UPDATE tblinvoice SET pdf_logo='".$in['logo']."' WHERE invoice_id = '".$in['invoice_id']."' "));

	  	msg::$success = gm("Pdf settings have been updated");
	  	return true;
		}
	 function sendNew(&$in){
      if(!$this->sendNewEmailValidate($in)){
        msg::error ( gm('Invalid email address').' - '.$in['new_email'],'error');
        json_out($in);
        return false;
      }
      $this->db->query("SELECT pim_stock_disp.pdf_logo, pim_stock_disp.pdf_layout
                        FROM pim_stock_disp
                        WHERE pim_stock_disp.stock_disp_id ='".$in['stock_disp_id']."'");
      $this->db->move_next();
      if($this->db->f('pdf_layout')){
        $in['type']=$this->db->f('pdf_layout');
        $in['logo']=$this->db->f('pdf_logo');
      }else {
        $in['type'] = ACCOUNT_ORDER_PDF_FORMAT;
        }
      $this->generate_pdf($in);
      $mail = new PHPMailer();
      $mail->WordWrap = 50;
      $def_email = $this->default_email();
      $body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');
      $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
      $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
      $subject=$in['e_subject'];
      $mail->Subject = $subject;
      if($in['include_pdf']){
         $mail->AddAttachment("stock_dispatching_.pdf", $in['serial_number'].'.pdf'); // attach files/invoice-user-1234.pdf, and rename it to invoice.pdf
      }
      

      if($in['use_html']){
        $mail->IsHTML(true);
        $head = '<style>p { margin: 0px; padding: 0px; }</style>';
        $body = stripslashes($in['e_message']);
       
        $mail->Body = $head.$body;
      }else{
        $mail->MsgHTML(nl2br(($body)));
      }

        $this->db->query("SELECT customer_contacts.*,pim_stock_disp.stock_disp_id,pim_stock_disp.customer_id,pim_stock_disp.serial_number,pim_stock_disp.pdf_logo, pim_stock_disp.pdf_layout
                          FROM pim_stock_disp
                          INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_stock_disp.to_customer_id
                          WHERE pim_stock_disp.stock_disp_id ='".$in['stock_disp_id']."'");
        $this->db->move_next();
      if($in['new_email_id'] && $in['new_email_id'] == $this->db->f('email')){
        $mail->AddAddress(trim($this->db->f('email')),$this->db->f('firstname').' '.$this->db->f('lastname'));
      }else{
        $mail->AddAddress(trim($in['new_email']));
      }
      $sent_date= time();
      $this->db->query("UPDATE pim_stock_disp SET sent='1' WHERE stock_disp_id='".$in['stock_disp_id']."'");
      $mail->Send();
      if($mail->IsError()){
        msg::error ( $mail->ErrorInfo,'error');
        json_out($in);
        return false;
      }
      if($mail->ErrorInfo){
        msg::notice( $mail->ErrorInfo,'notice');
      }
      /*if($mail->ErrorInfo){
        msg::error( $mail->ErrorInfo,'error');
        json_out($in);
        return false;
      }*/
      msg::success( gm('Stock Dispatch has been successfully sent'),'success');
    
      $msg_log= '{l}Dispatch note has been successfully sent by{endl} '.get_user_name($_SESSION['u_id']).'{l}to{endl}'.$in['new_email'];

      if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
	      update_log_message($in['logging_id'],' '.$in['new_email']);
	    }else{
	      $in['logging_id'] = insert_message_log($this->pag,$msg_log,$this->field_n,$in['stock_disp_id']);
	    }

      json_out($in);
      return true;
    }
    function sendNewEmailValidate(&$in)
    {
      $v = new validation($in);
      $in['new_email'] = trim($in['new_email']);
      $v->field('new_email', 'Email', 'email');
      return $v->run();
    }



	/**
	 * Mark as sent
	 *
	 * @return bool
	 * @author Mp
	 **/
	function mark_sent(&$in){
		$in['s_date']=time();

		if(!$this->mark_sent_validate($in))
		{
			return false;
		}

		$this->db->query("UPDATE pim_stock_disp SET sent='1',sent_date='".$in['s_date']."' WHERE stock_disp_id 	='".$in['stock_disp_id']."'");

		//$in['act'] = '';
        msg::success( gm('Dispatch note has been successfully marked as ready to deliver'),'success');
		insert_message_log($this->pag, '{l}Dispatch note has been successfully marked as sent by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);
		$in['pagl'] = $this->pag;
		return true;
	}
	function mark_as_draft(&$in)
	{
		if(!$this->mark_sent_validate($in)){

			return false;
		}

		$this->db->query("UPDATE pim_stock_disp SET sent='0',sent_date='' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
	}
	/****************************************************************
	* function send_validate(&$in)                                *
	****************************************************************/
	function send_validate(&$in)
	{
		$v = new validation($in);
		$v->field('stock_disp_id', 'ID', 'required:exist[pim_stock_disp.stock_disp_id]', gm('Invalid ID'));
		$is_ok = $v->run();
		if(!$in['recipients']){
			if(!$in['send_to'] && !$in['copy']){
				$in['error'].= 'Please add one recipient'.".<br>";
				return false;
			}
		}

		return $is_ok;
	}
	/****************************************************************
	* function mark_sent_validate(&$in)                                *
	****************************************************************/
	function mark_sent_validate(&$in)
	{

        $v = new validation($in);
		$v->field('stock_disp_id', 'ID', 'required:exist[pim_stock_disp.stock_disp_id]', gm('Invalid ID'));
		//$v->field('s_date','Sent date','required');
		return $v->run();
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		/*if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
			$array['from']['email'] = ACCOUNT_EMAIL;
		}*/
		if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
			if(MAIL_SETTINGS_PREFERRED_OPTION==2){
				if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
					$array['from']['email'] = MAIL_SETTINGS_EMAIL;
				}
			}else{
				if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
					$array['from']['email'] = ACCOUNT_EMAIL;
				}
			}
		}
		$this->db->query("SELECT * FROM default_data WHERE type='invoice_email' ");
		//if($this->db->move_next()){
		if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
			if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)) && $this->db->f('value')!=''){
				$array['from']['email'] = $this->db->f('value');
			}
		}	
		return $array;
	}

	function edit_delivery(&$in)
	{

		if(!$this->edit_delivery_validate($in)){
			return false;
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$now = time();
		$this->db->query("UPDATE pim_stock_disp SET rdy_invoice='2' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		$j=0;
		foreach ($in['quantity'] as $key => $value) {
			if(return_value($value)){
				$q = return_value($value);
				$this->db->query("UPDATE pim_stock_disp_articles SET delivered='0' WHERE stock_disp_articles_id='".$key."' ");
				# check to see if the value is bigger than what we have

				$this->db->query("UPDATE pim_stock_disp_delivery SET quantity='".$q."' , delivery_note='".$in['delivery_note']."' WHERE stock_disp_id='".$in['stock_disp_id']."' AND delivery_id='".$in['delivery_id']."' AND stock_disp_articles_id='".$key."' ");

				$delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_stock_disp_delivery WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$key."' ");
				$line = $this->db->field("SELECT quantity FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND stock_disp_articles_id='".$key."' AND content='0' ");
				# delivery complete
				if($delivered_all > $line || $delivered_all == $line ){
					$this->db->query("UPDATE pim_stock_disp_articles SET delivered='1' WHERE stock_disp_articles_id='".$key."' ");
				}
			}
			$j++;
		}
        Sync::end($in['stock_disp_id']);

		$articles = $this->db->field("SELECT count(stock_disp_articles_id) FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND content='0' ");
		$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' AND content='0' ");

		if($articles == $articles_delivered){
			$this->db->query("UPDATE pim_stock_disp SET rdy_invoice='1' WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		}
		insert_message_log($this->pag,'{l}Delivery edited by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['stock_disp_id']);
		return true;

	}

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author me
	 **/
	function edit_delivery_validate(&$in)
	{

		$v = new validation($in);
		$v->field('stock_disp_id', 'ID', 'required:exist[pim_stock_disp.stock_disp_id]', "Invalid Id");
		$v->field('delivery_id', 'ID', 'required:exist[pim_stock_disp_deliveries.delivery_id]', "Invalid Id");

		$sum_of_delivery_items = 0;

		foreach ($in['quantity'] as $key => $value) {
			if(return_value($value)<0) {
				return false;
			}
			$sum_of_delivery_items += (return_value($value));
		}
		if($sum_of_delivery_items==0){
			return false;
		}else{
			return $v->run();
		}
	}

	function edit_entry(&$in)
	{
		if(!$this->edit_entry_validate($in)){
			return false;
		}

			Sync::start(ark::$model.'-'.ark::$method);
		$now = time();
		$this->db->query("UPDATE pim_p_orders SET rdy_invoice='2' WHERE p_order_id='".$in['p_order_id']."' ");
		$j=0;
		foreach ($in['quantity'] as $key => $value) {
			if(return_value($value)){
				if(ALLOW_STOCK){

				$article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
				$article_data = $this->db->query("SELECT item_code, internal_name, stock FROM pim_articles WHERE article_id='".$article_id."' ");
				$article_data->next();
				$stock = $article_data->f("stock");
				$reset_stock = $stock - return_value($in['seted_quantity'][$key]);
				$new_stock = $reset_stock + return_value($in['quantity'][$key]);
				$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
				}
				$this->db->query("UPDATE pim_p_order_articles SET delivered='0' WHERE order_articles_id='".$key."' ");
				# check to see if the value is bigger than what we have
				$q = return_value($value);
				$line = $this->db->field("SELECT quantity FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' AND content='0' ");
				$delivered = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' ");
				if( $q+$delivered > $line){
					//$q = $line - $delivered;
				}
				# update deliveries
				$this->db->query("UPDATE pim_p_orders_delivery SET quantity='".$q."' , delivery_note='".$in['delivery_note']."' WHERE p_order_id='".$in['p_order_id']."' AND delivery_id='".$in['delivery_id']."' AND order_articles_id='".$key."' ");

				$delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' ");
				# delivery complete
				if($delivered_all > $line || $delivered_all == $line ){
					$this->db->query("UPDATE pim_p_order_articles SET delivered='1' WHERE order_articles_id='".$key."' ");
				}

				if(ALLOW_STOCK){
					if($in['old_quantity'][$key] != $in['quantity'][$key]){
						/*start for  stock movements*/
						$backorder_articles = $line - $delivered_all;
						$serial_number = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
			        	$this->db->insert("INSERT INTO stock_movements SET
			        													date 						=	'".$now."',
			        													created_by 					=	'".$_SESSION['u_id']."',
			        													p_order_id 					= 	'".$in['p_order_id']."',
			        													serial_number 				= 	'".$serial_number."',
			        													article_id 					=	'".$article_id."',
			        													article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
			        													item_code  					= 	'".addslashes($article_data->f("item_code"))."',
			        													movement_type				=	'10',
			        													quantity 					= 	'".return_value($in['quantity'][$key])."',
			        													stock 						=	'".$stock."',
			        													new_stock 					= 	'".$new_stock."',
			        													backorder_articles 			= 	'".$backorder_articles."',
			        													delivery_date 			='".$now."'
			        	");
				        /*end for  stock movements*/
				    }
		        }
			}
		}
        Sync::end();

		$articles = $this->db->field("SELECT count(order_articles_id) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' ");
		$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' ");

		if($articles == $articles_delivered){
			$this->db->query("UPDATE pim_p_orders SET rdy_invoice='1' WHERE p_order_id='".$in['p_order_id']."' ");
		}
		insert_message_log($this->pag,'{l}Entry made by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);


	}

	/**
	 * validate endtry
	 *
	 * @return bool
	 * @author me
	 **/
	function edit_entry_validate(&$in)
	{
		$v = new validation($in);
		$v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
		$v->field('delivery_id', 'ID', 'required:exist[pim_p_order_deliveries.delivery_id]', "Invalid Id");
		$sum_of_delivery_items = 0;
		foreach ($in['quantity'] as $key => $value) {
			if(return_value($value)<0) {
				return false;
			}
			$sum_of_delivery_items += (return_value($value));
		}
		if($sum_of_delivery_items==0){
			return false;
		}else{
		return $v->run();
		}
	}

	function order_note_add(&$in){

		if($in['notes']){
			$this->db->query("INSERT INTO default_data SET default_name='order note', value='".utf8_encode($in['notes'])."', type='order_note' ");
			msg::$success = gm("Note added").'.';
		}

		if($in['account_order_start']){
			$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_ORDER_START' ");
			if($this->db->move_next()){
				$this->db->query("UPDATE settings SET value='".$in['account_order_start']."' where constant_name='ACCOUNT_ORDER_START' ");
			}else{
				$this->db->query("INSERT INTO settings SET value='".$in['account_order_start']."', constant_name='ACCOUNT_ORDER_START' ");
			}
			msg::$success = gm("Settings updated").'.';
		}

		if($in['inc_part_o']){
			if(!defined('ACCOUNT_ORDER_DIGIT_NR')){
				$this->db->query("INSERT INTO settings SET value='".$in['inc_part_o']."', constant_name='ACCOUNT_ORDER_DIGIT_NR', module='billing' ");
				msg::$success = gm("Setting added.");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['inc_part_o']."' WHERE constant_name='ACCOUNT_ORDER_DIGIT_NR'");
				msg::$success = gm("Settings updated").'.';
			}
		}

		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_ORDER_REF' ");
		if($in['show_ref']){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_ORDER_REF' ");
		}
		$this->db->query("UPDATE settings SET value='".$in['ref_delimiter']."' WHERE constant_name='ACCOUNT_ORDER_DEL' ");
		return true;
	}

	function order_note_update(&$in){
		if($in['notes']){
			$this->db->query("UPDATE default_data SET value='".utf8_encode($in['notes'])."' WHERE type='order_note' AND default_main_id='0' ");
			msg::$success = gm("Note updated").'.';
		}

		if($in['account_order_start']){
			$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_ORDER_START' ");
			if($this->db->move_next()){
				$this->db->query("UPDATE settings SET value='".$in['account_order_start']."' WHERE constant_name='ACCOUNT_ORDER_START' ");
			}else{
				$this->db->query("INSERT INTO settings SET value='".$in['account_order_start']."', constant_name='ACCOUNT_ORDER_START' ");
			}
			msg::$success = gm("Settings updated").'.';
		}

		if($in['inc_part_o']){
			if(!defined('ACCOUNT_ORDER_DIGIT_NR')){
				$this->db->query("INSERT INTO settings SET value='".$in['inc_part_o']."', constant_name='ACCOUNT_ORDER_DIGIT_NR', module='billing' ");
				msg::$success = gm("Setting added.");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['inc_part_o']."' WHERE constant_name='ACCOUNT_ORDER_DIGIT_NR'");
				msg::$success = gm("Settings updated").'.';
			}
		}

		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_ORDER_REF' ");
		if($in['show_ref']){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_ORDER_REF' ");
		}
		$this->db->query("UPDATE settings SET value='".$in['ref_delimiter']."' WHERE constant_name='ACCOUNT_ORDER_DEL' ");
		return true;
	}

	function default_message(&$in)
	{
		if($in['use_html']){
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['stock_dispatch_subject']."',
					html_content	= '".$in['stock_dispatch_text']."'
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		}else{
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['stock_dispatch_subject']."',
					text	= '".$in['stock_dispatch_text']."'
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		}
		msg::$success.= gm("Message has been successfully updated");
		return true;
	}

	/**
	 * Activate output language
	 *
	 * @param array $in
	 * @return true
	 */

	/****************************************************************
	* function account_setting_update_validate(&$in)                                          *
	****************************************************************/
	function account_setting_update_validate(&$in)
	{
		$is_ok = true;

		return $is_ok;
	}

	function uploadify(&$in)
	{
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = $in['path'];
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
				if($in['folder']){
					global $database_config;

					$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => $in['folder'],
					);
					$db_upload = new sqldb($database_2);
				}
				$db_upload->query("UPDATE settings SET
	                           value = 'upload/".$in['folder']."/".$in['name']."'
	                           WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
			} else {
				echo gm('Invalid file type.');
			}
		}
	}

	function upload_file_o(&$in)
	{
		global $_FILES, $is_live;
		if(!$_FILES){
			return false;
		}

		$allowed['.gif']=1;
		$allowed['.jpg']=1;
		$allowed['.jpeg']=1;
		$f_ext = substr($_FILES['picture']['name'],strrpos($_FILES['picture']['name'],"."));
		if(!$allowed[strtolower($f_ext)])
		{
			msg::$error=gm("Only jpg, jpeg and gif files are accepted.");
			return false;
		}

		if(!is_numeric($_SESSION['u_id'])){
			msg::$error.=gm("Error. you are not allowed to upload files").'<br>';
			return false;
		}

		$f_title = "o_logo_img_".DATABASE_NAME.$f_ext;
		$f_out = '../../'.UPLOAD_PATH.DATABASE_NAME."/".$f_title;

		if(!$_FILES['picture']['tmp_name']){
			msg::$error.=gm("Please upload a file")."<br>";
			return false;
		}

		if(!$is_live || ($f_ext =='.gif')){
			if(FALSE === move_uploaded_file($_FILES['picture']['tmp_name'],$f_out)){
				// $in['error'].="Unable to upload the file.  Move operation failed."."<!-- Check file permissions -->";
				return false;
			}

		}else{

			$filename = $_FILES['picture']['tmp_name'];

			/*$size = getimagesize($filename);
			if($size[0] > 500){
				msg::$notice .= gm("Image too big. The image size should be under 500 pixels.");
				return false;
			}*/
			move_uploaded_file($_FILES['picture']['tmp_name'],$f_out);
			$this->db->query("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_ORDER'");
			if(!$this->db->move_next())
			{
				msg::$error.=gm("Error.").'<br>';
				return false;
			}
			else
			{
				@unlink(UPLOAD_PATH.DATABASE_NAME."/".$this->db->f('value'));
				$this->db->query("UPDATE settings SET value = '' WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
			}
			/*
			$get_width = $size[0];
			$get_height = $size[1];
			$ratio = 250 / 77;
			if($get_width/$get_height > $ratio ){
				$this->resize($_FILES['picture']['tmp_name'], 250, 0, $f_title);
			}else{
				$this->resize($_FILES['picture']['tmp_name'], 0, 77, $f_title);
			}
			*/
			@chmod($f_out, 0664);
		}

		$this->db->query("UPDATE settings SET
	                           value = '".UPLOAD_PATH.DATABASE_NAME."/".$f_title."'
	                           WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
		@chmod($f_out, 0664);
		return true;
	}



	/****************************************************************
	* function label_update(&$in)                                         *
	****************************************************************/
	function label_update(&$in)
	{
		$fields = '';
		$filter = '';
		$table = 'label_language';
		if($in['type'] == '2'){
			$table = 'label_language_quote';
			// source  ='".$in['source']."',
			// type  ='".$in['quote_type']."',
			// author  ='".$in['author']."',
			$fields =",
											sale_unit  ='".$in['sale_unit']."',
	                                        package ='".$in['package']."',
	                                        type = '".$in['quote_type']."',
	                                        grand_total='".$in['grand_total']."' ";
		}
		if($in['type'] == '3'){
			$table = 'label_language_order';
			$fields =",
											sale_unit  ='".$in['sale_unit']."',
	                                        package ='".$in['package']."',
	                                        gov_taxes='".$in['gov_taxes']."',
	                                        gov_taxes_code='".$in['gov_taxes_code']."',
	                                        gov_taxes_type='".$in['gov_taxes_type']."',
	                                        p_order_note='".$in['p_order_note']."',
	                                        `p_order`='".$in['p_order']."',
	                                        grand_total='".$in['grand_total']."' ";
		}
		$this->db->query("UPDATE $table SET invoice_note='".$in['invoice_note']."',
	                                        invoice='".$in['invoice']."',
	                                        order_note='".$in['order_note']."',
	                                        `order`='".$in['order']."',
	                                        article='".$in['article']."',
	                                        quote='".$in['quote']."',
	                                        reference='".$in['reference']."',
	                                        quote_note='".$in['quote_note']."',
	                                        billing_address='".$in['billing_address']."',
	                                        date='".$in['date']."',
	                                        customer='".$in['customer']."',
	                                        item='".$in['item']."',
	                                        unitmeasure='".$in['unitmeasure']."',
	                                        quantity='".$in['quantity']."',
	                                        unit_price='".$in['unit_price']."',
	                                        amount='".$in['amount']."',
	                                        subtotal='".$in['subtotal']."',
	                                        discount='".$in['discount']."',
	                                        vat='".$in['vat']."',
	                                        payments='".$in['payments']."',
	                                        amount_due='".$in['amount_due']."',
	                                        notes='".$in['notes']."',
	                                        duedate='".$in['duedate']."',
	                                        bank_details='".$in['bankd']."',
	                                        bank_name='".$in['bank_name']."',
	                                        iban='".$in['iban']."',
	                                        bic_code='".$in['bic_code']."',
	                                        phone='".$in['phone']."',
	                                        fax='".$in['fax']."',
	                                        email='".$in['email']."',
	                                        url='".$in['url']."',
	                                        our_ref='".$in['our_ref']."',
	                                        your_ref='".$in['your_ref']."',
	                                        vat_number ='".$in['vat_number']."'
	                                        ".$fields."
					  					WHERE label_language_id='".$in['label_language_id']."'");


		msg::$success = gm("Account has been successfully updated");
        return true;
	}

	/**
	 * Send a reminder to the company about the delivery order
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function notice(&$in)
	{

		if($in['stock_disp_id']){
			// $this->generate_pdf($in);
			$in['type'] = ACCOUNT_ORDER_PDF_FORMAT;
			include_once(__DIR__.'/../controller/stock_dispatch_print.php');
		}


		// require_once ('../../classes/class.phpmailer.php');
        // include_once ("../../classes/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

        $mail = new PHPMailer();
$mail->WordWrap = 50;
        $def_email = $this->default_email();
        $body = stripslashes($in['e_message']);
        $body= str_replace("\r\n", "\n", $body);
        // $body= strip_tags($body,'<table><tbody><tr><td><div><th><thead><strong>');
        $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
        $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);

        $subject=$in['subject'];

        $mail->Subject = $subject;

        $mail->MsgHTML(nl2br(($body)));

        if($in['copy']){
            $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
        }
        if($in['c_email']){
        	$mail->AddAddress(trim($in['c_email']),$in['c_email']);
        }

		if($in['send_to']){
			$mail->AddAddress(trim($in['send_to']));
		}

		if($in['contact_id']){
			$contact = $this->db->query("SELECT email, lastname, firstname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($contact->next()){
				$mail->AddAddress($contact->f('email'),$contact->f('lastname').' '.$contact->f('firstname'));
			}
		}

		if($in['include_pdf']){
			if($in['stock_disp_id']){
				$mail->AddAttachment("order.pdf", $in['serial_number'].'.pdf');
			}

        }

   		$mail->Send();

		msg::$success =gm('Mail has been successfully sent');
	}




	/****************************************************************
	* function article_settings(&$in)                                          *
	****************************************************************/
	function article_settings(&$in){

		   Sync::start(ark::$model.'-'.ark::$method);
		if(!$in['allow_article_packing']){
		   $in['allow_article_packing']=0;
		   $in['allow_quote_packing']=0;
		   $in['allow_quote_sale_unit']=0;
		}
		if(!$in['allow_article_sale_unit'] || $in['allow_article_packing'] == 0 ){
		    $in['allow_article_sale_unit']=0;
		}
		if(!$in['allow_article_sale_unit']){
		    $in['allow_quote_sale_unit']=0;
		}
		if(!$in['allow_quote_packing']){
		   $in['allow_quote_packing']=0;
		   $in['allow_quote_sale_unit']=0;
		}

		if(!$in['allow_quote_sale_unit'] || $in['allow_quote_packing'] == 0 ){
		    $in['allow_quote_sale_unit']=0;
		}

		$this->db->query("UPDATE settings SET value='".$in['allow_article_packing']."' WHERE constant_name='ALLOW_ARTICLE_PACKING'");
		$this->db->query("UPDATE settings SET value='".$in['allow_article_sale_unit']."' WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT'");


	    $this->db->query("UPDATE settings SET value='".$in['allow_quote_packing']."' WHERE constant_name='ALLOW_QUOTE_PACKING'");
		$this->db->query("UPDATE settings SET value='".$in['allow_quote_sale_unit']."' WHERE constant_name='ALLOW_QUOTE_SALE_UNIT'");

		if ($in['config']==1)
		{
			$this->db->query("UPDATE setup SET value=1 WHERE name='article_settings_ready'");
			$article_settings_ready = $this->db->field("SELECT value FROM setup WHERE name='article_settings_ready'");
			$article_price_ready = $this->db->field("SELECT value FROM setup WHERE name='article_price_ready'");
			$article_taxes_ready = $this->db->field("SELECT value FROM setup WHERE name='article_taxes_ready'");
			echo $article_price_ready."-".$article_settings_ready."-".$article_taxes_ready;
			if ($article_settings_ready==1 && $article_price_ready==1 && $article_taxes_ready==1)
			{
				$this->db->query("UPDATE setup SET value=1 WHERE name='articles_ready'");
				header("Location: index.php?do=home-account_configure");
			}
		}
		$this->db->query("UPDATE settings SET value='".$in['price_type']."' WHERE constant_name='PRICE_TYPE'");
		Sync::end();

		msg::$success = "Article settings has been successfully updated.";
		return true;
	}

	function select_edit(&$in){

		$query_sync=array();
		$i=0;
		foreach ($in['name_select'] as $key => $value){
			if(!empty($value)){
			$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$in['id_select'][$key]."' ");
				if(!$id){
					$inserted_id=$this->db->insert("INSERT INTO ".$in['table']." SET name='".addslashes($value)."', sort_order='".$key."' ");
					$query_sync[$i]="INSERT INTO ".$in['table']." SET id='".$inserted_id."',name='".addslashes($value)."', sort_order='".$key."'";
					$i++;
				}
			}
		}

		Sync::start(ark::$model.'-'.ark::$method);

        foreach ($in['name_select'] as $key=>$value){
			if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$in['id_select'][$key]."' ");
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value)."', sort_order='".$key."' WHERE id='".$id."' ");

				}
			}
		}

		Sync::end(0,$query_sync);
		return true;
	}

	/****************************************************************
	* function delete_select(&$in)                                          *
	****************************************************************/
	function delete_select(&$in){
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM ".$in['table']." WHERE id='".$in['id']."' ");
		Sync::end();
		return true;
	}


	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function insert_comment(&$in)
	{
		$in['log_id'] = insert_message_log($in['pag'],$in['comment'],$in['field_name'],$in['field_value'],1,$in['user_id']);
		if($in['to_user_id']){
			#do somehting
			$this->db->query("UPDATE logging SET to_user_id='".$in['to_user_id']."', due_date='".$in['comment_due_date']."', log_comment='New task' WHERE log_id='".$in['log_id']."' ");
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['to_user_id']."' AND name='send_email_type' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['to_user_id'],'name'=>'send_email_type']);
			if($this->db_users->next()){
				$send_email_type = $this->db_users->f('value');
				$send_email_type = explode(',',$send_email_type);
				if($send_email_type[0] == '1'){
					$this->send_notification_email($in);
				}
				$today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
				$today_end = mktime( 23, 59, 59, date('m'), date('d'), date('Y'));
				$due_date = $in['comment_due_date'];
				if($send_email_type[0] != '1' && $send_email_type[1] == '2' && $due_date > $today && $due_date < $today_end){
					$this->send_notification_email($in);
				}

			}
		}

		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function delete_comment(&$in)
	{
		$this->db->query("DELETE FROM logging WHERE log_id='".$in['log_id']."' ");
		return true;
	}


	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function toggle_done(&$in)
	{
		$this->db->query("UPDATE logging SET done='".$in['val']."' WHERE log_id='".$in['log_id']."' ");
		return true;
	}


	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function send_notification_email(&$in)
	{
		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		// require_once ('../../classes/class.phpmailer.php');
        // include_once ("../../classes/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

        $log = $this->db->query("SELECT * FROM logging WHERE log_id='".$in['log_id']."' ");
		$log->next();
		global $config;
		switch ($log->f('pag')) {
			case 'order':
				$link = '<a href="'.$config['site_url'].'pim/'.ENVIRONMENT.'/index.php?do=order-order&order_id='.$log->f('field_value').'">order</a>';
				break;
			case 'invoice':
				$link = '<a href="'.$config['site_url'].'pim/'.ENVIRONMENT.'/index.php?do=invoice-invoice&invoice_id='.$log->f('field_value').'">invoice</a>';
				break;
			case 'quote':
				$link = '<a href="'.$config['site_url'].'pim/'.ENVIRONMENT.'/index.php?do=quote-quote&quote_id='.$log->f('field_value').'">quote</a>';
				break;
			case 'project':
				$link = '<a href="'.$config['site_url'].'pim/'.ENVIRONMENT.'/index.php?do=project-project&project_id='.$log->f('field_value').'">project</a>';
				break;
			case 'customer':
				$link = '<a href="'.$config['site_url'].'pim/'.ENVIRONMENT.'/index.php?do=pim-customer&customer_id='.$log->f('field_value').'">customer</a>';
				break;
			case 'xcustomer_contact':
				$link = '<a href="'.$config['site_url'].'pim/'.ENVIRONMENT.'/index.php?do=company-xcustomer_contact&contact_id='.$log->f('field_value').'">contact</a>';
				break;
			case 'maintenance':
				$link = '<a href="'.$config['site_url'].'pim/'.ENVIRONMENT.'/index.php?do=maintenance-services&service_id='.$log->f('field_value').'">intervention</a>';
				break;
			default:
				$link = $log->f('pag');
				break;
		}
		$user_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$log->f('to_user_id')."' ");
        $mail = new PHPMailer();
        $mail->WordWrap = 50;
       /*
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug = 1; // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true; // enable SMTP authentication
        $mail->Host = SMTP_HOST; // sets the SMTP server
        $mail->Port = SMTP_PORT; // set the SMTP port for the GMAIL server
        $mail->Username = SMTP_USERNAME; // SMTP account username
        $mail->Password = SMTP_PASSWORD; // SMTP account password*/

        $body='Dear User,

You have received this internal message:
-------------------------------------------------------------------
<table width="100%"><tbody><tr><td width="10%" style="text-align:right">From :</td><td>&nbsp;&nbsp;'.utf8_decode(get_user_name($log->f('user_id'))).'</td></tr><tr><td style="text-align:right">Subject :</td><td>&nbsp;&nbsp;New task</td></tr><tr><td style="text-align:right">Date :</td><td>&nbsp;&nbsp;'.date(ACCOUNT_DATE_FORMAT,$log->f('date')).'</td></tr><tr><td style="text-align:right">To :</td><td>&nbsp;&nbsp;'.utf8_decode(get_user_name($log->f('user_id'))).'</td></tr><tr><td style="text-align:right">Item :</td><td>&nbsp;&nbsp;'.$link.'</td></tr></tboby></table>


'.$log->f('message').'

-------------------------------------------------------------------

Best regards,
Akti Customer Care
<a href="http://akti.com" >www.akti.com</a>';

		$def_email = $this->default_email();

        $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
        $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);

        $subject="New internal message";

        $mail->Subject = $subject;

        $mail->MsgHTML(nl2br(($body)));

        $mail->AddAddress($user_email);

		$sent_date= time();
    	$this->db->query("UPDATE logging SET sent='1' WHERE log_id='".$in['log_id']."' ");

   		$mail->Send();

		// msg::$success =gm('Invoice has been successfully sent');
		// $in['act'] = '';
		// $in['do']='invoice-invoice';
		// ark::run('invoice-invoice');
		return true;
	}


	function get_article_quantity_price(&$in)
	{

		$price_type=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
        $is_block_discount=$this->db->query("SELECT pim_product_article.product_id,pim_product_article.article_id,pim_product_article.block_discount
                               FROM pim_product_article
                               WHERE pim_product_article.article_id='".$in['article_id']."' AND pim_product_article.block_discount='1'");

        if($is_block_discount->move_next()){
	    		$price=$in['price'];
		}else{
			if($price_type==1){
		   		$price=$in['price'];
	    	}else{
	    		$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices
	    		                     WHERE pim_article_prices.from_q<='".$in['quantity']."'
	    		                     AND  pim_article_prices.article_id='".$in['article_id']."'
	                                 ORDER BY pim_article_prices.from_q DESC
	                                 LIMIT 1
	    		                     ");

		        if(!$price){
		        	$price=$in['price'];
		        }
	    	}
		}
	    $in['new_price']=$price;
	    $in['debug']=true;
		return true;
	}


function naming_set(&$in){


		if($in['account_stock_disp_start']){
			$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_STOCK_DISP_START' ");
			if($this->db->move_next()){
				$this->db->query("UPDATE settings SET value='".$in['account_stock_disp_start']."' WHERE constant_name='ACCOUNT_STOCK_DISP_START' ");
			}else{
				$this->db->query("INSERT INTO settings SET value='".$in['account_stock_disp_start']."', constant_name='ACCOUNT_STOCK_DISP_START' ");
			}
			msg::$success = gm("Settings updated").'.';
		}

		if($in['inc_part_o']){
			if(!defined('ACCOUNT_STOCK_DISP_DIGIT_NR')){
				$this->db->query("INSERT INTO settings SET value='".$in['inc_part_o']."', constant_name='ACCOUNT_STOCK_DISP_DIGIT_NR', module='order' ");
				msg::$success = gm("Setting added.");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['inc_part_o']."' WHERE constant_name='ACCOUNT_STOCK_DISP_DIGIT_NR'");
				msg::$success = gm("Settings updated").'.';
			}
		}

		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_STOCK_DISP_REF' ");
		if($in['show_ref']){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_STOCK_DISP_REF' ");
		}

		$this->db->query("UPDATE settings SET value='".$in['ref_delimiter']."' WHERE constant_name='ACCOUNT_STOCK_DISP_DEL' ");

		return true;
	}

function note_set(&$in){
		if($in['notes']){
            switch ($in['languages']){
				case '1':
					$lang = '';
					break;
				case '2':
					$lang = '_2';
					break;
				case '3':
					$lang = '_3';
					break;
				case '4':
					$lang = '_4';
					break;
				default:
					$lang = '';
					break;
			}

			$this->db->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='stock_disp_note".$lang."' AND default_main_id='0' ");
			msg::success ( gm("Note updated"),'success');
			
		}
	}

	/**
	 * add Address
	 *
	 * @return bool
	 * @author Mp
	 **/
	function add_address(&$in){
		$in['failure'] = false;
		if(!$this->add_address_validate($in)){
			$in['failure'] = true;
			return false;
		}

        $in['address_id'] = $this->db->insert("INSERT INTO dispatch_stock_address SET
        	                                                   naming  ='".$in['naming']."',
                                                               country_id  ='".$in['country_id']."',
                                                               zip         ='".$in['zip']."',
                                                               address     ='".$in['address']."',
                                                               city        ='".$in['city']."'

        	");



		msg::$success= gm('Address successfully added');

		return true;
	}

	function update_address(&$in){

		$in['failure'] = false;
		if(!$this->update_address_validate($in)){
			$in['failure'] = true;
			return false;
		}

        $this->db->query("UPDATE dispatch_stock_address SET
                                                               naming  ='".$in['naming']."',
                                                               country_id  ='".$in['country_id']."',
                                                               zip         ='".$in['zip']."',
                                                               address     ='".$in['address']."',
                                                               city  =    '".$in['city']."'


                          WHERE        address_id =  '".$in['address_id']."'
        	");



		msg::$success= gm('Address successfully updated');

		return true;
	}

	function delete_address(&$in){
		$in['failure'] = false;
		if(!$this->delete_address_validate($in)){
			$in['failure'] = true;
			return false;
		}

        $this->db->query("DELETE FROM  dispatch_stock_address WHERE  address_id =  '".$in['address_id']."'");
		msg::$success= gm('Address successfully deleted');

		return true;
	}

	/****************************************************************
	* function add_address_validate(&$in)                                *
	****************************************************************/
	function add_address_validate(&$in)
	{
		$v = new validation($in);
		$v->field('country_id', 'ID', 'required');
		$is_ok = $v->run();


		return $is_ok;
	}
	/****************************************************************
	* function update_address_validate(&$in)                                          *
	****************************************************************/
	function update_address_validate(&$in)
	{

		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[dispatch_stock_address.address_id]', "Invalid Id");
		if(!$v->run()){
			return false;
		} else {
			return $this->add_address_validate($in);
		}
	}

    	/****************************************************************
	* function delete_address_validate(&$in)                                          *
	****************************************************************/
	function delete_address_validate(&$in)
	{

		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[dispatch_stock_address.address_id]', "Invalid Id");
		$is_ok = $v->run();


		return $is_ok;
	}

	function stock_setting(&$in) {
    $this->db->query("UPDATE settings SET value='".$in['allow_stock']."' WHERE constant_name='ALLOW_STOCK'");
    $this->db->query("UPDATE settings SET value='".$in['allow_out_of_stock']."' WHERE constant_name='ALLOW_OUT_OF_STOCK'");
    $this->db->query("UPDATE settings SET value='".$in['show_stock_warning']."' WHERE constant_name='SHOW_STOCK_WARNING'");
    $this->db->query("UPDATE settings SET value='".$in['stock_multiple_locations']."' WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
    $this->db->query("UPDATE settings SET value='".$in['article_threshold_value']."' WHERE constant_name='ARTICLE_THRESHOLD_VALUE'");
    $this->db->query("UPDATE settings SET value='".$in['require_reason_stock_modif']."' WHERE constant_name='REQUIRE_REASON_MANUAL_STOCK_MODIF'");
        //verify if exist constant_name='ARTICLE_MAXIMUM_THRESHOLD_VALUE' in table settings
        $article_maximum_treshold_value=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='ARTICLE_MAXIMUM_THRESHOLD_VALUE'");
        if(!empty($article_maximum_treshold_value)){
            $this->db->query("UPDATE settings SET value='".$in['article_maximum_stock_treshold']."' WHERE constant_name='ARTICLE_MAXIMUM_THRESHOLD_VALUE'");
        }else{
            //for old database older then this update 10.11.2021
            if($in['article_maximum_stock_treshold']) {
                $this->db->query("INSERT INTO settings SET constant_name='ARTICLE_MAXIMUM_THRESHOLD_VALUE', value='" . $in['article_maximum_stock_treshold'] . "', long_value='NULL', module='PIM', type='1'");
            }
        }
    $this->db->query("UPDATE pim_articles SET article_threshold_value='".$in['article_threshold_value']."'
                           WHERE custom_threshold_value='0'");

        msg::success ( gm("Article stock settings has been successfully updated"),'success');
    return true;
    }

    function default_language(&$in){
       msg::success ( gm("Changes have been saved."),'success');
        return true;
    }

    function default_message_stock(&$in)
      {
        $set = "text  = '".$in['text']."', ";
        /*if($in['use_html']){*/
          $set1 = "html_content  = '".$in['html_content']."' ";
        /*}*/
        $this->db->query("UPDATE sys_message SET
              subject = '".$in['subject']."',
              ".$set."
              ".$set1."
              WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
        /*$this->db->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
        msg::success( gm('Message has been successfully updated'),'success');
        // json_out($in);
        return true;
      }

      function naming_set_stock(&$in){
	    if($in['ACCOUNT_STOCK_DISP_START']){
	        $this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_STOCK_DISP_START' ");
	        if($this->db->move_next()){
	  $this->db->query("UPDATE settings SET value='".$in['ACCOUNT_STOCK_DISP_START']."' WHERE constant_name='ACCOUNT_STOCK_DISP_START' ");
	        }else{
	  $this->db->query("INSERT INTO settings SET value='".$in['ACCOUNT_STOCK_DISP_START']."', constant_name='ACCOUNT_STOCK_DISP_START' ");
	        }
	        msg::success ( gm("Settings updated"),'success');
	    }

	    if($in['ACCOUNT_STOCK_DISP_DIGIT_NR']){
	        if(!defined('ACCOUNT_STOCK_DISP_DIGIT_NR')){
	  $this->db->query("INSERT INTO settings SET value='".$in['ACCOUNT_STOCK_DISP_DIGIT_NR']."', constant_name='ACCOUNT_STOCK_DISP_DIGIT_NR', module='order' ");
	  msg::success ( gm("Settings updated"),'success');
	        }else{
	  $this->db->query("UPDATE settings SET value='".$in['ACCOUNT_STOCK_DISP_DIGIT_NR']."' WHERE constant_name='ACCOUNT_STOCK_DISP_DIGIT_NR'");
	  msg::success ( gm("Settings updated"),'success');
	        }
	    }

	    $this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_STOCK_DISP_REF' ");
	    if($in['CHECKED']){
	        $this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_STOCK_DISP_REF' ");
	    }

	    $this->db->query("UPDATE settings SET value='".$in['ACCOUNT_STOCK_DISP_DEL']."' WHERE constant_name='ACCOUNT_STOCK_DISP_DEL' ");

	    return true;
    }

    function pick_articles_validation(&$in)
	{
		$is_ok=true;
		
/*		if(!($in['service_id'] && $in['location_id'])){
				$is_ok=false;
			}*/
		foreach($in['lines'] as $key => $value){
			if(return_value($value['picked_quantity'])>return_value($value['quantity'])){
				$value['is_error']='1';
				$is_ok=false;
			}
		}
		
		return $is_ok;
	}

	function pick_articles(&$in)
	{
		if(!$this->pick_articles_validation($in)){
			msg::error(gm('Total quantity lower than quantity selected'),"error");
			json_out($in);
		}

		foreach ($in['lines'] as $key => $value) {		
			$this->db->query("UPDATE service_reservation SET picked_quantity='".return_value($value['picked_quantity'])."' WHERE service_id='".$in['service_id']."' AND  location_id='".$in['location_id']."' AND a_id='".$value['a_id']."' AND reservation_id='".$value['reservation_id']."'");
		}
		msg::success ( gm('The articles have been picked'),'success');
		json_out($in);
	}

	function updateCustomerData(&$in){
		$in['stock_disp_id'] = $in['item_id'];

		if(is_numeric($in['item_id'])){

			$in['from_customer_id']=$in['buyer_id'];
	    if($in['from_customer_id']==0){
	       	$in['from_type']=1;
	       	$in['from_customer_name']='';     	

	       	$buyer_adr=$this->db->query("SELECT * FROM dispatch_stock_address WHERE customer_id = '0' and address_id='".$in['main_address_id']."'");

	       	$in['from_address']=$buyer_adr->f('address');
	        $in['from_zip']=$buyer_adr->f('zip');
			$in['from_city']=$buyer_adr->f('city');
			$in['from_country']=get_country_name($buyer_adr->f('country_id'));
			$in['from_naming']=$buyer_adr->f('naming');		       	

	    }else{
	    	$buyer_adr=$this->db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['main_address_id']."'");

        	$in['from_address']=$buyer_adr->f('address');
        	$in['from_zip']=$buyer_adr->f('zip');
        	$in['from_city']=$buyer_adr->f('city');
        	$in['from_country']=get_country_name($buyer_adr->f('country_id'));

        	$buyer_info = $this->db->query("SELECT customers.name FROM customers WHERE customer_id='".$in['from_customer_id']."' ");

        	$in['from_naming']=$buyer_info->f('name');
        	$in['from_type']=2;
        	$in['from_customer_name']=stripslashes($buyer_info->f('name'));

	    }
        $in['from_address_id']=$in['main_address_id'];       
	          
	    $in['to_customer_id']=$in['to_buyer_id'];
	    if($in['to_customer_id']==0){

	       	$in['to_type']=1;
	       	$in['to_customer_name']='';

	       	$to_buyer_adr=$this->db->query("SELECT * FROM dispatch_stock_address WHERE customer_id = '0' and address_id='".$in['to_main_address_id']."'");

	       	$in['to_address']=stripslashes($to_buyer_adr->f('address'));
	        $in['to_zip']=$to_buyer_adr->f('zip');
			$in['to_city']=stripslashes($to_buyer_adr->f('city'));
			$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));
			$in['to_naming']=stripslashes($to_buyer_adr->f('naming'));

	    }else{
	       	$in['to_type']=2;

	       	$to_buyer_adr=$this->db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['to_customer_id']."' and address_id='".$in['to_main_address_id']."'");
	       	$in['to_address']=stripslashes($to_buyer_adr->f('address'));
	        $in['to_zip']=$to_buyer_adr->f('zip');
			$in['to_city']=stripslashes($to_buyer_adr->f('city'));
			$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));

			$buyer_info = $this->db->query("SELECT customers.name FROM customers WHERE customer_id='".$in['to_customer_id']."' ");
			$in['to_naming']=stripslashes($buyer_info->f('name'));
			$in['to_customer_name']=stripslashes($buyer_info->f('name'));
	    }
	    $in['to_address_id']=$in['to_main_address_id'];

		$this->db->query("UPDATE pim_stock_disp SET
			from_customer_id      = '".$in['from_customer_id']."',
                  from_type             = '".$in['from_type']."',
                  from_address_id         = '".$in['from_address_id']."',
                  from_customer_name        = '".addslashes($in['from_customer_name'])."',
                  from_naming           = '".addslashes($in['from_naming'])."',
                  from_address        = '".addslashes($in['from_address'])."',
                  from_zip            = '".$in['from_zip']."',
                  from_city                 = '".addslashes($in['from_city'])."',
                  from_country            = '".$in['from_country']."',
                  to_customer_id          = '".$in['to_customer_id']."',
                  to_type             = '".$in['to_type']."',
                  to_address_id             = '".$in['to_address_id']."',
                  to_customer_name        = '".addslashes($in['to_customer_name'])."',
                  to_naming           = '".addslashes($in['to_naming'])."',
                  to_address        = '".addslashes($in['to_address'])."',
                  to_zip            = '".$in['to_zip']."',
                  to_city             = '".addslashes($in['to_city'])."'
              WHERE stock_disp_id='".$in['stock_disp_id']."' ");
		}
		return true;
	}

	function saveAddToPdfBatch(&$in)
  {
  	if($in['all']){
  		if($in['value'] == 1){
  			foreach ($in['item'] as $key => $value) {
			    $_SESSION['add_stock_batch'][$value]= $in['value'];
	  		}
  		}else{
  			foreach ($in['item'] as $key => $value) {
			    unset($_SESSION['add_stock_batch'][$value]);
	  		}
		  }
  	}else{
	    if($in['value'] == 1){
	    	$_SESSION['add_stock_batch'][$in['item']]= $in['value'];
	    }else{
	    	unset($_SESSION['add_stock_batch'][$in['item']]);
	    }
	  }
    $all_pages_selected=false;
    $minimum_selected=false;
    if($_SESSION['add_stock_batch'] && count($_SESSION['add_stock_batch'])){
      if($_SESSION['tmp_add_stock_batch'] && count($_SESSION['tmp_add_stock_batch']) == count($_SESSION['add_stock_batch'])){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

  function saveAddToPdfBatchAll(&$in){
    $all_pages_selected=false;
    $minimum_selected=false;
    if($in['value']){
      unset($_SESSION['add_stock_batch']);
      if($_SESSION['tmp_add_stock_batch'] && count($_SESSION['tmp_add_stock_batch'])){
        $_SESSION['add_stock_batch']=$_SESSION['tmp_add_stock_batch'];
        $all_pages_selected=true;
      }    
    }else{
      unset($_SESSION['add_stock_batch']);
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }
    function saveAddToExport(&$in)
    {
        if($in['all']){
            if($in['value'] == 1){
                foreach ($in['item'] as $key => $value) {
                    $_SESSION['add_to_export'][$value]= $in['value'];
                }
            }else{
                foreach ($in['item'] as $key => $value) {
                    unset($_SESSION['add_to_export'][$value]);
                }
            }
        }else{
            if($in['value'] == 1){
                $_SESSION['add_to_export'][$in['item']]= $in['value'];
            }else{
                unset($_SESSION['add_to_export'][$in['item']]);
            }
        }
        $all_pages_selected=false;
        $minimum_selected=false;

        if($_SESSION['add_to_export'] && count($_SESSION['add_to_export'])){
            if($_SESSION['tmp_add_to_export'] && count($_SESSION['tmp_add_to_export']) == count($_SESSION['add_to_export'])){
                $all_pages_selected=true;
            }else{
                $minimum_selected=true;
            }
        }
        $in['all_pages_selected']=$all_pages_selected;
        $in['minimum_selected']=$minimum_selected;
        json_out($in);
        //return true;
    }
    function saveAddToExportAll(&$in){
        $all_pages_selected=false;
        $minimum_selected=false;
        if($in['value']){
            unset($_SESSION['add_to_export']);
            if($_SESSION['tmp_add_to_export'] && count($_SESSION['tmp_add_to_export'])){
                $_SESSION['add_to_export']=$_SESSION['tmp_add_to_export'];
                $all_pages_selected=true;
            }
        }else{
            unset($_SESSION['add_to_export']);
        }
        $in['all_pages_selected']=$all_pages_selected;
        $in['minimum_selected']=$minimum_selected;
        json_out($in);
    }


     

}//end class

?>