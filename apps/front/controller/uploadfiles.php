<?php

$mtime = str_replace('.','-',str_replace(' ','-',microtime()));
$filename = $mtime.$_FILES['file']['name'];

$destination = 'upload/tmp/' . $filename;

if(exif_imagetype($_FILES['file']['tmp_name'])){
    // if we have a valid image
    move_uploaded_file( $_FILES['file']['tmp_name'] , $destination );
    @chmod($destination, 0777);
}else{
    exit();
}

$out['file'] = $filename;
json_out($out,true);

