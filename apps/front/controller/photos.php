<?php
list($width, $height) = explode("-", $in['sizes']);

if(!is_numeric($width)){
    if($width != 'x'){
        exit();
    }else{
        $width = 0;
    }
}

if(!is_numeric($height)){
    $height = 0;
}

if($width == $height){
    if($width == 0){
        exit();
    }
}

if(!file_exists ('upload/'.$in['sizes'])){
    mkdir('upload/'.$in['sizes'], 0777, true);
}

if(!file_exists ('upload/'.$in['filename'])){
    $in['filename'] = 'na_big.jpg';
}

$image = 'upload/'.$in['filename'];
$save_to = 'upload/'.$in['sizes'].'/'.$in['filename'];
$ext = pathinfo($image, PATHINFO_EXTENSION);

resize_image($image, $width, $height, $save_to);

// output to browser
header('Content-type: image/jpeg');
readfile($save_to);
exit();

function resize_image($original_image, $thumb_width, $thumb_height, $save_file)
{
    $f_type = pathinfo($original_image, PATHINFO_EXTENSION);

    if($save_file == ''){
        $filename=NULL;
    } else {
        $filename=$save_file;
    }
    if($f_type == "jpg") $type = "JPEG";
    else if($f_type == "gif") $type = "GIF";
    else if($f_type == "png") $type = "PNG";

    $create = 'ImageCreateFrom'.$type;

    $do = 'Image'.$type;

    $original_image=$create($original_image);
    $width = imagesx($original_image);
    $height = imagesy($original_image);
    $aspect_ratio = $width / $height;

    if ($thumb_width == 0)
    {
        $thumb_width = $aspect_ratio * $thumb_height;
    }
    elseif ($thumb_height == 0)
    {
        $thumb_height= $thumb_width / $aspect_ratio;
    }

    $thumb_aspect_ratio = $thumb_width / $thumb_height;

    if ( $aspect_ratio >= $thumb_aspect_ratio )
    {
        // If image is wider than thumbnail (in aspect ratio sense)
        $new_height = $thumb_height;
        $new_width = $width / ($height / $thumb_height);
    }
    else
    {
        // If the thumbnail is wider than the image
        $new_width = $thumb_width;
        $new_height = $height / ($width / $thumb_width);
    }

    $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

    // Resize and crop
    imagecopyresampled($thumb,
        $original_image,
        0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
        0 - ($new_height - $thumb_height) / 2, // Center the image vertically
        0, 0,
        $new_width, $new_height,
        $width, $height);



    @unlink($original_image);
    if($type=='PNG') $do($thumb, $filename, $quality=9);
    else $do($thumb, $filename, $quality=100);
    @chmod($filename, 0777);
    imagedestroy($thumb);
}