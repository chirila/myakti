<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}


/*	function get_logos($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'logos'=>array(), 'logo'=>array());
		$default_logo = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_PO_ORDER' ");
		if($default_logo == '' || $default_logo == 'pim/img/no-logo.png')   {
		   	array_push($data['logo'], array(
        'account_logo'=>'images/no-logo.png',
        'default'=>'images/no-logo.png'
        ));
		}
		else {
			$logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/{po_logo_img}_*',GLOB_BRACE);
		    foreach ($logos as $v) {
		        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
		        $size = @getimagesize($logo);
		        $ratio = 250 / 77;
		        if($size[0]/$size[1] > $ratio ){
		            $attr = 'width="250"';
		        }else{
		            $attr = 'height="77"';
		        }
		        array_push($data['logos'], array(
		        'account_logo'=>$logo,
		        'default'=>$default_logo,
		        'attr' => $attr
		        ));
		    }
		}
		$data['default_logo'] = $default_logo;


			
		return json_out($data, $showin,$exit);
	}*/
	function get_PDFlayout($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'layout_header'=>array(),'custom_layout'=>array(),'layout_body'=>array(),'layout_footer'=>array());
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");

		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_PDF_FORMAT' ");
		$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_PO_PDF' ");

		if(!$in['identity_id']){
			$in['identity_id']='0';
		}
		$data['multiple_identity']				= build_identity_dd($in['identity_id']);
		$data['identity']						= $in['identity_id'];

		$data['has_custom_layout']=false;
		if(DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11' || DATABASE_NAME=='caff4267_64a4_1db2_d49f2ab3760c'){
			$data['has_custom_layout']=true;
		}

		$data['use_custom_layout']= ($use ==1)? true:false;

		for ($j=1; $j <= 1; $j++) {
		array_push($data['custom_layout'], array(
				'view_invoice_custom_pdf' 						=>'index.php?do=po_order-p_order_print&custom_type='.$j.'&lid='.$language,
				'img_href_custom'								=> 'images/custom_type-'.$j.'.jpg',
				'custom_type'									=> $j,
				'selected_custom'								=> $def == $j && $use == 1 ? 'active' : '',
			
			));
		}


		
		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PO_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_PO_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PO_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_PO_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PO_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_PO_PDF' ");
		$number_type = $_SESSION['u_id']<10737 || $_SESSION['u_id']>11810 ? 2 : 1;
		for ($z=$def_header; $z <= $def_header; $z++) {
				array_push($data['layout_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=po_order-po_print&header='.$data['header'].'&layout='.$z.'&lid='.$language.'&header_id='.$def_header.'&save_as=preview',
					'img_href'						=> 'images/type_header-'.$z.'.jpg',
					'type'							=> $z,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=po_order-po_print&layout='.$y.'&lid='.$language.'&body='.$data['body'].'&save_as=preview',
					'img_href'						=> 'images/type_body-'.$y.'.jpg',
					'type'							=> $number_type,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($i=1; $i <= 2; $i++) {
				array_push($data['layout_footer'], array(
					'view_invoice_pdf' 				=>'index.php?do=po_order-po_print&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
					'img_href'						=> 'images/type_footer-'.$i.'.jpg',
					'type'							=> $i,
					'footer'						=> 'footer',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}

		$data['selected_header'] = $def_header;
		$data['selected_body'] = $def_body;
		$data['selected_footer'] = $def_footer;




		//

		return json_out($data, $showin,$exit);
	}
	function get_Convention($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('convention'=>array());
		$ACCOUNT_P_ORDER_START =  $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_START' AND type=1");
		$ACCOUNT_P_ORDER_DIGIT_NR = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_DIGIT_NR' AND type=1");
		$ACCOUNT_P_ORDER_REF = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_REF' AND type=1");
		$ACCOUNT_P_ORDER_DEL = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_DEL' AND type=1"); 


		$data['convention'] = array(
			'ACCOUNT_P_ORDER_START'=>$ACCOUNT_P_ORDER_START,
			'ACCOUNT_P_ORDER_DIGIT_NR'=>$ACCOUNT_P_ORDER_DIGIT_NR,
			'CHECKED' => $ACCOUNT_P_ORDER_REF ? true : false,
			'ACCOUNT_P_ORDER_DEL' => $ACCOUNT_P_ORDER_DEL,
			'EXAMPLE'=>ACCOUNT_P_ORDER_START.str_pad(1,ACCOUNT_P_ORDER_DIGIT_NR,"0",STR_PAD_LEFT),

		); 
/*			'ACTIVE_TABB'		=> $in['tabbo'],
			'show_for_one_user' => $show_for_one_user*/
		return json_out($data, $showin,$exit);
	}
	function get_general($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('general_condition'=>array());
		$filter = '';
		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
			if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
			}
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
				$filter = '_'.$in['languages'];
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
				if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
				}
			}
		}
		$default_table = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='p_order_note".$filter."' ");
		$data['general_condition']= array(
			'notes'			=> $default_table,
			'translate_cls'	=> 'form-language-'.$code,
			'languages'		=>  $in['languages'],
			'do'			=> 'po_order-settings-po_order-default_language',
			'xget'			=> 'general',
			'language_dd' 	=>  build_language_dd_new($in['languages']),
		);
		return json_out($data, $showin,$exit);
	}
	function get_customLabel($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'customLabel'=>array());

		$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
		while($pim_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 		=> gm($pim_lang->f('language')),
			'do'							=> 'po_order-settings',
			'xget'							=> 'labels',
			'label_language_id'	     		=> $pim_lang->f('lang_id'),
			'label_custom_language_id'	=> '',
			));
		}
		$pim_custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
		while($pim_custom_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 			=> $pim_custom_lang->f('language'),
			'do'								=> 'po_order-settings',
			'xget'								=> 'labels',
			'label_custom_language_id'	    	=> $pim_custom_lang->f('lang_id'),
			'label_language_id'	     			=> '',
			));
		}

		return json_out($data, $showin,$exit);
	}
	function get_labels($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$const = array();


		$table = 'label_language_p_order';

		if($in['label_language_id'])
		{
			$filter = "WHERE label_language_id='".$in['label_language_id']."'";
			$id = $in['label_language_id'];
		}elseif($in['label_custom_language_id'])
		{
			$filter = "WHERE lang_code='".$in['label_custom_language_id']."'";
			$id = $in['label_custom_language_id'];
		}

		$db->query("SELECT * FROM $table $filter");

		$data['labels']=array(
				'label_language_id'  				=> $db->f('label_language_id'),
				'delivery_address'  					=> $db->f('delivery_address'),
				'stock_dispatching_note'		 	=> $db->f('stock_dispatching_note'),
				'company_name'		 				=> $db->f('company_name'),
				'comp_reg_number'					=> $db->f('comp_reg_number'),
				'stock_dispatching'	         		=> $db->f('stock_dispatching'),
				'invoice_note'	     				=> $db->f('invoice_note'),
				'invoice'	     					=> $db->f('invoice'),
				'quote_note'	     				=> $db->f('quote_note'),
				'quote'								=> $db->f('quote'),
				'order_note'						=> $db->f('order_note'),
				'order'	    						=> $db->f('order'),
				'p_order_note'	     				=> $db->f('p_order_note'),
				'p_order'	 						=> $db->f('p_order'),
				'article'	             			=> $db->f('article'),
				'reference'	         				=> $db->f('reference'),
				'billing_address'	             	=> $db->f('billing_address'),
				'date'	         					=> $db->f('date'),
				'customer'	     					=> $db->f('customer'),
				'item'	         					=> $db->f('item'),
				'quantity'	         				=> $db->f('quantity'),
				'unitmeasure'	         			=> $db->f('unitmeasure'),
				'unit_price'	             		=> $db->f('unit_price'),
				'amount'	         				=> $db->f('amount'),
				'subtotal'	     					=> $db->f('subtotal'),
				'discount'	             			=> $db->f('discount'),
				'vat'	         					=> $db->f('vat'),
				'payments'				 			=> $db->f('payments'),
				'amount_due'			 			=> $db->f('amount_due'),
				'grand_total'			 			=> $db->f('grand_total'),
				'notes'				 				=> $db->f('notes'),
				'bank_details'			 			=> $db->f('bank_details'),
				'duedate'				 			=> $db->f('duedate'),
				'bank_name'				 			=> $db->f('bank_name'),
				'iban'				 				=> $db->f('iban'),
				'bic_code'				 			=> $db->f('bic_code'),
				'phone'				 				=> $db->f('phone'),
				'fax'			 					=> $db->f('fax'),
				'url'			 					=> $db->f('url'),
				'email'								=> $db->f('email'),
				'our_ref'	 						=> $db->f('our_ref'),
				'your_ref'		 					=> $db->f('your_ref'),
				'vat_number'		 				=> $db->f('vat_number'),
				'CUSTOMER_REF'		 				=> $db->f('CUSTOMER_REF'),
				'gov_taxes'		 					=> $db->f('gov_taxes'),
				'gov_taxes_code'		 			=> $db->f('gov_taxes_code'),
				'gov_taxes_type'					=> $db->f('gov_taxes_type'),
				'sale_unit'							=> $db->f('sale_unit'),
				'package'		 					=> $db->f('package'),
				'source'		 					=> $db->f('source'),
				'type'		 						=> $db->f('type'),
				'author'							=> $db->f('author'),
				'shipping_price'		 			=> $db->f('shipping_price'),
				'article_code'						=> $db->f('article_code'),
				'delivery_date'		 				=> $db->f('delivery_date'),
				'entry_date'		 				=> $db->f('entry_date'),
				'delivery_note'		 				=> $db->f('delivery_note'),
				'entry_note'		 				=> $db->f('entry_note'),
				'pick_up_from_store'		 		=> $db->f('pick_up_from_store'),
				'page'		 						=> $db->f('page'),
				'download_webl'	     				=> $db->f('download_webl'),
				'pdfPrint_webl'		    			=> $db->f('pdfPrint_webl'),
				'name_webl'	     	 				=> $db->f('name_webl'),
				'email_webl'	     				=> $db->f('email_webl'),
				'addCommentLabel_webl'				=> $db->f('addCommentLabel_webl'),
				'submit_webl'	     				=> $db->f('submit_webl'),
				'p_order_webl'	     				=> $db->f('p_order_webl'),
				'date_webl'	     					=> $db->f('date_webl'),
				'accept_webl'	     				=> $db->f('accept_webl'),
				'requestNewVersion_webl'			=> $db->f('requestNewVersion_webl'),
				'reject_webl'	    				=> $db->f('reject_webl'),
				'status_webl'	     				=> $db->f('status_webl'),
				'bankDetails_webl'	    			=> $db->f('bankDetails_webl'),
				'payWithIcepay_webl'				=> $db->f('payWithIcepay_webl'),
				'cancel_webl'						=> $db->f('cancel_webl'),
				'regularInvoiceProForma_webl'		=> $db->f('regularInvoiceProForma_webl'),
				'bicCode_webl'						=> $db->f('bicCode_webl'),
				'ibanCode_webl'						=> $db->f('ibanCode_webl'),
				'bank_webl'							=> $db->f('bank_webl'),
				'noData_webl'						=> $db->f('noData_webl'),
				'total_amount'						=> $db->f('total_amount'),
				'do'								=> 'po_order-settings-po_order-label_update',
				'xget'								=> 'labels',
				'label_language_id'					=> $id
		);
		return json_out($data, $showin,$exit);
	}
	function get_emailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array(),'message_labels'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Serial Number");
		$array_values['DATE']=gm("Date");
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['DISCOUNT']=gm("Discount Percent");
		$array_values['DISCOUNT_VALUE']=gm("Discount Value");
		$array_values['PAYMENTS']=gm("Payments");
		$array_values['AMOUNT_DUE']=gm("Amount due");
		$array_values['WEB_LINK']=gm("Web Link");
		$array_values['WEB_LINK_2']=gm("Short Weblink");
		$array_values['DUE_DATE']=gm("Due Date");
		$array_values['INVOICE_TYPE']=gm("Invoice type");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['LOGO']=gm("Logo");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['CONTACT_FIRST_NAME']=gm("Contact First Name");
		$array_values['CONTACT_LAST_NAME']=gm("Contact Last Name");
		$array_values['YOUR_REFERENCE']=gm("Your Reference");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);


$is_normal = true;
if($code=='nl'){
	$code='du';
}
$in['lang_code'] = $code;
$name = 'pordmess';
$text = '-----------------------------------
Order Summary
-----------------------------------
Order ID: [!SERIAL_NUMBER!]
Date: [!ORDER_DATE!]
Customer: [!CUSTOMER!]
Discount ([!DISCOUNT!]):  [!DISCOUNT_VALUE!]
Amount Due:  [!AMOUNT_DUE!]

The detailed order is attached as a PDF.

Thank you!
-----------------------------------';
$subject = 'Order #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'po_order-settings-po_order-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'po_order-settings-po_order-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}



		return json_out($data, $showin,$exit);
	}
	function get_defaultEmail($in,$showin=true,$exit=true){
			$db = new sqldb();
			$po_email = $db->query("SELECT * FROM default_data WHERE type = 'p_order_email' ");
			$default_email_bcc = $db->query("SELECT * FROM default_data WHERE type = 'bcc_p_order_email' ");

			$data['email_default']= array(
				'po_default_name'			=> $po_email->f('default_name'),
				'po_email_value'			=> $po_email->f('value'),
				'bcc_email'					=> $default_email_bcc->gf('value'),
				'do'						=> 'po_order-settings-po_order-update_default_email',
				'xget'						=> 'defaultEmail',
			);
			return json_out($data, $showin,$exit);
	}
	function get_articlefield($in,$showin=true,$exit=true){
			$db = new sqldb();
			$data = array('articlefields'=>array());
			$not_copy_article_info = $db->field("SELECT value FROM settings WHERE constant_name='NOT_COPY_ARTICLE_INFO'");
			$use_article_purchase_price = $db->field("SELECT value FROM settings WHERE constant_name='USE_ARTICLE_PURCHASE_PRICE'");
			$text = $db->field("SELECT long_value FROM settings WHERE constant_name='P_ORDER_FIELD_LABEL'");
			
			$data['articlefields'] = array(
					'not_copy_article_info'  				=> $not_copy_article_info == 1 ? true : false,
					'use_article_purchase_price'  			=> $use_article_purchase_price == 1 ? true : false,
					'text'  								=> $text,
					'xget'									=> 'articlefield',
					'ADV_PRODUCT' 							=> defined('ADV_PRODUCT') && ADV_PRODUCT == 1 ? true : false,
					'do'									=> 'po_order-settings-po_order-articlefieldup',
			);

			return json_out($data, $showin,$exit);
	}
	function get_weblink($in,$showin=true,$exit=true){
			$db = new sqldb();
			$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_PO_ORDER_WEB_LINK' ");
			$allow = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_PO_ORDER_COMMENTS' ");
			$allow2 = $db->field("SELECT value FROM settings WHERE constant_name='WEB_INCLUDE_PDF_PO' ");
			

			global $config;
			$data['weblink']= array(
				'use'							=>$use == '1' ? true : false,
				'allow'							=>$allow == '1' ? true : false,
				'also'							=>$allow2 == '1' ? true : false,
				'do'							=> 'po_order-settings-po_order-web_link',
				'xget'							=> 'weblink',
			);
			return json_out($data, $showin,$exit);
	}
	function get_stock($in,$showin=true,$exit=true){
			$db = new sqldb();
			$allow_stock = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK'");
			$allow_out_of_stock = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_OUT_OF_STOCK'");
			$show_stock_warning = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_STOCK_WARNING'");
			$article_threshold_value = $db->field("SELECT value FROM settings WHERE constant_name='ARTICLE_THRESHOLD_VALUE'");
			$stock_multiple_locations = $db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
			$data['stock']= array(
				'allow_stock'									=>$allow_stock == '1' ? true : false,
				'allow_out_of_stock'							=>$allow_out_of_stock == '1' ? true : false,
				'show_stock_warning'							=>$show_stock_warning == '1' ? true : false,
				'article_threshold_value'						=>$article_threshold_value,
				'stock_multiple_locations'						=>$stock_multiple_locations == '1' ? true : false,
				'do'											=> 'po_order-settings-po_order-stock_setting',
				'xget'											=> 'stock',
			);
			return json_out($data, $showin,$exit);
	}
	function get_dispatch($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('dispatch'=>array());
		$ACCOUNT_STOCK_DISP_START =  $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_STOCK_DISP_START' AND type=1");
		$ACCOUNT_STOCK_DISP_DIGIT_NR = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_STOCK_DISP_DIGIT_NR' AND type=1");
		$ACCOUNT_STOCK_DISP_REF = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_STOCK_DISP_REF' AND type=1");
		$ACCOUNT_STOCK_DISP_DEL = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_STOCK_DISP_DEL' AND type=1"); 


		$data['dispatch'] = array(
			'ACCOUNT_STOCK_DISP_START'=>$ACCOUNT_STOCK_DISP_START,
			'ACCOUNT_STOCK_DISP_DIGIT_NR'=>$ACCOUNT_STOCK_DISP_DIGIT_NR,
			'CHECKED' => $ACCOUNT_STOCK_DISP_REF == '1' ? true : false,
			'ACCOUNT_STOCK_DISP_DEL' => $ACCOUNT_STOCK_DISP_DEL,
			'EXAMPLE'=>ACCOUNT_STOCK_DISP_START.str_pad(1,ACCOUNT_STOCK_DISP_DIGIT_NR,"0",STR_PAD_LEFT),

		); 
		return json_out($data, $showin,$exit);
	}
	function get_dispatch_note($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('dispatch_note'=>array());
		$filter = '';
		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
			if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
			}
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
				$filter = '_'.$in['languages'];
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
				if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
				}
			}
		}
		$default_table = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='stock_disp_note".$filter."' ");
		$data['dispatch_note']= array(
			'notes'			=> $default_table,
			'translate_cls'	=> 'form-language-'.$code,
			'languages'		=>  $in['languages'],
			'do'			=> 'po_order-settings-po_order-default_language',
			'xget'			=> 'dispatch_note',
			'language_dd' 	=>  build_language_dd_new($in['languages']),
		);
			return json_out($data, $showin,$exit);
	}
	function get_dispatch_email($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('dispatch_email'=>array(),'message_label'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1'  AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Dispatch Note ID");
		$array_values['DISPATCH_NOTE_DATE']=gm("Dispatch Note Date");
		$array_values['CUSTOMER']=gm("Company Name");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_label'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);


$is_normal = true;
$in['lang_code'] = $code;
$name = 'stock_disp_mess';
$text = '-----------------------------------
Dispatch Note Summary
-----------------------------------
Dispatch Note ID: [!SERIAL_NUMBER!]
Date: [!DISPATCH_NOTE_DATE!]
Customer: [!CUSTOMER!]


The detailed of the dispatch note is attached as a PDF.

Thank you!
-----------------------------------';
$subject = 'Dispatch Note #  [!SERIAL_NUMBER!] for [!ACCOUNT_COMPANY!] ';
$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['dispatch_email']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'po_order-settings-po_order-default_message',
				'xget'						=> 'dispatch_email',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}else{
			$data['dispatch_email']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'language_dd' 				=> build_language_ecom_dd($in['languages']),
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'translate_cls' 			=> 'form-language-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'po_order-settings-po_order-default_message',
				'xget'						=> 'dispatch_email',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id
			);
		}



		return json_out($data, $showin,$exit);
	}
	function get_dispatch_address($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('dispatch_address'=>array(),'dispatch_address_add'=>array());
			$db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address");
			while ($db->next()) {
				array_push($data['dispatch_address'], array(
					'naming'	    		=> $db->f('naming'),
					'address'	    		=> $db->f('address'),
					'is_default_checked'   	=> $db->f('is_default')==1 ? true : false,
					'zip'           		=> $db->f('zip'),
					'city'          		=> $db->f('city'),
					'country'               => get_country_name($db->f('country_id')),
					'country_id'            => $db->f('country_id'),
					'countries'             => build_country_list($db->f('country_id')),
					'address_id'			=> $db->f('address_id'),
					'not_default'			=> $db->f('is_default')?0:1
					));
			}
			$data['dispatch_address_add'] =  array(
				'countries'             => build_country_list()
			);

			return json_out($data, $showin,$exit);
	};

	function get_delivery($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('delivery'=>array());
			$delivery = $db->field("SELECT value FROM settings WHERE constant_name='P_ORDER_DELIVERY_STEPS'");
				$data['delivery'] = array(
					'delivery_type'   		=> $delivery,
					'xget'					=> 'delivery',
					//'do'					=> 'order-settings-order-timesheetup',
					);

			return json_out($data, $showin,$exit);
	}

		function get_deliverySettings($in,$showin=true,$exit=true){
		$db = new sqldb();
		$delivery = $db->field("SELECT value FROM settings WHERE constant_name='P_ORDER_DELIVERY_STEPS'");
		$edit_confirmed_po_orders = $db->field("SELECT value FROM settings WHERE constant_name = 'EDIT_CONFIRMED_PO_ORDERS' ");
			$data = array('deliverySettings'=>array(
			'xget'						=> 'deliverySettings',
			'delivery'					=> array(
										'delivery_type'   		=> $delivery,
										'xget'					=> 'deliverySettings',
										),
			'edit_confirmed_po_orders'	=> $edit_confirmed_po_orders==1 ? true : false,
		));

	
		return json_out($data, $showin,$exit);
	}

	$result = array(
		/*'logos'				=> get_logos($in,true,false),*/
		'PDFlayout'			=> get_PDFlayout($in,true,false),
		'Convention' 		=> get_Convention($in,true,false),
	);


	json_out($result);
?>
