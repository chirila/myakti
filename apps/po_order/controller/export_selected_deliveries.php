<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '512M');

$headers = array('ORDER NUMBER',
        'CUSTOMER NAME',                
        'OUR REFERENCE',
        "YOUR REFERENCE",
        "DELIVERY DATE",
        "ARTICLE CODE",
        "ARTICLE DESCRIPTION",
        "ORDERED QUANTITY",
        "DELIVERED QUANTITY",

        "WEIGHT",
        "CODE OF ORIGIN",
        "VAT NUMBER",
        "BASE PRICE"
);
$filename ="deliveries_export.csv";

if(!$in['export_data']){
    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');

    $fp = fopen("php://output", 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $int_array as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp); 
    exit();
}
$export_data=(array) json_decode(urldecode(base64_decode($in['export_data'])));
foreach($export_data as $key=>$val){
  $in[$key]=$val;
}
unset($in['export_data']);

$db = new sqldb();

$order_by_array = array('delivery_date','serial_number','order_date','customer_name');

$l_r =ROW_PER_PAGE;

$order_by = " ORDER BY delivery_date DESC ";

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$filter=" 1=1 ";

if($in['search']){
    $filter.=" AND (pim_p_orders.serial_number LIKE '%".$in['search']."%' OR pim_p_orders.customer_name LIKE '%".$in['search']."%' OR pim_p_orders.our_ref LIKE '%".$in['search']."%' OR pim_p_orders.your_ref LIKE '%".$in['search']."%' )";
    $arguments.="&search=".$in['search'];
}

if($in['item_code']){
    $filter.=" AND pim_articles.item_code LIKE '%".$in['item_code']."%' ";
}

if($in['start_date'] && !empty($in['start_date'])){
    $in['start_date'] =strtotime($in['start_date']);
    $in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if($in['stop_date'] && !empty($in['stop_date'])){
    $in['stop_date'] =strtotime($in['stop_date']);
    $in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}

if($in['start_date'] && $in['stop_date']){
    $filter.=" AND pim_p_order_deliveries.date BETWEEN '".$in['start_date']."' AND '".$in['stop_date']."' ";
}
else if($in['start_date']){
    $filter.=" AND pim_p_order_deliveries.date > ".$in['start_date']." ";
}
else if($in['stop_date']){
    $filter.=" AND pim_p_order_deliveries.date < ".$in['stop_date']." ";
}

if($in['order_by']){
    if(in_array($in['order_by'], $order_by_array)){
        $order = " ASC ";
        if($in['desc'] == '1' || $in['desc']=='true'){
            $order = " DESC ";
        }
        $order_by =" ORDER BY ".$in['order_by']." ".$order;
    }
}

$final_data=array();

$deliveries = $db->query("SELECT pim_p_orders_delivery.*,pim_p_order_deliveries.date AS delivery_date,pim_p_orders.customer_vat_number,pim_p_orders.serial_number,pim_p_orders.buyer_ref,pim_p_orders.customer_name,pim_p_orders.date AS order_date,pim_p_orders.our_ref, pim_p_orders.your_ref,pim_articles.item_code,pim_p_order_articles.article,pim_p_order_articles.article_id,pim_p_order_articles.quantity AS ordered_quantity,pim_p_orders_delivery.quantity AS delivered_quantity, pim_articles.weight, pim_articles.origin_number FROM pim_p_order_deliveries
    INNER JOIN pim_p_orders_delivery ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id
    INNER JOIN pim_p_order_articles ON pim_p_orders_delivery.order_articles_id=pim_p_order_articles.order_articles_id
    LEFT JOIN pim_articles ON pim_articles.article_id=pim_p_order_articles.article_id 
    LEFT JOIN pim_p_orders ON pim_p_order_deliveries.p_order_id=pim_p_orders.p_order_id
    
    WHERE ".$filter.$order_by." ");

while($deliveries->next()){
    $ref = '';
    if(ACCOUNT_ORDER_REF && $deliveries->f('buyer_ref')){
        $ref = $deliveries->f('buyer_ref');
    }

    if($deliveries->f('article_id')){
        $base_price=$db->field("SELECT price FROM pim_article_prices  
        WHERE article_id ='".$deliveries->f('article_id')."' AND base_price='1' ");
    }


    $item=array(
        $deliveries->f('serial_number') ? $ref.$deliveries->f('serial_number') : '',
        stripslashes($deliveries->f('customer_name')),
        preg_replace("/[\n\r]/","",stripslashes($deliveries->f('our_ref'))),
        preg_replace("/[\n\r]/","",stripslashes($deliveries->f('your_ref'))),
        date(ACCOUNT_DATE_FORMAT,$deliveries->f('delivery_date')),
        ($deliveries->f('item_code') ? $deliveries->f('item_code') : ''),
        preg_replace("/[\n\r]/","",stripslashes($deliveries->f('article'))),
        $deliveries->f('ordered_quantity'),
        $deliveries->f('delivered_quantity'),
       
        $deliveries->f('weight'),
        ($deliveries->f('origin_number') ? $deliveries->f('origin_number') : ''),
        $deliveries->f('customer_vat_number'),
        $base_price
      
    );

    array_push($final_data,$item);
}

header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$fp = fopen("php://output", 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
exit();

?>