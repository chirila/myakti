
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}
/*
$db2 = new sqldb();
$db3 = new sqldb();*/

$l_r =10;
if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
	$offset=0;
}
else
{
	$offset=$in['offset'];
}
$def_lang = DEFAULT_LANG_ID;
if($in['lang_id']){
	$def_lang= $in['lang_id'];
}
if($in['lang_id']>=1000) {
	$def_lang = DEFAULT_LANG_ID;
}

switch ($def_lang) {
	case '1':
		$text = gm('Name');
		break;
	case '2':
		$text = gm('Name fr');
		break;
	case '3':
		$text = gm('Name du');
		break;
	default:
		$text = gm('Name');
		break;
}

$cat_id = $in['cat_id'];
//$filter.="pim_articles_lang.lang_id='".$def_lang."'";
$filter.="1=1";

if(!$in['from_address_id']) {
	$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id  AND pim_article_prices.base_price=1
						 
						   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
					 AND pim_articles_lang.lang_id=\''.$def_lang.'\'	   
					 LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id';
	if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
    	$table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
  	}
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
				pim_article_prices.price, pim_articles.internal_name,
				pim_articles_lang.description AS description,
				pim_articles_lang.name2 AS item_name2,
				pim_articles_lang.name AS item_name,
				pim_articles_lang.lang_id,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference,
				pim_article_categories.name AS article_category,
				pim_articles.weight,
						pim_articles.has_variants';

}else{
	$table = 'pim_articles  LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=1
	                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
					   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
					   LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id';
	if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
    	$table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
  	}
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,
				pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

				pim_articles_lang.description AS description,
				pim_articles_lang.name2 AS item_name2,
				pim_articles_lang.name AS item_name,
				pim_articles_lang.lang_id,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference,
				pim_article_categories.name AS article_category,
				pim_articles.weight,
						pim_articles.has_variants';
}

if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
    $filter.=' AND pim_article_variants.article_id IS NULL ';
}

//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

if ($in['search'])
{
	$filter.=" AND (pim_articles.supplier_reference LIKE '%".$in['search']."%' OR pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%'  OR pim_articles_lang.name LIKE '%".$in['search']."%')";
	// $arguments.="&search=".$in['search'];
}
if ($in['hide_article_ids'])
{
	$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
	// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
}
// if ($in['lang_id'])
// {

// 	$arguments.="&lang_id=".$in['lang_id'];
// }
// if ($in['is_purchase_order'])
// {

	// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
// }
if ($in['show_stock'])
{
	$filter.=" AND pim_articles.hide_stock=0";
	// $arguments.="&show_stock=".$in['show_stock'];
}
if ($in['from_customer_id1'])
{
	//$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
	// $arguments.="&from_customer_id=".$in['from_customer_id'];
}
if ($in['from_address_id1'])
{
	//$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
	// $arguments.="&from_address_id=".$in['from_address_id'];
}
if($in['article_id']){
	$filter.=" AND  pim_articles.article_id=".$in['article_id'];
}

$articles= array( 'lines' => array());
// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

$article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='P_ORDER_FIELD_LABEL'");


$time = time();

$j=0;
while($article->next()){
	$vat = $db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
	if($in['customer_id']){
		$cust_vat = $db->field("SELECT value FROM vats WHERE vat_id=(SELECT vat_id FROM customers WHERE customer_id='".$in['customer_id']."') ");		
		if($cust_vat == 0){
			$vat = '0';
		}
	}

	$values = $article->next_array();
	$values['weight'] = display_number($values['weight']);

	$tags = array_map(function($field){
		return '/\[\!'.strtoupper($field).'\!\]/';
	},array_keys($values));

	$label = preg_replace($tags, $values, $fieldFormat);

	$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");
	$code = $article->f('item_code');

	if($in['customer_id']){
      $supplier_article = $db->query("SELECT purchasing_price, supplier_ref FROM pim_article_references WHERE article_id='".$article->f('article_id')."' AND supplier_id='".$in['customer_id']."' ");
      if($supplier_article->move_next()){
        if($supplier_article->f('purchasing_price')>0){
      		$purchase_price = $supplier_article->f('purchasing_price');
      	}
      	if($supplier_article->f('supplier_ref')){
      		$code = $supplier_article->f('supplier_ref');
      	}      
      }
    }


	$base_price = $purchase_price;
	$price = $purchase_price;
 
	$linie = array(
	  	'article_id'				=> $article->f('article_id'),
	  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
	  	//'name'						=> htmlspecialchars_decode($article->f('internal_name')),
	  	'name'						=> html_entity_decode(stripslashes($article->f('internal_name')), ENT_QUOTES, 'UTF-8'),
	  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
	  	'article_category'			=> $article->f('article_category'),
	  	'weight'					=> display_number($article->f('weight')),
	    'stock'						=> $article->f('stock'),
	    'stock2'					=> remove_zero_decimals_dn(display_number($article->f('stock'))),
	    'quantity'		    		=> 1,
	    'pending_articles'  		=> intval($pending_articles),
	    'threshold_value'   		=> $article->f('article_threshold_value'),
	  	'sale_unit'					=> $article->f('sale_unit'),
	  	'percent'           		=> $vat_percent,
		'percent_x'         		=> display_number($vat_percent),
	    'packing'					=> remove_zero_decimals($article->f('packing')),
	  	'code'		  	    		=> $code,
	  	'supplier_reference'	   => defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? '' : ($article->f('supplier_reference')?'/ '.$article->f('supplier_reference'):''),
	  	'supplier_reference_x'	   => $article->f('supplier_reference'),
		//'price'						=> $price,
		'price'						=> $article->f('is_service') == 1 ? $article->f('price') : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $purchase_price) ,
		'price_vat'					=> $in['remove_vat'] == 1 ? (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price)  : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price + (($price*$vat)/100)),
		'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
		'purchase_price'			=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants')? 0 : $purchase_price,
		'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
		'quoteformat'    			=> htmlspecialchars_decode(gfn($label)),
		'base_price'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ?place_currency(display_number_var_dec(0))  : place_currency(display_number_var_dec($base_price)),
		'show_stock'				=> $article->f('hide_stock') ? false:true,
		'hide_stock'				=> $article->f('hide_stock'),
		'is_service'				=> $article->f('is_service'),
		'allow_stock'               =>ALLOW_STOCK == 1 ? true : false,
		'has_variants'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? true : false,
		 'has_variants_done'		=> 0,
		 'is_variant_for'				=> 0

	);
	array_push($articles['lines'], $linie);
  	
}

$articles['customer_id'] 		= $in['customer_id'];
$articles['lang_id'] 				= $in['lang_id'];
$articles['cat_id'] 				= $in['cat_id'];
$articles['txt_name']			  = $text;
$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

global $database_config;

    $db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

$db_users = new sqldb($db_config);
if(in_array(12,explode(';', $db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']])))){
	array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));
}

if($in['from_order_page']===true){
	return json_out($articles,true,false);
}
json_out($articles);

