<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	$db = new sqldb();

	$order_by_array = array('delivery_date','serial_number','order_date','customer_name');

    $l_r =ROW_PER_PAGE;
    $result = array('query'=>array(),'max_rows'=>0,'lr'=>$l_r);
	
	$order_by = " ORDER BY delivery_date DESC ";

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$filter=" 1=1 ";
	$filter_article="";

	if($in['search']){
		$filter.=" AND (pim_p_orders.serial_number LIKE '%".$in['search']."%' OR pim_p_orders.customer_name LIKE '%".$in['search']."%' OR pim_p_orders.our_ref LIKE '%".$in['search']."%' OR pim_p_orders.your_ref LIKE '%".$in['search']."%' )";
		$arguments.="&search=".$in['search'];
	}

	if($in['item_code']){
		$filter_article.=" INNER JOIN pim_p_orders_delivery ON pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id
		INNER JOIN pim_p_order_articles ON pim_p_orders_delivery.order_articles_id=pim_p_order_articles.order_articles_id
        INNER JOIN pim_articles ON pim_articles.article_id=pim_p_order_articles.article_id ";
        $filter.=" AND pim_articles.item_code LIKE '%".$in['item_code']."%' ";
	}

	if($in['start_date'] && !empty($in['start_date'])){
		$in['start_date'] =strtotime($in['start_date']);
		$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
	}
	if($in['stop_date'] && !empty($in['stop_date'])){
		$in['stop_date'] =strtotime($in['stop_date']);
		$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
	}

	if($in['start_date'] && $in['stop_date']){
		$filter.=" AND pim_p_order_deliveries.date BETWEEN '".$in['start_date']."' AND '".$in['stop_date']."' ";
	}
	else if($in['start_date']){
		$filter.=" AND pim_p_order_deliveries.date > ".$in['start_date']." ";
	}
	else if($in['stop_date']){
		$filter.=" AND pim_p_order_deliveries.date < ".$in['stop_date']." ";
	}

	if($in['order_by']){
	    if(in_array($in['order_by'], $order_by_array)){
		    $order = " ASC ";
		    if($in['desc'] == '1' || $in['desc']=='true'){
		        $order = " DESC ";
		    }
		    $order_by =" ORDER BY ".$in['order_by']." ".$order;
	    }
	}

	$filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";

	$result['max_rows']=$db->field("SELECT COUNT(DISTINCT(pim_p_order_deliveries.delivery_id)) FROM pim_p_order_deliveries 
		LEFT JOIN pim_p_orders ON pim_p_order_deliveries.p_order_id=pim_p_orders.p_order_id
		".$filter_article."
		WHERE ".$filter." ");

	$deliveries = $db->query("SELECT pim_p_order_deliveries.date AS delivery_date,pim_p_orders.serial_number,pim_p_orders.buyer_ref,pim_p_orders.customer_name,pim_p_orders.date AS order_date FROM pim_p_order_deliveries 
		LEFT JOIN pim_p_orders ON pim_p_order_deliveries.p_order_id=pim_p_orders.p_order_id
		".$filter_article."
		WHERE ".$filter." GROUP BY pim_p_order_deliveries.delivery_id ".$order_by.$filter_limit." ");
	while($deliveries->next()){
		$ref = '';
		if(ACCOUNT_ORDER_REF && $deliveries->f('buyer_ref')){
			$ref = $deliveries->f('buyer_ref');
		}

		$item=array(
			'delivery_date'	=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('delivery_date')),
			'serial_number'	=> $deliveries->f('serial_number') ? $ref.$deliveries->f('serial_number') : '',
			'customer_name'	=> stripslashes($deliveries->f('customer_name')),
			'order_date'	=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('order_date'))
		);
		array_push($result['query'], $item);
	}

	json_out($result);

?>