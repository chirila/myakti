<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
session_write_close();
ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf_type =array(4,5);
$fontname = TCPDF_FONTS::addTTFfont(__DIR__.'/../../../fonts/FreeSans.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, '', 10, '', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Purchase Order');
$pdf->SetSubject('Purchase Order');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

if($in['custom_type']==1 && DATABASE_NAME=='SalesAssist_11'){
	include(__DIR__.'/p_order_print_caff4267_64a4_1db2_d49f2ab3760c.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='salesassist_2'){
	include(__DIR__.'/p_order_print_caff4267_64a4_1db2_d49f2ab3760c.php');
	return;
}elseif($in['custom_type']==1 && DATABASE_NAME=='caff4267_64a4_1db2_d49f2ab3760c'){ 
	include(__DIR__.'/p_order_print_caff4267_64a4_1db2_d49f2ab3760c.php');
	return;
}

if(in_array($in['type'], $pdf_type)){
	$pdf->SetMargins(10, 5, 10);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, 50);
	$pdf->setPrintFooter(false);
	$hide_all = 2;
}
$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();

$html = include('p_order_print_body.php');
// print($html);exit();
if(isset($in['print'])){
	print_r($html);exit();
}
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->SetY($height);
if(in_array($in['type'], $pdf_type)){
	$hide_all = 1;
	$pdf->SetAutoPageBreak(TRUE, 0);

	$htmls = include('p_order_print_body.php');

	$pdf->writeHTML($htmls, true, false, true, false, '');
}
$pdf->lastPage();

if($in['do']=='po_order-p_order_print'){
	doQueryLog();
   $pdf->Output($serial_number.'.pdf','I');
}else{
   $pdf->Output(__DIR__.'/../../../p_order_.pdf', 'F');
}