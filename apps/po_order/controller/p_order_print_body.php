<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db=new sqldb();
$db2=new sqldb();
$db3 = new sqldb();
$db4 = new sqldb();
$db5 = new sqldb();
$db_date=new sqldb();
$path = ark::$viewpath;

global $config,$database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);

	$dbu_users = new sqldb($db_config);

/*$in['type'] = $_SESSION['u_id']<10737 ? 2 : 1;*/
//$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$user = $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
if(!$user->f('main_user_id')){
	if($user->f('migrated')){
		$in['type']=2;
	}else{
		$in['type'] = $_SESSION['u_id']<10737 || $_SESSION['u_id']>11810  ? 2 : 1;
	}
}else{
	$main_user= $dbu_users->query("SELECT migrated, main_user_id FROM users WHERE user_id='".$user->f('main_user_id')."' ");
	if($main_user->f('migrated')){
		$in['type']=2;
	}else{
		$in['type'] = $user->f('main_user_id')<10737 || $_SESSION['u_id']>11810 ? 2 : 1;
	}
}

if(DATABASE_NAME=='ed59345d_7594_71f6_0a090ec71dff' || DATABASE_NAME=='5b9ac9b7_c414_b1b2_e9a50156f24b' || DATABASE_NAME=='1008e331_1c14_2d80_8c2d63f6b832' || DATABASE_NAME=='92fb9e67_ef94_b93a_da8b4a8789fe'){
   $in['type']=2; 
}
if( DATABASE_NAME=='salesassist_2'){
	$in['type']=2; //2 e varianta noua de pdf, cu gri :)
}

if($in['type']){
	$html='p_order_print_'.$in['type'].'.html';
}elseif ($in['body']){
	$html='customizable_print_'.$in['layout'].'.html';
}elseif($in['body_id']){
	$html='customizable_body_print_'.$in['body_id'].'.html';
}elseif($in['header']){
	$html='customizable_print_header_'.$in['layout'].'.html';
}elseif($in['layout'] || $in['footer_id']){
	$html='customizable_print_footer_'.$in['layout'].'.html';
}else{
	$html='po_print_1.html';
}

// echo $path.$html;exit();
// $html='invoice_print_6.html';
$view = new at($path.$html);

if($in['type']){
	$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PO_HEADER_PDF_FORMAT' ");
	$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PO_BODY_PDF_FORMAT' ");
	$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PO_FOOTER_PDF_FORMAT' ");
	$in['header_id']=$def_header;
	$in['footer_id']=$def_footer;

		$view->assign(array(
		'head'			  => true,
		'foot'			  => true,
		));
}

$labels_query = $db->query("SELECT * FROM label_language_p_order WHERE label_language_id='".$in['lid']."'")->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
	$labels[$key] = $value;
	$j++;
}
$labels_query_custom = $db->query("SELECT * FROM label_language_p_order WHERE lang_code='".$in['lid']."'")->getAll();
if(is_array($labels_query_custom['0'])){
	foreach ($labels_query_custom['0'] as $key => $value) {
		$labels[$key] = $value;
		$j++;
	}
}

if($in['lid']<=4){
	$labels_quotes = $db->query("SELECT * FROM label_language_p_order WHERE label_language_id='".$in['lid']."'")->getAll();
}else{
	$labels_quotes = $db->query("SELECT * FROM label_language_p_order WHERE lang_code='".$in['lid']."'")->getAll();
}
$labels_q = array();
foreach ($labels_quotes['0'] as $key => $value) {
	$labels_q[$key] = $value;
	$j++;
}


//$view = new at(ark::$viewpath.$html);
if(!$in['p_order_id']){ //sample
	if($in['header']){
		$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='1' AND layout='".$in['layout']."' ");
		if($data_exist1){
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
		}else{
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='0' AND layout='".$in['layout']."' ");
		}
		$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
		if($data_exist2){
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
		}else{
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
		}

	}elseif($in['header_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}elseif($in['footer_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}
//header1
	$needle = "[label]";
	$needle_end = "[/label]";
	$positions = array();
	$raw_data=array();
	$final_data=array();
	$initPos = 0;
	while (($initPos = strpos($content1, $needle, $initPos))!== false) {
	 $lastPos=strpos($content1, $needle_end, $initPos);
	    $positions[$initPos + strlen($needle)] = $lastPos;
	    $initPos = $initPos + strlen($needle);
	}

	foreach ($positions as $key=>$value) {	 
	    $raw_data[]=substr($content1,$key,$value-$key);
	}
	foreach($raw_data as $value){
	 if($labels[$value]){
			$final_data[$value]=$labels[$value];		
		}else{
	 		$final_data[$value]=$labels_q[$value];
		}
	}
	foreach($final_data as $key=>$value){
	 	$content1=str_replace("[label]".$key."[/label]",$value,$content1);
	}

//header1

//footer1
	$needle2 = "[label]";
	$needle_end2 = "[/label]";
	$positions2 = array();
	$raw_data2=array();
	$final_data2=array();
	$initPos2 = 0;
	while (($initPos2 = strpos($content2, $needle2, $initPos2))!== false) {
	 $lastPos2=strpos($content2, $needle_end2, $initPos2);
	    $positions2[$initPos2 + strlen($needle2)] = $lastPos2;
	    $initPos2 = $initPos2 + strlen($needle2);
	}

	foreach ($positions2 as $key=>$value) {	 
	    $raw_data2[]=substr($content2,$key,$value-$key);
	}
	foreach($raw_data2 as $value){
		if($labels[$value]){
			$final_data2[$value]=$labels[$value];		
		}else{
	 		$final_data2[$value]=$labels_q[$value];
		}
	}
	
	foreach($final_data2 as $key=>$value){
	 	$content2=str_replace("[label]".$key."[/label]",$value,$content2);
	}
//footer1w


	//assign dummy data
$height = 251;
	$factur_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));

	$pdf->setQuotePageLabel(getpo_label_txt('page',$in['lid']));

	$view->assign(array(
	'account_logo'        => '../../images/no-logo.png',
	'billing_address_txt' => getpo_label_txt('billing_address',$in['lid']),
	'p_order_note_txt'      => getpo_label_txt('p_order_note',$in['lid']),
	'p_order_txt'         => $in['entry_preview'] ? getpo_label_txt('entry_note',$in['lid']) : getpo_label_txt('p_order',$in['lid']),
	'invoice_txt'         => getpo_label_txt('invoice',$in['lid']),
	'date_txt'            => getpo_label_txt('date',$in['lid']),
	'customer_txt'        => getpo_label_txt('customer',$in['lid']),
	'article_txt'         => getpo_label_txt('article',$in['lid']),
	'unitmeasure_txt'     => getpo_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => getpo_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => getpo_label_txt('unit_price',$in['lid']),
	'total_amount_txt'    => getpo_label_txt('total_amount',$in['lid']),
	'amount_txt'          => getpo_label_txt('amount',$in['lid']),
	'subtotal_txt'        => getpo_label_txt('subtotal',$in['lid']),
	'discount_txt'        => getpo_label_txt('discount',$in['lid']),
	'vat_txt'             => getpo_label_txt('vat',$in['lid']),
	'payments_txt'        => getpo_label_txt('payments',$in['lid']),
	'amount_due_txt'      => getpo_label_txt('amount_due',$in['lid']),
	'notes_txt'           => getpo_label_txt('notes',$in['lid']),
	//	'duedate'			  => getpo_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => getpo_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => getpo_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => getpo_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => getpo_label_txt('iban',$in['lid']),
	'our_ref_txt'		  => getpo_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => getpo_label_txt('your_ref',$in['lid']),
	'sale_unit_txt'		  => wrap(geto_label_txt('sale_unit',$in['lid']), 9, ' '),
    'package_txt'		  => wrap(geto_label_txt('package',$in['lid']), 9, ' '),
	'phone_txt'			  => getpo_label_txt('phone',$in['lid']),
	'fax_txt'			  => getpo_label_txt('fax',$in['lid']),
	'url_txt'			  => getpo_label_txt('url',$in['lid']),
	'email_txt'			  => getpo_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => getpo_label_txt('vat_number',$in['lid']),
	'article_code_txt'    => getpo_label_txt('article_code',$in['lid']),
	'entry_date_txt'   	  => getpo_label_txt('entry_date',$in['lid']),
	'entry_note_txt'   	  => getpo_label_txt('entry_note',$in['lid']),
	'delivery_date'		  => date(ACCOUNT_DATE_FORMAT, time()),
	'order_date'          		=> date(ACCOUNT_DATE_FORMAT, time()),
	'is_delivery_date'	  => true,
	'address_info'	      => '[Address]<br>[Zip][City]<br>[Country]',
	'delivery_note'		=> '[Entry note]',
	'notes'			=> '[Notes]',

	'seller_name'         => '[Account Name]',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_CITY'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'seller_b_country'    => '[Billing Country]',
	'seller_b_state'      => '[Billing State]',
	'seller_b_city'       => '[Billing City]',
	'seller_b_zip'        => '[Billing Zip]',
	'seller_b_address'    => '[Billing Address]',
	'serial_number'       => '####',
	'invoice_date'        => $factur_date,
	'invoice_vat'         => '##',
	'buyer_name'       	  => '[Customer Name]',
	'buyer_country'    	  => '[Customer Country]',
	'buyer_state'      	  => '[Customer State]',
	'buyer_city'       	  => '[Customer City]',
	'buyer_zip'           => '[Customer ZIP]',
	'buyer_address'    	  => '[Customer Address]',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_city'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	//  'notes'            	  => '[NOTES]',
	'bank'				  => ACCOUNT_BANK_NAME,
	'bic'				  => ACCOUNT_BIC_CODE,
	'iban'				  => ACCOUNT_IBAN,
	// 'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'seller_b_fax'        => '[Account Fax]',
	'seller_b_email'      => '[Account Email]',
	'seller_b_phone'      => '[Account Phone]',
	'seller_b_url'        => '[Account URL]',
	'our_ref'			=> '[Our Reference]',
	'your_ref'			=> '[Your Reference]',
	'hide_f'			  => true,
	'hide_e'			  => true,
	'hide_p'			  => true,
	'hide_u'			  => true,
	'is_delivery'		  => true,
	'sh_discount'		  => 'hide',
	'sh_vat'			  => 'hide',
	'delivery'				=> $in['entry_preview'] ? false : true,
	));


	$i=0;
	while ($i<3)
	{
		$vat_total = 4;
		$total = 120;
		$total_with_vat = 120;
		$view->assign(array(
		'article'  			    	=> 'Article Name',
		'quantity'      			=> display_number(2),
		'price_vat'      			=> display_number(40),
		'price'         			=> display_number_var_dec(20),
		'percent'					=> display_number(5),
		'vat_value'					=> display_number(1),
		'is_article_code'		  => true,
		'content'			=> true,
		'article_width'		=> 40,
		'delivery'				=> $in['entry_preview'] ? false : true,
		'q_class'			  => $in['entry_preview'] ? 'last right' : '',
		),'order_row');

		if($in['type']==4 &&$in['entry_preview']){
			$view->assign('article_width',68,'order_row');
		}

		$view->loop('order_row');
		$i++;

	}
	$view->assign(array(
	'vat_total'				=> display_number($vat_total),
	'total'					=> display_number($total),
	'total_vat'				=> display_number($total_with_vat),
	'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
	'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
	'currency_type'			=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
	));


}else{
	$db = new sqldb();
	$db->query("SELECT pim_p_orders.*, multiple_identity.* FROM pim_p_orders
			LEFT JOIN multiple_identity ON pim_p_orders.identity_id=multiple_identity.identity_id
			LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
			WHERE pim_p_orders.p_order_id='".$in['p_order_id']."'");

	$db->move_next();
	$identity_id = $db->f('identity_id');
    if(!$identity_id){
        $identity_id =0;
    }

	if($in['header']){
		$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='1' AND layout='".$in['layout']."' AND identity_id ='".$identity_id."'");
		if($data_exist1){
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
		}else{
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='0' AND layout='".$in['layout']."' ");
		}
		$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' AND identity_id ='".$identity_id."' ");
		if($data_exist2){
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
		}else{
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
		}

	}elseif($in['header_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='1' AND layout='".$in['header_id']."' AND identity_id ='".$identity_id."'");

			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' AND identity_id ='".$identity_id."'");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}elseif($in['footer_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='1' AND layout='".$in['header_id']."' AND identity_id ='".$identity_id."'");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' AND identity_id ='".$identity_id."'");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}


//header1
	$needle = "[label]";
	$needle_end = "[/label]";
	$positions = array();
	$raw_data=array();
	$final_data=array();
	$initPos = 0;
	while (($initPos = strpos($content1, $needle, $initPos))!== false) {
	 $lastPos=strpos($content1, $needle_end, $initPos);
	    $positions[$initPos + strlen($needle)] = $lastPos;
	    $initPos = $initPos + strlen($needle);
	}

	foreach ($positions as $key=>$value) {	 
	    $raw_data[]=substr($content1,$key,$value-$key);
	}
	foreach($raw_data as $value){
	 	if($labels[$value]){
			$final_data[$value]=$labels[$value];		
		}else{
	 		$final_data[$value]=$labels_q[$value];
		}
	}
	foreach($final_data as $key=>$value){
	 	$content1=str_replace("[label]".$key."[/label]",$value,$content1);
	}

//header1

//footer1
	$needle2 = "[label]";
	$needle_end2 = "[/label]";
	$positions2 = array();
	$raw_data2=array();
	$final_data2=array();
	$initPos2 = 0;
	while (($initPos2 = strpos($content2, $needle2, $initPos2))!== false) {
	 $lastPos2=strpos($content2, $needle_end2, $initPos2);
	    $positions2[$initPos2 + strlen($needle2)] = $lastPos2;
	    $initPos2 = $initPos2 + strlen($needle2);
	}

	foreach ($positions2 as $key=>$value) {	 
	    $raw_data2[]=substr($content2,$key,$value-$key);
	}

	foreach($raw_data2 as $value){
	 	if($labels[$value]){
			$final_data2[$value]=$labels[$value];		
		}else{
	 		$final_data2[$value]=$labels_q[$value];
		}
	}

	foreach($final_data2 as $key=>$value){
	 	$content2=str_replace("[label]".$key."[/label]",$value,$content2);
	}

//footer1w
	/*$db = new sqldb();
	$db->query("SELECT pim_p_orders.*, multiple_identity.* FROM pim_p_orders
			LEFT JOIN multiple_identity ON pim_p_orders.identity_id=multiple_identity.identity_id
			LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
			WHERE pim_p_orders.p_order_id='".$in['p_order_id']."'");

	$db->move_next();*/

	$apply_discount = $db->f('apply_discount');
	$global_disc = $db->f('discount');
    if ($apply_discount < 2) {
        $global_disc = 0;
    }

	//Get customer information
	$cdb = new sqldb();
	$resultcdb = $cdb->query("SELECT * FROM customers WHERE customer_id ='".$db->f('customer_id')."'");
	$resultcdb->move_next();

	if (!empty($ref = $db->f('buyer_ref'))){
        $serial_number = $ref.$db->f('serial_number');
    } else {
        $serial_number = $db->f('serial_number');
    }
    $allow_article_packing=($db->f('use_package') && ALLOW_ARTICLE_SALE_UNIT==1 && ALLOW_ARTICLE_PACKING==1)? 1:0;
    $allow_article_sale_unit=($db->f('use_sale_unit') && ALLOW_ARTICLE_SALE_UNIT==1 && ALLOW_ARTICLE_PACKING==1)? 1:0;
	$currency = get_commission_type_list($db->f('currency_type'));
	$vat = $db->f('vat_percent');

	$currency_type = $db->f('currency_type');
	$currency_rate = $db->f('currency_rate');


	$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
	$delivery_date = $db->f('del_date') ? date(ACCOUNT_DATE_FORMAT,  $db->f('del_date')) : '';
	$deliv_date = $db_date->field("SELECT date FROM pim_p_order_deliveries WHERE p_order_id = '".$in['p_order_id']."' AND delivery_id = '".$in['delivery_id']."' ");
	$db_date->query("SELECT minute,hour,pm FROM pim_p_order_deliveries WHERE p_order_id = '".$in['p_order_id']."' AND delivery_id = '".$in['delivery_id']."' ");
	$delivery_del_date = date(ACCOUNT_DATE_FORMAT,  $deliv_date);
	//	$due_date = date(ACCOUNT_DATE_FORMAT,  $db->f('due_date'));
	if($db_date->f('hour')){
		$delivery_del_date.= ' ('.$db_date->f('hour').':'.$db_date->f('minute').' '.$db_date->f('pm').')';
	}

	$img = 'images/no-logo.png';
	$attr = '';
	if($db->f('company_logo')){
		$print_logo_idnt=$db->f('company_logo');
	}
	if(ACCOUNT_LOGO){
		$img = ACCOUNT_LOGO;
		if($print_logo_idnt){
			$img=$print_logo_idnt;
		}
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="120"';
		}
	}
	  $buyer_adr_second=$db3->query("SELECT * FROM customer_addresses WHERE customer_id = '".$db->f('second_customer_id')."' and address_id='".$db->f('second_address_id')."'");
	  $buyer_second_name = $db3->field("SELECT name FROM customers WHERE customer_id = '".$db->f('second_customer_id')."'");

	    $in['second_address_info']=$buyer_adr_second->f('address').'
	'.$buyer_adr_second->f('zip').'  '.$buyer_adr_second->f('city').'
	'.get_country_name($buyer_adr_second->f('country_id'));

	$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'p_order'
								   AND item_name = 'notes'
								   AND active = '1'
								   AND item_id = '".$in['p_order_id']."' ");

	$buyer_reg_number = $db3->field("SELECT comp_reg_number FROM customers WHERE customer_id = '".$db->f('customer_id')."'");

	$main_adddress_info=nl2br(trim($db->f('address_info')));
	//do no change these - akti - 3441 (fix in the document itself)
	/*$main_address_data=$db3->query("SELECT * FROM customer_addresses WHERE address_id = '".$db->f('main_address_id')."'");
	while($main_address_data->next()){
		$main_adddress_info=nl2br($main_address_data->f('address').'
	'.$main_address_data->f('zip').'  '.$main_address_data->f('city').'
	'.get_country_name($main_address_data->f('country_id')));
	}*/
	//end

	$pdf->setQuotePageLabel(getpo_label_txt('page',$in['lid']));
	$view->assign(array(
	'account_logo'        => file_exists($img) ? $img : 'images/no-logo.png' ,
	'attr'				  => $attr,
	'billing_address_txt' => getpo_label_txt('billing_address',$in['lid']),
	'p_order_note_txt'    => getpo_label_txt('p_order_note',$in['lid']),
	'p_order_txt'         => $in['delivery_id'] ? getpo_label_txt('entry_note',$in['lid']) : getpo_label_txt('p_order',$in['lid']),
	'date_txt'            => getpo_label_txt('date',$in['lid']),
	'customer_txt'        => getpo_label_txt('customer',$in['lid']),
	'company_name'        => getpo_label_txt('company_name',$in['lid']),
	'article_txt'         => getpo_label_txt('article',$in['lid']),
	'delivery_address_txt'=> getpo_label_txt('delivery_address',$in['lid']),
	'unitmeasure_txt'     => getpo_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => getpo_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => getpo_label_txt('unit_price',$in['lid']),
	'total_amount_txt'    => getpo_label_txt('total_amount',$in['lid']),
	'amount_txt'          => getpo_label_txt('amount',$in['lid']),
	'subtotal_txt'        => getpo_label_txt('subtotal',$in['lid']),
	'discount_txt'        => getpo_label_txt('discount',$in['lid']),
	'vat'             => getpo_label_txt('vat',$in['lid']),
	'payments_txt'        => getpo_label_txt('payments',$in['lid']),
	'total_txt'      	  => getpo_label_txt('total',$in['lid']),
	'notes_txt'           => $notes ? getpo_label_txt('notes',$in['lid']) : '',
	'duedate_txt'		  => getpo_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => getpo_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => getpo_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => getpo_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => getpo_label_txt('iban',$in['lid']),
	'gov_taxes_txt'       => getpo_label_txt('gov_taxes',$in['lid']),
	'gov_taxes_code_txt'  => getpo_label_txt('gov_taxes_code',$in['lid']),
	'gov_taxes_type_txt'  => getpo_label_txt('gov_taxes_type',$in['lid']),
	//'sale_unit_txt'		  => getpo_label_txt('sale_unit',$in['lid']),
	//'package_txt'		  => getpo_label_txt('package',$in['lid']),
	'delivery_note_txt'	  => getpo_label_txt('delivery_note',$in['lid']),
	//'sale_unit_txt'		  => getpo_label_txt('sale_unit',$in['lid']),
	//'package_txt'		  => getpo_label_txt('package',$in['lid']),
	'our_ref_txt'		  => getpo_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => getpo_label_txt('your_ref',$in['lid']),
	'sale_unit_txt'		  => wrap(geto_label_txt('sale_unit',$in['lid']),9,' '),
    'packing_txt'		  => wrap(geto_label_txt('package',$in['lid']),9,' '),
	'phone_txt'			  => get_label_txt('phone',$in['lid']),
	'fax_txt'			  => get_label_txt('fax',$in['lid']),
	'url_txt'			  => get_label_txt('url',$in['lid']),
	'email_txt'			  => get_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => get_label_txt('vat_number',$in['lid']),
	'entry_date_txt'   	  => getpo_label_txt('entry_date',$in['lid']),
	'entry_note_txt'   	  => getpo_label_txt('entry_note',$in['lid']),
	'article_code_txt'    => getpo_label_txt('article_code',$in['lid']),

	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'client_delivery_address'	=>nl2br(trim($in['second_address_info'])),
	'serial_number'		  => $serial_number,
	'order_date'          => $factur_date,
	'buyer_name'       	  => $db->f('customer_name'),
	'buyer_country'    	  => get_country_name($db->f('customer_country_id')),
	'buyer_state'      	  => $db->f('customer_state'),
	'buyer_city'       	  => $db->f('customer_city'),
	'buyer_zip'        	  => $db->f('customer_zip'),
	'buyer_address'    	  => $db->f('customer_address'),
	'seller_name'         => $db->f('identity_id')? $db->f('company_name') : $db->f('seller_name'),
	'seller_d_country'    => $db->f('identity_id')? get_country_name($db->f('country_id')) : get_country_name($db->f('seller_d_country_id')),
	'seller_d_state'      => get_state_name($db->f('seller_d_state_id')),
	'seller_d_city'       => $db->f('identity_id')? $db->f('city_name') : utf8_decode($db->f('seller_d_city')),
	'seller_d_zip'        => $db->f('identity_id')? $db->f('company_zip') : $db->f('seller_d_zip'),
	'seller_d_address'    => $db->f('identity_id')? nl2br($db->f('company_address')) : nl2br(utf8_decode($db->f('seller_d_address'))),
	'p_order_contact_name'=> $db->f('contact_name'),
	'field_customer_id'   => $db->f('field')=='customer_id'?true:false,
	'buyer_vat'			  => ACCOUNT_VAT_NUMBER,
	'seller_b_vat'		  => ACCOUNT_VAT_NUMBER,
	//	'INVOICE_BUYER_VAT'   => ACCOUNT_VAT_NUMBER,
	'bank'				  => ACCOUNT_BANK_NAME,
	'bic'				  => ACCOUNT_BIC_CODE,
	'iban'				  => ACCOUNT_IBAN,
	'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'sh_discount' 		  => $apply_discount < 2 ? false : true,
	'discount_percent'    => ' ( '.$db->f('discount').'% )',
	'notes'               =>  $notes ? nl2br($notes) : '',
	'view_notes'          => $notes ? '' : 'hide' ,
	'hide_default_total'  => 'hide',//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide',
	'def_currency'		  => build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
	/*'your_ref'       	  => $db->f('your_ref'),
	'our_ref' 			  => $db->f('our_ref'),*/
	'our_ref' 	 		  => htmlspecialchars(html_entity_decode(stripslashes($db->f('our_ref')), ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8'),
	'your_ref'		      => htmlspecialchars(html_entity_decode(stripslashes($db->f('your_ref')), ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8'),
	'hide_our_ref'		  => $db->f('our_ref')? '':'hide',
	'hide_your_ref'		  => $db->f('your_ref')? '':'hide',

	'seller_b_fax'        => ACCOUNT_FAX,
	'seller_b_email'      => ACCOUNT_EMAIL,
	'seller_b_phone'      => ACCOUNT_PHONE,
	'seller_b_url'        => ACCOUNT_URL,

	'hide_f'			  => defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? false : true,
	'hide_e'			  => defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? false : true,
	'hide_p'			  => defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? false : true,
	'hide_u'			  => defined('ACCOUNT_URL') && !ACCOUNT_URL ? false : true,
	//'address_info'	      => nl2br($db->f('address_info')),
	'address_info'		=> $main_adddress_info,
	'delivery_date'		  => $delivery_date,
	'is_delivery_date'	  => ($in['delivery_id'] && $delivery_date) ? false : true,
	'delivery_del_date'	  => $delivery_del_date,
	'is_delivery_del_date'=> ($in['delivery_id'] && $delivery_del_date) ? true : false,
	'company_iban'		  => ACCOUNT_IBAN,
	'company_bic_code'	  => ACCOUNT_BIC_CODE,
	'company_vat'		  => ACCOUNT_VAT_NUMBER,

	//do no change these - akti - 3441 (fix in the document itself)
	/*'supplier_vat'		  => $db->f('customer_vat_number') ? $db->f('customer_vat_number') : $resultcdb->f('btw_nr'), 
	'supplier_phone'	  => $db->f('customer_phone') ? $db->f('customer_phone') : $resultcdb->f('comp_phone'),*/
	'supplier_vat'		  => $db->f('customer_vat_number'), 
	'supplier_phone'	  => $db->f('customer_phone'),
	//end

	'buyer_reg_number'   		=> $buyer_reg_number,
	'seller_reg_number'         => $db->f('identity_id')? $db->f('company_reg_number') : ACCOUNT_REG_NUMBER,
    'seller_b_reg_number'       => $db->f('identity_id')? $db->f('company_reg_number') : ACCOUNT_REG_NUMBER,

	));
$height = 251;
	$discount_percent= $db->f('discount');
	if($db->f('discount') == 0){
		//$view->assign('sh_discount','hide');
		$discount_procent=0;
	} else {
		$discount_procent = $db->f('discount');
		$view->assign('total_discount_procent',$db->f('discount'));
	}

 if($db->f('second_customer_id') && $db->f('client_delivery')){
 	
	$view->assign(array(
				//'delivery_address'=>nl2br($db->f('delivery_address')),
				'customer_name' =>$buyer_second_name,
				'delivery_address'=>nl2br(trim($in['second_address_info'])),
				//'buyer_address'=>nl2br($db->f('delivery_address')),
				'is_delivery'=>true
			));
}else{
	$view->assign(array(
				'customer_name' =>'',
				'delivery_address'=>'',
				//'buyer_address'=>'',
				'is_delivery'=>false
			));
}

	$i=0;
	$discount_total = 0;
	$show_discount = true;

	$vat_total = 0;
	$total = 0;
	$total_with_vat = 0;
	if($in['delivery_id']){
		$first_line_content_data = $db4->query("SELECT article, content FROM pim_p_order_articles WHERE p_order_id = '".$in['p_order_id']."' ORDER BY order_articles_id ASC LIMIT 1");
		$first_line_content_data->next();
		$is_first_content_line = false;
		if($first_line_content_data->f('content')=='1'){
			$is_first_content_line = true;
			$view->assign(array(
			'is_content_line2'			=> true,
			'content_line2'				=> $first_line_content_data->f('article'),
			'is_first_content_line'		=> $is_first_content_line,
			));

		}
		$db->query("SELECT pim_p_order_articles.*, pim_p_orders_delivery.* FROM pim_p_order_articles
					INNER JOIN pim_p_orders_delivery ON pim_p_orders_delivery.order_articles_id=pim_p_order_articles.order_articles_id
					WHERE pim_p_orders_delivery.p_order_id='".$in['p_order_id']."' AND pim_p_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");
		$content_line_data = $db4->query("SELECT order_articles_id, article FROM pim_p_order_articles WHERE p_order_id = '".$in['p_order_id']."' AND content = '1' ");
		$is_content_line = false;
		$content_line = array();
		while($content_line_data->next()){
			$content_line[$content_line_data->f('order_articles_id')] = $content_line_data->f('article');
			$is_content_line = true;
		}
	}else{
		$db->query("SELECT pim_p_order_articles.* FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND has_variants='0' ORDER BY sort_order ASC");
		$is_content_line = false;
	}
	while ($db->move_next())
	{
		if ($apply_discount == 1 || $apply_discount == 3) {
            $show_discount_total = true;
        }
		if ($in['type']==4 && $in['delivery_id'])
		{
			if($delivery_date || $delivery_del_date){
				$view->assign('width_if_del','40');
			}else{
				$view->assign('width_if_del','68');
			}
			$view->assign('article_width','68','order_row');
		}
		elseif($in['type']==4 && !$in['delivery_id'] && $db->f('content')){
			$view->assign('article_width','94','order_row');
		}
		else{

			$view->assign('article_width','38','order_row');

			if($delivery_date || $delivery_del_date){
				$view->assign('width_if_del','40');
			}else{
				$view->assign('width_if_del','68');
			}
		}

		if($is_content_line == true){
			foreach ($content_line as $key => $value) {
				$after_order_articles_id = $db->f('order_articles_id')+1;
				// echo $after_order_articles_id; echo " / "; echo $key; echo "<br>";
				if($key == $after_order_articles_id){
					$is_spec_content_line = true;
					$spec_content_line = $value;
					goto found;
				}else{
					$is_spec_content_line = false;
				}
			}
		}
		found:
		$discount_line = $db->f('discount');
        if ($apply_discount == 0 || $apply_discount == 2) {
            $discount_line = 0;
        }
        if ($apply_discount < 2) {
            $global_disc = 0;
        }
		// var_dump($is_content_line);
		// var_dump($is_spec_content_line);
		//$discount_total += 0;//($db->f('price')*$db->f('discount')/100)*$db->f('quantity')*($db->f('packing')/$db->f('sale_unit'));
		$price_line = $db->f('price') - ($db->f('price') * $discount_line / 100);
		$discount_total += $price_line * $global_disc / 100 * $db->f('quantity')* ($db->f('packing')/$db->f('sale_unit'));
		$vat_total += ($price_line*$db->f('vat_percent')/100)*$db->f('quantity')* ($db->f('packing')/$db->f('sale_unit'));
		$total += $price_line*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));
		// $total_with_vat += $db->f('price_with_vat')*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));


		//if show serial numbers
		if($in['delivery_id'] && $is_content_line == false && (SHOW_ART_S_N_ORDER_PDF==1)){
			$art_serial_number_list = '';
			$use_serial_no = $db3->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$db->f('article_id')."' ");
			if($use_serial_no){
				$serial_number_data = $db3->query("SELECT serial_number FROM serial_numbers WHERE p_delivery_id = '".$in['delivery_id']."' AND article_id = '".$db->f('article_id')."' ");
				$k=0;
				while($serial_number_data->next()){
					$art_serial_number_list .= "<br/>".$serial_number_data->f('serial_number');
					$k++;
				}
				$art_serial_number_list = "<br/>".gm("Serial Numbers").':'.$art_serial_number_list;
				$view->assign(array(
					'is_serial_number'		=> $k > 0 ? true : false,
					// 'count_s_n'				=> $k+1,
					// 'serial_nr_txt'			=> wrap2(gm('Serial Numbers')),
					'art_serial_number_list'	=> $art_serial_number_list,
				),'order_row');
			}
		}

		//if show batch numbers
		if($in['delivery_id'] && $is_content_line == false && (SHOW_ART_B_N_ORDER_PDF==1)){
			$art_batch_number_list = '';
			$use_batch_no = $db3->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$db->f('article_id')."' ");
			if($use_batch_no){
				$batch_number_data = $db3->query("SELECT * FROM batches WHERE p_delivery_id = '".$in['delivery_id']."' AND article_id = '".$db->f('article_id')."' ");
				$k=0;
				while($batch_number_data->next()){
					$art_batch_number_list .= "<br/>".$batch_number_data->f('batch_number');
					$k++;
				}
				$art_batch_number_list = "<br/>".gm("Batches").':'.$art_batch_number_list;
				$view->assign(array(
					'is_batch_number'		=> $k > 0 ? true : false,
					'art_batch_number_list'	=> $art_batch_number_list,
				),'order_row');
			}
		}

		if($db->f('is_variant_for')){
            $parentVariantDescription=$db4->field("SELECT article FROM pim_p_order_articles WHERE p_order_id = '".$in['p_order_id']."' AND article_id='".$db->f('is_variant_for')."' AND order_articles_id<'".$db->f('order_articles_id')."' LIMIT 1");
            //$descr=$parentVariantDescription ? $parentVariantDescription.' '.$db->f('article') : $db->f('article');
            $descr=$db->f('article');//requested akti-3934
        }else{
            $descr=$db->f('article');
        }


		$view->assign(array(
		'article'  			    	=> $db->f('content') ? html_entity_decode($descr) : nl2br($descr),
		'quantity'      			=> display_number($db->f('quantity')),
		'price_vat'      			=> display_number($db->f('quantity') * $price_line * ($db->f('packing') / $db->f('sale_unit'))),
		'total_amount'      		=> display_number($db->f('quantity') * ($db->f('packing') / $db->f('sale_unit'))),
		'price'         			=> display_number_var_dec($db->f('price')),
		'percent'					=> display_number($db->f('vat_percent')),
		'vat_value'					=> display_number($db->f('vat_value')),
		'discount'					=> display_number($db->f('discount')),
		'sale_unit'					=> $db->f('sale_unit'),
		'price_with_vat'            => display_number($db->f('price')+$db->f('vat_value')),
		'packing'					=> remove_zero_decimals($db->f('packing')),
		'q_class'					=> $in['delivery_id'] ? 'last' : '',
		//'colspan'					=> $db->f('content') ? ' colspan="8" ' : ( $db->f('article_id') || $db->f('tax_id') ? '': ' colspan="2" '),
		'colspan'					=> $db->f('content') ? ' colspan="8" ' : '',
		'content'					=> $db->f('content') ? false : true,
		'content_class'				=> $db->f('content') ? ' last_nocenter ' : '',
		/*'is_article_code'			=> ($db->f('supplier_reference') || $db->f('article_code')) ? true : false,*/
		'is_article_code'			=> ($db->f('article_id') || $db->f('quantity')!=0) ? true : false,
		/*'article_code'				=> $db->f('supplier_reference')? $db->f('supplier_reference'):$db->f('article_code'),*/
		'article_code'				=> ($db->f('article_id') || $db->f('quantity')!=0) ? ($db->f('supplier_reference')? $db->f('supplier_reference') : $db->f('article_code')) : ' ',
		'is_content_line'			=> ($is_content_line && $is_spec_content_line) ? true : false,
		'content_line'				=> $spec_content_line,
		'delivery' => $in['delivery_id'] ? false : true,
		),'order_row');

		$view->loop('order_row');

		$view->assign(array('delivery_note'	=> nl2br($db->f('delivery_note'))));
		$delivery_note = $db->f('delivery_note');

		$i++;

	}
	$view->assign('delivery',$in['delivery_id'] ? false : true);

// $discount_total=$total*(($discount_percent)/100);

	$total=$total-$discount_total;

	$total_with_vat=$vat_total+$total;

	if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
			$total_default = $total_with_vat*return_value($currency_rate);
		}

	$view->assign(array(
	//'discount_total'        		=> place_currency(display_number(number_format($discount_total,2)),get_commission_type_list($currency_type)),
	'discount_total'        		=> place_currency(display_number($discount_total),get_commission_type_list($currency_type)),	
	'vat_total'						=> place_currency(display_number($vat_total),get_commission_type_list($currency_type)),
	'total'							=> place_currency(display_number($total),get_commission_type_list($currency_type)),
	'total_vat'						=> place_currency(display_number($total_with_vat + $total_gov_taxes),get_commission_type_list($currency_type)),
	'gov_taxes_value'       		=> place_currency(display_number($total_gov_taxes),get_commission_type_list($currency_type)),
	'total_amount_due_default'		=> place_currency(display_number($total_default)),
	'hide_currency2'				=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
	'hide_currency1'				=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
	'currency'						=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
	'discount_percent' => $show_discount ? ' ( ' . $discount_percent . '% )' : "",
    'sh_discount_total' => $apply_discount == 1 || $apply_discount == 3 ? true : false,
	));

	if($vat_total == 0){
		$view->assign('sh_vat','hide');
	}

}



if($in['delivery_id']){
	$view->assign(array(
		'article_width'			=> 69,
		'show_delivery_note'	=> $delivery_note ? true : false ));
	$article_width =69;
}else{
	$view->assign(array(
		'article_width'			=> 47,
		'show_delivery_note'	=> false ));
	$article_width =47;
	if($show_discount_total){
		$article_width-=12;
	}
}


if($allow_article_sale_unit && $allow_article_packing){
	$view->assign('article_width',$article_width-23);
	if($in['delivery_id']){
		$view->assign('article_width',$article_width-13);
	}
}elseif($allow_article_packing && !$allow_article_sale_unit){
	$view->assign('article_width',$article_width-23);
	if($in['delivery_id']){
		$view->assign('article_width',$article_width-13);
	}
}else{
	$view->assign('article_width',$article_width);
}

if($in['entry_preview']){
	$view->assign('show_delivery_note'	, $in['entry_preview'] ? true : false);
}

$view->assign(array(
    	'allow_article_packing'	=> $allow_article_packing,
       	'allow_article_sale_unit'=> $allow_article_sale_unit
));

if($in['entry_preview']){
	$view->assign('article_width'	, 69 );
}

$is_table = true;
$is_data = false;

#for layout nr 4.
if($hide_all == 1){
	// $view->assign('hide_all','hide');
	$is_table = false;
	$is_data = true;
}else if($hide_all == 2){
	$is_table = true;
	$is_data = false;
	// $view->assign('hide_t','hide');
}

$view->assign(array(
	'is_table'=> $is_table,
	'is_data'=>$is_data,
));

$vars=$view->return_vars();
	$labels_query1 = $db->query("SELECT * FROM label_language WHERE label_language_id='".$in['lid']."'")->getAll();
	$labels1 = array();
	$j=1;
	foreach ($labels_query1['0'] as $key => $value) {
		$labels1[$key] = $value;
		$j++;
	}
	//dump all labels
	/*var_dump($labels1);*/



	$needle1 = "[!";
	$needle_end1 = "!]";
	$positions1 = array();
	$raw_data1=array();
	$final_data1=array();
	$initPos1 = 0;
	// var_dump($content1);	
	while (($initPos1 = strpos($content1, $needle1, $initPos1))!== false) {
	 	$lastPos1=strpos($content1, $needle_end1, $initPos1);
	    $positions1[$initPos1 + strlen($needle1)] = $lastPos1;
	    $initPos1 = $initPos1 + strlen($needle1);
	}

	foreach ($positions1 as $key=>$value) {	 
	    $raw_data1[]=substr($content1,$key,$value-$key);
	}

	foreach($raw_data1 as $value){
 		$final_data1[$value]=$vars->$value;
	}
	//dump all values
/*	var_dump($final_data1);
	exit();*/
	foreach($final_data1 as $key=>$value){
		if($key=='ACCOUNT_LOGO'){
			if (DATABASE_NAME == '9866660a_b374_59ab_779505722e8e'){
				$value='<img max-width="113px" height="113px" width="auto"  src="'.$img.'">';
			}else{
				$value='<img max-width="260px" height="160px" width="auto"  src="'.$img.'">';
			}
		}
/*		if(!$value){
			$value = '-';
		}*/
			
	 	$content1=str_replace("[!".$key."!]",$value,$content1);
	 	// if(!$value){
	 	// 	$content1=preg_replace('/\[if\:'.$key.'\!\](.*)\[end:'.$key.'\!\]/', '', $content1);
	 	// }
	}
//header1

//footer1
	$vars3=$view->return_vars();
	$needle3 = "[!";
	$needle_end3 = "!]";
	$positions3 = array();
	$raw_data3=array();
	$final_data3=array();
	$initPos3 = 0;
	while (($initPos3 = strpos($content2, $needle3, $initPos3))!== false) {
	 	$lastPos3=strpos($content2, $needle_end3, $initPos3);
	    $positions3[$initPos3 + strlen($needle3)] = $lastPos3;
	    $initPos3 = $initPos3 + strlen($needle3);
	}

	foreach ($positions3 as $key=>$value) {	 
	    $raw_data3[]=substr($content2,$key,$value-$key);
	}

	foreach($raw_data3 as $value){
 		$final_data3[$value]=$vars->$value;
	}
// var_dump($final_data3);
	foreach($final_data3 as $key=>$value){
		if($key=='ACCOUNT_LOGO'){
			$value='<img max-width="260px" height="160px" width="auto"  src="'.$img.'">';
		}
/*		if(!$value){
			$value = '-';
		}*/
		
	 	$content2=str_replace("[!".$key."!]",$value,$content2);
	}

//footer1
	if($in['header_id']){
		$pdf->setQuotePageLabel('salesassist_2');
		$pdf->Custom_footer($content2);
	}elseif($in['footer_id']){
		$pdf->setQuotePageLabel('salesassist_2');
		$pdf->Custom_footer($content2);

	}

	$view->assign(array(
		'HEADER'			  => $content1,
	));
//echo '<pre>';
//print_r($view);
//die();

return $view->fetch();