<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


$is_data = false;

$serial = array('s_n_row'=>array());

if($in['order_articles_id'] && intval(return_value($in['article_no']))>0){
	$is_data = true;	
	$count_c_s_n = 0;
	if($in['c_serial_numbers']){
		$c_serial_numbers = array_reverse($in['c_serial_numbers']);
		$used_serial_numbers = array_reverse($in['used_serial_numbers']);
		$serial_nr_ids = array_reverse($in['serial_nr_ids']);
		$count_c_s_n = count($in['c_serial_numbers']);
		if($count_c_s_n >= intval(return_value($in['article_no']))){
			$in['completed'] = intval(return_value($in['article_no']));
		}else{
			$in['completed'] = intval($count_c_s_n);
		}
	}
	for ($i=0; $i < intval(return_value($in['article_no'])); $i++) { 
		//$view_s_n->assign(array(
		array_push($serial['s_n_row'],array(	
			'order_articles_id'		=> $in['order_articles_id'],	
			'border_top'			=> $i == 0 ? 'border-top:1px solid #cccccc;' : '',
			'index'				=> $i,
			'serial_nr_id'			=> $serial_nr_ids[$i],
			'c_serial_numbers'		=> $c_serial_numbers[$i],
			'disable_edit_s_n'		=> $used_serial_numbers[$i] == 1 ? 'disabled' : '',
			'readonly'				=> $used_serial_numbers[$i] == 1 ? 'readonly="readonly"' : '',
			'disabled_s_n'			=> $used_serial_numbers[$i] == 1 ? 1 : '',
        ));
		//),'s_n_row');
		//$view_s_n->loop('s_n_row');
	}
	if(!$in['completed']){
		$in['completed']=0;
		$serial['completed'] = 0;
	}

	
		$serial['order_articles_id']	= $in['order_articles_id'];
		$serial['article_name']			= $in['article_name'];
		$serial['article_no']			= intval(return_value($in['article_no']));
		$serial['scroll']				= intval(return_value($in['article_no'])) > 10 ? 'overflow-y:scroll;' : '';
		$serial['total']				= intval(return_value($in['article_no']));		
		$serial['completed']			= $in['completed'] ? $in['completed']:0;
		$serial['is_completed']			= $in['completed'] < intval(return_value($in['article_no'])) ? '' : 'green';
		$serial['is_c_serial_numbers']	= $count_c_s_n > 0 ? false : true;
		$serial['delivery_id']			= $in['delivery_id'];		
	

}
	
	$serial['hide_for_view']= true;		

if($in['only_view_s_n']==1 && $in['selected_s_n']){
	$view_serial_numbers_data = $db->query("SELECT * FROM serial_numbers WHERE id IN(".$in['selected_s_n'].")");	
	
	$n = 0;
	while($view_serial_numbers_data->next()){
		//$view_s_n->assign(array(
		array_push($serial['s_n_row'],array(
			'order_articles_id'		=> $in['order_articles_id'],	
			'border_top'			=> $n == 0 ? 'border-top:1px solid #cccccc;' : '',
			'index'				=> $n,
			'readonly'				=> 'readonly="readonly"',
			'c_serial_numbers'		=> $view_serial_numbers_data->f('serial_number'),
			'disable_edit_s_n'		=> '',
			'disabled_s_n'			=> '',
			'serial_nr_id'			=> $view_serial_numbers_data->f('id'),
			));
		//),'s_n_row');
		//$view_s_n->loop('s_n_row');
		$i++;
	}
	$is_data = true;
			
		$serial['hide_for_view']		= false;	
		$serial['order_articles_id']	= $in['order_articles_id'];
		$serial['article_name']			= $in['article_name'];
		$serial['scroll']				= $n > 10 ? 'overflow-y:scroll;' : '';
	;	
}

if($i>0){
	$is_data = true;
}

$serial['is_data']				= $is_data;

json_out($serial);

?>