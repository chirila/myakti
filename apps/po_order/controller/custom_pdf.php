<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_codelabels($in,$showin=true,$exit=true){
		$db = new sqldb();
		
		$content='';
		if(!$in['identity_id']){
			$in['identity_id']=0;
		}
		if($in['header']=='header'){
			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}else{

			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}
		$data['header']=$in['header'];
		$data['footer']=$in['footer'];
		$data['variable_data'] = $content;
		$data['layout']=$in['layout'];
		$data['identity_id']=$in['identity_id'];
			
		return json_out($data, $showin,$exit);
	}

	function get_selectlabels($in,$showin=true,$exit=true){
		$db = new sqldb();

		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		$quote_columns=$db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_p_order' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ");
		$quote=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_p_order' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ");

		$columns=array();
		$make_out=array();
		if(!$in['make_id']){
			$in['make_id']='0';
		}

		$lang_code = $_SESSION['l'];
		$filter_lang= " lang_code ='".$lang_code."' ";
		if($lang_code=='nl' || $lang_code='du'){
			$filter_lang= " lang_code ='nl' OR lang_code ='du' ";
		}
		
		while($quote_columns->next()){
			$temp_description=$db->field("SELECT `{$quote_columns->f('COLUMN_NAME')}` FROM label_language_p_order WHERE $filter_lang ");
			if(!$temp_description){
				continue;
			}
			array_push($make_out, array(
            	'id_value'=>$quote_columns->f('COLUMN_NAME'),
            	'name'=>ucfirst($temp_description),
        	));
		}

		$data['make_id']=$in['make_id'];
		$data['make']=$make_out;

		asort($columns);
		

		$final_select='';
		foreach($columns as $key=>$value){
			array_push($data['labels'], array(
				'code'		=> $key,
				'value'		=> $value,
			));
		}
		
		$quote_colum=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_p_order' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active')  AND COLUMN_NAME='".$in['make_id']."'");

		if($data['make_id']!='0'){
			$data['selected_make']='[label]'.$quote_colum.'[/label]';
		}
		return json_out($data, $showin,$exit);
	}
	function get_selectdetails($in,$showin=true,$exit=true){
		$db = new sqldb();
		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		$array_values=array();
		$array_values['company_iban']=gm("IBAN Company");
		$array_values['company_bic_code']=gm("BIC company");
		$array_values['company_vat']=gm("Company VAT");
		$array_values['supplier_vat']=gm("Supplier VAT");
		$array_values['supplier_phone']=gm("Supplier Phone");

		$array_values['ACCOUNT_LOGO']=gm("Account Logo");
		$array_values['seller_name']=gm("Seller Name");
		$array_values['seller_d_address']=gm("Seller address");
		$array_values['seller_d_zip']=gm("Seller zip");
		$array_values['seller_d_city']=gm("Seller city");
		$array_values['seller_d_country']=gm("Seller country");
		$array_values['seller_b_phone']=gm("Buyer phone");
		$array_values['seller_b_fax']=gm("Buyer fax");
		$array_values['seller_b_email']=gm("Buyer email");
		$array_values['seller_b_url']=gm("Buyer url");
		$array_values['buyer_name']=gm("Buyer name");
		$array_values['customer_name']=gm("Customer name");
		$array_values['address_info']=gm("Address Information");
		$array_values['delivery_address']=gm("Delivery Address");
		$array_values['bank']=gm("Bank Name");
		$array_values['iban']=gm("Iban");
		$array_values['bic']=gm("Bic Code");
		$array_values['serial_number']=gm("Serial Number");
		$array_values['order_date']=gm("Order date");
		$array_values['delivery_date']=gm("Delivery date");
		$array_values['delivery_del_date']=gm("delivery delay date");
		$array_values['seller_reg_number']=gm("Company Registration Number");
		$array_values['buyer_reg_number']=gm("Buyer Registration Number");

		asort($array_values);

		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		$data['detail']=$detail;
		if($data['detail_id']!='0'){
			$data['selected_detail']='[!'.$in['detail_id'].'!]';
		}
		return json_out($data, $showin,$exit);
	}

	$result = array(
		'codelabels'		=> get_codelabels($in,true,false),
		'selectlabels'		=> get_selectlabels($in,true,false),
		'selectdetails'		=> get_selectdetails($in,true,false),
	);


json_out($result);
?>