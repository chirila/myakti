<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$o=array('recipients'=>array(),'lines'=>array(),'order_articles_id' =>array());

if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
	$o['view_location']=true;
}else{
	$o['view_location']=false;
}

$data = false;
if($in['p_order_id']&&!$in['delivery_id']){
	$o['p_order_id'] =$in['p_order_id'];
	$o['is_add'] = true;
	$o['page_title'] = gm('Add Delivery');

	$i = 0;
	$j = 0;
	$m = 0;
	$o['lines'] 		= array();
	$line = $db->query("SELECT * FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND delivered='0' AND content='0' AND tax_id='0' AND has_variants='0' ORDER BY sort_order ASC ");
	while ($line->next()) {
		$delivered = 0;
		$left = 0;
		$delivered = $db->field("SELECT SUM(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");
		$use_serial_no = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
		if($use_serial_no==1){
			$j++;
		}

		$use_batch_no = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
		if($use_batch_no==1){
			$m++;
		}

		$left = $line->f('quantity') - $delivered;

		
		$linie=array(
			'code'					=> $line->f('article_code'),
		    'article_id'				=> $line->f('article_id'),
			'article'				=> $line->f('article'),
			'quantity'				=> display_number($left),
			'p_price'				=> display_number($line->f('price')),
			'redistributed_cost'				=> display_number($line->f('redistributed_cost')),
			'order_articles_id' 		=> $line->f('order_articles_id'),
			'use_serial_no_art'		=> $use_serial_no == 1 ? true : false,
			'use_batch_no_art'		=> $use_batch_no == 1 ? true : false,
			'batchOk' 				=> false,
		);

	    
	    $article_ids.= $line->f('article_id').',';
		array_push($o['order_articles_id'] , $line->f('order_articles_id'));
		array_push($o['lines'],$linie);
		$i++;
	}
	if($i>0){
		$data = true;
        $article_ids=rtrim($article_ids,',');

		//$view_list->assign('location_dd',build_dispach_location_dd($in['select_main_location'],$article_ids,1));
		$o['location_dd']=build_dispach_location_dd($in['select_main_location'],$article_ids,1);
		$o['select_main_location'] = $in['select_main_location'] ? $in['select_main_location'] : $o['location_dd'][0]['id'];
	}

	

	
		$o['use_serial_no'] = $j > 0 && ALLOW_STOCK ? true : false;			
		$o['use_batch_no'] 	= $m > 0 && ALLOW_STOCK ? true : false;
		//$o['hour_dd']       = hourmin('hour','minute','pm',$in['hour'],$in['minute'],$in['pm']),		
	

}elseif($in['delivery_id']){
	$i = 0;
	$j = 0;
	$m = 0;
	$o['delivery_id']=$in['delivery_id'];
	$o['p_order_id']=$in['p_order_id'];
	$o['page_title'] = gm('Edit Delivery');
		$o['is_add']       	=  '';

	$dates = $db->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' AND delivery_id='".$in['delivery_id']."' ");
	while($dates->next()){
		//$in['date_del_line_h'] = $dates->gf('date');
		$o['delivery_date_js'] 				= $dates->gf('date')*1000;
		$o['date_del_line']    = date(ACCOUNT_DATE_FORMAT,$in['date_del_line_h']);
		$o['hour'] = $dates->gf('hour');
		$o['minute'] = $dates->gf('minute');
		$o['pm'] = $dates->gf('pm');
		$o['select_main_location']= $dates->gf('location');
	}	
	
	
	$o['lines'] 		= array();
	$line = $db->query("SELECT pim_p_order_articles.*, pim_p_orders_delivery.* FROM pim_p_order_articles 
					INNER JOIN pim_p_orders_delivery ON pim_p_orders_delivery.order_articles_id=pim_p_order_articles.order_articles_id 
					WHERE pim_p_orders_delivery.p_order_id='".$in['p_order_id']."' AND pim_p_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");	
	while ($line->next()) {		
		$delivered = 0;
		$left = 0;
		$delivered = $db->field("SELECT SUM(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");
		$use_serial_no = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
		/*if($use_serial_no==1){
			$j++;
			$k=0;
			$l=0;
			$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE p_delivery_id = '".$in['delivery_id']."' AND article_id = '".$line->f('article_id')."' ORDER BY status_details_2 ASC, id DESC ");
			while($serial_numbers->next()){
				$view_list->assign(array(
					'order_articles_id'		=> $line->f('order_articles_id'),
					'serial_number'			=> $serial_numbers->f('serial_number'),
					'used_s_n'				=> trim($serial_numbers->f('status_details_2')) ? 1 : '',
					'serial_nr_id'			=> $serial_numbers->f('id'),
				),'serial_number_line');
				$k++;
				if(trim($serial_numbers->f('status_details_2'))){
					$l++;
				}
				$view_list->loop('serial_number_line','order_line');
			}

		} */
		$selected_b_n = array();
		$use_batch_no = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
		if($use_batch_no==1){
			$m++;
			$n=0;
			$o=0;
			$batch_numbers = $db->query("SELECT * FROM batches WHERE p_delivery_id = '".$in['delivery_id']."' AND article_id = '".$line->f('article_id')."' ORDER BY status_details_2 ASC, id DESC ");
			while($batch_numbers->next()){
				$selected_b_n[]=array(
					'order_articles_id'		=> $batch_numbers->f('order_articles_id'),
					'batch_number_v'		=> $batch_numbers->f('batch_number'),
					'date_h' 				=> $batch_numbers->f('date_exp')<10000? '': $batch_numbers->f('date_exp')*1000,
					'batch_number_q' 		=> display_number($batch_numbers->f('quantity')),
					'batch_nr_id'			=> $batch_numbers->f('batch_id'),
				);
				$n++;
				if(trim($batch_numbers->f('status_details_2'))){
					$o++;
				}				
			}
		}

		$left = $line->f('quantity');
		
		//$view_list->assign(array(
		
		$linie=array(
			'code'					=> $line->f('article_code'),
			'article'				=> $line->f('article'),
			'quantity'				=> display_number($left),
			'p_price'				=> display_number($line->f('price')),
			'redistributed_cost'				=> display_number($line->f('redistributed_cost')),
			'order_articles_id' 		=> $line->f('order_articles_id'),
			'use_serial_no_art'		=> $use_serial_no == 1 ? true : false,	
			'use_batch_no_art'		=> $use_batch_no == 1 ? true : false,	
			'is_edit_delivery'		=> true,
			'serial_numbers_count'		=> $k,	
			'batch_numbers_count'		=> $n,
			'green_b'				=> 'green_b',
			'green_b_batch'			=> 'green_b',
			'count_blocked_s_n'		=> $l,
			'count_blocked_b_n'		=> $o,
			'selected_b_n'			=> $selected_b_n,
			);

		array_push($o['lines'],$linie);
		
		//),'order_line');

		//$view_list->loop('order_line');
		$o['delivery_note']=$line->f('delivery_note');
		$i++;
	}
	if($i>0){
		$data = true;


		//$view_list->assign('location_dd',build_dispach_location_dd($in['select_main_location'],1,1));
        $o['location_dd'] =  build_dispach_location_dd($in['select_main_location'],1,1);
	}
	//$view_list->assign(array(
		 $o['use_serial_no'] 	= $j > 0 && ALLOW_STOCK ? true : false;	
		 $o['use_batch_no'] 	= $m > 0 && ALLOW_STOCK ? true : false;			
	//));

	if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
	       $o['view_location']=true;
	       //$view_list->assign('view_location',true);
        }else{
           $o['view_location']=false;
          // $view_list->assign('view_location',false);
        }
}
$add_cost=$db->field("SELECT add_cost FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");

	$o['add_cost']			= display_number($add_cost);




	$o['do_me']			= $in['delivery_id'] ? 'po_order--po_order-edit_entry' :'po_order--po_order-make_entry';
	$o['style']     	= ACCOUNT_NUMBER_FORMAT;
	$o['is_data']		= $data;
	$o['pick_date_format']         = pick_date_format(ACCOUNT_DATE_FORMAT);
	$o['auto_aac'] 			    = AUTO_AAC ? true:false;


json_out($o);
?>