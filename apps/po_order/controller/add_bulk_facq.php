<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$db2=new sqldb();
/*if(!$in['order_id'])
{
	exit();
}*/
$o['lang_id'] = $_SESSION['lang_id'];

$o['lines']=array();
$i=0;
$art_out=0;

if($in['quote_id'] ){ //we have to check if there are more suppliers
    $articles= $db->query("SELECT article_id from tblquote_line WHERE id='".$in['quote_id']."'");
    $suppliers=array();
    while ($articles->move_next()) {
    	$supplier_id=$db2->field("SELECT supplier_id FROM pim_articles WHERE article_id='".$articles->f('article_id')."'");
        if($supplier_id && !in_array($supplier_id,$suppliers)) {
		   array_push($suppliers,$supplier_id);
        }
        if(!$articles->f('article_id')){ //if we have out of catalog articles
        	$art_out++;
        }
    }

}else{
    exit();
 }

 $diffSuppliers = (count($suppliers) === count(array_unique($suppliers)));

 //($diffSuppliers && ($in['order_id'] || $in['service_id']) && count($suppliers)>=1)

    if(($diffSuppliers && $in['quote_id'] ) || $art_out){
  
             $o['diff_supplier']=true;
           if( $in['quote_id']){
           		$o['allow_stock']= defined('ALLOW_STOCK') && ALLOW_STOCK==1 ? true : false;
			  	$articles=$db->query("SELECT  pim_articles.supplier_id,pim_articles.supplier_name,pim_articles.supplier_reference,pim_articles.article_id, pim_articles.internal_name, pim_articles.internal_name as article_name,pim_articles.item_code,tblquote_line.sale_unit,tblquote_line.package as packing,sum(tblquote_line.quantity) as qua,pim_articles.stock,pim_articles.hide_stock
                                      FROM pim_articles
                                      INNER JOIN tblquote_line ON tblquote_line.article_id=pim_articles.article_id AND tblquote_line.quote_id='".$in['quote_id']."' AND tblquote_line.facq='1'
                                
                                      GROUP BY pim_articles.article_id ORDER BY tblquote_line.id
			  		                 ");

			  	while($articles->move_next()){
			  		//$in['supplier_id']=$articles->f('supplier_id');
			  		$ant_stock=0;
			  		 if($o['allow_stock']){
			  		 	//items on purchase
						    $items_order=$db->field("SELECT SUM(pim_p_order_articles.quantity) 
							                      FROM  pim_p_order_articles 
							                      INNER JOIN pim_p_orders ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
							                      WHERE pim_p_order_articles.article_id='".$articles->f('article_id')."'
							                      AND pim_p_orders.rdy_invoice!=0");

						    $items_received=$db->field("SELECT SUM(pim_p_orders_delivery.quantity) 
							                         FROM   pim_p_orders_delivery 
							                         INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
							                         WHERE pim_p_order_articles.article_id='".$articles->f('article_id')."'");
						//items on orders	
						     $items_on_order=$db->field("SELECT SUM(pim_order_articles.quantity) 
							                      FROM  pim_order_articles 
							                      INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
							                      WHERE pim_order_articles.article_id='".$articles->f('article_id')."'
							                      AND pim_orders.sent=1");

						    $items_on_received=$db->field("SELECT SUM(pim_orders_delivery.quantity) 
							                         FROM   pim_orders_delivery 
							                         INNER JOIN pim_order_articles ON pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
							                         WHERE pim_order_articles.article_id='".$articles->f('article_id')."'");

						//items on project
						     $items_on_project=$db->field("SELECT SUM(project_articles.quantity) 
							                      FROM  project_articles 
							                      INNER JOIN  projects ON  projects.project_id=project_articles.project_id
							                      WHERE project_articles.article_id='".$articles->f('article_id')."'
							                      AND projects.stage!=0 AND project_articles.delivered=0");

						//items on intervetions
						     $items_on_intervetion=$db->field("SELECT SUM( servicing_support_articles.quantity) 
							                      FROM   servicing_support_articles 
							                      INNER JOIN  servicing_support ON  servicing_support.service_id=servicing_support_articles.service_id
							                      WHERE servicing_support_articles.article_id='".$articles->f('article_id')."'
							                      AND servicing_support.status!=0 AND servicing_support_articles.article_delivered=0");

						$ant_stock=$articles->f('stock')-($items_on_order-$items_on_received+$items_on_project+$items_on_intervetion ) + ($items_order-$items_received);
			  		 }
			  		 $in["term"]=$articles->f('supplier_name');
			  		 $linie=array(
				        'article_code'	=> $articles->f("item_code"),
				        'article_name'	=> $articles->f("article_name"),
				        'internal_name'	=> $articles->f("internal_name"),
				        'article_id'	=> $articles->f("article_id"),
				        // 'quantity'      => display_number($articles->f("qua")*($articles->f("packing")/$articles->f("sale_unit")) ),
	                    'quantity'      => defined('ALLOW_ARTICLE_PACKING') && ALLOW_ARTICLE_PACKING=='1' ? display_number($articles->f("qua")) : display_number($articles->f("qua")*($articles->f("packing")/$articles->f("sale_unit")) ),
	                    'packing'                    => remove_zero_decimals($articles->f('packing')),
	                    'sale_unit'                    => $articles->f('sale_unit'),
				        'supplier_dd'	        => get_cc($in),
				        'supplier_id'	        => $articles->f('supplier_id'),
				        'supplier_reference'	=> $articles->f('supplier_reference'),
				        'add_p_order'    		=> $articles->f('supplier_id')?true:false,
				        'is_article'			=>true,
				        'stock'					=> $articles->f('hide_stock')==1? '' : remove_zero_decimals($articles->f('stock')),
						'ant_stock'				=> $articles->f('hide_stock')==1? '' : '('.remove_zero_decimals($ant_stock).')',
				
                       ) ;
			  		  //$view->loop('article_row');
			  		   array_push($o['lines'], $linie);

			  		   //get taxes that are applied to purchase orders
			  		   	$taxes=$db->query("SELECT pim_articles_taxes.*, pim_article_tax.*, pim_article_tax_type.name FROM pim_articles_taxes 
			  						LEFT JOIN pim_article_tax ON pim_articles_taxes.tax_id = pim_article_tax.tax_id
			  						LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
									WHERE pim_articles_taxes.article_id ='".$articles->f("article_id")."' AND (pim_article_tax.apply_to ='0' OR pim_article_tax.apply_to ='2') ");
					  	$j=0;
					  	while($taxes->move_next()){
					  		$in["term"]='';
					  		 $linie=array(
						        'article_code'	=> $taxes->f("name"),
						        'article_name'	=> $taxes->f("code"),
						        'internal_name'	=> $taxes->f("code"),
						        'article_id'	=> $articles->f("article_id"),
						        'quantity'      => display_number($articles->f("qua")*($articles->f("packing")/$articles->f("sale_unit")) ),
						        'supplier_dd'	        => get_cc($in),
						        'supplier_id'	        => $articles->f('supplier_id'),
				        		//'supplier_reference'	=> $articles->f('supplier_reference'),
				        		'add_p_order'     		=> $articles->f('supplier_id')?true:false,
						        'is_article'			=>false,
						        'tax_id'				=> $taxes->f("tax_id"),
		                       );
					  		 
					  		   array_push($o['lines'], $linie);
					  		
					  	}
			  		
			  	}

			  	$supplier_id = $db->field("SELECT customer_id FROM  customers  WHERE   facq='1' ");

			  	//articles out of the catalog
			  	$articles_out=$db->query("SELECT * FROM  `tblquote_line` 
											WHERE  `quote_id` ='".$in['quote_id']."' AND article_id =  '0' AND line_type =  '2'  AND facq='1' ");
			  	$j=0;
			  	while($articles_out->move_next()){
			  		$in["term"]='facq';
			  		 $linie=array(
				        'article_code'				=> $articles_out->f("article_code"),
				        'article_name'				=> strip_tags(html_entity_decode($articles_out->f("name"))),
				        'internal_name'				=> strip_tags(html_entity_decode($articles_out->f("name"))),
				        'article_id'				=> $articles_out->f("article_id"),
				        'quantity'      			=> display_number($articles_out->f("quantity")),
				        'supplier_dd'	        	=> get_cc($in),
				        'add_p_order'     			=> true,
				        'order_articles_id'			=> $articles_out->f("id"),
				        'is_article'				=>true,
				        'supplier_id'	        	=> $supplier_id,
				        'supplier_reference'		=> $articles_out->f("article_code"),
                       );
			  		 
			  		   array_push($o['lines'], $linie);
			  		
			  	}

			  	$articles_out=$db->query("SELECT * FROM  `tblquote_line` 
											WHERE  `quote_id` ='".$in['quote_id']."' AND article_id =  '0' AND  line_type =  '3' AND facq='1' ");
			  	$j=0;
			  	while($articles_out->move_next()){
			  		$in["term"]='facq';
			  		 $linie=array(
				        'article_code'				=> $articles_out->f("article_code"),
				        'article_name'				=> strip_tags(html_entity_decode($articles_out->f("name"))),
				        'internal_name'				=> strip_tags(html_entity_decode($articles_out->f("name"))),
				        'article_id'				=> $articles_out->f("article_id"),
				        'quantity'      			=> display_number($articles_out->f("quantity")),
				        'supplier_dd'	        	=> get_cc($in),
				        'add_p_order'     			=> true,
				        'order_articles_id'			=> $articles_out->f("id"),
				        'is_article'				=> true,
				        'is_text_line'				=> true,
				        'supplier_id'	        	=> $supplier_id,
				        'supplier_reference'		=> $articles_out->f("article_code"),
                       );
			  		 
			  		   array_push($o['lines'], $linie);
			  		
			  	}

		

		}

        


    } else{
$o['diff_supplier']=false;
       
    }

$o['do_next']="po_order-po_orders-po_order-add_po_order_multiple";

$o['quote_id']=$in['quote_id'];


json_out($o);

function get_cc($in)
		{
			
$db = new sqldb();


global $database_config;
$database = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
$dbu_users =  new sqldb($database);

			$q = strtolower($in["term"]);
					
			$filter =" is_admin='0' AND customers.active=1 AND customers.is_supplier=1";
			// $filter_contact = ' 1=1 ';
			//console::log($in['supplier_id_linie']);
			if($in['supplier_id_linie']){
				$filter .=" AND customer_id='".$in['supplier_id_linie']."'";
			}

			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
				// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
			}

			//$admin_licence = $dbu_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			$admin_licence = $dbu_users->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
				FROM customers
				WHERE $filter			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array();
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);

				$result[]=array(
					"id"					=> $value['customer_id'],
					"name"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					'zip'					=> $value['zip_name'] ? $value['zip_name'] : '',
					'city'					=> $value['city_name'] ? $value['city_name'] : '',
					"bottom"				=> $value['zip_name'].' '.$value['city'].' '.$value['country_name'],
					"right"					=> $value['acc_manager_name']
				);
			}
			if($q){
				//array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				//array_push($result,array('id'=>'99999999999','value'=>''));
			}
			
			return $result;
		}

?>