<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db2 = new sqldb();
$db3 = new sqldb();

global $database_config;
$database = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
$dbu_users =  new sqldb($database);

if(!$in['p_order_id'])
{
	page_redirect('index.php?do=order-po_orders');
}

$order = $db->field("SELECT p_order_id FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");

if(!$order){
	msg::error('Purchase order does not exist','error');
	$in['item_exists']= false;
    json_out($in);
}

$page_title= gm("Purchase Order Information");

$is_inv_nr=false;

$db->query("SELECT pim_p_orders.*,pim_p_order_deliveries.delivery_id AS del_id FROM pim_p_orders
			LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
			LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
			WHERE pim_p_orders.p_order_id='".$in['p_order_id']."' GROUP BY pim_p_orders.p_order_id");
if(!$db->move_next()){
	page_redirect('index.php?do=order-po_orders');
}
$add_cost=$db->f('add_cost');

$in['lid'] = $db->f('email_language');

if(!$in['lid']){
	$in['lid'] = 1;
}

$link_b_text = 'contact_id';
$link_c_text = 'contact_name';
$customer_id=0;
$is_contacts = false;
if($db->f('field') == 'customer_id'){
	$customer_id = $db->f('customer_id');
	$link_b_text = 'buyer_id';
	$link_c_text = 'customer_name';
}else{
	$is_contacts = true;
	$contact_id = $db->f('contact_id');
}
$in['apply_discount'] = $db->f('apply_discount');

$link_end = '&type='.ACCOUNT_P_ORDER_PDF_FORMAT;

if(defined('USE_CUSTOME_PO_PDF') && USE_CUSTOME_PO_PDF == 1 && $db->f('pdf_layout') == 0){
	$link_end = '&custom_type='.ACCOUNT_ORDER_PDF_FORMAT;
}


$rate = return_value($db->f('currency_rate'));

$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
$po_d_factur_date = $db->f('date');
$factur_del_date = $db->f('del_date') ? date(ACCOUNT_DATE_FORMAT,  $db->f('del_date')) : '';
$currency = get_commission_type_list($db->f('currency_type'));
$currency_type = $db->f('currency_type');
if (!empty($ref = $db->f('buyer_ref'))) {
    $serial_number = $ref.$db->f('serial_number');

} else {
    $serial_number = $db->f('serial_number');
}
$customer_name = $db->f('customer_name');
$is_ref = false;
if($db->f('buyer_ref')){
	$is_ref = true;
}

$ref = '';
if(ACCOUNT_ORDER_REF && $db->f('buyer_ref')){
	$ref = $db->f('buyer_ref');
}else{
	$is_ref = false;
}

 //ark::run('order--order-external_id');

$selectedDraft = "";
$selectedWon = '';
$selectedSent = '';
if($db->f('invoiced')){
	if($db->f('rdy_invoice') == 1){
		$selectedWon = 'selected';
	}else{
		$selectedSent = 'selected';
	}
}else{
	if($db->f('rdy_invoice') == 2){
		$selectedSent = 'selected';
	}elseif($db->f('rdy_invoice') == 1){
		$selectedWon = 'selected';
	}elseif($db->f('sent') == 1){
		$selectedSent = 'selected';
	}else{
		$selectedDraft = "selected";
	}
}

$default_lang = $db->f('email_language');
if(!$default_lang){
	$default_lang = 1;
}




$delivery_status = $db->f('rdy_invoice');
$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'p_order'
								   AND item_name = 'notes'
								   And active = '1'
								   AND item_id = '".$in['p_order_id']."' ");

  $buyer_adr_second=$db3->query("SELECT * FROM customer_addresses WHERE customer_id = '".$db->f('second_customer_id')."' and address_id='".$db->f('second_address_id')."'");
  $buyer_second_name = $db3->field("SELECT name FROM customers WHERE customer_id = '".$db->f('second_customer_id')."'");

    $in['second_address_info']=$buyer_adr_second->f('address').'
'.$buyer_adr_second->f('zip').'  '.$buyer_adr_second->f('city').'
'.get_country_name($buyer_adr_second->f('country_id'));

$main_adddress_info=nl2br(trim($db->f('address_info')));
//do no change these - akti - 3441 (fix in the document itself)
/*$main_address_data=$db3->query("SELECT * FROM customer_addresses WHERE address_id = '".$db->f('main_address_id')."'");
while($main_address_data->next()){
	$main_adddress_info=nl2br($main_address_data->f('address').'
'.$main_address_data->f('zip').'  '.$main_address_data->f('city').'
'.get_country_name($main_address_data->f('country_id')));
}*/
//end

$post_active=$db3->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
$service_id = $db->f('service_id');
if($service_id){
	$service_sn=$db3->field("SELECT serial_number FROM servicing_support WHERE service_id='".$service_id."' ");
}

$facq_activated= $db3->field("SELECT active FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ")? true:false;
$is_supplier_facq= $db3->field("SELECT facq FROM customers WHERE customer_id='".$db->f('customer_id')."' ") =='1'? true:false;
$edit_confirmed_po_order = $db3->field("SELECT value FROM settings WHERE constant_name='EDIT_CONFIRMED_PO_ORDERS'");

$o = array(
	'item_exists'										=> true,
	'buyer_second_name'									=> $buyer_second_name,
	'second_address_info'								=> nl2br(trim($in['second_address_info'])),
	'default_lang'                                      =>$default_lang,
	'languages'					                        => $db->f('email_language'),
	'p_order_type'					                    => $db->f('p_order_type'),
	'selectedDraft'									    => $selectedDraft,
	'selectedWon'										=> $selectedWon,
	'selectedSent'									    => $selectedSent,
	'is_revert'						                    => $db->f('rdy_invoice') > 0  ? false : true,
	'link_c_text'										=> $link_c_text,
	'link_b_text'										=> $link_b_text,
	'factur_nr' 										=> $db->f('serial_number') ? $ref.$db->f('serial_number'): '',
	'ref' 												=> $ref,
	'is_ref'											=> $is_ref,
	'factur_date' 									    => $factur_date,
	'po_d_factur_date'									=> $po_d_factur_date * 1000,
	'factur_del_date' 									=> $factur_del_date,
	'del_date'                    						=> $db->f('del_date') ? $db->f('del_date')*1000 : '',
	'due_date'											=> date(ACCOUNT_DATE_FORMAT,$db->f('due_date')),
	'delete_link'   								=> 'index.php?do=order-po_orders-order-delete_po_order&p_order_id='.$db->f('p_order_id'),
	'duplicate_link'    				    => 'index.php?do=order-norder&duplicate_order_id='.$db->f('order_id'),
	'order_buyer_name'              => $db->f('field') == 'customer_id' ? $db->f('customer_name') : $db->f('customer_name')." > " ,
	'order_contact_name'            => $db->f('contact_name'),
	'field_customer_id'             => $db->f('field')=='customer_id'?true:false,
	'order_buyer_country'   				=> get_country_name($db->f('customer_country_id')),
	'order_buyer_state'     				=> $db->f('customer_state'),
	'order_buyer_city'      				=> $db->f('customer_city'),
	'order_buyer_zip'       				=> $db->f('customer_zip'),
	'order_buyer_address'   				=> $db->f('customer_address'),
	'buyer_email'                   => $db->f('customer_email'),
	'buyer_fax'                     => $db->f('customer_fax'),
	'buyer_phone'                   => $db->f('customer_phone'),
	'order_seller_name'             => utf8_decode($db->f('seller_name')),
	'order_seller_d_address'        => utf8_decode($db->f('seller_d_address')),
	'seller_d_city'                 => utf8_decode($db->f('seller_d_city')),
	'seller_d_zip'                  => $db->f('seller_d_zip'),
	'seller_d_country'              => get_country_name( $db->f('seller_d_country_id')),
	'order_vat_no'                  => $db->f('seller_bwt_nr'),
	'serial_number'			 						=> $db->f('serial_number'),
  'status'												=> $db->f('invoiced') ? ( $db->f('rdy_invoice') == 1 ? gm('Invoiced') : gm('Partially received')) : ( $db->f('rdy_invoice') == 2 ? gm('Partially received') : ($db->f('rdy_invoice') == 1 ? gm("Received") : gm('Draft') ) ),
	'img'														=> $db->f('rdy_invoice') ? ($db->f('rdy_invoice')==1 ? 'green' : 'orange') : 'gray',
	'is_editable'										=> $db->f('rdy_invoice') > 0 ? false : true,
    'edit_confirmed_po_order'		=> $edit_confirmed_po_order ? true : false,
	//'rdy_invoice_not_1'					=> $db->f('rdy_invoice') != 1 ? true : false,

  'notes'	 	  		                       => $notes,
	'buyer_id'											=> $db->f('customer_id'),
	'p_order_id'										=> $in['p_order_id'],
	'your_ref'       							=> $db->f('your_ref'),
	'our_ref' 				  			    	=> $db->f('our_ref'),
	'add_cost' 				  			    	=>  place_currency(display_number($db->f('add_cost'))),
	'is_delivered'								=> $delivery_status > 0 ? true : false ,
	'address_info'								=> $db->f('address_info'),
	'address_info1'								=> $main_adddress_info,
	'language_dd'	      					    => build_pdf_language_dd($in['lid'],1),
	'not_adveo'											=> $db->f('is_adveo') == 1 ? false : true ,
	'sent_adveo'                          => $db->f('sent_adveo') == 1 ? true : false ,
	'sent_adveo_date'                     => date('d/m/Y',$db->f('sent_adveo_date')-3600),
	'sent_adveo_time'                     => date('H:i',$db->f('sent_adveo_date')-3600),
	'post_order'					=> $db->f('postgreen_id')!='' ? true : false,
	'post_active'					=> !$post_active || $post_active==0 ? false : true,
	'segment'						=> $db->f('segment_id') ? $db3->field("SELECT name FROM tblquote_segment WHERE id='".$db->f('segment_id')."' ") : '',
	'segment_id'					=> $db->f('segment_id'),
	'source'						=> $db->f('source_id') ? $db3->field("SELECT name FROM tblquote_source WHERE id='".$db->f('source_id')."' ") : '',
	'source_id'						=> $db->f('source_id'),
	'xtype'							=> $db->f('type_id') ? $db3->field("SELECT name FROM tblquote_type WHERE id='".$db->f('type_id')."' ") : '',
	'type_id'						=> $db->f('type_id'),
	'cost_centre'					=> $db->f('cost_centre'),
	'source_dd'						=> get_categorisation_source(),
	'type_dd'						=> get_categorisation_type(),
	'segment_dd'					=> get_categorisation_segment(),
	'service_id'					=> $service_id,
	'service_sn'					=> $service_sn,
	'allow_article_packing'         => $db->f('use_package') ? true : false,
	'allow_article_sale_unit'       => $db->f('use_sale_unit') ? true : false,
	'is_active'						=> $db->f('active')!='0' ? true : false,
	'is_archived'					=> !$db->f('active') ? true : false,
	'facq_activated' 				=> $facq_activated && $is_supplier_facq ? true :false,
	'discount_percent'      		=> '( '.$db->f('discount').'% )',
	'discount'      				=> $db->f('discount'),
	'apply_discount'		=> $in['apply_discount'],
	'sh_discount'			=> $in['apply_discount'] == 0 || $in['apply_discount'] == 2 ? false : true,
	'sh_discount1'			=> $in['apply_discount'] < 2 ? false : true
);

$nr_status= $db->f('rdy_invoice') ? ($db->f('rdy_invoice')==1 ? 'green' : 'orange') : 'gray';
if($db->f('sent')==0 ){
    $type_title = gm('Draft');
}
if(($db->f('sent') == 1) && $db->f('rdy_invoice') == 0) {
    $nr_status = 'orange';
}
if($db->f('invoiced')){
    if($orders->f('rdy_invoice') == 1){
        $type_title = gm('Received');
    }else{
        $type_title = gm('Final');
    }
}else{
    if($db->f('rdy_invoice') == 2){
        if($db->f('sent')=='1' && $db->f('del_id') !=0){
    		$type_title = gm('Partially Delivered');
    	}else{
    		$type_title = gm('Final');
    	}
    }else{
        if($db->f('rdy_invoice') == 1){
            $type_title = gm('Received');
        }else{
            if($db->f('sent') == 1){
                $type_title = gm('Final');
            }else{
                $type_title = gm('Draft');
            }
        }
    }
}
if(!$db->f('active')){
	$type_title = gm('Archived');
	$nr_status='gray';
}
$o['type_title']=$type_title;
$o['nr_status']=$nr_status;

if($db->f('contact_id')){
			$contact = $db3->query(" SELECT CONCAT_WS(' ',firstname, lastname) as contact_name, cell, email FROM customer_contacts
																WHERE contact_id='".$db->f('contact_id')."' ");
			$o['contact_name'] 		= $contact->f('contact_name');
			$o['contact_id'] 		= $db->f('contact_id');
			if($db->f('customer_id')){
				$contact = $db3->query(" SELECT phone as cell, email FROM customer_contactsIds
										WHERE contact_id='".$db->f('contact_id')."' AND customer_id= '".$db->f('customer_id')."'");
			}	
			$o['contact_phone'] 		= $contact->f('cell');
			$o['contact_email'] 		= $contact->f('email');
		}

if($db->f('postgreen_id')!=''){  	
    	include_once('apps/po_order/model/po_order.php');
		$postg=new po_order(); 
		$post_status=json_decode($postg->postOrderData($in));
    	$o['postg_status']		= $post_status->status;
    }
    $post_library=array();
    if($post_active){
    	$in['ret_res']=true;
    	$post_library=include_once('apps/misc/controller/post_library.php');
    }
    $o['post_library']=$post_library;

if(!$in['s_date']){
	$in['s_date'] = time();
	$o['s_date'] = time();
}
$o['sent_date']								= is_numeric($in['s_date']) ? $in['s_date']*1000 : strtotime($in['s_date'])*1000;

$view->assign("view_send","style='display:none;'");
$o['view_send']="style='display:none;'";

if($notes == ''){
	$o['hide_notes']="hide";

}
$is_p_delivery_planned=false;
$show_confirm_reception=false;
$show_add_new_reception=true;
if(P_ORDER_DELIVERY_STEPS==2){
	$count_del = $db2->field("SELECT COUNT(delivery_id) FROM pim_p_order_deliveries WHERE p_order_id = '".$in['p_order_id']."' AND delivery_done = '0' ");
	$count_done_del = $db2->field("SELECT COUNT(delivery_id) FROM pim_p_order_deliveries WHERE p_order_id = '".$in['p_order_id']."' AND delivery_done = '1' ");
	// $o['delivery_steps_2'] = array(
		$o['status']= $db->f('invoiced') ? ( $db->f('rdy_invoice') == 1 ? gm('Invoiced') : gm('Partially delivered')) : ( ($db->f('rdy_invoice') == 2 && $count_done_del>0) ? gm('Partially delivered') : ( ($db->f('rdy_invoice') == 1 && $count_del==0) ? gm("Delivered")  :  ( ($db->f('sent') == 1 && $count_done_del==0) ? gm('Ready To Deliver') : ( $count_del>0 ? gm('Partially delivered') : gm('Draft') ) ) ) );
		$o['img']= $db->f('rdy_invoice') ? ( ($db->f('rdy_invoice')==1 && $count_del==0) ? 'green' : 'orange') : ($db->f('sent') == 1 ? 'orange' : 'gray');
		$o['disable_option']= ($db->f('rdy_invoice') && $is_del_not_invoiced==true) > 0 ? ($db->f('invoiced') ? ($is_del_not_invoiced==true ? false : true) : ($count_done_del > 0 ? false:true ) ) : true;
		$o['total_orderer']= ($db->f('rdy_invoice') == 1 && $total_order_incomplete == true) ? ($count_done_del > 0 ? '' :'&total_order=1') : '';
		//$o['selectedSent']= $db->f('invoiced') ? ( $db->f('rdy_invoice') == 1 ? '' : 'selected') : ( ($db->f('rdy_invoice') == 2 && $count_done_del>0) ? 'selected' : ( ($db->f('rdy_invoice') == 1 && $count_del==0) ? ''  :  ( ($db->f('sent') == 1 && $count_done_del==0) ? 'selected' : ( $count_del>0 ? 'selected' : '' ) ) ) );
		//$o['selectedWon']= $db->f('invoiced') ? ( $db->f('rdy_invoice') == 1 ? 'selected' : '') : ( ($db->f('rdy_invoice') == 2 && $count_done_del>0) ? '' : ( ($db->f('rdy_invoice') == 1 && $count_del==0) ? 'selected'  :  ( ($db->f('sent') == 1 && $count_done_del==0) ? '' : ( $count_del>0 ? '' : '' ) ) ) );
		$o['hide_this_or']= $count_done_del == 0 && $db->f('rdy_invoice') == 1 ? 'hide' : '';
		$o['are_delivery']= $db->f('rdy_invoice') == 2 ? true : ( $count_del > 0 ? true : false);

		if($db->f('rdy_invoice') == 2){
			$dels_p=$db2->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id = '".$in['p_order_id']."' ");
			while($dels_p->next()){
				if(!$dels_p->f('delivery_done')){
					$is_p_delivery_planned=true;
				}
			}
		}

	// );

		if($count_del){
			$show_confirm_reception=true;
		}

		$total_q_to_deliver=$db2->field("SELECT COALESCE(SUM(quantity),0) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' AND tax_id='0' AND has_variants='0'");
		$total_q_delivered=$db2->field("SELECT COALESCE(SUM(quantity),0) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND delivery_id IN (SELECT delivery_id FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' AND delivery_done = '1') ");
        $total_q_left_to_deliver = $db2->field("SELECT COALESCE(SUM(quantity),0) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND delivery_id IN (SELECT delivery_id FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' AND delivery_done = '0') ");

		if(($total_q_delivered>0 || $total_q_left_to_deliver>0) && ($total_q_delivered + $total_q_left_to_deliver == $total_q_to_deliver)){
			$show_add_new_reception=false;
		}

}
if($is_p_delivery_planned){
	$o['type_title']=gm('Delivery planned');
}
$o['del_two_steps']					= P_ORDER_DELIVERY_STEPS == 2 ? 1 : 0;
$o['show_confirm_reception']=$show_confirm_reception;
$o['show_add_new_reception']=$show_add_new_reception;

$discount_percent= $db->f('discount');
$global_disc = $db->f('discount');

$i=0;
$discount_total = 0;
$show_discount_total = false;
$fully_delivered = true;
$can_edit_del=true;
$find_if_delivered=$db->query("SELECT pim_p_order_articles.* FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' ORDER BY sort_order ASC");
while ($find_if_delivered->next()) {
	if(!$find_if_delivered->f('delivered')){
		$fully_delivered = false;
	}
	if($find_if_delivered->f('article_id')){
		$serial=$db->field("SELECT id FROM serial_numbers WHERE article_id='".$find_if_delivered->f('article_id')."'");
		$batch=$db->field("SELECT id FROM batches WHERE article_id='".$find_if_delivered->f('article_id')."'");
		if($serial || $batch){
			$can_edit_del=false;
		}
	}
}

	$db->query("SELECT pim_p_order_articles.* FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order ASC");
	
$o['lines']=array();
	while ($db->move_next())
	{

		$received_article_q = $db2->field("SELECT COALESCE(SUM(quantity),0) FROM pim_p_orders_delivery WHERE p_order_id = '".$in['p_order_id']."' AND order_articles_id = '".$db->f('order_articles_id')."' ");

		$sale_unit= $db->f('sale_unit');
		if(!ALLOW_ARTICLE_SALE_UNIT){
				$sale_unit=1;
			}
		$packing= $db->f('packing');
		if(!ALLOW_ARTICLE_PACKING){
				$packing=1;
				$sale_unit=1;
		}
		if(!$show_discount_total && $db->f('discount') !=0){
			$show_discount_total = true;
		}
		$discount_line = $db->f('discount');
		if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
			$discount_line = 0;
		}
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
		$q=$db->f('quantity');
		$price_line = ($db->f('price')-($db->f('price') * $discount_line /100))*$packing/$sale_unit;
		$line_total=$price_line * $q;
		$discount_total += $price_line * $global_disc / 100 * $q;

		$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));	 	

		//$total_with_vat += $db->f('price_with_vat')*$db->f('quantity')*$packing/$sale_unit;
		$total_with_vat += $line_total;
		$line = array(
			'article'  			    	=> $db->f('content') ? htmlspecialchars_decode($db->f('article')) : nl2br($db->f('article')),
			'quantity'      			=> display_number($db->f('quantity'), $nr_dec_q),
            'price'         			=> display_number_var_dec($db->f('price')),
			'price_vat'      			=> display_number($db->f('quantity')*$price_line ),   #changed to amount
			'colspan'					=> $db->f('content') ? ' colspan=5 ' : '',
			'content'					=> $db->f('content') ? false : true,
			'content_class'				=> $db->f('content') ? ' last_nocenter ' : '',
			'article_code'				=> $db->f('article_code'),
			'is_article_code'			=> $db->f('article_code') ? true : false,
			'supplier_reference'		=> $db->f('supplier_reference'),
			'is_supplier_reference'		=> $db->f('supplier_reference') ? true : false,
			'sale_unit'				    => $db->f('sale_unit'),
			'packing'				    => remove_zero_decimals($db->f('packing')),
			'has_variants' => $db->f('has_variants'),
		 	'has_variants_done' => $db->f('has_variants_done'),
		 	'is_variant_for' => $db->f('is_variant_for'),
		 	'variant_type' => $db->f('variant_type'), 
		 	'disc'						=> display_number($db->f('discount')),
		 	'to_receive'				=> display_number($db->f('quantity')-$received_article_q),
            'order_articles_id'			=> $db->f('order_articles_id')
        );
       array_push($o['lines'], $line);

		//$view->loop('order_row');
		$i++;
	}

$total_with_vat=$total_with_vat-$discount_total+$add_cost;

$total_currency = $total_with_vat * $rate;

$o['totalul'] = array(
	  
 	 	'discount_total'    	=> place_currency(display_number($discount_total),$currency),
	  	//'total_vat'			    => place_currency(display_number($total_with_vat)),
	  	'total_vat'			    => place_currency(display_number($total_with_vat),get_commission_type_list($currency_type)),
	  	'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
		'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
		'currency_type'			=> $currency,#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		'currency_d_type'		=> get_commission_type_list(ACCOUNT_CURRENCY_TYPE),#ACCOUNT_CURRENCY_TYPE == 1 ? 
		'total_default_c'		=> display_number($total_currency),
		'default_currency_name' => build_currency_name_list(ACCOUNT_CURRENCY_TYPE)
		
	);

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

if(!$in['include_pdf'])
{
	$in['include_pdf'] = 1;
}
if(defined('USE_PO_ORDER_WEB_LINK') && USE_PO_ORDER_WEB_LINK == 1){

	//$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['p_order_id']."' AND `type`='p' ");
	$exist_url = $db_users->field("SELECT url_code FROM urls WHERE `database`= :d AND `item_id`= :item_id AND `type`= :type ",['d'=>DATABASE_NAME,'item_id'=>$in['p_order_id'],'type'=>'p']);
	if(defined('WEB_INCLUDE_PDF_PO') && WEB_INCLUDE_PDF_PO == 0){
		$in['include_pdf'] = 0;
	}
}

global $cfg;
$message_data=get_sys_message('pordmess',$in['lid']);
$subject=$message_data['subject'];

$external_web_link = '<a href="'.$cfg['web_link_url'].'?q='.$exist_url.'">'.$cfg['web_link_url'].'?q='.$exist_url.'</a>';

$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
$subject=str_replace('[!SERIAL_NUMBER!]',$serial_number, $subject);
$subject=str_replace('[!PURCHASE_ORDER_DATE!]',$factur_date, $subject);
$subject=str_replace('[!CUSTOMER!]',$customer_name, $subject);

$subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($total_with_vat),$currency), $subject);

$body=$message_data['text'];
$account_company = ACCOUNT_COMPANY;
$account_company = htmlentities($account_company,ENT_COMPAT | ENT_HTML401,'UTF-8');
$body=str_replace('[!ACCOUNT_COMPANY!]',$account_company, $body );
$body=str_replace('[!SERIAL_NUMBER!]',$serial_number, $body);
$body=str_replace('[!PURCHASE_ORDER_DATE!]',$factur_date, $body);
$body=str_replace('[!CUSTOMER!]',$customer_name, $body);

$body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($total_with_vat),$currency), $body);
if($message_data['use_html']){
   $body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);
}else{
	 $body=str_replace('[!SIGNATURE!]','', $body);
}
//$account_manager_email = $db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
$account_manager_email = $db_users->field("SELECT email FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

    $o['include_pdf']  				 = $in['include_pdf'] ? true : false;
	$o['copy_checked']     = $in['copy'] ? 'checked="checked"' : '';
	$o['subject']              = $in['subject'] ? $in['subject'] : $subject;
	$o['e_message']            = $in['e_message'] ? $in['e_message'] : $body;
	$o['language_dd']	       = build_pdf_language_dd($in['lid'],1);
	$o['user_loged_email']		= $account_manager_email;
	$o['use_html']			   = $message_data['use_html'];


/*$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_p_orders.p_order_id,pim_p_orders.customer_id,pim_p_orders.serial_number
             FROM pim_p_orders
             INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_p_orders.customer_id
             WHERE pim_p_orders.p_order_id='".$in['p_order_id']."' AND customer_contacts.active ='1'");*/
	$tblinvoice_customer_contacts = $db->query("SELECT customers.*
		   FROM customers
             WHERE customers.customer_id='".$customer_id."' ");
	

$j = 0;


if($customer_id){
	// $autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php?customer_id='.$customer_id;
	$customer_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$customer_id."' ");
}
$jj=1;
/*$o['recipients']=array();
while ($tblinvoice_customer_contacts->move_next()){
	$in['recipients'][$tblinvoice_customer_contacts->f('customer_id')]=1;

	$recipient = array(
		'recipient_name'  			=> $tblinvoice_customer_contacts->f('name'),
		'recipient_email' 			=> $tblinvoice_customer_contacts->f('c_email'),
		'recipient_id'    			=> $tblinvoice_customer_contacts->f('customer_id'),
		'recipients_checked'    	=> $in['recipients'][$tblinvoice_customer_contacts->f('customer_id')] ? 'checked="checked"' : '',

		'contact_email_data' 			=> $tblinvoice_customer_contacts->f('email'),
		'contact_id_data'				=> $tblinvoice_customer_contacts->f('customer_id'),
		
		
	);

	$j++;
	$jj++;
	array_push($o['recipients'], $recipient);
}*/
$o['recipients_ids'] = array();
$c_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$customer_id."' ");
$invoice_email_type = $db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$customer_id."' ");

		if($customer_id){
			$filter .= " AND customers.customer_id='".$customer_id."'";
		}
		if($o['contact_id']){
			$filter .= " AND customer_contacts.contact_id='".$o['contact_id']."'";
		}

		$items = array();
		$items2 = array();
		$contact_email='';
		$customer_exist = false;
		if($customer_id && $o['contact_id']){
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$customer_id."' ORDER BY lastname ")->getAll();
		}else{
			/*$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.customer_id = customer_contactsIds.customer_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1  AND customers.customer_id='".$customer_id."'  ORDER BY lastname ")->getAll();*/	
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$customer_id."'  ORDER BY lastname ")->getAll();
		}
		if($customer_id && !$o['contact_id']){
			$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE 1=1 $filter ");
			$customer_exist = true;


		}else{
			$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ");
		}
		$items2 = array(
			    'id' => $customer->f('email'), 
			    'label' => $customer->f('name'),
			    'value' => $customer->f('name').' - '.$customer->f('email'),
		);
		if($invoice_email_type==1 && $customer_id){
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$customer_id."'  ORDER BY lastname ")->getAll();
		}
		$contacts_exist = false;
		foreach ($contacts as $key => $value) {

			if($value['name']){
				$name = $value['firstname'].' '.$value['lastname'].' > '.$value['name'];
			}else{
				$name = $value['firstname'].' '.$value['lastname'];
			}
			$items[] = array(
			    'id' => $value['email'], 
			    'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			    'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)).' - '.$value['email'],
			);
			$contacts_exist = true;
			if($value['contact_id']==$o['contact_id']){
				$contact_email=$value['email'];
			}
		}

		/*if($invoice_email_type==1){
			if(!$o['contact_id']){
				//array_push($o['recipients_ids'], $items2['id']);
			}else{
				array_push($o['recipients_ids'], $contact_email);
			}
		}else{
			if($c_email){
				$o['recipients_ids'] = array();
				array_push($o['recipients_ids'],$c_email);
			}
		}*/
		
		if($o['contact_id']){
			if($customer_id){
				$contact_email = $db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$o['contact_id']."' AND customer_id = '".$customer_id."'");
			}else{
				$contact_email = $db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$o['contact_id']."'");
			}
			if($contact_email){
				$cc_email = $contact_email;
			}else{
				$customer_email = $db->field("SELECT c_email FROM customers WHERE customer_id='".$customer_id."'");
				if($customer_email){
					$cc_email = $customer_email;
				}
			}
		}

		if($cc_email){
			$o['recipients_ids'] = $cc_email;
		}

		$added = false;
		foreach ($items as $key => $value) {
			if($value['id'] == $items2['id']){
				$added = true;
			}
		}
		if(!$added){
			array_push($items, $items2);
		}
		if($invoice_email_type==1 && $customer_id){
			array_push($items, $items2);
		}/*else{	
			if($c_email){
				array_push($items, array(
					'id' => $c_email, 
					'label' =>$c_email,
					'value' => $c_email,
				));
			}
		}*/
	//$o['contact_email_data'] 			= $invoice_email;
/*	$o['recipients'] = array_map(function($field){
										return $field['id'];
									},$items);*/

$o['recipients'] =$items;
$o['save_disabled'] 			= sizeof($o['recipients_ids'])? false: true;

$o['recipient_default'] = array(
			    'id' =>$o['contact_email'], 
			    'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$o['contact_name']),
			    'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($o['contact_name'])),
			);

/*$view->assign(array(
	'autocomplete_file'			=> $autocomplete_path,
));*/
if($j == 0){
	$o["hide_send_to"]="";
}
else{
	$o["hide_send_to"]="hide";
}

//sent box
$o["view_sent"]="style='display:none;'";




	$total_default = $total_with_vat*$rate;
	


  	    $o['total_vat']			    = display_number($total_with_vat);


  	    $o['hide_currency2']		= ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide';
		$o['hide_currency1']		= ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide';
		$o['currency_type']			= $currency;#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		$o['currency_d_type']		= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		$o['total_default']			= display_number($total_default);
	

$l=0;

	$o['pdf_del_type'] = $o['p_order_type']?$o['p_order_type']:ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
	  $o['generate_pdf_id']  	= '';
	  $o['hide_language']	   	= 'hide';
	  $o['pdf_link']          	= 'index.php?do=po_order-p_order_print&p_order_id='.$in['p_order_id'].'&lid='.$in['lid'].$link_end;
	

$l=0;





$deliveries = $db->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' ORDER BY delivery_id DESC ");
$del_nr = $deliveries->records_count();
$j=0;
$o['deliveries']=array();
while ($deliveries->next()) {
$j++;
	$delivery = array(
		'delivey'					=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('date')),
		// 'delivery_link'				=> 'index.php?do=order-p_order_print&p_order_id='.$in['p_order_id'].'&lid='.$default_lang.'&type='.ACCOUNT_ORDER_ENTRY_PDF_FORMAT.'&delivery_id='.$deliveries->f('delivery_id'),
		'delivery_link'				=> 'index.php?do=order-p_order_print&p_order_id='.$in['p_order_id'].'&lid='.$default_lang.'&type='.ACCOUNT_ORDER_DELIVERY_PDF_FORMAT.'&delivery_id='.$deliveries->f('delivery_id'),
		'del_delivery_link'			=> $deliveries->f('delivery_done')==0?'index.php?do=po_order-po_order-po_order-delete_entry&p_order_id='.$in['p_order_id'].'&del_nr='.$del_nr.'&delivery_id='.$deliveries->f('delivery_id').'&delivery_approved=1':'index.php?do=po_order-po_order-po_order-delete_entry&p_order_id='.$in['p_order_id'].'&del_nr='.$del_nr.'&delivery_id='.$deliveries->f('delivery_id'),
		'id'						=> $deliveries->f('delivery_id'),
		'active'					=> $j==1 && $delivery_status==1 ? 'active' : 'delivered',
		'del_title'					=> $delivery_status==1 ? gm('Received') : gm('Partially received'),
		'delete_delivery_title'		=> gm('Delete entry'),
		'delivery_id'				=> $deliveries->f('delivery_id'),
		'del_by_two_steps'			=> P_ORDER_DELIVERY_STEPS == 2 ? true : false,
		'not_approved'				=> $deliveries->f('delivery_done')==0 ? true : false,
	);

	if(P_ORDER_DELIVERY_STEPS==2){
		$delivery['active']	= ($j == 1 && $delivery_status == 1 &&  $count_del == 0) ? 'active' : ( $deliveries->f('delivery_done') == 1 ? 'delivered' : 'not_delivered');
	}	
	

	array_push($o['deliveries'], $delivery);
}
if($j && $delivery_status!=1){
	
          $o['are_delivery']=true ;
          $o['no_delivery']= false;
		
}else{
	      $o['are_delivery']=false ;
          $o['no_delivery']= true;
}
if($j){
	$o['no_delivery']= false;
}else{
	$o['no_delivery']= true;
}


$j = 0;
$is_returned = false;
$returns = $db->query("SELECT * FROM pim_p_orders_return WHERE p_order_id='".$in['p_order_id']."' ORDER BY return_id DESC");
$ret_nr = $returns->records_count();
$o['returns']=array();
while ($returns->next()) {
	$j++;

	$return = array(
		'date_ret'					=> date(ACCOUNT_DATE_FORMAT,$returns->f('date')),
		'del_return_link'			=> 'index.php?do=po_order-po_order-po_order-delete_return&p_order_id='.$in['p_order_id'].'&return_id='.$returns->f('return_id'),
		'id'						=> $returns->f('return_id'),
		'delete_return_title'			=> gm('Delete return'),
		'edit_return_title'			=> gm('Edit return'),
		'return_id'				=> $returns->f('return_id')
	);
	array_push($o['returns'],$return);
	// $view->loop('returns');
}
if($j > 0){
	$is_returns = true;
}

if(!$in['s_date']){
	$in['s_date'] = time();
}


$is_extra = false;
if($customer_id){
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");

	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<h4 >'.gm('Bank Details').'</h4>
	<p><strong>'.gm('Name').': </strong> <span>'.$c_details->f('bank_name').'</span></p>
	<p><strong>'.gm('BIC Code').': </strong> <span>'.$c_details->f('bank_bic_code').'</span></p>
	<p><strong>'.gm('IBAN').': </strong> <span>'.$c_details->f('bank_iban').'</span></p>
	';
	}
	if($extra_info){
		$is_extra = true;
		$extra_info = '<div style="width: inherit;" >'.$extra_info.'</div>';
	}
	$o['customer_notes']=stripslashes($c_details->f('customer_notes'));
}

// $customer_id = $db->field("SELECT customer_id from pim_p_orders WHERE p_order_id ='".$in['p_order_id']."' ");
$customer_data = $db->query("SELECT c_email, customer_id, name FROM customers WHERE customer_id = '".$customer_id."'");

//$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php';
if($customer_id){
	$customer_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$customer_id."' ");
	//$autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php?customer_id='.$customer_id;
}
$id= $customer_id;
$is_contact = 0;
if($is_contacts){
	$is_contact = 1;
	$id = $contact_id;
}

$is_drop = defined('DROPBOX') && (DROPBOX != '') ? true : false;
$dropbox_files = array();
$dropbox_images = array();
//type 2 - img // type 1 - file
$dropbox_files_q = $db->query("SELECT * FROM dropbox_files WHERE dropbox_folder = 'porders' AND parent_id = '". $in['p_order_id'] ."'");
while ($dropbox_files_q->next()) {
    if ($dropbox_files_q->f('type') == 2) {
        $unserialized_data=unserialize(stripslashes($dropbox_files_q->f('serialized_content')));
        $url = str_replace("?dl=0","?raw=1",stripslashes($dropbox_files_q->f('link')));
        $dropbox_images_tmp = array (
            'link'				=> stripslashes($dropbox_files_q->f('link')),
            'file' 				=> $unserialized_data->path_display,
            'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$unserialized_data->path_display,'item_id'=>$in['p_order_id'],'drop_folder'=>'porders','is_batch=1','file_id'=> $dropbox_files_q->f('id'), 'certificate_id'=> $dropbox_files_q->f('certificate_id')),
            'down_file_link'	=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($unserialized_data->path_display).'&item_id='.$in['p_order_id'].'&for=' .'porders'.'&type='.$unserialized_data->mime_type.'&file='.urlencode(str_replace('/','_', $unserialized_data->path_display)).'&is_batch=1',
            'file_id'			=> $dropbox_files_q->f('id'),
            'path'				=> $unserialized_data->path_display,
            'name'				=> $unserialized_data->name,
            'url'               => $url,
        );
        array_push($dropbox_images, $dropbox_images_tmp);


    } elseif ($dropbox_files_q->f('type') == 1) {
        $unserialized_data=unserialize(stripslashes($dropbox_files_q->f('serialized_content')));
        $dropbox_files_tmp = array(
            'link'				=> stripslashes($dropbox_files_q->f('link')),
            'file' 				=> $unserialized_data->path_display,
            'delete_file_link'	=> array('do'=>'misc--misc-deleteDropboxFile','path'=>$unserialized_data->path_display,'item_id'=>$in['p_order_id'],'drop_folder'=>'porders','is_batch=1','file_id'=> $dropbox_files_q->f('id'), 'certificate_id'=> $dropbox_files_q->f('certificate_id')),
            'down_file_link'	=> 'index.php?do=misc-download_dropbox_file&path='.urlencode($unserialized_data->path_display).'&item_id='.$in['p_order_id'].'&for='.'porders'.'&type='.$unserialized_data->mime_type.'&file='.urlencode(str_replace('/','_', $unserialized_data->path_display)).'&is_batch=1',
            'file_id'			=> $dropbox_files_q->f('id'),
            'path'				=> $unserialized_data->path_display,
            'name'				=> $unserialized_data->name,
        );
        array_push($dropbox_files, $dropbox_files_tmp);
    }
}


$dropbox = array('is_drop' => $is_drop, 'dropbox_files' => $dropbox_files, 'dropbox_images' => $dropbox_images);
$drop_info = array('drop_folder' => 'porders', 'customer_id' => $id, 'item_id' => $in['p_order_id'], 'isConcact' => $is_contact, 'serial_number' => $serial_number);
//$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

	$o['ADV_STOCK']								= ADV_STOCK==1 ? true : false;
    $o['s_date']								= $in['s_date'];
    $o['extra_info']							= $extra_info;
	$o['is_extra']							= $is_extra;
	
	$o['hide_activity']			    		= $_SESSION['access_level'] == 1 ? '' : 'hide';
	$o['page_title']	    	    		= $page_title;
	$o['pick_date_format']      			= pick_date_format();
	$o['search']							= $in['search']? '&search='.$in['search'] : '';
	$o['view']								= $in['view']? '&view='.$in['view'] : '';
	$o['total_hide']						= $currency_type ? ($currency_type != ACCOUNT_CURRENCY_TYPE ? '':'hide') : 'hide';
	$o['currency_type2']					= $currency_type;
	$o['is_deliveredd']						= !$fully_delivered ? true : false;
	$o['customer_id']						=	$customer_data->f('customer_id');
	$o['c_email']							=	$customer_data->f('c_email');
	$o['is_c_email']						=	$customer_data->f('c_email') ? true : false;
	$o['is_customer']						= true;
	$o['drop_info']							= $drop_info;
	$o['dropbox']							= $dropbox;

	$o['contact_id_data']					= $customer_id;
	$o['contact_name_data']					= $customer_data->f('c_email');
	$o['contact_email_data'] 				= $customer_data->f('c_email');
	//$o['autocomplete_file']					= $autocomplete_path;
	//$o['loging']							= ark::run('order-loging');
	$o['user_email'] 						= $user_email;
	$o['use_weblink']							= defined('USE_PO_ORDER_WEB_LINK') && USE_PO_ORDER_WEB_LINK == 1 ? true : false;
	$o['web_url']								= $cfg['web_link_url'].'?q=';
	$o['auto_aac'] 			    = AUTO_AAC ? true:false;


$o['external_url'] 			= $delivery_status == '0' ? false : $exist_url;
$o['can_edit_del']=$can_edit_del;

$o['can_edit_del']=$can_edit_del;

$o['can_edit_del']=$can_edit_del;

	$order_nr_row = array();

	$ord_nr = $db->query("SELECT tracking_line.*, tracking.*, pim_orders.serial_number, pim_orders.sent, pim_orders.invoiced, pim_orders.rdy_invoice,pim_orders.active FROM tracking
							INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
							INNER JOIN pim_orders ON tracking_line.origin_id = pim_orders.order_id
							WHERE tracking.target_id = '".$in['p_order_id']."' AND tracking.target_type ='6' AND tracking_line.origin_type ='4' ");

	while($ord_nr->move_next()){
		$status ='';
			if($ord_nr->f('sent')==0 ){
				$status = gm('Draft');
			}else if($ord_nr->f('invoiced')){
				if($ord_nr->f('rdy_invoice') == 1){
					$status = gm('Delivered');
				}else{
					$status = gm('Confirmed');
				}
			}else{
				if($ord_nr->f('rdy_invoice') == 2){
					$status = gm('Confirmed');
				}else{
					if($ord_nr->f('rdy_invoice') == 1){
						$status = gm('Delivered');
					}else{
						if($ord_nr->f('sent') == 1){
							$status = gm('Confirmed');
						}else{
							$status = gm('Draft');
						}
					}
				}
			}

			$is_delivery_planned=false;
			if(ORDER_DELIVERY_STEPS == '2'){
		   		$is_partial_deliver = false;
		   		if($ord_nr->f('rdy_invoice')==2){
		   			$dels=$db->query("SELECT * FROM pim_order_deliveries WHERE order_id='".$ord_nr->f('origin_id')."'");
			   		while($dels->next()){
			   			//daca cel putin una din livrarile partiale sunt confirmate, atunci is_partial_deliver = true
			   			if($dels->f('delivery_done')=='1'){
			   				$is_partial_deliver = true;
			   				$is_delivery_planned=false;
			   				break;
			   			}else{
			   				$is_delivery_planned=true;
			   			}
			   		}
		   		}
		   		

		    }else{
		    	 $is_partial_deliver= $ord_nr->f('rdy_invoice')==2?true:false;
		    }
			
			if($is_partial_deliver && !$is_delivery_planned && $ord_nr->f('sent') == 1){
				$status = gm('Partially Delivered');
			}else if($is_delivery_planned && $ord_nr->f('sent') == 1){
				$status = gm('Delivery planned');
			}

	    	array_push($order_nr_row, array( 'serial' => (!$ord_nr->f('active') ? '' : $ord_nr->f('serial_number')), 'id'=> $ord_nr->f('origin_id'), 'status'=> (!$ord_nr->f('active') ? gm('Archived') : $status)));	    
		}
	$o['orders']=$order_nr_row;

	$invoice_nr_row = array();
	$inv_nr = $db->query("SELECT tracking.*, tracking_line.*, tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.f_archived FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id = tracking.trace_id
						INNER JOIN tblinvoice ON tracking.target_id = tblinvoice.id
						WHERE tracking_line.origin_id = '".$in['p_order_id']."' AND tracking_line.origin_type ='6' AND (tracking.target_type ='1' OR tracking.target_type ='7') ");

	while($inv_nr->move_next()){
		$type_title='';
	    switch ($inv_nr->f('type')){
		case '0':
		    $type_title = gm('Regular invoice');
		    break;
		case '1':
		    $type_title = gm('Proforma invoice');
		    break;
		case '2':
		    $type_title = gm('Credit Invoice');
		    break;
	    }	    

	    if($inv_nr->f('sent') == '0' && $inv_nr->f('type')!='2'){
			$type_title = gm('Draft');
	    }else if($inv_nr->f('status')=='1'){
			$type_title = gm('Paid');
	    }else if($inv_nr->f('sent')!='0' && $inv_nr->f('not_paid')=='0'){
			$type_title = gm('Final');
	    } else if($inv_nr->f('sent')!='0' && $inv_nr->f('not_paid')=='1'){
			$type_title = gm('No Payment');
	    }

    	array_push($invoice_nr_row, array( 'serial' => ($inv_nr->f('f_archived') ? '' : $inv_nr->f('serial_number')), 'id'=> $inv_nr->f('target_id'), 'status'=> ($inv_nr->f('f_archived') ? gm('Archived') : $type_title)));
    
	}
	$o['invoices']=$invoice_nr_row;

	$quote_nr_row = array();
$quote_nr = $db->query("SELECT tracking_line.*, tracking.*, tblquote.serial_number, tblquote.sent, tblquote.status_customer, tblquote.f_archived FROM tracking
							INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
							INNER JOIN tblquote ON tracking_line.origin_id = tblquote.id
							WHERE tracking.target_id = '".$in['p_order_id']."' AND tracking.target_type ='6' AND tracking_line.origin_type ='2' ");

$quote_opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
while($quote_nr->move_next()){
	$status=$quote_opts[$quote_nr->f('status_customer')];
	if($quote_nr->f('sent') == 1 && $quote_nr->f('status_customer') == 0){
		$status=$quote_opts[5];
	}
    array_push($quote_nr_row, array( 'serial' => ($quote_nr->f('f_archived') ? '' : $quote_nr->f('serial_number')), 'id'=> $quote_nr->f('origin_id'), 'status'=> ($quote_nr->f('f_archived') ? gm('Archived') : $status)));   
}

$o['quotes']=$quote_nr_row;

$p_invoice_nr_row = array();
$p_inv_nr = $db->query("SELECT * FROM tblinvoice_incomming
						WHERE FIND_IN_SET('".$in['p_order_id']."',c_order_id) ");

while ($p_inv_nr->next()) {
	switch($p_inv_nr->f('status')){
		case '1':
			$type_title=$p_inv_nr->f('paid') ? ($p_inv_nr->f('paid')==2 ? gm('Partially Paid') : gm('Paid')) : gm('Approved');
			break;
		default:
			$type_title=gm('Uploaded');
			break;
	}
	if($p_inv_nr->f('paid') == '0' && $p_inv_nr->f('status') == '1' && $p_inv_nr->f('due_date') < time()){
		$type_title=gm('Late');
	}
	array_push($p_invoice_nr_row, array( 'serial' => ($p_inv_nr->f('f_archived') ? '' : $p_inv_nr->f('booking_number')), 'id'=> $p_inv_nr->f('invoice_id'), 'status'=> ($p_inv_nr->f('f_archived') ? gm('Archived') : $type_title)));
}
$o['p_invoices']=$p_invoice_nr_row;

	$mail_setting_option = $db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
	$o['sendgrid_selected'] = false;
	if($mail_setting_option==3){
		$o['sendgrid_selected'] =true;
	}

json_out($o);
?>