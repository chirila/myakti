<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['reset_list']){
 	if(isset($_SESSION['tmp_add_to_pdf_po'])){
 		unset($_SESSION['tmp_add_to_pdf_po']);
 	}
 	if(isset($_SESSION['add_to_pdf_po'])){
 		unset($_SESSION['add_to_pdf_po']);
	}
}

$db2=new sqldb();
global $config;
$l_r =ROW_PER_PAGE;

if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);


if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}


//$filter = 'WHERE 1=1 ';
$filter_po = 'WHERE 1=1 ';
$filter_c='';
$filter_article='';
if($in['created_from']){
	$filter_c = ', servicing_support.serial_number AS service_sn';
	$filter_po = ' INNER JOIN servicing_support ON servicing_support.service_id = pim_p_orders.service_id WHERE 1=1 ';
	$filter_po.=" and (servicing_support.serial_number like '%".$in['created_from']."%' )";
	$arguments.="&created_from=".$in['created_from'];
}

$filter_link = 'block';
$order_by = " ORDER BY t_date DESC, iii DESC ";

if(!$in['archived']){
	$filter_po.= " AND pim_p_orders.active=1";
}else{
	$filter_po.= " AND pim_p_orders.active=0";
	$arguments.="&archived=".$in['archived'];
	$arguments_o.="&archived=".$in['archived'];
}

if($in['filter']){
	$arguments.="&filter=".$in['filter'];
}
if($in['search']){
	$filter_po.=" and (pim_p_orders.serial_number like '%".$in['search']."%' OR pim_p_orders.our_ref like '%".$in['search']."%' OR pim_p_orders.customer_name like '%".$in['search']."%' OR pim_p_orders.your_ref like '%".$in['search']."%')";
	$arguments.="&search=".$in['search'];
}

if($in['article_name_search']){
	$filter_article.=" INNER JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
                       INNER JOIN pim_articles ON pim_p_order_articles.article_id=pim_articles.article_id AND (pim_articles.internal_name like '%".$in['article_name_search']."%' OR pim_p_order_articles.article_code like '%".$in['article_name_search']."%')";
	$arguments.="&article_name_search=".$in['article_name_search'];
}
if($in['order_by']){
	$order = " ASC ";
	if($in['desc']=='1' || $in['desc']=='true'){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	
}

if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
if($in['start_date'] && $in['stop_date']){
	$filter_po.=" and pim_p_orders.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if($in['start_date']){
	$filter_po.=" and cast(pim_p_orders.date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if($in['stop_date']){
	$filter_po.=" and cast(pim_p_orders.date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}

$dynamic_join="";
$dynamic_cols="";
$tmp_having_filter="";
$having_filter="";
if($in['associated_customer_search']){
    $dynamic_join .= " LEFT JOIN pim_orders ON pim_p_orders.order_id = pim_orders.order_id";
    $filter_po .= " AND pim_orders.customer_name LIKE '%" .$in['associated_customer_search']. "%'";
}
switch ($in['view']) {
		case '1':
			$filter_po.=" and pim_p_orders.rdy_invoice = '0' and pim_p_orders.sent='0' ";
			$arguments.="&view=1";
			break;
	  	case '2':
	  		if($in['archived']){
	  			$filter_po.=" and pim_p_orders.sent='0' and pim_p_orders.rdy_invoice != '0' and pim_p_orders.rdy_invoice != '1'";
	  		} else {
	  			$filter_po.=" and pim_p_orders.sent='1' and pim_p_orders.rdy_invoice != '1' ";
	  		}
			$arguments.="&view=2";
			break;		
		case '3':
			$filter_po.=" and pim_p_orders.rdy_invoice = '1' ";
			$arguments.="&view=3";
			break;
		case '4':
			$filter_po.=" and pim_p_orders.rdy_invoice = '2' and pim_p_orders.sent='1' and pim_p_order_deliveries.delivery_id !=0";
			$arguments.="&view=4";
			break;
		case '5':
			$dynamic_join.=" LEFT JOIN pim_p_order_deliveries AS p_order_deliveries_not_done ON p_order_deliveries_not_done.p_order_id=pim_p_orders.p_order_id AND p_order_deliveries_not_done.delivery_done='0' ";
			$dynamic_cols.=",COALESCE(COUNT(p_order_deliveries_not_done.delivery_id),0) AS total_deliveries_not_done";
			$filter.=" and pim_p_orders.rdy_invoice = '2' ";
			$having_filter=" HAVING total_deliveries_not_done>0 ";
			break;
		default:
			break;
	}

switch($in['invoice_status_id']){
    //Not invoiced.
    case '1':
        $dynamic_join.=" LEFT JOIN tblinvoice_incomming ON tblinvoice_incomming.c_order_id = pim_p_orders.p_order_id LEFT JOIN pim_p_orders_delivery ON pim_p_orders_delivery.p_order_id=pim_p_orders.order_id ";
        $dynamic_cols.=",tblinvoice_incomming.c_order_id";
        $filter_po.=" AND pim_p_orders.p_order_id NOT IN (SELECT c_order_id FROM tblinvoice_incomming)";
        break;
    //Partially invoiced
    case '2':
        $dynamic_join.="";
        $dynamic_cols.="";
        $filter_po.=" AND pim_p_orders.order_full = '2'";
        break;
//    Invoiced
    case '3':
        $dynamic_join.="";
        $dynamic_cols.="";
        $filter_po.=" AND pim_p_orders.order_full = '1' ";
        break;
}
if($tmp_having_filter){
    $having_filter=" HAVING ".ltrim($tmp_having_filter,"AND");
}

if($in['your_reference_search']){
	$filter_po.=" AND pim_p_orders.your_ref LIKE '%".addslashes($in['your_reference_search'])."%' ";
}

$nav= array();
$arguments = $arguments.$arguments_s;

$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
$max_rows_data = $db->query("SELECT  pim_p_orders.serial_number as iii, cast( FROM_UNIXTIME( pim_p_orders.date ) AS DATE ) AS t_date,pim_p_orders.p_order_id".$dynamic_cols."
			FROM pim_p_orders
			LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
			".$dynamic_join."
			".$filter_article."
			 ".$filter_po." 
			 GROUP BY pim_p_orders.p_order_id
			 ".$having_filter." ".$order_by);
$max_rows=$max_rows_data->records_count();

if(!$_SESSION['tmp_add_to_pdf_po'] || ($_SESSION['tmp_add_to_pdf_po'] && empty($_SESSION['tmp_add_to_pdf_po']))){
	while($max_rows_data->next()){	
		$_SESSION['tmp_add_to_pdf_po'][$max_rows_data->f('p_order_id')]=1;
		array_push($nav, (object)['p_order_id'=> $max_rows_data->f('p_order_id') ]);
	}

}else{
	while($max_rows_data->next()){	
		array_push($nav, (object)['p_order_id'=> $max_rows_data->f('p_order_id') ]);
	}
}

$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_to_pdf_po']){
 	if($max_rows>0 && count($_SESSION['add_to_pdf_po']) == $max_rows){
 		$all_pages_selected=true;
 	}else if(count($_SESSION['add_to_pdf_po'])){
 		$minimum_selected=true;
 	}
}

$db->query("SELECT pim_p_orders.*, pim_p_orders.serial_number as iii, cast( FROM_UNIXTIME( pim_p_orders.date ) AS DATE ) AS t_date,ROUND(cast(pim_p_orders.amount AS decimal(12, 4))+0.0012,2) AS formatted_amount ".$filter_c.", pim_p_order_deliveries.delivery_id AS del_id".$dynamic_cols."
			FROM pim_p_orders 
			LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
			".$dynamic_join."
			".$filter_article." ".$filter_po." GROUP BY p_order_id ".$having_filter.$order_by." LIMIT ".$offset*$l_r.",".$l_r);


$j=0;
$color = '';
global $config;
$result = array('query'=>array(),'max_rows'=>$max_rows,'all_pages_selected'=> $in['exported'] ? false : $all_pages_selected,'minimum_selected'=> $in['exported'] ? false :$minimum_selected, 'nav' => $nav);

while($db->move_next()){
	$ref = '';
	if(ACCOUNT_ORDER_REF && $db->f('buyer_ref')){
		$ref = $db->f('buyer_ref');

	}	
	if($db->f('sent')==0 ){
		$status = gm('Draft');
	}
	if($db->f('invoiced')){
		if($db->f('rdy_invoice') == 1){
			$status = gm('Received');
		}else{
			$status = gm('Final');
		}
	}else{
		if($db->f('rdy_invoice') == 2){
			if($db->f('sent')=='1' && $db->f('del_id') !=0){
				$status = gm('Partially Delivered');
			}else{
				$status = gm('Final');
			}
		}else{
			if($db->f('rdy_invoice') == 1){
				$status = gm('Received');
			}else{
				if($db->f('sent') == 1){
					$status = gm('Final');
				}else{
					$status = gm('Draft');
				}
			}
		}
	}

	if(!$db->f('active')){
		$status = gm('Archived');
	}

	$is_p_delivery_planned=false;
	if(P_ORDER_DELIVERY_STEPS==2){
		if($db->f('rdy_invoice') == 2){
			$dels_p=$db2->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id = '".$db->f('p_order_id')."' ");
			while($dels_p->next()){
				if(!$dels_p->f('delivery_done')){
					$is_p_delivery_planned=true;
				}
			}
		}
	}
	if($is_p_delivery_planned){
		$status=gm('Delivery planned');
	}

	$p_inv_nr = $db2->field("SELECT invoice_id FROM tblinvoice_incomming
						WHERE FIND_IN_SET('".$db->f('p_order_id')."',c_order_id) ");

	$associated_customer="";

	$ord_nr = $db2->query("SELECT pim_orders.customer_name FROM tracking
		INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
		INNER JOIN pim_orders ON tracking_line.origin_id = pim_orders.order_id
		WHERE tracking.target_id = '".$db->f('p_order_id')."' AND tracking.target_type ='6' AND tracking_line.origin_type ='4' ")->getAll();
	
	if(!empty($ord_nr)){
		if(count($ord_nr)>1){
			$associated_customer=stripslashes($ord_nr[0]['customer_name'])." ...";
		}else{
			$associated_customer=stripslashes($ord_nr[0]['customer_name']);
		}
	}
    if($db->f('identity_id')) {
        $business_unit = $db2->field("SELECT identity_name FROM multiple_identity WHERE identity_id ='".$db->f('identity_id')."'");
    } else {
        $business_unit = gm('Main Identity');
    }

	$item=array(
		
		'delete_link'  		      => array('do'=>'po_order-po_orders-po_order-delete_po_order','p_order_id'=>$db->f('p_order_id')),
		'undo_link' 	 		  => array('do'=>'po_order-po_orders-po_order-activate_po_order','p_order_id'=>$db->f('p_order_id')),
		'archive_link'  		  => array('do'=>'po_order-po_orders-po_order-archive_po_order','p_order_id'=>$db->f('p_order_id')),
		
		'id'              	  	=> $db->f('p_order_id'),
		'view_delete_link2'		=> $_SESSION['access_level'] == 1 ? ($in['archived'] ? '' : 'hide') : 'hide',
		'hide_on_a'				=> $in['archived'] == 1 ? 'hide' : '',
		
		'serial_number'     	=> $db->f('serial_number') ? $ref.$db->f('serial_number') : '',
		'created'           	=> date(ACCOUNT_DATE_FORMAT,$db->f('date')),
		'delivery_date'           	=> $db->f('del_date')?date(ACCOUNT_DATE_FORMAT,$db->f('del_date')):'',
		'our_ref'        		=> $db->f('our_ref'),
		'your_ref'        		=> $db->f('your_ref'),
		'amount'				=> place_currency(display_number($db->f('formatted_amount')), get_commission_type_list($db->f('currency_type'))),
		'buyer_name'        	=> $db->f('customer_name'),
		'is_editable'			=> $db->f('rdy_invoice') ? false : true,
		'is_archived'       	=> $in['archived'] ? true : false,
		'archived'				=> $db->f('active') == 1 ? true : false,
		'path'					=> $config['site_url'],
		'status2'				=> $status,
		'confirm'				=> gm('Confirm'),
		'ok'					=> gm('Ok'),
		'cancel'				=> gm('Cancel'),
		'status'				=> $db->f('rdy_invoice') ? ($db->f('rdy_invoice')==1 ? 'green' : 'orange') : ($db->f('sent') ? 'orange' :'gray'),
		'post_order'		    => $db->f('postgreen_id')!='' ? true : false,
		//'is_partial_deliver'    => $db->f('rdy_invoice')==2 && $db->f('sent')=='1' && $db->f('del_id') !=0 ? true : false,
		'partialDEL_info'       => gm('Partially Received'),
		'check_add_to_product'	=> $in['exported'] ? false : ($_SESSION['add_to_pdf_po'][$db->f('p_order_id')] == 1 ? true : false),
		'is_invoice'			=> $p_inv_nr ? true : false,
		'invAll'								=> ($db->f('order_full')==1 ) ? 'inv_ordGreenIcons' : '',
		'invAll_info'						=> ($db->f('order_full')==1  )?gm('Invoiced') : (  $p_inv_nr ? gm('Partially Invoiced'):''),
		"associated_customer"	=> $associated_customer,
        'business_unit'         => $business_unit,
		//'alt_for_delete_icon'	=> $in['archived'] == 1 ? 'Delete' : 'Archive',
	);

	if(!$db->f('active')){
		$item['status'] = 'gray';
	}
     array_push($result['query'], $item);
	
	//$view_list->loop('order_row');
	$j++;
}

$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");

$result['lr']		= $l_r;
$result['post_active']= !$post_active || $post_active==0 ? false : true;
$result['columns']=array();
$cols_default=default_columns_dd(array('list'=>'purchase_orders'));
$cols_order_dd=default_columns_order_dd(array('list'=>'purchase_orders'));
$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='purchase_orders' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
if(!count($cols_selected)){
	$i=1;
    $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'purchase_orders'));
    foreach($cols_default as $key=>$value){
		if($key=='associated_customer'){
	    	continue;
	    }
		$tmp_item=array(
			'id'				=> $i,
			'column_name'		=> $key,
			'name'				=> $value,
			'order_by'			=> $cols_order_dd[$key]
		);
        if ($default_selected_columns_dd[$key]) {
            array_push($result['columns'],$tmp_item);
        }
		$i++;
	}
}else{
	foreach($cols_selected as $key=>$value){
		$tmp_item=array(
			'id'				=> $value['id'],
			'column_name'		=> $value['column_name'],
			'name'				=> $cols_default[$value['column_name']],
			'order_by'			=> $cols_order_dd[$value['column_name']]
		);
		array_push($result['columns'],$tmp_item);
	}
}
$result['views']=[[
        'name'	=>  gm('All Statuses'),
        'id'	=>  0,
        'active'	=>  'active'
    ], [
        'name'	=>  gm('Draft'),
        'id'	=>  1,
        'active'	=>  ''
    ], [
        'name'	=>  gm('Sent'),
        'id'	=>  2,
        'active'	=>  ''
    ], [
        'name'	=>  gm('Received'),
        'id'	=>  3,
        'active'	=>  ''
    ], [
        'name'	=>  gm('Partially Received'),
        'id'	=>  4,
        'active'	=>  ''
    ]];
if(defined('P_ORDER_DELIVERY_STEPS') && P_ORDER_DELIVERY_STEPS == '2'){
	array_push($result['views'],[
		'name' => gm('Delivery planned'),
        'id' => 5,
        'active' => ''
	]);
}
$result['invoice_statuses']=[
    [
        'name' => gm('All Invoice Statuses'),
        'id'=> 0,
        'active' => ''
    ],
    [
        'name' => gm('Not Invoiced'),
        'id' => 1,
        'active' => ''
    ],
    [
        'name' => gm('Partially Invoiced'),
        'id' => 2,
        'active' => ''
    ],
    [
        'name' => gm('Invoiced'),
        'id' => 3,
        'active' => ''
    ]];

json_out($result);