<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

global $config;

// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
$db2 = new sqldb();
$db3 = new sqldb();
$db_lines=new sqldb();
$o = array(
	'main_comp_info'=>array(
		'name'		=> ACCOUNT_COMPANY,
		'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
		'zip'		=> ACCOUNT_DELIVERY_ZIP,
		'city'		=> ACCOUNT_DELIVERY_CITY,
		'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
		'phone'		=> ACCOUNT_PHONE,
		'fax'		=> ACCOUNT_FAX,
		'email'		=> ACCOUNT_EMAIL,
		'url'		=> ACCOUNT_URL,
		'logo'		=> defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',
		)
);
$in['main_comp_info']=array(
	'name'		=> ACCOUNT_COMPANY,
	'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
	'zip'		=> ACCOUNT_DELIVERY_ZIP,
	'city'		=> ACCOUNT_DELIVERY_CITY,
	'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
	'phone'		=> ACCOUNT_PHONE,
	'fax'		=> ACCOUNT_FAX,
	'email'		=> ACCOUNT_EMAIL,
	'url'		=> ACCOUNT_URL,
	'logo'		=> defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',
	);

$o['default_currency']     	= ACCOUNT_CURRENCY_TYPE;
$o['default_currency_name']    = build_currency_name_list(ACCOUNT_CURRENCY_TYPE);

$o['search_by']=$in['search_by'];
if(!$in['search_by']){
	$o['search_by']=1;
}
// $view->assign(array(
$o['search_by_name_check']		        = $in['search_by'] == 1 ? 'CHECKED' : '';
$o['search_by_company_number_check']	= $in['search_by'] == 2 ? 'CHECKED' : '';
// ));

$o['payment_term']=$in['payment_term'];
if(!$in['payment_term']){
	$o['payment_term']=15;
}
$cancel_link = "index.php?do=po_order-po_orders";

if((!$in['p_order_id'] && !$in['duplicate_p_order_id']) || $in['p_order_id'] == 'tmp' ){
	$o['do_request'] = 'po_order-npo_order-po_order-updateCustomerData';
		if($in['order_id'] && $in['order_id'] != 'tmp'){
			if($in['is_import_active']){
				if($in['force_adveo']){
					//$import_filter=" AND from_import='1' ";
					$import_filter='';
				}else{
					//$import_filter=" AND from_import!='1' ";
					$import_filter='';
				}
			}else{
				$import_filter='';
			}
			$in['buyer_id'] = $db->field("SELECT customer_id FROM pim_orders WHERE order_id='".$in['order_id']."'");
            $in['delivery_address_id']  = $db->field("SELECT main_address_id  FROM pim_orders WHERE order_id='".$in['order_id']."'");

			$in['our_ref'] = $db->field("SELECT serial_number FROM pim_orders WHERE order_id='".$in['order_id']."'");

			$articles = $db->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['order_id']."'".$import_filter." AND content='0' ORDER BY order_articles_id ASC ");
			$disc=0;
			$j=0;
			while ($articles->next())
			{
			    $q=$db->field("SELECT  pim_order_articles.quantity FROM  pim_order_articles WHERE order_id='".$in['order_id']."' AND order_articles_id='".$articles->f('order_articles_id')."'");

				// $price = $articles->f('price') - ($articles->f('price')*$articles->f('discount')/100);

           $purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$articles->f('article_id')."' AND base_price='1'");
				if($purchase_price>0 && !USE_ARTICLE_PURCHASE_PRICE){
					$price=$purchase_price;

				}else{
					$price = $articles->f('purchase_price');
				}

				$in['article_id'][$j] = $articles->f('article_id');
				$in['article'][$j] = $articles->f('article');
				$in['quantity'][$j] = $q*($articles->f('packing')/$articles->f('sale_unit'));
				$in['sale_unit'][$j] =1;
				$in['packing'][$j] =1;
				$in['price'][$j] = $price;



				$in['order'][$j] = $in['orders_id'];
				$in['article_code'][$j] = $articles->f('article_code');
			    $in['is_article_code'][$j] = true;

			    $supplier_reference = $db->field("SELECT supplier_reference FROM pim_articles WHERE article_id='".$articles->f('article_id')."'");
			    $in['supplier_reference'][$j] = $supplier_reference;
			    $in['is_supplier_reference'][$j] = $supplier_reference? true:false;
			    

				$in['readonly'][$j] = 1;
				$in['origin_id'][$i]=$in['order_id'];
          			$in['origin_type'][$i]='4';

				$j++;
			}

			//end shipping price
	}


	if($in['quote_id'] ){
		//transforma quote into a facq po
		$in['buyer_id']= $db->field("SELECT customer_id FROM customers WHERE facq='1' ");
		$in['customer_id']= $in['buyer_id'];

		$in['delivery_date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j")+1, date("Y")));
		$in['del_date'] = (time() + (1 * 24 * 60 * 60))*1000;

		$in['our_ref']= $db->field("SELECT serial_number FROM tblquote WHERE id='".$in['quote_id']."' ");
		$o['quote_id'] = $in['quote_id'];
		
	}

	if($in['buyer_id'] == 'tmp'){
		//$in['add_customer']				= true;
		$in['add_customer']				= false;
		$in['cc']						= get_cc($in,true,false);
		$in['sc']						= get_cc($in,true,false);
		$in['country_dd']				= build_country_list();
		$in['main_country_id']			= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$in['email_language'] 			= DEFAULT_LANG_ID;
		$in['payment_type_dd']     		= build_payment_type_dd($in['payment_type']);
		$in['payment_type']				= '0';
		$in['language_dd'] 				= build_language_dd_new($in['email_language']);
		$in['currency_id']				= ACCOUNT_CURRENCY_TYPE;
		$in['identity_id'] 				= get_user_customer_identity($_SESSION['u_id']);
		$in['articles_list']			= get_articles_list($in,true,false);
		$in['contacts']					= get_contacts($in,true,false);
		$in['customers']				= get_customers($in,true,false);
		$in['addresses']				= get_addresses($in,true,false);
		$in['languages']				= $in['email_language'];
		$in['do_request']				= 'po_order-npo_order-po_order-updateCustomerData';
		$in['colum'] = 4;
		$in['apply_discount'] 			= 0;
		$in['apply_discount_line'] 		= false;
		$in['apply_discount_global'] 	= false;
		json_out($in);
	}elseif ($in['buyer_id'] && !is_numeric($in['buyer_id'])) {
		$o = array('redirect'=>'po_orders');
		json_out($o);
		
	}

	
	$in['email_language'] 		= DEFAULT_LANG_ID;
	$in['languages']			= $in['email_language'];
	$in['currency_id']			= ACCOUNT_CURRENCY_TYPE;
	
	$in['language_dd']			= build_language_dd();
	$in['gender_dd']			= gender_dd();
	$in['title_dd']				= build_contact_title_type_dd(0);
	//ADD
	$cancel_link = "index.php?do=po_order-po_orders";
   
  	
  	$do_next = 'po_order-npo_order-po_order-add_order';

  	$apply_discount = 0;

 

  	if(!$in['date_h']){
		$in['date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		$in['date_h'] = time()*1000;
	}
	$o['date'] = $in['date'];
	$o['date_h'] = $in['date_h'];


	if(!$in['del_date']){
		//$in['delivery_date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		//$in['del_date'] = time()*1000;
	}
	$o['delivery_date'] = $in['delivery_date'];
	$o['del_date'] = $in['del_date'];

	/*if (!$in['serial_number']){
		$in['serial_number'] = generate_serial_number(DATABASE_NAME);
	}*/
	$o['serial_number'] = $in['serial_number'];


	if(!ALLOW_ARTICLE_PACKING){
		$o['allow_article_packing']=0;
	}else{
		$o['allow_article_packing']=1;
	}
	if(!ALLOW_ARTICLE_SALE_UNIT){
		$o['allow_article_sale_unit']=0;
	}else{
		$o['allow_article_sale_unit']=1;
	}
	
	$o['buyer_id'] = $in['buyer_id'];
	//$o['customer_id'] = $in['buyer_id'];
	$o['customer_id'] = $in['customer_id'];

	

	$details = array('table' 		=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field'		=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'		=> $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],
					 'name'			=> $in['buyer_id'] ? stripslashes($in['s_buyer_id']) : stripslashes(substr($in['s_customer_id'], 0, strpos($in['s_customer_id'],'>'))) ,
	);

	$ref = '';
	$o['customer_ref'] = '';
	
	$customer_notes="";
	if($in['buyer_id']){
		$customer_id = $in['buyer_id'];
		//$o['do_request'] = 'po_order-npo_order-po_order-add_order';
		$o['is_company']=true;
		$buyer_info = $db->query("SELECT deliv_disp_note, cat_id, c_email, our_reference ,comp_phone,name, customers.no_vat,customers.btw_nr, customers.internal_language,
								customers.line_discount, customers.apply_fix_disc,customers.currency_id, customers.apply_line_disc, customers.identity_id,customers.customer_notes,customers.language
								FROM customers WHERE ".$details['field']."='".$details['value']."' ");
		$buyer_info->next();

		$customer_notes=stripslashes($buyer_info->f('customer_notes'));
		//$o['discount_line_gen']= display_number($buyer_info->f('line_discount'));
		if($buyer_info->f('apply_line_disc')==1){
			$o['discount_line_gen']= display_number($buyer_info->f('line_discount'));
		} else {
			$o['discount_line_gen']= display_number(0);
		}

		if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
			$apply_discount = 3;
		}else{
			if($buyer_info->f('apply_fix_disc')){
				$apply_discount = 2;
			}
			if($buyer_info->f('apply_line_disc')){
				$apply_discount = 1;
			}
		}
		if(!$in['c_quote_id']){
		   	$in['apply_discount'] = $apply_discount;
		   	$o['apply_discount'] = $apply_discount;
	    }

		if($buyer_info->f('apply_line_disc')){
			$in['apply_discount'] = 1;
			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$in['apply_discount'] = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$in['apply_discount'] = 2;
				}
			}
		}else {
			$in['apply_discount']= 0;
			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$in['apply_discount'] = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$in['apply_discount'] = 2;
				}
			}	
		}
	  	$discount=0;
	  	if($in['buyer_id']){
	  		if($buyer_info->f('apply_line_disc')){
	  			$discount_fix= $db2->field('SELECT line_discount FROM customers WHERE customer_id ='.$in['buyer_id']);
	  		//} else {
	  		}
	  		if($buyer_info->f('apply_fix_disc')){
	  			$discount= $db2->field('SELECT fixed_discount FROM customers WHERE customer_id ='.$in['buyer_id']);
	  		}
    	}
    	$in['discount']	    		= display_number($discount);
    	$in['discount_fix']			= display_number($discount_fix);


		if(!$details['name'] ){
	    	$details['name']=$buyer_info->f('name');
	    }
	    $details['cat'] = $buyer_info->f('cat_id');
		$in['remove_vat'] = $buyer_info->f('no_vat');
		$o['customer_ref']=$buyer_info->f('our_reference');
		if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
			$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
		}
		// var_dump($buyer_info->f('internal_language'));

		 $in['email_language'] 		= $buyer_info->f('language') ? $buyer_info->f('language') : $in['email_language'];
		 $in['currency_id']			= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
		 $in['identity_id']			= $in['identity_id'] ? $in['identity_id'] : get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);
		 $in['del_note']			= $in['del_note'] ? $in['del_note'] : $buyer_info->f('deliv_disp_note');
		 $in['languages']			= $in['email_language'];
		
	}

	$o['customer_notes']=$customer_notes;
	//console::log('x'.$in['currency_id']);
			
	
	if($in['contact_id']){
		$contact_title = $db->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$in['contact_id']."'");
		if($contact_title)
		{
			$contact_title .= " ";
		}

	}


	$buyer = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ORDER BY is_primary DESC");
	$buyer->next();
	if($in['buyer_id'] && $buyer->f('delivery') == 0){
		$buyer_del_address = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' AND delivery=1 limit 1 ");
		if($buyer_del_address){
				$o['delivery_address'] = $buyer_del_address->f('address').'
'.$buyer_del_address->f('zip').'  '.$buyer_del_address->f('city').'
'.get_country_name($buyer_del_address->f('country_id'));
		}
	}

	if(!$in['apply_discount']){
		$in['apply_discount']=0;
	}
	
	  	/*$discount=0;
	  	if($in['buyer_id']){
      		$discount= $db2->field('SELECT fixed_discount FROM customers WHERE customer_id ='.$in['buyer_id']);
    	}
    	$in['discount']	    		= display_number($discount);*/
  	
	$in['customer_id'] 		= $details['value'];
	$in['field'] 			= $details['field'];
	$o['field'] 			= $details['field'];
	$o['price_category_id'] = $details['cat'];
	$in['price_category_id'] = $details['cat'];

  	$o['buyer_ref']	    	= $ref;
  	//$o['currency_name']    	= build_currency_name_list($in['currency_type']);
  	$o['currency_name']    	= build_currency_name_list($in['currency_id']);
  	$o['currency_rate']    	= $in['currency_rate'];

  	
    if($in['buyer_id']){
		$in['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').'  '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));
}
	$existIdentity = $db2->field("SELECT COUNT(identity_id) FROM multiple_identity ");

  $buyer_main_addres_id=$db->field("SELECT address_id FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND is_primary=1");
  if(!$in['main_address_id']){
   		$in['main_address_id']= $buyer_main_addres_id;
   }

  $buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['main_address_id']."'");
  
  if($in['delivery_address_id']){
		$buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['delivery_address_id']."'");
	}
 

    $in['address_info']=$buyer_adr->f('address').'
'.$buyer_adr->f('zip').'  '.$buyer_adr->f('city').'
'.get_country_name($buyer_adr->f('country_id'));
   	
   	$second_buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['sc_id']."' and address_id='".$in['second_address_id']."'");
   	$second_buyer_name=$db->field("SELECT name FROM customers WHERE customer_id = '".$in['sc_id']."'");
    $in['address_info2']=$second_buyer_adr->f('address').'
	'.$second_buyer_adr->f('zip').'  '.$second_buyer_adr->f('city').'
	'.get_country_name($second_buyer_adr->f('country_id'));

    $setting_order_ref = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_REF' AND type=1");

    $o['order_ref'] = $setting_order_ref;
    $o['apply_discount']								= $in['apply_discount'];
  	$o['main_address_id'] = $in['main_address_id'];
  	$o['client_delivery'] = $in['client_delivery'] == 1 ? true : false;
	$o['our_ref'] = $in['our_ref'];
	$o['your_ref'] = $in['your_ref'];
	$o['form_mode']                    			= 0; //0-add 1-edi;
	$o['sc_id']									= $in['sc_id'];
	//$in['second_id']							= $in['sc_id'];
	$o['second_address_id']						= $in['second_address_id'];
	//$o['second_addresses']						= get_addresses($in,true,false);	
	$o['payment_type_dd']      				 	= build_payment_type_dd($in['payment_type']);
	$o['payment_type_txt']       				= build_payment_type_dd($in['payment_type'],true);
	$o['payment_type']							= $db->f('payment_type') ? $db->f('payment_type') : '0';
	$o['vat_total']	                    		= number_format(0,2);
	$o['discount_percent']      				= '( '.$in['discount'].'% )';
	$o['discount_total']						= number_format(0,2);
	$o['del_note']           					= $in['del_note'];
	
	$o['total']									= number_format(0,2);
	$o['total_vat']								= number_format(0,2);
	$o['hide_currency2']						= ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide';
	$o['hide_currency1']						= ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide';
	$o['currency_type']							= get_commission_type_list($in['currency_id']);#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$';
	$o['default_currency_val']  				= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
	$o['currency_type_val']						= $in['currency_id'];
	$o['checked']								= $in['rdy_invoice'] ? '"checked"':'';
	$o['name_txt']								= !$in['buyer_id'] ? 'Name:' : 'Company Name:';
	$o['customer_name']							= stripslashes($details['name']);

	$o['ref_serial_number']						= $ref.$in['serial_number'];
	$o['ref_var']								= $ref ? " var ref= '".$ref."';" : " var ref;";
	$o['delivery_address']      				= $in['delivery_address'];
	$o['delivery_address_txt']     				= nl2br($in['delivery_address']);
	$o['delivery_address_id']     				= $in['delivery_address_id'];
	$o['customer_id']							= $in['customer_id'];
	$o['customer_address']						= $buyer->f('address');
	$o['customer_zip']							= $buyer->f('zip');
	$o['customer_vat_number']					= $buyer->f('btw_nr');
	$o['customer_city']							= $buyer->f('city');
	$o['customer_country_id']					= $buyer->f('country_id');
	$o['customer_cell']							= !$in['buyer_id'] ? $buyer->f('cell') : '';
	$o['customer_phone']						= !$in['buyer_id'] ? $buyer->f('phone') : $buyer->f('comp_phone');
	$o['customer_email']						= !$in['buyer_id'] ? $buyer->f('email') : $buyer->f('c_email');
	$o['buyer_country']							= get_country_name($buyer->f('country_id'));
	$o['comp_name']								= $in['s_buyer_id'];
	$o['is_buyer_id']							= $in['s_buyer_id'] ? true : false;
	$o['remove_vat']							= $in['remove_vat'];
	
	$o['is_buyer']			    				= $in['buyer_id'] ? true : false;
	
	$o['address_info_txt']						= nl2br(trim($in['address_info']));
	$o['address_info_txt2']						= nl2br(trim($in['address_info2']));
	$o['second_buyer_name']						= $second_buyer_name;
	$o['address_info']							= $in['address_info'];

	$o['total_currency_hide']					= 'hide';
	$o['contact_id']			    			= $in['contact_id'];
	$o['gender_dd']								= gender_dd();
	$o['title_dd']								= build_contact_title_type_dd(0);
	$o['language_dd'] 							= build_language_dd_new($in['email_language']);
	$o['source_dd']								= get_categorisation_source();
	$o['source_id']								= $in['source_id'];
	$o['type_dd']								= get_categorisation_type();
	$o['type_id']								= $in['type_id'];
	$o['segment_dd']							= get_categorisation_segment();
	$o['segment_id']							=$in['segment_id'];
	$o['DISCOUNT']              				= display_number($in['discount']);
	$o['discount']              				= display_number($in['discount']);
	$o['discount_fix']              			= display_number($in['discount_fix']);
	$o['hide_global_discount']					= $in['apply_discount'] < 2 ? false : true;
	$o['hide_line_global_discount']				= $in['apply_discount'] == 1 ? '' : 'hide';
	$o['hide_disc']								= $in['apply_discount'] == 1 || $in['apply_discount'] == 3 ? true : false;
	$o['total_currency_hide']					= 'hide';
	$o['disc_line']								= display_number($in['discount_line_gen']);

	$o['multiple_identity_txt'] 				= $db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$in['identity_id']."'");
	$o['multiple_identity_dd'] 					= build_identity_dd($in['identity_id']);
	$o['identity_id'] 							= $in['identity_id'] ? $in['identity_id'] : '0';
	$o['existIdentity']							= $existIdentity >0 ? true : false;
	$o['colum'] 							= $in['apply_discount'] == 1 || $in['apply_discount'] == 3 ? 3 : 4;
	$o['apply_discount_line']=$in['apply_discount'] == 0 || $in['apply_discount'] == 2 ? false : true;
	$o['apply_discount_global']=$in['apply_discount'] < 2 ? false : true;

	if($o['identity_id'] != '0'){
		$mm = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$o['identity_id']."'")->getAll();
		$value = $mm[0];
		$template = $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$o['identity_id']."' and module='order'");
		
		$o['main_comp_info']=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> $value['company_logo'],
				);
		
	}

	$i=0;
	$total = 0;
	$total_with_vat = 0;
	$discount_total = 0;
	$vat_total = 0;
	$global_disc = $in['discount'];
	$sub_discount = array();
	$o['tr_id']=array();
	
	// console::log($in['article']);
	/*if($in['article']){
		foreach ($in['article'] as $row){
			$row_id = 'tmp'.$i;	
			$q = return_value($row['quantity']);
			$price_line = return_value($row['price']);
			$line_total=$price_line * $q;
			$total += $line_total;
			$linie = $row;
			array_push($o['tr_id'], $linie);
			//array_push($o['tr_id'], $linie2);
			$i++;
		}
	}*/
	if($in['article'] && (!$in['order_id'] || !$in['quote_id'])){
		foreach ($in['article'] as $row){
			$discount_line = $in['changePrices'] ? return_value($o['disc_line']) : $row['disc'];

			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}
			if($in['apply_discount'] < 2){
				$global_disc = 0;
			}

			if($row['content'] != '1'){
				/*$q = return_value($row['quantity']);
				$price_line = return_value($row['price'])*$row['packing']/$row['sale_unit'];*/
				$q = return_value($row['quantity'])* $row['packing'] / $row['sale_unit'];
				$price_line =return_value($row['price']) - (return_value($row['price']) * $discount_line /100);
				$discount_total += $price_line * $global_disc / 100 * $q;
				$line_total=$price_line * $q;
			}else{
				$line_total=0;
			}
			$total += $line_total;

			$nr_dec_q = strlen(rtrim(substr(strrchr(return_value($row['quantity']), "."), 1),'0'));	 	
			$linie=array(
				'tr_id'						=> $row_id = 'tmp'.$i,
				'article'  					=> $row['article'],
				'article_id' 				=> $row['article_id'],
				'content'  					=> $row['content'] ? $row['content'] : false,
				'colspan'					=> $row['content'] ==1 ? 'colspan="8" class="last"' : '',
				'is_article'      	      	=> $row['article_id'] ? true : false,
				'tax_id'				    => $row['tax_id'],
				'tax_for_article_id'	    => $row['is_tax']==1?$row['tax_for_article_id']:'',
				'is_tax'      	            => $row['is_tax']==1 ? true : false,
				'quantity_old'      	    => display_number(return_value($row['quantity_old']),$nr_dec_q),
				'quantity'      			=> display_number(return_value($row['quantity']),$nr_dec_q),
				'price_vat'      			=> display_number_var_dec(return_value($row['price_vat'])),
				'price'         			=> display_number_var_dec(return_value($row['price'])),
				'percent'					=> $row['percent'],
				'vat_value'					=> $row['vat_value'],
				'percent_x'					=> $row['percent_x'],
				'vat_value_x'			    => $row['vat_value'],
				'sale_unit_x'				=> $row['sale_unit'],
				'packing_x'					=> remove_zero_decimals($row['packing']),
				'sale_unit'				    => $row['sale_unit'],
				'packing'					=> remove_zero_decimals($row['packing']),
				'article_code'				=> $row['article_code'],
				'is_article_code'			=> $row['article_code'] ? true : false,
				'is_article'      	      	=> $row['article_id'] ? true : false,
				'line_total'				=> display_number($line_total),
				'colum'						=> $row['article_code'] ? $o['colum'] : ($o['hide_disc'] ? 6 : 7),
				'has_variants' 				=> $row['has_variants'],
			 	'has_variants_done' 		=> $row['has_variants_done'],
			 	'is_variant_for' 			=> $row['is_variant_for'],
			 	'variant_type' 				=> $row['variant_type'],  
			 	'facq' 						=> $row['facq'],  
			 	'disc'				        => $in['changePrices']=='1' ? $o['disc_line'] : display_number($row['disc']),
			);
			array_push($o['tr_id'], $linie);
			$i++;
		}	
	}
	//console::log($total);

if($in['order_id']){
		$last_tr = '';
	$db->query("SELECT  pim_order_articles.* FROM  pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");
	while ($db->move_next())
	{
			$q = $db->f('quantity');
			$row_id = 'tmp'.$i;
			$price_line = $db->f('price')*$db->f('packing')/$db->f('sale_unit');
			$line_total=$price_line * $q;
			$total += $line_total;
		
			if($db->f('article_id')){
				$stock = $db2->field("SELECT stock from pim_articles WHERE article_id='".$db->f('article_id')."' ");
		 		$hide_stock = $db2->field("SELECT hide_stock from pim_articles WHERE article_id='".$db->f('article_id')."'");
			}
		  	$purchase_price = $db2->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$db->f('article_id')."' AND base_price='1'");
				if($purchase_price>0 && !USE_ARTICLE_PURCHASE_PRICE){
					$price=$purchase_price;
				}else{
					$price = $db->f('purchase_price');
				}
		
			if(!$db->f('tax_id')){
				$last_tr = $row_id;
			}

			$supplier_reference = $db2->field("SELECT supplier_reference FROM pim_articles WHERE article_id='".$db->f('article_id')."' ");
			//$article_code = $supplier_reference? $supplier_reference : $db->f('article_code');

			$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));	 	

		$linie = array(
			'tr_id'         					=> $row_id,
			'article'  			    			=> stripslashes(htmlspecialchars_decode($db->f('article'),ENT_QUOTES)),
	    	'article_code'						=> $db->f('article_code'),
	    	'is_article_code'					=> $db->f('article_code') ? true : false,
	    	'supplier_reference'				=> $supplier_reference,
	    	'is_supplier_reference'				=> $supplier_reference? true : false,
			'article_id'						=> $db->f('article_id')?$db->f('article_id'):$db->f('tax_id'),
			'tax_for_article_id'	    		=> $db->f('tax_for_article_id'),
			'is_article'      	      			=> $db->f('article_id') ? true : false,
			'is_article1'      	      			=> $db->f('article_id') ? true : false,
			'quantity_old'      	    		=> $db->f('quantity'),
			'quantity'      					=> display_number($db->f('quantity'), $nr_dec_q),
			'price_vat'      					=> display_number($db->f('price_with_vat')),
			'price'         					=> display_number_var_dec($price),
			'purchase_price'         			=> $db->f('purchase_price'),
			'stock'				    			=> $stock,	
			'content'							=> $db->f('content') ? true : false,
			'content_class'						=> $db->f('content') ? ' type_content ' : '',
	    	'line_total'						=> display_number($line_total),
	    	'origin_id'							=> $in['order_id'],
          	'origin_type'						=> '4',
          	'is_tax'      	          			=> $db->f('tax_id')?1:0,
			'for_article'						=> $db->f('tax_id') ? $last_tr : '',
			'sale_unit_x'						=> $db->f('sale_unit'),
			'packing_x'							=> remove_zero_decimals($db->f('packing')),
			'sale_unit'				    		=> $db->f('sale_unit'),
			'packing'							=> remove_zero_decimals($db->f('packing')),
			'has_variants' => $db->f('has_variants'),
		 	'has_variants_done' => $db->f('has_variants_done'),
		 	'is_variant_for' => $db->f('is_variant_for'),
		 	'variant_type' => $db->f('variant_type'), 
		);
		//reset prices for duplicate order depending of new article prices and price category of the company
		
		array_push($o['tr_id'], $linie);
		$i++;

	}

	}

	if($in['quote_id']){
		$last_tr = '';

		$db->query("SELECT tblquote_line.name, tblquote_line.article_code , tblquote_line.sale_unit, tblquote_line.package AS packing, tblquote_line.quantity, tblquote_line.purchase_price, tblquote_line.is_tax, tblquote_line.tax_for_line_id, tblquote_line.line_type,tblquote_line.line_order,tblquote_line.price,tblquote_line.sale_unit,tblquote_line.package AS packing,pim_articles.stock, pim_articles.hide_stock, pim_articles.supplier_id, pim_articles.supplier_name,pim_articles.supplier_reference, pim_articles.article_id, pim_articles.internal_name, pim_articles.internal_name AS article_name, pim_articles.item_code 
				FROM tblquote_line
				LEFT JOIN pim_articles ON tblquote_line.article_id = pim_articles.article_id
				WHERE tblquote_line.quote_id='".$in['quote_id']."' AND (tblquote_line.line_type='1' OR tblquote_line.line_type='3' )
        
              ORDER BY tblquote_line.line_order
                 ");

		while ($db->move_next())
		{
				$q = $db->f('quantity');
				$row_id = 'tmp'.$i;
				$price_line = $db->f('purchase_price')*$db->f('packing')/$db->f('sale_unit');
				$line_total=$price_line * $q;
				$total += $line_total;
			
				if($db->f('article_id')){
					$stock = $db2->field("SELECT stock from pim_articles WHERE article_id='".$db->f('article_id')."' ");
			 		$hide_stock = $db2->field("SELECT hide_stock from pim_articles WHERE article_id='".$db->f('article_id')."'");
				}
			  	$purchase_price = $db2->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$db->f('article_id')."' AND base_price='1'");
					if($purchase_price>0 && !USE_ARTICLE_PURCHASE_PRICE){
						$price=$purchase_price;
					}else{
						$price = $db->f('purchase_price');
					}
			
				if(!$db->f('is_tax')){
					$last_tr = $row_id;
				}

				$supplier_reference ='';
				if($db->f('article_id')){
					//$supplier_reference = $db2->field("SELECT supplier_reference FROM pim_articles WHERE article_id='".$db->f('article_id')."' ");
					$supplier_reference = $db->f('article_code');
				}
				
				
				$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));	 	

			$linie = array(
				'tr_id'         					=> $row_id,
				'article'  			    			=> stripslashes(strip_tags(html_entity_decode($db->f('name')))),
		    	'article_code'						=> $db->f('article_code'),
		    	'is_article_code'					=> $db->f('article_id') ? true : false,
		    	'supplier_reference'				=> $supplier_reference,
		    	'is_supplier_reference'				=> $supplier_reference? true : false,
				'article_id'						=> $db->f('article_id'),
				'tax_for_article_id'	    		=> $db->f('tax_for_line_id'),
				'is_article'      	      			=> $db->f('article_id') ? true : false,
				'is_article1'      	      			=> $db->f('article_id') ? true : false,
				'quantity_old'      	    		=> $db->f('quantity'),
				'quantity'      					=> display_number($db->f('quantity'), $nr_dec_q),
				'price_vat'      					=> display_number($db->f('price')+$db->f('price') * $db->f('vat')/100),
				'price'         					=> display_number_var_dec($price),
				'purchase_price'         			=> $db->f('purchase_price'),
				'stock'				    			=> $stock,	
				'content'							=> $db->f('line_type')=='3' ? true : false,
				'content_class'						=>$db->f('line_type')=='3' ? ' type_content ' : '',
		    	'line_total'						=> display_number($line_total),
		    	'origin_id'							=> $in['quote_id'],
	          	'origin_type'						=> '2',
	          	'is_tax'      	          			=> $db->f('is_tax')? true: false,
				'for_article'						=> $db->f('is_tax') ? $last_tr : '',
				'sale_unit_x'						=> $db->f('sale_unit'),
				'packing_x'							=> remove_zero_decimals($db->f('packing')),
				'sale_unit'				    		=> $db->f('sale_unit'),
				'packing'							=> remove_zero_decimals($db->f('packing')),
				'has_variants' 						=> $db->f('has_variants'),
			 	'has_variants_done' 				=> $db->f('has_variants_done'),
			 	'is_variant_for' 					=> $db->f('is_variant_for'),
			 	'variant_type' 						=> $db->f('variant_type'), 
			 	'facq' 								=> '1', 
			 	'colum' 							=>  $db->f('article_id') || $db->f('is_tax')? 4:7,
			);
			
			
			array_push($o['tr_id'], $linie);
			$i++;

		}

	}

	//$total_with_vat=$vat_total+$total;
	$total_with_vat=$vat_total+$total-$discount_total;
	
	$o['total']				    					= display_number($total);
	$o['total_vat']									= display_number($total_with_vat);
	$o['total_default_c']							= display_number($total_with_vat);
	$o['hide_add']				 					= 'hide';
	$o['p_order_id'] = $in['p_order_id'];
	if($discount_total){
		$show_discount_total = true;
	}
	$o['discount_total']        					= display_number($discount_total);

}else {
	//EDIT
	//$page_title = gm('Edit Order');
    $do_next = 'po_order-npo_order-po_order-update_order';
 	$o['do_request'] = 'po_order-npo_order-po_order-update_order';
 	if($in['duplicate_p_order_id']){
	
		if(!$in['is_duplicate'] || $in['changePrices']){
			$in['p_order_id'] = $in['duplicate_p_order_id'];
		};
		
		$o['duplicate_p_order_id']=$in['duplicate_p_order_id'];
		$page_title = gm('Duplicate Purchase Order');
		$do_next = 'po_order-npo_order-po_order-add_order';
		$o['do_request'] = 'po_order-npo_order-po_order-add_order';
		$type_title = gm('Draft');
		$nr_status = 1;
	}
	
	$o['p_order_id'] = $in['p_order_id'];
  	$order_query = $db->query("SELECT pim_p_orders.* ,customer_contacts.firstname,customer_contacts.lastname, customer_contacts.customer_id as cust_id
	            FROM pim_p_orders
	            LEFT JOIN customer_contacts ON customer_contacts.contact_id=pim_p_orders.customer_id
	            WHERE pim_p_orders.p_order_id='".$in['p_order_id']."'
	           ");
  	$cancel_link = "index.php?do=po_order-po_order&p_order_id=".$in['p_order_id'];
  
	if(!$order_query->move_next()){
		$o = array('redirect'=>'po_orders');
		json_out($o);
		// return ark::run('order-orders');
	}
	
	if($order_query->f('field') == 'customer_id'){
		$customer_id = $order_query->f('customer_id');
		$o['is_company']=true;
		$o['is_buyer']=true;
		$in['buyer_id']      	= $order_query->f('customer_id');
		$o['buyer_id'] = $in['buyer_id'];
	}else{
		$o['is_company']=false;
		$o['is_buyer']=false;
	}

	if ($order_query->f('customer_id'))
	{
		$price_category_id = $db2->field('SELECT cat_id FROM customers WHERE customer_id ='.$order_query->f('customer_id'));
	}
	else
	{
		$price_category_id =0;
	}

	$ref='';
	if(ACCOUNT_ORDER_REF && $order_query->gf('buyer_ref')){
		$ref = $order_query->gf('buyer_ref');
	}
   	$second_buyer_adr=$db2->query("SELECT * FROM customer_addresses WHERE customer_id = '".$order_query->f('second_customer_id')."' and address_id='".$order_query->f('second_address_id')."'");
   	$second_buyer_name=$db->field("SELECT name FROM customers WHERE customer_id = '".$order_query->f('second_customer_id')."'");
    $in['address_info2']=$second_buyer_adr->f('address').'
	'.$second_buyer_adr->f('zip').'  '.$second_buyer_adr->f('city').'
	'.get_country_name($second_buyer_adr->f('country_id'));

	$customer_notes="";
	if($in['buyer_id']){
		$buyer_info = $db2->query("SELECT customers.customer_notes
								FROM customers WHERE customer_id='".$in['buyer_id']."' ");
		$buyer_info->next();
		$customer_notes=stripslashes($buyer_info->f('customer_notes'));
	}
	$o['customer_notes']=$customer_notes;
	
	$o['show_discount']					= $order_query->gf('apply_discount');
	$o['main_address_id']      			= $order_query->gf('main_address_id');
	$o['sc_id']							= $order_query->gf('second_customer_id');
	$o['second_address_id']				= $order_query->gf('second_address_id');
	$o['client_delivery']				= $order_query->gf('client_delivery')==1 ? true : false; 
  	$in['delivery_id']      			= $order_query->f('delivery_id');
  	$o['delivery_id']      				= $order_query->f('delivery_id');
	$in['customer_id']      			= $order_query->gf('customer_id');
	$o['customer_id']      				= $order_query->gf('customer_id');
	$in['s_customer_name']  			= $order_query->f('firstname')." ".$order_query->f('lastname');
	$o['s_customer_name']  				= $order_query->f('firstname')." ".$order_query->f('lastname');
	$in['serial_number']    			= $order_query->gf('serial_number');
	$in['date']             			= date(ACCOUNT_DATE_FORMAT,$order_query->f('date'));
	$o['date']             				= date(ACCOUNT_DATE_FORMAT,$order_query->f('date'));
	$in['date_h']           			= $in['duplicate_p_order_id'] ? time() : $order_query->f('date');
	$o['date_h']           				= $in['duplicate_p_order_id'] ? time()*1000 : $order_query->f('date')*1000;
	$in['delivery_date']        		= $order_query->f('del_date') ? date(ACCOUNT_DATE_FORMAT,$order_query->f('del_date')) : "";
	$o['delivery_date']        			= $order_query->f('del_date') ? date(ACCOUNT_DATE_FORMAT,$order_query->f('del_date')) : "";
	$in['del_date']           			= $in['duplicate_p_order_id'] ? time() : ($order_query->f('del_date') ? $order_query->f('del_date') : '');
	$o['del_date']           			= $in['duplicate_p_order_id'] ? '' : ($order_query->f('del_date') ? $order_query->f('del_date')*1000 : '');
	$in['del_note']           			= $order_query->f('del_note');
	$o['del_note']           			= $order_query->f('del_note');
	$in['field']	           			= $order_query->f('field');
	$o['field']	           				= $order_query->f('field');
	$in['currency_name']    			= build_currency_name_list($order_query->f('currency_type'));
	$o['currency_name']    				= build_currency_name_list($order_query->f('currency_type'));
	$in['buyer_ref']					= $order_query->gf('buyer_ref');
	$o['buyer_ref']						= $order_query->gf('buyer_ref');
	$in['currency_rate']    			= $order_query->f('currency_rate');
	$o['currency_rate']    				= $order_query->f('currency_rate');
	$o['currency_type_val']				= $order_query->f('currency_type');
	// $in['notes']		        = $order_query->f('notes');
	$o['notes']							= get_notes($in);
	$in['our_ref']						= $order_query->f('our_ref');
	$o['our_ref']						= $order_query->f('our_ref');
	$in['your_ref']						= $order_query->f('your_ref');
	$o['your_ref']						= $order_query->f('your_ref');
	$in['languages']            		= $order_query->f('email_language');
	$o['languages']            			= $order_query->f('email_language');
	if($in['duplicate_p_order_id']){
		$in['serial_number'] = generate_serial_number(DATABASE_NAME);
	}
	$o['serial_number']    				= $order_query->gf('serial_number');

	$existIdentity = $db2->field("SELECT COUNT(identity_id) FROM multiple_identity ");

	if($in['buyer_changed'] || $in['buyer_changed_save']){
		//	
	}else{
		$in['discount']	           			= display_number($order_query->f('discount'));
	}
	
	$o['discount']	           			= $in['discount'];

	if($in['buyer_changed'] || $in['buyer_changed_save']){
		$o['apply_discount']=$in['apply_discount'];
		$o['discount_line_gen']			=	 display_number($in['discount_line_gen']);
	}else{
		$in['apply_discount']					= $order_query->f('apply_discount');
		$o['apply_discount']					= $order_query->f('apply_discount');
		$in['discount_line_gen']			= display_number($order_query->gf('discount_line_gen'));
		$o['discount_line_gen']			=	 display_number($order_query->gf('discount_line_gen'));
	}

	// $view->assign(array(
	$o['form_mode']             				= 1; //0-add 1-edi;
	$o['customer_vat_dd']       				= build_vat_dd($order_query->f('customer_vat_id'));
	$o['payment_type_dd']       				= build_payment_type_dd($order_query->f('payment_type'));
	$o['payment_type_txt']       				= build_payment_type_dd($order_query->f('payment_type'),true);
	$o['payment_type']							= $order_query->f('payment_type');
	$o['hide_currency2']						= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
	$o['hide_currency1']						= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
	$o['currency_type']							= get_commission_type_list($order_query->f('currency_type'));#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$';
	$o['default_currency_val']  				= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
	$o['checked']								= $order_query->f('rdy_invoice') ? 'checked="checked"':'';

	$o['customer_name']							= $order_query->f('customer_name');
	$o['customer_email']     					= $order_query->f('customer_email');
	$o['contact_id']							= $in['contact_id']? $in['contact_id'] : $order_query->f('contact_id');
	$o['contact_name']							= $in['contact_id'] ? $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ") : $order_query->f('contact_name');
	$o['customer_city']     					= $order_query->f('customer_city');
	$o['customer_zip']      					= $order_query->f('customer_zip');
	$o['customer_address']  					= $order_query->f('customer_address');
	$o['customer_phone']    					= $order_query->f('customer_phone');
	$o['customer_cell']     	 				= $order_query->f('customer_cell');
	$o['buyer_country']								= get_country_name($order_query->f('customer_country_id'));
	$o['customer_country_id']       	= $order_query->f('customer_country_id');
	$o['delivery_address_txt']     		= nl2br($order_query->f('delivery_address'));
	$o['delivery_address']     				= $order_query->f('delivery_address');
	//$o['view_delivery']             	= trim($order_query->gf('delivery_address')) ? true : false;
	$o['customer_vat_number']					= $order_query->gf('customer_vat_number');
	$o['name_txt']										= 'Name:';
	$o['ref_serial_number']						= $ref.$order_query->gf('serial_number');
	$o['ref_var']											= $order_query->gf('buyer_ref') ? " var ref= '".$order_query->gf('buyer_ref')."';" : " var ref;";

	$o['discount_percent']         		= '( '.$in['discount'].'% )';
	// $o['address_info']          			= nl2br($order_query->gf('address_info'));
	/* $address_info = $order_query->f('buyer_address').'
		'.$order_query->f('buyer_zip').' '.$order_query->f('buyer_city').'
		'.get_country_name($order_query->f('buyer_country_id'));*/
		$address_info = $order_query->f('customer_address').'
		'.$order_query->f('customer_zip').' '.$order_query->f('customer_city').'
		'.get_country_name($order_query->f('customer_country_id'));
	

   	$main_adddress_info=nl2br(trim($order_query->f('address_info'))) ? nl2br($order_query->f('address_info')) : nl2br($address_info); 
	$main_address_data=$db2->query("SELECT * FROM customer_addresses WHERE address_id = '".$order_query->f('main_address_id')."'");
	while($main_address_data->next()){
		$main_adddress_info=nl2br($main_address_data->f('address').'
	'.$main_address_data->f('zip').'  '.$main_address_data->f('city').'
	'.get_country_name($main_address_data->f('country_id')));
	}

	if($in['buyer_changed'] || $in['buyer_changed_save']){
		if($in['apply_discount']==0){
			$discount_line=false;
			$discount_global=false;
		}elseif($in['apply_discount']==1){
			$discount_line=true;
			$discount_global=false;
		}elseif($in['apply_discount']==2){
			$discount_line=false;
			$discount_global=true;
		}elseif($in['apply_discount']==3){
			$discount_line=true;
			$discount_global=true;
		}
	}else{
		if($order_query->f('apply_discount')==0){
			$discount_line=false;
			$discount_global=false;
		}elseif($order_query->f('apply_discount')==1){
			$discount_line=true;
			$discount_global=false;
		}elseif($order_query->f('apply_discount')==2){
			$discount_line=false;
			$discount_global=true;
		}elseif($order_query->f('apply_discount')==3){
			$discount_line=true;
			$discount_global=true;
		}
	}

	$o['apply_discount_line']      						= $discount_line;
	$o['apply_discount_global']     					= $discount_global;

	$o['DISCOUNT']              						= display_number($in['discount']);
	$o['hide_disc']										= $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? false : true;
	$o['hide_global_discount']							= $in['apply_discount'] < 2 ? false : true;
	$o['hide_line_global_discount']						= $in['apply_discount'] == 1 ? '' : 'hide';

	$o['disc_line']										= display_number($order_query->f('discount_line_gen'));
		
	$o['address_info_txt2']			= nl2br(trim($in['address_info2']));
	$o['second_buyer_name']			= $second_buyer_name;
	//$o['address_info_txt']						= nl2br(trim($order_query->f('address_info'))) ? nl2br($order_query->f('address_info')) : nl2br($address_info); 
	$o['address_info_txt']  		=$main_adddress_info;
	$o['address_info']				= trim($order_query->f('address_info')) ? nl2br($order_query->f('address_info')) : nl2br($address_info);
	$o['source_dd']					= get_categorisation_source();
	$o['source_id']					= $order_query->f('source_id');
	$o['type_dd']					= get_categorisation_type();
	$o['type_id']					= $order_query->f('type_id');
	$o['segment_dd']				= get_categorisation_segment();
	$o['segment_id']				= $order_query->f('segment_id');
	$o['acc_manager']				= $order_query->gf('author_id') ? get_user_name($order_query->gf('author_id')) : '';
	$o['total_currency_hide']		= $in['currency_rate'] ? true : false;
	$o['gender_dd']					= gender_dd();
	$o['title_dd']					= build_contact_title_type_dd(0);
	$o['language_dd'] 				= build_language_dd_new($in['email_language']);
	$o['acc_manager_id']     							= $order_query->gf('acc_manager_id');
	$o['acc_manager_name']   							= $order_query->gf('acc_manager_name');
	$o['multiple_identity_txt'] 						= $db2->field("SELECT identity_name from multiple_identity WHERE identity_id='".$order_query->gf('identity_id')."'");
	$o['multiple_identity_dd'] 							= build_identity_dd($order_query->gf('identity_id'));
	$o['existIdentity']									= $existIdentity >0 ? true : false;
  	$o['identity_id'] 									= $order_query->gf('identity_id') ? $order_query->gf('identity_id') : '0';  	
  	$in['delivery_address_id']							= $order_query->gf('delivery_address_id');
  	$o['delivery_address_id']							= $order_query->gf('delivery_address_id');

  	$o['allow_article_packing']         = $order_query->f('use_package') ? true : false;
	$o['allow_article_sale_unit']       = $order_query->f('use_sale_unit') ? true : false;
	$o['is_duplicate']			 						= $in['duplicate_p_order_id']? true: false;
	$o['colum'] = $in['apply_discount'] == 1 || $in['apply_discount'] == 3 ? 3 : 4;
	$o['hide_disc']								= $in['apply_discount'] == 1 || $in['apply_discount'] == 3 ? true : false;

  	if($o['identity_id'] != '0'){
		$mm = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$o['identity_id']."'")->getAll();
		$value = $mm[0];
		$template = $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$o['identity_id']."' and module='order'");
		
		$o['main_comp_info']=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> $value['company_logo'],
				);
		
	}

	$discount_total = 0;
	$show_discount_total = false;
	$vat_value = 0;
	$global_disc = $in['discount'];
	
	$old_currency_type = $in['currency_type'];

	// $use_package 			= $order_query->f('use_package');

	$o['tr_id']=array();
	$last_tr = '';
	$db->query("SELECT pim_p_order_articles.* FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order ASC");
	while ($db->move_next())
	{			
			$sale_unit= $db->f('sale_unit');
			if(!ALLOW_ARTICLE_SALE_UNIT){
				$sale_unit=1;
			}
			$packing= $db->f('packing');
			if(!ALLOW_ARTICLE_PACKING){
				$packing=1;
				$sale_unit=1;
			}
			if(!$show_discount_total && $db->f('discount') !=0){
				$show_discount_total = true;
			}
			$discount_line = $in['changePrices']=='1' ? $order_query->f('discount_line_gen') : $db->f('discount');
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}
			if($in['apply_discount'] < 2){
				$global_disc = 0;
			}
			$q = $db->f('quantity');
			$row_id = 'tmp'.$i;
			$price_line = ($db->f('price')-($db->f('price')* $discount_line /100))*$packing/$sale_unit;
			$discount_total += $price_line * $global_disc / 100 * $q;
			$line_total=$price_line * $q;
			$total += $line_total;

		if($db->f('article_id')){
			$stock = $db2->field("SELECT stock from pim_articles WHERE article_id='".$db->f('article_id')."' "); 	
	    	$hide_stock = $db2->field("SELECT hide_stock from pim_articles WHERE article_id='".$db->f('article_id')."'");
		}
	
		if(!$db->f('tax_id')){
			$last_tr = $row_id;
		}
		
		$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));	 	

		$linie = array(
			'tr_id'         					=> $row_id,
			'article'  			    			=> stripslashes(htmlspecialchars_decode($db->f('article'),ENT_QUOTES)),
	    	'article_code'						=> $db->f('article_code'),
	    	'is_article_code'					=> $db->f('article_code') ? true : false,
	    	'supplier_reference'				=> $db->f('supplier_reference'),
	    	'is_supplier_reference'				=> $db->f('supplier_reference') ? true : false,
			'article_id'						=> $db->f('article_id')?$db->f('article_id'):$db->f('tax_id'),
			'tax_for_article_id'	    		=> $db->f('tax_for_article_id'),
			'is_article'      	      			=> $db->f('article_id') ? true : false,
			'is_article1'      	      			=> $db->f('article_id') ? true : false,
			'quantity_old'      	    		=> $db->f('quantity'),
			'quantity'      					=> display_number($db->f('quantity'), $nr_dec_q),
			'price_vat'      					=> display_number($db->f('price_with_vat')),
			'price'         					=> display_number_var_dec($db->f('price')),
			'purchase_price'         			=> $db->f('purchase_price'),
			'stock'				    			=> $stock,
			'content'							=> $db->f('content') ? true : false,
			'content_class'						=> $db->f('content') ? ' type_content ' : '',
	    	'line_total'						=> display_number($line_total),
	    	'is_tax'      	          			=> $db->f('tax_id')?1:0,
			'for_article'						=> $db->f('tax_id') ? $last_tr : '',
			'colum'								=> $db->f('article_id') || $db->f('tax_id')? $o['colum'] : ($o['hide_disc'] ? 6 : 7),
			'sale_unit'				    		=> $db->f('sale_unit'),
			'packing'							=> remove_zero_decimals($db->f('packing')),
			'sale_unit_x'				    	=> display_number($db->f('sale_unit')),
			'packing_x'							=> remove_zero_decimals(display_number($db->f('packing'))),
			'has_variants' 						=> $db->f('has_variants'),
		 	'has_variants_done'					=> $db->f('has_variants_done'),
		 	'is_variant_for' 					=> $db->f('is_variant_for'),
		 	'variant_type' 						=> $db->f('variant_type'), 
		 	'facq' 								=> $db->f('facq'), 
		 	'disc'										=> $in['duplicate_order_id'] ? display_number($discount_line) : ($in['changePrices']=='1' ? display_number($order_query->f('discount_line_gen')) :  display_number($db->f('discount'))),
		);
		//reset prices for duplicate order depending of new article prices and price category of the company
		
		array_push($o['tr_id'], $linie);
		$i++;

	}
	$total = round($total,2);
	$vat_total = round($vat_total,2);
	$total_with_vat=$vat_total+$total-$discount_total;

	$total_currency = $total_with_vat * return_value($in['currency_rate']);

	$o['discount_total']        	= display_number($discount_total);
	$o['total']				    	= display_number($total);
	$o['total_vat']					= $in['remove_vat'] ? display_number($total) : display_number($total_with_vat);
	$o['total_default_c']			= display_number($total_currency);


}
$default_note = $db->field("SELECT value FROM default_data WHERE type='p_order_note'");

$is_extra = false;
if($customer_id){
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");

	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<h4 >'.gm('Bank Details').'</h4>
	<p><strong>'.gm('Name').': </strong> <span>'.$c_details->f('bank_name').'</span></p>
	<p><strong>'.gm('BIC Code').': </strong> <span>'.$c_details->f('bank_bic_code').'</span></p>
	<p><strong>'.gm('IBAN').': </strong> <span>'.$c_details->f('bank_iban').'</span></p>
	';
	}
	if($extra_info){
		$is_extra = true;
		$extra_info = '<div style="width: 924px;" >'.$extra_info.'</div>';
	}
}

	//$o['notes']                     = $in['notes'] ? $in['notes'] : ($default_note ? $default_note : '');
	$o['notes']                     = get_notes($in);
	$o['pick_date_format']          = pick_date_format(ACCOUNT_DATE_FORMAT);
	$o['style']                     = ACCOUNT_NUMBER_FORMAT;
	$o['page_title']			    = $page_title;
	$o['type_title']			    = $type_title;
	$o['nr_status']			    	= $nr_status;
	$o['do_next']	    		    = $do_next;
	// 'total_currency_hide'		=> 'hide',
	$o['extra_info']				= $extra_info;
	$o['is_extra']					= $is_extra;
	

////////// multilanguage for order note ////////////////

$transl_lang_id_active 		= $in['languages'];
if(!$transl_lang_id_active){
	$transl_lang_id_active = $in['email_language'];
}
// console::log('lang_d2 '.$transl_lang_id_active);
$o['translate_loop'] = array();
$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
while($langs->next()){
	if(!$in['p_order_id'] || $in['p_order_id'] =='tmp'){
		if($langs->f('lang_id')==1){
			$note_type = 'p_order_note';
		}else{
			$note_type = 'p_order_note_'.$langs->f('lang_id');
		}
		$order_note = $db3->field("SELECT value FROM default_data
								   WHERE default_name = 'order note'
								   AND type = '".$note_type."' ");
		
	}else{
			$order_note = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'p_order'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['p_order_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

			// $transl_lang_id_active = $db3->field("SELECT lang_id FROM note_fields
			// 					   WHERE item_type 	= 'p_order'
			// 					   AND 	item_name 	= 'notes'
			// 					   AND 	active 		= '1'
			// 					   AND 	item_id 	= '".$in['p_order_id']."' ");
			if($in['duplicate_p_order_id']){
				$transl_lang_id_active = $in['languages'];
			}
	}
	$translate_lang = 'form-language-'.$langs->f('code');
	if($langs->f('code')=='du'){
		$translate_lang = 'form-language-nl';
	}

	if($transl_lang_id_active != $langs->f('lang_id')){
		$translate_lang = $translate_lang.' hidden';
	}

	array_push($o['translate_loop'], array(
		'translate_cls' 	=> 		$translate_lang,
		'lang_id' 			=> 		$langs->f('lang_id'),
		'notes'				=> 		strip_tags(html_entity_decode($order_note)),
	));

}
$transl_lang_id_active_custom 		= $in['languages'];
if(!$transl_lang_id_active_custom){
	$transl_lang_id_active_custom = $in['email_language'];
}
$o['translate_loop_custom'] = array();

$in['only_contact_name'] = 1;
$in['title'] = 1;
$o['translate_lang_active']							= 'form-language-'.$code;
$o['language_dd'] 									= build_language_dd_new($transl_lang_id_active);
$o['email_language'] 								= $transl_lang_id_active;
$o['langs'] 										= $transl_lang_id_active;

$o['nr_decimals'] 									= ARTICLE_PRICE_COMMA_DIGITS;
$o['cancel_link']									= $cancel_link;
$o['language_txt']									= gm($lang_code);
$o['style']  										= ACCOUNT_NUMBER_FORMAT;

$o['accmanager']									= get_accmanager($in,true,false);

$o['contacts']										= get_contacts($in,true,false);
$o['customers']										= get_customers($in,true,false);

$in['second_id']									= '';
$o['addresses']										= get_addresses($in,true,false);
$in['second_id']									= $o['sc_id'];
$o['second_addresses']								= get_addresses($in,true,false);
/*if($in['p_order_id'] && $in['p_order_id']!='tmp'){
	$in['customer_id'] = $order_query->gf('second_customer_id');
	$o['second_addresses']								= get_addresses($in,true,false);
}*/
$o['cc']											= get_cc($in,true,false);
$in['sc_id']										= $o['sc_id'];
$o['sc']											= get_sc($in,true,false);
$o['articles_list']									= get_articles_list($in,true,false);
$o['country_dd']									= build_country_list();
$o['main_country_id']								= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';

json_out($o);


function get_accmanager($in,$showin=true,$exit=true){

	$q = strtolower($in["term"]);

	global $database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($database_users);

	$filter = '';

	if($q){
	  $filter .=" AND users.first_name LIKE '".$q."%'";
	}

	$db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

	$items = array();
	while($db_users->move_next()){
		array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>$db_users->f('first_name').' '.$db_users->f('last_name') ) );
	}
	return json_out($items, $showin,$exit);

}


function  get_notes($in){
	$db=new sqldb();
	/*$o = array();
	$transl_lang_id_active 		= $in['languages'];
	if(!$transl_lang_id_active){
		$transl_lang_id_active = $in['email_language'];
	}
	$o['translate_loop'] = array();
	$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
	while($langs->next()){
		if(!$in['p_order_id'] ){
			if($langs->f('lang_id')==1){
				$note_type = 'p_order_note';
			}else{
				$note_type = 'p_order_note_'.$langs->f('lang_id');
			}
			$order_note = $db->field("SELECT value FROM default_data
									   WHERE default_name = 'order note'
									   AND type = '".$note_type."' ");
			
		}else{
				$order_note = $db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'p_order'
									   AND 	item_name 	= 'notes'
									   AND 	item_id 	= '".$in['p_order_id']."'
									   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

				$transl_lang_id_active = $db->field("SELECT lang_id FROM note_fields
									   WHERE item_type 	= 'order'
									   AND 	item_name 	= 'notes'
									   AND 	active 		= '1'
									   AND 	item_id 	= '".$in['order_id']."' ");
				
				
			
		}
		$translate_lang = 'form-language-'.$langs->f('code');
		if($langs->f('code')=='du'){
			$translate_lang = 'form-language-nl';
		}

		if($transl_lang_id_active != $langs->f('lang_id')){
			$translate_lang = $translate_lang.' hidden';
		}

		array_push($o['translate_loop'], array(
			'translate_cls' 	=> 		$translate_lang,
			'lang_id' 			=> 		$langs->f('lang_id'),
			'notes'				=> 		$order_note,
			));

	}
	$transl_lang_id_active_custom 		= $in['languages'];
	if(!$transl_lang_id_active_custom){
		$transl_lang_id_active_custom = $in['email_language'];
	}
	$o['translate_loop_custom'] = array();



	
	$in['title'] = 1;

	$o['translate_lang_active']				= 'form-language-'.$code;
	$o['language_txt']								= gm($lang_code);

	return json_out($o, $showin,$exit);*/

	$terms = '';
		if($in['email_language']!=1){		
			$terms = '_'.$in['email_language'];
		}
		$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_main_id='0'
												   AND type = 'p_order_note".$terms."'  ");
		if($in['p_order_id']){ // the function is called from inside the class
			if($in['p_order_id'] == 'tmp'){
				$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_main_id='0'
												   AND type = 'p_order_note".$terms."'  ");
				$array = $quote_note;
			}else{
				$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_main_id='0'
												   AND type = 'p_order_note".$terms."'  ");
				$quote_notes = $db->field("SELECT item_value FROM note_fields
								 WHERE item_id= '".$in['p_order_id']."' AND lang_id ='".$in['languages']."' AND item_type='p_order' AND item_name='notes' AND active='1'");
				if($quote_notes){
					$array=$quote_notes;
				}else{
					$array=$quote_note;
				}
			}
		}else{ // the function is called when the user changed the language
			$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_main_id='0'
												   AND type = 'p_order_note".$terms."'  ");
			$array = $quote_note;
		}

	return $array;

}
function get_customers($in,$showin=true,$exit=true){
	
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" AND is_admin='0' AND is_supplier=1 ";

	if($q){
		$filter .=" AND name LIKE '%".addslashes($q)."%'";
	}

	if($is_admin){
		$filter =" AND is_admin='".$is_admin."' ";
	}

	if($in['current_id']){
		$filter .= " AND customer_id !='".$in['current_id']."'";
	}
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 $filter GROUP BY customers.customer_id ORDER BY customers.name limit 9 ")->getAll();

	# foreach e 2x mai rapid decat while-ul
	$result = array();

	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);
		
	}
	$added = false;
	if(count($result)==9){
		array_push($result,array('id'=>'99999999999'));
		$added = true;
	}
	
	if($in['buyer_id']){
		$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 AND customer_id='".$in['buyer_id']."' GROUP BY customers.customer_id ORDER BY customers.name ")->getAll();
		$value = $cust[0];
		$cname = trim($value['name']);
		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);
	}
	
	if(!$added){
		array_push($result,array('id'=>'99999999999'));		
	}
	return json_out($result, $showin,$exit);

}

function get_contacts($in,$showin=true,$exit=true){
	if(!$in['buyer_id'] || ($in['buyer_id'] && !is_numeric($in['buyer_id']))){
		return array();
	}

	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db2 = new sqldb();
	$db_user = new sqldb($database_users);
	$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";

	if($in['current_id']){
		$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
	}

	if($in['customer_id']){
		$filter .= " AND customers.customer_id='".$in['customer_id']."'";
	}
	if($in['buyer_id']){
		$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
	}
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$items = array();
	$db= new sqldb();

	$title = array();
	$titles = $db2->query("SELECT * FROM customer_contact_title ")->getAll();
	foreach ($titles as $key => $value) {
		$title[$value['id']] = $value['name'];
	}

	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, customer_contacts.language,
				customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
			 	LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
				LEFT JOIN country ON country.country_id=customer_contact_address.country_id
				WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
	$result = array();
	foreach ($contacts as $key => $value) {
		$contact_title = $title[$value['title']];
    	if($contact_title){
			$contact_title .= " ";
    	}		

	    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
	    $price_category = "1";
	    if ($value['customer_id']){
	  		$price_category = $value['cat_id'];
	  	}

		$result[]=array(
			'symbol'				=> '',
			"id"					=> $value['contact_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"email"					=> $value['email'],
			"price_category_id"		=> $price_category, 
			'customer_id'	 		=> $value['customer_id'], 
			'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
			'currency_id' 			=> $value['currency_id'], 
			"lang_id" 				=> $value['internal_language'], 
			'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
			'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
			'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']) ,
			'email_language'		=> $value['language']

		);

	}

	$added = false;
	if(count($result)==9){
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999'));
		}
		$added = true;
	}
	if($in['contact_id']){
		$cust = $db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, customer_contacts.language,
				customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 				LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
				LEFT JOIN country ON country.country_id=customer_contact_address.country_id
				WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
		$value = $cust[0];
		$contact_title = $title[$value['title']];
    	if($contact_title){
			$contact_title .= " ";
    	}		

	    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
	  	$price_category = "1";
		if ($value['customer_id']){
	  		$price_category = $value['cat_id'];
	  	}
	  	
		$result[]=array(
			"id"					=> $value['contact_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"email"					=> $value['email'],
			"price_category_id"		=> $price_category, 
			'customer_id'	 		=> $value['customer_id'], 
			'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
			'currency_id' 			=> $value['currency_id'], 
			"lang_id" 				=> $value['internal_language'], 
			'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
			'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
			'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']) ,
			'email_language'		=> $value['language']
		);
	}
	if(!$added){
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999'));
		}
	}
	
	return json_out($result, $showin,$exit);
}

function get_cc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	
	$db_user = new sqldb($database_users);
	$filter =" is_admin='0' AND customers.active=1 AND is_supplier=1 ";
	// $filter_contact = ' 1=1 ';
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
		// $filter_contact .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
	}
	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
	}

	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id		
			ORDER BY name
			LIMIT 9")->getAll();

	$result = array();
	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
		$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				/*"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
		);
		
	}
	if($q){
		array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	}else{
		array_push($result,array('id'=>'99999999999','value'=>''));
	}
	
	return json_out($result, $showin,$exit);

}
function get_sc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	
	$db_user = new sqldb($database_users);
	$filter =" 1=1 ";
	// $filter_contact = ' 1=1 ';
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
		// $filter_contact .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
	}

	if($in['sc_id']){
		$filter .= " AND customers.customer_id='".$in['sc_id']."'";	
	}

	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id		
			ORDER BY name
			LIMIT 9")->getAll();

	$result = array();
	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
		$result[]=array(
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				/*"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
		);
		
	}
	if($q){
		array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	}else{
		array_push($result,array('id'=>'99999999999','value'=>''));
	}
	array_push($result,array('id'=>'99999999999','value'=>''));
	
	return json_out($result, $showin,$exit);

}
function get_addresses($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	if($q){
		$filter .=" AND (customer_addresses.address LIKE '%".addslashes($q)."%' OR customer_addresses.zip LIKE '%".addslashes($q)."%' OR customer_addresses.city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
	}

	$filter_c = $in['buyer_id'];
	if($in['customer_id'] && !$in['second_id']){
		$filter_c = $in['customer_id'];
	}elseif( $in['second_id']){
		$filter_c = $in['second_id'];

	}
	// array_push($result,array('id'=>'99999999999','value'=>''));
	if($in['field'] == 'customer_id'){
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
								 FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
								 LEFT JOIN state ON state.state_id=customer_addresses.state_id
								 WHERE customer_addresses.customer_id='".$filter_c."' ".$filter."
								 ORDER BY customer_addresses.address_id limit 9");
	}
	else if($in['field'] == 'contact_id'){
	   $address= $db->query("SELECT customer_contact_address.*,country.name AS country 
							 FROM customer_contact_address 
							 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
							 WHERE customer_contact_address.contact_id='".$in['contact_id']."' 
							 AND customer_contact_address.delivery='1' limit 9");
	}

	// $max_rows=$db->records_count();
	// $db->move_to($offset*$l_r);
	// $j=0;
	$addresses=array();
	if($address){
		while($address->next()){
		  	$a = array(
		  		'symbol'				=> '',
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	/*'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
			  	'right'				=> '',
			  	'bottom'			=> '',
		  	);
			array_push($addresses, $a);
		}
	}
	$added = false;
	if(count($addresses)==9){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
		}
		$added = true;
	}
	if($in['delivery_address_id']){
		if($in['field'] == 'customer_id'){
			$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['customer_id']."' AND customer_addresses.address_id='".$in['delivery_address_id']."'
									 ORDER BY customer_addresses.address_id ");
		}
		else if($in['field'] == 'contact_id'){
		   $address= $db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='".$in['delivery_address_id']."'
								 AND customer_contact_address.delivery='1' ");
		}
		$a = array(
		  		'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	/*'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
			  	'right'				=> '',
			  	'bottom'			=> '',
		  	);
			array_push($addresses, $a);
	}
	if(!$added){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
		}
	}
	
	return json_out($addresses, $showin,$exit);

}
function get_articles_list(&$in,$showin=true,$exit=true){
	$in['lang_id'] = $in['languages'];
	$in['cat_id'] = $in['price_category_id'];
	$in['from_order_page'] = true;
	unset($in['xget']);
	// return false;
	return ark::run('po_order-addArticle');
}