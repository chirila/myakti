<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

	ini_set('memory_limit', '1000M');
	$in['p_orders_ids'] ='';
	if($_SESSION['add_to_pdf_po']){
	  foreach ($_SESSION['add_to_pdf_po'] as $key => $value) {
	    $in['p_orders_ids'] .= $key.',';
	  }
	}
    if(isset($_SESSION['tmp_add_to_pdf_po'])){
        unset($_SESSION['tmp_add_to_pdf_po']);
    }
    if(isset($_SESSION['add_to_pdf_po'])){
        unset($_SESSION['add_to_pdf_po']);
    }
	$in['p_orders_ids'] = rtrim($in['p_orders_ids'],',');
	if(!$in['p_orders_ids']){
	  $in['p_orders_ids']= '0';
	}

	$headers = array("SERIAL NUMBER",
	    "SUPPLIER NAME",
	    "DATE",
	    "DELIVERY DATE",
	    "OUR REFERENCE",
	    "YOUR REFERENCE",
	    "ARTICLE CODE",
	    "ARTICLE DESCRIPTION",
	    "SUPPLIER_REFERENCE",
        "ASSOCIATED CUSTOMER",
	    "QUANTITY",
	    "DELIVERED QUANTITY",
	    "UNIT PRICE",
	    "TOTAL"    
	);
	$db = new sqldb();
    $filename="export_purchase_orders.csv";
	$final_data=array();

	$p_order_data=$db->query("SELECT pim_p_order_articles.*,pim_p_orders.serial_number,pim_p_orders.customer_name,pim_p_orders.date,pim_p_orders.del_date,pim_p_orders.our_ref,pim_p_orders.your_ref,pim_articles.item_code,
		SUM(pim_p_orders_delivery.quantity) as delivered_q,pim_p_order_deliveries.delivery_done, pim_articles.supplier_reference, pim_orders.customer_name as associated_customer
		FROM pim_p_orders 
		LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id 
		LEFT JOIN pim_articles ON pim_p_order_articles.article_id=pim_articles.article_id
		LEFT JOIN pim_p_orders_delivery ON pim_p_order_articles.p_order_id=pim_p_orders_delivery.p_order_id AND pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
		LEFT JOIN pim_p_order_deliveries ON pim_p_orders_delivery.delivery_id=pim_p_order_deliveries.delivery_id
        LEFT JOIN pim_orders ON pim_orders.order_id=pim_p_orders.order_id
		WHERE pim_p_order_articles.article_id!='0' AND pim_p_order_articles.tax_for_article_id='0' AND pim_p_order_articles.p_order_id IN (".$in['p_orders_ids'].") GROUP BY pim_p_orders.p_order_id,pim_p_order_articles.order_articles_id ");
	while($p_order_data->next()){
		$tmp_line=array(
			$p_order_data->f('serial_number'),
			stripslashes($p_order_data->f('customer_name')),
			date(ACCOUNT_DATE_FORMAT,$p_order_data->f('date')),
			($p_order_data->f('del_date')?date(ACCOUNT_DATE_FORMAT,$p_order_data->f('del_date')):''),
			stripslashes(preg_replace("/[\n\r]/"," ",$p_order_data->f('our_ref'))),
			stripslashes(preg_replace("/[\n\r]/"," ",$p_order_data->f('your_ref'))),
			stripslashes($p_order_data->f('item_code')),
			stripslashes(preg_replace("/[\n\r]/"," ",$p_order_data->f('article'))),
			stripslashes(preg_replace("/[\n\r]/"," ",$p_order_data->f('supplier_reference'))),
            stripslashes($p_order_data->f('associated_customer')),
			display_number_exclude_thousand($p_order_data->f('quantity')),
            display_number_exclude_thousand($p_order_data->f('delivered_q')),
            display_number_exclude_thousand($p_order_data->f('price')),
            display_number_exclude_thousand($p_order_data->f('quantity')*$p_order_data->f('price'))
		);
		array_push($final_data,$tmp_line);
	}


    //modified in 25.11.2021
    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');

	//$from_location=INSTALLPATH.'upload/'.DATABASE_NAME.'/tmp_export_purchase_orders.csv';
    $fp = fopen("php://output", 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    doQueryLog();
    exit();

// set column width to auto
    foreach(range('A','W') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
    }

    $objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
    $rows_format='A1:A'.$xlsRow;
    $objPHPExcel->getActiveSheet()->getStyle($rows_format)
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    $objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('upload/'.$filename);





    define('ALLOWED_REFERRER', '');
    define('BASE_DIR','upload/');
    define('LOG_DOWNLOADS',false);
    define('LOG_FILE','downloads.log');
    $allowed_ext = array (

        // archives
        'zip' => 'application/zip',

        // documents
        'pdf' => 'application/pdf',
        'doc' => 'application/msword',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'csv' => 'text/csv',

        // executables
        //'exe' => 'application/octet-stream',

        // images
        'gif' => 'image/gif',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',

        // audio
        'mp3' => 'audio/mpeg',
        'wav' => 'audio/x-wav',

        // video
        'mpeg' => 'video/mpeg',
        'mpg' => 'video/mpeg',
        'mpe' => 'video/mpeg',
        'mov' => 'video/quicktime',
        'avi' => 'video/x-msvideo'
    );
    ####################################################################
    ###  DO NOT CHANGE BELOW
    ####################################################################

    // If hotlinking not allowed then make hackers think there are some server problems
    if (ALLOWED_REFERRER !== ''
        && (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
    ) {
        die("Internal server error. Please contact system administrator.");
    }
    $fname = basename($filename);
    function find_file ($dirname, $fname, &$file_path) {
        $dir = opendir($dirname);
        while ($file = readdir($dir)) {
            if (empty($file_path) && $file != '.' && $file != '..') {
                if (is_dir($dirname.'/'.$file)) {
                    find_file($dirname.'/'.$file, $fname, $file_path);
                }
                else {
                    if (file_exists($dirname.'/'.$fname)) {
                        $file_path = $dirname.'/'.$fname;
                        return;
                    }
                }
            }
        }

    } // find_file
    // get full file path (including subfolders)
    $file_path = '';
    find_file('upload', $fname, $file_path);
    if (!is_file($file_path)) {
        die("File does not exist. Make sure you specified correct file name.");
    }
    // file size in bytes
    $fsize = filesize($file_path);
    // file extension
    $fext = strtolower(substr(strrchr($fname,"."),1));
    // check if allowed extension
    if (!array_key_exists($fext, $allowed_ext)) {
        die("Not allowed file type.");
    }
    // get mime type
    if ($allowed_ext[$fext] == '') {
        $mtype = '';
        // mime type is not set, get from server settings
        if (function_exists('mime_content_type')) {
            $mtype = mime_content_type($file_path);
        }
        else if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME); // return mime type
            $mtype = finfo_file($finfo, $file_path);
            finfo_close($finfo);
        }
        if ($mtype == '') {
            $mtype = "application/force-download";
        }
    }
    else {
        // get mime type defined by admin
        $mtype = $allowed_ext[$fext];
    }
    doQueryLog();
    // set headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Type: $mtype");
    header("Content-Disposition: attachment; filename=\"$fname\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $fsize);
    // download
    //@readfile($file_path);
    $file = @fopen($file_path,"rb");
    if ($file) {
        while(!feof($file)) {
            print(fread($file, 1024*8));
            flush();
            if (connection_status()!=0) {
                @fclose($file);

                die();
            }
        }
        @fclose($file);
    }
    /*
    ark::loadLibraries(array('PHPExcel'));
	$objReader = PHPExcel_IOFactory::createReader('CSV');

	$objPHPExcel = $objReader->load($from_location);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	unlink($from_location);

	doQueryLog();

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="export_purchase_orders.xls"');
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');   
	exit();*/

?>