<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


$batch = array('b_n_row'=>array());
$is_data = false;
if(intval($in['article_no'])>0){
	$is_data = true;
}


	$batch['article_name']		        = $in['article_name'];
	$batch['order_articles_id']			= $in['order_articles_id'];
	$batch['article_no']				= intval(return_value($in['article_no']));
	$batch['q_added']					=0;	


$in['article_id'] =$db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id = '".$in['order_articles_id']."'");
$batches= $db->query("SELECT * FROM batches WHERE article_id='".$in['article_id']."'");
$batches_arr=array(); 
while($batches->next()){
	$batches_arr[$batches->f('id')] = $batches->f('batch_number');

}

if(!$in['p_delivery_id']){
	if(is_array($in['selected_batch_no_v'][$in['order_articles_id']])){		
		$count_batch_no = count($in['selected_batch_no_v'][$in['order_articles_id']]);	
		$count_q = 0;
		for($i=0; $i<$count_batch_no; $i++){
			$count_q +=$in['selected_batch_no_q'][$in['order_articles_id']][$i];
			$date_exp='';
			if( in_array($in['selected_batch_no_v'][$in['order_articles_id']][$i], $batches_arr)){
				$date_exp=$db->field("SELECT date_exp FROM batches WHERE batch_number='".$in['selected_batch_no_v'][$in['order_articles_id']][$i]."'");
			}
			array_push($serial['b_n_row'],array(	
				'order_articles_id'	    => $in['order_articles_id'],	
				'selected_batch_no_v' 	=> $in['selected_batch_no_v'][$in['order_articles_id']][$i],
				'selected_batch_no_ed' 	=> $date_exp==0? '':date(ACCOUNT_DATE_FORMAT,$date_exp),
				'selected_batch_no_q' 	=> $in['selected_batch_no_q'][$in['order_articles_id']][$i],
				'exp_date'				=> $date_exp,
				'count_batches'			=>$count_batch_no,
				'i'						=>$i,
				'is_edit'			=> false,
				'is_used'			=> true,
			));
		//$view_s_n->loop('batch_number_row');	
		}
				
			$batch['q_added']		    = $count_q;
			$batch['class_green']		= intval(return_value($in['article_no'])) == $count_q ? 'green' : '';
			$batch['is_edit']			= false;
		
	}else{

		array_push($batch['b_n_row'],array(
			'is_used'					=> true,
			'i'							=>'0',
		));
		
		
	}	
}else{
	$in['inserted_b_n'] = array_reverse($in['inserted_b_n']);
	$in['inserted_b_n_ed'] = array_reverse($in['inserted_b_n_ed']);
	$in['inserted_b_n_q'] = array_reverse($in['inserted_b_n_q']);
	$in['batch_no_ids'] = array_reverse($in['batch_no_ids']);
	$in['used_batch_numbers'] = array_reverse($in['used_batch_numbers']);
	$count_q = 0;
	for ($i=0; $i < $in['batch_numbers_count']; $i++) { 
		$count_q += $in['inserted_b_n_q'][$i];
		
		$view_s_n->assign(array(
			'order_articles_id'	=> $in['order_articles_id'],	
			'selected_batch_no_v' 	=> $in['inserted_b_n'][$i],
			'selected_batch_no_ed' 	=> $in['inserted_b_n_ed'][$i]==0? '':date(ACCOUNT_DATE_FORMAT,$in['inserted_b_n_ed'][$i]),
			'selected_batch_no_q' 	=> $in['inserted_b_n_q'][$i],
			'is_edit'				=> true,
			'i'						=>$i,
			'batch_nr_id'		=> $in['batch_no_ids'][$i],
			'disabled_b_n'		=> $in['used_batch_numbers'][$i] == 1 ? 1 : '',
			'disable_edit_b_n'	=> $in['used_batch_numbers'][$i] == 1 ? 'disabled' : '',
			'readonly'			=> $in['used_batch_numbers'][$i] == 1 ? 'readonly="readonly"' : '',
			'is_used'			=> $in['used_batch_numbers'][$i] == 1 ? false : true,
		),'batch_number_row');
		$view_s_n->loop('batch_number_row');
	}	
	$view_s_n->assign(array(
		'q_added'				=> $count_q,
		'class_green'			=> intval(return_value($in['article_no'])) == $count_q ? 'green' : '',
		'is_edit'				=> true,
	));
}

	$batch['hide_for_view'] = true;


if($in['only_view_b_n']==1 && $in['p_delivery_id'] && (intval($in['order_articles_id'])>0) ){
	$article_id = $db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	
	$view_batch_numbers_data = $db->query("SELECT * FROM batches WHERE article_id = '".$article_id."' AND p_delivery_id = '".$in['p_delivery_id']."' ");
	$i=0;	
	while($view_batch_numbers_data->next()){
		$view_s_n->assign(array(
			'order_articles_id'	=> $in['order_articles_id'],	
			'selected_batch_no_v' 	=> $view_batch_numbers_data->f('batch_number'),
			'selected_batch_no_ed' 	=> $view_batch_numbers_data->f('date_exp')? $view_batch_numbers_data->f('date_exp') : '',
			'selected_batch_no_q' 	=> $view_batch_numbers_data->f('quantity'),
			'is_edit'			=> true,
			'batch_nr_id'		=> $view_batch_numbers_data->f('id'),
			'disabled_b_n'		=> '',
			'disable_edit_b_n'	=> '',
			'readonly'			=> 'readonly="readonly"',
			'is_used'			=> false,
		),'batch_number_row');
		$view_s_n->loop('batch_number_row');
		$i++;
	}

	$view_s_n->assign(array(
		'hide_for_view' 		=> false,
	));
	$is_data = true;	
}


	$batch['is_data']	= $is_data;
	


json_out($batch);
?>