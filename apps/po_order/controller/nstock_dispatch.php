<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

global $config;

// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
$db2 = new sqldb();
$db3 = new sqldb();
$db_lines=new sqldb();
$o = array(
	'main_comp_info'=>array(
		'name'		=> ACCOUNT_COMPANY,
		'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
		'zip'		=> ACCOUNT_DELIVERY_ZIP,
		'city'		=> ACCOUNT_DELIVERY_CITY,
		'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
		'phone'		=> ACCOUNT_PHONE,
		'fax'		=> ACCOUNT_FAX,
		'email'		=> ACCOUNT_EMAIL,
		'url'		=> ACCOUNT_URL,
		'logo'		=> '../'.ACCOUNT_LOGO_ORDER,
		)
);
$in['main_comp_info']=array(
	'name'		=> ACCOUNT_COMPANY,
	'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
	'zip'		=> ACCOUNT_DELIVERY_ZIP,
	'city'		=> ACCOUNT_DELIVERY_CITY,
	'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
	'phone'		=> ACCOUNT_PHONE,
	'fax'		=> ACCOUNT_FAX,
	'email'		=> ACCOUNT_EMAIL,
	'url'		=> ACCOUNT_URL,
	'logo'		=> '../'.ACCOUNT_LOGO_ORDER,
	);

$o['default_currency']     	= ACCOUNT_CURRENCY_TYPE;
$o['default_currency_name']    = build_currency_name_list(ACCOUNT_CURRENCY_TYPE);

$o['search_by']=$in['search_by'];
if(!$in['search_by']){
	$o['search_by']=1;
}
// $view->assign(array(
$o['search_by_name_check']		        = $in['search_by'] == 1 ? 'CHECKED' : '';
$o['search_by_company_number_check']	= $in['search_by'] == 2 ? 'CHECKED' : '';
// ));

$o['payment_term']=$in['payment_term'];
if(!$in['payment_term']){
	$o['payment_term']=15;
}
$cancel_link = "index.php?do=po_order-stock_dispatching";

if(!$in['stock_disp_id'] || $in['stock_disp_id'] == 'tmp'){
	$o['do_request'] = 'po_order-nstock_dispatch-stock_dispatch-updateCustomerData';
	if($in['buyer_id'] == 'tmp'){
		$in['add_customer']				= true;
		$in['cc']						= get_cc($in,true,false);
		$in['country_dd']				= build_country_list();
		$in['main_country_id']			= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$in['email_language'] 			= DEFAULT_LANG_ID;
		$in['payment_type_dd']     		= build_payment_type_dd($in['payment_type']);
		$in['payment_type']				= '0';
		$in['language_dd'] 				= build_language_dd_new($in['email_language']);
		$in['currency_id']				= ACCOUNT_CURRENCY_TYPE;
		$in['identity_id'] 				= '0';
		$in['articles_list']			= get_articles_list($in,true,false);
		$in['customers']				= get_customers($in,true,false);
		$in['addresses']				= get_addresses($in,true,false);
		$in['languages']				= $in['email_language'];
		$in['do_request']				= 'po_order-nstock_dispatch-stock_dispatch-updateCustomerData';
		
		
		json_out($in);
	}elseif ($in['buyer_id'] && !is_numeric($in['buyer_id'])) {
		$o = array('redirect'=>'stock_dispatching');
		json_out($o);
		
	}

    $o['to_buyer_id']=$in['to_buyer_id'];
	if($in['to_buyer_id']){
		$in['to_addresses']				= get_to_addresses($in,true,false);
		
		$to_buyer_info = $db->query("SELECT deliv_disp_note, cat_id, c_email, our_reference ,comp_phone,name, customers.no_vat,customers.btw_nr, customers.internal_language,
								customers.line_discount, customers.apply_fix_disc,customers.currency_id, customers.apply_line_disc, customers.identity_id
								FROM customers WHERE customer_id='".$in['to_buyer_id']."' ");
		$to_buyer_info->next();

		$o['to_customer_name']							= stripslashes($to_buyer_info->f('name'));
		$in['to_naming']=stripslashes($to_buyer_info->f('name'));
		$o['to_naming']=stripslashes($to_buyer_info->f('name'));
		


        					  

		$to_buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['to_buyer_id']."' and address_id='".$in['to_main_address_id']."'");

		 $in['to_address_info']=$to_buyer_adr->f('address').'
        '.$to_buyer_adr->f('zip').'  '.$to_buyer_adr->f('city').'
        '.get_country_name($to_buyer_adr->f('country_id'));

        $in['to_zip']=$to_buyer_adr->f('zip');
		$o['to_zip']=$to_buyer_adr->f('zip');
		$in['to_city']=$to_buyer_adr->f('city');
		$o['to_city']=$to_buyer_adr->f('city');
		$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));
		$o['to_country']=get_country_name($to_buyer_adr->f('country_id'));
         



	}elseif($in['to_buyer_id']=='0'){
		$o['to_customer_name']							= 'Own Location';
		$in['to_customer_name']							= 'Own Location';
		$in['to_addresses']				= get_to_addresses($in,true,false);
		$to_buyer_adr=$db->query("SELECT * FROM dispatch_stock_address WHERE customer_id = '0' and address_id='".$in['to_main_address_id']."'");

      $in['to_address_info']=$to_buyer_adr->f('naming').' 
'.$to_buyer_adr->f('address').'
'.$to_buyer_adr->f('zip').'  '.$to_buyer_adr->f('city').'
'.get_country_name($to_buyer_adr->f('country_id'));
   $in['to_naming']=$to_buyer_adr->f('naming');
   $o['to_naming']=$to_buyer_adr->f('naming');
        $in['to_zip']=$to_buyer_adr->f('zip');
		$o['to_zip']=$to_buyer_adr->f('zip');
		$in['to_city']=$to_buyer_adr->f('city');
		$o['to_city']=$to_buyer_adr->f('city');
		$in['to_country']=get_country_name($to_buyer_adr->f('country_id'));
		$o['to_country']=get_country_name($to_buyer_adr->f('country_id'));

	}
	
	$in['email_language'] 		= DEFAULT_LANG_ID;
	$in['languages']			= $in['email_language'];
	$o['to_addresses']=$in['to_addresses'];
	$o['to_main_address_id']=$in['to_main_address_id'];
	
	//ADD
	$cancel_link = "index.php?do=po_order-stock_dispatching";
   
  	
  	$do_next = 'stock-nstock_dispatch-stock_dispatch-add';

  	if(!$in['date_h']){
		$in['date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		$in['date_h'] = time()*1000;
	}
	$o['date'] = $in['date'];
	$o['date_h'] = $in['date_h'];


	if (!$in['serial_number']){
		$in['serial_number'] = generate_stock_disp_serial_number(DATABASE_NAME);
	}
	$o['serial_number'] = $in['serial_number'];


	
	$o['buyer_id'] = $in['buyer_id'];
	$o['customer_id'] = $in['buyer_id'];
	

	$details = array('table' 		=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field'		=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'		=> $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],
					 'name'			=> $in['buyer_id'] ? stripslashes($in['s_buyer_id']) : stripslashes(substr($in['s_customer_id'], 0, strpos($in['s_customer_id'],'>'))) ,
	);

	$ref = '';
	$o['customer_ref'] = '';
	

		$customer_id = $in['buyer_id'];
		$o['is_company']=true;
		$buyer_info = $db->query("SELECT deliv_disp_note, cat_id, c_email, our_reference ,comp_phone,name, customers.no_vat,customers.btw_nr, customers.internal_language,
								customers.line_discount, customers.apply_fix_disc,customers.currency_id, customers.apply_line_disc, customers.identity_id
								FROM customers WHERE customer_id='".$details['value']."' ");
		$buyer_info->next();




		

		
		
			if(!$details['name'] ){
	    	$details['name']=$buyer_info->f('name');
	    }
	  
		

		 $in['email_language'] 		= $buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : $in['email_language'];
		
		 $in['del_note']			= $in['del_note'] ? $in['del_note'] : $buyer_info->f('deliv_disp_note');
		 $in['languages']			= $in['email_language'];
		
			
				
	$buyer = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ORDER BY is_primary DESC");
	$buyer->next();

	if($in['buyer_id'] && $buyer->f('delivery') == 0){
		$buyer_del_address = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' AND delivery=1 limit 1 ");
		if($buyer_del_address){
				$o['delivery_address'] = $buyer_del_address->f('address').'
'.$buyer_del_address->f('zip').'  '.$buyer_del_address->f('city').'
'.get_country_name($buyer_del_address->f('country_id'));
		}
	}
	
	  
	$in['customer_id'] 		= $details['value'];
	$in['field'] 			= $details['field'];
	$o['field'] 			= $details['field'];
	

  
  	
    if($in['buyer_id']){
$in['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').'  '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));

    }
	

 
  $buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['main_address_id']."'");
if($in['delivery_address_id']){
	$buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['delivery_address_id']."'");
}
 

    $in['address_info']=$buyer_adr->f('address').'
'.$buyer_adr->f('zip').'  '.$buyer_adr->f('city').'
'.get_country_name($buyer_adr->f('country_id'));
$in['from_naming']=stripslashes($details['name']);

if($in['buyer_id']==0){
	$in['delivery_address_id']=$in['main_address_id'];
	$buyer_adr=$db->query("SELECT * FROM dispatch_stock_address WHERE customer_id = '0' and address_id='".$in['delivery_address_id']."'");

      $in['address_info']=$buyer_adr->f('naming').' 
'.$buyer_adr->f('address').'
'.$buyer_adr->f('zip').'  '.$buyer_adr->f('city').'
'.get_country_name($buyer_adr->f('country_id'));
$in['from_naming']=$buyer_adr->f('naming');
	}


   

    $o['main_address_id'] = $in['main_address_id'];
	$o['our_ref'] = $in['our_ref'];
	$o['your_ref'] = $in['your_ref'];
	// $view->assign(array(
	$o['form_mode']                    			= 0; //0-add 1-edi;

    
	$o['del_note']           					= $in['del_note'];
    $o['customer_name']							= $details['name']?stripslashes($details['name']):'Own Location';
    


	
	$o['delivery_address']      				= $in['delivery_address'];
	$o['delivery_address_txt']     				= nl2br($in['delivery_address']);
	$o['delivery_address_id']     				= $in['delivery_address_id'];
	$o['customer_address']						= $in['buyer_id']?$buyer->f('address'):$buyer_adr->f('address');
	$o['customer_zip']							= $in['buyer_id']?$buyer->f('zip'):$buyer_adr->f('zip');
	$o['customer_city']							= $in['buyer_id']?$buyer->f('city'):$buyer_adr->f('city');
	$o['customer_country_id']					= $in['buyer_id']?$buyer->f('country_id'):$buyer_adr->f('country_id');
	$o['from_naming']						    = $in['from_naming'];
	$o['buyer_country']							= get_country_name($buyer->f('country_id'));
	$o['comp_name']								= $in['s_buyer_id'];
	$o['is_buyer_id']							= $in['s_buyer_id'] ? true : false;
	$o['is_buyer']			    				= $in['buyer_id'] ? true : false;
	
	$o['address_info_txt']						= nl2br(trim($in['address_info']));
	$o['address_info']							= $in['address_info'];
	$o['to_address_info_txt']						= nl2br(trim($in['to_address_info']));
	$o['to_address_info']							= $in['to_address_info'];

	
// ));
	



	$i=0;

	$o['tr_id']=array();
	
	
	if($in['article']){
		foreach ($in['article'] as $row){
			$row_id = 'tmp'.$i;
			

			
			$q = return_value($row['quantity']);

			
			
			
			$linie = $row;

			array_push($o['tr_id'], $linie);
			$i++;
		}
	}

	
	

}else {
	//EDIT
	
    $do_next = 'po_order-nstock_dispatch-stock_dispatch-update';
 	
	
	$o['stock_disp_id'] = $in['stock_disp_id'];
  	$order_query = $db->query("SELECT pim_stock_disp.* 
	            FROM pim_stock_disp
	          
	            WHERE pim_stock_disp.stock_disp_id='".$in['stock_disp_id']."'
	           ");
  	$cancel_link = "index.php?do=po_order-stock_dispatch&stock_disp_id=".$in['stock_disp_id'];
  
	if(!$order_query->move_next()){
		$o = array('redirect'=>'stock_dispatching');
		json_out($o);
		// return ark::run('order-orders');
	}
	

	

	
	$o['form_mode']             				= 1; //0-add 1-edi;
	

	

	

	

	$o['tr_id']=array();
	$last_tr = '';
	$db->query("SELECT pim_stock_disp_articles.* FROM pim_stock_disp_articles WHERE stock_disp_id='".$in['stock_disp_id']."' ORDER BY sort_order ASC");
	while ($db->move_next())
	{
		
			
			$q = $db->f('quantity');
			$row_id = 'tmp'.$i;
		
		

		
			$last_tr = $row_id;
		

		$linie = array(
			'tr_id'         					=> $row_id,
			'article'  			    			=> html_entity_decode($db->f('article')),
	    	'article_code'						=> $db->f('article_code'),
	    	'is_article_code'					=> $db->f('article_code') ? true : false,
			'article_id'							=> $db->f('article_id'),
			
			'is_article'      	      => $db->f('article_id') ? true : false,
			'is_article1'      	      => $db->f('article_id') ? true : false,
			
			
			'quantity'      					=> display_number($db->f('quantity')),
		
			
			'stock'				    				=> $stock,
			
			
			
			'content'						    => $db->f('content') ? true : false,
		
			'content_class'						=> $db->f('content') ? ' type_content ' : '',
	    	'line_total'							=> display_number($line_total),
	    	
		
		);
		//reset prices for duplicate order depending of new article prices and price category of the company
		
		array_push($o['tr_id'], $linie);
		$i++;

	}

	
	// ));

}
$default_note = $db->field("SELECT value FROM default_data WHERE type='p_order_note'");

$is_extra = false;
if($customer_id){
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");

	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<h4 >'.gm('Bank Details').'</h4>
	<p><strong>'.gm('Name').': </strong> <span>'.$c_details->f('bank_name').'</span></p>
	<p><strong>'.gm('BIC Code').': </strong> <span>'.$c_details->f('bank_bic_code').'</span></p>
	<p><strong>'.gm('IBAN').': </strong> <span>'.$c_details->f('bank_iban').'</span></p>
	';
	}
	if($extra_info){
		$is_extra = true;
		$extra_info = '<div style="width: 924px;" >'.$extra_info.'</div>';
	}
}

// $view->assign(array(
	$o['notes']                     = $in['notes'] ? $in['notes'] : ($default_note ? $default_note : '');
	$o['pick_date_format']          = pick_date_format(ACCOUNT_DATE_FORMAT);
	$o['style']                     = ACCOUNT_NUMBER_FORMAT;
	$o['page_title']			    = $page_title;
	$o['do_next']	    		    = $do_next;
	// 'total_currency_hide'		=> 'hide',
	$o['extra_info']				= $extra_info;
	$o['is_extra']					= $is_extra;
	
// ));

////////// multilanguage for order note ////////////////

$transl_lang_id_active 		= $in['languages'];
if(!$transl_lang_id_active){
	$transl_lang_id_active = $in['email_language'];
}
// console::log('lang_d2 '.$transl_lang_id_active);
$o['translate_loop'] = array();
$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
while($langs->next()){
	if(!$in['p_order_id'] || $in['p_order_id'] =='tmp'){
		if($langs->f('lang_id')==1){
			$note_type = 'p_order_note';
		}else{
			$note_type = 'p_order_note_'.$langs->f('lang_id');
		}
		$order_note = $db3->field("SELECT value FROM default_data
								   WHERE default_name = 'order note'
								   AND type = '".$note_type."' ");
		
	}else{
			$order_note = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'p_order'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['p_order_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

			$transl_lang_id_active = $db3->field("SELECT lang_id FROM note_fields
								   WHERE item_type 	= 'p_order'
								   AND 	item_name 	= 'notes'
								   AND 	active 		= '1'
								   AND 	item_id 	= '".$in['p_order_id']."' ");
			if($in['duplicate_order_id']){
				$transl_lang_id_active = $in['languages'];
			}
			


	}
	$translate_lang = 'form-language-'.$langs->f('code');
	if($langs->f('code')=='du'){
		$translate_lang = 'form-language-nl';
	}

	if($transl_lang_id_active != $langs->f('lang_id')){
		$translate_lang = $translate_lang.' hidden';
	}

	array_push($o['translate_loop'], array(
		'translate_cls' 	=> 		$translate_lang,
		'lang_id' 			=> 		$langs->f('lang_id'),
		'notes'				=> 		$order_note,
	));

}
$transl_lang_id_active_custom 		= $in['languages'];
if(!$transl_lang_id_active_custom){
	$transl_lang_id_active_custom = $in['email_language'];
}
$o['translate_loop_custom'] = array();


$in['title'] = 1;
// console::log('lang_d '.$transl_lang_id_active);
$o['translate_lang_active']							= 'form-language-'.$code;
$o['language_dd'] 									= build_language_dd_new($transl_lang_id_active);
$o['email_language'] 								= $transl_lang_id_active;
$o['langs'] 										= $transl_lang_id_active;

$o['nr_decimals'] 									= ARTICLE_PRICE_COMMA_DIGITS;
$o['cancel_link']									= $cancel_link;
$o['language_txt']									= gm($lang_code);
$o['style']  										= ACCOUNT_NUMBER_FORMAT;



$o['customers']										= get_customers($in,true,false);
$o['addresses']										= get_addresses($in,true,false);
$o['cc']											= get_cc($in,true,false);
$o['articles_list']									= get_articles_list($in,true,false);
$o['country_dd']									= build_country_list();
$o['main_country_id']								= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';

json_out($o);


function get_accmanager($in,$showin=true,$exit=true){

	$q = strtolower($in["term"]);

	global $database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($database_users);

	$filter = '';

	if($q){
	  $filter .=" AND users.first_name LIKE '".$q."%'";
	}

	$db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

	$items = array();
	while($db_users->move_next()){
		array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>$db_users->f('first_name').' '.$db_users->f('last_name') ) );
	}
	return json_out($items, $showin,$exit);

}


function  get_notes($in,$showin=true,$exit=true){
	$db=new sqldb();
	$o = array();
	$transl_lang_id_active 		= $in['languages'];
	if(!$transl_lang_id_active){
		$transl_lang_id_active = $in['email_language'];
	}
	$o['translate_loop'] = array();
	$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
	while($langs->next()){
		if(!$in['p_order_id'] ){
			if($langs->f('lang_id')==1){
				$note_type = 'p_order_note';
			}else{
				$note_type = 'p_order_note_'.$langs->f('lang_id');
			}
			$order_note = $db->field("SELECT value FROM default_data
									   WHERE default_name = 'order note'
									   AND type = '".$note_type."' ");
			
		}else{
				$order_note = $db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'p_order'
									   AND 	item_name 	= 'notes'
									   AND 	item_id 	= '".$in['p_order_id']."'
									   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

				/*$transl_lang_id_active = $db->field("SELECT lang_id FROM note_fields
									   WHERE item_type 	= 'order'
									   AND 	item_name 	= 'notes'
									   AND 	active 		= '1'
									   AND 	item_id 	= '".$in['order_id']."' ");*/
				
				
			
		}
		$translate_lang = 'form-language-'.$langs->f('code');
		if($langs->f('code')=='du'){
			$translate_lang = 'form-language-nl';
		}

		if($transl_lang_id_active != $langs->f('lang_id')){
			$translate_lang = $translate_lang.' hidden';
		}

		array_push($o['translate_loop'], array(
			'translate_cls' 	=> 		$translate_lang,
			'lang_id' 			=> 		$langs->f('lang_id'),
			'notes'				=> 		$order_note,
			));

	}
	$transl_lang_id_active_custom 		= $in['languages'];
	if(!$transl_lang_id_active_custom){
		$transl_lang_id_active_custom = $in['email_language'];
	}
	$o['translate_loop_custom'] = array();



	
	$in['title'] = 1;

	$o['translate_lang_active']				= 'form-language-'.$code;
	$o['language_txt']								= gm($lang_code);

	return json_out($o, $showin,$exit);
}
function get_customers($in,$showin=true,$exit=true){
	
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" AND is_admin='0' AND is_supplier=1 ";

	if($q){
		$filter .=" AND name LIKE '%".addslashes($q)."%'";
	}

	if($is_admin){
		$filter =" AND is_admin='".$is_admin."' ";
	}

	if($in['current_id']){
		$filter .= " AND customer_id !='".$in['current_id']."'";
	}
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 $filter GROUP BY customers.customer_id ORDER BY customers.name limit 5 ")->getAll();

	# foreach e 2x mai rapid decat while-ul
	$result = array();

	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
		
			$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
		$result[]=array(
			"id"					=> $value['customer_id'],
			'symbol'				=> $symbol,
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name'],

		);
		
	}
	$added = false;
	if(count($result)==5){
		array_push($result,array('id'=>'99999999999'));
		$added = true;
	}
	
	if($in['buyer_id']){
		$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 AND customer_id='".$in['buyer_id']."' GROUP BY customers.customer_id ORDER BY customers.name ")->getAll();
		$value = $cust[0];
		$cname = trim($value['name']);
		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);
	}
	
	if(!$added){
		array_push($result,array('id'=>'99999999999'));		
	}
	return json_out($result, $showin,$exit);

}
function get_cc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	
	$db_user = new sqldb($database_users);
	$filter =" is_admin='0' AND customers.active=1";
	// $filter_contact = ' 1=1 ';
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
		// $filter_contact .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
	}

	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
		FROM customers
		WHERE $filter
		ORDER BY name
		LIMIT 5")->getAll();

	$result = array();
	$result[0]=array(
			"id"					=> '0-0',
			'symbol'                =>'',
			"label"					=> 'Own Location',
			"value" 				=> 'Own Location',
			"top"	 				=> 'Own Location',
			"bottom"				=> '',
			"right"					=> ''
			
		);
	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}

		$result[]=array(
			"id"					=> $value['customer_id'].'-'.$value['contact_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"symbol"				=> $symbol,
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id" 			=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
			'country'				=> $value['country_name'] ? $value['country_name'] : '',
			'zip'					=> $value['zip_name'] ? $value['zip_name'] : '',
			'city'					=> $value['city_name'] ? $value['city_name'] : '',
			"bottom"				=> $value['zip_name'].' '.$value['city'].' '.$value['country_name'],
			"right"					=> $value['acc_manager_name']
		);
		
	}
	if($q){
		array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	}else{
		array_push($result,array('id'=>'99999999999','value'=>''));
	}
	array_push($result,array('id'=>'99999999999','value'=>''));
	
	return json_out($result, $showin,$exit);

}
function get_to_addresses($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	if($q){
		$filter .=" AND (customer_addresses.address LIKE '%".addslashes($q)."%' OR customer_addresses.zip LIKE '%".addslashes($q)."%' OR customer_addresses.city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
	}
	
	// array_push($result,array('id'=>'99999999999','value'=>''));
	
	if($in['to_buyer_id']!=0){
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
								 FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
								 LEFT JOIN state ON state.state_id=customer_addresses.state_id
								 WHERE customer_addresses.customer_id='".$in['to_buyer_id']."'
								 ORDER BY customer_addresses.address_id limit 5");
	}elseif($in['to_buyer_id']==0){

		$address= $db->query("SELECT * FROM  dispatch_stock_address where customer_id='0' ORDER BY is_default DESC");

	}
	
	
	$addresses=array();
	if($address){
		
		while($address->next()){
		  	$a = array(
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> $in['to_buyer_id']?nl2br($address->f('address')):$address->f('naming'),
			  	'top'				=> $in['to_buyer_id']?nl2br($address->f('address')):$address->f('naming'),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
		}
	}
	
	$added = false;

	if(count($addresses)==5){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
		}
		$added = true;
	}
	/*if($in['delivery_address_id']){
		if($in['field'] == 'customer_id'){
			$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['customer_id']."' AND customer_addresses.address_id='".$in['delivery_address_id']."'
									 ORDER BY customer_addresses.address_id ");
		}
		
		$a = array(
		  		'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> nl2br($address->f('address')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
	}*/
	if(!$added){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
		}
	}
	
	return json_out($addresses, $showin,$exit);

}

function get_addresses($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	if($q){
		$filter .=" AND (customer_addresses.address LIKE '%".addslashes($q)."%' OR customer_addresses.zip LIKE '%".addslashes($q)."%' OR customer_addresses.city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
	}
	
	// array_push($result,array('id'=>'99999999999','value'=>''));
	
	if($in['field'] == 'customer_id' && $in['customer_id']!=0){
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
								 FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
								 LEFT JOIN state ON state.state_id=customer_addresses.state_id
								 WHERE customer_addresses.customer_id='".$in['customer_id']."'
								 ORDER BY customer_addresses.address_id limit 5");
	}elseif($in['customer_id']==0){

		$address= $db->query("SELECT * FROM  dispatch_stock_address where customer_id='0' ORDER BY is_default DESC");

	}
	
	
	$addresses=array();
	if($address){
		
		while($address->next()){
		  	$a = array(
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> $in['customer_id']?nl2br($address->f('address')):$address->f('naming'),
			  	'top'				=> $in['customer_id']?nl2br($address->f('address')):$address->f('naming'),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
		}
	}
	
	$added = false;

	if(count($addresses)==5){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
		}
		$added = true;
	}
	/*if($in['delivery_address_id']){
		if($in['field'] == 'customer_id'){
			$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['customer_id']."' AND customer_addresses.address_id='".$in['delivery_address_id']."'
									 ORDER BY customer_addresses.address_id ");
		}
		
		$a = array(
		  		'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> nl2br($address->f('address')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
	}*/
	if(!$added){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
		}
	}
	
	return json_out($addresses, $showin,$exit);

}
function get_articles_list(&$in,$showin=true,$exit=true){
	$in['lang_id'] = $in['languages'];
	$in['cat_id'] = $in['price_category_id'];
	$in['from_order_page'] = true;
	// return false;
	return ark::run('order-addArticle');
}