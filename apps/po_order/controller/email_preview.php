<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $config;

if(defined('ACCOUNT_DATE_FORMAT') && ACCOUNT_DATE_FORMAT!=''){
    $date_format = ACCOUNT_DATE_FORMAT;
  }else{
    $date_format ='m/d/Y';
  }

$in['message'] = stripslashes($in['message']);
$p_order = $db->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");
$contact = $db->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$p_order->f('contact_id')."'");
$title_cont = $db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
$customer = $db->field("SELECT name FROM customers WHERE customer_id='".$p_order->f('customer_id')."'");
if (!empty($ref = $p_order->f('buyer_ref'))) {
    $serial_number = $ref.$p_order->f('serial_number');
} else {
    $serial_number = $p_order->f('serial_number');
}
$in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_ORDER."\" alt=\"\">",$in['message']);
$in['message']=str_replace('[!CONTACT_FIRST_NAME!]',"".$contact->f('firstname')."",$in['message']);
$in['message']=str_replace('[!CONTACT_LAST_NAME!]',"".$contact->f('lastname')."",$in['message']);
$in['message']=str_replace('[!SALUTATION!]',"".$title_cont."",$in['message']);
$in['message']=str_replace('[!SERIAL_NUMBER!]',"".$serial_number."",$in['message']);
$in['message']=str_replace('[!DATE!]',"".date($date_format, $p_order->f('date'))."",$in['message']);
$in['message']=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$in['message']);
$in['message']=str_replace('[!DUE_DATE!]',"".date($date_format, $p_order->f('del_date'))."",$in['message']);
$in['message']=str_replace('[!YOUR_REFERENCE!]',"".$p_order->f('your_ref')."",$in['message']);

$result = array(
  'e_message' => ($in['send_email'] ? $in['message'] : nl2br($in['message']))
);
json_out($result);
?>