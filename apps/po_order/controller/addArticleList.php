
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}


$l_r =10;

$order_by_array = array('item_code','internal_name','article_category','supplier_reference','stock');

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$def_lang = DEFAULT_LANG_ID;
if($in['lang_id']){
	$def_lang= $in['lang_id'];
}
if($in['lang_id']>=1000) {
	$def_lang = DEFAULT_LANG_ID;
}

switch ($def_lang) {
	case '1':
		$text = gm('Name');
		break;
	case '2':
		$text = gm('Name fr');
		break;
	case '3':
		$text = gm('Name du');
		break;
	default:
		$text = gm('Name');
		break;
}

$cat_id = $in['cat_id'];
$filter.="1=1";
if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
    $filter.=' AND pim_article_variants.article_id IS NULL ';
}
if(!$in['from_address_id']) {
	$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id  AND pim_article_prices.base_price=1

						   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
					 AND pim_articles_lang.lang_id=\''.$def_lang.'\'	  
					 LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id';
	if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
    $table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
  }
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
				pim_article_prices.price, pim_articles.internal_name,
				pim_articles_lang.description AS description,
				pim_articles_lang.name2 AS item_name2,
				pim_articles_lang.name AS item_name,
				pim_articles_lang.lang_id,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference,
				pim_article_categories.name AS article_category,
				pim_articles.weight,
        pim_articles.has_variants';

}else{
	$table = 'pim_articles  LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=1
	                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
					   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
					   LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id';
if(defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1){
    $table.=' LEFT JOIN pim_article_variants ON pim_articles.article_id=pim_article_variants.article_id ';
  }
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,
				pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

				pim_articles_lang.description AS description,
				pim_articles_lang.name2 AS item_name2,
				pim_articles_lang.name AS item_name,
				pim_articles_lang.lang_id,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference,
				pim_article_categories.name AS article_category,
				pim_articles.weight,
        pim_articles.has_variants';
}

//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

if ($in['search'])
{
	//$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_articles.supplier_reference LIKE '%".$in['search']."%' OR pim_article_categories.name LIKE '%".$in['search']."%')";
	// $arguments.="&search=".$in['search'];

	$search_words = explode(" ", $in['search']);
    $query_string2 ='';
    $all_tcl=1;
    $tcl=0;
    $len = count($search_words);

    for ($s = 0; $s < $len; $s++) {
        if ($search_words[$s]) {
/*            $query_string8 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.internal_name ";
            $query_string9 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.item_code ";
            $query_string10 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.supplier_reference ";
            $query_string11 .= "LIKE '%" . $search_words[$s] . "%' OR pim_article_categories.name ";*/

            if(strlen($search_words[$s])>3){
              $all_tcl=0;
            }else{
              $tcl=1;
            }
/*            if($len>1 && $s>0){
                  $query_string4 .= "+".$search_words[$s]." "; 
            }else{*/
              $query_string4 .= "+".$search_words[$s]."* "; 
              $query_string5 .= "+".$search_words[$s]." "; 
              if($s==0){
                $query_string6 .= "+".$search_words[$s]." "; 
              }else{
                $query_string6 .= "+".$search_words[$s]."* ";
              }
            
           /* }*/
        }
    }
    $query_string7 =SUBSTR($query_string4, 0, STRLEN($query_string4) - 2);

    if($all_tcl || $tcl ){
        $size = $len - 1;

        if($size){
            $perm = range(0, $size);
            $j = 0;

            do { 
                 foreach ($perm as $i) { $perms[$j][] = $search_words[$i]; }
            } while ($perm = pc_next_permutation($perm, $size) and ++$j);

            foreach ($perms as $p) {
                $query_string1 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.item_code ";
                $query_string2 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.internal_name ";
                $query_string3 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.supplier_reference ";
                $query_string33 .= "LIKE '%" . join(' ', $p) . "%' OR pim_article_categories.name ";
            }

            $query_string1 = SUBSTR($query_string1, 0, STRLEN($query_string1) - 27);
            $query_string2 = SUBSTR($query_string2, 0, STRLEN($query_string2) - 31);
            $query_string3 = SUBSTR($query_string3, 0, STRLEN($query_string3) - 36);
            $query_string33 = SUBSTR($query_string33, 0, STRLEN($query_string33) - 32);

        }else{
                $query_string1 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string2 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string3 .= "LIKE '%" . $search_words[0]. "%' ";
                $query_string33 .= "LIKE '%" . $search_words[0]. "%' ";

        }

        $query_string8 =SUBSTR($query_string8, 0, STRLEN($query_string8) - 31);
        $query_string9 =SUBSTR($query_string9, 0, STRLEN($query_string9) - 27);
        $query_string10 =SUBSTR($query_string10, 0, STRLEN($query_string10) - 36);
        $query_string11 =SUBSTR($query_string11, 0, STRLEN($query_string11) - 32);
        
    }
       
    //var_dump($filter);exit();
    if ($len>1){
      if($all_tcl || $tcl){ 
       /* if($tcl){
          $filter .= " AND ( pim_articles.item_code ".$query_string9."  OR pim_articles.internal_name ".$query_string8." OR pim_articles.supplier_reference ".$query_string10." OR pim_article_categories.name ".$query_string11."  ) ";
        }else{*/
          $filter .= " AND ( pim_articles.item_code ".$query_string1."  OR pim_articles.internal_name ".$query_string2." OR pim_articles.supplier_reference ".$query_string3." OR pim_article_categories.name ".$query_string33." ) ";
       /* }*/
        
      }else{
        $filter .= " AND (match(pim_articles.internal_name) against ('".$query_string4."' in boolean mode)
                    OR match(pim_articles.internal_name) against ('".$query_string5."' in boolean mode)
                    OR match(pim_articles.internal_name) against ('".$query_string6."' in boolean mode)   
                    OR match(pim_articles.internal_name) against ('".$query_string7."' in boolean mode)    
                    OR match(pim_articles.item_code) against ('".$query_string4."' in boolean mode)
                    OR match(pim_articles.supplier_reference) against ('".$query_string4."' in boolean mode) 
                    OR match(pim_article_categories.name) against ('".$query_string4."' in boolean mode)
                    OR match(pim_article_categories.name) against ('".$query_string5."' in boolean mode)
                    OR match(pim_article_categories.name) against ('".$query_string6."' in boolean mode)   
                    OR match(pim_article_categories.name) against ('".$query_string7."' in boolean mode)  
                           )";
      }
       
    }else{
        $filter .= " AND (pim_articles.internal_name LIKE '%".rtrim(ltrim($in['search']))."%'
                      OR pim_articles.item_code LIKE '%".rtrim(ltrim($in['search']))."%'
                      OR pim_articles.supplier_reference like '%" . rtrim(ltrim($in['search'])) . "%'
                      OR pim_article_categories.name like '%" . rtrim(ltrim($in['search'])) . "%' )"; 
    }
}
if ($in['hide_article_ids'])
{
	$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
	// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
}
// if ($in['lang_id'])
// {

// 	$arguments.="&lang_id=".$in['lang_id'];
// }
// if ($in['is_purchase_order'])
// {

	// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
// }
if ($in['show_stock'])
{
	$filter.=" AND pim_articles.hide_stock=0";
	// $arguments.="&show_stock=".$in['show_stock'];
}
if ($in['from_customer_id1'])
{
	$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
	// $arguments.="&from_customer_id=".$in['from_customer_id'];
}
if ($in['from_address_id1'])
{
	$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
	// $arguments.="&from_address_id=".$in['from_address_id'];
}

if($in['article_category_id']){
    $filter.=" AND pim_articles.article_category_id='".$in['article_category_id']."' ";
   // $arguments.="&article_category_id=".$in['article_category_id'];
}

$order_by=" ORDER BY pim_articles.item_code ";
if($in['order_by']){
    if(in_array($in['order_by'], $order_by_array)){
      $order = " ASC ";
      if($in['desc'] == '1' || $in['desc']=='true'){
          $order = " DESC ";
      }
      $order_by =" ORDER BY ".$in['order_by']." ".$order;
    }
}


$articles= array( 'lines' => array());
$articles['max_rows']= (int)$db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");


$article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1'  ".$order_by."  LIMIT ".$offset*$l_r.",".$l_r);


	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='P_ORDER_FIELD_LABEL'");


$time = time();

$j=0;
while($article->next()){
	$vat = $db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");

	$values = $article->next_array();
	$values['weight'] = display_number($values['weight']);

	$tags = array_map(function($field){
		return '/\[\!'.strtoupper($field).'\!\]/';
	},array_keys($values));

	$label = preg_replace($tags, $values, $fieldFormat);
	
  	$base_price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

  
 	$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");
  	$code = $article->f('item_code');


	if($in['customer_id']){
      $supplier_article = $db->query("SELECT purchasing_price, supplier_ref FROM pim_article_references WHERE article_id='".$article->f('article_id')."' AND supplier_id='".$in['customer_id']."' ");
      if($supplier_article->move_next()){
        if($supplier_article->f('purchasing_price')>0){
	      		$purchase_price = $supplier_article->f('purchasing_price');
	      	}
        if($supplier_article->f('supplier_ref')){
      		$code = $supplier_article->f('supplier_ref');
      	} 
      }
    }
	$base_price = $purchase_price;
	$price = $purchase_price;

	$ant_stock=0;
  
   
	//items on purchase
	    $items_order=$db->field("SELECT SUM(pim_p_order_articles.quantity) 
		                      FROM  pim_p_order_articles 
		                      INNER JOIN pim_p_orders ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
		                      WHERE pim_p_order_articles.article_id='".$article->f('article_id')."'
		                      AND pim_p_orders.rdy_invoice!=0");

	    $items_received=$db->field("SELECT SUM(pim_p_orders_delivery.quantity) 
		                         FROM   pim_p_orders_delivery 
		                         INNER JOIN pim_p_order_articles ON pim_p_order_articles.order_articles_id=pim_p_orders_delivery.order_articles_id
		                         WHERE pim_p_order_articles.article_id='".$article->f('article_id')."'");
	//items on orders	
	     $items_on_order=$db->field("SELECT SUM(pim_order_articles.quantity) 
		                      FROM  pim_order_articles 
		                      INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
		                      WHERE pim_order_articles.article_id='".$article->f('article_id')."'
		                      AND pim_orders.sent=1");

	    $items_on_received=$db->field("SELECT SUM(pim_orders_delivery.quantity) 
		                         FROM   pim_orders_delivery 
		                         INNER JOIN pim_order_articles ON pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
		                         WHERE pim_order_articles.article_id='".$article->f('article_id')."'");

	//items on project
	     $items_on_project=$db->field("SELECT SUM(project_articles.quantity) 
		                      FROM  project_articles 
		                      INNER JOIN  projects ON  projects.project_id=project_articles.project_id
		                      WHERE project_articles.article_id='".$article->f('article_id')."'
		                      AND projects.stage!=0 AND project_articles.delivered=0");

	//items on intervetions
	     $items_on_intervetion=$db->field("SELECT SUM( servicing_support_articles.quantity) 
		                      FROM   servicing_support_articles 
		                      INNER JOIN  servicing_support ON  servicing_support.service_id=servicing_support_articles.service_id
		                      WHERE servicing_support_articles.article_id='".$article->f('article_id')."'
		                      AND servicing_support.status!=0 AND servicing_support_articles.article_delivered=0");
	$ant_stock=$article->f('stock')-($items_on_order-$items_on_received+$items_on_project+$items_on_intervetion ) + ($items_order-$items_received);

	$linie = array(
	  	'article_id'				=> $article->f('article_id'),
	  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
	  	'name'						=> $article->f('internal_name'),
	  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
	  	'article_category'			=> $article->f('article_category'),
	  	'weight'					=> display_number($article->f('weight')),
	    'stock'						=> $article->f('stock'),
	    //'stock2'					=> remove_zero_decimals($article->f('stock')),
	    'stock2'					=> $article->f('hide_stock')==1 || $article->f('is_service') == 1? '' : remove_zero_decimals_dn(display_number($article->f('stock')*1)),
	    'ant_stock'					=> $article->f('hide_stock')==1 || $article->f('is_service') == 1? '' : '('.remove_zero_decimals_dn(display_number($ant_stock)).')',
	    'quantity'		    		=> 1,
	    'pending_articles'  		=> intval($pending_articles),
	    'threshold_value'   		=> $article->f('article_threshold_value'),
	  	'sale_unit'					=> $article->f('sale_unit'),
	  	'percent'           		=> $vat_percent,
		'percent_x'         		=> display_number($vat_percent),
	    'packing'					=> $article->f('packing')>0 ? remove_zero_decimals($article->f('packing')) :1,
	  	'code'		  	    		=> $code,
	  	'supplier_reference'	    => defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? '' : $article->f('supplier_reference'),
	  	'supplier_reference_x'	   	=> $article->f('supplier_reference'),
		'price'						=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price,
		'price_vat'					=> $in['remove_vat'] == 1 ? (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0 : $price)  : (defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0: $price + (($price*$vat)/100)) ,
		'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
		'purchase_price'			=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? 0: $purchase_price,
		'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
		'quoteformat'    			=> gfn($label),
		'base_price'				=> defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? place_currency(display_number_var_dec(0)) : place_currency(display_number_var_dec($base_price)),
		'show_stock'				=> $article->f('hide_stock') ? false:true,
		'hide_stock'				=> $article->f('hide_stock'),
		'is_service'				=> $article->f('is_service'),
		'allow_stock'               =>ALLOW_STOCK == 1 ? true : false,
		'has_variants'        => defined('ALLOW_ARTICLE_VARIANTS') && ALLOW_ARTICLE_VARIANTS==1 && $article->f('has_variants') ? true : false,
    'has_variants_done'   => 0,
         'is_variant_for'       => 0,
         'variant_type'       => 0,
          'stock_pack'                    => $article->f('hide_stock')==1 || $article->f('is_service') == 1? '' : remove_zero_decimals_dn(display_number($article->f('stock')/$article->f('packing'))),

	);
	array_push($articles['lines'], $linie);

}

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$articles['families']=array();
while($db->move_next()){
    $families = array(
    'name'  => $db->f('name'),
    'id'=> $db->f('id')
    );
      
    array_push($articles['families'], $families);
}

$articles['customer_id'] 		= $in['customer_id'];
$articles['lang_id'] 				= $in['lang_id'];
$articles['cat_id'] 				= $in['cat_id'];
$articles['txt_name']			  = $text;
$articles['lr']			  = $l_r;
$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;
$articles['offset']         = $offset;
$articles['article_category_id']         = $in['article_category_id'];
$articles['allow_stock_packing']        = ALLOW_ARTICLE_PACKING == '1' ? true : false;


if($in['from_order_page']===true){
	return json_out($articles,true,false);
}
json_out($articles);

