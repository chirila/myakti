<?php

class po_order {

    function __construct() {
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $this->db = new sqldb();
        $this->dbu = new sqldb();
    $this->db2 = new sqldb();
    $this->db_users = new sqldb($db_config);
    $this->db_users2 = new sqldb($db_config);
    $this->field_n = 'p_order_id';
    } 

 
 function start_to_order(&$in)
  {
    $in['debug'] = true;
    $in['same_order_time'] = time();
    json_out($in);
  }
   function to_order(&$in)
  {
    unset($_SESSION['articles_id']);
   $in['actiune']=array(); 
   $in['serial_display']=array();    
   $in['order_ts'] = time();
   $in['supplier_ids']=array();

    $article_ids="";
    foreach($in['selected_articles'] as $article_id => $supplier_id){
      $article_ids.="'".$article_id."',";
    }
    $article_ids=rtrim($article_ids,",");
    $i = 0;

   switch ($in['type']) {
     case 'orders':
        $in['same_order'] = $in['same_time'];
        $in['order_id']=$in['item'];

        /*$query = $this->dbu->query("SELECT pim_order_articles.article_id,pim_order_articles.packing,pim_order_articles.sale_unit,pim_articles.supplier_id,pim_orders.serial_number
                           FROM pim_order_articles
                           INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id 
                                     INNER JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id 
                           WHERE pim_order_articles.order_id='".$in['item']."'");
        while ( $query->next()) {
              if($query->f('supplier_id')){
                   $in['our_ref']=$query->f('serial_number');
                   array_push($in['supplier_ids'],$query->f('supplier_id'));
                     $i = 0;

                     $query_line = $this->dbu->query("SELECT pim_order_articles.*, pim_articles.supplier_reference 
                                   FROM pim_order_articles
                                             INNER JOIN pim_articles on  pim_articles.article_id=pim_order_articles.article_id
                                             WHERE pim_order_articles.order_id='".$in['item']."' 
                                             AND pim_order_articles.article_id!='0'  AND pim_articles.supplier_id='".$query->f('supplier_id')."' 
                                             ORDER BY pim_order_articles.sort_order ASC ");
            
                    while ($query_line->next()) {

                          $in['items'][$query->f('supplier_id')][$i] = 'tmp'.$i;
                          $in['from_type'][$query->f('supplier_id')][$i] = 'order';
                          $in['from_id'][$query->f('supplier_id')][$i] = $in['item'];
                          $in['article_id'][$query->f('supplier_id')][$i] = $query_line->f('article_id');
                          $in['article_code'][$query->f('supplier_id')][$i] = $query_line->f('article_code');
                          $in['article'][$query->f('supplier_id')][$i] = $query_line->f('article');
                          $in['quantity'][$query->f('supplier_id')][$i] = display_number($query_line->f('quantity')*($query_line->f('packing')/$query_line->f('sale_unit')));
                          $in['price'][$query->f('supplier_id')][$i] = display_number($this->db->field("SELECT purchase_price FROM pim_article_prices WHERE base_price='1' and article_id='".$query_line->f('article_id')."'"));
                          $in['supplier_reference'][$query->f('supplier_id')][$i] = $query_line->f('supplier_reference');
                          $in['is_article'][$query->f('supplier_id')][$i] = true;
                          $in['tax_id'][$query->f('supplier_id')][$i] =  0;

                           $i++;

                           //get taxes that are applied to purchase orders
                              $taxes=$this->dbu->query("SELECT pim_articles_taxes.*, pim_article_tax.*, pim_article_tax_type.name FROM pim_articles_taxes 
                                  LEFT JOIN pim_article_tax ON pim_articles_taxes.tax_id = pim_article_tax.tax_id
                                  LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
                                WHERE pim_articles_taxes.article_id ='".$query_line->f('article_id')."' AND (pim_article_tax.apply_to ='0' OR pim_article_tax.apply_to ='2') ");
                            $j=0;
                            while($taxes->move_next()){

                                $in['items'][$query->f('supplier_id')][$i] = 'tmp'.$i;
                                $in['from_type'][$query->f('supplier_id')][$i] = 'order';
                                $in['from_id'][$query->f('supplier_id')][$i] = $in['item'];
                                $in['article_id'][$query->f('supplier_id')][$i] = $query_line->f('article_id');
                                $in['article_code'][$query->f('supplier_id')][$i] = $taxes->f("name");
                                $in['article'][$query->f('supplier_id')][$i] = $taxes->f("code");
                                $in['quantity'][$query->f('supplier_id')][$i] = display_number($query_line->f('quantity')*($query_line->f('packing')/$query_line->f('sale_unit')));
                                $in['price'][$query->f('supplier_id')][$i] = display_number($taxes->f("amount"));
                                $in['supplier_reference'][$query->f('supplier_id')][$i] = '';
                                $in['is_article'][$query->f('supplier_id')][$i] = false;
                                $in['tax_id'][$query->f('supplier_id')][$i] =  $taxes->f("tax_id");
                         
                                 $i++;
                             
                            }
                      }
                     
                 }
          }*/

        $query_line = $this->dbu->query("SELECT pim_order_articles.*, pim_articles.supplier_reference,pim_orders.serial_number 
           FROM pim_order_articles
            INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
             INNER JOIN pim_articles on  pim_articles.article_id=pim_order_articles.article_id
             WHERE pim_order_articles.order_id='".$in['item']."' 
             AND pim_order_articles.article_id!='0'  AND pim_order_articles.article_id IN (".$article_ids.") 
             ORDER BY pim_order_articles.sort_order ASC ");

        while ($query_line->next()) {

          $in['our_ref']=$query_line->f('serial_number');

          if(!in_array($in['selected_articles'][$query_line->f('article_id')], $in['supplier_ids'])){
            array_push($in['supplier_ids'],$in['selected_articles'][$query_line->f('article_id')]);     
          }

          $in['items'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'tmp'.$i;
          $in['from_type'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'order';
          $in['from_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $in['item'];
          $in['article_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('article_id');
          $in['article_code'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('article_code');
          $in['article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('article');
          $in['quantity'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($query_line->f('quantity')*($query_line->f('packing')/$query_line->f('sale_unit')));
          $in['price'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($this->db->field("SELECT purchase_price FROM pim_article_prices WHERE base_price='1' and article_id='".$query_line->f('article_id')."'"));
          $in['supplier_reference'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('supplier_reference');
          $in['is_article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = true;
          $in['tax_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] =  0;

          $i++;

          //get taxes that are applied to purchase orders
          $taxes=$this->dbu->query("SELECT pim_articles_taxes.*, pim_article_tax.*, pim_article_tax_type.name FROM pim_articles_taxes 
                LEFT JOIN pim_article_tax ON pim_articles_taxes.tax_id = pim_article_tax.tax_id
                LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
              WHERE pim_articles_taxes.article_id ='".$query_line->f('article_id')."' AND (pim_article_tax.apply_to ='0' OR pim_article_tax.apply_to ='2') ");

          while($taxes->move_next()){
              $in['items'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'tmp'.$i;
              $in['from_type'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'order';
              $in['from_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $in['item'];
              $in['article_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('article_id');
              $in['article_code'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $taxes->f("name");
              $in['article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $taxes->f("code");
              $in['quantity'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($query_line->f('quantity')*($query_line->f('packing')/$query_line->f('sale_unit')));
              $in['price'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($taxes->f("amount"));
              $in['supplier_reference'][$in['selected_articles'][$query_line->f('article_id')]][$i] = '';
              $in['is_article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = false;
              $in['tax_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] =  $taxes->f("tax_id");
       
               $i++;
           
          }

        }
        
        //we update the order as purchase to remove from to order list if there are no articles for creating a purchase order
        $query_line_diff = $this->dbu->query("SELECT pim_order_articles.*, pim_articles.supplier_reference,pim_orders.serial_number 
           FROM pim_order_articles
            INNER JOIN pim_orders ON pim_orders.order_id=pim_order_articles.order_id
             INNER JOIN pim_articles on  pim_articles.article_id=pim_order_articles.article_id
             WHERE pim_order_articles.order_id='".$in['item']."' 
             AND pim_order_articles.article_id!='0'  AND pim_order_articles.article_id NOT IN (".$article_ids.") 
             ORDER BY pim_order_articles.sort_order ASC ")->getAll();
        if(empty($query_line_diff)){
          $this->dbu->query("UPDATE pim_orders SET is_purchase='1' WHERE pim_orders.order_id='".$in['item']."'");
        }

      break;
      case 'projects':
        $in['same_order'] = $in['same_time'];
        $in['project_id']=$in['item'];

        /*$query = $this->dbu->query("SELECT project_articles.article_id,pim_articles.supplier_id,projects.serial_number
                                   FROM project_articles
                                   INNER JOIN projects ON projects.project_id=project_articles.project_id 
                                     INNER JOIN pim_articles ON pim_articles.article_id=project_articles.article_id 
                                   WHERE project_articles.project_id='".$in['item']."'");
        while ( $query->next()) {
          if($query->f('supplier_id')){
              $in['our_ref']=$query->f('serial_number');
               array_push($in['supplier_ids'],$query->f('supplier_id'));
              
             $query_line = $this->dbu->query("SELECT project_articles.* ,pim_articles.item_code
                                       FROM project_articles
                                         INNER JOIN pim_articles on  pim_articles.article_id=project_articles.article_id
                                         WHERE project_articles.project_id='".$in['item']."' 
                                         AND project_articles.article_id!='0'  AND pim_articles.supplier_id='".$query->f('supplier_id')."' 
                                          ");
        
            while ($query_line->next()) {
                  $in['items'][$query->f('supplier_id')][$i] = 'tmp'.$i;
                  $in['from_type'][$query->f('supplier_id')][$i] = 'project';
                    $in['from_id'][$query->f('supplier_id')][$i] = $in['item'];
                  $in['article_id'][$query->f('supplier_id')][$i] = $query_line->f('article_id');
                  $in['article_code'][$query->f('supplier_id')][$i] = $query_line->f('item_code');
                  $in['article'][$query->f('supplier_id')][$i] = $query_line->f('name');
                  $in['quantity'][$query->f('supplier_id')][$i] = display_number($query_line->f('quantity'));
                   $in['price'][$query->f('supplier_id')][$i] = display_number($this->db->field("SELECT purchase_price FROM pim_article_prices WHERE base_price='1' and article_id='".$query_line->f('article_id')."'"));
                   $in['is_article'][$query->f('supplier_id')][$i] = true;
                   $in['tax_id'][$query->f('supplier_id')][$i] =  0;

          
         
               $i++;
              }
                 
             }
        }*/

        $query_line = $this->dbu->query("SELECT project_articles.* ,pim_articles.item_code,projects.serial_number
           FROM project_articles
            INNER JOIN projects ON projects.project_id=project_articles.project_id
             INNER JOIN pim_articles on  pim_articles.article_id=project_articles.article_id
             WHERE project_articles.project_id='".$in['item']."' 
             AND project_articles.article_id!='0' AND project_articles.article_id IN (".$article_ids.") ");
        while ($query_line->next()) {

          $in['our_ref']=$query_line->f('serial_number');

          if(!in_array($in['selected_articles'][$query_line->f('article_id')], $in['supplier_ids'])){
            array_push($in['supplier_ids'],$in['selected_articles'][$query_line->f('article_id')]);     
          }

          $in['items'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'tmp'.$i;
          $in['from_type'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'project';
            $in['from_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $in['item'];
          $in['article_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('article_id');
          $in['article_code'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('item_code');
          $in['article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('name');
          $in['quantity'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($query_line->f('quantity'));
           $in['price'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($this->db->field("SELECT purchase_price FROM pim_article_prices WHERE base_price='1' and article_id='".$query_line->f('article_id')."'"));
           $in['is_article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = true;
           $in['tax_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] =  0;

          $i++;
        }
        
        //we update the project as purchase to remove from to order list if there are no articles for creating a purchase order
        $query_line_diff = $this->dbu->query("SELECT project_articles.* ,pim_articles.item_code,projects.serial_number
           FROM project_articles
            INNER JOIN projects ON projects.project_id=project_articles.project_id
             INNER JOIN pim_articles on  pim_articles.article_id=project_articles.article_id
             WHERE project_articles.project_id='".$in['item']."' 
             AND project_articles.article_id!='0'  AND project_articles.article_id NOT IN (".$article_ids.") ")->getAll();
        if(empty($query_line_diff)){
          $this->dbu->query("UPDATE  projects SET is_purchase='1' WHERE project_id='".$in['item']."'");
        }
      
      break;
      case 'interventions':
        $in['same_order'] = $in['same_time'];
        $in['service_id']=$in['item'];

        /*$query = $this->dbu->query("SELECT servicing_support_articles.article_id,pim_articles.supplier_id,servicing_support.serial_number
                                   FROM servicing_support_articles
                                   INNER JOIN servicing_support ON servicing_support.service_id=servicing_support_articles.service_id 
                                     INNER JOIN pim_articles ON pim_articles.article_id=servicing_support_articles.article_id 
                                   WHERE servicing_support_articles.service_id='".$in['item']."' AND servicing_support_articles.article_delivered='0'");
        while ( $query->next()) {
          if($query->f('supplier_id')){
              $in['our_ref']=$query->f('serial_number');
               array_push($in['supplier_ids'],$query->f('supplier_id'));
              
                // $i = 0;
              $query_line = $this->dbu->query("SELECT servicing_support_articles.* ,pim_articles.item_code
                                       FROM servicing_support_articles
                                         INNER JOIN pim_articles on  pim_articles.article_id=servicing_support_articles.article_id
                                         WHERE servicing_support_articles.service_id='".$in['item']."' 
                                         AND servicing_support_articles.article_id!='0' AND servicing_support_articles.article_delivered='0' AND pim_articles.supplier_id='".$query->f('supplier_id')."' 
                                          ");
        
                while ($query_line->next()) {
                      $in['items'][$query->f('supplier_id')][$i] = 'tmp'.$i;
                       $in['from_type'][$query->f('supplier_id')][$i] = 'intervention';
                         $in['from_id'][$query->f('supplier_id')][$i] = $in['item'];
                      $in['article_id'][$query->f('supplier_id')][$i] = $query_line->f('article_id');
                      $in['article_code'][$query->f('supplier_id')][$i] = $query_line->f('item_code');
                      $in['article'][$query->f('supplier_id')][$i] = $query_line->f('name');
                      $in['quantity'][$query->f('supplier_id')][$i] = display_number($query_line->f('quantity'));
                      $in['price'][$query->f('supplier_id')][$i] = display_number($this->db->field("SELECT purchase_price FROM pim_article_prices WHERE base_price='1' and article_id='".$query_line->f('article_id')."'"));
                      $in['is_article'][$query->f('supplier_id')][$i] = true;
                       $in['tax_id'][$query->f('supplier_id')][$i] =  0;

             
                   $i++;
                  }
                 
             }
        }*/

        $query_line = $this->dbu->query("SELECT servicing_support_articles.* ,pim_articles.item_code,servicing_support.serial_number
           FROM servicing_support_articles
            INNER JOIN servicing_support ON servicing_support.service_id=servicing_support_articles.service_id
             INNER JOIN pim_articles on  pim_articles.article_id=servicing_support_articles.article_id
             WHERE servicing_support_articles.service_id='".$in['item']."' 
             AND servicing_support_articles.article_id!='0' AND servicing_support_articles.article_delivered='0' AND servicing_support_articles.article_id IN (".$article_ids.") ");

        while($query_line->next()){

          $in['our_ref']=$query_line->f('serial_number');

          if(!in_array($in['selected_articles'][$query_line->f('article_id')], $in['supplier_ids'])){
            array_push($in['supplier_ids'],$in['selected_articles'][$query_line->f('article_id')]);     
          }

          $in['items'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'tmp'.$i;
           $in['from_type'][$in['selected_articles'][$query_line->f('article_id')]][$i] = 'intervention';
             $in['from_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $in['item'];
          $in['article_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('article_id');
          $in['article_code'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('item_code');
          $in['article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = $query_line->f('name');
          $in['quantity'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($query_line->f('quantity'));
          $in['price'][$in['selected_articles'][$query_line->f('article_id')]][$i] = display_number($this->db->field("SELECT purchase_price FROM pim_article_prices WHERE base_price='1' and article_id='".$query_line->f('article_id')."'"));
          $in['is_article'][$in['selected_articles'][$query_line->f('article_id')]][$i] = true;
           $in['tax_id'][$in['selected_articles'][$query_line->f('article_id')]][$i] =  0;

         $i++;

        }
        
        //we update the intervention as purchase to remove from to order list if there are no articles for creating a purchase order
        $query_line_diff = $this->dbu->query("SELECT servicing_support_articles.* ,pim_articles.item_code,servicing_support.serial_number
           FROM servicing_support_articles
            INNER JOIN servicing_support ON servicing_support.service_id=servicing_support_articles.service_id
             INNER JOIN pim_articles on  pim_articles.article_id=servicing_support_articles.article_id
             WHERE servicing_support_articles.service_id='".$in['item']."' 
             AND servicing_support_articles.article_id!='0' AND servicing_support_articles.article_delivered='0' AND servicing_support_articles.article_id NOT IN (".$article_ids.") ")->getAll();
        if(empty($query_line_diff)){
          $this->dbu->query("UPDATE  servicing_support SET is_purchase='1' WHERE service_id='".$in['item']."'");
        }
      
      break;

}

if(empty($in['supplier_ids'])){
  msg::notice('Notice','notice');
  json_out($in);
}

foreach ($in['supplier_ids'] as $key => $supplier_id) {
    
 
   
  
   
   $in['actiune_id']=1;

      $buyer_details = $this->dbu->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers
                        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                        WHERE customer_id='".$supplier_id."' ");
      $buyer_address = $this->dbu->query("SELECT * FROM customer_addresses WHERE customer_id='".$supplier_id."' AND delivery ='1' ");
   
    $in['customer_name']=addslashes($buyer_details->f('name'));
    $in['company_number']=addslashes($buyer_details->f('company_number'));
    $in['customer_vat_id']=addslashes($buyer_details->f('vat_id'));
    $in['customer_email']=addslashes($buyer_details->f('c_email'));                                                       
    $in['customer_bank_name']=addslashes($buyer_details->f('bank_name'));                                                       
    $in['customer_bank_iban']=addslashes($buyer_details->f('bank_iban'));                                                           
    $in['customer_bic_code']=addslashes($buyer_details->f('bank_bic_code'));  
    $in['customer_vat_number']=addslashes($buyer_details->f('btw_nr')); 
    $in['customer_phone']=addslashes($buyer_details->f('comp_phone'));  
    $in['customer_cell']=addslashes($buyer_details->f('other_phone'));  
    $in['customer_address']=addslashes($buyer_address->f('address')); 
    $in['customer_country_id']=addslashes($buyer_address->f('country_id')); 
    $in['customer_state']=addslashes($buyer_address->f('state_id'));  
    $in['customer_city']=addslashes($buyer_address->f('city')); 
    $in['customer_zip']=addslashes($buyer_address->f('zip'));
    $in['address_info'] = addslashes($buyer_address->f('address').'
'.$buyer_address->f('zip').'  '.$buyer_address->f('city').'
'.get_country_name($buyer_address->f('country_id')));


    
    $in['field']='customer_id';                                                         
   // $in['our_ref'] = $query->f('serial_number');
   // $in['order_id']= $query->f('order_id');
    $in['p_order_type']=3;                                                    
    $in['email_language']=1;                                                      
                                                       
                                                         
       $in['tr_id']=array();
    if($in['items'][$supplier_id]){     
      foreach ($in['items'][$supplier_id] as $key=>$value){

       
        $line_total2 = 0;
        $row_id = 'tmp'.$key;
        

        $order_line=array(
          'tr_id'               => $row_id,
          'article'             => addslashes($in['article'][$supplier_id][$key]),
          'quantity'            => $in['quantity'][$supplier_id][$key],
          'price'               => $in['price'][$supplier_id][$key],
          
          'article_id'          => $in['is_article'][$supplier_id][$key]?$in['article_id'][$supplier_id][$key]:$in['tax_id'][$supplier_id][$key],
          'from_type'           => $in['from_type'][$supplier_id][$key],
          'from_id'             => $in['from_id'][$supplier_id][$key],
          'article_code'        => $in['article_code'][$supplier_id][$key],
          'supplier_reference'  => $in['supplier_reference'][$supplier_id][$key],
          'tax_id'              => $in['tax_id'][$supplier_id][$key],
          'tax_for_article_id'          =>$in['tax_id'][$supplier_id][$key]? $in['article_id'][$supplier_id][$key]:0,
          'is_tax'                      =>$in['is_article'][$supplier_id][$key]? 0:1,
        );

      
           array_push($in['tr_id'], $order_line);
        
      }
       unset($in['items'][$supplier_id]);
      
    }
                                                       
                                                          
   // console::log('xxx');                                                    

  if($in['same_order']){
     
      $in['customer_id'] = $supplier_id;
        
           if(!$this->same_order($in,$i)) {
           // json_out($in);
           continue;
          
     }
    }
   $in['serial_number'] = addslashes(generate_serial_number(DATABASE_NAME)); 
   
    
   if(!$this->add_order($in)){
      $in['actiune'] = gm('failed');
    }else{
     array_push($in['actiune'],array(gm('Added'),$in['serial_number']) );
 // array_push($in['serial_display'],$in['serial_number']);
    }
   // json_out($in);
 }

json_out($in);

  }
  /**
   * undocumented function
   *
   * @return void
   * @author
   **/
  function same_order(&$in,$i)
  {
    

    $in['same_order_id'] = $this->dbu->field("SELECT p_order_id FROM  pim_p_orders WHERE customer_id='".$in['customer_id']."' AND same_order='".$in['same_order']."' ");
    if($in['same_order_id']){
       $in['serial_number'] = $this->dbu->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id='".$in['same_order_id']."'");
    
      array_push($in['actiune'],array(gm('updated'),$in['serial_number']) );
    
         $in['actiune_id']=2;
        $this->add_lines($in);
        return false;
    }
    return true;
  }
  function add_lines(&$in)
  {
  $in['serial_number'] = $this->dbu->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id='".$in['same_order_id']."'");
  

    //add articles
        $order_total=$this->dbu->field("SELECT amount FROM pim_p_orders WHERE p_order_id='".$in['same_order_id']."'");
    


    foreach ($in['tr_id'] as $nr => $row)
    {   $line_total=0;
        // console::log($in['quantity'][$nr],$in['packing'][$nr],$in['sale_unit'][$nr]);
           $line_total= return_value($row['quantity']) * return_value($row['price']);
           # in case we need packing and sales unit
           // $line_total= (return_value($in['quantity'][$nr])*($in['packing'][$nr]/$in['sale_unit'][$nr])) * return_value($in['price'][$nr]);

            $order_total+= $line_total;
          // $stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id='".$in['article_id'][$nr]."'");
          // $new_stock = $stock + $in['quantity'][$nr];
          // $this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id'][$nr]."'");

            if($row['is_tax'] != '1'){
                $in['order_articles_id'] = $this->db->insert("INSERT INTO pim_p_order_articles SET

                           p_order_id = '".$in['same_order_id']."',
                           article_id = '".$row['article_id']."',
                           discount = '0',
                           quantity = '".return_value($row['quantity'])."',
                           article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
                           price = '".return_value($row['price'])."',
                           price_with_vat = '".return_value($row['price'])."',
                           vat_value = '".$row['vat_value']."',
                           vat_percent = '".$row['vat_percent']."',
                           sort_order='".$nr."',
                           sale_unit = '1',
                           packing = '1',
                           content = '".$row['content']."',
                           article_code = '".addslashes($row['article_code'])."',
                           supplier_reference = '".addslashes($row['supplier_reference'])."'
                            ");
            }else{ //is_tax

               $in['order_articles_id'] = $this->db->insert("INSERT INTO pim_p_order_articles SET

                           p_order_id = '".$in['same_order_id']."',
                           tax_id = '".$row['tax_id']."',
                           tax_for_article_id = '".$row['tax_for_article_id']."',
                           discount = '0',
                           quantity = '".return_value($row['quantity'])."',
                           article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
                           price = '".return_value($row['price'])."',
                           price_with_vat = '".return_value($row['price'])."',
                           vat_value = '".$row['vat_value']."',
                           vat_percent = '".$row['vat_percent']."',
                           sort_order='".$nr."',
                           sale_unit = '1',
                           packing = '1',
                           content = '".$row['content']."',
                           article_code = '".addslashes($row['article_code'])."',
                           supplier_reference = '".addslashes($row['supplier_reference'])."'
                            ");
              }

            /*$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_p_order_articles SET

                       p_order_id = '".$in['same_order_id']."',
                       article_id = '".$row['article_id']."',
                       discount = '0',
                       quantity = '".return_value($row['quantity'])."',
                       article = '".addslashes($row['article'])."',
                       price = '".return_value($row['price'])."',
                       price_with_vat = '".return_value($row['price'])."',
                       vat_value = '".$row['vat_value']."',
                       vat_percent = '".$row['vat_percent']."',
                       sort_order='".$nr."',
                       sale_unit = '1',
                       packing = '1',
                       content = '".$row['content']."',
                       article_code = '".addslashes($row['article_code'])."',
                       supplier_reference = '".addslashes($row['supplier_reference'])."'
                        ");*/
         }
      
      //addtrack
      $this->db->insert("INSERT INTO pim_p_order_track SET
                                  order_articles_id = '".$in['order_articles_id']."',
                                  article_id = '".$row['article_id']."',
                                  from_type = '".$row['from_type']."',
                                  from_id = '".$row['from_id']."'
        ");



        if($in['currency_type'] && $in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
        $order_total = $order_total*return_value($in['currency_rate']);
    }
 

    $this->db->query("UPDATE pim_p_orders SET amount='".$order_total."' WHERE p_order_id='".$in['same_order_id']."'");
    if($in['order_id'] ){
      $tracking_data=array(
                'target_id'         => $in['same_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['order_id'],'origin_type'=>'4')
                                        )
              );
          addTracking($tracking_data);
    }
    return true;
  }


/****************************************************************
    * function add_order(&$in)                                          *
****************************************************************/
    function add_order(&$in)
    {
    //if(!$this->add_order_validate($in)){
        //return false;
    //  }
      $in['serial_number'] = generate_serial_number(DATABASE_NAME);
    if(isset($in['date_h']) && !empty($in['date_h']) ){
        $in['date_h'] = strtotime($in['date_h']);
    }
    if(isset($in['del_date']) && !empty($in['del_date']) ){
        $in['del_date'] = strtotime($in['del_date']);
    }
    if($in['order_ts']){
    $in['date_h'] = $in['order_ts'];
    $in['del_date'] = $in['order_ts'];
    }


    if(!$in['notes']){
        $in['notes'] = $this->db->field("SELECT value FROM default_data WHERE type='p_order_note'");
    }
    if(!$in['pdf_layout'] && $in['identity_name']){
      $in['pdf_layout'] = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_name']."' and module='porder'");
    }

    if(!$in['currency_rate']){
        $in['currency_rate']=1;
    }
/*    $in['sc_id'] = str_replace('-',' ',$in['sc_id']);*/
   if($in['contact_id']){
      $in['contact_name']= $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
  }

  /*if(!$in['service_id'] && !$in['deal_id']){
      $in['identity_id']= $this->db_users->field("SELECT default_identity_id FROM user_info WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    }else{
      $in['identity_id']=0;
    }*/
    
    //Set email language as account / contact language
      $emailMessageData = array('buyer_id'    => $in['customer_id'],
                    'contact_id'    => $in['contact_id'],
                    'param'     => 'add');
      $in['email_language'] = get_email_language($emailMessageData);
      //End Set email language as account / contact language

    $customer = $this->db->query("SELECT customers.c_email, customers.comp_phone, customers.btw_nr,customers.name AS company_name,customer_legal_type.name as l_name FROM customers
      LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
     WHERE customers.customer_id='".$in['customer_id']."' ");
    $in['customer_name']=addslashes(stripslashes($customer->f('company_name').' '.$customer->f('l_name')));
    $in['customer_phone']      = $customer->f('comp_phone');
    $in['customer_email']      = $customer->f('c_email');
    $in['customer_vat_number']   = $customer->f('btw_nr');

    $buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' ORDER BY is_primary DESC");
        $in['address_info'] = addslashes($buyer_addr->f('address')).'
        '.$buyer_addr->f('zip').'  '.addslashes($buyer_addr->f('city')).'
        '.get_country_name($buyer_addr->f('country_id'));
        $in['customer_address']=addslashes($buyer_addr->f('address'));
        $in['customer_zip']=$buyer_addr->f('zip');
        $in['customer_city']=addslashes($buyer_addr->f('city'));
        $in['customer_country_id']=$buyer_addr->f('country_id');

    $setting_order_ref = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_REF' AND type=1");
    if (!$setting_order_ref) {
        $buyer_ref = '';
    } else {
        $buyer_ref = $in['buyer_ref'];
    }

    $in['p_order_id'] = $this->db->insert("INSERT INTO pim_p_orders SET
              same_order                  = '".$in['same_order']."',
              customer_id         = '".$in['customer_id']."',
              second_customer_id  = '".$in['sc_id']."',
              second_address_id   = '".$in['second_address_id']."',
              client_delivery     = '".$in['client_delivery']."',
              main_address_id     = '".$in['main_address_id']."',
              customer_name         = '".$in['customer_name']."',
              contact_name        = '".$in['contact_name']."',
              contact_id          = '".$in['contact_id']."',
              company_number        = '".$in['company_number']."',
              customer_vat_id       = '".$in['customer_vat_id']."',
              customer_email        = '".$in['customer_email']."',
              customer_bank_name      = '".$in['customer_bank_name']."',
              customer_bank_iban      = '".$in['customer_bank_iban']."',
              customer_bic_code       = '".$in['customer_bic_code']."',
              customer_vat_number     = '".$in['customer_vat_number']."',
              customer_country_id     = '".$in['customer_country_id']."',
              customer_state        = '".$in['customer_state']."',
              customer_city         = '".$in['customer_city']."',
              rdy_invoice         = '".$rdy_invoice."',
              customer_zip        = '".$in['customer_zip']."',
              customer_phone        = '".$in['customer_phone']."',
              customer_cell         = '".$in['customer_cell']."',
              customer_address      = '".$in['customer_address']."',
              seller_name             = '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
              seller_d_address        = '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
              seller_d_zip            = '".addslashes(ACCOUNT_DELIVERY_ZIP)."',
              seller_d_city             = '".utf8_encode(addslashes(ACCOUNT_DELIVERY_CITY))."',
              seller_d_country_id       = '".ACCOUNT_DELIVERY_COUNTRY_ID."',
              seller_d_state_id         = '".ACCOUNT_DELIVERY_STATE_ID."',
              seller_bwt_nr               = '".addslashes(ACCOUNT_VAT_NUMBER)."',
              delivery_id           = '".$in['delivery_id']."',
              delivery_address      = '".$in['delivery_address']."',
                                              `date`            = '".$in['date_h']."',
              payment_term        = '".$in['payment_term']."',
              payment_type        = '".$in['payment_type']."',
                                                        serial_number         = '".$in['serial_number']."',
                                                        field           = '".$in['field']."',
                                                        active            = '1',
                                                        buyer_ref         = '".$buyer_ref."',
                                                        discount          = '".return_value($in['discount'])."',
                                                        use_package                 = '".ALLOW_ARTICLE_PACKING."',
                                                            use_sale_unit               = '".ALLOW_ARTICLE_SALE_UNIT."',
                                                        currency_rate       = '".$in['currency_rate']."',
                                                        our_ref           = '".$in['our_ref']."',
                                                        your_ref          = '".$in['your_ref']."',
                                                        del_date          = '".$in['del_date']."',
                                                            quote_id          = '".$in['quote_id']."',
                                                            currency_type       = '".$in['currency_type_val']."',
                                                            address_info            = '".$in['address_info']."' ,
                                                            is_adveo              = '".$in['is_adveo']."' ,
                                                            p_order_type            = '".$in['p_order_type']."' ,
                                                            order_id              = '".$in['order_id']."' ,
                                                            email_language        = '".$in['email_language']."',
                                                            identity_id       ='".$in['identity_id']."',
                                                            pdf_layout       ='".$in['pdf_layout']."',
                                                             add_cost       ='".return_value($in['add_cost'])."',
                                                             source_id      ='".$in['source_id']."',
                                                             type_id        ='".$in['type_id']."',
                                                             segment_id     ='".$in['segment_id']."',
                                                             apply_discount           = '".$in['apply_discount']."',
                                                             discount_line_gen          =   '".return_value($in['discount_line_gen'])."'

                                                            ");

    //notes                     = '".utf8_encode($in['notes'])."',

  // add multilanguage order notes in note_fields
    $lang_note_id = $in['email_language'];
    
    /*$langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($langs->next()){
    foreach ($in['translate_loop'] as $key => $value) {
        if($value['lang_id'] == $langs->f('lang_id')){
          $in['nota'] = $value['notes'];
        break;
        }
    }

    if($lang_note_id == $langs->f('lang_id')){
        $active_lang_note = 1;
    }else{
        $active_lang_note = 0;
    }

    $this->db->insert("INSERT INTO note_fields SET
            item_id           = '".$in['p_order_id']."',
            lang_id           =   '".$langs->f('lang_id')."',
            active            =   '".$active_lang_note."',
            item_type         =   'p_order',
            item_name         =   'notes',
            item_value        =   '".$in['nota']."'
    ");
    
    }

    $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active = '1' ORDER BY sort_order ");
    while($custom_langs->next()) {
    foreach ($in['translate_loop_custom'] as $key => $value) {
        if($value['lang_id'] == $custom_langs->f('lang_id')){
  $in['nota'] = $value['notes'];
  break;
        }
    }
    if($lang_note_id == $custom_langs->f('lang_id')) {
        $active_lang_note = 1;
    } else {
        $active_lang_note = 0;
    }
    $this->db->insert("INSERT INTO note_fields SET
        item_id     = '".$in['p_order_id']."',
        lang_id     = '".$custom_langs->f('lang_id')."',
        active      = '".$active_lang_note."',
        item_type     = 'p_order',
        item_name     = 'notes',
        item_value    = '".$in['nota']."'
    ");
    }*/

    $emailLanguageDefaultNotesData = array('email_language' => $in['email_language'],
                              'default_name_note' => 'p_order_note',
                              'default_type_note' => 'p_order_note',
                              'default_name_free_text_content' => 'quote_term',
                              'default_type_free_text_content' => 'quote_terms',
                            );

        $defaultNotesData = get_email_message_default_notes($emailLanguageDefaultNotesData);
        //End Add default notes values based on language

      $this->db->insert("INSERT INTO note_fields SET
                        item_id           = '".$in['p_order_id']."',
                        lang_id           =   '".$in['email_language']."',
                        active            =   '1',
                        item_type         =   'p_order',
                        item_name         =   'notes',
                        item_value        =   '".$in['notes']."'
      ");

    //add articles
    $global_disc = return_value($in['discount']);
    if($in['apply_discount'] < 2){
      $global_disc = 0;
    }
        $order_total=0;
    foreach ($in['tr_id'] as $nr => $row)
    {   $line_total=0;
        // console::log($in['quantity'][$nr],$in['packing'][$nr],$in['sale_unit'][$nr]);
           //$line_total= return_value($row['quantity']) * return_value($row['price']);
           # in case we need packing and sales unit

          $sale_unit= $row['sale_unit'];
            if(!ALLOW_ARTICLE_SALE_UNIT){
              $sale_unit=1;
            }
          $packing= $row['packing'];
          if(!ALLOW_ARTICLE_PACKING){
              $packing=1;
              $sale_unit=1;
            }
            $discount_line = return_value($row['disc']);
            if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
              $discount_line = 0;
            }
            $price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);
            $line_total= (return_value($row['quantity'])*( $packing/$sale_unit)) * $price_line;

            $order_total+= $line_total- ($line_total*$global_disc/100);
            // $stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id='".$in['article_id'][$nr]."'");
            // $new_stock = $stock + $in['quantity'][$nr];
            // $this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$in['article_id'][$nr]."'");


            if($row['is_tax'] != 1){
                $in['order_articles_id'] = $this->db->insert("INSERT INTO pim_p_order_articles SET

                           p_order_id = '".$in['p_order_id']."',
                           article_id = '".$row['article_id']."',
                           discount = '".return_value($row['disc'])."',
                           quantity = '".return_value($row['quantity'])."',
                           article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
                           price = '".return_value($row['price'])."',
                           price_with_vat = '".return_value($row['price'])."',
                           vat_value = '".$row['vat_value']."',
                           vat_percent = '".$row['vat_percent']."',
                           sort_order='".$nr."',
                           sale_unit = '".$sale_unit."',
                           packing = '". $packing."',
                           content = '".$row['content']."',
                           article_code = '".addslashes($row['article_code'])."',
                           supplier_reference = '".addslashes($row['supplier_reference'])."',
                             has_variants = '".$row['has_variants']."',
                            has_variants_done ='".$row['has_variants_done']."',
                            is_variant_for ='".$row['is_variant_for']."',
                            variant_type='".$row['variant_type']."'
                            ");
            }else{ //is_tax

               $in['order_articles_id'] = $this->db->insert("INSERT INTO pim_p_order_articles SET

                           p_order_id = '".$in['p_order_id']."',
                           tax_id = '".$row['article_id']."',
                           tax_for_article_id = '".$row['tax_for_article_id']."',
                           discount = '".return_value($row['disc'])."',
                           quantity = '".return_value($row['quantity'])."',
                           article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
                           price = '".return_value($row['price'])."',
                           price_with_vat = '".return_value($row['price'])."',
                           vat_value = '".$row['vat_value']."',
                           vat_percent = '".$row['vat_percent']."',
                           sort_order='".$nr."',
                           sale_unit = '1',
                           packing = '1',
                           content = '".$row['content']."',
                           article_code = '".addslashes($row['article_code'])."',
                           supplier_reference = '".addslashes($row['supplier_reference'])."'
                            ");
              }

    }
     $this->db->insert("INSERT INTO pim_p_order_track SET
                                  order_articles_id = '".$in['order_articles_id']."',
                                  article_id = '".$row['article_id']."',
                                  from_type = '".$row['from_type']."',
                                  from_id = '".$row['from_id']."'
        ");

        if($in['currency_type'] && $in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
        $order_total = $order_total*return_value($in['currency_rate']);
    }
        if($in['add_cost']){
            $order_total=$order_total+return_value($in['add_cost']);
        }

      if($in['service_id'] ){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['service_id'],'origin_type'=>'5')
                                        )
              );
          addTracking($tracking_data);
    }
    if($in['duplicate_p_order_id']){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['duplicate_p_order_id'],'origin_type'=>'6')
                                        )
              );
          addTracking($tracking_data);
    }
    if($in['quote_id'] ){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['quote_id'],'origin_type'=>'2')
                                        )
              );
          addTracking($tracking_data);
    }
    if($in['order_id'] ){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['order_id'],'origin_type'=>'4')
                                        )
              );
          addTracking($tracking_data);
    }
    if(!$in['service_id'] && !$in['duplicate_p_order_id'] && !$in['quote_id'] && !$in['order_id']){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array()
              );
          addTracking($tracking_data);
    }

    $this->db->query("UPDATE pim_p_orders SET amount='".$order_total."' WHERE p_order_id='".$in['p_order_id']."'");

        msg::success (gm("Purchase order successfully added"),'success');
       

        $this->pag = 'p_order';
        $this->field_n = 'p_order_id';

    insert_message_log($this->pag,'{l}Purchase order successfully added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
    $in['pagl'] = $this->pag;
    $in['add_problem'] = $this->po_pag;
    
    if($in['duplicate_p_order_id']){
      $original_duplicate_serial=$this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id='".$in['duplicate_p_order_id']."' ");

      //on the original purchase order
      insert_message_log('p_order','{l}This purchase order was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} <a href="[SITE_URL]purchase_order/view/'.$in['p_order_id'].'">{l}Purchase Order{endl} '.$in['serial_number'].'</a>',$this->field_n,$in['duplicate_p_order_id']);

      //on the resulting purchase order
      insert_message_log('p_order','{l}This purchase order was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}from{endl} <a href="[SITE_URL]purchase_order/view/'.$in['duplicate_p_order_id'].'">{l}Purchase Order{endl} '.$original_duplicate_serial.'</a>',$this->field_n,$in['p_order_id']);

      //insert_message_log($this->pag,'{l}Purchase order successfully added by duplicating{endl} '.$original_duplicate_serial,$this->field_n,$in['p_order_id']);
    }

    /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id  = '".$_SESSION['u_id']."'
            AND name    = 'order-po_orders_show_info' ");*/
    $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id  = :user_id
            AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'order-po_orders_show_info']);            
    if(!$show_info->move_next()) {
        /*$this->db_users->query("INSERT INTO user_meta SET   user_id = '".$_SESSION['u_id']."',
        name  = 'order-po_orders_show_info',
        value   = '1' ");*/
        $this->db_users->insert("INSERT INTO user_meta SET   user_id = :user_id,
                                                            name  = :name,
                                                            value   = :value ",
                                                          ['user_id' => $_SESSION['u_id'],
                                                           'name'  => 'order-po_orders_show_info',
                                                           'value'   => '1']
                                                        );
    } else {
        /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
            AND name    = 'order-po_orders_show_info' ");*/
        $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
            AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'order-po_orders_show_info']);            
    }

    $count = $this->db->field("SELECT COUNT(p_order_id) FROM pim_p_orders ");
    if($count == 1){
      doManageLog('Created the first purchase order.');
    }

  return true;

    }

    /****************************************************************
    * function add_order_validate(&$in)                                          *
    ****************************************************************/
    function add_order_validate(&$in)
    {
    $v = new validation($in);
    $v->field('serial_number', 'Name', 'required',gm('Order Nr').'* '.gm('is required').'<br />');
    if(!$v->run()){
        return false;
    }
    if (ark::$method == 'add_order') {
        $v->field('serial_number', 'Serial Number', 'unique[pim_p_orders.serial_number]',gm('Order Nr').' '.gm('is already in use'));
        if(!$v->run()){
  $v = null;
  $in['serial_number'] = generate_serial_number(DATABASE_NAME);
  $v = new validation($in);
  $v->field('serial_number', 'Name', 'required',gm('Order Nr').'* '.gm('is required').'<br />');
        }
    }else{
        $v->field('serial_number', "Serial Number", "unique[pim_p_orders.serial_number.( p_order_id!='".$in['p_order_id']."')]",gm('Order Nr').'* '.gm('is already in use').'.<br />');
    }

    $v->field('customer_name', "Company Name", "required",gm('Customer Name').' '.gm('is required').'.<br />');

    return $v->run();
    
    
    }

    /****************************************************************
    * function update_order(&$in)                                          *
    ****************************************************************/
    function update_order(&$in)
    {
       $in['address_info']=str_replace('<br />',' ',$in['address_info']);
    if(!$this->update_order_validate($in)){
        return false;
    }
    if(isset($in['date_h']) && !empty($in['date_h']) ){
        $in['date_h'] = strtotime($in['date_h']);
    }
    if(isset($in['del_date']) && !empty($in['del_date']) ){
        $in['del_date'] = strtotime($in['del_date']);
    }


      //Set email language as account / contact language
      $emailMessageData = array('buyer_id'    => $in['customer_id'],
                    'contact_id'    => $in['contact_id'],
                    'item_id'     => $in['p_order_id'],
                    'email_language'  => $in['email_language'],
                    'table'     => 'pim_p_orders',
                    'table_label'   => 'p_order_id',
                    'table_buyer_label' => 'customer_id',
                    'table_contact_label' => 'contact_id',
                    'param'     => 'edit');
      $in['email_language'] = get_email_language($emailMessageData);
      //End Set email language as account / contact language

     $default_email_language = $this->db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
     if($in['email_language']==0){
        $in['email_language'] = $default_email_language;
     }

      if($in['contact_id']){
      $in['contact_name']= $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
     }

     if(!$in['client_delivery']){
        $in['sc_id'] =0;
        $in['second_address_id'] =0;
     }
     if($in['customer_id']){
      $customer_data=$this->dbu->query("SELECT customers.c_email,customers.comp_phone,customers.btw_nr,customers.name AS company_name,customer_legal_type.name as l_name FROM customers
          LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
         WHERE customers.customer_id='".$in['customer_id']."' ");
          $in['customer_name']=addslashes(stripslashes($customer_data->f('company_name').' '.$customer_data->f('l_name')));
          $in['customer_email']=$customer_data->f('c_email');
          $in['customer_phone']=$customer_data->f('comp_phone');
          $in['customer_vat_number']=$customer_data->f('btw_nr');
          
        $buyer = $this->dbu->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' ORDER BY is_primary DESC");
        $in['address_info'] = $buyer->f('address').'
        '.$buyer->f('zip').'  '.$buyer->f('city').'
        '.get_country_name($buyer->f('country_id'));
        $in['customer_address']=addslashes($buyer->f('address'));
        $in['customer_zip']=$buyer->f('zip');
        $in['customer_city']=addslashes($buyer->f('city'));
        $in['customer_country_id']=$buyer->f('country_id');
     }

    $this->db->query("UPDATE pim_p_orders SET           customer_id         = '".$in['customer_id']."',
                                                        serial_number       = '".$in['serial_number']."',
                                                        customer_name       = '".$in['customer_name']."',
                                                        contact_id          = '".$in['contact_id']."',
                                                        contact_name        = '".$in['contact_name']."',
                                                        company_number      = '".$in['company_number']."',
                                                        email_language      = '".$in['email_language']."',
                                                        customer_vat_id     = '".$in['customer_vat_id']."',
                                                        customer_email      = '".$in['customer_email']."',
                                                        customer_bank_name  = '".$in['customer_bank_name']."',
                                                        customer_bank_iban  = '".$in['customer_bank_iban']."',
                                                        customer_bic_code   = '".$in['customer_bic_code']."',
                                                        customer_vat_number = '".$in['customer_vat_number']."',
                                                        customer_country_id = '".$in['customer_country_id']."',
                                                        customer_state    = '".$in['customer_state']."',
                                                        customer_city     = '".addslashes($in['customer_city'])."',
                                                        rdy_invoice     = '".$rdy_invoice."',
                                                        customer_zip    = '".$in['customer_zip']."',
                                                        customer_phone    = '".$in['customer_phone']."',
                                                        buyer_ref       = '".$in['buyer_ref']."',
                                                        customer_address  = '".$in['customer_address']."',
                                                        seller_name         = '".utf8_encode(addslashes(ACCOUNT_COMPANY))."',
                                                        seller_d_address    = '".utf8_encode(addslashes(ACCOUNT_DELIVERY_ADDRESS))."',
                                                        seller_d_zip        = '".ACCOUNT_DELIVERY_ZIP."',
                                                        seller_d_city       = '".utf8_encode(addslashes(ACCOUNT_DELIVERY_CITY))."',
                                                        seller_d_country_id = '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                                        seller_d_state_id   = '".ACCOUNT_DELIVERY_STATE_ID."',
                                                        seller_bwt_nr       = '".ACCOUNT_VAT_NUMBER."',
                                                        `date`        = '".$in['date_h']."',
                                                        payment_term    = '".$in['payment_term']."',
                                                        payment_type    = '".$in['payment_type']."',
                                                        serial_number     = '".$in['serial_number']."',
                                                        discount      = '".return_value($in['discount'])."',
                                                        delivery_id     = '".$in['delivery_id']."',
                                                        our_ref       = '".$in['our_ref']."',
                                                        your_ref      = '".$in['your_ref']."',
                                                        del_date      = '".$in['del_date']."',
                                                        p_order_type        = '".$in['p_order_type']."' ,
                                                        delivery_address  = '".$in['delivery_address']."',
                                                        address_info        = '".addslashes($in['address_info'])."',
                                                        identity_id ='".$in['identity_id']."',
                                                        add_cost        ='".return_value($in['add_cost'])."',
                                                        client_delivery       ='".return_value($in['client_delivery'])."',
                                                        second_customer_id       ='".$in['sc_id']."',
                                                        second_address_id       ='".$in['second_address_id']."',
                                                        source_id      ='".$in['source_id']."',
                                                        type_id        ='".$in['type_id']."',
                                                        segment_id     ='".$in['segment_id']."',
                                                        apply_discount          = '".$in['apply_discount']."',
                                                        discount_line_gen       = '".return_value($in['discount_line_gen'])."'
                                                        WHERE p_order_id='".$in['p_order_id']."' ");
    //notes               = '".utf8_encode($in['notes'])."' ,


    // update multilanguage order note in note_fields
    $lang_note_id = $in['email_language'];
    $exist_note = $this->db->field("SELECT note_fields_id FROM note_fields WHERE active='1' AND item_type='p_order' AND item_name='notes' AND lang_id='".$in['email_language']."' AND item_id='".$in['p_order_id']."'");
    if($exist_note){
      //Update other entries as inactive
        $this->db->query("UPDATE note_fields SET
                        active          =   '0'
                    WHERE   item_type         =   'p_order'
                    AND   item_name         =   'notes'
                    AND   item_id         =   '".$in['p_order_id']."'"
                    );

        $this->db->query("UPDATE note_fields SET
                        active          =   '1',
                        item_value        =   '".$in['notes']."'
                    WHERE   item_type         =   'p_order'
                    AND   item_name         =   'notes'
                    AND   lang_id         =   '".$in['email_language']."'
                    AND   item_id         =   '".$in['p_order_id']."'

        ");
      }else{
        //Update other entries as inactive
        $this->db->query("UPDATE note_fields SET
                        active          =   '0'
                    WHERE   item_type         =   'p_order'
                    AND   item_name         =   'notes'
                    AND   item_id         =   '".$in['p_order_id']."'"
                    );

        $this->db->insert("INSERT INTO note_fields SET
                        item_id         = '".$in['p_order_id']."',
                        lang_id         =   '".$in['email_language']."',
                        active          =   '1',
                        item_type         =   'p_order',
                        item_name         =   'notes',
                        item_value        =   '".$in['notes']."'
        ");
      }


    /*$langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($langs->next()){
    foreach ($in['translate_loop'] as $key => $value) {
        if($value['lang_id'] == $langs->f('lang_id')){
  $in['nota'] = $value['notes'];
  break;
        }
    }
    if($lang_note_id == $langs->f('lang_id')){
        $active_lang_note = 1;
    }else{
        $active_lang_note = 0;
    }
    
        $this->db->query("UPDATE note_fields SET
            active          =   '".$active_lang_note."',
            item_value        =   '".$in['nota']."'
        WHERE   item_type         =   'p_order'
        AND   item_name         =   'notes'
        AND   lang_id         =   '".$langs->f('lang_id')."'
        AND   item_id         =   '".$in['p_order_id']."'

        ");
    
    }
    $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
    while($custom_langs->next()) {
    foreach ($in['translate_loop_custom'] as $key => $value) {
        if($value['lang_id'] == $custom_langs->f('lang_id')){
  $in['nota'] = $value['notes'];
  break;
        }
    }
    if($lang_note_id == $custom_langs->f('lang_id')) {
        $active_lang_note_custom = 1;
    } else {
        $active_lang_note_custom = 0;
    }
    $this->db->query("UPDATE note_fields SET  active    =   '".$active_lang_note_custom."',
          item_value  =   '".$in['notes_'.$custom_langs->f('lang_id')]."'
             WHERE  item_type   =   'p_order'
             AND  item_name   =   'notes'
             AND  lang_id   =   '".$custom_langs->f('lang_id')."'
             AND  item_id   =   '".$in['p_order_id']."'
    ");
      }*/

    

    $this->db->query("DELETE FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."'");

    $use_package=$this->db->field("SELECT use_package FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");
    $use_sale_unit=$this->db->field("SELECT use_sale_unit FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");

    $global_disc = return_value($in['discount']);
    if($in['apply_discount'] < 2){
      $global_disc = 0;
    }
    $order_total=0;
    foreach ($in['tr_id'] as $nr => $row)
    {
          $sale_unit= $row['sale_unit'];
          if(!ALLOW_ARTICLE_SALE_UNIT){
            $sale_unit=1;
          }
          $packing= $row['packing'];
          if(!ALLOW_ARTICLE_PACKING){
            $packing=1;
            $sale_unit=1;
          }

          $discount_line = return_value($row['disc']);
          if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
            $discount_line = 0;
          }

          $price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);

            $line_total=(return_value($row['quantity']) * $price_line * return_value($packing)/$sale_unit);

            $order_total+= $line_total- ($line_total*$global_disc/100);

            if($row['is_tax'] != 1){
              $in['order_articles_id'] = $this->db->insert("INSERT INTO pim_p_order_articles SET
                       p_order_id = '".$in['p_order_id']."',
                       article_id = '".$row['article_id']."',
                       quantity = '".return_value($row['quantity'])."',
                       discount = '".return_value($row['disc'])."',
                       article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
                       price = '".return_value($row['price'])."',
                       price_with_vat = '".return_value($row['price'])."',
                       vat_value   = '".$row['vat_value']."',
                       vat_percent = '".$row['vat_percent']."',
                       sort_order  ='".$nr."',
                       sale_unit = '".$sale_unit."',
                       packing = '".$packing."',
                       content = '".$row['content']."',
                       article_code = '".addslashes($row['article_code'])."',
                       supplier_reference = '".addslashes($row['supplier_reference'])."',
                       has_variants = '".$row['has_variants']."',
                       has_variants_done ='".$row['has_variants_done']."',
                       is_variant_for ='".$row['is_variant_for']."',
                       variant_type='".$row['variant_type']."',
                       facq='".$row['facq']."' 
                        ");
            }else{//is_tax
                $in['order_articles_id'] = $this->db->insert("INSERT INTO pim_p_order_articles SET
                       p_order_id = '".$in['p_order_id']."',
                       tax_id = '".$row['article_id']."',
                       tax_for_article_id = '".$row['tax_for_article_id']."',
                       quantity = '".return_value($row['quantity'])."',
                       discount = '".return_value($row['disc'])."',
                       article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
                       price = '".return_value($row['price'])."',
                       price_with_vat = '".return_value($row['price'])."',
                       vat_value   = '".$row['vat_value']."',
                       vat_percent = '".$row['vat_percent']."',
                       sort_order  ='".$nr."',
                       sale_unit = '1',
                       packing = '1',
                       content = '".$row['content']."',
                       article_code = '".addslashes($row['article_code'])."',
                       supplier_reference = '".addslashes($row['supplier_reference'])."'
                        ");
            }

    }

     
    if($in['currency_type_val'] != ACCOUNT_CURRENCY_TYPE){
         $order_total = $order_total*return_value($in['currency_rate']);
    }

    if($in['add_cost']){
       $order_total=$order_total+return_value($in['add_cost']);
    }
    $trace_id=$this->db->field("SELECT trace_id FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");
    if($trace_id){
      $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['customer_id']."',target_buyer_id='".$in['customer_id']."' WHERE trace_id='".$trace_id."' ");
    }


    $this->db->query("UPDATE pim_p_orders SET amount='".$order_total."' WHERE p_order_id='".$in['p_order_id']."'");

    //msg::$success = gm("Purchase order successfully updated");
     msg::success (gm("Purchase order successfully updated"),'success');
   

    $this->pag = 'p_order';
    $this->field_n = 'p_order_id';

    insert_message_log($this->pag,'{l}Purchase order successfully updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
    $in['pagl'] = $this->pag;
    $in['add_problem'] = $this->po_pag;



    return true;
    }
    /****************************************************************
    * function update_order_validate(&$in)                                          *
    ****************************************************************/
    function update_order_validate(&$in)
    {
    $v = new validation($in);
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    if(!$v->run()){
        return false;
    } else {
        return $this->add_order_validate($in);
    }

    }

    /**
     * undocumented function
     *
     * @return boolean
     * @author PM
     **/
    function updateCustomerData(&$in)


    {
    
        
    $sql = "UPDATE pim_p_orders SET ";
    if($in['buyer_id']){
        $buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name AS company_name, customers.no_vat, customers.btw_nr, 
        customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, customer_addresses.address_id,
        customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount,customer_legal_type.name as l_name
        FROM customers
        LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
        AND customer_addresses.is_primary=1
        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
        WHERE customers.customer_id='".$in['buyer_id']."' ");
        $buyer_info->next();

        if($in['main_address_id']){
          $is_customer_address=$this->db->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['main_address_id']."' AND customer_id='".$in['buyer_id']."' ");
          if(!$is_customer_address){
            unset($in['main_address_id']);
          }
        }
        
        if(!$in['main_address_id']){
          $in['main_address_id'] = $buyer_info->f('address_id');
        }

        if(!$buyer_info->f('apply_line_disc')){
          if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
            $apply_discount = 3;
          }else{
            if($buyer_info->f('apply_fix_disc')){
              $apply_discount = 2;
            }
          }
      } else {
        $apply_discount = 1;
        if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
          $apply_discount = 3;
        }else{
          if($buyer_info->f('apply_fix_disc')){
            $apply_discount = 2;
          }
        }
      }

      $in['apply_discount'] = $apply_discount;
      $in['apply_fix_disc'] = $apply_fix_disc;
      $in['discount']=$buyer_info->f('fixed_discount');
      $in['discount_line_gen']=$buyer_info->f('line_discount');

        
        if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
  $ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
        }
        
        $in['currency_rate']=0;
        $in['currency_id']  = $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
        if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
          $in['currency_rate'] = $this->currencyRate(currency::get_currency($in['currency_id'],'code'));
        }
        if($in['contact_id']){
          $in['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
        }
        
        if($in['p_order_id'] == 'tmp'){
               $in['delivery_address_id'] = $buyer_info->f('address_id');
             }

        //Set email language as account / contact language
        $emailMessageData = array('buyer_id'    => $in['buyer_id'],
                    'contact_id'    => $in['contact_id'],
                    'item_id'     => $in['p_order_id'],
                    'email_language'  => $in['email_language'],
                    'table'     => 'pim_p_orders',
                    'table_label'   => 'p_order_id',
                    'table_buyer_label' => 'customer_id',
                    'table_contact_label' => 'contact_id',
                    'param'     => 'update_customer_data');
        $in['email_language'] = get_email_language($emailMessageData);
        //End Set email language as account / contact language   

        $in['identity_id'] = get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);  

        $sql .= " customer_id = '".$in['buyer_id']."', ";
        $sql .= " second_customer_id = '".$in['sc_id']."', ";
        $sql .= " second_address_id = '".$in['second_address_id']."', ";
        $sql .= " customer_name = '".addslashes(stripslashes($buyer_info->f('company_name').' '.$buyer_info->f('l_name')))."', ";
        $sql .= " customer_email = '".$buyer_info->f('c_email')."', ";
        $sql .= " customer_vat_number = '".$buyer_info->f('btw_nr')."', ";
        $sql .= " customer_country_id = '".$buyer_info->f('country_id')."', ";
        $sql .= " customer_city = '".addslashes($buyer_info->f('city'))."', ";
        $sql .= " customer_zip = '".$buyer_info->f('zip')."', ";
        $sql .= " customer_phone = '".$buyer_info->f('comp_phone')."', ";
        $sql .= " customer_address = '".addslashes($buyer_info->f('address'))."', ";
        $sql .= " main_address_id = '".$in['main_address_id']."', ";
        $sql .= " email_language = '".$in['email_language']."', ";
        // $sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
        //$sql .= " delivery_address = '".addslashes($in['delivery_address'])."', ";
        //$sql .= " delivery_address_id = '".$in['delivery_address_id']."', ";
       // $sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
        $sql .= " identity_id = '".$in['identity_id']."', ";
       // $sql .= " customer_ref = '".addslashes($buyer_info->f('our_reference'))."', ";
        $sql .= " buyer_ref = '".addslashes($ref)."', ";
        $sql .= " currency_type = '".$in['currency_id']."', ";
        $sql .= " currency_rate = '".$in['currency_rate']."', ";
        
        
        $sql .= " contact_name = '".$in['contact_name']."', ";
       $sql .= " contact_id = '".$in['contact_id']."', ";
        
        

        $in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));

        if($buyer_info->f('address_id') != $in['main_address_id']){
            $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
            $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
          }
        
        $new_address_txt='';
        if($in['second_address_id']) {
              $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['second_address_id']."' ");
              $new_address->next();
              $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
              $sql .= " delivery_address = '".addslashes($new_address_txt)."', ";
              $sql .= " delivery_id = '".addslashes($in['second_address_id'])."', ";
        }     

      
        //$sql .= " address_info = '".addslashes($new_address_txt)."', ";      
      $sql .= " address_info = '".addslashes($in['address_info'])."', ";      

    
        


        $sql .= " field = 'customer_id' ";

        if($in['changePrices']==1){
          foreach ($in['article'] as $key => $value) {
            $params = array(
              'article_id' => $value['article_id'], 
              'customer_id' => $in['buyer_id'],
              'has_variants' => ($value['has_variants'] ? true : false)
            );
            $in['article'][$key]['price'] = $value['article_id']? display_number($this->getArticlePrice($params)) : $value['price'] ;
          }
        }
        
    }
    $sql .=" WHERE p_order_id ='".$in['item_id']."' ";
    if(!$in['isAdd']){
    
        $this->db->query($sql); 
        if($in['item_id'] && is_numeric($in['item_id'])){
          $trace_id=$this->db->field("SELECT trace_id FROM pim_p_orders WHERE p_order_id='".$in['item_id']."' ");
          if($trace_id && $in['buyer_id']){
            $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
          }
      }
    }
    $in['p_order_id'] = $in['item_id'];
    msg::$success = gm('Sync successfull.');
    return true;
    }



    function archive_po_order(&$in){
    if(!$this->delete_order_validate($in)){
      return false;
    }
    

    $this->db->query("UPDATE  pim_p_orders SET active='0', serial_number='' WHERE p_order_id='".$in['p_order_id']."' ");

    msg::success ( gm("Purchase order has been archived"),'success');
    insert_message_log('p_order','{l}Purchase order has been archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
    $in['pagl'] = 'p_order';
    $tracking_trace=$this->db->field("SELECT trace_id FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");
        if($tracking_trace){
          $this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
        }
    return true;
  }
   
   function delete_po_order(&$in)
  {
    if(!$this->delete_order_validate($in)){
      return false;
    }
    $tracking_trace=$this->db->field("SELECT trace_id FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");
    
    if(ALLOW_STOCK){
    

    $delivery = $this->db->query("SELECT order_articles_id, quantity
                            FROM pim_p_orders_delivery
                            WHERE p_order_id='".$in['p_order_id']."'");


    while ($delivery->next())
    {
      $article_data = $this->db->query("SELECT article_id, article FROM pim_p_order_articles WHERE order_articles_id='".$delivery->f('order_articles_id')."'");
      $article_data->next();
      $article_id = $article_data->f("article_id");
      // $article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$delivery->f('order_articles_id')."'");
      $stock_and_item_code = $this->db->query("SELECT stock, item_code, packing, sale_unit FROM pim_articles WHERE article_id=".$article_id);
      $item_code = $stock_and_item_code->f("item_code");
      $stock = $stock_and_item_code->f("stock");
      // $stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id=".$article_id);
      $new_stock = $stock - ($delivery->f('quantity')*$stock_and_item_code->f('packing')/$stock_and_item_code->f('sale_unit'));
      $this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id=".$article_id);
      
    }
      
       }




        $this->db->query("DELETE FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' ");
    $this->db->query("DELETE FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' ");
    $this->db->query("DELETE  FROM pim_p_orders  WHERE p_order_id='".$in['p_order_id']."' ");
    $this->db->query("DELETE  FROM pim_p_order_articles  WHERE p_order_id='".$in['p_order_id']."' ");
    $this->db->query("DELETE FROM note_fields WHERE item_type = 'p_order' AND item_name = 'notes' AND item_id = '".$in['p_order_id']."'");

    
    msg::success ( gm("Purchase Order has been deleted"),'success');
    if($tracking_trace){
          $this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
          $this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
    }
    return true;
  }

    function activate_po_order(&$in){
    if(!$this->delete_order_validate($in)){
      return false;
    }

    $serial_number = generate_serial_number(DATABASE_NAME);
      $this->db->query("UPDATE pim_p_orders SET active='1', serial_number='".$serial_number."' WHERE p_order_id='".$in['p_order_id']."' ");

    msg::success ( gm("Purchase order has been activated"),'success');
    insert_message_log('p_order','{l}Purchase order has been activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
    $in['pagl'] = 'p_order';
    $tracking_trace=$this->db->field("SELECT trace_id FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");
          if($tracking_trace){
              $this->db->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
          }
      return true;
  }

   function delete_order_validate(&$in)
    {
    $v = new validation($in);
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    return $v->run();
    }

    /**
     * Mark as sent
     *
     * @return bool
     * @author Mp
     **/
    function mark_sent(&$in){
    if(!$this->mark_sent_validate($in))
    {
        return false;
    }
    
    $this->db->query("UPDATE pim_p_orders SET rdy_invoice='2', sent='1', send_date='".strtotime($in['sent_date'])."' WHERE p_order_id='".$in['p_order_id']."' ");
        $this->pag = 'p_order';
        $this->field_n = 'p_order_id';
    msg::success( gm('Order has been successfully marked as sent'),'success');
    insert_message_log($this->pag, '{l}Purchase Order has been successfully marked as ready to receive by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
    $in['pagl'] = $this->pag;
    return true;

    }
    function mark_as_draft(&$in)
    {
    if(!$this->delete_order_validate($in)){
        return false;
    }
        $deliveries = $this->db->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' ORDER BY delivery_id DESC ");
    while ($deliveries->next()) {
        $in['delivery_id'] = $deliveries->f('delivery_id');
        $this->delete_entry($in);
    }
    $this->db->query("UPDATE pim_p_orders SET rdy_invoice='0', sent='0',send_date='' WHERE p_order_id='".$in['p_order_id']."' ");
    $this->db->query("UPDATE pim_p_order_articles SET delivered='0' WHERE p_order_id='".$in['p_order_id']."' ");
    }

    /****************************************************************
    * function mark_sent_validate(&$in)                                *
    ****************************************************************/
    function mark_sent_validate(&$in)
    {

    $v = new validation($in);
        $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");

        if(!empty($in['sent_date'])){
          $v->field('sent_date', 'ID', 'required');

          if($v->run()){
              $poDateQ = $this->db->query("SELECT date FROM pim_p_orders
              LEFT JOIN pim_p_order_articles ON pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
              WHERE pim_p_orders.p_order_id='".$in['p_order_id']."'");
              $in['po_date'] = gmdate("Y-m-d\TH:i:s\Z",(int)$poDateQ->f('date'));
              
              $poDate =  new DateTime($in['po_date']);
              $poDate->setTime(0,0,0);
              $poDate = $poDate->format('U')+7200;
              
             if(is_numeric($in['sent_date'])){
                  $in['sent_date'] = $in['sent_date'] / 1000 ; 
                  $sentDate = mktime(0,0,0,
                    date("n",$in['sent_date']),
                    date("j",$in['sent_date']), 
                    date("Y",$in['sent_date'])
                  )+7200;
              } else {
                  $sentDate = new DateTime($in['sent_date']);
                  $sentDate->setTime(0,0,0);
                  $sentDate = $sentDate->format('U')+7200;
              }

              if($sentDate < $poDate){
                msg::error(gm('Sent Date must be equal or be grater than Purchase Order Date'),'error');
                return false;
              }
          }
        }
        
      return $v->run();
    }
    function mark_as_delivered(&$in)
    {
    if(!$this->mark_sent_validate($in)){
        return false;
    }
    

    $this->db->query("UPDATE pim_p_order_articles SET delivered='1' WHERE p_order_id='".$in['p_order_id']."' AND content='0' ");
    $this->db->query("UPDATE pim_p_orders SET rdy_invoice='1' WHERE p_order_id='".$in['p_order_id']."' ");

    insert_message_log('p_order','{l}Order was marked as received by{endl} '.get_user_name($_SESSION['u_id']),'p_order_id',$in['p_order_id']);
    msg::success ( gm('Changes saved'),'success');
    
    }

    function delete_entry(&$in){
    if(!$this->delete_entry_validate($in)){
        return false;
    }

    //check to see if we are allowed to delete serial numbers
    $count_used_serial_no = $this->db->field("SELECT COUNT(id) FROM serial_numbers WHERE p_delivery_id = '".$in['delivery_id']."' AND status_details_2 != '' ");
    if($count_used_serial_no > 0){
        
        msg::success( gm('This delivery has articles that are used'),'notice');
        return false;
    }

    //check to see if we are allowed to delete batch numbers
    $count_used_batch_no = $this->db->field("SELECT COUNT(id) FROM batches WHERE p_delivery_id = '".$in['delivery_id']."' AND status_details_2 != '' ");
    if($count_used_batch_no > 0){
        
        msg::success( gm('This delivery has articles that are used'),'notice');

        return false;
    }

    
    $articles_ids=array();
    $articles = $this->db->query("SELECT * FROM pim_p_orders_delivery WHERE p_order_id ='".$in['p_order_id']."' AND delivery_id='".$in['delivery_id']."' ");
    $delivery_date = $this->db->field("SELECT pim_p_order_deliveries.date FROM pim_p_order_deliveries WHERE p_order_id ='".$in['p_order_id']."' AND delivery_id='".$in['delivery_id']."' ");
    while($articles->next()){
        $order_article_id = $articles->f('order_articles_id');
        $article_id = $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id ='".$order_article_id."'");
         array_push($articles_ids,$article_id);

        if((P_ORDER_DELIVERY_STEPS == 2 && !$in['delivery_approved']) || P_ORDER_DELIVERY_STEPS == 1){
            //delete serial numbers
            $this->db->query("DELETE FROM serial_numbers WHERE article_id = '".$article_id."' AND p_delivery_id = '".$in['delivery_id']."' ");

             $batches = $this->db->query("SELECT * FROM batches_from_orders WHERE p_order_id ='".$in['p_order_id']."' AND p_delivery_id='".$in['delivery_id']."' AND article_id = '".$article_id."'");
             while($batches->next()){
                $location_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$batches->f('address_id')."' and customer_id='0' and batch_id='".$batches->f('batch_id')."'");

                if($location_current_stock_batch->move_next()){
                //update dispach batch from stock
                  $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')-$batches->f('quantity'))."' WHERE article_id='".$article_id."' AND  address_id='".$batches->f('address_id')."' and customer_id='0' and batch_id='".$batches->f('batch_id')."'");
                }
             }

          }

        //delete batch numbers
           $this->db->query("DELETE FROM batches_from_orders WHERE article_id = '".$article_id."' AND p_delivery_id = '".$in['delivery_id']."' ");
          
           $this->db->query("DELETE FROM batches WHERE article_id = '".$article_id."' AND p_delivery_id = '".$in['delivery_id']."' ");
        

        $this->db->query("DELETE FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$order_article_id."' AND delivery_id='".$articles->f('delivery_id')."' ");
        $this->db->query("UPDATE pim_p_order_articles SET delivered = '0' WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$order_article_id."' ");


        $hide_stock =  $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$article_id."'");
        if(ALLOW_STOCK && $hide_stock==0) {
          if((P_ORDER_DELIVERY_STEPS == 2 && !$in['delivery_approved']) || P_ORDER_DELIVERY_STEPS == 1){
              $article_quantity = $articles->f('quantity');

              $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
              $article_data->next();
              $stock = $article_data->f("stock");

              $stock_packing = $article_data->f('packing');
              //$stock_packing = 1;
              if(!ALLOW_ARTICLE_PACKING){
                  $stock_packing = 1;
              }
              $stock_sale_unit = $article_data->f('sale_unit');
              //$stock_sale_unit = 1;
              if(!ALLOW_ARTICLE_SALE_UNIT){
                  $stock_sale_unit = 1;
              }

              $new_stock = $stock - ($article_quantity*$stock_packing/$stock_sale_unit);
              $this->db->query("DELETE FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$order_article_id."' AND delivery_id='".$articles->f('delivery_id')."' ");
              $this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");
              /*start for  stock movements*/
              $order_article_quantity = $this->db->field("SELECT quantity FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND article_id='".$article_id."' ");
              $delivered = $this->db->field("SELECT SUM(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$order_article_id."' ");
              $backorder_articles = ($order_article_quantity - $delivered)*$stock_packing/$stock_sale_unit;
              $now = time();
              $serial_number = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
                  $this->db->insert("INSERT INTO stock_movements SET
                        date            = '".time()."',
                        created_by          = '".$_SESSION['u_id']."',
                        p_order_id         =   '".$in['p_order_id']."',
                        serial_number         =   '".$serial_number."',
                        article_id          = '".$article_id."',
                        article_name            =   '".addslashes($article_data->f("internal_name"))."',
                        item_code           =   '".addslashes($article_data->f("item_code"))."',
                        movement_type       = '7',
                        quantity          =   '".$article_quantity*$stock_packing/$stock_sale_unit."',
                        stock             = '".$stock."',
                        new_stock           =   '".$new_stock."',
                        backorder_articles      =   '".$backorder_articles."'
                  ");
                        /*end for  stock movements*/
                       
                          //we remove the stock in main location

              //$default_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1 ");
              $default_address_id=$this->db->field("SELECT location FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' AND delivery_id='".$in['delivery_id']."' ");
                    if(!$default_address_id){
                      $default_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1 ");
                    }

                    $current_in_stock=$this->db->field("SELECT stock FROM dispatch_stock WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$default_address_id."'");
              $this->db->query("UPDATE dispatch_stock SET stock='".($current_in_stock-($article_quantity*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$default_address_id."'");

            } //end if delivery steps


        }//end if allow stock
    }

    $this->db->query("DELETE FROM pim_p_order_deliveries WHERE p_order_id='".$in['p_order_id']."' AND delivery_id='".$in['delivery_id']."' ");
    $rdy_invoice=$this->db->field("SELECT rdy_invoice FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");

    if($in['del_nr']==1){
        $this->db->query("UPDATE pim_p_orders SET rdy_invoice='".($rdy_invoice=='1' ? '2' : '0')."' WHERE p_order_id='".$in['p_order_id']."' ");
    }else{
        $this->db->query("UPDATE pim_p_orders SET rdy_invoice='2' WHERE p_order_id='".$in['p_order_id']."' ");
    }
    
        
     if(WAC){
         foreach ($articles_ids as $key => $value) {
                generate_purchase_price($value);
          }

      }
        $this->pag = 'p_order';
        $this->field_n = 'p_order_id';
        insert_message_log($this->pag,'{l}Entry was deleted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
     msg::success ( gm("Entry was succesfully deleted"),'success');
        return true;

   }

    function delete_entry_validate(&$in){
    $v = new validation($in);
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    $v->field('delivery_id', 'ID', 'required:exist[pim_p_order_deliveries.delivery_id]', "Invalid Id");

    return $v->run();
    }

     function sendNew(&$in){
      if(!$this->sendNewEmailValidate($in)){
        msg::error ( gm('Invalid email address').' - '.$in['new_email'],'error');
        json_out($in);
        return false;
      }
      global $config;
      $this->db->query("SELECT pim_p_orders.pdf_logo, pim_p_orders.pdf_layout,rdy_invoice
                        FROM pim_p_orders
                        WHERE pim_p_orders.p_order_id ='".$in['p_order_id']."'");

      $this->db->move_next();
      if($this->db->f('pdf_layout')){
        $in['type']=$this->db->f('pdf_layout');
        $in['logo']=$this->db->f('pdf_logo');
      }else {
        $in['type'] = ACCOUNT_ORDER_PDF_FORMAT;
        }
      $ready_invoice=$this->db->f('rdy_invoice');
      $this->generate_pdf($in);
      $mail = new PHPMailer();
      $mail->WordWrap = 50;
      $def_email = $this->default_email();

      //add images to mail
        $xxx=$this->getImagesFromMsg($in['e_message'],'upload');
        foreach($xxx as $location => $value){
          $tmp_start=strpos($location,'/');
          $tmp_end=strpos($location,'.');
          $tmp_cid_name=substr($location,$tmp_start+1,$tmp_end-1-$tmp_start);
          $tmp_file_name=$tmp_cid_name.substr($location, $tmp_end, strlen($location)-$tmp_end);
          $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].$config['install_path'].$location, 'sign_'.$tmp_cid_name, $tmp_file_name); 
          //$in['e_message']=str_replace($value,'<img src="cid:'.'sign_'.$tmp_cid_name.'">',$in['e_message']);
          $in['e_message']=str_replace($value,generate_img_with_cid($value,$tmp_cid_name),$in['e_message']);
        } 

      //$body= stripslashes(htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8')); 
       // $body=stripslashes(utf8_decode($in['e_message']));
        $in['sendgrid_selected'] = true;
      if($in['sendgrid_selected']){
        $body= stripslashes($in['e_message']);
      } else {
        $body= stripslashes(htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8'));
      } 

      if($def_email['from']['email']){
          $mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']),0);
        }
        if($def_email['reply']['email']){
          $mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
          $mail->set('Sender',$def_email['reply']['email']);
        }

     // $subject=$in['e_subject'];
      $subject= stripslashes(utf8_decode($in['e_subject']));
      $mail->Subject = $subject;
      
      if($in['dropbox_files']){
        $path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
        if(!file_exists($path_dropbox)){
          mkdir($path_dropbox,0775,true);
        }
        $dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

          foreach ($in['dropbox_files'] as $key => $value) {
            if($value['checked'] == 1){
              $value['file'] = str_replace('/', '_', $value['file']);

              $file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
              
              $mime = mime_content_type($path_dropbox.$value['file']);
              $mail->AddAttachment($path_dropbox.$value['file'], $value['file']);
            }
          }
      }

      if($in['include_pdf']){
         $mail->AddAttachment("p_order_.pdf", $in['serial_number'].'.pdf'); // attach files/invoice-user-1234.pdf, and rename it to invoice.pdf
      }
      if($in['files']){
        foreach ($in['files'] as $key => $value) {
          if($value['checked'] == 1){
              $mime = mime_content_type($value['path'].$value['file']);
              $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
            }
        }
      }
    

      if($in['file_d_id']){
        $body.="\n\n";
        foreach ($in['file_d_id'] as $key => $value) {
          $body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
        }
      }
        if($in['copy']){
            //$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
          $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
            if($u->f('email')){
              $emails .=$u->f('email').', ';
              $mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
            }
            // $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
        }
            $order = $this->dbu->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");
            $cust_id = $order->f('buyer_id');
            $contact =  $this->dbu->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$order->f('contact_id')."'");
            $title_cont = $this->dbu->field("SELECT name FROM customer_contact_title WHERE id='".$order->f('contact_id')."'");
            $customer =  $this->dbu->field("SELECT name FROM customers WHERE customer_id='".$order->f('buyer_id')."'"); 
            
      if($in['use_html']){
          $mail->IsHTML(true);
          if($contact->f('firstname')==''){
              $contactfirstname='';
            }else{
              $contactfirstname=$contact->f('firstname');
            }
            if($contact->f('lastname')==''){
              $contactlasttname='';
            }else{
              $contactlasttname=$contact->f('lastname');
            }

            if(defined('ACCOUNT_DATE_FORMAT') && ACCOUNT_DATE_FORMAT!=''){
                $date_format = ACCOUNT_DATE_FORMAT;
              }else{
                $date_format ='m/d/Y';
              }
          $head = '<style>p { margin: 0px; padding: 0px; }</style>';
          $body = stripslashes($in['e_message']);
          $body=str_replace('[!CONTACT_FIRST_NAME!]',"".$contactfirstname."",$body);
          $body=str_replace('[!CONTACT_LAST_NAME!]',"".$contactlasttname."",$body);
          $body=str_replace('[!SALUTATION!]',"".$title_cont."",$body);
          $body=str_replace('[!SERIAL_NUMBER!]',"".$order->f('serial_number')."",$body);
          $body=str_replace('[!DATE!]',"".date($date_format , $order->f('date'))."",$body);
          $body=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$body);
          $body=str_replace('[!DUE_DATE!]',"".date($date_format , $order->f('del_date'))."",$body);
          $body=str_replace('[!YOUR_REFERENCE!]',"".$order->f('your_ref')."",$body);

        if($in['copy']){
             $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$body);
            }elseif($in['copy_acc']){
              $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
            }else{
              $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO."\" alt=\"\">",$body);
            }


        if($in['file_d_id']){
          $body .="<br><br>";
          foreach ($in['file_d_id'] as $key => $value) {
            $body.="<p><a href=".$in['file_d_path'][$key].">".$in['file_d_name'][$key]."</a></p>";
          }
        }
        $mail->Body = $head.$body;
      }else{
        $mail->MsgHTML(nl2br(($body)));
      }
/*
        $this->db->query("SELECT customer_contacts.*,pim_p_orders.p_order_id,pim_p_orders.customer_id,pim_p_orders.serial_number,pim_p_orders.pdf_logo, pim_p_orders.pdf_layout
                          FROM pim_p_orders
                          INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_p_orders.customer_id
                          WHERE pim_p_orders.p_order_id ='".$in['p_order_id']."'");
        $this->db->move_next();*/
      // if($in['new_email_id']==$this->db->f('email')){
      //   $mail->AddAddress(trim($this->db->f('email')),$this->db->f('firstname').' '.$this->db->f('lastname'));
      // }else{
        $mail->AddAddress(trim($in['new_email']));
      // }
      $sent_date= time();
      $this->db->query("UPDATE pim_p_orders SET sent='1' WHERE p_order_id='".$in['p_order_id']."'");
      if($ready_invoice!='1'){
        $this->db->query("UPDATE pim_p_orders SET rdy_invoice='2' WHERE p_order_id='".$in['p_order_id']."' ");
      }   

      $msg_log = '{l}Purchase Order has been sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to:{endl} '.$in['new_email'];
      $this->pag = 'p_order';
      $this->field_n = 'p_order_id';

      if($in['sendgrid_selected']){

          $body=$head.$body;

          include_once(__DIR__."/../../misc/model/sendgrid.php");
          $sendgrid = new sendgrid($in, $this->pag, $this->field_n,$in['p_order_id'], $body,$def_email, DATABASE_NAME);

          $sendgrid_data = $sendgrid->get_sendgrid($in);

          if($sendgrid_data['error']){
            msg::error($sendgrid_data['error'],'error');
          }elseif($sendgrid_data['success']){
            msg::success(gm("Email sent by Sendgrid"),'success');
          }

         // $msg_log = $msg_log .= " {l}via SendGrid{endl}.";
          
        }else{
            $mail->Send();
            if($mail->IsError()){
              msg::error ( $mail->ErrorInfo,'error');
              json_out($in);
              return false;
            }
            msg::success( gm('Purchase Order has been successfully sent'),'success');
            if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
             update_log_message($in['logging_id'],' '.$in['email']);
            }else{
              $in['logging_id'] =insert_message_log($this->pag,$msg_log,$this->field_n,$in['p_order_id'],false,$_SESSION['u_id'],true,$cust_id,$sent_date);
            }
        }


      foreach($xxx as $location => $value){
          unlink($location);
        }
        //json_out($in); //commented so that page will refresh
      return true;
    }
    function sendNewEmailValidate(&$in)
    {
      $v = new validation($in);
      $in['email'] = trim($in['email']);
      $v->field('new_email', 'Email', 'email');
      return $v->run();
    }
    function currencyRate($currency=''){
      if(empty($currency)){
        $currency = "USD";
      }
      $separator = ACCOUNT_NUMBER_FORMAT;
      $into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
      return currency::getCurrency($currency, $into, 1,$separator);
    }
    function generate_pdf(&$in)
    {
    include_once(__DIR__.'/../controller/p_order_print.php');
    }
    /**
     * return the default email address
     *
     * @return array
     * @author
     **/
    function default_email()
    {
    $array = array();
    $array['from']['name'] = ACCOUNT_COMPANY;
    $array['from']['email'] = 'noreply@akti.com';
    $array['reply']['name'] = ACCOUNT_COMPANY;
    //$array['reply']['email'] = ACCOUNT_EMAIL;
    $array['reply']['email'] = 'noreply@akti.com';
    /*if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
      $array['from']['email'] = ACCOUNT_EMAIL;
    }*/
    if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
      if(MAIL_SETTINGS_PREFERRED_OPTION==2){
        if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
          $array['from']['email'] = MAIL_SETTINGS_EMAIL;
        }
      }else{
        if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
          $array['from']['email'] = ACCOUNT_EMAIL;
        }
      }
    }
    $this->db->query("SELECT * FROM default_data WHERE type='p_order_email' ");
    if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
        //if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)) && $this->db->f('value')!=''){
          $array['reply']['name'] = $this->db->f('default_name');
          $array['reply']['email'] = $this->db->f('value');
        //}       
        $array['from']['name'] = $this->db->f('default_name');
        //$array['from']['email'] = $this->db->f('value');
        if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)) && $this->db->f('value')!=''){
          $array['from']['email'] = $this->db->f('value');
        }
    }

    $this->db->query("SELECT * FROM default_data WHERE type='bcc_p_order_email' ");
    if($this->db->move_next() && $this->db->f('value')){
      $array['bcc']['email'] = $this->db->f('value'); // pot fi valori multiple, delimitate de ;
    }

    return $array;
    }

    function getImagesFromMsg($msg, $tmpFolderPath)
    {
        $arrSrc = array();
        if (!empty($msg))
        {
            preg_match_all('/<img[^>]+>/i', stripcslashes($msg), $imgTags);
            preg_match_all('/<img[^>]+>/i', $msg, $imgTags_1);

            for ($i=0; $i < count($imgTags[0]); $i++)
            {
                preg_match('/src="([^"]+)/i', $imgTags[0][$i], $withSrc);
                //Remove src
                $withoutSrc = str_ireplace('src="', '', $withSrc[0]);

                //data:image/png;base64,
                if (strpos($withoutSrc, ";base64,"))
                {
                    //data:image/png;base64,.....
                    list($type, $data) = explode(";base64,", $withoutSrc);
                    //data:image/png
                    list($part, $ext) = explode("/", $type);
                    //Paste in temp file
                    $withoutSrc = $tmpFolderPath."/".uniqid("temp_").".".$ext;
                    @file_put_contents($withoutSrc, base64_decode($data));
                    $arrSrc[$withoutSrc] = $imgTags_1[0][$i];
                }      
                //$arrSrc[$withoutSrc] = $imgTags_1[0][$i];
            }
        }
        return $arrSrc;
    }

function make_entry(&$in)
    {


    if(!$this->make_entry_validate($in)){
        return false;
    }

    if(!$in['confirm_over_total_delivery']) {
        if (!$this->check_entry_quantities($in)) {
            json_out($in);
        }
    }

/*printvar($in); exit();*/
    $const =array();
    $const['AUTO_AAC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='AUTO_AAC' ");
    $const['WAC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='WAC' ");
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
    $const['P_ORDER_DELIVERY_STEPS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='P_ORDER_DELIVERY_STEPS' ");
    
    $in['order_articles_id']=array();
    $in['article_name']=array();
    $in['quantity']=array();
    $in['seted_quantity']=array();
    $in['old_quantity']=array();
        $in['selected_s_n']=array();
        $in['selected_b_n']=array();
    

    foreach($in['lines'] as $key_line => $key_value){
               array_push($in['order_articles_id'],$key_value['order_articles_id']);
               

    }
    foreach ($in['order_articles_id'] as $o_key => $o_value) {
          $in['article_name'][$o_value]=$in['lines'][$o_key]['article'];
          $in['quantity'][$o_value]=$in['lines'][$o_key]['quantity'];
          $in['old_quantity'][$o_value]=$in['lines'][$o_key]['quantity'];
              $in['selected_s_n'][$o_value]=$in['lines'][$o_key]['selected_s_n'];
              $in['selected_b_n'][$o_value]=$in['lines'][$o_key]['selected_b_n'];
         
          
    }


    $location_batch=explode("-",$in['select_main_location']);
    $now = time();
    $delivery_done = 0;
    if($const['P_ORDER_DELIVERY_STEPS']==2 && $in['delivery_approved']){
      $delivery_done = 1;
    }
    if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
        $in['date_del_line_h'] = strtotime($in['date_del_line']);
    }


    $del = $this->db->insert("INSERT INTO pim_p_order_deliveries SET
      p_order_id='".$in['p_order_id']."',
      date='".$in['date_del_line_h']."',
      hour = '".$in['hour']."',
      minute = '".$in['minute']."',
      pm = '".$in['pm']."', 
       delivery_done  = '".$delivery_done."' ");
    $this->db->query("UPDATE pim_p_orders SET rdy_invoice='2' WHERE p_order_id='".$in['p_order_id']."' ");
        
       

    foreach ($in['quantity'] as $key => $value) {
          

        if(return_value($value) != '0.00'){

            $article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
            $hide_stock =  $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$article_id."'");

            $use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id='".$article_id."'");
            $use_batch_no = $this->db->field("SELECT use_batch_no FROM pim_articles WHERE article_id='".$article_id."'");

            $serial_number = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
            if($const['ALLOW_STOCK'] && $hide_stock==0){

              if($const['P_ORDER_DELIVERY_STEPS'] == 2 ){
                  $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
                  $article_data->next();
                  $stock = $article_data->f("stock");

                  $stock_packing = $article_data->f('packing');
                 // $stock_packing = 1;
                  if(!$const['ALLOW_ARTICLE_PACKING']){
                      $stock_packing = 1;
                  }
                  $stock_sale_unit = $article_data->f('sale_unit');
                  //$stock_sale_unit = 1;
                  if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
                      $stock_sale_unit = 1;
                  }
                    if($in['delivery_approved']){
                        $new_stock = $stock + (return_value($in['quantity'][$key])*$stock_packing/$stock_sale_unit);
                        $this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
                        /*start for  stock movements*/
                        $backorder_articles = (return_value($in['old_quantity'][$key]) - return_value($in['quantity'][$key])) * $stock_packing/$stock_sale_unit;
                                $this->db->insert("INSERT INTO stock_movements SET
                                  date            = '".time()."',
                                  created_by          = '".$_SESSION['u_id']."',
                                  p_order_id          =   '".$in['p_order_id']."',
                                  serial_number         =   '".$serial_number."',
                                  article_id          = '".$article_id."',
                                  article_name            =   '".addslashes($article_data->f("internal_name"))."',
                                  item_code           =   '".addslashes($article_data->f("item_code"))."',
                                  movement_type       = '6',
                                  quantity          =   '".return_value($in['quantity'][$key])*$stock_packing/$stock_sale_unit."',
                                  stock             = '".$stock."',
                                  new_stock           =   '".$new_stock."',
                                  backorder_articles      =   '".$backorder_articles."',
                                  delivery_date       ='".$in['date_del_line_h']."'
                            ");
                      }      
                  }else{
                     $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
                    $article_data->next();
                    $stock = $article_data->f("stock");

                    $stock_packing = $article_data->f('packing');
                   // $stock_packing = 1;
                    if(!$const['ALLOW_ARTICLE_PACKING']){
                        $stock_packing = 1;
                    }
                    $stock_sale_unit = $article_data->f('sale_unit');
                    //$stock_sale_unit = 1;
                            if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
                        $stock_sale_unit = 1;
                    }

                    $new_stock = $stock + (return_value($in['quantity'][$key])*$stock_packing/$stock_sale_unit);
                    $this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
                    /*start for  stock movements*/
                    $backorder_articles = (return_value($in['old_quantity'][$key]) - return_value($in['quantity'][$key])) * $stock_packing/$stock_sale_unit;
                            $this->db->insert("INSERT INTO stock_movements SET
                              date            = '".time()."',
                              created_by          = '".$_SESSION['u_id']."',
                              p_order_id          =   '".$in['p_order_id']."',
                              serial_number         =   '".$serial_number."',
                              article_id          = '".$article_id."',
                              article_name            =   '".addslashes($article_data->f("internal_name"))."',
                              item_code           =   '".addslashes($article_data->f("item_code"))."',
                              movement_type       = '6',
                              quantity          =   '".return_value($in['quantity'][$key])*$stock_packing/$stock_sale_unit."',
                              stock             = '".$stock."',
                              new_stock           =   '".$new_stock."',
                              backorder_articles      =   '".$backorder_articles."',
                              delivery_date       ='".$in['date_del_line_h']."'
                        ");
                  } // end delivery steps
            
             } // end allow stock

            # check to see if the value is bigger than what we have
            $q = return_value($value);
            $line = $this->db->field("SELECT quantity FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' AND content='0' ");
            $delivered = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' ");
            if( $q+$delivered > $line){
                //$q = $line - $delivered;
            }
            # update deliveries
            $this->db->query("INSERT INTO pim_p_orders_delivery SET p_order_id='".$in['p_order_id']."' , order_articles_id='".$key."', quantity='".$q."', delivery_id='".$del."', delivery_note='".$in['delivery_note']."' ");

            if(($const['P_ORDER_DELIVERY_STEPS'] == 2 && $in['delivery_approved']) ){
               $this->db->query("UPDATE serial_numbers SET p_delivery_id='".$del."' WHERE p_delivery_id='".$in['delivery_id']."' ");
            }

            # serial numbers
            if($use_serial_no == 1 && $const['ALLOW_STOCK'] ){
                
                //$in['serial_numbers'][$key] = array_slice(array_reverse($in['serial_numbers'][$key]), 0,intval($q));
                //$status_details_1 = $serial_number;
                foreach ($in['selected_s_n'][$key] as $key2 => $info) {
                $this->db->query("INSERT INTO serial_numbers
                  SET   serial_number     ='".trim($info['serial_number'])."' ,
                  article_id      ='".$article_id."',
                  date_in       ='".$now."',
                  status_id       ='1',
                  status_details_1    ='".trim($serial_number)."',
                  p_delivery_id     ='".$del."'  ");
                }
            }

            #batch numbers
            if($use_batch_no == 1 && $const['ALLOW_STOCK'] ){
                      //$stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id='".$article_id."' ");
                     /* $exist_batch = $this->db->field("SELECT count(*) FROM batches WHERE article_id='".$article_id."' ");
                        //if there is no batch, we reset the article stock and the user will add the batches and the coresponding stock manually (for that case when the option "use batches" was not checked from the begining)
                        if($exist_batch==0 ){
                          $this->db->query("UPDATE pim_articles SET stock=0 WHERE article_id='".$article_id."' ");
                        }*/
                    
                //$count_b_n = count($in['selected_b_n_v'][$key]);

                $status_details_1 = $serial_number;
             
                         $i=0;

                          
                   foreach ($in['selected_b_n'][$key] as $key2 => $info) {
                        if(isset($info['date_h']) && !empty($info['date_h']) ){
                                $in['date_h'] = is_numeric($info['date_h']) ? $info['date_h'] : strtotime($info['date_h']);
                          }

                          $quantity_batch = return_value($info['batch_number_q'])*$stock_packing/$stock_sale_unit;

                          $in['batch_id'] = $this->db->insert("INSERT INTO batches SET
                            batch_number    = '".$info['batch_number_v']."',
                            article_id      = '".$article_id."',
                            quantity      = '".$quantity_batch."',
                            in_stock      = '".$quantity_batch."',
                            p_delivery_id     = '".$del."',
                            date_in       ='".$now."',
                            date_exp      ='".$in['date_h']."',
                            status_id       = '1',
                            active      = '1',
                            status_details_1    ='".$status_details_1."'
                          ");

                          $this->db->query("INSERT INTO batches_from_orders SET
                            order_articles_id =     '".$key."',
                            article_id        =     '".$article_id."',
                            batch_id          =     '".$in['batch_id']."',
                            p_delivery_id     =     '".$del."',
                            p_order_id        =     '".$in['p_order_id']."',
                            return_id         =       '0',
                            address_id        =     '".$location_batch[1]."',
                            quantity          =     '".$quantity_batch."'  ");


                          $batch_current_in_stock=$this->db->field("SELECT quantity FROM batches_dispatch_stock WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$location_batch[1]."' AND batch_id= '".$in['batch_id']."'");
                          $is_main= $this->db->field("SELECT dispatch_stock_id FROM batches_dispatch_stock WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$location_batch[1]."' AND batch_id= '".$in['batch_id']."'");
                          if($is_main){
                               if(($const['P_ORDER_DELIVERY_STEPS'] == 2 && $in['delivery_approved']) || $const['P_ORDER_DELIVERY_STEPS'] == 1){
                                  $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($batch_current_in_stock+$quantity_batch)."' WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$location_batch[1]."' AND batch_id= '".$in['batch_id']."'");
                                }
                            }else{
                              if(($const['P_ORDER_DELIVERY_STEPS'] == 2 && $in['delivery_approved']) || $const['P_ORDER_DELIVERY_STEPS'] == 1){
                                  $this->db->query("INSERT INTO batches_dispatch_stock SET quantity='".($batch_current_in_stock+$quantity_batch)."' , article_id='".$article_id."' , customer_id=0 ,  address_id='".$location_batch[1]."' , batch_id= '".$in['batch_id']."'");
                               }
                              }
                   }
                      
            }
        $filter_2 ='';
        if($const['P_ORDER_DELIVERY_STEPS'] == 2  ){
               $filter_2 =" AND pim_p_order_deliveries.delivery_done = '1' ";
            }

        $delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery
          LEFT JOIN pim_p_order_deliveries ON pim_p_order_deliveries.delivery_id = pim_p_orders_delivery.delivery_id 
          WHERE pim_p_orders_delivery.p_order_id='".$in['p_order_id']."' AND pim_p_orders_delivery.order_articles_id='".$key."' ".$filter_2);
        # delivery complete
        if($delivered_all > $line || $delivered_all == $line ){
            $this->db->query("UPDATE pim_p_order_articles SET delivered='1' WHERE order_articles_id='".$key."' ");
        }
        //we add the stock in main location
        //$default_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1 ");
        $location=explode("-", $in['select_main_location']);
        
        $default_address_id=$location[1];
        $this->db->query("UPDATE pim_p_orders_delivery SET location='".$default_address_id."' WHERE  delivery_id   ='".$del."'");
        $this->db->query("UPDATE  pim_p_order_deliveries SET location='".$default_address_id."' WHERE  delivery_id   ='".$del."'");


        $current_in_stock=$this->db->field("SELECT stock FROM dispatch_stock WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$default_address_id."'");
        $is_main= $this->db->field("SELECT dispatch_stock_id FROM dispatch_stock WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$default_address_id."'");
        if($is_main){
             if(($const['P_ORDER_DELIVERY_STEPS'] == 2 && $in['delivery_approved']) || $const['P_ORDER_DELIVERY_STEPS'] == 1){
                $this->db->query("UPDATE dispatch_stock SET stock='".($current_in_stock+return_value($value)*($stock_packing/$stock_sale_unit ))."' WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$default_address_id."'");
              }
          }else{
            if(($const['P_ORDER_DELIVERY_STEPS'] == 2 && $in['delivery_approved']) || $const['P_ORDER_DELIVERY_STEPS'] == 1){
                $this->db->query("INSERT INTO dispatch_stock SET stock='".($current_in_stock+return_value($value)*($stock_packing/$stock_sale_unit ))."' , article_id='".$article_id."' , customer_id=0 , main_address=1, address_id='".$default_address_id."'");
             }
            }
           

      } // end if return value
    }// end foreach
     

         $articles = $this->db->field("SELECT count(order_articles_id) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' AND tax_id='0' AND has_variants='0'");
         $articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' AND tax_id='0'");


        if($articles == $articles_delivered){
             $this->db->query("UPDATE pim_p_orders SET rdy_invoice='1' WHERE p_order_id='".$in['p_order_id']."' ");
        }

        $this->pag = 'p_order';
        $this->field_n = 'p_order_id';

        $this->pag = 'p_order';
        $this->field_n = 'p_order_id';

    insert_message_log($this->pag,'{l}Entry made by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
   if($const['WAC']){
     foreach ($in['quantity'] as $key => $value) {
      if(return_value($value) != '0.00'){
        $article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
         generate_purchase_price($article_id);
      }
    }
  }
   if($const['AUTO_AAC']){
     $total=0;
     if ($in['p_price']){
        foreach ($in['p_price'] as $key => $value) {
           $this->db->query("UPDATE  pim_p_order_articles SET price='".return_value($value)."',price_with_vat='".return_value($value)."' WHERE order_articles_id='".$key."'");
             $total=$total+(return_value($value)*return_value($in['quantity'][$key]));
             /* $article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
                 generate_aac_price($article_id);*/
        }
         $total=$total+return_value($in['add_cost']);
        $this->db->query("UPDATE pim_p_orders SET amount='".$total."',add_cost='".return_value($in['add_cost'])."' WHERE p_order_id='".$in['p_order_id']."' ");
          foreach ($in['redistributed_cost'] as $key => $value) {
           $this->db->query("UPDATE  pim_p_order_articles SET redistributed_cost='".return_value($value)."' WHERE order_articles_id='".$key."'");
        
        }

        foreach ($in['p_price'] as $key => $value) {
         
              $article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
                 generate_aac_price($article_id);
        }
     }
       


   }

    return true;

   }

    function check_entry_quantities(&$in){
        $is_ok=true;
        foreach ($in['lines'] as $key => $value) {
            $entered = $this->db->field("SELECT COALESCE(SUM(quantity),0) FROM pim_p_orders_delivery WHERE p_order_id='" . $in['p_order_id'] . "' AND order_articles_id='" . $value['order_articles_id'] . "' ");
            $article_quantity = $this->db->field("SELECT quantity FROM pim_p_order_articles WHERE p_order_id='" . $in['p_order_id'] . "' AND order_articles_id='" . $value['order_articles_id'] . "' ");
            if ($article_quantity > 0) {
                if ($entered + return_value($value['quantity']) > $article_quantity) {
                    $is_ok = false;
                    msg::notice('Quantities exceed total', 'notice');
                    break;
                }
            }
        }
        return $is_ok;
    }

    /**
     * validate endtry
     *
     * @return bool
     * @author me
     **/
    function make_entry_validate(&$in)
    {

    $v = new validation($in);
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    $sum_of_delivery_items = 0;
    $articles = 0;
    foreach ($in['lines'] as $key => $value) {
        $articles++;
        $sum_of_delivery_items += (return_value($value['quantity']));
    }
    if($sum_of_delivery_items==0 && $articles == 0){
        return false;
    }


    // #validate for serial_numbers  and batch numbers before return $v->run
    $err = false;
    $err2 = false;

    $const['P_ORDER_DELIVERY_STEPS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='P_ORDER_DELIVERY_STEPS' ");

    if(($const['P_ORDER_DELIVERY_STEPS'] == 2 && !$in['delivery_approved']) || $const['P_ORDER_DELIVERY_STEPS'] == 1){
        
        foreach ($in['order_articles_id'] as $key => $order_articles_id) {
            if(intval(return_value($in['quantity'][$order_articles_id]))>0){
                $article_id = $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id = '".$order_articles_id."' ");

                //serial numbers
                $use_art_s_n = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$article_id."' ");
                if($use_art_s_n==1 && ALLOW_STOCK ){
                  if(intval(return_value($in['quantity'][$order_articles_id])) > count($in['serial_numbers'][$order_articles_id])){
                      $error_match .= $in['article_name'][$order_articles_id].'<br />';
                      $err = true;
                  }
                }

              //batch numbers
              $use_art_b_n = $this->db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$article_id."' ");
              if($use_art_b_n==1 && ALLOW_STOCK ){
                $quant = 0;
                if(!is_array($in['selected_b_n_q'][$order_articles_id])){
                    $in['selected_b_n_q'][$order_articles_id] = array($in['selected_b_n_q'][$order_articles_id]);
                }
                foreach ($in['selected_b_n_q'][$order_articles_id] as $key => $quants) {
                    $quant += $quants;
                }
                if(intval(return_value($in['quantity'][$order_articles_id])) > $quant ){
                    $error_match2 .= $in['article_name'][$order_articles_id].'<br />';
                    $err2 = true;
                }
              }

           }
        }
      }  
        //serial numbers
        if($err==true){
            msg::success(gm("Serial Number").' '.gm("quantities don't match for").': <br />'.rtrim($error_match,'<br />'),'error');
            msg::success(gm("Serial Number").' '.gm("quantities don't match for").': <br />'.rtrim($error_match,'<br />'),'error');
            return false;
        }

        //batch numbers
        if($err2==true){
            msg::success(gm("Batch Number").' '.gm("quantities don't match for").': <br />'.rtrim($error_match,'<br />'),'error');
            return false;
        }

    return $v->run();
    }

    function edit_delivery(&$in)
    {
    if(!$this->edit_delivery_validate($in)){
        return false;
    }
        // printvar($in);exit();
    //Sync::start(ark::$model.'-'.ark::$method);
    $now = time();
    if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
            $in['date_del_line_h'] = strtotime($in['date_del_line']);
        }
    $this->db->query("UPDATE  pim_p_order_deliveries SET
      `date` = '".$in['date_del_line_h']."',
      `hour` = '".$in['hour']."',
      `minute` = '".$in['minute']."',
      `pm` = '".$in['pm']."'
      WHERE p_order_id='".$in['p_order_id']."' and delivery_id='".$in['delivery_id']."' ");
    if($in['delivery_note']){
       $this->db->query("UPDATE  pim_p_orders_delivery
                      SET     `delivery_note` = '".$in['delivery_note']."'
                              WHERE       p_order_id='".$in['p_order_id']."' and delivery_id='".$in['delivery_id']."' ");
    }


    $in['order_articles_id']=array();
    $in['article_name']=array();
    $in['quantity']=array();
    $in['seted_quantity']=array();
    $in['old_quantity']=array();
    

    foreach($in['lines'] as $key_line => $key_value){
               array_push($in['order_articles_id'],$key_value['order_articles_id']);

    }
    foreach ($in['order_articles_id'] as $o_key => $o_value) {
          $in['article_name'][$o_value]=$in['lines'][$o_key]['article'];
          $in['quantity'][$o_value]=$in['lines'][$o_key]['quantity'];
          $in['old_quantity'][$o_value]=$in['lines'][$o_key]['quantity'];

         
          
    }

    insert_message_log($this->pag,'{l}Entry made by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
    
    if(P_ORDER_DELIVERY_STEPS == 2 && $in['delivery_approved']){
      if($this->approve_delivery($in) ){
         return true;
      };

    }


 /*   $this->db->query("UPDATE pim_p_orders SET rdy_invoice='2' WHERE p_order_id='".$in['p_order_id']."' ");
    $j=0;
    foreach ($in['quantity'] as $key => $value) {
        if(return_value($value)){

  $article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");

  if(ALLOW_STOCK){
  $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
  $article_data->next();
  $stock = $article_data->f("stock");

  // $stock_packing = $article_data->f("packing");
  $stock_packing = 1;
  if(!ALLOW_ARTICLE_PACKING){
      $stock_packing = 1;
  }
  // $stock_sale_unit = $article_data->f("sale_unit");
  $stock_sale_unit = 1;
  if(!ALLOW_ARTICLE_SALE_UNIT){
      $stock_sale_unit = 1;
  }

  $reset_stock = $stock - (return_value($in['seted_quantity'][$key])*$stock_packing/$stock_sale_unit);
  $new_stock = $reset_stock + (return_value($in['quantity'][$key])*$stock_packing/$stock_sale_unit);
  $this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
  }
  $this->db->query("UPDATE pim_p_order_articles SET delivered='0' WHERE order_articles_id='".$key."' ");
  # check to see if the value is bigger than what we have
  $q = return_value($value);
  $line = $this->db->field("SELECT quantity FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' AND content='0' ");
  $delivered = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' ");
  if( $q+$delivered > $line){
      //$q = $line - $delivered;
  }
  # update deliveries
  $this->db->query("UPDATE pim_p_orders_delivery SET quantity='".$q."' , delivery_note='".$in['delivery_note']."' WHERE p_order_id='".$in['p_order_id']."' AND delivery_id='".$in['delivery_id']."' AND order_articles_id='".$key."' ");

  # serial numbers
  $use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id='".$article_id."'");
  if($use_serial_no == 1 && ALLOW_STOCK){
      $del = $in['delivery_id'];
      $k=0;
      $in['serial_numbers'][$key] = array_reverse($in['serial_numbers'][$key],true);
      foreach ($in['serial_numbers'][$key] as $key2 => $serial_no) {
      // $status_details_1 = '<a href="index.php?do=order-p_order&p_order_id='.$in['p_order_id'].'" target="_blank" >'.$serial_number.'</a>';
      $status_details_1 = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."' ");
      if($k<$q){//add or edit
          if(substr($key2, 0,3)=='add'){//add
    $this->db->query("INSERT INTO serial_numbers
        SET   serial_number     ='".trim($serial_no)."' ,
        article_id      ='".$article_id."',
        date_in       ='".$now."',
        status_id       ='1',
        status_details_1    ='".$status_details_1."',
        p_delivery_id     ='".$del."'  ");
          }else{//edit
    $this->db->query("UPDATE serial_numbers
        SET   serial_number     ='".trim($serial_no)."' ,
        date_in       ='".$now."'
        WHERE id        = '".$key2."'  ");
          }
      }else{//delete
          $this->db->query("DELETE FROM serial_numbers WHERE id = '".$key2."' ");
      }
      $k++;
      }

  }


        
  $delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_p_orders_delivery WHERE p_order_id='".$in['p_order_id']."' AND order_articles_id='".$key."' ");
  # delivery complete
  if($delivered_all > $line || $delivered_all == $line ){
      $this->db->query("UPDATE pim_p_order_articles SET delivered='1' WHERE order_articles_id='".$key."' ");
  }

        $hide_stock =  $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$article_id."'");
  if(ALLOW_STOCK && $hide_stock==0){
      if($in['old_quantity'][$key] != $in['quantity'][$key]){
      //start for  stock movements
      $backorder_articles = ($line - $delivered_all)*$stock_packing/$stock_sale_unit;
      $serial_number = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
          $this->db->insert("INSERT INTO stock_movements SET
                date            = '".$now."',
                p_order_id          =   '".$in['p_order_id']."',
                serial_number         =   '".$serial_number."',
                article_id          = '".$article_id."',
                article_name            =   '".addslashes($article_data->f("internal_name"))."',
                item_code           =   '".addslashes($article_data->f("item_code"))."',
                movement_type       = '10',
                quantity          =   '".return_value($in['quantity'][$key])*$stock_packing/$stock_sale_unit."',
                stock             = '".$stock."',
                new_stock           =   '".$new_stock."',
                backorder_articles      =   '".$backorder_articles."'
          ");
         // end for  stock movements
             // $default_address_id=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default=1 ");
                      $location=explode("-", $in['select_main_location']);
        
                      $default_address_id=$location[1];
                  $this->db->query("UPDATE pim_p_orders_delivery SET location='".$default_address_id."' WHERE  delivery_id='".$in['delivery_id']."'");
          $this->db->query("UPDATE  pim_p_order_deliveries SET location='".$default_address_id."' WHERE  delivery_id='".$in['delivery_id']."'");
                  
                  

  $current_in_stock=$this->db->field("SELECT stock FROM dispatch_stock WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$default_address_id."'");


  $this->db->query("UPDATE dispatch_stock SET stock='".($current_in_stock-(return_value($in['old_quantity'][$key])- return_value($in['quantity'][$key])*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."' AND customer_id=0 and address_id='".$default_address_id."'");
      if(WAC){
               generate_purchase_price($article_id);
                   }

                }
          }
        }
    }
        
 if(AUTO_AAC){

       $total=0;
       foreach ($in['p_price'] as $key => $value) {
                 $this->db->query("UPDATE  pim_p_order_articles SET price='".return_value($value)."',price_with_vat='".return_value($value)."' WHERE order_articles_id='".$key."'");
                 $total=$total+(return_value($value)*return_value($in['quantity'][$key]));
                 //$article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
                 //generate_aac_price($article_id);
          }
        $total=$total+return_value($in['add_cost']);
        
        $this->db->query("UPDATE pim_p_orders SET amount='".$total."' ,add_cost='".return_value($in['add_cost'])."' WHERE p_order_id='".$in['p_order_id']."' ");   
        
        foreach ($in['redistributed_cost'] as $key => $value) {
           $this->db->query("UPDATE  pim_p_order_articles SET redistributed_cost='".return_value($value)."' WHERE order_articles_id='".$key."'");
        }  

      foreach ($in['p_price'] as $key => $value) {
               
                 $article_id =  $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id='".$key."'");
                 generate_aac_price($article_id);
          }

   }


    $articles = $this->db->field("SELECT count(order_articles_id) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' ");
    $articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' AND content='0' ");

    if($articles == $articles_delivered){
        $this->db->query("UPDATE pim_p_orders SET rdy_invoice='1' WHERE p_order_id='".$in['p_order_id']."' ");
    }*/
    
    

    }

    /**
     * validate endtry
     *
     * @return bool
     * @author me
     **/
    function edit_delivery_validate(&$in)
    {

    $v = new validation($in);
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    $v->field('delivery_id', 'ID', 'required:exist[pim_p_order_deliveries.delivery_id]', "Invalid Id");
    $sum_of_delivery_items = 0;
    $articles = 0;
    foreach ($in['lines'] as $key => $value) {
        if(return_value($value)<0) {
  //return false;
        }
        $articles++;
        $sum_of_delivery_items += (return_value($value['quantity']));
    }
    if($sum_of_delivery_items==0 && $articles == 0){
        return false;
    }else{

        // #validate for serial_numbers and batch_numbers before return $v->run
        $err = false;
        $err2 = false;
        foreach ($in['order_articles_id'] as $key => $order_articles_id) {
  if(intval(return_value($in['quantity'][$order_articles_id]))>0){
      $article_id = $this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id = '".$order_articles_id."' ");
      //serial numbers
      $use_art_s_n = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$article_id."' ");
      if($use_art_s_n==1 && ALLOW_STOCK ){
      if(intval(return_value($in['quantity'][$order_articles_id])) > count($in['serial_numbers'][$order_articles_id])){
          $error_match .= $in['article_name'][$order_articles_id].'<br />';
          $err = true;
      }
      }
      //batch numbers
      $use_art_b_n = $this->db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$article_id."' ");

      if($use_art_b_n==1 && ALLOW_STOCK ){
      if(intval(return_value($in['quantity'][$order_articles_id])) != array_sum($in['batch_numbers_q'][$order_articles_id])){
          $error_match2 .= $in['article_name'][$order_articles_id].'<br />';
          $err2 = true;
      }
      }
  }
        }
        //serial numbers
        if($err==true){
  msg::success(gm("Serial Number").' '.gm("quantities don't match for").': <br />'.rtrim($error_match,'<br />'),'error');
  return false;
        }

        //batch numbers
        if($err2==true){
  msg::success(gm("Batch Number").' '.gm("quantities don't match for").': <br />'.rtrim($error_match2,'<br />'),'error');
  return false;
        }


    return $v->run();
    }

    }

      function approve_delivery(&$in){
          if(!$this->make_entry_validate($in) || !$in['delivery_id']){
            return false;
          }
          if(!$this->delete_entry($in)){
            return false;
          }
          if(!$this->make_entry($in)){
            return false;
          }

          if($in['approve_from_list']==1){
            $in['is_art_in_back'] = $this->db->field("  SELECT COUNT(p_order_id) FROM pim_p_orders
                          WHERE p_order_id= '".$in['p_order_id']."'
                          AND rdy_invoice!='1'
                          AND sent=1
                          AND pim_p_orders.active=1
                          GROUP BY pim_p_orders.p_order_id");
          }
          msg::success ( gm('Entry was approved'),'success');

          return true;
        }

      function uploadify(&$in)
      {
        $response=array();
        if (!empty($_FILES)) {
          $tempFile = $_FILES['Filedata']['tmp_name'];
          // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
          $targetPath = 'upload/'.DATABASE_NAME;
          $in['name'] = 'po_logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
          $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
          // Validate the file type
          $fileTypes = array('jpg','jpeg','gif'); // File extensions
          $fileParts = pathinfo($_FILES['Filedata']['name']);
          @mkdir(str_replace('//','/',$targetPath), 0775, true);
          if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
            move_uploaded_file($tempFile,$targetFile);
            global $database_config;

            $database_2 = array(
              'hostname' => $database_config['mysql']['hostname'],
              'username' => $database_config['mysql']['username'],
              'password' => $database_config['mysql']['password'],
              'database' => DATABASE_NAME,
            );
            $db_upload = new sqldb($database_2);
            $logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_PO_ORDER' ");
            if($logo == ''){
              if(defined('ACCOUNT_LOGO')){
                $db_upload->query("UPDATE settings SET
                                   value = 'upload/".DATABASE_NAME."/".$in['name']."'
                                   WHERE constant_name='ACCOUNT_LOGO_PO_ORDER' ");
              }else{
                $db_upload->query("INSERT INTO settings SET
                                   value = 'upload/".DATABASE_NAME."/".$in['name']."',
                                   constant_name='ACCOUNT_LOGO_PO_ORDER' ");
              }
            }
            $response['success'] = 'success';
            echo json_encode($response);
            // ob_clean();
          } else {
            $response['error'] = gm('Invalid file type.');
            echo json_encode($response);
            // echo gm('Invalid file type.');
          }
        }
      }
      function delete_logo(&$in)
      {
          if($in['name'] == ACCOUNT_LOGO){
            msg::error ( gm('You cannot delete the default logo'),'error');
            json_out($in);
            return true;
          }
          if($in['name'] == '../img/no-logo.png'){
            msg::error ( gm('You cannot delete the default logo'),'error');
            json_out($in);
            return true;
          }
          $exists = $this->dbu->field("SELECT COUNT(service_id) FROM servicing_support WHERE pdf_logo='".$in['name']."' ");
          if($exists>=1){
            msg::error ( gm('This logo is set for one ore more invoices.'),'error');
            json_out($in);
            return false;
          }
          @unlink($in['name']);
          msg::success ( gm('File deleted'),'success');
          return true;
      }
      function set_default_logo(&$in)
      {

          $this->dbu->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_LOGO_PO_ORDER' ");
          msg::success (gm("Default logo set."),'success');
          json_out($in);
          return true;
      }
      function default_pdf_format_header(&$in)
      {


          $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PO_PDF' ");
          $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_PO_HEADER_PDF_FORMAT'");
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PO_PDF_MODULE'");
          global $config;
          msg::success ( gm('Changes saved'),'success');
        return true;
      }
      function default_pdf_format_body(&$in)
      {
          $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PO_PDF' ");
          $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_PO_BODY_PDF_FORMAT'");
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PO_PDF_MODULE'");
          global $config;

          msg::success ( gm('Changes saved'),'success');
        return true;
      }
      function default_pdf_format_footer(&$in)
      {

          $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_PO_PDF' ");
          $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_PO_FOOTER_PDF_FORMAT'");
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_PO_PDF_MODULE'");
          global $config;
          msg::success ( gm('Changes saved'),'success');
         
        return true;
      }
      function reset_data(&$in){
        if($in['header'] && $in['header']!='undefined'){
          $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
        }elseif($in['footer'] && $in['footer']!='undefined'){
          $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='po' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
        }
        $result=array("var_data" => $variable_data);
        json_out($result);
        return true;
      }

      function pdfSaveData(&$in){

        if($in['header']=='header'){
          $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='po' AND type='".$in['header']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
          $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
          if($exist){
            $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
          }else{
            $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='po',
                              type='".$in['header']."',
                              content='".$in['variable_data']."',
                              initial='1', 
                              layout='".$in['layout']."',
                              `default`='1',
                            identity_id='".$in['identity_id']."' ");
          }
        }else{
          $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='po' AND type='".$in['footer']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
          $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='po' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

          if($exist){
            $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
          }else{
            $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='po',
                              type='".$in['footer']."',
                              content='".$in['variable_data']."',
                              initial='1', 
                              layout='".$in['layout']."',
                              `default`='1',
                            identity_id='".$in['identity_id']."' ");
          }
        }
        msg::success ( gm('Data saved'),'success');
        return true;
      }

    function Convention(&$in){

    $this->dbu->query("UPDATE settings SET value='".$in['ACCOUNT_P_ORDER_START']."' WHERE constant_name='ACCOUNT_P_ORDER_START' ");
    $this->dbu->query("UPDATE settings SET value='".$in['ACCOUNT_P_ORDER_DIGIT_NR']."' WHERE constant_name='ACCOUNT_P_ORDER_DIGIT_NR'");
    $this->dbu->query("UPDATE settings SET value='".$in['CHECKED']."' WHERE constant_name='ACCOUNT_P_ORDER_REF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['ACCOUNT_P_ORDER_DEL']."' WHERE constant_name='ACCOUNT_P_ORDER_DEL' ");
        msg::success ( gm("Changes have been saved."),'success');
        return true;
    }
    function default_language(&$in){
       msg::success ( gm("Changes have been saved."),'success');
        return true;
    }

      function save_language(&$in){
        switch ($in['languages']){
          case '1':
            $lang = '';
            break;
          case '2':
            $lang = '_2';
            break;
          case '3':
            $lang = '_3';
            break;
          case '4':
            $lang = '_4';
            break;
          default:
            $lang = '';
            break;
        }

        # extra check for custom_langs
        if($in['languages']>=1000) {
          $lang = '_'.$in['languages'];
        }

        $entry_exist=$this->dbu->field("SELECT default_id FROM default_data WHERE type='p_order_note".$lang."' AND default_main_id='0' ");
        if($entry_exist){
          $this->dbu->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='p_order_note".$lang."' AND default_main_id='0' ");
        }else{
          $this->dbu->query("INSERT INTO default_data SET value='".$in['notes']."',type='p_order_note".$lang."', default_main_id='0' ");
        }       
        msg::success ( gm("Changes have been saved."),'success');
        return true;
      }
    function label_update(&$in)
    {
    $this->dbu->query("UPDATE label_language_p_order SET 
                                            invoice_note          ='".$in['invoice_note']."',
                                            invoice             ='".$in['invoice']."',
                                            company_name          ='".$in['company_name']."',
                                            comp_reg_number       = '".$in['comp_reg_number']."',
                                            order_note            ='".$in['order_note']."',
                                            `order`             ='".$in['order']."',
                                             stock_dispatching_note     ='".$in['stock_dispatching_note']."',
                                            `stock_dispatching`       ='".$in['stock_dispatching']."',
                                            article             ='".$in['article']."',
                                            quote               ='".$in['quote']."',
                                            reference             ='".$in['reference']."',
                                            quote_note            ='".$in['quote_note']."',
                                            billing_address         ='".$in['billing_address']."',
                                            date              ='".$in['date']."',
                                            customer            ='".$in['customer']."',
                                            item                ='".$in['item']."',
                                            unitmeasure           ='".$in['unitmeasure']."',
                                            quantity            ='".$in['quantity']."',
                                            unit_price            ='".$in['unit_price']."',
                                            amount              ='".$in['amount']."',
                                            subtotal            ='".$in['subtotal']."',
                                            discount            ='".$in['discount']."',
                                            vat               ='".$in['vat']."',
                                            payments            ='".$in['payments']."',
                                            amount_due            ='".$in['amount_due']."',
                                            notes               ='".$in['notes']."',
                                            duedate             ='".$in['duedate']."',
                                            bank_details          ='".$in['bankd']."',
                                            bank_name             ='".$in['bank_name']."',
                                            iban              ='".$in['iban']."',
                                            bic_code            ='".$in['bic_code']."',
                                            phone               ='".$in['phone']."',
                                            fax               ='".$in['fax']."',
                                            email               ='".$in['email']."',
                                            url               ='".$in['url']."',
                                            our_ref             ='".$in['our_ref']."',
                                            your_ref            ='".$in['your_ref']."',
                                            vat_number            ='".$in['vat_number']."',
                                            shipping_price          ='".$in['shipping_price']."',
                                            article_code          ='".$in['article_code']."',
                                            delivery_date           ='".$in['delivery_date']."',
                                            entry_date            ='".$in['entry_date']."',
                                            delivery_note           ='".$in['delivery_note']."',
                                            entry_note            ='".$in['entry_note']."',
                                            p_order_note          ='".addslashes($in['p_order_note'])."',
                                            `p_order`             ='".$in['p_order']."',
                                            grand_total           ='".$in['grand_total']."',
                                            pick_up_from_store        ='".$in['pick_up_from_store']."',
                                            CUSTOMER_REF          ='".$in['customer_ref']."',
                                            page              ='".$in['page']."',
                                            download_webl           = '".$in['download_webl']."',
                                            pdfPrint_webl           = '".$in['pdfPrint_webl']."',
                                            name_webl               = '".$in['name_webl']."',
                                            email_webl              = '".$in['email_webl']."',
                                            addCommentLabel_webl      = '".$in['addCommentLabel_webl']."',
                                            submit_webl           = '".$in['submit_webl']."',
                                            p_order_webl          = '".$in['p_order_webl']."',
                                            date_webl             = '".$in['date_webl']."',
                                            accept_webl           = '".$in['accept_webl']."',
                                            requestNewVersion_webl      = '".$in['requestNewVersion_webl']."',
                                            reject_webl           = '".$in['reject_webl']."',
                                            status_webl           = '".$in['status_webl']."',
                                            bankDetails_webl        = '".$in['bankDetails_webl']."',
                                            payWithIcepay_webl        = '".$in['payWithIcepay_webl']."',
                                            cancel_webl           = '".$in['cancel_webl']."',
                                            regularInvoiceProForma_webl   = '".$in['regularInvoiceProForma_webl']."',
                                            bicCode_webl          = '".$in['bicCode_webl']."',
                                            ibanCode_webl           = '".$in['ibanCode_webl']."',
                                            bank_webl             = '".$in['bank_webl']."',
                                            noData_webl           = '".$in['noData_webl']."',
                                            total_amount           = '".$in['total_amount']."'
            WHERE label_language_id='".$in['label_language_id']."'");
    
    msg::success ( gm("Account has been successfully updated"),'success');
        return true;
    }

      function default_message(&$in)
      {
        $set = "text  = '".$in['text']."', ";
        /*if($in['use_html']){*/
          $set1 = "html_content  = '".$in['html_content']."' ";
        /*}*/
        $this->dbu->query("UPDATE sys_message SET
              subject = '".$in['subject']."',
              ".$set."
              ".$set1."
              WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
        /*$this->dbu->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
        msg::success( gm('Message has been successfully updated'),'success');
        // json_out($in);
        return true;
      }
    function articlefieldup(&$in){

      //Check if User Has Catalog +
      //Check if Article Name, Article Name 2 or Description are written in text input
      if(!$in['ADV_PRODUCT']){
        $fieldExist = false;
        $fields = array('[!ITEM_NAME!]','[!ITEM_NAME2!]','[!DESCRIPTION!]','[!WEIGHT!]','[!ARTICLE_CATEGORY!]');
        foreach ($fields as $value) {
          if(strpos($in['text'],$value) !== false){
            $fieldExist = true;
            break;
          }
        }
        if($fieldExist){
          msg::error(gm('You must have Catalag+ in order to add the article desired fields'),'error');
          return false;
        }
      }

      $this->dbu->query("UPDATE settings SET value='".$in['not_copy_article_info']."' WHERE constant_name='NOT_COPY_ARTICLE_INFO' ");
      $this->dbu->query("UPDATE settings SET value='".$in['use_article_purchase_price']."' WHERE constant_name='USE_ARTICLE_PURCHASE_PRICE' ");
      $this->dbu->query("UPDATE settings SET long_value='".$in['text']."' WHERE constant_name='P_ORDER_FIELD_LABEL' ");
      msg::success ( gm("Changes have been saved."),'success');
      return true;
    }
    function web_link(&$in)
    {
        $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_PO_ORDER_WEB_LINK' ");
        $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_PO_ORDER_COMMENTS' ");
        $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='WEB_INCLUDE_PDF_PO' ");
        if($in['use']){
          $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='USE_PO_ORDER_WEB_LINK' ");
          if($in['allow']){
            $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_PO_ORDER_COMMENTS' ");
          }
          if($in['also']){
            $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='WEB_INCLUDE_PDF_PO' ");
          }
        }
        msg::success ( gm("Changes have been saved."),'success');
      return true;
    }
    function stock_setting(&$in) {
    $this->dbu->query("UPDATE settings SET value='".$in['allow_stock']."' WHERE constant_name='ALLOW_STOCK'");
    $this->dbu->query("UPDATE settings SET value='".$in['allow_out_of_stock']."' WHERE constant_name='ALLOW_OUT_OF_STOCK'");
    $this->dbu->query("UPDATE settings SET value='".$in['show_stock_warning']."' WHERE constant_name='SHOW_STOCK_WARNING'");
    $this->dbu->query("UPDATE settings SET value='".$in['stock_multiple_locations']."' WHERE constant_name='STOCK_MULTIPLE_LOCATIONS'");
    $this->dbu->query("UPDATE settings SET value='".$in['article_threshold_value']."' WHERE constant_name='ARTICLE_THRESHOLD_VALUE'");
        $this->dbu->query("UPDATE pim_articles SET article_threshold_value='".$in['article_threshold_value']."'
                           WHERE custom_threshold_value='0'");
        msg::success ( gm("Article stock settings has been successfully updated"),'success');
    return true;
    }
    function naming_set(&$in){
    if($in['ACCOUNT_STOCK_DISP_START']){
        $this->dbu->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_STOCK_DISP_START' ");
        if($this->dbu->move_next()){
  $this->dbu->query("UPDATE settings SET value='".$in['ACCOUNT_STOCK_DISP_START']."' WHERE constant_name='ACCOUNT_STOCK_DISP_START' ");
        }else{
  $this->dbu->query("INSERT INTO settings SET value='".$in['ACCOUNT_STOCK_DISP_START']."', constant_name='ACCOUNT_STOCK_DISP_START' ");
        }
        msg::success ( gm("Settings updated"),'success');
    }

    if($in['ACCOUNT_STOCK_DISP_DIGIT_NR']){
        if(!defined('ACCOUNT_STOCK_DISP_DIGIT_NR')){
  $this->dbu->query("INSERT INTO settings SET value='".$in['ACCOUNT_STOCK_DISP_DIGIT_NR']."', constant_name='ACCOUNT_STOCK_DISP_DIGIT_NR', module='order' ");
  msg::success ( gm("Settings updated"),'success');
        }else{
  $this->dbu->query("UPDATE settings SET value='".$in['ACCOUNT_STOCK_DISP_DIGIT_NR']."' WHERE constant_name='ACCOUNT_STOCK_DISP_DIGIT_NR'");
  msg::success ( gm("Settings updated"),'success');
        }
    }

    $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_STOCK_DISP_REF' ");
    if($in['CHECKED']){
        $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_STOCK_DISP_REF' ");
    }

    $this->dbu->query("UPDATE settings SET value='".$in['ACCOUNT_STOCK_DISP_DEL']."' WHERE constant_name='ACCOUNT_STOCK_DISP_DEL' ");

    return true;
    }
    function note_set(&$in){
    if($in['notes']){
            switch ($in['languages']){
  case '1':
      $lang = '';
      break;
  case '2':
      $lang = '_2';
      break;
  case '3':
      $lang = '_3';
      break;
  case '4':
      $lang = '_4';
      break;
  default:
      $lang = '';
      break;
        }

        $this->dbu->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='stock_disp_note".$lang."' AND default_main_id='0' ");
        msg::success ( gm("Note updated"),'success');
    }
    }
    function add_address(&$in){
    $in['failure'] = false;
    if(!$this->add_address_validate($in)){
        $in['failure'] = true;
        return false;
    }
        $in['address_id'] = $this->db->insert("INSERT INTO dispatch_stock_address SET
                                                     naming  ='".$in['naming']."',
                                                               country_id  ='".$in['country_id']."',
                                                               zip         ='".$in['zip']."',
                                                               address     ='".$in['address']."',
                                                               city        ='".$in['city']."'

        ");
    msg::success ( gm("Address successfully added"),'success');
    return true;
    }
    function update_address(&$in){
    $in['failure'] = false;
    if(!$this->update_address_validate($in)){
        $in['failure'] = true;
        return false;
    }
        $this->db->query("UPDATE dispatch_stock_address SET
                            naming      ='".$in['naming']."',
                            country_id    ='".$in['country_id']."',
                            zip           ='".$in['zip']."',
                            address       ='".$in['address']."',
                            city        ='".$in['city']."'
                    WHERE   address_id    ='".$in['address_id']."'
  ");
    msg::success ( gm("Address successfully updated"),'success');
    return true;
    }
    function update_address_validate(&$in)
    {
    $v = new validation($in);
    $v->field('address_id', 'ID', 'required:exist[dispatch_stock_address.address_id]', "Invalid Id");
    if(!$v->run()){
        return false;
    } else {
        return $this->add_address_validate($in);
    }
    }
    function add_address_validate(&$in)
    {
    $v = new validation($in);
    $v->field('country_id', 'ID', 'required');
    $is_ok = $v->run();
    return $is_ok;
    }
    function delete_address(&$in){
    $in['failure'] = false;
    if(!$this->delete_address_validate($in)){
        $in['failure'] = true;
        return false;
    }

        $this->db->query("DELETE FROM  dispatch_stock_address WHERE  address_id =  '".$in['address_id']."'");
    msg::success ( gm("Address successfully deleted"),'success');
    return true;
    }
    function delete_address_validate(&$in)
    {

    $v = new validation($in);
    $v->field('address_id', 'ID', 'required:exist[dispatch_stock_address.address_id]', "Invalid Id");
    $is_ok = $v->run();


    return $is_ok;
    }
    function set_main(&$in)
    {
    if(!$this->set_main_validate($in)){
        return false;
    }
        $this->db->query("UPDATE  dispatch_stock_address SET is_default='0' ");
        $this->db->query("UPDATE  dispatch_stock_address SET is_default='1' WHERE address_id='".$in['address_id']."' ");
    msg::success ( gm("Main Depot has been set"),'success');
    return true;
    }
    function set_main_validate(&$in)
    {
    $v = new validation($in);
    $v->field('address_id', 'ID', 'required:exist[dispatch_stock_address.address_id]', "Invalid Id");

    return $v->run();
    }
    function check_vies_vat_number(&$in){
    $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

    if(!$in['value'] || $in['value']==''){      
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }
    $value=trim($in['value']," ");
    $value=str_replace(" ","",$value);
    $value=str_replace(".","",$value);
    $value=strtoupper($value);

    $vat_numeric=is_numeric(substr($value,0,2));

    /*if($vat_numeric || substr($value,0,2)=="BE"){
        $trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
        $trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
        $trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

        if(!$trends_access_token || $trends_expiration<time()){
  $ch = curl_init();
  $headers=array(
      "Content-Type: x-www-form-urlencoded",
      "Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
      );
  /*$trends_data=!$trends_access_token ? "grant_type=password&username=akti_api&password=akti_api" : "grant_type=refresh_token&&refresh_token=".$trends_refresh_token;* /
  $trends_data="grant_type=password&username=akti_api&password=akti_api";

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
            curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

            $put = json_decode(curl_exec($ch));
            $info = curl_getinfo($ch);

            /*console::log($put);
            console::log('aaaaa');
            console::log($info);* /

            if($info['http_code']==400){
      if(ark::$method == 'check_vies_vat_number'){
      msg::error ( $put->error,'error');
      json_out($in);
      }
      return false;
            }else{
      if(!$trends_access_token){
          $this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
          $this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
          $this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
      }else{
          $this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
          $this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
          $this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
      }

      $ch = curl_init();
      $headers=array(
      "Authorization: Bearer ".$put->access_token
      );
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      if($vat_numeric){
          curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
      }else{
          curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
      }
      $put = json_decode(curl_exec($ch));
      $info = curl_getinfo($ch);

      /*console::log($put);
      console::log('bbbbb');
      console::log($info);* /

      if($info['http_code']==400 || $info['http_code']==429){
      if(ark::$method == 'check_vies_vat_number'){
    msg::error ( $put->error,'error');
    json_out($in);
          }
      return false;
      }else if($info['http_code']==404){
      if($vat_numeric){
          if(ark::$method == 'check_vies_vat_number'){
        msg::error ( gm("Not a valid vat number"),'error');
        json_out($in);
    }
          return false;
      }
      }else{
      if($in['customer_id']){
          $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
          if($country_id != 26){
    $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
    $in['remove_v']=1;
          }else if($country_id == 26){
        $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
        $in['add_v']=1;
    }
      }
      $in['comp_name']=$put->officialName;
      $in['comp_address']=$put->street.' '.$put->houseNumber;
      $in['comp_zip']=$put->postalCode;
      $in['comp_city']=$put->city;
      $in['comp_country']='26';
      $in['trends_ok']=true;
      $in['trends_lang']=$_SESSION['l'];
      $in['full_details']=$put;
      if(ark::$method == 'check_vies_vat_number'){
    msg::success(gm('Success'),'success');
    json_out($in);
          }
      return false;
      }
            }
        }else{
  $ch = curl_init();
  $headers=array(
      "Authorization: Bearer ".$trends_access_token
      );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            if($vat_numeric){
                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
            }else{
                curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
            }
            $put = json_decode(curl_exec($ch));
            $info = curl_getinfo($ch);

            /*console::log($put);
  console::log('ccccc');
  console::log($info);* /

            if($info['http_code']==400 || $info['http_code']==429){
      if(ark::$method == 'check_vies_vat_number'){
      msg::error ( $put->error,'error');
      json_out($in);
      }
      return false;
            }else if($info['http_code']==404){
      if($vat_numeric){
          if(ark::$method == 'check_vies_vat_number'){
          msg::error (gm("Not a valid vat number"),'error');
          json_out($in);
      }
      return false;
      }
            }else{
      if($in['customer_id']){
          $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
      if($country_id != 26){
          $this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
          $in['remove_v']=1;
      }else if($country_id == 26){
          $this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
          $in['add_v']=1;
      }
      }
      $in['comp_name']=$put->officialName;
      $in['comp_address']=$put->street.' '.$put->houseNumber;
      $in['comp_zip']=$put->postalCode;
      $in['comp_city']=$put->city;
      $in['comp_country']='26';
      $in['trends_ok']=true;
      $in['trends_lang']=$_SESSION['l'];
      $in['full_details']=$put;
      if(ark::$method == 'check_vies_vat_number'){
      msg::success(gm('Success'),'success');
      json_out($in);
      }
      return false;
            }
        }
    }*/


    if(!in_array(substr($value,0,2), $eu_countries)){
        $value='BE'.$value;
    }
    if(in_array(substr($value,0,2), $eu_countries)){
        $search   = array(" ", ".");
        $vat = str_replace($search, "", $value);
        $_GET['a'] = substr($vat,0,2);
        $_GET['b'] = substr($vat,2);
        $_GET['c'] = '1';
        $dd = include('valid_vat.php');
    }else{
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }
    if(isset($response) && $response == 'invalid'){
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }else if(isset($response) && $response == 'error'){
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }else if(isset($response) && $response == 'valid'){
        $full_address=explode("\n",$result->address);
        switch($result->countryCode){
  case "RO":
      $in['comp_address']=$full_address[1];
      $in['comp_city']=$full_address[0];
      $in['comp_zip']=" ";
      break;
  case "NL":
      $zip=explode(" ",$full_address[2],2);
      $in['comp_address']=$full_address[1];
      $in['comp_zip']=$zip[0];
      $in['comp_city']=$zip[1];
      break;
  default:
      $zip=explode(" ",$full_address[1],2);
      $in['comp_address']=$full_address[0];
      $in['comp_zip']=$zip[0];
      $in['comp_city']=$zip[1];
      break;
        }

        $in['comp_name']=$result->name;

        $in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
        $in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
        $in['full_details']=$result;
        $in['vies_ok']=1;
        if($in['customer_id']){
        $country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
        if($in['comp_country'] != $country_id){
            $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
            $this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
            $in['remove_v']=1;
          }else if($in['comp_country'] == $country_id){
            $vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
            $this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
            $in['add_v']=1;
          }
        }
        if(ark::$method == 'check_vies_vat_number'){
  msg::success ( gm('VAT Number is valid'),'success');
  json_out($in);
        }
        return true;
    }else{
        if(ark::$method == 'check_vies_vat_number'){
  msg::error ( gm('Error'),'error');
  json_out($in);
        }
        return false;
    }
    if(ark::$method == 'check_vies_vat_number'){
        json_out($in);
    }
    }
    function tryAddC(&$in){
    if(!$this->tryAddCValid($in)){ 
        json_out($in);
        return false; 
    }
    $in['p_order_id'] = $in['item_id'];
    //$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
    $name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    //Set account default vat on customer creation
    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

    if(empty($vat_regime_id)){
      $vat_regime_id = 0;
    }

    $c_types = '';
    $c_type_name = '';
    if($in['c_type']){
      /*foreach ($in['c_type'] as $key) {
        if($key){
          $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
          $c_type_name .= $type.',';
        }
      }
      $c_types = implode(',', $in['c_type']);
      $c_type_name = rtrim($c_type_name,',');*/
      if(is_array($in['c_type'])){
        foreach ($in['c_type'] as $key) {
          if($key){
            $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
            $c_type_name .= $type.',';
          }
        }
        $c_types = implode(',', $in['c_type']);
        $c_type_name = rtrim($c_type_name,',');
      }else{
        $c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");        
        $c_types = $in['c_type'];
      }
    }


    if($in['user_id']==''){
      $in['user_id']=$_SESSION['u_id'];
    }


    if($in['user_id']){
      /*foreach ($in['user_id'] as $key => $value) {
        $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
          $acc_manager .= $manager_id.',';
      }     
      $in['acc_manager'] = rtrim($acc_manager,',');
      $acc_manager_ids = implode(',', $in['user_id']);*/
      if(is_array($in['user_id'])){
        foreach ($in['user_id'] as $key => $value) {
          $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
            $acc_manager .= $manager_id.',';
        }     
        $in['acc_manager'] = rtrim($acc_manager,',');
        $acc_manager_ids = implode(',', $in['user_id']);
      }else{
        //$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
        $in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
        $acc_manager_ids = $in['user_id'];
      }
    }else{
      $acc_manager_ids = '';
      $in['acc_manager'] = '';
    }
    $vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
          $vat_default=str_replace(',', '.', $vat);
    $selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");

    if($in['add_customer']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
       $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
       if(SET_DEF_PRICE_CAT == 1){
          $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
        }
        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['name'])."',
                                            our_reference = '".$account_reference_number."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            type          = '0',
                                            is_supplier='1',
                                            zip_name  = '".$in['zip']."',
                                            commercial_name     = '".$in['commercial_name']."',
                                            legal_type        = '".$in['legal_type']."',
                                            c_type          = '".$c_types."',
                                            website         = '".$in['website']."',
                                            language        = '".$in['language']."',
                                            sector          = '".$in['sector']."',
                                            comp_fax        = '".$in['comp_fax']."',
                                            activity        = '".$in['activity']."',
                                            comp_size       = '".$in['comp_size']."',
                                            lead_source       = '".$in['lead_source']."',
                                            various1        = '".$in['various1']."',
                                            various2        = '".$in['various2']."',
                                            c_type_name       = '".$c_type_name."',
                                            vat_id          = '".$selected_vat."',
                                            invoice_email_type    = '1',
                                            sales_rep       = '".$in['sales_rep_id']."',
                                            internal_language   = '".$in['internal_language']."',
                                            is_customer       = '".$in['is_customer']."',
                                            stripe_cust_id      = '".$in['stripe_cust_id']."',
                                            cat_id          = '".$in['cat_id']."'");
      
       $in['main_address_id'] = $this->db->insert("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
          foreach ($in['extra'] as $key => $value) {
            $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
            if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
              $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
            }
          }
        }

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
  $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
  $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
  /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
        $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                          [ 'user_id' => $_SESSION['u_id'],
                            'name'    => 'company-customers_show_info',
                            'value'   => '1']
                        );                            
        } else {
  /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
  doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }

        if($in['item_id'] && is_numeric($in['item_id'])){
        $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
        $this->db->query("UPDATE pim_p_orders SET 
            customer_id='".$in['buyer_id']."', 
            customer_name='".$in['name']."',
            contact_id='',
            main_address_id='".$in['main_address_id']."', 
            address_info = '".$address."'
            WHERE p_order_id='".$in['item_id']."' ");         
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
    if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
        if(SET_DEF_PRICE_CAT == 1){
          $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
        }
        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['lastname'])."',
                                            our_reference = '".$account_reference_number."',
                                            firstname = '".addslashes($in['firstname'])."',
                                            user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            comp_phone ='".$in['phone']."',
                                            type = 1,
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            is_supplier='1',
                                            zip_name  = '".$in['zip']."',
                                            commercial_name     = '".$in['commercial_name']."',
                                            legal_type        = '".$in['legal_type']."',
                                            c_type          = '".$c_types."',
                                            website         = '".$in['website']."',
                                            language        = '".$in['language']."',
                                            sector          = '".$in['sector']."',
                                            comp_fax        = '".$in['comp_fax']."',
                                            activity        = '".$in['activity']."',
                                            comp_size       = '".$in['comp_size']."',
                                            lead_source       = '".$in['lead_source']."',
                                            various1        = '".$in['various1']."',
                                            various2        = '".$in['various2']."',
                                            c_type_name       = '".$c_type_name."',
                                            vat_id          = '".$selected_vat."',
                                            invoice_email_type    = '1',
                                            sales_rep       = '".$in['sales_rep_id']."',
                                            internal_language   = '".$in['internal_language']."',
                                            is_customer       = '".$in['is_customer']."',
                                            stripe_cust_id      = '".$in['stripe_cust_id']."',
                                            cat_id          = '".$in['cat_id']."'");
      
        $in['main_address_id'] = $this->db->insert("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        $in['customer_name'] = $in['name'];

        if($in['extra']){
          foreach ($in['extra'] as $key => $value) {
            $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
            if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
              $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
            }
          }
        }

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
          $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                          ['user_id' => $_SESSION['u_id'],
                           'name'    => 'company-customers_show_info',
                           'value'   => '1']
                        );                            
        } else {
          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
          $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
        $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
        $this->db->query("UPDATE pim_p_orders SET 
            customer_id='".$in['buyer_id']."', 
            customer_name='".addslashes($in['name'])."',
            contact_id='',
            main_address_id='".$in['main_address_id']."', 
            address_info = '".$address."'
            WHERE p_order_id='".$in['item_id']."' ");         
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
    if($in['add_contact']){
        $customer_name='';
        if($in['buyer_id']){
  $customer_name = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
        }
        if($in['birthdate']){
        $in['birthdate'] = strtotime($in['birthdate']);
      }
        $in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
            customer_id = '".$in['buyer_id']."',
            firstname = '".$in['firstname']."',
            lastname  = '".$in['lastname']."',
            email   = '".$in['email']."',
            birthdate = '".$in['birthdate']."',
            cell    = '".$in['cell']."',
            sex     = '".$in['sex']."',
            title   = '".$in['title_contact_id']."',
            language  = '".$in['language']."',
            company_name= '".$customer_name."',
            `create`  = '".time()."'");
        $this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
      if($in['extra']){
        foreach ($in['extra'] as $key => $value) {
          $this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
        }
      }

        if($in['buyer_id']){
        $contact_accounts_sql="";
        $accepted_keys=["position","department","title","e_title","email","phone","fax","s_email","customer_address_id"];
        foreach($in['accountRelatedObj'] as $key=>$value){
          if(in_array($value['model'],$accepted_keys)){
            $contact_accounts_sql.="`".$value['model']."`='".addslashes($value['model_value'])."',";
          }
        }
        if($in['email']){
          $contact_accounts_sql.="`email`='".$in['email']."',";
        }
        if(!empty($contact_accounts_sql)){
          $contact_accounts_sql=rtrim($contact_accounts_sql,",");
          $this->db->query("INSERT INTO customer_contactsIds SET 
                        customer_id='".$in['buyer_id']."', 
                        contact_id='".$in['contact_id']."',
                        ".$contact_accounts_sql." ");
        } 
      }
        $in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
        if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
                $vars=array();
                if($in['buyer_id']){
                    $vars['customer_id']=$in['buyer_id'];
                    $vars['customer_name']=$customer_name;
                }          
                $vars['contact_id']=$in['contact_id'];
                $vars['firstname']=$in['firstname'];
                $vars['lastname']=$in['lastname'];
                $vars['table']='customer_contacts';
                $vars['email']=$in['email'];
                $vars['op']='add';
                synctoZendesk($vars);
        }
        if($in['country_id']){
  $this->db->query("INSERT INTO customer_contact_address SET
                          address='".$in['address']."',
                          zip='".$in['zip']."',
                          city='".$in['city']."',
                          country_id='".$in['country_id']."',
                          contact_id='".$in['contact_id']."',
                          is_primary='1',
                          delivery='1' ");
        }
        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                    AND name    = 'company-contacts_show_info'  ");*/
        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                    AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);                                    
        if(!$show_info->move_next()) {
  /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                                  name    = 'company-contacts_show_info',
                                  value   = '1' ");*/
        $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                                  name    = :name,
                                  value   = :value ",
                                ['user_id' => $_SESSION['u_id'],
                                 'name'    => 'company-contacts_show_info',
                                 'value'   => '1']
                              );                                  
        } else {
  /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                      AND name    = 'company-contacts_show_info' ");*/
        $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                      AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);                                      
        }
        $count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
        if($count == 1){
  doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
           $this->db->query("UPDATE pim_p_orders SET contact_id='".$in['contact_id']."' WHERE p_order_id='".$in['item_id']."' ");  
        }
        insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
        msg::success (gm('Success'),'success');
    }
    
    return true;
    }

    /**
    * undocumented function
    *
    * @return void
    * @author PM
    **/
    function tryAddCValid(&$in)
    {
    if($in['add_customer']){
        $v = new validation($in);
        $v->field('name', 'name', 'required:unique[customers.name]');
        $v->field('country_id', 'country_id', 'required');
        return $v->run();  
    }
    if($in['add_contact']){
        $v = new validation($in);
        $v->field('firstname', 'firstname', 'required');
        $v->field('lastname', 'lastname', 'required');
        //Removed Email Validation on contact creation
        // $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
        $v->field('country_id', 'country_id', 'required');
        return $v->run();  
    }
    return true;
    }

        function tryAddSC(&$in){
    if(!$this->tryAddSCValid($in)){ 
        json_out($in);
        return false; 
    }
    $in['p_order_id'] = $in['item_id'];
    //$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
    $name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    //Set account default vat on customer creation
    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

    if(empty($vat_regime_id)){
      $vat_regime_id = 1;
    }
    if($in['add_customer']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
  $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $in['sc_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['name'])."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            user_id = '".$_SESSION['u_id']."',
                                            acc_manager_name = '".addslashes($name_user)."',
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            type          = '0',
                                            zip_name  = '".$in['zip']."'");
      
       $in['second_address_id']= $this->db->insert("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['sc_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        

      // include_once('../apps/company/admin/model/customer.php');
        //$in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
  $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['sc_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
  $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['sc_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
  /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
        $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                          ['user_id' => $_SESSION['u_id'],
                           'name'    => 'company-customers_show_info',
                           'value'   => '1']
                        );                            
        } else {
  /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
  doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }

        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          $this->db->query("UPDATE pim_p_orders SET 
            second_customer_id='".$in['sc_id']."', 
            second_address_id='".$in['second_address_id']."',
           
            WHERE p_order_id='".$in['item_id']."' ");         
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
    if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $in['sc_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".addslashes($in['lastname'])."',
                                            firstname = '".addslashes($in['firstname'])."',
                                            user_id = '".$_SESSION['u_id']."',
                                            acc_manager_name = '".addslashes($name_user)."',
                                            btw_nr='".$in['btw_nr']."',
                                            vat_regime_id     = '".$vat_regime_id."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            type = 1,
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            creation_date = '".time()."',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."'");
      
        $in['second_address_id']= $this->db->insert("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['sc_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1',
                        site='1' ");
        //$in['customer_name'] = $in['name'];
        
      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['sc_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['sc_id']."' ");
        }

        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
        if(!$show_info->move_next()) {
          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");*/
          $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
                            name    = :name,
                            value   = :value ",
                          ['user_id' => $_SESSION['u_id'],
                           'name'    => 'company-customers_show_info',
                           'value'   => '1']
                        );                            
        } else {
          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");*/
          $this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);                                
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          $this->db->query("UPDATE pim_p_orders SET 
            second_customer_id='".$in['sc_id']."', 
            second_address_id='".$in['second_address_id']."',
           
            WHERE p_order_id='".$in['item_id']."' ");         
        }
        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['sc_id']);
        msg::success (gm('Success'),'success');
        return true;
    }
      
    return true;
    }

    /**
    * undocumented function
    *
    * @return void
    * @author PM
    **/
    function tryAddSCValid(&$in)
    {
    if($in['add_customer']){
        $v = new validation($in);
        $v->field('name', 'name', 'required:unique[customers.name]');
        $v->field('country_id', 'country_id', 'required');
        return $v->run();  
    }
    if($in['add_contact']){
        $v = new validation($in);
        $v->field('firstname', 'firstname', 'required');
        $v->field('lastname', 'lastname', 'required');
        $v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
        $v->field('country_id', 'country_id', 'required');
        return $v->run();  
    }
    return true;
    }

function add_po_order_multiple(&$in){

$suppliers=array();
$in['supplier_selected']=array();
$in['add_p_order']=array();
$in['supplier_selected_out']=array();
$in['add_p_order_out']=array();
$in['article']=array();
$in['article_out']=array();

foreach ($in['list'] as $key =>$value){
  if(!$in['article'][$value['supplier_id']]){
    $in['article'][$value['supplier_id']]=array();
  }
  if($value['article_id'] && $value['is_article'] ){
   $in['article'][$value['supplier_id']][$value['article_id']]=$value['quantity'];
  }elseif($value['order_articles_id']){
    $in['article_out'][$value['supplier_id']][$value['order_articles_id']]=$value['quantity'];
  }
}



foreach ($in['list'] as $key =>$value){
  if($value['add_p_order']){
       if($value['article_id']){
          $in['supplier_selected']+=array($value['article_id']=>$value['supplier_id']);
          $in['add_p_order']+=array($value['article_id']=>$value['supplier_id']);
          //$in['article']+=array($value['supplier_id']=>array($value['article_id']=>1));
       }elseif($value['order_articles_id']){
          $in['supplier_selected_out']+=array($value['order_articles_id']=>$value['supplier_id']);
          $in['add_p_order_out']+=array($value['order_articles_id']=>$value['supplier_id']);
          //$in['article']+=array($value['supplier_id']=>array($value['article_id']=>1));
       }
       
   }

}

        foreach ($in['supplier_selected'] as $article_id => $supplier_id) {
            //we have to create an array with all suppliers_selects
            if(array_key_exists($article_id,$in['add_p_order'])){
                if(!array_key_exists($supplier_id, $suppliers)){
                    $suppliers[$supplier_id]=array();
                }
                array_push( $suppliers[$supplier_id],  $article_id);
           }

        }

       foreach ($in['supplier_selected_out'] as $article_id => $supplier_id) {
            //we have to create an array with all suppliers_selects
            if(array_key_exists($article_id,$in['add_p_order_out'])){
                if(!array_key_exists($supplier_id, $suppliers)){
                    $suppliers[$supplier_id]=array();
                }
                array_push( $suppliers[$supplier_id],  $article_id);
           }

        }

        if(empty($suppliers)){
          msg::error(gm('No article'),'error');
          json_out($in);
        }

     foreach ($suppliers as $key => $articles) {
          $in['customer_id']=$key;
          $in['multiple']=1;

        $this->add_po_order_direct($in);

 //echo '<pre>';
          // print_r($in['article']);
         //$this->add_po_order_direct($in);
 //echo '</pre>';
//exit();

     }
  unset($in['order_id']);
      $in['add']=false;
      unset($_SESSION['add_to_purchase']);


    msg::success ( gm("Purchase orders successfully added"),'success');
    json_out($in);
  }

  /****************************************************************
  * function add_po_order_direct(&$in)                                          *
  ****************************************************************/
  function add_po_order_direct(&$in)
  {


        $in['notes'] = $this->db->field("SELECT value FROM default_data WHERE type='order_note'");


         if(!$in['payment_term']){
            $in['payment_term']=15;
          }
         if(!$in['del_date']){

          $in['del_date'] = time();
        }
        // if (!$in['serial_number']){
          $in['serial_number'] = generate_serial_number(DATABASE_NAME);
      // }

          if($in['order_id']){
           $this->dbu->query("UPDATE pim_orders SET is_purchase='1' WHERE pim_orders.order_id='".$in['order_id']."'");
             $in['our_ref'] = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id='".$in['order_id']."'");
             $in['your_ref'] = $this->db->field("SELECT your_ref FROM pim_orders WHERE order_id='".$in['order_id']."'");
             $in['identity_id'] = $this->db->field("SELECT identity_id FROM customers WHERE customer_id = '". $in['customer_id'] ."'");
          }elseif($in['service_id']){
            $this->dbu->query("UPDATE servicing_support SET is_purchase='1' WHERE servicing_support.service_id='".$in['service_id']."'");
             if($in['cost_centre']){
                $in['our_ref']=$in['cost_centre'].'/'.$in['serial_number'];
             }else{
                $in['our_ref'] = $this->db->field("SELECT serial_number FROM servicing_support WHERE service_id='".$in['service_id']."'");
             }           
          }elseif($in['project_id']){
            $this->dbu->query("UPDATE projects SET is_purchase='1' WHERE projects.project_id='".$in['project_id']."'");
              $in['our_ref'] = $this->db->field("SELECT serial_number FROM projects WHERE project_id='".$in['project_id']."'");           
          }elseif($in['quote_id']){
           // $this->dbu->query("UPDATE tblquote SET is_purchase='1' WHERE tblquote.id='".$in['quote_id']."'");
           //   $in['our_ref'] = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id='".$in['order_id']."'");
           //   $in['your_ref'] = $this->db->field("SELECT your_ref FROM pim_orders WHERE order_id='".$in['order_id']."'");

          }
          if($in['order_id']){
            $customer_currency_type=$this->db->field("SELECT currency_id FROM customers WHERE customer_id='".$in['customer_id']."'");
              $order_currency_type=$in['currency_type'];
            $order_currency_type=$this->db->field("SELECT currency_type FROM pim_orders WHERE order_id='".$in['order_id']."'");
          }
         
         if(NOT_COPY_ARTICLE_INFO == 1 || $in['add_to_purchase'] || $in['service_id'] || $in['quote_id'] || $in['project_id']){
            //we have to select the supplier info
            $in['currency_type'] = $this->db->field("SELECT currency_id FROM customers WHERE customer_id='".$in['customer_id']."'");
              //$in['email_language'] = $this->db->field("SELECT internal_language FROM customers WHERE customer_id='".$in['customer_id']."'");
              $emailMessageData = array('buyer_id'    => $in['customer_id'],
                    'contact_id'    => '',
                    'param'     => 'add');
              $in['email_language'] = get_email_language($emailMessageData);
         }else{
              $in['currency_type'] = $this->db->field("SELECT currency_type FROM pim_orders WHERE order_id='".$in['order_id']."'");
              //$in['email_language'] = $this->db->field("SELECT email_language FROM pim_orders WHERE order_id='".$in['order_id']."'");
              $emailMessageData = array('buyer_id'    => $in['customer_id'],
                    'contact_id'    => '',
                    'param'     => 'add');
              $in['email_language'] = get_email_language($emailMessageData);
         }

      if(!$in['email_language'] || $in['email_language']==0){
          $in['email_language']=1;
      }

        $in['default_currency']       = ACCOUNT_CURRENCY_TYPE;
        if($in['default_currency']!=$in['currency_type']){
           $in['currency_name'] = build_currency_name_list(ACCOUNT_CURRENCY_TYPE);
           $in['old_currency_name'] = build_currency_name_list($in['currency_type']);
           $in['currency_rate']= currency::getCurrency($in['old_currency_name'], $in['currency_name'], 1,ACCOUNT_NUMBER_FORMAT);
        }

        //first we select the customer details
         $this->db->query("SELECT  customers.*
                         FROM customers
                           WHERE customer_id='".$in['customer_id']."'
                         ");
         if($this->db->move_next())
        {
             if(ACCOUNT_ORDER_REF && $this->db->f('our_reference')){
              $in['buyer_ref'] = $this->db->f('our_reference').ACCOUNT_ORDER_DEL;
            }


             $address=$this->db2->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."'");
             $address->move_next();
             $in['address_info'] = $address->f('address').'
'.$address->f('zip').'  '.$address->f('city').'
'.get_country_name($address->f('country_id'));
$is_adveo=0;
if($in['customer_id']==2403){
  $is_adveo=1;
}

$setting_p_order_ref = $this->db2->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_P_ORDER_REF' AND type=1");
if (!$setting_p_order_ref) {
    $in['buyer_ref'] = '';
}

        $in['p_order_id'] = $this->db2->insert("INSERT INTO pim_p_orders SET
                                                        customer_id             = '".$in['customer_id']."',
                                                        customer_name           = '".addslashes($this->db->f('name'))."',
                                                        company_number          = '".$this->db->f('company_number')."',
                                                        customer_vat_id         = '".$this->db->f('vat_id')."',
                                                        customer_email          = '".$this->db->f('c_email')."',
                                                        customer_bank_name      = '".addslashes($this->db->f('bank_name'))."',
                                                        customer_bank_iban      = '".addslashes($this->db->f('bank_iban'))."',
                                                        customer_bic_code       = '".addslashes($this->db->f('bank_bic_code'))."',
                                                        customer_vat_number     = '".addslashes($this->db->f('btw_nr'))."',
                                                        customer_country_id     = '".$address->f('country_id')."',
                                                        customer_city           = '".addslashes($address->f('city'))."',
                                                        customer_zip            = '".addslashes($address->f('zip'))."',
                                                        customer_phone          = '".$this->db->f('comp_phone')."',
                                                        customer_cell           = '".$in['customer_cell']."',
                                                        customer_address        = '".addslashes($address->f('address'))."',
                                                        seller_name             = '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                                                        seller_d_address        = '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
                                                        seller_d_zip            = '".addslashes(ACCOUNT_DELIVERY_ZIP)."',
                                                        seller_d_city           = '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
                                                        seller_d_country_id     = '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                                        seller_d_state_id       = '".ACCOUNT_DELIVERY_STATE_ID."',
                                                        seller_bwt_nr           = '".addslashes(ACCOUNT_VAT_NUMBER)."',

                                                        `date`                = '".$in['del_date']."',
                                                        payment_term          = '".$in['payment_term']."',

                                                        serial_number         = '".$in['serial_number']."',
                                                        field                 = 'customer_id',
                                                        active                = '1',
                                                        buyer_ref             = '".$in['buyer_ref']."',

                                                        use_package           = '".ALLOW_ARTICLE_PACKING."',
                                                        use_sale_unit         = '".ALLOW_ARTICLE_SALE_UNIT."',

                                                        our_ref               = '".$in['our_ref']."',
                                                        your_ref              = '".addslashes($in['your_ref'])."',
                                                        identity_id           = '".$in['identity_id']."',
                                                        del_date              = '".$in['del_date']."',
                                                        currency_rate         = '".$in['currency_rate']."',
                                                        currency_type         = '".$in['currency_type']."',
                                                        address_info          = '".addslashes($in['address_info'])."',
                                                        p_order_type          = '3' ,
                                                        is_adveo              = '".$is_adveo."' ,
                                                        order_id              = '".$in['order_id']."' ,
                                                        service_id            = '".$in['service_id']."' ,
                                                        email_language        = '".$in['email_language']."',
                                                        type_id               ='".$in['type_id']."',
                                                        source_id             ='".$in['source_id']."',
                                                        segment_id            ='".$in['segment_id']."',
                                                        cost_centre           ='".$in['cost_centre']."' ");

if($in['quote_id']){


  $main_address_id = $this->db->field("SELECT address_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary ='1' ");
  $this->db->query("UPDATE pim_p_orders SET main_address_id='".$main_address_id."' WHERE p_order_id='".$in['p_order_id']."'");
}
if($in['order_id'] && $in['deliver_address']){
  $order_data=$this->db->query("SELECT * FROM pim_orders WHERE order_id='".$in['order_id']."' ");
  $this->db->query("UPDATE pim_p_orders SET client_delivery='1',second_customer_id='".$order_data->f('customer_id')."',second_address_id='".$order_data->f('delivery_address_id')."' WHERE p_order_id='".$in['p_order_id']."'");
}

          // add multilanguage order notes in note_fields
    $lang_note_id = $in['email_language'];


        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        while($langs->next()){
          if($lang_note_id == $langs->f('lang_id')){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }
          $note = $this->db2->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = 'p_order_note_".$langs->f('lang_id')."'");
          $this->db->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['p_order_id']."',
                            lang_id         =   '".$langs->f('lang_id')."',
                            active          =   '".$active_lang_note."',
                            item_type         =   'p_order',
                            item_name         =   'notes',
                            item_value        =   '".addslashes($note)."'
          ");
        }

        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
        while($custom_langs->next()) {
          if($lang_note_id == $custom_langs->f('lang_id')) {
            $active_lang_note = 1;
          } else {
            $active_lang_note = 0;
          }
          $note = $this->db2->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = 'p_order_note_".$custom_langs->f('lang_id')."'");
          $this->db->insert("INSERT INTO note_fields SET
                          item_id     = '".$in['p_order_id']."',
                          lang_id     = '".$custom_langs->f('lang_id')."',
                          active      = '".$active_lang_note."',
                          item_type     = 'p_order',
                          item_name     = 'notes',
                          item_value    =   '".addslashes($note)."'");
        }

       //copy lines
if($in['generate_po']){
  $in['add_to_purchase']=1;
}



     if($in['order_id']){
        //$this->db->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY pim_order_articles.order_articles_id");
      $this->db->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY pim_order_articles.sort_order");
     }elseif($in['service_id']){
        $this->db->query("SELECT * FROM servicing_support_articles WHERE service_id='".$in['service_id']."'");
     }elseif($in['project_id']){
        $this->db->query("SELECT * FROM project_articles WHERE project_id='".$in['project_id']."'");
     }elseif($in['quote_id']){
        $this->db->query("SELECT tblquote_line.*, tblquote_line.package as packing FROM tblquote_line WHERE quote_id='".$in['quote_id']."' AND facq='1' ORDER BY sort_order ASC, line_order ASC");
     }elseif($in['add_to_purchase']){
        foreach ($in['add_p_order'] as $key => $value) {
           $article_ids.=$key.',';
        }
        $article_ids=rtrim($article_ids,',');
        $this->db->query("SELECT * FROM pim_articles WHERE article_id  in (".$article_ids.") ");
     }

        $order_total=0;
        while ($this->db->move_next()) {

            if($this->db->f('article_id')){

                 $purchase_price=$this->db2->field("SELECT purchase_price FROM
                                                 pim_article_prices
                                                 WHERE article_id='".$this->db->f('article_id')."' AND base_price=1

                                                  ");
             
                if(USE_ARTICLE_PURCHASE_PRICE && $in['order_id']){                 
                   if($order_currency_type && $customer_currency_type && $this->db->f('purchase_price_other_currency_id') && $in['currency_type'] && $this->db->f('purchase_price_other_currency_id') == $in['currency_type'] && $order_currency_type != $customer_currency_type){
                      $purchase_price=$this->db->f('purchase_price_other_currency');
                   }else{
                      $purchase_price=$this->db2->field("SELECT purchase_price FROM
                                                  pim_order_articles
                                                 WHERE article_id='".$this->db->f('article_id')."' AND order_id='".$in['order_id']."'

                                                  ");
                   }
                }elseif($in['quote_id']){
                   $purchase_price=$this->db2->field("SELECT purchase_price FROM
                                                  tblquote_line
                                                 WHERE id='".$this->db->f('id')."' AND quote_id='".$in['quote_id']."'

                                                  ");
                }

             $article_line=$this->db->f('article');

             $supplier_reference=$this->db2->field("SELECT supplier_reference FROM
                                                 pim_articles
                                                 WHERE article_id='".$this->db->f('article_id')."' ");

             if(NOT_COPY_ARTICLE_INFO==1 || $in['add_to_purchase']){
                  $def_lang = DEFAULT_LANG_ID;
                 if($in['email_language']){
                   $def_lang= $in['email_language'];
                 }
                if($in['email_language']>=1000) {
                 $def_lang = DEFAULT_LANG_ID;
                 }

                  $table = "pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
             LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id='".$def_lang."' 
             LEFT JOIN pim_article_brands ON pim_article_brands.id = pim_articles.article_brand_id";
                  $columns = 'pim_articles.*,pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
                   pim_article_prices.price, pim_articles.internal_name,
                   pim_article_brands.name AS article_brand,
                   pim_articles_lang.description AS description,
                   pim_articles_lang.name2 AS item_name2,
                   pim_articles_lang.name AS item_name,
                   pim_articles_lang.lang_id,
                   pim_articles.vat_id,
                   pim_articles.block_discount';
                  $filter_new="";
                  $filter_new.=" pim_articles.article_id='".$this->db->f('article_id')."'  and ";

                  $fieldFormat = $this->db2->field("SELECT long_value FROM settings WHERE constant_name='P_ORDER_FIELD_LABEL'");

                  $article = $this->db2->query("SELECT $columns FROM $table WHERE $filter_new pim_articles.active='1' ");

                 // $article->next();

                  $values = $article->next_array();

            $tags = array_map(function($field){
                return '/\[\!'.strtoupper($field).'\!\]/';
                },array_keys($values));

          $article_line = preg_replace($tags, $values, $fieldFormat);



              }


              if(!$in['multiple']){
                  $quantity = $this->db->f('quantity')*($this->db->f('packing')/$this->db->f('sale_unit'));
                  $last_sort_order = $this->db2->field("SELECT sort_order FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order DESC LIMIT 1");
                  $last_sort_order++;

                  $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                     p_order_id = '".$in['p_order_id']."',
                                     article_id = '".$this->db->f('article_id')."',
                                     discount = '0',
                                     quantity = '".$quantity."',
                                     article = '".addslashes($article_line)."',
                                     price = '".$purchase_price."',
                                     price_with_vat = '".$purchase_price."',

                                     sort_order='".$last_sort_order."',
                                     sale_unit = '1',
                                     packing = '1',
                                     content = '".addslashes($this->db->f('content'))."',
                                     article_code = '".addslashes($this->db->f('article_code')?$this->db->f('article_code'):$article->f('item_code'))."',
                                     supplier_reference = '".addslashes($supplier_reference)."'
                              ");
                  if($in['quote_id']){
                      $this->db2->query("UPDATE pim_p_order_articles SET facq='1' WHERE order_articles_id='".$in['p_order_articles_id']."' ");
                  }


                  $order_total+=$purchase_price*$quantity;

                  //add taxes
                  //get taxes that are applied to purchase orders
                    $taxes=$this->dbu->query("SELECT pim_articles_taxes.*, pim_article_tax.*, pim_article_tax_type.name FROM pim_articles_taxes 
                        LEFT JOIN pim_article_tax ON pim_articles_taxes.tax_id = pim_article_tax.tax_id
                        LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
                      WHERE pim_articles_taxes.article_id ='".$this->db->f('article_id')."' AND (pim_article_tax.apply_to ='0' OR pim_article_tax.apply_to ='2') ");
                      $j=$last_sort_order+1;
                      while($taxes->move_next()){
                         $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                     p_order_id = '".$in['p_order_id']."',
                                     article_id = '0',
                                     tax_id   ='".$taxes->f("tax_id")."',
                                     tax_for_article_id   ='".$this->db->f('article_id')."',
                                     discount = '0',
                                     quantity = '".$quantity."',
                                     article = '".addslashes($taxes->f("code"))."',
                                     price = '".$taxes->f("amount")."',
                                     price_with_vat = '".$taxes->f("amount")."',

                                     sort_order='".$j."',
                                     sale_unit = '1',
                                     packing = '1',
                                     content = '0',
                                     article_code = '".addslashes($taxes->f("name"))."',
                                     supplier_reference = ''
                              ");
                          $j++;
                         $order_total+=$taxes->f("amount")*$quantity;
                        if($in['quote_id']){
                            $this->db2->query("UPDATE pim_p_order_articles SET facq='1' WHERE order_articles_id='".$in['p_order_articles_id']."' ");
                        }
                      }

              }else{


                 if(array_key_exists($this->db->f('article_id'),$in['add_p_order']) && is_array($in['article'][$in['customer_id']]) && array_key_exists($this->db->f('article_id'), $in['article'][$in['customer_id']] ) )
                 {
                    $quantity=return_value($in['article'][$in['customer_id']][$this->db->f('article_id')]) ;
                    $already_added= $this->db2->field("SELECT article_id FROM pim_p_order_articles WHERE article_id='".$this->db->f('article_id')."' and  p_order_id = '".$in['p_order_id']."'");
                    if(!$already_added){
                        $last_sort_order = $this->db2->field("SELECT sort_order FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order DESC LIMIT 1");
                        $last_sort_order++;

                        $sale_unit=$this->db->f('sale_unit') ? $this->db->f('sale_unit') : 1;
                        $packing=$this->db->f('packing') ? $this->db->f('packing') : 1;

                        $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                     p_order_id = '".$in['p_order_id']."',
                                     article_id = '".$this->db->f('article_id')."',
                                     discount = '0',
                                     quantity = '".$quantity."',
                                     article = '".addslashes($article_line)."',
                                     price = '".$purchase_price."',
                                     price_with_vat = '".$purchase_price."',

                                     sort_order='".$last_sort_order."',
                                     sale_unit = '".$sale_unit."',
                                     packing = '".$packing."',
                                     content = '".addslashes($this->db->f('content'))."',
                                      article_code = '".addslashes($this->db->f('article_code')?$this->db->f('article_code'):$article->f('item_code'))."',
                                      supplier_reference = '".addslashes($supplier_reference)."'
                              ");

                         $order_total+=$purchase_price*$quantity*($packing/$sale_unit);

                         if($in['quote_id']){
                            $this->db2->query("UPDATE pim_p_order_articles SET facq='1' WHERE order_articles_id='".$in['p_order_articles_id']."' ");
                        }

                        //get taxes that are applied to purchase orders
                        $taxes=$this->dbu->query("SELECT pim_articles_taxes.*, pim_article_tax.*, pim_article_tax_type.name FROM pim_articles_taxes 
                            LEFT JOIN pim_article_tax ON pim_articles_taxes.tax_id = pim_article_tax.tax_id
                            LEFT JOIN pim_article_tax_type ON pim_article_tax.type_id = pim_article_tax_type.id
                          WHERE pim_articles_taxes.article_id ='".$this->db->f('article_id')."' AND (pim_article_tax.apply_to ='0' OR pim_article_tax.apply_to ='2') ");
                          $j=$last_sort_order+1;
                          while($taxes->move_next()){
                              $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                         p_order_id = '".$in['p_order_id']."',
                                         article_id = '0',
                                         tax_id   ='".$taxes->f("tax_id")."',
                                         tax_for_article_id   ='".$this->db->f('article_id')."',
                                         discount = '0',
                                         quantity = '".$quantity."',
                                         article = '".addslashes($taxes->f("code"))."',
                                         price = '".$taxes->f("amount")."',
                                         price_with_vat = '".$taxes->f("amount")."',

                                         sort_order='".$j."',
                                         sale_unit = '1',
                                         packing = '1',
                                         content = '0',
                                         article_code = '".addslashes($taxes->f("name"))."'
                                  ");
                              $j++;
                             $order_total+=$taxes->f("amount")*$quantity;

                             if($in['quote_id']){
                                  $this->db2->query("UPDATE pim_p_order_articles SET facq='1' WHERE order_articles_id='".$in['p_order_articles_id']."' ");
                              }
                          }

                     } //end if already added
                 }
              }
            }elseif(!$this->db->f('article_id') && !$this->db->f('content') ){ //articles out of catalog

               if(USE_ARTICLE_PURCHASE_PRICE && $in['order_id']){
                  if($order_currency_type && $customer_currency_type && $this->db->f('purchase_price_other_currency_id') && $in['currency_type'] && $this->db->f('purchase_price_other_currency_id') == $in['currency_type'] && $order_currency_type != $customer_currency_type){
                    $purchase_price=$this->db->f('purchase_price_other_currency');
                  }else{
                    $purchase_price=$this->db2->field("SELECT purchase_price FROM
                                                    pim_order_articles
                                                   WHERE order_articles_id='".$this->db->f('order_articles_id')."' AND order_id='".$in['order_id']."'

                                                    ");
                  }                
                }elseif($in['quote_id']){
                   $purchase_price=$this->db2->field("SELECT purchase_price FROM
                                                  tblquote_line
                                                 WHERE id='".$this->db->f('id')."' AND quote_id='".$in['quote_id']."'

                                                  ");
                }else{
                  $purchase_price=$this->db2->field("SELECT price FROM
                                                  pim_order_articles
                                                 WHERE order_articles_id='".$this->db->f('order_articles_id')."' AND order_id='".$in['order_id']."'

                                                  ");
                }

                 $article_line=$this->db->f('article');
                 if($in['quote_id']){
                     $article_line=$this->db->f('name');
                 }

                 $supplier_reference='';


              if(!$in['multiple']){

                if( $in['quote_id']){

                    $quantity=$this->db->f('quantity')*($this->db->f('package')/$this->db->f('sale_unit'));

                    $content =0;
                    if($this->db->f('line_type')=='3'){
                        $content =1;
                        $article_line = strip_tags(html_entity_decode($article_line));
                    }

                   $last_sort_order = $this->db2->field("SELECT sort_order FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order DESC LIMIT 1");
                   $last_sort_order++;
                   
                  $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                     p_order_id = '".$in['p_order_id']."',
                                     article_id = '0',
                                     discount = '0',
                                     quantity = '".$quantity."',
                                     article = '".addslashes($article_line)."',
                                     price = '".$purchase_price."',
                                     price_with_vat = '".$purchase_price."',

                                     sort_order='".$last_sort_order."',
                                     sale_unit = '1',
                                     packing = '1',
                                     content = '".$content."',
                                     article_code = '".$this->db->f('article_code')."',
                                    supplier_reference = '".$this->db->f('article_code')."',
                                    facq='1'
                              ");

                         $order_total+=$purchase_price*$quantity;
                       
                }else{
                  $quantity = $this->db->f('quantity')*($this->db->f('packing')/$this->db->f('sale_unit'));
                  $last_sort_order = $this->db2->field("SELECT sort_order FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order DESC LIMIT 1");
                  $last_sort_order++;

                  $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                     p_order_id = '".$in['p_order_id']."',
                                     article_id = '0',
                                     discount = '0',
                                     quantity = '".$quantity."',
                                     article = '".addslashes($article_line)."',
                                     price = '".$purchase_price."',
                                     price_with_vat = '".$purchase_price."',

                                     sort_order='".$last_sort_order."',
                                     sale_unit = '1',
                                     packing = '1',
                                     content = '".addslashes($this->db->f('content'))."',
                                     article_code = '',
                                     supplier_reference = ''
                              ");
                  $order_total+=$purchase_price*$quantity;
                }
                  
              }else{


                 if(array_key_exists($this->db->f('order_articles_id'),$in['add_p_order_out']) && is_array($in['article_out'][$in['customer_id']]) && array_key_exists($this->db->f('order_articles_id'), $in['article_out'][$in['customer_id']] )  )
                 {
                    $quantity=return_value($in['article_out'][$in['customer_id']][$this->db->f('order_articles_id')]) ;
                    // $already_added= $this->db2->field("SELECT article_id FROM pim_p_order_articles WHERE article_id='".$this->db->f('article_id')."' and  p_order_id = '".$in['p_order_id']."'");
                    // if(!$already_added){
                   $last_sort_order = $this->db2->field("SELECT sort_order FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order DESC LIMIT 1");
                   $last_sort_order++;
                   
                        $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                     p_order_id = '".$in['p_order_id']."',
                                     article_id = '0',
                                     discount = '0',
                                     quantity = '".$quantity."',
                                     article = '".addslashes($article_line)."',
                                     price = '".$purchase_price."',
                                     price_with_vat = '".$purchase_price."',

                                     sort_order='".$last_sort_order."',
                                     sale_unit = '1',
                                     packing = '1',
                                     content = '".addslashes($this->db->f('content'))."',
                                      article_code = '',
                                      supplier_reference = ''
                              ");

                         $order_total+=$purchase_price*$quantity;
                     //}
                 }


                 if( $in['quote_id'] && array_key_exists($this->db->f('id'),$in['add_p_order_out']) && is_array($in['article_out'][$in['customer_id']]) && array_key_exists($this->db->f('id'), $in['article_out'][$in['customer_id']] )  )
                 {
                    $quantity=return_value($in['article_out'][$in['customer_id']][$this->db->f('id')]) ;
                    // $already_added= $this->db2->field("SELECT article_id FROM pim_p_order_articles WHERE article_id='".$this->db->f('article_id')."' and  p_order_id = '".$in['p_order_id']."'");
                    // if(!$already_added){
                    $content =0;
                    if($this->db->f('line_type')=='3'){
                        $content =1;
                        $article_line = strip_tags(html_entity_decode($article_line));
                    }

                   $last_sort_order = $this->db2->field("SELECT sort_order FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order DESC LIMIT 1");
                   $last_sort_order++;
                   
                        $in['p_order_articles_id'] = $this->db2->insert("INSERT INTO pim_p_order_articles SET

                                     p_order_id = '".$in['p_order_id']."',
                                     article_id = '0',
                                     discount = '0',
                                     quantity = '".$quantity."',
                                     article = '".addslashes($article_line)."',
                                     price = '".$purchase_price."',
                                     price_with_vat = '".$purchase_price."',

                                     sort_order='".$last_sort_order."',
                                     sale_unit = '1',
                                     packing = '1',
                                     content = '".$content."',
                                     article_code = '".$this->db->f('article_code')."',
                                    supplier_reference = '".$this->db->f('article_code')."',
                                    facq='1'
                              ");

                         $order_total+=$purchase_price*$quantity;
                     //}
                 }
              }


            }

        } //end while

        if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
      $order_total = $order_total*return_value($in['currency_rate']);
    }

      if($in['service_id'] ){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['service_id'],'origin_type'=>'5')
                                        )
              );
          addTracking($tracking_data);
    }

    if($in['project_id'] ){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['project_id'],'origin_type'=>'3')
                                        )
              );
          addTracking($tracking_data);
    }

     if($in['order_id'] ){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['order_id'],'origin_type'=>'4')
                                        )
              );
          addTracking($tracking_data);
    }
 
     if($in['quote_id'] ){
      $tracking_data=array(
                'target_id'         => $in['p_order_id'],
                'target_type'       => '6',
                'target_buyer_id'   => $in['customer_id'],
                'lines'             => array(
                                              array('origin_id'=>$in['quote_id'],'origin_type'=>'2')
                                        )
              );
          addTracking($tracking_data);
    }


        $this->db->query("UPDATE pim_p_orders SET amount='".$order_total."' WHERE p_order_id='".$in['p_order_id']."'");
        msg::success(gm("Purchase order successfully added"),'success');
       

        $this->pag = 'p_order';
        $this->field_n = 'p_order_id';

    insert_message_log($this->pag,'{l}Purchase order successfully added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['p_order_id']);
    
    if(!$in['multiple']){
    //   ark::$controller = 'p_order';
       // ark::run('order-p_order');
      }
      }
    return true;
  }

   function saveAddToPurchase(&$in)
  {
    if($in['all']){
      if($in['value'] == 1){
        foreach ($in['item'] as $key => $value) {
          $_SESSION['add_to_purchase'][$value]= $in['value'];
        }
      }else{
        foreach ($in['item'] as $key => $value) {
          unset($_SESSION['add_to_purchase'][$value]);
        }
      }
    }else{
      if($in['value'] == 1){
        $_SESSION['add_to_purchase'][$in['item']]= $in['value'];
      }else{
        unset($_SESSION['add_to_purchase'][$in['item']]);
      }
    }
      json_out($_SESSION['add_to_purchase']);
    return true;
  }
    function update_default_email(&$in)
    {
      if(!$this->validate_default_email($in)){
        msg::error ( gm('Write email'),'error');
        return false;
      }
      $this->db->query("UPDATE default_data SET default_name='".$in['po_default_name']."', value='".$in['po_email_value']."' WHERE type='p_order_email' ");
      $this->db->query("SELECT * FROM default_data WHERE type='bcc_p_order_email' ");
      if($this->db->next()){
        $this->db->query("UPDATE default_data SET value='".$in['bcc_email']."' WHERE type='bcc_p_order_email' ");
      }else{
        $this->db->query("INSERT INTO default_data SET value='".$in['bcc_email']."', type='bcc_p_order_email' ");
      }
      msg::success ( gm('Default email updated.'),'success');
      return true;
    }
    function validate_default_email(&$in)
    {
      $v = new validation($in);
      $v->field('po_value', 'Email', 'email');
      if($in['bcc_email']){   
        $v->field('bcc_email', 'Type of link', 'emails');
      }

      return $v->run();
    }

    /**
   * make_return
   *
   * @return bool
   * @author PM
   **/
    function make_return(&$in)
    {
    $in['failure'] = false;
    if(!$this->make_return_validate($in)){
      $in['failure'] = true;
      return false;
    }

    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }
    if(isset($in['date']) && !empty($in['date']) ){
      $in['date'] = strtotime($in['date']);
    }
    $return_id=$this->db->insert("INSERT INTO  pim_p_orders_return
                            SET p_order_id='".$in['p_order_id']."',
                               `date` = '".$in['date']."',
                                note = '".$in['note']."' ");

    if($in['lines']){
      foreach ($in['lines'] as $key => $val) {
        $article_id = $val['article_id'];
        $q = $val['ret_quantity'];

        $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        $article_data->next();
        $stock = $article_data->f("stock");

        $stock_packing = $article_data->f("packing");
        if(!$const['ALLOW_ARTICLE_PACKING']){
          $stock_packing = 1;
        }
        $stock_sale_unit = $article_data->f("sale_unit");
        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          $stock_sale_unit = 1;
        }
        
        $this->db->query("INSERT INTO  pim_p_articles_return
                            SET return_id='".$return_id."',
                                article_id = '".$article_id."',
                                quantity = '".return_value($q)."',
                                p_order_id='".$in['p_order_id']."' ");
        //update batches
        foreach ($val['selected_b_n'] as $key3 => $v) {

          if($v['batch_no_checked']){
           $location_id = $v['address_id'];
           $v['b_n_q_selected'] = return_value($v['b_n_q_selected']);

            $current_in_stock = $this->db->field("SELECT in_stock FROM batches WHERE id = '".$v['batch_id']."' AND article_id = '".$article_id."' ");
            $new_in_stock = $current_in_stock-$v['b_n_q_selected']*$stock_packing/$stock_sale_unit;

            $status_id = 1;
            if($new_in_stock==0){
              $status_id = 2;
            }

            $this->db->query("UPDATE batches SET in_stock= '".$new_in_stock."' WHERE id = '".$v['batch_id']."' AND article_id = '".$article_id."' ");

            $location_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['batch_id']."'");

              if($location_current_stock_batch->move_next()){
                    //update dispach batch from stock
                      $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')-($v['b_n_q_selected']*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['batch_id']."'");
                }



            $current_in_batches = $this->db->field("SELECT quantity FROM batches_from_orders WHERE batch_id = '".$v['batch_id']."' AND article_id = '".$article_id."' AND p_order_id='".$in['p_order_id']."' ");
                if($current_in_batches){
                  $this->db->query("INSERT INTO batches_from_orders SET
                      order_articles_id =   '".$v['order_articles_id']."',
                      article_id    =     '".$article_id."',
                      batch_id    =     '".$v['batch_id']."',
                      return_id     =     '".$return_id."',
                      p_order_id    =     '".$in['p_order_id']."',
                      quantity    =     '".$v['b_n_q_selected']*$stock_packing/$stock_sale_unit."',
                      address_id    =     '".$v['address_id']."',
                      delivery_id     =       '0'  
                      ");
                }
        }
      }
        //update stock
        $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        $article_data->next();
        $stock = $article_data->f("stock");

        $stock_packing = $article_data->f("packing");
        if(!$const['ALLOW_ARTICLE_PACKING']){
          $stock_packing = 1;
        }
        $stock_sale_unit = $article_data->f("sale_unit");
        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          $stock_sale_unit = 1;
        }

        $new_stock = $stock - (return_value($q)*$stock_packing/$stock_sale_unit);
        $this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

        $now=time();
        $serial_number = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
        $this->db->insert("INSERT INTO stock_movements SET
          `date`              = '".time()."',
          created_by          = '".$_SESSION['u_id']."',
          p_order_id          =   '".$in['p_order_id']."',
          serial_number         =   '".$serial_number."',
          article_id          = '".$article_id."',
          article_name            =   '".addslashes($article_data->f("internal_name"))."',
          item_code           =   '".addslashes($article_data->f("item_code"))."',
          movement_type       = '13',
          quantity          =   '".return_value($q)*$stock_packing/$stock_sale_unit."',
          stock               = '".$stock."',
          new_stock           =   '".$new_stock."',
          delivery_date       ='".$in['date']."' ");
        if(WAC){
          generate_purchase_price($article_id);
        }
        //now we have to modify the stock on main location

        if($const['DEFAULT_STOCK_ADDRESS']){
          $current_stock_main= $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
          $this->db->query("UPDATE dispatch_stock SET stock='".($current_stock_main -(return_value($q)*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
        }
      }
    }
    if($const['CAN_SYNC']){
      $packet = array(
        'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['p_order_id']);
    }
    msg::success ( gm('Changes saved'),'success');
    return true;
  }

  /**
   * make_return_validate
   *
   * @return bool
   * @author PM
   **/
  function make_return_validate(&$in){

    $v = new validation($in);
    if($in['lines']){
      foreach ($in['lines'] as $key => $val) {
        if(return_value($val['ret_quantity'])> return_value($val['quantity_rem'])) {
          msg::error(gm('Invalid Quantities'),'error');
          return false;
        }
      }
    }
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    return $v->run();

  }

  /**
   * edit_return
   *
   * @return bool
   * @author me
   **/
  function edit_return(&$in)
  {//var_dump($in);exit();

    if(!$this->update_return_validate($in)){
      return false;
    }
    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }

    if(isset($in['date']) && !empty($in['date']) ){
      $in['date'] = strtotime($in['date']);
    }

    $this->db->query("UPDATE pim_p_orders_return SET
                               `date` = '".$in['date']."',
                                note = '".$in['note']."'
                      WHERE  return_id='".$in['return_id']."' ");

    $this->db->query("DELETE FROM pim_p_articles_return WHERE  return_id='".$in['return_id']."'");

    if($in['lines']){
      foreach ($in['lines'] as $key => $val) {
        $article_id = $val['article_id'];
        $q = $val['ret_quantity'];
        $this->db->query("INSERT INTO  pim_p_articles_return
                            SET return_id='".$in['return_id']."',
                                article_id = '".$article_id."',
                                quantity = '".return_value($q)."',
                                p_order_id='".$in['p_order_id']."' ");

       
        $q=return_value($q)-$val['old_ret_quantity'];
        //update stock
        $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        $article_data->next();
        $stock = $article_data->f("stock");

        $stock_packing = $article_data->f("packing");
        if(!$const['ALLOW_ARTICLE_PACKING']){
          $stock_packing = 1;
        }
        $stock_sale_unit = $article_data->f("sale_unit");
        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          $stock_sale_unit = 1;
        }

        $new_stock = $stock - ($q*$stock_packing/$stock_sale_unit);
        $this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

        $now=time();
        $serial_number = $this->db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");
        if($new_stock!=$stock){
          $this->db->insert("INSERT INTO stock_movements SET
            date              = '".time()."',
            created_by          = '".$_SESSION['u_id']."',
            p_order_id          =   '".$in['p_order_id']."',
            serial_number     =   '".$serial_number."',
            article_id        = '".$article_id."',
            article_name        =   '".addslashes($article_data->f("internal_name"))."',
            item_code           =   '".addslashes($article_data->f("item_code"))."',
            movement_type       = '14',
            quantity          =   '".return_value($q)*$stock_packing/$stock_sale_unit."',
            stock               = '".$stock."',
            new_stock           =   '".$new_stock."',
            delivery_date       ='".$in['date']."' ");
          //now we have to modify the stock on main location
          if(WAC){
            generate_purchase_price($article_id);
          }

          if($const['DEFAULT_STOCK_ADDRESS']){
            $current_stock_main= $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
            $this->db->query("UPDATE dispatch_stock SET stock='".($current_stock_main - (return_value($q)*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
          }
        }
      }
    }

    if($const['CAN_SYNC']){
      $packet = array(
        'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['p_order_id']);
    }
    msg::success ( gm('Changes saved'),'success');
    return true;
  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function update_return_validate(&$in){

    $v = new validation($in);
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    $v->field('return_id', 'ID', 'required:exist[pim_p_orders_return.return_id]', "Invalid Id");
    if($in['lines']){
      foreach ($in['lines'] as $key => $val) {
        if(return_value($val['ret_quantity'])> return_value($val['quantity_rem'])){
          msg::error(gm('Invalid Quantities'),'error');
          return false;
        }
      }
    }
    return $v->run();

  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function delete_return(&$in){

    if(!$this->delete_return_validate($in)){
      return false;
    }

    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }

    $return_date = $this->db->field("SELECT pim_p_orders_return.date FROM pim_p_orders_return WHERE p_order_id='".$in['p_order_id']."' AND return_id='".$in['return_id']."' ");
    $this->db->query("SELECT * FROM pim_p_articles_return WHERE  return_id='".$in['return_id']."' ");
    while ($this->db->move_next()) {
      //update stock
      $article_data = $this->db2->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$this->db->f('article_id')."' ");
      $article_data->next();
      $stock = $article_data->f("stock");

      $stock_packing = $article_data->f("packing");
      if(!$const['ALLOW_ARTICLE_PACKING']){
        $stock_packing = 1;
      }
      $stock_sale_unit = $article_data->f("sale_unit");
      if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
        $stock_sale_unit = 1;
      }

      $new_stock = $stock + ($this->db->f('quantity')*$stock_packing/$stock_sale_unit);
      $this->db2->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$this->db->f('article_id')."'");

      //delete order details from article batches
      $batches_data = $this->db2->query("SELECT * FROM batches_from_orders
            WHERE     p_order_id    =   '".$in['p_order_id']."'
            AND       return_id   =   '".$in['return_id']."'
            AND       article_id =  '".$this->db->f('article_id')."' ");
      while($batches_data->next()){
            $batch_in_stock = $this->db2->field("SELECT in_stock FROM batches WHERE id = '".$batches_data->f('batch_id')."' ");
            $new_batch_stock = $batch_in_stock + $batches_data->f('quantity');
            $this->db2->query("UPDATE batches SET status_id = 1, in_stock = '".$new_batch_stock."', status_details_2 = '' WHERE id = '".$batches_data->f('batch_id')."' ");

            $location_current_stock_batch = $this->db2->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$batches_data->f('article_id')."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");

            if($location_current_stock_batch->move_next()){
                  //update dispach batch from stock
                    $this->db2->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')+ $batches_data->f('quantity'))."' WHERE article_id='".$batches_data->f('article_id')."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");
              }
          }
      $this->db2->query("DELETE FROM batches_from_orders
            WHERE     p_order_id    =   '".$in['p_order_id']."'
            AND       return_id   =   '".$in['return_id']."' 
            AND       article_id =  '".$this->db->f('article_id')."' ");

      $now = time();
      $serial_number = $this->db2->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id = '".$in['p_order_id']."'");

      $this->db2->insert("INSERT INTO stock_movements SET
      date            = '".time()."',
      created_by          = '".$_SESSION['u_id']."',
      serial_number         =   '".$serial_number."',
      p_order_id          =   '".$in['p_order_id']."',
      article_id          = '".$this->db->f('article_id')."',
      article_name            =   '".addslashes($article_data->f("internal_name"))."',
      item_code           =   '".addslashes($article_data->f("item_code"))."',
      movement_type       = '15',
      quantity          =   '".$this->db->f('quantity')*$stock_packing/$stock_sale_unit."',
      stock             = '".$stock."',
      new_stock           =   '".$new_stock."',
      backorder_articles      =   '".$backorder_articles."'
      ");
      /*end for  stock movements*/
      array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$this->db->f('article_id')."'");
      if(WAC){
        generate_purchase_price($article_id);
      }
      //now we have to mod ify the stock on main location

      if($const['DEFAULT_STOCK_ADDRESS']){
        $current_stock_main= $this->db2->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$this->db->f('article_id')."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
        $this->db2->query("UPDATE dispatch_stock SET stock='".($current_stock_main +($this->db->f('quantity')*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$this->db->f('article_id')."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
      }
    }

    $this->db->query("DELETE FROM pim_p_orders_return WHERE p_order_id='".$in['p_order_id']."' AND return_id='".$in['return_id']."' ");
    $this->db->query("DELETE FROM pim_p_articles_return WHERE  return_id='".$in['return_id']."' ");

    if(CAN_SYNC){
      $packet = array(
        'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
      Sync::end();
    }

    msg::success ( gm('Return was succesfully deleted'),'success');
    return true;
  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function delete_return_validate(&$in){

    $v = new validation($in);
    $v->field('p_order_id', 'ID', 'required:exist[pim_p_orders.p_order_id]', "Invalid Id");
    $v->field('return_id', 'ID', 'required:exist[pim_p_orders_return.return_id]', "Invalid Id");

    return $v->run();
  }



    function postRegister(&$in){
        global $config;

        $env_usr = $config['postgreen_user'];
        $env_pwd = $config['postgreen_pswd'];

        $post_green_data = array('user_id' => $_SESSION['u_id'],
                      'database_name' => DATABASE_NAME,
                      'item_id'   => '',
                      'module'    => 'po_order',
                      'date'      => time(),
                      'action'    => 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'],
                      'content'   => '',
                      'login_user'  => addslashes($in['user']),
                      'error_message' => ''
              );

        try{

        $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
        $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
        $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
        $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
        $soap->__setSoapHeaders(array($objHeader_Session_Outside));
        $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$in['user'],'password'=>$in['pass']));

        if($res->action->data[0]->_ == '000'){
          $post_id=$this->dbu->insert("INSERT INTO apps SET
                                                  main_app_id='0',
                                                  name='PostGreen',
                                                  api='".$in['user']."',
                                                  type='main',
                                                  active='1' ");
          $this->dbu->query("INSERT INTO apps SET main_app_id='".$post_id."', api='".$in['pass']."' ");
          /*msg::success(gm("PostGreen service activated"),"success");*/
          msg::success ( gm("Changes have been saved."),'success');
        }else{
          msg::error(gm('Incorrect credentials provided'),"error");

        }

      } catch (Exception $e) {
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");
        $post_green_data['error_message'] = addslashes($e->getMessage());  
        error_post($post_green_data);
        
        return true;
    }

        return true;
    }

    function postIt(&$in){
        global $config;

        $id=$in['id'];

        $order=$this->dbu->query("SELECT * FROM pim_p_orders WHERE p_order_id='".$id."' ");

        $buyer_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".$order->f('customer_country_id')."' ");
        $seller_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".ACCOUNT_BILLING_COUNTRY_ID."' ");

        $in['p_order_id']=$id;
        $in['lid'] = $order->f('email_language');
        if(!$in['lid']){
          $in['lid'] = 1;
        }
        $in['type']=$order->f('p_order_type') ? $order->f('p_order_type') : ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
        $this->generate_pdf($in);
        $in['attach_file_name'] = 'p_order_.pdf';
        $doc_name='purchase_order_'.$order->f('serial_number').'.pdf';

        $handle = fopen($in['attach_file_name'], "r");   
        $contents = fread($handle, filesize($in['attach_file_name'])); 
        fclose($handle);                               
        $decodeContent = base64_encode($contents);

        $filename ="addresses.csv";
        ark::loadLibraries(array('PHPExcel'));
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Akti")
                 ->setLastModifiedBy("Akti")
                 ->setTitle("Addresses")
                 ->setSubject("Addresses")
                 ->setDescription("Addresses")
                 ->setKeywords("Addresses")
                 ->setCategory("Addresses");

        $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A1', "TITLE")
              ->setCellValue('B1', "FIRST_NAME")
              ->setCellValue('C1', "LAST_NAME")
              ->setCellValue('D1', "EMAIL")
              ->setCellValue('E1', "PHONE")
              ->setCellValue('F1', "MOBILE")
              ->setCellValue('G1', "FAX")
              ->setCellValue('H1', "COMPANY")
              ->setCellValue('I1', "ADDRESS_LINE_1")
              ->setCellValue('J1', "ADDRESS_LINE_2")
              ->setCellValue('K1', "ADDRESS_LINE_3")
              ->setCellValue('L1', "ADDRESS_POSTCODE")
              ->setCellValue('M1', "ADDRESS_CITY")
              ->setCellValue('N1', "ADDRESS_STATE")
              ->setCellValue('O1', "ADDRESS_COUNTRY");

        $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A2', "")
              ->setCellValue('B2', "")
              ->setCellValue('C2', "")
              ->setCellValue('D2', $order->f('customer_email'))
              ->setCellValue('E2', $order->f('customer_phone'))
              ->setCellValue('F2', "")
              ->setCellValue('G2', $order->f('customer_fax'))
              ->setCellValue('H2', $order->f('customer_name'))
              ->setCellValue('I2', $order->f('customer_address'))
              ->setCellValue('J2', "")
              ->setCellValue('K2', "")
              ->setCellValue('L2', $order->f('customer_zip'))
              ->setCellValue('M2', $order->f('customer_city'))
              ->setCellValue('N2', "")
              ->setCellValue('O2', $buyer_country);

        $objPHPExcel->getActiveSheet()->setTitle('Addresses');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
        $objWriter->setDelimiter(';');
        $objWriter->save($filename);

        $handle_csv = fopen($filename, "r");   
        $contents_csv = fread($handle_csv, filesize($filename)); 
        fclose($handle_csv);                               
        $csvContent   = base64_encode($contents_csv);
       
        $env_usr = $config['postgreen_user'];
        $env_pwd = $config['postgreen_pswd'];
        $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
        $panel_usr=$postg_data->f("api");
        $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");

        $post_green_data = array('user_id' => $_SESSION['u_id'],
                      'database_name' => DATABASE_NAME,
                      'item_id'   => $in['id'],
                      'module'    => 'po_order',
                      'date'      => time(),
                      'action'    => 'ORDER_CREATE'.' - '.$config['postgreen_wsdl'],
                      'login_user'  => addslashes($panel_usr),
                      'content'   => '',
                      'error_message' => ''
              );

        try{

        $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
        $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
        $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
        $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
        $soap->__setSoapHeaders(array($objHeader_Session_Outside));
        $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));

        $soapVr='
          <order_create_IN>
            <action type="string">ORDER_CREATE</action> 
            <attachment_list type="stringlist"></attachment_list>'; 
           if($in['stationary_id']){
              $soapVr.='<background type="string">'.$in['stationary_id'].'</background>';
            }else{
              $soapVr.='<background type="string"></background>';
            } 
            $soapVr.='<content_parameters type="indstringlist"></content_parameters> 
            <csv type="file" extension="csv"> 
              <nir:data>'.$csvContent.'</nir:data> 
            </csv> 
            <document type="file" extension="pdf"> 
              <nir:data>'.$decodeContent.'</nir:data> 
            </document> 
            <document_name type="string">'.$doc_name.'</document_name> 
            <get_proof type="string">NO</get_proof> 
            <identifier type="indstringlist"> 
              <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data> 
            </identifier> 
            <insert_list type="indstringlist"></insert_list> 
            <mailing_type type="string">NONE</mailing_type> 
            <order_parameters type="indstringlist"></order_parameters> 
            <page_list type="stringlist"></page_list> 
            <return_address type="indstringlist">
              <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
              <nir:data key="ADDRESSLINE2"></nir:data>
              <nir:data key="ADDRESSLINE3"></nir:data>
              <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
              <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
              <nir:data key="ADDRESSLINE6"></nir:data>
              <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
            </return_address> 
            <sender_address type="indstringlist">
              <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
              <nir:data key="ADDRESSLINE2"></nir:data>
              <nir:data key="ADDRESSLINE3"></nir:data>
              <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
              <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
              <nir:data key="ADDRESSLINE6"></nir:data>
              <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
            </sender_address> 
            <service_profile type="indstringlist"> 
              <nir:data key="service_id">CUSTOM_ENVELOPE</nir:data> 
              <nir:data key="print_mode">1SIDE</nir:data>
              <nir:data key="envelop_type">CUSTOM</nir:data>
              <nir:data key="paper_weight">80g</nir:data>
              <nir:data key="print_color">COLOR</nir:data>
              <nir:data key="mail_type">EXPRESS</nir:data>
              <nir:data key="address_page">EXTRA_PAGE</nir:data>
              <nir:data key="validation">NO</nir:data>
            </service_profile> 
          </order_create_IN>';

          $post_green_data['content'] = addslashes($soapVr);
          
        $params = new \SoapVar($soapVr, XSD_ANYXML);
        
        $result = $soap->order_create($params);
        if($result->action->data[0]->_ == '000'){
          $this->dbu->query("UPDATE pim_p_orders SET postgreen_id='".$result->order_id->_."' WHERE p_order_id='".$id."' ");
          $in['sent_date']= date('c');
          $this->mark_sent($in);
          $msg_txt =gm('Purchase order successfully exported');
        }else{
          msg::error($result->action->data[1]->_,"error");
          return true;
        }

      } catch (Exception $e) {
        unlink($filename);
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");
        $post_green_data['error_message'] = addslashes($e->getMessage());
        if(empty($soapVr)){
          $post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
        }
        error_post($post_green_data);
        
        json_out($in);
    }

        unlink($filename);

        msg::success($msg_txt,"success");
        if($in['related']=='view'){        
          // return true;
        }else{
          unset($in['p_order_id']); 
        }
        return true;
    }

    function postOrderData(&$in){
      if($in['old_obj']['no_status']){
          json_out($in);
      }else{
          try{
            global $config;
            $env_usr = $config['postgreen_user'];
            $env_pwd = $config['postgreen_pswd'];
            $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
            $panel_usr=$postg_data->f("api");
            $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");

            $post_green_data = array('user_id' => $_SESSION['u_id'],
                      'database_name' => DATABASE_NAME,
                      'item_id'   => $in['p_order_id'],
                      'module'    => 'po_order',
                      'date'      => time(),
                      'action'    => 'ORDER_DETAILS'.' - '.$config['postgreen_wsdl'],
                      'login_user'  => addslashes($panel_usr),
                      'content'   => '',
                      'error_message' => ''
              );

            $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
            $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
            $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
            $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
            $soap->__setSoapHeaders(array($objHeader_Session_Outside));
            $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));
            
            $order_id=$this->dbu->field("SELECT postgreen_id FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");

            $post_green_data['content'] = addslashes('<order_details_IN>
                <action type="string">ORDER_DETAILS</action>
                <get_billing>NO</get_billing>
                <get_document>NO</get_document>
                <get_proof>NO</get_proof>
                <identifier type="indstringlist">
                  <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
                </identifier>
                <order_id type="string">'.$order_id.'</order_id>
              </order_details_IN>');

            $params = new \SoapVar('
              <order_details_IN>
                <action type="string">ORDER_DETAILS</action>
                <get_billing>NO</get_billing>
                <get_document>NO</get_document>
                <get_proof>NO</get_proof>
                <identifier type="indstringlist">
                  <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
                </identifier>
                <order_id type="string">'.$order_id.'</order_id>
              </order_details_IN>',
               XSD_ANYXML);

            $result=$soap->order_details($params);
        }catch(Exception $e){
          $post_green_data['error_message'] = addslashes($e->getMessage());

          if(empty($post_green_data['content'])){
            $post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
            } 

          error_post($post_green_data);
              $post_status=array('status'=>gm('Postgreen service is temporarily unavailable. Please retry later.'));
            if($in['related']=='minilist'){
              json_out($post_status);
            }else if($in['related']=='status'){
              $in['status']=$post_status['status'];
              json_out($in);
            }else{
              return json_encode($post_status);
            }
        }
        $PGreen_status=array(
          '100'   => gm('PostGreen Received'),
          '140'   => gm('PostGreen Waiting for validation'),
          '-140'  => gm('PostGreen Rejected'),
          '160'   => gm('PostGreen Saved'),
          '200'   => gm('PostGreen Confirmed'),
          '210'   => gm('PostGreen Processing'),
          '300'   => gm('PostGreen Processed'),
          '400'   => gm('PostGreen Validated'),
          '500'   => gm('PostGreen Printed'),
          '600'   => gm('PostGreen Posted'),
          '700'   => gm('PostGreen Sent'),
          '800'   => gm('PostGreen Arrived'),
          '810'   => gm('PostGreen Opened'),
          '900'   => gm('PostGreen Canceled')
        );
        $post_status=array('status'=>gm('Post status').": ".$PGreen_status[$result->order_info->row->col[3]->data]);
        if($in['related']=='minilist'){
          json_out($post_status);
        }else if($in['related']=='status'){
          $in['status']=$post_status['status'];
          json_out($in);
        }else{
          return json_encode($post_status);
        }
      }        
    }

    function updateSegment(&$in){
      $this->db->query("UPDATE pim_p_orders SET segment_id='".$in['segment_id']."' WHERE p_order_id='".$in['p_order_id']."' ");
      if($in['segment_id']){
        $in['segment'] = $this->db->field("SELECT name FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
      }
      msg::success ( gm("Changes have been saved."),'success');
      json_out($in);
    }

    function updateSource(&$in){
      $this->db->query("UPDATE pim_p_orders SET source_id='".$in['source_id']."' WHERE p_order_id='".$in['p_order_id']."' ");
      if($in['source_id']){
        $in['source'] = $this->db->field("SELECT name FROM tblquote_source WHERE id='".$in['source_id']."' ");
      }
      msg::success ( gm("Changes have been saved."),'success');
      json_out($in);
    }

    function updateType(&$in){
      $this->db->query("UPDATE pim_p_orders SET type_id='".$in['type_id']."' WHERE p_order_id='".$in['p_order_id']."' ");
      if($in['type_id']){
        $in['xtype']=$this->db->field("SELECT name FROM tblquote_type WHERE id='".$in['type_id']."' ");
      }
      msg::success ( gm("Changes have been saved."),'success');
      json_out($in);
    }

    function changedelivery(&$in){

      if(!$in['delivery_type'] || $in['delivery_type'] > 2){
        msg::error ( gm("Invalid ID"),'error');
        return false;
      }else{
        $order_del_step = $this->db->field("SELECT value FROM settings WHERE constant_name = 'P_ORDER_DELIVERY_STEPS' ");
        $this->db->query("UPDATE settings SET value = '".$in['delivery_type']."' WHERE constant_name = 'P_ORDER_DELIVERY_STEPS' ");
        if($in['delivery_type']==2&&$order_del_step==1){
          $this->db->query("UPDATE pim_p_order_deliveries SET delivery_done = '1' ");
        }
        msg::success ( gm("Changes have been saved."),'success');
        return true;
      }
/*      $this->db->query("UPDATE settings SET value='".$in['delivery_type']."' WHERE constant_name='P_ORDER_DELIVERY_STEPS'");
      msg::success ( gm("Changes have been saved."),'success');
      return true;*/
    }

    function get_article_batch_expdate(&$in){
  
    $in['article_id'] =$this->db->field("SELECT article_id FROM pim_p_order_articles WHERE order_articles_id = '".$in['order_articles_id']."'");
    if( $in['article_id']){
      $batches= $this->db->query("SELECT * FROM batches WHERE article_id='".$in['article_id']."'");
      $batches_arr=array(); 
      while($batches->next()){
        $batches_arr[$batches->f('id')] = $batches->f('batch_number');

      }
               
 /*    $in['date_exp']=array();
     if (!is_array($in['batch_number_v'][$in['order_articles_id']])){
       if( in_array($in['batch_number_v'][$in['order_articles_id']], $batches_arr)){
              $in['date_exp'][$i]=$this->db->field("SELECT date_exp FROM batches WHERE batch_number='".$in['batch_number_v'][$in['order_articles_id']]."'");
        }
      }else{
        foreach($in['batch_number_v'][$in['order_articles_id']] as $key=>$value){*/
        if( in_array($in['batch_number'], $batches_arr)){
              $in['date_exp']=$this->db->field("SELECT date_exp FROM batches WHERE batch_number='".$in['batch_number']."'");
              $in['date_exp_js']=$in['date_exp']*1000;
        }else{
          $in['date_exp']=0;
          $in['date_exp_js']=0;
        }
     /* }

      }*/
    }
       
    json_out($in);
  }

  function saveAddToPdf(&$in)
  {
    if($in['all']){
      if($in['value'] == 1){
        foreach ($in['item'] as $key => $value) {
          $_SESSION['add_to_pdf_po'][$value]= $in['value'];
        }
      }else{
        foreach ($in['item'] as $key => $value) {
          unset($_SESSION['add_to_pdf_po'][$value]);
        }
      }
    }else{
      if($in['value'] == 1){
        $_SESSION['add_to_pdf_po'][$in['item']]= $in['value'];
      }else{
        unset($_SESSION['add_to_pdf_po'][$in['item']]);
      }
    }
    $all_pages_selected=false;
    $minimum_selected=false;
    if($_SESSION['add_to_pdf_po'] && count($_SESSION['add_to_pdf_po'])){
      if($_SESSION['tmp_add_to_pdf_po'] && count($_SESSION['tmp_add_to_pdf_po']) == count($_SESSION['add_to_pdf_po'])){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
    //return true;
  }

  function saveAddToPdfAll(&$in){
   $all_pages_selected=false;
   $minimum_selected=false;
   if($in['value']){
    unset($_SESSION['add_to_pdf_po']);
    if($_SESSION['tmp_add_to_pdf_po'] && count($_SESSION['tmp_add_to_pdf_po'])){
      $_SESSION['add_to_pdf_po']=$_SESSION['tmp_add_to_pdf_po'];
      $all_pages_selected=true;
    }
   }else{
      unset($_SESSION['add_to_pdf_po']);
   }
   $in['all_pages_selected']=$all_pages_selected;
   $in['minimum_selected']=$minimum_selected;
   json_out($in);
  }

  /**
   * undocumented function
   *
   * @return void
   * @author PM
   **/
  function tryAddAddress(&$in){
    if(!$this->CanEdit($in)){
      json_out($in);
      return false;
    }
    if(!$this->tryAddAddressValidate($in)){
      json_out($in);
      return false;
    }

    $country_n = get_country_name($in['country_id']);
    if($in['field']=='customer_id'){
      Sync::start(ark::$model.'-'.ark::$method);

      $address_id = $this->db->insert("INSERT INTO customer_addresses SET
                                        customer_id     = '".$in['customer_id']."',
                                        country_id      = '".$in['country_id']."',
                                        state_id      = '".$in['state_id']."',
                                        city        = '".$in['city']."',
                                        zip         = '".$in['zip']."',
                                        address       = '".$in['address']."',
                                        billing       = '".$in['billing']."',
                                        is_primary      = '".$in['primary']."',
                                        delivery      = '".$in['delivery']."'");
      if($in['billing']){
        $this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
      }

      Sync::end($address_id);

      /*if($in['delivery']){
        $this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
      }*/
      if($in['primary']){
        if($in['address'])
        {
          if(!$in['zip'] && $in['city'])
          {
            $address = $in['address'].', '.$in['city'].', '.$country_n;
          }elseif(!$in['city'] && $in['zip'])
          {
            $address = $in['address'].', '.$in['zip'].', '.$country_n;
          }elseif(!$in['zip'] && !$in['city'])
          {
            $address = $in['address'].', '.$country_n;
          }else
          {
            $address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
          }
          $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
          $output= json_decode($geocode);
          $lat = $output->results[0]->geometry->location->lat;
          $long = $output->results[0]->geometry->location->lng;
        }
        $this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
        $this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
        $this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
      }
    }else{      
      $address_id = $this->db->insert("INSERT INTO customer_contact_address SET
                                        contact_id      = '".$in['contact_id']."',
                                        country_id      = '".$in['country_id']."',                                        city        = '".$in['city']."',
                                        zip         = '".$in['zip']."',
                                        address       = '".$in['address']."',
                                        is_primary      = '".$in['primary']."',
                                        delivery      = '".$in['delivery']."'");
    }
    msg::success ( gm("Changes have been saved."),'success');
    insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
    $in['second_address_id'] = $address_id;
    $in['country'] = $country_n;
    $in['p_order_id'] = $in['item_id'];
    if($in['p_order_id'] && is_numeric($in['p_order_id'])){
      $delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
      $this->db->query("UPDATE pim_p_orders SET 
          delivery_address='".addslashes($delivery_address)."', 
          delivery_id='".$address_id."' WHERE p_order_id='".$in['p_order_id']."'  "); 
      }elseif ($in['p_order_id'] == 'tmp') {
        $in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
      }
      $in['sc_id'] = $in['customer_id'];
    // $in['pagl'] = $this->pag;
    
    // json_out($in);
    return true;

  }

  /**
   * undocumented function
   *
   * @return void
   * @author PM
   **/
  function tryAddAddressValidate(&$in)
  {
    $v = new validation($in);

    if($in['customer_id']){
      $v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
      if(!$in['primary'] && !$in['delivery'] && !$in['billing'] && !$in['site']){
        $v->field('primary', 'primary', 'required',gm("Main Address is mandatory"));
        $v->field('delivery', 'delivery', 'required',gm("Delivery Address is mandatory"));
        $v->field('billing', 'billing', 'required',gm("Billing Address is mandatory"));
        $v->field('site', 'site', 'required',gm("Site Address is mandatory"));

        $message = 'You must select an address type.';
        $is_ok = $v->run();
        if(!$is_ok){
          msg::error(gm($message),'error');
          return false;   
        }
      } else {
        $v->field('address', 'Address', 'required:text',gm("Street is mandatory"));
        $v->field('zip', 'Zip', 'required:text',gm("Zip Code is mandatory"));
        $v->field('city', 'City', 'required:text',gm("City is mandatory"));
        $v->field('country_id', 'Country', 'required:exist[country.country_id]');
      }
    }else if($in['contact_id']){
      $v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
    }else{
      msg::error(gm('Invalid ID'),'error');
      return false;
    }
    $v->field('country_id', 'Country', 'required:exist[country.country_id]',gm("Country is mandatory"));

    return $v->run();
  }

  function CanEdit(&$in){
    if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
      return true;
    }
    $c_id = $in['customer_id'];
    if(!$in['customer_id'] && $in['contact_id']) {
      $c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
    }
    if($c_id){
      if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
        $u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
        if($u){
          $u = explode(',', $u);
          if(in_array($_SESSION['u_id'], $u)){
            return true;
          }
          else{
            msg::$warning = gm("You don't have enought privileges");
            return false;
          }
        }else{
          msg::$warning = gm("You don't have enought privileges");
          return false;
        }
      }
    }
    return true;
  }

  function PVCheck(&$in){
      global $config;
      $app=$this->db->query("SELECT * FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");
    
      if($app->next()){
      if($app->f('active') == 1){


          $facq_id = $app->f('app_id');
          $UserID=$this->db->field("SELECT api FROM apps WHERE type='UserID' AND main_app_id='".$facq_id."' ");
          $Login=$this->db->field("SELECT api FROM apps WHERE type='Login' AND main_app_id='".$facq_id."' ");
          $Password=$this->db->field("SELECT api FROM apps WHERE type='Password' AND main_app_id='".$facq_id."' ");

          $opts = array(
              'http' => array(
                  'user_agent' => 'PHPSoapClient'
              ),
              'ssl' => array(
              'verify_peer' => false,
              'verify_peer_name' => false,
              'allow_self_signed' => true
              )
          );
          $context = stream_context_create($opts);

         // $facq_lines=$this->db->query("SELECT * FROM  pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."'  AND facq='1' ");
           $facq_lines=$this->db->query("SELECT * FROM  pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."'  ");

          while($facq_lines->next()){
            if($facq_lines->f('article_code') && $facq_lines->f('article_code')!='000000' && $facq_lines->f('article_code')!='990000'){
              $requestParams =array(
                    'UserID'                  => $UserID,
                    'Login'                   => $Login,
                    'Password'                => $Password,
                    'CultureID'               => 'FR',   
                    'ListPVCheckRequestProduct' =>array('PVCheckRequestProduct'=>array('ProductID'=>$facq_lines->f('article_code')))
                  );

               try
                {
                    $soap = new SoapClient($config['facq_wsdl'],array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY, 'stream_context' => $context));
                  $res = $soap->PVCheck(array("request" => $requestParams));
      

                  $status = $res->PVCheckResult->Status;
                  $product = $res->PVCheckResult->ListProduct->Product;
                  $code = $product->Status->Code;
                  $purchase_price = $product->NetPriceTaxExcluded;
                  $supplier_reference = $product->ProductID;
               
                    if($code=='0'){
                      $article_id= $this->db->field("SELECT article_id FROM pim_articles WHERE supplier_reference='".$supplier_reference."' and facq='1' ");
                      if($article_id){
                        //var_dump($article_id);
                        //$this->update_facq_purchase_price($article_id, $purchase_price);
                      //$this->db->query("UPDATE pim_p_order_articles SET price = '".$purchase_price."', price_with_vat = '".$purchase_price."' WHERE article_id='".$article_id."' AND p_order_id='".$in['p_order_id']."' AND facq='1'");
                        $this->db->query("UPDATE pim_p_order_articles SET price = '".$purchase_price."', price_with_vat = '".$purchase_price."' WHERE article_id='".$article_id."' AND p_order_id='".$in['p_order_id']."' ");
                      $this->recalculate_total($in);
                      insert_message_log('p_order','{l}Facq prices were updated by{endl} '.get_user_name($_SESSION['u_id']),'p_order_id',$in['p_order_id']);
                
                      }
                     
                    }else{
                      msg::error($product->Status->Description,"error");
                    return false;
                    }
                     }
                catch(SoapFault $fault)
                {
                    msg::error ( gm('Error'), 'error');         
                    return false;
              
                }

            }//end if
            

          }//end while


            
      }
    }
    msg::success ( gm("Facq prices were updated."),'success');
    //json_out($in);
    return true;
    }


    function recalculate_total(&$in){
    global $config;

    $in['totalul'] = array();
    $price_line =0;
    $total_currency =0;
    $total_with_vat = 0;

    $order = $this->db->query("SELECT add_cost, currency_rate, currency_type FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");

    $add_cost = $order->f('add_cost');
    $rate = return_value($order->f('currency_rate'));
    $currency_type = $order->f('currency_type');
    $currency = get_commission_type_list($currency_type);

    $amount_data = $this->db->query("SELECT pim_p_order_articles.* FROM pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."' ");

    while ($amount_data->move_next()){
        $sale_unit= $amount_data->f('sale_unit');
        if(!ALLOW_ARTICLE_SALE_UNIT){
            $sale_unit=1;
          }
        $packing= $amount_data->f('packing');
        if(!ALLOW_ARTICLE_PACKING){
            $packing=1;
            $sale_unit=1;
        }
        $price_line = $amount_data->f('price')*$packing/$sale_unit;

        $total_with_vat += $amount_data->f('price_with_vat')*$amount_data->f('quantity')*$packing/$sale_unit;
        
      }

    $total_with_vat=$total_with_vat+$add_cost;

    $total_currency = $total_with_vat * $rate;

    $in['totalul'] = array(
        'total_vat_val'     => $total_with_vat,
        'total_vat'         => place_currency(display_number($total_with_vat),get_commission_type_list($currency_type)),
        'hide_currency2'    => ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
        'hide_currency1'    => ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
        'currency_type'     => $currency,#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
        'currency_d_type'   => get_commission_type_list(ACCOUNT_CURRENCY_TYPE),#ACCOUNT_CURRENCY_TYPE == 1 ? 
        'total_default_c'   => display_number($total_currency),
        'default_currency_name' => build_currency_name_list(ACCOUNT_CURRENCY_TYPE)
        
      );

    //console::log($in['totalul']);

    $this->db->query("UPDATE pim_p_orders SET amount='".$total_with_vat."' WHERE p_order_id='".$in['p_order_id']."'");

      return true;

    }


     function orderFacq(&$in){
      global $config;
      $app=$this->db->query("SELECT * FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");
    
      if($app->next()){
         if($app->f('active') == 1){


          $facq_id = $app->f('app_id');
          $UserID=$this->db->field("SELECT api FROM apps WHERE type='UserID' AND main_app_id='".$facq_id."' ");
          $Login=$this->db->field("SELECT api FROM apps WHERE type='Login' AND main_app_id='".$facq_id."' ");
          $Password=$this->db->field("SELECT api FROM apps WHERE type='Password' AND main_app_id='".$facq_id."' ");

          $facq_lines=$this->db->query("SELECT * FROM  pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."'   ORDER BY sort_order ASC ");

          $p_order=$this->db->query("SELECT * FROM  pim_p_orders WHERE p_order_id='".$in['p_order_id']."' ");
          $def_email = $this->default_email();

          $opts = array(
              'http' => array(
                  'user_agent' => 'PHPSoapClient'
              ),
              'ssl' => array(
              'verify_peer' => false,
              'verify_peer_name' => false,
              'allow_self_signed' => true
              )
          );
          $context = stream_context_create($opts);
          
            
          $requestParams =array(
                'UserID'                  => $UserID,
                'Login'                   => $Login,
                'Password'                => $Password,
                'CultureID'               => $p_order->f('email_language')=='2'?'FR':'NL',   
                'OrderRequestHeader'      => array( 'DeliveryName1'=>'Delivery',
                                                    'DeliveryAddress1'=>ACCOUNT_DELIVERY_ADDRESS,
                                                    'DeliveryZipcode'=>ACCOUNT_DELIVERY_ZIP,
                                                    'DeliveryLocality'=>ACCOUNT_DELIVERY_CITY,
                                                    'DeliveryCountry'=>get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
                                                    'DeliveryDate'=>date("c",$p_order->f('del_date')),
                                                    'Tel'=>$p_order->f('customer_phone'),
                                                    'Email'=>$def_email['from']['email'],
                                                    
                                                ),
                'ListOrderRequestItem' => array('OrderRequestItem'=>array())
              );

              while($facq_lines->next()){
                $item = array('ProductID'=>$facq_lines->f('content')?'000000':$facq_lines->f('supplier_reference'),
                              'Quantity'=>intval($facq_lines->f('quantity')),
                              'Label'=>$facq_lines->f('article'),
                              'OrderLine'=>intval($facq_lines->f('sort_order')),
                            );
                  array_push($requestParams['ListOrderRequestItem']['OrderRequestItem'], $item);
                 }

               try
                {
                  //var_dump($requestParams['ListOrderRequestItem']);exit();
                  $soap = new SoapClient($config['facq_wsdl'],array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY, 'stream_context' => $context));
                  $res = $soap->Order(array("request" => $requestParams));
      
                  $status = $res->OrderResult->Status;
                  $code = $status->Code;
                  $message = $status->Message;
                  $description = $status->Description;

                
               
                    if($code=='0' || $code=='9'){ 
                      $facq_order_id = $res->OrderResult->OrderResponseHeader->OrderID;
                    
                     
                      //if($facq_order_id){
            
                        
                        $this->db->query("UPDATE pim_p_orders SET facq_order_id = '".$facq_order_id."', rdy_invoice='2', sent='1', send_date='".time()."' WHERE p_order_id='".$in['p_order_id']."' ");
                    
                        insert_message_log('p_order','Facq OrderID - '.$facq_order_id.' - '.'{l}This order was sent to Facq by{endl} '.get_user_name($_SESSION['u_id']),'p_order_id',$in['p_order_id']);
                
                        // }
                     
                    }else{
                      if(!$message){
                        $message =  $description;
                      }
                      insert_message_log('p_order',$message,'p_order_id',$in['p_order_id']);
                      msg::error($message,"error");
                    return false;
                    }
                 }
                catch(SoapFault $fault)
                {

                   //var_dump($fault); exit();
                    insert_message_log('p_order','{l}Error{endl} - '.$fault->faultstring,'p_order_id',$in['p_order_id']);
                    msg::error ( gm('Error').'-'.$fault->faultstring, 'error');         
                    return false;
              
                }

            
      }
    }
    msg::success ( gm("This order was sent to Facq."),'success');
    //json_out($in);
    return true;
    }


     function validate_facq_articles(&$in){
      global $config;

      $exist_out =0;
      $exist_out=$this->db->field("SELECT COUNT( order_articles_id ) FROM pim_p_order_articles
                                    WHERE p_order_id = '".$in['p_order_id']."' AND article_id = '0' AND tax_id = '0' AND content = '0'");

      if($exist_out){
                //insert_message_log('p_order','{l}Error{endl} - '.'{l}There are non valid articles in the PO. Please, check you order.{endl} ','p_order_id',$in['p_order_id']);
                msg::error ( gm('Error').'-'.gm('There are non valid articles in the PO. Please, check you order.'), 'error');         
                json_out($in);
      }else{
        $all_valid = true;

        $facq_lines=$this->db->query("SELECT * FROM  pim_p_order_articles WHERE p_order_id='".$in['p_order_id']."'   ORDER BY sort_order ASC ");

        while($facq_lines->next()){
          if($facq_lines->f('article_id')){
             $facq_info= $this->db->query("SELECT pim_articles.supplier_reference, customers.customer_id, customers.facq FROM  pim_articles 
                                          LEFT JOIN customers ON customers.customer_id = pim_articles.supplier_id
                                           WHERE article_id='".$facq_lines->f('article_id')."'  ");
              if(!$facq_info->f('facq')  || !check_facq_reference($facq_lines->f('supplier_reference'))){
                $all_valid = false;
                break;
              }
          }
         
        }

        if(!$all_valid){
            msg::error ( gm('Error').'-'.gm('There are non valid articles in the PO. Please, check you order.'), 'error');         
            json_out($in);
        }else{
           return true;
        }

       

      }


    }

    function updateDeliveryDate(&$in){
      if(!$in['del_date']){
        json_out($in);
      }
      $this->db->query("UPDATE pim_p_orders SET del_date='".$in['del_date']."' WHERE p_order_id='".$in['p_order_id']."' ");
      $in['factur_del_date']=date(ACCOUNT_DATE_FORMAT,  $in['del_date']);
      msg::success ( gm("Changes have been saved."),'success');
      json_out($in);
    }


      function set_custom_pdf(&$in)
  {
    $this->dbu->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_PO_PDF' ");
    $this->dbu->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_PO_PDF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_P_ORDER_PDF_FORMAT'");
    msg::success ( gm('Changes saved'),'success');

    return true;
  }

  function updateOurReference(&$in){
    $this->db->query("UPDATE pim_p_orders SET our_ref='".addslashes($in['our_ref'])."' WHERE p_order_id='".$in['p_order_id']."' ");
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function updateYourReference(&$in){
    $this->db->query("UPDATE pim_p_orders SET your_ref='".addslashes($in['your_ref'])."' WHERE p_order_id='".$in['p_order_id']."' ");
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function getArticlePrice($in){
      $purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND base_price='1' ");

      if($in['customer_id']){
        $supplier_article = $this->db->query("SELECT purchasing_price, supplier_ref FROM pim_article_references WHERE article_id='".$in['article_id']."' AND supplier_id='".$in['customer_id']."' ");
        if($supplier_article->move_next()){
          if($supplier_article->f('purchasing_price')>0){
            $purchase_price = $supplier_article->f('purchasing_price');
          }     
        }
      }

      if($in['has_variants']){
        $purchase_price=0;
    }
      return $purchase_price;
  }

    function saveterm(&$in){
        $edit_confirmed_po_orders=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='EDIT_CONFIRMED_PO_ORDERS' ");
        if(is_null($edit_confirmed_po_orders)){
            $this->db->query("INSERT INTO settings SET value='".$in['edit_confirmed_po_orders']."', constant_name='EDIT_CONFIRMED_PO_ORDERS', module='po_orders' ");
        }else{
            $this->db->query("UPDATE settings SET value='".$in['edit_confirmed_po_orders']."' WHERE constant_name='EDIT_CONFIRMED_PO_ORDERS' ");
        }

        msg::success ( gm("Changes have been saved."),'success');
        return true;
    }

    function update_po_order_line($in)
    {
        $this->db->query("UPDATE pim_p_order_articles SET 
      discount = '".return_value($in['disc'])."',
      price = '".return_value($in['price'])."',
      price_with_vat = '".return_value($in['price_vat'])."'
      WHERE order_articles_id='".$in['order_articles_id']."' ");

        $use_package=$this->db->field("SELECT use_package FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");
        $use_sale_unit=$this->db->field("SELECT use_sale_unit FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");

        $lines=array();
        $p_order_lines=$this->db->query("SELECT * FROM pim_p_order_articles 
        WHERE p_order_id='".$in['p_order_id']."' ORDER BY sort_order ASC");

        while ($p_order_lines->next())
        {
            $nr_dec_q = strlen(rtrim(substr(strrchr($p_order_lines->f('quantity'), "."), 1),'0'));

            $line = array(
                'quantity'            => display_number($p_order_lines->f('quantity'), $nr_dec_q),
                'price'               => display_number_var_dec($p_order_lines->f('price')),
                'sale_unit'           => $p_order_lines->f('sale_unit'),
                'packing'           => remove_zero_decimals($p_order_lines->f('packing')),
                'disc'            => display_number($p_order_lines->f('discount')),
            );
            array_push($lines, $line);
        }

        $global_disc = $in['discount'];
        if($in['apply_discount'] < 2){
            $global_disc = 0;
        }
        $p_order_total=0;
        foreach ($lines as $nr => $row)
        {
            if(!$use_package){
                $row['packing']=1;
            }
            if(!$use_sale_unit){
                $row['sale_unit']=1;
            }
            $discount_line = return_value($row['disc']);
            if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
                $discount_line = 0;
            }
            $q = return_value2($row['packing']) && $row['sale_unit'] ? @(return_value($row['quantity']) * return_value2($row['packing']) / $row['sale_unit']) : 0;
            $price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);
            $line_total=$price_line * $q;
            $p_order_total+= $line_total - ($line_total*$global_disc/100);
        }

        $this->db->query("UPDATE pim_p_orders SET amount='".$p_order_total."' WHERE p_order_id='".$in['p_order_id']."'");

        msg::success ( gm("Changes have been saved."),'success');
        return true;
    }

}

?>