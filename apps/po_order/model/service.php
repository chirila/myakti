<?php
/**
 * Service class
 *
 * @author      Arkweb SRL
 * @link        https://app.akti.com/
 * @copyright   [description]
 * @package 	Service
 */
class service
{
	var $pag = 'service';
	var $field_n = 'article_id';

	/**
	 * [article description]
	 * @return [type] [description]
	 */
	function service()
	{
		$this->db = new sqldb;
	}

	/**
	 * Add service
	 * @param array $in
	 * @return boolean
	 */
	function add(&$in)
	{
	    if(!$this->add_validate($in)){
			return false;
		}
		$in['article_id'] = $this->db->insert("INSERT INTO pim_articles SET															
												internal_name       		= '".$in['internal_name']."',
												item_code  					= '".$in['item_code']."',
												article_category_id       	= '".$in['article_category_id']."',
												d_price			        	= '".return_value($in['d_price'])."',
												h_price			        	= '".return_value($in['h_price'])."',
												billable			        = '".$in['billable']."',
												ledger_account_id			= '".$in['ledger_account_id']."',
												is_service			        = '1',
															created_at='".time()."' ");
		
       update_articles($in['article_id'],DATABASE_NAME);
		msg::success(gm("Service succefully added."),'success');
       return true;
	}

	/**
	 * Validate add article
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in)
	{
		$v = new validation($in);
		
		$v -> f('item_code', 'Item Code', 'required:unique[pim_articles.item_code]');
		$v -> f('internal_name', 'Internal name', 'required');	
		return $v -> run();
	}

	

}	
