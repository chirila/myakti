<?php
 
class order {

 	var $pag = 'order';

	function __construct() {
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db = new sqldb();
    $this->dbu = new sqldb();
		$this->db2 = new sqldb();
		$this->db_users = new sqldb($db_config);
		$this->db_users2 = new sqldb($db_config);
		$this->field_n = 'order_id';
	}

	 function saveAddToPdf(&$in)
  {
  	if($in['all']){
  		if($in['value'] == 1){
  			foreach ($in['item'] as $key => $value) {
			    $_SESSION['add_to_pdf_o'][$value]= $in['value'];
	  		}
  		}else{
  			foreach ($in['item'] as $key => $value) {
			    // $_SESSION['add_to_pdf_o'][$value]= $in['value'];
			    unset($_SESSION['add_to_pdf_o'][$value]);
	  		}
		  }
  	}else{
	    if($in['value'] == 1){
	    	$_SESSION['add_to_pdf_o'][$in['item']]= $in['value'];
	    }else{
	    	unset($_SESSION['add_to_pdf_o'][$in['item']]);
	    }
	  }
    return true;
  }

  function sendNew(&$in){

      if(!$this->sendNewEmailValidate($in)){
        msg::error ( gm('Invalid email address').' - '.$in['new_email'],'error');
        json_out($in);
        return false;
      }
      global $config;
      $this->db->query("SELECT pim_orders.pdf_logo, pim_orders.pdf_layout,pim_orders.acc_manager_id
                        FROM pim_orders
                        WHERE pim_orders.order_id ='".$in['order_id']."'");
      $this->db->move_next();
      if($this->db->f('pdf_layout')){
        $in['type']=$this->db->f('pdf_layout');
        $in['logo']=$this->db->f('pdf_logo');
      }else {
        $in['type'] = ACCOUNT_ORDER_PDF_FORMAT;
        }
        $acc_manager_id = $this->db->f('acc_manager_id');
      $this->generate_pdf($in);
      $mail = new PHPMailer();
      $mail->WordWrap = 50;
      $def_email = $this->default_email();
      $body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');
      $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
      $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
      $subject=$in['e_subject'];
      $mail->Subject = $subject;
      if($in['include_pdf']){
         $mail->AddAttachment("order_.pdf", $in['serial_number'].'.pdf'); // attach files/invoice-user-1234.pdf, and rename it to invoice.pdf
      }
      if($in['files']){
        foreach ($in['files'] as $key => $value) {
        	if($value['checked'] == 1){
	          $mime = mime_content_type($value['path'].$value['file']);
	          $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
	        }
        }
      }
      if($in['file_d_id']){
        $body.="\n\n";
        foreach ($in['file_d_id'] as $key => $value) {
          $body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
        }
      }
        if($in['copy']){
            //$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
            $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
            if($u->f('email')){
              $emails .=$u->f('email').', ';
              $mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
            }
            // $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
        }
        if($in['copy_acc']){
            $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$acc_manager_id."' ");
            if($u->f('email')){
              $emails .=$u->f('email').', ';
              $mail->AddBCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
            }
            // $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
        }
      if($in['use_html']){
      	    $order = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id='".$in['order_id']."'");
          	$contact =  $this->dbu->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$order->f('contact_id')."'");
          	$title_cont =  $this->dbu->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
            $customer =  $this->dbu->field("SELECT name FROM customers WHERE customer_id='".$order->f('buyer_id')."'");

      	$head = '<style>p { margin: 0px; padding: 0px; }</style>';
        $mail->IsHTML(true);
        $body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 35px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 30px;">'.$in['e_message'].'</div>';
        $body = stripslashes($body_style);
		$body=str_replace('[!CONTACT_FIRST_NAME!]',"".$contact->f('firstname')."",$body);
		$body=str_replace('[!CONTACT_LAST_NAME!]',"".$contact->f('lastname')."",$body);
		$body=str_replace('[!SALUTATION!]',"".$title_cont."",$body);
		$body=str_replace('[!SERIAL_NUMBER!]',"".$order->f('serial_number')."",$body);
		$body=str_replace('[!DATE!]',"".date('m/d/Y', $order->f('date'))."",$body);
		$body=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$body);
		$body=str_replace('[!DUE_DATE!]',"".date('m/d/Y', $order->f('del_date'))."",$body);
		$body=str_replace('[!YOUR_REFERENCE!]',"".$order->f('your_ref')."",$body);
        if($in['copy']){
	    	$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url'].ACCOUNT_LOGO_ORDER."\" alt=\"\">",$body);
		}elseif($in['copy_acc']){
			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
		}else{
		     $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url'].ACCOUNT_LOGO_ORDER."\" alt=\"\">",$body);
		}
        if($in['file_d_id']){
          $body .="<br><br>";
          foreach ($in['file_d_id'] as $key => $value) {
            $body.="<p><a href=".$in['file_d_path'][$key].">".$in['file_d_name'][$key]."</a></p>";
          }
        }
        $mail->Body = $head.$body;
      }else{
        $mail->MsgHTML(nl2br(($body)));
      }

        $this->db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number,pim_orders.pdf_logo, pim_orders.pdf_layout
                          FROM pim_orders
                          INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_orders.customer_id
                          WHERE pim_orders.order_id ='".$in['order_id']."'");
        $this->db->move_next();
      if($in['new_email_id']==$this->db->f('email')){
        $mail->AddAddress(trim($this->db->f('email')),$this->db->f('firstname').' '.$this->db->f('lastname'));
      }else{
        $mail->AddAddress(trim($in['new_email']));
      }
      $sent_date= time();
      $this->db->query("UPDATE pim_orders SET sent='1',sent_date='".$sent_date."' WHERE order_id='".$in['order_id']."'");
      $mail->Send();
      if($mail->IsError()){
        msg::error ( $mail->ErrorInfo,'error');
        json_out($in);
        return false;
      }
      if($mail->ErrorInfo){
        msg::notice( $mail->ErrorInfo,'notice');
      }

      /*if($mail->ErrorInfo){
        msg::error( $mail->ErrorInfo,'error');
        json_out($in);
        return false;
      }*/
      msg::success( gm('Order has been successfully sent').' - '.$in['new_email'],'success');

      json_out($in);
      return true;
    }

    function sendNewEmailValidate(&$in)
    {
      $v = new validation($in);
      $in['new_email'] = trim($in['new_email']);
      $v->field('new_email', 'Email', 'email');
      return $v->run();
    }

    /**
	 * Send order by email
	 *
	 * @return bool
	 * @author Mp
	 **/
	function generate_pdf(&$in)
	{
		include_once(__DIR__.'/../controller/order_print.php');
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		$this->db->query("SELECT * FROM default_data WHERE type='order_email' ");
		//if($this->db->move_next()){
		if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
		}
		return $array;
	}

	/**
	 * Mark as sent
	 *
	 * @return bool
	 * @author Mp
	 **/
	function mark_sent(&$in){
		if(!$this->mark_sent_validate($in))
		{
			return false;
		}
    $in['s_date']=strtotime('now');

		$this->db->query("UPDATE pim_orders SET sent='1',sent_date='".$in['s_date']."' WHERE order_id='".$in['order_id']."'");

		msg::success( gm('Order has been successfully marked as ready to deliver'),'success');
		insert_message_log($this->pag, '{l}Order has been successfully marked as ready to deliver by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		return true;
	}

	/****************************************************************
	* function mark_sent_validate(&$in)                                *
	****************************************************************/
	function mark_sent_validate(&$in)
	{
		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', gm('Invalid ID'));
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author me
	 **/
	function mark_as_draft(&$in)
	{
		if(!$this->delete_order_validate($in)){
			return false;
		}
		$this->db->query("UPDATE pim_orders SET sent='0',sent_date='' WHERE order_id='".$in['order_id']."' ");
	}

	/****************************************************************
	* function delete_po_order_validate(&$in)                                          *
	****************************************************************/
	function delete_order_validate(&$in)
	{
		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
		return $v->run();
	}

	/**
	 * make delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function make_delivery(&$in)
	{

		if(!$this->make_delivery_validate($in)){
			return false;
		}
// printvar($in);exit();
		$const =array();
    $articles_ids=array();
		$const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
		$const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
		$const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
		$const['ORDER_DELIVERY_STEPS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_STEPS' ");
		$commands = array();
		if($const['CAN_SYNC']){
		    Sync::start(ark::$model.'-'.ark::$method);
	  }
		$now = time();
		$delivery_done = 0;
		if($const['ORDER_DELIVERY_STEPS']==2 && $in['delivery_approved']){
			$delivery_done = 1;
		}
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$del = $this->db->insert("INSERT INTO pim_order_deliveries
			                      SET order_id='".$in['order_id']."',
			                         `date` = '".$in['date_del_line_h']."',
                                     `hour` = '".$in['hour']."',
                                     `minute` = '".$in['minute']."',
                                     `pm` = '".$in['pm']."',
                                      contact_name 	= '".$in['contact_name']."',
		                              contact_id	= '".$in['contact_id']."',
		                              delivery_address	= '".$in['delivery_address']."',
		                              delivery_done 	= '".$delivery_done."'

			                         ");
		if($in['approve_from_list']){
			$in['new_delivery_id'] = $del;
		}
		if($in['mark_as_delivered']==1){
			$this->db->query("UPDATE pim_orders SET rdy_invoice='1' WHERE order_id='".$in['order_id']."' ");
		}else{
			$this->db->query("UPDATE pim_orders SET rdy_invoice='2' WHERE order_id='".$in['order_id']."' ");
		}
		$in['quantity_dispatch'] = array();
		foreach ($in['lines'] as $key => $value) {
			if(return_value($value['quantity']) != '0.00'){
				$article_id =  $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id='".$value['order_articles_id']."'");

        array_push($articles_ids,$article_id);

        $hide_stock =  $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$article_id."'");

        $in['quantity_dispatch'][$value['order_articles_id']]=array();
				foreach ($value['disp_addres_info'] as $k => $v) {
					$in['quantity_dispatch'][$value['order_articles_id']][$v['cust_addr_id']]=$v['quantity_delivered'];
				}

				if($const['ALLOW_STOCK'] && $hide_stock==0 ){

					$use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$article_id."' ");
					$use_batch_no = $this->db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$article_id."' ");

					if($const['ORDER_DELIVERY_STEPS'] == 2){
						$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");
						if($in['delivery_approved']){
							$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
							$article_data->next();
							$stock = $article_data->f("stock");

							$stock_packing = $article_data->f("packing");
							if(!$const['ALLOW_ARTICLE_PACKING']){
								$stock_packing = 1;
							}
							$stock_sale_unit = $article_data->f("sale_unit");
							if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
								$stock_sale_unit = 1;
							}

							$new_stock = $stock - (return_value($value['quantity'])*$stock_packing/$stock_sale_unit);

							$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");


							/*start for  stock movements*/
							$backorder_articles = (return_value($value['quantity2']) - return_value($value['quantity'])) * $stock_packing/$stock_sale_unit;
							$this->db->insert("INSERT INTO stock_movements SET
								date 						=	'".$now."',
								created_by 					=	'".$_SESSION['u_id']."',
								order_id 					= 	'".$in['order_id']."',
								serial_number 				= 	'".$serial_number."',
								article_id 					=	'".$article_id."',
								article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
								item_code  					= 	'".addslashes($article_data->f("item_code"))."',
								movement_type				=	'3',
								quantity 					= 	'".return_value($value['quantity'])*$stock_packing/$stock_sale_unit."',
								stock 						=	'".$stock."',
								new_stock 					= 	'".$new_stock."',
								backorder_articles 			= 	'".$backorder_articles."',
								location_info               =   '".serialize($in['quantity_dispatch'][$value['order_articles_id']])."',
								delivery_id                 =    '".$del."'

					        	");
					        	@array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");

              /*end for  stock movements*/
						}
					}else{
						$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
						$article_data->next();
						$stock = $article_data->f("stock");

						$stock_packing = $article_data->f("packing");
						if(!$const['ALLOW_ARTICLE_PACKING']){
							$stock_packing = 1;
						}
						$stock_sale_unit = $article_data->f("sale_unit");
						if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
							$stock_sale_unit = 1;
						}

						$new_stock = $stock - (return_value($value['quantity'])*$stock_packing/$stock_sale_unit);

						$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");

						/*start for  stock movements*/
						$backorder_articles = (return_value($value['quantity2']) - return_value($value['quantity'])) * $stock_packing/$stock_sale_unit;
						$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

						$this->db->insert("INSERT INTO stock_movements SET
							date 						=	'".$now."',
							created_by 					=	'".$_SESSION['u_id']."',
							order_id 					= 	'".$in['order_id']."',
							serial_number 				= 	'".$serial_number."',
							article_id 					=	'".$article_id."',
							article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
							item_code  					= 	'".addslashes($article_data->f("item_code"))."',
							movement_type				=	'3',
							quantity 					= 	'".return_value($value['quantity'])*$stock_packing/$stock_sale_unit."',
							stock 						=	'".$stock."',
							new_stock 					= 	'".$new_stock."',
							backorder_articles 			= 	'".$backorder_articles."',
							location_info               =   '".serialize($in['quantity_dispatch'][$value['order_articles_id']])."',
							delivery_id                 =    '".$del."'
				        	");
				        	@array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
						/*end for  stock movements*/

					}

					# article serial numbers
					if($use_serial_no == 1){
						$status_details_2 = $serial_number;
						// $selected_s_n = explode(',', $in['selected_s_n'][$key]);

						foreach ($value['selected_s_n'] as $key2 => $v) {
							$this->db->query("UPDATE serial_numbers
										SET 			status_id 	= '2',
												status_details_2 	= '".$status_details_2."',
												date_out 		= '".$now."',
												delivery_id 	= '".$del."'
										WHERE id = '".$v['serial_nr_id']."' ");
						}

					}

					# batch numbers
					if( $use_batch_no == 1 ){
						$status_details_2 = $serial_number;

						foreach ($value['selected_b_n'] as $key3 => $v) {
							$current_in_stock = $this->db->field("SELECT in_stock FROM batches WHERE id = '".$v['b_n_id']."' AND article_id = '".$article_id."' ");
							$new_in_stock = $current_in_stock-$v['b_n_q_selected'];
							$status_id = 1;
							if($new_in_stock==0){
								$status_id = 2;
							}
							$this->db->query("	UPDATE batches
										SET 	status_id 		= '".$status_id."',
											status_details_2 	= '".$status_details_2."',
											in_stock 		= '".$new_in_stock."',
											date_out 		= '".$now."',
											delivery_id 	= '".$del."'
										WHERE id = '".$v['b_n_id']."' AND article_id = '".$article_id."' ");

							$this->db->query("INSERT INTO batches_from_orders SET
								order_articles_id = 		'".$value['order_articles_id']."',
								article_id 		= 		'".$article_id."',
								batch_id 		= 		'".$v['b_n_id']."',
								delivery_id 	= 		'".$del."',
								order_id 		= 		'".$in['order_id']."',
								quantity 		= 		'".$v['b_n_q_selected']."'  ");
						}

					}
				}

				# check to see if the value is bigger than what we have
				$q = return_value($value['quantity']);
				$line = $this->db->field("SELECT quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' AND content='0' ");
				$delivered = $this->db->field("SELECT sum(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' ");
				if( $q+$delivered > $line){
					//$q = $line - $delivered;
				}
				# update deliveries
				$this->db->query("INSERT INTO pim_orders_delivery SET order_id='".$in['order_id']."' , order_articles_id='".$value['order_articles_id']."', quantity='".$q."', delivery_id='".$del."', delivery_note='".$in['delivery_note']."' ");


        $delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' ");
				if($in['delivery_approved']){
					$this->db->query("UPDATE pim_order_articles SET delivered='0' WHERE order_articles_id='".$value['order_articles_id']."' ");
				}
				# delivery complete
				if($delivered_all > $line || $delivered_all == $line ){

						$this->db->query("UPDATE pim_order_articles SET delivered='1' WHERE order_articles_id='".$value['order_articles_id']."' ");

				}
			}
		}
		if($in['quantity_dispatch'] ){
      foreach ($in['quantity_dispatch'] as $order_articles_id => $stock_disp_delivery_info) {
        foreach ($stock_disp_delivery_info as $location_info => $dispatch_quantity) {
          if(return_value($dispatch_quantity) != '0.00') {
            $article_id =  $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id='".$order_articles_id."'");

            $art_data = $this->db->query("SELECT packing, sale_unit FROM pim_articles WHERE article_id = '".$article_id."' ");
            $art_data->move_next();

            $stock_packing = $art_data->f('packing');
            if(!$const['ALLOW_ARTICLE_PACKING']){
							$stock_packing = 1;
						}
						$stock_sale_unit = $art_data->f('sale_unit');
            if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
							$stock_sale_unit = 1;
						}
         	}

					$dispatch_quantity=return_value($dispatch_quantity);
          //new way of working we have to decrese stock at that address
          //select from adress stock info
	        if($const['ORDER_DELIVERY_STEPS'] == 2){
          	if($in['delivery_approved']){
            	$location=explode("-", $location_info);
              $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");

              $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -($dispatch_quantity*($stock_packing/$stock_sale_unit ))) ."' WHERE article_id='".$article_id."'   AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");
            }
          }else{
            $location=explode("-", $location_info);
            $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");
            $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -($dispatch_quantity*($stock_packing/$stock_sale_unit )))."' WHERE article_id='".$article_id."'   AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");
          }
				}
     	}
		}

		if(WAC){
		  foreach ($articles_ids as $key => $value) {
		    generate_purchase_price($value);
		  }
		}
    if($const['CAN_SYNC']){
      $packet = array(
			  'command' => base64_encode(serialize($commands)),
			  'command_id' => 1
		  );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['order_id']);
    }

		$articles = $this->db->field("SELECT count(order_articles_id) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' and article_id!=0");
		//$articles = $this->db->field("SELECT count(order_articles_id) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' ");
		$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' and article_id!=0");
		//$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' ");

		if($articles == $articles_delivered){
			$this->db->query("UPDATE pim_orders SET rdy_invoice='1' WHERE order_id='".$in['order_id']."' ");
		}

		insert_message_log($this->pag,'{l}Delivery made by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);

		return true;
	}

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author me
	 **/
	function make_delivery_validate(&$in)
	{
		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");

		$sum_of_delivery_items = 0;
		foreach ($in['lines'] as $key => $value) {
			// if(return_value($value)<0) {
				//return false;
			// }
			$sum_of_delivery_items += (return_value($value['quantity']));
		}
		if($sum_of_delivery_items==0){
			return false;
		}else{

			// validation for articles that use serial numbers
			$err = false;
			if($in['order_articles_id']){
				foreach($in['order_articles_id'] as $key => $order_articles_id){
					$val = 0;
					$count_selected_s_n = 0;
					$name = '';
					foreach ($in['lines'] as $k => $value) {
						if($value['order_articles_id'] == $order_articles_id){
							$val = intval(return_value($value['quantity']));
							$count_selected_s_n = intval($value['count_selected_s_n']);
							$name = $value['article'];
							break;
						}
					}
					if( $val > 0 ){
						$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$order_articles_id."' ");
						$use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$article_id."' ");
				 		if( $use_serial_no == 1 && ALLOW_STOCK ){
				 			if($val != $count_selected_s_n){
				 				$error_match .= $name."\n";
				 				$err = true;
				 			}
						}
					}
				}
				if($err != ''){
				 	msg::error (gm("Serial Numbers").' '.gm("quantities don't match for").": \n".rtrim($error_match,"\n"),'error');
				 	return false;
				}
			}

			//validation for articles that use batches
			$err = false;
			if($in['order_articles_id']){
				foreach($in['order_articles_id'] as $key => $order_articles_id){
					$val = 0;
					$count_selected_s_n = 0;
					$name = '';
					foreach ($in['lines'] as $k => $value) {
						if($value['order_articles_id'] == $order_articles_id){
							$val = intval(return_value($value['quantity']));
							$count_selected_s_n = intval($value['count_selected_s_n']);
							$name = $value['article'];
							break;
						}
					}
					if( $val > 0 ){
						$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$order_articles_id."' ");
						$use_batch_no = $this->db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$article_id."' ");
						if($use_batch_no == 1 && ALLOW_STOCK){
							if(!$value['count_selected_b_n']){
								$error_match2 = $name."\n";
				 				$err2 = true;
							}else{#I have no idea
								$count_batch_q = $value['count_selected_b_n'];
								// $in['penis'] = $count_batch_q.' #### '.intval(return_value($value['quantity']));
								// var_dump($count_batch_q.' #### '.intval(return_value($value['quantity'])));
								if($count_batch_q !=intval(return_value($value['quantity']))) {
									$error_match2 = $name."\n";
				 					$err2 = true;
								}
							}
						}
					}
				}
				if($err2 != ''){
				 	msg::error ( gm("Batch Numbers").' '.gm("quantities don't match for").": \n".rtrim($error_match2,"\n"),'error');
				 	return false;
				}
			}

			return $v->run();
		}
	}

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function edit_delivery(&$in)
	{
		// printvar($in);exit();
		if(!$this->edit_delivery_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$now = time();
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$this->db->query("UPDATE  pim_order_deliveries
		                  SET 	`date` = '".$in['date_del_line_h']."',
                                    `hour` = '".$in['hour']."',
                                    `minute` = '".$in['minute']."',
                                    `pm` = '".$in['pm']."',
                                    contact_name 	= '".$in['contact_name']."',
		                        contact_id	= '".$in['contact_id']."',
		                        delivery_address	= '".$in['delivery_address']."'
                              WHERE order_id='".$in['order_id']."' and delivery_id='".$in['delivery_id']."' ");
		$this->db->query("UPDATE 	pim_orders_delivery
		                 	SET   	`delivery_note` = '".$in['delivery_note']."'
                              WHERE     	order_id='".$in['order_id']."' and delivery_id='".$in['delivery_id']."' ");
        	Sync::end($in['order_id']);
		insert_message_log($this->pag,'{l}Delivery edited by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		if(ORDER_DELIVERY_STEPS == 2 && $in['delivery_approved']){
			$this->approve_delivery($in);
		}
		return true;
	}

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function edit_delivery_validate(&$in)
	{

		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
		$v->field('delivery_id', 'ID', 'required:exist[pim_order_deliveries.delivery_id]', "Invalid Id");
		// validation for articles that uses serial numbers
		$err = false;
		foreach($in['order_articles_id'] as $key => $order_articles_id){
			$val = 0;
			$count_selected_s_n = 0;
			$name = '';
			foreach ($in['lines'] as $k => $value) {
				if($value['order_articles_id'] == $order_articles_id){
					$val = intval(return_value($value['quantity']));
					$count_selected_s_n = intval($value['count_selected_s_n']);
					$name = $value['article'];
					break;
				}
			}
			if( $val > 0 ){
		 		$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$order_articles_id."' ");
				$use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$article_id."' ");
		 		if( $use_serial_no == 1 && ALLOW_STOCK){
		 			if($val != $count_selected_s_n ){
		 				$error_match .= $name.'<br />';
		 				$err = true;
		 			}
				}
			}
		}
		if($err != ''){
		 	msg::$error = gm("Quantities don't match for").': <br />'.rtrim($error_match,'<br />');
		 	return false;
		}


	return $v->run();

	}

	/**
	 * approve delivery
	 *
	 * @return bool
	 * @author PM
	 **/

	function approve_delivery(&$in){
		if(!$this->make_delivery_validate($in) || !$in['delivery_id']){
			return false;
		}
		if(!$this->delete_delivery($in)){
			return false;
		}
		if(!$this->make_delivery($in)){
			return false;
		}

		if($in['approve_from_list']==1){
			$in['is_art_in_back'] = $this->db->field("	SELECT COUNT(order_id) FROM pim_orders
										WHERE order_id= '".$in['order_id']."'
										AND rdy_invoice!='1'
										AND sent=1
										AND pim_orders.active=1
										GROUP BY pim_orders.order_id");
		}
		msg::success ( gm('Delivery was approved'),'success');

		return true;
	}

	/**
	 * approve delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function delete_delivery(&$in){

		if(!$this->delete_delivery_validate($in)){
			return false;
		}
		$commands = array();
    if(CAN_SYNC){
		  Sync::start(ark::$model.'-'.ark::$method);
    }
    $articles_ids=array();
		$articles = $this->db->query("SELECT * FROM pim_orders_delivery WHERE order_id ='".$in['order_id']."' AND delivery_id='".$in['delivery_id']."' ");
		while($articles->next()){
			$article_id = $this->db2->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$articles->f('order_articles_id')."' ");
			$order_article_id = $articles->f('order_articles_id');

      array_push($articles_ids,$article_id);

      $this->db->query("DELETE FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' AND delivery_id='".$articles->f('delivery_id')."' ");
			if($in['mark_as_delivered']!=1){
				$this->db->query("UPDATE pim_order_articles SET delivered = '0' WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");
			}

			//delete order details from article serial numbers
			$this->db->field("UPDATE 	serial_numbers
						SET 		date_out 		= '0',
								delivery_id 	= '0',
								status_details_2 	= '',
								status_id 		= 1
						WHERE delivery_id = '".$in['delivery_id']."' ");


			//delete order details from article batches
			$batches_data = $this->db2->query("SELECT * FROM batches_from_orders
				WHERE 		order_id 		= 	'".$in['order_id']."'
				AND 			delivery_id 	= 	'".$in['delivery_id']."'
				AND 			order_articles_id = 	'".$order_article_id."' ");
			while($batches_data->next()){
				$batch_in_stock = $this->db2->field("SELECT in_stock FROM batches WHERE id = '".$batches_data->f('batch_id')."' ");
				$new_batch_stock = $batch_in_stock + $batches_data->f('quantity');
				$this->db2->query("UPDATE batches SET status_id = 1, in_stock = '".$new_batch_stock."', status_details_2 = '' WHERE id = '".$batches_data->f('batch_id')."' ");
			}
			$this->db2->query("DELETE FROM batches_from_orders
				WHERE 		order_id 		= 	'".$in['order_id']."'
				AND 			delivery_id 	= 	'".$in['delivery_id']."'
				AND 			order_articles_id = 	'".$order_article_id."' ");

		  $article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id ='".$order_article_id."'");
			$hide_stock =  $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$article_id."'");

      if(ALLOW_STOCK && $hide_stock==0){

				if(ORDER_DELIVERY_STEPS==2){

					if(!$in['delivery_approved']){

						$article_quantity = $articles->f('quantity');

						//$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id ='".$order_article_id."'");
						$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
						$article_data->next();
						$stock = $article_data->f("stock");

						$stock_packing = $article_data->f("packing");
						if(!ALLOW_ARTICLE_PACKING){
							$stock_packing = 1;
						}
						$stock_sale_unit = $article_data->f("sale_unit");
						if(!ALLOW_ARTICLE_SALE_UNIT){
							$stock_sale_unit = 1;
						}

						$new_stock = $stock + ($article_quantity*$stock_packing/$stock_sale_unit);

						$this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

						// $this->db->query("UPDATE pim_order_articles SET delivered = '0' WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");

						/*start for  stock movements*/
						$order_article_quantity = $this->db->field("SELECT quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND article_id='".$article_id."' ");
						$delivered = $this->db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");
						$backorder_articles = ($order_article_quantity - $delivered)*$stock_packing/$stock_sale_unit;
						$now = time();
						$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

        		$this->db->insert("INSERT INTO stock_movements SET
						date 						=	'".$now."',
						created_by 					=	'".$_SESSION['u_id']."',
						serial_number 				= 	'".$serial_number."',
						article_id 					=	'".$article_id."',
						article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
						item_code  					= 	'".addslashes($article_data->f("item_code"))."',
						movement_type				=	'4',
						quantity 					= 	'".$article_quantity*$stock_packing/$stock_sale_unit."',
						stock 						=	'".$stock."',
						new_stock 					= 	'".$new_stock."',
						backorder_articles			= 	'".$backorder_articles."'
        		");
		        /*end for  stock movements*/
		        array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
	      	}
				}else{
					$article_quantity = $articles->f('quantity');

					//$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id ='".$order_article_id."'");
					$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
					$article_data->next();
					$stock = $article_data->f("stock");

					$stock_packing = $article_data->f("packing");
					if(!ALLOW_ARTICLE_PACKING){
						$stock_packing = 1;
					}
					$stock_sale_unit = $article_data->f("sale_unit");
					if(!ALLOW_ARTICLE_SALE_UNIT){
						$stock_sale_unit = 1;
					}

					$new_stock = $stock + ($article_quantity*$stock_packing/$stock_sale_unit);

					$this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

					// $this->db->query("UPDATE pim_order_articles SET delivered = '0' WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");

					/*start for  stock movements*/
					$order_article_quantity = $this->db->field("SELECT quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND article_id='".$article_id."' ");
					$delivered = $this->db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");
					$backorder_articles = ($order_article_quantity - $delivered)*$stock_packing/$stock_sale_unit;
					$now = time();
					$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

      		$this->db->insert("INSERT INTO stock_movements SET
					date 						=	'".$now."',
					created_by 					=	'".$_SESSION['u_id']."',
					serial_number 				= 	'".$serial_number."',
					article_id 					=	'".$article_id."',
					article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
					item_code  					= 	'".addslashes($article_data->f("item_code"))."',
					movement_type				=	'4',
					quantity 					= 	'".$article_quantity*$stock_packing/$stock_sale_unit."',
					stock 						=	'".$stock."',
					new_stock 					= 	'".$new_stock."',
					backorder_articles			= 	'".$backorder_articles."'
      		");
	        /*end for  stock movements*/
	        array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
				}

				//new way of working we have to decrese stock at that address
                         //select from adress stock info
	      $this->db->query("SELECT location_info FROM stock_movements WHERE delivery_id='".$in['delivery_id']."'  and article_id='".$article_id."'");
				if($this->db->f('location_info')){
					$location=unserialize($this->db->f('location_info'));
					foreach ($location as $key => $value) {
				    if(return_value($value)!=0.00){
				      $info=explode("-", $key);
				      $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
				      $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock+return_value($value*$stock_packing/$stock_sale_unit ))."' WHERE article_id='".$article_id."'   AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
				    }
					}
				}
			}
		}
		$this->db->query("DELETE FROM pim_order_deliveries WHERE order_id='".$in['order_id']."' AND delivery_id='".$in['delivery_id']."' ");

		if(ORDER_DELIVERY_STEPS==2){
			if(!$in['delivery_approved']){
				$this->db->query("DELETE FROM dispatch_stock_movements WHERE  delivery_id='".$in['delivery_id']."'");
			}
		}else{
			$this->db->query("DELETE FROM dispatch_stock_movements WHERE  delivery_id='".$in['delivery_id']."'");

		}

		if(ark::$method=='delete_delivery' && ark::$model=='order'){
			$update = $this->db->query("UPDATE `pim_order_articles` SET  `delivered` = '0'
				WHERE order_id='".$in['order_id']."'
				AND `order_articles_id` NOT IN (SELECT `order_articles_id` FROM `pim_orders_delivery` WHERE `order_id` ='".$in['order_id']."' GROUP BY `order_articles_id` ) ");
		}


		if($in['del_nr']==1){
			$this->db->query("UPDATE pim_orders SET rdy_invoice='0' WHERE order_id='".$in['order_id']."' ");
		}else{
			$this->db->query("UPDATE pim_orders SET rdy_invoice='2' WHERE order_id='".$in['order_id']."' ");
		}

    if(CAN_SYNC){
		 	$packet = array(
			  'command' => base64_encode(serialize($commands)),
			  'command_id' => 1
		  );
			Sync::end();
		}

    if(WAC){
     	foreach ($articles_ids as $key => $value) {
        generate_purchase_price($value);
      }
 		}

		msg::success ( gm('Delivery was succesfully deleted'),'success');
		return true;
	}
	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	**/
	function delete_delivery_validate(&$in){

		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
		$v->field('delivery_id', 'ID', 'required:exist[pim_order_deliveries.delivery_id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function mark_as_delivered(&$in)
	{
		if(!$this->delete_order_validate($in)){
			return false;
		}

		$this->db->query("UPDATE pim_orders SET rdy_invoice='1' WHERE order_id='".$in['order_id']."' ");
		msg::success ( gm('Changes saved'),'success');
	}

	/**
	 * Send a reminder to the company about the delivery order
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function notice(&$in)
	{
		if(!$in['p_order_id'] && !$in['order_id']){
	    msg::error ( gm('Invalid ID'),'error');
			return false;
		}
		$in['type'] = ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;

		if($in['order_id']){
			include_once(__DIR__.'/../controller/order_print.php');
		}else{
			include_once('../apps/order/admin/controller/p_order_print.php');
		}

		// require_once ('../../classes/class.phpmailer.php');
        // include_once ("../../classes/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

    $mail = new PHPMailer();
		$mail->WordWrap = 50;
    if($in['order_id']){
			$def_email = $this->default_email();
    }else{
    	$def_email = $this->default_email_po();
    }

    $body = stripslashes($in['e_message']);
    $body = str_replace("\r\n", "\n", $body);
    // $body= strip_tags($body,'<table><tbody><tr><td><div><th><thead><strong>');
    $mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
    $mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);

    $subject=$in['subject'];

    $mail->Subject = $subject;

    $mail->MsgHTML(nl2br(($body)));

    /*if($in['use_html']){
    	$mail->IsHTML(true);
    	$head = '<style>p {	margin: 0px; padding: 0px; }</style>';
    	$body = stripslashes($in['e_message']);
      $mail->Body    = $head.$body;
    }*/

    if($in['copy']){
        $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
    }
    /*if($in['c_email']){
    	$mail->AddAddress($in['c_email'],$in['c_email']);
    }*/

    /*if($in['email_address']){
      foreach ($in['email_address'] as $contact_id => $email) {
        $mail->AddAddress(trim($email),$email);
      }
    }*/


		if($in['send_to']){
			$mail->AddAddress(trim($in['send_to']));
		}

		if($in['contact_id']){
			$contact = $this->db->query("SELECT email, lastname, firstname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($contact->next()){
				$mail->AddAddress($contact->f('email'),$contact->f('lastname').' '.$contact->f('firstname'));
			}
		}
		/*if($in['contacts_id']){
			$contact = $this->db->query("SELECT email, lastname, firstname FROM customer_contacts WHERE contact_id='".$in['contacts_id']."' ");
			if($contact->next()){
				$mail->AddAddress($contact->f('email'),$contact->f('lastname').' '.$contact->f('firstname'));
			}
		}*/

		if($in['include_pdf']){
			if($in['order_id']){
				$mail->AddAttachment("order_.pdf", $in['serial_number'].'.pdf');
			}else{
				$mail->AddAttachment("p_order.pdf", $in['serial_number'].'.pdf');
			}
        	}

   		$mail->Send();

		msg::success (gm('Mail has been successfully sent'),'success');
	}

	/**
   * make_return
   *
   * @return bool
   * @author PM
   **/
  function make_return(&$in)
  {
    $in['failure'] = false;
    if(!$this->make_return_validate($in)){
    	$in['failure'] = true;
      return false;
    }

    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }
    if(isset($in['date']) && !empty($in['date']) ){
			$in['date'] = strtotime($in['date']);
		}
  	$return_id=$this->db->insert("INSERT INTO  pim_orders_return
                            SET order_id='".$in['order_id']."',
                               `date` = '".$in['date']."',
                                note = '".$in['note']."' ");

   	if($in['lines']){
    	foreach ($in['lines'] as $key => $val) {
    		$article_id = $val['article_id'];
    		$q = $val['ret_quantity'];
        $this->db->query("INSERT INTO  pim_articles_return
                            SET return_id='".$return_id."',
                                article_id = '".$article_id."',
                                quantity = '".return_value($q)."',
                                order_id='".$in['order_id']."' ");
        //update stock
        $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        $article_data->next();
        $stock = $article_data->f("stock");

        $stock_packing = $article_data->f("packing");
        if(!$const['ALLOW_ARTICLE_PACKING']){
          $stock_packing = 1;
        }
        $stock_sale_unit = $article_data->f("sale_unit");
        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          $stock_sale_unit = 1;
        }

        $new_stock = $stock + (return_value($q)*$stock_packing/$stock_sale_unit);
        $this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

        $now=time();
        $serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");
        $this->db->insert("INSERT INTO stock_movements SET
          `date`              = '".$now."',
          created_by 					=	'".$_SESSION['u_id']."',
          order_id          =   '".$in['order_id']."',
          serial_number         =   '".$serial_number."',
          article_id          = '".$article_id."',
          article_name            =   '".addslashes($article_data->f("internal_name"))."',
          item_code           =   '".addslashes($article_data->f("item_code"))."',
          movement_type       = '13',
          quantity          =   '".return_value($q)*$stock_packing/$stock_sale_unit."',
          stock               = '".$stock."',
          new_stock           =   '".$new_stock."' ");
        if(WAC){
          generate_purchase_price($article_id);
        }
        //now we have to modify the stock on main location

        if($const['DEFAULT_STOCK_ADDRESS']){
          $current_stock_main= $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
				  $this->db->query("UPDATE dispatch_stock SET stock='".($current_stock_main +(return_value($q)*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
        }
      }
    }
    if($const['CAN_SYNC']){
      $packet = array(
	      'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['order_id']);
    }
    msg::success ( gm('Changes saved'),'success');
    return true;
  }

  /**
   * make_return_validate
   *
   * @return bool
   * @author PM
   **/
  function make_return_validate(&$in){

    $v = new validation($in);
    if($in['lines']){
			foreach ($in['lines'] as $key => $val) {
				if(return_value($val['ret_quantity'])> return_value($val['quantity_rem'])) {
				 	msg::error(gm('Invalid Quantities'),'error');
				 	return false;
				}
			}
    }
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    return $v->run();

  }

  /**
   * edit_return
   *
   * @return bool
   * @author me
   **/
  function edit_return(&$in)
  {

    if(!$this->update_return_validate($in)){
      return false;
    }
    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }

    if(isset($in['date']) && !empty($in['date']) ){
			$in['date'] = strtotime($in['date']);
		}

    $this->db->query("UPDATE pim_orders_return SET
                               `date` = '".$in['date']."',
                                note = '".$in['note']."'
                      WHERE  return_id='".$in['return_id']."' ");

    $this->db->query("DELETE FROM pim_articles_return WHERE  return_id='".$in['return_id']."'");

    if($in['lines']){
      foreach ($in['lines'] as $key => $val) {
      	$article_id = $val['article_id'];
      	$q = $val['ret_quantity'];
        $this->db->query("INSERT INTO  pim_articles_return
                            SET return_id='".$in['return_id']."',
                                article_id = '".$article_id."',
                                quantity = '".return_value($q)."',
                                order_id='".$in['order_id']."' ");

        $q=return_value($q)-return_value($in['old_ret_quantity'][$article_id]);
        //update stock
        $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        $article_data->next();
        $stock = $article_data->f("stock");

        $stock_packing = $article_data->f("packing");
        if(!$const['ALLOW_ARTICLE_PACKING']){
          $stock_packing = 1;
        }
        $stock_sale_unit = $article_data->f("sale_unit");
        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          $stock_sale_unit = 1;
        }

        $new_stock = $stock + ($q*$stock_packing/$stock_sale_unit);
        $this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

        $now=time();
        $serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");
        if($new_stock!=$stock){
	        $this->db->insert("INSERT INTO stock_movements SET
	          date              = '".$now."',
	          created_by 					=	'".$_SESSION['u_id']."',
	          order_id          =   '".$in['order_id']."',
	          serial_number     =   '".$serial_number."',
	          article_id        = '".$article_id."',
	          article_name        =   '".addslashes($article_data->f("internal_name"))."',
	          item_code           =   '".addslashes($article_data->f("item_code"))."',
	          movement_type       = '14',
	          quantity          =   '".return_value($q)*$stock_packing/$stock_sale_unit."',
	          stock               = '".$stock."',
	          new_stock           =   '".$new_stock."' ");
	        //now we have to modify the stock on main location
		      if(WAC){
		        generate_purchase_price($article_id);
		      }

		      if($const['DEFAULT_STOCK_ADDRESS']){
		        $current_stock_main= $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
		        $this->db->query("UPDATE dispatch_stock SET stock='".($current_stock_main +(return_value($q)*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
		      }
		    }
			}
    }

    if($const['CAN_SYNC']){
      $packet = array(
        'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['order_id']);
    }
    msg::success ( gm('Changes saved'),'success');
    return true;
  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function update_return_validate(&$in){

    $v = new validation($in);
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    $v->field('return_id', 'ID', 'required:exist[pim_orders_return.return_id]', "Invalid Id");
    if($in['lines']){
			foreach ($in['lines'] as $key => $val) {
			  if(return_value($val['ret_quantity'])> return_value($val['quantity_rem'])){
			  	msg::error(gm('Invalid Quantities'),'error');
			    return false;
				}
      }
    }
    return $v->run();

  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function delete_return(&$in){

    if(!$this->delete_return_validate($in)){
      return false;
    }

    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }
   	$this->db->query("SELECT * FROM pim_articles_return WHERE  return_id='".$in['return_id']."' ");
   	while ($this->db->move_next()) {
      //update stock
      $article_data = $this->db2->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$this->db->f('article_id')."' ");
      $article_data->next();
      $stock = $article_data->f("stock");

      $stock_packing = $article_data->f("packing");
      if(!$const['ALLOW_ARTICLE_PACKING']){
        $stock_packing = 1;
      }
      $stock_sale_unit = $article_data->f("sale_unit");
      if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
        $stock_sale_unit = 1;
      }

      $new_stock = $stock - ($this->db->f('quantity')*$stock_packing/$stock_sale_unit);
      $this->db2->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$this->db->f('article_id')."'");

      $now = time();
      $serial_number = $this->db2->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

      $this->db2->insert("INSERT INTO stock_movements SET
      date            = '".$now."',
      created_by 					=	'".$_SESSION['u_id']."',
      serial_number         =   '".$serial_number."',
      order_id          =   '".$in['order_id']."',
      article_id          = '".$this->db->f('article_id')."',
      article_name            =   '".addslashes($article_data->f("internal_name"))."',
      item_code           =   '".addslashes($article_data->f("item_code"))."',
      movement_type       = '15',
      quantity          =   '".$this->db->f('quantity')*$stock_packing/$stock_sale_unit."',
      stock             = '".$stock."',
      new_stock           =   '".$new_stock."',
      backorder_articles      =   '".$backorder_articles."'
      ");
      /*end for  stock movements*/
      array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$this->db->f('article_id')."'");
      if(WAC){
        generate_purchase_price($article_id);
      }
      //now we have to mod ify the stock on main location

      if($const['DEFAULT_STOCK_ADDRESS']){
        $current_stock_main= $this->db2->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$this->db->f('article_id')."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
        $this->db2->query("UPDATE dispatch_stock SET stock='".($current_stock_main -($this->db->f('quantity')*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$this->db->f('article_id')."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
      }
   	}

    $this->db->query("DELETE FROM pim_orders_return WHERE order_id='".$in['order_id']."' AND return_id='".$in['return_id']."' ");
    $this->db->query("DELETE FROM pim_articles_return WHERE  return_id='".$in['return_id']."' ");

    if(CAN_SYNC){
     	$packet = array(
        'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
    	Sync::end();
  	}

    msg::success ( gm('Return was succesfully deleted'),'success');
    return true;
  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function delete_return_validate(&$in){

    $v = new validation($in);
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    $v->field('return_id', 'ID', 'required:exist[pim_orders_return.return_id]', "Invalid Id");

    return $v->run();
  }

  /**
	 * Send order by email
	 *
	 * @return bool
	 * @author PM
	 **/
	function pdf_settings(&$in){
   	if(!$in['p_order_id']){
		  if(!($in['order_id']&&$in['pdf_layout']&&$in['logo']) )
			{
				return false;
			}
    }else{
      if(!$in['p_order_id'] )
      {
        return false;
      }
    }
		if($in['logo'] == '../img/no-logo.png'){
			$in['logo'] = 'img/no-logo.png';
		}
    if($in['p_order_id']){
     
      $this->db->query("UPDATE pim_p_orders SET email_language='".$in['email_language']."',p_order_type='".$in['pdf_layout']."',pdf_logo='".$in['logo']."', identity_id='".$in['identity_id']."' WHERE p_order_id = '".$in['p_order_id']."' ");
	  }else{
	    $this->db->query("UPDATE pim_orders SET pdf_layout='".$in['pdf_layout']."',pdf_logo='".$in['logo']."',email_language='".$in['email_language']."', identity_id='".$in['identity_id']."' WHERE order_id = '".$in['order_id']."' ");
    }
		  // $this->db->query("UPDATE pim_orders SET WHERE order_id = '".$in['order_id']."' ");

	  msg::success ( gm("Pdf settings have been updated"),'success');
	  return true;
	}

	/**
	 * prepare the orders
	 *
	 * @return nothing
	 * @author PM
	 */

	function order_prepare(&$in){

		if(!$this->prepare_validate($in))
		{
			json_out($in);
			return false;
		}
		// if(!$in['purchase']){
		// 	ark::$controller = 'norder';
		// 	// ark::run('order-norder');
		// }else{
		// 	if(!$this->tryAddC($in)){
  //     return false;
  //   }

  //     ark::$controller = 'npo_order';
		// 	// ark::run('order-npo_order');
		// }
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	/**
	 * order_prepare validate
	 * if none of the fields are filed it
	 * will return false else will return true
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */

	function prepare_validate(&$in)
	{

		$v = new validation($in);
		$v->field('contact_id','Contact','required');
		$contact = $v->run();
		$z = new validation($in);
		$z->field('buyer_id','Company','required');
		$customer = $z->run();
		if(!$contact && !$customer){
			if(ark::$controller=='orders'){
				msg::error ( gm('Please select a company or a contact'),'error');
			}elseif(!$in['add_customer']){
				msg::error ( gm('Please select a Company'),'error');
			}
			return false;
		}
		return true;
	}

	/**
	 * order add
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function add_order(&$in)
	{
		if(!$this->add_order_validate($in)){
			return false;
		}

		if ($in['rdy_invoice'])
		{
			$rdy_invoice = 1;
		}
		else
		{
			$rdy_invoice = 0;
		}

		if(!$in['notes']){
			$in['notes'] = $this->db->field("SELECT value FROM default_data WHERE type='order_note'");
    }
    if(!$in['pdf_layout'] && $in['identity_name']){
      $in['pdf_layout'] = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_name']."' and module='order'");
    }
    if(isset($in['date_h']) && !empty($in['date_h']) ){
			$in['date_h'] = strtotime($in['date_h']);
		}
		if(isset($in['del_date']) && !empty($in['del_date']) ){
			$in['del_date'] = strtotime($in['del_date']);
		}

		 if($in['contact_id']) {
      $in['contact_name']=addslashes($this->dbu->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE customer_contacts.contact_id='".$in['contact_id']."'"));
   }

   if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            $free_field=addslashes($address_info);
       }else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);      
        }
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
   
      $same_address=0;  
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);      
      }
   } 
		$in['order_id'] = $this->db->insert("INSERT INTO pim_orders SET
										customer_id 						= '".$in['customer_id']."',
										customer_name 						= '".$in['customer_name']."',
										contact_name 						= '".$in['contact_name']."',
										contact_id							= '".$in['contact_id']."',
										company_number 						= '".$in['company_number']."',
										customer_vat_id 					= '".$in['customer_vat_id']."',
										customer_email 						= '".$in['customer_email']."',
										customer_bank_name 					= '".$in['customer_bank_name']."',
										customer_bank_iban 					= '".$in['customer_bank_iban']."',
										customer_bic_code 					= '".$in['customer_bic_code']."',
										customer_vat_number 				= '".$in['customer_vat_number']."',
										customer_country_id 				= '".$in['customer_country_id']."',
										customer_state						= '".$in['customer_state']."',
										customer_city 						= '".$in['customer_city']."',
										rdy_invoice 						= '".$rdy_invoice."',
										customer_zip 						= '".$in['customer_zip']."',
										customer_phone 						= '".$in['customer_phone']."',
										customer_cell 						= '".$in['customer_cell']."',
										customer_address 					= '".$in['customer_address']."',
										created_by							= '".$_SESSION['u_id']."',
										seller_name       					= '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                                        seller_d_address     				= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
                                        seller_d_zip       					= '".addslashes(ACCOUNT_DELIVERY_ZIP)."',
                                        seller_d_city         				= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
                                        seller_d_country_id   				= '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                        seller_d_state_id     				= '".ACCOUNT_DELIVERY_STATE_ID."',
                                        seller_bwt_nr         				= '".addslashes(ACCOUNT_VAT_NUMBER)."',
                                        delivery_id 			    		= '".$in['delivery_id']."',
                                        delivery_address 					= '".$in['delivery_address']."',
                                        `date` 								= '".$in['date_h']."',
					    				payment_term 						= '".$in['payment_term']."',
					    				payment_type 						= '".$in['payment_type']."',
										serial_number 						= '".$in['serial_number']."',
										field								= '".$in['field']."',
										active								= '1',
										buyer_ref							= '".$in['buyer_ref']."',
										customer_ref						= '".$in['customer_ref']."',
										discount							= '".return_value($in['discount'])."',
										use_package           				= '".ALLOW_ARTICLE_PACKING."',
                                       	use_sale_unit         				= '".ALLOW_ARTICLE_SALE_UNIT."',
										currency_rate						= '".$in['currency_rate']."',
										our_ref								= '".$in['our_ref']."',
										your_ref							= '".$in['your_ref']."',
										del_date							= '".$in['del_date']."',
                                        del_note                    		= '".$in['del_note']."',
										remove_vat							= '".$in['remove_vat']."',
                                        quote_id 							= '".$in['quote_id']."',
                                        email_language						= '".$in['email_language']."',
										currency_type						= '".$in['currency_type_val']."' ,
										show_vat              				= '".$in['show_vat']."',
										address_info        				= '".$free_field."',
										default_delivery_address    		= '".$in['delivery_address']."',
										apply_discount						= '".$in['apply_discount']."',
										author_id							= '".$in['author_id']."',
                                        acc_manager_id           			= '".$in['acc_manager_id']."',
                                        acc_manager_name           			= '".$in['acc_manager_name']."',
										discount_line_gen					= 	'".return_value($in['discount_line_gen'])."',
                                        identity_id             			= '".$in['identity_id']."',
                                        delivery_address_id					= '".$in['delivery_address_id']."',
                                        pdf_layout              			= '".$in['pdf_layout']."',
                                          same_address='".$same_address."',
                                          main_address_id           = '".$in['main_address_id']."'
		                                    ");

		// add multilanguage order notes in note_fields
		$lang_note_id = $in['email_language'];
		
    $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($langs->next()){
    	foreach ($in['translate_loop'] as $key => $value) {
    		if($value['lang_id'] == $langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}

    	if($lang_note_id == $langs->f('lang_id')){
    		$active_lang_note = 1;
    	}else{
    		$active_lang_note = 0;
    	}
    	$this->db->insert("INSERT INTO note_fields SET
    										item_id 					=	'".$in['order_id']."',
    										lang_id 					= 	'".$langs->f('lang_id')."',
    										active 						= 	'".$active_lang_note."',
    										item_type 				= 	'order',
    										item_name 				= 	'notes',
    										item_value				= 	'".$in['nota']."'
    	");
    	
    }
    $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active = '1' ORDER BY sort_order ");
    while($custom_langs->next()) {
    	foreach ($in['translate_loop_custom'] as $key => $value) {
    		if($value['lang_id'] == $custom_langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}
    	if($lang_note_id == $custom_langs->f('lang_id')) {
    		$active_lang_note = 1;
    	} else {
    		$active_lang_note = 0;
    	}
    	$this->db->insert("INSERT INTO note_fields SET
    									item_id 		= '".$in['order_id']."',
    									lang_id 		= '".$custom_langs->f('lang_id')."',
    									active 			= '".$active_lang_note."',
    									item_type 		= 'order',
    									item_name 		= 'notes',
    									item_value 		= '".$in['nota']."'
    	");
    }

		//add articles
		$global_disc = return_value($in['discount']);
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
    $order_total=0;
		foreach ($in['tr_id'] as $nr => $row)
		{   $line_total=0;

	    if(!$row['sale_unit'] || !ALLOW_ARTICLE_PACKING){
				$row['sale_unit']=1;
			}
			if(!$row['packing'] ||  !ALLOW_ARTICLE_PACKING){
				$row['packing']=1;
			}

			$discount_line = return_value($row['disc']);
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}

			$q = return_value($row['quantity']) * return_value2($row['packing']) / $row['sale_unit'];

			$price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);

			$line_total=$price_line * $q;

      $order_total+= $line_total - ($line_total*$global_disc/100);
      $is_from_import = $this->db->field("SELECT from_adveo FROM pim_articles WHERE article_id='".$row['article_id']."'");
			if($row['is_tax'] != 1){

				$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 article_id = '".$row['article_id']."',
																		 discount = '".return_value($row['disc'])."',
																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".htmlentities($row['article'])."',
																		 price = '".return_value($row['price'])."',
                                     purchase_price = '".$row['purchase_price']."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order='".$nr."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."'
															");
			}else{ //is_tax
				$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 tax_id = '".$row['article_id']."',
																		 tax_for_article_id = '".$row['tax_for_article_id']."',
																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".htmlentities($row['article'])."',
																		 price = '".return_value($row['price'])."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order='".$nr."',
																		 discount = '".return_value($row['disc'])."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."'
															");
			}
		}

   	if($in['currency_type_val'] != ACCOUNT_CURRENCY_TYPE){
			$order_total = $order_total*return_value($in['currency_rate']);
		}
		if($in['c_quote_id']){
			$this->db->query("UPDATE tblquote SET pending = '0' WHERE id = '".$in['c_quote_id']."' ");
			$tracking_data=array(
		            'target_id'         => $in['order_id'],
		            'target_type'       => '4',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['c_quote_id'],'origin_type'=>'2')
		                                    )
		          );
		      addTracking($tracking_data);
		}
		$this->db->query("UPDATE pim_orders SET amount='".$order_total."' WHERE order_id='".$in['order_id']."'");
    msg::success ( gm("Order successfully added"),'success');
		insert_message_log($this->pag,'{l}Order successfully added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$in['add_problem'] = $this->pag;
		// ark::$controller = 'order';
		/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
															AND name		= 'order-orders_show_info'	");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
															AND name		= :name	",['user_id'=>$_SESSION['u_id'],'name'=>'order-orders_show_info']);															
		if(!$show_info->move_next()) {
			/*$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
													name 	= 'order-orders_show_info',
													value 	= '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET 	user_id = :user_id,
													name 	= :name,
													value 	= :value ",
												['user_id' => $_SESSION['u_id'],
												 'name' 	=> 'order-orders_show_info',
												 'value' 	=> '1']
											);													
		} else {
			/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
														  AND name 	 	= 'order-orders_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
														  AND name 	 	= :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'order-orders_show_info']);														  
		}
		unset($in['c_quote_id']);
		unset($in['quote_id']);
    $count = $this->db->field("SELECT COUNT(order_id) FROM pim_orders ");
    if($count == 1){
      doManageLog('Created the first order.','',array( array("property"=>'first_module_usage',"value"=>'Order') ));
    }

		return true;
	}
	/**
	 * order add validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function add_order_validate(&$in)
	{
		$v = new validation($in);
		$v->field('serial_number', 'Name', 'required',gm('Order Nr').'* '.gm('is required').'<br />');
		if(!$v->run()){
			return false;
		}
		if (ark::$method == 'add_order') {
			$v->field('serial_number', 'Serial Number', 'unique[pim_orders.serial_number]',gm('Order Nr').' '.gm('is already in use'));
			if(!$v->run()){
				$v = null;
				$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
				$v = new validation($in);
				$v->field('serial_number', 'Name', 'required',gm('Order Nr').'* '.gm('is required').'<br />');
			}
		}else{
			$v->field('serial_number', "Serial Number", "unique[pim_orders.serial_number.( order_id!='".$in['order_id']."')]",gm('Order Nr').'* '.gm('is already in use').'.<br />');
		}

		$v->field('customer_name', "Company Name", "required",gm('Customer Name').' '.gm('is required').'.<br />');

		return $v->run();
	}

	/**
	 * order update
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function update_order(&$in)
	{
		if(!$this->update_order_validate($in)){
			return false;
		}
		if ($in['rdy_invoice'])
		{
			$rdy_invoice = 1;
		}
		else
		{
			$rdy_invoice = 0;
		}

		if(!$in['delivery_cost_amount']){
			$in['delivery_cost_amount'] = return_value(0);
		}
		if(!$in['show_shipping_price']){
			$in['show_shipping_price'] = 0;
		}
		if(!$in['acc_manager_name']){
		  $in['acc_manager_id']=0;
		}
		if(isset($in['date_h']) && !empty($in['date_h']) ){
			$in['date_h'] = strtotime($in['date_h']);
		}
		if(isset($in['del_date']) && !empty($in['del_date']) ){
			$in['del_date'] = strtotime($in['del_date']);
		}

		   if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customer_addresses.*
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            if($buyer_info->f('address_id') != $in['main_address_id']){
                $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
                $address_info = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
            }
            $free_field=addslashes($address_info);
       }else{
            $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
            $same_address=$in['delivery_address_id'];
            $free_field=addslashes($new_address_txt);      
        }
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
   
      $same_address=0;  
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);      
      }
   }

    	$this->db->query("UPDATE pim_orders SET customer_name 					= '".$in['customer_name']."',
                                            contact_name 					= '".$in['contact_name']."',
                                            contact_id						= '".$in['contact_id']."',
                                            company_number 					= '".$in['company_number']."',
                                            customer_vat_id 				= '".$in['customer_vat_id']."',
                                            customer_email 					= '".$in['customer_email']."',
                                            customer_bank_name 				= '".$in['customer_bank_name']."',
                                            customer_bank_iban 				= '".$in['customer_bank_iban']."',
                                            customer_bic_code 				= '".$in['customer_bic_code']."',
                                            customer_vat_number 			= '".$in['customer_vat_number']."',
                                            customer_country_id 			= '".$in['customer_country_id']."',
                                            customer_state 					= '".$in['customer_state']."',
                                            customer_city 					= '".$in['customer_city']."',
                                            rdy_invoice 					= '".$rdy_invoice."',
                                            customer_zip 					= '".$in['customer_zip']."',
                                            customer_phone 					= '".$in['customer_phone']."',
                                            buyer_ref 						= '".$in['buyer_ref']."',
                                            customer_address 				= '".$in['customer_address']."',
                                            seller_name       				= '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                                            seller_d_address    			= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
                                      		seller_d_zip       				= '".ACCOUNT_DELIVERY_ZIP."',
                                     		seller_d_city       			= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
                                      		seller_d_country_id 			= '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                      		seller_d_state_id   			= '".ACCOUNT_DELIVERY_STATE_ID."',
                                      		seller_bwt_nr       			= '".addslashes(ACCOUNT_VAT_NUMBER)."',
											`date` 							= '".$in['date_h']."',
											payment_term 					= '".$in['payment_term']."',
											payment_type 					= '".$in['payment_type']."',
                                            serial_number 					= '".$in['serial_number']."',
                                            discount						= '".return_value($in['discount'])."',
                                            delivery_id						= '".$in['delivery_id']."',
                                            our_ref							= '".$in['our_ref']."',
                                            your_ref						= '".$in['your_ref']."',
                                            del_date						= '".$in['del_date']."',
                                            del_note      					= '".$in['del_note']."',
                                            delivery_address 				= '".$in['delivery_address']."',
                                            show_vat            			= '".$in['show_vat']."',
                                            address_info        			= '".trim($free_field)."',
                                           main_address_id           = '".$in['main_address_id']."',
                                                same_address='".$same_address."',
                                            apply_discount					= '".$in['apply_discount']."',
                                            author_id						= '".$in['author_id']."',
                                            discount_line_gen				= '".return_value($in['discount_line_gen'])."',
                                            shipping_price 		        	= '".$in['delivery_cost_amount']."',
                                            show_shipping_price 		  	= '".$in['show_shipping_price']."',
                                            acc_manager_id            		= '".$in['acc_manager_id']."',
                                            acc_manager_name          		= '".$in['acc_manager_name']."',
                                            shipping_price 					= '".$in['delivery_cost_amount']."',
                                            show_shipping_price 			= '".$in['show_shipping_price']."',
                                            email_language					= '".$in['email_language']."',
                                            delivery_address_id				= '".$in['delivery_address_id']."',
                                            identity_id             		='".$in['identity_id']."'
                                            WHERE order_id 					='".$in['order_id']."' ");

		// update multilanguage order note in note_fields
		$lang_note_id = $in['email_language'];
    $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($langs->next()){
    	foreach ($in['translate_loop'] as $key => $value) {
    		if($value['lang_id'] == $langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}
    	if($lang_note_id == $langs->f('lang_id')){
    		$active_lang_note = 1;
    	}else{
    		$active_lang_note = 0;
    	}
    	if(!$in['order_from_front']){
    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'".$active_lang_note."',
    										item_value				= 	'".$in['nota']."'
    								WHERE 	item_type 				= 	'order'
    								AND		item_name 				= 	'notes'
    								AND		lang_id 				= 	'".$langs->f('lang_id')."'
    								AND		item_id 				= 	'".$in['order_id']."'

    		");
    	}else{
    		$this->db->insert("INSERT INTO note_fields SET
    										item_id 				=	'".$in['order_id']."',
    										lang_id 				= 	'".$langs->f('lang_id')."',
    										active 					= 	'".$active_lang_note."',
    										item_type 				= 	'order',
    										item_name 				= 	'notes',
    										item_value				= 	'".$in['nota']."'
    		");
    	}
    }
    $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
    while($custom_langs->next()) {
    	foreach ($in['translate_loop_custom'] as $key => $value) {
    		if($value['lang_id'] == $custom_langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}
    	if($lang_note_id == $custom_langs->f('lang_id')) {
    		$active_lang_note_custom = 1;
    	} else {
    		$active_lang_note_custom = 0;
    	}
    	$this->db->query("UPDATE note_fields SET 	active 		=  	'".$active_lang_note_custom."',
    												item_value 	=  	'".$in['notes_'.$custom_langs->f('lang_id')]."'
    										 WHERE  item_type 	=  	'order'
    										 AND 	item_name 	=  	'notes'
    										 AND 	lang_id 	= 	'".$custom_langs->f('lang_id')."'
    										 AND 	item_id 	= 	'".$in['order_id']."'
    	");
    }

    $this->db->query("DELETE FROM pim_order_articles WHERE order_id='".$in['order_id']."'");

		$use_package=$this->db->field("SELECT use_package FROM pim_orders WHERE order_id='".$in['order_id']."'");
		$use_sale_unit=$this->db->field("SELECT use_sale_unit FROM pim_orders WHERE order_id='".$in['order_id']."'");

		$global_disc = return_value($in['discount']);
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
		$order_total=0;
		foreach ($in['tr_id'] as $nr => $row)
		{
	    if(!$use_package){
				$row['packing']=1;
			}
			if(!$use_sale_unit){
				$row['sale_unit']=1;
			}

			$discount_line = return_value($row['disc']);
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}

			$q = @(return_value($row['quantity']) * return_value2($row['packing']) / $row['sale_unit']);

			$price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);

			$line_total=$price_line * $q;

			$order_total+= $line_total - ($line_total*$global_disc/100);

			$is_from_import = $this->db->field("SELECT from_adveo FROM pim_articles WHERE article_id='".$row['article_id']."'");

			if($row['is_tax']!=1){

      	$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 article_id = '".$row['article_id']."',
																		 quantity = '".return_value($row['quantity'])."',
																		 discount = '".return_value($row['disc'])."',
																		 article = '".htmlentities($row['article'])."',
																     	 purchase_price = '".$row['purchase_price']."',
                                     									 price = '".return_value($row['price'])."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value   = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order  ='".$nr."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."'
															");
			}else{
				$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 tax_id = '".$row['article_id']."',
																		 tax_for_article_id = '".$row['tax_for_article_id']."',
																		 quantity = '".return_value($row['quantity'])."',
																		 discount = '".return_value($row['disc'])."',
																		 article  = '".htmlentities($row['article'])."',
																		 price    = '".return_value($row['price'])."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order  ='".$nr."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."'
															");
			}

		}
		if($in['currency_type_val'] != ACCOUNT_CURRENCY_TYPE){
			$order_total = $order_total*return_value($in['currency_rate']);
		}
		$this->db->query("UPDATE pim_orders SET amount='".$order_total."' WHERE order_id='".$in['order_id']."'");

		msg::success ( gm("Order successfully updated"),'success');
		// ark::$controller = 'order';
		// ark::run('order-order');
		insert_message_log($this->pag,'{l}Order successfully updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$in['add_problem'] = $this->pag;
		// json_out($in);
		return true;
	}

	/**
	 * order update validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function update_order_validate(&$in)
	{

		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', gm('Invalid ID'));
		if(!$v->run()){
			return false;
		} else {
			return $this->add_order_validate($in);
		}
	}

	/**
	 * order update validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function get_article_quantity_price(&$in)
	{
	    if($in['customer_id']){
	      $customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
	      if($customer_custom_article_price->move_next()){
	        $price = $customer_custom_article_price->f('price');
	        if($in['asString']){
	        	return $price;
	        }
	        $in['new_price']=$price;
	        json_out($in);
	        return true;
	      }
	    }
		$price_type=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
    	$is_block_discount=$this->db->query("SELECT pim_articles.block_discount FROM pim_articles
                               WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.block_discount='1'");

	    if($is_block_discount->next()){
		  	$price=$in['price'];
		}else{
			if($price_type==1){
		   		$price=$in['price'];
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices
		    		                     WHERE pim_article_prices.from_q<='".$in['quantity']."'
		    		                     AND  pim_article_prices.article_id='".$in['article_id']."'
		                                 ORDER BY pim_article_prices.from_q DESC
		                                 LIMIT 1
		    		                     ");

			    if(!$price){
			     	$price=$in['price'];
			    }
		    }
		}
		if($in['asString']){
        	return $price;
        }
	  	$in['new_price']=$price;
    	json_out($in);
		return true;
	}

	/**
	 * order update validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	/*function update_purchase_price(&$in) {


   /* Sync::start(ark::$model.'-'.ark::$method);
    $this->db->query("UPDATE pim_order_articles SET purchase_price='".return_value($in['purchase_price'])."' WHERE order_id='".$in['order_id']."' AND article_id='".$in['article_id']."'");


    Sync::end();* /
    msg::$success = gm("Price settings has been successfully updated");
    return true;
  }*/
  /**
	 * Archive a order
	 *
	 * @param array $in
	 * @return bool
	 * @author PM
	 */
	function archive_order(&$in){
		if(!$this->delete_order_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);

		$this->db->query("UPDATE pim_orders SET active='0' WHERE order_id='".$in['order_id']."' ");

		msg::success ( gm("Order has been archived"),'success');
		insert_message_log($this->pag,'{l}Order has been archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$tracking_trace=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
	    	if($tracking_trace){
	      	$this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
	    	}
		Sync::endSend(WEB_URL, HOME_API_KEY);
		return true;
	}
		function delete_order(&$in)
	{
		if(!$this->delete_order_validate($in)){
			return false;
		}
		$tracking_trace=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		if(ALLOW_STOCK){
		Sync::start(ark::$model.'-'.ark::$method);
		$delivery = $this->db->query("SELECT order_articles_id, quantity
			                      FROM pim_orders_delivery
			                      WHERE order_id='".$in['order_id']."'");


		while ($delivery->next())
		{
			// $article_id =  $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id='".$delivery->f('order_articles_id')."'");
			$article_data = $this->db->query("SELECT article_id, article FROM pim_order_articles WHERE order_articles_id='".$delivery->f('order_articles_id')."'");
			$article_data->next();
			$article_id = $article_data->f("article_id");
			// $stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id=".$article_id);
			$stock_and_item_code = $this->db->query("SELECT stock, item_code, packing, sale_unit FROM pim_articles WHERE article_id=".$article_id);
			$item_code = $stock_and_item_code->f("item_code");
			$stock = $stock_and_item_code->f("stock");
			$new_stock = $stock + ($delivery->f('quantity')*$stock_and_item_code->f("packing")/$stock_and_item_code->f("sale_unit"));
			$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id=".$article_id);

		}
		Sync::end($in['order_id']);
       }


		$this->db->query("DELETE FROM pim_order_deliveries WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_order_articles  WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_order_articles  WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'order' AND item_name = 'notes' AND item_id = '".$in['order_id']."'");
        $this->db->query("DELETE FROM dispatch_stock_movements  WHERE order_id='".$in['order_id']."' ");


		msg::success (gm("Order has been deleted"),'success');
		insert_message_log($this->pag,'{l}Order deleted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		if($tracking_trace){
	      	$this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
	      	$this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
	    	}
		return true;
	}

	/**
	 * Archive a order
	 *
	 * @param array $in
	 * @return bool
	 * @author PM
	 */
	function activate_order(&$in){
		if(!$this->delete_order_validate($in)){
			return false;
		}


		$this->db->query("UPDATE pim_orders SET active='1' WHERE order_id='".$in['order_id']."' ");

		msg::success ( gm("Order has been activated"),'success');
		insert_message_log($this->pag,'{l}Order has been activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$tracking_trace=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
        	if($tracking_trace){
          		$this->db->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
        	}
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		/*if($vat_numeric || substr($value,0,2)=="BE"){
			$trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
			$trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
			$trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

			if(!$trends_access_token || $trends_expiration<time()){
				$ch = curl_init();
				$headers=array(
					"Content-Type: x-www-form-urlencoded",
					"Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
					);
				/*$trends_data=!$trends_access_token ? "grant_type=password&username=akti_api&password=akti_api" : "grant_type=refresh_token&&refresh_token=".$trends_refresh_token;* /
				$trends_data="grant_type=password&username=akti_api&password=akti_api";

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
		       	curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
			    curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
			 	console::log('aaaaa');
			 	console::log($info);* /

			 	if($info['http_code']==400){
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else{
			 		if(!$trends_access_token){
			 			$this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
					 	$this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
					 	$this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
			 		}else{
			 			$this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
			 			$this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
			 			$this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
			 		}

			 		$ch = curl_init();
					$headers=array(
						"Authorization: Bearer ".$put->access_token
						);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    if($vat_numeric){
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
				    }else{
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
				    }
				    $put = json_decode(curl_exec($ch));
				 	$info = curl_getinfo($ch);

				 	/*console::log($put);
				 	console::log('bbbbb');
				 	console::log($info);* /

				    if($info['http_code']==400 || $info['http_code']==429){
				    	if(ark::$method == 'check_vies_vat_number'){
					 		msg::error ( $put->error,'error');
					 		json_out($in);
					 	}
						return false;
				 	}else if($info['http_code']==404){
				 		if($vat_numeric){
				 			if(ark::$method == 'check_vies_vat_number'){
					 			msg::error ( gm("Not a valid vat number"),'error');
					 			json_out($in);
					 		}
							return false;
				 		}
				 	}else{
				 		if($in['customer_id']){
				 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 			if($country_id != 26){
				 				$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 				$in['remove_v']=1;
				 			}else if($country_id == 26){
					 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
					 			$in['add_v']=1;
					 		}
				 		}
				 		$in['comp_name']=$put->officialName;
				 		$in['comp_address']=$put->street.' '.$put->houseNumber;
						$in['comp_zip']=$put->postalCode;
						$in['comp_city']=$put->city;
				 		$in['comp_country']='26';
				 		$in['trends_ok']=true;
				 		$in['trends_lang']=$_SESSION['l'];
				 		$in['full_details']=$put;
				 		if(ark::$method == 'check_vies_vat_number'){
					 		msg::success(gm('Success'),'success');
					 		json_out($in);
					 	}
				 		return false;
				 	}
			 	}
			}else{
				$ch = curl_init();
				$headers=array(
					"Authorization: Bearer ".$trends_access_token
					);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    if($vat_numeric){
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
			    }else{
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
			    }
			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
				console::log('ccccc');
				console::log($info);* /

			    if($info['http_code']==400 || $info['http_code']==429){
			    	if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else if($info['http_code']==404){
			 		if($vat_numeric){
			 			if(ark::$method == 'check_vies_vat_number'){
				 			msg::error (gm("Not a valid vat number"),'error');
				 			json_out($in);
				 		}
						return false;
			 		}
			 	}else{
			 		if($in['customer_id']){
			 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 		if($country_id != 26){
				 			$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['remove_v']=1;
				 		}else if($country_id == 26){
				 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['add_v']=1;
				 		}
				 	}
			 		$in['comp_name']=$put->officialName;
			 		$in['comp_address']=$put->street.' '.$put->houseNumber;
					$in['comp_zip']=$put->postalCode;
					$in['comp_city']=$put->city;
				 	$in['comp_country']='26';
			 		$in['trends_ok']=true;
			 		$in['trends_lang']=$_SESSION['l'];
			 		$in['full_details']=$put;
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::success(gm('Success'),'success');
				 		json_out($in);
				 	}
			 		return false;
			 	}
			}
		}*/


		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	function tryAddC(&$in){
		console::benchmark('tryAddCValid');
		if(!$this->tryAddCValid($in)){ 
			console::end('tryAddCValid');
			json_out($in);
			return false; 
		}

		//$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

		//Set account default vat on customer creation
	    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	    if(empty($vat_regime_id)){
	      $vat_regime_id = 0;
	    }

		if($in['add_customer']){
		  	$payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       		$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
		  	$in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
		                                        name='".$in['name']."',
		                                        btw_nr='".$in['btw_nr']."',
		                                        vat_regime_id     = '".$vat_regime_id."',
		                                        city_name = '".$in['city']."',
		                                        c_email = '".$in['email']."',
                                                comp_phone ='".$in['phone']."',
		                                        user_id = '".$_SESSION['u_id']."',
		                                        acc_manager_name = '".$name_user."',
		                                        country_name ='".get_country_name($in['country_id'])."',
		                                        active='1',
		                                        payment_term 			= '".$payment_term."',
												payment_term_type 		= '".$payment_term_type."',
		                                        zip_name  = '".$in['zip']."'");
		  
		  	$this->db->query("INSERT INTO customer_addresses SET
		                    address='".$in['address']."',
		                    zip='".$in['zip']."',
		                    city='".$in['city']."',
		                    country_id='".$in['country_id']."',
		                    customer_id='".$in['buyer_id']."',
		                    is_primary='1',
		                    delivery='1',
		                    billing='1' ");
		  	$in['customer_name'] = $in['name'];

		  // include_once('../apps/company/admin/model/customer.php');
		  	$in['value']=$in['btw_nr'];
		  // $comp=new customer();       
		  // $this->check_vies_vat_number($in);
		  	if($this->check_vies_vat_number($in) != false){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
		  	}else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
		  	}

		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                            AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);		                            
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                        name    = 'company-customers_show_info',
		                        value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                        name    = :name,
		                        value   = :value ",
		                    ['user_id' => $_SESSION['u_id'],
		                     'name'    => 'company-customers_show_info',
		                     'value'   => '1']
		                );		                        
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                            AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);		                            
		 	}
		  	$count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
		  	if($count == 1){
		    	doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
		  	}
		  	if($in['quote_id'] && is_numeric($in['quote_id'])){
		  		$address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
		  		$this->db->query("UPDATE tblquote SET 
		  			customer_id='".$in['buyer_id']."', 
		  			customer_name='".$in['name']."',
		  			contact_id='',
		  			contact_name='',
		  			field='customer_id',
		  			delivery_address='', 
		  			delivery_address_id='', 
		  			address_info = '".$address."'
		  			WHERE id='".$in['quote_id']."' ");		  		
		  	}
		  	msg::success (gm('Success'),'success');
		  	return true;
		}
		if($in['add_contact']){
			$customer_name='';
			if($in['buyer_id']){
				$customer_name = $this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' ");
			}
		  	$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
		                                          firstname='".$in['firstname']."',
		                                          lastname='".$in['lastname']."',
		                                          sex='".$in['gender_id']."',
                                              	  title ='".$in['title_id']."',
                                              	  language ='".$in['language_id']."',
		                                          `create`  = '".time()."',
		                                          customer_id = '".$in['buyer_id']."',
		                                          company_name=	'".$customer_name."',
		                                          email='".$in['email']."' ");
		  	$in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
		  	if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1){
	                $vars=array();
	                if($in['buyer_id']){
	                    $vars['customer_id']=$in['buyer_id'];
	                    $vars['customer_name']=$customer_name;
	                }          
	                $vars['contact_id']=$in['contact_id'];
	                $vars['firstname']=$in['firstname'];
	                $vars['lastname']=$in['lastname'];
	                $vars['table']='customer_contacts';
	                $vars['email']=$in['email'];
	                $vars['op']='add';
	                synctoZendesk($vars);
	            }
		  	if($in['country_id']){
		    	$this->db->query("INSERT INTO customer_contact_address SET
		                      address='".$in['address']."',
		                      zip='".$in['zip']."',
		                      city='".$in['city']."',
		                      country_id='".$in['country_id']."',
		                      contact_id='".$in['contact_id']."',
		                      is_primary='1',
		                      delivery='1' ");
		  	}
		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                                AND name    = 'company-contacts_show_info'  ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                                AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                              name    = 'company-contacts_show_info',
		                              value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                              name    = :name,
		                              value   = :value ",
		                            ['user_id' => $_SESSION['u_id'],
		                             'name'    => 'company-contacts_show_info',
		                             'value'   => '1']
		                        );		                              
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                                  AND name    = 'company-contacts_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                                  AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                  
		  	}
		  	$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
		  	if($count == 1){
		    	doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
		  	}
		  	if($in['quote_id'] && is_numeric($in['quote_id'])){
		  		$this->db->query("UPDATE tblquote SET contact_id='".$in['contact_id']."', contact_name='".($in['firstname'].' '.$in['lastname'])."' WHERE id='".$in['quote_id']."' ");	
		  	}
		  	msg::success (gm('Success'),'success');
		}
		return true;
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddCValid(&$in)
	{
		if($in['add_customer']){
		  	$v = new validation($in);
			$v->field('name', 'name', 'required:unique[customers.name]');
			$v->field('country_id', 'country_id', 'required');
			return $v->run();  
		}
		if($in['add_contact']){
			$v = new validation($in);
			$v->field('firstname', 'firstname', 'required');
			$v->field('lastname', 'lastname', 'required');
/*			$v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');*/
/*			$v->field('country_id', 'country_id', 'required');*/
			return $v->run();  
		}
		return true;
	}

	

	/**
	 * undocumented function
	 *
	 * @return boolean
	 * @author PM
	 **/
	function updateCustomerData(&$in)


	{
		if($in['field'] == 'contact_id'){
			$in['buyer_id'] ='';
		}
		$sql = "UPDATE pim_orders SET ";
		if($in['buyer_id']){
			$buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, 
									customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, 
									customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount
									FROM customers
									LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
									AND customer_addresses.is_primary=1
									WHERE customers.customer_id='".$in['buyer_id']."' ");
			$buyer_info->next();
			/*if(!$in['delivery_address']){
				$delivery_addr = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 LIMIT 1 ");
				if($delivery_addr){
					$in['delivery_address'] = $delivery_addr->f('address')."\n".$delivery_addr->f('zip').'  '.$delivery_addr->f('city')."\n".get_country_name($delivery_addr->f('country_id'));
					$in['delivery_address_id']=$delivery_addr->f('address_id');
				}
			}*/

			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$apply_discount = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$apply_discount = 2;
				}
				if($buyer_info->f('apply_line_disc')){
					$apply_discount = 1;
				}
			}
			$in['apply_discount'] = $apply_discount;
		    
			if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
				$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
			}
			
			$in['currency_rate']=0;
			$in['currency_id']	= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
			if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
				$in['currency_rate'] = $this->currencyRate(currency::get_currency($in['currency_id'],'code'));
			}
			$in['contact_name'] ='';
			if($in['order_id'] == 'tmp'){
        $in['delivery_address_id'] = $buyer_info->f('address_id');
      }


			$sql .= " customer_id = '".$in['buyer_id']."', ";
			$sql .= " customer_name = '".addslashes($buyer_info->f('name'))."', ";
			$sql .= " customer_email = '".$buyer_info->f('c_email')."', ";
			$sql .= " customer_vat_number = '".$buyer_info->f('btw_nr')."', ";
			$sql .= " customer_country_id = '".$buyer_info->f('country_id')."', ";
			$sql .= " customer_city = '".addslashes($buyer_info->f('city'))."', ";
			$sql .= " customer_zip = '".$buyer_info->f('zip')."', ";
			$sql .= " customer_phone = '".$buyer_info->f('comp_phone')."', ";
			$sql .= " customer_address = '".addslashes($buyer_info->f('address'))."', ";
			$sql .= " main_address_id = '".$in['main_address_id']."', ";
			$sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
			//$sql .= " delivery_address = '".addslashes($in['delivery_address'])."', ";
			//$sql .= " delivery_address_id = '".$in['delivery_address_id']."', ";
			$sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
			$sql .= " customer_ref = '".addslashes($buyer_info->f('our_reference'))."', ";
			$sql .= " buyer_ref = '".addslashes($ref)."', ";
			$sql .= " currency_type = '".$in['currency_id']."', ";
			$sql .= " currency_rate = '".$in['currency_rate']."', ";
			
			if($in['contact_id']){
				$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
				$sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
				$sql .= " contact_id = '".$in['contact_id']."', ";
				$in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
			}else{
				$sql .= " contact_name = '', ";
				$sql .= " contact_id = '', ";
			}
			

			$in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));

			if($buyer_info->f('address_id') != $in['main_address_id']){
        $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
      }
               if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " address_info = '".addslashes($in['address_info'])."', ";
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " address_info = '".addslashes($new_address_txt)."', ";      
      }

		
			


			$sql .= " field = 'customer_id' ";


			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '".$in['apply_discount']."' ";
				$sql .= ",  remove_vat = '".$buyer_info->f('no_vat')."' ";
				$sql .= ",  discount_line_gen = '".$buyer_info->f('line_discount')."' ";
				$sql .= ",  discount = '".$buyer_info->f('fixed_discount')."' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => $value['price'], 
						'quantity' => $value['quantity'], 
						'customer_id' => $in['buyer_id'],
						'cat_id' => $buyer_info->f('cat_id'),
						'asString' => true
					);
					$in['article'][$key]['price'] = display_number($this->getArticlePrice($params));
				}
			}
			
		}else{
			if(!$in['contact_id']){
				msg::error ( gm('Please select a company or a contact'),'error');
				return false;
			}
			$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
			$sql .= " customer_id = '".$in['contact_id']."', ";
			$sql .= " customer_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
			$sql .= " contact_name = '', ";
			$sql .= " contact_id = '".$in['contact_id']."', ";
			$sql .= " customer_email = '".$contact_info->f('email')."', ";
			$sql .= " customer_vat_number = '', ";
			$sql .= " customer_country_id = '".$contact_address->f('country_id')."', ";
			$sql .= " customer_city = '".addslashes($contact_address->f('city'))."', ";
			$sql .= " customer_zip = '".$contact_address->f('zip')."', ";
			$sql .= " customer_phone = '".$contact_info->f('phone')."', ";
			$sql .= " customer_address = '".addslashes($contact_address->f('address'))."', ";
			$sql .= " email_language = '".DEFAULT_LANG_ID."', ";
			//$sql .= " delivery_address = '', ";
			//$sql .= " delivery_address_id = '', ";
			$sql .= " identity_id = '".$in['identity_id']."', ";
			$sql .= " customer_ref = '', ";
			$sql .= " buyer_ref = '', ";
			$sql .= " currency_type = '".ACCOUNT_CURRENCY_TYPE."', ";
			$sql .= " currency_rate = '0', ";
			$in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
			$sql .= " address_info = '".addslashes($in['address_info'])."', ";
			$sql .= " field = 'contact_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '0' ";
				$sql .= ",  remove_vat = '0' ";
				$sql .= ",  discount_line_gen = '0' ";
				$sql .= ",  discount = '0' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => return_value($value['price']), 
						'quantity' => return_value($value['quantity']),
						'cat_id' => '0',
						'asString' => true
					);
					$in['article'][$key]['price'] = display_number($this->getArticlePrice($params));
				}
			}
		}
		$sql .=" WHERE order_id ='".$in['item_id']."' ";
		if(!$in['isAdd']){
		
			$this->db->query($sql);	

		}
		$in['order_id'] = $in['item_id'];
		msg::$success = gm('Sync successfull.');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function getArticlePrice($in){
		$article = $this->db->query("SELECT article_category_id, block_discount,price_type FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		if($article->f('price_type')==1){

		    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");
	        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$in['article_id']."' and category_id='".$cat_id."' ");

	       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
	            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$in['article_id']."'");
	        }else{
	       	   	$price_value=$price_value_custom_fam;

	         	 //we have to apply to the base price the category spec
	    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
	    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
	    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

	    	    if($cat_price_type==2){
	                $article_base_price=get_article_calc_price($in['article_id'],3);
	            }else{
	                $article_base_price=get_article_calc_price($in['article_id'],1);
	            }

	       		switch ($cat_type) {
					case 1:                  //discount
						if($price_value_type==1){  // %
							$price = $article_base_price - $price_value * $article_base_price / 100;
						}else{ //fix
							$price = $article_base_price - $price_value;
						}
						break;
					case 2:                 //profit margin
						if($price_value_type==1){  // %
							$price = $article_base_price + $price_value * $article_base_price / 100;
						}else{ //fix
							$price =$article_base_price + $price_value;
						}
						break;
				}
	        }

		    if(!$price || $article->f('block_discount')==1 ){
	        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$in['article_id']."' AND base_price=1");
	        }
	    }else{
	    	$price = $in['price'];
	    	// return $this->get_article_quantity_price($in);
	    }

	    $start= mktime(0, 0, 0);
	    $end= mktime(23, 59, 59);
	    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$in['article_id']."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
	    if($promo_price->move_next()){
	    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

	        }else{
	            $price=$promo_price->f('price');
	        }
	    }
	 	if($in['customer_id']){
	  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
	    	if($customer_custom_article_price->move_next()){
	            $price = $customer_custom_article_price->f('price');
	       	}
	   	}
	   	return $price;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
		if(!$this->CanEdit($in)){
			json_out($in);
			return false;
		}
		if(!$this->tryAddAddressValidate($in)){
			json_out($in);
			return false;
		}
		$country_n = get_country_name($in['country_id']);
		if($in['field']=='customer_id'){
	 		Sync::start(ark::$model.'-'.ark::$method);

			$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																				customer_id			=	'".$in['customer_id']."',
																				country_id			=	'".$in['country_id']."',
																				state_id			=	'".$in['state_id']."',
																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				billing				=	'".$in['billing']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
			if($in['billing']){
				$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}

			Sync::end($address_id);

			/*if($in['delivery']){
				$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}*/
			if($in['primary']){
				if($in['address'])
				{
					if(!$in['zip'] && $in['city'])
					{
						$address = $in['address'].', '.$in['city'].', '.$country_n;
					}elseif(!$in['city'] && $in['zip'])
					{
						$address = $in['address'].', '.$in['zip'].', '.$country_n;
					}elseif(!$in['zip'] && !$in['city'])
					{
						$address = $in['address'].', '.$country_n;
					}else
					{
						$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
					}
					$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
					$output= json_decode($geocode);
					$lat = $output->results[0]->geometry->location->lat;
					$long = $output->results[0]->geometry->location->lng;
				}
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
				$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
				$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
			}
		}else{			
			$address_id = $this->db->insert("INSERT INTO customer_contact_address SET
																				contact_id			=	'".$in['contact_id']."',
																				country_id			=	'".$in['country_id']."',																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
		}
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['address_id'] = $address_id;
		$in['country'] = $country_n;
		$in['order_id'] = $in['item_id'];
		if($in['order_id'] && is_numeric($in['order_id'])){
			$delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
			$this->db->query("UPDATE pim_orders SET 
	  			delivery_address='".$delivery_address."', 
	  			delivery_address_id='".$address_id."' WHERE order_id='".$in['order_id']."'  ");	
	  	}elseif ($in['order_id'] == 'tmp') {
	  		$in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	  		$in['delivery_address_id'] = $address_id;
	  	}
	  	$in['buyer_id'] = $in['customer_id'];
		// $in['pagl'] = $this->pag;
		
		// json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddressValidate(&$in)
	{
		$v = new validation($in);
		if($in['customer_id']){
			$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		}else if($in['contact_id']){
			$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		}else{
			msg::error(gm('Invalid ID'),'error');
			return false;
		}
		$v->field('country_id', 'Country', 'required:exist[country.country_id]');

		return $v->run();
	}

	function CanEdit(&$in){
		if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
			return true;
		}
		$c_id = $in['customer_id'];
		if(!$in['customer_id'] && $in['contact_id']) {
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		}
		if($c_id){
			if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
				$u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
				if($u){
					$u = explode(',', $u);
					if(in_array($_SESSION['u_id'], $u)){
						return true;
					}
					else{
						msg::$warning = gm("You don't have enought privileges");
						return false;
					}
				}else{
					msg::$warning = gm("You don't have enought privileges");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addDispatchNote(&$in){
		if(!$this->addDispatchNoteValid($in)){
			json_out($in);
			return false;
		}
		$in['del_note'] = trim($in['del_note']);
		$this->db->query("UPDATE pim_orders SET del_note='".$in['del_note']."' WHERE order_id='".$in['order_id']."' ");
		msg::success ( gm('Changes saved'),'success');
		$in['del_note_txt'] = nl2br($in['del_note']);
		json_out($in);

	}
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addDispatchNoteValid(&$in)
	{
		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', gm('Invalid ID'));
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->updateCustomer_validate($in)){
			return false;
		}
        $query_sync=array();

		// $c_type = explode(',', $in['c_type']);
		
		foreach ($in['c_type'] as $key) {
			if($key){
				$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
				$c_type_name .= $type.',';
			}
		}
		$c_types = implode(',', $in['c_type']);
		$c_type_name = rtrim($c_type_name,',');


		if($in['user_id']){
			foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT first_name,last_name FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET name 				= '".$in['name']."',
											serial_number_customer  =   '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											active					= '".$in['active']."',
											legal_type				= '".$in['legal_type']."',
											user_id					= '".$acc_manager_ids."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['c_email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['comp_phone']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											c_type_name				= '".$c_type_name."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											acc_manager_name		= '".$in['acc_manager']."',
											sales_rep				= '".$in['sales_rep']."',
											is_supplier				= '".$in['is_supplier']."',
											our_reference			= '".$in['our_reference']."'
					  	WHERE customer_id='".$in['customer_id']."' ");

		if($in['customer_id_linked']){
			$link_id = $this->db->insert("INSERT INTO customer_link SET customer_id='".$in['customer_id']."',
														customer_id_linked='".$in['customer_id_linked']."',
														link_type='".$in['link_type']."'");
			if($in['link_type'] == 1){
				$type = 2;
			}
			else{
				$type = 1;
			}
			$this->db->query("INSERT INTO customer_link SET customer_id='".$in['customer_id_linked']."',
														customer_id_linked='".$in['customer_id']."',
														link_type='".$type."',
														link_id='".$link_id."'");
		}
		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."'	WHERE buyer_id='".$in['customer_id']."' ");

		
		Sync::end($in['customer_id']);

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		// $in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if($in['customer_link']){
			$v->field('link_type', 'Type of link', 'required');
		}
		return $this -> addCustomer_validate($in);

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addCustomer_validate(&$in)
	{
		$in['c_email'] = trim($in['c_email']);
		$v = new validation($in);
		$v->field('name', 'Name', 'required');

		if (ark::$method == 'updateCustomer') {
			$our_ref_val=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_COMPANY_NUMBER' ");
		  	if($in['our_reference'] && $our_ref_val){
		 		$v->field('our_reference','Company Nr.',"unique[customers.our_reference.(customer_id!='".$in['customer_id']."')]");
		  	}
	  	}


		if($in['c_email']){
			$v->field('c_email','Email',"email:unique[customers.c_email.(customer_id!='".$in['customer_id']."')]");			
		}
		
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_field(&$in)
	{
		$this->db->query("DELETE FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['customer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendCustomerToZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$vars_post = array();
		$data = $this->db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ");
		if($data){
			$bar_code_start = null;
			$bar_code_end = null;
			$custom_fields = $this->db->query("SELECT * FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
			while($custom_fields->next()){
				if($custom_fields->f('field_id') == 1){
					$bar_code_start = $custom_fields->f('value');
				}
				if($custom_fields->f('field_id') == 2){
					$bar_code_end = $custom_fields->f('value');
				}
			}
			//asdkauas
			$primary = array();
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
			if($address) {
				$primary = array(
					"Unformatted"=> null,
			    "Label"=> null,
			    "Building"=> null,
			    "Street"=> $address->f('address'),
			    "PostalCode"=> $address->f('zip'),
			    "City"=> $address->f('city'),
			    "State"=> $address->f('state_id'),
			    "Country"=> get_country_name($address->f('country_id')),
			    "CountryCode"=> get_country_code($address->f('country_id'))
			  );
			}
			$test = array();
			$vars_post = array(
				'Id'											=> (string)$in['customer_id'],
				"Name"										=> $data->f('name'),
			  "VatNumber"								=> $data->f('btw_nr') ? $data->f('btw_nr') : null,
			  "TimeZone"								=> null,
			  "Note"										=> null,
			  "PictureURI"							=> null,
			  "Importance"							=> null,
			  "Blog"										=> null,
			  "WebSite"									=> $data->f('website') ? $data->f('website') : null,
			  "FixedPhoneNumber"				=> $data->f('comp_phone') ? $data->f('comp_phone') : null,
			  "MobilePhoneNumber"				=> $data->f('other_phone') ? $data->f('other_phone') : null,
			  "FaxNumber"								=> $data->f('comp_fax') ? $data->f('comp_fax') : null,
			  "EMailAddress"						=> $data->f('c_email') ? $data->f('c_email') : null,
			  "DeliveryPostalAddress"		=> null,
			  "InvoicePostalAddress"		=> null,
			  "PostalAddress"						=> $primary,
			  "Fields"									=> array(
					array(
					  "Name"								=> "Reference",
					  "Value"								=> $data->f('our_reference')
					),
					array(
					  "Name"								=> "BarcodeFrom",
					  "Value"								=> $bar_code_start
					),
					array(
					  "Name"								=> "BarcodeTo",
					  "Value"								=> $bar_code_end
					)
			  ),
			  "Links"										=> $test,
			  "Categories"							=> null
			);

			$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'put',$vars_post);
			console::log($put);
			$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->update_financial_valid($in)){
			return false;
		}
		$apply_fix_disc = 0;
		if($in['apply_fix_disc']){$apply_fix_disc = 1;}
		$apply_line_disc = 0;
		if($in['apply_line_disc']){$apply_line_disc = 1;}
		$check_vat_number = $this->db->field("SELECT check_vat_number FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($in['btw_nr']!=$check_vat_number){
			$check_vat_number = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET
											btw_nr								=	'".$in['btw_nr']."',
											bank_name							=	'".$in['bank_name']."',
											payment_term					=	'".$in['payment_term']."',
											payment_term_type 		= '".$in['payment_term_start']."',
											bank_bic_code					=	'".$in['bank_bic_code']."',
											fixed_discount				=	'".return_value($in['fixed_discount'])."',
											bank_iban							=	'".$in['bank_iban']."',
                      						vat_id								=	'".$in['vat_id']."',
											cat_id								=	'".$in['cat_id']."',
											no_vat								=	'".$in['no_vat']."',
											currency_id						= '".$in['currency_id']."',
											invoice_email					=	'".$in['invoice_email']."',
											attention_of_invoice		   =	'".$in['attention_of_invoice']."',
											invoice_note2					=	'".$in['invoice_note2']."',
											deliv_disp_note				=	'".$in['deliv_disp_note']."',
											external_id						=	'".$in['external_id']."',
											internal_language			=	'".$in['internal_language']."',
											apply_fix_disc				= '".$apply_fix_disc."',
											apply_line_disc				= '".$apply_line_disc."',
											line_discount 				= '".return_value($in['line_discount'])."',
											comp_reg_number 				= '".$in['comp_reg_number']."',
											vat_regime_id				= '".$in['vat_regime_id']."',
											siret						= '".$in['siret']."',
											check_vat_number 			= '".$check_vat_number."',
											identity_id                 = '".$in['identity_id']."'
											WHERE customer_id			= '".$in['customer_id']."' ");
		//quote_reference			=	'".$in['quote_reference']."',

		Sync::end($in['customer_id']);
		// msg::$success = gm("Changes have been saved.");
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial_valid(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		if($in['invoice_email']){
			$v->field('invoice_email', 'Type of link', 'email');
		}

		return $v->run();
	}

	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 * @author PM
	 */
	function contact_update(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->contact_update_validate($in)){
			return false;
		}
		if(isset($in['birthdate']) && !empty($in['birthdate']) ){
			$in['birthdate'] = strtotime($in['birthdate']);
		}
		$position = implode(',', $in['position']);
		$pos = '';
		foreach ($in['position_n'] as $key => $value) {
			$pos .= $value['name'].',';
		}
		$pos = addslashes(rtrim($pos,','));
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET
                                            customer_id	=	'".$in['customer_id']."',
                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$position."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title']."',
										    e_title		=	'".$in['e_title']."',
											fax			=	'".$in['fax']."',
											company_name=	'".$in['customer']."',
											title_name	=	'".$in['title_name']."',
											position_n  = 	'".$pos."',
										    last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."'");
		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}

		Sync::end($in['contact_id']);

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}
		if(!$in['customer_id']){
			$this->removeFromAddress($in);
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],'contact_id',$in['contact_id']);

		//save extra fields values
		$this->update_custom_contact_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		// $in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}

		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return true
	 * @param array $in
	 * @author PM
	 **/
	function contact_add_validate(&$in)
	{
		$in['email'] = trim($in['email']);
		$v = new validation($in);
		$v->field('firstname', 'First name', 'required');
		$v->field('lastname', 'Last name', 'required');
		if ($in['do']=='company-xcustomer_contact-customer-contact_add')
		{
			$v->field('email', 'Email', 'email:unique[customer_contacts.email]');
		}
		else
		{
			$v->field('email','Email',"email:unique[customer_contacts.email.(contact_id!='".$in['contact_id']."')]");
		}
		return $v->run();

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function removeFromAddress(&$in)
	{
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	function currencyRate($currency=''){
		if(empty($currency)){
			$currency = "USD";
		}
		$separator = ACCOUNT_NUMBER_FORMAT;
		$into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		return currency::getCurrency($currency, $into, 1,$separator);
	}
	function uploadify(&$in)
	{
	    $response=array();
	    if (!empty($_FILES)) {
	      $tempFile = $_FILES['Filedata']['tmp_name'];
	      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
	      $targetPath = 'upload/'.DATABASE_NAME;
	      $in['name'] = 'o_logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
	      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
	      // Validate the file type
	      $fileTypes = array('jpg','jpeg','gif'); // File extensions
	      $fileParts = pathinfo($_FILES['Filedata']['name']);
	      @mkdir(str_replace('//','/',$targetPath), 0775, true);
	      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
	        move_uploaded_file($tempFile,$targetFile);
	        global $database_config;

	        $database_2 = array(
	          'hostname' => $database_config['mysql']['hostname'],
	          'username' => $database_config['mysql']['username'],
	          'password' => $database_config['mysql']['password'],
	          'database' => DATABASE_NAME,
	        );
	        $db_upload = new sqldb($database_2);
	        $logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
	        if($logo == ''){
	          if(defined('ACCOUNT_LOGO')){
	            $db_upload->query("UPDATE settings SET
	                               value = 'upload/".DATABASE_NAME."/".$in['name']."'
	                               WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
	          }else{
	            $db_upload->query("INSERT INTO settings SET
	                               value = 'upload/".DATABASE_NAME."/".$in['name']."',
	                               constant_name='ACCOUNT_LOGO_ORDER' ");
	          }
	        }
	        $response['success'] = 'success';
	        echo json_encode($response);
	        // ob_clean();
	      } else {
	        $response['error'] = gm('Invalid file type.');
	        echo json_encode($response);
	        // echo gm('Invalid file type.');
	      }
	    }
	}
	  function default_pdf_format_header(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
	      $this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_HEADER_PDF_FORMAT'");
	      $this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_PDF_MODULE'");
	    }
	      global $config;
	      $this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");
	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_body(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_BODY_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_PDF_MODULE'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_footer(&$in)
	  {

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");
	      }else{
	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_FOOTER_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_PDF_MODULE'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function reset_data(&$in){
	    if($in['header']){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
	    }elseif($in['footer']){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
	    }
	    $result=array("var_data" => $variable_data);
	    json_out($result);
	    return true;
	  }

	  function pdfSaveData(&$in){

	    if($in['header']=='header'){
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order' AND type='".$in['header']."' AND initial='1' ");
	      $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");
	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order',
	                          type='".$in['header']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1' ");
	      }
	    }else{
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order' AND type='".$in['footer']."' AND initial='1' ");
	      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order',
	                          type='".$in['footer']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1' ");
	      }
	    }
	    msg::success ( gm('Data saved'),'success');
	    return true;
	  }
	   function default_pdf_format_header_delivery(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_DELIVERY_PDF' ");
	      $this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_DELIVERY_HEADER_PDF_FORMAT'");
	      $this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_DELIVERY_PDF_MODULE'");
	    }
	      global $config;
	      $this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");
	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_body_delivery(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_DELIVERY_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_DELIVERY_BODY_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_DELIVERY_PDF_MODULE'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_footer_delivery(&$in)
	  {

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");
	      }else{
	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_DELIVERY_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_DELIVERY_FOOTER_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_DELIVERY_PDF_MODULE'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function reset_data_delivery(&$in){
	    if($in['header']){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
	    }elseif($in['footer']){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
	    }
	    $result=array("var_data" => $variable_data);
	    json_out($result);
	    return true;
	  }

	  function pdfSaveData_delivery(&$in){

	    if($in['header']=='header'){
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order_delivery' AND type='".$in['header']."' AND initial='1' ");
	      $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");
	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order_delivery',
	                          type='".$in['header']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1' ");
	      }
	    }else{
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order_delivery' AND type='".$in['footer']."' AND initial='1' ");
	      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order_delivery',
	                          type='".$in['footer']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1' ");
	      }
	    }
	    msg::success ( gm('Data saved'),'success');
	    return true;
	  }
	  function Convention(&$in){

			if($in['ACCOUNT_ORDER_START']){
				$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_ORDER_START' ");
				if($this->db->move_next()){
					$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_ORDER_START']."' WHERE constant_name='ACCOUNT_ORDER_START' ");
				}else{
					$this->db->query("INSERT INTO settings SET value='".$in['ACCOUNT_ORDER_START']."', constant_name='ACCOUNT_ORDER_START' ");
				}
				msg::success ( gm('Settings updated'),'success');
			}

			if(!defined('ACCOUNT_ORDER_DIGIT_NR')){
				$this->db->query("INSERT INTO settings SET value='".$in['ACCOUNT_ORDER_DIGIT_NR']."', constant_name='ACCOUNT_ORDER_DIGIT_NR', module='billing' ");
					msg::success ( gm('Setting added'),'success');
				}else{
					$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_ORDER_DIGIT_NR']."' WHERE constant_name='ACCOUNT_ORDER_DIGIT_NR'");
					msg::success ( gm('Settings updated'),'success');
				}

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_ORDER_REF' ");
			if($in['CHECKED']){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_ORDER_REF' ");
			}
		      $this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_ORDER_WEIGHT' ");
		      if($in['WEIGHT_CHECKED']){
		        $this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_ORDER_WEIGHT' ");
		      }
			$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_ORDER_DEL']."' WHERE constant_name='ACCOUNT_ORDER_DEL' ");
			msg::success ( gm('Changes saved'),'success');

		return true;
	}
	function save_language(&$in){
			switch ($in['languages']){
				case '1':
					$lang = '';
					break;
				case '2':
					$lang = '_2';
					break;
				case '3':
					$lang = '_3';
					break;
				case '4':
					$lang = '_4';
					break;
				default:
					$lang = '';
					break;
			}
			if($in['languages']>=1000) {
				$lang = '_'.$in['languages'];
			}
			$this->db->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='order_note".$lang."' AND default_main_id='0' ");

		msg::success ( gm('Changes saved'),'success');
		return true;
	}
	function default_language(&$in){
	   msg::success ( gm("Changes have been saved."),'success');
	    return true;

	}
	function label_update(&$in){

      $fields = '';
      $table = 'label_language_order';

      $exist = $this->dbu->query("SELECT label_language_id FROM $table WHERE label_language_id='".$in['label_language_id']."' ");
      if($exist->next()){
        $this->dbu->query("UPDATE $table SET
				stock_dispatching_note		 		= '".$in['STOCK_DISPATCHING_NOTE']."',     
				stock_dispatching	         		= '".$in['STOCK_DISPATCHING']."',
				invoice_note		 				= '".$in['INVOICE_NOTE']."',     
				invoice	         					='".$in['INVOICE']."',
				quote_note		 					='".$in['QUOTE_NOTE']."',     
				quote	             				='".$in['QUOTE']."',
				order_note		 					='".addslashes($in['ORDER_NOTE'])."',     
				`order`             				='".$in['ORDER']."',
				p_order_note		 				='".addslashes($in['P_ORDER_NOTE'])."',     
				p_order	         					='".$in['P_ORDER']."',
				article	         					='".$in['ARTICLE']."',
				reference	         				='".$in['REFERENCE']."',
				billing_address	 					='".$in['BILLING_ADDRESS']."',
				date	             				='".$in['DATE']."',
				customer	         				='".$in['CUSTOMER']."',
				item	             				='".$in['ITEM']."',
				quantity	         				='".$in['QUANTITY']."',
				unitmeasure	     					='".$in['UNITMEASURE']."',
				unit_price	     					='".$in['UNIT_PRICE']."',
				amount	         					='".$in['AMOUNT']."',
				subtotal	         				='".$in['SUBTOTAL']."',
				discount	         				='".$in['DISCOUNT']."',
				vat	             					='".$in['VAT']."',
				payments         					='".$in['PAYMENTS']."',
				amount_due	     					='".$in['AMOUNT_DUE']."',
				grand_total	     					='".$in['GRAND_TOTAL']."',
				notes	             				='".$in['NOTES']."',
				bank_details				 		='".$in['BANKD']."',
				duedate			 					='".$in['DUEDATE']."',
				bank_name			 				='".$in['BANK_NAME']."',
				iban				 				='".$in['IBAN']."',
				bic_code			 				='".$in['BIC_CODE']."',
				phone				 				='".$in['PHONE']."',
				fax				 					='".$in['FAX']."',
				url				 					='".$in['URL']."',
				email				 				='".$in['EMAIL']."',
				our_ref								='".$in['OUR_REF']."',
				your_ref			 				='".$in['YOUR_REF']."',
				vat_number		 					='".$in['VAT_NUMBER']."',
				CUSTOMER_REF		 				='".$in['CUSTOMER_REF']."',
				gov_taxes			 				='".$in['GOV_TAXES']."',
				gov_taxes_code	 					='".$in['GOV_TAXES_CODE']."',
				gov_taxes_type	 					='".$in['GOV_TAXES_TYPE']."',
				sale_unit	         				='".$in['SALE_UNIT']."',
				package	         					='".$in['PACKAGE']."',
				shipping_price	 					='".$in['SHIPPING_PRICE']."',
				article_code	     				='".$in['ARTICLE_CODE']."',
				delivery_date	     				='".$in['DELIVERY_DATE']."',
				entry_date	     					='".$in['ENTRY_DATE']."',
				delivery_note	 	 				='".$in['DELIVERY_NOTE']."',
				entry_note	 	 					='".$in['ENTRY_NOTE']."',
				pick_up_from_store 					='".$in['PICK_UP_FROM_STORE']."',
				page				 				='".$in['PAGE']."'
                WHERE label_language_id='".$in['LABEL_LANGUAGE_ID']."'");
      }else{
        $this->dbu->query("UPDATE $table SET
				stock_dispatching_note		 		='".$in['STOCK_DISPATCHING_NOTE']."',     
				stock_dispatching	         		='".$in['STOCK_DISPATCHING']."',
				invoice_note		 				='".$in['INVOICE_NOTE']."',     
				invoice	         					='".$in['INVOICE']."',
				quote_note		 					='".$in['QUOTE_NOTE']."',     
				quote	             				='".$in['QUOTE']."',
				order_note		 					='".addslashes($in['ORDER_NOTE'])."',     
				`order`	             				='".$in['ORDER']."',
				p_order_note		 				='".addslashes($in['P_ORDER_NOTE'])."',     
				p_order	         					='".$in['P_ORDER']."',
				article	         					='".$in['ARTICLE']."',
				reference	         				='".$in['REFERENCE']."',
				billing_address	 					='".$in['BILLING_ADDRESS']."',
				date	             				='".$in['DATE']."',
				customer	         				='".$in['CUSTOMER']."',
				item	             				='".$in['ITEM']."',
				quantity	         				='".$in['QUANTITY']."',
				unitmeasure	     					='".$in['UNITMEASURE']."',
				unit_price	     					='".$in['UNIT_PRICE']."',
				amount         						='".$in['AMOUNT']."',
				subtotal	         				='".$in['SUBTOTAL']."',
				discount	         				='".$in['DISCOUNT']."',
				vat	             					='".$in['VAT']."',
				payments	         				='".$in['PAYMENTS']."',
				amount_due	     					='".$in['AMOUNT_DUE']."',
				grand_total	     					='".$in['GRAND_TOTAL']."',
				notes	             				='".$in['NOTES']."',
				bank_details				 		='".$in['BANKD']."',
				duedate			 					='".$in['DUEDATE']."',
				bank_name			 				='".$in['BANK_NAME']."',
				iban				 				='".$in['IBAN']."',
				bic_code			 				='".$in['BIC_CODE']."',
				phone				 				='".$in['PHONE']."',
				fax				 					='".$in['FAX']."',
				url				 					='".$in['URL']."',
				email				 				='".$in['EMAIL']."',
				our_ref								='".$in['OUR_REF']."',
				your_ref			 				='".$in['YOUR_REF']."',
				vat_number		 					='".$in['VAT_NUMBER']."',
				CUSTOMER_REF		 				='".$in['CUSTOMER_REF']."',
				gov_taxes			 				='".$in['GOV_TAXES']."',
				gov_taxes_code	 					='".$in['GOV_TAXES_CODE']."',
				gov_taxes_type	 					='".$in['GOV_TAXES_TYPE']."',
				sale_unit	         				='".$in['SALE_UNIT']."',
				package	         					='".$in['PACKAGE']."',
				shipping_price	 					='".$in['SHIPPING_PRICE']."',
				article_code	     				='".$in['ARTICLE_CODE']."',
				delivery_date	     				='".$in['DELIVERY_DATE']."',
				entry_date	     					='".$in['ENTRY_DATE']."',
				delivery_note	 	 				='".$in['DELIVERY_NOTE']."',
				entry_note	 	 					='".$in['ENTRY_NOTE']."',
				pick_up_from_store 					='".$in['PICK_UP_FROM_STORE']."',
				page				 				='".$in['PAGE']."'
                WHERE label_language_id='".$in['LABEL_LANGUAGE_ID']."'");
    }
      /*msg::success ( gm('Labels has been successfully updated'),'success');*/
      msg::success ( gm("Changes have been saved."),'success');
      // $in['failure']=false;
      
      return true;
    }
      function default_message(&$in)
	  {
	    $set = "text  = '".$in['text']."', ";
	    /*if($in['use_html']){*/
	      $set1 = "html_content  = '".$in['html_content']."' ";
	    /*}*/
	    $this->dbu->query("UPDATE sys_message SET
	          subject = '".$in['subject']."',
	          ".$set."
	          ".$set1."
	          WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
	    /*$this->dbu->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
	    msg::success( gm('Message has been successfully updated'),'success');
	    // json_out($in);
	    return true;
	  }
	function default_message_order(&$in)
	{
		if($in['use_html']){
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					html_content	= '".$in['html_content']."'
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		}else{
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					text	= '".$in['html_content']."'
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		}
		 msg::success( gm('Message has been successfully updated'),'success');
		return true;
	}
	function default_message_po(&$in)
	  {
	    $set = "text  = '".$in['text_po']."', ";
	    /*if($in['use_html']){*/
	      $set1 = "html_content  = '".$in['html_content_po']."' ";
	    /*}*/
	    $this->dbu->query("UPDATE sys_message SET
	          subject = '".$in['subject_po']."',
	          ".$set."
	          ".$set1."
	          WHERE name='".$in['name_po']."' AND lang_code='".$in['lang_code']."' ");
	    /*$this->dbu->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
	    msg::success( gm('Message has been successfully updated'),'success');
	    // json_out($in);
	    return true;
	  }
	function default_message_order_po(&$in)
	{
		if($in['use_html']){
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject_po']."',
					html_content	= '".$in['html_content_po']."'
					WHERE name='".$in['name_po']."' AND lang_code='".$in['lang_code']."' ");
		}else{
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject_po']."',
					text	= '".$in['html_content_po']."'
					WHERE name='".$in['name_po']."' AND lang_code='".$in['lang_code']."' ");
		}
		 msg::success( gm('Message has been successfully updated'),'success');
		return true;
	}
	function update_default_email(&$in)
	{
		if(!$this->validate_default_email($in)){
			msg::error ( gm('Write email'),'error');
			return false;
		}
		$this->db->query("UPDATE default_data SET default_name='".$in['default_name']."', value='".$in['email_value']."' WHERE type='order_email' ");
		$this->db->query("UPDATE default_data SET default_name='".$in['po_default_name']."', value='".$in['po_email_value']."' WHERE type='p_order_email' ");
		msg::success ( gm('Default email updated.'),'success');
		return true;
	}
	function validate_default_email(&$in)
	{
		$v = new validation($in);
		$v->field('value', 'Email', 'email');
		$v->field('po_value', 'Email', 'email');

		return $v->run();
	}
	function label(&$in){
		$v = new validation($in);
		$v->field('text', 'Field', 'required', gm("Invalid Field Label"));

    	$this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='NOT_COPY_ARTICLE_ORD' ");

    	if($in['not_copy_article_ord']){
       		$this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='NOT_COPY_ARTICLE_ORD' ");
    	}

		if(!$v->run()){
			return false;
		}
		$this->db->query("UPDATE `settings` SET `long_value` = '".$in['text']."' WHERE `constant_name` = 'ORDER_FIELD_LABEL'");
		msg::success ( gm('Field label has been updated'),'success');
		return true;
	}
	  function atach(&$in)
	  {
	    $response=array();
	    if (!empty($_FILES)) {
	      $tempFile = $_FILES['Filedata']['tmp_name'];
	      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
	/*      $targetPath = $in['path'];
	*/      $in['name'] = $_FILES['Filedata']['name'];
	      $targetPath = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/orders';

	      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

	      // Validate the file type
	      $fileTypes = array('jpg','jpeg','gif','pdf','docx'); // File extensions
	      $fileParts = pathinfo($_FILES['Filedata']['name']);
	      @mkdir(str_replace('//','/',$targetPath), 0775, true);
	      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

	        move_uploaded_file($tempFile,$targetFile);
	        global $database_config;

	        $database_2 = array(
	          'hostname' => $database_config['mysql']['hostname'],
	          'username' => $database_config['mysql']['username'],
	          'password' => $database_config['mysql']['password'],
	          'database' => DATABASE_NAME,
	        );
	        $db_upload = new sqldb($database_2);
	        $file_id = $db_upload->insert("INSERT INTO attached_files SET `path` = 'upload/".DATABASE_NAME."/orders/', name = '".$in['name']."', type='3' ");
	        $response['success'] = 'success';
	        echo json_encode($response);
	        // ob_clean();
	      } else {
	        $response['error'] = gm('Invalid file type.');
	        echo json_encode($response);
	        // echo gm('Invalid file type.');
	      }
	    }
	  }
	  function deleteatach(&$in){

	    $this->dbu->query("DELETE FROM attached_files WHERE file_id='".$in['id']."' AND name='".$in['name']."' ");
	    msg::success ( gm("Changes have been saved."),'success');
	    return true;
	  }
	  function defaultcheck(&$in){
	    if($in['default_id']==1){
	      $this->dbu->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
	      msg::success ( gm("Changes have been saved."),'success');
	    }else{
	      $this->dbu->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
	      msg::success ( gm("Changes have been saved."),'success');
	    }
	        return true;
	  }
	 function setSetting(&$in)
	{
/*		if(!$this->setSetting_validate($in)){
			return false;
		}
		switch ($in['setting']) {
			case 1:
				$const = 'ACTIVATE_NOTIFICATION_MOBILE_ORDER';
				break;
			case 2:
				$const = 'USER_NOTIFICATION_MOBILE_ORDER';
				break;
			case 3:
				$const = 'SHOW_MY_ORDERS_ONLY';
				break;
			case 4:
				$const = 'ALLOW_ORDER_DROP_BOX';
				break;
			default:
				$const = null;
				break;
		}
		if(!$const){
			return;
		}*/
/*		$this->db->query("SELECT * FROM settings WHERE constant_name='ALLOW_ORDER_DROP_BOX' ");
		if($this->db->next()){*/
			$this->db->query("UPDATE settings SET value='".$in['allow']."' WHERE constant_name='ALLOW_ORDER_DROP_BOX' ");
		/*}else{
			$this->db->query("INSERT INTO settings SET value='".$in['allow']."', constant_name='ALLOW_ORDER_DROP_BOX' ");
		}
*/		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function changedelivery(&$in){
		$this->db->query("UPDATE settings SET value='".$in['delivery_type']."' WHERE constant_name='ORDER_DELIVERY_STEPS'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function setMobile(&$in){

		$this->db->query("UPDATE settings SET value='".$in['active']."' WHERE constant_name='ACTIVATE_NOTIFICATION_MOBILE_ORDER'");
		$this->db->query("UPDATE settings SET value='".$in['user_id']."' WHERE constant_name='USER_NOTIFICATION_MOBILE_ORDER'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function order_note_list(&$in){
		$is_set=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='SHOW_MY_ORDERS_ONLY' ");
	    if(is_null($is_set)){
	      $this->db->insert("INSERT INTO settings SET constant_name='SHOW_MY_ORDERS_ONLY',value='".$in['list']."',type='1' ");
	    }else{
	      $this->db->insert("UPDATE settings SET value='".$in['list']."' WHERE constant_name='SHOW_MY_ORDERS_ONLY'  ");
	    } 
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	  function set_default_logo(&$in)
	  {

	      $this->dbu->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
	      msg::success (gm("Default logo set."),'success');
	      json_out($in);
	      return true;
	  }

	  function postRegister(&$in){
        global $config;

        $env_usr = $config['postgreen_user'];
        $env_pwd = $config['postgreen_pswd'];

        $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
        $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
        $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
        $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
        $soap->__setSoapHeaders(array($objHeader_Session_Outside));
        $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$in['user'],'password'=>$in['pass']));

        if($res->action->data[0]->_ == '000'){
          $post_id=$this->dbu->insert("INSERT INTO apps SET
                                                  main_app_id='0',
                                                  name='PostGreen',
                                                  api='".$in['user']."',
                                                  type='main',
                                                  active='1' ");
          $this->dbu->query("INSERT INTO apps SET main_app_id='".$post_id."', api='".$in['pass']."' ");
          /*msg::success(gm("PostGreen service activated"),"success");*/
          msg::success ( gm("Changes have been saved."),'success');
        }else{
          msg::error(gm('Incorrect credentials provided'),"error");

        }
        return true;
    }

    function postIt(&$in){
	      global $config;

	      $id=$in['id'];

	      $order=$this->dbu->query("SELECT * FROM pim_orders WHERE order_id='".$id."' ");

	      $buyer_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".$order->f('customer_country_id')."' ");
	      $seller_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".ACCOUNT_BILLING_COUNTRY_ID."' ");

	      $in['order_id']=$id;
	      $in['lid'] = $order->f('email_language');
		  if(!$in['lid']){
				$in['lid'] = 1;
		  }
		  if($order->f('pdf_layout')){
		  	$in['type']=$order->f('pdf_layout');
		  	$in['logo']=$order->f('pdf_logo');
		  }else{
		  	$in['type']=ACCOUNT_ORDER_PDF_FORMAT;
		  }
	      $this->generate_pdf($in);
	      $in['attach_file_name'] = 'order_.pdf';
	      $doc_name='order_'.$order->f('serial_number').'.pdf';

	      $handle = fopen($in['attach_file_name'], "r");   
	      $contents = fread($handle, filesize($in['attach_file_name'])); 
	      fclose($handle);                               
	      $decodeContent = base64_encode($contents);

	      $filename ="addresses.csv";
	      ark::loadLibraries(array('PHPExcel'));
	      $objPHPExcel = new PHPExcel();
	      $objPHPExcel->getProperties()->setCreator("Akti")
	               ->setLastModifiedBy("Akti")
	               ->setTitle("Addresses")
	               ->setSubject("Addresses")
	               ->setDescription("Addresses")
	               ->setKeywords("Addresses")
	               ->setCategory("Addresses");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', "TITLE")
	            ->setCellValue('B1', "FIRST_NAME")
	            ->setCellValue('C1', "LAST_NAME")
	            ->setCellValue('D1', "EMAIL")
	            ->setCellValue('E1', "PHONE")
	            ->setCellValue('F1', "MOBILE")
	            ->setCellValue('G1', "FAX")
	            ->setCellValue('H1', "COMPANY")
	            ->setCellValue('I1', "ADDRESS_LINE_1")
	            ->setCellValue('J1', "ADDRESS_LINE_2")
	            ->setCellValue('K1', "ADDRESS_LINE_3")
	            ->setCellValue('L1', "ADDRESS_POSTCODE")
	            ->setCellValue('M1', "ADDRESS_CITY")
	            ->setCellValue('N1', "ADDRESS_STATE")
	            ->setCellValue('O1', "ADDRESS_COUNTRY");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A2', "")
	            ->setCellValue('B2', "")
	            ->setCellValue('C2', "")
	            ->setCellValue('D2', $order->f('customer_email'))
	            ->setCellValue('E2', $order->f('customer_phone'))
	            ->setCellValue('F2', "")
	            ->setCellValue('G2', $order->f('customer_fax'))
	            ->setCellValue('H2', $order->f('customer_name'))
	            ->setCellValue('I2', $order->f('customer_address'))
	            ->setCellValue('J2', "")
	            ->setCellValue('K2', "")
	            ->setCellValue('L2', $order->f('customer_zip'))
	            ->setCellValue('M2', $order->f('customer_city'))
	            ->setCellValue('N2', "")
	            ->setCellValue('O2', $buyer_country);

	      $objPHPExcel->getActiveSheet()->setTitle('Addresses');
	      $objPHPExcel->setActiveSheetIndex(0);
	      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
	      $objWriter->setDelimiter(';');
	      $objWriter->save($filename);

	      $handle_csv = fopen($filename, "r");   
	      $contents_csv = fread($handle_csv, filesize($filename)); 
	      fclose($handle_csv);                               
	      $csvContent   = base64_encode($contents_csv);
	     
	      $env_usr = $config['postgreen_user'];
	      $env_pwd = $config['postgreen_pswd'];
	      $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	      $panel_usr=$postg_data->f("api");
	      $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
	      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
	      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
	      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
	      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
	      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
	      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));

	      $params = new \SoapVar('
	        <order_create_IN>
	          <action type="string">ORDER_CREATE</action> 
	          <attachment_list type="stringlist"></attachment_list> 
	          <background type="string"></background> 
	          <content_parameters type="indstringlist"></content_parameters> 
	          <csv type="file" extension="csv"> 
	            <nir:data>'.$csvContent.'</nir:data> 
	          </csv> 
	          <document type="file" extension="pdf"> 
	            <nir:data>'.$decodeContent.'</nir:data> 
	          </document> 
	          <document_name type="string">'.$doc_name.'</document_name> 
	          <get_proof type="string">NO</get_proof> 
	          <identifier type="indstringlist"> 
	            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data> 
	          </identifier> 
	          <insert_list type="indstringlist"></insert_list> 
	          <mailing_type type="string">NONE</mailing_type> 
	          <order_parameters type="indstringlist"></order_parameters> 
	          <page_list type="stringlist"></page_list> 
	          <return_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </return_address> 
	          <sender_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </sender_address> 
	          <service_profile type="indstringlist"> 
	            <nir:data key="service_id">CUSTOM_ENVELOPE</nir:data> 
	            <nir:data key="print_mode">1SIDE</nir:data>
	            <nir:data key="envelop_type">CUSTOM</nir:data>
	            <nir:data key="paper_weight">80g</nir:data>
	            <nir:data key="print_color">COLOR</nir:data>
	            <nir:data key="mail_type">EXPRESS</nir:data>
	            <nir:data key="address_page">EXTRA_PAGE</nir:data>
	            <nir:data key="validation">NO</nir:data>
	          </service_profile> 
	        </order_create_IN>',
	         XSD_ANYXML);
	      
	      $result = $soap->order_create($params);
	      if($result->action->data[0]->_ == '000'){
	        $this->dbu->query("UPDATE pim_orders SET postgreen_id='".$result->order_id->_."' WHERE order_id='".$id."' ");
	        $in['sent_date']= time();
	        $this->mark_sent($in);
	        $msg_txt =gm('Order successfully exported');
	      }else{
	        msg::error($result->action->data[1]->_,"error");
	        return true;
	      }

	      unlink($filename);

	      msg::success($msg_txt,"success");
	      if($in['related']=='view'){        
	        // return true;
	      }else{
	        unset($in['order_id']); 
	      }
	      return true;
	}

	function postOrderData(&$in){
	    global $config;
	      $env_usr = $config['postgreen_user'];
	      $env_pwd = $config['postgreen_pswd'];
	      $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	      $panel_usr=$postg_data->f("api");
	      $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
	      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
	      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
	      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
	      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
	      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
	      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));
	      
	      $order_id=$this->dbu->field("SELECT postgreen_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
	      $params = new \SoapVar('
	        <order_details_IN>
	          <action type="string">ORDER_DETAILS</action>
	          <get_billing>NO</get_billing>
	          <get_document>NO</get_document>
	          <get_proof>NO</get_proof>
	          <identifier type="indstringlist">
	            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
	          </identifier>
	          <order_id type="string">'.$order_id.'</order_id>
	        </order_details_IN>',
	         XSD_ANYXML);

	      $result=$soap->order_details($params);
	      $PGreen_status=array(
	        '100'   => gm('PostGreen Received'),
	        '140'   => gm('PostGreen Waiting for validation'),
	        '-140'  => gm('PostGreen Rejected'),
	        '160'   => gm('PostGreen Saved'),
	        '200'   => gm('PostGreen Confirmed'),
	        '210'   => gm('PostGreen Processing'),
	        '300'   => gm('PostGreen Processed'),
	        '400'   => gm('PostGreen Validated'),
	        '500'   => gm('PostGreen Printed'),
	        '600'   => gm('PostGreen Posted'),
	        '700'   => gm('PostGreen Sent'),
	        '800'   => gm('PostGreen Arrived'),
	        '810'   => gm('PostGreen Opened'),
	        '900'   => gm('PostGreen Canceled')
	      );
	      $post_status=array('status'=>gm('Post status').": ".$PGreen_status[$result->order_info->row->col[3]->data]);
	      if($in['related']=='minilist'){
	        json_out($post_status);
	      }else if($in['related']=='status'){
	        $in['status']=$post_status['status'];
	        json_out($in);
	      }else{
	        return json_encode($post_status);
	      }   
	}

}



?>