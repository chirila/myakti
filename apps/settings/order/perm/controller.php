<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps;
global $p_access;
perm::controller('all', 'admin',in_array($apps['order'], $p_access));
perm::controller('all', 'user',in_array($apps['order'], $p_access));
if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
	perm::controller(array('order_note','xdefault_message','xdefault_format','xlanguage','article_settings','article_price_categories','article_price_categories_list','article_taxes','article_taxes_list','stock_settings','article_taxes_list','article_price_categories_list','xlabel'), 'user', false);
}
if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::controller('all', 'admin',false);
	perm::controller('all', 'user',false);
}