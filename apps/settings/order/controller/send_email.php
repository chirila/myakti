<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
//global $config;
// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
if($in['order_id']){
	$serial_number = $db->field("SELECT serial_number FROM pim_orders WHERE order_id ='".$in['order_id']."' ");
	$email_language = $db->field("SELECT email_language FROM pim_orders WHERE order_id ='".$in['order_id']."' ");
	if(!$email_language){
		$email_language = 1;
	}
	$customer_id = $db->field("SELECT customer_id from pim_orders WHERE order_id ='".$in['order_id']."' ");
	/*$order_data = $db->query("SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles
					INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
					WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");*/
	$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number
	             FROM pim_orders
	             INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_orders.customer_id
	             WHERE pim_orders.order_id='".$in['order_id']."' AND customer_contacts.active ='1'");

	$recipients=array();
	while ($tblinvoice_customer_contacts->move_next()){
		$in['recipients'][$tblinvoice_customer_contacts->f('contact_id')]=1;

		$recipient = array(
			'recipient_name'  			=> $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
			'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
			'recipient_id'    			=> $tblinvoice_customer_contacts->f('contact_id'),
		);

		array_push($recipients, $recipient);

	}
}elseif($in['p_order_id']){
	$serial_number = $db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id ='".$in['p_order_id']."' ");
	$customer_id = $db->field("SELECT customer_id from pim_p_orders WHERE p_order_id ='".$in['p_order_id']."' ");
	/*$order_data = $db->query("SELECT pim_p_order_articles.*, pim_p_orders_delivery.* FROM pim_p_order_articles
					INNER JOIN pim_p_orders_delivery ON pim_p_orders_delivery.order_articles_id=pim_p_order_articles.order_articles_id
					WHERE pim_p_orders_delivery.p_order_id='".$in['p_order_id']."' AND pim_p_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");*/
}

$customer_data = $db->query("SELECT c_email, customer_id, name FROM customers WHERE customer_id = '".$customer_id."'");
$customer_data->next();

$email =array(
	'customer_id'				=> $customer_data->f('customer_id'),
	'c_email'						=> $customer_data->f('c_email'),
	'is_c_email'				=> $customer_data->f('c_email') ? true : false,
	'c_email_ch'				=> $customer_data->f('c_email') ? true : false,
	'customer_name'			=> $customer_data->f('name'),
	'language_dd'	    	=> build_pdf_language_dd($in['lid'],1),
	'serial_number'	    => $serial_number,
	'order_id'					=> $in['order_id'],
	'do_next'						=> $in['order_id'] ? 'order-order-order-notice' : 'order-p_order-order-notice' ,
	'pdf_version_text'	=> $in['order_id'] ? gm('Include a pdf version of the delivery') : gm('Include a pdf version of the entry') ,
	'recipients'				=> $recipients,
	'include_pdf'				=> true,
	'lid'								=> $email_language,
);

json_out($email);
?>